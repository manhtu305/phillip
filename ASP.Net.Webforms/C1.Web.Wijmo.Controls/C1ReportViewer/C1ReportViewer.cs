﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Web;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Converters;
using C1.Web.Wijmo.Controls.C1ReportViewer.ReportService;
using System.Globalization;
using System.Web.UI.HtmlControls;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1ReportViewer", "wijmo")]

namespace C1.Web.Wijmo.Controls.C1ReportViewer
{
	using C1Splitter;
	using C1.Web.Wijmo.Controls.C1ReportViewer.ReportService;
	using C1.C1Preview;
	using System.Drawing.Design;
    using C1.C1Preview.Export;

	/// <summary>
	/// Delegate to be used with RegisterDocument method.
	/// </summary>
	public delegate object C1MakeDocumentDelegate();

	/// <summary>
	/// Represents a report in an ASP.NET Web page.
	/// </summary>
	[ParseChildren(true)]
	[ToolboxData("<{0}:C1ReportViewer Width=\"800px\" Height=\"600px\" runat=server></{0}:C1ReportViewer>")]
	[ToolboxBitmap(typeof(C1ReportViewer), "C1ReportViewer.png")]
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1ReportViewer.C1ReportViewerDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1ReportViewer.C1ReportViewerDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1ReportViewer.C1ReportViewerDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1ReportViewer.C1ReportViewerDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[LicenseProviderAttribute()]
	[WidgetDependencies(        
        typeof(WijReportViewer),
        typeof(C1Menu.C1Menu),
        "extensions.c1reportviewer.css",
		"extensions.c1reportviewer.js",
		ResourcesConst.C1WRAPPER_CONTROL
	)]//+3
	public class C1ReportViewer : C1TargetControlBase, ICultureControl
	{
		#region ** fields 
		internal static ReportCache _reportCache = new ReportCache();
		internal static bool _showInputParameters = true;//qq:remove
		internal bool _productLicensed = false;
		private bool _shouldNag;
		private C1ReportViewerToolbar _toolBar;
		private LocalizationOption _LocalizationOption;
        private PdfSecurity _pdfSecurity;
        private PdfSecuritySetting _pdfSecuritySetting;
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ReportViewer"/> class.
		/// </summary>
		public C1ReportViewer()
			: base("div")
		{
			VerifyLicense();
		}

		#endregion

		#region ** ctors for licensed C1.C1Report.C1Report and C1PrintDocument

		private static Dictionary<string, C1MakeDocumentDelegate> _makeDocumentDelegates = new Dictionary<string, C1MakeDocumentDelegate>();


		/// <summary>
		/// Removes a previously cached document from cache.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="reportName">Name of the report.</param>
		/// <returns>Returns true if the specified document has actually been removed from the cache.</returns>
		public static bool ClearCachedDocument(string fileName, string reportName)
		{
			string documentKey = KeyMaker.GetDocumentKeyByFileAndReportName(fileName, reportName);
			if (string.IsNullOrEmpty(documentKey))
			{
				DocIdentity di = new DocIdentity(fileName, reportName, null);
				documentKey = KeyMaker.CachedDocumentKey(di,
					_reportCache.GetShareBetweenSessions(),
					_showInputParameters, HttpContext.Current);
			}
			return ClearCachedDocument(documentKey);
		}

		/// <summary>
		/// Removes a previously cached document from cache.
		/// </summary>
		/// <param name="documentKey">The document key.</param>
		/// <returns>Returns true if the specified document has actually been removed from the cache.</returns>
		public static bool ClearCachedDocument(string documentKey)
		{			
			return CachedDocument.ClearCachedDocument(documentKey);
		}

		/// <summary>
		/// Registers an in-memory(memory-based) document
		/// </summary>
		/// <param name="documentName">Unique identifier of the document. 
		/// Make sure that this name does not conflict with already existent file-based/memory-based report names.</param>
		/// <param name="d">Delegate that will be called in order to generate document.</param>
		public static void RegisterDocument(string documentName, C1MakeDocumentDelegate d)
		{
			_makeDocumentDelegates[documentName] = d;
		}

		/// <summary>
		/// Remove in-memory document.
		/// </summary>
		/// <param name="documentName">Name of the document.</param>
		public static void UnRegisterDocument(string documentName)
		{
			if (string.IsNullOrEmpty(documentName))
				return;
			if (_makeDocumentDelegates.ContainsKey(documentName))
			{
				_makeDocumentDelegates.Remove(documentName);
			}
		}

		/// <summary>
		/// Determines whether an in-memory document is registered.
		/// </summary>
		/// <param name="documentName">Name of the document.</param>
		public static bool IsDocumentRegistered(string documentName) {
			if (string.IsNullOrEmpty(documentName))
				return false;
			return _makeDocumentDelegates.ContainsKey(documentName);
		}

		/// <summary>
		/// Determines whether a previously cached report or document is available.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="reportName">Name of the report.</param>
		public static bool HasCachedDocument(string fileName, string reportName)
		{
			DocIdentity di = new DocIdentity(fileName, reportName, null);
			string documentKey = KeyMaker.CachedDocumentKey(di, 
				_reportCache.GetShareBetweenSessions(), 
				_showInputParameters, HttpContext.Current);
			CachedDocument cdoc = CacheHandler.GetCachedDocument(documentKey);
			return cdoc != null;
		}

		/// <summary>
		/// Gets the previously registered in-memory document.
		/// Note, document will be automatically generated if needed.
		/// </summary>
		/// <param name="documentName">Name of the document.</param>
		/// <returns>Returns an in-memory document or returns null if document is not registered.</returns>
		public static object GetDocument(string documentName)
		{
			if (string.IsNullOrEmpty(documentName))
				return null;
			if (_makeDocumentDelegates.ContainsKey(documentName))
			{
				return _makeDocumentDelegates[documentName]();
			}
			return null;
		}

		/// <summary>
		/// Creates a new instance of <see cref="C1.C1Report.C1Report"/>
		/// with its properties adjusted to better work with <c>C1ReportViewer</c>.
		/// This method creates a licensed instance of <c>C1Report</c> provided
		/// it is invoked on a licensed instance of <see cref="C1ReportViewer"/>.
		/// </summary>
		/// <returns>The new instance of <see cref="C1.C1Report.C1Report"/>.</returns>
		/// <remarks>
		/// The following properties are adjusted on the created <see cref="C1.C1Report.C1Report"/>:
		/// <list type="bullet">
		/// <item>
		/// <see cref="C1.C1Report.C1Report.UseGdiPlusTextRendering"/> is set to <c>true</c>;
		/// </item>
		/// <item>
		/// <see cref="C1.C1Report.C1Report.EmfType"/> is set to <c>EmfType.EmfPlusOnly</c>;
		/// </item>
		/// <item>
		/// <see cref="C1.C1Report.C1Report.ColorizeHyperlinks"/> is set to <c>true</c>.
		/// </item>
		/// </list>
		/// </remarks>
		public static C1.C1Report.C1Report CreateC1Report()
		{
            C1.C1Report.C1Report rep = new C1.C1Report.C1Report("c1v01w7vFsMWqxJTDpMOCwprDo3bCmQ5uw7Q=");
			Util.AdjustNewDocOrReport(rep);
			return rep;
		}

		/// <summary>
		/// Creates a new instance of <see cref="C1.C1Preview.C1PrintDocument"/>
		/// with its properties adjusted to better work with <c>C1ReportViewer</c>.
		/// This method creates a licensed instance of <c>C1PrintDocument</c> provided
		/// it is invoked on a licensed instance of <see cref="C1ReportViewer"/>.
		/// </summary>
		/// <returns>The new instance of <see cref="C1.C1Preview.C1PrintDocument"/>.</returns>
		/// <remarks>
		/// The following properties are adjusted on the created <see cref="C1.C1Preview.C1PrintDocument"/>:
		/// <list type="bullet">
		/// <item>
		/// <see cref="C1.C1Preview.C1PrintDocument.UseGdiPlusTextRendering"/> is set to <c>true</c>;
		/// </item>
		/// <item>
		/// <see cref="C1.C1Preview.C1PrintDocument.EmfType"/> is set to <c>EmfType.EmfPlusOnly</c>.
		/// </item>
		/// </list>
		/// </remarks>
		public static C1.C1Preview.C1PrintDocument CreateC1PrintDocument()
		{
            C1.C1Preview.C1PrintDocument doc = new C1.C1Preview.C1PrintDocument("c1v01xJ3FmMSzxZLDgMaWw4DCjMKUw6B2wqXDm8Kp");
			Util.AdjustNewDocOrReport(doc);
			return doc;
		}

		/// <summary>
		/// Creates a new instance of <see cref="C1.C1Rdl.Rdl2008.C1RdlReport"/>
		/// with its properties adjusted to better work with <c>C1ReportViewer</c>.
		/// This method creates a licensed instance of <c>C1RdlReport</c> provided
		/// it is invoked on a licensed instance of <see cref="C1ReportViewer"/>.
		/// </summary>
		/// <returns>The new instance of <see cref="C1.C1Rdl.Rdl2008.C1RdlReport"/>.</returns>
		/// <remarks>
		/// The following properties are adjusted on the created <see cref="C1.C1Rdl.Rdl2008.C1RdlReport"/>:
		/// <list type="bullet">
		/// <item>
		/// <see cref="C1.C1Rdl.Rdl2008.C1RdlReport.UseGdiPlusTextRendering"/> is set to <c>true</c>;
		/// </item>
		/// </list>
		/// </remarks>
		public static C1.C1Rdl.Rdl2008.C1RdlReport CreateC1RdlReport()
		{
			C1.C1Rdl.Rdl2008.C1RdlReport rdl = new C1.C1Rdl.Rdl2008.C1RdlReport("c1v01wqHEhcOVxpXDuMO+w7/EqsO9w5FONQ==");
			Util.AdjustNewDocOrReport(rdl);
			return rdl;
		}
		#endregion

		#region ** properties

		/// <summary>
		/// Name for the exported file.
		/// </summary>
		[DefaultValue("")]
		[Json(true)]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1ReportViewer.ExportedFileName", "Name for the exported file.")]
		public string ExportedFileName
		{
			get
			{
				return GetPropertyValue<string>("ExportedFileName", "");
			}
			set
			{
				SetPropertyValue<string>("ExportedFileName", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[Layout(LayoutType.Behavior)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
        [C1Category("Category.Appearance")]
		[C1Description("C1ReportViewer.Localization",
				"Use the Localization property in order to customize localization strings.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public LocalizationOption Localization
		{
			get
			{
				if (_LocalizationOption == null)
				{
					_LocalizationOption = new LocalizationOption(this);
				}
				return _LocalizationOption;
			}
		}

		/// <summary>
		/// Indicates whether to automatically generate and
		///	show parameter input forms for reports with parameters. The default is
		///	true.
		/// </summary>
		[DefaultValue(true)]
		[Json(true)]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1ReportViewer.ShowParameterInputForm", "Indicates whether to automatically generate and show parameter input forms for reports with parameters. The default is true.")]
		public bool ShowParameterInputForm
		{
			get
			{
				return this.ViewState["ShowParameterInputForm"] == null ? true : (bool)this.ViewState["ShowParameterInputForm"];
			}
			set
			{				
				this.ViewState["ShowParameterInputForm"] = value;
				_showInputParameters = value;//qq:remove
			}
		}

        /// <summary>
        /// The property used to set the Pdf document security, include:
        ///	AllowCopyContent;AllowEditAnnotations;AllowEditContent;
        ///	AllowPrint;UserPassword;OwnerPassword properties.
        /// </summary>
        internal PdfSecurity PdfSecurity
        {
            get
            {
                if (_pdfSecurity == null)
                {
                    _pdfSecurity = new PdfSecurity();
                }
                _pdfSecurity.AllowCopyContent = PdfSecuritySetting.AllowCopyContent;
                return _pdfSecurity;
            }
        }

        /// <summary>
        /// The property used to set the Pdf document security, now including:
        ///	AllowCopyContent: allow the pdf can be copied.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PdfSecuritySetting PdfSecuritySetting
        {
            get
            {
                if (_pdfSecuritySetting == null)
                {
                    _pdfSecuritySetting = new PdfSecuritySetting();
                }
                return _pdfSecuritySetting;
            }
        }

		/// <summary>
		/// Additional report options. This property is hidden.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Dictionary<string, object> ExtraOptions
		{
			get
			{
				Dictionary<string, object> props = new Dictionary<string, object>();
				props["ShowParameterInputForm"] = ShowParameterInputForm;
                props["PdfSecurity"] = PdfSecurity;
				return props;
			}
		}

		/// <summary>
		/// Specifies the set of tools that will be available for the user
		/// </summary>
		/// <value>The available tools.</value>
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
		[DefaultValue(AvailableReportTools.All)]
        [C1Category("Category.Appearance")]
		[C1Description("C1ReportViewer.AvailableTools", "Specifies the set of tools that will be available for the user.")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		public AvailableReportTools AvailableTools
		{
			get
			{
				return GetPropertyValue<AvailableReportTools>("AvailableTools", AvailableReportTools.All);
			}
			set
			{
				SetPropertyValue<AvailableReportTools>("AvailableTools", value);
			}
		}

		/// <summary>
		/// The default tool that will be expanded in the tools panel.
		/// </summary>
		[DefaultValue(ReportTools.Outline)]
        [C1Category("Category.Appearance")]
		[C1Description("C1ReportViewer.ExpandedTool", "The default tool that will be expanded in the tools panel.")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		public ReportTools ExpandedTool
		{
			get
			{
				return GetPropertyValue<ReportTools>("ExpandedTool", ReportTools.Outline);
			}
			set
			{
				SetPropertyValue<ReportTools>("ExpandedTool", value);
			}
		}


		/// <summary>
		/// Specifies whether the tools panel will be collapsed.
		/// </summary>
		[DefaultValue(false)]
        [C1Category("Category.Behavior")]
		[C1Description("C1ReportViewer.CollapseToolsPanel", "Specifies whether the tools panel will be collapsed.")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption()]
		public bool CollapseToolsPanel
		{
			get
			{
				return GetPropertyValue<bool>("CollapseToolsPanel", false);
			}
			set
			{
				SetPropertyValue<bool>("CollapseToolsPanel", value);
			}
		}

		/// <summary>
		/// A value indicates the location of the splitter, in pixels, from the left edge of the splitter.
		/// </summary>
        [C1Category("Category.Layout")]
		[DefaultValue(250)]
		[C1Description("C1ReportViewer.SplitterDistance", "A value indicates the location of the splitter, in pixels, from the left edge of the splitter.")]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[Layout(LayoutType.Behavior)]
		public int SplitterDistance
		{
			get
			{
				return this.GetPropertyValue("SplitterDistance", 250);
			}
			set
			{
				if (value < 0)
				{
					value = 0;
				}
				this.SetPropertyValue("SplitterDistance", value);
			}
		}

		/// <summary>
		/// Enables built-in log console.
		/// </summary>
		[DefaultValue(false)]
        [C1Category("Category.Behavior")]
		[C1Description("C1ReportViewer.EnableLogs", "Enables built-in log console.")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption()]
		/*#if GRAPECITY
				[EditorBrowsable(EditorBrowsableState.Never)]
				[Browsable(false)]
		#endif*/
		public bool EnableLogs
		{
			get
			{
				return GetPropertyValue<bool>("EnableLogs", false);
			}
			set
			{
				SetPropertyValue<bool>("EnableLogs", value);
			}
		}

		//

		/// <summary>
		/// Gets or sets the name of the file with report.
		/// </summary>
		[C1Description("C1ReportViewer.FileName", "The name of the file with report.")]
		[Layout(LayoutType.Data)]
		[DefaultValue("")]
		[Json(true)]
		//[Editor(typeof(ReportFileLocationEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public string FileName
		{
			get
			{
				return this.ViewState["FileName"] == null ? "" : (string)this.ViewState["FileName"];
			}
			set
			{
				this.ViewState["FileName"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the report to view.
		/// </summary>
		[C1Description("C1ReportViewer.ReportName", "The name of the report to view.")]
		[Layout(LayoutType.Data)]
		[DefaultValue("")]
		[Json(true)]
		//[TypeConverter(typeof(SubreportFieldConverter))]
		public string ReportName
		{
			get
			{
				string reportName = this.ViewState["ReportName"] == null ? "" : (string)this.ViewState["ReportName"];
				if (!IsDesignMode)
				{
					if (string.IsNullOrEmpty(reportName))
					{
						string[] reports = GetReportList(this.FileName);
						if (reports != null && reports.Length > 0)
							return reports[0];
					}

					return reportName;
				}
				else
				{
					return reportName;
				}
			}
			set
			{
				this.ViewState["ReportName"] = value;
			}
		}

		/// <summary>
		/// Gets array of the available report names for given report file.
		/// </summary>
		/// <returns></returns>
		public string[] GetReportList(string fileName)
		{
			if (!string.IsNullOrEmpty(fileName))
			{
				try
				{
					string[] reports = C1.C1Report.C1Report.GetReportList(ResolveFileServerPath(fileName));
					if (reports != null)
						return reports;
				}
				catch (Exception)
				{
					return new string[0];
				}
			}
			return new string[0];
		}

		/// <summary>
		/// Gets a reference to the <see cref="ReportCache"/> object that controls how reports 
		/// are cached on the server.
		/// </summary>
		/// <remarks>
		/// <para>The report cache is one of the most powerful features in the <see cref="C1ReportViewer"/>
		/// control. By default, every time the control renders a report, it compresses the resulting 
		/// Html stream and stores it in the <see cref="Page.Cache"/> object. If the same report is 
		/// requested again, the control simply fetches it back from the cache and sends it directly 
		/// to the client. This results in fast response times and consumes little memory (because the 
		/// reports are cached in compressed form).</para>
		/// <para>The control is smart enough to detect changes to the report definition and keep track 
		/// of different versions in the cache. It also detects changes to the report definition file 
		/// and discards old reports from the cache.</para>
		/// <para>The cache object itself is provided by the ASP.NET framework (<see cref="Page.Cache"/> 
		/// property), and can be set up to match your server configuration, including Web Farm scenarios.</para>
		/// <para>The <see cref="ReportCache"/> object allows you to disable the cache, specify time out 
		/// values, and add dependencies (to data source files for example).</para>
		/// </remarks>
		/// <example>
		/// The code below configures the <see cref="ReportCache"/> to keep rendered reports in the 
		/// cache for one hour, renewing the time out period every time a report is retrieved from the
		/// cache. It also adds a dependency on a SQL server database file, so whenever the underlying 
		/// data changes, the control know that the report must be rendered again (the report definition 
		/// file is automatically added as a dependency).
		/// <code>
		/// // enable cache
		/// _c1wr.Cache.Enabled = true;
		/// 
		/// // cached reports expire after one hour
		/// _c1wr.Cache.Expiration = 60;
		/// 
		/// // renew one-hour limit whenever a report is retrieved from the cache
		/// _c1wr.Cache.Sliding = true;
		/// </code>
		/// </example>
		[
		TypeConverter(typeof(ExpandableObjectConverter)),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		PersistenceMode(PersistenceMode.InnerProperty),
		C1Category("Category.Behavior"),
		C1Description("C1ReportViewer.Cache", "The report cache parameters.")
		]
		public ReportCache Cache
		{
			get
			{
				/*
				if (_reportCache == null)
				{
					_reportCache = new ReportCache();
				}*/
				return _reportCache;
			}
		}

		/// <summary>
		/// Gets or sets the relative path to folder that will be used to store generated report data.
		/// </summary>
		/// <value>The relative path to folder that will be used to store generated report data.</value>
		[C1Description("C1ReportViewer.ReportsFolderPath", "The relative path to folder that will be used to store generated report data.")]
		[DefaultValue("~/tempReports")]
        [C1Category("Category.Behavior")]
		[Json(true)]
		public string ReportsFolderPath
		{
			get
			{
				return this.ViewState["ReportsFolderPath"] == null ? "~/tempReports" : (string)this.ViewState["ReportsFolderPath"];
			}
			set
			{
				this.ViewState["ReportsFolderPath"] = value;
			}
		}

		/// <summary>
		/// Specifies whether the viewer must show document pages individually (with scrollbars covering the current page only) or continuously (with scrollbars covering the whole document).
		/// </summary>
		[DefaultValue(true)]
        [C1Category("Category.Behavior")]
		[C1Description("C1ReportViewer.PagedView", "Specifies whether the viewer must show document pages individually (with scrollbars covering the current page only) or continuously (with scrollbars covering the whole document).")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption()]
		public bool PagedView
		{
			get
			{
				return GetPropertyValue<bool>("PagedView", true);
			}
			set
			{
				SetPropertyValue<bool>("PagedView", value);
			}
		}

		/// <summary>
		/// Specifies whether the viewer must be shown in full screen mode.
		/// Note, when full screen mode is enabled document overflow style 
		///	will be changed to hidden and viewer will be fit in parent 
		///	container bounds.
		/// </summary>
		[DefaultValue(false)]
        [C1Category("Category.Behavior")]
		[C1Description("C1ReportViewer.FullScreen", "Specifies whether the viewer must be shown in full screen mode.")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption()]
		public bool FullScreen
		{
			get
			{
				return GetPropertyValue<bool>("FullScreen", false);
			}
			set
			{
				SetPropertyValue<bool>("FullScreen", value);
			}
		}

		/// <summary>
		/// The URL to the report service.
		/// </summary>
		/// <value>The HTTP handler URL.</value>
        [C1Category("Category.Behavior")]
		[C1Description("C1ReportViewer.ReportServiceUrl", "The URL to the report service.")]
		[WidgetOption()]
		[UrlProperty()]
		[DefaultValue("~/reportService.ashx")]
		public string ReportServiceUrl
		{
			get
			{
				if (!IsDesignMode)
				{
					return ResolveClientUrl(GetPropertyValue<string>("ReportServiceUrl", "~/reportService.ashx"));
				}
				return GetPropertyValue<string>("ReportServiceUrl", "~/reportService.ashx");
			}
			set
			{
				SetPropertyValue<string>("ReportServiceUrl", value);
			}
		}

		/// <summary>
		/// Indicates whether the toolbar will be visible.
		/// </summary>
		[DefaultValue(true)]
        [C1Category("Category.Appearance")]
		[C1Description("C1ReportViewer.ToolBarVisible", "Indicates whether the toolbar will be visible.")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		public bool ToolBarVisible
		{
			get
			{
				return GetPropertyValue<bool>("ToolBarVisible", true);
			}
			set
			{
				SetPropertyValue<bool>("ToolBarVisible", value);
			}
		}

		/// <summary>
		/// Indicates whether the status bar will be visible.
		/// </summary>
		[DefaultValue(true)]
        [C1Category("Category.Appearance")]
		[C1Description("C1ReportViewer.StatusBarVisible", "Indicates whether the status bar will be visible.")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		public bool StatusBarVisible
		{
			get
			{
				return GetPropertyValue<bool>("StatusBarVisible", true);
			}
			set
			{
				SetPropertyValue<bool>("StatusBarVisible", value);
			}
		}

		/// <summary>
		/// Gets the tool bar control. 
		/// Use this property if you want to customize toolbar at runtime.
		/// </summary>
		/// <value>The tool bar.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1ReportViewerToolbar ToolBar
		{
			get
			{
				if (_toolBar == null)
				{
					_toolBar = C1ReportViewerToolbar.CreateToolbar(this);
				}
				return _toolBar;
			}

		}

		/// <summary>
		/// The page zoom value. Accepts named zoom values like "actual size", "fit page", "fit width", "fit height" or value in percentages, e.g. "50%", "70%".
		/// </summary>
		[DefaultValue("100%")]
		[C1Description("C1ReportViewer.Zoom", "The page zoom value. Accepts named zoom values like \"actual size\", \"fit page\", \"fit width\", \"fit height\" or value in percentages, e.g. \"50%\", \"70%\".")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption()]
		[TypeConverter(typeof(ZoomValueConverter))]
		public string Zoom
		{
			get
			{
				return GetPropertyValue<string>("Zoom", "100%");
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					value = "100%";
				}
				value = value.ToLower();
				switch (value)
				{
					case "actual size":
						break;
					case "fit page":
						break;
					case "fit width":
						break;
					case "fit height":
						break;
					default:
						int i;
						if (!int.TryParse(value.Replace("%", ""), out i))
						{
							throw new ArgumentException("Given zoom value is incorrect: " + value);
						}
						value = i + "%";
						break;
				}
				SetPropertyValue<string>("Zoom", value);
			}
		}

		/// <summary>
		/// Gets or sets the width of the control.
		/// </summary>
		[C1Description("C1ReportViewer.Width", "The width of the control.")]
		[DefaultValue(typeof(System.Web.UI.WebControls.Unit), "800px")]
		[Layout(LayoutType.Sizes)]
		[Json(true)]
		public new System.Web.UI.WebControls.Unit Width
		{
			get
			{
				object o = ViewState["Width"];
				if (o == null)
					return System.Web.UI.WebControls.Unit.Pixel(800);
				return (System.Web.UI.WebControls.Unit)o;
			}
			set
			{
				//Note, percentages are supported by client.
				ViewState["Width"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the height of the control.
		/// </summary>
		[C1Description("C1ReportViewer.Height", "The height of the control")]
		[DefaultValue(typeof(System.Web.UI.WebControls.Unit), "600px")]
		[Layout(LayoutType.Sizes)]
		[Json(true)]
		public new System.Web.UI.WebControls.Unit Height
		{
			get
			{
				object o = ViewState["Height"];
				if (o == null)
					return System.Web.UI.WebControls.Unit.Pixel(600);
				return (System.Web.UI.WebControls.Unit)o;
			}
			set
			{
				//Note, percentages are supported by client.
				ViewState["Height"] = value;
			}
		}

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region ** methods

		C1Splitter _mainSplitter;

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use 
		/// composition-based implementation to create any child controls they contain 
		/// in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			ControlCollection currentContextControls = this.Controls;
			if (IsDesignMode)
			{
				if (this.ToolBarVisible)
				{
					currentContextControls.Add(this.ToolBar);
				}
			}
			else
			{
				currentContextControls.Add(this.ToolBar);
			}

            _mainSplitter = new C1Splitter("c1v01xIXFksavw5HDgsOLwp/ClRxGd33Djw==");//"ttZzKzJ5mwNr/IihpBl2pA==");
			_mainSplitter.CssClass = "c1-c1reportviewer-splitter";
			_mainSplitter.Orientation = Orientation.Vertical;
			_mainSplitter.Panel1.MinSize = 100;
			_mainSplitter.Panel1.ContentTemplate = new C1ToolsPaneTemplate(this);
			_mainSplitter.Panel1.ScrollBars = System.Web.UI.WebControls.ScrollBars.None;
			if (this.CollapseToolsPanel)
			{
				_mainSplitter.Panel1.Collapsed = true;
			}
			_mainSplitter.Panel2.ContentTemplate = new C1ReportPaneTemplate(this);
			_mainSplitter.Panel2.ScrollBars = System.Web.UI.WebControls.ScrollBars.None;
			_mainSplitter.SplitterDistance = this.SplitterDistance;
			_mainSplitter.FullSplit = true;
			if (IsDesignMode)
			{
				int splitterHeight = DetermineDesignPixelHeight() - (this.ToolBarVisible ? 47 : 0) -
					(this.StatusBarVisible ? 24 : 0);
				if (splitterHeight < 0)
					splitterHeight = 0;
				_mainSplitter.Width = System.Web.UI.WebControls.Unit.Pixel(this.DetermineDesignPixelWidth());
				_mainSplitter.Height = System.Web.UI.WebControls.Unit.Pixel(splitterHeight);
			}



			currentContextControls.Add(_mainSplitter);
		}

		private int DetermineDesignPixelHeight()
		{
			if (this.Height.IsEmpty || this.Height.Type == System.Web.UI.WebControls.UnitType.Percentage)
			{
				return 600;
			}
			return (int)this.Height.Value;
		}

		private int DetermineDesignPixelWidth()
		{
			if (this.Width.IsEmpty || this.Width.Type == System.Web.UI.WebControls.UnitType.Percentage)
			{
				return 800;
			}
			return (int)this.Width.Value;
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
			if (IsDesignMode)
			{
				writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.DetermineDesignPixelWidth() + "px");
				writer.AddStyleAttribute(HtmlTextWriterStyle.Height, this.DetermineDesignPixelHeight() + "px");
			}
			else
			{				
				if (!this.Width.IsEmpty)
				{
					writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.Width.ToString());
				}
				if (!this.Height.IsEmpty)
				{
					writer.AddStyleAttribute(HtmlTextWriterStyle.Height, this.Height.ToString());
				}
			}

			string cssClass = string.IsNullOrEmpty(this.CssClass) ? "" : (this.CssClass + " ");
			if (!IsDesignMode)
			{
				cssClass += C1.Web.Wijmo.Utils.GetHiddenClass();
			}
			else
			{
				cssClass += string.Format("c1-c1reportviewer wijmo-wijreportviewer ui-widget ui-helper-reset{0}",
					this.PagedView ? " wijmo-wijreportviewer-paged" : "");
			}
			writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
		}
		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (HttpContext.Current != null)
			{
				HttpContext.Current.Cache[this.ClientID + "_ReportsFolderPath"] = this.ReportsFolderPath;
				HttpContext.Current.Cache[this.ClientID + "_ReportCache"] = this.Cache;
				HttpContext.Current.Cache[this.ClientID + "_ExtraOptions"] = this.ExtraOptions;
			}
			// fix for 20888:
			if (_mainSplitter != null)
			{
				// fixed the disabled issue for reporterview.  The disabled handled by the reporterview, and spliter allways enabled.
				_mainSplitter.Enabled = true;
			}
			this.EnsureHttpHandlerExists();
			this.EnsureChildControls();
			base.Render(writer);
		}

		/// <summary>
		/// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void RenderContents(HtmlTextWriter writer)
		{
			base.RenderContents(writer);			
			/*
			writer.Write("<div class=\"c1-c1reportviewer-splitter\">");
			writer.Write("<div>");//panel1>
			writer.Write("<div class=\"c1-c1reportviewer-tabs\">");
			writer.Write("<ul></ul>");
			writer.Write("</div>");
			writer.Write("</div>");//<panel1
			writer.Write("<div>");//panel2>
			writer.Write("<div class=\"wijmo-wijreportviewer-reportpane\"><center class=\"wijmo-wijreportviewer-content\"></center></div>");
			writer.Write("</div>");//<panel2>
			writer.Write("</div>");//<splitter
			*/
			if (IsDesignMode)
			{
				if (this.StatusBarVisible)
				{
					writer.Write("<div class=\"c1-c1reportviewer-statusbar ui-widget-header ui-corner-all\"><span class=\"status\">Status text.</span></div>");
				}
			}
			else
			{
				writer.Write("<div class=\"c1-c1reportviewer-statusbar\"></div>");
			}
		}



		/// <summary>
		/// Gets the content of the dialog template.
		/// </summary>
		/// <param name="dialogName">Name of the dialog.</param>
		/// <returns></returns>
		public static string GetDialogTemplateContent(string dialogName)
		{
			try
			{
				string input = string.Empty;
				string name = "C1.Web.Wijmo.Controls.C1ReportViewer.Resources.Dialogs." + dialogName + ".htm";
				Assembly assembly = Assembly.GetAssembly(typeof(C1ReportViewer));
				using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(name)))
				{
					input = reader.ReadToEnd();
				}
				return _LocalizeDialogContent(input, dialogName);
			}
			catch (Exception ex)
			{
				return "<div class=\"c1-c1reportviewer-error\"><span>Can't load dialog '" + dialogName + "', " + ex.Message + "</span></div>";
			}
		}

		private static string _LocalizeDialogContent(string input, string dialogName)
		{
#if DEBUG
			CultureInfo culture = new CultureInfo("ja-JP");
			//CultureInfo culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
#else
			CultureInfo culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
#endif
			//this.UsedCulture?qq
			dialogName=dialogName.ToLowerInvariant();
			if (dialogName == "print")
			{
				input = input.Replace("Print Range", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.PrintRange", "Print Range", culture));
				input = input.Replace("Visible pages", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.VisiblePages", "Visible pages", culture));
				input = input.Replace(">All<", ">" + C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.All", "All", culture) + "<");
				input = input.Replace("Pages&nbsp;", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.Pages", "Pages", culture) + "&nbsp;");
				input = input.Replace("Subset:&nbsp;", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.Subset", "Subset:", culture) + "&nbsp;");
				input = input.Replace("All pages in range", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.Allpagesinrange", "All pages in range", culture));
				input = input.Replace("Odd pages only", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.Oddpagesonly", "Odd pages only", culture));
				input = input.Replace("Even pages only", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.Evenpagesonly", "Even pages only", culture));
				input = input.Replace("Reverse pages", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintOptions.Reversepages", "Reverse pages", culture));
				input = input.Replace(">Preview", ">" + C1Localizer.GetString("C1ReportViewer.PrintDialog.PreviewTitle", "Preview", culture));
				input = input.Replace("Subset:", C1Localizer.GetString("C1ReportViewer.PrintDialog.PrintRangeSubset", "Subset:", culture));

			}
			else if (dialogName == "search")
			{
				//
				input = input.Replace("Case sensitive", C1Localizer.GetString("C1ReportViewer.SearchDialog.Casesensitive", "Case sensitive", culture));
				input = input.Replace(">Search", ">" + C1Localizer.GetString("C1ReportViewer.SearchDialog.SearchButton", "Search", culture));
				//
			}
			else if (dialogName == "outline")
			{
				
				//
				input = input.Replace("<span class=\"status\">&lt;no outline&gt;", "<span class=\"status\">" + C1Localizer.GetString("C1ReportViewer.Localization.outlineStatus", "[no outline]", culture));
			}

			return input;
		}

		#endregion

		#region ** ICompositeControlDesignerAccessor interface implementation

		/// <summary>
		/// Recreates child controls.
		/// </summary>
		public void RecreateChildControls()
		{
			CreateChildControls();
		}

		#endregion

		#region ** IC1Serializable interface implementations

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1ReportViewerSerializer sz = new C1ReportViewerSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1ReportViewerSerializer sz = new C1ReportViewerSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1ReportViewerSerializer sz = new C1ReportViewerSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1ReportViewerSerializer sz = new C1ReportViewerSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}


		#endregion

		#region ** private implemetation

		private void EnsureHttpHandlerExists()
		{

			string siteRootPath = "";
			if (!IsDesignMode)
				siteRootPath = Page.Server.MapPath("~/");
			else
				siteRootPath = ResolvePhysicalPath(this, "~/");
			siteRootPath = siteRootPath.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar;

			Assembly assembly = Assembly.GetAssembly(typeof(C1ReportViewer));
			string name = "C1.Web.Wijmo.Controls.C1ReportViewer.Resources.reportService.ashx";
			string fileName = siteRootPath + "reportService.ashx";

			FileInfo file = new FileInfo(fileName);
			if (!file.Exists)
			{
				try
				{
					string input = string.Empty;
					using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(name)))
					{
						input = reader.ReadToEnd();
					}
					using (StreamWriter sw = new StreamWriter(file.FullName, false))
					{
						sw.Write(input);
					}
				}
				catch (Exception)
				{
					throw new HttpException("Http handler 'reportService.ashx' is not found at site root. C1ReportViewer is not configured properly. Attempt to create file dynamically, failed.");
				}
			}
		}

		private string ResolveFileServerPath(string relativePath)
		{
			if (!IsDesignMode)
				return this.Page.Server.MapPath(relativePath);
			else if (this.Page != null)
			{
				string path = ResolvePhysicalPath(this, relativePath);
				if (path.EndsWith(Path.DirectorySeparatorChar.ToString()))
				{
					path = path.Remove(path.Length - Path.DirectorySeparatorChar.ToString().Length);
				}
				return path;
			}
			else
				return relativePath;
		}

		#region ** licensing

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ReportViewer), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
		}

		#endregion

		#region ** design time methods

		private static string ResolvePhysicalPath(System.Web.UI.Control control, string url)
		{
			System.ComponentModel.ISite site = GetSite(control);
			if (site == null) return url;

			System.Web.UI.Design.IWebApplication service = (System.Web.UI.Design.IWebApplication)site.GetService(typeof(System.Web.UI.Design.IWebApplication));
			System.Web.UI.Design.IProjectItem item = service.GetProjectItemFromUrl(url);
			if (item == null)
				return url.Replace("~/", service.RootProjectItem.PhysicalPath);

			string path = item.PhysicalPath;
			return path;
		}

		private static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}

		#endregion

		#endregion

		/// <summary>
		/// Gets or sets culture ID.
		/// </summary>
		[C1Description("C1ReportViewer.CultureInfo")]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(CultureInfo), "en-US")]
		[TypeConverter(typeof(CultureInfoConverter))]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public CultureInfo Culture
		{
			get
			{
				return GetCulture(GetPropertyValue<CultureInfo>("Culture", null));
			}
			set
			{
				SetPropertyValue<CultureInfo>("Culture", value);
			}
		}

		/// <summary>
		/// Localizes the string.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string LocalizeString(string key, string defaultValue)
		{
#if DEBUG
			return C1Localizer.GetString(key, defaultValue, new CultureInfo("ja-JP"));
			//return C1Localizer.GetString(key, defaultValue, System.Threading.Thread.CurrentThread.CurrentUICulture);
#else
			return C1Localizer.GetString(key, defaultValue, System.Threading.Thread.CurrentThread.CurrentUICulture);
#endif

		}
	}

	internal class ZoomValueConverter : StringListConverter
	{
		public ZoomValueConverter()
		{
			SetList(false, new string[]
						{
								

								"10%", "25%", "50%", "75%", "100%", "125%", "150%", "200%", "400%", 
								"actual size", "fit page", "fit width", "fit height"

						});
		}
	}


	internal class C1ToolsPaneTemplate : ITemplate
	{

		#region ** fields

		private C1ReportViewer _reportViewer;

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ToolsPaneTemplate"/> class.
		/// </summary>
		/// <param name="reportViewer">The report viewer.</param>
		public C1ToolsPaneTemplate(C1ReportViewer reportViewer)
		{
			_reportViewer = reportViewer;
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Defines the System.Web.UI.Control object that
		/// child controls and templates belong to. These child controls are in turn
		/// defined within an inline template.
		/// </summary>
		/// <param name="control">The control.</param>
		public void InstantiateIn(Control control)
		{
			HtmlGenericControl tabs = new HtmlGenericControl("div");
			tabs.Attributes["class"] = "c1-c1reportviewer-tabs";
			tabs.InnerHtml = "<ul></ul>";
			control.Controls.Add(tabs);
		}

		#endregion

	}

	internal class C1ReportPaneTemplate : ITemplate
	{
		private C1ReportViewer _reportViewer;

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ReportPaneTemplate"/> class.
		/// </summary>
		/// <param name="reportViewer">The report viewer.</param>
		public C1ReportPaneTemplate(C1ReportViewer reportViewer)
		{
			_reportViewer = reportViewer;
		}

		/// <summary>
		/// Defines the System.Web.UI.Control object that
		/// child controls and templates belong to. These child controls are in turn
		/// defined within an inline template.
		/// </summary>
		/// <param name="control">The control.</param>
		public void InstantiateIn(Control control)
		{
			HtmlGenericControl reportPane = new HtmlGenericControl("div");
			reportPane.Attributes["class"] = "wijmo-wijreportviewer-reportpane";
			reportPane.InnerHtml = "<center class=\"wijmo-wijreportviewer-content\"></center>";
			control.Controls.Add(reportPane);
		}

	}


	/// <summary>
	/// Format of the text for the day header in the day/week or list view.
	/// </summary>
	public class LocalizationOption : Settings, IJsonEmptiable
	{

		C1ReportViewer _reportViewer;

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalizationOption"/> class.
		/// </summary>
		public LocalizationOption(C1ReportViewer reportViewer)
		{
			_reportViewer = reportViewer;
		}

		#region ** localization string properties

		/// <summary>
		/// buttonClearAll string.
		/// </summary>
		[DefaultValue("Clear All")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonClearAll
		{
			get
			{

				return GetPropertyValue<string>("buttonClearAll",
					C1Localizer.GetString("C1ReportViewer.Localization.buttonClearAll",
					"Clear All",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonClearAll", value);
			}
		}
		//	buttonClearAll: "Clear All"

		/// <summary>
		/// buttonClose string.
		/// </summary>
		[DefaultValue("Close")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonClose
		{
			get
			{

				return GetPropertyValue<string>("buttonClose",
					C1Localizer.GetString("C1ReportViewer.Localization.buttonClose",
					"Close",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonClose", value);
			}
		}
		//	buttonClose: "Close"

		/// <summary>
		/// activityStatusSearchingText string.
		/// </summary>
		[DefaultValue("Searching text...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityStatusSearchingText
		{
			get
			{

				return GetPropertyValue<string>("activityStatusSearchingText",
					C1Localizer.GetString("C1ReportViewer.Localization.activityStatusSearchingText",
					"Searching text...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityStatusSearchingText", value);
			}
		}
		//	activityStatusSearchingText: "Searching text..."

		/// <summary>
		/// searchNoMatchesFoundFormat string.
		/// </summary>
		[DefaultValue("No Matches Found for '{0}'")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string searchNoMatchesFoundFormat
		{
			get
			{

				return GetPropertyValue<string>("searchNoMatchesFoundFormat",
					C1Localizer.GetString("C1ReportViewer.Localization.searchNoMatchesFoundFormat",
					"No Matches Found for '{0}'",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("searchNoMatchesFoundFormat", value);
			}
		}
		//	searchNoMatchesFoundFormat: "No Matches Found for '{0}'"

		/// <summary>
		/// searchMatchesFoundFormat string.
		/// </summary>
		[DefaultValue("{0} Matches Found")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string searchMatchesFoundFormat
		{
			get
			{

				return GetPropertyValue<string>("searchMatchesFoundFormat",
					C1Localizer.GetString("C1ReportViewer.Localization.searchMatchesFoundFormat",
					"{0} Matches Found",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("searchMatchesFoundFormat", value);
			}
		}
		//	searchMatchesFoundFormat: "{0} Matches Found"

		/// <summary>
		/// searchResultPageFormat string.
		/// </summary>
		[DefaultValue("Page {0}:")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string searchResultPageFormat
		{
			get
			{

				return GetPropertyValue<string>("searchResultPageFormat",
					C1Localizer.GetString("C1ReportViewer.Localization.searchResultPageFormat",
					"Page {0}:",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("searchResultPageFormat", value);
			}
		}
		//	searchResultPageFormat: "Page {0}:"

		/// <summary>
		/// printTips string.
		/// </summary>
		[DefaultValue("1. Make sure that preview area contains the correct content,\n"
					 + "   if needed wait until preview area has updated successfully\n"
					 + "2. Be aware that content inside the preview area of C1ReportViewer will be printed 'as is'.\n"
					 + "\n"
					 + "Note on printing:\n"
					 + "  While the Print button provides a convenient and quick way to print"
					 + "  all or part of a document, it should be understood that it has"
					 + "  significant limitations."
					 + "  Anything printed from within a Web browser is subject to the browser’s"
					 + "  formatting, page headers and footers and so on. So normally, to print"
					 + "  the final copy of a document you would probably want to use the Save"
					 + "  button to save it as PDF and print that instead."
					 + "  PDF documents created by Report Viewer should be identical in appearance"
					 + "  to the documents in the viewer – but should print much better as they may"
					 + "  be printed avoiding limitations imposed by the Web browser.")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string printTips
		{
			get
			{

				return GetPropertyValue<string>("printTips",
					C1Localizer.GetString("C1ReportViewer.Localization.printTips",
					"1. Make sure that preview area contains the correct content,\n"
					 + "   if needed wait until preview area has updated successfully\n"
					 + "2. Be aware that content inside the preview area of C1ReportViewer will be printed 'as is'.\n"
					 + "\n"
					 + "Note on printing:\n"
					 + "  While the Print button provides a convenient and quick way to print"
					 + "  all or part of a document, it should be understood that it has"
					 + "  significant limitations."
					 + "  Anything printed from within a Web browser is subject to the browser’s"
					 + "  formatting, page headers and footers and so on. So normally, to print"
					 + "  the final copy of a document you would probably want to use the Save"
					 + "  button to save it as PDF and print that instead."
					 + "  PDF documents created by Report Viewer should be identical in appearance"
					 + "  to the documents in the viewer – but should print much better as they may"
					 + "  be printed avoiding limitations imposed by the Web browser.",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("printTips", value);
			}
		}
		/*	printTips: "1. Make sure that preview area contains the correct content,\n"
					 + "   if needed wait until preview area has updated successfully\n"
					 + "2. Be aware that content inside the preview area of C1ReportViewer will be printed 'as is'.\n"
					 + "\n"
					 + "Note on printing:\n"
					 + "  While the Print button provides a convenient and quick way to print"
					 + "  all or part of a document, it should be understood that it has"
					 + "  significant limitations."
					 + "  Anything printed from within a Web browser is subject to the browser’s"
					 + "  formatting, page headers and footers and so on. So normally, to print"
					 + "  the final copy of a document you would probably want to use the Save"
					 + "  button to save it as PDF and print that instead."
					 + "  PDF documents created by Report Viewer should be identical in appearance"
					 + "  to the documents in the viewer – but should print much better as they may"
					 + "  be printed avoiding limitations imposed by the Web browser."*/

		/// <summary>
		/// titleReportparameters string.
		/// </summary>
		[DefaultValue("report parameters")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string titleReportparameters
		{
			get
			{

				return GetPropertyValue<string>("titleReportparameters",
					C1Localizer.GetString("C1ReportViewer.Localization.titleReportparameters",
					"report parameters",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("titleReportparameters", value);
			}
		}
		//	titleReportparameters: "report parameters"

		/// <summary>
		/// buttonSetparameters string.
		/// </summary>
		[DefaultValue("Set parameters")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonSetparameters
		{
			get
			{

				return GetPropertyValue<string>("buttonSetparameters",
					C1Localizer.GetString("C1ReportViewer.Localization.buttonSetparameters",
					"Set parameters",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonSetparameters", value);
			}
		}
		//	buttonSetparameters: "Set parameters"

		/// <summary>
		/// activityReportneedsparameters string.
		/// </summary>
		[DefaultValue("Report needs parameters")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityReportneedsparameters
		{
			get
			{

				return GetPropertyValue<string>("activityReportneedsparameters",
					C1Localizer.GetString("C1ReportViewer.Localization.activityReportneedsparameters",
					"Report needs parameters",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityReportneedsparameters", value);
			}
		}
		//	activityReportneedsparameters: "Report needs parameters"

		/// <summary>
		/// toolNameThumbs string.
		/// </summary>
		[DefaultValue("Thumbs")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string toolNameThumbs
		{
			get
			{

				return GetPropertyValue<string>("toolNameThumbs",
					C1Localizer.GetString("C1ReportViewer.Localization.toolNameThumbs",
					"Thumbs",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("toolNameThumbs", value);
			}
		}
		//	toolNameThumbs: "Thumbs"

		/// <summary>
		/// toolNameSearch string.
		/// </summary>
		[DefaultValue("Search")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string toolNameSearch
		{
			get
			{

				return GetPropertyValue<string>("toolNameSearch",
					C1Localizer.GetString("C1ReportViewer.Localization.toolNameSearch",
					"Search",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("toolNameSearch", value);
			}
		}
		//	toolNameSearch: "Search"

		/// <summary>
		/// toolNameOutline string.
		/// </summary>
		[DefaultValue("Outline")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string toolNameOutline
		{
			get
			{

				return GetPropertyValue<string>("toolNameOutline",
					C1Localizer.GetString("C1ReportViewer.Localization.toolNameOutline",
					"Outline",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("toolNameOutline", value);
			}
		}
		//	toolNameOutline: "Outline"

		/// <summary>
		/// outlineStatus string.
		/// </summary>
		[DefaultValue("[no outline]")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string outlineStatus
		{
			get
			{

				return GetPropertyValue<string>("outlineStatus",
					C1Localizer.GetString("C1ReportViewer.Localization.outlineStatus",
					"[no outline]",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("outlineStatus", value);
			}
		}
		//	outlineStatus: "[no outline]"

		/// <summary>
		/// activityLoading string.
		/// </summary>
		[DefaultValue("Loading...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityLoading
		{
			get
			{

				return GetPropertyValue<string>("activityLoading",
					C1Localizer.GetString("C1ReportViewer.Localization.activityLoading",
					"Loading...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityLoading", value);
			}
		}
		//	activityLoading: "Loading..."

		/// <summary>
		/// activityLoadingOutline string.
		/// </summary>
		[DefaultValue("Loading outline...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityLoadingOutline
		{
			get
			{

				return GetPropertyValue<string>("activityLoadingOutline",
					C1Localizer.GetString("C1ReportViewer.Localization.activityLoadingOutline",
					"Loading outline...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityLoadingOutline", value);
			}
		}
		//	activityLoadingOutline: "Loading outline..."

		/// <summary>
		/// activityWaitingForInput string.
		/// </summary>
		[DefaultValue("Waiting for input...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityWaitingForInput
		{
			get
			{

				return GetPropertyValue<string>("activityWaitingForInput",
					C1Localizer.GetString("C1ReportViewer.Localization.activityWaitingForInput",
					"Waiting for input...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityWaitingForInput", value);
			}
		}
		//	activityWaitingForInput: "Waiting for input..."

		/// <summary>
		/// activityUpdatingPages string.
		/// </summary>
		[DefaultValue("Updating pages...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityUpdatingPages
		{
			get
			{

				return GetPropertyValue<string>("activityUpdatingPages",
					C1Localizer.GetString("C1ReportViewer.Localization.activityUpdatingPages",
					"Updating pages...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityUpdatingPages", value);
			}
		}
		//	activityUpdatingPages: "Updating pages..."

		/// <summary>
		/// activityGenerating string.
		/// </summary>
		[DefaultValue("Generating...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityGenerating
		{
			get
			{

				return GetPropertyValue<string>("activityGenerating",
					C1Localizer.GetString("C1ReportViewer.Localization.activityGenerating",
					"Generating...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityGenerating", value);
			}
		}
		//	activityGenerating: "Generating..."

		/// <summary>
		/// activityGeneratingDocument string.
		/// </summary>
		[DefaultValue("Generating document...")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityGeneratingDocument
		{
			get
			{

				return GetPropertyValue<string>("activityGeneratingDocument",
					C1Localizer.GetString("C1ReportViewer.Localization.activityGeneratingDocument",
					"Generating document...",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityGeneratingDocument", value);
			}
		}
		//	activityGeneratingDocument: "Generating document..."

		/// <summary>
		/// activityStatusReceived string.
		/// </summary>
		[DefaultValue("documentStatus received, state is {0}")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityStatusReceived
		{
			get
			{

				return GetPropertyValue<string>("activityStatusReceived",
					C1Localizer.GetString("C1ReportViewer.Localization.activityStatusReceived",
					"documentStatus received, state is {0}",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityStatusReceived", value);
			}
		}
		//	activityStatusReceived: "documentStatus received, state is {0}"

		/// <summary>
		/// activityStatusFormat string.
		/// </summary>
		[DefaultValue("Report is currently generating, total page count may change. Current page count: {0}, completed {1}%")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityStatusFormat
		{
			get
			{

				return GetPropertyValue<string>("activityStatusFormat",
					C1Localizer.GetString("C1ReportViewer.Localization.activityStatusFormat",
					"Report is currently generating, total page count may change. Current page count: {0}, completed {1}%",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityStatusFormat", value);
			}
		}
		//	activityStatusFormat: "Report is currently generating, total page count may change. Current page count: {0}, completed {1}%"

		/// <summary>
		/// activityStatusReadyFormat string.
		/// </summary>
		[DefaultValue("Report is completely generated. Total page count: {0}")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityStatusReadyFormat
		{
			get
			{

				return GetPropertyValue<string>("activityStatusReadyFormat",
					C1Localizer.GetString("C1ReportViewer.Localization.activityStatusReadyFormat",
					"Report is completely generated. Total page count: {0}",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityStatusReadyFormat", value);
			}
		}
		//	activityStatusReadyFormat: "Report is completely generated. Total page count: {0}"

		/// <summary>
		/// messageFileNameNotSpecified string.
		/// </summary>
		[DefaultValue("Report definition file not specified.")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string messageFileNameNotSpecified
		{
			get
			{

				return GetPropertyValue<string>("messageFileNameNotSpecified",
					C1Localizer.GetString("C1ReportViewer.Localization.messageFileNameNotSpecified",
					"Report definition file not specified.",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("messageFileNameNotSpecified", value);
			}
		}
		//	messageFileNameNotSpecified: "Report definition file not specified."

		/// <summary>
		/// messageGenerateInDisabledState string.
		/// </summary>
		[DefaultValue("Can't execute generate, report viewer is disabled.")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string messageGenerateInDisabledState
		{
			get
			{

				return GetPropertyValue<string>("messageGenerateInDisabledState",
					C1Localizer.GetString("C1ReportViewer.Localization.messageGenerateInDisabledState",
					"Can't execute generate, report viewer is disabled.",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("messageGenerateInDisabledState", value);
			}
		}
		//	messageGenerateInDisabledState: "Can't execute generate, report viewer is disabled."


		/// <summary>
		/// buttonPrint string.
		/// </summary>
		[DefaultValue("Print")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonPrint
		{
			get
			{

				return GetPropertyValue<string>("buttonPrint",
					C1Localizer.GetString("C1ReportViewer.Localization.buttonPrint",
					"Print",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonPrint", value);
			}
		}
		//	buttonPrint: "Print"

		/// <summary>
		/// buttonPrintTips string.
		/// </summary>
		[DefaultValue("Print Tips")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonPrintTips
		{
			get
			{

				return GetPropertyValue<string>("buttonPrintTips",
					C1Localizer.GetString("C1ReportViewer.Localization.buttonPrintTips",
					"Print Tips",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonPrintTips", value);
			}
		}
		//	buttonPrintTips: "Print Tips"

		/// <summary>
		/// buttonCancel string.
		/// </summary>
		[DefaultValue("Cancel")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonCancel
		{
			get
			{

				return GetPropertyValue<string>("buttonCancel",
					C1Localizer.GetString("C1ReportViewer.Localization.buttonCancel",
					"Cancel",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonCancel", value);
			}
		}
		//	buttonCancel: "Cancel"

		/// <summary>
		/// labelPrint string.
		/// </summary>
		[DefaultValue("Print")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelPrint
		{
			get
			{

				return GetPropertyValue<string>("labelPrint",
					C1Localizer.GetString("C1ReportViewer.Localization.labelPrint",
					"Print",
					_reportViewer.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelPrint", value);
			}
		}
		//	labelPrint: "Print"

		#endregion

		#region ** implement ICustomOptionType interface

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return base.ToString();
		}

		internal bool ShouldSerialize()
		{
			return true;
		}

		#endregion end of ** IJsonEmptiable interface implementation


		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}

}
