﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1Upload
{

	/// <summary>
	/// Represents a collection of C1FileInfo.
	/// </summary>
    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class C1FileInfoCollection : CollectionBase
	{
		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1FileInfoCollection"/> class.
		/// </summary>
		public C1FileInfoCollection()
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds a C1FileInfo to this collection.
		/// </summary>
		/// <param name="fi">The C1FileInfo object to add.</param>
		/// <returns>The added position.</returns>
		public int Add(C1FileInfo fi)
		{
			return base.List.Add(fi);
		}

		/// <summary>
		/// Inserts a C1FileInfo to this collection.
		/// </summary>
		/// <param name="index">The position to insert.</param>
		/// <param name="fi">The C1FileInfo object to insert.</param>
		public void Insert(int index, C1FileInfo fi)
		{
			base.List.Insert(index, fi);
		}

		/// <summary>
		/// Removes the C1FileInfo from this collection.
		/// </summary>
		/// <param name="fi">The C1FileInfo object to remove.</param>
		public void Remove(C1FileInfo fi)
		{
			base.List.Remove(fi);
		}

		/// <summary>
		/// Indexer of this collection.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public C1FileInfo this[object obj]
		{
			get
			{
				int index = this.IndexOf(obj);
				if (index >= 0)
				{
					return (C1FileInfo)base.List[index];
				}
				return null;
			}
			set
			{
				int index = this.IndexOf(obj);
				if (index >= 0)
				{
					base.List[index] = value;
				}
				else
				{
					this.Add(value);
				}
			}
		}

		/// <summary>
		/// Tests whether an instance of C1FileInfo is contained in this collection.
		/// </summary>
		/// <param name="fi">The C1FileInfo to test.</param>
		/// <returns>True if exists.</returns>
		public bool Contains(C1FileInfo fi)
		{
			return base.List.Contains(fi);
		}

		/// <summary>
		/// Gets the index of the object.
		/// </summary>
		/// <param name="obj">The object to test.</param>
		/// <returns>The index of this object.</returns>
		public int IndexOf(object obj)
		{
			if (obj is int)
			{
				return (int)obj;
			}

			if (obj == null || !(obj is C1FileInfo))
			{
				throw new ArgumentException("Only a C1FileInfo or an integer is allowed.");
			}

			for (int i = 0; i < base.List.Count; i++)
			{
				if (object.Equals(base.List[i], obj))
				{
					return i;
				}
			}

			return -1;
		}

		#endregion
	}
}
