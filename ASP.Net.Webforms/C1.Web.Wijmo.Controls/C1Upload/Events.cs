﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Upload
{
	/// <summary>
	/// Provides data for the FileExists event.
	/// </summary>
	public class UploadedFileEventArgs : EventArgs
	{
		#region Fields

		private C1FileInfo _uploadedFile;

		#endregion

		#region Constructor

		internal UploadedFileEventArgs(C1FileInfo uploadedFile)
		{
			this._uploadedFile = uploadedFile;
		}

		#endregion

		#region Properties

		/// <summary>
		/// The C1FileInfo that contains data about the uploaded file.
		/// </summary>
		public C1FileInfo UploadedFile
		{
			get
			{
				return this._uploadedFile;
			}
		}

		#endregion
	}

	/// <summary>
	/// Represents the method that will handle the FileExists event.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A UploadedFileEventArgs that contains the event data.</param>
	public delegate void UploadedFileEventHandler(object sender, UploadedFileEventArgs e);

	/// <summary>
	/// Provides data for the ValidatingFile event.
	/// </summary>
	public class ValidateFileEventArgs : UploadedFileEventArgs
	{
		#region Fields

		private bool _isValid;
		private string _message = "";

		#endregion

		#region Constructor

		internal ValidateFileEventArgs(C1FileInfo uploadedFile, bool valid)
			: base(uploadedFile)
		{
			this._isValid = valid;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets whether the specified C1FileInfo passed validation. 
		/// </summary>
		public bool IsValid
		{
			get { return this._isValid; }
			set { this._isValid = value; }
		}

		/// <summary>
		/// Gets or sets the message if C1FileInfo failed validation.
		/// </summary>
		public string Message
		{
			get
			{
				return this._message;
			}
			set
			{
				_message = value;
			}
		}

		#endregion
	}

	/// <summary>
	/// Represents the method that will handle the ValidatingFile event.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A ValidateFileEventArgs that contains the event data.</param>
	public delegate void ValidateFileEventHandler(object sender, ValidateFileEventArgs e);

}
