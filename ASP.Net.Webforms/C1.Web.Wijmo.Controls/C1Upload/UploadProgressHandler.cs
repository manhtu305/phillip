﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Web.SessionState;


namespace C1.Web.Wijmo.Controls.C1Upload
{
	/// <summary>
	/// IHttpHandler for retrieving the progress data from the client side.
	/// </summary>
	/// <remarks>
	/// This class is not proposed to be used by user.
	/// </remarks>
    public class UploadProgressHandler : IHttpHandler, IReadOnlySessionState
	{
		/// <summary>
		/// Initializes a new instance of the UploadProgressHandler class.
		/// </summary>
		public UploadProgressHandler()
		{
		}

		/// <summary>
		/// Gets a value indicating whether another request can use the IHttpHandler instance.
		/// </summary>
		bool IHttpHandler.IsReusable
		{
			get { return true; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the IHttpHandler interface.
		/// </summary>
		/// <param name="context"></param>
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
            if (context.Request.Params["isswfupload"] != null)
            {
                string folder = "";
                if (context.Request.Params["filefolder"] != null)
                {
                    folder = context.Request.Params["filefolder"];
                }
                string sDirectory = HttpContext.Current.Server.MapPath(folder);
                if (!Directory.Exists(sDirectory))
                {
                    Directory.CreateDirectory(sDirectory);
                }

                HttpFileCollection oFiles = context.Request.Files;
                if (oFiles != null && oFiles.Count > 0)
                {
                    for (int i = 0; i < oFiles.Count; i++)
                    {
                        string fileName = oFiles[i].FileName;
                        int idx = fileName.LastIndexOf("\\");
                        if (idx > -1)
                        {
                            fileName = fileName.Substring(idx + 1);
                        }

                        //C1FileInfo _currentFile = new C1FileInfo();
                        //_currentFile._fileName = sDirectory + "\\" + fileName;
                        //_currentFile._fileName = _currentFile._fileName.Substring(0, _currentFile._fileName.IndexOf("\""));
                        //if (_currentFile._fileName.IndexOf(@"\") >= 0)
                        //{
                        //    _currentFile._fileName = _currentFile._fileName.Substring(_currentFile._fileName.LastIndexOf(@"\") + 1);
                        //}
                        ////_currentFile._contentType = str.Substring(contentTypePos + "content-type: ".Length);
                        ////_currentFile._contentType = this._currentFile.ContentType.Substring(0, this._currentFile.ContentType.IndexOf("\r\n"));
                        //_currentFile._extension = "";
                        //if (_currentFile._fileName.IndexOf('.') > 0)
                        //{
                        //    int extPos = _currentFile._fileName.LastIndexOf('.');
                        //    if (extPos < (_currentFile._fileName.Length - 1))
                        //    {
                        //        _currentFile._extension = _currentFile._fileName.Substring(extPos + 1);
                        //    }
                        //}

                        oFiles[i].SaveAs(sDirectory + "\\" + fileName);
                    }
                    context.Response.Write("Sucess");
                }
                else
                {
                    context.Response.Write("Fail");
                }
            }
            else
            {
                string id = context.Request.Params["C1UploadId"];
                string total = context.Request.Params["Total"];
                string ids = context.Request.Params["ids"];
                if (string.IsNullOrEmpty(total) || string.IsNullOrEmpty(ids))
                {
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    context.Response.ContentType = "text/xml";
                    var session = UploadSession.FromHttpApp(id);
                    if ((session != null) && !string.IsNullOrEmpty(session.Error))
                    {
                        context.Response.Write("<UploadError><![CDATA[");
                        context.Response.Write("{\"error\": \"" + session.Error + "\"}");
                        context.Response.Write("]]></UploadError>");
                    }
                    else
                    {
                        UploadProgress progress = session == null ? new UploadProgress(null) : session.UploadProgress;
                        if (session.Progress == 1.0)
                        {
                            PopulateUploadedFiles(context, session);
                        }

                        context.Response.Write("<UploadProgress><![CDATA[");
                        context.Response.Write(progress.GetJSONString());
                        context.Response.Write("]]></UploadProgress>");
                        if (session.Progress == 1.0)
                        {
							session.Complete();
                        }
                    }
                }
                // get total progress
                else
                {
                    string[] arrid = ids.Split(',');
                    long totalSize = 0;
                    long receivedSize = 0;
                    bool isAllBeginUpload = true;
                    var temp = new UploadSession();
                    foreach (string s in arrid)
                    {
                        var sessiont = UploadSession.FromHttpApp(s);
                        if (sessiont != null)
                        {
                            receivedSize += sessiont.ReceivedBytes;
                            totalSize += sessiont.TotalBytes;
                            if (sessiont.TotalBytes == 0)
                            {
                                isAllBeginUpload = false;
                            }
                            if (sessiont.Progress == 1)
                            {
                                //fixed bug 28278
                                if (sessiont.Upload.IsSeparatorProgress == false)
                                {
                                    PopulateUploadedFiles(context, sessiont);
									sessiont.Complete();
                                }
                            }
                        }
                    }
                    temp.ReceivedBytes = receivedSize;
                    temp.TotalBytes = totalSize;
                    if (isAllBeginUpload)
                    {
                        temp.Progress = receivedSize * 1.0 / totalSize;
                    }
                    else
                    {
                        temp.Progress = 0;
                    }

                    context.Response.Write(temp.UploadProgress.GetJSONString());
                }
            }
		}

		private void PopulateUploadedFiles(HttpContext context, UploadSession session)
		{
			string targetFolder = this.GetTargetFolder(session.TargetFolder, context);
			bool isValid = true;
			if (string.IsNullOrEmpty(targetFolder)) return;
			foreach (C1FileInfo file in session.UploadedFiles)
			{
				if (file.Size == 0 || string.IsNullOrEmpty(file.FileName)) continue;

				if (!Directory.Exists(targetFolder))
				{
					Directory.CreateDirectory(targetFolder);
				}
				if (File.Exists(Path.Combine(session.TempFolder, file.TempFileName)))
				{
					if (IsValidUploadedFile(file, session))
					{
						file.MoveTo(Path.Combine(targetFolder, file.FileName), true);
					}
					else {
						isValid = false;
					}
				}
			}

			session.IsValid = isValid;

			foreach (C1FileInfo file in session.UploadedFiles)
			{
				if (File.Exists(Path.Combine(session.TempFolder, file.TempFileName)))
				{
					file.DeleteTempFile();
				}
			}
		}
		private string GetTargetFolder(string targetFolder,HttpContext context)
		{
			if (targetFolder.Length == 0)
			{
				return string.Empty;
			}
			return context.Server.MapPath(targetFolder);
		}

		#region Validating

		private bool IsValidSize(C1FileInfo file, UploadSession session)
		{
			return (file.Size <= (session.MaximumBytes * 0x400));
		}

		private bool IsValidExtension(C1FileInfo file, UploadSession session)
		{
			foreach (string str in session.ValidFileExtensions)
			{
				string s = str.Replace("*", "");
				s = s.Replace(".", "");

				if (file.Extension.ToLower() == s.ToLower())
				{
					return true;
				}
			}
			return false;
		}

		private bool IsValidMimeType(C1FileInfo file, UploadSession session)
		{
			foreach (string str in session.ValidMimeTypes)
			{
				if (file.ContentType.ToLower() == str.ToLower())
				{
					return true;
				}
			}
			return false;
		}

		private bool IsValidUploadedFile(C1FileInfo file, UploadSession session)
		{
			return this.IsValidUploadedFile(file, session, session.MaximumBytes > 0, 
					session.ValidFileExtensions.Length > 0, session.ValidMimeTypes.Length > 0);
		}

		private bool IsValidUploadedFile(C1FileInfo fi, UploadSession session, bool checkingFileSize, bool checkingFileExtension, bool checkingMimeType)
		{
			bool valid = true;
			if (valid && checkingFileSize && !this.IsValidSize(fi, session))
			{
				valid = false;
			}
			if (valid && checkingFileExtension && !this.IsValidExtension(fi, session))
			{
				valid = false;
			}
			if (valid && checkingMimeType && !this.IsValidMimeType(fi, session))
			{
				valid = false;
			}

			return session.ValidateEvent(fi, valid);
		}

		#endregion
	}
}
