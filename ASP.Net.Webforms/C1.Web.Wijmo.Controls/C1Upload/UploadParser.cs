﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace C1.Web.Wijmo.Controls.C1Upload
{
	using C1.Web.Wijmo.Controls.Localization;


	internal class UploadParser
	{
		#region Fields

		private byte[] _buffer;
		private bool _bufferInitialized;
		private long _totalSize;
		private C1FileInfo _currentFile;
		private int _currentOffset;
		private Random _rand;
		
		private const int BUFFER_SIZE = 0x8000;
		private const int OVERLAP_SIZE = 0x400;
		private const int TOTAL_BUFFER_SIZE = 0x8400;

		#endregion

		#region Constructor

		public UploadParser()
		{
			this._currentFile = null;
			this._bufferInitialized = false;
			this._rand = new Random();
		}

		#endregion

		#region Methods

		public void StartProcess(long totalSize)
		{
			this._buffer = new byte[TOTAL_BUFFER_SIZE];
			Array.Clear(this._buffer, 0, TOTAL_BUFFER_SIZE);
			this._currentOffset = 0;
			this._totalSize = totalSize;
		}

		public void ProcessChunk(UploadSession session, byte[] buf, int size)
		{
			session.LastAccessTime = new DateTime();
			session.Progress = ((double)this._currentOffset) / ((double)this._totalSize);
			if (this._bufferInitialized)
			{
				if (size < BUFFER_SIZE)
				{
					byte[] destinationArray = new byte[OVERLAP_SIZE];
					Array.Copy(this._buffer, BUFFER_SIZE, destinationArray, 0, OVERLAP_SIZE);
					Array.Clear(this._buffer, 0, BUFFER_SIZE);
					Array.Copy(destinationArray, 0, this._buffer, 0, OVERLAP_SIZE);
					Array.Copy(buf, 0, this._buffer, 0x400, size);
				}
				else
				{
					Array.Copy(this._buffer, BUFFER_SIZE, this._buffer, 0, OVERLAP_SIZE);
					Array.Copy(buf, 0, this._buffer, OVERLAP_SIZE, BUFFER_SIZE);
				}
			}
			else
			{
				Array.Copy(buf, 0, this._buffer, 0, BUFFER_SIZE);
				Array.Copy(buf, 0x7c00, this._buffer, BUFFER_SIZE, OVERLAP_SIZE);
			}
			string str = Encoding.ASCII.GetString(this._buffer, 0, this._bufferInitialized ? TOTAL_BUFFER_SIZE : BUFFER_SIZE);
			int index = str.IndexOf(session.Boundary);
			if (index >= 0)
			{
				string lowStr = str.ToLower();
				while (index >= 0)
				{
					if (this._currentFile != null)
					{
						if ((this._currentOffset + index) > this._currentFile.StartOffset)
						{
							this._currentFile.EndOffset = (this._currentOffset + index) - "\r\n".Length;
							this._currentFile._size = this._currentFile.EndOffset - this._currentFile.StartOffset;
							if (this._currentFile._size > 0)
							{
								session.UploadedFiles.Add(this._currentFile);
								this.FinishWrite(this._currentFile);
							}
							this._currentFile = null;
						}
					}
					int nextBoundPos = str.IndexOf(session.Boundary, (int)(index + 1));
					int fileNamePos = lowStr.IndexOf("filename=\"", index);
					int contentTypePos = lowStr.IndexOf("content-type: ", index);
					if (((fileNamePos > index) && (contentTypePos > fileNamePos)) && ((nextBoundPos < 0) || (fileNamePos < nextBoundPos)))
					{
						int contentPos = lowStr.IndexOf("\r\n\r\n", contentTypePos);
						if (contentPos > contentTypePos)
						{
							if ((session.UploadedFiles.Count + 1) > session.MaximumFiles)
							{
								throw new Exception(C1Localizer.GetString("Too many files."));
							}
							this._currentFile = new C1FileInfo();
							int namePos = fileNamePos + "filename=\"".Length;
							this._currentFile._fileName = Encoding.UTF8.GetString(this._buffer, namePos, (this._bufferInitialized ? TOTAL_BUFFER_SIZE : BUFFER_SIZE) - namePos);
							this._currentFile._fileName = this._currentFile._fileName.Substring(0, this._currentFile._fileName.IndexOf("\""));
							if (this._currentFile._fileName.IndexOf(@"\") >= 0)
							{
								this._currentFile._fileName = this._currentFile._fileName.Substring(this._currentFile._fileName.LastIndexOf(@"\") + 1);
							}
							this._currentFile.StartOffset = (this._currentOffset + contentPos) + "\r\n\r\n".Length;
							this._currentFile._contentType = str.Substring(contentTypePos + "content-type: ".Length);
							this._currentFile._contentType = this._currentFile.ContentType.Substring(0, this._currentFile.ContentType.IndexOf("\r\n"));
							this._currentFile._extension = "";
							if (this._currentFile._fileName.IndexOf('.') > 0)
							{
								int extPos = this._currentFile._fileName.LastIndexOf('.');
								if (extPos < (this._currentFile._fileName.Length - 1))
								{
									this._currentFile._extension = this._currentFile._fileName.Substring(extPos + 1);
								}
							}
							this._currentFile.SetTempFileName(session.TempFolder + @"\" + this.CreateTempFileName(this._currentFile));
							session.CurrentFile = (string.IsNullOrEmpty(session.CurrentFile) ? "" : session.CurrentFile + ";") + this._currentFile._fileName;
						}
					}
					index = nextBoundPos;
				}
			}
			if (this._currentFile != null)
			{
				if (size < BUFFER_SIZE)
				{
					this._currentFile.EndOffset = this._currentOffset + size;
					this._currentFile._size = this._currentFile.EndOffset - this._currentFile.StartOffset;
					this.FinishWrite(this._currentFile);
					this._currentFile = null;
				}
				else
				{
					this.WriteChunk(this._currentFile);
				}
			}
			this._currentOffset += this._bufferInitialized ? BUFFER_SIZE : 0x7c00;
			this._bufferInitialized = true;

#if DEBUG
			System.Threading.Thread.Sleep(100);
#endif
		}

		public void FinishProcess(UploadSession session)
		{
			session.Progress = 1.0;
		}

		#endregion

		#region Private Methods

		private string CreateTempFileName(C1FileInfo upFileInfo)
		{
			return string.Format("{0}_{1}_{2}", DateTime.Now.ToString("yyyyMMddHHmmssffff"), this._rand.Next(), upFileInfo.FileName);
		}

		private void WriteChunk(C1FileInfo upFileInfo)
		{
			int num = this._bufferInitialized ? BUFFER_SIZE : 0x7c00;
			if (((this._currentOffset >= upFileInfo.LastWriteOffset) && (upFileInfo.EndOffset != upFileInfo.StartOffset)) && (upFileInfo.StartOffset < (this._currentOffset + num)))
			{
				if (upFileInfo.FileStream == null)
				{
					upFileInfo.FileStream = new FileStream(upFileInfo.TempFileName, FileMode.CreateNew, FileAccess.Write);
				}
				int offset = ((upFileInfo.StartOffset > this._currentOffset) && (upFileInfo.StartOffset < (this._currentOffset + num))) ? (((int)upFileInfo.StartOffset) - this._currentOffset) : (((int)upFileInfo.LastWriteOffset) - this._currentOffset);
				int fileNamePos = (upFileInfo.EndOffset > 0L) ? (((int)upFileInfo.EndOffset) - this._currentOffset) : num;
				upFileInfo.FileStream.Write(this._buffer, offset, fileNamePos - offset);
				upFileInfo.LastWriteOffset = this._currentOffset + fileNamePos;
			}
		}

		private void FinishWrite(C1FileInfo upFileInfo)
		{
			if (upFileInfo.FileStream == null)
			{
				upFileInfo.FileStream = new FileStream(upFileInfo.TempFileName, FileMode.CreateNew, FileAccess.Write);
			}
			if (this._currentOffset >= upFileInfo.LastWriteOffset)
			{
				int num = this._bufferInitialized ? TOTAL_BUFFER_SIZE : BUFFER_SIZE;
				int offset = ((upFileInfo.StartOffset >= this._currentOffset) && (upFileInfo.StartOffset < (this._currentOffset + num))) ? (((int)upFileInfo.StartOffset) - this._currentOffset) : (((int)upFileInfo.LastWriteOffset) - this._currentOffset);
				int endOffset = (upFileInfo.EndOffset < (this._currentOffset + num)) ? (((int)upFileInfo.EndOffset) - this._currentOffset) : num;
				upFileInfo.FileStream.Write(this._buffer, offset, endOffset - offset);
			}
			upFileInfo.FileStream.Flush();
			upFileInfo.FileStream.Close();
			upFileInfo.FileStream = null;
		}

		#endregion
	}
}
