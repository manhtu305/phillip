﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;


namespace C1.Web.Wijmo.Controls.C1Upload
{
    /// <summary>
    /// Class that represents the information of the uploaded file.
    /// </summary>
    public class C1FileInfo
    {
        internal FileStream FileStream;
        internal long StartOffset;
        internal long EndOffset;
        internal long LastWriteOffset;
        internal string _contentType = "";
        internal string _extension = "";
        internal string _fileName = "";
        internal long _size = 0;
        private string _tempFileName = "";
        private string _streamPath = "";

        /// <summary>
		/// Initializes a new instance of the <see cref="C1FileInfo"/> class.
        /// </summary>
        public C1FileInfo()
        {
        }

        /// <summary>
        /// Gets the HTTP MIME type of the output stream.
        /// </summary>
        public string ContentType
        {
            get { return _contentType; }
        }

        /// <summary>
        /// Gets the name of the uploaded file.
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
        }

        /// <summary>
        /// Gets the file extension of the uploaded file.
        /// </summary>
        public string Extension
        {
            get { return _extension; }
        }

        /// <summary>
        /// Gets the file size.
        /// </summary>
        public long Size
        {
            get { return _size; }
        }

        /// <summary>
        /// Gets the full path of the temporary file.
        /// </summary>
        public string TempFileName
        {
            get { return _tempFileName; }
        }

        /// <summary>
        /// Gets the file stream.
        /// </summary>
        /// <returns></returns>
        public FileStream GetStream()
        {
            return File.Open(_streamPath, FileMode.Open, FileAccess.Read);
        }

        /// <summary>
        /// Deletes the file from the temp folder.
        /// </summary>
        public void Delete()
        {
            File.Delete(_streamPath);
        }

        /// <summary>
        /// Saves the file to a specified path.
        /// </summary>
        /// <param name="path">The target path to save to.</param>
        public void SaveAs(string path)
        {
            this.SaveAs(path, false);
        }

        /// <summary>
        /// Saves the file to a specified path.
        /// </summary>
        /// <param name="path">The target path to save to.</param>
        /// <param name="overwrite">Whether to overwrite the existing file.</param>
        public void SaveAs(string path, bool overwrite)
        {
            File.Copy(this._streamPath, path, overwrite);
        }

        internal void SetTempFileName(string fileName)
        {
            _tempFileName = fileName;
            _streamPath = fileName;
        }

        internal void MoveTo(string path, bool overwrite)
        {
            File.Copy(this.TempFileName, path, overwrite);
            File.Delete(this.TempFileName);
            _streamPath = path;
        }

        internal void DeleteTempFile()
        {
            if (string.IsNullOrEmpty(this.TempFileName)) return;
            if (File.Exists(this.TempFileName))
            {
                try
                {
                    File.Delete(this.TempFileName);
                }
                catch
                {
                }
            }
        }
    }
}
