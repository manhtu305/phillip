﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web;
using System.IO;
using System.Collections;
using System.Web.UI.WebControls;
using System.Reflection;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Upload
{
	/// <summary>
	/// Represents a Upload control.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Upload.C1UploadDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Upload.C1UploadDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer("C1.Web.Wijmo.Controls.Design.C1Upload.C1UploadDesigner, C1.Web.Wijmo.Controls.Design.3" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Upload.C1UploadDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Upload runat=server width=300px></{0}:C1Upload>")]
	[ToolboxBitmap(typeof(C1Upload), ("Upload.png"))]
	[LicenseProviderAttribute()]
	public partial class C1Upload: ICallbackEventHandler
	{
		#region Fields

		private string[] _validFileExtensions;
		private string[] _validMimeTypes;

		#region license
		private bool _productLicensed = false;
		private bool _shouldNag;

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1Upload"/> class.
		/// </summary>
		public C1Upload()
		{
			VerifyLicense();
			_validFileExtensions = new string[0];
			_validMimeTypes = new string[0];
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Upload), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		
		#endregion

		#region Properties

		/// <summary>
		/// Keyboard shortcut used by the control.
		/// </summary>
		[C1Description("C1Upload.AccessKey")]
		[WidgetOption]
		[C1Category("Category.Accessibility")]
		[Layout(LayoutType.Accessibility)]
		public override string AccessKey
		{
			get
			{
				return base.AccessKey;
			}
			set
			{
				base.AccessKey = value;
			}
		}

		/// <summary>
		/// Gets or sets the virtual path of the folder in which to place temporary upload files.
		/// </summary>
		[DefaultValue("~/Temp")]
		[C1Description("C1Upload.TempFolder")]
		[C1Category("Category.Behavior")]
        [WidgetOption]
		[Layout(LayoutType.Behavior)]
		public string TempFolder
		{
			get
			{
				return this.GetPropertyValue("TempFolder", "~/Temp");
			}
			set
			{
				this.SetPropertyValue("TempFolder", value);
			}
		}

		/// <summary>
		/// Gets or sets the virtual path of the folder where C1Upload will automatically save the valid files after the upload completes.
		/// </summary>
		[C1Category("Category.Behavior")]
		[DefaultValue("~/UploadedFiles")]
		[C1Description("C1Upload.TargetFolder")]
        [WidgetOption]
        [WidgetOptionName("folder")]
		[Layout(LayoutType.Behavior)]
		public string TargetFolder
		{
			get
			{
				return this.GetPropertyValue("TargetFolder", "~/UploadedFiles");
			}
			set
			{
				this.SetPropertyValue("TargetFolder", value);
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(true)]
		public string Folder
		{
			get
			{
				return base.ResolveClientUrl(TargetFolder);
			}
		}

        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1Upload.EnableSWFUploadOnIE")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool EnableSWFUploadOnIE
        {
            get
            {
                return this.GetPropertyValue("EnableSWFUploadOnIE", false);
            }
            set
            {
                this.SetPropertyValue("EnableSWFUploadOnIE", value);
            }
        }

        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1Upload.EnableSWFUpload")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool EnableSWFUpload
        {
            get
            {
                return this.GetPropertyValue("EnableSWFUpload", false);
            }
            set
            {
                this.SetPropertyValue("EnableSWFUpload", value);
            }
        }

        /// <summary>
        /// SWF url path.
        /// </summary>
        [C1Description("C1Upload.SWFPath")]
        [WidgetOption]
        [DefaultValue("")]
        [C1Category("Category.Accessibility")]
        [Layout(LayoutType.Accessibility)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string SwfPath
        {
            get
            {
                return C1TargetControlHelper.ResolveEmbeddedResourceUrl("C1.Web.Wijmo.Controls.Resources.SWFUpload.swf", this.Page, false);
            }            
        }

		/// <summary>
		/// Gets or sets a value that indicates whether to show uploaded file list after uploaded.
		/// </summary>
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[C1Description("C1Upload.ShowUploadedFiles")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public bool ShowUploadedFiles
		{
			get
			{
				return this.GetPropertyValue("ShowUploadedFiles", false);
			}
			set
			{
				this.SetPropertyValue("ShowUploadedFiles", value);
			}
		}

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        [WidgetOption]
        public bool ShouldValidateFiles
        {
            get
            {
                return this.ValidatingFile != null;
            }
        }

		/// <summary>
		/// Gets or sets the unique ID that identifies the upload session. 
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DefaultValue("")]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public string UploadId
		{
			get
			{
				return this.GetPropertyValue("UploadId", "");
			}
			set
			{
				this.SetPropertyValue("UploadId", value);
			}
		}

		/// <summary>
		/// Gets or sets the maximum upload size to allow in KB.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Upload.MaximumFileSize")]
		[DefaultValue(0)]
        [WidgetOption]
		[Layout(LayoutType.Behavior)]
		public int MaximumFileSize
		{
			get
			{
				return this.GetPropertyValue("MaximumFileSize", 0);
			}
			set
			{
				if (value < 0) throw new ArgumentOutOfRangeException("MaximumFileSize can not be less than zero.");
				this.SetPropertyValue("MaximumFileSize", value);
			}
		}

		/// <summary>
		/// Gets or sets the array of allowed file extensions.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Upload.ValidFileExtensions")]
		[PersistenceMode(PersistenceMode.Attribute)]
		[TypeConverter(typeof(StringArrayConverter))]
		[Layout(LayoutType.Misc)]
		public string[] ValidFileExtensions
		{
			get
			{
				return this.GetPropertyValue("ValidFileExtensions", _validFileExtensions);
			}
			set
			{
				this.SetPropertyValue("ValidFileExtensions", value);
			}
		}

		/// <summary>
		/// Gets or sets the array of allowed mime types.
		/// </summary>
		[C1Category("Category.Behavior")]
		[PersistenceMode(PersistenceMode.Attribute)]
		[C1Description("C1Upload.ValidMimeTypes")]
		[TypeConverter(typeof(StringArrayConverter))]
		[Layout(LayoutType.Misc)]
		public string[] ValidMimeTypes
		{
			get
			{
				return this.GetPropertyValue("ValidMimeTypes", _validMimeTypes);
			}
			set
			{
				this.SetPropertyValue("ValidMimeTypes", value);
			}
		}

		/// <summary>
		/// Gets or sets a value that indicates whether to show separator progress for each file input.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Upload.IsSeparatorProgress")]
		[WidgetOption]
		[DefaultValue(true)]
		[Layout(LayoutType.Misc)]
		public bool IsSeparatorProgress
		{
			get
			{
				return GetPropertyValue<bool>("IsSeparatorProgress", true);
			}
			set
			{
				SetPropertyValue<bool>("IsSeparatorProgress", value);
			}
		}

		/// <summary>
		/// This event is for the client validatedFile event, when the server ValidatingFile event is fired, the validating event's information
		/// will be sent to client side. User can bind this event to show the server side validating messages.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[C1Description("C1Upload.OnClientValidatedFile")]
		[WidgetEvent("ev, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientValidatedFile 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientValidatedFile", "");
			}
			set {
				SetPropertyValue<string>("OnClientValidatedFile", value);
			}
		}

		/// <summary>
		/// Gets the value that corresponds to this Web server control. 
		/// This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region Methods

		/// <summary>
		/// Handles the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}
		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="HtmlTextWriter"/>
		/// </summary>
		/// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the server control content.</param>
		
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			if (!this.IsDesignMode)
			{
				this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass);
			}

			// to avoid accesskey attribute being render to the outmost div element.
			string accessKey = this.AccessKey;
			this.AccessKey = string.Empty;
			Unit height = this.Height;
			this.Height = Unit.Empty;
			base.AddAttributesToRender(writer);

			this.CssClass = cssClass;
			this.AccessKey = accessKey;
			this.Height = height;
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (this.IsDesignMode)
			{
				this.Controls.Clear();
				this.Controls.Add(CreateUploadButton());
			}
			base.Render(writer);
		}

		private Control CreateUploadButton()
		{
			HtmlAnchor uploadBtn = new HtmlAnchor();
			uploadBtn.HRef = "#";
			uploadBtn.Attributes["class"] = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";

			HtmlGenericControl span = new HtmlGenericControl("span");
			span.Attributes["class"] = "ui-button-text";
			span.InnerText = C1Localizer.GetString("C1Upload.Localization.UploadFiles",
					"Upload files");
			uploadBtn.Controls.Add(span);

			return uploadBtn;
		}

		/// <summary>
		/// Raises the PreRender event.
		/// </summary>
		/// <remarks>
		/// Provides additional processing during the pre-render phase of the control and invokes the OnPreRender method of its base class in order to raise the PreRender event.
		/// </remarks>
		/// <param name="e">An EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
			Page.ClientScript.GetCallbackEventReference(this, "", "", "");
		}

		/// <summary>
		/// Handles the <see cref="E:System.Web.UI.Control.Load"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			//if ((HttpContext.Current != null) && (HttpContext.Current.Request.Params["C1UploadId"] != null))
			//{
			//    this.UploadId = HttpContext.Current.Request.Params["C1UploadId"];
			//}

			//if (this.UploadId != "")
			//{
			//    //System.Diagnostics.Debugger.Launch();
			//    UploadSession info = (UploadSession)HttpContext.Current.Application["C1Upload_" + this.UploadId];
			//    //if (info != null && !String.IsNullOrEmpty(info.CurrentFile))
			//    if (info != null)
			//    {
			//        if (info.Progress == 1.0)
			//        {
			//            //this.PopulateUploadedFiles();
			//            //this.ProcessValidFiles();
			//            //this.OnUploaded(EventArgs.Empty);
			//        }

			//        info.Processed = true;
			//        //info.Reset();
			//    }
			//}

			//this.UploadId = this.CreateUploadSession();
			this.UploadId = this.UniqueID;
		}


		private string CreateUploadSession()
		{
			var info = new UploadSession();
			info.TempFolder = this.Page.MapPath(this.TempFolder);
			if (!Directory.Exists(info.TempFolder))
			{
				Directory.CreateDirectory(info.TempFolder);
			}
			info.TargetFolder = this.TargetFolder;
			info.MaximumBytes = this.MaximumFileSize * 0x400;
			info.ValidFileExtensions = this.ValidFileExtensions;
			info.ValidMimeTypes = this.ValidMimeTypes;
			info.Upload = this;
			info.MaximumFiles = this.MaximumFiles == 0 ? int.MaxValue : this.MaximumFiles;
			info.AddToHttpApp();
			return info.Id;
		}

		#endregion

		#region Events

		/// <summary>
		/// Occurs when validating the uploaded file.
		/// </summary>
		[C1Description("C1Upload.ValidatingFile")]
        public event ValidateFileEventHandler ValidatingFile;

		/// <summary>
		/// Fires the ValidatingFile event.
		/// </summary>
		/// <param name="e">ValidateFileEventArgs that contains the event data.</param>
		protected virtual void OnValidatingFile(ValidateFileEventArgs e)
		{
			if (this.ValidatingFile != null)
				this.ValidatingFile(this, e);
		}

		internal bool ValidateEvent(C1FileInfo fi, bool valid, out string message)
		{
			ValidateFileEventArgs e = new ValidateFileEventArgs(fi, valid);
			this.OnValidatingFile(e);
			message = e.Message;
			return e.IsValid;
		}

		#endregion

		#region ** ICallbackEventHandler

		private Hashtable _argument;

		/// <summary>
		/// ICallbackEventHandler.GetCallbackResult implementation.
		/// </summary>
		/// <returns>Call back result.</returns>
		string ICallbackEventHandler.GetCallbackResult()
		{
			string commandName = _argument["commandName"].ToString();
			if (commandName == "GetUploadID")
			{
				return CreateUploadSession();
			}
			else if (commandName == "CancelUpload") 
			{
				string id = _argument["fileID"].ToString();
				var info = UploadSession.FromHttpApp(id);
				if (info != null)
				{
					info.Complete();
				}
				return string.Empty;
			}
            else if (commandName == "ValidateFile")
            {
                C1FileInfo info = new C1FileInfo();
                if (_argument["size"] != null)
                {
                    info._size = Convert.ToInt64(_argument["size"]);
                    info._extension = _argument["type"].ToString();
                    info._fileName = _argument["name"].ToString();
                }
                ValidateFileEventArgs e = new ValidateFileEventArgs(info, true);
                this.OnValidatingFile(e);
                return e.IsValid ? "1" : "0";
            }
			//else if (commandName == "UploadComplete")
			//{
			//    string uploadId = _argument["uploadId"].ToString();
			//    if (uploadId != "")
			//    {
			//        UploadSession info = (UploadSession)HttpContext.Current.Application["C1Upload_" + uploadId];
			//        if (info != null)
			//        {
			//            if (info.Progress == 1.0)
			//            {
			//                //this.PopulateUploadedFiles();
			//                //this.ProcessValidFiles();
			//                //this.OnUploaded(EventArgs.Empty);
			//            }
			//            info.Processed = true;
			//        }
			//    }
			//    return "";
			//}
			return "";
		}

		/// <summary>
		/// ICallbackEventHandler.RaiseCallbackEvent implementation.
		/// </summary>
		/// <param name="eventArgument"> A System.String that represents an optional event argument to be passed to the event handler.</param>
		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			this._argument = JsonHelper.StringToObject(eventArgument);
		} 
		#endregion
	}
}
