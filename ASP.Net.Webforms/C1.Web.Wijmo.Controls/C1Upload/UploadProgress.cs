﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Upload
{

	/// <summary>
	/// Class that wraps the data of the uploading progress.
	/// </summary>
	public class UploadProgress
	{
		private UploadSession _session;

		/// <summary>
		/// Constructor of the UploadProgress.
		/// </summary>
		/// <param name="session">The upload session.</param>
		internal UploadProgress(UploadSession session)
		{
			_session = session;
		}

		/// <summary>
		/// Gets a value that indicates whether the session is active.
		/// </summary>
		[Json(true, true, true)]
		public bool Active
		{
			get { return _session != null; }
		}

		/// <summary>
		/// Gets the number of bytes received during upload
		/// </summary>
		[Json(true, true, 0)]
		public long ReceivedBytes
		{
			get 
			{
				if (_session == null) return 0;

				return _session.ReceivedBytes; 
			}
		}

		/// <summary>
		/// Gets the total number of bytes received during upload.
		/// </summary>
		[Json(true, true, 0)]
		public long TotalBytes
		{
			get 
			{
				if (_session == null) return 0;

				return _session.TotalBytes; 
			}
		}

		/// <summary>
		/// Gets the file that is currently uploading.
		/// </summary>
		[Json(true, true, "")]
		public string CurrentFile
		{
			get 
			{
				if (_session == null) return "";

				return _session.CurrentFile; 
			}
		}

		/// <summary>
		/// Gets the progress value ranging from 0.0 to 1.0.
		/// </summary>
		[Json(true, true, 0.0)]
		public double Progress
		{
			get 
			{
				if (_session == null) return 0.0;

				return _session.Progress; 
			}
		}

		/// <summary>
		/// Gets the elapsed time in second.
		/// </summary>
		[Json(true, true, 0)]
		public double EllapsedTime
		{
			get 
			{
				if (_session == null) return 0;

				return _totalSeconds; 
			}
		}

		/// <summary>
		/// Gets the amount of time remaining for the upload in seconds.
		/// </summary>
		[Json(true, true, 0)]
		public double RemainingTime
		{
			get 
			{
				if (_session == null) return 0;

				return (_session.Progress == 0.0) ? 0.0 : ((_totalSeconds * (1.0 - _session.Progress)) / _session.Progress); 
			}
		}

		/// <summary>
		/// Gets the upload speed.
		/// </summary>
		[Json(true, true, 0.0)]
		public double Speed
		{
			get 
			{
				if (_session == null) return 0;

				return ((double)(_session.ReceivedBytes / 0x400L)) / _totalSeconds; 
			}
		}
		[Json(true, true, "")]
		public string Message
		{
			get
			{
				return _session.Message;
			}
		}

		[Json(true, false, false)]
		public bool IsValid
		{
			get
			{
				return _session.IsValid;
			}
		}

		private double _totalSeconds = 0;
		private void CalcTotalSeconds()
		{
			if (_session == null) return;

			_totalSeconds = Math.Max(0.1, DateTime.Now.Subtract(_session.StartTime).TotalSeconds);
		}

		internal string GetJSONString()
		{
			this.CalcTotalSeconds();
			string s = JsonHelper.ObjectToString(this, null);
			//string s = JsonHelper.WidgetToString(this, null);
			return s;
		}
	}
}
