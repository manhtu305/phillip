﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Web;
using System.Reflection;
using System.Threading;


namespace C1.Web.Wijmo.Controls.C1Upload
{
	using C1.Web.Wijmo.Controls.Localization;


	/// <summary>
	/// HttpModule for C1Upload to handle http requests.
	/// </summary>
	public class UploadModule : IHttpModule
	{
		/// <summary>
		/// Initializes a new instance of the UploadModule class.
		/// </summary>
		public UploadModule()
		{
		}

		private DateTime _lastCleanTime;

		#region IHttpModule

		/// <summary>
		/// Initializes a module and prepares it to handle requests.
		/// </summary>
		/// <param name="app"></param>
		void IHttpModule.Init(HttpApplication app)
		{
			_lastCleanTime = new DateTime();

			app.BeginRequest += new EventHandler(this.HandleBeginRequest);
			app.Error += new EventHandler(this.HandleError);
		}

		/// <summary>
		/// Disposes of the resources (other than memory) used by the UploadModule.
		/// </summary>
		void IHttpModule.Dispose()
		{
		}

		#endregion

		#region HandleUpload

		private void HandleUpload(HttpApplication app, HttpWorkerRequest wkReq, UploadSession session)
		{
			if (!wkReq.HasEntityBody()) return;

			const int BUFFER_SIZE = 0x8000;
			int length = 0;
			byte[] destArray = new byte[BUFFER_SIZE];

			// Get upload file size
			session.ReceivedBytes = 0;
			session.TotalBytes = int.Parse(wkReq.GetKnownRequestHeader(HttpWorkerRequest.HeaderContentLength));

			// Is the upload too big
			if ((session.MaximumBytes > 0) && (session.TotalBytes > session.MaximumBytes))
			{
				throw new Exception(C1Localizer.GetString("C1Uploade.MaxSizeReached"));
			}

			// Start parser
			UploadParser parser = new UploadParser();
			parser.StartProcess(session.TotalBytes);

			 //Load pre-loaded data
			byte[] preloadedEntityBody = wkReq.GetPreloadedEntityBody();
			if (preloadedEntityBody != null)
			{
				session.ReceivedBytes = length = preloadedEntityBody.Length;
				int sourceIndex = 0;

				// Is there enough stuff preloaded for processing?
				while (length >= BUFFER_SIZE)
				{
					Array.Copy(preloadedEntityBody, sourceIndex, destArray, 0, BUFFER_SIZE);
					parser.ProcessChunk(session, destArray, BUFFER_SIZE);
					sourceIndex += BUFFER_SIZE;
					length -= BUFFER_SIZE;
				}

				// Is there leftover data?
				if (length > 0)
				{
					Array.Copy(preloadedEntityBody, sourceIndex, destArray, 0, length);
				}
			}

			// Is there more?
			if (!wkReq.IsEntireEntityBodyIsPreloaded())
			{
				int receivedBytes  = 0;
				while ((session.TotalBytes > session.ReceivedBytes) && wkReq.IsClientConnected())
				{
					// Read the next chunk
					receivedBytes  = wkReq.ReadEntityBody(destArray, length, Math.Min(BUFFER_SIZE - length, (int)(session.TotalBytes - session.ReceivedBytes)));
					if (receivedBytes  == 0)
					{
						break;
					}
					session.ReceivedBytes += receivedBytes ;
					length += receivedBytes ;

					// Have we filled the buffer?
					if (length == BUFFER_SIZE)
					{
						// Process the chunk
						parser.ProcessChunk(session, destArray, BUFFER_SIZE);

						// Restart buffer
						length = 0;
					}
				}
			}
			// Process the last chunk
			parser.ProcessChunk(session, destArray, length);

			// Complete process
			parser.FinishProcess(session);
		}

		#endregion

		#region Http Events

		private void HandleBeginRequest(object sender, EventArgs args)
		{
			var app = (HttpApplication)sender;
			if (!IsUploadRequest(app.Request)) return;

			var httpWorkerRequest = GetHttpWorkerRequest(app.Context);
			if (httpWorkerRequest == null) return;

			var session = this.InitSession(app);

			try
			{
				if (!session.Aborted)
				{
					HandleUpload(app, httpWorkerRequest, session);
				}
			}
			catch (Exception exception)
			{
				session.Error = exception.Message;
				session.Aborted = true;
			}
			finally
			{
				session.RemoveFromHttpApp(20000);
				app.Response.End();
			}
		}
		
		private void HandleError(object sender, EventArgs args)
		{
			var application = (HttpApplication)sender;
			if (IsUploadRequest(application.Request))
			{
				RemoveSession((HttpApplication)sender);
			}
		}

		#endregion

		#region Private Stuff

		private bool IsUploadRequest(HttpRequest req)
		{
			return (req.HttpMethod != null && req.HttpMethod.ToLower() == "post" 
				&& req.ContentType.Substring(0, Math.Min("multipart/form-data".Length, req.ContentType.Length)).ToLower().StartsWith("multipart/form-data") 
				&& !String.IsNullOrEmpty(req.QueryString["C1UploadId"]));
		}

		private string GetBoundaryString(HttpRequest req)
		{
			string id = "boundary=";
			int index = req.ContentType.IndexOf(id);
			if (index < 0)
			{
				return null;
			}
			return ("--" + req.ContentType.Substring(index + id.Length));
		}

		private HttpWorkerRequest GetHttpWorkerRequest(HttpContext context)
		{
			return (HttpWorkerRequest)context.GetType().GetProperty("WorkerRequest", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(context, null);
		}

		private UploadSession InitSession(HttpApplication app)
		{
			string id = app.Request.QueryString["C1UploadId"];
			var session = UploadSession.FromHttpApp(id);

			if (session != null)
			{
				session.Reset();
				session.Boundary = this.GetBoundaryString(app.Request);
				session.StartTime = session.LastAccessTime = DateTime.Now;
				return session;
			}

			session = new UploadSession(id)
			{
				Aborted = true,
				Error = "Upload session is expired."
			};
			session.AddToHttpApp();
			return session;
		}

		private void RemoveSession(HttpApplication app)
		{
			var id = app.Request.QueryString["C1UploadId"];
			UploadSession.RemoveFromHttpApp(id);
		}
		#endregion

	}
}
