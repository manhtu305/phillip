﻿using System;
using System.Collections;
using System.Web;
using System.Timers;


namespace C1.Web.Wijmo.Controls.C1Upload
{
	internal class UploadSession : IDisposable
	{
        public string Error = "";
		public string Message = "";
		public bool IsValid = false;
        public bool Aborted;
        public long MaximumBytes;
        public int MaximumFiles;
        public string TempFolder = "";
        public string TargetFolder = "";
        public ArrayList UploadedFiles;
        public string[] ValidFileExtensions = new string[0];
        public string[] ValidMimeTypes = new string[0];
        public C1Upload Upload = null;

        internal string Boundary = "";
        internal DateTime StartTime;
        internal DateTime LastAccessTime;
        public long ReceivedBytes;
        public long TotalBytes;
        public double Progress;
        public string CurrentFile = "";
        private UploadProgress _progress;
		private Timer _destroyTimer;
		private const string StateIdPrefix = "C1Upload_";
		private readonly string _stateId;


		public UploadSession(string id = null)
		{
			Id = id ?? Guid.NewGuid().ToString();
			_stateId = StateIdPrefix + Id;
			UploadedFiles = new ArrayList();
			_progress = new UploadProgress(this);
		}

	    public void Dispose()
        {
            foreach (C1FileInfo info in this.UploadedFiles)
            {
                info.Delete();
            }
        }

	    public string Id { get; private set; }

	    public UploadProgress UploadProgress
        {
            get { return _progress; }
        }

        public void Reset()
        {
            this.Progress = 0;
            this.CurrentFile = "";
            this.TotalBytes = 0;
            this.ReceivedBytes = 0;
            this.Error = "";
            this.Aborted = false;
            this.Boundary = "";
            this.StartTime = DateTime.Now;
            this.UploadedFiles.Clear();
	        ResetDestroyTimer();
        }

		public void Complete()
		{
			RemoveFromHttpApp();
		}

        public bool ValidateEvent(C1FileInfo fi, bool valid)
        {
			if (Upload != null)
			{
				string message = "";

				valid = Upload.ValidateEvent(fi, valid, out message);
				Message = Message == "" ? message : Message + ";" + message;
			}
			IsValid = valid;
			return valid;
        }

		/// <summary>
		/// Remove current session from HttpApplicationState.
		/// </summary>
		/// <param name="millisecond">A integer indicates after how many milliseconds to remove the session.</param>
		public void RemoveFromHttpApp(int millisecond = 0)
		{
			ResetDestroyTimer();

			if (FromHttpApp(Id) == null)
			{
				return;
			}

			if (millisecond == 0)
			{
				InnerRemoveHttpApp();
				return;
			}

			_destroyTimer = new Timer(millisecond);
			_destroyTimer.Elapsed += DestroyTimerElapsed;
			_destroyTimer.Start();
		}

		private void ResetDestroyTimer()
		{
			if (_destroyTimer != null)
			{
				RemoveDestroyTimer();
				_destroyTimer.Dispose();
				_destroyTimer = null;
			}
		}

		private void DestroyTimerElapsed(object sender, ElapsedEventArgs e)
		{
			InnerRemoveHttpApp();
			RemoveDestroyTimer();
		}

		private void RemoveDestroyTimer()
		{
			if (_destroyTimer == null)
			{
				return;
			}

			if (_destroyTimer.Enabled)
			{
				_destroyTimer.Stop();
			}

			_destroyTimer.Elapsed -= DestroyTimerElapsed;
		}

		private void InnerRemoveHttpApp()
		{
			var session = FromHttpApp(Id);
			if (session != null)
				HttpContext.Current.Application.Remove(_stateId);
		}

		public void AddToHttpApp()
		{
			HttpContext.Current.Application[_stateId] = this;
		}

		public static void RemoveFromHttpApp(string id, int millisecond = 0)
		{
			var session = FromHttpApp(id);
			if (session != null)
				session.RemoveFromHttpApp(millisecond);
		}

		public static UploadSession FromHttpApp(string id)
		{
			return (UploadSession)HttpContext.Current.Application[StateIdPrefix + id];
		}
	}
}
