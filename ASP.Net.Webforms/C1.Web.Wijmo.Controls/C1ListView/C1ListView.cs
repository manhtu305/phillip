﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.IO;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo;
using System.Collections.Specialized;
using System.Reflection;
using C1.Web.Wijmo.Controls.Base.Interfaces;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    /// <summary>
    /// Represents a ListView in an ASP.NET Web page.
    /// </summary>
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1ListView.C1ListViewDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1ListView.C1ListViewDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35                               
    [Designer("C1.Web.Wijmo.Controls.Design.C1ListView.C1ListViewDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else                                         
    [Designer("C1.Web.Wijmo.Controls.Design.C1ListView.C1ListViewDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1ListView runat=server></{0}:C1ListView>")]
    [ToolboxBitmap(typeof(C1ListView), "C1ListView.png")]
    [LicenseProviderAttribute()]
    public partial class C1ListView : C1TargetHierarchicalDataBoundControlBase, INamingContainer,
        IC1ListViewItemCollectionOwner, IC1Serializable
    {
        #region ** Fields

        private C1ListViewItemCollection _items = null;
        private C1ListViewItemCollection _itemsFromDB = null;
        private C1ListViewItemCollection _itemsTotal = null;
        private C1ListViewItemBindingCollection _bindings;
        internal int normalItemIndex = 0;
        private bool _isDirty = false;

        #region licensing
        internal bool _productLicensed;
        private bool _shouldNag;
        #endregion

        #endregion

        #region ** constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ListView"/> class.
        /// </summary>
        public C1ListView()
        {
            VerifyLicense();
        }

        #endregion

        internal virtual void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ListView), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #region ** Properties

        internal override bool IsWijWebControl
        {
            get
            {
                return false;
            }
        }

        [DefaultValue(WijmoControlMode.Mobile)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override WijmoControlMode WijmoControlMode
        {
            get
            {
                return base.WijmoControlMode;
            }
            set
            {
                base.WijmoControlMode = value;
            }
        }

        /// <summary>
        /// Gets a <see cref="C1ListViewItemCollection"/> collection that contains the item <see cref="C1ListView"/> object.
        /// </summary>
        [C1Description("C1ListView.Items")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Appearance)]
        [NotifyParentProperty(true)]
        [Browsable(false)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        public C1ListViewItemCollection Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new C1ListViewItemCollection(this);
                }
                return this._items;
            }
        }

        [NotifyParentProperty(true)]
        [Browsable(false)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal C1ListViewItemCollection DBItems
        {
            get
            {
                if (_itemsFromDB == null)
                {
                    _itemsFromDB = new C1ListViewItemCollection(this);
                }
                return this._itemsFromDB;
            }
        }

        [NotifyParentProperty(true)]
        [Browsable(false)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        private C1ListViewItemCollection TotalItems
        {
            get
            {
                if (_itemsTotal == null)
                {
                    _itemsTotal = new C1ListViewItemCollection(this);
                }
                return this._itemsTotal;
            }
        }

        /// <summary>
        /// There is no such owner for <see cref="C1ListView"/>.
        /// </summary>
        [Browsable(false)]
        public IC1ListViewItemCollectionOwner Owner
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Determines whether to set C1ListView as numbered list style.
        /// </summary>
        [DefaultValue(false)]
        [C1Description("C1ListView.UseNumberedList")]
        [Layout(LayoutType.Appearance)]
        [C1Category("Category.Appearance")]
        public bool UseNumberedList
        {
            get { return this.GetPropertyValue("UseNumberedList", false); }
            set { this.SetPropertyValue("UseNumberedList", value); }
        }

        [DefaultValue(-1)]
        [C1Category("Category.Behavior")]
        internal int DataBindStartlevel
        {
            get { return this.GetPropertyValue("DataBindStartLevel", -1); }
            set { this.SetPropertyValue("DataBindStartLevel", value); }
        }

        /// <summary>
        /// Gets or sets a value indicates whether this instance is dirty.
        /// </summary>
        [Browsable(false)]
        [DesignOnly(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsDirty
        {
            get
            {
                return this._isDirty;
            }
            set
            {
                this._isDirty = value;
            }
        }

        internal virtual bool AutoGenerateDataBindings
        {
            get
            {
                if (this._bindings == null)
                    return true;
                return this._bindings.Count == 0;
            }
        }

        #endregion

        #region ** Data Binding

        /// <summary>
        /// Create a new instance of the <see cref="C1ListViewItem"/> class.
        /// </summary>
        /// <returns></returns>
        public C1ListViewItem CreateListViewItem(DataBindingItemType itemtype, string datatype)
        {
            C1ListViewItem item = null;
            switch (itemtype)
            {
                case DataBindingItemType.divideritem:
                    item = new C1ListViewDividerItem();
                    break;
                case DataBindingItemType.buttonitem:
                    item = new C1ListViewButtonItem();
                    break;
                case DataBindingItemType.inputitem:
                    item = new C1ListViewInputItem();
                    break;
                case DataBindingItemType.linkitem:
                    item = new C1ListViewLinkItem();
                    break;
                case DataBindingItemType.nesteditem:
                    item = new C1ListViewNestedItem();
                    break;
                case DataBindingItemType.flipswitchitem:
                    item = new C1ListViewFlipSwitchItem();
                    break;
                case DataBindingItemType.controlgroupitem:
                    item = new C1ListViewControlGroupItem();
                    break;
            }

            if (item == null)
            {
                switch (datatype)
                {
                    case "listviewdivideritem":
                        item = new C1ListViewDividerItem();
                        break;
                    case "listviewbuttonitem":
                        item = new C1ListViewButtonItem();
                        break;
                    case "listviewinputitem":
                        item = new C1ListViewInputItem();
                        break;
                    case "listviewlinkitem":
                        item = new C1ListViewLinkItem();
                        break;
                    case "listviewnesteditem":
                        item = new C1ListViewNestedItem();
                        break;
                    case "listviewflipswitchitem":
                        item = new C1ListViewFlipSwitchItem();
                        break;
                    case "listviewcontrolgroupitem":
                        item = new C1ListViewControlGroupItem();
                        break;
                    default:
                        item = new C1ListViewItem();
                        break;
                }
            }
            item.ListView = this;
            return item;
        }

        [C1Category("Category.Data")]
        [C1Description("C1ListView.DataBindings")]
        [DefaultValue((string)null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public C1ListViewItemBindingCollection DataBindings
        {
            get
            {
                if (this._bindings == null)
                {
                    this._bindings = new C1ListViewItemBindingCollection(this);
                }
                return this._bindings;
            }
        }

        internal virtual C1ListViewItemBinding GetBinding(string dataMember, int depth)
        {
            if (this._bindings == null)
                return null;
            return this._bindings.GetBinding(dataMember, depth);
        }

        /// <summary>
        /// Binds a data source to the invoked <see cref="C1ListView"/> list view and all its items.
        /// </summary>
        public sealed override void DataBind()
        {
            base.DataBind();
        }

        /// <summary>
        /// When overridden in a derived class, binds data from the data source to the
        /// control.
        /// </summary>
        protected override void PerformDataBinding()
        {
            base.PerformDataBinding();
            this.DBItems.Clear();

            if (!IsBoundUsingDataSourceID && (DataSource == null))
            {
                return;
            }
            if (!IsBoundUsingDataSourceID && DataSource.GetType().Name == "HierarchicalSampleData")
            {
                return;
            }
            HierarchicalDataSourceView view = this.GetData(null);
            if (view == null)
            {
                throw new InvalidOperationException("NoDataSourceView");
            }
            IHierarchicalEnumerable enumerable = view.Select();

            CreateItemsFromDataSource(enumerable);

            CreateChildControlsFromItems();
        }

        private void CreateItemsFromDataSource(IHierarchicalEnumerable enumerable)
        {
            foreach (object o in enumerable)
            {
                IHierarchyData data = enumerable.GetHierarchyData(o);

                string dataType = data.Type;

                C1ListViewItemBinding binding = this.GetBinding(dataType, DataBindStartlevel);

                if (binding == null)
                {
                    binding = this.GetBinding(dataType, 0);
                }

                C1ListViewItem item = null;
                if (binding != null)
                {
                    item = this.CreateListViewItem(binding.ItemType, dataType);
                }
                else
                {
                    item = this.CreateListViewItem(DataBindingItemType.item, dataType);
                }
                item.DataBind(binding, enumerable, o, this.AutoGenerateDataBindings);
                this.DBItems.Add(item);

                if (data.HasChildren && (DataBindStartlevel > item.CalculateCurrentLevel() || DataBindStartlevel == -1))
                {
                    IHierarchicalEnumerable childEnum = data.GetChildren();
                    if (childEnum != null)
                    {
                        item.DataBindInternal(childEnum, 1, binding);
                    }
                }
            }
        }

        #endregion

        #region ** Override Methods

        bool _subControlsDataBound;
        /// <summary>
        /// Calls the System.Web.UI.WebControls.BaseDataBoundControl.DataBind() method
        /// if the System.Web.UI.WebControls.BaseDataBoundControl.DataSourceID property
        /// is set and the data-bound control is marked to require binding.
        /// </summary>
        protected override void EnsureDataBound()
        {
            base.EnsureDataBound();
            if (!this._subControlsDataBound)
            {
                foreach (Control control in this.Controls)
                {
                    control.DataBind();
                }
                this._subControlsDataBound = true;
            }
        }

        protected override void EnsureChildControls()
        {
            base.EnsureChildControls();
            if (this.Controls.Count < 1 && this.Items.Count > 0 || this.IsDirty)
            {
                this.CreateChildControls();
                this.IsDirty = false;
            }
        }

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            if (base.RequiresDataBinding && (!string.IsNullOrEmpty(this.DataSourceID) || (this.DataSource != null)))
            {
                this.EnsureDataBound();
            }

            CreateChildControlsFromItems();
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            if (!IsDesignMode)
            {
                int count = this.TotalItems.Count;

                for (int i = 0; i < count; i++)
                {
                    this.TotalItems[i].RenderControl(writer);
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }
            base.OnInit(e);
            if (!IsDesignMode &&
                (!string.IsNullOrEmpty(this.DataSourceID)
                || (this.DataSource != null)))
            {
                this.RequiresDataBinding = true;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }

        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers. 
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                if (DesignMode)
                {
                    return HtmlTextWriterTag.Div;
                }
                if (UseNumberedList)
                {
                    return HtmlTextWriterTag.Ol;
                }
                return HtmlTextWriterTag.Ul;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (DesignMode)
            {
                CreateChildControlsFromItems();
            }

            AddItemsAsControlsIntoListView();

			if (this.Filter)
			{
				writer.AddAttribute("data-filter", "true");
				writer.AddAttribute("data-filter-placeholder", this.FilterPlaceholder);
			}

            base.Render(writer);

            if (DesignMode)
            {
                if (this.Filter)
                {
                    string itemCssClass = "";

                    string filterThemeClass = this.FilterTheme == "" ? this.ThemeSwatch : this.FilterTheme;

                    itemCssClass = "ui-listview-filter ui-bar-" + filterThemeClass;
                    writer.AddAttribute("class", itemCssClass);
                    writer.RenderBeginTag("div");

                    itemCssClass = "ui-input-search ui-shadow-inset ui-btn-corner-all ui-btn-shadow ui-icon-searchfield ui-body-" + filterThemeClass;
                    writer.AddAttribute("class", itemCssClass);
                    writer.RenderBeginTag("div");

                    itemCssClass = "ui-input-text ui-body-" + filterThemeClass;
                    writer.AddAttribute("class", itemCssClass);
                    writer.AddAttribute("value", this.FilterPlaceholder);
                    writer.RenderBeginTag("input");

                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }

                if (Inset)
                {
                    writer.AddAttribute("class", "ui-listview ui-listview-inset ui-corner-all ui-shadow");
                    writer.AddAttribute("style", "margin: 1em 0; border-radius: 0.6em 0.6em 0.6em 0.6em;");
                }
                else
                {
                    writer.AddAttribute("class", "ui-listview");
                }
                writer.RenderBeginTag("ul");
                normalItemIndex = 1;

                FindTopAndBottomItem(TotalItems);

                foreach (C1ListViewItem item in this.TotalItems)
                {
                    if (!item.ScreenHidden)
                    {
                        item.RenderControl(writer);
                        if (item.SetAsDivider)
                        {
                            normalItemIndex = 1;
                        }
                        else
                        {
                            normalItemIndex++;
                        }
                    }
                }
                writer.RenderEndTag();
            }
        }

        #endregion

        #region ** private methods

        private void CreateChildControlsFromItems()
        {
            this.Controls.Clear();

            for (int i = 0; i < Items.Count; i++)
            {
                C1ListViewItem item = this.Items[i];

                item.CreateChildControlsInListView(this);
            }

            for (int i = 0; i < DBItems.Count; i++)
            {
                C1ListViewItem item = this.DBItems[i];

                item.CreateChildControlsInListView(this);
            }

            ChildControlsCreated = true;
        }

        private void AddItemsAsControlsIntoListView()
        {
            this.TotalItems.Clear();
            foreach (C1ListViewItem item in Items)
            {
                if (!(AutoDividers && item is C1ListViewDividerItem))
                {
                    TotalItems.Add(item);
                }
                this.Controls.Add(item);
            }
            foreach (C1ListViewItem item in DBItems)
            {
                if (!(AutoDividers && item is C1ListViewDividerItem))
                {
                    TotalItems.Add(item);
                }
                this.Controls.Add(item);
            }

            if (IsDesignMode && AutoDividers)
            {
                int count = TotalItems.Count;
                int index = 0;
                string flagString = "AA";
                while (index < count)
                {
                    string strForAD = TotalItems[index].GetStringForAutoDivider();
                    if (strForAD != flagString)
                    {
                        C1ListViewDividerItem divider = new C1ListViewDividerItem();
                        divider.Text = strForAD;
                        TotalItems.Insert(index, divider);

                        flagString = strForAD;
                        count++;
                        //index++;
                    }
                    index++;
                }
            }
        }

        private void FindTopAndBottomItem(C1ListViewItemCollection targetItems)
        {
            if (targetItems != null)
            {
                int count = targetItems.Count;
                if (count > 0)
                {
                    int up = 0;
                    int bottom = count - 1;
                    while (targetItems[up].ScreenHidden || targetItems[bottom].ScreenHidden)
                    {
                        if (up == bottom)
                        {
                            break;
                        }
                        if (targetItems[up].ScreenHidden)
                        {
                            up++;
                        }
                        if (targetItems[bottom].ScreenHidden)
                        {
                            bottom--;
                        }
                    }
                    targetItems[up].isTop = true;
                    targetItems[bottom].isBottom = true;
                }
            }
        }

        #endregion

        #region ** IC1Serializable Layout
        /// <summary>
        /// Saves the control layout properties to the file.
        /// </summary>
        /// <param name="filename">The file where the values of the layout properties will be saved.</param> 
        public void SaveLayout(string filename)
        {
            C1ListViewSerializer sz = new C1ListViewSerializer(this);
            sz.SaveLayout(filename);
        }

        /// <summary>
        /// Saves control layout properties to the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be saved.</param> 
        public void SaveLayout(System.IO.Stream stream)
        {
            C1ListViewSerializer sz = new C1ListViewSerializer(this);
            sz.SaveLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        public void LoadLayout(string filename)
        {
            C1ListViewSerializer sz = new C1ListViewSerializer(this);
            sz.LoadLayout(filename);
        }

        /// <summary>
        /// Load control layout properties from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
        public void LoadLayout(System.IO.Stream stream)
        {
            C1ListViewSerializer sz = new C1ListViewSerializer(this);
            sz.LoadLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        public void LoadLayout(string filename, LayoutType layoutTypes)
        {
            C1ListViewSerializer sz = new C1ListViewSerializer(this);
            sz.LoadLayout(filename, layoutTypes);
        }

        /// <summary>
        /// Loads the control layout properties with specified types from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        public void LoadLayout(System.IO.Stream stream, LayoutType layoutTypes)
        {
            C1ListViewSerializer sz = new C1ListViewSerializer(this);
            sz.LoadLayout(stream, layoutTypes);
        }
        #endregion

        #region ** IStateManager

        protected override void LoadViewState(object savedState)
        {
            if (savedState == null)
            {
                base.LoadViewState(null);
                return;
            }
            else
            {
                Pair p = (Pair)savedState;
                if (p == null)
                {
                    throw new ArgumentException("Wrrong ViewState Data");
                }
                base.LoadViewState(p.First);
                if (p.Second != null)
                {
                    object[] saveItems = (object[])p.Second;

                    for (int i = 0; i < Items.Count; i++)
                    {
                        ((IStateManager)Items[i]).LoadViewState(saveItems[i]);
                    }
                }
            }
        }

        protected override object SaveViewState()
        {
            Pair p = new Pair();
            p.First = base.SaveViewState();
            object[] saveItems = new object[this.Items.Count];
            for (int i = 0; i < Items.Count; i++)
            {
                saveItems[i] = ((IStateManager)Items[i]).SaveViewState();
            }
            p.Second = saveItems;

            for (int i = 0; i < 2; i++)
            {
                if (p.First != null || p.Second != null)
                {
                    return p;
                }
            }

            return null;
        }

        #endregion
    }


}
