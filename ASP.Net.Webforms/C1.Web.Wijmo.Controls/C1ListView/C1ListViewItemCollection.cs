﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    public class C1ListViewItemCollection : C1ObservableItemCollection<IC1ListViewItemCollectionOwner, C1ListViewItem>
    {
        #region ** constructor

        /// <summary>
        /// Explicit constructor for <see cref="C1ListViewItemCollection"/>.
        /// </summary>
        /// <param name="owner"></param>
        public C1ListViewItemCollection(IC1ListViewItemCollectionOwner owner)
            : base(owner)
        {

        }

        #endregion

        #region ** methods ** TODO

        /// <summary>
        /// Adds new listview item to the end of the list.
        /// </summary>
        /// <param name="child">Child menu item</param>
        public new void Add(C1ListViewItem child)
        {
            if (child != null)
            {
                this.Insert(this.Count, child);
            }
        }

        /// <summary>
        /// Insert <see cref="C1MenuItem"/> item to specific position into the collection.
        /// </summary>
        /// <param name="index">Position, value  should be mayor or equal to 0</param>
        /// <param name="child">Child menu item</param>
        public new void Insert(int index, C1ListViewItem child)
        {
            child.SetParent(Owner);

            base.InsertItem(index, child);
        }

        #endregion
    }
}
