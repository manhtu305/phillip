﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    public enum InputType
    {
        label,
        text,
        textarea,
        search,
        slider
    }

    public enum ControlGroupType
    { 
        normal,
        checkboxlist,
        radiolist
    }

    public enum DataBindingItemType
    {
        item,
        divideritem,
        buttonitem,
        inputitem,
        linkitem,
        nesteditem,
        flipswitchitem,
        controlgroupitem,
        controlgroupinnerlistitem
    }
}
