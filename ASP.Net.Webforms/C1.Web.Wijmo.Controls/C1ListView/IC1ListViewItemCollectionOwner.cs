﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    /// <summary>
    /// Used to establish a relationship between the <see cref="C1ListViewItemCollection"/> nodes collection 
    /// and its <see cref="IC1ListViewItemCollectionOwner"/> owner.
    /// </summary>
    public interface IC1ListViewItemCollectionOwner
    {
        /// <summary>
        /// Collection that contains <see cref="C1ListViewItem"/> nodes.
        /// </summary>
        C1ListViewItemCollection Items
        {
            get;
        }

        /// <summary>
        /// Owner of <see cref="C1ListViewItemCollection"/> collection.
        /// </summary>
        IC1ListViewItemCollectionOwner Owner
        {
            get;
        }
    }
}
