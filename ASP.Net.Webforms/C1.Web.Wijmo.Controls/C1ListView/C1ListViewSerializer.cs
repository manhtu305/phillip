﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    public class C1ListViewSerializer : C1BaseSerializer<C1ListView, C1ListViewItem, IC1ListViewItemCollectionOwner>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="C1ListViewSerializer"/> class.
        /// </summary>
        /// <param name="obj"></param>
        public C1ListViewSerializer(object obj)
            : base(obj)
        { }

        #region ** prottected override

        /// <summary>
        /// This method preliminary used by control developers..
        /// </summary>
        /// <param name="collection">ListView item collection</param>
        /// <param name="item">ListView item</param>
        /// <returns>Return true if object is added to array or collection.</returns>
        protected override bool OnAddItem(object collection, object item)
        {
            if (collection is C1ListViewItemCollection)
            {
                ((C1ListViewItemCollection)collection).Add((C1ListViewItem)item);
                return true;
            }
            return base.OnAddItem(collection, item);
        }

        /// <summary>
        /// This method preliminary used by control developers..
        /// </summary>
        /// <param name="collection">ListView item collection</param>
        /// <returns>Return true if array or collection is cleared.</returns>
        protected override bool OnClearItems(object collection)
        {
            if (collection is C1ListViewItemCollection)
            {
                ((C1ListViewItemCollection)collection).Clear();
                return true;
            }
            return base.OnClearItems(collection);
        }

        #endregion

    }
}
