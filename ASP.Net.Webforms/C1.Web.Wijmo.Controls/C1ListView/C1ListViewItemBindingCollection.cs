﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using C1.Web.Wijmo.Controls.Base.Collections;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    public class C1ListViewItemBindingCollection : C1ObservableItemCollection<C1ListView, C1ListViewItemBinding>
    {
        #region ** field
        private C1ListViewItemBinding _defaultBinding;
        #endregion

        #region ** constructors
        internal C1ListViewItemBindingCollection(C1ListView owner)
            : base(owner)
        { }

        #endregion

        #region ** private and internal implementation

        private void FindDefaultBinding()
        {
            this._defaultBinding = null;
            foreach (C1ListViewItemBinding binding in this)
            {
                if (binding.Depth == -1 && binding.DataMember.Length == 0)
                {
                    this._defaultBinding = binding;
                    return;
                }
            }
        }

        internal C1ListViewItemBinding GetBinding(string dataMember, int depth)
        {
            C1ListViewItemBinding binding = null;
            int num = 0;
            if ((dataMember != null) && (dataMember.Length == 0))
            {
                dataMember = null;
            }
            foreach (C1ListViewItemBinding bind in this)
            {
                if (bind.Depth == depth)
                {
                    if (string.Compare(bind.DataMember, dataMember, true, Thread.CurrentThread.CurrentCulture) == 0)
                    {
                        return bind;
                    }
                    if ((num < 1) && (bind.DataMember.Length == 0))
                    {
                        binding = bind;
                        num = 1;
                    }
                }
                if (string.Compare(bind.DataMember, dataMember, true, Thread.CurrentThread.CurrentCulture) == 0)
                {
                    if (bind.Depth == depth)
                    {
                        return bind;
                    }
                    if ((num < 2) && (bind.Depth == -1))
                    {
                        binding = bind;
                        num = 2;
                    }
                }
            }
            if ((binding != null) || (this._defaultBinding == null))
            {
                return binding;
            }
            if ((this._defaultBinding.Depth != -1) || (this._defaultBinding.DataMember.Length != 0))
            {
                this.FindDefaultBinding();
            }
            return this._defaultBinding;
        }

        #endregion
    }
}
