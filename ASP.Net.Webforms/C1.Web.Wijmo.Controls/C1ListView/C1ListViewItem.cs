﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using C1.Web.Wijmo.Controls.Base;
using System.Globalization;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    [ToolboxItem(false)]
    public class C1ListViewItem : UIElement, INamingContainer,
        IC1ListViewItemCollectionOwner, IJsonRestore, IStateManager
    {
        #region ** Fields

        protected Hashtable _properties = new Hashtable();
        internal C1ListViewItemCollection _items;
        private IC1ListViewItemCollectionOwner _owner;
        private C1ListView _listView;
        private ITemplate _template;

        internal HtmlGenericControl _spanNumber = null;
        internal HtmlGenericControl _spanContent = null;
        internal HtmlGenericControl _aExtendLink = null;
        internal HtmlGenericControl _paraSupplement = null;
        internal HtmlGenericControl _spanCountBubble = null;
        internal HtmlGenericControl _imgThumbNail = null;
        internal HtmlGenericControl _imgIcon = null;
        internal HtmlGenericControl _templateContainer;

        internal bool isTop = false;
        internal bool isBottom = false;
        internal string _itemID;

        #endregion

        #region ** contructor
        /// <summary>
        /// Initializes a new instance of the <see cref="C1ListViewItem"/> class
        /// </summary>
        public C1ListViewItem()
        {

        }

        #endregion

        #region ** Properties

        /// <summary>
        /// Gets a <see cref="C1ListViewItemCollection"/> that contains the first-level child items of the current item
        /// </summary>
        [Browsable(false)]
        public virtual C1ListViewItemCollection Items
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// The owner of <see cref="C1ListViewItemCollection"/> to which this node belongs to.
        /// </summary>
        [Browsable(false)]
        public IC1ListViewItemCollectionOwner Owner
        {
            get { return this._owner; }
        }

        /// <summary>
        /// Gets reference to <see cref="C1ListView"/> ListView object.
        /// </summary>
        [Browsable(false)]
        public C1ListView ListView
        {
            get
            {
                if (_listView == null)
                {
                    if (this.Owner != null)
                    {
                        if (this.Owner is C1ListViewItem)
                        {
                            _listView = ((C1ListViewItem)this.Owner).ListView;
                        }
                        else if (this.Owner is C1ListView)
                        {
                            _listView = (C1ListView)this.Owner;
                        }
                    }
                }
                return _listView;
            }
            internal set
            {
                _listView = value;
            }
        }

        /// <summary>
        /// Sets or gets the text of item
        /// </summary>
        [C1Description("C1ListViewItem.Text")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public virtual string Text
        {
            get
            {
                return _properties["Text"] == null ? ""
                    : _properties["Text"].ToString();
            }
            set
            {
                _properties["Text"] = value;
            }
        }

        /// <summary>
        /// Add a content for filter to search against. 
        /// If it is added, the contents of the list item are ignored.
        /// </summary>
        [C1Description("C1ListViewItem.FilterText")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string FilterText
        {
            get
            {
                return _properties["FilterText"] == null ? ""
                    : _properties["FilterText"].ToString();
            }
            set
            {
                _properties["FilterText"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the Supplemental information of item
        /// </summary>
        [C1Description("C1ListViewItem.Supplement")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string Supplement
        {
            get
            {
                return _properties["Supplement"] == null ? ""
                    : _properties["Supplement"].ToString();
            }
            set
            {
                _properties["Supplement"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the message in countbubble of item
        /// </summary>
        [C1Description("C1ListViewItem.CountBubbleMessage")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string CountBubbleMessage
        {
            get
            {
                return _properties["CountBubbleMessage"] == null ? ""
                    : _properties["CountBubbleMessage"].ToString();
            }
            set
            {
                _properties["CountBubbleMessage"] = value;
            }
        }

        [C1Description("C1ListViewItem.DataIcon")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string DataIcon
        {
            get
            {
                return _properties["DataIcon"] == null ? ""
                   : (string)_properties["DataIcon"];
            }
            set
            {
                _properties["DataIcon"] = value;
            }
        }

        [C1Description("C1ListViewItem.Theme")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string Theme
        {
            get
            {
                return _properties["Theme"] == null ? ""
                    : (string)_properties["Theme"];
            }
            set
            {
                _properties["Theme"] = value;
            }
        }

        [C1Description("C1ListViewItem.ThumbNailSrc")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public virtual string ThumbNailSrc
        {
            get
            {
                return _properties["ThumbNailSrc"] == null ? ""
                    : (string)_properties["ThumbNailSrc"];
            }
            set
            {
                _properties["ThumbNailSrc"] = value;
            }
        }

        [C1Description("C1ListViewItem.IconSrc")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public virtual string IconSrc
        {
            get
            {
                return _properties["IconSrc"] == null ? ""
                    : (string)_properties["IconSrc"];
            }
            set
            {
                _properties["IconSrc"] = value;
            }
        }

        [C1Description("C1ListViewItem.ScreenHidden")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        public bool ScreenHidden
        {
            get
            {
                return _properties["ScreenHidden"] == null ? false
                    : (bool)_properties["ScreenHidden"];
            }
            set
            {
                _properties["ScreenHidden"] = value;
            }
        }

        /// <summary>
        /// Flag which can assign that this item is a divider.
        /// </summary>
        [C1Description("C1ListViewItem.SetAsDivider", "Set this property to true if you want to render this item as divider")]
        [DefaultValue(false)]
        [RefreshProperties(RefreshProperties.All)]
        [Layout(LayoutType.Appearance)]
        [Browsable(false)]
        internal virtual bool SetAsDivider
        {
            get
            {
                return false;
            }
        }

        [C1Description("C1ListViewItem.SetAsLinkItem", "If this item is link item or nested item, it will return true")]
        [DefaultValue(false)]
        [RefreshProperties(RefreshProperties.All)]
        [Layout(LayoutType.Appearance)]
        [Browsable(false)]
        internal virtual bool SetAsLinkItem
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets or sets the template that will be used for the specified C1ListViewItem.
        /// </summary>
        [C1Description("C1ListViewItem.Template")]
        [C1Category("Category.Behavior")]
        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public virtual ITemplate Template
        {
            get
            {
                return this._template;
            }
            set
            {
                this._template = value;
            }
        }

        /// <summary>
        /// Gets or sets the template container.
        /// </summary>
        /// <value>The template container.</value>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public HtmlGenericControl TemplateContainer
        {
            get
            {
                return this._templateContainer;
            }
            set
            {
                this._templateContainer = value;
            }
        }

        protected bool UseDefaultTheme
        {
            get
            {
                return this.Theme == "";
            }
        }

        protected string ItemID
        {
            get
            {
                if (String.IsNullOrEmpty(_itemID))
                {
                    if (this.Owner is C1ListView)
                    {
                        _itemID = this.ListView.ID + "_ItemPrivateID" + this.ID + this.ListView.Items.IndexOf(this).ToString() + this.ListView.DBItems.IndexOf(this).ToString();
                    }
                    else
                    {
                        _itemID = ((C1ListViewItem)(this.Owner)).ItemID + "_NestedItemPrivateID" + this.ID + this.Owner.Items.IndexOf(this).ToString();
                    }
                }
                return _itemID;
            }
        }

        #endregion

        #region ** Data Binding

        internal int CalculateCurrentLevel()
        {
            if (Owner != null && Owner is C1ListViewItem)
            {
                return ((C1ListViewItem)Owner).CalculateCurrentLevel() + 1;
            }
            else
            {
                return 0;
            }
        }

        internal virtual bool DataBind(C1ListViewItemBinding binding, IHierarchicalEnumerable enumerable, object data, bool auto)
        {
            bool bound = false;
            IHierarchyData hdata = enumerable.GetHierarchyData(data);
            string hType = hdata.Type;
            if (binding != null)
            {
                PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(data);

                SetValueFromDataBindingField(binding, data, pdc, bound);
            }
            else if (data is INavigateUIData)
            {
                INavigateUIData n_data = (INavigateUIData)data;
                this.Text = n_data.Name;
                //this.NavigateUrl = n_data.NavigateUrl;
                //this.SetAsDivider = false;
                bound = true;
            }

            if (auto && string.IsNullOrEmpty(this.Text))
            {
                this.Text = data.ToString();
                bound = true;
            }

            return bound;
        }

        protected virtual void SetValueFromDataBindingField(C1ListViewItemBinding binding, object data, PropertyDescriptorCollection pdc, bool bound)
        {
            object fieldValue = null;

            //Text
            if (!string.IsNullOrEmpty(binding.TextField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.TextField);
                if (fieldValue != null)
                {
                    this.Text = (string)fieldValue;
                    bound = true;
                }
            }

            //ThumbNailSrc
            if (!string.IsNullOrEmpty(binding.ThumbNailSrcField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.ThumbNailSrcField);
                if (fieldValue != null)
                {
                    this.ThumbNailSrc = (string)fieldValue;
                    bound = true;
                }
            }

            //IconSrc
            if (!string.IsNullOrEmpty(binding.IconSrcField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.IconSrcField);
                if (fieldValue != null)
                {
                    this.IconSrc = (string)fieldValue;
                    bound = true;
                }
            }

            //CountBubbleMessage
            if (!string.IsNullOrEmpty(binding.CountbubbleMessageField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.CountbubbleMessageField);
                if (fieldValue != null)
                {
                    this.CountBubbleMessage = (string)fieldValue;
                    bound = true;
                }
            }

            //SupplementField
            if (!string.IsNullOrEmpty(binding.SupplementField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.SupplementField);
                if (fieldValue != null)
                {
                    this.Supplement = (string)fieldValue;
                    bound = true;
                }
            }

            //Theme
            if (!string.IsNullOrEmpty(binding.ThemeField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.ThemeField);
                if (fieldValue != null)
                {
                    this.Theme = (string)fieldValue;
                    bound = true;
                }
            }
        }

        protected object GetFieldValue(object data, PropertyDescriptorCollection propCollection, string propName)
        {
            PropertyDescriptor descr = propCollection.Find(propName, true);
            if (descr != null)
            {
                return descr.GetValue(data);
            }
            else return null;
        }

        internal virtual void DataBindInternal(IHierarchicalEnumerable enumerable, int depth, C1ListViewItemBinding parBinding)
        {
            
        }

        #endregion

        #region ** Override Methods

        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Li;
            }
        }

        /// <summary>
        /// Renders the HTML opening tag of the control to the specified writer. This
        /// method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">
        /// A System.Web.UI.HtmlTextWriter that represents the output stream to render
        /// HTML content on the client.
        /// </param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            if (IsDesignMode)
            {
                string itemCssClass = string.Empty;

                //Call a virtual method to get different Css for different type of items.
                itemCssClass = RenderBeginTagCssInDesign();

                if (isTop)
                {
                    if (this.ListView.Inset)
                    {
                        itemCssClass += " ui-corner-top";
                    }
                }
                if (isBottom)
                {
                    if (this.ListView.Inset)
                    {
                        itemCssClass += " ui-corner-bottom";
                    }
                    itemCssClass += " ui-li-last";
                }

                if (!string.IsNullOrEmpty(this.CountBubbleMessage))
                {
                    itemCssClass += " ui-li-has-count";
                }
                if (!string.IsNullOrEmpty(this.ThumbNailSrc))
                {
                    itemCssClass += " ui-li-has-thumb";
                    if (!(this is C1ListViewButtonItem || this is C1ListViewInputItem || SetAsLinkItem))
                    {
                        writer.AddAttribute("style", "height: 60px; display: block");
                    }
                }
                if (!string.IsNullOrEmpty(this.IconSrc))
                {
                    itemCssClass += " ui-li-has-icon";
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Class, itemCssClass);
            }
            else
            {
                RenderBeginTagDataInRunTime();
            }

            base.RenderBeginTag(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            InitContentSpan();
            InitExtendControls();

            this.EnsureChildControls();

            // ** Design time
            if (IsDesignMode)
            {
                if (this.SetAsLinkItem)
                {
                    RenderLinkItemContentInDesign(writer);
                }
                else
                {
                    RenderStaticItemContentInDesign(writer);
                }
            }
            // ** Run time
            else
            {
                RenderBasicSection(writer);

                RenderCustomSection(writer);

                RenderCountBubble(writer);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            this.EnsureChildControls();

            base.Render(writer);
        }

        #endregion

        #region ** Coustome protected Methods for rendering

        protected virtual void InitContentSpan()
        {
            _spanContent = new HtmlGenericControl("span");
            if (IsDesignMode)
            {
                if (Template == null)
                {
                    _spanContent.InnerHtml = this.Text;
                }
                else
                {
                    Template.InstantiateIn(_spanContent);
                }
            }
            else
            {
                if (Template == null)
                {
                    _spanContent.InnerHtml = this.Text;
                }
            }
        }

        /// <summary>
        /// Initialize controls which 
        /// contains Item's index number, ThumbNail image, Icon image, Supplement information, Count bubble.
        /// </summary>
        protected virtual void InitExtendControls()
        {
            _spanNumber = null;
            _imgThumbNail = null;
            _imgIcon = null;
            _paraSupplement = null;
            _spanCountBubble = null;
            if (!this.SetAsDivider && this.ListView.UseNumberedList)
            {
                _spanNumber = new HtmlGenericControl("span");
                _spanNumber.Attributes.Add("style", "font-weight: normal; font-size: 12px;");
                _spanNumber.InnerHtml = this.ListView.normalItemIndex.ToString() + ". ";
            }
            if (!string.IsNullOrEmpty(this.ThumbNailSrc))
            {
                _imgThumbNail = new HtmlGenericControl("img");
                if (IsDesignMode)
                {
                    _imgThumbNail.Attributes.Add("style", "height: 80px");
                }
                _imgThumbNail.Attributes.Add("src", this.ThumbNailSrc);
                _imgThumbNail.Attributes.Add("alt", this.Text);
                _imgThumbNail.Attributes.Add("class", "ui-li-thumb");
            }
            if (!string.IsNullOrEmpty(this.IconSrc))
            {
                _imgIcon = new HtmlGenericControl("img");
                if (IsDesignMode)
                {
                    _imgIcon.Attributes.Add("style", "height: 16px; width: 16px");
                }
                _imgIcon.Attributes.Add("src", this.IconSrc);
                _imgIcon.Attributes.Add("alt", this.Text);
                _imgIcon.Attributes.Add("class", "ui-li-icon ui-li-thumb");
            }
            if (!string.IsNullOrEmpty(this.Supplement))
            {
                _paraSupplement = new HtmlGenericControl("p");
                string asideCss = IsDesignMode ? "ui-li-aside ui-li-desc" : "ui-li-aside";
                _paraSupplement.Attributes.Add("class", asideCss);
                _paraSupplement.InnerHtml = this.Supplement;
                if (Template == null)
                {
                    _paraSupplement.Attributes.Add("style", "margin: -0.5em 0 0.6em;");
                }
            }
            if (!string.IsNullOrEmpty(this.CountBubbleMessage))
            {
                _spanCountBubble = new HtmlGenericControl("span");
                _spanCountBubble.InnerHtml = this.CountBubbleMessage;
                if (!IsDesignMode)
                {
                    _spanCountBubble.Attributes.Add("class", "ui-li-count");
                }
                else
                {
                    _spanCountBubble.Attributes.Add("class",
                        "ui-li-count ui-btn-up-" + this.ListView.CountTheme + " ui-btn-corner-all");
                }
            }
        }

        #region ** private Methods for rendering different items in design time

        /// <summary>
        /// Helper function for getting a string to construct CSS when rendering begin tag in design time.
        /// </summary>
        /// <returns></returns>
        protected virtual string RenderBeginTagCssInDesign()
        {
            string itemCssClass = string.Empty;
            string themeClass = UseDefaultTheme ? this.ListView.ThemeSwatch : this.Theme;
            itemCssClass += "ui-li ui-li-static" + " ui-btn-up-" + themeClass;

            return itemCssClass;
        }

        private void RenderStaticItemContentInDesign(HtmlTextWriter writer)
        {
            HtmlGenericControl staticItemContainer = new HtmlGenericControl("div");
            if (_imgThumbNail != null)
            {
                staticItemContainer.Controls.Add(_imgThumbNail);
            }
            if (_imgIcon != null)
            {
                _imgIcon.Style.Add(HtmlTextWriterStyle.Height, "16px");
                staticItemContainer.Controls.Add(_imgIcon);
            }
            if (_paraSupplement != null)
            {
                staticItemContainer.Controls.Add(_paraSupplement);
            }

            if (_spanNumber != null)
            {
                staticItemContainer.Controls.Add(_spanNumber);
            }
            
            this.RenderCustomContentInDesign(writer, staticItemContainer);

            if (_spanContent != null)
            {
                staticItemContainer.Controls.Add(_spanContent);
            }

            if (_spanCountBubble != null)
            {
                staticItemContainer.Controls.Add(_spanCountBubble);
            }
            staticItemContainer.RenderControl(writer);
        }

        private void RenderLinkItemContentInDesign(HtmlTextWriter writer)
        {
            writer.AddAttribute("class", "ui-btn-inner ui-li");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute("class", "ui-btn-text");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            HtmlGenericControl innerLink = new HtmlGenericControl("a");
            innerLink.Attributes.Add("class", "ui-link-inherit");
            if (_imgThumbNail != null)
            {
                innerLink.Attributes.Add("style", "height: 60px; display: block");
                innerLink.Controls.Add(_imgThumbNail);
            }
            if (_imgIcon != null)
            {
                innerLink.Controls.Add(_imgIcon);
            }

            if (_spanNumber != null)
            {
                innerLink.Controls.Add(_spanNumber);
            }

            if (_paraSupplement != null)
            {
                innerLink.Controls.Add(_paraSupplement);
            }

            if (_spanContent != null)
            {
                innerLink.Controls.Add(_spanContent);
            }

            innerLink.RenderControl(writer);

            if (_spanCountBubble != null)
            {
                _spanCountBubble.RenderControl(writer);
            }

            writer.RenderEndTag();

            this.RenderCustomContentInDesign(writer, null);
        }

        /// <summary>
        /// This method is used for rendering the content inside each item in design time.
        /// </summary>
        /// <param name="writer"></param>
        protected virtual void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {

        }

        #endregion

        #region ** different steps of rendering item in Run Time.

        /// <summary>
        /// Helper function for adding data property when rendering begin tag in run time
        /// </summary>
        protected virtual void RenderBeginTagDataInRunTime()
        {
            if (ScreenHidden)
            {
                this.Attributes.Add("class", "ui-screen-hidden");
            }
            if (!UseDefaultTheme)
            {
                this.Attributes.Add("data-theme", this.Theme);
            }
            if (this.Template != null)
            {
                this.Attributes.Add("data-role", "fieldcontain");
            }
            if (!string.IsNullOrEmpty(this.FilterText))
            {
                this.Attributes.Add("data-filtertext", this.FilterText);
            }
        }

        /// <summary>
        /// BasicSection contains ThumbNail, Icon, content area, split button.
        /// This method is used for rendering in run time.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="span"></param>
        protected virtual void RenderBasicSection(HtmlTextWriter writer)
        {
            if (_paraSupplement != null)
            {
                this.Controls.Add(_paraSupplement);
                _paraSupplement.RenderControl(writer);
            }

            if (_imgThumbNail != null)
            {
                this.Controls.Add(_imgThumbNail);
                _imgThumbNail.RenderControl(writer);
            }
            if (_imgIcon != null)
            {
                this.Controls.Add(_imgIcon);
                _imgIcon.RenderControl(writer);
            }
            if (Template == null)
            {
                if (_spanContent != null)
                {
                    this.Controls.Add(_spanContent);
                    _spanContent.RenderControl(writer);
                }
            }
        }

        /// <summary>
        /// Method for rendering section which contains template and some special part of some particular items
        /// such as "ButtonControl" in C1ListViewButtonItem, "TextBox" in C1ListViewInputItem.
        /// </summary>
        /// <param name="writer"></param>
        protected virtual void RenderCustomSection(HtmlTextWriter writer)
        {
            if (Template != null)
            {
                foreach (Control control in TemplateContainer.Controls)
                {
                    control.RenderControl(writer);
                }
            }
        }

        /// <summary>
        /// Method for rendering some other information .
        /// Contains supplemental information, count bubble.
        /// 1. Confused that when split button exist, supplemental information will render behind split button.
        /// 2. Not sure what should be renderd in count bubble for now.
        /// </summary>
        /// <param name="writer"></param>
        private void RenderCountBubble(HtmlTextWriter writer)
        {
            if (_spanCountBubble != null)
            {
                this.Controls.Add(_spanCountBubble);
                _spanCountBubble.RenderControl(writer);
            }
        }

        #endregion

        #endregion

        #region ** Internal Methods

        internal void SetParent(IC1ListViewItemCollectionOwner aParent)
        {
            this._owner = aParent;
        }

        /// <summary>
        ///  Initialize some important child controls in ListView. 
        ///  As a result, they can be accessed in Page_Load, Page_PreRenderComplete.
        ///  controls in template can not be accessed in Page_Load.
        /// </summary>
        /// <param name="parent"></param>
        internal virtual void CreateChildControlsInListView(C1ListView parent)
        {
            RenderTemplateIntoContainer(parent);
        }

        protected void RenderTemplateIntoContainer(C1ListView parent)
        {
            ITemplate template = this.Template;


            if (template != null)
            {
                HtmlGenericControl container = new HtmlGenericControl("span");
                this.TemplateContainer = container;
                template.InstantiateIn(container);

                parent.Controls.Add(container);
            }

            if (IsDesignMode)
            {
                return;
            }

            this.ChildControlsCreated = true;
        }

        /// <summary>
        /// Get a string to simulate the return value of client function "AutodividersSelector".
        /// </summary>
        /// <returns></returns>
        internal virtual string GetStringForAutoDivider()
        {
            string original = string.Empty;
            if (!string.IsNullOrEmpty(Supplement))
            {
                return Supplement.ToCharArray()[0].ToString().ToUpper();
            }
            else if (this.Template != null)
            {
                foreach (object control in this.TemplateContainer.Controls)
                {
                    LiteralControl item = control as LiteralControl;
                    if (item != null)
                    {
                        string orignal = item.Text;
                        string[] originalStringArray = item.Text.Split(new char[] { '<', '>' });
                        if (originalStringArray.Count() > 2)
                        {
                            return originalStringArray[2].ToCharArray()[0].ToString().ToUpper();
                        }
                    }
                }
            }
            else if (!string.IsNullOrEmpty(Text))
            {
                return Text.ToCharArray()[0].ToString().ToUpper();
            }
            return "";
        }

        #endregion

        #region ** IJsonRestore interface implementations
        /// <summary>
        /// Avoid to deserialize the items property.
        /// </summary>
        /// <param name="obj">object to deserialize.</param>
        public void C1DeserializeItems(object obj)
        {

        }

        void IJsonRestore.RestoreStateFromJson(object state)
        {
            this.RestoreStateFromJson(state);
        }

        protected virtual void RestoreStateFromJson(object state)
        {
            JsonRestoreHelper.RestoreStateFromJson(this, state);
        }

        #endregion

        #region ** IStateManager

        public new bool IsTrackingViewState
        {
            get { return base.IsTrackingViewState; }
        }

        public new void LoadViewState(object state)
        {
            this._properties = (Hashtable)state;
        }

        public new object SaveViewState()
        {
            return this._properties;
        }

        public new void TrackViewState()
        {
            base.TrackViewState();
        }

        #endregion
    }

    /// <summary>
    /// Divider Items.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewDividerItem : C1ListViewItem
    {
        /// <summary>
        /// Flag which can assign that this item is a divider.
        /// </summary>
        internal override bool SetAsDivider
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Helper function for getting a string to construct CSS when rendering begin tag in design time.
        /// </summary>
        /// <returns></returns>
        protected override string RenderBeginTagCssInDesign()
        {
            string itemCssClass = string.Empty;
            string dividerThemeClass = UseDefaultTheme ? this.ListView.DividerTheme : this.Theme;

            itemCssClass += "ui-li ui-li-divider ui-bar-" + dividerThemeClass;

            return itemCssClass;
        }

        /// <summary>
        /// Helper function for adding data property when rendering begin tag in run time
        /// </summary>
        protected override void RenderBeginTagDataInRunTime()
        {
            base.RenderBeginTagDataInRunTime();

            this.Attributes.Add("data-role", "list-divider");
        }
    }

    /// <summary>
    /// Button Items.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewButtonItem : C1ListViewItem
    {
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1ListViewButtonItem.OnClientClick")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientClick
        {
            get
            {
                return _properties["OnClientClick"] == null ? ""
                    : _properties["OnClientClick"].ToString();
            }
            set
            {
                _properties["OnClientClick"] = value;
            }
        }
        
        private HtmlInputButton _buttonControl;

        public HtmlInputButton ButtonControl
        {
            get
            {
                if (_buttonControl == null)
                {
                    _buttonControl = new HtmlInputButton();
                }
                return _buttonControl;
            }
        }

        #region ** Data Binding

        #endregion

        protected override void InitContentSpan()
        {
            if (Template != null && IsDesignMode)
            {
                _spanContent = new HtmlGenericControl("span");
                Template.InstantiateIn(_spanContent);
            }
            
            ButtonControl.Value = this.Text;
            if (!string.IsNullOrEmpty(this.OnClientClick))
            {
                ButtonControl.Attributes.Add("onclick", this.OnClientClick);
            }
        }

        /// <summary>
        /// Method for rendering content of a complex item as button item.
        /// This method is used for rendering in design time.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="content"></param>
        protected override void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            HtmlGenericControl divContainer = new HtmlGenericControl("div");
            divContainer.Attributes.Add("class", "ui-btn ui-shadow ui-btn-corner-all ui-btn-up-c");

            HtmlGenericControl spanContainer = new HtmlGenericControl("span");
            spanContainer.Attributes.Add("class", "ui-btn-inner ui-btn-corner-all");

            HtmlGenericControl spanInner = new HtmlGenericControl("span");
            spanInner.Attributes.Add("class", "ui-btn-text");
            spanInner.InnerHtml = this.Text;

            spanContainer.Controls.Add(spanInner);

            HtmlGenericControl inputButton = new HtmlGenericControl("input");
            inputButton.Attributes.Add("class", "ui-btn-hidden");
            inputButton.Attributes.Add("type", "button");

            divContainer.Controls.Add(spanContainer);
            divContainer.Controls.Add(inputButton);

            container.Controls.Add(divContainer);
        }

        /// <summary>
        /// Method for rendering template and button control.
        /// </summary>
        /// <param name="writer"></param>
        protected override void RenderCustomSection(HtmlTextWriter writer)
        {
            ButtonControl.RenderControl(writer);

            if (Template != null)
            {
                TemplateContainer.RenderControl(writer);
            }
            else
            {
                if (_spanContent != null)
                {
                    this.Controls.Add(_spanContent);
                    _spanContent.RenderControl(writer);
                }
            }
        }
    }

    /// <summary>
    /// Input Items.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewInputItem : C1ListViewItem
    {
        private string _textValue = string.Empty;
        private int _sliderValue = 0;
        
        private Label _labelControl;
        private HtmlGenericControl _inputContainer;
        private HtmlTextArea _textArea;
        private HtmlInputText _inputControl;
        private Label _inputLabel;

        #region ** property for slider
        [C1Description("C1ListViewInputItem.Value")]
        [C1Category("Category.Appearance")]
        [DefaultValue(0)]
        [Layout(LayoutType.Appearance)]
        public virtual int Value
        {
            get
            {
                if (this.Type == InputType.slider && _inputControl != null)
                {
                    Int32.TryParse(_inputControl.Value, out _sliderValue);
                }

                return _sliderValue;
            }
            set
            {
                _sliderValue = value;
                if (_inputControl != null)
                {
                    if (Type == InputType.slider)
                    {
                        _inputControl.Value = value.ToString();
                    }
                }
            }
        }

        [C1Description("C1ListViewInputItem.Min")]
        [C1Category("Category.Appearance")]
        [DefaultValue(0)]
        [Layout(LayoutType.Appearance)]
        public virtual int Min
        {
            get
            {
                return _properties["Min"] == null ? 0
                    : (int)_properties["Min"];
            }
            set
            {
                _properties["Min"] = value;

                if (_inputControl != null)
                {
                    if (Type == InputType.slider)
                    {
                        this._inputControl.Attributes.Add("min", value.ToString());
                    }
                }
            }
        }

        [C1Description("C1ListViewInputItem.Max")]
        [C1Category("Category.Appearance")]
        [DefaultValue(0)]
        [Layout(LayoutType.Appearance)]
        public virtual int Max
        {
            get
            {
                return _properties["Max"] == null ? 0
                    : (int)_properties["Max"];
            }
            set
            {
                _properties["Max"] = value;

                if (_inputControl != null)
                {
                    if (Type == InputType.slider)
                    {
                        this._inputControl.Attributes.Add("max", value.ToString());
                    }
                }
            }
        }

        [C1Description("C1ListViewInputItem.Step")]
        [C1Category("Category.Appearance")]
        [DefaultValue(1)]
        [Layout(LayoutType.Appearance)]
        public virtual int Step
        {
            get
            {
                return _properties["Step"] == null ? 1
                    : (int)_properties["Step"];
            }
            set
            {
                _properties["Step"] = value;

                if (_inputControl != null)
                {
                    if (Type == InputType.slider)
                    {
                        this._inputControl.Attributes.Add("step", value.ToString());
                    }
                }
            }
        }

        [C1Description("C1ListViewInputItem.FillHighlight")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [Layout(LayoutType.Behavior)]
        public virtual bool FillHighlight
        {
            get
            {
                return _properties["FillHighlight"] == null ? false
                    : (bool)_properties["FillHighlight"];
            }
            set
            {
                _properties["FillHighlight"] = value;

                if (_inputControl != null)
                {
                    if (Type == InputType.slider)
                    {
                        this._inputControl.Attributes.Add("data-highlight", value ? "true" : "false");
                    }
                }
            }
        }
        #endregion

        [C1Description("C1ListViewInputItem.Disable")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [Layout(LayoutType.Behavior)]
        [RefreshProperties(System.ComponentModel.RefreshProperties.All)]
        public virtual bool Disable
        {
            get
            {
                return _properties["Disable"] == null ? false
                    : (bool)_properties["Disable"];
            }
            set
            {
                _properties["Disable"] = value;
                if (_inputControl != null)
                {
                    if (value)
                    {
                        _inputControl.Attributes.Add("disabled", "disabled");
                    }
                    else
                    {
                        _inputControl.Attributes.Remove("disabled");
                    }
                }
                if (_textArea != null)
                {
                    if (value)
                    {
                        _textArea.Attributes.Add("disabled", "disabled");
                    }
                    else
                    {
                        _textArea.Attributes.Remove("disabled");
                    }
                }
            }
        }

        [C1Description("C1ListViewInputItem.MinStyle")]
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(System.ComponentModel.RefreshProperties.All)]
        public virtual bool MiniStyle
        {
            get
            {
                return _properties["MiniStyle"] == null ? false
                    : (bool)_properties["MiniStyle"];
            }
            set
            {
                _properties["MiniStyle"] = value;
                if (_inputControl != null)
                {
                    _inputControl.Attributes.Add("data-mini", value ? "true" : "false");
                }
                if (_textArea != null)
                {
                    _textArea.Attributes.Add("data-mini", value ? "true" : "false");
                }
            }
        }

        [C1Description("C1ListViewInputItem.UseClearButton")]
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(System.ComponentModel.RefreshProperties.All)]
        public virtual bool UseClearButton
        {
            get
            {
                return _properties["UseClearButton"] == null ? false
                    : (bool)_properties["UseClearButton"];
            }
            set
            {
                _properties["UseClearButton"] = value;
                if (_inputControl != null)
                {
                    _inputControl.Attributes.Add("data-clear-btn", value ? "true" : "false");
                }
            }
        }

        [C1Description("C1ListViewInputItem.HideLabel")]
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(System.ComponentModel.RefreshProperties.All)]
        public virtual bool HideLabel
        {
            get
            {
                return _properties["HideLabel"] == null ? false
                    : (bool)_properties["HideLabel"];
            }
            set
            {
                _properties["HideLabel"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the text of item
        /// </summary>
        [C1Description("C1ListViewItem.Text")]
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public override string Text
        {
            get
            {
                if (Type == InputType.textarea)
                {
                    this._textValue = this.TextArea.Value;
                }
                else if (Type == InputType.label)
                {
                    this._textValue = this.InputLabel.Text;
                }
                else
                {
                    this._textValue = this.InputControl.Value;
                }
                return this._textValue;
            }
            set
            {
                if (this._textValue != value)
                {
                    this._textValue = value;

                    ResetTextValueForControls(this._textValue);
                }
            }
        }

        [C1Description("C1ListViewInputItem.LabelText")]
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string LabelText
        {
            get
            {
                return _properties["LabelText"] == null ? ""
                    : (string)_properties["LabelText"];
            }
            set
            {
                _properties["LabelText"] = value;
            }
        }

        [C1Description("C1ListViewInputItem.Placeholder")]
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public virtual string Placeholder
        {
            get
            {
                return _properties["Placeholder"] == null ? ""
                    : (string)_properties["Placeholder"];
            }
            set
            {
                _properties["Placeholder"] = value;
            }
        }

        [C1Description("C1ListViewInputItem.Type")]
        [C1Category("Category.Appearance")]
        [DefaultValue(InputType.text)]
        [Layout(LayoutType.Appearance)]
        public virtual InputType Type
        {
            get
            {
                return _properties["Type"] == null ? InputType.text
                    : (InputType)_properties["Type"];
            }
            set
            {
                _properties["Type"] = value;
            }
        }

        [C1Description("C1ListViewInputItem.ControlTheme")]
        [C1Category("Category.Style")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public virtual string ControlTheme
        {
            get
            {
                return _properties["ControlTheme"] == null ? ""
                    : (string)_properties["ControlTheme"];
            }
            set
            {
                _properties["ControlTheme"] = value;
            }
        }

        [C1Description("C1ListViewInputItem.TrackTheme")]
        [C1Category("Category.Style")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public virtual string TrackTheme
        {
            get
            {
                return _properties["TrackTheme"] == null ? ""
                    : (string)_properties["TrackTheme"];
            }
            set
            {
                _properties["TrackTheme"] = value;
            }
        }

        [Browsable(false)]
        protected Label LabelControl
        {
            get
            {
                if (_labelControl == null)
                {
                    _labelControl = new Label();
                    _labelControl.Attributes.Add("class", "select ui-input-text");
                    _labelControl.Text = LabelText;
                }

                return _labelControl;
            }
        }

        [Browsable(false)]
        protected HtmlTextArea TextArea
        {
            get
            {
                if (_textArea == null)
                {
                    _textArea = new HtmlTextArea();
                    _textArea.Value = this._textValue;
                    if (!string.IsNullOrEmpty(this.Placeholder))
                    {
                        _textArea.Attributes.Add("placeholder", this.Placeholder);
                    }

                    SetBasicStyleForInputControl(_textArea);
                }
                return _textArea;
            }
        }

        [Browsable(false)]
        protected HtmlInputText InputControl
        {
            get
            {
                if (_inputControl == null)
                {
                    _inputControl = new HtmlInputText();

                    if (Type == InputType.slider)
                    {
                        _inputControl = new HtmlInputText("range");
                        _inputControl.Value = this._sliderValue.ToString();
                        _inputControl.Attributes.Add("min", Min.ToString());
                        _inputControl.Attributes.Add("max", Max.ToString());
                        if (Step > 1)
                        {
                            _inputControl.Attributes.Add("step", Step.ToString());
                        }
                        if (FillHighlight)
                        {
                            _inputControl.Attributes.Add("data-highlight", "true");
                        }
                        if (!string.IsNullOrEmpty(this.TrackTheme))
                        {
                            _inputControl.Attributes.Add("data-track-theme", this.TrackTheme);
                        }
                    }
                    else
                    {
                        if (Type == InputType.text)
                        {
                            _inputControl = new HtmlInputText("text");
                            _inputControl.Value = this._textValue.ToString();
                        }
                        else if (Type == InputType.search)
                        {
                            _inputControl = new HtmlInputText("search");
                            _inputControl.Value = this._textValue.ToString();
                        }

                        if (UseClearButton)
                        {
                            _inputControl.Attributes.Add("data-clear-btn", "true");
                        }
                        if (!string.IsNullOrEmpty(this.Placeholder))
                        {
                            _inputControl.Attributes.Add("placeholder", this.Placeholder);
                        }
                    }

                    SetBasicStyleForInputControl(_inputControl);
                }
                return _inputControl;
            }
        }

        [Browsable(false)]
        protected Label InputLabel
        {
            get
            {
                if (_inputLabel == null)
                {
                    _inputLabel = new Label();
                    _inputLabel.Text = this._textValue;
                }
                return _inputLabel;
            }
        }

        protected HtmlGenericControl InputContaier
        {
            get
            {
                return _inputContainer;
            }
            set
            {
                _inputContainer = value;
            }
        }

        #region ** Data Binding

        protected override void SetValueFromDataBindingField(C1ListViewItemBinding binding, object data, PropertyDescriptorCollection pdc, bool bound)
        {
            base.SetValueFromDataBindingField(binding, data, pdc, bound);

            object fieldValue = null;
            //Type
            if (!string.IsNullOrEmpty(binding.TypeField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.TypeField);
                if (fieldValue != null)
                {
                    string result = (string)fieldValue;
                    switch (result)
                    {
                        case "search":
                            this.Type = InputType.search;
                            break;
                        case "slider":
                            this.Type = InputType.slider;
                            break;
                        case "textarea":
                            this.Type = InputType.textarea;
                            break;
                        case "label":
                            this.Type = InputType.label;
                            break;
                        default:
                            this.Type = InputType.text;
                            break;
                    }
                    bound = true;
                }
            }

            //LabelText
            if (!string.IsNullOrEmpty(binding.LabelTextField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.LabelTextField);
                if (fieldValue != null)
                {
                    this.LabelText = (string)fieldValue;
                    bound = true;
                }
            }

            if (this.Type != InputType.slider)
            {
                //TextboxValue
                if (!string.IsNullOrEmpty(binding.TextField))
                {
                    fieldValue = GetFieldValue(data, pdc, binding.TextField);
                    if (fieldValue != null)
                    {
                        this.Text = (string)fieldValue;
                        bound = true;
                    }
                }
            }
            else
            {
                // Min
                if (!string.IsNullOrEmpty(binding.MinField))
                {
                    fieldValue = GetFieldValue(data, pdc, binding.MinField);
                    if (fieldValue != null)
                    {
                        if (Type == InputType.slider)
                        {
                            int result = 0;
                            Int32.TryParse((string)fieldValue, out result);
                            this.Min = result;
                        }
                        bound = true;
                    }
                }
                // Max
                if (!string.IsNullOrEmpty(binding.MaxField))
                {
                    fieldValue = GetFieldValue(data, pdc, binding.MaxField);
                    if (fieldValue != null)
                    {
                        if (Type == InputType.slider)
                        {
                            int result = 0;
                            Int32.TryParse((string)fieldValue, out result);
                            this.Max = result;
                        }
                        bound = true;
                    }
                }
                // Value
                if (!string.IsNullOrEmpty(binding.ValueField))
                {
                    fieldValue = GetFieldValue(data, pdc, binding.ValueField);
                    if (fieldValue != null)
                    {
                        if (Type == InputType.slider)
                        {
                            int result = 0;
                            Int32.TryParse((string)fieldValue, out result);
                            this.Value = result;
                        }
                        bound = true;
                    }
                }
            }
        }

        #endregion

        /// <summary>
        ///  Initialize LabelContorl and TextArea/InputControl in ListView. 
        ///  As a result, they can be accessed in Page_Load, Page_PreRenderComplete.
        /// </summary>
        /// <param name="parent"></param>
        internal override void CreateChildControlsInListView(C1ListView root)
        {
            HtmlGenericControl spanContainer = new HtmlGenericControl("span");
            this.InputContaier = spanContainer;
            if (Type == InputType.textarea)
            {
                this.TextArea.ID = ItemID + "InputTA";
                this.LabelControl.AssociatedControlID = this.TextArea.ID;
                spanContainer.Controls.Add(this.LabelControl);
                spanContainer.Controls.Add(this.TextArea);
            }
            else if (Type == InputType.label)
            {
                this.InputLabel.ID = ItemID + "InputLA";
                this.LabelControl.AssociatedControlID = this.InputLabel.ID;
                spanContainer.Controls.Add(this.LabelControl);
                spanContainer.Controls.Add(this.InputLabel);
            }
            else
            {
                this.InputControl.ID = ItemID + "InputIC";
                this.LabelControl.AssociatedControlID = this.InputControl.ID;
                spanContainer.Controls.Add(this.LabelControl);
                spanContainer.Controls.Add(this.InputControl);
            }
            root.Controls.Add(spanContainer);
            base.CreateChildControlsInListView(root);
        }

        protected override void InitContentSpan()
        {
            if (Template != null && IsDesignMode)
            {
                _spanContent = new HtmlGenericControl("span");
                Template.InstantiateIn(_spanContent);
            }
        }

        protected override string RenderBeginTagCssInDesign()
        {
            string inputCss = base.RenderBeginTagCssInDesign();
            inputCss += " ui-field-contain";

            return inputCss;
        }

        /// <summary>
        /// Method for rendering content of a complex item as input item.
        /// This method is used for rendering in design time.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="content"></param>
        protected override void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            HtmlGenericControl label = new HtmlGenericControl("label");
            label.InnerHtml = this.LabelText;

            HtmlGenericControl controlContainer = new HtmlGenericControl("div");

            string tempCss = string.Empty;
            string controlTheme = string.IsNullOrEmpty(this.ControlTheme) ? "c" : this.ControlTheme;
            string trackTheme = string.IsNullOrEmpty(this.TrackTheme) ? "c" : this.TrackTheme;

            #region Style of jqm-1.3.1.css

            switch (Type)
            {
                case InputType.search:
                    label.Attributes.Add("class", "select ui-input-text");
                    HtmlGenericControl searchClearButton = null;
                    tempCss = "ui-input-search ui-shadow-inset ui-btn-corner-all ui-btn-shadow ui-icon-searchfield ui-body-" + controlTheme;
                    if (this.MiniStyle)
                    {
                        tempCss += " ui-mini";
                    }

                    HtmlGenericControl searchInput = new HtmlGenericControl("input");
                    searchInput.Attributes.Add("class", "ui-input-text ui-body-" + controlTheme);
                    if (!string.IsNullOrEmpty(this._textValue))
                    {
                        searchInput.Attributes.Add("value", this._textValue);

                        searchClearButton = CreateClearButtonForDesignTime(controlTheme);
                    }

                    controlContainer.Attributes.Add("class", tempCss);
                    controlContainer.Controls.Add(searchInput);
                    if (searchClearButton != null)
                    {
                        controlContainer.Controls.Add(searchClearButton);
                    }
                    break;
                case InputType.slider:
                    label.Attributes.Add("class", "ui-input-text ui-slider");
                    tempCss = "ui-slider";
                    if (this.MiniStyle)
                    {
                        tempCss += " ui-mini";
                    }
                    controlContainer.Attributes.Add("class", tempCss);

                    HtmlGenericControl inputNumber = new HtmlGenericControl("input");
                    inputNumber.Attributes.Add("class", "ui-input-text ui-body-" + controlTheme + " ui-corner-all ui-shadow-inset ui-slider-input");
                    inputNumber.Attributes.Add("type", "number");
                    inputNumber.Attributes.Add("value", this.Value.ToString());

                    HtmlGenericControl sliderContainer = CreateSliderForDesignTime(controlTheme, trackTheme);

                    controlContainer.Controls.Add(inputNumber);
                    controlContainer.Controls.Add(sliderContainer);
                    break;
                case InputType.textarea:
                    HtmlGenericControl textArea = new HtmlGenericControl("textarea");
                    textArea.Attributes.Add("class", "ui-input-text ui-body-c ui-corner-all ui-shadow-inset");
                    if (!string.IsNullOrEmpty(this._textValue))
                    {
                        textArea.InnerHtml = this._textValue;
                    }
                    if (!string.IsNullOrEmpty(this.Placeholder))
                    {
                        textArea.Attributes.Add("placeholder", this.Placeholder);
                    }
                    controlContainer = textArea;
                    break;
                case InputType.label:
                    label.Attributes.Add("class", "select ui-input-text");
                    HtmlGenericControl labelSpan = new HtmlGenericControl("span");
                    labelSpan.InnerHtml = this._textValue;
                    controlContainer = labelSpan;
                    break;
                default:
                    label.Attributes.Add("class", "ui-input-text");
                    HtmlGenericControl textClearButton = null;
                    tempCss = "ui-input-text ui-shadow-inset ui-corner-all ui-btn-shadow ui-body-" + controlTheme;
                    if (this.MiniStyle)
                    {
                        tempCss += " ui-mini";
                    }

                    HtmlGenericControl inputText = new HtmlGenericControl("input");
                    inputText.Attributes.Add("class", "ui-input-text ui-body-c");
                    inputText.Attributes.Add("type", "text");
                    if (!string.IsNullOrEmpty(this._textValue))
                    {
                        inputText.Attributes.Add("value", this._textValue);

                        if (this.UseClearButton)
                        {
                            textClearButton = CreateClearButtonForDesignTime(controlTheme);
                        }
                    }

                    if (this.UseClearButton)
                    {
                        tempCss += " ui-input-has-clear";
                    }

                    if (!string.IsNullOrEmpty(this.Placeholder))
                    {
                        inputText.Attributes.Add("placeholder", this.Placeholder);
                    }

                    controlContainer.Attributes.Add("class", tempCss);
                    controlContainer.Controls.Add(inputText);
                    if (textClearButton != null)
                    {
                        controlContainer.Controls.Add(textClearButton);
                    }
                    break;
            }

            container.Controls.Add(label);
            container.Controls.Add(controlContainer);

            #endregion
        }

        /// <summary>
        /// Helper function for adding data property when rendering begin tag in run time
        /// </summary>
        protected override void RenderBeginTagDataInRunTime()
        {
            base.RenderBeginTagDataInRunTime();

            this.Attributes.Add("data-role", "fieldcontain");
            if (this.HideLabel)
            {
                this.Attributes.Add("class", "ui-hide-label");
            }
        }

        /// <summary>
        /// Method for rendering template and LabelControl, TextBoxControl.
        /// </summary>
        /// <param name="writer"></param>
        protected override void RenderCustomSection(HtmlTextWriter writer)
        {
            foreach (Control control in InputContaier.Controls)
            {
                control.RenderControl(writer);
            }

            if (Template != null)
            {
                TemplateContainer.RenderControl(writer);
            }
            else
            {
                if (_spanContent != null)
                {
                    this.Controls.Add(_spanContent);
                    _spanContent.RenderControl(writer);
                }
            }
        }

        /// <summary>
        /// Set some normal properties into InputControl, such as "Disable", "MiniStyle", "ControlTheme".
        /// </summary>
        /// <param name="targetControl"></param>
        protected virtual void SetBasicStyleForInputControl(HtmlControl targetControl)
        {
            if (Disable)
            {
                targetControl.Attributes.Add("disabled", "disabled");
            }
            else
            {
                targetControl.Attributes.Remove("disabled");
            }
            if (MiniStyle)
            {
                targetControl.Attributes.Add("data-mini", "true");
            }

            if (!string.IsNullOrEmpty(this.ControlTheme))
            {
                targetControl.Attributes.Add("data-theme", this.ControlTheme);
            }
        }

        internal override string GetStringForAutoDivider()
        {
            string original = string.Empty;
            if (!string.IsNullOrEmpty(Supplement))
            {
                return Supplement.ToCharArray()[0].ToString().ToUpper();
            }
            else if (!string.IsNullOrEmpty(LabelText))
            {

                return LabelText.ToCharArray()[0].ToString().ToUpper();
            }
            return "";
        }

        #region private methods

        private HtmlGenericControl CreateSliderForDesignTime(string controlTheme, string trackTheme)
        {
            HtmlGenericControl sliderContainer = new HtmlGenericControl("div");
            string sliderConCss = "ui-slider-track ui-btn-down-" + trackTheme + " ui-btn-corner-all";
            sliderContainer.Attributes.Add("class", sliderConCss);

            float location = 0;
            if (this.Max > this.Min)
            {
                location = ((this._sliderValue - this.Min) * 100) / (this.Max - this.Min);
            }

            HtmlGenericControl sliderA = new HtmlGenericControl("a");
            sliderA.Attributes.Add("class", "ui-slider-handle ui-btn ui-shadow ui-btn-corner-all ui-btn-up-" + controlTheme);
            sliderA.Attributes.Add("style", "left: " + location.ToString() + "%");

            HtmlGenericControl innerASpan = new HtmlGenericControl("span");
            innerASpan.Attributes.Add("class", "ui-btn-inner");

            HtmlGenericControl innerASpanSpan = new HtmlGenericControl("span");
            innerASpanSpan.Attributes.Add("class", "ui-btn-text");

            innerASpan.Controls.Add(innerASpanSpan);
            sliderA.Controls.Add(innerASpan);

            if (this.FillHighlight)
            {
                HtmlGenericControl highlightDiv = new HtmlGenericControl("div");
                highlightDiv.Attributes.Add("class", "ui-slider-bg ui-btn-active ui-btn-corner-all");
                highlightDiv.Attributes.Add("style", "width: " + location.ToString() + "%");

                sliderContainer.Controls.Add(highlightDiv);
            }

            sliderContainer.Controls.Add(sliderA);

            return sliderContainer;
        }

        private HtmlGenericControl CreateClearButtonForDesignTime(string controlTheme)
        {
            HtmlGenericControl clearButton = new HtmlGenericControl("a");

            string cBCSS = "ui-input-clear ui-btn ui-btn-up-" + controlTheme;
            cBCSS += this.MiniStyle ?
                " ui-shadow ui-btn-corner-all ui-mini ui-btn-icon-notext" :
                " ui-shadow ui-btn-corner-all ui-fullsize ui-btn-icon-notext";

            clearButton.Attributes.Add("class", cBCSS);
            clearButton.Attributes.Add("data-icon", "delete");

            HtmlGenericControl innerCB_Span = new HtmlGenericControl("span");
            innerCB_Span.Attributes.Add("class", "ui-btn-inner");

            HtmlGenericControl innerCBS_Text = new HtmlGenericControl("span");
            innerCBS_Text.Attributes.Add("class", "ui-btn-text");
            innerCBS_Text.InnerHtml = "clear text";

            HtmlGenericControl innerCBS_Icon = new HtmlGenericControl("span");
            innerCBS_Icon.Attributes.Add("class", "ui-icon ui-icon-delete ui-icon-shadow");

            innerCB_Span.Controls.Add(innerCBS_Text);
            innerCB_Span.Controls.Add(innerCBS_Icon);

            clearButton.Controls.Add(innerCB_Span);

            return clearButton;
        }

        private void ResetTextValueForControls(string textValue)
        {
            if (_inputControl != null)
            {
                if (Type == InputType.text)
                {
                    _inputControl.Value = textValue;
                }
                else if (Type == InputType.search)
                {
                    _inputControl.Value = textValue;
                }
            }

            if (_inputLabel != null)
            {
                _inputLabel.Text = textValue;
            }

            if (_textArea != null)
            {
                _textArea.Value = textValue;
            }
        }

        #endregion
    }

    /// <summary>
    /// Flip Switch Items.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewFlipSwitchItem : C1ListViewInputItem
    {
        private HtmlSelect _listBoxControl;
        private ListItem _itemON;
        private ListItem _itemOFF;

        #region ** Hide some properties

        [Browsable(false)]
        public override int Value
        {
            get
            {
                return base.Value;
            }
        }
        [Browsable(false)]
        public override int Max
        {
            get
            {
                return base.Max;
            }
        }
        [Browsable(false)]
        public override int Min
        {
            get
            {
                return base.Min;
            }
        }
        [Browsable(false)]
        public override int Step
        {
            get
            {
                return base.Step;
            }
        }
        [Browsable(false)]
        public override bool FillHighlight
        {
            get
            {
                return base.FillHighlight;
            }
        }

        [Browsable(false)]
        public override InputType Type
        {
            get
            {
                return base.Type;
            }
        }

        #endregion

        [C1Description("C1ListViewInputItem.Disable")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        public override bool Disable
        {
            get
            {
                return base.Disable;
            }
            set
            {
                _properties["Disable"] = value;
                if (_listBoxControl != null)
                {
                    if (value)
                    {
                        _listBoxControl.Attributes.Add("disabled", "disabled");
                    }
                    else
                    {
                        _listBoxControl.Attributes.Remove("disabled");
                    }
                }
            }
        }

        public override bool MiniStyle
        {
            get
            {
                return base.MiniStyle;
            }
            set
            {
                _properties["MiniStyle"] = value;
                if (_listBoxControl != null)
                {
                    _listBoxControl.Attributes.Add("data-mini", value ? "true" : "false");
                }
            }
        }

        [C1Description("C1ListViewFlipSwitchItem.OnMessage")]
        [DefaultValue("On")]
        [Layout(LayoutType.Appearance)]
        public string ONMessage
        {
            get
            {
                return _properties["ONMessage"] == null ? "On"
                    : (string)_properties["ONMessage"];
            }
            set
            {
                _properties["ONMessage"] = value;
                ItemON.Text = value;
            }
        }

        [C1Description("C1ListViewFlipSwitchItem.ONValue")]
        [DefaultValue("On")]
        [Layout(LayoutType.Appearance)]
        public string ONValue
        {
            get
            {
                return _properties["ONValue"] == null ? "On"
                    : (string)_properties["ONValue"];
            }
            set
            {
                _properties["ONValue"] = value;
                ItemON.Value = value;
            }
        }

        [C1Description("C1ListViewFlipSwitchItem.OFFMessage")]
        [DefaultValue("Off")]
        [Layout(LayoutType.Appearance)]
        public string OFFMessage
        {
            get
            {
                return _properties["OFFMessage"] == null ? "Off"
                    : (string)_properties["OFFMessage"];
            }
            set
            {
                _properties["OFFMessage"] = value;
                ItemOFF.Text = value;
            }
        }

        [C1Description("C1ListViewFlipSwitchItem.OFFValue")]
        [DefaultValue("Off")]
        [Layout(LayoutType.Appearance)]
        public string OFFValue
        {
            get
            {
                return _properties["OFFValue"] == null ? "Off"
                    : (string)_properties["OFFValue"];
            }
            set
            {
                _properties["OFFValue"] = value;
                ItemOFF.Value = value;
            }
        }

        [C1Description("C1ListViewFlipSwitchItem.SelectedON")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        public bool SelectedON
        {
            get
            {
                if (ItemON.Selected)
                    return true;
                return false;
            }
            set
            {
                if (value)
                {
                    ItemON.Selected = true;
                    ItemOFF.Selected = false;
                }
                else
                {
                    ItemON.Selected = false;
                    ItemOFF.Selected = true;
                }
            }
        }

        private ListItem ItemON
        {
            get
            {
                if (_itemON == null)
                {
                    _itemON = new ListItem(ONMessage, ONValue);
                    _itemON.Selected = false;
                }
                return _itemON;
            }
        }

        private ListItem ItemOFF
        {
            get
            {
                if (_itemOFF == null)
                {
                    _itemOFF = new ListItem(OFFMessage, OFFValue);
                    _itemOFF.Selected = true;
                }
                return _itemOFF;
            }
        }

        private HtmlSelect ListBoxControl
        {
            get
            {
                if (_listBoxControl == null)
                {
                    _listBoxControl = new HtmlSelect();
                    _listBoxControl.Items.Add(ItemOFF);
                    _listBoxControl.Items.Add(ItemON);

                    _listBoxControl.Attributes.Add("data-role", "slider");

                    SetBasicStyleForInputControl(_listBoxControl);

                    if (!string.IsNullOrEmpty(this.TrackTheme))
                    {
                        _listBoxControl.Attributes.Add("data-track-theme", this.TrackTheme);
                    }
                }
                return _listBoxControl;
            }
        }

        #region ** DataBinding

        protected override void SetValueFromDataBindingField(C1ListViewItemBinding binding, object data, PropertyDescriptorCollection pdc, bool bound)
        {
            base.SetValueFromDataBindingField(binding, data, pdc, bound);

            object fieldValue = null;
            // ONMessage
            if (!string.IsNullOrEmpty(binding.OnMessageField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.OnMessageField);
                if (fieldValue != null)
                {
                    this.ONMessage = (string)fieldValue;
                    bound = true;
                }
            }

            // ONValue
            if (!string.IsNullOrEmpty(binding.OnValueField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.OnValueField);
                if (fieldValue != null)
                {
                    this.ONValue = (string)fieldValue;
                    bound = true;
                }
            }

            // OFFMessage
            if (!string.IsNullOrEmpty(binding.OffMessageField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.OffMessageField);
                if (fieldValue != null)
                {
                    this.OFFMessage = (string)fieldValue;
                    bound = true;
                }
            }

            // OFFValue
            if (!string.IsNullOrEmpty(binding.OffValueField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.OffValueField);
                if (fieldValue != null)
                {
                    this.OFFValue = (string)fieldValue;
                    bound = true;
                }
            }

            // SelectValue
            if (!string.IsNullOrEmpty(binding.SelectedONField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.SelectedONField);
                if (fieldValue != null)
                {
                    this.SelectedON = ((string)fieldValue == "true");
                    bound = true;
                }
            }
        }

        #endregion

        internal override void CreateChildControlsInListView(C1ListView root)
        {
            RenderTemplateIntoContainer(root);
            
            HtmlGenericControl spanContainer = new HtmlGenericControl("span");
            this.InputContaier = spanContainer;

            this.ListBoxControl.ID = ItemID;
            this.LabelControl.AssociatedControlID = this.ListBoxControl.ID;

            spanContainer.Controls.Add(this.LabelControl);
            spanContainer.Controls.Add(this.ListBoxControl);
            root.Controls.Add(spanContainer);
        }

        protected override void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            if (!string.IsNullOrEmpty(this.LabelText))
            {
                HtmlGenericControl label = new HtmlGenericControl("label");
                label.Attributes.Add("class", "ui-slider");//ui-input-text
                label.InnerHtml = this.LabelText;
                container.Controls.Add(label);
            }
            HtmlGenericControl sliderContainer = new HtmlGenericControl("div");
            sliderContainer.Attributes.Add("class", "ui-slider ui-slider-switch ui-btn-down-c ui-btn-corner-all");

            HtmlGenericControl onSpan = new HtmlGenericControl("span");
            onSpan.Attributes.Add("class", "ui-slider-label ui-slider-label-b ui-btn-down-c ui-btn-corner-all");
            onSpan.Attributes.Add("style", "width: 0%;");
            onSpan.InnerHtml = "On";

            HtmlGenericControl offSpan = new HtmlGenericControl("span");
            offSpan.Attributes.Add("class", "ui-slider-label ui-slider-label-b ui-btn-down-c ui-btn-corner-all");
            offSpan.Attributes.Add("style", "width: 100%;");
            offSpan.InnerHtml = "Off";

            HtmlGenericControl innerContainer = new HtmlGenericControl("div");
            innerContainer.Attributes.Add("class", "ui-slider-inneroffset");

            HtmlGenericControl innerA = new HtmlGenericControl("a");
            innerA.Attributes.Add("class", "ui-slider-handle ui-slider-handle-snapping ui-btn ui-shadow ui-btn-corner-all ui-btn-up-c");

            HtmlGenericControl innerSpanIcon = new HtmlGenericControl("span");
            innerSpanIcon.Attributes.Add("class", "ui-btn-inner");

            innerA.Controls.Add(innerSpanIcon);
            innerContainer.Controls.Add(innerA);

            sliderContainer.Controls.Add(onSpan);
            sliderContainer.Controls.Add(offSpan);
            sliderContainer.Controls.Add(innerContainer);

            container.Controls.Add(sliderContainer);
        }
    }

    /// <summary>
    /// Control Group Items.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewControlGroupItem : C1ListViewInputItem, INamingContainer
    {
        private ListItem _leaderItem = null;
        
        private ListBox _listBoxControl;
        private HtmlSelect _listBoxForRender;
        private HtmlGenericControl _fieldsetContainer;
        
        #region ** Hide some properties

        [Browsable(false)]
        public override int Value
        {
            get
            {
                return base.Value;
            }
        }
        [Browsable(false)]
        public override int Max
        {
            get
            {
                return base.Max;
            }
        }
        [Browsable(false)]
        public override int Min
        {
            get
            {
                return base.Min;
            }
        }
        [Browsable(false)]
        public override int Step
        {
            get
            {
                return base.Step;
            }
        }
        [Browsable(false)]
        public override bool FillHighlight
        {
            get
            {
                return base.FillHighlight;
            }
        }

        [Browsable(false)]
        public override InputType Type
        {
            get
            {
                return base.Type;
            }
        }

        [Browsable(false)]
        public override string TrackTheme
        {
            get
            {
                return base.TrackTheme;
            }
        }

        #endregion

        public override bool Disable
        {
            get
            {
                return base.Disable;
            }
            set
            {
                _properties["Disable"] = value;
                if (this._listBoxForRender != null)
                {
                    if (value)
                    {
                        this._listBoxForRender.Attributes.Add("disabled", "disabled");
                    }
                    else
                    {
                        this._listBoxForRender.Attributes.Remove("disabled");
                    }
                }
                if (this._fieldsetContainer != null)
                {
                    if (value)
                    {
                        this._fieldsetContainer.Attributes.Add("disabled", "disabled");
                    }
                    else
                    {
                        this._fieldsetContainer.Attributes.Remove("disabled");
                    }
                }
            }
        }

        public override bool MiniStyle
        {
            get
            {
                return base.MiniStyle;
            }
            set
            {
                _properties["MiniStyle"] = value;
                if (this._listBoxForRender != null)
                {
                    this._listBoxForRender.Attributes.Add("data-mini", value ? "true" : "false");
                }
                if (this._fieldsetContainer != null)
                {
                    this._fieldsetContainer.Attributes.Add("data-mini", value ? "true" : "false");
                }
            }
        }

        [C1Description("C1ListViewControlGroupItem.Horizontal")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        public bool Horizontal
        {
            get
            {
                return _properties["Horizontal"] == null ? false
                    : (bool)_properties["Horizontal"];
            }
            set
            {
                _properties["Horizontal"] = value;

                if (_fieldsetContainer != null)
                {
                    if (value)
                    {
                        _fieldsetContainer.Attributes.Add("data-type", "horizontal");
                    }
                    else
                    {
                        _fieldsetContainer.Attributes.Add("data-type", "vertical");
                    }
                }
            }
        }

        [C1Description("C1ListViewControlGroupItem.Type")]
        [DefaultValue(ControlGroupType.normal)]
        [Layout(LayoutType.Appearance)]
        public ControlGroupType ControlGroupType
        {
            get
            {
                return _properties["ControlGroupType"] == null ? ControlGroupType.normal
                    : (ControlGroupType)_properties["ControlGroupType"];
            }
            set
            {
                _properties["ControlGroupType"] = value;
            }
        }

        [C1Description("C1ListViewControlGroupItem.NativeMenu")]
        [DefaultValue(true)]
        [Layout(LayoutType.Appearance)]
        public bool NativeMenu
        {
            get
            {
                return _properties["NativeMenu"] == null ? true
                    : (bool)_properties["NativeMenu"];
            }
            set
            {
                _properties["NativeMenu"] = value;

                if (_listBoxForRender != null)
                {
                    SettingNativeMenu(value);
                    if (value)
                    {
                        _listBoxForRender.Attributes.Add("data-native-menu", "true");
                        if (!string.IsNullOrEmpty(this.Placeholder) && _leaderItem != null)
                        {
                            _listBoxForRender.Items.Remove(_leaderItem);
                        }
                    }
                }
            }
        }

        [C1Description("C1ListViewControlGroupItem.Multiple")]
        [DefaultValue(false)]
        [Layout(LayoutType.Appearance)]
        public bool Multiple
        {
            get
            {
                return _properties["Mutiple"] == null ? false
                    : (bool)_properties["Mutiple"];
            }
            set
            {
                _properties["Mutiple"] = value;

                if (_listBoxForRender != null)
                {
                    if (value && !NativeMenu)
                    {
                        _listBoxForRender.Attributes.Add("multiple", "multiple");
                    }
                    else
                    {
                        _listBoxForRender.Attributes.Remove("multiple");
                    }
                }
            }
        }

        [C1Description("C1ListViewControlGroupItem.Placeholder")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public override string Placeholder
        {
            get
            {
                if (!NativeMenu)
                {
                    return _properties["Placeholder"] == null ?
                        "" : (string)_properties["Placeholder"];
                }
                return null;
            }
            set
            {
                _properties["Placeholder"] = value;
            }
        }

        [C1Description("C1ListViewControlGroupItem.OverlayTheme")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        public string OverlayTheme
        {
            get
            {
                return _properties["OverlayTheme"] == null ? ""
                    : (string)_properties["OverlayTheme"];
            }
            set
            {
                _properties["OverlayTheme"] = value;
            }
        }

        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [Layout(LayoutType.Behavior)]
        public ListBox InnerListControls
        {
            get
            {
                if (_listBoxControl == null)
                {
                    _listBoxControl = new ListBox();
                }

                return _listBoxControl;
            }
        }

        private HtmlSelect ListBoxForRender
        {
            get
            {
                if (_listBoxForRender == null)
                {
                    _listBoxForRender = new HtmlSelect();
                    if (!string.IsNullOrEmpty(this.DataIcon))
                    {
                        this._listBoxForRender.Attributes.Add("data-icon", this.DataIcon);
                    }

                    SetBasicStyleForInputControl(_listBoxForRender);

                    if (Multiple && !NativeMenu)
                    {
                        this._listBoxForRender.Attributes.Add("multiple", "multiple");
                    }
                    else
                    {
                        this._listBoxForRender.Attributes.Remove("multiple");
                    }

                    if (!string.IsNullOrEmpty(this.OverlayTheme) && !NativeMenu)
                    {
                        this._listBoxForRender.Attributes.Add("data-overlay-theme", this.OverlayTheme);
                    }
                }
                _listBoxForRender.Items.Clear();

                SettingNativeMenu(NativeMenu);

                foreach (ListItem item in this.InnerListControls.Items)
                {
                    ListItem option = new ListItem();
                    option.Text = item.Text;
                    option.Value = item.Value;
                    option.Selected = item.Selected;
                    if (!item.Enabled)
                    {
                        option.Attributes.Add("disabled", "disabled");
                    }
                    _listBoxForRender.Items.Add(option);
                }

                return _listBoxForRender;
            }
        }

        private HtmlGenericControl FieldsetContainer
        {
            get
            {
                if (ControlGroupType != ControlGroupType.normal)
                {
                    if (_fieldsetContainer == null)
                    {
                        _fieldsetContainer = new HtmlGenericControl("fieldset");

                        SetBasicStyleForInputControl(_fieldsetContainer);

                        _fieldsetContainer.Attributes.Add("data-role", "controlgroup");
                        if (Horizontal)
                        {
                            _fieldsetContainer.Attributes.Add("data-type", "horizontal");
                        }

                        HtmlGenericControl legend = new HtmlGenericControl("legend");
                        legend.InnerHtml = this.LabelText;
                        _fieldsetContainer.Controls.Add(legend);

                        switch (ControlGroupType)
                        {
                            case ControlGroupType.radiolist:
                                for (int i = 0; i < InnerListControls.Items.Count; i++)
                                {
                                    ListItem item = InnerListControls.Items[i];
                                    HtmlInputRadioButton radioInput = new HtmlInputRadioButton();
                                    radioInput.ID = ItemID + i.ToString();
                                    radioInput.Name = ItemID + "0";
                                    radioInput.Attributes.Add("value", item.Value);
                                    if (item.Selected)
                                    {
                                        radioInput.Attributes.Add("checked", "checked");
                                    }
                                    
                                    Label label = new Label();
                                    label.AssociatedControlID = radioInput.ID;
                                    label.Text = item.Text;
                                    if (this.Disable)
                                    {
                                        // Remove select behavior.
                                        radioInput.Attributes.Add("disabled", "disabled");
                                        // Remove style for input.
                                        radioInput.Attributes.Add("class", "ui-disabled");
                                        // Remove mouse click style for IE.
                                        label.Attributes.Add("class", "ui-disabled");
                                    }

                                    _fieldsetContainer.Controls.Add(radioInput);
                                    _fieldsetContainer.Controls.Add(label);
                                }
                                break;
                            case ControlGroupType.checkboxlist:
                                for (int i = 0; i < InnerListControls.Items.Count; i++)
                                {
                                    ListItem item = InnerListControls.Items[i];
                                    HtmlInputCheckBox checkboxInput = new HtmlInputCheckBox();
                                    checkboxInput.ID = ItemID + i.ToString();
                                    checkboxInput.Attributes.Add("value", InnerListControls.Items[i].Value);
                                    if (InnerListControls.Items[i].Selected)
                                    {
                                        checkboxInput.Attributes.Add("checked", "checked");
                                    }

                                    Label label = new Label();
                                    label.AssociatedControlID = checkboxInput.ID;
                                    label.Text = item.Text;

                                    if (this.Disable)
                                    {
                                        checkboxInput.Attributes.Add("disabled", "disabled");
                                        checkboxInput.Attributes.Add("class", "ui-disabled");
                                        label.Attributes.Add("class", "ui-disabled");
                                    }

                                    _fieldsetContainer.Controls.Add(checkboxInput);
                                    _fieldsetContainer.Controls.Add(label);
                                }
                                break;
                        }
                    }
                }
                return _fieldsetContainer;
            }
        }

        #region ** DataBinding

        protected override void SetValueFromDataBindingField(C1ListViewItemBinding binding, object data, PropertyDescriptorCollection pdc, bool bound)
        {
            base.SetValueFromDataBindingField(binding, data, pdc, bound);

            object fieldValue = null;
            // Horizontal
            if (!string.IsNullOrEmpty(binding.HorizontalField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.HorizontalField);
                if (fieldValue != null)
                {
                    this.Horizontal = ((string)fieldValue == "true");
                    bound = true;
                }
            }

            // ControlGroupType
            if (!string.IsNullOrEmpty(binding.ControlGroupTypeField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.ControlGroupTypeField);
                if (fieldValue != null)
                {
                    string result = (string)fieldValue;
                    switch (result)
                    {
                        case "checkboxlist":
                            this.ControlGroupType = Wijmo.Controls.C1ListView.ControlGroupType.checkboxlist;
                            break;
                        case "radiolist":
                            this.ControlGroupType = Wijmo.Controls.C1ListView.ControlGroupType.radiolist;
                            break;
                        default:
                            this.ControlGroupType = Wijmo.Controls.C1ListView.ControlGroupType.normal;
                            break;
                    }

                }
            }

            // NativeMenu
            if (!string.IsNullOrEmpty(binding.NativeMenuField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.NativeMenuField);
                if (fieldValue != null)
                {
                    this.NativeMenu = ((string)fieldValue == "true");
                    bound = true;
                }
            }

            // LeaderMessage
            if (!string.IsNullOrEmpty(binding.PlaceholderField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.PlaceholderField);
                if (fieldValue != null)
                {
                    this.Placeholder = (string)fieldValue;
                    bound = true;
                }
            }
        }

        internal override void DataBindInternal(IHierarchicalEnumerable enumerable, int depth, C1ListViewItemBinding parBinding)
        {
            base.DataBindInternal(enumerable, depth, parBinding);

            foreach (object o in enumerable)
            {
                IHierarchyData data = enumerable.GetHierarchyData(o);
                string dataType = data.Type;

                C1ListViewItemBinding binding = this.ListView.GetBinding(dataType, depth);
                if (binding == null)
                {
                    if (dataType == "InnerListItem")
                    {
                        binding = new C1ListViewItemBinding();
                        binding.InnerTextField = "Text";
                        binding.InnerValueField = "Value";
                        binding.InnerSelectedField = "Selected";
                    }
                }

                if (binding != null)
                {
                    if (binding.ItemType == DataBindingItemType.controlgroupinnerlistitem || dataType == "InnerListItem")
                    {
                        ListItem item = CreateListItemFormDataBinding(binding, data);
                        if (item != null)
                        {
                            this.InnerListControls.Items.Add(item);
                        }
                    }
                }

            }
        }

        #endregion

        internal override void CreateChildControlsInListView(C1ListView root)
        {
            RenderTemplateIntoContainer(root);
            
            HtmlGenericControl spanContainer = new HtmlGenericControl("span");
            this.InputContaier = spanContainer;
            if (ControlGroupType == Wijmo.Controls.C1ListView.ControlGroupType.normal)
            {
                this.InputContaier = spanContainer;

                spanContainer.Controls.Add(this.LabelControl);
                if (this.ListBoxForRender.Items.Count > 0)
                {
                    this.ListBoxForRender.ID = "select-" + ItemID;
                    this.LabelControl.AssociatedControlID = this.ListBoxForRender.ID;
                    spanContainer.Controls.Add(this.ListBoxForRender);
                }
            }
            else
            {
                spanContainer.Controls.Add(this.FieldsetContainer);
            }

            root.Controls.Add(spanContainer);
        }

        protected override void RenderCustomSection(HtmlTextWriter writer)
        {
            foreach (Control control in InputContaier.Controls)
            {
                control.RenderControl(writer);
            }
            if (Template != null)
            {
                foreach (Control control in TemplateContainer.Controls)
                {
                    control.RenderControl(writer);
                }
            }
        }

        protected override void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            if (this.ControlGroupType != ControlGroupType.normal)
            {
                RenderCheckboxAndRadioListInDesign(writer, container);
            }
            else
            {
                RenderNormalSelectListInDesign(writer, container);
            }
        }

        private void RenderNormalSelectListInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            string selectedText = string.Empty;
            foreach (ListItem item in this.InnerListControls.Items)
            {
                if (item.Selected)
                {
                    selectedText = item.Text;
                }
            }
            if (string.IsNullOrEmpty(selectedText) && this.InnerListControls.Items.Count > 0)
            {
                if (!NativeMenu && !string.IsNullOrEmpty(Placeholder))
                {
                    selectedText = Placeholder;
                }
                else
                {
                    selectedText = this.InnerListControls.Items[0].Text;
                }
            }

            HtmlGenericControl label = new HtmlGenericControl("label");
            label.Attributes.Add("class", "select ui-select");
            label.InnerHtml = this.LabelText;

            HtmlGenericControl controlItem = new HtmlGenericControl("div");
            controlItem.Attributes.Add("class", "ui-select");

            HtmlGenericControl innerContainer = new HtmlGenericControl("div");
            innerContainer.Attributes.Add("class", "ui-btn ui-shadow ui-btn-corner-all ui-btn-icon-right ui-btn-up-c");

            HtmlGenericControl innerSpan = new HtmlGenericControl("span");
            innerSpan.Attributes.Add("class", "ui-btn-inner");

            HtmlGenericControl innerSpanText = new HtmlGenericControl("span");
            innerSpanText.Attributes.Add("class", "ui-btn-text");
            innerSpanText.InnerHtml = "<span>" + selectedText + "</span>";

            HtmlGenericControl innerSpanIcon = new HtmlGenericControl("span");
            innerSpanIcon.Attributes.Add("class", "ui-icon ui-icon-arrow-d ui-icon-shadow");

            innerSpan.Controls.Add(innerSpanText);
            innerSpan.Controls.Add(innerSpanIcon);

            innerContainer.Controls.Add(innerSpan);
            controlItem.Controls.Add(innerContainer);

            container.Controls.Add(label);
            container.Controls.Add(controlItem);
        }

        private void RenderCheckboxAndRadioListInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            HtmlGenericControl fieldsetContainer = new HtmlGenericControl("fieldset");
            string fieldsetCss = "ui-corner-all ui-controlgroup ui-controlgroup-";
            fieldsetCss = fieldsetCss + (this.Horizontal ? "horizontal" : "vertical");
            fieldsetContainer.Attributes.Add("class", fieldsetCss);

            HtmlGenericControl cgLabelContainer = new HtmlGenericControl("div");
            cgLabelContainer.Attributes.Add("class", "ui-controlgroup-label");

            HtmlGenericControl cgLabel = new HtmlGenericControl("legend");
            cgLabel.InnerHtml = this.LabelText;

            cgLabelContainer.Controls.Add(cgLabel);

            HtmlGenericControl cgControlsContainer = new HtmlGenericControl("div");
            cgControlsContainer.Attributes.Add("class", "ui-controlgroup-controls");

            int count = this.InnerListControls.Items.Count;
            string typeIdentify = string.Empty;

            switch (this.ControlGroupType)
            {
                case ControlGroupType.checkboxlist:
                    typeIdentify = "checkbox";
                    break;
                case ControlGroupType.radiolist:
                    typeIdentify = "radio";
                    break;
            }

            for (int i = 0; i < count; i++)
            {
                ListItem item = this.InnerListControls.Items[i];
                HtmlGenericControl controlItem = new HtmlGenericControl("div");
                controlItem.Attributes.Add("class", "ui-" + typeIdentify);

                if (Horizontal)
                {
                    controlItem.Attributes.Add("style", "display: inline");
                }

                HtmlGenericControl innerLabelContainer = new HtmlGenericControl("label");
                string innerLabelCss = string.Empty;
                innerLabelCss += "ui-btn ui-btn-corner-all ui-fullsize ui-btn-icon-left ui-btn-up-c";
                if (i == count)
                {
                    innerLabelCss += " ui_last_child";
                }
                else if (i == 0)
                {
                    innerLabelCss += " ui_first_child";
                }
                innerLabelCss += (item.Selected ? " ui-" + typeIdentify + "-on" : " ui-" + typeIdentify + "-off");
                innerLabelCss += (item.Selected && Horizontal ? " ui-btn-active" : "");
                innerLabelContainer.Attributes.Add("class", innerLabelCss);

                HtmlGenericControl innerSpan = new HtmlGenericControl("span");
                innerSpan.Attributes.Add("class", "ui-btn-inner");

                HtmlGenericControl innerSpanText = new HtmlGenericControl("span");
                innerSpanText.Attributes.Add("class", "ui-btn-text");
                innerSpanText.InnerHtml = item.Text;

                HtmlGenericControl innerSpanIcon = new HtmlGenericControl("span");
                string innerSpanIconCss = "ui-icon";
                innerSpanIconCss += (item.Selected ? " ui-icon-" + typeIdentify + "-on" : " ui-icon-" + typeIdentify + "-off");
                innerSpanIconCss += " ui-icon-shadow";
                innerSpanIcon.Attributes.Add("class", innerSpanIconCss);

                innerSpan.Controls.Add(innerSpanText);
                if (!Horizontal)
                {
                    innerSpan.Controls.Add(innerSpanIcon);
                }

                innerLabelContainer.Controls.Add(innerSpan);

                controlItem.Controls.Add(innerLabelContainer);

                cgControlsContainer.Controls.Add(controlItem);
            }

            fieldsetContainer.Controls.Add(cgLabelContainer);
            fieldsetContainer.Controls.Add(cgControlsContainer);
            container.Controls.Add(fieldsetContainer);
        }

        private ListItem CreateListItemFormDataBinding(C1ListViewItemBinding binding, IHierarchyData data)
        {
            string textField = string.IsNullOrEmpty(binding.InnerTextField) ? "Text" : binding.InnerTextField;
            string valueField = string.IsNullOrEmpty(binding.InnerValueField) ? "Value" : binding.InnerValueField;
            string selectedField = string.IsNullOrEmpty(binding.InnerSelectedField) ? "Selected" : binding.InnerSelectedField;

            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(data);

            string textValue = string.Empty;
            string valueValue = string.Empty;
            bool selectedValue = false;

            object fieldValue = null;
            bool flag = false;
            fieldValue = GetFieldValue(data, pdc, textField);
            if (fieldValue != null)
            {
                textValue = (string)fieldValue;
                flag = true;
            }
            fieldValue = GetFieldValue(data, pdc, valueField);
            if (fieldValue != null)
            {
                valueValue = (string)fieldValue;
                flag = true;
            }
            fieldValue = GetFieldValue(data, pdc, selectedField);
            if (fieldValue != null)
            {
                selectedValue = ((string)fieldValue == "true");
                flag = true;
            }

            ListItem item = null;
            if (flag)
            {
                item = new ListItem(textValue, valueValue);
                item.Selected = selectedValue;
            }

            return item;
        }

        private void SettingNativeMenu(bool nativeMenu)
        {
            if (_listBoxForRender != null)
            {
                if (!nativeMenu)
                {
                    _listBoxForRender.Attributes.Add("data-native-menu", "false");
                    if (!string.IsNullOrEmpty(this.Placeholder))
                    {
                        ListItem leaderItem = new ListItem(this.Placeholder, string.Empty);
                        leaderItem.Attributes.Add("data-placeholder", "true");
                        _listBoxForRender.Items.Add(leaderItem);

                        _leaderItem = leaderItem;
                    }
                }
            }
        }
    }


    /// <summary>
    /// Link items.
    /// Second link will render as SplitButton.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewLinkItem : C1ListViewItem
    {
        internal override bool SetAsLinkItem
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Sets or gets the navigate url link of item
        /// </summary>
        [C1Description("C1ListViewLinkItem.NavigateUrl")]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue("")]
        public string NavigateUrl
        {
            get
            {
                return _properties["NavigateUrl"] == null ? ""
                    : _properties["NavigateUrl"].ToString();
            }
            set
            {
                _properties["NavigateUrl"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the navigate url title of item
        /// </summary>
        [C1Description("C1ListViewLinkItem.Title")]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue("")]
        public string Title
        {
            get
            {
                return _properties["Title"] == null ? ""
                    : _properties["Title"].ToString();
            }
            set
            {
                _properties["Title"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the split button url link of item
        /// </summary>
        [C1Description("C1ListViewLinkItem.SplitUrl")]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue("")]
        public string SplitUrl
        {
            get
            {
                return _properties["SplitUrl"] == null ? ""
                    : _properties["SplitUrl"].ToString();
            }
            set
            {
                _properties["SplitUrl"] = value;
            }
        }

        /// <summary>
        /// Sets the split button color scheme (swatch) of item.
        /// </summary>
        [C1Description("C1ListViewLinkItem.SplitTheme")]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue("")]
        public string SplitTheme
        {
            get
            {
                return _properties["SplitTheme"] == null ? ""
                    : _properties["SplitTheme"].ToString();
            }
            set
            {
                _properties["SplitTheme"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the split button url title of item
        /// </summary>
        [C1Description("C1ListViewLinkItem.SplitTitle")]
        [Layout(LayoutType.Appearance)]
        [RefreshProperties(RefreshProperties.All)]
        [DefaultValue("")]
        public string SplitTitle
        {
            get
            {
                return _properties["SplitTitle"] == null ? ""
                    : _properties["SplitTitle"].ToString();
            }
            set
            {
                _properties["SplitTitle"] = value;
            }
        }

        #region ** Data Binding

        protected override void SetValueFromDataBindingField(C1ListViewItemBinding binding, object data, PropertyDescriptorCollection pdc, bool bound)
        {
            base.SetValueFromDataBindingField(binding, data, pdc, bound);

            object fieldValue = null;
            //NavigateUrl
            if (!string.IsNullOrEmpty(binding.NavigateUrlField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.NavigateUrlField);
                if (fieldValue != null)
                {
                    this.NavigateUrl = (string)fieldValue;
                    bound = true;
                }
            }

            //Title
            if (!string.IsNullOrEmpty(binding.TitleField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.TitleField);
                if (fieldValue != null)
                {
                    this.Title = (string)fieldValue;
                    bound = true;
                }
            }

            //ExtendLink
            if (!string.IsNullOrEmpty(binding.SplitUrlField))
            {
                fieldValue = GetFieldValue(data, pdc, binding.SplitUrlField);
                if (fieldValue != null)
                {
                    string url = (string)fieldValue;
                    string title = string.Empty;
                    if (!string.IsNullOrEmpty(binding.SplitTitleField))
                    {
                        fieldValue = GetFieldValue(data, pdc, binding.SplitTitleField);
                        if (fieldValue != null)
                        {
                            title = (string)fieldValue;
                        }
                    }
                    this.SplitUrl = url;
                    this.SplitTitle = title;
                    bound = true;
                }
            }
        }

        #endregion

        /// <summary>
        /// Helper function for getting a string to construct CSS when rendering begin tag in design time.
        /// </summary>
        /// <returns></returns>
        protected override string RenderBeginTagCssInDesign()
        {
            string itemCssClass = string.Empty;
            string themeClass = UseDefaultTheme ? this.ListView.ThemeSwatch : this.Theme;

            if (string.IsNullOrEmpty(this.SplitUrl))
            {
                itemCssClass += "ui-btn" + " ui-btn-up-" + themeClass + " ui-btn-icon-right ui-li-has-arrow ui-li";
            }
            else
            {
                //** For split icon
                itemCssClass += "ui-btn" + " ui-btn-up-" + themeClass + " ui-btn-icon-right ui-li-has-alt ui-li";
            }

            return itemCssClass;
        }

        protected override void RenderBeginTagDataInRunTime()
        {
            base.RenderBeginTagDataInRunTime();

            if (!string.IsNullOrEmpty(this.DataIcon))
            {
                this.Attributes.Add("data-icon", this.DataIcon);
            }
        }

        /// <summary>
        /// Method for rendering content of a complex item as Link item.
        /// This method is used for rendering in design time.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="content"></param>
        protected override void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            if (this._aExtendLink == null)
            {
                string itemIcon = string.IsNullOrEmpty(this.DataIcon) ? this.ListView.Icon : this.DataIcon;
                writer.AddAttribute("class", "ui-icon ui-icon-" + itemIcon + " ui-icon-shadow");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.RenderEndTag();

                writer.RenderEndTag();
            }
            else
            {
                writer.RenderEndTag();
                _aExtendLink.RenderControl(writer);
            }
        }

        protected override void InitExtendControls()
        {
            base.InitExtendControls();

            if (!string.IsNullOrEmpty(this.SplitUrl))
            {
                _aExtendLink = new HtmlGenericControl("a");
                if (!string.IsNullOrEmpty(this.SplitTheme))
                {
                    _aExtendLink.Attributes.Add("data-theme", this.SplitTheme);
                }
                _aExtendLink.Attributes.Add("href", base.ResolveClientUrl(this.SplitUrl));
                if (IsDesignMode)
                {
                    string linkTheme = UseDefaultTheme ? this.ListView.ThemeSwatch : this.Theme;
                    string splitTheme = string.IsNullOrEmpty(this.SplitTheme) ? this.ListView.SplitTheme : this.SplitTheme;
                    string splitIcon = string.IsNullOrEmpty(this.DataIcon) ? this.ListView.SplitIcon : this.DataIcon;

                    _aExtendLink.Attributes.Add("class", "ui-li-link-alt ui-btn ui-btn-up-" + linkTheme + " ui-btn-icon-notext");

                    HtmlGenericControl warpperSpan = new HtmlGenericControl("span");
                    warpperSpan.Attributes.Add("class", "ui-btn-inner");

                    HtmlGenericControl textSpan = new HtmlGenericControl("span");
                    textSpan.Attributes.Add("class", "ui-btn-text");

                    HtmlGenericControl splitSpan = new HtmlGenericControl("span");
                    splitSpan.Attributes.Add("class",
                        "ui-btn ui-btn-up-" + splitTheme + " ui-shadow ui-btn-corner-all ui-btn-icon-notext");

                    HtmlGenericControl innerSpan = new HtmlGenericControl("span");
                    innerSpan.Attributes.Add("class", "ui-btn-inner");

                    HtmlGenericControl innerTextSpan = new HtmlGenericControl("span");
                    innerTextSpan.Attributes.Add("class", "ui-btn-text");

                    HtmlGenericControl innerIconSpan = new HtmlGenericControl("span");
                    innerIconSpan.Attributes.Add("class", "ui-icon ui-icon-" + splitIcon + " ui-icon-shadow");

                    innerSpan.Controls.Add(innerTextSpan);
                    innerSpan.Controls.Add(innerIconSpan);

                    splitSpan.Controls.Add(innerSpan);

                    warpperSpan.Controls.Add(textSpan);
                    warpperSpan.Controls.Add(splitSpan);

                    _aExtendLink.Controls.Add(warpperSpan);
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.SplitTitle))
                    {
                        _aExtendLink.InnerHtml = this.SplitTitle;
                    }
                }
            }
        }

        protected override void RenderBasicSection(HtmlTextWriter writer)
        {
            HtmlGenericControl link = new HtmlGenericControl("a");
            link.Attributes.Add("href", base.ResolveClientUrl(this.NavigateUrl));

            if (!string.IsNullOrEmpty(this.Title))
            {
                link.Attributes.Add("title", base.ResolveClientUrl(this.Title));
            }

            if (_imgThumbNail != null)
            {
                link.Controls.Add(_imgThumbNail);
            }
            if (_imgIcon != null)
            {
                link.Controls.Add(_imgIcon);
            }

            if (_paraSupplement != null)
            {
                link.Controls.Add(_paraSupplement);
                _paraSupplement = null;
            }

            if (Template != null)
            {
                link.Controls.Add(TemplateContainer);
            }
            else
            {
                if (_spanContent != null)
                {
                    link.Controls.Add(_spanContent);
                }
            }

            this.Controls.Add(link);
            link.RenderControl(writer);
        }

        /// <summary>
        /// Method for rendering Split button.
        /// PS: Template of link item has been rendered inside "a" item.
        /// </summary>
        /// <param name="writer"></param>
        protected override void RenderCustomSection(HtmlTextWriter writer)
        {
            if (this._aExtendLink != null)
            {
                this.Controls.Add(_aExtendLink);
                _aExtendLink.RenderControl(writer);
            }
        }
    }

    /// <summary>
    /// Nested item which can contains several items as children nodes.
    /// </summary>
    [ToolboxItem(false)]
    public class C1ListViewNestedItem : C1ListViewItem
    {
        internal HtmlControl _itemsContainer;

        public C1ListViewNestedItem()
            : base()
        {
            _items = new C1ListViewItemCollection(this);
            _itemsContainer = new HtmlGenericControl("ul");
        }

        internal override bool SetAsLinkItem
        {
            get
            {
                return true;
            }
        }

        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [Layout(LayoutType.Behavior)]
        [Browsable(false)]
        public override C1ListViewItemCollection Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new C1ListViewItemCollection(this);
                }
                return _items;
            }
        }

        #region ** DataBinding

        internal override void DataBindInternal(IHierarchicalEnumerable enumerable, int depth, C1ListViewItemBinding parBinding)
        {
            base.DataBindInternal(enumerable, depth, parBinding);

            foreach (object o in enumerable)
            {
                IHierarchyData data = enumerable.GetHierarchyData(o);
                string dataType = data.Type;

                C1ListViewItemBinding binding = this.ListView.GetBinding(dataType, depth);

                C1ListViewItem item = null;
                if (binding != null)
                {
                    item = this.ListView.CreateListViewItem(binding.ItemType, dataType);
                }
                else
                {
                    item = this.ListView.CreateListViewItem(DataBindingItemType.item, dataType);
                }
                item.DataBind(binding, enumerable, o, this.ListView.AutoGenerateDataBindings);
                this.Items.Add(item);

                if (data.HasChildren)
                {
                    if (this.ListView.DataBindStartlevel > item.CalculateCurrentLevel() || this.ListView.DataBindStartlevel == -1)
                    {
                        IHierarchicalEnumerable childEnum = data.GetChildren();
                        if (childEnum != null)
                        {
                            item.DataBindInternal(childEnum, depth + 1, binding);
                        }
                    }
                }
            }

        }

        #endregion

        /// <summary>
        ///  Deal with nested items inside.
        /// </summary>
        /// <param name="parent"></param>
        internal override void CreateChildControlsInListView(C1ListView parent)
        {
            base.CreateChildControlsInListView(parent);

            for (int i = 0; i < this.Items.Count; i++)
            {
                this.Items[i].CreateChildControlsInListView(parent);
            }

            this.ChildControlsCreated = true;
        }

        /// <summary>
        /// Helper function for getting a string to construct CSS when rendering begin tag in design time.
        /// </summary>
        /// <returns></returns>
        protected override string RenderBeginTagCssInDesign()
        {
            string itemCssClass = string.Empty;
            string themeClass = UseDefaultTheme ? this.ListView.ThemeSwatch : this.Theme;

            itemCssClass += "ui-btn" + " ui-btn-up-" + themeClass + " ui-btn-icon-right ui-li-has-arrow ui-li";

            return itemCssClass;
        }

        /// <summary>
        /// Method for rendering content of a complex item which has nested items.
        /// This method is used for rendering in design time.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="content"></param>
        protected override void RenderCustomContentInDesign(HtmlTextWriter writer, HtmlGenericControl container)
        {
            writer.AddAttribute("class", "ui-icon ui-icon-" + this.ListView.Icon + " ui-icon-shadow");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.RenderEndTag();

            writer.RenderEndTag();
        }

        protected override void RenderBeginTagDataInRunTime()
        {
            base.RenderBeginTagDataInRunTime();

            if (!string.IsNullOrEmpty(this.DataIcon))
            {
                this.Attributes.Add("data-icon", this.DataIcon);
            }
        }

        protected override void RenderBasicSection(HtmlTextWriter writer)
        {
            if (Template == null)
            {
                if (_spanContent != null)
                {
                    this.Controls.Add(_spanContent);
                    _spanContent.RenderControl(writer);
                }
            }

            if (_imgThumbNail != null)
            {
                this.Controls.Add(_imgThumbNail);
                _imgThumbNail.RenderControl(writer);
            }
            if (_imgIcon != null)
            {
                this.Controls.Add(_imgIcon);
                _imgIcon.RenderControl(writer);
            }
        }

        /// <summary>
        /// Method for rendering template.
        /// </summary>
        /// <param name="writer"></param>
        protected override void RenderCustomSection(HtmlTextWriter writer)
        {
            if (Template != null)
            {
                foreach (Control control in TemplateContainer.Controls)
                {
                    control.RenderControl(writer);
                }
            }
            if (_paraSupplement != null)
            {
                this.Controls.Add(_paraSupplement);
                _paraSupplement.RenderControl(writer);
                _paraSupplement = null;
            }

            if (_spanCountBubble != null)
            {
                this.Controls.Add(_spanCountBubble);
                _spanCountBubble.RenderControl(writer);
                _spanCountBubble = null;
            }

            _itemsContainer.Controls.Clear();
            this.Controls.Add(_itemsContainer);

            if (this.Items != null)
            {
                foreach (C1ListViewItem item in this.Items)
                {
                    _itemsContainer.Controls.Add(item);
                }
            }

            if (_itemsContainer.Controls.Count > 0)
            {
                _itemsContainer.RenderControl(writer);
            }
        }
    }
}
