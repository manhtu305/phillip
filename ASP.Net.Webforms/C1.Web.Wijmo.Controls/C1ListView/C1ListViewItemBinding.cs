﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1ListView
{
    [DefaultProperty("TextField")]
    public sealed class C1ListViewItemBinding : ICloneable, IDataSourceViewSchemaAccessor
    {
        #region **field

        private int _depth = -1;
        private string _dataMember = string.Empty;

        #region ** Normal fields **

        private string _textField = string.Empty;
        private string _themeField = string.Empty;
        private string _thumbNailSrcField = string.Empty;
        private string _iconSrcField = string.Empty;
        private string _countbubbleMessageField = string.Empty;
        private string _supplementField = string.Empty;

        #endregion

        #region ** fields for LinkItem **

        private string _navigateUrlField = string.Empty;
        private string _titleField = string.Empty;
        private string _splitUrlField = string.Empty;
        private string _splitTitleField = string.Empty;

        #endregion

        #region ** fields for InputItem **

        private string _labelTextField = string.Empty;
        private string _typeValueField = string.Empty;
        private string _minValueField = string.Empty;
        private string _maxValueField = string.Empty;
        private string _valueValueField = string.Empty;

        #endregion

        #region ** fields for FlipSwitch **

        private string _onMessageField = string.Empty;
        private string _onValueField = string.Empty;
        private string _offMessageField = string.Empty;
        private string _offValueField = string.Empty;
        private string _selectedONField = string.Empty;

        #endregion

        #region ** fields for ControlGroup **

        private string _horizontalField = string.Empty;
        private string _controlGroupTypeField = string.Empty;
        private string _nativeMenuField = string.Empty;
        private string _placeholderField = string.Empty;

        #region *** fields for InnerListItem ***
        private string _innerTextField = string.Empty;
        private string _innerValueField = string.Empty;
        private string _innerSelectedField = string.Empty;
        #endregion

        #endregion

        private object _dataSourceViewSchema;
        private DataBindingItemType _itemType = DataBindingItemType.item;

        #endregion

        #region ** constructor

        /// <summary>
        /// Create new instance of the C1ListViewItemBinding class.
        /// </summary>
        public C1ListViewItemBinding()
        { }

        /// <summary>
        /// Create new instance of the C1ListViewItemBinding class.
        /// </summary>
        /// <param name="dataMember">Data member to bind a listview item.</param>
        public C1ListViewItemBinding(string dataMember)
        {
            this._dataMember = dataMember;
        }

        #endregion

        #region ** properties

        /// <summary>
        /// Gets or sets the listview depth to which the C1ListViewItemBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.Depth")]
        [C1Category("Category.Data")]
        [DefaultValue(-1)]
        public int Depth
        {
            get
            {
                return this._depth;
            }
            set
            {
                this._depth = value;
            }
        }

        [C1Description("C1ListViewItemBinding.DataMember")]
        [C1Category("Category.Data")]
        [DefaultValue("")]
        public string DataMember
        {
            get
            {
                return this._dataMember;
            }
            set
            {
                this._dataMember = value;
            }
        }

        #region ** Properties of Normal **

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Text property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.TextField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string TextField
        {
            get { return _textField; }
            set { _textField = value; }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Theme property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.ThemeField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string ThemeField
        {
            get { return _themeField; }
            set { _themeField = value; }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the ThumbNailSrc property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.ThumbNailSrcField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string ThumbNailSrcField
        {
            get { return _thumbNailSrcField; }
            set { _thumbNailSrcField = value; }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the IconSrc property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.IconSrcField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string IconSrcField
        {
            get { return _iconSrcField; }
            set { _iconSrcField = value; }
        }

        [C1Description("C1ListViewItemBinding.CountbubbleMessageField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string CountbubbleMessageField
        {
            get { return _countbubbleMessageField; }
            set { _countbubbleMessageField = value; }
        }

        [C1Description("C1ListViewItemBinding.SupplementField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string SupplementField
        {
            get { return _supplementField; }
            set { _supplementField = value; }
        }

        #endregion

        #region ** Properties for LinkItem

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the NavigateUrl property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.NavigateUrlField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string NavigateUrlField
        {
            get { return _navigateUrlField; }
            set { _navigateUrlField = value; }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Title property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.TitleField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string TitleField
        {
            get { return _titleField; }
            set { _titleField = value; }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the SplitUrl property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.SplitUrlField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string SplitUrlField
        {
            get { return _splitUrlField; }
            set { _splitUrlField = value; }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the SplitTitle property of a C1ListViewItem object to which the C1ListViewBinding object is applied.
        /// </summary>
        [C1Description("C1ListViewItemBinding.SplitTitleField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string SplitTitleField
        {
            get { return _splitTitleField; }
            set { _splitTitleField = value; }
        }

        #endregion

        #region ** Properties for InputItem

        [C1Description("C1ListViewItemBinding.LabelTextField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string LabelTextField
        {
            get { return _labelTextField; }
            set { _labelTextField = value; }
        }

        [C1Description("C1ListViewItemBinding.TypeField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string TypeField
        {
            get { return _typeValueField; }
            set { _typeValueField = value; }
        }

        [C1Description("C1ListViewItemBinding.MinField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string MinField
        {
            get { return _minValueField; }
            set { _minValueField = value; }
        }

        [C1Description("C1ListViewItemBinding.MaxField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string MaxField
        {
            get { return _maxValueField; }
            set { _maxValueField = value; }
        }

        [C1Description("C1ListViewItemBinding.ValueField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string ValueField
        {
            get { return _valueValueField; }
            set { _valueValueField = value; }
        }

        #endregion

        #region ** Properties for FlipSwitch **

        [C1Description("C1ListViewItemBinding.OnMessageField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string OnMessageField
        {
            get { return _onMessageField; }
            set { _onMessageField = value; }
        }

        [C1Description("C1ListViewItemBinding.OnValueField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string OnValueField
        {
            get { return _onValueField; }
            set { _onValueField = value; }
        }

        [C1Description("C1ListViewItemBinding.OffMessageField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string OffMessageField
        {
            get { return _offMessageField; }
            set { _offMessageField = value; }
        }

        [C1Description("C1ListViewItemBinding.OffValueField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string OffValueField
        {
            get { return _offValueField; }
            set { _offValueField = value; }
        }

        [C1Description("C1ListViewItemBinding.SelectedON")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string SelectedONField
        {
            get { return _selectedONField; }
            set { _selectedONField = value; }
        }
        #endregion

        #region ** Properties for ControlGroup **

        [C1Description("C1ListViewItemBinding.HorizontalField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string HorizontalField
        {
            get { return _horizontalField; }
            set { _horizontalField = value; }
        }

        [C1Description("C1ListViewItemBinding.ControlGroupTypeField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string ControlGroupTypeField
        {
            get { return _controlGroupTypeField; }
            set { _controlGroupTypeField = value; }
        }

        [C1Description("C1ListViewItemBinding.NativeMenuField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string NativeMenuField
        {
            get { return _nativeMenuField; }
            set { _nativeMenuField = value; }
        }

        [C1Description("C1ListViewItemBinding.LeaderMessageField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string PlaceholderField
        {
            get { return _placeholderField; }
            set { _placeholderField = value; }
        }

        #region *** Properties for InnerListItem ***
        [C1Description("C1ListViewItemBinding.InnerTextField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string InnerTextField
        {
            get { return _innerTextField; }
            set { _innerTextField = value; }
        }
        [C1Description("C1ListViewItemBinding.InnerValueField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string InnerValueField
        {
            get { return _innerValueField; }
            set { _innerValueField = value; }
        }
        [C1Description("C1ListViewItemBinding.InnerSelectedField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string InnerSelectedField
        {
            get { return _innerSelectedField; }
            set { _innerSelectedField = value; }
        }
        #endregion

        #endregion

        #endregion

        [C1Description("C1ListViewItemBinding.ItemType")]
        [DefaultValue(DataBindingItemType.item)]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public DataBindingItemType ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        #region ** ICloneable
        object ICloneable.Clone()
        {
            C1ListViewItemBinding binding = new C1ListViewItemBinding();
            binding.DataMember = this.DataMember;
            binding.Depth = this.Depth;
            binding.TextField = this.TextField;
            binding.ThemeField = this.ThemeField;
            binding.ThumbNailSrcField = this.ThumbNailSrcField;
            binding.IconSrcField = this.IconSrcField;
            binding.CountbubbleMessageField = this.CountbubbleMessageField;
            binding.SupplementField = this.SupplementField;
            binding.NavigateUrlField = this.NavigateUrlField;
            binding.TitleField = this.TitleField;
            binding.SplitUrlField = this.SplitUrlField;
            binding.SplitTitleField = this.SplitTitleField;
            binding.LabelTextField = this.LabelTextField;
            binding.TypeField = this.TypeField;
            binding.MinField = this.MinField;
            binding.MaxField = this.MaxField;
            binding.ValueField = this.ValueField;
            binding.OnMessageField = this.OnMessageField;
            binding.OnValueField = this.OnValueField;
            binding.OffMessageField = this.OffMessageField;
            binding.OffValueField = this.OffValueField;
            binding.SelectedONField = this.SelectedONField;
            binding.HorizontalField = this.HorizontalField;
            binding.ControlGroupTypeField = this.ControlGroupTypeField;
            binding.NativeMenuField = this.NativeMenuField;
            binding.PlaceholderField = this.PlaceholderField;
            binding.InnerTextField = this.InnerTextField;
            binding.InnerValueField = this.InnerValueField;
            binding.InnerSelectedField = this.InnerSelectedField;
            binding.ItemType = this.ItemType;

            return binding;
        }
        #endregion

        #region ** IDataSourceViewSchemaAccessor
        object IDataSourceViewSchemaAccessor.DataSourceViewSchema
        {
            get
            {
                return _dataSourceViewSchema;
            }
            set
            {
                _dataSourceViewSchema = value;
            }
        }
        #endregion
    }
}
