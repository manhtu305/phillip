﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.C1ToolTip
{
    internal class C1ToolTipSerializer : C1BaseSerializer<C1ToolTip,object,object>
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="C1ToolTipSerializer"/> class.
        /// </summary>
        /// <param name="serializableObject">Serializable Object.</param>
        public C1ToolTipSerializer(object serializableObject) : base(serializableObject)
        {
        }

        /// <summary>
		/// Initializes a new instance of the <see cref="C1ToolTipSerializer"/> class.
        /// </summary>
		/// <param name="componentChangeService">IComponentChangeService</param>
		/// <param name="serializableObject">Serializable Object.</param>
        public C1ToolTipSerializer(IComponentChangeService componentChangeService, object serializableObject)
			: base(componentChangeService, serializableObject)
        {
        }
    }
}
