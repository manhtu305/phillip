﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using C1.Web.Wijmo.Controls.Localization;
using System.Collections;
using C1.Util.Licensing;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Reflection;

namespace C1.Web.Wijmo.Controls.C1ToolTip
{
#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1ToolTip.C1ToolTipDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1ToolTip.C1ToolTipDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1ToolTip.C1ToolTipDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1ToolTip.C1ToolTipDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1ToolTip runat=server></{0}:C1ToolTip>")]
	[ToolboxBitmap(typeof(C1ToolTip), ("C1ToolTip.png"))]
	[LicenseProviderAttribute()]
	public partial class C1ToolTip : C1TargetControlBase, INamingContainer, IPostBackDataHandler, ICallbackEventHandler, IC1Serializable
	{

		#region **fields
		internal bool _productLicensed;
		private bool _shouldNag;
		private ITemplate _template;
		private UpdatePanel _updatePanel;
		private string _requestKeyID;
		#endregion

		#region ** Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ToolTip"/> class.
		/// </summary>
		public C1ToolTip():base()
		{
			VerifyLicense();
			this.InitTooltip();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ToolTip), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

//        /// <summary>
//        /// Initializes a new instance of the <see cref="C1ToolTip"/> class.
//        /// </summary>
//        /// <param name="key">The key.</param>
//        [EditorBrowsable(EditorBrowsableState.Never)] 
//        public C1ToolTip(string key)
//        {
//#if GRAPECITY
//            _productLicensed = true;
//#else
//            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ToolTip), this,
//                Assembly.GetExecutingAssembly(), key);
//            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
//#endif
//            this.InitTooltip();
//        }

		private void InitTooltip() 
		{
			this.Position = new PositionSettings();
			this.Position.My = new Position() { Left = HPosition.Left, Top = VPosition.Bottom };
			this.Position.At = new Position() { Left = HPosition.Right, Top = VPosition.Top };
			this.Position.Offset = new Offset();
			this.Position.Collision = new Collision() { Left = CollisionMode.Flip, Top = CollisionMode.Flip };
			this.Animation = new Animation() { Animated = new AnimatedOption() { Effect = "fade" }, Duration = 400, Easing = Easing.Swing, Option = new Dictionary<string, string>() };
			this.ShowAnimation = new Animation() { Animated = new AnimatedOption() { Effect = "fade" } };
			this.HideAnimation = new Animation() { Animated = new AnimatedOption() { Effect = "fade" } };
			this.CalloutAnimation = new SlideAnimation();
			this.Content = new ContentOption();
		}
		#endregion

		#region ** properties
		/// <summary>
		/// Gets the innerStates for tooltip. it send to client side. to deserialize the client function option.
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Hashtable InnerStates
		{
			get
			{
				if (this.Content.Function != "")
				{
					if (base.InnerStates["Content"] == null)
					{
						base.InnerStates.Add("Content", this.Content.Function);
					}
					else
					{
						base.InnerStates["Content"] = this.Content.Function;
					}

					if (base.InnerStates["Title"] == null)
					{
						base.InnerStates.Add("Title", this.Title.Function);
					}
					else
					{
						base.InnerStates["Title"] = this.Title.Function;
					}
				}
				return base.InnerStates;
			}
		}

		/// <summary>
		/// Gets or sets the selector.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1ToolTip.TargetSelector")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue("")]
		public string TargetSelector 
		{
			get
			{
				return GetPropertyValue<string>("TargetSelector", "");
			}
			set
			{
				SetPropertyValue<string>("TargetSelector", value);
			}
		}

		/// <summary>
		/// Gets or sets the selector.
		/// </summary>
		[WidgetOption]
		[DefaultValue("")]
		public override string CssClass
		{
			get
			{
				return GetPropertyValue<string>("CssClass", "");
			}
			set
			{
				SetPropertyValue<string>("CssClass", value);
			}
		}

		/// <summary>
		/// Gets or sets the template for the content area of the C1ToolTip control.
		/// </summary>
		[Browsable(false)]
		[Bindable(false)]
		[DefaultValue(null)]
		[C1Description("C1ToolTip.Template")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public ITemplate Template
		{
			get
			{
				return this._template;
			}
			set
			{
				_template = value;
			}
		}

		[WidgetOption]
		[DefaultValue(false)]
		[Browsable(false)]
		[EditorBrowsable( EditorBrowsableState.Never)]
		public bool HasTemplate 
		{
			get
			{
				return Template != null;
			}
		}

		/// <summary>
		/// Gets the UpdatePanel of the tooltip.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1ToolTip.UpdatePanel")]
		[Layout(LayoutType.Behavior)]
		public UpdatePanel UpdatePanel
		{
			get
			{
				return _updatePanel;
			}
		}

		/// <summary>
		/// Gets or sets the width of the C1ToolTip.
		/// </summary>
		[DefaultValue(typeof(Unit), "")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption]
		[WidgetOptionName("controlwidth")]
		public override Unit Width
		{
			get
			{
				return GetPropertyValue<Unit>("Width", Unit.Empty);
			}
			set
			{
				SetPropertyValue<Unit>("Width", value);
			}
		}

		/// <summary>
		/// Gets or sets the height of the C1ToolTip.
		/// </summary>
		[DefaultValue(typeof(Unit), "")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption]
		[WidgetOptionName("controlheight")]
		public override Unit Height
		{
			get
			{
				return GetPropertyValue<Unit>("Height", Unit.Empty);
			}
			set
			{
				SetPropertyValue<Unit>("Height", value);
			}
		}

		/// <summary>
		/// Gets the clientID of the tooltip.
		/// </summary>
		[WidgetOption]
		[WidgetOptionName("id")]
		[DefaultValue("")]
		public override string ClientID
		{
			get
			{
				return base.ClientID;
			}
		}

		/// <summary>
		/// Gets the uniqueID of the tooltip.
		/// </summary>
		[WidgetOption]
		[DefaultValue("")]
		public override string UniqueID
		{
			get
			{
				return base.UniqueID;
			}
		}

		/// <summary>
		/// Gets or sets a value that determines whether to enable callback to load tooltip's content on demand.
		/// </summary>
		[C1Description("C1ToolTip.EnableCallBackMode")]
		[DefaultValue(false)]
		[Bindable(false)]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption()]
		public bool EnableCallBackMode
		{
			get
			{
				return GetPropertyValue<bool>("EnableCallBackMode", false);
			}
			set
			{
				SetPropertyValue<bool>("EnableCallBackMode", value);
			}
		}

		/// <summary>
		/// Return true if the page has an ScriptManager control,  send it to client side.
		/// If true, use UpdatePanel's postback, else use callback.
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[Json(true)]
		[EditorBrowsable( EditorBrowsableState.Never)]
		public bool UseAjaxPostback
		{
			get
			{
				if (this.Page != null)
				{
					return ScriptManager.GetCurrent(this.Page) != null &&
						this.OnAjaxUpdate != null && this.Template != null;
				}
				return false;
			}
		}

		#region UnBrowsable Properties

		/// <summary>
		/// The property should be hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string AccessKey
		{
			get
			{
				return base.AccessKey;
			}
			set
			{
				base.AccessKey = value;
			}
		}

		/// <summary>
		/// The property should be hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string ToolTip
		{
			get
			{
				return base.ToolTip;
			}
			set
			{
				base.ToolTip = value;
			}
		}
		#endregion

		#endregion

		#region ** override methods

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			string script = string.Format("$('{0}').wijSaveState('{1}', 'encode');", this.TargetSelector, this.WidgetName);
			string scriptId = this.ClientID + "_SubmitScript";
			ScriptManager sm = ScriptManager.GetCurrent(Page);
			if (sm != null)
			{
				ScriptManager.RegisterOnSubmitStatement(this, typeof(C1TargetControlBase), scriptId, script);
			}
			else
			{
				this.Page.ClientScript.RegisterOnSubmitStatement(typeof(C1TargetControlBase), scriptId, script);
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified
		/// System.Web.UI.HtmlTextWriterTag. This method is used primarily by control
		/// developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			if (!IsDesignMode)
			{
				this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass);
			}
			base.AddAttributesToRender(writer);
			this.CssClass = cssClass;
			writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
			Page.ClientScript.GetCallbackEventReference(this, "", "", "");
		}

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}
		
		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			if(UseAjaxPostback)
			{
				if (ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack) 
				{
					if (!string.IsNullOrEmpty(_requestKeyID))
					{
						C1ToolTipCallBackEventArgs args = new C1ToolTipCallBackEventArgs();
						args.Source = _requestKeyID;
						args.UpdatePanel = _updatePanel;
						OnAjaxUpdate(args);
						this.Content.Content = args.Content;
						this.Title.Content = args.Title;
					}
				}
			}
			base.OnPreRender(e);
			Page.RegisterRequiresPostBack(this);
			EnsureChildControls();
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			base.Render(writer);            
		}

		protected override void RegisterScriptDescriptors()
		{
			if (!IsDesignMode)
			{
				this.TargetSelector = this.GetClientSelector(this.TargetSelector);
			}

			List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)GetScriptDescriptors(this.TargetSelector);
			foreach (WidgetDescriptor desc in scriptDescr)
			{
				ScriptManager.RegisterStartupScript(this, this.GetType(), "c" + this.ClientID, desc.GetScriptInternal(this.WijmoControlMode, IsControlInAppviewPage()), true);
			}
		}

		private bool IsControlInAppviewPage()
		{
			bool isControlInAppviewPage = false;
			Control ctrl = this.Parent;
			if (this.WijmoControlMode == WijmoControlMode.Web)
			{
				return false;
			}
			while (ctrl != Page)
			{
				if (ctrl is C1AppView.C1AppViewPage)
				{
					isControlInAppviewPage = true;
					break;
				}
				ctrl = ctrl.Parent;
			}
			return isControlInAppviewPage;
		}
		
		private IEnumerable<ScriptDescriptor> GetScriptDescriptors(string targetSelector)
		{
			WidgetDescriptor descriptor = new WidgetDescriptor(this.WidgetName, targetSelector);
			WidgetObjectBuilder.DescribeWidget(this, descriptor);
			return new List<ScriptDescriptor>(new WidgetDescriptor[] { descriptor });
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			base.CreateChildControls();

			if (UseAjaxPostback)
			{
				_updatePanel = new UpdatePanel();
				_updatePanel.ID = "UpdatePanel1";
				_updatePanel.ContentTemplate = Template;
				this.Controls.Add(_updatePanel);
			}
			else
			{
				if (Template != null)
				{
					Template.InstantiateIn(this);
				}
			}
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <param name="targetControl">The server control to which the extender is associated.</param>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			//if (!this.Enabled)
			//{
			//    return base.GetScriptDescriptors();
			//}
			if (!IsDesignMode)
			{
				this.TargetSelector = this.GetClientSelector(this.TargetSelector);
			}

			WidgetDescriptor descriptor = new WidgetDescriptor(this.WidgetName, this.TargetSelector);
			WidgetObjectBuilder.DescribeWidget(this, descriptor);
			return new List<ScriptDescriptor>(new WidgetDescriptor[] { descriptor });
		}

		#endregion

		#region ** private & internal methods
		private string GetClientSelector(string selector)
		{
			string[] selectors = selector.Split(',');
			for (int i = 0; i < selectors.Length; i++)
			{
				string s = selectors[i];
				if (s.StartsWith("#"))
				{
					Control c = this.NamingContainer.FindControl(s.Substring(1));
					if (c != null)
					{
						selectors[i] = string.Format("#{0}", c.ClientID);
					}
				}
			}
			return string.Join(",", selectors);
		}
		#endregion

		#region ** IPostBackDataHandler
		/// <summary>
		/// Restores view-state information from a previous request that was saved with
		/// the System.Web.UI.WebControls.WebControl.SaveViewState() method.
		/// </summary>
		/// <param name="savedState">
		/// An object that represents the control state to restore.
		/// </param>
		protected override void LoadViewState(object savedState)
		{
			Hashtable data = JsonSerializableHelper.GetJsonData(Page.Request.Form);
			JsonRestoreHelper.RestoreStateFromJson(this, data);
			//handle encode content.
			if (!string.IsNullOrEmpty(this.Content.Content))
			{
				this.Content.Content = this.Page.Server.UrlDecode(this.Content.Content);
			}

			if (!string.IsNullOrEmpty(this.Title.Content))
			{
				this.Title.Content = this.Page.Server.UrlDecode(this.Title.Content);
			}
		}

		/// <summary>
		/// Saves any state that was modified after the System.Web.UI.WebControls.Style.TrackViewState()
		/// method was invoked.
		/// </summary>
		/// <returns>
		/// An object that contains the current view state of the control; otherwise,
		/// if there is no view state associated with the control, null.
		/// </returns>
		protected override object SaveViewState()
		{
			object[] state = new object[2];
			state[0] = base.SaveViewState();
			state[1] = DateTime.Now;
			return state;
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			string targetID = postCollection["__EVENTTARGET"];
			//The "targetID" that postback from client side is UpdatePanel's ClientID
			//if (targetID != this.ClientID)
			if (targetID != this.ClientID + "_UpdatePanel1")
			{
				return false;
			}
			Hashtable data = JsonHelper.StringToObject(postCollection["__EVENTARGUMENT"]);
			bool changed = true;
			if (data != null)
			{
				string requestKeyID = (string)data["target"];
				if (!string.IsNullOrEmpty(requestKeyID))
				{
					_requestKeyID = requestKeyID;
				}
			}
			return changed;
			//throw new NotImplementedException();
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			//throw new NotImplementedException();
		}
		#endregion

		/// <summary>
		/// Occurs on the server after the AJAX request back to server-side. 
		/// </summary>
		[C1Description("C1ToolTip.OnAjaxUpdate")]
		public event GetContentCallbackHander OnAjaxUpdate;

		#region ** ICallbackEventHandler

		private string _argument;

		string ICallbackEventHandler.GetCallbackResult()
		{
			if (!UseAjaxPostback)
			{
				Hashtable paramters = JsonHelper.StringToObject(this._argument);
				if (paramters.Count > 0)
				{
					if (paramters["key"].ToString() == "AjaxCallback")
					{
						C1ToolTipCallBackEventArgs e = new C1ToolTipCallBackEventArgs();
						e.Source = paramters["target"].ToString();
						OnAjaxUpdate(e);
						Hashtable hash = new Hashtable();
						hash.Add("title", e.Title);
						hash.Add("content", e.Content);
						//return JsonHelper.ObjectToString(hash, this);
						return JsonHelper.WidgetToString(hash, this);
					}
				}
				return "";
			}
			return "";
		}

		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			if (!UseAjaxPostback)
			{
				this._argument = eventArgument;
			}
		}
		#endregion

		#region ** IC1Serializable Layout
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1ToolTipSerializer sz = new C1ToolTipSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(System.IO.Stream stream)
		{
			C1ToolTipSerializer sz = new C1ToolTipSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			C1ToolTipSerializer sz = new C1ToolTipSerializer(this);
			sz.LoadLayout(filename);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(System.IO.Stream stream)
		{
			C1ToolTipSerializer sz = new C1ToolTipSerializer(this);
			sz.LoadLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1ToolTipSerializer sz = new C1ToolTipSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(System.IO.Stream stream, LayoutType layoutTypes)
		{
			C1ToolTipSerializer sz = new C1ToolTipSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion
	}

	#region ** CallbackEvent
	/// <summary>
	/// C1ToolTipCallBackEventArgs is used for the OnAjaxUpdate event.
	/// </summary>
	public class C1ToolTipCallBackEventArgs : EventArgs
	{
		private string _title;
		private string _content;
		private string _source;
		private UpdatePanel _updatePanel;
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ToolTipCallBackEventArgs"/> class.
		/// </summary>
		public C1ToolTipCallBackEventArgs()
			: base()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ToolTipCallBackEventArgs"/> class.
		/// </summary>
		/// <param name="title">Tooltip's title</param>
		/// <param name="content">Tooltip's content</param>
		public C1ToolTipCallBackEventArgs(string title, string content)
		{
			_title = title;
			_content = content;
		}

		/// <summary>
		/// Gets or sets the Title of the tooltip
		/// </summary>
		public string Title 
		{
			get
			{
				return _title;            
			}
			set
			{
				_title = value;
			}
		}

		/// <summary>
		/// Gets or sets the content of the tooltip
		/// </summary>
		public string Content
		{
			get
			{
				return _content;
			}
			set
			{
				_content = value;
			}
		}

		/// <summary>
		/// Gets or sets the source control of the tooltip.
		/// </summary>
		public string Source
		{
			get
			{
				return _source;
			}
			set
			{
				_source = value;
			}
		}

		/// <summary>
		/// Gets or sets the updatepanel of the tooltip.
		/// </summary>
		public UpdatePanel UpdatePanel 
		{
			get
			{
				return _updatePanel;
			}
			set
			{
				_updatePanel = value;
			}
		}

	}
	/// <summary>
	/// Delegate type for handling OnAjaxUpdate events.
	/// </summary>
	/// <param name="e">C1ToolTipCallBackEventArgs object that contains the event data</param>
	public delegate void GetContentCallbackHander(C1ToolTipCallBackEventArgs e);
	#endregion
}
