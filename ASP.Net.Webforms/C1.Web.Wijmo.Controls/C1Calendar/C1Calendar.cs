﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;
using System.IO;



namespace C1.Web.Wijmo.Controls.C1Calendar
{
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;
	using System.Collections.Specialized;

	[ControlValueProperty("DisplayDate")]
	[DefaultProperty("DisplayDate")]
	[ToolboxData("<{0}:C1Calendar runat=server></{0}:C1Calendar>")]
	[ToolboxBitmap(typeof(C1Calendar), "C1Calendar.png")]
	[ParseChildren(true)]
	[PersistChildren(false)]
	[LicenseProviderAttribute()]
	[Localizable(true)]
	[AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	[AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Calendar.C1CalendarDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Calendar.C1CalendarDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Calendar.C1CalendarDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Calendar.C1CalendarDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	public partial class C1Calendar : C1TargetControlBase, 
		INamingContainer,
		IPostBackEventHandler,
		IPostBackDataHandler
	{
		#region Fields

		//private C1DateCollection _selWebScheduleDates;
		private Hashtable _monthViews = new Hashtable();
		private DateTime _groupStartDate;
		private DateTime _groupEndDate;

		private bool _productLicensed = false;
		private bool _shouldNag;

		#endregion

		#region Constructor


		/// <summary>
		/// Construct a new instance of C1Calendar.
		/// </summary>
		[C1Description("C1Calendar.Constructor")]
		public C1Calendar()
			: base(HtmlTextWriterTag.Div)
		{
			VerifyLicense();
		}

		internal C1Calendar(string key)
			: base(HtmlTextWriterTag.Div)
		{
			this.VerifyLicense(key);
		}

		#endregion

		#region Licensing

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Calendar), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Verifies the license.
		/// </summary>
		/// <param name="key">The key.</param>
		internal void VerifyLicense(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			// ttZzKzJ5mwNr/IihpBl2pA==
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Calendar), this,
				Assembly.GetExecutingAssembly(), key);
			_shouldNag = licinfo.ShouldNag;
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		#endregion

		#region Properties

		/// <summary>
		/// PostBack Event Reference. Empty string if AutoPostBack property is false.
		/// </summary>
		[WidgetOption()]
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string PostBackEventReference
		{
			get
			{
				if (!IsDesignMode && this.AutoPostBack)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		/// <summary>
		/// Gets or sets a value that determines whether post back to server automatically when user selects a date.
		/// </summary>
		[WidgetOption()]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[C1Description("C1Calendar.AutoPostBack")]
		public bool AutoPostBack
		{
			get
			{
				object o = ViewState["AutoPostBack"];
				return (o != null) ? (bool)o : false;
			}
			set
			{
				ViewState["AutoPostBack"] = value;
			}
		}

		#endregion

		#region C1Schedule


//        /// <summary>
//        /// Gets or sets the C1Schedule control to interact with.
//        /// </summary>
//#if ASP_NET4
//        [Editor("C1.Web.UI.Design.UITypeEditors.C1ScheduleEditor, C1.Web.UI.Design.4", typeof(UITypeEditor))]
//#elif ASP_NET35
//        [Editor("C1.Web.UI.Design.UITypeEditors.C1ScheduleEditor, C1.Web.UI.Design.3", typeof(UITypeEditor))]
//#else
//        [Editor("C1.Web.UI.Design.UITypeEditors.C1ScheduleEditor, C1.Web.UI.Design.2", typeof(UITypeEditor))]
//#endif
//        [DefaultValue("")]
//        public string WebSchedule
//        {
//            get
//            {
//                object o = this.ViewState["WebSchedule"];
//                return (o != null) ? (string)o : "";
//            }
//            set
//            {
//                this.ViewState["WebSchedule"] = value;
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        [Browsable(false)]
//        [EditorBrowsable(EditorBrowsableState.Never)]
//        [Json(true, true, "")]
//        public string WebScheduleClientObjID
//        {
//            get
//            {
//                string schedule = this.WebSchedule;
//                if (!string.IsNullOrEmpty(schedule))
//                {
//                    Control c = this.GetWebSchedule();
//                    if (c != null)
//                    {
//                        schedule = c.ClientID;
//                        Type t = c.GetType();
//                        PropertyInfo pi = t.GetProperty("ClientObjectID");
//                        if (pi != null)
//                        {
//                            schedule = (string)pi.GetValue(c, null);
//                        }
//                    }
//                    else
//                        schedule = "";
//                }

//                return schedule;
//            }
//        }

//        private Control GetWebSchedule()
//        {
//            string schedule = this.WebSchedule;
//            if (string.IsNullOrEmpty(schedule)) return null;

//            return InternalResolveControlByID(schedule, this.Page, this.NamingContainer);
//        }

//        internal static System.Web.UI.Control InternalResolveControlByID(string id, Page page, System.Web.UI.Control namingContainer)
//        {
//            System.Web.UI.Control control1 = page != null ? page.FindControl(id) : null;
//            if ((control1 == null) && (namingContainer != null))
//            {
//                control1 = namingContainer.FindControl(id);
//            }
//            if (control1 == null && page != null)
//            {
//                ControlCollection col = page.Controls;
//                control1 = InternalFindControlByID(col, id);
//            }
//            return control1;
//        }

//        private static System.Web.UI.Control InternalFindControlByID(ControlCollection col, string id)
//        {
//            if (col != null)
//            {
//                for (int i = 0; i < col.Count; i++)
//                {
//                    System.Web.UI.Control control = col[i];
//                    if (control.ID == id)
//                        return control;
//                    System.Web.UI.Control cur = null;
//                    if (control is System.Web.UI.UserControl)
//                    {
//                        cur = InternalFindControlByID(((System.Web.UI.UserControl)control).Controls, id);
//                    }
//                    else
//                    {
//                        cur = InternalFindControlByID(control.Controls, id);
//                    }
//                    if (cur != null)
//                        return cur;
//                }
//            }
//            return null;
//        }


		#endregion

		#region IPostbak Events

		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			switch (eventArgument)
			{
				case "selectedDatesChanged":
					this.OnSelectedDatesChanged(EventArgs.Empty);
					break;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeSelectedDates(object value)
		{
			ArrayList list = value as ArrayList;
			if (list != null)
			{
				this.SelectedDates = (DateTime[])list.ToArray(typeof(DateTime));
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeDisabledDates(object value)
		{
			ArrayList list = value as ArrayList;
			if (list != null)
			{
				this.DisabledDates = (DateTime[])list.ToArray(typeof(DateTime));
			}
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			this.RestoreStateFromJson(data);
			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			Console.WriteLine("RaisePostDataChangedEvent");
		}

		#endregion

		#region Events

		/// <summary>
		/// Fires when the SelectedDates property is changed by mouse clicking.
		/// </summary>
		[C1Description("C1Calendar.SelectedDatesChanged")]
		public event EventHandler SelectedDatesChanged;

		/// <summary>
		/// Fires the SelectedDatesChanged event.
		/// </summary>
		/// <param name="e">The event arg.</param>
		public void OnSelectedDatesChanged(EventArgs e)
		{
			if (this.SelectedDatesChanged != null)
				this.SelectedDatesChanged(this, e);
		}

		/// <summary>
		/// Fires when the DisplayDate property is changed by navigating.
		/// </summary>
		[C1Description("C1Calendar.DisplayDateChanged")]
		public event EventHandler DisplayDateChanged;

		/// <summary>
		/// Fires the DisplayDateChanged event.
		/// </summary>
		/// <param name="e">The event arg.</param>
		public void OnDisplayDateChanged(EventArgs e)
		{
			if (DisplayDateChanged != null)
				this.DisplayDateChanged(this, e);
		}

		#endregion

		#region Render

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this); 

			base.OnPreRender(e);

			if (!this.IsDesignMode)
			{
				if (this.Page != null)
				{
					this.Page.RegisterRequiresRaiseEvent(this);
					this.Page.RegisterRequiresPostBack(this);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected override void RenderContents(HtmlTextWriter writer)
		{
			base.RenderContents(writer);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.DesignMode)
			{
				if (!this.Visible)
					return;

				string cssClass = string.IsNullOrEmpty(this.CssClass) ? "" : (this.CssClass + " ");
				cssClass += Utils.GetHiddenClass();
				writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass); // hide markup during the client-side initialization.
			}				

			if (this.Page != null)
				this.Page.VerifyRenderingInServerForm(this);

			this.EnsureChildControls();
			base.Render(writer);
		}

		internal WebControl CreateNavButton(string cssClass, string imgClass)
		{
			HyperLink btnLink = new HyperLink();
			btnLink.NavigateUrl = "#";
			btnLink.CssClass = cssClass;

			HtmlGenericControl span = new HtmlGenericControl("span");
			span.Attributes["class"] = imgClass;

			btnLink.Controls.Add(span);

			return btnLink;
		}

		internal string GetTitleText(DateTime date)
		{
			try
			{
				if (date == null)
					date = this.GetDisplayDate();

				return date.ToString(this.TitleFormat, this.Culture.DateTimeFormat);
			}
			catch (FormatException)
			{
				throw new FormatException(C1Localizer.GetString("TitleFormat is invalid."));
			}
		}


		internal WebControl CreateTileText(DateTime date)
		{
			Panel txtPanel = new Panel();
			txtPanel.CssClass = "ui-datepicker-title wijmo-wijcalendar-title ui-state-default ui-corner-all";
			txtPanel.Controls.Add(new LiteralControl(this.GetTitleText(date)));

			return txtPanel;
		}

		internal WebControl CreateTitle(DateTime date, bool showPrevButtons, bool showNextButtons)
		{
			bool isRTL = false;
			if (this.NavButtons == Wijmo.Controls.C1Calendar.NavButtons.Quick)
			{
				Panel quickNavPanel = new Panel();
				quickNavPanel.CssClass = "ui-widget-header wijmo-wijcalendar-header ui-helper-clearfix ui-corner-all";

				Panel navPanel = new Panel();
				navPanel.CssClass = "ui-datepicker-header wijmo-wijcalendar-header-inner";

				if (showPrevButtons)
				{
					quickNavPanel.Controls.Add(this.CreateNavButton("wijmo-wijcalendar-navbutton ui-datepicker-prev ui-corner-all", "ui-icon ui-icon-seek-" + (isRTL ? "next" : "prev")));
					navPanel.Controls.Add(this.CreateNavButton("wijmo-wijcalendar-navbutton ui-datepicker-prev ui-corner-all", "ui-icon ui-icon-circle-triangle-" + (isRTL ? "e" : "w")));
				}

				quickNavPanel.Controls.Add(navPanel);
				navPanel.Controls.Add(this.CreateTileText(date));

				if (showNextButtons)
				{
					navPanel.Controls.Add(this.CreateNavButton("wijmo-wijcalendar-navbutton ui-datepicker-next ui-corner-all", "ui-icon ui-icon-circle-triangle-" + (isRTL ? "w" : "e")));
					quickNavPanel.Controls.Add(this.CreateNavButton("wijmo-wijcalendar-navbutton ui-datepicker-next ui-corner-all", "ui-icon ui-icon-seek-" + (isRTL ? "prev" : "next")));
				}

				return quickNavPanel;
			}
			else
			{
				Panel navPanel = new Panel();
				navPanel.CssClass = "ui-datepicker-header ui-widget-header ui-datepicker-header ui-helper-clearfix ui-corner-all";

				if (showPrevButtons)
				{
					navPanel.Controls.Add(this.CreateNavButton("wijmo-wijcalendar-navbutton ui-datepicker-prev ui-corner-all", "ui-icon ui-icon-circle-triangle-" + (isRTL ? "e" : "w")));
				}

				navPanel.Controls.Add(this.CreateTileText(date));

				if (showNextButtons)
				{
					navPanel.Controls.Add(this.CreateNavButton("wijmo-wijcalendar-navbutton ui-datepicker-next ui-corner-all", "ui-icon ui-icon-circle-triangle-" + (isRTL ? "w" : "e")));
				}

				return navPanel;
			}
		}

		private bool _isSelectable(DayType dayType) {
			return (dayType & (DayType.OutOfRange | DayType.Disabled)) == 0;
		}

		internal C1CssCell GetCellClassName(DayType dayType, DateTime date)
		{
			string cssCell = "";
			string cssText = "ui-state-default";
			bool  allowSelDay = (this.SelectionMode.Day || this.SelectionMode.Days);

			if (this.Enabled && allowSelDay && this._isSelectable(dayType)) {
				cssCell += " wijmo-wijcalendar-day-selectable";
			}

			if ((dayType & DayType.WeekEnd) != 0) {
				cssCell += " ui-datepicker-week-end";
			}
			if ((dayType & DayType.OtherMonth) != 0) {
				cssCell += " ui-datepicker-other-month";
				cssText += " ui-priority-secondary";
			}
			if ((dayType & DayType.OutOfRange) != 0) {
				cssCell += " wijmo-wijcalendar-outofrangeday";
				cssText += " ui-priority-secondary";
			}
			if ((dayType & DayType.Gap) != 0) {
				cssCell += " wijmo-wijcalendar-gap";
			} else {
				if ((dayType & DayType.Disabled) != 0) {
					cssCell += " ui-datepicker-unselectable";
					cssText += " ui-state-disabled";
				}
				if ((dayType & DayType.Today) != 0) {
					cssCell += " ui-datepicker-days-cell-over ui-datepicker-today";
					cssText += " ui-state-highlight";
				}
				if ((dayType & DayType.Selected) != 0 &&
				((dayType & (DayType.OutOfRange | DayType.Disabled)) == 0)) {
					cssCell += " ui-datepicker-current-day";
					cssText += " ui-state-active";
				}
				if ((dayType & DayType.Gap) != 0) {
					cssCell += " wijmo-wijcalendar-gap";
				}
				if ((dayType & DayType.Custom) != 0) {
					cssCell += " wijmo-wijcalendar-customday";
				}
			}

			return new C1CssCell(cssCell, cssText);
		}

		private C1MonthView GetMonthView(DateTime date) 
		{
			string monthID = this.GetMonthID(date);
			return this._monthViews[monthID] as C1MonthView;
		}

		private void ResetWidth()
		{
			this.Style.Remove(HtmlTextWriterStyle.Height);
			if (this.MonthCols > 1)
			{
				this.Style.Add(HtmlTextWriterStyle.Width, string.Format("{0}em", 17 * this.MonthCols));
			}
			else
			{
				this.Style.Remove(HtmlTextWriterStyle.Width);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();
			base.CreateChildControls();

			if (this.IsDesignMode)
			{
				this.CreateMonthViews();

				DateTime date = this.GetDisplayDate();
				C1MonthView mv;

				Panel calendar = new Panel();
				calendar.CssClass = "wijmo-wijcalendar ui-datepicker-inline ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" + (this.IsSingleMonth() ? "" : " ui-datepicker-multi");
				calendar.Style.Remove(HtmlTextWriterStyle.Height);
				if (this.MonthCols > 1)
				{
					calendar.Style.Add(HtmlTextWriterStyle.Width, string.Format("{0}em", 17 * this.MonthCols));
				}
				else
				{
					calendar.Style.Remove(HtmlTextWriterStyle.Width);
				}

				if (this.IsSingleMonth())
				{
					mv = this.GetMonthView(date);
					mv._showPreview = this.AllowPreview;
					calendar.Controls.Add(mv);

					if (this.AllowPreview)
					{
						Panel previewWrap = new Panel();
						previewWrap.CssClass = "wijmo-wijcalendar-preview-wrapper ui-helper-clearfix";
						previewWrap.Controls.Add(calendar);
						this.Controls.Add(previewWrap);
					}
					else 
					{
						this.Controls.Add(calendar);
					}
				}
				else
				{
					string width = string.Format("{0}%", 100 / this.MonthCols);
					for (int r = 0; r < this.MonthRows; r++)
					{
						for (int c = 0; c < this.MonthCols; c++)
						{
							Panel mp = new Panel();
							mp.CssClass = "ui-datepicker-group" + (c == 0 ? " ui-datepicker-group-first" : "") + (c == this.MonthCols - 1 ? " ui-datepicker-group-last" : "");
							mp.Style.Add(HtmlTextWriterStyle.Width, width);
							mv = this.GetMonthView(date);
							mv._showPreview = false;
							mp.Controls.Add(mv);
							calendar.Controls.Add(mp);

							date = date.AddMonths(1);
						}
					}

					this.Controls.Add(calendar);
				}
			}
		}


		#endregion

		#region Internal Properties and Methods

		private string GetMonthID(DateTime date) {
			return string.Format("{0}_{1}", date.Year, date.Month);
		}

		private void CreateMonthViews()
		{
			this._monthViews = new Hashtable();
			string monthID = "";
			C1MonthView mv;
			DateTime date = this.GetDisplayDate();
			for (int row = 0; row < this.MonthRows; row++) {
				for (int col = 0; col < this.MonthCols; col++) {
					monthID = this.GetMonthID(date);
					mv = new C1MonthView(this, date);
					this._monthViews[monthID] = mv;

					if (row == 0) {
						if (col == 0) {
							mv._isFirst = true;
						}

						if (col == this.MonthCols - 1) {
							mv._isLast = true;
						}
					}

					date = date.AddMonths(1);
				}
			}
			date = this.GetDisplayDate();
			monthID = this.GetMonthID(date);
			mv = this._monthViews[monthID] as C1MonthView;
			if (mv != null) {
				this._groupStartDate = mv.StartDate;
			}
			var count = this.MonthRows * this.MonthCols;
			if (count > 1) {
				date = date.AddMonths(count - 1);
				monthID = this.GetMonthID(date);
				mv = this._monthViews[monthID] as C1MonthView;
			}
			if (mv != null) {
				this._groupEndDate = mv.EndDate;
			}
		}

		private Control FindControlInternal(string id, ControlCollection controls)
		{
			if (controls == null)
				controls = this.Controls;

			foreach (Control ctrl in controls)
			{
				if (ctrl.ID == id)
					return ctrl;

				if (ctrl.Controls.Count > 0)
				{
					Control child = this.FindControlInternal(id, ctrl.Controls);
					if (child != null)
						return child;
				}
			}
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public override Control FindControl(string id)
		{
			this.EnsureChildControls();
			return this.FindControlInternal(id, this.Controls);
		}

		internal bool IsSingleMonth()
		{
			return this.MonthCols * this.MonthRows == 1;
		}

		internal DateTime GetDisplayDate()
		{
			DateTime date = this.DisplayDate;

			// if Display date is untouched, then use the current date
			if (date == new DateTime(1900, 1, 1))
				date = DateTime.Today;

			return date;
		}

		protected override void EnsureEnabledState()
		{
			return;
		}
		

		#endregion
	}
}
