﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.IO;
using System.Web.UI.HtmlControls;




namespace C1.Web.Wijmo.Controls.C1Calendar
{
	internal class C1CssCell
	{
		public string CssCell;
		public string CssText;

		public C1CssCell(string cssCell, string cssText)
		{
			CssCell = cssCell;
			CssText = cssText;
		}
	}

	internal class C1MonthView : WebControl
	{
		#region Fileds,

		private C1Calendar _owner;
		private DateTime _startDate;
		private DateTime _endDate;
		private DateTime _startDateInMonth;
		private DateTime _endDateInMonth;
		internal bool _isFirst = false;
		internal bool _isLast = false;
		internal bool _tableOnly = false;
		internal bool _showPreview = false;

		#endregion

		#region Constructor

		public C1MonthView(C1Calendar owner)
		{
			_owner = owner;

			DateTime date = _owner.DisplayDate;
			if (date == new DateTime(1900, 1, 1))
				date = DateTime.Today;

			this.ID = date.Year.ToString() + "_" + date.Month.ToString();
			this.CalcDates(date);
		}

		public C1MonthView(C1Calendar owner, DateTime displayDate)
		{
			_owner = owner;

			this.ID = displayDate.Year.ToString() + "_" + displayDate.Month.ToString();
			this.CalcDates(displayDate);
		}

		#endregion

		#region Properties

		public DateTime MonthDate
		{
			get { return _startDateInMonth; }
			set
			{
				this.CalcDates(value);
			}
		}


		public DateTime StartDate
		{
			get
			{
				return this._startDate;
			}
			
		}

		public DateTime EndDate
		{
			get
			{
				return this._endDate;
			}
			
		}


		#endregion

		#region Internal Properties

		internal System.Globalization.Calendar Calendar
		{
			get
			{
				return _owner.Culture.DateTimeFormat.Calendar;
			}
		}

		internal DateTimeFormatInfo DateTimeFormat
		{
			get
			{
				return _owner.Culture.DateTimeFormat;
			}
		}

		#endregion

		#region Render

		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if (!_tableOnly && this._owner.ShowTitle)
			{
				this.Controls.Add(_owner.CreateTitle(this._startDateInMonth, this._isFirst, this._isLast));
			}

			if (!_tableOnly && this._showPreview)
			{
				Panel prevBtn = new Panel();
				prevBtn.CssClass = "wijmo-wijcalendar-prevpreview-button";

				HyperLink prevLink = new HyperLink();
				prevLink.NavigateUrl = "#";
				prevLink.Text = "&#160;";
				prevLink.CssClass = "ui-icon ui-icon-grip-dotted-vertical";

				prevBtn.Controls.Add(prevLink);
				this.Controls.Add(prevBtn);
			}

			Table tblMonth = new Table();
			tblMonth.ID = this.ID;
			tblMonth.CssClass = "ui-datepicker-calendar wijmo-wijcalendar-table";

			if (_owner.ShowWeekDays)
			{
				TableHeaderRow tr = new TableHeaderRow();
				if (_owner.ShowWeekNumbers)
				{
					TableHeaderCell tc = new TableHeaderCell();
					tc.CssClass = "ui-datepicker-week-col wijmo-wijcalendar-monthselector";

					if (_owner.SelectionMode.Month)
					{
						HyperLink msLink = new HyperLink();
						msLink.CssClass = "ui-icon ui-icon-triangle-1-se";
						tc.Controls.Add(msLink);
					}
					else
                        tc.Controls.Add(new LiteralControl(this._owner.WeekString));

					tr.Controls.Add(tc);
				}

				DayOfWeek dayOfWeek = this.Calendar.GetDayOfWeek(this._startDate);
				for (int c = 0; c < _owner.DayCols; c++)
				{
					bool weekEnd = dayOfWeek == DayOfWeek.Sunday || dayOfWeek == DayOfWeek.Saturday;

					TableHeaderCell tc = new TableHeaderCell();
					tc.CssClass = "ui-datepicker-week-day" + (weekEnd ? " ui-datepicker-week-end" : "") + (_owner.SelectionMode.WeekDay ? " wijmo-wijcalendar-selectable" : "");

					HtmlGenericControl span = new HtmlGenericControl("span");
					span.InnerText = this.GetWeekDayText(dayOfWeek);

					tc.Controls.Add(span);
					tr.Controls.Add(tc);

					dayOfWeek = (DayOfWeek)((((int)dayOfWeek) + 1) % 7);
				}

				tblMonth.Controls.Add(tr);
			}

			DateTime weekDate = this._startDateInMonth;
			DateTime dt = this._startDate;
			for (int r = 0; r < _owner.DayRows; r++)
			{
				TableHeaderRow tr = new TableHeaderRow();
				if (_owner.ShowWeekNumbers)
				{
					TableHeaderCell tc = new TableHeaderCell();
					tc.CssClass = "ui-datepicker-week-col wijmo-wijcalendar-week-num" + (_owner.SelectionMode.WeekNumber ? " wijmo-wijcalendar-selectable" : "");
					int week = this.Calendar.GetWeekOfYear(weekDate, this.DateTimeFormat.CalendarWeekRule, this._owner.Culture.DateTimeFormat.FirstDayOfWeek);
					tc.Text = week.ToString();
					tr.Controls.Add(tc);

					weekDate = this.Calendar.AddDays(weekDate, this._owner.DayCols);
				}

				for (int c = 0; c < this._owner.DayCols; c++)
				{
					TableCell tc = new TableCell();
					this.FillDayCell(tc, dt);
					tr.Controls.Add(tc);
					dt = this.Calendar.AddDays(dt, 1);
				}

				tblMonth.Controls.Add(tr);
			}

			this.Controls.Add(tblMonth);

			if (!_tableOnly && this._showPreview)
			{
				Panel nextBtn = new Panel();
				nextBtn.CssClass = "wijmo-wijcalendar-nextpreview-button";

				HyperLink nextLink = new HyperLink();
				nextLink.NavigateUrl = "#";
				nextLink.Text = "&#160;";
				nextLink.CssClass = "ui-icon ui-icon-grip-dotted-vertical";

				nextBtn.Controls.Add(nextLink);
				this.Controls.Add(nextBtn);
			}
		}

		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}

		#endregion

		public string GetViewHtml()
		{
			StringBuilder sb = new StringBuilder();
			StringWriter sw = new StringWriter(sb);
			HtmlTextWriter writer = new HtmlTextWriter(sw);
			this.RenderControl(writer);

			return sb.ToString();
		}

		internal void Refresh()
		{
			this.CalcDates(this.MonthDate);
		}

		internal void CalcDates(DateTime date)
		{
			int daysInMonth = this.Calendar.GetDaysInMonth(date.Year, date.Month);
			_startDateInMonth = new DateTime(date.Year, date.Month, 1, this.Calendar);
			_endDateInMonth = this.Calendar.AddDays(_startDateInMonth, daysInMonth - 1);

			_startDate = this.GetWeekStartDate(_startDateInMonth);
			_endDate = this.Calendar.AddDays(_startDate, _owner.DayRows * _owner.DayCols - 1);
		}

		internal string GetWeekDayText(DayOfWeek day)
		{
			DateTimeFormatInfo formatInfo = this.DateTimeFormat;
			WeekDayFormat format = this._owner.WeekDayFormat;
			switch (format)
			{
				case WeekDayFormat.Full:
					return formatInfo.GetDayName(day);

				case WeekDayFormat.Abbreviated:
					return formatInfo.GetAbbreviatedDayName(day);

				case WeekDayFormat.FirstLetter:
					return formatInfo.GetDayName(day).Substring(0, 1);

				case WeekDayFormat.Short:
					return formatInfo.GetShortestDayName(day);
			}

			return formatInfo.GetAbbreviatedDayName(day);
		}

		internal DateTime GetWeekStartDate(DateTime date)
		{
			int offset = this.Calendar.GetDayOfWeek(date) - this.DateTimeFormat.FirstDayOfWeek;
			if (offset < 0) offset += 7;

			return Calendar.AddDays(date, -offset);
		}

		private bool Contains(DateTime date, DateTime[] dates)
		{
			foreach (DateTime d in dates) 
			{
				if (d.Year == date.Year && d.Month == date.Month && d.Day == date.Day) 
				{
					return true;
				}
			}

			return false;
		}

		private DayType GetDayType(DateTime date)
		{ 
			DayType dayType = DayType.General;
			int dow = date.Day;
			bool weekEnd = dow == 6 || dow == 0; // Saturday or Sunday
			bool outOfRange = date < this._owner.MinDate || date > this._owner.MaxDate;
			bool otherMonth = date < this._startDateInMonth || date > this._endDateInMonth;
			bool isDisabled = outOfRange || this.Contains(date, this._owner.DisabledDates);
			bool isSelected = this.Contains(date, this._owner.SelectedDates);
			DateTime today = new DateTime();
			bool isToday = today.Year == date.Year && today.Month == date.Month && today.Day == date.Day;
			bool isCustom = false;
			if (weekEnd) {
				dayType |= DayType.WeekEnd;
			}
			if (isToday) {
				dayType |= DayType.Today;
			}
			if (isDisabled) {
				dayType |= DayType.Disabled;
			}
			if (otherMonth) {
				dayType |= DayType.OtherMonth;
			}
			if (outOfRange) {
				dayType |= DayType.OutOfRange;
			}
			if (isSelected) {
				dayType |= DayType.Selected;
			}
			if (isCustom) {
				dayType |= DayType.Custom;
			}
			if (otherMonth && !this._owner.ShowOtherMonthDays) {
				dayType |= DayType.Gap;
			}
			return dayType;
		}

		private void FillDayCell(TableCell cell, DateTime date)
		{
			string text = date.ToString("%d", this._owner.Culture);;
			DayType dayType = this.GetDayType(date);
			C1CssCell css = this._owner.GetCellClassName(dayType, date);
			text = (this._owner.ShowDayPadding && text.Length == 1) ? '0' + text : text;

			cell.CssClass = css.CssCell;

			if ((dayType & DayType.Gap) != 0) {
				cell.Text = "&#160;";
			} else 
			{
				HyperLink link = new HyperLink();
				link.NavigateUrl = "#";
				link.CssClass = css.CssText;
				link.Text = text;

				cell.Controls.Add(link);
			}
		}
	}
}
