﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using System.Collections;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Base;
using C1.Web.Wijmo.Extenders;
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls;
using C1.Web.Wijmo.Controls.Localization;
#endif




#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1LightBox
#else
namespace C1.Web.Wijmo.Controls.C1LightBox
#endif
{
    /// <summary>
    /// The item of lightbox control.
    /// </summary>
    [ToolboxItem(false)]
    public class C1LightBoxItem : Settings 
    {

        #region ** fields

        private Hashtable _properties = new Hashtable();
        //private HtmlImage _image;
        //private HtmlGenericControl _link;

        #endregion

        #region ** constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1LightBoxItem"/> class.
        /// </summary>
        public C1LightBoxItem()
        {
        }

        #endregion

        #region ** properties

        /// <summary>
        /// Unique ID used for designer form.
        /// </summary>
        [DefaultValue("")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string ID
        {
            get
            {
                return _properties["ID"] == null ? "" : _properties["ID"].ToString();
            }
            set
            {
                _properties["ID"] = value;
            }
        }


        /// <summary>
        /// Image url of Lightbox item.
        /// </summary>
        [WidgetOption]
		[C1Description("C1LightBoxItem.ImageUrl", "A value that indicates the Image Url of the LightBox Item.")]
        [DefaultValue("")]
        [Editor("System.Web.UI.Design.ImageUrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
        [UrlProperty()]
        [C1Category("Category.Appearance")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string ImageUrl
        {
            get
            {
                return _properties["ImageUrl"] == null ? "" : _properties["ImageUrl"].ToString();
            }
            set
            {
                _properties["ImageUrl"] = value;
            }
        }

        /// <summary>
        /// Link url of Lightbox item.
        /// </summary>
        [WidgetOption]
		[C1Description("C1LightBoxItem.LinkUrl", "A value that indicates the Link Url of the LightBox Item.")]
        [Editor("System.Web.UI.Design.UrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
        [UrlProperty()]
        [DefaultValue("")]
        [C1Category("Category.Appearance")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string LinkUrl
        {
            get
            {
                return _properties["LinkUrl"] == null ? "" : _properties["LinkUrl"].ToString();
            }
            set
            {
                _properties["LinkUrl"] = value;
            }
        }

        /// <summary>
        /// Title of image.
        /// </summary>
		[C1Description("C1LightBoxItem.Title", "A value that indicates the Title Url of the LightBox Item.")]
        [DefaultValue("")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string Title
        {
            get
            {
                return _properties["Title"] == null ? "" : _properties["Title"].ToString();
            }
            set
            {
                _properties["Title"] = value;
            }
        }

        /// <summary>
        /// Text of image.
        /// </summary>
		[C1Description("C1LightBoxItem.Text", "A value that indicates the Text of the LightBox Item.")]
        [DefaultValue("")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string Text
        {
            get
            {
                return _properties["Text"] == null ? "" : _properties["Text"].ToString();
            }
            set
            {
                _properties["Text"] = value;
            }
        }

        /// <summary>
        /// Determines the name of player to host the content.
        /// </summary>
        [WidgetOption]
        [DefaultValue(PlayerName.AutoDetect)]
        [C1Description("C1LightBoxItem.Player", "Determines the name of player to host the content.")]
        public PlayerName Player
        {
            get
            {
                return _properties["Player"] == null ? PlayerName.AutoDetect : (PlayerName)_properties["Player"];
            }
            set
            {
                _properties["Player"] = value;
            }
        }

        /// <summary>
        /// A value that indicates the height of the content.
        /// </summary>
        [C1Description("C1LightBoxItem.Height", "A value that indicates the height of the content.")]
        [NotifyParentProperty(true)]
        [DefaultValue(0)]
#if !EXTENDER
        [Layout(LayoutType.Sizes)]
#endif
        public int Height
        {
            get
            {
                return _properties["Height"] == null ? 0 : (int)_properties["Height"];
            }
            set
            {
                _properties["Height"] = value;
            }
        }

        /// <summary>
        /// A value that indicates the width of the content.
        /// </summary>
        [C1Description("C1LightBoxItem.Width", "A value that indicates the width of the content.")]
        [NotifyParentProperty(true)]
        [DefaultValue(0)]
#if !EXTENDER
        [Layout(LayoutType.Sizes)]
#endif
        public int Width
        {
            get
            {
                return _properties["Width"] == null ? 0 : (int)_properties["Width"];
            }
            set
            {
                _properties["Width"] = value;
            }
        }


        #endregion end of ** properties.

        #region ** Override Methods

        ///// <summary>
        ///// CreateChildControls override.
        ///// </summary>
        //protected override void CreateChildControls()
        //{
        //    if (!string.IsNullOrEmpty(this.ImageUrl))
        //    {
        //        _image = new HtmlImage();
        //        _image.Src = base.ResolveClientUrl(this.ImageUrl);
        //        _image.Attributes.Add("title", this.Title);
        //        _image.Alt = this.Text;
        //    }

        //    if (!string.IsNullOrEmpty(this.LinkUrl))
        //    {
        //        _link = new HtmlGenericControl("a");
        //        _link.Attributes.Add("href", base.ResolveClientUrl(this.LinkUrl));
        //        this.Controls.Add(_link);
        //        if (_image != null)
        //            _link.Controls.Add(_image);
        //    }
        //    else if (_image != null)
        //    {
        //        this.Controls.Add(_image);
        //    }

        //    base.CreateChildControls();
        //}

        ///// <summary>
        ///// Renders the control to the specified HTML writer.
        ///// </summary>
        ///// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
        //protected override void Render(HtmlTextWriter writer)
        //{
        //    this.EnsureChildControls();
        //    base.Render(writer);
        //}

        ///// <summary>
        ///// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. 
        ///// This method is used primarily by control developers.
        ///// </summary>
        ///// <param name="writer">
        ///// A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.
        ///// </param>
        //protected override void AddAttributesToRender(HtmlTextWriter writer)
        //{
        //    //if (this.IsDesignMode)
        //    //{
        //    //    writer.AddAttribute(HtmlTextWriterAttribute.Class,
        //    //        string.Format("{0} {1}", CSS_ITEM, CSS_JQ_CLEARFIX));
        //    //    writer.AddStyleAttribute(HtmlTextWriterStyle.Height,
        //    //        this.Carousel.ItemHeight + "px");
        //    //    writer.AddStyleAttribute(HtmlTextWriterStyle.Width,
        //    //        this.Carousel.ItemWidth + "px");
        //    //}
        //    base.AddAttributesToRender(writer);
        //}

        #endregion
    }
}
