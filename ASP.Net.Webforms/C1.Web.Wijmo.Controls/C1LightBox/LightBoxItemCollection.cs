﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Localization;


namespace C1.Web.Wijmo.Controls.C1LightBox
{
    public class C1LightBoxItemCollection : List<C1LightBoxItem>, IJsonRestore
    {
        #region ** IJsonRestore interfact implementations
        void IJsonRestore.RestoreStateFromJson(object state)
        {
            Clear();

            ArrayList list = state as ArrayList;
            if (list != null)
            {
                foreach (Hashtable obj in list)
                {
                    C1LightBoxItem  item = new C1LightBoxItem();
                    ((IJsonRestore)item).RestoreStateFromJson(obj);
                    Add(item);
                }
            }
        }

        #endregion end of ** IJsonRestore interfact implementations.
    }
}
