﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;
using System.IO;
using System.Text.RegularExpressions;


namespace C1.Web.Wijmo.Controls.C1LightBox
{
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;
	using C1.Web.Wijmo.Controls.Licensing;
	using System.Collections.Specialized;


	[ControlValueProperty("DisplayDate")]
	[DefaultProperty("DisplayDate")]
	[ToolboxData("<{0}:C1LightBox runat=server></{0}:C1LightBox>")]
	[ToolboxBitmap(typeof(C1LightBox), "C1LightBox.png")]
	[ParseChildren(true)]
	[PersistChildren(false)]
	[LicenseProviderAttribute()]
	[Localizable(true)]
	[AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	[AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1LightBox.C1LightBoxDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1LightBox.C1LightBoxDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1LightBox.C1LightBoxDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1LightBox.C1LightBoxDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    public partial class C1LightBox : C1TargetControlBase, IPostBackDataHandler
	{
		#region Fields

		private bool _productLicensed = false;
        private C1LightBoxItemCollection _items = null;
        private bool _shouldNag;

		#endregion

		#region Constructor


		/// <summary>
		/// Construct a new instance of C1LightBox.
		/// </summary>
		[C1Description("C1LightBox.Constructor")]
		public C1LightBox()
			: base(HtmlTextWriterTag.Div)
		{
			VerifyLicense();
		}

		internal C1LightBox(string key)
			: base(HtmlTextWriterTag.Div)
		{
			this.VerifyLicense(key);
		}

		#endregion

		#region Licensing

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1LightBox), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Verifies the license.
		/// </summary>
		/// <param name="key">The key.</param>
		internal void VerifyLicense(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			// ttZzKzJ5mwNr/IihpBl2pA==
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1LightBox), this,
				Assembly.GetExecutingAssembly(), key);
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region Properties

        /// <summary>
        /// Gets a <see cref="C1LightBoxItemCollection"/> 
        /// object that contains the items of the current <see cref="C1LightBox"/> control.
        /// </summary>
        [C1Description("C1LightBox.Items")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [RefreshProperties(RefreshProperties.All)]
        [CollectionItemType(typeof(C1LightBoxItem))]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
		public C1LightBoxItemCollection Items
		{
            get
			{
				if (this._items == null)
				{
					this._items = new C1LightBoxItemCollection();
				}
				return this._items;
			}
		}

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region Override Methods

		///// <summary>
		///// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		///// </summary>
		///// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		//protected override void OnPreRender(EventArgs e)
		//{
		//    LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
		//    base.OnPreRender(e);
		//}

        private bool IsAbsoluteUrl(string url) { 
            if (!Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute)) 
            { 
                throw new ArgumentException("URL was in an invalid format", "url"); 
            } 
            return Uri.IsWellFormedUriString(url, UriKind.Absolute);
        } 

        private string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl)
        {
            return string.Format("{0}://{1}{2}{3}", Page.Request.Url.Scheme, Page.Request.Url.Host, 
                Page.Request.Url.Port == 80 ? string.Empty : ":" + Page.Request.Url.Port, Page.ResolveUrl(relativeUrl));
        }

        private string GetExt(string url) 
        {
            int q = url.IndexOf("?");
            if (q > -1)
            {
                url = url.Substring(0, q);
            }

            q = url.LastIndexOf(".");
            if (q > -1)
            {
                url = url.Substring(q + 1);
            }

            return url;
        }

        private bool IsFlashVideo(C1LightBoxItem item)
        {
            if (item.Player == PlayerName.Flv) return true;
            if (item.Player == PlayerName.AutoDetect)
            {
                if (this.Player == PlayerName.Flv) return true;

                string ext = this.GetExt(item.LinkUrl);
                return (ext == "flv" || ext == "m4v");
            }

            return false;
        }

		private HyperLink CreateLink(C1LightBoxItem item)
		{
			HyperLink link = new HyperLink();
			System.Web.UI.WebControls.Image img = null;
			if (!string.IsNullOrEmpty(item.ImageUrl))
            {
                //fixed bug 32082 by Daniel.He 2013/3/22
                img = new System.Web.UI.WebControls.Image();
				//img.Src = base.ResolveClientUrl(item.ImageUrl);
                img.Attributes.Add("src", base.ResolveClientUrl(item.ImageUrl));
				img.Attributes.Add("title", item.Title);
                img.Attributes.Add("alt", item.Text);
                //img.Alt = item.Text;
			}

			string rel = string.Format("wijlightbox[{0}]", this.ID);
			if (item.Width > 0) 
			{
				rel += string.Format(";width={0}", item.Width);
			}
            if (item.Height > 0)
            {
                rel += string.Format(";height={0}", item.Height);
            }

			link.Attributes.Add("rel", rel);

			if (!string.IsNullOrEmpty(item.LinkUrl))
			{
                if (!this.IsDesignMode && this.IsFlashVideo(item) && !this.IsAbsoluteUrl(item.LinkUrl))
                {
                    link.NavigateUrl = this.ConvertRelativeUrlToAbsoluteUrl(item.LinkUrl);
                }
                else
                {
                    link.NavigateUrl = item.LinkUrl;
                }
                
				
				if (img != null)
					link.Controls.Add(img);
			}

			return link;
		}

        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);

            base.OnPreRender(e);
        }


		/// <summary>
		/// CreateChildControls override.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			for (int i = 0; i < this.Items.Count; i++)
			{
				this.Controls.Add(this.CreateLink(this.Items[i]));
			}

			base.CreateChildControls();
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (this.IsDesignMode)
			{
				Unit width = this.Width;
				Unit height = this.Height;

				if (width.IsEmpty)
				{
					width = Unit.Percentage(100);
				}

				if (height.IsEmpty)
				{
					height = Unit.Pixel(40);
				}

				writer.AddStyleAttribute(HtmlTextWriterStyle.Width, width.ToString());
				writer.AddStyleAttribute(HtmlTextWriterStyle.Height, height.ToString());
			}

			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
            this.ChildControlsCreated = false;
			this.EnsureChildControls();
			base.Render(writer);
		}



		#endregion

        #region ** IPostBackDataHandler

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
            if (data.ContainsKey("width"))
            {
                data.Add("maxWidth", data["width"]);
                data.Remove("width");
            }
            if (data.ContainsKey("height"))
            {
                data.Add("maxHeight", data["height"]);
                data.Remove("height");
            }
            this.RestoreStateFromJson(data);

            return false;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        #endregion
    }

}
