﻿namespace C1.Web.Wijmo.Controls.C1Sparkline
{
	/// <summary>
	/// Supported Sparkline types.
	/// </summary>
	public enum SparklineType
	{
		/// <summary>
		/// Line chart
		/// </summary>
		Line,

		/// <summary>
		/// Area chart
		/// </summary>
		Area,

		/// <summary>
		/// Column chart
		/// </summary>
		Column
	}
}
