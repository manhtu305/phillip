﻿using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1Chart;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Sparkline
{
    /// <summary>
    /// Series styles of Sparkline.
    /// </summary>
    public class SparklineStyle : ChartStyle
    {
        /// <summary>
        /// Create an instance of SparklineStyle.
        /// </summary>
        public SparklineStyle()
        {
            NegStyle = new ChartStyle();
            ZeroStyle = new ChartStyle();
        }

        /// <summary>
        /// Styles for negative values.
        /// </summary>
        [C1Description("SparklineStyle.NegStyle")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        public ChartStyle NegStyle { get; set; }

        /// <summary>
        /// Styles for zero values.
        /// </summary>
        [C1Description("SparklineStyle.ZeroStyle")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        public ChartStyle ZeroStyle { get; set; }
    }
}
