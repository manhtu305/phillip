﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Linq;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1Sparkline
{
    using C1.Web.Wijmo.Controls.Localization;

    /// <summary>
    /// The data series of sparkline control.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class SparklineSeries : Settings
    {
        /// <summary>
        /// Create a SparklineSeries instance.
        /// </summary>
        public SparklineSeries()
        {
            SeriesStyle = new SparklineStyle();
            Data = new List<double>();
        }

        /// <summary>
        /// Get or set the array to use as a source for data that you can bind to the sparkline widget.
        /// </summary>
        [C1Description("C1Sparkline.Data")]
        [C1Category("Category.Data")]
        //[CollectionItemType(typeof(double))]
        [NotifyParentProperty(true)]
        [DefaultValue(null)]
        [WidgetOption]
        [TypeConverter(typeof(ListConverter<double>))]
        public List<double> Data { get; set; }

        /// <summary>
        /// Get or set the chart type of the data series.
        /// </summary>
        [C1Description("SparklineSeries.Type")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(SparklineType.Line)]
        [NotifyParentProperty(true)]
        public SparklineType Type
        {
            get { return GetPropertyValue("Type", SparklineType.Line); }
            set { SetPropertyValue("Type", value); }
        }

        /// <summary>
        /// Get or set the style of the data series.
        /// </summary>
        [C1Description("SparklineSeries.SeriesStyle")]
        [C1Category("Category.Style")]
        [WidgetOption]
        [DefaultValue("")]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public SparklineStyle SeriesStyle { get; set; }

        /// <summary>
        /// Get or set the name of the field for the series to bind to.
        /// </summary>
        [C1Description("SparklineSeries.Bind")]
        [C1Category("Category.Databindings")]
        [DefaultValue("")]
        [NotifyParentProperty(true)]
        public string Bind
        {
            get { return GetPropertyValue("Bind", string.Empty); }
            set { SetPropertyValue("Bind", value); }
        }
    }

    /// <summary>
    /// Type converter supporting comma-separated string form of list.
    /// </summary>
    /// <typeparam name="T">Item type of the list.</typeparam>
    public class ListConverter<T> : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return true;
            }
            if (destinationType == typeof(InstanceDescriptor))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                return FromString(value as string);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value is List<T>)
            {
                var list = value as List<T>;
                if (destinationType == typeof(string))
                {
                    return Stringify(list);
                }
                if (destinationType == typeof(InstanceDescriptor))
                {
                    return new InstanceDescriptor(GetType().GetMethod("FromString"),
                        new object[] { Stringify(list) }
                    );
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        private static string Stringify(List<T> list)
        {
            return string.Join(",", list.Select(i => i.ToString()).ToArray());
        }

        public static List<T> FromString(string value)
        {
            return value
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => (T)Convert.ChangeType(s.Trim(), typeof(T)))
                .ToList();
        }
    }
}