﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Sparkline", "wijmo")]

namespace C1.Web.Wijmo.Controls.C1Sparkline
{
    /// <summary>
    /// C1Sparkline is an inline minichart control.
    /// </summary>
    [ToolboxData("<{0}:C1Sparkline runat=server></{0}:C1Sparkline>")]
    [ToolboxBitmap(typeof(C1Sparkline), "C1Sparkline.png")]
    [LicenseProvider]
#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1Sparkline.C1SparklineDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1Sparkline.C1SparklineDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer(
        "C1.Web.Wijmo.Controls.Design.C1Sparkline.C1SparklineDesigner, C1.Web.Wijmo.Controls.Design.3" +
        C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [WidgetDependencies(
        typeof(WijSparkline)
        , "extensions.c1sparkline.js",
		ResourcesConst.C1WRAPPER_PRO
        )]
	public class C1Sparkline : C1TargetDataBoundControlBase, IPostBackDataHandler
    {
        #region Fields

        private bool _productLicensed;
        private bool _shouldNag;
        private readonly List<SparklineSeries> _seriesList = new List<SparklineSeries>();

        #endregion

        #region Constructor

        /// <summary>
        /// Construct a new instance of C1Sparkline.
        /// </summary>
        [C1Description("C1Sparkline.Constructor")]
        public C1Sparkline()
        {
            VerifyLicense();
            InitC1Sparkline();
        }

        private void InitC1Sparkline()
        {
            Width = Unit.Pixel(200);
            Height = Unit.Pixel(50);
            Animation = new SparklineAnimation();
        }

        #endregion

        #region Licensing

        private void VerifyLicense()
        {
            var licinfo = Util.Licensing.ProviderInfo.Validate(typeof(C1Sparkline), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #endregion

        #region Options

        /// <summary>
        /// Get or set the height of C1Sparkline.
        /// </summary>
        [C1Description("C1Sparkline.Height")]
        [C1Category("Category.Layout")]
        [DefaultValue(typeof(Unit), "50")]
        public override Unit Height
        {
            get { return base.Height; }
            set { base.Height = value; }
        }

        /// <summary>
        /// Get or set the width of C1Sparkline.
        /// </summary>
        [C1Description("C1Sparkline.Width")]
        [C1Category("Category.Layout")]
        [DefaultValue(typeof(Unit), "200")]
        public override Unit Width
        {
            get { return base.Width; }
            set { base.Width = value; }
        }

        /// <summary>
        /// Get or set whether to show the value axis.
        /// </summary>
        [C1Description("C1Sparkline.ValueAxis")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool ValueAxis
        {
            get { return GetPropertyValue("ValueAxis", false); }
            set { SetPropertyValue("ValueAxis", value); }
        }

        /// <summary>
        /// Center the value axis at the origin option setting value.
        /// </summary>
        /// <remarks>This option just works when ValueAxis is set to true.</remarks>
        [C1Description("C1Sparkline.Origin")]
        [C1Category("Category.Layout")]
        [WidgetOption]
        [DefaultValue(0d)]
        public double Origin
        {
            get { return GetPropertyValue("Origin", 0d); }
            set { SetPropertyValue("Origin", value); }
        }

        /// <summary>
        /// Get or set the maximum value of the sparkline.
        /// </summary>
        [C1Description("C1Sparkline.Max")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(null)]
        public double? Max
        {
            get { return GetPropertyValue("Max", default(double?)); }
            set { SetPropertyValue("Max", value); }
        }

        /// <summary>
        /// Get or set the minimum value of the sparkline.
        /// </summary>
        [C1Description("C1Sparkline.Min")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(null)]
        public double? Min
        {
            get { return GetPropertyValue("Min", default(double?)); }
            set { SetPropertyValue("Min", value); }
        }

        /// <summary>
        /// Specify the display style for the tooltip of the sparkline.
        /// </summary>
        [C1Description("C1Sparkline.TooltipStyle")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue("")]
        public string TooltipFormat
        {
            get { return GetPropertyValue("TooltipFormat", string.Empty); }
            set { SetPropertyValue("TooltipFormat", value); }
        }

        /// <summary>
        /// The animation option defines the animation effect and controls other aspects of the widget's animation, such as duration and easing.
        /// </summary>
        [C1Description("C1Sparkline.Animation")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public SparklineAnimation Animation
        {
            get { return GetPropertyValue("Animation", new SparklineAnimation()); }
            set { SetPropertyValue("Animation", value); }
        }

        /// <summary>
        /// An array of series objects that contain data values and labels to display in the chart.
        /// </summary>
        [C1Description("C1Sparkline.SeriesList")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [WidgetOption]
        [CollectionItemType(typeof(SparklineSeries))]
        public List<SparklineSeries> SeriesList
        {
            get { return _seriesList; }
        }

        #endregion

        #region Client events

        /// <summary>
        /// This event fires when the user moves the mouse pointer while it is over a sparkline.
        /// </summary>
        [C1Description("C1Sparkline.OnClientMouseMove")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent("e, data")]
        [DefaultValue("")]
        public string OnClientMouseMove
        {
            get { return GetPropertyValue("OnClientMouseMove", string.Empty); }
            set { SetPropertyValue("OnClientMouseMove", value); }
        }

        /// <summary>
        /// This event fires when the user clicks the sparkline.
        /// </summary>
        [C1Description("C1Sparkline.OnClientClick")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent("e, data")]
        [DefaultValue("")]
        public string OnClientClick
        {
            get { return GetPropertyValue("OnClientClick", string.Empty); }
            set { SetPropertyValue("OnClientClick", value); }
        }

        #endregion

        #region Render

        /// <summary>
        /// Binds data from the data source to the control.
        /// </summary>
        /// <param name="dataSource">The IEnumerable list of data returned from a PerformSelect method call.</param>
        protected override void PerformDataBinding(IEnumerable dataSource)
        {
            if (dataSource == null || IsDesignMode)
            {
                return;
            }

            var seriesDatas = SeriesList.Select(series => new List<double>()).ToList();
            var seriesCount = SeriesList.Count;
            foreach (var item in dataSource)
            {
                for (var index = 0; index < seriesCount; index++)
                {
                    var series = SeriesList[index];
                    var bind = series.Bind;
                    seriesDatas[index].Add(GetBindNumberData(item, bind));
                }
            }

            for (var index = 0; index < seriesCount; index++)
            {
                SeriesList[index].Data = seriesDatas[index];
            }
        }

        private static double GetBindNumberData(object data, string fieldName)
        {
            var value = data;
            if (!string.IsNullOrEmpty(fieldName))
            {
                var pdc = TypeDescriptor.GetProperties(data);
                var descr = pdc.Find(fieldName, true);
                if (descr == null)
                {
                    throw new InvalidOperationException("Cannot find the specified field.");
                }

                value = descr.GetValue(data);
            }

            if (value == null || value == DBNull.Value)
                throw new NullReferenceException("Cannot cast null to a double.");

            var typeCode = Type.GetTypeCode(value.GetType());
            switch (typeCode)
            {
                case TypeCode.Boolean:
                    return (bool)value ? 1 : 0;
                case TypeCode.Byte:
                    return (byte)value;
                case TypeCode.Char:
                    return (char)value;
                case TypeCode.DateTime:
                    throw new InvalidOperationException("Cannot cast a DateTime to a double.");
                case TypeCode.Decimal:
                    return (double)(decimal)value;
                case TypeCode.Double:
                    return (double)value;
                case TypeCode.Int16:
                    return (short)value;
                case TypeCode.Int32:
                    return (int)value;
                case TypeCode.Int64:
                    return (long)value;
                case TypeCode.Object:
                    throw new InvalidOperationException("Cannot cast a Object to a double.");
                case TypeCode.SByte:
                    return (sbyte)value;
                case TypeCode.Single:
                    return (float)value;
                case TypeCode.String:
                    return double.Parse((string)value);
                case TypeCode.UInt16:
                    return (ushort)value;
                case TypeCode.UInt32:
                    return (uint)value;
                case TypeCode.UInt64:
                    return (ulong)value;
                default:
                    throw new InvalidOperationException("Cannot cast to a double.");
            }
        }

        /// <summary>
        /// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value that corresponds to this Web server control. This property is used primarily by control developers.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// One of the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> enumeration values.
        /// </returns>
        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Div; }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">
        /// An <see cref="T:System.EventArgs"/> object that contains the event data.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }
            base.OnInit(e);
        }

        /// <summary>
        /// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }

        #endregion

		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			
		}
	}
}