﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Sparkline
{
    /// <summary>
    /// The animation option defines the animation effect and controls other aspects of the widget's animation, such as duration and easing.
    /// </summary>
    [ParseChildren(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class SparklineAnimation : Settings
    {
        /// <summary>
        /// A value that determines whether to show the animation. Set this option to false in order to disable easing.
        /// </summary>
        [C1Description("SparklineAnimation.Enabled")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        public bool Enabled
        {
            get
            {
                return GetPropertyValue("Enabled", true);
            }
            set
            {
                SetPropertyValue("Enabled", value);
            }
        }

        /// <summary>
        /// Sets the type of animation easing effect.
        /// </summary>
        [C1Description("SparklineAnimation.Easing")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("linear")]
        [NotifyParentProperty(true)]
        public string Easing
        {
            get
            {
                return GetPropertyValue("Easing", "linear");
            }
            set
            {
                SetPropertyValue("Easing", value);
            }
        }

        /// <summary>
        /// A value that indicates the duration for the animation.
        /// </summary>
        [C1Description("SparklineAnimation.Duration")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(2000)]
        [NotifyParentProperty(true)]
        public int Duration
        {
            get
            {
                return GetPropertyValue("Duration", 2000);
            }
            set
            {
                SetPropertyValue("Duration", value);
            }
        }
    }
}
