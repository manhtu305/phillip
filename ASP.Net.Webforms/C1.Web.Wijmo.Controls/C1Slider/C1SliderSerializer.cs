﻿#region File Header
//==============================================================================
//  FileName    :   C1SliderSerializer.cs
//
//  Description :   
//
//  Copyright (c) 2001 - 2008 GrapeCity, Inc.
//  All rights reserved.
//
//                                                          ... Jacky Qiu
//==============================================================================

//------------------------------------------------------------------------------
//  Update Log:
//
//  Status          Date            Name                    BUG-ID
//  ----------------------------------------------------------------------------
//  Created         2008/06/26      Jacky Qiu				None
//  Updated         2008/07/25      Jacky Qiu               Fix event's bug
//
//------------------------------------------------------------------------------
#endregion end of File Header.

using System;
using System.Collections.Generic;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Slider
{
	/// <summary>
	/// Serializes C1Slider Control.
	/// </summary>
	public class C1SliderSerializer : C1BaseSerializer<C1Slider, object, object>
	{
		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1SliderSerializer"/> class.
		/// </summary>
		/// <param name="obj">Serializable Object.</param>
		public C1SliderSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
