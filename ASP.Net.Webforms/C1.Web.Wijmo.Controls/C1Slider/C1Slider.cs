﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Localization;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1Slider
{
	/// <summary>
	/// Represents a slider in an ASP.NET Web page.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Slider.C1SliderDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Slider.C1SliderDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Slider.C1SliderDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Slider.C1SliderDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Slider runat=server ></{0}:C1Slider>")]
	[ToolboxBitmap(typeof(C1Slider), "C1Slider.png")]
	[LicenseProviderAttribute()]
	public partial class C1Slider : C1TargetControlBase, INamingContainer, IPostBackDataHandler, IC1Serializable
	{
		#region ** fields
		#region ** licensing
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion

		#region ** constant members

		private const string CONST_CSS_HORIZONTAL = "-horizontal";
		private const string CONST_CSS_VERTICAL = "-vertical";
		private const string CONST_CSS_HANDLE = "-handle";
		private const string CONST_CSS_CONTENT = "-content";
		private const string CONST_CSS_DECBUTTON = "-decbutton";
		private const string CONST_CSS_INCBUTTON = "-incbutton";
		private const string CONST_CSS_TRIANGLE = "-triangle-1";
		private const string CONST_CSS_W = "-w";
		private const string CONST_CSS_E = "-e";
		private const string CONST_CSS_N = "-n";
		private const string CONST_CSS_S = "-s";

		private const string CONST_CSS_UIICON = "ui-icon";
		private const string CONST_CSS_UIICONTRIANGLE_W = CONST_CSS_UIICON + CONST_CSS_TRIANGLE + CONST_CSS_W;
		private const string CONST_CSS_UIICONTRIANGLE_E = CONST_CSS_UIICON + CONST_CSS_TRIANGLE + CONST_CSS_E;
		private const string CONST_CSS_UIICONTRIANGLE_N = CONST_CSS_UIICON + CONST_CSS_TRIANGLE + CONST_CSS_N;
		private const string CONST_CSS_UIICONTRIANGLE_S = CONST_CSS_UIICON + CONST_CSS_TRIANGLE + CONST_CSS_S;

		private const string CONST_CSS_UICORNERALL = "ui-corner-all";
		private const string CONST_CSS_UISTATEDEFAULT = "ui-state-default";
		private const string CONST_CSS_UIWIDGET = "ui-widget";
		private const string CONST_CSS_UIWIDGETCONTENT = CONST_CSS_UIWIDGET + CONST_CSS_CONTENT;

		private const string CONST_CSS_WIJSLIDER = "wijmo-wijslider";
		private const string CONST_CSS_WIJSLIDERDECBUTTON = CONST_CSS_WIJSLIDER + CONST_CSS_DECBUTTON;
		private const string CONST_CSS_WIJSLIDERINCBUTTON = CONST_CSS_WIJSLIDER + CONST_CSS_INCBUTTON;

		private const string CONST_CSS_UISLIDER = "ui-slider";
		private const string CONST_CSS_UISLIDERHANDLE = CONST_CSS_UISLIDER + CONST_CSS_HANDLE;
		private const string CONST_CSS_UISLIDERHORIZONTAL = CONST_CSS_UISLIDER + CONST_CSS_HORIZONTAL;
		private const string CONST_CSS_UISLIDERVERTICAL = CONST_CSS_UISLIDER + CONST_CSS_VERTICAL;

		private const string CONST_DEFAULTSIZE = "200px";
		#endregion end of ** constant members.
		#endregion end of ** fields.

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1Slider"/> class.
		/// </summary>
		public C1Slider()
			: base()
		{
			//update for fixing bug 15804
			//this.Width = Unit.Parse("200");
			//this.Height = Unit.Parse("30");
			//end for bug 15804

			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Slider), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion end of ** constructor.

		#region ** properties

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Gets or sets the height of the slider.
		/// </summary>
		[WidgetOption()]
		[Layout(LayoutType.Sizes)]
		public override Unit Height
		{
			get
			{
				//update for fix bug 15804
				return base.Height;
				//if (base.Height == Unit.Empty)
				//{
				//    if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
				//    {
				//        return Unit.Parse("30");
				//    }
				//    else
				//    {
				//        return Unit.Parse("200");
				//    }
				//}
				//else 
				//{ 
				//    return base.Height;
				//}
				//end for bug 15804
				
			}
			set
			{
				if (base.Height != value)
				{
					base.Height = value;
					if (this.IsDesignMode)
					{
						this.ChildControlsCreated = false;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the width of the slider.
		/// </summary>
		[WidgetOption()]
		[Layout(LayoutType.Sizes)]
		public override Unit Width
		{
			get
			{
				//update for fix bug 15804
				return base.Width;
				//if (base.Width == Unit.Empty)
				//{
				//    if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
				//    {
				//        return Unit.Parse("200");
				//    }
				//    else
				//    {
				//        return Unit.Parse("30");
				//    }
				//}
				//else
				//{
				//    return base.Width;
				//}
				//end for bug 15804
			}
			set
			{
				if (base.Width != value)
				{
					base.Width = value;
					if (this.IsDesignMode)
					{
						this.ChildControlsCreated = false;
					}
				}
			}
		}

		/// <summary>
		/// Changed the type of  control's  tag.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		#endregion end of ** properties

		#region ** methods

		#region ** CreateChildControls override
		
		/// <summary>
		/// Create slider's elements.
		/// </summary>
		protected override void CreateChildControls()
		{
			if (this.IsDesignMode)
			{
				this.Controls.Clear();
				this.BackColor = Color.Transparent;
				this.Controls.Add(CreateDecreButton());
				this.Controls.Add(CreateUISlider());
				this.Controls.Add(CreateIncreButton());
			}
			base.CreateChildControls();
		}

		private Control CreateDecreButton()
		{
			HtmlAnchor decBtn = new HtmlAnchor();
			decBtn.Attributes["class"] = string.Format("{0} {1} {2}", CONST_CSS_WIJSLIDERDECBUTTON, CONST_CSS_UICORNERALL, CONST_CSS_UISTATEDEFAULT);// "wijmo-wijslider-decbutton ui-corner-all ui-state-default";

			HtmlGenericControl span = new HtmlGenericControl("span");
			span.Attributes["class"] = string.Format("{0} {1}", CONST_CSS_UIICON, (this.Orientation== Wijmo.Controls.Orientation.Horizontal? CONST_CSS_UIICONTRIANGLE_W : CONST_CSS_UIICONTRIANGLE_N));// "ui-icon ui-icon-triangle-1-w";
			decBtn.Controls.Add(span);

			//added for height settings at 2011/7/12
			if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
			{
				decBtn.Style[HtmlTextWriterStyle.Left] = "0px";
				if (this.Height.IsEmpty)
				{
					decBtn.Style[HtmlTextWriterStyle.Top] = "0px";
				}
				else
				{
					decBtn.Style[HtmlTextWriterStyle.Top] = this.Height.Value / 2 - 8 + "px";
				}
			}
			else
			{
				decBtn.Style[HtmlTextWriterStyle.Top] = "0px";
				if (this.Width.IsEmpty)
				{
					decBtn.Style[HtmlTextWriterStyle.Left] = "0px";
				}
				else
				{
					decBtn.Style[HtmlTextWriterStyle.Left] = this.Width.Value / 2 - 8 + "px";
				}
			}
			//end for height setting 

			return decBtn;
		}

		private Control CreateIncreButton()
		{
			HtmlAnchor incBtn = new HtmlAnchor();
			incBtn.Attributes["class"] = string.Format("{0} {1} {2}", CONST_CSS_WIJSLIDERINCBUTTON, CONST_CSS_UICORNERALL, CONST_CSS_UISTATEDEFAULT);// "wijmo-wijslider-incbutton ui-corner-all ui-state-default";

			//added for height settings at 2011/7/12
			//if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
			//{
			//    incBtn.Style[HtmlTextWriterStyle.Top] = "0px";
			//}
			if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
			{
				incBtn.Style["right"] = "0px";
				if (this.Height.IsEmpty)
				{
					incBtn.Style[HtmlTextWriterStyle.Top] = "0px";
				}
				else
				{
					incBtn.Style[HtmlTextWriterStyle.Top] = this.Height.Value / 2 - 8 + "px";
				}
			}
			else
			{
				incBtn.Style["bottom"] = "0px";
				if (this.Width.IsEmpty)
				{
					incBtn.Style[HtmlTextWriterStyle.Left] = "0px";
				}
				else
				{
					incBtn.Style[HtmlTextWriterStyle.Left] = this.Width.Value / 2 - 8 + "px";
				}
			}
			//end for height setting 

			HtmlGenericControl span = new HtmlGenericControl("span");
			span.Attributes["class"] = string.Format("{0} {1}", CONST_CSS_UIICON, (this.Orientation== Wijmo.Controls.Orientation.Horizontal? CONST_CSS_UIICONTRIANGLE_E: CONST_CSS_UIICONTRIANGLE_S));// "ui-icon ui-icon-triangle-1-e";
			incBtn.Controls.Add(span);

			return incBtn;
		}

		private Control CreateUISlider()
		{
			Panel slider = new Panel();
			
			if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
			{
				slider.Style[HtmlTextWriterStyle.Left] = "30px";
				//update for height setting at 2011/7/12
				//slider.Style[HtmlTextWriterStyle.Top] = "5px";
				if (!this.Width.IsEmpty)
				{
					slider.Width = Unit.Pixel((int)Math.Abs(this.Width.Value - 60));
				}
				else
				{
					//width:200-60
					slider.Width = Unit.Pixel((int)Math.Abs(140));
				}

				if (!this.Height.IsEmpty)
				{
					slider.Height = Unit.Pixel((int)Math.Abs(this.Height.Value));
				}
				//end at 2011/7/12

				slider.CssClass = string.Format("{0} {1} {2} {3} {4} {5}", 
					CONST_CSS_UISLIDER, CONST_CSS_UISLIDERHORIZONTAL, 
					CONST_CSS_UIWIDGET, CONST_CSS_UIWIDGETCONTENT,
					CONST_CSS_UICORNERALL, this.CssClass);// "ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all";
			}
			else
			{
				slider.Style[HtmlTextWriterStyle.Top] = "30px";
				//update for height setting at 2011/7/12
				//slider.Style[HtmlTextWriterStyle.Left] = "5px";
				if (!this.Height.IsEmpty)
				{
					slider.Height = Unit.Pixel((int)Math.Abs(this.Height.Value - 60));
				}
				else
				{
					//height : 200-60
					slider.Height = Unit.Pixel((int)Math.Abs(140));
				}

				if (!this.Width.IsEmpty)
				{
					slider.Width = Unit.Pixel((int)Math.Abs(this.Width.Value));
				}
				//end at 2011/7/12

				slider.CssClass = string.Format("{0} {1} {2} {3} {4} {5}", 
					CONST_CSS_UISLIDER, CONST_CSS_UISLIDERVERTICAL, 
					CONST_CSS_UIWIDGET, CONST_CSS_UIWIDGETCONTENT,
					CONST_CSS_UICORNERALL, this.CssClass);// "ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all";
			}

			HtmlAnchor thumb = new HtmlAnchor();
			thumb.Attributes["class"] = string.Format("{0} {1} {2}", CONST_CSS_UISLIDERHANDLE, CONST_CSS_UICORNERALL, CONST_CSS_UISTATEDEFAULT);// "ui-slider-handle ui-state-default ui-corner-all";
			slider.Controls.Add(thumb);

			return slider;
		}

		#endregion end of ** CreateChildControls override

		#region ** render methods

		/// <summary>
		/// Determine compound css class.
		/// </summary>
		/// <returns></returns>
		private string DetermineCompoundCssClass()
		{
			if (!this.IsDesignMode)
			{
				return Utils.GetHiddenClass();
			}

			string sCompoundCssClass = "";
			if (this.Orientation == Orientation.Horizontal)
			{
				sCompoundCssClass = CONST_CSS_WIJSLIDER + CONST_CSS_HORIZONTAL;
				if(this.IsDesignMode)
				{
					sCompoundCssClass += " " + CONST_CSS_UISLIDERHORIZONTAL;
				}
			}
			else if (this.Orientation == Orientation.Vertical)
			{
				sCompoundCssClass = CONST_CSS_WIJSLIDER + CONST_CSS_VERTICAL;
				if(this.IsDesignMode)
				{
					sCompoundCssClass += " " + CONST_CSS_UISLIDERVERTICAL;
				}
			}
			return sCompoundCssClass;
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified HtmlTextWriterTag.
		/// </summary>
		/// <param name="writer">A HtmlTextWriter that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			bool enabled = this.Enabled;
			this.Enabled = true;

			this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass);

			//update for fix bug 15804
			//Unit width = base.Width;
			//Unit height = base.Height;
			//base.Width = this.Width;
			//base.Height = this.Height;
			//end for bug 15804

			//update slider height settings at 2011/7/12
			if (this.Width.IsEmpty && this.Orientation == Orientation.Horizontal)
			{
				writer.AddStyleAttribute(HtmlTextWriterStyle.Width, CONST_DEFAULTSIZE);
			}

			if (this.Height.IsEmpty && this.Orientation == Orientation.Vertical)
			{
				writer.AddStyleAttribute(HtmlTextWriterStyle.Height, CONST_DEFAULTSIZE);
			}
			//end for slider height settings

			base.AddAttributesToRender(writer);

			//update for fix bug 15804
			//base.Width = width;
			//base.Height = height;
			//end for bug 15804

			this.CssClass = cssClass;
			this.Enabled = enabled;
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}
		
		#endregion end of ** render methods

		protected override void EnsureEnabledState()
		{
			return;
		}

		#endregion end of ** methods

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		/// <summary>
		/// Notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1SliderSerializer sz = new C1SliderSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1SliderSerializer sz = new C1SliderSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1SliderSerializer sz = new C1SliderSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1SliderSerializer sz = new C1SliderSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations
	}
}
