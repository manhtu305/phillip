﻿using System;
using System.IO;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base.Interfaces;

namespace C1.Web.Wijmo.Controls.C1FlipCard
{
    /// <summary>
    /// C1FlipCard is a panel of two sides that can flip between each other.
    /// </summary>
    [ToolboxData("<{0}:C1FlipCard runat=server></{0}:C1FlipCard>")]
    [ToolboxBitmap(typeof(C1FlipCard), "C1FlipCard.png")]
    [ParseChildren(true)]
    [PersistChildren(false)]
    [LicenseProvider]
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1FlipCard.C1FlipCardDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1FlipCard.C1FlipCardDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer("C1.Web.Wijmo.Controls.Design.C1FlipCard.C1FlipCardDesigner, C1.Web.Wijmo.Controls.Design.3" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    public partial class C1FlipCard : C1TargetControlBase, ICompositeControlDesignerAccessor, IPostBackDataHandler, IC1Serializable
    {
        #region Fields

        private C1FlipCardPane _frontPanel = new C1FlipCardPane();
        private C1FlipCardPane _backPanel = new C1FlipCardPane();
        private ITemplate _front;
        private ITemplate _back;
	    private bool _productLicensed;
        private bool _shouldNag;

        #endregion

        #region Constructor

        /// <summary>
        /// Construct a new instance of C1FlipCard.
        /// </summary>
        [C1Description("C1FlipCard.Constructor")]
        public C1FlipCard()
            : base(HtmlTextWriterTag.Div)
        {
	        VerifyLicense();

			Initialize();
        }

		private void Initialize()
	    {
		    Animation = new FlipCardAnimation();
		    Width = Unit.Pixel(160);
		    Height = Unit.Pixel(180);
	    }

	    #endregion

        #region Licensing

        private void VerifyLicense()
        {
            var licinfo = Util.Licensing.ProviderInfo.Validate(typeof(C1FlipCard), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #endregion

        #region Properties

		/// <summary>
		/// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[C1Description("C1FlipCard.AutoPostBack")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public virtual bool AutoPostBack
		{
			get
			{
				return this.GetPropertyValue<bool>("AutoPostBack", false);
			}
			set
			{
				this.SetPropertyValue<bool>("AutoPostBack", value);
			}
		}

		/// <summary>
		/// PostBack Event Reference. Empty string if AutoPostBack property is false. For internal use.
		/// </summary>
		[WidgetOption]
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string PostBackEventReference
		{
			get
			{
				return (Page == null || IsDesignMode || !AutoPostBack) ? string.Empty 
					: Page.ClientScript.GetPostBackEventReference(this, "SideChanged");
			}
		}

        /// <summary>
        /// A value that indicates the width of the wijflipcard widget.
        /// </summary>
        [C1Description("C1FlipCard.Width")]
        [C1Category("Category.Layout")]
        [WidgetOption]
        [DefaultValue(typeof(Unit), "160")]
        public override Unit Width
        {
            get
            {
                return base.Width;
            }
            set
            {
                base.Width = value;
            }
        }

        /// <summary>
        /// A value that indicates the height of the wijflipcard widget.
        /// </summary>
        [C1Description("C1FlipCard.Height")]
        [C1Category("Category.Layout")]
        [WidgetOption]
        [DefaultValue(typeof(Unit), "180")]
        public override Unit Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                base.Height = value;
            }
        }

        /// <summary>
        /// Get or set the template used to display the content of the front pane.
        /// </summary>
        [C1Category("Category.Templates")]
        [C1Description("C1FlipCard.FrontSide")]
		[Browsable(false)]
		[DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(C1FlipCardPane))]
        [TemplateInstance(TemplateInstance.Single)]
        [RefreshProperties(RefreshProperties.All)]
        [Layout(LayoutType.Misc)]
        public ITemplate FrontSide
        {
            get
            {
                return _front;
            }
            set
            {
                _front = value;
            }
        }

        /// <summary>
        /// Get or set the template used to display the content of the back pane.
        /// </summary>
        [C1Category("Category.Templates")]
        [C1Description("C1FlipCard.BackSide")]
		[Browsable(false)]
		[DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(C1FlipCardPane))]
        [TemplateInstance(TemplateInstance.Single)]
        [RefreshProperties(RefreshProperties.All)]
        [Layout(LayoutType.Misc)]
        public ITemplate BackSide
        {
            get
            {
                return _back;
            }
            set
            {
                _back = value;
            }
        }

        /// <summary>
        /// Front pane container
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public C1FlipCardPane FrontPanel
        {
            get { return _frontPanel ?? (_frontPanel = new C1FlipCardPane()); }
        }

        /// <summary>
        /// Back pane container
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public C1FlipCardPane BackPanel
        {
            get { return _backPanel ?? (_backPanel = new C1FlipCardPane()); }
        }

        /// <summary>
        /// Get or set the current side of the card.
        /// </summary>
        [C1Description("C1FlipCard.CurrentSide")]
        [C1Category("Category.Behavior")]
        [DefaultValue(FlipCardSide.Front)]
        [Json(true)]
        public FlipCardSide CurrentSide
        {
            get
            {
                return GetPropertyValue("CurrentSide", FlipCardSide.Front);
            }
            set
            {
                SetPropertyValue("CurrentSide", value);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Raised on post back if the side changed.
        /// </summary>
        [C1Description("C1FlipCard.SideChanged")]
        [C1Category("Category.Events")]
        public event EventHandler SideChanged;

        /// <summary>
        /// Raise the SideChanged event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSideChanged(EventArgs e)
        {
            if (SideChanged != null)
            {
                SideChanged(this, e);
            }
        }

        #endregion

        #region Render

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">
        /// An <see cref="T:System.EventArgs"/> object that contains the event data.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }
            base.OnInit(e);
            this.EnsureChildControls();
        }

        /// <summary>
        /// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }

        /// <summary>
        /// Overrides the method CreateChildControls in order to create the child controls.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (DesignMode)
            {
                CreateDefaultContentTemplateForDesign();
            }
            if (FrontSide != null)
            {
                FrontSide.InstantiateIn(FrontPanel);
            }
            if (BackSide != null)
            {
                BackSide.InstantiateIn(BackPanel);
            }

            Controls.Clear();
            Controls.Add(FrontPanel);
            Controls.Add(BackPanel);
        }

        private void CreateDefaultContentTemplateForDesign()
        {
            ISite site = GetSite(this);
            if (site != null)
            {
                IDesignerHost host = (IDesignerHost)site.GetService(typeof(IDesignerHost));
                if (host != null)
                {
                    if (FrontSide == null) FrontSide = ControlParser.ParseTemplate(host, " ");
                    if (BackSide == null) BackSide = ControlParser.ParseTemplate(host, " ");
                }
            }
        }

        private static ISite GetSite(Control webControl)
        {
            if (webControl.Site != null) return webControl.Site;
            for (var control = webControl.Parent; control != null; control = control.Parent)
            {
                if (control.Site != null) return control.Site;
            }
            return null;
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">
        /// A System.Web.UI.HtmlTextWriter that represents the output stream to render HTML content on the client.
        /// </param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);

            if (IsDesignMode)
            {
                var div = new HtmlGenericControl("div");
                div.Style["width"] = Width.ToString();
                div.Style["height"] = Height.ToString();
                div.Attributes["class"] = "ui-widget";
	            div.Controls.Add(CurrentSide == FlipCardSide.Front ? FrontPanel : BackPanel);
	            div.RenderControl(writer);
            }
        }

        #endregion

        #region C1 overrides

        #endregion

        #region IPostBackDataHandler members

        private const string PostbackFieldNameCurrentSide = "currentSide";

        bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            var data = JsonSerializableHelper.GetJsonData(postCollection);
            var side0 = CurrentSide;
            RestoreStateFromJson(data);
            var side = data[PostbackFieldNameCurrentSide].SafelyToString().ToEnum<FlipCardSide>();
            return side != side0;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            OnSideChanged(EventArgs.Empty);
        }

        #endregion

        # region IC1Serializable members

        /// <summary>
        /// Saves the control layout properties to the file.
        /// </summary>
        /// <param name="filename">The file where the values of the layout properties will be saved.</param> 
        public void SaveLayout(string filename)
        {
            C1FlipCardSerializer sz = new C1FlipCardSerializer(this);
            sz.SaveLayout(filename);
        }

        /// <summary>
        /// Saves control layout properties to the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be saved.</param> 
        public void SaveLayout(Stream stream)
        {
            C1FlipCardSerializer sz = new C1FlipCardSerializer(this);
            sz.SaveLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        public void LoadLayout(string filename)
        {
            LoadLayout(filename, LayoutType.All);
        }

        /// <summary>
        /// Load control layout properties from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
        public void LoadLayout(Stream stream)
        {
            LoadLayout(stream, LayoutType.All);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        public void LoadLayout(string filename, LayoutType layoutTypes)
        {
            C1FlipCardSerializer sz = new C1FlipCardSerializer(this);
            sz.LoadLayout(filename, layoutTypes);
        }

        /// <summary>
        /// Loads the control layout properties with specified types from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        public void LoadLayout(Stream stream, LayoutType layoutTypes)
        {
            C1FlipCardSerializer sz = new C1FlipCardSerializer(this);
            sz.LoadLayout(stream, layoutTypes);
        }

        #endregion

        #region ICompositeControlDesignerAccessor members

        public void RecreateChildControls()
        {
            CreateChildControls();
        }

        #endregion
    }
}