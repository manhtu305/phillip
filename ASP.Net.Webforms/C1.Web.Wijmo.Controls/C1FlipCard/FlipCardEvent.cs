﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1FlipCard
{
    /// <summary>
    /// Store the FlipCard callback result.
    /// </summary>
    public class FlipCardEventArgs : EventArgs
    {
        /// <summary>
        /// The input data.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// The result.
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// Construct an instance.
        /// </summary>
        public FlipCardEventArgs()
            : base()
        {
        }

        /// <summary>
        /// Construct an instance.
        /// </summary>
        /// <param name="data"></param>
        public FlipCardEventArgs(string data)
            : base()
        {
            Data = data;
        }
    }

    /// <summary>
    /// FlipCard callback event handler.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void FlipCardEventHandler(object sender, FlipCardEventArgs e);
}
