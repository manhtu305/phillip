﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Design;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1FlipCard
{
    /// <summary>
    /// Represents C1FlipCardSerializer which will serialize C1FlipCard.
    /// </summary>
    public class C1FlipCardSerializer : C1BaseSerializer<C1FlipCard, C1FlipCardPane, C1FlipCard>
    {
        /// <summary>
        /// Initializes a new instance of the C1FlipCardSerializer class..
        /// </summary>
        /// <param name="serializableObject">Object to serialize.</param>
        public C1FlipCardSerializer(object serializableObject) : base(serializableObject)
        {
        }

        /// <summary>
        /// Initializes a new instance of the C1FlipCardSerializer class.
        /// </summary>
        /// <param name="componentChangeService">IComponentChangeService</param>
        /// <param name="serializableObject">Object to serialize.</param>
        public C1FlipCardSerializer(IComponentChangeService componentChangeService, object serializableObject)
            : base(componentChangeService, serializableObject)
        {
        }
    }
}
