﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1FlipCard
{
    /// <summary>
    /// Represent one side of the card.
    /// </summary>
    [ToolboxItem(false)]
    public class C1FlipCardPane : Panel, INamingContainer
    {
    }
}
