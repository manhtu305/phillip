﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo;
using C1.Web.Wijmo.Controls.Base.Interfaces;

#endregion end of using

namespace C1.Web.Wijmo.Controls.C1Dialog
{
	/// <summary>
	/// Represents a dialog window.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Dialog.C1DialogDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Dialog.C1DialogDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Dialog.C1DialogDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Dialog.C1DialogDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Dialog runat=\"server\"></{0}:C1Dialog>")]
	[ToolboxBitmap(typeof(C1Dialog), "C1Dialog.png")]
	[ParseChildren(true)]
	[LicenseProvider()]
	public partial class C1Dialog : C1TargetControlBase, INamingContainer, 
		IPostBackDataHandler, IUrlResolutionService, IC1Serializable
	{
		#region ** fields

		#region const

		private const string CSS_JQ_WIDGET = "ui-widget";
		private const string CSS_JQ_CONTENT = "ui-widget-content";
		private const string CSS_JQ_CLEARFIX = "ui-helper-clearfix";
		private const string CSS_JQ_CORNER = "ui-corner-all";
		private const string CSS_JQ_HEADER = "ui-widget-header";
		private const string CSS_JQ_DIALOG = "ui-dialog";
		private const string CSS_JQ_DEFAULT = "ui-state-default";


		private const string CSS_C1HEADING = "ui-dialog-titlebar";
		private const string CSS_CAPTION_ICON_OUTER = "wijmo-wijdialog-captionbutton";
		private const string CSS_CAPTION_ICON = "ui-icon";
		private const string CSS_CAPTION_CLOSE = "ui-icon-close";
		private const string CSS_CAPTION_MAXIMIZER = "ui-icon-extlink";
		private const string CSS_CAPTION_MINIMIZER = "ui-icon-minus";
		private const string CSS_CAPTION_PINNER = "ui-icon-pin-w";
		private const string CSS_CAPTION_TOGGLER = "ui-icon-carat-1-n";
		private const string CSS_CAPTION_REFRESHER = "ui-icon-refresh";
		private const string CSS_CAPTION_TEXT = "ui-dialog-title";
		private const string CSS_CP_C1CONTENT = "ui-dialog-content";
		private const string CSS_CP_BUTTONPANE = "ui-dialog-buttonpane";
		private const string CSS_CP_BUTTONSET = "ui-dialog-buttonset";
		private const string CSS_CP_BUTTON = "ui-button";
		private const string CSS_CP_BUTTON_TEXTONLY = "ui-button-text-only";
		private const string CSS_CP_BUTTON_TEXT = "ui-button-text";
		//private const int HEADER_HEIGHT = 54; //in my test demo
		//private const int HEADER_HEIGHT = 35; //in wijmo sample site demo, different styles has different values.
		//private const int FOOTER_HEIGHT = 80; //in my test demo
		//private const int FOOTER_HEIGHT = 55; //in wijmo sample site demo, different styles has different values.
		private const string REGISTER_FUNC = "prepareDataForSubmit";
		private const string CSS_WIJ_DIALOG = "wjimo-wijdialog";

		#endregion

		#region html elements

		private HtmlGenericControl _c1Wrapper;
		private HtmlGenericControl _c1ContentPanel;
		private HtmlGenericControl _c1Heading;
		private HtmlGenericControl _c1textnode;
		internal HtmlGenericControl _closer;
		internal HtmlGenericControl _maximizer;
		internal HtmlGenericControl _minimizer;
		internal HtmlGenericControl _pinner;
		internal HtmlGenericControl _refresh;
		internal HtmlGenericControl _toggler;
		private HtmlGenericControl _buttonPane;

		#endregion

		#region licensing

		internal bool _productLicensed;
		private bool _shouldNag;

		#endregion

		#region other

		private ITemplate _content;
		private Hashtable _propertyStates = null;

		#endregion

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1Dialog"/> class.
		/// </summary>
		public C1Dialog()
		{
			VerifyLicense();
			this.InitWindow();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1Dialog"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public C1Dialog(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Dialog), this,
				Assembly.GetExecutingAssembly(), key);
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif

			this.InitWindow();
		}

		private void InitWindow()
		{
			CaptionButtons = new DialogCaptionButtons();
			CollapsingAnimation = new Animation();
			ExpandingAnimation = new Animation();
			
		}
		#endregion

		#region ** properties
		#region ** private properties
		private Hashtable PropertyStates
		{
			get
			{
				if (this._propertyStates == null)
				{
					this._propertyStates = new Hashtable();
				}

				return this._propertyStates;
			}
		}

		private int HeaderHeight
		{
			get {
				switch (this.Theme)
				{
					case "aristo":
						return 35;
					case "midnight":
						return 41;
					case "rocket":
						return 41;
					case "ui-lightness":
						return 35;
					default:
						return 35;

				}		
			}
		}

		private int FooterHeight
		{
			get
			{
				switch (this.Theme)
				{
					case "aristo":
						return 55;
					case "midnight":
						return 64;
					case "rocket":
						return 63;
					case "ui-lightness":
						return 55;
					default:
						return 55;

				}		
			}
		}
		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Gets or sets if dialog is Enabled.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Base.Enabled", "Indicates whether control is enabled.")]
		[Layout(LayoutType.Behavior)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		[DefaultValue(typeof(Unit), "300px")]
		[RefreshProperties(RefreshProperties.All)]
		[WidgetOption]
		[Layout(LayoutType.Sizes)]
		public override Unit Width
		{
			get
			{
				if (base.Width == Unit.Empty)
				{
					return Unit.Pixel(300);
				}
				return base.Width;
			}
			set
			{
				if (this.MinWidth > (int)value.Value)
				{
					base.Width = Unit.Pixel(this.MinWidth);
				}
				else
				{
					base.Width = value;
				}
			}
		}

		[DefaultValue(typeof(Unit), "150px")]
		[RefreshProperties(RefreshProperties.All)]
		[WidgetOption]
		[Layout(LayoutType.Sizes)]
		public override Unit Height
		{
			get
			{
				if (base.Height == Unit.Empty)
				{
					return Unit.Pixel(150);
				}
				return base.Height;
			}
			set
			{
				if (this.MinHeight > (int)value.Value)
				{
					base.Height = Unit.Pixel(this.MinHeight);
				}
				else
				{
					base.Height = value;
				}
			}
		}

		/// <summary>
		/// Auto Expand window.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Dialog.AutoExpand")]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public bool AutoExpand
		{
			get
			{
				return GetPropertyValue("AutoExpand", false);
			}
			set
			{
				SetPropertyValue("AutoExpand", value);
			}
		}

		/// <summary>
		/// A value indicates whether maintain the open state when post back.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use MaintainStatesOnPostback property instead.")]
		public bool MaintainVisibilityOnPostback
		{
			get
			{
				return this.MaintainStatesOnPostback;
			}
			set
			{
				this.MaintainStatesOnPostback = value;
			}
		}

		/// <summary>
		/// A value indicates the open state of the dialog.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use DialogStates property instead.")]
		public bool IsOpen
		{
			get
			{
				return this.DialogStates["isOpen"] == null ? 
					false : 
					(bool)this.DialogStates["isOpen"];
			}
			set
			{
				this.DialogStates["isOpen"] = value;
			}
		}

		/// <summary>
		/// A value indicates whether maintain the states when post back.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Dialog.MaintainVisibilityOnPostback")]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public bool MaintainStatesOnPostback
		{
			get
			{
				return GetPropertyValue("MaintainStatesOnPostback", false);
			}
			set
			{
				SetPropertyValue("MaintainStatesOnPostback", value);
			}
		}		

		/// <summary>
		/// A value indicates the open state of the dialog.
		/// </summary>
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		public Hashtable DialogStates
		{
			get
			{
				return GetPropertyValue <Hashtable>("DialogStates", null);
			}
			set
			{
				SetPropertyValue <Hashtable>("DialogStates", value);
			}
		}

		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		public string UpdatePanelID 		
		{

			get
			{
				return GetPropertyValue<string>("UpdatePanelID", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("UpdatePanelID", value);
			}
		}

		/// <summary>
		/// Gets or sets the template for the content area of the <see cref="C1Dialog"/> control. 
		/// </summary>
		/// <remarks>
		/// Use the ContentTemplate property to control the contents of the dialog window. 
		/// 
		/// To specify a template for the dialog, reset the <see cref="ContentUrl"/> property, place the &lt;ContentTemplate&gt; tags between the opening and closing tags of the <seealso cref="C1Window"/> control. 
		/// You can then list the contents of the template between the opening and closing &lt;ContentTemplate&gt; tags.
		/// </remarks>
		[Browsable(false)]
		[DefaultValue(null)]
		[C1Description("C1Dialog.Content")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		[Layout(LayoutType.Behavior)]
		public ITemplate Content
		{
			get
			{
				return _content;
			}
			set
			{
				_content = value;
			}
		}

		#endregion

		#region ** methods

		#endregion

		#region ** override
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
			EnsureChildControls();
		}

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified
		/// System.Web.UI.HtmlTextWriterTag. This method is used primarily by control
		/// developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			EnsureID();
			string cssClass = this.CssClass;
			if (!IsDesignMode)
			{
				//writer.AddAttribute(HtmlTextWriterAttribute.Title, this.Title);


				this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass);
			}
			// hide markup during the client-side initialization.
			//else
			//{
			//    string oldCssName = CssClass;
			//    CssClass = "";//CSS_JQ_DIALOG;
			//    if (!oldCssName.Equals(string.Empty))
			//    {
			//        CssClass = string.Format("{0} {1}", CssClass, oldCssName);
			//    }
			//}
			if (this.AutoExpand == true)
			{
				this.Height = Unit.Empty;
			}
			base.AddAttributesToRender(writer);
			this.CssClass = cssClass;
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for
		/// posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if (this.IsDesignMode)
			{
				string css = string.Format("{0} {1} {2} {3} {4}",
				CSS_JQ_DIALOG, CSS_JQ_WIDGET, CSS_JQ_CONTENT, 
				CSS_JQ_CORNER, CSS_WIJ_DIALOG);

				_c1Wrapper = BuildHtmlElement(css);
				SetDialogBound();
				_c1Wrapper.Controls.Add(CreateDialogHeader());
				_c1Wrapper.Controls.Add(CreateDialogContent());
				if (Buttons.Count > 0)
				{
					_c1Wrapper.Controls.Add(CreateDialogButtons());
				}
				SetDialogContentBound();
				this.Controls.Add(_c1Wrapper);
			}
			else if (Content != null && String.IsNullOrEmpty(this.ContentUrl))
			{
				Content.InstantiateIn(this);
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
			if (!IsDesignMode)
			{
				this.Page.RegisterRequiresPostBack(this);
			}

			// find the updatepabel control
			Control c = this;
			while (c.Parent != null)
			{
				if (c.Parent is UpdatePanel) 
				{
					this.UpdatePanelID = c.Parent.ClientID;
					break;
				}
				c = c.Parent;
			}
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">
		/// The System.Web.UI.HtmlTextWriter object that receives the control content.
		/// </param>
		protected override void Render(HtmlTextWriter writer)
		{
			EnsureChildControls();
			base.Render(writer);
		}

		///// <summary>
		///// RenderContents override
		///// </summary>
		///// <param name="writer">HtmlTextWriter</param>
		//protected override void RenderContents(HtmlTextWriter writer)
		//{
		//    base.RenderContents(writer);
		//}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(REGISTER_FUNC);
		}

		#endregion

		#region ** private methods

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Dialog), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		//private string GetDialogStyle()
		//{
		//    string css = string.Format("{0} {1} {2} {3} {4}",
		//        CSS_JQ_DIALOG, CSS_JQ_WIDGET, CSS_JQ_CONTENT, 
		//        CSS_JQ_CORNER, CSS_WIJ_DIALOG);
		//    return css; ;
		//}

		///// <summary>
		///// add invisible css to a control
		///// </summary>
		///// <param name="c">target html control</param>
		///// <param name="visible">visible or not</param>
		//private static void SetControl(HtmlControl c, bool visible)
		//{
		//    c.Visible = visible;
		//}

		
		/// <summary>
		/// Create Header Controls for web dialog
		/// </summary>
		private Control CreateDialogHeader()
		{
			_c1Heading = BuildHtmlElement(string.Format("{0} {1} {2} {3}",
				CSS_C1HEADING, CSS_JQ_HEADER, CSS_JQ_CORNER, CSS_JQ_CLEARFIX));
			_c1textnode = BuildHtmlElement(CSS_CAPTION_TEXT, "span");
			_c1textnode.InnerText = this.Title;
			_c1Heading.Controls.Add(_c1textnode);

			if (CaptionButtons.Pin.Visible)
			{
				_pinner = BuildCaptionButton(CSS_CAPTION_ICON_OUTER, string.Format("{0} {1}", CSS_CAPTION_ICON, CSS_CAPTION_PINNER));
				_c1Heading.Controls.Add(_pinner);
			}
			if (CaptionButtons.Refresh.Visible)
			{
				_refresh = BuildCaptionButton(CSS_CAPTION_ICON_OUTER, string.Format("{0} {1}", CSS_CAPTION_ICON, CSS_CAPTION_REFRESHER));
				_c1Heading.Controls.Add(_refresh);
			}

			if (CaptionButtons.Toggle.Visible)
			{
				_toggler = BuildCaptionButton(CSS_CAPTION_ICON_OUTER, string.Format("{0} {1}", CSS_CAPTION_ICON, CSS_CAPTION_TOGGLER));
				_c1Heading.Controls.Add(_toggler);
			}

			if (CaptionButtons.Minimize.Visible)
			{
				_minimizer = BuildCaptionButton(CSS_CAPTION_ICON_OUTER, string.Format("{0} {1}", CSS_CAPTION_ICON, CSS_CAPTION_MINIMIZER));
				_c1Heading.Controls.Add(_minimizer);
			}

			if (CaptionButtons.Maximize.Visible)
			{
				_maximizer = BuildCaptionButton(CSS_CAPTION_ICON_OUTER, string.Format("{0} {1}", CSS_CAPTION_ICON, CSS_CAPTION_MAXIMIZER));
				_c1Heading.Controls.Add(_maximizer);
			}

			if (CaptionButtons.Close.Visible)
			{
				_closer = BuildCaptionButton(CSS_CAPTION_ICON_OUTER, string.Format("{0} {1}", CSS_CAPTION_ICON, CSS_CAPTION_CLOSE));
				_c1Heading.Controls.Add(_closer);
			}

			return _c1Heading;
		}

		/// <summary>
		/// Create Content Controls for web dialog
		/// </summary>
		private Control CreateDialogContent()
		{
			_c1ContentPanel = BuildHtmlElement(string.Format("{0} {1}", CSS_CP_C1CONTENT, CSS_JQ_CONTENT));
			if (IsDesignMode)
			{
				_c1ContentPanel.Attributes.Add("height", "100%");
				_c1ContentPanel.Attributes.Add("DesignerRegionAttributeName", "0");
				if (!String.IsNullOrEmpty(this.ContentUrl))
				{
					HtmlGenericControl iframe = BuildHtmlElement("", "iframe");
					iframe.Attributes.Add("src", this.ContentUrl);
					_c1ContentPanel.Controls.Add(iframe);
				}
				else if (Content != null)
				{
					Content.InstantiateIn(_c1ContentPanel);
				}
			}
			return _c1ContentPanel;
		}

		/// <summary>
		/// Create dialog footer controls
		/// </summary>
		private Control CreateDialogButtons()
		{
			_buttonPane = BuildHtmlElement(string.Format("{0} {1} {2}"
				, CSS_CP_BUTTONPANE, CSS_JQ_CONTENT, CSS_JQ_CLEARFIX));
			HtmlGenericControl buttonSet = BuildHtmlElement(CSS_CP_BUTTONSET);
			foreach (DialogButton btn in Buttons)
			{
				HtmlGenericControl button = BuildHtmlElement(string.Format(
					"{0} {1} {2} {3} {4}"
					, CSS_CP_BUTTON, CSS_JQ_WIDGET, CSS_JQ_DEFAULT, CSS_JQ_CORNER, CSS_CP_BUTTON_TEXTONLY),
					"button");
				HtmlGenericControl text = BuildHtmlElement(CSS_CP_BUTTON_TEXT, "span");
				text.InnerHtml = btn.Text;
				button.Controls.Add(text);
				buttonSet.Controls.Add(button);
			}
			_buttonPane.Controls.Add(buttonSet);
			return _buttonPane;
		}

		/// <summary>
		/// instantiating a caption button HtmlGenericControl
		/// </summary>
		/// <param name="anthorClass">anthor class</param>
		/// <param name="spanClass">class of the icon</param>
		/// <returns></returns>
		private static HtmlGenericControl BuildCaptionButton(string anthorClass, string spanClass)
		{
			HtmlGenericControl anthor = BuildHtmlElement(anthorClass, "a");
			HtmlGenericControl span = BuildHtmlElement(spanClass, "span");
			anthor.Controls.Add(span);
			return anthor;
		}

		/// <summary>
		/// instantiating a div HtmlGenericControl
		/// </summary>
		/// <param name="cssClass"></param>
		/// <returns></returns>
		private static HtmlGenericControl BuildHtmlElement(string cssClass)
		{
			return BuildHtmlElement(cssClass, "div");
		}

		/// <summary>
		/// instantiating a div HtmlGenericControl
		/// </summary>
		/// <param name="cssClass"></param>
		/// <param name="tag"></param>
		/// <returns></returns>
		private static HtmlGenericControl BuildHtmlElement(string cssClass, string tag)
		{
			HtmlGenericControl div = new HtmlGenericControl(tag);

			if (!String.IsNullOrEmpty(cssClass))
			{
				div.Attributes.Add("class", cssClass);
			}
			if (tag.ToLower() == "a")
			{
				div.Attributes.Add("href", "#");
			}

			return div;
		}


		/// <summary>
		/// Set window design time content area bound.
		/// </summary>
		private void SetDialogBound()
		{
			//string width = Width.ToString() + "px";
			//string height = Height == 0
			//    ? "auto"
			//    : Height.ToString() + "px";
			if (this._c1Wrapper != null)
			{
				this._c1Wrapper.Style.Add(HtmlTextWriterStyle.Width, Width.ToString());
				if (!this.AutoExpand)
				{
					this._c1Wrapper.Style.Add(HtmlTextWriterStyle.Height, Height.ToString());
				}
				this._c1Wrapper.Style.Add(HtmlTextWriterStyle.Position,
					"relative");//to replace "absolute" in cssClass "ui-dialog"
			}
		}

		/// <summary>
		/// Set window design time content area bound.
		/// </summary>
		private void SetDialogContentBound()
		{
			if (!this.AutoExpand)
			{
				int contentHeight = ((int)Height.Value) - this.HeaderHeight;
				if (_buttonPane != null)
				{
					contentHeight = contentHeight - this.FooterHeight;
				}
				_c1ContentPanel.Style.Add(HtmlTextWriterStyle.OverflowY, "auto");
				_c1ContentPanel.Style.Add(HtmlTextWriterStyle.Height, Unit.Pixel(contentHeight).ToString());
			}
		}

		/// <summary>
		/// Load Json properties from data posted back from client
		/// </summary>
		/// <param name="jsonData">json Data</param>
		/// <returns></returns>
		private bool LoadDialogPropertiesFromJsonData(Hashtable jsonData)
		{
			if (jsonData == null || jsonData.Count == 0)
			{
				return false;
			}

			return false;
		}

		#endregion

		#region ** viewState management

		///// <summary>
		///// Loads the server control's previously saved view state to C1Window object.
		///// </summary>
		///// <param name="savedState">An System.Object that contains the saved view state values for the control.</param>
		//protected override void LoadViewState(object savedState)
		//{
		//    if (savedState != null)
		//    {
		//        object[] stateObj = (object[])savedState;

		//        base.LoadViewState(stateObj[0]);
		//        ((IStateManager)CaptionButtons).LoadViewState(stateObj[1]);
		//    }
		//}

		///// <summary>
		///// Saves the changes to a server control's view state to an Object.
		///// </summary>
		///// <returns>The Object that contains the view state changes.</returns>
		//protected override object SaveViewState()
		//{
		//    //return base.SaveViewState();
		//    object[] arr = new object[2];
		//    arr[0] = base.SaveViewState();
		//    if (!this.Visible)
		//    {
		//        arr[1] = ((IStateManager)this.CaptionButtons).SaveViewState();
		//    }
		//    return arr;
		//}

		///// <summary>
		/////  Causes the control to track changes to its view state so they can be stored in the object's System.Web.UI.Control.ViewState property.
		///// </summary>
		//protected override void TrackViewState()
		//{
		//    base.TrackViewState();

		//    if (!this.Visible)
		//    {
		//        ((IStateManager)this.CaptionButtons).TrackViewState();
		//    }
		//}

		#endregion

		#region ** IPostBackDataHandler

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}

		#endregion

		#region ** IUrlResolutionService

		string IUrlResolutionService.ResolveClientUrl(string url)
		{
			return ResolveClientUrl(url);
		}

		#endregion

		#region ** IC1Serializable

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1DialogSerializer sz = new C1DialogSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1DialogSerializer sz = new C1DialogSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1DialogSerializer sz = new C1DialogSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1DialogSerializer sz = new C1DialogSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}

		#endregion
	}
}
