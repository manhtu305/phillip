﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;


namespace C1.Web.Wijmo.Controls.C1Dialog
{
	public partial class DialogCaptionButtons
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DialogCaptionButtons"/> class.
		/// </summary>
		public DialogCaptionButtons()
		{
			this.Pin = new DialogCaptionButton("ui-icon-pin-w", "ui-icon-pin-s");
			this.Maximize = new DialogCaptionButton("ui-icon-extlink","");
			this.Minimize = new DialogCaptionButton("ui-icon-minus","");
			this.Refresh = new DialogCaptionButton("ui-icon-refresh","");
			this.Toggle = new DialogCaptionButton();
			this.Close = new DialogCaptionButton("ui-icon-close","");
		}

		//protected override void TrackViewState()
		//{
		//    base.TrackViewState();

		//    ((IStateManager)this.Pin).TrackViewState();
		//    ((IStateManager)this.Refresh).TrackViewState();
		//    ((IStateManager)this.Toggle).TrackViewState();
		//    ((IStateManager)this.Minimize).TrackViewState();
		//    ((IStateManager)this.Maximize).TrackViewState();
		//    ((IStateManager)this.Close).TrackViewState();

		//}

		//protected override void LoadViewState(object savedState)
		//{
		//    if (savedState != null)
		//    {
		//        object[] states = (object[])savedState;
		//        int len = states.Length;

		//        if (len > 0 && states[0] != null)
		//        {
		//            base.LoadViewState(states[0]);
		//        }

		//        if (len > 1 && states[1] != null)
		//        {
		//            ((IStateManager)this.Pin).LoadViewState(states[1]);
		//        }
		//        if (len > 2 && states[2] != null)
		//        {
		//            ((IStateManager)this.Refresh).LoadViewState(states[1]);
		//        }

		//        if (len > 3 && states[3] != null)
		//        {
		//            ((IStateManager)this.Toggle).LoadViewState(states[1]);
		//        }

		//        if (len > 4 && states[4] != null)
		//        {
		//            ((IStateManager)this.Minimize).LoadViewState(states[1]);
		//        }

		//        if (len > 5 && states[5] != null)
		//        {
		//            ((IStateManager)this.Maximize).LoadViewState(states[1]);
		//        }

		//        if (len > 6 && states[6] != null)
		//        {
		//            ((IStateManager)this.Close).LoadViewState(states[1]);
		//        }
		//    }
		//}

		//protected override object SaveViewState()
		//{
		//    object[] states = new object[2];
		//    states[0] = base.SaveViewState();

		//    states[1] = ((IStateManager)this.Pin).SaveViewState();
		//    states[2] = ((IStateManager)this.Refresh).SaveViewState();
		//    states[3] = ((IStateManager)this.Toggle).SaveViewState();
		//    states[4] = ((IStateManager)this.Minimize).SaveViewState();
		//    states[5] = ((IStateManager)this.Maximize).SaveViewState();
		//    states[6] = ((IStateManager)this.Close).SaveViewState();

		//    return states;
		//}
	}

}
