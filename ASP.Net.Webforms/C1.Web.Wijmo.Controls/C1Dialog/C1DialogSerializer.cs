﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using System.Web.UI;
using System.Web;
using System.Globalization;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.C1Dialog
{
    internal class C1DialogSerializer : C1BaseSerializer<C1Dialog, object, object>
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="C1DialogSerializer"/> class.
        /// </summary>
        /// <param name="serializableObject">Serializable Object.</param>
        public C1DialogSerializer(object serializableObject) : base(serializableObject)
        {
        }

        /// <summary>
		/// Initializes a new instance of the <see cref="C1DialogSerializer"/> class.
        /// </summary>
        /// <param name="componentChangeService"></param>
        /// <param name="serializableObject"></param>
		public C1DialogSerializer(IComponentChangeService componentChangeService, object serializableObject)
			: base(componentChangeService, serializableObject)
        {
        }
    }
}