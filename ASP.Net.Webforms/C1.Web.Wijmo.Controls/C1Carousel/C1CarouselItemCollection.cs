﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Carousel
{
	/// <summary>
	/// Represents the structure that manages the <see cref="C1CarouselItem"/> items.
	/// </summary>
	public class C1CarouselItemCollection : C1ObservableItemCollection<C1Carousel, C1CarouselItem>
	{
		
		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1CarouselItemCollection"/> class.
		/// </summary>
		/// <param name="owner">Parent ComboBox.</param>
		public C1CarouselItemCollection(C1Carousel owner)
			: base(owner)
		{

		}

		/// <summary>
		/// Adds <see cref="C1CarouselItem"/> node to the collection.
		/// </summary>
		/// <param name="item">item to add.</param>
		public new void Add(C1CarouselItem item)
		{
			if (item != null)
			{
				Insert(base.Count, item);
			}
		}

		/// <summary>
		/// Insert a <see cref="C1CarouselItem"/> item into a specific position in the collection.
		/// </summary>
		/// <param name="index">Item position. Value should be greater or equal to 0</param>
		/// <param name="child">The new <see cref="C1CarouselItem"/> item.</param>
		public new void Insert(int index, C1CarouselItem child)
		{
			if (this.Count < index)
				this.InsertItem(this.Count, child);
			else
				this.InsertItem(index, child);
		}

		#endregion end of ** constructor
	}
}
