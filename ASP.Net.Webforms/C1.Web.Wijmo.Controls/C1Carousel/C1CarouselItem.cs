﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.Localization;
using System.Web;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1Carousel
{
    /// <summary>
    /// Items within the carousel control.
    /// </summary>
    [ToolboxItem(false)]
	public class C1CarouselItem : UIElement, IDataItemContainer
	{

		#region ** fields

		private const string CSS_ITEM = "wijmo-wijcarousel-item";
		private const string CSS_JQ_CLEARFIX = "ui-helper-clearfix";
		private const string CSS_JQ_CONTENT = "ui-widget-content";
		private const string CSS_CAPTION = "wijmo-wijcarousel-caption";
		private const string CSS_IMAGE = "wijmo-wijcarousel-image";		

		private Hashtable _properties = new Hashtable();
		private ITemplate _template;
		private C1Carousel _carousel;
		private Image _image;
		private HtmlGenericControl _link;
		private HtmlGenericControl _caption;
		private HtmlGenericControl _templateContainer;

		private int _dataItemIndex;

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1CarouselItem"/> class.
		/// </summary>
		public C1CarouselItem()
		{
			//this.IsDesignMode = true;
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="C1CarouselItem"/> class.
        /// </summary>
		public C1CarouselItem(int index)
		{
			_dataItemIndex = index;
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Internal set the data item in data source. 
		/// </summary>
		public virtual object DataItem
		{
			get;
			protected internal set;
		}

		/// <summary>
		/// Whether the carousel item is loaded (on call back mode).
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[DefaultValue(false)]
		public bool IsLoaded
		{
			get
			{
				return _properties["IsLoad"] == null ? false : (bool)_properties["IsLoad"];
			}
			set
			{
				_properties["IsLoad"] = value;
			}
		}

		/// <summary>
		/// Index of carousel item.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable( EditorBrowsableState.Never)]
		[WidgetOption]
		[DefaultValue(0)]
		public int Index
		{
			get
			{
				return _properties["Index"] == null ? 0 : (int)_properties["Index"];
			}
			set
			{
				_properties["Index"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the URL that provides the path to an image to display in the item.
		/// </summary>
		[WidgetOption]
		[C1Description("C1CarouselItem.ImageUrl", "Image url of carousel item.")]
		[DefaultValue("")]
        [Editor("System.Web.UI.Design.ImageUrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
		[UrlProperty()]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string ImageUrl
		{
			get
			{
				return _properties["ImageUrl"] == null ? "" : _properties["ImageUrl"].ToString();
			}
			set
			{
				_properties["ImageUrl"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the URL of the link item to which this item links.
		/// </summary>
		[WidgetOption]
		[C1Description("C1CarouselItem.LinkUrl", "Link url of carousel item.")]
		[UrlProperty()]
        [Editor("System.Web.UI.Design.UrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
		[DefaultValue("")]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string LinkUrl
		{
			get
			{
				return _properties["LinkUrl"] == null ? "" : _properties["LinkUrl"].ToString();
			}
			set
			{
				_properties["LinkUrl"] = value;
			}
		}

        [WidgetOption]
        [DefaultValue(true)]
        [Layout(LayoutType.Appearance)]
        public override bool Visible
        {
            get
            {
                return _properties["Visible"] == null ? true : (bool)_properties["Visible"];
            }
            set
            {
                _properties["Visible"] = value;
            }
        }

        /// <summary>
        /// Hide this property.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
            }
        }

		///// <summary>
		///// Content of carousel item.
		///// </summary>
		//[WidgetOption]
		//[C1Description("C1CarouselItem.Content", "Content of carousel item.")]
		//[DefaultValue("")]
		//[C1Category("Appearance")]
		//[Layout(LayoutType.Appearance)]
		//public string Content
		//{
		//    get
		//    {
		//        return _properties["Content"] == null ? "" : _properties["Content"].ToString();
		//    }
		//    set
		//    {
		//        _properties["Content"] = value;
		//    }
		//}

		/// <summary>
        /// Gets or sets the carousel item's caption text.
		/// </summary>
		[WidgetOption]
		[C1Description("C1CarouselItem.Caption", "Caption of carousel item.")]
		[DefaultValue("")]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string Caption
		{
			get
			{
				return _properties["Caption"] == null ? "" : _properties["Caption"].ToString();
			}
			set
			{
				_properties["Caption"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the image title.
		/// </summary>
		[C1Description("C1CarouselItem.Title", "Title of image.")]
		[DefaultValue("")]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string Title
		{
			get
			{
				return _properties["Title"] == null ? "" : _properties["Title"].ToString();
			}
			set
			{
				_properties["Title"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the content template used to display the content of the control's item.
		/// </summary>
		[C1Description("C1CarouselItem.Template")]
		[Browsable(false)]
		[DefaultValue(null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Misc)]
		public ITemplate Template
		{
			get
			{
				return this._template;
			}
			set
			{
				this._template = value;
			}
		}

		/// <summary>
		/// Get or set the carousel. 
		/// </summary>
		[Browsable(false)]
		internal C1Carousel Carousel
		{
			get 
			{
				return this._carousel;
			}
			set {
				this._carousel = value;
			}
		}

		#endregion end of ** properties.

		#region ** Override Methods

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Li;
			}
		}

		/// <summary>
		/// CreateChildControls override.
		/// </summary>
		protected override void CreateChildControls()
		{
			ITemplate template = this.Template;
			if (template == null)
			{
				template = this.Carousel.ItemContent;
			}
			if (template != null)
			{
				_templateContainer = new HtmlGenericControl("div");
				_templateContainer.Attributes.Add("class", CSS_JQ_CLEARFIX);
				template.InstantiateIn(_templateContainer);
				this.Controls.Add(_templateContainer);
				this.DataBind();
			}
			else
			{
				if (!string.IsNullOrEmpty(this.ImageUrl))
				{
                    //fixed bug 32082 by Daniel.He 2013/3/22
					_image = new Image();
					//_image.Src = base.ResolveClientUrl(this.ImageUrl);
                    _image.Attributes.Add("src", base.ResolveClientUrl(this.ImageUrl));                    
					_image.Attributes.Add("title", this.Title);
				}

				if (!string.IsNullOrEmpty(this.LinkUrl))
				{
					_link = new HtmlGenericControl("a");
					_link.Attributes.Add("href", base.ResolveClientUrl(this.LinkUrl));					
					this.Controls.Add(_link);
					if (_image != null)
						_link.Controls.Add(_image);
				}
				else if (_image != null)
				{
					this.Controls.Add(_image);
				}

				if (this.IsDesignMode)
				{
					if (_image != null)
					{
						_image.Attributes.Add("class", CSS_IMAGE);
					}
				}

				CreateCaption();
			}

			base.CreateChildControls();
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. 
		/// This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (this.IsDesignMode) {
				writer.AddAttribute(HtmlTextWriterAttribute.Class,
					string.Format("{0} {1}", CSS_ITEM, CSS_JQ_CLEARFIX));
				writer.AddStyleAttribute( HtmlTextWriterStyle.Height,
					this.Carousel.ItemHeight + "px");
				writer.AddStyleAttribute(HtmlTextWriterStyle.Width,
					this.Carousel.ItemWidth + "px");
			}
			base.AddAttributesToRender(writer);
		}

		#endregion	
		
		#region ** private\internal methods

		#region **** internal methods

		#endregion

		#region **** private methods

		/// <summary>
		/// Create caption of the image.
		/// </summary>
		private void CreateCaption()
		{ 
			string caption = string.IsNullOrEmpty(Caption)? Title: Caption;
			if (!string.IsNullOrEmpty(caption) && this.Carousel.ShowCaption)
			{
				if (this.IsDesignMode)
				{
					_caption = new HtmlGenericControl("div");
					if (this.Carousel != null)
					{						
						_caption.Attributes.Add("class", string.Format("{0} {1} {2}",
							CSS_JQ_CONTENT, CSS_JQ_CLEARFIX, CSS_CAPTION));
						_caption.Style.Add(HtmlTextWriterStyle.Color, "#F1F1F1");
						_caption.Style.Add(HtmlTextWriterStyle.Width,
							this.Carousel.ItemWidth.ToString() + "px");
					}
				}
				else
				{
					_caption = new HtmlGenericControl("span");
				}
				_caption.InnerText = caption;
				this.Controls.Add(_caption);
			}			
		}

		#endregion

		#endregion

		#region ** IDataItemContainer interface implementations
		/// <summary>
		/// Gets DataItem of this C1CarouselItem.
		/// </summary>
		[Browsable(false)]
		object IDataItemContainer.DataItem
		{
			get { return this.DataItem; }
		}

		/// <summary>
		/// Gets DataItemIndex of this C1CarouselItem.
		/// </summary>
		[Browsable(false)]
		int IDataItemContainer.DataItemIndex
		{
			get { return this._dataItemIndex; }
		}

		/// <summary>
		/// Gets DisplayIndex of this C1CarouselItem.
		/// </summary>
		[Browsable(false)]
		int IDataItemContainer.DisplayIndex
		{
			get { return this._dataItemIndex; }
		}

		#endregion end of ** IDataItemContainer interface implementations.
	}
}
