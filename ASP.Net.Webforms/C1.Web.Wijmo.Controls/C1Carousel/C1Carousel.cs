﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Pager;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Licensing;

namespace C1.Web.Wijmo.Controls.C1Carousel
{
	/// <summary>
	/// Dynamically displays content in a carousel view.
	/// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Carousel.C1CarouselDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Carousel.C1CarouselDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Carousel.C1CarouselDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Carousel.C1CarouselDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Carousel runat=server></{0}:C1Carousel>")]
	[ToolboxBitmap(typeof(C1Carousel), "C1Carousel.png")]
	[LicenseProviderAttribute()]
	public partial class C1Carousel : C1TargetDataBoundControlBase, IC1Serializable, ICallbackEventHandler, IPostBackDataHandler, INamingContainer//,IPostBackEventHandler
	{
		#region ** fields

		#region **** licensing

		internal bool _productLicensed;
		private bool _shouldNag;

		#endregion

		#region **** const
		private const string CSS_JQ_WIDGET = "ui-widget";
		private const string CSS_JQ_CONTENT = "ui-widget-content";
		private const string CSS_JQ_CLEARFIX = "ui-helper-clearfix";
		private const string CSS_JQ_RESET = "ui-helper-reset";
		private const string CSS_JQ_CORNER = "ui-corner-all";
		private const string CSS_JQ_HEADER = "ui-widget-header";
		private const string CSS_JQ_DEFAULT = "ui-state-default";
		private const string CSS_JQ_UIICON = "ui-icon";
		private const string CSS_TRIANGLE = "-triangle-1";
		private const string CSS_W = "-w";
		private const string CSS_E = "-e";
		private const string CSS_N = "-n";
		private const string CSS_S = "-s";
		private const string CSS_HORIZONTAL = "-horizontal";
		private const string CSS_VERTICAL = "-vertical";
		private const string CSS_MULTI = "-multi";
		private const string CSS_NEXT = "-next";
		private const string CSS_BUTTON = "-button";
		private const string CSS_PREVIOUS = "-previous";
		private const string CSS_PLAY = "-play";
		private const string CSS_PAGER = "-pager";

		private const string CSS_WIJ_BASE = "wijmo-wijcarousel";
		private const string CSS_WIJ_PAGER_LIST = "wijmo-list";
		private const string CSS_WIJ_TIMER = CSS_WIJ_BASE + "-timerbar";
		private const string CSS_WIJ_TIMER_INNER = CSS_WIJ_BASE + "-timerbar-inner";
		private const string CSS_WIJ_CLIP = CSS_WIJ_BASE + "-clip";
		private const string CSS_WIJ_LIST = CSS_WIJ_BASE + "-list";
		private const string CSS_WIJ_DOT = CSS_WIJ_BASE + "-dot";
		private const string CSS_WIJ_PAGE = CSS_WIJ_BASE + "-page";
		private const string CSS_WIJ_PAGER = CSS_WIJ_BASE + CSS_PAGER;
		private const string CSS_WIJ_BUTTON = CSS_WIJ_BASE + CSS_BUTTON;
		private const string CSS_WIJ_BUTTON_NEXT = CSS_WIJ_BUTTON + CSS_NEXT;
		private const string CSS_WIJ_BUTTON_PREVIOUS = CSS_WIJ_BUTTON + CSS_PREVIOUS;
		private const string CSS_WIJ_HORIZONTAL = CSS_WIJ_BASE + CSS_HORIZONTAL;
		private const string CSS_WIJ_VERTICAL = CSS_WIJ_BASE + CSS_VERTICAL;
		private const string CSS_WIJ_HORIZONTAL_MULTI = CSS_WIJ_HORIZONTAL + CSS_MULTI;
		private const string CSS_WIJ_VERTICAL_MULTI = CSS_WIJ_VERTICAL + CSS_MULTI;

		private const string CSS_UIICON_TRIANGLE_W = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_W;
		private const string CSS_UIICON_TRIANGLE_E = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_E;
		private const string CSS_UIICON_TRIANGLE_N = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_N;
		private const string CSS_UIICON_TRIANGLE_S = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_S;
		private const string CSS_UIICON_PLAY = CSS_JQ_UIICON + CSS_PLAY;

		private const double LENGTH = 38;
		private const double BREADTH = 18;
		private const double PADDING_LENGTH = 18;
		private const double PADDING_BREADTH = 5;
		private const int PAGER_HEIGHT = 20;
		private const int PAGER_WIDTH = 22;
		private const int SLIDER_WIDTH = 210;

		#endregion

		#region **** privite

		private int pagerWidth;
		private int pagerHeight;
		private double _outerWidth;
		private double _outerHeight;
		private double _itemWidth;
		private double _itemHeight;
		private HtmlControl _itemContainer;
		private HtmlControl _nextBtn;
		private HtmlControl _prevBtn;
		private HtmlControl _timer;
		private HtmlControl _pager;
		private HtmlGenericControl _clip;
		private C1CarouselItemCollection _items = null;		
		private PositionSettings _prevPosition;
		private PositionSettings _nextPosition;
		private ITemplate _itemContent;
		private IEnumerable _data;
		private string _response;

		#endregion

		#endregion

		#region ** constructor
		///<summary>
		/// Initializes a new instance of the <seealso cref="C1Carousel"/> class.
		///</summary>
		public C1Carousel()
		{
			VerifyLicense();
			// this.IsDesignMode = true;
			Items.ItemAdded += new Base.Collections.C1ObservableItemCollection<C1Carousel, C1CarouselItem>.CollectionChangedEventHandler(Items_ItemAdded);

			_prevPosition = new PositionSettings()
			{
				My =
				{
					Left = this.ButtonPosition == ButtonPosition.Inside? HPosition.Right : HPosition.Left,
					Top = VPosition.Center
				},
				At = { Left = HPosition.Right, Top = VPosition.Center }
			};

			_nextPosition = new PositionSettings()
			{
				My =
				{
					Left = this.ButtonPosition == ButtonPosition.Inside ? HPosition.Left : HPosition.Right,
					Top = VPosition.Center
				},
				At = { Left = HPosition.Left, Top = VPosition.Center }
			};

			this.PagerPosition.My.Left = HPosition.Right;
			this.PagerPosition.My.Top = VPosition.Top;
			this.PagerPosition.At.Left = HPosition.Right;
			this.PagerPosition.At.Top = VPosition.Bottom;
			this.PagerPosition.SerializeAll = true;
		}

		protected void Items_ItemAdded(object sender, C1ObservableItemCollection<C1Carousel, C1CarouselItem>.CollectionChangedEventArgs e)
		{
			C1CarouselItem item = e.Item as C1CarouselItem;
			if (item != null) {
				item.Carousel = this;
			}
		}

		#endregion

		#region ** Properties

		/// <summary>
		/// Gets a value indicating whether the carousel object has an active template set.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[DefaultValue(false)]
		public bool IsTemplated
		{
			get
			{
				return GetPropertyValue("IsTemplated", false);
			}
			set
			{
				SetPropertyValue("IsTemplated", value);
			}
		}

		/// <summary>
		/// Set data in C1Gallery and other C1Controls.
		/// </summary>
		internal IEnumerable Data 
		{
			get 
			{
				return _data;
			}
			set {
				_data = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether items are loaded on demand.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
        [C1Description("C1Carousel.LoadOnDemand")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public bool LoadOnDemand
		{
			get
			{
				return GetPropertyValue("LoadOnDemand", false);
			}
			set
			{
				SetPropertyValue("LoadOnDemand", value);
			}
		}

		///// <summary>
		///// Determined how much images when the carousel loaded.
		///// </summary>
		//[DefaultValue(0)]
		//[C1Category("Category.Behavior"), C1Description("C1Carousel.LoadNumber")]
		//[Layout(LayoutType.Behavior)]
		//[WidgetOption]
		//public int LoadNumber
		//{
		//    get
		//    {
		//        return GetPropertyValue("LoadNumber", 0);
		//    }
		//    set
		//    {
		//        SetPropertyValue("LoadNumber", value);
		//    }
		//}

		/// <summary>
		/// The option need to serialize to client side.
		/// </summary>
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption()]
		public override string UniqueID
		{
			get
			{
				return base.UniqueID;
			}
		}

		/// <summary>
		/// The option need to serialize to client side.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		public int Count
		{
			get 
			{
				return this.Items.Count;
			}
		}

		/// <summary>
		/// Gets or sets the height of the Carousel.
		/// </summary>
		public override Unit Height
		{
			get
			{
				return base.Height.IsEmpty ?
					Unit.Parse("300px") : base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the width of the Carousel.
		/// </summary>
		public override Unit Width
		{
			get
			{
				return base.Width.IsEmpty ?
					Unit.Parse("600px") : base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		#region used for calculate the size on design time

		/// <summary>
		/// Get the outer width of the carousel according to the Width property.
		/// </summary>
		internal double OuterWidth
		{
			get
			{
				return _outerWidth;
			}
		}

		/// <summary>
		/// Get the outer height of the carousel according to the Height property.
		/// </summary>
		internal double OuterHeight
		{
			get
			{
				return _outerHeight;
			}
		}
		
		/// <summary>
		/// Get the width of the carousel item.
		/// </summary>
		internal double ItemWidth
		{
			get
			{
				return this._itemWidth;
			}
		}

		/// <summary>
		/// Get the height of the carousel item.
		/// </summary>
		internal double ItemHeight
		{
			get
			{
				return this._itemHeight;
			}
		}

		#endregion

		/// <summary>
		/// Gets or sets the name of the field from the data source to bind to the attribute "src" of the image.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Carousel.DataImageUrlField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataImageUrlField
		{
			get
			{
				return this.GetPropertyValue<string>("DataImageUrlField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataImageUrlField", value);
			}
		}

		/// <summary>
		/// Gets or sets the name of the field from the data source to bind to the caption of the images.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Carousel.DataCaptionField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataCaptionField
		{
			get
			{
				return this.GetPropertyValue<string>("DataCaptionField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataCaptionField", value);
			}
		}

		/// <summary>
		/// Gets or sets the name of the field from the data source to bind to the link.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Carousel.DataLinkUrlField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataLinkUrlField
		{
			get
			{
				return this.GetPropertyValue<string>("DataLinkUrlField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataLinkUrlField", value);
			}
		}

		/// <summary>
		/// Gets or sets the template for the content area of the <see cref="C1Carousel"/> control. 
		/// </summary>
		/// <remarks>
		/// Use the ContentTemplate property to control the contents of the dialog window. 
		/// 
		/// To specify a template for the dialog, reset the <see cref="ContentUrl"/> property, place the &lt;ContentTemplate&gt; tags between the opening and closing tags of the <seealso cref="C1Window"/> control. 
		/// You can then list the contents of the template between the opening and closing &lt;ContentTemplate&gt; tags.
		/// </remarks>
		[Browsable(false)]
		[DefaultValue(null)]
		[C1Description("C1Carousel.ItemContent")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public ITemplate ItemContent
		{
			get
			{
				return _itemContent;
			}
			set
			{
				_itemContent = value;
			}
		}

		/// <summary>
		/// Gets a <see cref="C1CarouselItemCollection"/> 
		/// object that contains the items of the current <see cref="C1Carousel"/> control.
		/// </summary>
		[C1Description("C1Carousel.Items")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[RefreshProperties(RefreshProperties.All)]
		[CollectionItemType(typeof(C1CarouselItem))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Layout(LayoutType.Appearance)]
		[WidgetOption]
		public C1CarouselItemCollection Items
		{
			get
			{
				if (this._items == null)
				{
					this._items = new C1CarouselItemCollection(this);
				}
				return this._items;
			}
		}

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region ** Events

		///<summary>
		/// Occurs before <see cref="C1CarouselItem"/> is created in data binding.
		///</summary>
		[C1Category("Category.Misc")]
		[C1Description("C1Carousel.ItemDataBinding")]
		public event C1CarouselItemEventHandler ItemDataBinding;

		private void OnItemDataBinding(C1CarouselItemEventArgs e)
		{
			if (ItemDataBinding != null)
			{
				ItemDataBinding(this, e);
			}
		}

		///<summary>
		/// Occurs after <see cref="C1CarouselItem"/> is created in data binding.
		///</summary>
		[C1Category("Category.Misc")]
		[C1Description("C1Carousel.ItemDataBound")]
		public event C1CarouselItemEventHandler ItemDataBound;

		private void OnItemDataBound(C1CarouselItemEventArgs e)
		{
			if (ItemDataBound != null)
			{
				ItemDataBound(this, e);
			}
		}

		#endregion

		#region ** Override Methods

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Called before control initialization.
		/// </summary>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
			if (!IsDesignMode &&
			(!string.IsNullOrEmpty(this.DataSourceID)
			|| (this.DataSource != null)))
			{
				this.RequiresDataBinding = true;
			}
			if (this.ItemContent != null) 
			{
				this.IsTemplated = true;
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// CreateChildControls override.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if (base.RequiresDataBinding && (!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				this.EnsureDataBound();
			}
			else
			{
				CreateChildItems();
			}

			if (this.IsDesignMode)
			{
				CreateDesignTimeControls();
			}

			if (this.LoadOnDemand)
			{
				Page.ClientScript.GetCallbackEventReference(this, "", "", "");
			}

			base.CreateChildControls();
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. 
		/// This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			EnsureID();
			string cssClass = this.CssClass;
			this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass);

			if (this.IsDesignMode)
			{
				CalculateSize();
				_clip.Attributes.Add("class", CSS_WIJ_CLIP);
				_clip.Style.Add(HtmlTextWriterStyle.Width, this.Width.ToString());
				_clip.Style.Add(HtmlTextWriterStyle.Height, this.Height.ToString());
				_itemContainer.Attributes.Add("class", string.Format("{0} {1}", CSS_WIJ_LIST, CSS_JQ_CLEARFIX));
				if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
				{
					_itemContainer.Style.Add(HtmlTextWriterStyle.Width,
						(this.Width.Value * this.Items.Count).ToString() + "px");
					_itemContainer.Style.Add(HtmlTextWriterStyle.Height, this.Height.ToString());
					ApplyPositionStyle(this.OuterWidth, this.OuterHeight, BREADTH, LENGTH, _nextBtn, _nextPosition);
					ApplyPositionStyle(this.OuterWidth, this.OuterHeight, BREADTH, LENGTH, _prevBtn, _prevPosition);
				}
				else
				{
					_itemContainer.Style.Add(HtmlTextWriterStyle.Width, this.Width.ToString());
					_itemContainer.Style.Add(HtmlTextWriterStyle.Height,
						(this.Height.Value * this.Items.Count).ToString() + "px");

					_prevPosition.My.Left = _prevPosition.At.Left =
					_nextPosition.At.Left = _nextPosition.My.Left = HPosition.Center;

					_prevPosition.My.Top = this.ButtonPosition ==
						ButtonPosition.Inside ? VPosition.Top : VPosition.Bottom;
					_prevPosition.At.Top = VPosition.Top;
					_nextPosition.My.Top = this.ButtonPosition ==
						ButtonPosition.Inside ? VPosition.Bottom : VPosition.Top;
					_nextPosition.At.Top = VPosition.Bottom;

					ApplyPositionStyle(this.OuterWidth, this.OuterHeight, LENGTH, BREADTH, _prevBtn, _prevPosition);
					ApplyPositionStyle(this.OuterWidth, this.OuterHeight, LENGTH, BREADTH, _nextBtn, _nextPosition);
				}

				pagerHeight = this.PagerType == Wijmo.Controls.C1Carousel.PagerType.Thumbnails ?
					this.Thumbnails.ImageHeight : PAGER_HEIGHT;

				ApplyPositionStyle(this.OuterWidth, this.OuterHeight, pagerWidth, pagerHeight, this._pager, this.PagerPosition);

				//Get no items design time view.
				if (this.Items.Count == 0) { 
					this.CssClass = string.Format("{0} {1}", this.CssClass, CSS_JQ_CONTENT);
					this._clip.InnerHtml = String.Format("{0} {1} {2}", "<span>",
						 C1Localizer.GetString("C1Carousel.NoItemAlert",
						"There is no item."), "</span>");
					this._clip.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
				}
			}

			base.AddAttributesToRender(writer);
			this.CssClass = cssClass;
		}

		protected override void EnsureEnabledState()
		{
			return;
		}

		#endregion

		#region ** DataBind method

		/// <summary>
		/// This method overrides the generic DataBind method to bind the related data to the control.
		/// </summary>
		public sealed override void DataBind()
		{
			base.DataBind();
		}

		// <summary>
		/// Binds data from the data source to the control.
		/// </summary>
		/// <param name="retrievedData">The IEnumerable list of data returned from a PerformSelect method call.</param>
		protected override void PerformDataBinding(IEnumerable data)
		{
			base.PerformDataBinding(data);
			if (IsDesignMode || data == null)
			{
				return;
			}

			this._data = data;
			IEnumerator enumerator = data.GetEnumerator();			
			this.Items.Clear();
			int count = 0;
			while (enumerator.MoveNext())
			{
				object itemData = enumerator.Current;

				C1CarouselItem item = new C1CarouselItem(count);
				if (this.LoadOnDemand)
				{
					if (this.Display > count)
						GenerateItemFromData(itemData, count, item);
				}
				else
				{
					GenerateItemFromData(itemData, count, item);
				}

				C1CarouselItemEventArgs arg = new C1CarouselItemEventArgs(item);
				OnItemDataBinding(arg);
				this.Items.Add(item);
				OnItemDataBound(arg);

				count++;
			}
			this.Controls.Clear();
			this.CreateChildItems();
		}

		#endregion

		#region ** private\internal methods

		#region **** internal methods
 
		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Carousel), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region **** create child control

		/// <summary>
		/// Create items markup to carousel
		/// </summary>
		private void CreateChildItems()
		{
			_itemContainer = new HtmlGenericControl("ul");


			foreach (C1CarouselItem item in this.Items)
			{
				if (item.DisplayVisible)
				{
					_itemContainer.Controls.Add(item);
				}
			}

			if (IsDesignMode)
			{
				_clip = new HtmlGenericControl("div");
				_clip.Controls.Add(this._itemContainer);
				this.Controls.Add(this._clip);
			}
			else 
			{
				this.Controls.Add(this._itemContainer);
			}
		}

		/// <summary>
		/// Create DesignTime Elements
		/// </summary>
		private void CreateDesignTimeControls()
		{
			CreateButtons();
			if (this.ShowTimer)
			{
				CreateTimer();
			}
			if (this.ShowPager)
			{
				CreatePager();
			}
		}

		/// <summary>
		/// Create buttons.
		/// </summary>
		private void CreateButtons()
		{
			string nextBtnClass = this.Orientation == Wijmo.Controls.Orientation.Horizontal ? 
				CSS_UIICON_TRIANGLE_E : CSS_UIICON_TRIANGLE_S;
			string prevBtnClass = this.Orientation == Wijmo.Controls.Orientation.Horizontal ?
				CSS_UIICON_TRIANGLE_W : CSS_UIICON_TRIANGLE_N;

			if (string.IsNullOrEmpty(this.NextClass.DefaultClass))
			{
				this._nextBtn = CreateButton(CSS_WIJ_BUTTON_NEXT, nextBtnClass);
			}
			else
			{
				this._nextBtn = CreateButton(this.NextClass.DefaultClass, "");
			}

			if (string.IsNullOrEmpty(this.PrevClass.DefaultClass))
			{
				this._prevBtn = CreateButton(CSS_WIJ_BUTTON_PREVIOUS, prevBtnClass);
			}
			else
			{
				this._prevBtn = CreateButton(this.PrevClass.DefaultClass, "");
			}

			this.Controls.Add(this._nextBtn);
			this.Controls.Add(this._prevBtn);
		}

		/// <summary>
		/// Create pager.
		/// </summary>
		private void CreatePager()
		{
			this._pager = new HtmlGenericControl("div");
			C1TargetControlBase control;
			switch (this.PagerType)
			{
				case Wijmo.Controls.C1Carousel.PagerType.Dots:
					this.GenerateDotsPager(this.Items.Count);
					pagerWidth = this.Items.Count * PAGER_WIDTH;
					break;
				case Wijmo.Controls.C1Carousel.PagerType.Slider:
					control = new C1Slider.C1Slider();
					this._pager.Controls.Add(control);
					pagerWidth = SLIDER_WIDTH;
					break;
				case Wijmo.Controls.C1Carousel.PagerType.Thumbnails:
					this.GenerateThumbsPager();
					break;
				case Wijmo.Controls.C1Carousel.PagerType.Numbers:
				default:
					control = new C1Pager.C1Pager() {
						PageCount = this.Items.Count == 0 ? 1 : this.Items.Count 
					};
					this._pager.Controls.Add(control);
					pagerWidth = this.Items.Count * PAGER_WIDTH;
					break;
			}		
			
			this.Controls.Add(this._pager);
		}

		/// <summary>
		/// Create pager of thumbnails mode.
		/// </summary>
		private void GenerateThumbsPager()
		{
			HtmlGenericControl ul = new HtmlGenericControl("ul");
			ul.Attributes.Add("class",
					string.Format("{0} {1}",
					CSS_WIJ_PAGER_LIST, CSS_JQ_CLEARFIX));
			this._pager.Controls.Add(ul);
			for (int i = 0; i < this.Items.Count && i < this.Thumbnails.Images.Length; i++)
			{
				HtmlGenericControl thumb = new HtmlGenericControl("li");
				HtmlGenericControl img = new HtmlGenericControl("img");
				thumb.Attributes.Add("class",
					string.Format("{0} {1}",
					CSS_JQ_DEFAULT, CSS_WIJ_PAGE));
				thumb.Style.Add(HtmlTextWriterStyle.Width, this.Thumbnails.ImageWidth.ToString() + "px");
				thumb.Style.Add(HtmlTextWriterStyle.Height, this.Thumbnails.ImageHeight.ToString() + "px");
				thumb.Style.Add(HtmlTextWriterStyle.Overflow, "hidden");

				img.Attributes.Add("src", base.ResolveClientUrl(this.Thumbnails.Images[i]));
				thumb.Controls.Add(img);
				ul.Controls.Add(thumb);
			}
		}

		/// <summary>
		/// Create pager of dots mode.
		/// </summary>
		private void GenerateDotsPager(int count)
		{
			HtmlGenericControl ul = new HtmlGenericControl("ul");
			ul.Attributes.Add("class",
					string.Format("{0} {1}", 
					CSS_JQ_CORNER, CSS_JQ_CLEARFIX));
			ul.Style.Add(HtmlTextWriterStyle.Margin, "0px");
			ul.Style.Add(HtmlTextWriterStyle.Padding, "0px");
			this._pager.Controls.Add(ul);
			for (int i = 0; i < count; i++)
			{
				HtmlGenericControl dot = new HtmlGenericControl("li");
				dot.Attributes.Add("class",
					string.Format("{0} {1} {2}",
					CSS_JQ_DEFAULT, CSS_WIJ_PAGE, CSS_WIJ_DOT));
				ul.Controls.Add(dot);
			}
		}

		/// <summary>
		/// Create timer.
		/// </summary>
		private void CreateTimer()
		{
			HtmlGenericControl progress = new HtmlGenericControl("div");
			HtmlGenericControl button = CreateButton(CSS_WIJ_BUTTON,
				CSS_UIICON_PLAY);
			this._timer = new HtmlGenericControl("div");
			this._timer.Attributes["class"] = CSS_WIJ_TIMER;
			progress.Attributes["class"] = CSS_WIJ_TIMER_INNER;

			this._timer.Controls.Add(progress);
			this._timer.Controls.Add(button);
			this.Controls.Add(this._timer);
		}

		/// <summary>
		/// Create button.
		/// </summary>
		private HtmlGenericControl CreateButton(string btnClass, string iconClass)
		{
			HtmlGenericControl button = new HtmlGenericControl("a");
			HtmlGenericControl icon = new HtmlGenericControl("span");

			button.Attributes["class"] = string.Format("{0} {1}",CSS_JQ_DEFAULT, btnClass);
			if (!string.IsNullOrEmpty(iconClass))
			{
				icon.Attributes["class"] = string.Format("{0} {1}", CSS_JQ_UIICON, iconClass);
			}
			button.Controls.Add(icon);
			
			return button;
		}

		/// <summary>
		/// get value from data source.
		/// </summary>
		/// <returns>value if the item</returns>
		private string GetValueByIndex(int index, object dataItem)
		{
			PropertyDescriptorCollection props = TypeDescriptor.GetProperties(dataItem);
			string contentContent = String.Empty;

			if (props.Count >= 2)
			{
				if (null != props[1].GetValue(dataItem))
				{
					contentContent = props[1].GetValue(dataItem).ToString();
				}
			}
			return contentContent;
		}

		/// <summary>
		/// calculate the actually width & height of the carousel and item. 
		/// </summary>
		private void CalculateSize()
		{
			if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
			{
				this._itemWidth = this.Width.Value / this.Display;
				this._itemHeight = this.Height.Value;
				this._outerWidth = Display > 1 ? this.Width.Value + PADDING_LENGTH * 2 : this.Width.Value;
				this._outerHeight = Display > 1 ? this.Height.Value + PADDING_BREADTH * 2 : this.Height.Value;
			}
			else
			{
				this._itemWidth = this.Width.Value;
				this._itemHeight = this.Height.Value / this.Display;
				this._outerWidth = Display > 1 ? this.Width.Value + PADDING_BREADTH * 2 : this.Width.Value;
				this._outerHeight = Display > 1 ? this.Height.Value + PADDING_LENGTH * 2 : this.Height.Value;
			}		
		}

		/// <summary>
		/// Determine compound css class.
		/// </summary>
		/// <returns></returns>
		private string DetermineCompoundCssClass()
		{
			if (!this.IsDesignMode)
			{
				return Utils.GetHiddenClass();
			}

			string sCompoundCssClass = "{0} {1} {2}";
			string subClass = "";
			string[] HorizontalCss = { CSS_WIJ_HORIZONTAL_MULTI, CSS_JQ_CONTENT };
			string[] VertitalCss = { CSS_WIJ_VERTICAL_MULTI, CSS_JQ_CONTENT };
			if (this.Orientation == Orientation.Horizontal)
			{
				if (this.Display > 1)
				{
					subClass = string.Join(" ", HorizontalCss);
					
				}
				else {
					subClass = CSS_WIJ_HORIZONTAL;
				}
			}
			else 
			{ 
				if (this.Display > 1)
				{
					subClass = string.Join(" ", VertitalCss);
				}
				else {
					subClass = CSS_WIJ_VERTICAL;
				}
			}

			return string.Format(sCompoundCssClass, 
				CSS_WIJ_BASE, 
				CSS_JQ_WIDGET, 
				subClass);
		}

		/// <summary>
		/// Generate <see cref="C1CarouselItem"/> item from the item data in data source.
		/// </summary>
		/// <param name="itemData">Item data that gets from data source. </param>
		/// <param name="index">Index of the <see cref="C1CarouselItem"/>.</param>
		/// <param name="item">Instance of <see cref="C1CarouselItem"/>.</param>
		/// <returns>The Generated <see cref="C1CarouselItem"/></returns>
		internal C1CarouselItem GenerateItemFromData(object itemData, int index, C1CarouselItem item)
		{
			item.DataItem = itemData;

			if (!String.IsNullOrEmpty(this.DataImageUrlField))
			{
				item.ImageUrl = DataBinder.GetPropertyValue(itemData, this.DataImageUrlField).ToString();
			}
			else
			{
				item.ImageUrl = GetValueByIndex(0, itemData);
			}

			if (!String.IsNullOrEmpty(this.DataLinkUrlField))
			{
				item.LinkUrl = DataBinder.GetPropertyValue(itemData, this.DataLinkUrlField).ToString();
			}
			else
			{
				item.LinkUrl = GetValueByIndex(1, itemData);
			}

			if (!String.IsNullOrEmpty(this.DataCaptionField))
			{
				item.Caption = DataBinder.GetPropertyValue(itemData, this.DataCaptionField).ToString();
			}
			else
			{
				item.Caption = GetValueByIndex(2, itemData);
			}

			item.Index = index;
			item.IsLoaded = true;

			return item;
		}

		#endregion

		#region **** Add style for control

		/// <summary>
		/// calculate left
		/// </summary>
		/// <returns>left</returns>
		private double ApplyLeft(double pWidth, double cWidth, PositionSettings position)
		{
			double left;
			//VPosition.Bottom
			switch (position.At.Left)
			{
				case HPosition.Center:
					left = pWidth / 2;
					break;
				case HPosition.Right:
					left = pWidth;
					break;
				case HPosition.Left:
				default:
					left = 0;
					break;
			}
			switch (position.My.Left)
			{
				case HPosition.Center:
					left -= cWidth / 2;
					break;
				case HPosition.Right:
					left -= cWidth;
					break;
				case HPosition.Left:
				default:
					break;
			}
			left = left + position.Offset.Left;
			return left;
		}

		/// <summary>
		/// calculate top
		/// </summary>
		/// <returns>top</returns>
		private double ApplyTop(double pHeight, double cHeight, PositionSettings position)
		{
			double top;
			//VPosition.Bottom
			switch (position.At.Top)
			{
				case VPosition.Center:
					top = pHeight / 2;
					break;
				case VPosition.Bottom:
					top = pHeight;
					break;
				case VPosition.Top:
				default:
					top = 0;
					break;
			}
			switch (position.My.Top)
			{
				case VPosition.Center:
					top -= cHeight / 2;
					break;
				case VPosition.Bottom:
					top -= cHeight;
					break;
				case VPosition.Top:
				default:
					break;
			}
			top = top + position.Offset.Top;
			return top;
		}

		/// <summary>
		/// Apply position for Web Control, for jquery.ui.position.js at design time.
		/// </summary>
		private void ApplyPositionStyle(double pWidth, double pHeight,
			double cWidth, double cHeight, HtmlControl control,
			PositionSettings position)
		{
			if (control == null) {
				return;
			}
			double left, top;
			left = ApplyLeft(pWidth, cWidth, position);
			top = ApplyTop(pHeight, cHeight, position);

			control.Style.Add(HtmlTextWriterStyle.Position, "absolute");
			control.Style.Add(HtmlTextWriterStyle.Left, left.ToString() + "px");
			control.Style.Add(HtmlTextWriterStyle.Top, top.ToString() + "px");
		}

		/// <summary>
		/// Apply position for Web Control, for jquery.ui.position.js in design time.
		/// </summary>
		private void ApplyPositionStyle(WebControl pControl,
			double cWidth, double cHeight, HtmlControl control,
			PositionSettings position)
		{
			ApplyPositionStyle(pControl.Width.Value,
				pControl.Height.Value,
				cWidth, cHeight,
				control,
				position);
		}			

		#endregion

		#endregion

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for a <see cref="C1Carousel"/> control.
		/// </summary>
		/// <param name="postDataKey">
		/// The key identifier for the control.
		/// </param>
		/// <param name="postCollection">
		/// The collection of all incoming name values.
		/// </param>
		/// <returns>
		/// Returns true if the server control's state changes as a result of the postback;
		/// otherwise, false.
		/// </returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		/// <summary>
		/// Signals the <see cref = "C1Carousel"/> control to notify the ASP.NET
		/// application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations

		#region ** ICallBackEventHandler

		/// <summary>
		/// Implement of ICallbackEventHandler.GetCallbackResult
		/// </summary>
		string ICallbackEventHandler.GetCallbackResult()
		{
			return _response;
		}

		/// <summary>
		/// Implement of ICallbackEventHandler.RaiseCallbackEvent
		/// </summary>
		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			try
			{
				Hashtable data = JsonHelper.StringToObject(eventArgument);
				string commandName = (string)data["CommandName"];
				ArrayList commandData = data["CommandData"] as ArrayList;
				_response = ProcessAjaxCommand(commandName, commandData);
			}
			catch (Exception ex)
			{
				_response = ex.Message;
			}
		}

		/// <summary>
		/// Identify the action to take as response to the client request
		/// </summary>
		private string ProcessAjaxCommand(string commandName, ArrayList commandData)
		{
			if (commandName == "GetItems")
			{
				object callbackData = LoadRequiredData(commandData);
				return JsonHelper.WidgetToString(callbackData, this);
			}
			return "";
		}

		/// <summary>
		/// Load callback data of specified nodes.
		/// </summary>
		private IEnumerable LoadRequiredData(ArrayList idxs)
		{
			EnsureDataBound();
			IList<object> bindItems = new List<object>() { };

			int count = 0;
			if (this._data == null)
			{
				return null;
			}
			foreach(object itemData in this._data)
			{
				if (idxs.Contains(count))
				{
					C1CarouselItem item = this.Items[count];
					this.GenerateItemFromData(itemData, count, item);					
					if (this.IsTemplated)
					{
						//it seems has some bug.
						bindItems.Add(GetCallbackResult(item));
					}
					else
					{
						bindItems.Add(item);
					}
				}
				count++;
			}

			return bindItems;
		}

		/// <summary>
		/// Load callback data of template item.
		/// </summary>
		private Hashtable GetCallbackResult(C1CarouselItem item)
		{
			string templateHtml = string.Empty;
			using (System.IO.StringWriter sw = new System.IO.StringWriter())
			using (HtmlTextWriter hw = new HtmlTextWriter(sw))
			{
				item.RenderControl(hw);
				sw.Flush();
				templateHtml = sw.ToString();
			}
			Hashtable result = new Hashtable();
			result["Index"] = item.Index;
			result["Html"] = templateHtml;
			result["Item"] = item;
			return result;
		}

		#endregion

		#region ** IC1Serializable

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1CarouselSerializer sz = new C1CarouselSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1CarouselSerializer sz = new C1CarouselSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1CarouselSerializer sz = new C1CarouselSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1CarouselSerializer sz = new C1CarouselSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}

		#endregion
	}
}
