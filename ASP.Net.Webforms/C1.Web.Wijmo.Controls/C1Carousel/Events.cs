﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Carousel
{
	///<summary>ItemDataBound
	/// Represents the method that will handle an data binding events of <see cref="C1CarouselItem"/>.
    /// Such as <see cref="C1CarouselItem.BoundItemCreated"/> event and <see cref="C1CarouselItem.ItemDataBound"/> event.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">An <see cref="C1CarouselItemEventArgs"/> that contains event data.</param>
	public delegate void C1CarouselItemEventHandler(object sender, C1CarouselItemEventArgs args);

	/// <summary>
	/// Contains event data for data binding events.
    /// Such as <see cref="C1CarouselItem.BoundItemCreated"/> event and <see cref="C1CarouselItem.ItemDataBound"/> event.
	/// </summary>
	public class C1CarouselItemEventArgs : EventArgs
	{
		private C1CarouselItem _item;

		/// <summary>
		/// Gets the <see cref="C1CarouselItem"/> related with this event.
		/// </summary>
		public C1CarouselItem Item
		{
			get
			{
				return this._item;
			}
		}

		/// <summary>
		/// Initializes a new instance of the C1CarouselItem class.
		/// </summary>
		/// <param name="item"><see cref="C1CarouselItem"/> related with this event</param>
		public C1CarouselItemEventArgs(C1CarouselItem item)
		{
			this._item = item;
		}
	}
}
