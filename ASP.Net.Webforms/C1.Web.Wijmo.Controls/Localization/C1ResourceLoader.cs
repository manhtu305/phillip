﻿using System.Collections;
using System.Globalization;
using System.Reflection;

namespace C1.Web.Wijmo.Controls.Localization
{
    /// <summary>
    /// C1ResourceLoader
    /// </summary>
    public class C1ResourceLoader
    {
        private IResourceProvider _globalProvider;
        private IResourceProvider _embededProvider;
        private readonly string _baseName;
        private readonly Assembly _assembly;
        private static readonly Hashtable _verifiedGlobalResources = new Hashtable();

        /// <summary>
        /// Resource Loader.
        /// </summary>
        public C1ResourceLoader()
        {
			_baseName = "C1.Web.Wijmo.Controls.Localization.Resources.WijmoControls";
            _assembly = Assembly.GetExecutingAssembly();
            Init();
        }

        /// <summary>
        /// Resource Loader.
        /// </summary>
        /// <param name="baseName">base name</param>
        /// <param name="assembly">assembly</param>
        public C1ResourceLoader(string baseName, Assembly assembly)
        {
            _baseName = baseName;
            _assembly = assembly;
            Init();
        }

        private void Init()
        {
            _globalProvider = new GlobalResourceProvider(_baseName.Substring(_baseName.LastIndexOf('.')).TrimStart('.'));
            _embededProvider = new EmbeddedResourceProvider(_baseName, _assembly);
        }

        /// <summary>
        /// Get string by key.
        /// </summary>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        public string GetString(string resourceKey)
        {
            return GetString(resourceKey, CultureInfo.CurrentCulture);
            //return GetString(resourceKey, new CultureInfo("zh-CN"));
        }

        /// <summary>
        /// Get string by key and culture.
        /// </summary>
        /// <param name="resourceKey">key</param>
        /// <param name="culture">culture</param>
        /// <returns></returns>
        public string GetString(string resourceKey, CultureInfo culture)
        {
            string s = _globalProvider.GetString(resourceKey, culture);
            if (s==null)
            {
                s = _embededProvider.GetString(resourceKey, culture);
            }
            return s;
        }

        /// <summary>
        /// GetResourceLoader
        /// </summary>
        /// <returns></returns>
        public static C1ResourceLoader GetResourceLoader(string baseName, Assembly assembly)
        {
            return new C1ResourceLoader(baseName, assembly);
        }

        interface IResourceProvider
        {
            string GetString(string resourceKey, CultureInfo culture);
        }

        private class EmbeddedResourceProvider : IResourceProvider
        {
            private readonly System.Resources.ResourceManager _rm;


            public EmbeddedResourceProvider(string baseName, Assembly assembly)
            {
                _rm = new System.Resources.ResourceManager(baseName, assembly);
                _rm.IgnoreCase = true;
            }

            public string GetString(string resourceKey, CultureInfo culture)
            {
                return _rm.GetString(resourceKey, culture);
            }
        }

        private class GlobalResourceProvider : IResourceProvider
        {
            private readonly string _classKey;
            public GlobalResourceProvider(string classKey)
            {
                _classKey = classKey;
            }

            public string GetString(string resourceKey, CultureInfo culture)
            {
                string s = null;
                string str = (culture == null) ? CultureInfo.CurrentUICulture.Name : culture.Name;
                string fileExists = _verifiedGlobalResources[_classKey + str] as string;
                if (fileExists == "no")
                {
                    return s;
                }

                try
                {
                    s = (string) System.Web.HttpContext.GetGlobalResourceObject(_classKey, resourceKey, culture);
                }
                catch
                {
                    _verifiedGlobalResources[_classKey + str] = "no";
                }
                return s;
            }
        }
    }
}