﻿using System.Globalization;
using System.Reflection;

namespace C1.Web.Wijmo.Controls.Localization
{
    internal class C1Localizer
    {
        private static C1ResourceLoader _loader;

		internal static string GetString(string key)
		{
			return GetString(key, null, null);
		}

		internal static string GetString(string key, string defaultValue)
		{
			return GetString(key, defaultValue, null);
		}

		internal static string GetString(string key, CultureInfo c)
		{
			return GetString(key, null, c);
		}

		internal static string GetResourceText(string key, CultureInfo c)
		{
			return c == null ? Loader.GetString(key) : Loader.GetString(key, c);
		}

		internal static string GetString(string key, string defaultValue, CultureInfo c)
		{
			string s = GetResourceText(key, c);
			return s == null ? (defaultValue == null ? key : defaultValue) : s;
		}


        private static C1ResourceLoader Loader
        {
            get
            {
                if (_loader == null)
                {
                    _loader = new C1ResourceLoader();
                }
                return _loader;
            }
        }
    }
}
