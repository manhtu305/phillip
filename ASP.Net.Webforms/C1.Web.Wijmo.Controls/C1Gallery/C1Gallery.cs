﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Pager;
using C1.Web.Wijmo.Controls.C1Carousel;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Licensing;

namespace C1.Web.Wijmo.Controls.C1Gallery
{
	/// <summary>
	/// C1Gallery is an image gallery control that displays images and their thumbnail images.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Gallery.C1GalleryDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Gallery.C1GalleryDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Gallery.C1GalleryDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Gallery.C1GalleryDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Gallery runat=server></{0}:C1Gallery>")]
	[ToolboxBitmap(typeof(C1Gallery), "C1Gallery.png")]
	[LicenseProviderAttribute()]
	public partial class C1Gallery : C1TargetDataBoundControlBase, IC1Serializable, ICallbackEventHandler, IPostBackDataHandler//, IPostBackEventHandler
	{
		#region ** fields

		#region **** licensing

		internal bool _productLicensed;
		private bool _shouldNag;

		#endregion

		#region **** const

		private const string CSS_JQ_CONTENT = "ui-widget-content";
		private const string CSS_JQ_CLEARFIX = "ui-helper-clearfix";
		private const string CSS_JQ_WIDGET = "ui-widget";
		private const string CSS_JQ_CORNER = "ui-corner-all";
		private const string CSS_JQ_HEADER = "ui-widget-header";
		private const string CSS_JQ_DEFAULT = "ui-state-default";
		private const string CSS_JQ_UIICON = "ui-icon";
		private const string CSS_JQ_OVERLAY = "ui-widget-overlay";

		private const string CSS_TRIANGLE = "-triangle-1";
		private const string CSS_W = "-w";
		private const string CSS_E = "-e";
		private const string CSS_N = "-n";
		private const string CSS_S = "-s";
		private const string CSS_NEXT = "-next";
		private const string CSS_BUTTON = "-button";
		private const string CSS_PREVIOUS = "-previous";
		private const string CSS_PLAY = "-play";

		private const string CSS_WIJ_BASE = "wijmo-wijgallery";
		private const string CSS_WIJ_TIMER = CSS_WIJ_BASE + "-timerbar";
		private const string CSS_WIJ_TIMER_INNER = CSS_WIJ_BASE + "-timerbar-inner";
		private const string CSS_WIJ_COUNTER = CSS_WIJ_BASE + "-counter";
		private const string CSS_WIJ_FRAME = CSS_WIJ_BASE + "-frame";
		private const string CSS_WIJ_CONTENT = CSS_WIJ_BASE + "-content";
		private const string CSS_WIJ_CAPTION = CSS_WIJ_BASE + "-caption";
		private const string CSS_WIJ_CURRENT = CSS_WIJ_BASE + "-current";
		private const string CSS_WIJ_BUTTON = CSS_WIJ_BASE + CSS_BUTTON;
		private const string CSS_WIJ_BUTTON_NEXT = CSS_WIJ_BUTTON + CSS_NEXT;
		private const string CSS_WIJ_BUTTON_PREVIOUS = CSS_WIJ_BUTTON + CSS_PREVIOUS;

		private const string CSS_UIICON_TRIANGLE_W = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_W;
		private const string CSS_UIICON_TRIANGLE_E = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_E;
		private const string CSS_UIICON_TRIANGLE_N = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_N;
		private const string CSS_UIICON_TRIANGLE_S = CSS_JQ_UIICON + CSS_TRIANGLE + CSS_S;
		private const string CSS_UIICON_PLAY = CSS_JQ_UIICON + CSS_PLAY;

		private const double LENGTH = 38;
		private const double BREADTH = 18;

		private const double PADDING_BREADTH = 32;
		private const double PADDING_LENGTH = 20;

		#endregion

		#region **** private

		private HtmlGenericControl _text;
		private HtmlControl _image;
		private HtmlControl _frame;
		private HtmlControl _content;
		private HtmlControl _itemContainer;
		private C1GalleryItemCollection _items = null;
		private C1Carousel.C1Carousel _thumbnails = null;
		private HtmlControl _nextBtn;
		private HtmlControl _prevBtn;
		private HtmlControl _timer;
		private HtmlControl _counter;
		private HtmlControl _pager;
		private PositionSettings _prevPosition;
		private PositionSettings _nextPosition;
		private IEnumerable _data;

		#endregion

		#endregion

		#region ** constructor
		///<summary>
		/// Initializes a new instance of the <seealso cref="C1Gallery"/> class.
		///</summary>
		public C1Gallery()
		{
			//this.IsDesignMode = true;

			VerifyLicense();
			Items.ItemAdded += new Base.Collections.C1ObservableItemCollection<C1Gallery, C1GalleryItem>.CollectionChangedEventHandler(Items_ItemAdded);
			this._thumbnails = new C1Carousel.C1Carousel();

			_prevPosition = new PositionSettings()
			{
				My = { Left = HPosition.Right, Top = VPosition.Center },
				At = { Left = HPosition.Right, Top = VPosition.Center }
			};

			_nextPosition = new PositionSettings()
			{
				My = { Left = HPosition.Left, Top = VPosition.Center },
				At = { Left = HPosition.Left, Top = VPosition.Center }
			};
		}

		protected void Items_ItemAdded(object sender, C1ObservableItemCollection<C1Gallery, C1GalleryItem>.CollectionChangedEventArgs e)
		{
			C1GalleryItem item = e.Item as C1GalleryItem;
			if (item != null)
			{
				item.Gallery = this;
			}
			C1CarouselItem carouselItem = new C1CarouselItem()
			{
				ImageUrl = item.ImageUrl,
				LinkUrl = item.LinkUrl			 
			};
			this._thumbnails.Items.Add(carouselItem);
		}

		#endregion

		#region ** Properties

		/// <summary>
		/// Gets a <see cref="C1GalleryItemCollection"/> 
		/// object that contains the items of the current <see cref="C1Gallery"/> control.
		/// </summary>
		[C1Description("C1Gallery.Items")]
        [C1Category("Category.Default")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[CollectionItemType(typeof(C1GalleryItem))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public C1GalleryItemCollection Items
		{
			get
			{
				if (this._items == null)
				{
					this._items = new C1GalleryItemCollection(this);
				}
				return this._items;
			}
		}

		/// <summary>
		/// Gets or sets the height of the gallery.
		/// </summary>
		public override Unit Height
		{
			get
			{
				return base.Height.IsEmpty ?
					Unit.Parse("300px") : base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the width of the gallery.
		/// </summary>
		public override Unit Width
		{
			get
			{
				return base.Width.IsEmpty ?
					Unit.Parse("600px") : base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether items are loaded on demand.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
        [C1Description("C1Gallery.LoadOnDemand")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public bool LoadOnDemand
		{
			get
			{
				return GetPropertyValue("LoadOnDemand", false);
			}
			set
			{
				SetPropertyValue("LoadOnDemand", value);
			}
		}

		///// <summary>
		///// Gets or sets the value that indicates whether or not Loads on demand is enabled.
		///// </summary>
		//[EditorBrowsable( EditorBrowsableState.Never)]
		//[Browsable(false)]
		//[Layout(LayoutType.Behavior)]
		//[DefaultValue("")]
		//[WidgetOption]
		//public string ThumbsUniqueID
		//{
		//    get
		//    {
		//        return this._thumbnails.UniqueID;
		//    }
		//}

		/// <summary>
		/// Gets the unique, hierarchically qualified identifier for the server control.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		[Layout(LayoutType.Behavior)]
		[DefaultValue("")]
		[WidgetOption]
		public override string UniqueID
		{
			get
			{
				return base.UniqueID;
			}
		}

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region ** DataBind method

		/// <summary>
		/// This method overrides the generic DataBind method to bind the related data to the control.
		/// </summary>
		public sealed override void DataBind()
		{
			base.DataBind();
		}

		// <summary>
		/// Binds data from the data source to the control.
		/// </summary>
		/// <param name="retrievedData">The IEnumerable list of data returned from a PerformSelect method call.</param>
		protected override void PerformDataBinding(IEnumerable data)
		{
			base.PerformDataBinding(data);
			if (IsDesignMode || data == null)
			{
				return;
			}

			this._data = data;
			IEnumerator enumerator = data.GetEnumerator();
			this.Items.Clear();
			int count = 0;			
			if (this.LoadOnDemand)
			{
				this._thumbnails.LoadOnDemand = true;
				this._thumbnails.DataCaptionField = this.DataCaptionField;
				this._thumbnails.DataImageUrlField = this.DataImageUrlField;
				this._thumbnails.DataLinkUrlField = this.DataLinkUrlField;
				this._thumbnails.Data = data;
			}

			while (enumerator.MoveNext())
			{
				object itemData = enumerator.Current;

				C1GalleryItem item = new C1GalleryItem(count);
				if (this.LoadOnDemand)
				{
					if (this.ThumbsDisplay > count)
						GenerateItemFromData(itemData, count, item);
				}
				else
				{
					GenerateItemFromData(itemData, count, item);
				}

				OnItemDataBinding(item);
				this.Items.Add(item);
				OnItemDataBound(item);

				count++;
			}

			this.Controls.Clear();
			this.CreateChildItems();
		}


		/// <summary>
		/// Gets or sets the name of the field from the data source that contains the values to bind to the "src" attribute of each image.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Gallery.DataImageUrlField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataImageUrlField
		{
			get
			{
				return this.GetPropertyValue<string>("DataImageUrlField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataImageUrlField", value);
			}
		}

		/// <summary>
		/// Gets or sets the name of the data source field to bind to the image captions.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Gallery.DataCaptionUrlField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataCaptionField
		{
			get
			{
				return this.GetPropertyValue<string>("DataCaptionUrlField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataCaptionUrlField", value);
			}
		}

		/// <summary>
		/// Gets or sets the name of the data source field to bind to the link.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Gallery.DataLinkUrlField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataLinkUrlField
		{
			get
			{
				return this.GetPropertyValue<string>("DataLinkUrlField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataLinkUrlField", value);
			}
		}

		#endregion

		#region ** Override Methods

		public override void Dispose()
		{
			base.Dispose();
		}

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Called before control initialization.
		/// </summary>
		protected override void OnInit(EventArgs e)
		{

			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
			if (!IsDesignMode && (!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				this.RequiresDataBinding = true;
			}
		}
	

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for
		/// posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if (base.RequiresDataBinding && (!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				this.EnsureDataBound();
			}
			else
			{
				if (!this.IsDesignMode)
				{
					this.CreateChildItems();
				}
				//CreateThumbs();
			}

			if (this.IsDesignMode)
			{
				this.CreateDesignTimeControls();
			}

			if (this.LoadOnDemand)
			{
				Page.ClientScript.GetCallbackEventReference(this, "", "", "");
			}
			base.CreateChildControls();
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}

		protected override void RenderChildren(HtmlTextWriter writer)
		{
			//foreach (Control c in this.Controls)
			//{
			//    if (!(c is C1Carousel.C1Carousel))
			//    {
			//        c.RenderControl(writer);
			//    }				
			//}
			base.RenderChildren(writer);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. 
		/// This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			double frameWidth, frameHeight;

			EnsureID();
			string cssClass = this.CssClass;
			this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass);

			if (this.IsDesignMode)
			{
				this._frame.Attributes["class"] = string.Format("{0} {1} {2}", 
					CSS_WIJ_FRAME, CSS_JQ_CONTENT, CSS_JQ_CLEARFIX);

				if (this.ThumbnailOrientation == Orientation.Vertical) {
					this._thumbnails.Style["float"] = "left";
					this._frame.Style["float"] = "left";
				}			

				if (this.ThumbnailOrientation == Orientation.Horizontal)
				{
					frameWidth = this.Width.Value;
					frameHeight = this.Height.Value - (this.ShowPager ? 0 : this.ThumbsLength) - PADDING_LENGTH;
				}
				else
				{
					frameWidth = this.Width.Value - (this.ShowPager ? 0 : this.ThumbsLength) - PADDING_LENGTH;
					frameHeight = this.Height.Value;
				}

				this._frame.Style["width"] = this._image.Style["width"] 
					= string.Format("{0}px", frameWidth.ToString());
				this._frame.Style["height"] = this._image.Style["height"] 
					= string.Format("{0}px", frameHeight.ToString());
				//if (this.ThumbnailOrientation == Orientation.Horizontal)
				//{
					ApplyPositionStyle(frameWidth, frameHeight, BREADTH, LENGTH, _nextBtn, _nextPosition);
					ApplyPositionStyle(frameWidth, frameHeight, BREADTH, LENGTH, _prevBtn, _prevPosition);
				//}
				//else
				//{
				//    _prevPosition.My.Left = _prevPosition.At.Left =
				//        _nextPosition.At.Left = _nextPosition.My.Left = HPosition.Center;

				//    _prevPosition.My.Top = VPosition.Top;
				//    _prevPosition.At.Top = VPosition.Top;
				//    _nextPosition.My.Top = VPosition.Bottom;
				//    _nextPosition.At.Top = VPosition.Bottom;

				//    ApplyPositionStyle(frameWidth, frameHeight, LENGTH, BREADTH, _prevBtn, _prevPosition);
				//    ApplyPositionStyle(frameWidth, frameHeight, LENGTH, BREADTH, _nextBtn, _nextPosition);
				//}
			}
			
			base.AddAttributesToRender(writer);
			this.CssClass = cssClass;
		}

		#endregion

		#region ** private\internal methods

		#region **** create controls

		/// <summary>
		/// Create items markup to gallery
		/// </summary>
		private void CreateChildItems()
		{
			_itemContainer = new HtmlGenericControl("ul");
			foreach (C1GalleryItem item in this.Items)
			{
				if (item.DisplayVisible)
				{
					_itemContainer.Controls.Add(item);
				}
			}
			this.Controls.Add(_itemContainer);
		}

		/// <summary>
		/// Create the control for design time.
		/// </summary>
		private void CreateDesignTimeControls()
		{
			if (ThumbnailDirection == ThumbsPosition.After)
			{
				CreateFrame();
				CreateThumbs();
			}
			else
			{
				CreateThumbs();
				CreateFrame();
			}
			if (this.ShowPager)
			{
				CreatePager();
			}
		}

		private void CreateThumbs()
		{
			if (this.ShowPager)
			{
				return;
			}

			//this._thumbnails.Interval = this.Interval;
			this._thumbnails.Orientation = this.ThumbnailOrientation;
			this._thumbnails.Display = this.ThumbsDisplay;
			if (this.ThumbnailOrientation == Orientation.Horizontal)
			{
				this._thumbnails.Height = this.ThumbsLength;
				this._thumbnails.Width = (int)(this.Width.Value - PADDING_BREADTH);				
			}
			else 
			{
				this._thumbnails.Height = (int)(this.Height.Value - PADDING_BREADTH);
				this._thumbnails.Width = this.ThumbsLength;				
			}
			
			this.Controls.Add(this._thumbnails);
		}

		private void CreateFrame()
		{
			this._frame = new HtmlGenericControl("DIV");
			CreateContent();
			CreateButtons();
			if (this.ShowTimer)
			{
				CreateTimer();
			}
			if (this.ShowCounter)
			{
				CreateCounter();
			}

			this.Controls.Add(this._frame);
		}

		private void CreateContent()
		{
			this._content = new HtmlGenericControl("DIV");
			this._content.Attributes["class"] = CSS_WIJ_CONTENT;
			CreateImage();
			if (this.ShowCaption)
			{
				CreateText();
			}
			this._frame.Controls.Add(this._content);
		}

		/// <summary>
		/// Create the caption of the frame..
		/// </summary>
		private void CreateText()
		{
			this._text = new HtmlGenericControl("DIV");
			this._text.Attributes["class"] = string.Format("{0} {1} {2}",
				CSS_JQ_CONTENT, CSS_JQ_CLEARFIX, CSS_WIJ_CAPTION);
			this._text.Style.Add(HtmlTextWriterStyle.Color, "#F1F1F1");
			this._text.Style.Add(HtmlTextWriterStyle.Width,
				"100%");
			if (this.Items.Count > 0 && !string.IsNullOrEmpty(this.Items[0].Caption))
			{
				this._text.InnerText = this.Items[0].Caption;
			}
			this._content.Controls.Add(this._text);
		}

		/// <summary>
		/// Create image view of the frame.
		/// </summary>
		private void CreateImage()
		{
			HtmlImage _img = new HtmlImage();
			this._image = new HtmlGenericControl("DIV");
			this._image.Attributes["class"] = string.Format("{0} {1}", 
				CSS_JQ_OVERLAY, CSS_WIJ_CURRENT);

			if (this.Items.Count > 0)
			{
				_img.Src = this.Items[0].LinkUrl;
				this._image.Controls.Add(_img);
			}
			this._content.Controls.Add(this._image);
		}

		/// <summary>
		/// Create counter of the frame..
		/// </summary>
		private void CreateCounter()
		{
			this._counter = new HtmlGenericControl("div");
			HtmlGenericControl innerText = new HtmlGenericControl("span");
			innerText.InnerText = this.Counter.Replace("[","").Replace("]","");
			this._counter.Attributes.Add("class", 
				string.Format("{0} {1}", CSS_WIJ_COUNTER, CSS_JQ_DEFAULT));
			this._counter.Controls.Add(innerText);
			this._counter.Style.Add(HtmlTextWriterStyle.WhiteSpace, "nowrap");
			this._frame.Controls.Add(this._counter);
		}

		/// <summary>
		/// Create pager instead of thumbs.
		/// </summary>
		private void CreatePager()
		{
			this._pager = new HtmlGenericControl("div");
			C1TargetControlBase control;
			control = new C1Pager.C1Pager()
			{
				PageCount = this.Items.Count == 0 ? 1 : this.Items.Count
			};
			this._pager.Controls.Add(control);
			this.Controls.Add(this._pager);
		}

		/// <summary>
		/// Create previous and next buttons.
		/// </summary>
		private void CreateButtons()
		{
			//string nextBtnClass = 
			//    this.ThumbnailOrientation == Wijmo.Controls.Orientation.Horizontal ?
			//    CSS_UIICON_TRIANGLE_E : CSS_UIICON_TRIANGLE_S;
			//string prevBtnClass = 
			//    this.ThumbnailOrientation == Wijmo.Controls.Orientation.Horizontal ?
			//    CSS_UIICON_TRIANGLE_W : CSS_UIICON_TRIANGLE_N;
			//this._nextBtn = CreateButton(CSS_WIJ_BUTTON_NEXT, nextBtnClass);
			//this._prevBtn = CreateButton(CSS_WIJ_BUTTON_PREVIOUS, prevBtnClass);

			this._nextBtn = CreateButton(CSS_WIJ_BUTTON_NEXT, CSS_UIICON_TRIANGLE_E);
			this._prevBtn = CreateButton(CSS_WIJ_BUTTON_PREVIOUS, CSS_UIICON_TRIANGLE_W);


			this._frame.Controls.Add(this._nextBtn);
			this._frame.Controls.Add(this._prevBtn);
		}

		private HtmlGenericControl CreateButton(string btnClass, string iconClass)
		{
			HtmlGenericControl button = new HtmlGenericControl("a");
			HtmlGenericControl icon = new HtmlGenericControl("span");

			button.Attributes["class"] = string.Format("{0} {1}", 
				CSS_JQ_DEFAULT, btnClass);
			icon.Attributes["class"] = string.Format("{0} {1}", 
				CSS_JQ_UIICON, iconClass);
			button.Controls.Add(icon);

			return button;
		}

		/// <summary>
		/// Create timer of the frame.
		/// </summary>
		private void CreateTimer()
		{
			HtmlGenericControl progress = new HtmlGenericControl("div");
			HtmlGenericControl button = CreateButton(CSS_WIJ_BUTTON,
				CSS_UIICON_PLAY);
			this._timer = new HtmlGenericControl("div");
			this._timer.Attributes["class"] = CSS_WIJ_TIMER;
			progress.Attributes["class"] = CSS_WIJ_TIMER_INNER;

			this._timer.Controls.Add(progress);
			this._timer.Controls.Add(button);
			this._frame.Controls.Add(this._timer);
		}

		#endregion

		#region **** internal methods
		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Gallery), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		#region **** apply styles

		/// <summary>
		/// Determine compound css class.
		/// </summary>
		/// <returns></returns>
		private string DetermineCompoundCssClass()
		{
			if (!this.IsDesignMode)
			{
				return Utils.GetHiddenClass();
			}
			string sCompoundCssClass = "{0} {1}";
			return string.Format(sCompoundCssClass, CSS_WIJ_BASE, CSS_JQ_WIDGET);
		}

		#endregion

		#region **** Add style for control

		/// <summary>
		/// calculate left
		/// </summary>
		/// <returns>left</returns>
		private double ApplyLeft(double pWidth, double cWidth, PositionSettings position)
		{
			double left;
			//VPosition.Bottom
			switch (position.At.Left)
			{
				case HPosition.Center:
					left = pWidth / 2;
					break;
				case HPosition.Right:
					left = pWidth;
					break;
				case HPosition.Left:
				default:
					left = 0;
					break;
			}
			switch (position.My.Left)
			{
				case HPosition.Center:
					left -= cWidth / 2;
					break;
				case HPosition.Right:
					left -= cWidth;
					break;
				case HPosition.Left:
				default:
					break;
			}
			left = left + position.Offset.Left;
			return left;
		}

		/// <summary>
		/// calculate top
		/// </summary>
		/// <returns>top</returns>
		private double ApplyTop(double pHeight, double cHeight, PositionSettings position)
		{
			double top;
			//VPosition.Bottom
			switch (position.At.Top)
			{
				case VPosition.Center:
					top = pHeight / 2;
					break;
				case VPosition.Bottom:
					top = pHeight;
					break;
				case VPosition.Top:
				default:
					top = 0;
					break;
			}
			switch (position.My.Top)
			{
				case VPosition.Center:
					top -= cHeight / 2;
					break;
				case VPosition.Bottom:
					top -= cHeight;
					break;
				case VPosition.Top:
				default:
					break;
			}
			top = top + position.Offset.Top;
			return top;
		}

		/// <summary>
		/// Apply position for Web Control, for jquery.ui.position.js at design time.
		/// </summary>
		private void ApplyPositionStyle(double pWidth, double pHeight,
			double cWidth, double cHeight, HtmlControl control,
			PositionSettings position)
		{
			if (control == null)
			{
				return;
			}
			double left, top;
			left = ApplyLeft(pWidth, cWidth, position);
			top = ApplyTop(pHeight, cHeight, position);

			control.Style.Add(HtmlTextWriterStyle.Position, "absolute");
			control.Style.Add(HtmlTextWriterStyle.Left, left.ToString() + "px");
			control.Style.Add(HtmlTextWriterStyle.Top, top.ToString() + "px");
		}

		/// <summary>
		/// Apply position for Web Control, for jquery.ui.position.js in design time.
		/// </summary>
		private void ApplyPositionStyle(WebControl pControl,
			double cWidth, double cHeight, HtmlControl control,
			PositionSettings position)
		{
			ApplyPositionStyle(pControl.Width.Value,
				pControl.Height.Value,
				cWidth, cHeight,
				control,
				position);
		}

		#endregion

		/// <summary>
		/// Generate <see cref="C1GalleryItem"/> item from the item data in data source.
		/// </summary>
		/// <param name="itemData">Item data that gets from data source. </param>
		/// <param name="index">Index of the <see cref="C1GalleryItem"/>.</param>
		/// <param name="item">Instance of <see cref="C1GalleryItem"/>.</param>
		/// <returns>The Generated <see cref="C1GalleryItem"/></returns>
		private C1GalleryItem GenerateItemFromData(object itemData, int index, C1GalleryItem item)
		{
			item.DataItem = itemData;

			if (!String.IsNullOrEmpty(this.DataImageUrlField))
			{
				item.ImageUrl = DataBinder.GetPropertyValue(itemData, this.DataImageUrlField).ToString();
			}
			else
			{
				item.ImageUrl = GetValueByIndex(0, itemData);
			}

			if (!String.IsNullOrEmpty(this.DataLinkUrlField))
			{
				item.LinkUrl = DataBinder.GetPropertyValue(itemData, this.DataLinkUrlField).ToString();
			}
			else
			{
				item.LinkUrl = GetValueByIndex(1, itemData);
			}

			if (!String.IsNullOrEmpty(this.DataCaptionField))
			{
				item.Caption = DataBinder.GetPropertyValue(itemData, this.DataCaptionField).ToString();
			}
			else
			{
				item.Caption = GetValueByIndex(2, itemData);
			}

			item.Index = index;
			item.IsLoaded = true;

			return item;
		}


		/// <summary>
		/// get value from data source.
		/// </summary>
		/// <returns>value if the item</returns>
		private string GetValueByIndex(int index, object dataItem)
		{
			PropertyDescriptorCollection props = TypeDescriptor.GetProperties(dataItem);
			string contentContent = String.Empty;

			if (props.Count >= 2)
			{
				if (null != props[1].GetValue(dataItem))
				{
					contentContent = props[1].GetValue(dataItem).ToString();
				}
			}
			return contentContent;
		}

		#endregion

		#region ** Events

		///<summary>
		/// Occurs before <see cref="C1GalleryItem"/>  being created in data binding.
		///</summary>
		[C1Category("Category.Misc")]
		[C1Description("C1Gallery.ItemDataBinding")]
		public event C1GalleryItemEventHandler ItemDataBinding;

		private void OnItemDataBinding(C1GalleryItem item)
		{
			if (ItemDataBinding != null)
			{
				C1GalleryItemEventArgs e = new C1GalleryItemEventArgs(item);
				ItemDataBinding(this, e);
			}
		}

		///<summary>
		/// Occurs after <see cref="C1GalleryItem"/> is created in data binding.
		///</summary>
		[C1Category("Category.Misc")]
		[C1Description("C1Gallery.ItemDataBound")]
		public event C1GalleryItemEventHandler ItemDataBound;

		private void OnItemDataBound(C1GalleryItem item)
		{
			if (ItemDataBound != null)
			{
				C1GalleryItemEventArgs e = new C1GalleryItemEventArgs(item);
				ItemDataBound(this, e);
			}
		}

		#endregion

		#region ** ICallBackEventHandler

		private string _response;

		/// <summary>
		/// Implement of ICallbackEventHandler.GetCallbackResult
		/// </summary>
		string ICallbackEventHandler.GetCallbackResult()
		{
			return _response;
		}

		/// <summary>
		/// Implement of ICallbackEventHandler.RaiseCallbackEvent
		/// </summary>
		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			try
			{
				Hashtable data = JsonHelper.StringToObject(eventArgument);
				string commandName = (string)data["CommandName"];
				ArrayList commandData = data["CommandData"] as ArrayList;
				_response = ProcessAjaxCommand(commandName, commandData);
			}
			catch (Exception ex)
			{
				_response = ex.Message;
			}
		}

		/// <summary>
		/// Identify the action to take as response to the client request
		/// </summary>
		private string ProcessAjaxCommand(string commandName, ArrayList commandData)
		{
			if (commandName == "GetItems")
			{
				object callbackData = LoadRequiredData(commandData);
				return JsonHelper.WidgetToString(callbackData, this);
			}
			return "";
		}

		/// <summary>
		/// Load callback data of specified nodes.
		/// </summary>
		private IEnumerable LoadRequiredData(ArrayList idxs)
		{
			EnsureDataBound();
			IList<object> bindItems = new List<object>() { };

			int count = 0;
			if (this._data == null) 
				//when use update panel, the data is lost after callback, 
				//so need bind() again.
				//fixed bug 
			{
				base.DataBind();
				if (this._data == null)
				{
					return null;
				}
			}
			foreach (object itemData in this._data)
			{
				if (idxs.Contains(count))
				{
					C1CarouselItem item = this._thumbnails.Items[count];
					this._thumbnails.GenerateItemFromData(itemData, count, item);

					bindItems.Add(item);
				}
				count++;
			}

			return bindItems;
		}	

		#endregion

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for a <see cref="C1Gallery"/> control.
		/// </summary>
		/// <param name="postDataKey">
		/// The key identifier for the control.
		/// </param>
		/// <param name="postCollection">
		/// The collection of all incoming name values.
		/// </param>
		/// <returns>
		/// Returns true if the server control's state changes as a result of the postback;
		/// otherwise, false.
		/// </returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		/// <summary>
		/// Signals the <see cref = "C1Splitter"/> control to notify the ASP.NET
		/// application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations

		#region ** IC1Serializable

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1GallerySerializer sz = new C1GallerySerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1GallerySerializer sz = new C1GallerySerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1GallerySerializer sz = new C1GallerySerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1GallerySerializer sz = new C1GallerySerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}

		#endregion
	}
}
