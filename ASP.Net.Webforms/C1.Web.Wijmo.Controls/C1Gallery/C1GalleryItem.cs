﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;
using System.Web;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Gallery
{
	/// <summary>
    /// Items within the gallery control.
	/// </summary>
    [ToolboxItem(false)]
	public class C1GalleryItem : UIElement, IDataItemContainer
	{
		#region ** fields

		private const string CSS_ITEM = "wijmo-wijgallery-item";

		private int _dataItemIndex;
		private Hashtable _properties = new Hashtable();
		private Image _image;
		private HtmlGenericControl _link;
		private HtmlGenericControl _caption;

		#endregion

		#region ** constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1GalleryItem"/> class.
        /// </summary>
		public C1GalleryItem()
		{
			//this.IsDesignMode = true;
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="C1GalleryItem"/> class.
        /// </summary>
		public C1GalleryItem(int index)
		{
			_dataItemIndex = index;
		}

		#endregion

		#region ** properties

        /// <summary>
        /// Gets or sets the underlying data object that the C1GalleryItem object is bound to.
        /// </summary>
		public virtual object DataItem
		{
			get;
			protected internal set;
		}

		/// <summary>
		/// Get the gallery
		/// </summary>
		internal C1Gallery Gallery
		{
			get;
			set;
		}

		/// <summary>
        /// Gets or sets the image URL of gallery item.
		/// </summary>
		[WidgetOption]
		[C1Description("C1GalleryItem.ImageUrl", "Gets or sets the image URL of gallery item.")]
		[DefaultValue("")]
        [Editor("System.Web.UI.Design.ImageUrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
		[UrlProperty()]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string ImageUrl
		{
			get
			{
				return _properties["ImageUrl"] == null ? "" : _properties["ImageUrl"].ToString();
			}
			set
			{
				_properties["ImageUrl"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the index of gallery item.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[DefaultValue(0)]
		public int Index
		{
			get
			{
				return _properties["Index"] == null ? 0 : (int)_properties["Index"];
			}
			set
			{
				_properties["Index"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the gallery item's caption.
		/// </summary>
		[WidgetOption]
        [C1Description("C1GalleryItem.Caption", "Gets or sets the gallery item's caption.")]
		[DefaultValue("")]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string Caption
		{
			get
			{
				return _properties["Caption"] == null ? "" : _properties["Caption"].ToString();
			}
			set
			{
				_properties["Caption"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the link URL for the gallery item.
		/// </summary>
		[WidgetOption]
        [C1Description("C1GalleryItem.LinkUrl", "Gets or sets the link URL for the gallery item.")]
        [Editor("System.Web.UI.Design.UrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
		[UrlProperty()]
		[DefaultValue("")]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string LinkUrl
		{
			get
			{
				return _properties["LinkUrl"] == null ? "" : _properties["LinkUrl"].ToString();
			}
			set
			{
				_properties["LinkUrl"] = value;
			}
		}

		/// <summary>
        /// Gets or sets the image's title.
		/// </summary>
        [C1Description("C1GalleryItem.Title", "Gets or sets the image's title.")]
		[DefaultValue("")]
        [C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		public string Title
		{
			get
			{
				return _properties["Title"] == null ? "" : _properties["Title"].ToString();
			}
			set
			{
				_properties["Title"] = value;
			}
		}

		/// <summary>
        /// Gets or sets whether the gallery item is loaded (on call back mode).
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[DefaultValue(false)]
		public bool IsLoaded
		{
			get
			{
				return _properties["IsLoad"] == null ? false : (bool)_properties["IsLoad"];
			}
			set
			{
				_properties["IsLoad"] = value;
			}
		}

        [WidgetOption]
        [DefaultValue(true)]
        [Layout(LayoutType.Appearance)]
        public override bool Visible
        {
            get
            {
                return _properties["Visible"] == null ? true : (bool)_properties["Visible"];
            }
            set
            {
                _properties["Visible"] = value;
            }
        }

        /// <summary>
        /// Hide this property.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
            }
        }

		#endregion

		#region ** Override Methods

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Li;
			}
		}

		/// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based
        /// implementation to create any child controls they contain in preparation for
        /// posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			if (!string.IsNullOrEmpty(this.ImageUrl))
			{
                //fixed bug 32082 by Daniel.He 2013/3/22
				_image = new Image();
				//_image.Src = base.ResolveClientUrl(this.ImageUrl);
                _image.Attributes.Add("src", base.ResolveClientUrl(this.ImageUrl));
				_image.Attributes.Add("title", this.Title);
			}

			if (!string.IsNullOrEmpty(this.LinkUrl))
			{
				_link = new HtmlGenericControl("a");
				_link.Attributes.Add("href", base.ResolveClientUrl(this.LinkUrl));
				this.Controls.Add(_link);
				if (_image != null)
					_link.Controls.Add(_image);
			}
			else if (_image != null)
			{
				this.Controls.Add(_image);
			}

			if (!string.IsNullOrEmpty(this.Caption))
			{
				_caption = new HtmlGenericControl("span");
				_caption.InnerText = this.Caption;
				this.Controls.Add(_caption);
			}

			base.CreateChildControls();
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. 
		/// This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
		}

		#endregion	

		#region ** IDataItemContainer interface implementations
		/// <summary>
		/// Gets DataItem of this C1GalleryItem.
		/// </summary>
		[Browsable(false)]
		object IDataItemContainer.DataItem
		{
			get { return this.DataItem; }
		}

		/// <summary>
		/// Gets DataItemIndex of this C1GalleryItem.
		/// </summary>
		[Browsable(false)]
		int IDataItemContainer.DataItemIndex
		{
			get { return this._dataItemIndex; }
		}

		/// <summary>
		/// Gets DisplayIndex of this C1GalleryItem.
		/// </summary>
		[Browsable(false)]
		int IDataItemContainer.DisplayIndex
		{
			get { return this._dataItemIndex; }
		}

		#endregion end of ** IDataItemContainer interface implementations.
	}
}
