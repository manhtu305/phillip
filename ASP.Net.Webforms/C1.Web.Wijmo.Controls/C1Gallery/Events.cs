﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Gallery
{
	///<summary>
    ///Represents the method that will handle an data binding events of C1GalleryItem, 
    ///such as the C1Gallery.ItemDataBound and C1Gallery.ItemDataBinding events.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">An <see cref="C1GalleryItemEventArgs"/> that contains event data.</param>
	public delegate void C1GalleryItemEventHandler(object sender, C1GalleryItemEventArgs args);

	/// <summary>
    ///Contains event data for data binding events, 
    ///such as the C1Gallery.ItemDataBound and C1Gallery.ItemDataBinding events.
    /// </summary>
	public class C1GalleryItemEventArgs : EventArgs
	{
		private C1GalleryItem _item;

		/// <summary>
		/// Gets the <see cref="C1GalleryItem"/> related with this event.
		/// </summary>
		public C1GalleryItem Item
		{
			get
			{
				return this._item;
			}
		}

		/// <summary>
		/// Initializes a new instance of the C1GalleryItem class.
		/// </summary>
		/// <param name="item"><see cref="C1GalleryItem"/> related with this event</param>
		public C1GalleryItemEventArgs(C1GalleryItem item)
		{
			this._item = item;
		}

	}
}
