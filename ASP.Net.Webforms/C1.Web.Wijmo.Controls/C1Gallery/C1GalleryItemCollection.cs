﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Gallery
{
	/// <summary>
	/// Represents the structure that manages the <see cref="C1GalleryItem"/> items.
	/// </summary>
	public class C1GalleryItemCollection : C1ObservableItemCollection<C1Gallery, C1GalleryItem>
	{

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1GalleryItemCollection"/> class.
		/// </summary>
		/// <param name="owner">Parent ComboBox.</param>
		public C1GalleryItemCollection(C1Gallery owner)
			: base(owner)
		{
			//add
		}

		/// <summary>
		/// Adds <see cref="C1GalleryItem"/> node to the collection.
		/// </summary>
		/// <param name="item">item to add.</param>
		public new void Add(C1GalleryItem item)
		{
			if (item != null)
			{
				Insert(base.Count, item);
			}
		}

		/// <summary>
		/// Insert a <see cref="C1GalleryItem"/> item into a specific position in the collection.
		/// </summary>
		/// <param name="index">Item position. Value should be greater or equal to 0</param>.
		/// <param name="child">The new <see cref="C1GalleryItem"/> item</param>.
		public new void Insert(int index, C1GalleryItem child)
		{
			if (this.Count < index)
				this.InsertItem(this.Count, child);
			else
				this.InsertItem(index, child);
		}

		#endregion end of ** constructor
	}
}
