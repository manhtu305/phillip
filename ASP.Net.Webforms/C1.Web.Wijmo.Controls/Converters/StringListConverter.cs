﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Converters
{

	/// <summary>
	/// Provides a type converter to convert string objects to and from other representations.
	/// </summary>
	public abstract class StringListConverter : StringConverter
	{
		private StandardValuesCollection _common = null;
		private bool _exclusive = false;
		
		/// <summary>
		/// Sets the list.
		/// </summary>
		/// <param name="exclusive">if set to <c>true</c> [exclusive].</param>
		/// <param name="values">The values.</param>
		protected void SetList(bool exclusive, string[] values)
		{
			_common = new StandardValuesCollection(values);
			_exclusive = exclusive;
		}
		/// <summary>
		/// Gets the standard values supported.
		/// </summary>
		/// <param name="ctx">The CTX.</param>
		/// <returns></returns>
		public override bool GetStandardValuesSupported(ITypeDescriptorContext ctx)
		{
			return _common != null;
		}
		/// <summary>
		/// Gets the standard values exclusive.
		/// </summary>
		/// <param name="ctx">The CTX.</param>
		/// <returns></returns>
		public override bool GetStandardValuesExclusive(ITypeDescriptorContext ctx)
		{
			return _exclusive;
		}
		/// <summary>
		/// Gets the standard values.
		/// </summary>
		/// <param name="ctx">The CTX.</param>
		/// <returns></returns>
		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext ctx)
		{
			return _common;
		}
	}

}
