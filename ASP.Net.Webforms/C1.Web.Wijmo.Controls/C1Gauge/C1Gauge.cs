﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Gauge
{
	/// <summary>
	/// The Gauge is a web control that shows a gauge in web page.
	/// </summary>
	//[ToolboxItem(false)]
	public abstract partial class C1Gauge : C1TargetControlBase, IPostBackDataHandler
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="C1Gauge"/> class.
		/// </summary>
		public C1Gauge()
			: base()
		{
			this.InitProperties();

			if (this.Pointer.Template != null)
			{
				this.InnerStates["template"] = this.Pointer.Template;
			}

			if (this.Face != null)
			{
				this.InnerStates["face"] = this.Face;
			}
		}

		#region ** fields
		private Nullable<double> _asynPostBackOldValue;
		#endregion

		#region ** Options
		/// <summary>
		/// A value that indicates the gauge's width.
		/// </summary>
		[C1Description("C1Gauge.Width")]
		[DefaultValue(typeof(Unit), "600")]
		[C1Category("Category.Layout")]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// A value that indicates the gauge's height.
		/// </summary>
		[DefaultValue(typeof(Unit), "400")]
		[C1Description("C1Gauge.Height")]
		[C1Category("Category.Layout")]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}


		[WidgetOption]
		[WidgetOptionName("oldValue")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public Nullable<double> AsynPostBackOldValue
		{
			get
			{
				return this._asynPostBackOldValue;
			}
		}
		#endregion

		#region ** Override methods
		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value that corresponds to this Web server control. This property is used primarily by control developers.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// One of the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> enumeration values.
		/// </returns>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		#endregion

		#region ** IPostBackDataHandler interface implementations
		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);
			this._asynPostBackOldValue = this.Value;

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{

		}
		#endregion

		#region ** Methods
		/// <summary>
		/// Determine whether the AsynPostBackOldValue property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if the postback comes from updatepanel.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAsynPostBackOldValue()
		{
			return IsUpdatePanelPostBack() && Animation.Enabled == true;
		}


		private bool IsUpdatePanelPostBack()
		{
			var scriptManager = (ScriptManager)this.GetScriptManager();

			if (scriptManager != null && scriptManager.IsInAsyncPostBack)
			{
				Control control = this.Parent;
				while (control != null)
				{
					if (control is UpdatePanel)
					{
						return true;
					}
					control = control.Parent;
				}
			}
			return false;
		}

		private ScriptManager GetScriptManager()
		{
			Page page = this.Page;

			if (page == null)
				throw new InvalidOperationException("Page cannot be null");

			return ScriptManager.GetCurrent(page);
		}
		#endregion
	}
}
