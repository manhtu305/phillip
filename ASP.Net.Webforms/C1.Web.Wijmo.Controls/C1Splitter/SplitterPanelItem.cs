using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1Splitter
{

	/// <summary>
	/// This class represents template container of SplitterPanel.
	/// </summary>
	[ToolboxItem(false)]
	public class SplitterPanelItem : WebControl, INamingContainer
	{

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This
		/// method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
		}

		/// <summary>
		/// Renders the HTML closing tag of the control into the specified writer. This
		/// method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{
		}
	}
}
