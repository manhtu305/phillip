﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1Splitter
{

	/// <summary>
	/// Defines the information for panels of splitter.
	/// </summary>
	public partial class SplitterPanel
	{

		#region ** fields
		private ITemplate _contentTemplate;
		private Control _childControl = null;
		#endregion end of ** fields.

		#region ** properties
		/// <summary>
		/// Gets or sets the template for the content area of the C1Splitter control. 
		/// </summary> 
		[Browsable(false)]
		[MergableProperty(false)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateContainer(typeof(SplitterPanelItem))]
		[TemplateInstance(TemplateInstance.Single)]
		public ITemplate ContentTemplate
		{
			get
			{
				return this._contentTemplate;
			}
			set
			{
				this._contentTemplate = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public Control ChildControl
		{
			get
			{
				return this._childControl;
			}
			set
			{
				this._childControl = value;
			}
		}
		#endregion end of ** properties.
	}
}
