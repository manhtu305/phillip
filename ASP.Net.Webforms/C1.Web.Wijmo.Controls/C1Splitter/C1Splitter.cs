using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Resources;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Drawing.Design;
using System.IO;
using System.Web;
using System.Web.UI.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Splitter
{
	
	/// <summary>
	/// C1Slitter is a container control that consists a movable bar that divides a container's display area into two resizable panels.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Splitter.C1SplitterDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Splitter.C1SplitterDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Splitter.C1SplitterDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Splitter.C1SplitterDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[LicenseProviderAttribute()]
	[ToolboxData("<{0}:C1Splitter runat=server ></{0}:C1Splitter>")]
	[ToolboxBitmap(typeof(C1Splitter), "C1Splitter.png")]
	public partial class C1Splitter : C1TargetControlBase, INamingContainer, IC1Serializable, IPostBackDataHandler
	{

		#region ** fields
		#region ** private members
		private Panel wrapper;
		private Panel panel1;
		private Panel panel2;
		private Panel bar;
		private Panel expander;
		private SplitterPanelItem item1;
		private SplitterPanelItem item2;
		private HtmlGenericControl expanderIcon;

		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion end of ** private members.

		#region ** constant members
		private const int CONST_BAR_SIZE = 2;

		#region ** constant css class
		private const string CONST_WIJSPLITTER_PREFIX = "wijmo-wijsplitter-";
		private const string CONST_WIJSPLITTER_WRAPPER = CONST_WIJSPLITTER_PREFIX + "wrapper";
		private const string CONST_WIJSPLITTER_HORIZONTAL = CONST_WIJSPLITTER_PREFIX + "horizontal";
		private const string CONST_WIJSPLITTER_VERTICAL = CONST_WIJSPLITTER_PREFIX + "vertical";
		private const string CONST_WIJSPLITTER_HORIZONTAL_PREFIX = CONST_WIJSPLITTER_PREFIX + "h-";
		private const string CONST_WIJSPLITTER_VERTICAL_PREFIX = CONST_WIJSPLITTER_PREFIX + "v-";
		private const string CONST_WIJSPLITTER_PANEL1 = "panel1";
		private const string CONST_WIJSPLITTER_PANEL2 = "panel2";
		private const string CONST_WIJSPLITTER_BAR = "bar";
		private const string CONST_WIJSPLITTER_EXPANDER = "expander";
		private const string CONST_WIJSPLITTER_COLLAPSED = "collapsed";
		private const string CONST_WIJSPLITTER_EXPANDED = "expanded";
		private const string CONST_WIJSPLITTER_CONTENT = "content";
		private const string CONST_WIDGET_CONTENT = "ui-widget-content";
		private const string CONST_WIDGET_HEADER = "ui-widget-header";
		private const string CONST_CORNER_TL = "ui-corner-tl";
		private const string CONST_CORNER_TR = "ui-corner-tr";
		private const string CONST_CORNER_BL = "ui-corner-bl";
		private const string CONST_STATE_DEFAULT = "ui-state-default";
		private const string CONST_ICON = "ui-icon";
		private const string CONST_ICON_ARROW = CONST_ICON + "-triangle-1-";

		//Add comments by RyanWu@20110317.
		//This class is used to indicate that the control is a splitter control.
		//When a splitter is put into another splitter, because the child splitter 
		//is first created, but if the splitter is set to full-split mode, then 
		//the splitter maybe will have an incorrect size(width and height) for its
		//parent hasn't been initialized.  So we need use this label to judget whether
		//the current splitter is put in another splitter.
		private const string CONST_C1SPLITTER = "__c1splitter";
		//end by RyanWu@20110317.
		#endregion end of ** constant css class.
		#endregion end of ** constant members.
		#endregion end of ** fields.

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1Splitter"/> class.
		/// </summary>
		public C1Splitter()
		{
			VerifyLicense();

			this.InitSplitter();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1Splitter"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public C1Splitter(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			// ttZzKzJ5mwNr/IihpBl2pA==
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Splitter), this,
				Assembly.GetExecutingAssembly(), key);
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif

			this.InitSplitter();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Splitter), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#region ** initialize splitter
		private void InitSplitter()
		{
			base.Width = Unit.Pixel(400);
			base.Height = Unit.Pixel(250);
		}
		#endregion end of ** initialize splitter.
		#endregion end of ** constructor.

		#region ** properties
		#region ** private properties
		private int InnerSplitterDistance
		{
			get
			{
				if (this.Panel1.Collapsed)
				{
					return 2;
				}
				else if (this.Panel2.Collapsed)
				{
					return (int)(this.Orientation == Orientation.Horizontal ?
						this.Height.Value : this.Width.Value) - CONST_BAR_SIZE - 8;
				}

				return this.SplitterDistance;
			}
		}
		#endregion end of ** private properties.

		#region ** override properties
		/// <summary>
		/// Gets or sets if C1Splitter is enabled.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Splitter.Enabled", "Gets or sets if C1Splitter is enabled.")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
		/// Gets or sets the width of the splitter.
		/// </summary>
		[DefaultValue(typeof(Unit), "400")]
		[Layout(LayoutType.Sizes)]
		[RefreshProperties(RefreshProperties.All)]
		[WidgetOption]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;

				if (this.Orientation == Orientation.Vertical)
				{
					this.UpdateSplitterDistanceWithSizeChanged((int)value.Value);
				}
			}
		}

		/// <summary>
		/// Gets or sets the height of the splitter.
		/// </summary>
		[DefaultValue(typeof(Unit), "250")]
		[Layout(LayoutType.Sizes)]
		[RefreshProperties(RefreshProperties.All)]
		[WidgetOption]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;

				if (this.Orientation == Orientation.Horizontal)
				{
					this.UpdateSplitterDistanceWithSizeChanged((int)value.Value);
				}
			}
		}
		#endregion end of ** override properties.

		#region ** protected properties
		/// <summary>
		/// Gets the outmost tag key of the splitter.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}
		#endregion end of ** protected properties.
		#endregion end of ** properties.

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region ** methods
		#region ** private methods
		private void UpdateSplitterDistanceWithSizeChanged(int value)
		{
			if (value == 0)
			{
				this.SplitterDistance = 0;

				if (this.IsDesignMode)
				{
					this.Style[HtmlTextWriterStyle.Overflow] = "hidden";
				}
			}
			else
			{
				if (value < this.SplitterDistance + CONST_BAR_SIZE)
				{
					this.SplitterDistance = value - CONST_BAR_SIZE;
				}
			}
		}

		private string GetSplitterClassByOriention()
		{
			return this.Orientation == Orientation.Horizontal ?
					CONST_WIJSPLITTER_HORIZONTAL_PREFIX :
					CONST_WIJSPLITTER_VERTICAL_PREFIX;
		}

		private string GetCornerClassByOrientation()
		{
			return this.Orientation == Orientation.Horizontal ?
					CONST_CORNER_TR : CONST_CORNER_BL;
		}

		private string GetExpanderIconClass()
		{
			return this.Orientation == Orientation.Horizontal ?
					(this.Panel1.Collapsed ? "s" : "n") :
					(this.Panel1.Collapsed ? "e" : "w");
		}

		/// <summary>
		/// Determine compound css class.
		/// </summary>
		/// <returns>Returns the css string.</returns>
		private string DetermineCompoundCssClass()
		{
			if (!this.IsDesignMode)
			{
				return string.Format("{0} {1}", CONST_C1SPLITTER, Utils.GetHiddenClass());
			}

			string cssClass = string.Empty;

			if (this.Orientation == Orientation.Horizontal)
			{
				return string.Format("{0} {1}{2}",
					CONST_WIJSPLITTER_HORIZONTAL,
					CONST_WIJSPLITTER_HORIZONTAL_PREFIX,
					this.Panel1.Collapsed ? CONST_WIJSPLITTER_COLLAPSED :
					CONST_WIJSPLITTER_EXPANDED);
			}

			return string.Format("{0} {1}{2}",
					CONST_WIJSPLITTER_VERTICAL,
					CONST_WIJSPLITTER_VERTICAL_PREFIX,
					this.Panel1.Collapsed ? CONST_WIJSPLITTER_COLLAPSED :
					CONST_WIJSPLITTER_EXPANDED);
		}
		#endregion end of ** private methods.

		#region ** create child controls
		/// <summary>
		/// Create a container for the control.
		/// </summary>
		/// <returns>Return a Web control.</returns>
		private Control CreateWrapper()
		{
			wrapper = new Panel();

			return wrapper;
		}

		/// <summary>
		/// Creates the panel1 element.
		/// </summary>
		/// <returns>Return a Panel instance.</returns>
		private Panel CreatePanel1()
		{
			panel1 = new Panel();

			if (this.IsDesignMode)
			{
				panel1.ScrollBars = this.Panel1.ScrollBars;
				panel1.Attributes.Add("{0}", "0");

				//Add comments by RyanWu@20100904.
				//I must provide a ChildControl property to allow user to set child control of the panel1/panel2,
				//In vs2010 we can't use the intantiatein method to instantiate a ITemplate control(#11251).
				//So we must provide an alternative one.
				if (this.Panel1.ChildControl != null)
				{
					panel1.Controls.Add(this.Panel1.ChildControl);
				}
				//end by RyanWu@20100904.

				LiteralControl lc = new LiteralControl("<span style=\"color:White;\">.</span>");
				panel1.Controls.Add(lc);
			}
			else
			{
				item1 = new SplitterPanelItem();

				if (this.Panel1.ContentTemplate != null)
				{
					this.Panel1.ContentTemplate.InstantiateIn(item1);
					panel1.Controls.Add(item1);
				}
			}

			return panel1;
		}

		/// <summary>
		/// Creates the panel2 element.
		/// </summary>
		/// <returns>Return a ContentPanel.</returns>
		private Panel CreatePanel2()
		{
			panel2 = new Panel();

			if (this.IsDesignMode)
			{
				panel2.ScrollBars = this.Panel2.ScrollBars;
				panel2.Attributes.Add("{1}", "1");

				if (this.Panel2.ChildControl != null)
				{
					panel2.Controls.Add(this.Panel2.ChildControl);
				}

				LiteralControl lc = new LiteralControl("<span style=\"color:White;\">.</span>");
				panel2.Controls.Add(lc);
			}
			else
			{
				item2 = new SplitterPanelItem();

				if (this.Panel2.ContentTemplate != null)
				{
					this.Panel2.ContentTemplate.InstantiateIn(item2);
					panel2.Controls.Add(item2);
				}
			}

			return panel2;
		}

		/// <summary>
		/// Create the bar element for the control.
		/// </summary>
		/// <returns>Returns the bar element.</returns>
		private Panel CreateBar()
		{
			bar = new Panel();

			if (this.IsDesignMode)
			{
				if (this.ShowExpander)
				{
					bar.Controls.Add(this.CreateExpander());
				}
			}

			return bar;
		}

		/// <summary>
		/// Create the expander element for the control.
		/// </summary>
		/// <returns>Returns the expander element.</returns>
		private Panel CreateExpander()
		{
			expander = new Panel();
			expander.Controls.Add(this.CreateExpanderIcon());

			return expander;
		}

		private HtmlGenericControl CreateExpanderIcon()
		{
			expanderIcon = new HtmlGenericControl("span");

			return expanderIcon;
		}
		#endregion end of ** create child controls.
		
		#region ** apply style to child controls
		private void ApplyStyle2Wrapper()
		{
			if (wrapper != null)
			{
				wrapper.CssClass = CONST_WIJSPLITTER_WRAPPER;

				if (this.Orientation == Orientation.Vertical)
				{
					//For the vertical orientation, all child elements of the wrapper have the "float" style attribute.
					//If the width of the wrapper is less than the total width of the sub elements, 
					//then the last element will flow down.  So we must set the double width value to the wrapper.
					wrapper.Width = Unit.Pixel((int)this.Width.Value * 2);
					wrapper.Height = this.Height;
				}
			}
		}

		private void ApplyStyle2Panel1()
		{
			if (panel1 != null)
			{
				panel1.CssClass = string.Format("{0}{1} {0}{1}-{2} {3}",
								this.GetSplitterClassByOriention(), CONST_WIJSPLITTER_PANEL1,
								CONST_WIJSPLITTER_CONTENT, CONST_WIDGET_CONTENT);

				//if (this.Panel1.CssClass != "")
				//{
				//    panel1.CssClass += " " + this.Panel1.CssClass;
				//}

				if (this.Orientation == Orientation.Horizontal)
				{
					panel1.Width = Unit.Pixel((int)this.Width.Value - 2);
					panel1.Height = Unit.Pixel(this.InnerSplitterDistance);
				}
				else
				{
					panel1.Height = Unit.Pixel((int)this.Height.Value - 2);
					panel1.Width = Unit.Pixel(this.InnerSplitterDistance);
				}
			}
		}

		private void ApplyStyle2Panel2()
		{
			if (panel2 != null)
			{
				panel2.CssClass = string.Format("{0}{1} {0}{1}-{2} {3}",
								this.GetSplitterClassByOriention(), CONST_WIJSPLITTER_PANEL2,
								CONST_WIJSPLITTER_CONTENT, CONST_WIDGET_CONTENT);

				//if (this.Panel2.CssClass != "")
				//{
				//    panel2.CssClass += " " + this.Panel2.CssClass;
				//}

				if (this.Orientation == Orientation.Horizontal)
				{
					panel2.Width = Unit.Pixel((int)this.Width.Value - 2);
					panel2.Height = Unit.Pixel((int)this.Height.Value - this.InnerSplitterDistance - CONST_BAR_SIZE - 6);
				}
				else
				{
					panel2.Height = Unit.Pixel((int)this.Height.Value - 2);
					panel2.Width = Unit.Pixel((int)this.Width.Value - this.InnerSplitterDistance - CONST_BAR_SIZE - 6);
				}
			}
		}

		private void ApplyStyle2Bar()
		{
			if (bar != null)
			{
				bar.CssClass = string.Format("{0}{1} {2}",
								this.GetSplitterClassByOriention(),
								CONST_WIJSPLITTER_BAR,
								CONST_WIDGET_HEADER);

				if (this.Orientation == Orientation.Horizontal)
				{
					bar.Width = this.Width;
					bar.Height = Unit.Pixel(CONST_BAR_SIZE);
				}
				else
				{
					bar.Height = this.Height;
					bar.Width = Unit.Pixel(CONST_BAR_SIZE);
				}
			}
		}

		private void ApplyStyle2Expander()
		{
			if (expander != null)
			{
				expander.CssClass = string.Format("{0}{1} {2} {3} {4}",
							 this.GetSplitterClassByOriention(),
							 CONST_WIJSPLITTER_EXPANDER, CONST_STATE_DEFAULT,
							 CONST_CORNER_TL, this.GetCornerClassByOrientation());

				expander.Style.Clear();

				if (this.Orientation == Orientation.Horizontal)
				{
					//For fixing bug 15988	by wuhao
					//note : outerwidth = width(16) + padding(24) + border(2)
					//expander.Style.Add(HtmlTextWriterStyle.Left, (this.Width.Value - 16) / 2 + "px");
					expander.Style.Add(HtmlTextWriterStyle.Left, (this.Width.Value - 42) / 2 + "px");
					//end for bug 15988
				}
				else
				{
					//For fixing bug 15988	by wuhao
					//note : outerwidth = height(16) + padding(24) + border(2)
					//expander.Style.Add(HtmlTextWriterStyle.Top, (this.Height.Value - 16) / 2   + "px");
					expander.Style.Add(HtmlTextWriterStyle.Top, (this.Height.Value - 42) / 2 + "px");
					//end for bug 15988
				}
			}
		}

		private void ApplyStyle2ExpanderIcon()
		{
			if (expanderIcon != null)
			{
				expanderIcon.Attributes.Add("class", string.Format("{0} {1}{2}",
					CONST_ICON, CONST_ICON_ARROW, this.GetExpanderIconClass()));
			}
		} 
		#endregion end of ** apply style to child controls.

		#region ** override methods
		/// <summary>
		/// Searches the current naming container for a server control 
		/// with the specified id parameter.
		/// </summary>
		/// <param name="id">The identifier for the control to be found.</param>
		/// <returns>The specified control, or null if the specified control does not exist.</returns>
		public override Control FindControl(string id)
		{
			Control ctrl = null;

			//if (panel1 != null)
			// add item1, item2 to fixed findcontorl bug 21276
			if (panel1 != null && item1 != null)
			{
				ctrl = item1.FindControl(id);
			}

			if (ctrl != null)
			{
				return ctrl;
			}

			if (panel2 != null && item2 != null)
			{
				return item2.FindControl(id);
			}

			return null;
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls 
		/// that use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if (this.IsDesignMode)
			{
				Control container = this.CreateWrapper();

				container.Controls.Add(this.CreatePanel1());
				container.Controls.Add(this.CreateBar());
				container.Controls.Add(this.CreatePanel2());

				this.Controls.Add(container);
			}
			else
			{
				this.Controls.Add(this.CreatePanel1());
				this.Controls.Add(this.CreatePanel2());
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
			EnsureChildControls();
		}

		/// <summary>
		/// Raises the PreRender event.
		/// </summary>
		/// <param name="e">An EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			
			base.OnPreRender(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified HtmlTextWriterTag.
		/// </summary>
		/// <param name="writer">A HtmlTextWriter that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			bool enabled = this.Enabled;
			this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass);
			this.Enabled = true;

			base.AddAttributesToRender(writer);

			this.CssClass = cssClass;
			this.Enabled = enabled;
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();

			if (this.IsDesignMode)
			{
				this.ApplyStyle2Wrapper();
				this.ApplyStyle2Panel1();
				this.ApplyStyle2Panel2();
				this.ApplyStyle2Bar();
				this.ApplyStyle2Expander();
				this.ApplyStyle2ExpanderIcon();
			}

			base.Render(writer);
		} 
		#endregion end of ** override methods.
		#endregion end of ** methods.

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1SplitterSerializer sz = new C1SplitterSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1SplitterSerializer sz = new C1SplitterSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1SplitterSerializer sz = new C1SplitterSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1SplitterSerializer sz = new C1SplitterSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for a <see cref="C1Splitter"/> control.
		/// </summary>
		/// <param name="postDataKey">
		/// The key identifier for the control.
		/// </param>
		/// <param name="postCollection">
		/// The collection of all incoming name values.
		/// </param>
		/// <returns>
		/// Returns true if the server control's state changes as a result of the postback;
		/// otherwise, false.
		/// </returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		/// <summary>
		/// Signals the <see cref = "C1Splitter"/> control to notify the ASP.NET
		/// application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations
	}
}
