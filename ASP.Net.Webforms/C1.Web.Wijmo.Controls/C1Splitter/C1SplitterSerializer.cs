﻿#region File Header
//==============================================================================
//  FileName    :   C1SliderSerializer.cs
//
//  Description :   
//
//  Copyright (c) 2001 - 2008 GrapeCity, Inc.
//  All rights reserved.
//
//                                                          ... Jacky Qiu
//==============================================================================

//------------------------------------------------------------------------------
//  Update Log:
//
//  Status          Date            Name                    BUG-ID
//  ----------------------------------------------------------------------------
//  Created         2008/06/26      Jacky Qiu				None
//  Updated         2008/07/25      Jacky Qiu               Fix event's bug
//
//------------------------------------------------------------------------------
#endregion end of File Header.

using System;
using System.Collections.Generic;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Splitter
{

	/// <summary>
	/// Serializes C1Splitter Control.
	/// </summary>
	public class C1SplitterSerializer : C1BaseSerializer<C1Splitter, object, object>
	{

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1SplitterSerializer"/> class.
		/// </summary>
		/// <param name="obj">Object to serialize</param>
		public C1SplitterSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
