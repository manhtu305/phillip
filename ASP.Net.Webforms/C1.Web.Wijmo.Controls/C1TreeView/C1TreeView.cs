﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.IO;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo;
using System.Collections.Specialized;
using System.Reflection;
using C1.Web.Wijmo.Controls.Base.Interfaces;

namespace C1.Web.Wijmo.Controls.C1TreeView
{
	/// <summary>
	/// Represents a Tree in an ASP.NET Web page.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeView.C1TreeViewDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeView.C1TreeViewDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeView.C1TreeViewDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeView.C1TreeViewDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	//[ParseChildren(true)]
	[ToolboxData("<{0}:C1TreeView runat=server></{0}:C1TreeView>")]
	[ToolboxBitmap(typeof(C1TreeView), "C1TreeView.png")]
	[LicenseProviderAttribute()]
	public partial class C1TreeView : C1TargetHierarchicalDataBoundControlBase,
		IC1TreeViewNodeCollectionOwner, IPostBackDataHandler,
		IPostBackEventHandler, IC1Serializable,
		ICallbackEventHandler
	{
		#region ** fields


		private const string CSS_JQ_WIDGET = "ui-widget";
		private const string CSS_JQ_CONTENT = "ui-widget-content";
		private const string CSS_JQ_CLEARFIX = "ui-helper-clearfix";
		private const string CSS_JQ_RESET = "ui-helper-reset";
		private const string CSS_JQ_CORNER = "ui-corner-all";
		private const string CSS_JQ_HEADER = "ui-widget-header";
		private const string CSS_JQ_DEFAULT = "ui-state-default";

		private const string CSS_WIJ_TREE = "wijmo-wijtree";
		private const string CSS_WIJ_TREE_LIST = "wijmo-wijtree-list";
		private const string REGISTER_FUNC = "saveTreeviewState";

		private C1TreeViewNodeCollection _nodes = null;
		private HtmlGenericControl _nodesContainer;
		private Hashtable _properties;
		private List<C1TreeViewNode> _selectedNodes;
		private List<C1TreeViewNode> _checkedNodes;
		private IDictionary<string, C1TreeViewNode> _staticNodes;
		private ITemplate _nodesTemplate;
		//private List<C1TreeViewNodeTemplateContainer> _allTemplateContainers = new List<C1TreeViewNodeTemplateContainer>();

		private static readonly object NodeDroppedEvent = new object();
		private static readonly object NodeExpandedEvent = new object();
		private static readonly object NodeCollapsedEvent = new object();
		private static readonly object NodeCheckChangedEvent = new object();
		private static readonly object SelectedNodesChangedEvent = new object();
		private static readonly object NodeClickedEvent = new object();
		private static readonly object NodeCreatedEvent = new object();
		private static readonly object NodeRemovedEvent = new object();
		private static readonly object NodeTextChangedEvent = new object();
		private static readonly object NodeDataBoundEvent = new object();

		#region licensing
		internal bool _productLicensed;
		private bool _shouldNag;
		#endregion
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1TreeView"/> class.
		/// </summary>
		public C1TreeView()
		{
			VerifyLicense();
			this.InitTreeView();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1TreeView"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		[EditorBrowsable(EditorBrowsableState.Never)] // [20100302DMA] added public constructor with license key to use from C1ReportViewer
		public C1TreeView(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			// ttZzKzJ5mwNr/IihpBl2pA==
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1TreeView), this,
				Assembly.GetExecutingAssembly(), key);
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
			this.InitTreeView();
		}

		private void InitTreeView()
		{
			//_nodes = new C1TreeViewNodeCollection(this);
			_nodesContainer = new HtmlGenericControl("ul");
			_properties = new Hashtable();
		}
		#endregion end of ** constructor

		#region ** Data Binding

		private C1TreeViewNodeBindingCollection _bindings;
		private int _dataBindStartLevel = -1;

		/// <summary>
		/// Binds a data source to the invoked <see cref="C1TreeView"/> tree view and all its nodes.
		/// </summary>
		public sealed override void DataBind()
		{
			base.DataBind();
		}

		/// <summary>
		/// When overridden in a derived class, binds data from the data source to the
		/// control.
		/// </summary>
		protected override void PerformDataBinding()
		{
			base.PerformDataBinding();

			if (!IsBoundUsingDataSourceID && (DataSource == null))
			{
				CreateChildControlsFromNodes(false);
				return;
			}
			//Xml
			if (!IsBoundUsingDataSourceID && DataSource.GetType().Name == "HierarchicalSampleData")
			{
				CreateChildControlsFromNodes(true);
				return;
			}
			//SiteMap
			HierarchicalDataSourceView view = this.GetData(null);

			if (view == null)
			{
				throw new InvalidOperationException
					(C1Localizer.GetString("C1TreeView.NoViewReturn"));
			}

			IHierarchicalEnumerable enumerable = view.Select();
			DataBind(enumerable);
			CreateChildControlsFromNodes(true);
		}

		/// <summary>
		/// Binds a data source to the invoked server control and all its child controls.
		/// </summary>
		/// <param name="enumerable">Enumerable object to bind</param>
		protected virtual void DataBind(IHierarchicalEnumerable enumerable)
		{
			this.Nodes.Clear();
			foreach (object o in enumerable)
			{
				IHierarchyData data = enumerable.GetHierarchyData(o);
				//string text = data.ToString();
				string text = data.Type;
				C1TreeViewNode node = this.CreateTreeViewNode();
				//((IStateManager)node).TrackViewState();
				C1TreeViewNodeBinding binding = this.GetBinding(text, DataBindStartLevel);

				//if null,binding root node
				if (binding == null)
				{
					binding = this.GetBinding(text, 0);
				}
				node.DataBind(binding, enumerable, o, this.AutoGenerateDataBindings);
				this.Nodes.Add(node);
				if (this.LoadOnDemand)
				{
					node.SearchIndexes = new ArrayList();
					node.SearchIndexes.Add(this.Nodes.IndexOf(node));
				}
				if (data.HasChildren && (DataBindStartLevel > node.CalculateCurrentLevel() || DataBindStartLevel == -1))
				{
					IHierarchicalEnumerable childEnum = data.GetChildren();
					if (childEnum != null)
					{
						node.DataBindInternal(childEnum, 1, binding);
					}
				}
				else
				{
					node.Expanded = false;
				}

				node.HasChildren = data.HasChildren;
			}
		}


		bool _subControlsDataBound;
		/// <summary>
		/// Calls the System.Web.UI.WebControls.BaseDataBoundControl.DataBind() method
		/// if the System.Web.UI.WebControls.BaseDataBoundControl.DataSourceID property
		/// is set and the data-bound control is marked to require binding.
		/// </summary>
		protected override void EnsureDataBound()
		{
			base.EnsureDataBound();
			if (!this._subControlsDataBound)
			{
				foreach (Control control in this.Controls)
				{
					control.DataBind();
				}
				this._subControlsDataBound = true;
			}
		}

		/// <summary>
		/// Creates new instance of the <see cref="C1TreeViewNode"/> class.
		/// </summary>
		/// <returns></returns>
		public C1TreeViewNode CreateTreeViewNode()
		{
			C1TreeViewNode c1TreeViewNode = new C1TreeViewNode();
			c1TreeViewNode.TreeView = this;
			return c1TreeViewNode;
		}

		#endregion

		#region ** Override Methods
		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
			if (!IsDesignMode &&
				(!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				this.RequiresDataBinding = true;
			}
			//EnsureChildControls();
			_staticNodes = new Dictionary<string, C1TreeViewNode>();
			int i = 0;
			foreach (C1TreeViewNode childNode in this.Nodes)
			{
				childNode.StaticKey = this.ID + "_" + i.ToString();
				_staticNodes.Add(childNode.StaticKey, childNode);
				InitializeStaticNode(childNode);
				i++;
			}
		}

		private void InitializeStaticNode(C1TreeViewNode node)
		{
			int i = 0;
			foreach (C1TreeViewNode childNode in node.Nodes)
			{
				childNode.StaticKey = node.StaticKey + i.ToString();
				_staticNodes.Add(childNode.StaticKey, childNode);
				InitializeStaticNode(childNode);
				i++;
			}
		}

		/// <summary>
		/// OnPreRender override.
		/// Performs registration in ScriptManager and register <see cref="C1TreeView"/> treeview for PostBack.
		/// Relate all <see cref="C1TreeViewNode"/> nodes to its <see cref="C1TreeView"/> treeview
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);

			if (this.Page != null)
			{
				this.Page.RegisterRequiresRaiseEvent(this);
				this.Page.RegisterRequiresPostBack(this);
				_postBackEventReference = this.Page.ClientScript.GetPostBackEventReference(this, string.Empty);
			}

			if (this.LoadOnDemand)
			{
				Page.ClientScript.GetCallbackEventReference(this, "", "", "");
			}
			//EnsureEnabledState();
		}


		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();
			if (base.RequiresDataBinding && (!string.IsNullOrEmpty(this.DataSourceID) || (this.DataSource != null)))
			{
				this.EnsureDataBound();
			}
			else
			{
				this.CreateChildControlsFromNodes(false);
				base.ClearChildViewState();
			}

			_nodesContainer.Controls.Clear();
			this.Controls.Add(_nodesContainer);

			if (this.IsDesignMode)
			{
				_nodesContainer.Attributes.Add("class",
					string.Format("{0} {1}", CSS_WIJ_TREE_LIST, CSS_JQ_RESET));
			}

			foreach (C1TreeViewNode node in this.Nodes)
			{
				if (!_nodesContainer.Controls.Contains(node))
				{
					_nodesContainer.Controls.Add(node);
				}
			}
			this.EnsureChildControls();
		}

		/// <summary>
		/// Creates a child controls collection using the <see cref="C1TreeViewNode"/> collection.
		/// </summary>
		/// <remarks> This method is primarily used by control developers.</remarks>
		/// <param name="dataBinding">Indicates if data binding is used to create the collection.</param>
		private void CreateChildControlsFromNodes(bool dataBinding)
		{
			for (int i = 0; i < this.Nodes.Count; i++)
			{
				C1TreeViewNode curNode = this.Nodes[i];
				CreateChildControlsFromNodes(curNode, i, 0, dataBinding);
			}
		}

		/// <summary>
		/// Creates node template
		/// </summary>
		/// <param name="node"></param>
		/// <param name="position"></param>
		/// <param name="depth"></param>
		/// <param name="dataBinding"></param>
		private void CreateChildControlsFromNodes(C1TreeViewNode node, int position, int depth, bool dataBinding)
		{
			if (dataBinding)
			{
				C1TreeViewEventArgs e = new C1TreeViewEventArgs(node);
				OnNodeDataBound(e);
			}

			ITemplate template = node.Template;
			if (template == null && node.Templated && (_staticNodes != null))
			{
				if (_staticNodes.ContainsKey(node.StaticKey))
				{
					template = _staticNodes[node.StaticKey].Template;
				}
			}
			if (template == null)
			{
				template = NodesTemplate;
			}
			if (template != null)
			{
				node.Templated = true;
				HtmlGenericControl container = new HtmlGenericControl("div");
				container.Attributes.Add("class", CSS_JQ_CLEARFIX);
				container.Style.Add("float", "left");
				node.TemplateContainer = container;
				template.InstantiateIn(container);
				//this.Controls.Add(container);				
			}
			else
			{
				node.Templated = false;
			}
			if (node.Nodes.Count > 0)
			{
				for (int i = 0; i < node.Nodes.Count; i++)
				{
					C1TreeViewNode curNode = node.Nodes[i];
					CreateChildControlsFromNodes(curNode, i, depth + 1, dataBinding);
				}
			}
		}

		/// <summary>
		/// Renders <see cref="C1TreeView"/> elements. Renders hidden element used be the control for serialization. 
		/// </summary>
		/// <param name="writer"></param>
		protected override void Render(HtmlTextWriter writer)
		{
			EnsureChildControls();
			if (ShowCheckBoxes && AllowTriState)
			{
				//ResetCheckState();
			}
			base.Render(writer);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to
		/// the specified <see cref="HtmlTextWriterTag"/>.
		/// </summary>
		/// <param name="writer">A <see cref="HtmlTextWriter"/>that represents
		/// the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			if (this.IsDesignMode)
			{
				this.CssClass =string.Format("{0} {1} {2} {3} {4} {5}",
					cssClass,
					CSS_WIJ_TREE,
					CSS_JQ_WIDGET,
					CSS_JQ_CONTENT,
					CSS_JQ_CLEARFIX,
					CSS_JQ_CORNER);
			}
			else
			{
				this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass);
			}
			base.AddAttributesToRender(writer);
			this.CssClass = cssClass;
		}

		/// <summary>
		/// Outputs server control content to a provided System.Web.UI.HtmlTextWriter
		///	object and stores tracing information about the control if tracing is enabled.
		/// </summary>
		/// <param name="writer">
		/// The System.Web.UI.HTmlTextWriter object that receives the control content.
		/// </param>
		protected override void RenderChildren(HtmlTextWriter writer)
		{
			foreach (Control con in this.Controls)
			{
				if (!(con is C1TreeViewNode))//C1TreeViewNodeTemplateContainer
				{
					con.RenderControl(writer);
				}
			}

			if (this.IsDesignMode)
			{
				HtmlControl clear = new HtmlGenericControl("div");
				clear.Style.Add("clear", "both");
				this.Controls.Add(clear);
				clear.RenderControl(writer);
			}
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(REGISTER_FUNC);
		}

		#endregion

		#region ** Properties

		/// <summary>
		/// Gets a <see cref="C1TreeViewNodeCollection"/> object that contains the root nodes of the current <see cref="C1TreeView"/> control.
		/// </summary>
		[Layout(LayoutType.Appearance)]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[RefreshProperties(RefreshProperties.All)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public C1TreeViewNodeCollection Nodes
		{
			get
			{
				if (_nodes == null)
				{
					_nodes = new C1TreeViewNodeCollection(this);
				}
				return this._nodes;
			}
		}

		/// <summary>
		/// There is no such owner for <see cref="C1TreeView"/>.
		/// </summary>
		[Browsable(false)]
		public IC1TreeViewNodeCollectionOwner Owner
		{
			get { return null; }
		}

		/// <summary>
		/// Get the unique ID of this control
		/// </summary>
		[Browsable(false)]
		[DefaultValue("")]
		[WidgetOption()]
		[WidgetOptionName("UniqueID")]
		public override string UniqueID
		{
			get
			{
				return base.UniqueID;
			}
		}

		/// <summary>
		/// Gets the SelectedNodes of C1TreeView.
		/// </summary>
		[Browsable(false)]
		public List<C1TreeViewNode> SelectedNodes
		{
			get
			{
				if (_selectedNodes == null)
				{
					_selectedNodes = new List<C1TreeViewNode>();
					//_selectedNodes. += new EventHandler(_selectedNodes_OnCollectionChanged);
				}
				return _selectedNodes;
			}
		}

		/// <summary>
		///  Gets the CheckedNodes of C1TreeView.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public List<C1TreeViewNode> CheckedNodes
		{
			get
			{
				if (_checkedNodes == null)
				{
					_checkedNodes = new List<C1TreeViewNode>();
				}
				return _checkedNodes;
			}
		}

		/// <summary>
		/// If specified, this template will be applied for all <see cref="C1TreeViewNode"/> nodes that does not have other defined templates.
		/// </summary>
		[C1Description("C1TreeView.NodesTemplate")]
		//[TemplateContainer(typeof(C1TreeViewNodeTemplateContainer))]
		[Browsable(false)]
		[Bindable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public virtual ITemplate NodesTemplate
		{
			get
			{
				return this._nodesTemplate;
			}
			set
			{
				this._nodesTemplate = value;
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeView.AutoPostBack")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public bool AutoPostBack
		{
			get
			{
				return GetPropertyValue("AutoPostBack", false);
			}
			set
			{
				SetPropertyValue("AutoPostBack", value);
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether or not Loads on demand is enabled.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior"), C1Description("C1TreeView.LoadOnDemand")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public bool LoadOnDemand
		{
			get
			{
				return GetPropertyValue("LoadOnDemand", false);
			}
			set
			{
				SetPropertyValue("LoadOnDemand", value);
			}
		}

		///// <summary>
		///// NodeClicked PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string NodeClickedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[NodeClickedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}
		
		///// <summary>
		///// NodeCheckChanged PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string NodeCheckChangedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[NodeCheckChangedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		///// <summary>
		///// NodeCollapsed PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string NodeCollapsedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[NodeCollapsedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		///// <summary>
		///// NodeExpanded PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string NodeExpandedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[NodeExpandedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		///// <summary>
		///// NodeDropped PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string NodeDroppedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[NodeDroppedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		///// <summary>
		///// NodeTextChanged PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string NodeTextChangedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[NodeTextChangedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		///// <summary>
		///// SelectedNodesChanged PostBack Event Reference.  
		///// Empty string if this event is not defined.
		///// </summary>
		/// <summary>
		/// Infrastructure.
		/// </summary>
		[WidgetOption()]
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string SelectedNodesChangedPostBackReference
		{
			get
			{
				if (!this.IsDesignMode 
					&& (C1TreeViewEventHandler)Events[SelectedNodesChangedEvent] != null)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}
				return "";
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates DataBind start level.
		/// If value is 0, only root nodes will be bounded on start. For the value equals to 1, root nodes and first level nodes will be bounded.
		/// Default value -1 indicates that property will be ignored.
		/// </summary>
		[DefaultValue(-1)]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeView.DataBindStartLevel")]
		[Layout(LayoutType.Data)]
		public int DataBindStartLevel
		{
			get
			{
				return LoadOnDemand ? _dataBindStartLevel : -1;
			}
			set 
			{ 
				_dataBindStartLevel = value; 
			}
		}

		/// <summary>
		/// Data bindings for items in the control.
		/// </summary>
		[MergableProperty(false)]
		[C1Category("Category.Data")]
		[C1Description("C1TreeView.DataBindings")]
		[DefaultValue((string)null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Layout(LayoutType.Data)]
		public C1TreeViewNodeBindingCollection DataBindings
		{
			get
			{
				if (this._bindings == null)
				{
					this._bindings = new C1TreeViewNodeBindingCollection(this);
					if (base.IsTrackingViewState)
					{
						((IStateManager)this._bindings).TrackViewState();
					}
				}
				return this._bindings;
			}
		}

		#endregion

		#region ** IPostBackDataHandler Members

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			return true;
			//Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			//this.RestoreStateFromJson(data);

			//return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			//RaiseOccuredEvent();
		}

		/// <summary>
		/// OnLoad override.
		/// At this point tree view raise all its events.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			//RaiseOccuredEvent();
		}

		#region Public methods

		/// <summary>
		/// Searches <see cref="C1TreeViewNode"/> node through the collection by the NavigateUrl.
		/// Recursive searching is used until <see cref="C1TreeViewNode"/> node is found.
		/// </summary>
		/// <param name="url"></param>
		/// <returns>First <see cref="C1TreeViewNode"/> node that match the searched text.</returns>
		public C1TreeViewNode FindNodeByNavigateUrl(string url)
		{
			C1TreeViewNode searchedNode = this.Nodes.FindNodeByNavigateUrl(url, this.Page);

			if (searchedNode == null)
			{
				foreach (C1TreeViewNode node in _nodes)
				{
					searchedNode = FindNodeByNavigateUrl(url, node, Page);
					if (searchedNode != null)
					{
						return searchedNode;
					}
				}
			}


			return searchedNode;
		}

		/// <summary>
		/// Searches <see cref="C1TreeViewNode"/> node through the collection by the Text.
		/// Recursive searching is used until <see cref="C1TreeViewNode"/> node is found.
		/// </summary>
		/// <param name="text"></param>
		/// <returns>First <see cref="C1TreeViewNode"/> node that match the searched text.</returns>
		public C1TreeViewNode FindNodeByText(string text)
		{
			C1TreeViewNode searchedNode = this.Nodes.FindNodeByText(text);

			if (searchedNode == null)
			{
				foreach (C1TreeViewNode node in _nodes)
				{
					searchedNode = FindNodeByText(text, node);
					if (searchedNode != null)
					{
						return searchedNode;
					}
				}
			}


			return searchedNode;
		}

		/// <summary>
		/// Searches <see cref="C1TreeViewNode"/> node through the collection by the Value. 
		/// Recursive searching is used until <see cref="C1TreeViewNode"/> node is found. 
		/// </summary>
		/// <param name="value">node property</param>
		/// <returns>First <see cref="C1TreeViewNode"/> node that match the searched text.</returns>
		public C1TreeViewNode FindNodeByValue(string value)
		{
			C1TreeViewNode searchedNode = this.Nodes.FindNodeByValue(value);

			if (searchedNode == null)
			{
				foreach (C1TreeViewNode node in _nodes)
				{
					searchedNode = FindNodeByValue(value, node);
					if (searchedNode != null)
					{
						return searchedNode;
					}
				}
			}


			return searchedNode;
		}

		#endregion

        public virtual void ExpandAllNode()
        {
            ExpandChildNodes(this.Nodes);
        }

		public virtual void ExpandAllNode(IC1TreeViewNodeCollectionOwner control)
		{
            if (control is C1TreeView)
            {
                ExpandAllNode();
            }
            else if (control is C1TreeViewNode)
            {
                var node = control as C1TreeViewNode;
                node.Expanded = true;
                ExpandChildNodes(node.Nodes);
            }
		}

        private void ExpandChildNodes(C1TreeViewNodeCollection nodes)
        {
            foreach (var node in nodes)
            {
                node.Expanded = true;
                if (node.Nodes.Count > 0)
                {
                    ExpandChildNodes(node.Nodes);
                }
            }
        }

		#endregion

		#region ** IPostBackEventHandler Members

		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (!string.IsNullOrEmpty(eventArgument))
			{
				RaiseOccuredEvent(eventArgument);
			}
		}

		internal string _postBackEventReference;

		#endregion

		#region ** ICallBackEventHandler

		private string _response;
		string ICallbackEventHandler.GetCallbackResult()
		{
			return _response;
		}

		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			try
			{
				Hashtable data = JsonHelper.StringToObject(eventArgument);
				string commandName = (string)data["CommandName"];
				Hashtable commandData = (Hashtable)data["CommandData"];
				_response = ProcessAjaxCommand(commandName, commandData);
			}
			catch (Exception ex)
			{
				_response = ex.Message;
			}
		}

		#endregion

		#region ** IC1Serializable Layout

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">
		/// The file where the values of the layout properties will be saved.
		/// </param> 
		public void SaveLayout(string filename)
		{
			C1TreeViewSerializer sz = new C1TreeViewSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">
		/// The stream where the values of layout properties will be saved.
		/// </param> 
		public void SaveLayout(Stream stream)
		{
			C1TreeViewSerializer sz = new C1TreeViewSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">
		/// The file where the values of layout properties will be loaded.
		/// </param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">
		/// The stream where the values of layout properties will be loaded.
		/// </param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">
		/// The file where the values of layout properties will be loaded.
		/// </param> 
		/// <param name="layoutTypes">
		/// The layout types to load.
		/// </param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1TreeViewSerializer sz = new C1TreeViewSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">
		/// The stream where the values of the layout properties will be loaded.
		/// </param> 
		/// <param name="layoutTypes">
		/// The layout types to load.
		/// </param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1TreeViewSerializer sz = new C1TreeViewSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}


		#endregion

		#region ** Server Side Events

		/// <summary>
		/// Occurs on the server after the <see cref="TreeView"/> node has been dropped. 
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeDropped")]
		public event C1TreeViewEventHandler NodeDropped
		{
			add
			{
				base.Events.AddHandler(NodeDroppedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(NodeDroppedEvent, value);
			}
		}

		/// <summary>
		/// Occurs on the server after the <see cref="TreeView"/> node is expanded. 
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeExpanded")]
		public event C1TreeViewEventHandler NodeExpanded
		{
			add
			{
				base.Events.AddHandler(NodeExpandedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(NodeExpandedEvent, value);
			}
		}

		/// <summary>
		/// Occurs on the server after the <see cref="TreeView"/> node is collapsed.
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeCollapsed")]
		public event C1TreeViewEventHandler NodeCollapsed
		{
			add
			{
				base.Events.AddHandler(NodeCollapsedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(NodeCollapsedEvent, value);
			}
		}

		/// <summary>
		///	Occurs on the server if a node in the TreeView control changes its check status.
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeCheckChanged")]
		public event C1TreeViewEventHandler NodeCheckChanged
		{
			add
			{
				base.Events.AddHandler(NodeCheckChangedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(NodeCheckChangedEvent, value);
			}
		}

		/// <summary>
		///	Occurs on the server if a node in the TreeView control has been selected.
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.SelectedNodesChanged")]
		public event C1TreeViewEventHandler SelectedNodesChanged
		{
			add
			{
				base.Events.AddHandler(SelectedNodesChangedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(SelectedNodesChangedEvent, value);
			}
		}

		/// <summary>
		///	Occurs on the server if a node in the TreeView control has been clicked.
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeClicked")]
		public event C1TreeViewEventHandler NodeClicked
		{
			add
			{
				base.Events.AddHandler(NodeClickedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(NodeClickedEvent, value);
			}
		}


		/// <summary>
		/// Occurs on the server when a node's Text property is changed.
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeTextChanged")]
		public event C1TreeViewEventHandler NodeTextChanged
		{
			add
			{
				base.Events.AddHandler(NodeTextChangedEvent, value);
			}
			remove
			{
				base.Events.RemoveHandler(NodeTextChangedEvent, value);
			}

		}

		/// <summary>
		/// Occurs after a node is data bound.
		/// </summary>
		[C1Category("Category.Action"), C1Description("C1TreeView.NodeDataBound")]
		public event C1TreeViewEventHandler NodeDataBound;

		private void OnNodeDataBound(C1TreeViewEventArgs e)
		{
			if (this.NodeDataBound != null)
			{
				this.NodeDataBound(this, e);
			}
		}

		#endregion

		#region ** private methods

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1TreeView), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		private void RaiseOccuredEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("_NodeClicked_"))
			{
				string path = eventArgument.Replace("_NodeClicked_", "");
				if (!string.IsNullOrEmpty(path))
				{
					RaiseNodeClickedEvent(path);
				}
			}
			if (eventArgument.StartsWith("_NodeCheckChanged_"))
			{
				string path = eventArgument.Replace("_NodeCheckChanged_", "");
				if (!string.IsNullOrEmpty(path))
				{
					RaiseNodeCheckChangedEvent(path);
				}
			}
			if (eventArgument.StartsWith("_NodeExpanded_"))
			{
				string path = eventArgument.Replace("_NodeExpanded_", "");
				if (!string.IsNullOrEmpty(path))
				{
					RaiseNodeExpandedEvent(path);
				}
			}
			if (eventArgument.StartsWith("_NodeCollapsed_"))
			{
				string path = eventArgument.Replace("_NodeCollapsed_", "");
				if (!string.IsNullOrEmpty(path))
				{
					RaiseNodeCollapsedEvent(path);
				}
			}
			if (eventArgument.StartsWith("_NodeDropped_"))
			{
				string path = eventArgument.Replace("_NodeDropped_", "");
				if (!string.IsNullOrEmpty(path))
				{
					RaiseNodeDroppedEvent(path);
				}
			}
			if (eventArgument.StartsWith("_NodeTextChanged_"))
			{
				string path = eventArgument.Replace("_NodeTextChanged_", "");
				if (!string.IsNullOrEmpty(path))
				{
					RaiseNodeTextChangedEvent(path);
				}
			}
			if (eventArgument.StartsWith("_SelectedNodesChanged_"))
			{
				string path = eventArgument.Replace("_SelectedNodesChanged_", "");
				if (path.StartsWith("_NodeClicked_"))
				{
					path = path.Replace("_NodeClicked_", "");
					if (!string.IsNullOrEmpty(path))
					{
						RaiseNodeClickedEvent(path);
					}
				}

				if (!string.IsNullOrEmpty(path))
				{
					RaiseSelectedNodesChangedEvent(path);
				}
			}				
		}

		Dictionary<string, C1TreeViewNode> _loadedNodes = new Dictionary<string, C1TreeViewNode>();

		private void RaiseNodeExpandedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[NodeExpandedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		private void RaiseNodeCollapsedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[NodeCollapsedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		private void RaiseNodeCheckChangedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[NodeCheckChangedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		private void RaiseNodeDroppedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[NodeDroppedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		private void RaiseNodeTextChangedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[NodeTextChangedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		private void RaiseSelectedNodesChangedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[SelectedNodesChangedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		private void RaiseNodeClickedEvent(string path)
		{
			C1TreeViewEventHandler eh = (C1TreeViewEventHandler)Events[NodeClickedEvent];
			if (eh != null)
			{
				C1TreeViewNode node = _loadedNodes[path];
				eh(this, new C1TreeViewEventArgs(node));
			}
		}

		internal virtual C1TreeViewNodeBinding GetBinding(string dataMember, int depth)
		{
			if (this._bindings == null)
				return null;
			return this._bindings.GetBinding(dataMember, depth);
		}

		internal virtual bool AutoGenerateDataBindings
		{
			get
			{
				if (this._bindings == null)
					return true;
				return this._bindings.Count == 0;
			}
		}

		private C1TreeViewNode FindNodeByNavigateUrl(string url, C1TreeViewNode parentNode, Page page)
		{
			C1TreeViewNode searchedNode = parentNode.Nodes.FindNodeByNavigateUrl(url, page);
			if (searchedNode == null)
			{
				foreach (C1TreeViewNode node in parentNode.Nodes)
				{
					searchedNode = FindNodeByNavigateUrl(url, node, page);
					if (searchedNode != null)
					{
						return searchedNode;
					}
				}
			}
			return searchedNode;
		}
		private C1TreeViewNode FindNodeByText(string text, C1TreeViewNode parentNode)
		{
			C1TreeViewNode searchedNode = parentNode.Nodes.FindNodeByText(text);
			if (searchedNode == null)
			{
				foreach (C1TreeViewNode node in parentNode.Nodes)
				{
					searchedNode = FindNodeByText(text, node);
					if (searchedNode != null)
					{
						return searchedNode;
					}
				}
			}
			return searchedNode;
		}
		private C1TreeViewNode FindNodeByValue(string value, C1TreeViewNode parentNode)
		{
			C1TreeViewNode searchedNode = parentNode.Nodes.FindNodeByValue(value);
			if (searchedNode == null)
			{
				foreach (C1TreeViewNode node in parentNode.Nodes)
				{
					searchedNode = FindNodeByValue(value, node);
					if (searchedNode != null)
					{
						return searchedNode;
					}
				}
			}
			return searchedNode;
		}

		#region Ajax Enhancements

		/// <summary>
		/// Identify the action to take as response to the client request
		/// </summary>
		private string ProcessAjaxCommand(string CommandName, Hashtable commandData)
		{
			if (CommandName == "GetNodes")
			{
				ArrayList searchIndexes = (ArrayList)commandData["SearchIndexes"];
				return JsonHelper.WidgetToString(CreateTreeViewStructure(searchIndexes), this);
			}
			return "";
		}

		/// <summary>
		/// Recovers the data that will be used to recreate some part of <see cref="C1TreeView"/> treeview.
		/// </summary>
		private IHierarchicalEnumerable LoadRequiredData(ArrayList searchIndexes)
		{
			int index = 0;
			int curentLevel = 0;

			base.PerformDataBinding();

			HierarchicalDataSourceView view = this.GetData(null);
			IHierarchicalEnumerable enumerable = view.Select();

			foreach (object o in enumerable)
			{
				if (index == (int)searchIndexes[curentLevel])
				{
					IHierarchyData data = enumerable.GetHierarchyData(o);

					if (curentLevel == searchIndexes.Count - 1)
					{
						return data.GetChildren();
					}
					else
					{
						IHierarchicalEnumerable childEnum = data.GetChildren();
						return LoadRequiredDataForLevel(searchIndexes, curentLevel + 1, childEnum);
					}
				}
				index++;
			}
			return null;
		}

		private IHierarchicalEnumerable LoadRequiredDataForLevel(ArrayList searchIndexes, int currentLevel, IHierarchicalEnumerable enumerable)
		{
			int index = 0;

			foreach (object o in enumerable)
			{
				if (index == (int)searchIndexes[currentLevel])
				{
					IHierarchyData data = enumerable.GetHierarchyData(o);

					if (currentLevel == searchIndexes.Count - 1)
					{
						return data.GetChildren();
					}
					else
					{
						IHierarchicalEnumerable childEnum = data.GetChildren();
						return LoadRequiredDataForLevel(searchIndexes, currentLevel + 1, childEnum);
					}
				}
				index++;
			}
			return null;
		}

		private IList DataBind(IHierarchicalEnumerable enumerable, IList bindedNodes, ArrayList searchIndexes)
		{
			int currentNodeLevel = DataBindStartLevel;

			foreach (object o in enumerable)
			{
				IHierarchyData data = enumerable.GetHierarchyData(o);
				string text = data.Type;

				C1TreeViewNode node = this.CreateTreeViewNode();

				C1TreeViewNodeBinding binding = this.GetBinding(text, DataBindStartLevel);

				//if null,binding root node
				if (binding == null)
				{
					binding = this.GetBinding(text, 0);
				}

				node.DataBind(binding, enumerable, o, this.AutoGenerateDataBindings);
				//if (node.DataBind(binding, enumerable, o, this.AutoGenerateDataBindings))
				//{
				bindedNodes.Add(node);

				node.SearchIndexes = new ArrayList();
				node.SearchIndexes.AddRange(searchIndexes);
				node.SearchIndexes.Add(bindedNodes.IndexOf(node));
				//}

				node.HasChildren = data.HasChildren;
			}

			return bindedNodes;
		}

		/// <summary>
		/// Create a part of <see cref="C1TreeView"/> treeview requested by the client.
		/// </summary>
		private IList CreateTreeViewStructure(ArrayList searchIndexes)
		{
			return DataBind(LoadRequiredData(searchIndexes), new ArrayList(), searchIndexes);
		}

		#endregion

		#endregion

		#region ** Data Restore (handled in viewstate)

		/// <summary>
		/// Restores view-state information from a previous request that was saved with
		/// the System.Web.UI.WebControls.WebControl.SaveViewState() method.
		/// </summary>
		/// <param name="savedState">
		/// An object that represents the control state to restore.
		/// </param>
		protected override void LoadViewState(object savedState)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(Page.Request.Form);
			this.RestoreStateFromJson(data);
			_loadedNodes.Clear();
			LoadChildNodesFromJsonData(this, (ArrayList)data["nodes"]);

		}

		private void LoadChildNodesFromJsonData(IC1TreeViewNodeCollectionOwner owner, ArrayList jsonItems)
		{
			if (jsonItems == null)
			{
				return;
			}

			C1TreeViewNodeCollection nodes = new C1TreeViewNodeCollection(owner);
			if (jsonItems != null && jsonItems.Count > 0)
			{
				foreach (object o in jsonItems)
				{
					Hashtable jsonItem = (Hashtable)o;
					C1TreeViewNode curItem = new C1TreeViewNode();
					if (jsonItem["staticKey"] != null && jsonItem["staticKey"].ToString().Length > 0)
					{
						FindAndReplaceInitNodes(ref curItem, jsonItem["staticKey"].ToString(), _nodes);						
					}
					if (jsonItem["guid"] != null && jsonItem["guid"].ToString().Length > 0) 
					{
						curItem.Guid = jsonItem["guid"].ToString();
					}

					JsonRestoreHelper.RestoreStateFromJson(curItem, jsonItem);
					nodes.Add(curItem);

					if (jsonItem["guid"] != null
						&& !_loadedNodes.ContainsKey(jsonItem["guid"].ToString()))
					{
						_loadedNodes.Add(jsonItem["guid"].ToString(), curItem);
					}

					if (jsonItem["nodes"] != null)
					{
						LoadChildNodesFromJsonData(curItem, (ArrayList)jsonItem["nodes"]);
					}
				}
			}
			if (owner is C1TreeView)
			{
				_nodes = nodes;
			}
			else
			{
				((C1TreeViewNode)owner)._nodes = nodes;
			}
		}

		private bool FindAndReplaceInitNodes(ref C1TreeViewNode item, string strStaticKey, C1TreeViewNodeCollection tarItems)
		{
			for (int i = 0; i < tarItems.Count; i++)
			{
				if (tarItems[i].StaticKey == strStaticKey)
				{
					item = tarItems[i];
					return true;
				}
				C1TreeViewNode itemBase = tarItems[i];
				if (itemBase.Nodes != null && itemBase.Nodes.Count > 0)
				{
					if (FindAndReplaceInitNodes(ref item, strStaticKey, itemBase.Nodes))
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Saves any state that was modified after the System.Web.UI.WebControls.Style.TrackViewState()
		/// method was invoked.
		/// </summary>
		/// <returns>
		/// An object that contains the current view state of the control; otherwise,
		/// </returns>
		protected override object SaveViewState()
		{
			object[] arr = new object[2];
			arr[0] = base.SaveViewState();
			if (!this.Visible)
			{
				arr[1] = DateTime.Now;
			}
			return arr;
		}

		#endregion

	}
}
