﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Security.Permissions;
using System.Drawing.Design;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1TreeView
{
    /// <summary>
    /// Defines the relationship between a data item and the node it is binding to in a <see cref="C1TreeView"/> control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class C1TreeViewNodeBinding : IStateManager, ICloneable, IDataSourceViewSchemaAccessor
    {
        #region Constructor

        /// <summary>
		/// Initializes a new instance of the <see cref="C1TreeViewNodeBinding"/> class.
        /// </summary>
        public C1TreeViewNodeBinding()
        {
        }


        /// <summary>
		/// Initializes a new instance of the <see cref="C1TreeViewNodeBinding"/> class.
        /// </summary>
        /// <param name="dataMember"></param>
        public C1TreeViewNodeBinding(string dataMember)
        {
            this.DataMember = dataMember;
        }

        #endregion

        #region ICloneable

        /// <summary>
        /// Clone this object.
        /// </summary>
        /// <returns></returns>
		object ICloneable.Clone()
        {
            C1TreeViewNodeBinding binding = new C1TreeViewNodeBinding();
            binding.DataMember = this.DataMember;
            binding.Depth = this.Depth;
            binding.TextField = this.TextField;
            binding.NavigateUrlField = this.NavigateUrlField;
            binding.ItemIconClassField = this.ItemIconClassField;
            binding.CollapsedIconClassField = this.CollapsedIconClassField;
            binding.ExpandedIconClassField = this.ExpandedIconClassField;
            binding.ExpandedField = this.ExpandedField;
            binding.EnabledField = this.EnabledField;
            return binding;
        }
        #endregion

        #region --- Properties ---

        /// <summary>
        /// Gets or sets the value to match against a <see cref="IHierarchyData"/>. Type property for a data item to determine whether to apply the tree node binding.
        /// </summary>
        [C1Description("C1TreeViewNodeBinding.DataMember")]
        [DefaultValue("")]
        [C1Category("Category.Data")]
        public string DataMember
        {
            get
            {
                object obj2 = this.ViewState["DataMember"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["DataMember"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the node depth at which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Data")]
        [DefaultValue(-1)]
        [TypeConverter(typeof(TreeNodeBindingDepthConverter))]
        [C1Description("C1TreeViewNodeBinding.Depth")]
        public int Depth
        {
            get
            {
                object obj2 = this.ViewState["Depth"];
                if (obj2 == null)
                {
                    return -1;
                }
                return (int)obj2;
            }
            set
            {
                this.ViewState["Depth"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the string that specifies the display format 
        /// for the text of the <see cref="C1TreeViewNode"/> node to which the <see cref="C1TreeViewNodeBinding"/>object is applied.
        /// </summary>
        [C1Description("C1TreeViewNodeBinding.FormatString")]
        [DefaultValue("")]
        [Localizable(true)]
        [C1Category("Category.Databindings")]
        public string FormatString
        {
            get
            {
                object obj2 = this.ViewState["FormatString"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["FormatString"] = value;
            }
        }




        /// <summary>
		/// Gets or sets the name of the field from the data source to bind to the ItemIconClass property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
		[C1Description("C1TreeViewNodeBinding.ItemIconClassField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
		public string ItemIconClassField
        {
            get
            {
				object obj2 = this.ViewState["ItemIconClassField"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
				this.ViewState["ItemIconClassField"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the CollapsedImageUrl property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
        [C1Description("C1TreeViewNodeBinding.CollapsedIconClassField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
		public string CollapsedIconClassField
        {
            get
            {
                object obj2 = this.ViewState["CollapsedIconClassField"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["CollapsedIconClassField"] = value;
            }
        }      

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the ExpandedImageUrl property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
        [C1Description("C1TreeViewNodeBinding.ExpandedIconClassField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string ExpandedIconClassField
        {
            get
            {
                object obj2 = this.ViewState["ExpandedIconClassField"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["ExpandedIconClassField"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the NavigateUrl property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [C1Category("Category.Databindings")]
        [C1Description("C1TreeViewNodeBinding.NavigateUrlField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string NavigateUrlField
        {
            get
            {
                object obj2 = this.ViewState["NavigateUrlField"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["NavigateUrlField"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Text property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
        [C1Description("C1TreeViewNodeBinding.TextField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string TextField
        {
            get
            {
                object obj2 = this.ViewState["TextField"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["TextField"] = value;
            }
        }      

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Value property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
        [C1Description("C1TreeViewNodeBinding.ValueField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string ValueField
        {
            get
            {
                object obj2 = this.ViewState["ValueField"];
                if (obj2 == null)
                {
                    return string.Empty;
                }
                return (string)obj2;
            }
            set
            {
                this.ViewState["ValueField"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Expanded property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1TreeViewNodeBinding.ExpandedField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string ExpandedField
        {
            get
            {
                object obj2 = this.ViewState["ExpandedField"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                this.ViewState["ExpandedField"] = value;
            }
        }      

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Enabled property of a <see cref="C1TreeViewNode"/> object to which the <see cref="C1TreeViewNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1TreeViewNodeBinding.EnabledField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string EnabledField
        {
            get
            {
                object obj2 = this.ViewState["EnabledField"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                this.ViewState["EnabledField"] = value;
            }
        }


        #endregion


        object IDataSourceViewSchemaAccessor.DataSourceViewSchema
        {
            get
            {
                return this.ViewState["IDataSourceViewSchemaAccessor.DataSourceViewSchema"];
            }
            set
            {
                this.ViewState["IDataSourceViewSchemaAccessor.DataSourceViewSchema"] = value;
            }
        }		

        #region --- ViewState ---

        void IStateManager.LoadViewState(object state)
        {
            if (state != null)
            {
                ((IStateManager)this.ViewState).LoadViewState(state);
            }
        }

        object IStateManager.SaveViewState()
        {
            if (this._viewState != null)
            {
                return ((IStateManager)this._viewState).SaveViewState();
            }
            return null;
        }

        void IStateManager.TrackViewState()
        {
            this._isTrackingViewState = true;
            if (this._viewState != null)
            {
                ((IStateManager)this._viewState).TrackViewState();
            }
        }

        bool IStateManager.IsTrackingViewState
        {
            get
            {
                return this._isTrackingViewState;
            }
        }

        private StateBag ViewState
        {
            get
            {
                if (this._viewState == null)
                {
                    this._viewState = new StateBag();
                    if (this._isTrackingViewState)
                    {
                        ((IStateManager)this._viewState).TrackViewState();
                    }
                }
                return this._viewState;
            }
        }

        internal bool IsEmpty(string propName)
        {
            return (this.ViewState[propName] == null);
        }

        internal void SetDirty()
        {
            this.ViewState.SetDirty(true);
        }

        #endregion

        // Fields
        private bool _isTrackingViewState;
        private StateBag _viewState;
    }
}
