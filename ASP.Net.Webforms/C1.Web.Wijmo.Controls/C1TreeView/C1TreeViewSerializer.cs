using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using System.Web.UI;
using System.Web;
using System.Globalization;
using System.ComponentModel.Design;


namespace C1.Web.Wijmo.Controls.C1TreeView
{
    /// <summary>
    /// Represent serializer used by <see cref="C1TreeView"/> tree view. 
    /// </summary>
    public class C1TreeViewSerializer: C1BaseSerializer<C1TreeView, C1TreeViewNode, IC1TreeViewNodeCollectionOwner>
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="C1TreeViewSerializer"/> class.
        /// </summary>
        public C1TreeViewSerializer(Object obj)
            : base(obj)
        {
        }

        #region ** protected override

		/// <summary>
		/// Override this method if you want to add deserialized object 
		/// to array or collection using custom way.
		/// </summary>
		/// <example>
		/// Override example:
		/// <code>
		/// protected override bool OnAddItem(object collection, object item)
		/// {
		///     if (collection is C1MenuItemCollection)
		///     {
		///         ((C1MenuItemCollection)collection).Add((C1MenuItem)item);
		///         return true;
		///     }
		///     return false;
		/// }
		/// </code>
		/// </example>
		/// <remarks>This method preliminary used by control developers.</remarks>
		/// <param name="collection"></param>
		/// <param name="item"></param>
		/// <returns>Return true if object is added to array or collection.</returns>
        protected override bool OnAddItem(object collection, object item)
        {
            if (collection is C1TreeViewNodeCollection)
            {
                ((C1TreeViewNodeCollection)collection).Add((C1TreeViewNode)item);
                return true;
            }
            return base.OnAddItem(collection, item);
        }

		/// <summary>
		/// Override this method to implement custom clear for deserialized array.
		/// </summary>
		/// <example>
		/// Override example:
		/// <code>
		/// protected override bool OnClearItems(object collection)
		/// {
		///     if (collection is C1MenuItemCollection)
		///     {
		///         ((C1MenuItemCollection)collection).Clear();
		///         return true;
		///     }
		///     return false;
		/// }
		/// </code>
		/// </example>
		/// <remarks>This method preliminary used by control developers.</remarks>
		/// <param name="collection"></param>
		/// <returns>Return true if array or collection is cleared.</returns>
        protected override bool OnClearItems(object collection)
        {
            if (collection is C1TreeViewNodeCollection)
            {
                ((C1TreeViewNodeCollection)collection).Clear();
                return true;
            }
            return base.OnClearItems(collection);
        }

        #endregion
    }
}