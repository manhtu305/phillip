using System;

namespace C1.Web.Wijmo.Controls.C1TreeView
{

    /// <summary>
    /// Specifies <see cref="C1TreeViewNode"/> check state.
    /// </summary>
    public enum C1TreeViewNodeCheckState
    {
        /// <summary>
        /// Checked.
        /// </summary>
        Checked = 0,
        /// <summary>
        /// Indeterminate
        /// </summary>
        Indeterminate = 1,
        /// <summary>
        /// UnChecked
        /// </summary>
        UnChecked = 2
    }

    /// <summary>
    /// Contains event data for all events raised by the <see cref="C1TreeView"/> control.  
    /// </summary>
	public class C1TreeViewEventArgs : EventArgs
	{
		#region fields

		private C1TreeViewNode _node;

		#endregion

		#region constructor

        /// <summary>
        /// Explicit constructor for <see cref="C1TreeViewEventArgs"/> 
        /// </summary>
        /// <param name="node">A <see cref="C1TreeViewNode"/> that represents the current node when the event is raised.</param>
		public C1TreeViewEventArgs(C1TreeViewNode node)
		{
			_node = node;
		}

		#endregion

		#region properties

        /// <summary>
        /// <see cref="C1TreeViewNode"/> node associated to the event data.
        /// </summary>
		public C1TreeViewNode Node
		{
			get
			{
				return _node;
			}
		}

		#endregion
	}

    /// <summary>
    /// Provides data for the NodeDropped event of the <see cref="C1TreeView"/> class. 
    /// </summary>
    public class C1TreeViewNodeDroppedEventArgs : C1TreeViewEventArgs
    {
        #region fields

		private bool _handled;
        private bool _copyUsed;

		#endregion

		#region constructor

        /// <summary>
        /// Explicit constructor for <see cref="C1TreeViewEventArgs"/> 
        /// </summary>
        /// <param name="node">A <see cref="C1TreeViewNode"/> that represents the current node when the event is raised.</param>
        /// <param name="handled">A value that indicates whether the dropped event has been managed from the outside.</param>
        /// <param name="copyUsed">A value that indicates whether the copy method of <see cref="C1TreeViewNode"/> node has been used to perform the drop.</param>
        public C1TreeViewNodeDroppedEventArgs(C1TreeViewNode node, bool handled, bool copyUsed ):base(node)
		{
            _handled = handled;
            _copyUsed = copyUsed;
		}

		#endregion

		#region properties

        /// <summary>
        /// Gets a value that indicates whether the dropped event has been managed from the outside. 
        /// </summary>
        public bool Handled
        {
            get { return _handled; }
        }
        
        /// <summary>
        /// Gets a value that indicates whether the copy method of <see cref="C1TreeViewNode"/> node has been used to perform the drop. 
        /// </summary>
        public bool CopyUsed
        {
            get { return _copyUsed; }
        }

		#endregion

    }

	/// <summary>
	/// Delegate type for handling events that are related to the nodes.
	/// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e"><see cref="C1TreeViewEventArgs"/><see cref="C1TreeViewEventArgs"/> object that contains the event data.</param>
	public delegate void C1TreeViewEventHandler(object sender, C1TreeViewEventArgs e);


}


