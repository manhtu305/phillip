using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using C1.Web.Wijmo.Controls.Base;
using System.Globalization;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1TreeView
{
    /// <summary>
    /// Represents a node in the <see cref="C1TreeView"/> control. 
    /// </summary>
    [ToolboxItem(false)]
    public class C1TreeViewNode : UIElement, INamingContainer,
        IC1TreeViewNodeCollectionOwner, IComparable, IJsonRestore
    {
        #region ** Fields

        private Hashtable _properties = new Hashtable();
        internal C1TreeViewNodeCollection _nodes;
        private IC1TreeViewNodeCollectionOwner _owner;
        private C1TreeView _treeView;
        private ITemplate _template;

        private int _nodeIndex;
        private string _staticKey;
        private string _guid;
        private HtmlControl _body;
        private HtmlControl _checkBox;
        private HtmlControl _icon;
        private HtmlControl _link;
        private HtmlControl _text;
        private HtmlControl _hitArea;
        private HtmlGenericControl _templateContainer;
        internal HtmlControl _nodesContainer;

        #endregion

        #region ** constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1TreeViewNode"/> class.
        /// </summary>
        public C1TreeViewNode()
        {
            _nodes = new C1TreeViewNodeCollection(this);
            _nodesContainer = new HtmlGenericControl("ul");

            this.ItemIconClass = string.Empty;
            this.ExpandedIconClass = string.Empty;
            this.CollapsedIconClass = string.Empty;
        }

        #endregion

        #region ** Properties

        /// <summary>
        /// Gets a <see cref="C1TreeViewNodeCollection"/> that contains the first-level child nodes of the current node.
        /// </summary>
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [Layout(LayoutType.Behavior)]
        [Browsable(false)]
        [WidgetOption]
        public C1TreeViewNodeCollection Nodes
        {
            get
            {
                return _nodes;
            }
        }

        /// <summary>
        /// Gets the server control identifier.
        /// </summary>
        [WidgetOption]
        [Browsable(false)]
        [WidgetOptionName("guid")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string Guid
        {
            get
            {
                return _guid;
            }
            internal set
            {
                this._guid = value;
            }
        }

        /// <summary>
        /// Index of the C1TreeViewNode.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [WidgetOption]
        public int NodeIndex
        {
            get
            {
                return _nodeIndex;
            }
            set
            {
                _nodeIndex = value;
            }
        }

        /// <summary>
        /// The owner of <see cref="C1TreeViewNodeCollection"/> to which this node belongs to.
        /// </summary>
        [Browsable(false)]
        public IC1TreeViewNodeCollectionOwner Owner
        {
            get
            {
                return this._owner;
            }
            internal set
            {
                _owner = value;
            }

        }

        /// <summary>
        /// Gets or Sets the StaticKey for C1TreeViewNode.
        /// </summary>
        [WidgetOption]
        [Json(true)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string StaticKey
        {
            get
            {
                return _staticKey;
            }
            //internal set
            set
            {
                _staticKey = value;
            }
        }

        /// <summary>
        /// Sets or gets the navigate url link of the node
        /// </summary>
        [C1Description("C1TreeViewNode.Url")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public string Url
        {
            get
            {
                return _properties["Url"] == null ? ""
                    : _properties["Url"].ToString();
            }
            set
            {
                _properties["Url"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the text the node .
        /// </summary>
        [C1Description("C1TreeViewNode.Text")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public string Text
        {
            get
            {
                return _properties["Text"] == null ? ""
                    : _properties["Text"].ToString();
            }
            set
            {
                _properties["Text"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the value the node .
        /// </summary>
        [C1Description("C1TreeViewNode.Value")]
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public string Value
        {
            get
            {
                return _properties["Value"] == null ? ""
                    : _properties["Value"].ToString();
            }
            set
            {
                _properties["Value"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the expanded state of the node.
        /// </summary>
        [C1Description("C1TreeViewNode.Expanded")]
        [DefaultValue(false)]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool Expanded
        {
            get
            {
                return _properties["Expanded"] == null ? false
                    : (bool)_properties["Expanded"];
            }
            set
            {
                _properties["Expanded"] = value;
            }
        }

        /// <summary>
        /// Sets or gets the check state of the node. 
        /// </summary>
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [Localizable(true)]
        [C1Description("C1TreeViewNode.Checked")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool Checked
        {
            get
            {
                object obj2 = this._properties["Checked"];
                if (obj2 == null)
                {
                    return false;
                }
                return (bool)obj2;
            }
            set
            {
                SetChecked(value, false);
            }
        }

        /// <summary>
        /// Sets or gets the select state of the node. 
        /// </summary>
        [DefaultValue(false)]
        [C1Description("C1TreeViewNode.Selected")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool Selected
        {
            get
            {
                return _properties["Selected"] == null ? false
                    : (bool)_properties["Selected"];
            }
            set
            {
                _properties["Selected"] = value;
            }
        }


        /// <summary>
        /// Set the icon class of the node when it's expanded.
        /// </summary>
        [DefaultValue("")]
        [Layout(LayoutType.Appearance)]
        [C1Description("C1TreeViewNode.ExpandedIconClass")]
        [WidgetOption]
        public string ExpandedIconClass
        {
            get
            {
                return _properties["ExpandedIconClass"] == null ? ""
                    : _properties["ExpandedIconClass"].ToString();
            }
            set
            {
                _properties["ExpandedIconClass"] = value;
            }
        }


        /// <summary>
        /// Set the icon class of the node when it's collapsed.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1TreeViewNode.CollapsedIconClass")]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public string CollapsedIconClass
        {
            get
            {
                return _properties["CollapsedIconClass"] == null ? ""
                    : _properties["CollapsedIconClass"].ToString();
            }
            set
            {
                _properties["CollapsedIconClass"] = value;
            }
        }

        /// <summary>
        /// Set the icon class of the node.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1TreeViewNode.ItemIconClass")]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public string ItemIconClass
        {
            get
            {
                return _properties["ItemIconClass"] == null ? ""
                    : _properties["ItemIconClass"].ToString();
            }
            set
            {
                _properties["ItemIconClass"] = value;
            }
        }

        /// <summary>
        /// Gets reference to <see cref="C1TreeView"/> TreeView object.
        /// </summary>
        [Browsable(false)]
        public C1TreeView TreeView
        {
            get
            {
                if (_treeView == null)
                {
                    if (this.Owner != null)
                    {
                        if (this.Owner is C1TreeViewNode)
                        {
                            _treeView = ((C1TreeViewNode)this.Owner).TreeView;
                        }
                        else if (this.Owner is C1TreeView)
                        {
                            _treeView = (C1TreeView)this.Owner;
                        }
                    }
                }
                return _treeView;
            }
            internal set { _treeView = value; }
        }

        /// <summary>
        /// Gets or sets the template that will be used for the specified C1TreeViewNodes.
        /// </summary>
        [C1Description("C1TreeViewNode.Template")]
        //[TemplateContainer(typeof(C1TreeViewNodeTemplateContainer))]
        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public virtual ITemplate Template
        {
            get
            {
                return this._template;
            }
            set
            {
                this._template = value;
                //this.Templated = (value != null);
            }
        }

        /// <summary>
        /// Gets value that indicates if <see cref="C1TreeViewNode"/> node will be initialized as an template.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        [WidgetOption]
        public bool Templated
        {
            get
            {
                object obj2 = this._properties["Templated"];
                if (obj2 == null)
                {
                    return false;
                }
                return (bool)obj2;
            }
            internal set
            {
                this._properties["Templated"] = value;
            }
        }

        /// <summary>
        /// Gets value that can be used for tracking original position inside of data source.
        /// This property only used if <see cref="C1TreeView"/> tree view is bounded to data source and callbacks are enabled.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        [WidgetOption]
        public ArrayList SearchIndexes
        {
            get { return (ArrayList)_properties["SearchIndexes"]; }
            set { _properties["SearchIndexes"] = value; }
        }

        /// <summary>
        /// Gets value that indicates if this <see cref="C1TreeViewNode"/> node has child nodes which can be initialized later.
        /// This property only used if <see cref="C1TreeView"/> treeview is bounded to data source and callbacks are enabled.
        /// </summary>
        [Browsable(false)]
        [DefaultValue(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [WidgetOption]
        public bool HasChildren
        {
            get
            {
                object obj2 = this._properties["HasPotentialChildren"];
                if (obj2 == null)
                {
                    return false;
                }
                return (bool)obj2;
            }
            set
            {
                this._properties["HasPotentialChildren"] = value;
            }
        }

        /// <summary>
        /// Gets a value that indicates treeview node's check state.
        /// </summary>
        [DefaultValue(C1TreeViewNodeCheckState.UnChecked)]
        [C1Category("Category.Behavior")]
        [Localizable(true)]
        [C1Description("C1TreeViewNode.CheckState")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public C1TreeViewNodeCheckState CheckState
        {
            get
            {
                object obj = this._properties["CheckState"];
                if (obj == null)
                {
                    if (this.Checked)
                    {
                        return C1TreeViewNodeCheckState.Checked;
                    }
                    return C1TreeViewNodeCheckState.UnChecked;
                }
                return (C1TreeViewNodeCheckState)obj;
            }
            //internal set 
            set
            {
                this._properties["CheckState"] = value;
            }
        }

        internal HtmlGenericControl TemplateContainer
        {
            get
            {
                return this._templateContainer;
            }
            set
            {
                this._templateContainer = value;
            }
        }

        /// <summary>
        /// Gets zero-based Level of current <see cref="C1TreeViewNode"/> node.
        /// </summary>
        /// <returns></returns>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int Level
        {
            get { return CalculateCurrentLevel(); }
        }

        /// <summary>
        /// Hidden this property.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
            }
        }

        /// <summary>
		/// Allow to node to perform drag.
        /// </summary>
        [C1Description("C1TreeViewNode.AllowDrag")]
        [C1Category("Category.Behavior")]
        [DefaultValue(null)]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool? AllowDrag
        {
            get
            {
                return (bool?)_properties["AllowDrag"];
            }
            set
            {
                _properties["AllowDrag"] = value;
            }
        }

        /// <summary>
		/// Allow to node to perform drop.
        /// </summary>
        [C1Description("C1TreeViewNode.AllowDrop")]
        [C1Category("Category.Behavior")]
        [DefaultValue(null)]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool? AllowDrop
        {
            get
            {
                return (bool?)_properties["AllowDrop"];
            }
            set
            {
                _properties["AllowDrop"] = value;
            }
        }
        //

        #endregion

        #region ** Data Binding
        internal virtual bool DataBind(C1TreeViewNodeBinding binding, IHierarchicalEnumerable enumerable, object data, bool auto)
        {
            bool bound = false;
            IHierarchyData hdata = enumerable.GetHierarchyData(data);
            string hType = hdata.Type;
            if (binding != null)
            {
                PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(data);

                string aText = null;
                if (!string.IsNullOrEmpty(binding.TextField))
                {
                    if (GetFieldValue(data, pdc, binding.TextField) != null)
                    {
                        aText = (string)GetFieldValue(data, pdc, binding.TextField);
                        bound = true;
                    }
                }

                // Format with current culture.
                if (aText != null)
                {
                    if (binding.FormatString.Length > 0)
                    {
                        Text = string.Format(CultureInfo.CurrentCulture, binding.FormatString, new object[] { aText });
                    }
                    else
                    {
                        Text = aText;
                    }
                }

                // Value
                if (!string.IsNullOrEmpty(binding.ValueField))
                {
                    if (GetFieldValue(data, pdc, binding.ValueField) != null)
                    {
                        this.Value = (string)GetFieldValue(data, pdc, binding.ValueField);
                        bound = true;
                    }
                }

                // Url
                if (!string.IsNullOrEmpty(binding.NavigateUrlField))
                {
                    if (GetFieldValue(data, pdc, binding.NavigateUrlField) != null)
                    {
                        this.Url = (string)GetFieldValue(data, pdc, binding.NavigateUrlField);
                        bound = true;
                    }
                }

                // ItemIconClass
                if (!string.IsNullOrEmpty(binding.ItemIconClassField))
                {
                    if (GetFieldValue(data, pdc, binding.ItemIconClassField) != null)
                    {
                        this.ItemIconClass = (string)GetFieldValue(data, pdc, binding.ItemIconClassField);
                        bound = true;
                    }
                }
                // CollapsedIconClass
                if (!string.IsNullOrEmpty(binding.CollapsedIconClassField))
                {
                    if (GetFieldValue(data, pdc, binding.CollapsedIconClassField) != null)
                    {
                        this.CollapsedIconClass = (string)GetFieldValue(data, pdc, binding.CollapsedIconClassField);
                        bound = true;
                    }
                }
                // ExpandedIconClass
                if (!string.IsNullOrEmpty(binding.ExpandedIconClassField))
                {
                    if (GetFieldValue(data, pdc, binding.ExpandedIconClassField) != null)
                    {
                        this.ExpandedIconClass = (string)GetFieldValue(data, pdc, binding.ExpandedIconClassField);
                        bound = true;
                    }
                }
                // Expanded
                if (!string.IsNullOrEmpty(binding.ExpandedField) && !this.TreeView.LoadOnDemand)
                {
                    if (GetFieldValue(data, pdc, binding.ExpandedField) != null)
                    {
                        this.Expanded = Convert.ToBoolean(GetFieldValue(data, pdc, binding.ExpandedField));
                        bound = true;
                    }
                }
                // Enabled
                if (!string.IsNullOrEmpty(binding.EnabledField))
                {
                    if (GetFieldValue(data, pdc, binding.EnabledField) != null)
                    {
                        this.Enabled = Convert.ToBoolean(GetFieldValue(data, pdc, binding.EnabledField));
                        bound = true;
                    }
                }
            }
            else if (data is INavigateUIData)
            {
                INavigateUIData n_data = (INavigateUIData)data;
                Text = n_data.Name;
                Value = n_data.Value;
                Url = n_data.NavigateUrl;
                bound = true;
            }
            if (auto && String.IsNullOrEmpty(this.Text)
                && String.IsNullOrEmpty(this.ItemIconClass))
            {
                Text = data.ToString();
                bound = true;
            }
            return bound;
        }

        private object GetFieldValue(object data, PropertyDescriptorCollection propCollection, string propName)
        {
            PropertyDescriptor descr = propCollection.Find(propName, true);
            if (descr != null)
            {
                return descr.GetValue(data);
            }
            else return null;
        }

        internal virtual void DataBindInternal(IHierarchicalEnumerable enumerable, int depth, C1TreeViewNodeBinding parBinding)
        {
            bool hasChild = false;
            foreach (object o in enumerable)
            {
                hasChild = true;
                IHierarchyData data = enumerable.GetHierarchyData(o);
                string text = data.Type;
                C1TreeViewNode node = this.TreeView.CreateTreeViewNode();
                //((IStateManager)node).TrackViewState();
                C1TreeViewNodeBinding binding = this.TreeView.GetBinding(text, depth);
                node.DataBind(binding, enumerable, o, this.TreeView.AutoGenerateDataBindings);
                //if (node.DataBind(binding, enumerable, o, this.TreeView.AutoGenerateDataBindings))
                //{
                this.Nodes.Add(node);

                if (this.TreeView.LoadOnDemand)
                {
                    node.SearchIndexes = new ArrayList();
                    node.SearchIndexes.AddRange(this.SearchIndexes);
                    node.SearchIndexes.Add(this.Nodes.IndexOf(node));
                }
                if (data.HasChildren)
                {
                    if (this.TreeView.DataBindStartLevel > node.CalculateCurrentLevel() || this.TreeView.DataBindStartLevel == -1)
                    {
                        IHierarchicalEnumerable childEnum = data.GetChildren();
                        if (childEnum != null)
                        {
                            node.DataBindInternal(childEnum, depth + 1, binding);
                        }
                    }
                    //Jul 30,2010 add comments by daniel.he to fix bug #12051
                    else
                    {
                        node.HasChildren = true;
                        node.Expanded = false;
                    }
                    //end comments
                }
                else
                {
                    node.Expanded = false;
                }

            }

            if (hasChild)
            {
                this.HasChildren = true;
            }
        }


        #endregion

        #region ** Override Methods
        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Li;
            }
        }

        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based
        ///	implementation to create any child controls they contain in preparation for
        ///	posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            //if (this.Container != null)
            //{
            //    //this.Controls.Add(this.Container);
            //}
            _nodesContainer.Controls.Clear();
            this.Controls.Add(_nodesContainer);

            foreach (C1TreeViewNode node in this.Nodes)
            {
                //node.AttachToParents(this);
                if (!_nodesContainer.Controls.Contains(node))
                {
                    _nodesContainer.Controls.Add(node);
                }
            }
            this.EnsureChildControls();
        }

        /// <summary>
        /// Renders the control to the specified HTML writer.
        /// </summary>
        /// <param name="writer">
        /// The System.Web.UI.HtmlTextWriter object that receives the control content.
        /// </param>
        protected override void Render(HtmlTextWriter writer)
        {
            this.EnsureChildControls();

            if (!this.DesignMode)
                this.AssignGuid();
            base.Render(writer);
        }


        /// <summary>
        /// Adds HTML attributes and styles that need to be rendered to the specified
        ///	System.Web.UI.HtmlTextWriterTag. This method is used primarily by control
        ///	developers.
        /// </summary>
        /// <param name="writer">
        /// A System.Web.UI.HtmlTextWriter that represents the output stream to render
        ///	HTML content on the client.
        /// </param>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);

            if (this.IsDesignMode)
            {
                string oldClass = string.Empty;
                if (this.CssClass != string.Empty)
                {
                    oldClass = CssClass + " ";
                }

                if (this.Nodes.Count != 0 || this.HasChildren)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, oldClass + "wijmo-wijtree-parent");
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, oldClass + "wijmo-wijtree-item");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(this.ItemIconClass))
                {
                    this.Attributes.Add("itemiconclass", this.ItemIconClass);
                }
                if (string.IsNullOrEmpty(this.ExpandedIconClass))
                {
                    this.Attributes.Add("expandediconclass", this.ExpandedIconClass);
                }
                if (string.IsNullOrEmpty(this.CollapsedIconClass))
                {
                    this.Attributes.Add("collapsediconclass", this.CollapsedIconClass);
                }
            }
        }

        /// <summary>
        /// Outputs the content of a server control's children to a provided System.Web.UI.HtmlTextWriter
        /// object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">
        /// The System.Web.UI.HtmlTextWriter object that receives the rendered content.
        /// </param>
        protected override void RenderChildren(HtmlTextWriter writer)
        {
            _body = new HtmlGenericControl("div");
            if (this.IsDesignMode)
            {
                _body.Attributes.Add("class",
                    "wijmo-wijtree-node wijmo-wijtree-header ui-state-default");
                _body.Style.Add(HtmlTextWriterStyle.Width, "100%");

                HtmlGenericControl bodyWrapper = new HtmlGenericControl("span");
                bodyWrapper.Style.Add(HtmlTextWriterStyle.BorderWidth, "0");

                //bodyWrapper.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#dddddd");				
                //bodyWrapper.Style.Add(HtmlTextWriterStyle.Width, "100%");
                //bodyWrapper.Style.Add(HtmlTextWriterStyle.Display, "block");

                if (this.Selected)
                {
                    bodyWrapper.Attributes.Add("class",
                        "ui-helper-clearfix wijmo-wijtree-inner ui-corner-all ui-state-active");
                }
                else
                {
                    bodyWrapper.Attributes.Add("class",
                        "ui-helper-clearfix wijmo-wijtree-inner ui-corner-all");
                }

                _body.Controls.Add(bodyWrapper);

                // show hitarea & nodes
                if ((this.Nodes.Count != 0 || this.HasChildren) && this.TreeView.ShowExpandCollapse)
                {
                    _hitArea = new HtmlGenericControl("span");
                    string hitCss = "";
                    if (this.Expanded == false)
                    {
                        hitCss = "ui-icon ui-icon-triangle-1-e";
                        _nodesContainer.Style.Add(HtmlTextWriterStyle.Display, "none");
                    }
                    else
                    {
                        hitCss = "ui-icon ui-icon-triangle-1-se";

                    }
                    _hitArea.Attributes.Add("class", hitCss);//VisualizeHitArea()
                    bodyWrapper.Controls.Add(_hitArea);
                }
                // show icon
                if ((!string.IsNullOrEmpty(this.CollapsedIconClass) &&
                    !string.IsNullOrEmpty(this.ExpandedIconClass))
                    || !string.IsNullOrEmpty(this.ItemIconClass))
                {
                    this._icon = new HtmlGenericControl("span");
                    if (!string.IsNullOrEmpty(this.CollapsedIconClass) &&
                    !string.IsNullOrEmpty(this.ExpandedIconClass))
                    {
                        if (this.Expanded)
                        {
                            _icon.Attributes.Add("class", "ui-icon " + ExpandedIconClass);
                        }
                        else
                        {
                            _icon.Attributes.Add("class", "ui-icon " + CollapsedIconClass);
                        }
                    }
                    else
                    {
                        _icon.Attributes.Add("class", "ui-icon " + ItemIconClass);
                    }
                    bodyWrapper.Controls.Add(_icon);
                }
                // show checkbox
                if (this.TreeView.ShowCheckBoxes)
                {
                    _checkBox = new HtmlGenericControl("div");
                    _checkBox.Attributes.Add("class", "wijmo-checkbox ui-widget");

                    HtmlControl checkBoxInner = new HtmlGenericControl("div");
                    checkBoxInner.Attributes.Add("class",
                        "wijmo-checkbox-box ui-widget ui-corner-all ui-state-default");
                    checkBoxInner.Style.Add(HtmlTextWriterStyle.Position, "relative");
                    _checkBox.Controls.Add(checkBoxInner);
                    HtmlControl checkBoxIcon = new HtmlGenericControl("span");
                    checkBoxInner.Controls.Add(checkBoxIcon);
                    switch (CheckState)
                    {
                        case C1TreeViewNodeCheckState.Checked:
                            checkBoxIcon.Attributes.Add("class",
                                "wijmo-checkbox-icon ui-icon ui-icon-check");
                            break;
                        case C1TreeViewNodeCheckState.Indeterminate:
                            checkBoxIcon.Attributes.Add("class",
                                "wijmo-checkbox-icon ui-icon ui-icon-stop");
                            break;
                        case C1TreeViewNodeCheckState.UnChecked:
                            checkBoxIcon.Attributes.Add("class", "wijmo-checkbox-icon");
                            break;
                        default:
                            checkBoxIcon.Attributes.Add("class", "wijmo-checkbox-icon");
                            break;
                    }
                    bodyWrapper.Controls.Add(_checkBox);
                }

                if (!Templated)
                {
                    _link = new HtmlGenericControl("a");
                    _link.Attributes.Add("href", "#");
                    //_text = new HtmlGenericControl("span");
                    //_link.Controls.Add(_text);
                    //((HtmlContainerControl)_text).InnerHtml = this.Text;
                    ((HtmlContainerControl)_link).InnerHtml = this.Text;

                    bodyWrapper.Controls.Add(_link);
                }
                else
                {
                    if (this.TemplateContainer != null)
                    {
                        bodyWrapper.Controls.Add(this.TemplateContainer);
                    }
                }

                this.Controls.Add(_body);

                _body.RenderControl(writer);

                _nodesContainer.Attributes.Add("class",
                    "wijmo-wijtree-list ui-helper-reset wijmo-wijtree-child");

                if (Nodes.Count > 0)
                {
                    _nodesContainer.RenderControl(writer);
                }
            }
            else
            {
                if (!Templated)
                {
                    _link = new HtmlGenericControl("a");
                    if (string.IsNullOrEmpty(this.Url))
                    {
                        _link.Attributes.Add("href", "#");
                    }
                    else
                    {
                        _link.Attributes.Add("href", base.ResolveClientUrl(this.Url));
                    }
                    _text = new HtmlGenericControl("span");
                    _link.Controls.Add(_text);
                    ((HtmlContainerControl)_text).InnerHtml = this.Text;
                    this.Controls.Add(_link);
                    _link.RenderControl(writer);
                }
                else
                {
                    if (this.TemplateContainer != null)
                    {
                        _body.Controls.Add(this.TemplateContainer);
                    }
                    this.Controls.Add(_body);
                    _body.RenderControl(writer);
                }

                if (Nodes.Count > 0)
                {
                    _nodesContainer.RenderControl(writer);
                }
            }
        }

        #endregion

        #region ** Public Methods
        /// <summary>
        /// Determines if NavigateUrl property match value of SearchedUrl.
        /// </summary>
        /// <param name="searchedUrl"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public bool IsSearchedUrl(string searchedUrl, Page page)
        {
            bool isSearchedUrl = false;

            string nodeUrl = page.ResolveUrl(this.Url);

            Uri nodeUri;
            bool createdNode = Uri.TryCreate(nodeUrl, UriKind.RelativeOrAbsolute, out nodeUri);

            Uri searchedUri;
            bool reatedSearched = Uri.TryCreate(searchedUrl, UriKind.RelativeOrAbsolute, out  searchedUri);

            if (createdNode && createdNode)
            {
                if (Uri.Compare(nodeUri, searchedUri, UriComponents.HttpRequestUrl, UriFormat.Unescaped, StringComparison.CurrentCultureIgnoreCase) == 0)
                    isSearchedUrl = true;
            }
            return isSearchedUrl;
        }
        #endregion

        #region ** Private & internal methods

        internal int CalculateCurrentLevel()
        {
            if (_owner != null && _owner is C1TreeViewNode)
            {
                return ((C1TreeViewNode)_owner).CalculateCurrentLevel() + 1;
            }
            else
            {
                return 0;
            }
        }


        internal void SetChecked(bool isChecked, bool isSetInAssembly)
        {
            this._properties["Checked"] = isChecked;

            if (this.TreeView != null && this.TreeView.ShowCheckBoxes)
            {
                if (isChecked)
                {
                    if (!this.TreeView.CheckedNodes.Contains(this))
                    {
                        this.TreeView.CheckedNodes.Add(this);
                    }
                    //this.CheckState = C1TreeViewNodeCheckState.Checked;
                }
                else
                {
                    if (this.TreeView.CheckedNodes.Contains(this))
                    {
                        this.TreeView.CheckedNodes.Remove(this);
                    }
                    //this.CheckState = C1TreeViewNodeCheckState.UnChecked;
                }

                if (!isSetInAssembly)
                {
                    this.Check(isChecked);
                }
            }
        }

        internal void Check(bool isChecked)
        {
            if (this.TreeView.AutoCheckNodes)
            {
                foreach (C1TreeViewNode node in this.Nodes)
                {
                    node.Checked = isChecked;
                }
            }
        }

        internal void AssignGuid()
        {
            this.Guid = System.Guid.NewGuid().ToString();
        }

        #endregion

        #region ** IComparable Members

        int IComparable.CompareTo(object obj)
        {
            C1TreeViewNode node = (C1TreeViewNode)obj;
            return String.Compare(this.Text, node.Text);
        }

        #endregion

        #region ** IJsonRestore interface implementations
        void IJsonRestore.RestoreStateFromJson(object state)
        {
            this.RestoreStateFromJson(state);
        }

        protected virtual void RestoreStateFromJson(object state)
        {
            JsonRestoreHelper.RestoreStateFromJson(this, state);
        }
        #endregion

    }
}
