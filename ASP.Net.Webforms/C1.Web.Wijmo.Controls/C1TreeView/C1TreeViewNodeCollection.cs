using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1TreeView
{
	/// <summary>
	/// Represents the structure that manages the <see cref="C1TreeViewNode"/> nodes.
	/// </summary>
    public class C1TreeViewNodeCollection : C1ObservableItemCollection<IC1TreeViewNodeCollectionOwner, C1TreeViewNode>/*, IStateManager*/
	{
		#region ** fields
		
		#endregion

		#region ** constructor

		/// <summary>
		/// Explicit constructor for <see cref="C1TreeViewNodeCollection"/>.
		/// </summary>
		/// <param name="owner"></param>
        public C1TreeViewNodeCollection(IC1TreeViewNodeCollectionOwner owner)
			: base(owner)
		{ }

		
		#endregion

		#region ** methods

		/// <summary>
		/// Checks <see cref="C1TreeViewNode"/> nodes contained by the collection.
		/// </summary>
		/// <param name="check"></param>
		public void CheckNodes(bool check)
		{
			foreach (C1TreeViewNode node in this)
			{
				node.Checked = check;
				if (check)
				{
					node.CheckState = C1TreeViewNodeCheckState.Checked;
				}
				else
				{
					node.CheckState = C1TreeViewNodeCheckState.UnChecked;
				}
				node.Nodes.CheckNodes(check);
			}
		}
		
		/// <summary>
		/// Adds <see cref="C1TreeViewNode"/> node to the collection.
		/// </summary>
		/// <param name="node">node to add</param>
		public new void Add(C1TreeViewNode node)
		{
			if (node != null)
			{
				Insert(base.Count, node);
			}
		}

		/// <summary>
		/// Inserts <see cref="C1TreeViewNode"/> node to the collection.
		/// </summary>
		/// <param name="index">collection index</param>
		/// <param name="node">node to insert</param>
		public new void Insert(int index, C1TreeViewNode node)
		{
			AddSelectCheckNodes(node);
			node.Owner = this.Owner;
			base.InsertItem(index, node);
			//((IStateManager)node).TrackViewState();

			if (node.NodeIndex == -1)
			{
				if (index < base.Count)
				{
					for (int i = 0; i < base.Count; i++)
					{
						if (this[i].NodeIndex >= index)
						{
							this[i].NodeIndex = this[i].NodeIndex + 1;
						}
					}
				}

				node.NodeIndex = index;

				if (!(GetParentTreeView() == null) && GetParentTreeView().AllowSorting)
				{
					SortNodes();
				}
			}
		}

		/// <summary>
		/// Removes <see cref="C1TreeViewNode"/> node from the collection.
		/// <param name="node">node to remove</param>
		/// </summary>
		public new void Remove(C1TreeViewNode node)
		{
			int index = IndexOf(node);
			RemoveSelectCheckNodes(node);
			node.Owner = null;
			this.RemoveItem(index);

			if (index < base.Count)
			{
				for (int i = 0; i < base.Count; i++)
				{
					if (this[i].NodeIndex >= index)
					{
						this[i].NodeIndex = this[i].NodeIndex - 1;
					}
				}
			}
		}

		internal void SortNodes()
		{
			ArrayList list = new ArrayList(base.Count);

			foreach (C1TreeViewNode node in this)
			{
				list.Add(node);
			}
			list.Sort();

			Clear();
			foreach (C1TreeViewNode node in list)
			{
				Add(node);
				//this[base.Count] = node;
			}
		}
		internal void UnSortNodes()
		{
			ArrayList list = new ArrayList(base.Count);

			foreach (C1TreeViewNode node in this)
			{
				list.Add(node);
			}
			foreach (C1TreeViewNode node in list)
			{
				this[node.NodeIndex] = node;
			}
		}

		private C1TreeView GetParentTreeView()
		{
			C1TreeView tv;
			if (this.Owner is C1TreeView)
			{
				tv = this.Owner as C1TreeView;
			}
			else
			{
				tv = (this.Owner as C1TreeViewNode).TreeView;
			}
			return tv;
		}
		private void AddSelectCheckNodes(C1TreeViewNode node)
		{
			if(!node.Checked && !node.Selected)
			{
				return;
			}
			C1TreeView tv = GetParentTreeView();
			if (tv == null)
			{
				return;
			}
			AddSelectCheckNodes(node, tv);
		}
		private void AddSelectCheckNodes(C1TreeViewNode node, C1TreeView treeView)
		{
			if (node.Checked)
			{
				if (treeView.ShowCheckBoxes)
				{
                    // fixed bug 35341 by Daniel.He @ 20130326
					// throw new Exception(C1Localizer.GetString("C1TreeNodeCollection.CannotSetChecked"));
                    if (!treeView.CheckedNodes.Contains(node))
                    {
                        treeView.CheckedNodes.Add(node);
                    }
				}
				
			}
			if (node.Selected)
			{
				if (!treeView.SelectedNodes.Contains(node))
				{
					treeView.SelectedNodes.Add(node);
				}
			}
			if (node.Nodes.Count > 0)
			{
				foreach (C1TreeViewNode tvNode in node.Nodes)
				{
					AddSelectCheckNodes(tvNode, treeView);
				}
			}
		}	

		private void RemoveSelectCheckNodes(C1TreeViewNode node)
		{
			if (!node.Checked && !node.Selected)
			{
				return;
			}
			C1TreeView tv = GetParentTreeView();
			if (tv == null)
			{
				return;
			}
			RemoveSelectCheckNodes(node, tv);
		}

		private void RemoveSelectCheckNodes(C1TreeViewNode node, C1TreeView treeView)
		{
			if (node.Checked)
			{
				if (treeView.CheckedNodes.Contains(node))
				{
					treeView.CheckedNodes.Remove(node);
				}
			}
			if (node.Selected)
			{
				if (treeView.SelectedNodes.Contains(node))
				{
					treeView.SelectedNodes.Remove(node);
				}
			}
			if (node.Nodes.Count > 0)
			{
				foreach (C1TreeViewNode tvNode in node.Nodes)
				{
					RemoveSelectCheckNodes(tvNode, treeView);
				}
			}
		}

		/// <summary>
		/// Searches <see cref="C1TreeViewNode"/> node through the collection by the Value. 
		/// </summary>
		/// <param name="value">node property</param>
		/// <returns>First <see cref="C1TreeViewNode"/> node that match the searched text.</returns>
		public C1TreeViewNode FindNodeByValue(string value)
		{
			foreach (C1TreeViewNode node in this)
			{
				if (node.Value == value)
				{
					return node;
				}
			}
			return null;
		}

		/// <summary>
		/// Searches <see cref="C1TreeViewNode"/> node through the collection by the Text.
		/// </summary>
		/// <param name="text"></param>
		/// <returns>First <see cref="C1TreeViewNode"/> node that match the searched text.</returns>
		public C1TreeViewNode FindNodeByText(string text)
		{
			foreach (C1TreeViewNode node in this)
			{
				if (node.Text == text)
				{
					return node;
				}
			}
			return null;
		}

		internal C1TreeViewNode FindNodeByNavigateUrl(string searchedUrl, Page page)
		{
			foreach (C1TreeViewNode node in this)
			{
				if (node.IsSearchedUrl(searchedUrl, page))
				{
					return node;
				}
			}
			return null;
		}

	
		/// <summary>
		/// Searches <see cref="C1TreeViewNode"/> node through the collection by id.
		/// </summary>
		/// <param name="id">node id</param>
		/// <returns></returns>
		public C1TreeViewNode FindNode(string id)
		{
			foreach (C1TreeViewNode node in this)
			{
				if (node.ID == id)
				{
					return node;
				}
			}
			return null;

		}		
		#endregion		
	}
}
