using System;
using System.Collections.Generic;
using System.Text;


namespace C1.Web.Wijmo.Controls.C1TreeView
{
	/// <summary>
	/// Used to establish a relationship between the <see cref="C1TreeViewNodeCollection"/> nodes collection 
	/// and its <see cref="IC1TreeViewNodeCollectionOwner"/> owner.
	/// </summary>
	public interface IC1TreeViewNodeCollectionOwner
	{
		/// <summary>
		/// Collection that contains <see cref="C1TreeViewNode"/> nodes.
		/// </summary>
		C1TreeViewNodeCollection Nodes
		{
			get;
		}

		/// <summary>
		/// Owner of <see cref="C1TreeViewNodeCollection"/> collection.
		/// </summary>
		IC1TreeViewNodeCollectionOwner Owner
		{
			get;
		}
	}
}
