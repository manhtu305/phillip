/// <reference path="../../../../../Widgets/Wijmo/wijappview/jquery.wijmo.wijappview.ts"/>

module c1 {

	var $ = jQuery;

	export class c1appview extends wijmo.appview.wijappview {

		_getAllPages(html: JQuery, url: String) {
			var allPages = super._getAllPages(html, url),
				script;
			if (allPages.length && url !== this._documentUrl) {
				script = html.find("script");
				$(allPages[allPages.length - 1]).append(script);
			}
			return allPages;
		}

		_clickLinks(e) {
			var link = this._findClosestLink(e.target);
			if ($(link).hasClass("ui-state-disabled")) {
				return;
			}
			super._clickLinks(e);
		}
	}

	c1appview.prototype.widgetEventPrefix = "c1appview";

	c1appview.prototype.options = $.extend(true, {}, $.wijmo.wijappview.prototype.options, {
		postbackReference: null
	});

	$.wijmo.registerWidget("c1appview", $.wijmo.wijappview, c1appview.prototype);
}