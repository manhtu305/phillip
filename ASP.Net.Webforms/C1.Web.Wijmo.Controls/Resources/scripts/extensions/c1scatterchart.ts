﻿/// <reference path="../../../../../Widgets/Wijmo/wijscatterchart/jquery.wijmo.wijscatterchart.ts"/>

module C1 {
	class c1scatterchart extends wijmo.chart.wijscatterchart {
		hintContent: any;
		_create() {
			window["c1chart"].initChartExport();
			var self = this,
				o = self.options,
				content = o.hint.content,
				isContentFunc;
			self.hintContent = content;
			if (o.hint && o.hint.enable) {
				isContentFunc = $.isFunction(content);
				o.hint.content = function () {
					return window["c1chart"].handleHintContents(content, this, self, (data) => {
						var hintContents = data.hintContents,
							index = data.index;
						if (hintContents && hintContents.length &&
								index < hintContents.length) {
							return self._fotmatTooltip(hintContents[index]);
						}
						return null;
					});
				}
			}
			self.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		adjustOptions () {
			var self = this,
                o = self.options,
                axis = o.axis;
			o.hint.content = self.hintContent;
			if (axis && axis.x) {
			    if (axis.x.autoMin) {
			        axis.x.min = null;
			    }
			    if (axis.x.autoMax) {
			        axis.x.max = null;
			    }
			}
			if (axis && axis.y) {
			    if ($.isArray(axis.y)) {
			        $.each(axis.y, (i, yaxis) => {
			            if (yaxis.autoMin) {
			                yaxis.min = null;
			            }
			            if (yaxis.autoMax) {
			                yaxis.max = null;
			            }
			        });
			    } else {
			        if (axis.y.autoMin) {
			            axis.y.min = null;
			        }
			        if (axis.y.autoMax) {
			            axis.y.max = null;
			        }
			    }
			}
		}

		_isBarChart () {
			return false;
		}

		_hasAxes () {
			return true;
		}
	}

	$.extend(c1scatterchart.prototype.options, {
		innerStates: null
	});

	c1scatterchart.prototype.widgetEventPrefix = "c1scatterchart";
	$.wijmo.registerWidget("c1scatterchart", c1scatterchart.prototype);
}

