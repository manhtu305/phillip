﻿/// <reference path="c1dialog.ts" />
/// <reference path="../../../../../Widgets/Wijmo/Base/jquery.wijmo.widget.ts"/>

declare var c1common;
declare var __JSONC1;
declare var WebForm_DoCallback;

interface JQuery {
	c1spellchecker;
}

interface JQueryStatic {
	textSelectionHelper: any;
}

module c1 {


	var $ = jQuery;
	var _leftContainerCss = "wijspellchecker-leftcontainer",
		_rightContainerCss = "wijspellchecker-rightcontainer",
		_dialogCss = "wijspellchecker-dialog";

	export class c1spellchecker extends wijmo.JQueryUIWidget {
		_id: string;
		_errorsList: any;
		_currentBadWordIndex: number;
		_curBadWordIndex: number;
		_spellDialogInited: boolean;
		_notInDictField: JQuery;
		_changeToField: JQuery;
		_suggestionsCombo: JQuery;
		_ignoreBtn: JQuery;
		_changeBtn: JQuery;
		_ignoreAllBtn: JQuery;
		_changeAllBtn: JQuery;
		_add2DicBtn: JQuery;
		_cancelBtn: JQuery;
		spellDialog: JQuery;
		_checkWithDialogMode: boolean;
		_inspectedControlType: number;
		_inspectedControl: JQuery;
		_pEditorDocument: any;
		_ignoredIndexes: any;
		_correctionOffset: number;
		_badWordsCount: number;
		_curError: any;

		_create() {
			var self = this;

			self._id = self.element.attr("id");
			self._errorsList = [];
			self._currentBadWordIndex = -1;
			self._curBadWordIndex = -1;
			self._spellDialogInited = false;
			self._createDialog();
		}

		_createDialog() {
			var dialog = $("<div></div>").addClass(_dialogCss),
				self = this,
				leftContainer = $("<div></div>").addClass(_leftContainerCss),
				rightContainer = $("<div></div>").addClass(_rightContainerCss),
				notInDictLabel = $("<span>" + this.localizeString("notInDictionary", "Not in Dictionary:") + "</span>"),
				notInDictField = $("<input type='text' readonly='readonly'></input>"),
				changeToLabel = $("<span>" + this.localizeString("changeTo", "Change To:") + "</span>"),
				changeToField = $("<input type='text'></input>"),
				suggestionsLabel = $("<span>" + this.localizeString("suggestions", "Suggestions:") + "</span>"),
				suggestionsCombo = $("<select></select>"),
				ignoreBtn = $("<input type='button' title = 'Ignore' " +
					"value='" + this.localizeString("ignore", "Ignore") + "'></input>"),
				changeBtn = $("<input type='button' title = 'Change' " +
					"value='" + this.localizeString("change", "Change") + "'></input>"),
				ignoreAllBtn = $("<input type='button' title = 'Ignore All' " +
					"value='" + this.localizeString("ignoreAll", "Ignore All") + "'></input>"),
				changeAllBtn = $("<input type='button' title = 'Change All'" +
					" value='" + this.localizeString("changeAll", "Change All") + "'></input>"),
				add2DicBtn = $("<input type='button' title = 'Add to Dictionary'" +
					" value='" + this.localizeString("add2Dictionary", "Add to Dictionary") + "'></input>"),
				cancelBtn = $("<input type='button' title = 'Cancel'" +
					" value='" + this.localizeString("cancel", "Cancel") + "'></input>");

			leftContainer.append(notInDictLabel)
				.append(notInDictField)
				.append(changeToLabel)
				.append(changeToField)
				.append(suggestionsLabel)
				.append(suggestionsCombo);

			rightContainer.append(ignoreBtn)
				.append(changeBtn)
				.append(ignoreAllBtn)
				.append(changeAllBtn)
				.append(add2DicBtn)
				.append(cancelBtn);

			dialog.append(leftContainer)
				.append(rightContainer)
				.appendTo("body");

			self._notInDictField = notInDictField;
			self._changeToField = changeToField;
			self._suggestionsCombo = suggestionsCombo;
			self._ignoreBtn = ignoreBtn;
			self._changeBtn = changeBtn;
			self._ignoreAllBtn = ignoreAllBtn;
			self._changeAllBtn = changeAllBtn;
			self._add2DicBtn = add2DicBtn;
			self._cancelBtn = cancelBtn;
			self.spellDialog = dialog;
			self.spellDialog.c1dialog({
				close: function (e) {
					self._clearBadWordLinks();
					self._trigger("spellCheckerDialogClose", e);
				}, modal: true, title: this.localizeString("text", "Spell Checker"), autoOpen: false, width: "500px",
				height: "230px", resizable: false
			});
		}

		checkControl(textControl) {
			this._checkWithDialogMode = true;
			this._checkControl(textControl);
		}

		_checkControl(textControl) {
			var self = this, stext, frm;
			self._cancelSpellCheckingIfNeeded();
			if (typeof (textControl) === "string") {
				textControl = $("#" + textControl);
			}

			if (!textControl || textControl.length === 0) {
				return;
			}

			self._inspectedControl = textControl;

			if (textControl.is(":input") && textControl.val() !== "") {
				self._inspectedControlType = 0;
			}
			else if (textControl.is("iframe")) {
				self._inspectedControlType = 1;
				frm = textControl.get(0);
				if (frm.contentDocument) {
					self._pEditorDocument = frm.contentDocument;
				}
				else {
					self._pEditorDocument = frm.contentWindow.document;
				}
			}
			else {
				self._inspectedControlType = 2;
			}
			stext = self._getInspectedElementText();
			self._checkText(stext);
		}

		_checkText(text) {
			var sencText = $.wij.encodeString(text);
			this._doCallback("CheckText|" + sencText, "CheckText");
		}

		_addWord2Dictionary() {
			var word = this._notInDictField.val();
			this._doCallback("AddWord2Dic|" + word, "AddWord2Dic");
		}

		_cancelSpellCheckingIfNeeded() {
			var self = this,
				doc = $(document),
				widgetName = self.widgetName;

			doc.unbind("mouseup." + widgetName);
			doc.unbind("mousedown." + widgetName);
			self._ignoredIndexes = {};
			if (self.spellDialog) {
				self.spellDialog.c1dialog("close");
			}
			self._onSpellCheckCompleted();
		}

		_callbackComplate(returnValue, context) {
			var self = this, info, dataItem, i, cur,
				hasErrors = false;

			if (returnValue.indexOf("[AddWord]") === 0) {
				info = returnValue.substr(9, returnValue.length - 9);

				alert(info);
				if (self._errorsList.length > 1) {
					self._ignore();
				}
				else {
					if (self.spellDialog) {
						self.spellDialog.c1dialog("close");
					}
				}
				return;
			}

			if (returnValue === "") {
				return;
			}

			dataItem = $.parseJSON(returnValue);
			for (i = 0; i < dataItem.length; i++) {
				cur = dataItem[i];
				switch (cur.type_name) {
					case "error":
						alert("Request error:" + cur.message);
						break;
					case "no_spell_errors":
						hasErrors = false;
						self._showSpellDialog(hasErrors, dataItem);
						break;
					case "word_error":
						hasErrors = true;
						self._showSpellDialog(hasErrors, dataItem);
						break;
					default:
						break;
				}
				if (hasErrors) {
					break;
				}
			}
		}

		_showSpellDialog(hasErrors, arr) {
			var self = this,
				src, curOffs, start, end, sLeft, sRight, sCenterText, j,
				i = 0, sErrorText, sLeftAdd, sRightAdd,
				adjusted = false;
			if (!self._inspectedControl) {
				return;
			}

			if (hasErrors) {
				self._errorsList = arr;
				if (self._errorsList) {
					for (i = 0; i < self._errorsList.length; i++) {
						self._errorsList[i].text =
						$.wij.decodeString(self._errorsList[i].text);
					}
				}
				self._currentBadWordIndex = -1;
				self._correctionOffset = 0;
				self._badWordsCount = arr.length;

				if (self._checkWithDialogMode) {
					if (self.spellDialog) {
						self.spellDialog.c1dialog("open");

					}
				}

				if (self._inspectedControlType === 1 ||
					self._inspectedControlType === 2) {
					src = self._getInspectedElementText();
					curOffs = 0;
					for (i = 0; i < self._errorsList.length; i++) {
						start = self._errorsList[i].start + curOffs;
						end = self._errorsList[i].end + curOffs;
						sLeft = src.substring(0, start);
						sRight = src.substring(end, src.length);
						sCenterText = src.substring(start, end);
						if (sCenterText !== self._errorsList[i].text) {
							for (j = 0; j < 30; j++) {
								curOffs++;
								start++;
								end++;
								sLeft = src.substring(0, start);
								sRight = src.substring(end, src.length);
								sCenterText = src.substring(start, end);
								if (sCenterText === self._errorsList[i].text) {
									adjusted = true;
									break;
								}
							}
							if (!adjusted) {
								curOffs = curOffs - 29;
								start = start - 29;
								end = end - 29;
								for (j = 0; j < 5; j++) {
									curOffs--;
									start--;
									end--;
									sLeft = src.substring(0, start);
									sRight = src.substring(end, src.length);
									sCenterText = src.substring(start, end);
									if (sCenterText === self._errorsList[i].text) {
										adjusted = true;
										break;
									}
								}
								if (!adjusted) {
									sErrorText = "(e02123)WebSpellChecker error:" +
									sCenterText;
									sErrorText += " != " + self._errorsList[i].text;
									alert(sErrorText);
								}
							}
						}
						sLeftAdd = "<a bad_word_index=\"" + i + "\" ID=\"" +
						self._id +
						"_badword" + i +
						"a\" style=\"border-bottom:dotted red 1px;\">";
						sRightAdd = "</a>";
						src = sLeft + sLeftAdd + sCenterText + sRightAdd + sRight;
						curOffs += sLeftAdd.length + sRightAdd.length;
					}
					if (self._inspectedControlType === 1) {
						self._inspectedControl.get(0).contentWindow
						.document.body.innerHTML = src;
					}
					else {
						self._inspectedControl.html(src);
					}
				}

				if (self._checkWithDialogMode) {
					self._moveToNextError();
				}
				self.spellDialog.focus();
			}
			else {
				alert(this.localizeString("noErrors", "No errors"));
				self._onSpellCheckCompleted();
			}
		}

		_cancelSpellCheck() {
			this._cancelSpellCheckingIfNeeded();
		}

		_ignoreOrChangeBadWord(aBadWordIndex, newText) {
			var self = this,
				src = self._getInspectedElementText();
			src = self._ignoreOrChangeBadWord2(aBadWordIndex, newText, src);
			self._setInspectedElementText(src);
			self._checkIfSpellingComplete();
		}

		_ignoreOrChangeBadWord2(aBadWordIndex, newText, src) {
			var self = this,
				ch = ' ',
				aLinkId, k, leftCutPos, rightCutPos, sLeft, sRight, err;
			if (self._inspectedControlType === 1 || self._inspectedControlType === 2) {
				aLinkId = self._id + "_badword" + aBadWordIndex + "a";
				k = src.indexOf(aLinkId);
				if (k !== -1) {
					leftCutPos = k;
					while (ch !== '<' && leftCutPos >= 0) {
						leftCutPos--;
						ch = src.charAt(leftCutPos);
					}
					rightCutPos = src.indexOf("</A>", k);
					if (rightCutPos === -1) {
						rightCutPos = src.indexOf("</a>", k);
					}

					sLeft = src.substring(0, leftCutPos);
					sRight = src.substring(rightCutPos + 4, src.length);
					err = self._errorsList[aBadWordIndex];
					src = sLeft + newText + sRight;
				}
			}
			return src;
		}

		_clearBadWordLinks() {
			var self = this,
				src = self._getInspectedElementText(), i;
			for (i = 0; i < self._errorsList.length; i++) {
				src = self._ignoreOrChangeBadWord2(i, self._errorsList[i].text, src);
			}
			self._setInspectedElementText(src);
		}

		_ignore() {
			var self = this,
				sText = self._curError.text;

			self._ignoreOrChangeBadWord(self._currentBadWordIndex, sText);
			self._ignoredIndexes[self._curBadWordIndex.toString()] = true;
			if (self._checkWithDialogMode) {
				self._moveToNextError();
			}
		}

		_ignoreAll() {
			var self = this,
				src = self._getInspectedElementText(),
				sText = self._curError.text,
				i = 0;
			for (; i < self._errorsList.length; i++) {
				if (self._errorsList[i].text === sText) {
					src = self._ignoreOrChangeBadWord2(i, self._errorsList[i].text, src);
					self._ignoredIndexes[i.toString()] = true;
				}
			}
			self._setInspectedElementText(src);
			if (self._checkWithDialogMode) {
				self._moveToNextError();
			}
		}

		_moveToNextError() {
			var self = this,
				aSuggestions, i, oOption;

			self._initSpellDialog();
			self._currentBadWordIndex++;
			while (self._ignoredIndexes[self._currentBadWordIndex.toString()]) {
				self._currentBadWordIndex++;
			}
			if (self._currentBadWordIndex >= self._badWordsCount) {
				alert(this.localizeString("spellCheckDone", "Spell checking done."));
				self._cancelSpellCheckingIfNeeded();
				return;
			}
			self._curError = self._errorsList[self._currentBadWordIndex];
			if (!self._curError) {
				return;
			}
			self._notInDictField.val($.wij.decodeString(self._curError.text));
			aSuggestions = self._curError.suggestions;
			if (aSuggestions.length > 0) {
				$(".ChangeToField", self.element)
					.val($.wij.decodeString(aSuggestions[0]));
			}
			else {
				$(".ChangeToField", self.element).val("");
			}

			self._suggestionsCombo.children("option").remove();
			for (i = 0; i < aSuggestions.length; i++) {
				oOption = $("<option></option>");
				oOption.text($.wij.decodeString(aSuggestions[i]));
				oOption.attr("value", $.wij.decodeString(aSuggestions[i]));
				self._suggestionsCombo.append(oOption);
			}

			if (aSuggestions.length > 0) {
				self._changeToField.val($.wij.decodeString(aSuggestions[0]));
			}

			if (self._hightlightErrorTextInControl(self._curError.start,
				self._curError.end, self._curError.length, self._notInDictField.val())) {
				alert("Can't locate spelling text");
			}
		}


		_changeWord() {
			var self = this,
				prevText = self._notInDictField.val(),
				newText = self._changeToField.val(),
				aWordsDiff, s, left, right;
			if (self._inspectedControlType === 0) {
				aWordsDiff = newText.length - prevText.length;
				s = self._getInspectedElementText();
				left = s.substring(0, self._curError.start + self._correctionOffset);
				right = s.substring(self._curError.end +
					self._correctionOffset, s.length);
				s = left + newText + right;
				self._setInspectedElementText(s);
				self._correctionOffset = self._correctionOffset + aWordsDiff;
			}
			else {
				if (self._currentBadWordIndex > -1) {
					self._ignoreOrChangeBadWord(self._currentBadWordIndex, newText);
				}
			}
			if (self._checkWithDialogMode) {
				self._moveToNextError();
			}
		}

		_changeAllWords() {
			var self = this,
				newText = self._changeToField.val(),
				src = self._getInspectedElementText(),
				sText = self._curError.text,
				i = 0;
			for (; i < self._errorsList.length; i++) {
				if (self._errorsList[i].text === sText) {
					src = self._ignoreOrChangeBadWord2(i, newText, src);
					self._ignoredIndexes[i.toString()] = true;
				}
			}
			self._setInspectedElementText(src);
			if (self._checkWithDialogMode) {
				self._moveToNextError();
			}
		}

		_hightlightErrorTextInControl(start, end, length, text) {
			var self = this,
				nCount = 0,
				s, i, sTest, aLinkId, el;
			if (self._inspectedControlType === 0) {
				s = self._inspectedControl.val();
				if ($.browser.msie) {
					for (i = 0; i < self._curError.end + self._correctionOffset; i++) {
						if (s.charAt(i) === "\n") {
							nCount++;
						}
					}
				}
				sTest = s.substring(start + self._correctionOffset, end +
					self._correctionOffset);
				if (sTest) {
					return false;
				}

				$.textSelectionHelper.selectTextBounds(self._inspectedControl.get(0),
					start - nCount + self._correctionOffset, end -
					nCount + self._correctionOffset);
				return true;
			}
			else {
				aLinkId = self._id + "_badword" + self._currentBadWordIndex + "a";
				if (self._inspectedControlType === 1) {
					el = self._inspectedControl.get(0)
						.contentWindow.document.getElementById(aLinkId);
				}
				else {
					el = document.getElementById(aLinkId);
				}
				if (el) {
					if (self._inspectedControlType === 1) {
						$.textSelectionHelper
							.selectDOMElement(self._inspectedControl.get(0)
							.contentWindow, el);
					}
					else {
						$.textSelectionHelper.selectDOMElement(window, el);
					}
				}
			}
			return false;
		}

		_checkIfSpellingComplete() {
			var self = this, aLinkId, k,
				src = self._getInspectedElementText();
			if (self._inspectedControlType === 1 || self._inspectedControlType === 2) {
				aLinkId = self._id + "_badWord";
				k = src.indexOf(aLinkId);
				if (k === -1) {
					self._onSpellCheckCompleted();
				}
			}
		}

		_initSpellDialog() {
			var self = this;
			if (self._spellDialogInited) {
				return;
			}
			self._spellDialogInited = true;
			self._changeToField.bind("change", function (e) {
				self._changeToField_TextChanged(e);
			});
			self._ignoreBtn.bind("click", function (e) {
				self._ignoreBtn_Click(e);
			});
			self._changeBtn.bind("click", function (e) {
				self._changeBtn_Click(e);
			});
			self._cancelBtn.bind("click", function (e) {
				self._cancelBtn_Click(e);
			});
			self._ignoreAllBtn.bind("click", function (e) {
				self._ignoreAllBtn_Click(e);
			});
			self._changeAllBtn.bind("click", function (e) {
				self._changeAllBtn_Click(e);
			});
			self._add2DicBtn.bind("click", function (e) {
				self._add2DicBtn_Click(e);
			});
			self._suggestionsCombo.bind("change", function (e) {
				self._suggestionsCombo_changed(e);
			});
		}

		_suggestionsCombo_changed(e) {
			var self = this;
			self._suggestionsCombo.children("option").each(function () {
				if ($(this).attr("selected")) {
					self._changeToField.val($(this).text());
					return false;
				}
			});
		}

		_cancelBtn_Click(e) {
			this._cancelSpellCheck();
		}

		_changeToField_TextChanged(e) {
			var self = this;
			self._cancelBtn.attr("disabled", self._changeToField.val() === "");
		}

		_add2DicBtn_Click(e) {
			this._addWord2Dictionary();
		}

		_changeAllBtn_Click(e) {
			this._changeAllWords();
		}

		_changeBtn_Click(e) {
			this._changeWord();
		}

		_ignoreAllBtn_Click(e) {
			this._ignoreAll();
		}

		_ignoreBtn_Click(e) {
			this._ignore();
		}

		_C1WebDialog_Hidden(e) {
			this._cancelSpellCheck();
			this._trigger("spellCheckerDialogClose", e);
			this._clearChildElementValue();
		}

		_clearChildElementValue() {
			var self = this;
			self._changeToField.val("");
			self._notInDictField.val("");
			self._suggestionsCombo.html("");
		}

		_onSpellCheckCompleted(e?) {

			this._trigger("SpellCheckCompleted", e);
		}

		_getInspectedElementText() {
			var self = this;
			if (self._inspectedControlType === 0) {
				return self._inspectedControl.val();
			}
			else if (self._inspectedControlType === 1) {
				return self._inspectedControl.get(0)
					.contentWindow.document.body.innerHTML;
			}
			else {
				if (self._inspectedControl) {
					return self._inspectedControl.html();
				} else {
					return null;
				}
			}
		}

		_setInspectedElementText(src) {
			var self = this;
			if (self._inspectedControlType === 0) {
				self._inspectedControl.val(src);
			}
			else if (self._inspectedControlType === 1) {
				self._inspectedControl.get(0).contentWindow
				.document.body.innerHTML = src;
			}
			else {
				if (self._inspectedControl) {
					self._inspectedControl.html(src);
				}
			}
		}

		_callbackError(arg) {
			alert(arg);
		}

		_doCallback(argument, context) {
			var self = this,
				o = self.options,
				id = self._id;
			self.element["wijSaveState"](self);
			c1common.updateWebFormsInternals(id, c1common.JSON.stringify(o));
			WebForm_DoCallback(self.options.uniqueID,
				argument,
				function (returnValue, context) {
					self._callbackComplate(returnValue, context);
				},
				context,
				function (arg) {
					self._callbackError(arg);
				},
				true);
		}

		localizeString(key, defaultValue) {
			var o = this.options;
			if (o.localization && o.localization[key]) {
				return o.localization[key];
			}
			return defaultValue;
		}

	}

	c1spellchecker.prototype.widgetEventPrefix = "c1spellchecker";

	c1spellchecker.prototype.options = $.extend(true, {}, {
		//	textControlClientId: "",
		//	suppressBuiltinContextMenu: false
		///	<summary>
		/// Gets the unique, hierarchically qualified identifier
		/// for the server control.
		///	</summary>
		/// Default: "".
		/// Type: String.
		uniqueID: "",
		///	<summary>
		///	Use the localization option in order to localize
		///	text which not depends on culture.
		/// Default: {
		///	dialogCancel: "Cancle",
		///	dialogOK: "Ok"
		/// }
		/// Type: Object.
		/// Code example: $("#c1spellchecker").c1spellchecker(
		///					{ 
		///						localization: {
		///							dialogOK: "OK",
		///							dialogCancel: "Cancel"
		///						}
		///					});
		///	</summary>
		localization: null
	});
	$.wijmo.registerWidget("c1spellchecker", c1spellchecker.prototype);

	if (!$.textSelectionHelper) {
		$.extend({
			textSelectionHelper: {
				selectDOMElement: function (aWindow, elementToSelect) {
					if (!elementToSelect) {
						return false;
					}
					var t = aWindow.document, o0, ld9, selection;
					if (t.selection && window["opera"] === undefined) {
						switch (elementToSelect.tagName) {
							case "TABLE":
							case "IMG":
							case "HR":
							case "INPUT":
								o0 = t.body.createControlRange();
								$(o0).append(elementToSelect);
								break;
							case "UL":
							case "OL":
								o0 = t.body.createTextRange();
								o0.MoveToElementText(elementToSelect);
								ld9 = o0.parentElement();
								if (ld9.tagName !== "UL" || ld9.tagName !== "OL") {
									o0.moveEnd("character", -1);
								}
								break;
							default:
								o0 = t.body.createTextRange();
								o0.moveToElementText(elementToSelect);
								break;
						}
						if (o0) {
							o0.select();
							return true;
						}
					}
					else if (aWindow.getSelection) {
						o0 = t.createRange();
						o0.selectNode(elementToSelect);
						if ($.browser.opera) {
							o0.selectNodeContents(elementToSelect);
						}
						selection = aWindow.getSelection();
						if (!$.browser.safari) {
							selection.removeAllRanges();
							selection.addRange(o0);
						}
						return true;
					}
					return false;
				},

				selectTextBounds: function (element, start, end) {
					var rng, range;
					if (document.selection &&
						element.selectionStart === null && element.createTextRange) {
						range = element.createTextRange();
						range.collapse(true);
						range.moveStart("character", start);
						range.moveEnd("character", end - start);
						range.select();
					}
					else if (element.setSelectionRange) {
						element.setSelectionRange(start, end);
					}
					else {
						if ($.browser.msie) {
							element.focus();
							rng = document.selection.createRange();
							rng.moveToElementText(element);
							rng.collapse(true);
							rng.moveStart("character", start);
							rng.moveEnd("character", end - start);
							rng.select();
						}
					}
				},

				selectDocumentSelectionBounds: function (aWindow, start, end) {
					var newselbnds, oSel;
					if (end < start) {
						end = start;
					}
					if ($.browser.msie) {
						this._selectDocumentSelectionBounds2_IE(aWindow, start, end);
						newselbnds = this.getDocumentSelectionBounds(aWindow.document);
						if (newselbnds.start < start || newselbnds.end < end) {
							this._selectDocumentSelectionBounds2_IE(aWindow,
								start + 1, end + 1);
						}
						else {
							this._selectDocumentSelectionBounds2_IE(aWindow,
								start - 1, end - 1);
						}
					}
					else {
						oSel = aWindow.getSelection();
						oSel.removeAllRanges();
						oSel.collapse(aWindow.document.body, 0);
						oSel.selectionStart = start;
						oSel.selectionEnd = end;
					}
				},

				_selectDocumentSelectionBounds2_IE: function (aWindow, start, end) {
					var doc = aWindow.document, aTextRange, range;
					try {
						aTextRange = doc.selection.createRange();
						aTextRange.collapse(true);
						aTextRange.moveStart("textedit", -1);
						aTextRange.moveEnd("textedit", -1);
						aTextRange.select();
					}
					catch (ex) {
					}

					range = doc.selection.createRange();
					range.collapse(true);
					range.moveEnd("character", end);
					range.moveStart("character", start);
					range.select();
				},

				getTextSelectionBounds: function (element) {
					if (document.selection && element.selectionStart === null) {
						return this._ieGetSelection(element);
					}
					else {
						return this._mozillaGetSelection(element);
					}
				},

				getDocumentSelectionBounds: function (doc) {
					var r, start, end;
					try {
						if ($.browser.msie) {
							r = doc.selection.createRange();
							start = Math.abs(r.moveStart("character", -1000000));
							end = Math.abs(r.moveEnd("character", -1000000));
							return { start: start, end: end };
						}
						if (document.selection && doc.selectionStart === null) {
							return this._ieGetSelection(doc);
						}
						else {
							return this._mozillaGetSelection(doc);
						}
					}
					catch (ex) {
						return null;
					}
				},

				_ieGetSelection: function (element) {
					var aStart = 0,
						aEnd = 0;
					try {
						aStart = Math.abs(document.selection.createRange()
							.moveStart("character", -1000000));
						aEnd = aStart;
					}
					catch (ex) {
					}

					try {
						aEnd = aStart + document.selection.createRange().text.length;
					}
					catch (ex1) {
					}

					return { start: aStart, end: aEnd };
				},

				_mozillaGetSelection: function (element) {
					return { start: element.selectionStart, end: element.selectionEnd };
				}

			}
		});
	}
}
