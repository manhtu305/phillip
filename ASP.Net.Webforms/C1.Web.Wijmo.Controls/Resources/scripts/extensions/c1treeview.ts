/// <reference path="../../../../../Widgets/Wijmo/wijtree/jquery.wijmo.wijtree.ts"/>

declare var __doPostBack, c1common, WebForm_DoCallback;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1treeview extends wijmo.tree.wijtree {
		_isClick: any;
		widgetBaseClass: any;
		options: any;

		_setOption(key, value) {
			//this.element.wijSaveOptionsState(this);
			//todo: add to jsonstate
			$.wijmo.wijtree.prototype._setOption.apply(this, arguments);
		}

		_create() {
			$.wijmo.wijtree.prototype._create.apply(this, arguments);
			//this._doPostBack();
			this.element.removeClass("ui-helper-hidden-accessible");
			this._bindPostBackEvent();
		}

		_initState() {
			$.wijmo.wijtree.prototype._initState.apply(this, arguments);
			this.nodeWidgetName = "c1treeviewnode";
		}

		_createChildNodes() {
			var self = this, nodes = [],
				nodesOption = self.options.nodes;

			self.$nodes.children("li").each(function (idx) {
				var $li = $(this), options;
				options = nodesOption[idx];
				self._createNodeWidget($li, options);
				if (!$.browser.safari) {
					$li.children(".wijmo-wijtree-node").find(".wijmo-wijtree-inner a").addClass("wijmo-wijtree-link");
				}
				nodes.push(self._getNodeWidget($(this)));
			});
			self._hasChildren = nodes.length > 0;
			self._setField("nodes", nodes);
			self.nodes = nodes;
		}

		getChildNodes(node) {
			var o = node.options, self = this,
				searchIndexes = o.searchIndexes, arg,
				commandData = {
					SearchIndexes: undefined
				}, requestData = {
					CommandName: undefined,
					CommandData: undefined
				}, id = self.options.uniqueID;

			if (!self.options.loadOnDemand) {
				return;
			}

			commandData.SearchIndexes = searchIndexes;
			requestData.CommandName = 'GetNodes';
			requestData.CommandData = commandData;

			arg = c1common.JSON.stringify(requestData);

			WebForm_DoCallback(id,
				arg,
				function (returnValue, context) {//When success
					var value = returnValue, nodes;
					nodes = $.parseJSON(value);
					if (nodes.length) {
						$.each(nodes, function (i, n) {
							if ($.isPlainObject(n)) {
								self.add(n, node);
							}
							o.nodes.push();
						});
					}
					node._hasChildren = true;
					o.hasChildren = false;
					node.expand();
				},
				'GetNodes',
				function (arg) {//When error
					alert(arg);
				},
				true);
		}

		_nodeSelector() {
			return ":wijmo-c1treeviewnode";
		}

		add(item, content, index?) {
			var parent, self = this;
			if (typeof content === "string") {
				parent = $(content);
				self._parentAdd(item, parent, index);
			}
			else if (content.jquery) {
				if (index) {
					self._parentAdd(item, content, index);
				}
				else {
					$.wijmo.wijtree.prototype.add.apply(this, [content]);
				}
			}
			else if (content &&
				content.widgetName &&
				(content.widgetName === "c1treeviewnode" ||
				content.widgetName === "c1treeview")) {
				parent = content.element;
				self._parentAdd(item, parent, index);
			}
			else if (content === undefined || content === null) {
				$.wijmo.wijtree.prototype.add.apply(this, [item, index]);
			}
			else if (typeof content === "number") {
				$.wijmo.wijtree.prototype.add.apply(this, [item, content]);
			}
		}

		_parentAdd(item, parent, index) {
			var self = this;
			if (parent.is(self._nodeSelector())) {
				parent.c1treeviewnode("add", item, index);
			}
			else if (parent.is(":" + self.widgetBaseClass)) {
				$.wijmo.wijtree.prototype.add.apply(this, [item, index]);
			}
		}

		remove(content, index?) {
			var parent, self = this;
			if (typeof content === "string") {
				parent = $(content);
				self._parentRemove(parent, index);
			}
			else if (content.jquery) {
				if (index) {
					self._parentRemove(content, index);
				}
				else {
					$.wijmo.wijtree.prototype.remove.apply(this, [content]);
				}
			}
			else if (content &&
				content.widgetName &&
				(content.widgetName === "c1treeviewnode" ||
				content.widgetName === "c1treeview")) {
				content.remove(index);
			}
			else if (!content && typeof content !== "number") {
				$.wijmo.wijtree.prototype.remove.apply(this, [index]);
			}
			else if (typeof content === "number") {
				$.wijmo.wijtree.prototype.remove.apply(this, [content]);
			}
		}

		_parentRemove(parent, index) {
			var self = this;
			if (parent.is(self._nodeSelector())) {
				parent.c1treeviewnode("remove", index);
			}
			else if (parent.is(":" + self.widgetBaseClass)) {
				$.wijmo.wijtree.prototype.remove.apply(this, [index]);
			}
		}

		_changeCollection(idx, nodeWidget?) {
			var nodes = this._getField("nodes"), o = this.options;
			if (nodeWidget) {
				nodes.splice(idx, 0, nodeWidget);
				o.nodes.splice(idx, 0, nodeWidget.options);
			}
			else {
				nodes.splice(idx, 1);
				o.nodes.splice(idx, 1);
			}
		}

		_doPostBack(e, data) {
			var staticKey, args, eType, ref, d;
			if (e.data) {
				if (e.type === "c1treeviewnodedropped") {
					staticKey = data.widget.options["guid"];
				}
				else {
					staticKey = data.options["guid"];
				}
				d = e.data;
				ref = d.reference;
				eType = ref ? d.eType : "";

				if (!window.event) {
					// Workaround for an ASP.NET 4 issue: Sys$WebForms$PageRequestManager$_doPostBack function is trying to access arguments.callee if window.event is undefined.
					// Access to arguments.caller\ arguments.callee throw an exception when "strict mode" is used.
					window.event = <any>{
						target: null,
						srcElement: null
					};
				}

				if (e.type === "c1treeviewselectednodechanged" &&
					data._tree._isClick && d.click) {
					eType += "_NodeClicked_";
					ref = d.click;
				}

				if (eType && ref) {
					args = eType + staticKey;
					c1common.nonStrictEval(ref.replace("{0}", args));
				}
				else {
					__doPostBack('C1TreeView1', '');
				}
			}
		}

		_bindPostBackEvent() {
			var self = this, ele = self.element, o = self.options,
				prefix = self.widgetEventPrefix;
			if (o.autoPostBack) {
				if (o.nodeCollapsedPostBackReference) {
					ele.bind(prefix + "nodecollapsed",
						{
							eType: "_NodeCollapsed_",
							reference: o.nodeCollapsedPostBackReference
						},
						self._doPostBack);
				}
				if (o.nodeExpandedPostBackReference) {
					ele.bind(prefix + "nodeexpanded",
						{
							eType: "_NodeExpanded_",
							reference: o.nodeExpandedPostBackReference
						},
						self._doPostBack);
				}


				//ele.bind(prefix + "nodeclick",
				//{
				//	  eType: "_NodeClicked_",
				//	  reference: o.nodeClickedPostBackReference
				//},
				//self._doPostBack);

				// handle postback in selectednodechanged
				ele.bind(prefix + "nodeclick", function (e, d) {
					self._isClick = true;
				});

				ele.bind(prefix + "nodecheckchanged",
					{
						eType: "_NodeCheckChanged_",
						reference: o.nodeCheckChangedPostBackReference
					},
					self._doPostBack);

				ele.bind(prefix + "nodetextchanged",
					{
						eType: "_NodeTextChanged_",
						reference: o.nodeTextChangedPostBackReference
					},
					self._doPostBack);

				ele.bind(prefix + "nodedropped",
					{
						eType: "_NodeDropped_",
						reference: o.nodeDroppedPostBackReference
					},
					self._doPostBack);

				ele.bind(prefix + "selectednodechanged",
					{
						eType: "_SelectedNodesChanged_",
						reference: o.selectedNodesChangedPostBackReference,
						click: o.nodeClickedPostBackReference
					},
					self._doPostBack);
			}
		}

		saveTreeviewState() {
			var self = this, o = self.options, node, id, i;
			if (o.nodes && self.nodes) {
				for (i = 0; i < self.nodes.length; i++) {
					node = self.nodes[i];
					if (node) {
						node._loadTreeNodeState();
						//$.extend(true, o.nodes[i], node.options);
						o.nodes[i] = node.options;
					}
				}
			}

			o.selectedNodes =
			id = o.id || self.element.attr("id");
			c1common.updateJsonHiddenField(id, c1common.JSON.stringify(o));
		}
	}

	c1treeview.prototype.widgetEventPrefix = "c1treeview";

	c1treeview.prototype.options = $.extend(true, {}, $.wijmo.wijtree.prototype.options, {
		nodes: null,
		///	<summary>
		///	A value indicates whether or not Loads on demand is enabled.
		/// Default: false.
		/// Type: Boolean.
		///	</summary>
		loadOnDemand: false,
		///	<summary>
		/// Gets the unique, hierarchically qualified identifier
		/// for the server control.
		///	</summary>
		/// Default: "".
		/// Type: String.
		uniqueID: ""
	});

	$.wijmo.registerWidget("c1treeview", c1treeview.prototype);

	export class c1treeviewnode extends wijmo.tree.wijtreenode {
		options: any;

		_create() {
			var self = this, o = self.options;
			if (!o.guid) {
				o.guid = self._newGuid();
			}
			//o.nodes = [];
			$.wijmo.wijtreenode.prototype._create.apply(self, arguments);
			self.element.data("widgetName", "wijmoC1treeviewnode");
			if (o.selected) {
				self._tree._selectedNodes.push(self);
			}
		}

		_createChildNodes() {
			var self = this, o = self.options, nodes = [], $li = self.element;
			if (self._hasChildren || !!o.hasChildren) {
				$li.addClass("wijmo-wijtree-parent");
				self.$nodeBody
					.addClass("wijmo-wijtree-node wijmo-wijtree-header ui-state-default");
				self.$hitArea = $("<span>");
				self.$inner.prepend(self.$hitArea);
				if (self._hasChildren) {
					self.$nodes = $li.find("ul:eq(0)")
						.addClass("wijmo-wijtree-list ui-helper-reset wijmo-wijtree-child");
					nodes = self._createChildNode();
				}
			}
			else {
				$li.addClass("wijmo-wijtree-item");
				self.$nodeBody.addClass("wijmo-wijtree-node ui-state-default");
			}
			//fix #39816, ie10 renders bullets for ul even when list-style-type is set none.
			//it's known issue for ie10. Please refer to http://bugs.jqueryui.com/ticket/8844.
			$li.css("list-style-type", "none");
			self._setField("nodes", nodes);
		}

		_createChildNode() {
			var self = this, nodes = [],
				nodesOption = self.options.nodes;
			self.$nodes.children().filter("li").each(function (idx) {
				var el = this, $li = $(el), options, nodeWidget;
				options = nodesOption[idx];
				self._createNodeWidget($li, options);
				if (!$.browser.safari) {
					$li.children(".wijmo-wijtree-node").find(".wijmo-wijtree-inner a").addClass("wijmo-wijtree-link");
				}
				nodeWidget = self._getNodeWidget(el);
				nodeWidget._index = idx;
				nodes.push(nodeWidget);
			});
			return nodes;
		}

		_createMarkup(options) {
			var itemDom,
				icon = "", $node;
			if ($.browser.safari) {
				itemDom = "<li><a href='{0}'>{1}</a></li>";
			} else {
				itemDom = "<li><a href='{0}' class='wijmo-wijtree-link'>{1}</a></li>";
			}
			if (options.itemIconClass) {
				icon += " itemiconclass=" + options.itemIconClass;
			}
			if (options.expandIconClass) {
				icon += " expandiconclass=" + options.expandIconClass;
			}
			if (options.collapseIconClass) {
				icon += " collapseiconclass=" + options.collapseIconClass;
			}

			itemDom.replace(/\{2\}/, icon);
			$node = $(itemDom
				.replace(/\{0\}/, options.url)
				.replace(/\{1\}/, options.text));
			$node.c1treeviewnode(options);
		}

		_getChildren() {
			return (this.element.find(">ul:first>li").length > 0 &&
				!!this.element.children("ul:first"));
		}

		_setExpanded(value) {
			var self = this, o = self.options;
			if (self._expanded === value) {
				return;
			}
			if (self._hasChildren) {
				self._expandNode(value);
			}
			else if (o.hasChildren) {
				(<any>self._tree).getChildNodes(this);
			}
		}

		_createNodeWidget($li, options?) {
			if ($.fn.c1treeviewnode) {
				$li.data("owner", this);
				if (!!options && $.isPlainObject(options)) {
					$.extend(options, { treeClass: this.options.treeClass });
					$li.c1treeviewnode(options);
				}
				else {
					$li.c1treeviewnode({ treeClass: this.options.treeClass });
				}
			}
			return $li;
		}

		_changeCollection(idx, nodeWidget?) {
			var nodes = this._getField("nodes"), o = this.options;
			if (nodeWidget) {
				nodes.splice(idx, 0, nodeWidget);
				o.nodes.splice(idx, 0, nodeWidget.options);
			}
			else {
				nodes.splice(idx, 1);
				o.nodes.splice(idx, 1);
			}
		}

		_loadTreeNodeState() {
			var self = this, o = self.options, node, nodes, i;
			nodes = self._getField("nodes");
			if (o.nodes && nodes) {
				for (i = 0; i < nodes.length; i++) {
					node = nodes[i];
					if (node) {
						node._loadTreeNodeState();
						//$.extend(true, o.nodes[i], node.options);
						o.nodes[i] = node.options;
					}
				}
			}
		}

		_newGuid() {
			var S4 = function () {
				return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
			}
			return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
		}
	}

	c1treeviewnode.prototype.options = $.extend(true, {}, $.wijmo.wijtreenode.prototype.options, {
		searchIndexes: [],
		nodes: null,
		///	<summary>
		///	The value determines 
		/// whether this node has child nodes need be add after callback.
		/// Type:String.
		/// Default:"".
		/// Code example:$(".selector").wijtreenode("toolTip","Node 1 toolTip").
		///	</summary>
		hasChildren: false
	});

	$.wijmo.registerWidget("c1treeviewnode", c1treeviewnode.prototype);
}