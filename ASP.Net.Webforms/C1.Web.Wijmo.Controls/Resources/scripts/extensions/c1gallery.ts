/// <reference path="../../../../../Widgets/Wijmo/wijgallery/jquery.wijmo.wijgallery.ts"/>

declare var __doPostBack, WebForm_DoCallback, c1common;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1gallery extends wijmo.gallery.wijgallery {
		loadedCount: number;
		scrollToIdx: number;

		_create() {
			var self = this, o = self.options;
			self.scrollToIdx = -1;
			self.element.removeClass(hiddenCss);
			if (o.showPager) {
				o.pagingPosition = true;
				o.thumbnails = false;
			}
			$.wijmo.wijgallery.prototype._create.apply(self, arguments);
			if (self.thumbs) {
				self.thumbs[self.thumbWidgetName]("option", {
					loadOnDemand: o.loadOnDemand,
					items: o.items,
					uniqueID: o.uniqueID,
					itemCreated: function (event, data) {
						var item = { url: undefined, thumbUrl: undefined, caption: undefined },
							image = self.images[data.index];
						if (!image || (!!image && !image.url)) {
							item.url = data.item.linkUrl;
							item.thumbUrl = data.item.imageUrl;
							item.caption = data.item.caption;
							self.images[data.index] = item;
						}

						if (self.scrollToIdx == data.index) {
							self.show(data.index);
						}
					}
				});
			}
		}

		next() {
			var self = this, o = self.options, image;

			if (o.loadOnDemand && self.thumbs) {
				self.scrollToIdx = self.currentIdx + 1;
				image = self.images[self.scrollToIdx];
				if (self.scrollToIdx > (o.thumbsDisplay + self.currentThumbIdx - 1)
					&& (!image || (!!image && !image.url))) {
					self.thumbs[self.thumbWidgetName]("scrollTo",
						self.scrollToIdx - o.thumbsDisplay + 1);
				}
				else {
					$.wijmo.wijgallery.prototype.next.apply(self, arguments);
				}
			} else {
				$.wijmo.wijgallery.prototype.next.apply(self, arguments);
			}
		}

		previous() {
			var self = this, o = self.options, image;

			if (o.loadOnDemand && self.thumbs) {
				self.scrollToIdx = self.currentIdx - 1;
				image = self.images[self.scrollToIdx];
				if (self.scrollToIdx < self.currentThumbIdx
					&& (!image || (!!image && !image.url))) {
					self.thumbs[self.thumbWidgetName]("scrollTo", self.scrollToIdx);
				}
				else {
					$.wijmo.wijgallery.prototype.previous.apply(self, arguments);
				}
			} else {
				$.wijmo.wijgallery.prototype.previous.apply(self, arguments);
			}
		}
	}

	c1gallery.prototype.widgetEventPrefix = "c1gallery";
	c1gallery.prototype.thumbWidgetName = "c1carousel";

	c1gallery.prototype.options = $.extend(true, {}, $.wijmo.wijgallery.prototype.options, {
		items: [],
		autoExpand: false,
		loadOnDemand: false,
		uniqueID: ""
	});

	$.wijmo.registerWidget("c1gallery", c1gallery.prototype);
}