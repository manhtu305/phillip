/// <reference path="../../../../../Widgets/Wijmo/wijprogressbar/jquery.wijmo.wijprogressbar.ts"/>

declare var __doPostBack, theForm;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1progressbar extends wijmo.progressbar.wijprogressbar {
		loadedCount: number;
		scrollToIdx: number;


		_setOption(key, value) {
			if (key === "startTaskButton" || key === "stopTaskButton") {
				this._bindEvents2Buttons();
			}

			$.wijmo.wijprogressbar.prototype._setOption.apply(this, arguments);
		}

		_create() {
			var self = this;

			//$.wijmo.wijprogressbar.prototype._create.apply(self, arguments);
			super._create();
			self.element.removeClass("ui-helper-hidden-accessible");
			self._bindEvents2Buttons();
		}

		_bindEvents2Buttons() {
			var self = this,
				o = self.options,
				startButton, stopButton;

			if (o.startTaskButton !== "") {
				startButton = $("#" + o.startTaskButton);
				startButton.bind("click", function () {
					self.startTask();
					startButton.attr("disabled", "disabled");
					stopButton.removeAttr("disabled");
					return false;
				}).removeAttr("disabled");
			}

			if (o.stopTaskButton !== "") {
				stopButton = $("#" + o.stopTaskButton);
				stopButton.bind("click", function () {
					self.stopTask();
					startButton.removeAttr("disabled");
					stopButton.attr("disabled", "disabled");
					return false;
				}).attr("disabled", "disabled");
			}
		}

		_doPostBack(args) {
			var self = this,
				form = theForm,
				oldTarget = form.target,
				id = self.element.attr("id"),
				postBackEventReference = self.options.postBackEventReference;

			if (!form) {
				return;
			}

			form.target = "RunTaskTarget_" + id;
			//__doPostBack("C1ProgressBarRunTask_" + id, args);
			if (postBackEventReference) {
				c1common.nonStrictEval(postBackEventReference.replace("{0}", args));
			}
			form.target = oldTarget;
		}

		startTask() {
			/// <summary>Start the progress</summary>
			this._setOption("animationOptions", { disabled: true });
			this._setOption("value", 0);
			this._doPostBack("Start");
		}

		stopTask() {
			/// <summary>Stop the progress</summary>
			this._doPostBack("Stop|" + this.value());
		}
	}

	c1progressbar.prototype.widgetEventPrefix = "c1progressbar";

	c1progressbar.prototype.options = $.extend(true, {}, $.wijmo.wijprogressbar.prototype.options, {
		/// <summary>
		/// A value indicating the id of the button 
		/// which starts a server side task.
		/// Default: "".
		/// Type: String.
		/// </summary>
		startTaskButton: "",
		/// <summary>
		/// A value indicates the id of the button 
		/// which stops a server side task.
		/// Default: "".
		/// Type: String.
		/// </summary>
		stopTaskButton: ""
	});

	$.wijmo.registerWidget("c1progressbar", c1progressbar.prototype);
}