﻿/// <reference path="../../../../../Widgets/Wijmo/wijlinechart/jquery.wijmo.wijlinechart.ts"/>

module c1 {
	class c1linechart extends wijmo.chart.wijlinechart {
		hintContent: any;
		_create() {
			window["c1chart"].initChartExport();
			var self = this,
				o = self.options,
				content = o.hint.content,
				seriesList = o.seriesList,
				isContentFunc;
			self.hintContent = content;
			if (o.hint && o.hint.enable) {
				isContentFunc = $.isFunction(content);

				o.hint.content = function () {
					return window["c1chart"].handleHintContents(content, this, self, (data) => {
						var index = data.index,
							series = data.lineSeries,
							hintContents = series.hintContents;

						if (hintContents && hintContents.length &&
								index < hintContents.length) {
							return self._fotmatTooltip(hintContents[index]);
						}
						return null;
					});
				}
			}
			self.element.removeClass("ui-helper-hidden-accessible");
			$.wijmo.wijlinechart.prototype._create.apply(self, arguments);
		}

		adjustOptions() {
		    var o = this.options,
                 axis = o.axis;
			o.hint.content = this.hintContent;
			if (axis && axis.x) {
			    if (axis.x.autoMin) {
			        axis.x.min = null;
			    }
			    if (axis.x.autoMax) {
			        axis.x.max = null;
			    }
			}
			if (axis && axis.y) {
			    if ($.isArray(axis.y)) {
			        $.each(axis.y, (i, yaxis) => {
			            if (yaxis.autoMin) {
			                yaxis.min = null;
			            }
			            if (yaxis.autoMax) {
			                yaxis.max = null;
			            }
			        });
			    } else {
			        if (axis.y.autoMin) {
			            axis.y.min = null;
			        }
			        if (axis.y.autoMax) {
			            axis.y.max = null;
			        }
			    }
			}
		}

		_isBarChart () {
			return false;
		}

		_hasAxes () {
			return true;
		}
	}

	c1linechart.prototype.widgetEventPrefix = "c1linechart";
	$.extend(true, c1linechart.prototype.options, $.wijmo.wijlinechart.prototype.options, {
		innerStates: null
	});
	$.wijmo.registerWidget("c1linechart", $.wijmo.wijlinechart, c1linechart.prototype);
}