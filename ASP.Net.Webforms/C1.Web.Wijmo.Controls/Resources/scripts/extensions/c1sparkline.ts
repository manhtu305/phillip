﻿/// <reference path="../../../../../Widgets/Wijmo/wijsparkline/jquery.wijmo.wijsparkline.ts"/>

module c1 {

	var $ = jQuery;

	export class c1sparkline extends wijmo.sparkline.wijsparkline {
	}

	c1sparkline.prototype.widgetEventPrefix = "c1sparkline";

	c1sparkline.prototype.options = $.extend(true, {}, $.wijmo.wijsparkline.prototype.options);

	$.wijmo.registerWidget("c1sparkline", c1sparkline.prototype);
}