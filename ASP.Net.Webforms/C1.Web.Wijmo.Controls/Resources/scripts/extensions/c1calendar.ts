﻿/// <reference path="../../../../../Widgets/Wijmo/wijcalendar/jquery.wijmo.wijcalendar.ts"/>

declare var c1common;
declare var __JSONC1;
declare var WebForm_DoCallback;

interface JQuery {
	c1calendar;
}

module c1 {


	var $ = jQuery;

	export class c1calendar extends wijmo.calendar.wijcalendar {

		_create() {
			this.element.removeClass("ui-helper-hidden-accessible");
			//$.wijmo.wijcalendar.prototype._create.apply(this, arguments);
			super._create();
		}

		_init() {
			//$.wijmo.wijcalendar.prototype._init.apply(this, arguments);
			super._init();

			var o = this.options;
			if (o.autoPostBack) {
				this.element.bind("c1calendarselecteddateschanged", jQuery.proxy(this._onSelectDatesChanged, this));
			}
		}
		_onSelectDatesChanged(e, data) {
			var o = this.options;
			if (o.postBackEventReference) {
				c1common.nonStrictEval(o.postBackEventReference.replace("{0}", "selectedDatesChanged"));
			}
		}
	}


	c1calendar.prototype.widgetEventPrefix = "c1calendar";

	c1calendar.prototype.options = $.extend(true, {}, $.wijmo.wijcalendar.prototype.options, {
		postBackEventReference: null,
		autoPostBack: false
	});
	$.wijmo.registerWidget("c1calendar", $.wijmo.wijcalendar, c1calendar.prototype);
}