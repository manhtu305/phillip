/// <reference path="../../../../../Widgets/Wijmo/wijlightbox/jquery.wijmo.wijlightbox.ts"/>

declare var __doPostBack, c1common;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1lightbox extends wijmo.lightbox.wijlightbox {
		loadedCount: number;
		_create() {
			var self = this;
			self.element.removeClass(hiddenCss);
			self.loadedCount = self.options.display;
			$.wijmo.wijlightbox.prototype._create.apply(self, arguments);
		}
	}

	c1lightbox.prototype.widgetEventPrefix = "c1lightbox";

	c1lightbox.prototype.options = $.extend(true, {}, $.wijmo.wijlightbox.prototype.options, {});

	$.wijmo.registerWidget("c1lightbox", c1lightbox.prototype);
}