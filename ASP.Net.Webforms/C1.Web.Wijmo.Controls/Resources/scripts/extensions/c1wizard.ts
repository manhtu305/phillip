﻿/// <reference path="../../../../../Widgets/Wijmo/wijwizard/jquery.wijmo.wijwizard.ts"/>

module c1 {


	var $ = jQuery;

	export class c1wizard extends wijmo.wizard.wijwizard {
		_init() {
			super._init();
			this.element.bind("c1wizardactiveindexchanged", jQuery.proxy(this._onActiveIndexChanged, this));
		}

		_create() {
			this.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		private _onActiveIndexChanged(e, ui) {
			var o = this.options;
			if (o.postBackEventReference) {
				c1common.nonStrictEval(o.postBackEventReference.replace("{0}", ui.index));
			}
		}
	}

	c1wizard.prototype.widgetEventPrefix = "c1wizard";

	c1wizard.prototype.options = $.extend(true, {}, $.wijmo.wijwizard.prototype.options, {
		postbackReference: null
	});

	$.wijmo.registerWidget("c1wizard", $.wijmo.wijwizard, c1wizard.prototype);
}