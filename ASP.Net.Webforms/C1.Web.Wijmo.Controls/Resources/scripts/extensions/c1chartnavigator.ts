﻿/// <reference path="../../../../../Widgets/Wijmo/wijchartnavigator/jquery.wijmo.wijchartnavigator.ts"/> 

module c1 {
    class c1chartnavigator extends wijmo.chart.wijchartnavigator {
        _create() {
            var self = this,
                create = super._create;
            // Add a timeout to make sure that all the target charts have been created !
            setTimeout(function () {
                self.element.removeClass("ui-helper-hidden-accessible");
                create.call(self);
            }, 0);
        }

        adjustOptions() {
            var self = this;

            if (!self.targetChartInfos) {
                return;
            }
            $.each(self.targetChartInfos, function (idx, targetChartInfo) {
                var type = targetChartInfo.type,
                    chartObj = targetChartInfo.chartObj
                self._recoverTarget(targetChartInfo);

                chartObj.element["wijSaveState"](type, "adjustOptions");
            });
        }
    }

    c1chartnavigator.prototype.targetChartTypes = ["c1barchart", "c1candlestickchart", "c1linechart"];

    c1chartnavigator.prototype.widgetEventPrefix = "c1chartnavigator";

    $.wijmo.registerWidget("c1chartnavigator", $.wijmo.wijchartnavigator, c1chartnavigator.prototype);
}