﻿/// <reference path="../../../../../Widgets/Wijmo/wijbubblechart/jquery.wijmo.wijbubblechart.ts"/>

module C1 {
	class c1bubblechart extends wijmo.chart.wijbubblechart {
		hintContent: any;
		_create() {
			window["c1chart"].initChartExport();
			var self = this,
				o = self.options,
				content = o.hint.content,
			//seriesList = o.seriesList,
				isContentFunc;

			self.hintContent = content;
			if (o.hint && o.hint.enable) {
				isContentFunc = $.isFunction(content);
				o.hint.content = function () {
					return window["c1chart"].handleHintContents(content, this, self, (data) => {
						var hintContents = data.hintContents,
							index = data.index;
						if (hintContents && hintContents.length &&
								index < hintContents.length) {
							return self._fotmatTooltip(hintContents[index]);
						}
						return null;
					});
				};
			}
			self.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		_hasAxes() {
			if (this.widgetName === "c1piechart") {
				return false;
			}
			return true;
		}

		adjustOptions() {
		    var self = this,
                o = self.options,
                axis = o.axis;
			o.hint.content = self.hintContent;
			if (axis && axis.x) {
			    if (axis.x.autoMin) {
			        axis.x.min = null;
			    }
			    if (axis.x.autoMax) {
			        axis.x.max = null;
			    }
			}
			if (axis && axis.y) {
			    if ($.isArray(axis.y)) {
			        $.each(axis.y, (i, yaxis) => {
			            if (yaxis.autoMin) {
			                yaxis.min = null;
			            }
			            if (yaxis.autoMax) {
			                yaxis.max = null;
			            }
			        });
			    } else {
			        if (axis.y.autoMin) {
			            axis.y.min = null;
			        }
			        if (axis.y.autoMax) {
			            axis.y.max = null;
			        }
			    }
			}
		}
	}

	c1bubblechart.prototype.widgetEventPrefix = "c1bubblechart";
	$.wijmo.registerWidget("c1bubblechart", c1bubblechart.prototype);
}