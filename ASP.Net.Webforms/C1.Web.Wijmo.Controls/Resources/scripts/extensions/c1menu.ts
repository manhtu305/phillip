﻿/// <reference path="../../../../../Widgets/Wijmo/wijmenu/jquery.wijmo.wijmenu.ts"/>

module c1 {

	var $ = jQuery;

	export class c1menu extends wijmo.menu.wijmenu {
		_create() {
			if (this.options.displayVisible === false) {
				return;
			}

			//this.element.wijAssignOptionsFromJsonInput(this);
			this.element.removeClass("ui-helper-hidden-accessible");
			super._create();

			this._markPath(null);
			this._raiseClickEvent();
		}

		_init() {
			if (this.options.displayVisible === false) {
				return;
			}

			super._init();
		}

		destroy() {
			if (this.options.displayVisible === false) {
				return;
			}

			super.destroy();
		}

		_setOption(key, value) {
			if (this.options.displayVisible === false) {
				return;
			}

			if (key === "width" || key === "height") {
				this.refresh();
			}

			super._setOption(key, value);
		}

		_flyout() {
			var o = this.options,
				width = o.width,
				height = o.height, container;
			container = this.domObject.menucontainer;
			if (width !== "") {
				container.width(width);
			}
			if (height !== "") {
				container.height(height);
			}
			super._flyout();
		}

		_drilldow() {
			var o = this.options,
				width = o.width,
				height = o.height, container;
			container = this.domObject.menucontainer;
			if (width !== "") {
				container.width(width);
			}
			if (height !== "") {
				container.height(height);
			}
			super._drilldown();
		}

		private _markPath(owner) {
			var path = "";
			if (owner) {
				path = owner.parent("li").data("path");
			}
			else {
				owner = this.element;
			}
			if (path !== "") {
				path += "-";
			}
			$(">li", owner).each((i, n) => {
				$(n).data("path", path + i.toString());
				var childOwner = $(n).children("ul");
				if (childOwner.length > 0) {
					this._markPath(childOwner);
				}
			});
		}

		private _raiseClickEvent() {
			var _postBackEventReference = this.options.postBackEventReference;
			if (_postBackEventReference) {
				$(".wijmo-wijmenu-link", this.element).each(function () {
					var link = $(this), href, newRenference;
					if (link.is("a")) {
						href = link.attr("href");
						if (href === undefined || href === "") {
							return;
						}
						newRenference = _postBackEventReference
							.replace("_Args", "_ItemClick_" + link.parent("li").data("path"));
						href = href.replace(_postBackEventReference, newRenference);
						link.attr("href", href);
					}
				});
			}
		}
	}

	c1menu.prototype.widgetEventPrefix = "c1menu";

	c1menu.prototype.options = $.extend(true, {}, $.wijmo.wijmenu.prototype.options, {
		postbackReference: null,
		///	<summary>
		///	A value indicating the width of the wijsplitter.
		/// Default: 400.
		/// Type: Int.
		///	</summary>
		width: "",
		///	<summary>
		///	A value indicates the height of the wijsplitter.
		/// Default: 250.
		/// Type: Int.
		///	</summary>
		height: ""
	});

	$.wijmo.registerWidget("c1menu", $.wijmo.wijmenu, c1menu.prototype);
}

interface JQuery {
	c1menu: Function;
}