/// <reference path="../../../../../Widgets/Wijmo/wijcombobox/jquery.wijmo.wijcombobox.ts"/>

declare var c1common;
declare var __JSONC1;
declare var WebForm_DoCallback, __doPostBack;

interface JQuery {
	c1combobox;
}

module c1 {

	var cmd_se_ItemPopulate = "ScrollingToEnd-ItemPopulate",
		cmd_se_DataBind = "ScrollingToEnd-DataBind",
		dir_CommandName = "CommandName",
		dir_RequestItemCount = "RequestItemCount",
		dir_PageIndex = "PageIndex",
		dir_ItemCount = "ItemCount",
		dir_IsLastPage = "IsLastPage",
		dir_Items = "Items",
		dir_Text = "Text",
		dir_Value = "Value",
		dir_Selected = "Selected",
		dir_ComboboxText = "Text",
		dir_TemplateHtml = "TemplateHtml";

	var $ = jQuery;

	export class c1combobox extends wijmo.combobox.wijcombobox {
		isTriggerClick: boolean;
		_callEnabled: boolean;
		options: any;

		_setOption(key, value) {
			if (key === "width" || key === "height") {
				this._trimPx(key, value);
			}
			//this.element.wijSaveOptionsState();
			// $.wijmo.wijcombobox.prototype._setOption.apply(this, arguments);
			super._setOption(key, value);
		}

		_trimPx(key, value) {
			var idx;
			if (value) {
				idx = value.indexOf("px");
				if (idx !== -1) {
					this.options[key] = value.substring(0, idx);
				}
			}
		}

		_create() {
			var self = this, curIndex, o = self.options, inputEle,
				oldSelectedIndex, oldItem, inputText,
				multipleSelectionSeparator = o.multipleSelectionSeparator;

			self.listHasCreated = undefined;
			self._callEnabled = true;

			//update for fixing issue 20257 by wh at 2012/3/2
			if (o.selectionMode === 'multiple') {
				if (o.selectedIndices.length > 1 || o.selectedIndices[0] !== -1) {
					o.selectedIndex = o.selectedIndices;
				}
			} else {
				if (o.selectedIndices.length > 0) {
					o.selectedIndex = o.selectedIndices[0];
				} else {
					o.selectedIndex = -1;
				}

			}
			//end for 20257 issue


			//note:	 move the code upper because need to
			//write text into option so server can get the text value
			//update for adding text property of server by wuhao at 2011/8/10
			//self.element.bind("change.c1combobox", function (e) {
			//    inputText = self.element.val();
			//    if (o.text !== inputText) {
			//        self._trigger("textChanged", null, {
			//            oldText: o.text,
			//            newText: inputText
			//        });
			//        o.text = inputText;
			//    }
			//}).bind("c1comboboxselect", function (e, data) {
			//    if (o.selectionMode === "single") {
			//        self.options.text = data.label;
			//    } else {
			//        if (data && data.label !== undefined) {
			//            //update for fixing issue 20257 by wh at 2012/3/2
			//            if (self.options.text === "") {
			//                multipleSelectionSeparator = "";
			//            }
			//            //end for 20257 issue 
			//            //update for issue 20257 issue: text show twice on label
			//            //by wh at 2012/3/12
			//            /*
			//			self.options.text = self.element.val() +
			//			multipleSelectionSeparator +
			//			data.label;	*/
			//            self.options.text = self._getSelectionsText();
			//            //end for 20257 issue
			//        }
			//    }
			//});
			//end for add

			if (o.autoPostBack) {
				self.element.bind("c1comboboxselect", function (e, data) {
					var selectedIndex = [], selectedItems = [];
					//fixed bug 20256 ,ignore item select when click trigger button.
					if (self.isTriggerClick) {
						return;
					}

					//update for fixing bug 17528 by wh at 2011/10/9
					curIndex = data.element.index();
					//end for 17528
					if (o.selectedIndex !== curIndex) {
						//update for fixing issue 20257 by wh at 2012/3/12
						if (o.selectionMode === "single") {
							oldSelectedIndex = o.selectedIndex;
							oldItem = self.selectedItem;
							if (oldItem !== null) {
								oldItem.selected = false;
							}
							o.selectedIndex = curIndex;
							// fix the issue 42936, I am not sure why the postback bind in the combobox select event. 
							// in select event, the text and selectedIndex option is not set to the option. 
							// here just set selected item's label to the text option.
							o.text = self.items[curIndex].label;
						} else {
							if (o.selectedIndex && o.selectedIndex.length > 0) {
								oldSelectedIndex = o.selectedIndex[o.selectedIndex.length - 1];
							}
							// when the mode is multiple, the update the option's selectedIndex by the items.
							$.each(self.items, (i, item) => {
								if (item.selected) {
									selectedIndex.push(i);
									selectedItems.push(item);
								}
							});
							o.selectedIndex = selectedIndex;
							// fix the issue 42936, set the combobox text accordion the selected items.
							self._selectedItemsToInputVal(selectedItems);
						}

						if (!window.event) {
							// Workaround for an ASP.NET 4 issue: Sys$WebForms$PageRequestManager$_doPostBack function is trying to access arguments.callee if window.event is undefined.
							// Access to arguments.caller\ arguments.callee throw an exception when "strict mode" is used.
							try {
								window.event = <MSEventObj><any>{
									target: null,
									srcElement: null
								};
							} catch (er) {
							}
						}
						//__doPostBack(self.options.uniqueID, curIndex);
						if (oldSelectedIndex === undefined) {
							oldSelectedIndex = -1;
						}
						__doPostBack(self.options.uniqueID, oldSelectedIndex + "," + curIndex);
						//end 
					}
				});
			}

			self.element.removeClass("ui-helper-hidden-accessible");

			//if it is template, move the div style attribute to input
			if (self.element.is("div")) {
				inputEle = $(self.element.children()[0]);
				inputEle.attr("style", self.element.attr("style"));
				inputEle.attr("class", self.element.attr("class"));
				self.element.removeAttr("style");
				if (o.displayVisible === false) {
					self.element.css("display", "none");
				}
			}
			//this.element.wijAssignOptionsFromJsonInput(this);
			//$.wijmo.wijcombobox.prototype._create.apply(this, arguments);
			super._create();
		}

		//Add the method for 20357 2 issue: get text twice
		_getSelectionsText() {
			var s = "", self, sep, items;

			self = this;
			sep = self.options.multipleSelectionSeparator;
			items = self.selectedItems;

			$.each(items, function (index, item) {
				if (item && item.selected) {
					s += item.label + sep;
				}
			});
			if (s.length > 0) {
				s = s.substr(0, s.lastIndexOf(sep));
			}

			return s;
		}

		adjustOptions() {
			var o = this.options, i;
			this.listHasCreated = undefined;

			// when postback, the selectedIndex will used in server side, we should convert it to selectedIndices.


			if (o.selectionMode === 'multiple' && o.selectedIndex !== -1) {
				o.selectedIndices = o.selectedIndex;
			}
			else {
				o.selectedIndices = [o.selectedIndex];
			}

			//use the items to restore the data option.
			if (o.data && o.data.length > 0) {
				o.data = this.items;
			}

			// remove list widget object from the data option to avoid dead loop when string to JSON.
			if (o.data !== null) {
				for (i = 0; i < o.data.length; i++) {
					delete o.data[i].element;
					delete o.data[i].list;
					delete o.data[i].match;
					delete o.data[i].text;
				}
			}
			this.menu.active = null;
			return o;
		}

		_triggerClick() {
			var self = this;
			if (self.menu.element.is(":visible") === false) {
				if (self.options.data !== null) {
					if (self.options.data.length === 0) {
						self._scrollPage(self);
					}
				}
			}

			self.isTriggerClick = true;
			$.wijmo.wijcombobox.prototype._triggerClick.apply(this, arguments);
			self.isTriggerClick = false;
		}

		_openlist(items, data, searchTerm) {
			var self = data.self, v, opened = false;
			opened = $.wijmo.wijcombobox.prototype._openlist.apply(this, arguments);
			if (opened) {
				if ($.support.isTouchEnabled && $.support.isTouchEnabled()) {
					self.menu.element.wijsuperpanel({
						scroll: function (e, data) {
							var position = data.position, superpanelContainer,
								superpanel = self.menu.element;
							superpanelContainer = self.menu.element.find(".wijmo-wijsuperpanel-contentwrapper-touch");
							if (superpanel.height() + position >= superpanelContainer.height()) {
								self._scrollPage(self);
							}
						}
					});
				}
				else {
					self.menu.element.wijsuperpanel({
						scrolled: function (e, data) {
							v = self.menu.element.wijsuperpanel('option', 'vScroller');
							if (Math.round(v.scrollValue) +
								Math.round(v.scrollLargeChange) - 1 ===
								v.scrollMax) {
								self._scrollPage(self);
							}
						}
					});
				}
			}
			return opened;
		}

		// _callEnabled: true,
		_scrollPage(self) {
			if (self.options.enableCallBackMode) {
				if (self.options.endRequest === false && self._callEnabled) {
					self._callEnabled = false;
					if (self.options.callbackDataPopulate === "itemPopulate") {
						self._scrollingToEnd_ItemPopulate();
					}
					else {
						self._scrollingToEnd_DataBind();
					}
				}
			}
		}

		_scrollingToEnd_ItemPopulate() {
			var dic = {}, arg;
			dic[dir_CommandName] = cmd_se_ItemPopulate;
			dic[dir_RequestItemCount] = this.options.requestItemCount;
			//update for adding text property of server by wuhao at 2011/8/10
			dic[dir_ComboboxText] = this.options.text;
			//end for add
			arg = c1common.JSON.stringify(dic);
			this._doCallback(arg, cmd_se_ItemPopulate);
		}

		_scrollingToEnd_DataBind() {
			var dic = {}, arg;
			dic[dir_CommandName] = cmd_se_DataBind;
			dic[dir_PageIndex] = this.options.pageIndex;
			//update for adding text property of server by wuhao at 2011/8/10
			dic[dir_ComboboxText] = this.options.text;
			//end for add
			arg = c1common.JSON.stringify(dic);
			this._doCallback(arg, cmd_se_DataBind);
		}

		_callbackComplete(returnValue, context) {
			var dic, arr, itemCount, cur, item, i;
			if (returnValue === "") {
				this._callEnabled = true;
				this.options.endRequest = true;
				return;
			}
			//update for case 20689 at 2012/4/11
			this.listHasCreated = undefined;
			//end for case 20689 
			dic = __JSONC1.parse(returnValue);
			switch (context) {
				case cmd_se_ItemPopulate:
					itemCount = dic[dir_ItemCount];
					arr = dic[dir_Items];
					for (i = 0; i < arr.length; i++) {
						cur = arr[i];
						item = {
							label: cur[dir_Text],
							value: cur[dir_Value],
							templateHtml: cur[dir_TemplateHtml],
							selected: cur[dir_Selected]
						};
						this.add(item);
					}
					this.innerData = this.options.data;
					this.options.requestItemCount += itemCount;
					break;
				case cmd_se_DataBind:
					arr = dic[dir_Items];
					for (i = 0; i < arr.length; i++) {
						cur = arr[i];
						item = {
							label: cur[dir_Text],
							value: cur[dir_Value],
							templateHtml: cur[dir_TemplateHtml],
							selected: cur[dir_Selected]
						};
						this.add(item);
					}
					this.innerData = this.options.data;
					this.options.pageIndex = dic[dir_PageIndex] + 1;
					this.options.endRequest = dic[dir_IsLastPage];
					break;
			}
			this._callEnabled = true;
			if (!this.menu.element.is(":visible")) {
				this.search(this._input.val(), this);
			}
		}

		_callbackError(arg) {
			alert(arg);
		}

		add(item, index?) {
			var self = this;
			if (index === undefined) {
				if (self.menu.items !== self.options.data) {
					self.options.data.push(item);
				}
			}
			else {
				self.options.data.splice(index, 0, item);
			}
			self.menu.addItem(item, index);
		}

		remove(param) {
			var self = this, index = -1;
			if (param === undefined) {
				self.pop();
				self.menu.popItem();
			}
			else {
				if (!isNaN(param)) {
					index = param;
				}
				else {
					index = self.indexOf(param);
				}
				if (index >= 0) {
					self.removeAt(index);
					self.menu.removeItemAt(index);
				}
			}
		}

		removeAt(index) {
			var self = this;
			self.options.data.splice(index, 1);
		}

		pop() {
			var self = this;
			self.options.data.pop();
		}

		indexOf(item) {
			var self = this, index = -1, i = 0, oItem;
			for (i = 0; i < self.options.data.length; i++) {
				oItem = self.options.data[i];
				if (oItem.label === item.label &&
					oItem.value === item.value &&
					oItem.cells === item.cells) {
					index = i;
					break;
				}
			}
			return index;
		}

		_doCallback(argument, context) {
			var self = this;
			WebForm_DoCallback(self.options.uniqueID,
				argument,
				function (returnValue, context) {
					self._callbackComplete(returnValue, context);
				},
				context,
				function (arg) {
					self._callbackError(arg);
				},
				true);
		}

		removeAll() {
			var self = this;
			self.options.data = [];
			self._input.val("");
		}
	}

	c1combobox.prototype.widgetEventPrefix = "c1combobox";

	c1combobox.prototype.options = $.extend(true, {}, $.wijmo.wijcombobox.prototype.options, {
		///	<summary>
		///	A value indicating the width of the c1combobox.
		/// Default: 160.
		/// Type: Integer.
		///	</summary>
		width: 160,
		///	<summary>
		///	A value indicates the height of the c1combobox.
		/// Default: 22.
		/// Type: Integer.
		///	</summary>
		height: 22,
		///	<summary>
		/// A value that specifies the index of the item to select
		///	</summary>
		/// Default: -1.
		/// Type: Integer.
		selectedIndex: -1,
		///	<summary>
		/// Gets the unique, hierarchically qualified identifier
		/// for the server control.
		///	</summary>
		/// Default: "".
		/// Type: String.
		uniqueID: "",
		///	<summary>
		/// a value that indicates whether or not the control posts back to 
		/// the server each time a user interacts with the control.
		///	</summary>
		/// Default: false.
		/// Type: Boolean.
		autoPostBack: false,
		///	<summary>
		/// A value determines which means to populate data(without posting back).
		///	</summary>
		/// Default: "dataBind".
		/// Type: String.
		callbackDataPopulate: "dataBind",
		///	<summary>
		/// A value determines whether to enable callback to load
		///	</summary>
		/// Default: false.
		/// Type: Boolean.
		enableCallBackMode: false,
		///	<summary>
		/// The following options are only used for transmission 
		/// from client side to server side.
		///	</summary>
		requestItemCount: 0,
		pageIndex: 0,
		endRequest: false,
		text: ""
	});
	$.wijmo.registerWidget("c1combobox", $.wijmo.wijcombobox, c1combobox.prototype);

}
