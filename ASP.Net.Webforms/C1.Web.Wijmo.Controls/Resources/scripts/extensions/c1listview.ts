/// <reference path="../../../../../Widgets/Wijmo/wijlistview/jquery.wijmo.wijlistview.ts"/>

module c1 {

	var $ = jQuery;

	export class c1listview extends wijmo.listview.wijlistview {
		_init() {
			super._init();
		}

		_getListViewFromData() {
			return this.element.data("wijmo-c1listview");
		}
	}

	c1listview.prototype.widgetName = "c1listview";

	c1listview.prototype.widgetEventPrefix = "listview";

	c1listview.prototype.options = $.extend(true, {}, $.wijmo.wijlistview.prototype.options);

	$.wijmo.registerWidget("c1listview", $.wijmo.wijlistview, c1listview.prototype);
}