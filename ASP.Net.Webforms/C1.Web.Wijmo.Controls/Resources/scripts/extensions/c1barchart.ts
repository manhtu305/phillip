﻿/// <reference path="../../../../../Widgets/Wijmo/wijbarchart/jquery.wijmo.wijbarchart.ts"/>

module C1 {	
	class c1barchart extends wijmo.chart.wijbarchart {
	    hintContent: any;
	    storeAxisXMax: number;
		storeAxisXMin: number;

		_create() {
			window["c1chart"].initChartExport();
			var self = this,
				o = this.options,
				content = o.hint.content,
				isContentFunc;
			this.hintContent = content;
			if (o.hint && o.hint.enable) {
				isContentFunc = $.isFunction(content);
				o.hint.content = function() {
					return window["c1chart"].handleHintContents(content, this, self, (data) => {
						var hintContents = data.hintContents,
							index = data.index;
						if (hintContents && hintContents.length &&
								index < hintContents.length) {
							return self._fotmatTooltip(hintContents[index]);
						}
						return null;
					});
				}
			}
			if (o.axis && o.axis.x) {
			    self.storeAxisXMax = o.axis.x.max;
			    self.storeAxisXMin = o.axis.x.min;
			}
			this.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		adjustOptions() {
			var self = this,
                o = self.options, compass,
		        axis = o.axis;
			o.hint.content = self.hintContent;
			if (axis && axis.x) {
			    if (axis.x.autoMin) {
			        axis.x.min = null;
			    }
			    if (axis.x.autoMax) {
			        axis.x.max = null;
			    }
			}
			if (axis && axis.y) {
			    if ($.isArray(axis.y)) {
			        $.each(axis.y, (i, yaxis) => {
			            if (yaxis.autoMin) {
			                yaxis.min = null;
			            }
			            if (yaxis.autoMax) {
			                yaxis.max = null;
			            }
			        });
			    } else {
			        if (axis.y.autoMin) {
			            axis.y.min = null;
			        }
			        if (axis.y.autoMax) {
			            axis.y.max = null;
			        }
			    }
			}
			//when post back exchange the compass of the barchart.
			if (o.axis && o.axis.x && o.axis.y && o.horizontal) {
				compass = o.axis.x.compass;
				o.axis.x.compass = o.axis.y.compass;
				o.axis.y.compass = compass;
			}
		}

		_isBarChart () {
			return true;
		}

		_hasAxes () {
			return true;
		}
	}

	c1barchart.prototype.widgetEventPrefix = "c1barchart";
	$.extend(c1barchart.prototype.options, {
		innerStates: null
	});
	$.wijmo.registerWidget("c1barchart", c1barchart.prototype);
}