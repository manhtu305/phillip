/// <reference path="../../../../../Widgets/Wijmo/wijcarousel/jquery.wijmo.wijcarousel.ts"/>

declare var __doPostBack, WebForm_DoCallback, c1common;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1carousel extends wijmo.carousel.wijcarousel {
		loadedCount: number;

		_create() {
			var self = this;
			self.element.removeClass(hiddenCss);
			self.loadedCount = self.options.display;
			$.wijmo.wijcarousel.prototype._create.apply(self, arguments);
		}

		_getItems(idxs) {
			if (!idxs.length) {
				return;
			}
			var self = this, o = self.options, id = o.uniqueID,
				requestData = { CommandName: undefined, CommandData: undefined }, arg;

			requestData.CommandName = "GetItems";
			requestData.CommandData = idxs;

			arg = c1common.JSON.stringify(requestData);
			WebForm_DoCallback(id,
				arg,
				function (returnValue, context) {//When success
					var value = returnValue, items;
					items = $.parseJSON(value);
					if (items && items.length) {
						$.each(items, function (i, n) {
							var idx = n.index, li = self._getItemByIndex(idx), item;
							if ($.isPlainObject(n)) {
								item = o.items[idx];
								if (item) {
									$.extend(item, o.isTemplated ? n.item : n);
								}
								self._fillItem(li, n);
							}
							self._createItem(li, idx);
							self._trigger("itemCreated", null, { item: item, index: idx });
						});
					}
				},
				'GetItems',
				function (error) {//When error
					alert(error);
				},
				true);
		}

		_fillItem(li, item) {
			var self = this, o = self.options, a, img, caption, content;

			if (!o.isTemplated) {
				if (typeof item.linkUrl === "string" && item.linkUrl) {
					a = $("<a>").attr("src", item.linkUrl);
					a.appendTo(li);
				}
				if (typeof item.imageUrl === "string" && item.imageUrl) {
					img = $("<img>").attr("src", item.imageUrl);
					if (a) {
						img.appendTo(a);
					}
					else {
						img.appendTo(li);
					}
				}
				if (typeof item.caption === "string" && item.caption) {
					caption = $("<span>").html(item.caption);
					caption.appendTo(li);
				}
			}
			else {
				if (typeof item.html === "string" && item.html) {
					li.html($(item.html).html());
					//li.html(item.html);
				}
			}
		}

		_createItem(item, idx) {
			var self = this, o = self.options;

			if (idx < o.items.length && o.isTemplated &&
				!o.items[idx].isLoaded) {
				item.empty();
			}
			$.wijmo.wijcarousel.prototype._createItem.apply(self, arguments);
		}

		_setCurrentState() {
			var self = this, o = self.options, cIdx, lIdx, i, li, itemIdxs = [];
			$.wijmo.wijcarousel.prototype._setCurrentState.apply(self, arguments);

			if (o.loadOnDemand) {
				cIdx = self.currentIdx;
				lIdx = self.currentIdx + o.display - 1;
				for (i = cIdx; i <= lIdx; i++) {
					li = o.items[i];
					if (li && !li.isLoaded) {
						itemIdxs.push(i);
					}
				}
				self._getItems(itemIdxs);
			}
		}
	}

	c1carousel.prototype.widgetEventPrefix = "c1carousel";

	c1carousel.prototype.options = $.extend(true, {}, $.wijmo.wijcarousel.prototype.options, {
		items: [],
		loadOnDemand: false,
		isTemplated: false,
		///Trigger when item added.
		itemCreated: null
	});

	$.wijmo.registerWidget("c1carousel", c1carousel.prototype);
}