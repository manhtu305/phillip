﻿/// <reference path="../../../../../Widgets/Wijmo/wijpiechart/jquery.wijmo.wijpiechart.ts"/>

module c1 {
	class c1piechart extends wijmo.chart.wijpiechart {
		hintContent: any;
		_create() {
			window["c1chart"].initChartExport();
			var self = this,
				o = self.options,
				content = o.hint.content,
				isContentFunc;
			self.hintContent = content;
			if (o.hint && o.hint.enable) {
				isContentFunc = $.isFunction(content);
				o.hint.content = function () {
					return window["c1chart"].handleHintContents(content, this, self, (data) => {
						return data.hintContent;
					});
				}
			}
			self.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		adjustOptions() {
			var o = this.options;
			o.hint.content = this.hintContent;
		}

		_isBarChart() {
			return false;
		}

		_hasAxes() {
			return false;
		}
	}

	c1piechart.prototype.widgetEventPrefix = "c1piechart";

	$.extend(true, c1piechart.prototype.options, $.wijmo.wijpiechart.prototype.options, {
		innerStates: null
	});
	$.wijmo.registerWidget("c1piechart", $.wijmo.wijpiechart, c1piechart.prototype);
}