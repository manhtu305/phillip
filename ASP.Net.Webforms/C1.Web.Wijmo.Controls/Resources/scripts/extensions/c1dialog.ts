/// <reference path="../../../../../Widgets/Wijmo/wijdialog/jquery.wijmo.wijdialog.ts"/>

declare var __doPostBack, WebForm_DoCallback, c1common;

module c1 {


	var $ = jQuery, uiStateHover = "ui-state-hover", zonCSS = "wijmo-wijdialog-defaultdockingzone";

	export class c1dialog extends wijmo.dialog.wijdialog {
		originSize: any;
		originPos: any;
		originAppendTo: any;

		_setOption(key, value) {
			if (this.options.displayVisible === false) {
				return;
			}

			if (key === "width" || key === "height") {
				this._trimPx(key, value);
			}
			//this.element.wijSaveOptionsState(this);
			$.wijmo.wijdialog.prototype._setOption.apply(this, arguments);
		}

		_init() {
			var self = this, o = self.options;
			if (o.displayVisible === false) {
				return;
			}

			if (o.autoOpen || o.maintainStatesOnPostback) {
				if (o.maintainStatesOnPostback && o.dialogStates.isOpen) {
					self._openWithoutAnimated();
				}
				else if (o.autoOpen) {
					if (o.disabled) {
						self._openWithoutAnimated();
					} else {
						self.open();
					}
				}
			}
		}

		_trimPx(key, value) {
			var idx;
			if (value) {
				idx = value.indexOf("px");
				if (idx !== -1) {
					this.options[key] = value.substring(0, idx);
				}
			}
		}

		_create() {
			var self = this,
				o = self.options,
				regexp: RegExp;

			if (o.displayVisible === false) {
				return;
			}

			regexp = /^\[((['"][a-z]+['"])|(\d+)),((['"][a-z]+['"])|(\d+))\]$/;

			self.element.removeClass("ui-helper-hidden-accessible");
			self._trimPx("width", o.width);
			if (o.autoExpand) {
				o.height = "auto";
			}
			else {
				self._trimPx("height", o.height);
			}
			self.originSize = { w: o.width, h: o.height };

			if (o.position) {
				if (regexp.test(o.position)) {
					self.originPos = o.position;
					o.position = eval(o.position);
				}
			}

			if (this.options.updatePanelID !== "") {
				this.originAppendTo = this.options.appendTo;
				this.options.appendTo = "#" + this.options.updatePanelID;
			}

			//fixed bug #20015
			self.element.removeAttr("disabled");
			//			self.form = self.element.closest("form[id]");
			$.wijmo.wijdialog.prototype._create.apply(self, arguments);
			//			if (self.form.length) {
			//				self.uiDialog.appendTo(self.form);
			//			}
			self._restoreFromStates();
		}

		destroy() {
			if (this.options.displayVisible === false) {
				return;
			}

			super.destroy();
		}

		_openWithoutAnimated() {
			var self = this, o = self.options, animate;
			animate = o.show;
			o.show = undefined;
			self.open();
			o.show = animate;
		}

		_restoreFromStates() {
			var self = this, o = self.options, left, top;

			if (!o.maintainStatesOnPostback || $.isEmptyObject(o.dialogStates)) {
				return;
			}

			self.normalState = {
				height: o.dialogStates.normalHeight,
				width: o.dialogStates.normalWidth,
				top: o.dialogStates.normalTop,
				left: o.dialogStates.normalLeft,
				innerHeight: o.dialogStates.normalInnerHeight,
				innerWidth: o.dialogStates.normalInnerWidth,
				innerMinWidth: o.dialogStates.normalInnerMinWidth,
				innerMinHeight: o.dialogStates.normalInnerMinHeight
			};
			self.collapsed = o.dialogStates.collapseState;
			if (this.originAppendTo) {
				this.options.appendTo = this.originAppendTo;
				this.originAppendTo = null;
			}
			switch (o.dialogStates.windowState) {
				case "maximized":
					self.maximized = true;
					self.minimized = false;
					if (self.maximizeButton !== undefined) {
						self._doButtonAction(self.minimizeButton, "show");
						self._doButtonAction(self.maximizeButton, "hide");
						self._restoreButton(true, self.minimizeButton, "After");
					}
					break;
				case "minimized":
					self.maximized = false;
					self.minimized = true;
					self._minimizeWithoutAnimate();
					break;
				default:
					self.maximized = false;
					self.minimized = false;

					left = (self.normalState && self.normalState.left) ? parseInt(self.normalState.left.toString()) : 0;
					top = (self.normalState && self.normalState.top) ? parseInt(self.normalState.top.toString()) : 0;
					if (left && top) {
						self._setOption("position", [left, top]);
					}
					if (self.minimizeButton) {
						self._doButtonAction(self.minimizeButton, "show");
						self._doButtonAction(self.maximizeButton, "show");
					}
					break;
			}

			if (self.collapsed) {
				self._collapseDialogContent(false);
			}
		}

		//fixed bug 21430
		_appendToBody(dlg) {
			if (!this.innerFrame) {
				if (this.form.length) {
					dlg.appendTo(this.form);
				}
				else {
					dlg.appendTo(document.body);
				}
			}
			else {
				this.uiDialogTitlebar.prependTo(dlg);
				dlg.show();
			}
		}

		_minimizeWithoutAnimate() {
			var self = this, dlg = self.uiDialog, o = self.options, miniZone = null,
				defaultZone, scrollTop, top, position, size = {},
				form = self.element.closest("form[id]");;

			if (self.maximized) {
				self.maximized = false;
				self.restoreButton.remove();
			}
			else if (self.collapsed) {
				self._expandDialogContent(false);
			}
			self._enableDisableResizer(true);
			self._enableDisableDragger(true);
			if (self.collapsed) {
				self._collapseDialogContent(false);
			}

			self.contentWrapper.hide();
			if (self.uiDialogButtonPane.length) {
				self.uiDialogButtonPane.hide();
			}

			dlg.height("auto");
			dlg.width("auto");
			self._doButtonAction(self.minimizeButton, "hide");
			self._restoreButton(true, self.minimizeButton, "After");
			self._doButtonAction(self.pinButton, "hide");
			self._doButtonAction(self.refreshButton, "hide");
			self._doButtonAction(self.toggleButton, "hide");
			self._doButtonAction(self.maximizeButton, "show");

			if ($.browser.webkit) {
				$(".wijmo-wijdialog-captionbutton", self.uiDialog).css("float", "left");
			}

			if (o.minimizeZoneElementId.length > 0) {
				miniZone = $("#" + o.minimizeZoneElementId);
			}
			if (miniZone !== null && miniZone.size() > 0) {
				miniZone.append(self.uiDialog);
			}
			else {
				defaultZone = $("." + zonCSS);
				if (defaultZone.size() === 0) {
					defaultZone = $('<div class="' + zonCSS + '"></div>')
						.appendTo(form);
				}
				dlg.show().appendTo(defaultZone);
				defaultZone.css("z-index", dlg.css("z-index"));
			}
			dlg.css({ position: "static", float: "left" });

			if ($.browser.msie && $.browser.version === "6.0") {
				scrollTop = $(document).scrollTop();
				top = document.documentElement.clientHeight -
				defaultZone.height() + scrollTop;
				defaultZone.css({
					position: 'absolute',
					left: "0px",
					top: top
				});
			}
		}

		_bindFormOnSubimt() {
			var self = this;
			super._bindFormOnSubimt();
			if ($.browser.mozilla) {
				self.uiDialog.find("input[type='submit'], button[type='submit']").on('mousedown.wijdialog', function () {
					if (!self.minimized && !self._isInnerFrame()) {
						self.uiDialog.appendTo(self.form);
					}
				});
			}
		}

		prepareDataForSubmit() {
			var self = this, o = self.options, id;

			o.dialogStates.windowState = self.getState();
			o.dialogStates.isOpen = self._isOpen;
			o.dialogStates.collapseState = !!self.collapsed;

			if (self.normalState) {
				o.dialogStates.normalHeight = self.normalState.height;
				o.dialogStates.normalWidth = self.normalState.width;
				o.dialogStates.normalTop = self.normalState.top;
				o.dialogStates.normalLeft = self.normalState.left;
				o.dialogStates.normalInnerHeight = self.normalState.innerHeight;
				o.dialogStates.normalInnerWidth = self.normalState.innerWidth;
				o.dialogStates.normalInnerMinWidth = self.normalState.innerMinWidth;
				o.dialogStates.normalInnerMinHeight = self.normalState.innerMinHeight;
			}
			if (!o.maintainStatesOnPostback && self.originSize) {
				//				o.width = undefined;
				//				o.height = undefined;
				//fixed bug 20895
				o.width = self.originSize.w;
				o.height = self.originSize.h;
			}

			if (self.originPos) {
				o.position = self.originPos;
			}

			o.close = null;
			id = o.id || self.element.attr("id");
			c1common.updateJsonHiddenField(id, c1common.JSON.stringify(o));

			if (self.options.appendTo === "body" && self.uiDialog && self.uiDialog.closest(self.form).length === 0) {
				if (self.form.length) {
					self._appendToForm();
				}
			}
		}
	}

	c1dialog.prototype.widgetEventPrefix = "c1dialog";

	c1dialog.prototype.options = $.extend(true, {}, $.wijmo.wijdialog.prototype.options, {
		///	<summary>
		///	A value indicating the width of the wijdialog.
		/// Default: "300px".
		/// Type: string.
		///	</summary>
		width: "300px",
		///	<summary>
		///	A value indicates the height of the wijdialog.
		/// Default: "150px".
		/// Type: string.
		///	</summary>
		height: "150px",
		///	<summary>
		///	A value indicates whether to set the option height to auto.
		/// Default: 250.
		/// Type: Int.
		///	</summary>
		autoExpand: false,
		///	<summary>
		///	A value indicates whether maintain the open state when postback.
		/// Default: false.
		/// Type: boolean.
		///	</summary>
		maintainStatesOnPostback: false,
		///	<summary>
		///	A value indicates the dialog state of the dialog.
		/// Include open state, window state, normal size, expand state, etc.
		/// Default: {}.
		/// Type: Object.
		///	</summary>
		dialogStates: {},
		///	<summary>
		/// This option is used for get the updatepanel id, if the dialog is inside of an update panel, 
		/// when popup the dialog, append the dialog to update pabel element, not to default "body".
		/// Default: "".
		/// Type: String.
		///	</summary>
		updatePanelID: ""
	});

	$.wijmo.registerWidget("c1dialog", $.wijmo.wijdialog, c1dialog.prototype);
}

interface JQuery {
	c1dialog: Function;
}