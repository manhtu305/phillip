﻿/// <reference path="../../../../../Widgets/Wijmo/wijaccordion/jquery.wijmo.wijaccordion.ts"/>

declare var c1common;
module C1 {
	class c1Accordion extends wijmo.accordion.wijaccordion {
		_init() {
			super._init();
			this.element.bind("c1accordionselectedindexchanged", jQuery.proxy(this._onSelectedIndexChanged, this));
			this.element.removeClass("ui-helper-hidden-accessible");
		}

		_create() {
			super._create();
			var expandedIndices, i, o = this.options, headers;
			if (o.expandedIndices) {
				expandedIndices = o.expandedIndices;
				headers = this.element.children(".wijmo-wijaccordion-header");
				for (i = 0; i < expandedIndices.length; i += 1) {
					if (headers[expandedIndices[i]] && !$(headers[expandedIndices[i]]).hasClass("ui-state-active")) {
						if (expandedIndices[i] !== o.selectedIndex) {
							// fix for 25688
							this.activate(expandedIndices[i]);
						}
					}
				}
			}
		}

		// get accordion panes count
		count () {
			return this.element.find(".wijmo-wijaccordion-header").length;
		}
		// remove all accordion panes 
		clear () {
			this.element.html("");
		}

		// remove accordion pane by index 
		removeAt (index) {
			var header = $(this.element.find(".wijmo-wijaccordion-header")[index]),
				dir = this.options.expandDirection;
			if (dir == "left" || dir == "top") {
				header.prev(".wijmo-wijaccordion-content").remove();
			} else {
				header.next(".wijmo-wijaccordion-content").remove();
			}
			header.remove();
		}

		// add accordion pane to the end of the accordion 
		add(header, content) {
			this.insert(this.count(), header, content);
		}

		// insert accordion pane at the specified index 
		insert (index, header, content) {
			var dir = this.options.expandDirection,
				insertionPointElem, count = this.count(),
				newHeader = $("<div>" + header + "</div>"),
				newContent = $("<div>" + content + "</div>");
			if (index <= 0) {
				index = 0;
				newHeader.prependTo(this.element);
			}
			else if (index >= count) {
				index = count;
				newHeader.appendTo(this.element);
			} else {
				insertionPointElem = $(this.element.find(".wijmo-wijaccordion-header")[index]);
				newHeader.insertBefore(insertionPointElem);
			}
			if (dir == "left" || dir == "top") {
				newContent.insertBefore(newHeader);
			} else {
				newContent.insertAfter(newHeader);
			}
			this._initHeader(index, newHeader[0]);
			this._ensureOnlyOnePaneSelected(this.options.selectedIndex);
		}

		_ensureOnlyOnePaneSelected (index) {
			var o = this.options,
				headers = this.element.children(".wijmo-wijaccordion-header"),
				nextHeader = $(headers[index]),
				prevHeader = this.element.find(".wijmo-wijaccordion-header.ui-state-active"),
				rightToLeft = this.element.data("rightToLeft"), nextContent, prevContent;

			 nextContent = rightToLeft ?
							nextHeader.prev(".wijmo-wijaccordion-content") :
							nextHeader.next(".wijmo-wijaccordion-content");
			prevContent = rightToLeft ?
							prevHeader.prev(".wijmo-wijaccordion-content") :
							prevHeader.next(".wijmo-wijaccordion-content");
			if (prevHeader.length === 0 && nextHeader.length === 0) {
				return false;
			}

			prevHeader.removeClass("ui-state-active")
			.removeClass(this._headerCornerOpened)
			.addClass("ui-state-default ui-corner-all")
			.attribute({
				"aria-expanded": "false",
				tabIndex: -1
			})
			.find("> .ui-icon").removeClass(this._triangleIconOpened)
			.addClass(this._triangleIconClosed);
			nextHeader.removeClass("ui-state-default ui-corner-all")
			.addClass("ui-state-active")
			.addClass(this._headerCornerOpened)
			.attribute({
				"aria-expanded": "true",
				tabIndex: 0
			})
			.find("> .ui-icon").removeClass(this._triangleIconClosed)
			.addClass(this._triangleIconOpened);

			prevContent.removeClass("wijmo-wijaccordion-content-active");
			nextContent.addClass("wijmo-wijaccordion-content-active");
			prevContent.css('display', '');
			nextContent.css('display', '');
			/*if ($.fn.wijlinechart) {
			prevContent.find(".wijmo-wijlinechart").wijlinechart("redraw"); //?
			nextContent.find(".wijmo-wijlinechart").wijlinechart("redraw"); //?
			}*/
		}

		_onSelectedIndexChanged () {
			var o = this.options, headers, activeHeaders, expandedIndices,
				doPostback = true;
			if (!o.requireOpenedPane) {
				headers = this.element.find(".wijmo-wijaccordion-header");
				activeHeaders = $(jQuery.grep(headers,
							function (a) { return $(a).hasClass("ui-state-active"); }));
				expandedIndices = [];
				activeHeaders.each(function (indexInArray, valueOfElement) {
					expandedIndices[expandedIndices.length] = headers.index(valueOfElement);
				});

				if (o.expandedIndices) {
					// fix for 30434:
					doPostback = !($(o.expandedIndices).not(expandedIndices).length == 0
					&& $(expandedIndices).not(o.expandedIndices).length == 0);
				}
				o.expandedIndices = expandedIndices;
			} else {
				o.expandedIndices = [o.selectedIndex];
			}

			if (this.options.postBackEventReference && doPostback) {
				// fix for 29709:
				c1common.nonStrictEval(this.options.postBackEventReference.replace("{0}", o.selectedIndex));
			}
		}
	}

	c1Accordion.prototype.options = $.extend(true, {}, $.wijmo.wijaccordion.prototype.options, {
		postBackEventReference: null,
		expandedIndices: null
	});

	c1Accordion.prototype.widgetEventPrefix = "c1accordion";
	$.wijmo.registerWidget("c1accordion", c1Accordion.prototype);

}

