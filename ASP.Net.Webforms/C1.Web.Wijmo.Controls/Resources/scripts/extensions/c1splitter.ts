/// <reference path="../../../../../Widgets/Wijmo/wijsplitter/jquery.wijmo.wijsplitter.ts"/>

declare var __doPostBack, c1common;

module c1 {


	var $ = jQuery, c1splitterCss = "__c1splitter";

	export class c1splitter extends wijmo.splitter.wijsplitter {
		_setOption(key, value) {
			if (key === "width" || key === "height") {
				this.refresh(true, false);
			}

			$.wijmo.wijsplitter.prototype._setOption.apply(this, arguments);
		}

		_updateDisabledDiv(ele: JQuery, hidden?: boolean) {
			if (ele == null) {
				return;
			}
			if (hidden) {
				ele.hide();
			} else {
				ele.show();
			}
		}

		_createDisabledDiv(outerEle: JQuery) {
			var disabledDiv: JQuery = super._createDisabledDiv(outerEle);
			this._updateDisabledDiv(disabledDiv, this.options.displayVisible === false);
			return disabledDiv;
		}

		_create() {
			var self = this,
				element = self.element,
				parent = self.element.parents("." + c1splitterCss + ":first");

			//Remove the temp css label which is set at server side.
			element.removeClass(c1splitterCss);
			element.removeClass("ui-helper-hidden-accessible");

			if (self.options.fullSplit) {
				if (parent && parent.length > 0) {
					parent.one("c1splitterload", function (e, data) { //here we should use one other than bind to prevent propagation.
						if (data.element.attr("id") !== element.attr("id")) {
							$.wijmo.wijsplitter.prototype._create.apply(self, arguments);
						}
					});

					return;
				}
			}

			$.wijmo.wijsplitter.prototype._create.apply(self, arguments);
		}
	}

	c1splitter.prototype.widgetEventPrefix = "c1splitter";

	c1splitter.prototype.options = $.extend(true, {}, $.wijmo.wijsplitter.prototype.options, {
		/// <summary>
		/// A value indicating the width of the wijsplitter.
		/// Default: 400.
		/// Type: Int.
		/// </summary>
		width: 400,
		/// <summary>
		/// A value indicates the height of the wijsplitter.
		/// Default: 250.
		/// Type: Int.
		/// </summary>
		height: 250
	});

	$.wijmo.registerWidget("c1splitter", c1splitter.prototype);
}

interface JQuery {
	c1splitter: Function;
}