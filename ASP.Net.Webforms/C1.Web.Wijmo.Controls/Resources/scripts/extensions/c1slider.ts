/// <reference path="../../../../../Widgets/Wijmo/wijslider/jquery.wijmo.wijslider.ts"/>

declare var c1common;
declare var __JSONC1;
declare var WebForm_DoCallback;

interface JQuery {
	c1slider;
}

module c1 {


	var $ = jQuery;

	export class c1slider extends wijmo.slider.wijslider {

		_setOption(key, value) {
			if (key === "width" || key === "height") {
				this._trimPx(key, value);
			}
			//this.element.wijSaveOptionsState();
			//$.wijmo.wijslider.prototype._setOption.apply(this, arguments);
			super._setOption(key, value);
			return this;
		}

		_trimPx(key, value) {
			var idx;
			if (value) {
				idx = value.indexOf("px");
				if (idx !== -1) {
					this.options[key] = value.substring(0, idx);
				}
			}
		}
		_create() {
			//this.element.wijAssignOptionsFromJsonInput(this);
			this.element.removeClass("ui-helper-hidden-accessible");
			//$.wijmo.wijslider.prototype._create.apply(this, arguments);
			super._create();
		}

		_init() {
			super._init();
			if (this.options.displayVisible === false) {
				this._container.hide();
			} else {
				this._container.show();
			}
		}
	}

	c1slider.prototype.widgetEventPrefix = "c1slider";

	c1slider.prototype.options = $.extend(true, {}, $.wijmo.wijslider.prototype.options, {

	});
	$.wijmo.registerWidget("c1slider", $.wijmo.wijslider, c1slider.prototype);

}