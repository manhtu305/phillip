﻿/// <reference path="../../../../../Widgets/Wijmo/wijflipcard/jquery.wijmo.wijflipcard.ts"/>

declare var c1common;
module c1 {

	var $ = jQuery;

	export class c1flipcard extends wijmo.flipcard.wijflipcard {
		private _isSilentFlip: boolean;

		_create() {
			var self = this;
			super._create();
			//Default side is front. If currentSide is back, need to flip.
			if (self.options.currentSide === "back") {
				self._silentFlip();
			}
		}

		// flip without animation and events
		private _silentFlip() {
			var self = this, o = self.options, savedAnimation = $.extend({}, o.animation);

			// if use o.animation = {} to disable the flip effect, the flipcard will become invisible,
			// so need use set duration = 0.01 to disable the flip effect.
			if (o.animation.type === "flip") {
				//set animation's duration to 0.01 to disable the effect. Cannot set it to 0, 0 means default.
				o.animation.duration = 0.01;
			}
			else {
				o.animation = {};
			}

			self._isSilentFlip = true;
			self._flipPanels(null);

			o.animation = savedAnimation;
		}

		_onFlipping() {
			if (this._isSilentFlip) {
				return true;
			}
			return super._onFlipping();
		}

		_onFlipped() {
			var self = this;
			if (self._isSilentFlip) {
				// revertedSettings is used for flipping from back to front in IE 7/8/9
				var revertedSettings = self.element.data('flipRevertedSettings');
				if (revertedSettings) {
					var animation = self.options.animation;
					revertedSettings.speed = (animation && animation.duration) ? animation.duration : 500;
				}
				self._isSilentFlip = false;
				return;
			}

			super._onFlipped();
			self._changeSide();
		}

		private _changeSide() {
			var self = this, o = self.options;
			o.currentSide = o.currentSide === "front" ? "back" : "front";
			self._doPostBackEvent();
		}

		private _doPostBackEvent() {
			var o = this.options;
			if (o.autoPostBack && o.postBackEventReference) c1common.nonStrictEval(o.postBackEventReference);
		}
	}

	c1flipcard.prototype.widgetEventPrefix = "c1flipcard";

	c1flipcard.prototype.options = $.extend(true, {}, $.wijmo.wijflipcard.prototype.options, {
		currentSide: "front",
		autoPostBack: false,
		postBackEventReference: null
	});

	$.wijmo.registerWidget("c1flipcard", c1flipcard.prototype);
}