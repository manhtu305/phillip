﻿/// <reference path="../../../../../Widgets/Wijmo/wijpager/jquery.wijmo.wijpager.ts"/>

module c1 {

	var $ = jQuery;

	export class c1pager extends wijmo.pager.wijpager {
		_init() {
			super._init.apply(this, arguments);

			if (this.options.autoPostBack && this.options.postbackReference) {
				this.element
					.bind("c1pagerpageindexchanging", $.proxy(this._onPageIndexChanging, this))
					.bind("c1pagerpageindexchanged", $.proxy(this._onPageIndexChanged, this));
			}
		}

		private _onPageIndexChanging(e, args) {
			args.handled = true;
		}

		private _onPageIndexChanged(e, args) {
			var params = ["page",args.newPageIndex];
			this._doPostBack(params);
		}

		private _doPostBack(argumentsArray) {
			var params = this._stringFormat(this.options.postbackReference, argumentsArray.join(":"));

			if (!window.event) {
				// Workaround for an ASP.NET 4 issue: Sys$WebForms$PageRequestManager$_doPostBack function is trying to access arguments.callee if window.event is undefined.
				// Access to arguments.caller\ arguments.callee throw an exception when "strict mode" is used.
				window.event = <MSEventObj><any>{
					target: null,
					srcElement: null
				};
			}

			eval(params);
		}

		private _stringFormat(pattern: string, ...params: any[]): string {
			var i: number,
				len: number;

			if (!pattern) {
				return "";
			}

			for (i = 0, len = params.length; i < len; i++) {
				pattern = pattern.replace(new RegExp("\\{" + i + "\\}", "gm"), params[i]);
			}

			return pattern;
		}
	}

	c1pager.prototype.widgetEventPrefix = "c1pager";

	c1pager.prototype.options = $.extend(true, {}, $.wijmo.wijpager.prototype.options, {
		postbackReference: null,
		autoPostBack: false
	});

	$.wijmo.registerWidget("c1pager", $.wijmo.wijpager, c1pager.prototype);
}