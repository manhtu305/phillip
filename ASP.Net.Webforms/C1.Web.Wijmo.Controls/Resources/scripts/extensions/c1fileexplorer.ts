/// <reference path="../../../../../Widgets/Wijmo/wijfileexplorer/jquery.wijmo.wijfileexplorer.ts"/>

declare var c1common, WebForm_DoCallback;

module c1 {

	var $ = jQuery,
		defaultSearchPatterns: string[] = ["*.jpg", "*.jpeg", "*.gif", "*.png"];

	export class c1fileexplorer extends wijmo.fileexplorer.wijfileexplorer {

		private _treeInitData: wijmo.fileexplorer.IFileExplorerItem[];
		private _clientID: string;
		private _uniqueID: string;
		private _localization;

		_create() {
			var self = this;
			self._loadStates();
			super._create();
		}

		private _loadStates() {
			var self = this,
				states: InnerStates = self.options.innerStates;

			delete self.options.innerStates;
			self._treeInitData = states.treeInitData;
			self._clientID = states.clientID;
			self._uniqueID = states.uniqueID;

			self._data(states.currentFolderData);
			self._filterExpression(states.filterExpression);
			self._pageCount(states.pageCount);
			self._pageIndex(states.pageIndex);
			self._sortDirection(states.sortDirection);
			self._sortExpression(states.sortExpression);

			self._localization = self.options.localization;
			delete self.options.localization;
		}

		/** @ignore */
		_getTreeInitData(): wijmo.fileexplorer.IFileExplorerItem[] {
			return this._treeInitData;
		}

		/** @ignore */
		_enableAjax(): boolean {
			return true;
		}

		/** @ignore */
		adjustOptions() {
			var self = this;
			self.options.innerStates = <InnerStates>{
				filterExpression: self._filterExpression(),
				sortDirection: self._sortDirection(),
				sortExpression: self._sortExpression(),
				pageIndex: self._pageIndex()
			};
		}

		/** @ignore */
		_createAjaxAction(data: wijmo.fileexplorer.IFileExplorerRequest, context, success, error) {
			var self = this,
				processResult = result => $.parseJSON(result),
				onSuccess = result => {
					if (success) {
						success(processResult(result));
					}
				},
				onError = result => {
					if (error) {
						error(processResult(result));
					}
				};
			return () => {
				WebForm_DoCallback(self._uniqueID, c1common.JSON.stringify(data), onSuccess, context, onError, true);
			};
		}

		/** @ignore */
		_getDefaultSearchPattern(): string[] {
			return defaultSearchPatterns;
		}

		/** @ignore */
		_getLocalization() {
			return this._localization;
		}
	}

	interface InnerStates {
		clientID?: string;
		uniqueID?: string;
		currentFolderData?: wijmo.fileexplorer.IFileExplorerItem[];
		treeInitData?: wijmo.fileexplorer.IFileExplorerItem[];
		pageIndex: number;
		pageCount?: number;
		sortDirection: string;
		sortExpression: string;
		filterExpression: string;
		treeFolderClosedImage?: string;
	}

	c1fileexplorer.prototype.widgetEventPrefix = "c1fileexplorer";

	c1fileexplorer.prototype.options = $.extend(true, {}, $.wijmo.wijfileexplorer.prototype.options, {
		searchPatterns: defaultSearchPatterns.slice()
	});

	$.wijmo.registerWidget("c1fileexplorer", $.wijmo.wijfileexplorer, c1fileexplorer.prototype);
}