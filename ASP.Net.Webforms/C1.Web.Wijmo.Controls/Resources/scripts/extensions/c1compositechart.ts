﻿/// <reference path="../../../../../Widgets/Wijmo/wijcompositechart/jquery.wijmo.wijcompositechart.ts"/>
module C1 {
	class c1compositechart extends wijmo.chart.wijcompositechart {
	    hintContent: any;
	    storeAxisXMax: number;
		storeAxisXMin: number;
		_create() {
			window["c1chart"].initChartExport();
			var self = this,
				o = self.options,
				content = o.hint.content,
				seriesList = o.seriesList,
				isContentFunc;

			self.hintContent = content;
			if (o.hint && o.hint.enable) {
				isContentFunc = $.isFunction(content);
				o.hint.content = function () {
					return window["c1chart"].handleHintContents(content, this, self, (data) => {
						var index, hintContents, hintContent, seriesIndex, pointIndex, series;
						if (data.type === "pie") {
							return data.hintContent;
						} else {
							index = data.index;
							if (data.type === "line" || data.type === "spline" || data.type === "bezier" || data.type === "marker") {
							    series = data.lineSeries;

							    if (series) {
							        hintContents = series.hintContents;
							    } else {
							        hintContents = data.hintContents;
							    }
							}
							else {
								hintContents = data.hintContents;
							}
							if (hintContents && hintContents.length &&
							index < hintContents.length) {
								return hintContents[index];
							}
						}
						return null;
					});
				}
			}
			if (o.axis && o.axis.x) {
			    self.storeAxisXMax = o.axis.x.max;
			    self.storeAxisXMin = o.axis.x.min;
			}
			self.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		adjustOptions () {
			var self = this,
                o = self.options,
                axis = o.axis;
			o.hint.content = self.hintContent;
		    //when post back restore original value for Axis-x.Max and Min.
			if (axis && axis.x) {
			    if (axis.x.autoMin) {
			        axis.x.min = null;
			    }
			    if (axis.x.autoMax) {
			        axis.x.max = null;
			    }
			}
			if (axis && axis.y) {
			    if ($.isArray(axis.y)) {
			        $.each(axis.y, (i, yaxis) => {
			            if (yaxis.autoMin) {
			                yaxis.min = null;
			            }
			            if (yaxis.autoMax) {
			                yaxis.max = null;
			            }
			        });
			    } else {
			        if (axis.y.autoMin) {
			            axis.y.min = null;
			        }
			        if (axis.y.autoMax) {
			            axis.y.max = null;
			        }
			    }
			}
			if (this.isContainsCandlestick && this.timeUtil) {
				$.each(o.seriesList, (i, series) => {
					if (series.data && series.data.x) {
						$.each(series.data.x, (j, val) => {
							if (typeof (val) === "number") {
								series.data.x[j] = this.timeUtil.getTime(val);
							}
						});
					}
				});
			}
		}
	}

	c1compositechart.prototype.widgetEventPrefix = "c1compositechart";
	$.wijmo.registerWidget("c1compositechart", c1compositechart.prototype);
}