﻿/// <reference path="../../../../../Widgets/Wijmo/wijtreemap/jquery.wijmo.wijtreemap.ts"/>
module c1 {


	var $ = jQuery;
	export class c1treemap extends wijmo.treemap.wijtreemap {
		_create() {
			super._create();
		}
	}
	c1treemap.prototype.widgetEventPrefix = "c1treemap";
	$.extend(true, c1treemap.prototype.options, $.wijmo.wijtreemap.prototype.options);
	$.wijmo.registerWidget("c1treemap", $.wijmo.wijtreemap, c1treemap.prototype);
}