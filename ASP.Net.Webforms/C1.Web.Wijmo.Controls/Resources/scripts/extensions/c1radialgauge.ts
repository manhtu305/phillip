﻿/// <reference path="../../../../../Widgets/Wijmo/wijradialgauge/jquery.wijmo.wijradialgauge.ts"/>

module C1 {
	class c1radialgauge extends wijmo.gauge.wijradialgauge {
		valueHolder: number;

		_create() {
			var self = this,
				o = self.options,
				isOldValueToValue = ((typeof (o.oldValue) !== "undefined") && o.animation.enabled === true);

			o.width = self.element.width();
			o.height = self.element.height();
			if (isOldValueToValue) {
				self._zeroToOldValue();
			}

			super._create();

			if (isOldValueToValue) {
				self._oldValueToValue();
			}
		}

		_zeroToOldValue() {
			var self = this,
				o = self.options;
			self.valueHolder = o.value;
			o.value = o.oldValue;
			o.animation.enabled = false;
		}

		_oldValueToValue() {
			var self = this,
				o = self.options;
			o.value = self.valueHolder;
			o.animation.enabled = true;

			self._set_value(o.value, o.oldvalue);
		}
	}

	c1radialgauge.prototype.widgetEventPrefix = "c1radialgauge";
	$.wijmo.registerWidget("c1radialgauge", c1radialgauge.prototype);
}

