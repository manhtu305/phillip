﻿/// <reference path="../../../../../Widgets/Wijmo/wijtooltip/jquery.wijmo.wijtooltip.ts"/>

declare var __doPostBack, WebForm_DoCallback, c1common, escape;

module c1 {

	var $ = jQuery;

	export class c1tooltip extends wijmo.tooltip.wijtooltip {
		_create() {
			var self = this,
				o = self.options,
				element = self.element, id, label;

			if (o.displayVisible === false) {
				return;
			}

			if (o.enableCallBackMode && !o.useAjaxPostback && !o.ajaxCallback) {
				o.ajaxCallback = function () {
					var ele = this,
					paramters = {
						key: "AjaxCallback",
						target: ""
					},
					target = "";

					if (o.beforeAjaxCallback) {
						target = o.beforeAjaxCallback.call(ele);
					} else {
						target = ele.attr("id");
						if (target === "" || target === undefined) {
							$.each($(o.targetSelector).toArray(), (i, n) => {
								if (n === ele.get(0)) {
									target = i.toString();
									return false;
								}
							});
						}
					}
					paramters.target = target;
					if (!o.useAjaxPostback) {
						self._doCallback(c1common.JSON.stringify(paramters), "ajaxCallback");
					}
				};
			}
			super._create();
			self.element.removeClass("ui-helper-hidden-accessible");

			// add checkbox and radio's label's tooltip
			if (element.is(":checkbox, :radio")) {
				id = element.attr("id");
				if (id !== "") {
					label = $("[for='" + id + "']");
					label.c1tooltip(self.options);
				}
			}

			self._tooltipCache._$tooltip.appendTo($("form:first"));
		}

		_init() {
			if (this.options.displayVisible === false) {
				return;
			}

			super._init();
		}

		destroy() {
			if (this.options.displayVisible === false) {
				return;
			}

			super.destroy();
		}

		_setOption(key, value) {
			var o = this.options;

			if (o.displayVisible === false) {
				return;
			}

			super._setOption(key, value);

			if (key === "content" && $.isFunction(value)) {
				if (o.innerStates && o.innerStates[key]) {
					o.innerStates[key] = null;
				}
			}
		}

		_setText() {
			var o = this.options,
				container = $("#" + o.id),
				content = container.contents(),
				ele = this.element,
				tooltip = this._tooltipCache,
				hasTemplate = o.hasTemplate, title, remover,
				jqTitle = tooltip && tooltip._title;

			if (ele.is(":checkbox, :radio, label") &&
					ele.attr("title") === "" && ele.parent().is("span")) {
				this._content = ele.parent().attr("title");
			}

			if (hasTemplate) {

				if (content.length > 0) {
					tooltip._template = content;
				}

				if (tooltip._template) {
					remover = $("<div></div>")
							.appendTo("body")
							.append(tooltip._container.contents());
					tooltip._template.appendTo(tooltip._container);
					remover.remove();
				}

				title = this._getContent(o.title);
				if (title !== "") {
					jqTitle.html(title).show();
				} else {
					jqTitle.hide();
				}

			} else {
				super._setText();
			}
		}

		show() {
			var ele = this.element,
				o = this.options,
				paramters = {
					key: "AjaxCallback",
					target: ""
				},
				target = "";
			if (o.useAjaxPostback) {
				if (o.beforeAjaxCallback) {
					target = o.beforeAjaxCallback.call(ele);
				}
				else {
					target = ele.attr("id");
					if (target === "" || target === undefined) {
						$(o.targetSelector).each(function (i, n) {
							if (n === ele.get(0)) {
								target = i.toString();
							}
						});
					}
				}
				paramters.target = target;
				__doPostBack(o.id + "_UpdatePanel1", c1common.JSON.stringify(paramters));
			}
			super.show();
		}

		private encode() {
			var o = this.options;

			if (typeof (o.content) === "string") {
				o.content = escape(o.content);
			}
			if (typeof (o.title) === "string") {
				o.title = escape(o.title);
			}
		}

		private _callbackComplete(returnValue, context) {
			if (returnValue === "") {
				return;
			}
			var obj = $.parseJSON(returnValue),
			ele = this.element;
			
			if (obj) {
				if (obj.title && obj.title !== "") {
					this.options.title = obj.title;
				}
				if (obj.content && obj.content !== "") {
					this.options.content = obj.content;
				}
			}
			ele.c1tooltip("option", "content", this.options.content);
			//end
		}

		private _callbackError(arg) {
			alert(arg);
		}

		private _doCallback(argument, context) {
			var self = this, o = self.options;
			self.element["wijSaveState"](self);
			c1common.updateWebFormsInternals(o.id, c1common.JSON.stringify(o));
			WebForm_DoCallback(o.uniqueID,
			argument,
			function (returnValue, context) {
				self._callbackComplete(returnValue, context);
			},
			context,
			function (arg) {
				self._callbackError(arg);
			},
			true);
		}
	}

	c1tooltip.prototype.widgetEventPrefix = "c1tooltip";

	c1tooltip.prototype.options = $.extend(true, {}, $.wijmo.wijtooltip.prototype.options, {
		width: 200,
		height: 200,
		targetSelector: "",
		id: "",
		uniqueID: "",
		enableCallBackMode: false,
		beforeAjaxCallback: null,
		useAjaxPostback: false,
		innerStates: null,
		hasTemplate: false
	});

	$.wijmo.registerWidget("c1tooltip", $.wijmo.wijtooltip, c1tooltip.prototype);
}

interface JQuery {
	c1tooltip: JQueryWidgetFunction;
}