/// <reference path="../../../../../Widgets/Wijmo/wijupload/jquery.wijmo.wijupload.ts"/>

declare var __doPostBack, c1common, WebForm_DoCallback, escape;

module c1 {

	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible",
		uploadProgressClass = "wijmo-wijupload-progress",
		uploadFileRowClass = "wijmo-wijupload-fileRow",
		isUploadFileRow = "." + uploadFileRowClass,
		uploadCancelClass = "wijmo-wijupload-cancel",
		isUploadCancel = "." + uploadCancelClass,
		uploadedFilesListClass = "wijmo-wijupload-uploadedFilesList",
		uploadedFileClass = "wijmo-wijupload-uploadedFile",
		uiContentClass = "ui-widget-content",
		uiCornerClass = "ui-corner-all",
		uiHighlight = "ui-state-highlight";

	export class c1upload extends wijmo.upload.wijupload {
		uploadedFilesList: any;
		totalProgress: any;
		fileIds: any;
		c1fileCount: number;

		_create () {
			var o = this.options;
			if (!o.swfUploadOptions) {
				o.swfUploadOptions = {};
			}
			o.swfUploadOptions.swf = o.swfPath;
			if ((o.enableSWFUploadOnIE && $.browser.msie) || o.enableSWFUpload) {
				o.action = "C1UploadProgress.axd?isswfupload=1&filefolder=" + o.folder;
				//var action = $("form")[0].action,
				//act = action + (action.indexOf("?") > 0 ? "&" : "?") +
				//	"C1UploadId=1";
				//o.action = act;
			}
			this.element.removeClass("ui-helper-hidden-accessible");//Fixed calculate button size error bug.
			$.wijmo.wijupload.prototype._create.apply(this, arguments);            
		}

		_createContainers () {
			var self = this,
				o = self.options,
				uploadedFilesList = $("<ul>");
			$.wijmo.wijupload.prototype._createContainers.apply(self, arguments);
			if (o.showUploadedFiles) {
				uploadedFilesList.addClass(uploadedFilesListClass);
				self.filesList.after(uploadedFilesList);
				self.uploadedFilesList = uploadedFilesList;
			}
		}

		_createUploadButton () {
			var self = this,
				accessKey = self.options.accessKey;
			$.wijmo.wijupload.prototype._createUploadButton.apply(self, arguments);
			if (self.addBtn && accessKey && accessKey.length) {
				self.addBtn.attr("accessKey", accessKey);
			}
		}

		_wijFileRowRemoved (fileRow, fileInput, isComplete) {
			var self = this,
				o = self.options,
				fileName;

			$.wijmo.wijupload.prototype._wijFileRowRemoved.apply(self, arguments);
			if (isComplete && o.showUploadedFiles
                && fileInput && fileInput.length) {
                var files = fileInput[0].files;
                if (files) {
                    $.each(files, function (i, f) {
                        self._createUploadedFiles(f.name);
                    });
                }
			}
		}

		_createUploadedFiles (fileName?) {
			var self = this,
				o = self.options,
				fileRow = $("<li>"),
				file = $("<a>");
			if (self.uploadedFilesList) {
				fileRow.addClass(uploadFileRowClass)
					.addClass(uiContentClass)
					.addClass(uiCornerClass);
				file = $("<a>" + fileName + "</a>")
					.addClass(uploadedFileClass)
					.addClass(uiHighlight)
					.addClass(uiCornerClass);
				file.attr("target", "_blank");
				file.attr("href", (o.folder + "/" + fileName).replace("\\", "/"));
				fileRow.append(file);
				self.uploadedFilesList.append(fileRow);
			}
		}

		_upload (fileRow?, isSwfUpload?) {
			var self = this;

			if (!isSwfUpload) {
				var action = (<any>$("form")[0]).action,
				fileInput = $("input", fileRow[0]),
				uploader = this.uploaders[fileInput.attr("id")],
				id = uploader.fileid,
				progressSpan = $("." + uploadProgressClass, fileRow),
				act = action + (action.indexOf("?") > 0 ? "&" : "?") +
					"C1UploadId=" + id,

				context = { input: fileInput, ui: self, progress: progressSpan, type: "GetUploadID" };

				if (self.uploadAll) {
					act += "&uploadAll=true";
					self.options.action = act;
					if (uploader) {
						uploader.updateAction(act);
						uploader.upload();

						if (self.options.isSeparatorProgress) {
							self._getData(id, progressSpan, fileInput);
						}
					}
				}
				else {
					self._doCallback({ commandName: "GetUploadID" }, context);
				}
			}
			else {
				var fileId;
				if (fileRow === true) {//upload all
					fileId = undefined;

					if (!self.totalProgress) {
						self.totalProgress = $("<div></div>").appendTo(self.element);
						self.totalProgress.wijprogressbar();
					}
					self.totalProgress.show();
					self.fileIds = [];
				}
				else {
					fileId = fileRow;
				}
				self._swfUploadFile(fileId);
			}
		}

		_cleanSwfProgress () {
			var self = this;
			if (self.totalProgress) {
				self.totalProgress.fadeOut(1500, function () {
					self._updateTotalProgress(0);
				});
			}
		}

		_updateSwfProgress (loaded?, total?) {
			var percent = loaded * 100 / total, self = this;
			if (self.totalProgress && self.totalProgress.data("wijmo-wijprogressbar")) {
				self.totalProgress.wijprogressbar("option", "maxValue", 100);
				self.totalProgress.wijprogressbar("option", "value", percent);
			}
		}

		_swfUploadFile (fileId?) {
			var self = this, file = self.swfupload.getFile(fileId);
			if (file != null) {
				if (self.options.shouldValidateFiles) {
					self._validateFile($.extend({ commandName: "ValidateFile" }, file), "ValidateFile");
				}
				else {
					self.swfupload.startUpload(fileId);
				}
			}
		}

		_getFileInfo () {

		}

		_wijUpload () {
			return false;
		}

		_updateTotalProgress(val) {
			if (this.totalProgress && this.totalProgress.data("wijmo-wijprogressbar")) {
				this.totalProgress.wijprogressbar("option", "value", val);
			}
		}

		_wijuploadAll (uploadBtns) {
			var self = this;
			if (!self.totalProgress) {
				self.totalProgress = $("<div></div>");
			}
			self.fileIds = [];
			self.totalProgress.appendTo(self.element);
			self.totalProgress.wijprogressbar();
			self.c1fileCount = 0;

			uploadBtns.each(function (i, btn) {
				var uploadBtn = $(btn),
					fileRow = uploadBtn.parents(isUploadFileRow),
					fileInput = $("input", fileRow[0]),
					progressSpan = $("." + uploadProgressClass, fileRow),
					context = { input: fileInput, ui: self, progress: progressSpan, buttons: uploadBtns, type: "GetUploadID" };

				if (self._trigger("upload", null, fileInput) === false) {
					return false;
				}

				self._doCallback({ commandName: "GetUploadID" }, context);
			});
		}

		_wijcancel (fileInput?) {
			var self = this,
			context = { file: fileInput, type: "cancel" },
			uploader = self.uploaders[fileInput.attr("id")],
			id = uploader.fileid;

			if (id === undefined) {
				if (uploader) {
					uploader.cancel();
				}
				return;
			}

			if (self.uploadAll) {
				var index = -1;
				$.each(self.fileIds, function (i, n) {
					if (n === id) {
						index = i;
						return false;
					}
				});

				if (index != -1) {
					self.fileIds.splice(index, 1);
				}

				if (self.fileIds.length === 0) {
					self._updateTotalProgress(0)
					self.totalProgress.remove();
				}
			}
			self._doCallback({ commandName: "CancelUpload", fileID: id }, context);
			if (uploader) {
				uploader.cancel();
			}
		}

		_getData (id, progressSpan, fileInput) {
			var self = this;

			$.get("C1UploadProgress.axd?C1UploadId=" + id, null, function (data) {
				var obj = $.parseJSON($(data).text()),
					uploader,
					fileRow;
				try {
					obj = $.parseJSON($(data).text());
				}
				catch (e) {
					obj = null;
				}

				if (obj === null) {
					return;
				}

				if (obj.error) {
					fileRow = progressSpan.parents(isUploadFileRow);
					$(isUploadCancel, fileRow).click();
					//uploader = self.uploaders[fileInput.attr("id")];
					//uploader.cancel();
					//progressSpan.html("");
					//if (self.totalProgress) {
					//	self.totalProgress.wijprogressbar("option", "value", 0);
					//	self.totalProgress.remove();
					//}
					alert(obj.error);
					return;
				}

				if (obj.CurrentFile && obj.TotalBytes) {
					self._trigger("progress", null, {
						sender: obj.CurrentFile,
						loaded: obj.ReceivedBytes,
						total: obj.TotalBytes
					});
				}
				progressSpan.html(Math.round(obj.Progress * 100) + "%");
				if (obj.Progress < 1) {
					setTimeout(function () {
						self._getData(id, progressSpan, fileInput);
					}, 200);
				} else {
					self._triggerValidatedFile(obj);
					self._trigger("complete", null, {
						input: fileInput,
						message: obj.Message,
						isValid: obj.IsValid
					});
					self.totalUploadFiles--;
					uploader = self.uploaders[fileInput.attr("id")];
					fileRow = progressSpan.parents(isUploadFileRow);
					if (uploader && fileRow.length) {
						self._removeFileRow(fileRow, uploader, true);
					}
					if (self.totalUploadFiles === 0 && self.uploadAll) {
						self._trigger("totalComplete");
						setTimeout(function () {
							self._updateTotalProgress(0);
							self.totalProgress.remove();
							self.totalProgress = undefined;
						}, 300);
					}
				}
			});
		}

		_triggerValidatedFile(data) {
			var args = {
				file: data.CurrentFile,
				isValid: data.IsValid,
				message: data.Message
			};
			this._trigger("validatedFile", null, args);
		}

		_getTotalProgress () {
			var self = this;
			if (self.fileIds.length === 0) {
				return;
			}
			$.get("C1UploadProgress.axd?Total=1&ids=" + escape(self.fileIds.join(",")) +
				"&t" + Math.random().toString(),
				null, function (data) {
					var obj;
					try {
						obj = $.parseJSON(data);
					}
					catch (e) {
						obj = null;
					}

					if (obj === null) {
						return;
					}
					if (obj.ReceivedBytes && obj.TotalBytes) {
						self._trigger("totalProgress", null, {
							loaded: obj.ReceivedBytes,
							total: obj.TotalBytes
						});
					}

					var progressvalue = obj.Progress * 100;
					self._updateTotalProgress(obj.Progress * 100);
					//self.totalProgress.text(progressvalue);
					if (progressvalue < 100) {
						setTimeout(function () {
							self._getTotalProgress();
						}, 200);
					}
					else {

						if (!self.options.isSeparatorProgress && self.uploadAll) {
							//self._trigger("totalComplete");
							//console.log("totalComplete");
							setTimeout(function () {
								//console.log("removeAll");
								self._removeAllFileRows();
								self._updateTotalProgress(0);
								self.totalProgress.remove();
							}, 400); // must longer than 200 in self._getTotalProgress();
						}
					}
				});
		}

		_removeAllFileRows () {
			var self = this, isFadeout = false,
			rows = $("ul.wijmo-wijupload-filesList>li.wijmo-wijupload-fileRow", self.element);

			rows.fadeOut(1500, function () {
				//var files = [];
				if (isFadeout) { return; }
				isFadeout = true;
				rows.remove();

				rows.find("span.wijmo-wijupload-file")
				.each(function () {
					var text = $(this).text();
					if (text) {
						self._createUploadedFiles(text);
					}
				});

				if (self.uploaders) {
					self.uploaders = {};
				}

				self.commandRow.hide();
				self._resetProgressAll();
			});

		}

		supportXhr () {
			return false;
		}

		_callbackComplete (returnValue, context) {
			var self = this,
				type = context.type,
				action, ui, act, uploader;
			if (returnValue === "") {
				return;
			}

			if (type === "GetUploadID") {

				if (self.uploadAll) {
					uploader = self.uploaders[context.input.attr("id")];
					uploader.fileid = returnValue;
					self.fileIds.push(returnValue);
					self.c1fileCount++;
					if (context.buttons.length === self.c1fileCount) {
						$.each(context.buttons, function (idx, button) {
							$(button).click();
						});
						setTimeout(function () {
							self._getTotalProgress();
						}, 200);

					}
				}
				else {
					action = (<any>$("form")[0]).action;
					ui = context.ui;
					act = action + (action.indexOf("?") > 0 ? "&" : "?") +
					"C1UploadId=" + returnValue;
					uploader = this.uploaders[context.input.attr("id")];
					//context.input.data("fileid", returnValue);
					uploader.fileid = returnValue;
					ui.options.action = act;
					if (uploader) {
						uploader.updateAction(act);
						uploader.upload();
						setTimeout(function () {
							if (ui.uploadAll) {
								if (ui.options.isSeparatorProgress) {

									ui._getData(returnValue, context.progress, context.input);
								}
								ui._getTotalProgress();
							}
							else {
								ui._getData(returnValue, context.progress, context.input);
							}
						}, 100);
					}
				}
			}
		}
		_callbackError (arg) {
			alert(arg);
		}

		_validateFile (argument, context) {
			var self = this,
				o = self.options;

			WebForm_DoCallback(o.uploadId,
				c1common.JSON.stringify(argument),
				function (returnValue, context) {
					if (returnValue == "1") {
						self.swfupload.startUpload(argument.id);
					} else {
						self.swfupload.cancelUpload(argument.id);
						if (self.uploadAll) {
							self._swfUploadFile(undefined);
						}
					}
				},
				context,
				function (arg) {
					self._callbackError(arg);
				},
				true);
		}

		_doCallback (argument, context) {
			var self = this,
				o = self.options;
			self.element["wijSaveState"](self);
			c1common.updateWebFormsInternals(o.uploadId, c1common.JSON.stringify(o));
			//WebForm_DoCallback(o.uploadId.replace(/\_/g, "$"),
			WebForm_DoCallback(o.uploadId,
				c1common.JSON.stringify(argument),
				function (returnValue, context) {
					self._callbackComplete(returnValue, context);
				},
				context,
				function (arg) {
					self._callbackError(arg);
				},
				true);
		}
	}

	c1upload.prototype.widgetEventPrefix = "c1upload";

	c1upload.prototype.options = $.extend(true, {}, $.wijmo.wijupload.prototype.options, {
		showUploadedFiles: false,
		folder: "",
		uploadId: "",
		isSeparatorProgress: true,
		accessKey: "",
		maximumFiles: 0,
		maximumFileSize: 0,
		/**/
		validatedFile: null
	});

	$.wijmo.registerWidget("c1upload", c1upload.prototype);
}

