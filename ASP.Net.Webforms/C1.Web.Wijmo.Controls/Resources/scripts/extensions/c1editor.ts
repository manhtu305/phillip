﻿/// <reference path="c1spellchecker.ts" />
/// <reference path="../../../../../Widgets/Wijmo/wijeditor/jquery.wijmo.wijeditor.ts"/>

declare var c1common;
declare var __JSONC1;
declare var WebForm_DoCallback, theForm, __doPostBack;

interface JQuery {
	c1editor;
}

module c1 {


	var $ = jQuery;
	var cmd_tplList = "tplList",
		cmd_saveTpl = "saveTpl",
		cmd_deleteTpl = "delTpl",
		cmd_deleteImg = "delImg",
		cmd_imgList = "imgList",
		cmd_uploadImage = "uploadImage",
		cmd_uploadLink = "uploadLink",
		cmd_textSaved = "textSaved",
		const_fileuploadname = "c1fileupload",
		const_frmhelpername = "c1framehelper",
		c1Alert = alert,
		hasLoadCompleted = false,
		wijribbonDataRender = wijmo.ribbon.wijribbonDataRender,

		css_editor = "wijmo-wijeditor",
		css_imgdlg = css_editor + "-imagedlg",
		css_imgdlg_content = css_imgdlg + "-content",
		css_imgdlg_fields = css_imgdlg + "-fields",
		css_imgdlg_field = css_imgdlg + "-imagefield",
		css_imgdlg_list = css_imgdlg + "-imagelist",
		css_imgdlg_preview = css_imgdlg + "-preview",
		css_imgdlg_url = css_imgdlg + "-imagesrc",
		css_imgdlg_alt = css_imgdlg + "-imagealt",
		css_imgdlg_width = css_imgdlg + "-imagewidth",
		css_imgdlg_height = css_imgdlg + "-imageheight",
		css_imgdlg_css = css_imgdlg + "-css",


		css_linkdlg = css_editor + "-linkdlg",
		css_linkdlg_address = css_linkdlg + "-address",
		css_linkdlg_linktype = css_linkdlg + "-linktype",
		css_linkdlg_anchor = css_linkdlg + "-anchor",
		css_linkdlg_text = css_linkdlg + "-text",
		css_linkdlg_target = css_linkdlg + "-target",
		css_linkdlg_css = css_linkdlg + "-css",
		css_linkdlg_url = css_linkdlg + "-src",
		css_linkdlg_width = css_linkdlg + "-width",
		css_linkdlg_height = css_linkdlg + "-height",
		css_linkdlg_imagecontainer = css_linkdlg + "-imagecontainer",
		css_linkdlg_linkicontype = css_linkdlg + "-linkicontype",

		css_dlg = css_editor + "-dialog",
		css_dlg_button = css_dlg + "-button",
		css_dlg_text = css_dlg + "-text",
		css_dlg_upload = css_dlg + "-upload",
		css_dlg_fileupload = css_dlg + "-fileupload",
		css_dlg_uploadbutton = css_dlg_upload + "button",
		css_dlg_delbtn = css_dlg + "-delbutton",
		css_helper_hidden = "ui-helper-hidden-accessible",

		defaultFontSizes =
		{
			"VerySmall": {
				tip: "VerySmall",
				name: "xx-small",
				text: "VerySmall"
			},
			"Smaller": {
				tip: "Smaller",
				name: "x-small",
				text: "Smaller"
			},
			"Small": {
				tip: "Small",
				name: "small",
				text: "Small"
			},
			"Medium": {
				tip: "Medium",
				name: "medium",
				text: "Medium"
			},
			"Large": {
				tip: "Large",
				name: "large",
				text: "Large"
			},
			"Larger": {
				tip: "Larger",
				name: "x-large",
				text: "Larger"
			},
			"VeryLarge": {
				tip: "VeryLarge",
				name: "xx-large",
				text: "VeryLarge"
			}
		},

		defaultFontNames =
		{
			"Arial": {
				tip: "Arial",
				name: "Arial",
				text: "Arial"
			},
			"Courier New": {
				tip: "Courier New",
				name: "Courier New",
				text: "Courier New"
			},
			"Garamond": {
				tip: "Garamond",
				name: "Garamond",
				text: "Garamond"
			},
			"Tahoma": {
				tip: "Tahoma",
				name: "Tahoma",
				text: "Tahoma"
			},
			"Times New Roman": {
				tip: "Times New Roman",
				name: "Times New Roman",
				text: "Times New Roman"
			},
			"Verdana": {
				tip: "Verdana",
				name: "Verdana",
				text: "Verdana"
			},
			"Wingdings": {
				tip: "Wingdings",
				name: "Wingdings",
				text: "Wingdings"
			}
		};

	export class c1editor extends wijmo.editor.wijeditor {
		_form: any;
		_spellChecker: JQuery;
		_isTextEncoded: boolean;
		_create() {
			var self = this,
				o = self.options;

			self._form = theForm || document.getElementsByTagName("form")[0];
			self._spellChecker = $("#" + o.innerStates.spcid, self.element);
			self._spellChecker.appendTo("body");
			//add for 27652 issue
			self._spellChecker.bind("c1spellcheckerspellcheckerdialogclose", function () {
				self.focus();
			});

			o.mode = o.mode.toLowerCase();
			//$.wijmo.wijeditor.prototype._create.apply(self, arguments);
			super._create();
			//self.element.removeClass("ui-helper-hidden-accessible");
			self.element.removeClass(css_helper_hidden);
			// when create the editor, it added "ui-helper-hidden-accessible" CSS class, it can't focus.
			// in this case, call the focus method to focus the editor.
			if (!o.disabled && this.element.is(":visible")) {
				// fix the issue 43133, when the editor is place another container widget, such as accordion, and the container is invisible after 
				// the container widget init. if firstly create the editor widget, and then create the container widget,
				// the iframe will be focusd in ie9, and in the container widget, it looks like someting can edit.
				// fixed the issue 68672, this issue is also exist in IE10 and IE11.
				if ($.browser.msie && parseInt($.browser.version) >= 9) {
					setTimeout(function () {
						self.focus();
					}, 0);
				}
				else {
					self.focus();
				}
			}
		}

		_contextMenuWidgetName() {
			return "c1menu";
		}

		//callback
		_callbackComplete(response, context) {
			var self = this;

			if (context !== cmd_deleteImg && response === "") {
				return;
			}

			if (context === cmd_tplList) {
				self.updateTemplateList($.parseJSON(response), undefined);
			} else if (context === cmd_imgList) {
				self.updateImageList($.parseJSON(response), undefined);
			} else if (context === cmd_deleteImg) {
				if (response === "") {
					self.updateImageList({}, undefined);
				} else {
					self.updateImageList($.parseJSON(response), undefined);
				}
				$("input[type='text']", self.dialog).val("");
				$('img', self.dialog).attr("src", "");
			}
		}

		_callbackError(arg) {
			c1Alert(arg);
		}

		_doCallback(argument, context) {
			var self = this;

			WebForm_DoCallback(self.options.innerStates.uid,
				argument,
				function (response, context) {
					self._callbackComplete(response, context);
				},
				context,
				function (arg) {
					self._callbackError(arg);
				},
				true);
		}

		_uploadFile(frmName, command) {
			hasLoadCompleted = false;

			var self = this,
				form = self._form,
				oldTarget = form.target,
				oldEncoding = form.encoding,
				oldText = self.options.text;

			form.encoding = 'multipart/form-data';
			form.target = frmName;

			__doPostBack(command);
			form.target = oldTarget;
			form.encoding = oldEncoding;
			$("#" + const_fileuploadname).load(function () {
				self._isTextEncoded = false;
				self.options.text = oldText;
			});
		}

		_deleteIamge(arg) {
			var self = this,
				argument = cmd_deleteImg + arg;

			self._doCallback(argument, cmd_deleteImg);
		}

		uploadFileComplete(response) {
			c1Alert(response.msg);

			if (!response.isSuc) {
				//update for issue 20020 by wh at 2012/2/20
				//note: we control the response ourself, so must clear the
				//__EVENTTARGET value
				if ($("#__EVENTTARGET").val() === cmd_uploadLink ||
					$("#__EVENTTARGET").val() === cmd_uploadImage) {
					$("#__EVENTTARGET").val("");
				}
				//end for issue 20020
				return;
			}

			var self = this,
				name = response.name,
				url = response.url,
				list;

			if (response.cmd === cmd_uploadImage) {
				list = self._imgList;
				list[name] = url;
				self.updateImageList(list, undefined);
			} else if (response.cmd === cmd_uploadLink) {
				self._updateLinkDialog(response.url);
			}

			//update for issue 19401 by wh at 2012/2/1
			//note: we control the response ourself, so must clear the
			//__EVENTTARGET value
			if ($("#__EVENTTARGET").val() === cmd_uploadLink ||
				$("#__EVENTTARGET").val() === cmd_uploadImage) {
				$("#__EVENTTARGET").val("");
			}
			//end for issue 19401

		}

		//template dialog
		_onTemplateList() {
			var self = this;

			self.updateTemplateList({}, undefined);

			$.wijmo.wijeditor.prototype._onTemplateList.apply(self, arguments);

			self._doCallback(cmd_tplList, cmd_tplList);
		}

		//update for fixing issue 18777 by wuhao at 2011/12/15
		_saveText(arg?) {
			var self = this,
				argument = cmd_textSaved + self.options.text

			super._saveText(argument);
			// update for 44276: updatepanel collect the textarea'value(sourceview) of editor to window.__theFormPostData,
			// if callback invoke, the value that include "<" unencode tag will be postbacked to server,
			// it will cause exception.
			// the textarea's value is useless to editor, editor use hidden value to postback
			// so if the __theFormPostData include the textarea value, remove it.
			// Note: framework 4.5 is ok.
			if (self.sourceView.val() && self.sourceView.val() !== "") {
				c1common.removeFieldFromPostData(self.sourceView.val());
			}

			self._doCallback(argument, cmd_textSaved);
		}
		//end for fixing issue

		_onSaveTemplate(arg) {
			var self = this,
				argument = cmd_saveTpl + arg.name + '|' +
				arg.desc + '|' + arg.text;

			$.wijmo.wijeditor.prototype._onSaveTemplate.apply(self, arguments);

			self._doCallback(argument, cmd_saveTpl);
		}

		_onDeleteTemplate(arg) {
			var self = this,
				argument = cmd_deleteTpl + arg;

			$.wijmo.wijeditor.prototype._onDeleteTemplate.apply(self, arguments);

			self._doCallback(argument, cmd_deleteTpl);
		}

		_createUpload(hasDelBtn?) {
			var self = this,
				upload = self._createDiv(css_dlg_upload);

			upload.add(self._createElement("input", {
				type: "file",
				name: const_fileuploadname,
				"class": css_dlg_fileupload,
				"aria-label": "c1fileupload"
			}));

			upload.add(self._createElement("input", {
				type: "button",
				value: this.localizeString("hyperLinkDialogUpload", "Upload"),
				"class": css_dlg_button + " " + css_dlg_uploadbutton
			}));

			if (hasDelBtn) {
				upload.add(self._createElement("input", {
					type: "button",
					value: this.localizeString("imageDialogDelImg", "Delete Selected"),
					"class": css_dlg_button + " " + css_dlg_delbtn
				}));
			}
			return upload;
		}

		private _createDiv(className, title?) {
			return wijribbonDataRender.createDiv(className, title);
		}

		private _createElement(tagName, innerText?, attribs?) {
			return wijribbonDataRender.createElement(tagName, innerText, attribs);
		}

		_createIframe() {
			return this._createElement("iframe", {
				name: const_frmhelpername,
				id: const_fileuploadname,
				style: "display:none"
			});
		}

		//image dialog
		_getDialogRes_ImageBrowser() {
			var self = this,
				dialog = self._createDiv(css_imgdlg),
				content = self._createDiv(css_imgdlg_content),
				fields = self._createDiv(css_imgdlg_fields),
				imgField = self._createDiv(css_imgdlg_field),
				imglist = self._createDiv(css_imgdlg_list),
				imgpreview = self._createDiv(css_imgdlg_preview);

			fields.add(self._createTextField(
				this.localizeString("imageEditorDialogImageSrc", "Image Src:"),
				css_imgdlg_url));
			fields.add(self._createTextField(
				this.localizeString("imageEditorDialogImageAltText", "Image alt text:"),
				css_imgdlg_alt));
			fields.add(self._createTextField(
				this.localizeString("imageEditorDialogImageWidth", "Image width:"),
				css_imgdlg_width, this.localizeString("dialogPixel", "px")));
			fields.add(self._createTextField(
				this.localizeString("imageEditorDialogImageHeight", "Image height:"),
				css_imgdlg_height, this.localizeString("dialogPixel", "px")));
			fields.add(self._createTextField(
				this.localizeString("imageEditorDialogCssText", "Css text:"),
				css_imgdlg_css));
			content.add(fields);

			imgField.add(imglist);
			imglist.add(self._createElement("select", { size: 8, "aria-label": "imagelist" }));
			imgField.add(imgpreview);
			imgpreview.add(self._createElement("img", { src: "", alt: "" }));
			content.add(imgField);

			content.add(self._createSeparator());
			content.add(self._createUpload(true));

			content.add(self._createSeparator());
			content.add(self._createOKCancelButtons());

			dialog.add(content);
			dialog.add(self._createIframe());

			return dialog.render();
		}

		_onImageList() {
			var self = this;

			self.updateImageList({}, undefined);

			$.wijmo.wijeditor.prototype._onImageList.apply(self, arguments);

			self._doCallback(cmd_imgList, cmd_imgList);
		}

		initImageBrowserDialog() {
			var self = this,
				dlg = self._getDialog();

			$.wijmo.wijeditor.prototype.initImageBrowserDialog.apply(self, arguments);

			dlg.delegate("." + css_dlg_uploadbutton, "click." + self.widgetName,
				function () {
					self._uploadFile(const_frmhelpername, cmd_uploadImage);
				});

			dlg.delegate("." + css_dlg_delbtn, "click." + self.widgetName,
				function () {
					var selectedImg = $("select", dlg).val();
					if (selectedImg) {
						self._deleteIamge(selectedImg);
					}
				});

			$(self._form).append(dlg.parent());
		}

		_getDialogRes_link_linkType_options() {
			return [{
				id: "radUrl", value: "url",
				text: this.localizeString("hyperLinkDialogUrl", "url")
			}, {
					id: "radAnchor", value: "anchor", checked: true,
					text: this.localizeString("hyperLinkDialogAnchor", "anchor")
				}, {
					id: "radMail", value: "email",
					text: this.localizeString("hyperLinkDialogEmail", "email")
				}, {
					id: "radFile", value: "local file",
					text: this.localizeString("hyperLinkDialogLocalFile",
						"local file")
				}];
		}

		//link dialog
		_getDialogRes_Link() {
			var self = this,
				dialog = self._createDiv(css_linkdlg);
			dialog.add(self._getDialogRes_link_address());
			dialog.add(self._getDialogRes_link_linkType());

			dialog.add(self._createSeparator());
			dialog.add(self._getDialogRes_Link_linkIconType());
			dialog.add(self._getDialogRes_Link_text());
			dialog.add(self._getDialogRes_Link_image());
			dialog.add(self._getDialogRes_link_target());

			dialog.add(self._getDialogRes_Link_Css());
			dialog.add(self._createSeparator());
			dialog.add(self._createUpload());
			dialog.add(self._createSeparator());
			dialog.add(self._createOKCancelButtons());
			dialog.add(self._createIframe());
			return dialog.render();
		}

		initHyperLinkDialog() {
			var self = this,
				dlg = self._getDialog();

			$.wijmo.wijeditor.prototype.initHyperLinkDialog.apply(self, arguments);

			dlg.delegate("." + css_dlg_uploadbutton, "click." + self.widgetName,
				function () {
					self._uploadFile(const_frmhelpername, cmd_uploadLink);
				});

			$(self._form).append(dlg.parent());
		}

		_updateLinkDialog(url) {
			var self = this,
				dlg = self._getDialog();

			$("#radFile", dlg).attr("checked", "checked");
			$("." + css_linkdlg_address + " input", dlg).val(url);
		}

		//spellchecker
		_doSpellCheck() {
			var self = this;

			if (self._spellChecker) {
				self._spellChecker.c1spellchecker("checkControl", self._getDesignView());
			}
		}

		//remove the client text option
		//another method is decode in client, then encolde in server
		adjustOptions() {
			var self = this;
			if (!self._isTextEncoded) {
				self.options.text = (self.options.text === undefined) ? "" : c1common.htmlEncode(self.options.text);
				self._isTextEncoded = true;
			}
		}
	}

	c1editor.prototype.widgetEventPrefix = "c1editor";

	c1editor.prototype.options = $.extend(true, {}, $.wijmo.wijeditor.prototype.options, {

	});
	$.wijmo.registerWidget("c1editor", $.wijmo.wijeditor, c1editor.prototype);

}