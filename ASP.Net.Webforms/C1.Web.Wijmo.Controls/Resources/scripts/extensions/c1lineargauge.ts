﻿/// <reference path="../../../../../Widgets/Wijmo/wijlineargauge/jquery.wijmo.wijlineargauge.ts"/>

module C1 {
	class c1lineargauge extends wijmo.gauge.wijlineargauge {
		valueHolder: number;

		_create() {
			var self = this,
				o = self.options,
				isOldValueToValue = ((typeof (o.oldValue) !== "undefined") && o.animation.enabled === true);

			o.width = self.element.width();
			o.height = self.element.height();
			if (isOldValueToValue) {
				self._zeroToOldValue();
			}

			super._create();

			if (isOldValueToValue) {
				self._oldValueToValue();
			}
		}

		_zeroToOldValue() {
			var self = this,
				o = self.options;
			self.valueHolder = o.value;
			o.value = o.oldValue;
			o.animation.enabled = false;
		}

		_oldValueToValue() {
			var self = this,
				o = self.options;
			o.value = self.valueHolder;
			o.animation.enabled = true;

			self._set_value(o.value, o.oldvalue);
		}
	}

	c1lineargauge.prototype.widgetEventPrefix = "c1lineargauge";
	$.wijmo.registerWidget("c1lineargauge", c1lineargauge.prototype);
}
