﻿/// <reference path="../../../../../Widgets/Wijmo/wijmaps/maps.ts/wijmaps.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijmaps/maps.ts/MapSources.ts"/>

module c1 {

	var $ = jQuery;

	export class c1maps extends wijmo.maps.wijmaps {
	}

	c1maps.prototype.widgetEventPrefix = "c1maps";

	c1maps.prototype.options = $.extend(true, {}, $.wijmo.wijmaps.prototype.options, {});

	$.wijmo.registerWidget("c1maps", $.wijmo.wijmaps, c1maps.prototype);

}