﻿/// <reference path="../../../../../Widgets/Wijmo/wijinput/jquery.wijmo.wijinputcore.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijinput/jquery.wijmo.wijinpututility.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijinput/jquery.wijmo.wijinputdate.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijinput/jquery.wijmo.wijinputmask.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijinput/jquery.wijmo.wijinputcore.ts"/>

declare var c1common;

module c1 {

	var $ = jQuery;

	function inputFactory(name, baseName) {
		var baseClass = $.wijmo[baseName];

		//change namespace to wijmo
		//$.widget("c1." + name, baseClass, {
		$.widget("wijmo." + name, baseClass, {
			options: {
				postBackEventReference: null,
				autoPostBack: false
			},

			widgetEventPrefix: name,

			_create: function () {
				this.element.removeClass("ui-helper-hidden-accessible");
				baseClass.prototype._create.apply(this, arguments);

				if (this.options.cssClass) {
					this.outerDiv.addClass(this.options.cssClass);
				}
			},
			_init: function () {
				var changeEvents;
				baseClass.prototype._init.apply(this, arguments);

				if (this.options.displayVisible === false) {
					this.outerDiv.hide();
				} else {
					this.outerDiv.show();
				}

				if (this.options.autoPostBack && this.options.postBackEventReference) {
					changeEvents = name + "change " + baseName + "change";
					this.element.bind(changeEvents, $.proxy(this._makePostBackIfNeeded, this));
				}
			},
			_makePostBackIfNeeded: function () {
				var o = this.options;
				if (o.postBackEventReference) {
					c1common.nonStrictEval(o.postBackEventReference);
					return true;
				}
				return false;
			},

			_onEnterDown: function (e) {
				if (!this._makePostBackIfNeeded()) {
					this._super(e);
				}
			}
		});
	}

	inputFactory("c1inputcurrency", "wijinputnumber");
	inputFactory("c1inputnumeric", "wijinputnumber");
	inputFactory("c1inputpercent", "wijinputnumber");
	inputFactory("c1inputdate", "wijinputdate");
	inputFactory("c1inputmask", "wijinputmask");
	inputFactory("c1inputtext", "wijinputtext");

}