/// <reference path="../../../../../Widgets/Wijmo/Base/jquery.wijmo.widget.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijpager/jquery.wijmo.wijpager.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijtextbox/jquery.wijmo.wijtextbox.ts"/>

declare var __doPostBack, WebForm_DoCallback, c1common;

// Module
module c1 {


	// Class
	var $ = jQuery, command = "GetItems";;

	export class c1autocomplete extends wijmo.JQueryUIWidget {
		menu: any;
		source: any;

		_setOption(key, value) {

			$.ui.autocomplete.prototype._setOption.apply(this, arguments);
		}

		_create() {
			var self = this, o = self.options, el = self.element;
			el.removeClass("ui-helper-hidden-accessible").wijtextbox();

			if (o.items && o.items.length) {
				//				o.source = [];
				//				$.each(o.items, function (i, n) {
				//					o.source.push(n.label);
				//				});
				o.source = o.items
			}

			$.ui.autocomplete.prototype._create.apply(self, arguments);

			$(self.menu.element).css("z-index", 100);

			self.element.bind("focus", () => {
				var parent,
					zIndex = 0;
				parent = self.element.parent();
				while (parent && !parent.is("body")) {
					if (parent.css("z-Index") && zIndex < parseInt(parent.css("z-Index"))) {
						zIndex = parseInt(parent.css("z-Index"));
					}
					parent = parent.parent();
				}
				if (parseInt($(self.menu.element).css("z-index")) <= zIndex) {
					$(self.menu.element).css("z-index", zIndex + 10);
				}
			});

			self.menu.element.bind("menuselect", function (event, ui) {
				// update to jui 1.9.1 (API changed)
				// menuselected => menuselect,item.autocomplete => uiAutocompleteItem
				var item = ui.item.data("uiAutocompleteItem"),
					args = "itemSelected_" + item.value;
				el.val(item.value);
				if (o.autoPostBack) {
					__doPostBack(o.uniqueID, args);
				}
			});
		}

		_initSource() {
			var self = this, o = self.options;
			if (o.loadOnDemand) {
				self.source = function (request, response) {
					self._doCallBack(request, response);
				}
			}
			else {
				$.ui.autocomplete.prototype._initSource.apply(this, arguments);
			}
		}

		_doCallBack(request, response) {
			var self = this, o = self.options,
				id = o.uniqueID,
				requestData = { CommandName: undefined, CommandData: undefined }, arg,
				successCallBack = function (returnValue, context) {//When success
					var value = returnValue, items;
					items = $.parseJSON(value);
					if ($.isArray(items)) {
						response(items);
					}
					else { // When throw exception.
						alert(items.toString());
					}
				}, errorCallBack = function (error) {//When error
					alert(error);
				};


			requestData.CommandName = command;
			requestData.CommandData = request;
			arg = c1common.JSON.stringify(requestData);

			WebForm_DoCallback(id, arg, successCallBack, command, errorCallBack, true);
		}



		saveAutoCompleteState() {
			var o = this.options, id = this.element.attr("id");
			//o.text = this.element.val();
			c1common.updateJsonHiddenField(id, c1common.JSON.stringify(o));
		}

	}

	c1autocomplete.prototype.options = <any> $.extend(true, {}, $.ui.autocomplete.prototype.options, {
		source: []
	});

	$.wijmo.registerWidget("c1autocomplete", $.ui.autocomplete, c1autocomplete.prototype);
}

interface JQueryUI {
	autocomplete: any;
}

