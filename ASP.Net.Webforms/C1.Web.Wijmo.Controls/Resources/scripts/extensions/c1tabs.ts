﻿/// <reference path="../../../../../Widgets/Wijmo/wijtabs/jquery.wijmo.wijtabs.ts"/>

module c1 {

	var $ = jQuery;

	export class c1tabs extends wijmo.tabs.wijtabs {
		_init() {
			super._init();

			this.element.bind("c1tabsselect", jQuery.proxy(this._onSelect, this));
		}

		_create() {
			this.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}

		private _onSelect(e, ui) {
			var o = this.options;
			if (o.postBackEventReference) {
				c1common.nonStrictEval(o.postBackEventReference.replace("{0}", ui.index));
			}
			e.stopPropagation();
		}
	}

	c1tabs.prototype.widgetEventPrefix = "c1tabs";

	c1tabs.prototype.options = $.extend(true, {}, $.wijmo.wijtabs.prototype.options, {
		postbackReference: null
	});

	$.wijmo.registerWidget("c1tabs", $.wijmo.wijtabs, c1tabs.prototype);
}