﻿/// <reference path="../../../../../Widgets/Wijmo/wijrating/jquery.wijmo.wijrating.ts"/>

module c1 {

	var $ = jQuery;

	export class c1rating extends wijmo.rating.wijrating {

		_create() {
			this.element.removeClass("ui-helper-hidden-accessible");
			super._create();
		}
	}

	c1rating.prototype.widgetEventPrefix = "c1rating";

	$.extend(c1rating.prototype.options.wijCSS, $.wijmo.wijrating.prototype.options.wijCSS);

	$.wijmo.registerWidget("c1rating", $.wijmo.wijrating, c1rating.prototype);
}