/// <reference path="../../../../../Widgets/Wijmo/Base/jquery.wijmo.widget.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijevcal/jquery.wijmo.wijevcal.ts"/>

declare var __doPostBack, c1common;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible", customColorStyleIdPrefix = "c1-c1evcal-color-", customColorTable = {}, customColorTableIndex = 0;

	export class c1eventscalendar extends wijmo.evcal.wijevcal {
		_setOption(key, value) {
			$.wijmo.wijevcal.prototype._setOption.apply(this, arguments);
		}

		_create() {
			var self = this, o = self.options,
				eventDialogTemplate, eventTemplate,
				edtSelector = "#" + self.element[0].id + "_EventDialogTemplateContainer";
			self.element.addClass("c1-c1eventscalendar");
			eventDialogTemplate = $(edtSelector);
			eventTemplate = $("#" + self.element[0].id + "_EventTemplateContainer");
			if (eventDialogTemplate.length > 0 && eventDialogTemplate.html()) {
				o.editEventDialogTemplate = edtSelector;
			}
			if (eventTemplate.length > 0) {
				o.eventTemplate = eventTemplate.html();
			}
			eventTemplate.remove();
			$.wijmo.wijevcal.prototype._create.apply(self, arguments);
		}

		_init() {
			// remove ui-helper-hidden-accessible before invalidate:
			this.element.removeClass("ui-helper-hidden-accessible");
			$.wijmo.wijevcal.prototype._init.apply(this, arguments);
		}

		// begin override base method
		_onCalendarsChanged() {
			var self = this;
			self._normalizeItemColorName(self.options.calendars);
			super._onCalendarsChanged();
		}

		_onEventsDataChanged() {
			var self = this;
			self._normalizeItemColorName(self.options.eventsData);
			super._onEventsDataChanged();
		}

		addEvent(o, successCallback, errorCallback) {
			return super.addEvent(this._restoreColorValue(o), successCallback, errorCallback);
		}

		addCalendar(o, successCallback, errorCallback) {
			return super.addCalendar(this._restoreColorValue(o), successCallback, errorCallback);
		}

		updateEvent(o, successCallback?, errorCallback?) {
			return super.updateEvent(this._restoreColorValue(o), successCallback, errorCallback);
		}

		updateCalendar(o, successCallback, errorCallback) {
			return super.updateCalendar(this._restoreColorValue(o), successCallback, errorCallback);
		}
		// end override base method

		_normalizeItemColorName(data) {
			var self = this, colorName;
			$.each(data, function (i, v) {
				if (self._isCustomizedColor(v.color)) {
					// If the color is already customized(e.g. "black"- > "color1" where "color1" is the customized color name) and the input is the customized color name("color1"), then returns directly.
					if (customColorTable[v.color]) {
						return;
					}

					// If the color is already customized(e.g. "black"->"color1" where "color1" is the customized color name) and the input is the real color value("black") , then retrieves the color name from customColorTable member.
					colorName = self._getColorNameByColorValue(v.color)
					if (colorName) {
						v.color = colorName;
						return;
					}

					colorName = "color" + customColorTableIndex++;
					customColorTable[colorName] = v.color;
					self._generateCustomStylesheet(colorName, v.color);
					v.color = colorName;
				}
			});
		}

		_restoreColorValue(o) {
			var self = this;
			if (self._isCustomizedColor(o.color) && customColorTable[o.color]) {
				o.color = customColorTable[o.color];
			}
			return o;
		}

		_isCustomizedColor(color: string): boolean {
			var self = this, o = self.options;
			return !o.colors || (color && color !== "default" && $.inArray(color, o.colors) === -1);
		}

		_getColorNameByColorValue(colorValue: string): string {
			var colorName;
			$.each(customColorTable, function (key, val) {
				if (val === colorValue) {
					colorName = key;
				}
			});
			return colorName;
		}

		_generateCustomStylesheet(colorName: string, colorValue: string) {
			var self = this, id = customColorStyleIdPrefix + colorName, eventColorSelector = ".wijmo-wijev-event-color-" + colorName, eventTitleColorSelector = ".wijmo-wijev-title";
			if ($("#" + id).length === 0) {
				$("<style id=\"" + id + "\">" + eventColorSelector + ",\r\n" + eventColorSelector + " " + eventTitleColorSelector + "\r\n {background-color: " + colorValue + "}</style>").appendTo($("head"));
			}
		}

		// public methods>

		adjustOptions() {
			delete this.options.editEventDialogTemplate;
			delete this.options.eventTemplate;
		}

		///	<summary>
		///	Destroy the widget and reset the DOM element.
		///	</summary>
		destroy() {
			this.element.removeClass("c1-c1eventscalendar");
			$.wijmo.wijevcal.prototype.destroy.apply(this, arguments);
		}
	}

	c1eventscalendar.prototype.widgetEventPrefix = "c1eventscalendar";

	c1eventscalendar.prototype.options = $.extend(true, {}, $.wijmo.wijevcal.prototype.options, {});

	$.wijmo.registerWidget("c1eventscalendar", $.wijmo.wijevcal, c1eventscalendar.prototype);
}