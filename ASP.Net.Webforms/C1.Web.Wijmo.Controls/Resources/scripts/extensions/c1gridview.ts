﻿/// <reference path="../../../../../Widgets/Wijmo/wijpager/jquery.wijmo.wijpager.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/wijgrid/Grid.ts/Grid/wijgrid.ts"/>
/// <reference path="../../../../../Widgets/Wijmo/export/gridexport.ts"/>

declare var c1common;
declare var __JSONC1;

declare var WebForm_DoCallback;
declare module Sys.WebForms {
    class PageRequestManager {
        add_endRequest(endRequestHandler: (sender: any, args: any) => void): void;
        remove_endRequest(endRequestHandler: (sender: any, args: any) => void): void;
        static getInstance(): PageRequestManager;
        get_isInAsyncPostBack(): boolean;
    }
}

interface JQuery {
    c1gridview;
    c1detailgridview;
}

module c1 {


    var $ = jQuery;

    export class c1gridview extends wijmo.grid.wijgrid {
        static C1GridViewCss = {
            selectedRow: "c1-c1gridview-selectedrow",
            editRow: "c1-c1gridview-editrow",
            expandedRow: "c1-c1gridview-expandedrow",
            collapsedRow: "c1-c1gridview-collapsedrow",
            marker: "c1gridview-marker"
        }

        public options: IC1GridViewOptions;

        public mInsertNewRow: boolean;
        public mPartialCallback: boolean;
        public mCallbackInProgress: boolean;

        private mClonedTable: HTMLTableElement;
        private mFirstEditedRowIndex: number;
        private mTemplateCellsHandled: boolean;
        private mMobileSafari: boolean;
        private mCallbackAction: gridview.callbackAction;
        private mBatchValues: gridview.batchValues;
        private mHasDetails: boolean;
        private mParent: c1gridview = null;

        _onRendered(): void {
            this._resetDetailGrids();
            super._onRendered.apply(this, arguments);
        }

        pageCount(): number {
            return this.options.pageCount;
        }

        _serverShaping(): boolean {
            return true;
        }

        ensureControl(loadData: boolean, userData?: any) {
            super.ensureControl.apply(this, arguments);
        }

        _canInteract() {
            return super._canInteract.apply(this, arguments) && !this.mCallbackInProgress;
        }

        _freezingAllowed(): boolean {
            return !this.options.innerState._hd // disabled in a hierarchical grid
                && super._freezingAllowed.apply(this, arguments);
        }

        _init() {
            // ** map server properties to wijgrid
            if (this.options.allowClientEditing) {
                this.options.editingMode = "cell";
            }

            if (this.options.clientSelectionMode) {
                this.options.selectionMode = this.options.clientSelectionMode;
            }

            this.options.showSelectionOnRender = this.options.showClientSelectionOnRender;

            if (!this.options.culture) {
                this.options.culture = this.options.innerState._culture;
            }
            // map server properties to wijgrid **

            super._init.apply(this, arguments);
        }

        _create() {
            var self = this,
                topmost = this._topmostElement().removeClass("wijmo-wijgrid");

            this.options._aspNetDisabledClass = this.options.innerState._dc || "aspNetDisabled";

            if (!(topmost[0].style.width && (topmost[0].style.width !== "auto")))
                topmost.attr("has-no-initial-width", "true");


            this.mCallbackAction = gridview.enumConverter.fromString(gridview.callbackAction, this.options.callbackSettings.action);

            if (this.options.innerState.scrollingState) {
                this.mScrollingState = this.options.innerState.scrollingState;
            }

            this.mMobileSafari = wijmo.grid.isMobileSafari();
            this.mInsertNewRow = false;

            super._create.apply(this, arguments);

            // 341626, check auto size by the outer marker div
            var styleHeight = topmost[0].style.height;
            var styleWidth = topmost[0].style.width;
            var autoHeight = (styleHeight == "" || styleHeight == "auto");
            var autoWidth = (styleWidth == "" || styleWidth == "auto");
            this.mResizeOnWindowResize = autoHeight || ((styleHeight + "").indexOf("%") > 0) || autoWidth || ((styleWidth + "").indexOf("%") > 0);

            this.mAdjustHeaderText = false;

            // duplicate of the original table, used for cloning attributes and styles of cells and rows (server-side styles).
            this.mClonedTable = <HTMLTableElement>this.element.clone(false)[0];

            this.mFirstEditedRowIndex = -1;

            // localize filters display names if available.
            if (this.options.innerState._filterDisplayNames) {
                this._localizeFilterOperators(this.options.innerState._filterDisplayNames);
            }

            this.mHasDetails = !!this.options.innerState._hd;
            this.mCallbackInProgress = false;
            this.mTemplateCellsHandled = false;
        }

        _callbackAction(): gridview.callbackAction {
            return this.mCallbackAction;
        }

        _testCallback(action: gridview.callbackAction): boolean {
            return ((this._callbackAction() & action) !== gridview.callbackAction.none);
        }

        _topmostElement(): JQuery {
            return this.element.closest("." + c1gridview.C1GridViewCss.marker);
        }

        _originalHeaderRowData(): any[] {
            var header = this._tHead();

            if (header && header.length && this.options.showHeader) {
                var headerIdx = this.options.showFilter
                    ? header.length - 2
                    : header.length - 1;

                if (headerIdx < header.length) {
                    return header[headerIdx];
                }
            }

            return null;
        }

        _isServerSideEditRow(row: wijmo.grid.IRowInfo): boolean {
            var editIndex = this.options.innerState._bodyEditIndex;

            return !!row && (editIndex >= 0) && (row.dataItemIndex + this.mDataOffset === editIndex);
        }

        _buildSketchRow(wrappedDataItem: wijmo.grid.IWrappedDataItem, leaves: wijmo.grid.c1basefield[]): wijmo.grid.SketchRow {
            var meta: { value: wijmo.grid.IRowAttributes; } = { value: null };

            if (wijmo.grid.dataViewWrapper.isHierarchyDetailItem(wrappedDataItem.values, meta)) {
                var ra = meta.value.rowAttributes,
                    ca = meta.value.cellsAttributes,
                    detailRow = new c1.gridview.SketchHierarchyDetailRow(wrappedDataItem.originalRowIndex, wijmo.grid.renderState.rendering, ra);

                for (var i = 0; i < leaves.length && (leaves[i] instanceof wijmo.grid.c1rowheaderfield); i++) { // process rowheader\ detailheader columns
                    detailRow.add(new wijmo.grid.ValueCell(null, {})); // append empty cells
                }

                $.each(wrappedDataItem.values, (key, value) => { // process detail cells
                    detailRow.add(new wijmo.grid.HtmlCell(value, (ca ? ca[key] : null) || {})); // append empy cells
                });

                return detailRow;
            } else {
                var sketchRow: wijmo.grid.SketchRow = super._buildSketchRow.apply(this, arguments),
                    isCollapsedHierarchyRow = wijmo.grid.dataViewWrapper.testMetaForClass(meta && meta.value, c1gridview.C1GridViewCss.collapsedRow);

                if (isCollapsedHierarchyRow || wijmo.grid.dataViewWrapper.testMetaForClass(meta && meta.value, c1gridview.C1GridViewCss.expandedRow)) {
                    sketchRow.rowType |= wijmo.grid.rowType.dataHeader;

                    if (isCollapsedHierarchyRow) {
                        sketchRow.extInfo.state |= wijmo.grid.renderStateEx.collapsed;
                    }
                }

                return sketchRow;
            }
        }

        _cloneNode(src: Node): Node {
            var result = src.cloneNode(false), // Do not clone child elements, only duplicate the element itself with its attributes (needed to involve server-side styles).
                child: Node;

            // important: original node will be empty
            while (child = src.firstChild) {
                result.appendChild(child);
            }

            return result;
        }

        _moveContent(src: Node, dst: Node): void {
            if (src && dst) {
                // clear dst
                while (dst.firstChild) {
                    dst.removeChild(dst.firstChild);
                }

                // src -> dst
                while (src.firstChild) {
                    dst.appendChild(src.firstChild);
                }
            }
        }

        _onViewCreateEmptyCell(rowInfo: wijmo.grid.IRowInfo, dataCellIndex: number, column: wijmo.grid.c1basefield): HTMLTableCellElement {
            var rt = wijmo.grid.rowType,
                opt = column.options;

            if (!this.mInsertNewRow && (dataCellIndex >= 0 || rowInfo.type === rt.header)) {
                // move content from the original table to a new one during the first rendering only.

                var columnHeadersTable, cell, tmp, tableSection, headerCellIndex;

                switch (rowInfo.type & ~(rt.dataAlt | rt.dataDetail | rt.dataHeader)) {
                    case rt.header:
                        tableSection = this.mClonedTable.tHead;

                        //clone table row header [0] does not include virtual columns like rowHeader, detailHeader. So, excluding virtual columns index:
                        headerCellIndex = opt._thX - (opt._thY > 0 ? 0 : this._virtualLeaves().length);

                        // clone the original header cell (to support header templates)
                        if (headerCellIndex >= 0 && tableSection && (tmp = tableSection.rows[opt._thY]) && (tmp = tmp.cells[headerCellIndex])) {
                            return <any>$(tmp).clone()[0];
                        }

                        break;

                    case rt.filter:
                        tableSection = this.mClonedTable.tHead;
                        if (tableSection && (tmp = tableSection.rows[tableSection.rows.length - 1]) && (tmp = tmp.cells[dataCellIndex])) {
                            return tmp.cloneNode(false);
                        }
                        break;

                    case rt.data:
                        if (this.options.allowCustomContent) {
                            if (tableSection = this.mClonedTable.tBodies) {
                                tableSection = tableSection[0];
                            }

                            if (tableSection && (tmp = tableSection.rows[rowInfo.dataItemIndex]) && (tmp = tmp.cells[dataCellIndex])) {
                                return <any>this._cloneNode(tmp);
                            }
                        }

                        break;

                    case rt.footer:
                        tableSection = this.mClonedTable.tFoot;

                        if (tableSection && (tmp = tableSection.rows[0]) && (tmp = tmp.cells[dataCellIndex])) {
                            return <any>this._cloneNode(tmp);
                        }

                        break;
                }
            }

            // groupFooter, groupHeader, banded row header, or C1GridView is not bounded to any data.
            return super._onViewCreateEmptyCell.apply(this, arguments);
        }

        _onViewCellRendered(cell: JQuery, container: JQuery, row: wijmo.grid.IRowInfo, cellIndex: number, column: wijmo.grid.c1basefield) {
            if (row.type & wijmo.grid.rowType.data) {
                var opt = <wijmo.grid.IColumn>column.options,
                    htmlContent = (row.data.html && row.data.html[opt.dataKey]) || null; // a cell has an unparsed content?

                if (row.type & wijmo.grid.rowType.dataDetail) {
                    if (cellIndex === this._virtualLeaves().length + 1) { // a detail cell
                        htmlContent = null;
                    }
                }

                if (htmlContent) {
                    if (!(column instanceof c1.gridview.c1gridviewtemplatefield)) {
                        var isEditRow = this._isServerSideEditRow(row),
                            isC1Editor = this.options.allowC1InputEditors === true;

                        if (isEditRow && (htmlContent !== row.data[opt.dataKey]) // raw and parsed data is different
                            && ((isC1Editor && !this.mRenderCounter) // in a partial callback mode we are using html strings, not a live DOM.
                                || (!isC1Editor && (<c1.gridview.c1gridviewboundfield>column).options.applyFormatInEditMode))) {

                            if (!isC1Editor) { // regular textbox + applyFormatInEditMode
                                var formattedValue = this.toStr(opt, row.data[opt.dataKey]);
                                var regularEditor = container.find(":input");

                                if (!regularEditor.length && htmlContent) { // no editor?
                                    container.html(htmlContent); // update content with a raw html
                                    regularEditor = container.find(":input");
                                }

                                container.find(":input").val(formattedValue);
                            } else { // c1editor + 1st render stage
                                // do nothing
                            }
                        } else {
                            if (isEditRow && isC1Editor) {
                                // 79648, 79165: child controls are initialized first, so htmlContent contains not original markup, but the one when C1Input has been initialized already. Use original markup instead.
                                var serverSideMarkup = this._getC1InplaceEditorMarkup(<c1.gridview.c1gridviewtableboundfield>column);
                                if (serverSideMarkup) {
                                    htmlContent = serverSideMarkup;
                                }
                            }

                            container.html(htmlContent); // update content with a raw html
                        }
                    } else {
                        if (this.mRenderCounter > 0) { // there is nothing to clone because original (cloned) table has been emptied already. Update content using raw html
                            container.html(htmlContent);
                        }
                    }
                }
            }

            super._onViewCellRendered.apply(this, arguments);
        }

        _onViewInsertEmptyRow(rowType: wijmo.grid.rowType, renderState: wijmo.grid.renderState, sectionRowIndex: number, dataRowIndex: number, dataItemIndex: number, virtualDataItemIndex: number, sketchRowIndex: number, groupByValue: any): HTMLTableRowElement {
            var domRow, columnHeadersTable, tableSection,
                rt = wijmo.grid.rowType;

            if (!this.mInsertNewRow && this.mRenderCounter === 0) {
                switch (rowType & ~(rt.dataAlt | rt.dataDetail | rt.dataHeader)) {
                    case rt.header:
                        columnHeadersTable = this._columnsHeadersTable();

                        if (sectionRowIndex === columnHeadersTable.length - 1) { // the last header row is the row rendered by C1GridView, clone it.
                            if (tableSection = this.mClonedTable.tHead) {
                                domRow = tableSection.rows[0];
                            }
                        }

                        break;

                    case rt.filter:
                        if (tableSection = this.mClonedTable.tHead) {
                            domRow = tableSection.rows[tableSection.rows.length - 1]; // last header row;
                        }

                        break;

                    case rt.data:
                        domRow = (<HTMLTableSectionElement>this.mClonedTable.tBodies[0]).rows[dataItemIndex];
                        break;

                    case rt.footer:
                        if (tableSection = this.mClonedTable.tFoot) {
                            domRow = tableSection.rows[0];
                        }

                        break;
                }
            }

            tableSection = null;

            return (domRow)
                ? domRow.cloneNode(false) // // pass a copy of the original row (without content) to the wijgrid instead of creating a new one.
                : super._onViewInsertEmptyRow.apply(this, arguments); // groupHeader, groupFooter, banded row header, or C1GridView is not bouned to any data.
        }

        _onViewRowRendered(rowInfo: wijmo.grid.IRowInfo, rowAttr?, rowStyle?) {
            super._onViewRowRendered.apply(this, arguments);

            var className;

            if (rowAttr && (className = rowAttr["class"])) {
                if (className.indexOf(c1gridview.C1GridViewCss.selectedRow) >= 0) {
                    rowInfo.$rows.addClass(this.options.wijCSS.stateHighlight); // provide a default style for the server-side selected row
                }

                if (this.options.innerState._cbEditRowInitScript && (className.indexOf(c1gridview.C1GridViewCss.editRow) >= 0)) {
                    eval(this.options.innerState._cbEditRowInitScript); // initialize c1input editors during partial callback or virtual scrolling
                }
            }

            if (rowInfo.type & wijmo.grid.rowType.dataDetail) { // set colSpan attribute
                var cnt = wijmo.grid.rowAccessor.cellsCount(rowInfo.$rows),
                    detailCell = wijmo.grid.rowAccessor.getCell$(rowInfo.$rows, cnt - 1);

                detailCell.attr("colspan", this._visibleLeaves().length - cnt + 1);
            }
        }

        _render() {
            super._render.apply(this, arguments);

            this.element
                .closest("#" + this.element[0].id + "_div")
                .removeClass("ui-helper-hidden-accessible");
        }

        _createGroupAreaColumnHeader(element: JQuery, column: wijmo.grid.c1field): wijmo.grid.c1groupedfield {
            return new c1.gridview.c1gridviewgroupedfield(this, <wijmo.grid.IColumn>column.options, element);
        }

        _allowHVirtualScrolling(): boolean {
            var result = super._allowHVirtualScrolling.apply(this, arguments);
            return result && !this._hasDetails(); // because of spanned cells
        }

        _serverSideVirtualScrolling(): boolean {
            return this._allowVVirtualScrolling() && this._testCallback(gridview.callbackAction.scrolling);
        }

        _batchValues(): c1.gridview.batchValues {
            if (!this.mBatchValues) {
                this.mBatchValues = new c1.gridview.batchValues(this);
            }

            return this.mBatchValues;
        }

        _needToCreatePagerItem(): boolean {
            return (this.options.pagerSettings.visible === true) && super._needToCreatePagerItem.apply(this, arguments);
        }

        _rebuildLeaves() {
            super._rebuildLeaves.apply(this, arguments);

            // no header - clear span table
            if (!this.options.showHeader) {
                this._columnsHeadersTable([]);
            }
        }

        _onLeavesCreated() {
            if (!this.mTemplateCellsHandled) { // handle server-side controls
                var leaves = this._leaves(),
                    srcTable: HTMLTableElement = <any>this.element[0],
                    dstTable: HTMLTableElement = this.mClonedTable,
                    editIndex = this.options.innerState._bodyEditIndex;

                // * Move child elements of the row being edited to clonedTable. *
                if (this.options.allowC1InputEditors && (editIndex >= 0)) {
                    var srcBody: HTMLTableSectionElement = <any>srcTable.tBodies[0],
                        dstBody: HTMLTableSectionElement = <any>dstTable.tBodies[0],

                        rowIndex = editIndex - this.mDataOffset,
                        srcRow: HTMLTableRowElement = <HTMLTableRowElement>srcBody.rows[rowIndex];

                    if (srcRow) {
                        $.each(leaves, (idx, column: wijmo.grid.c1basefield) => {
                            var opt = <wijmo.grid.IColumn>column.options;

                            if (!(column instanceof c1.gridview.c1gridviewtemplatefield)) { // cells can contains a server-side controls
                                var srcCell = srcRow.cells[opt.dataKey],
                                    dstCell = (<HTMLTableRowElement>dstBody.rows[rowIndex]).cells[opt.dataKey];

                                this._moveContent(srcCell, dstCell);
                            }
                        });
                    }
                }

                // * Move child elements from the template column cells of the original table to clonedTable. *
                var tmplLeaves: wijmo.grid.c1basefield[] = [];

                $.each(leaves, (idx, column: wijmo.grid.c1basefield) => {
                    if (column instanceof c1.gridview.c1gridviewtemplatefield) {
                        tmplLeaves.push(column);
                    }
                });

                if (this._hasDetails() || tmplLeaves.length) {
                    for (var i: number = 0, srcRow: HTMLTableRowElement; srcRow = <HTMLTableRowElement>srcTable.rows[i]; i++) {
                        if (srcRow.className.indexOf(wijmo.grid.wijgrid.CSS.emptyDataRow) >= 0) {
                            continue; // skip emptyDataTemplate\ emptyDataText row
                        }

                        var dstRow = <HTMLTableRowElement>dstTable.rows[i];

                        if (this._hasDetails() && (srcRow.className.indexOf(wijmo.grid.wijgrid.CSS.detailRow) >= 0)) {
                            for (var j = 0; j < srcRow.cells.length; j++) {
                                this._moveContent(srcRow.cells[j], dstRow.cells[j]);
                            }
                        } else {
                            if (tmplLeaves.length) {
                                $.each(tmplLeaves, (_, leaf: c1.gridview.c1gridviewtemplatefield) => {
                                    var dataKey = leaf.options.dataKey;
                                    this._moveContent(srcRow.cells[dataKey], dstRow.cells[dataKey]);
                                });
                            }
                        }
                    }
                }

                this.mTemplateCellsHandled = true;
            }

            super._onLeavesCreated.apply(this, arguments);
        }

        _needParseColumnDOM(column: wijmo.grid.IColumn): boolean {
            return super._needParseColumnDOM(column) || (column._clientType === "c1templatefield");
        }

        _getDOMReaderCulture(): GlobalizeCulture {
            return Globalize.findClosestCulture("en"); // use invariant culture when reading values from DOM.
        }

        /** @ignore */
        parseCtx(column: wijmo.grid.IColumn, value: any, dataItem: any, cell: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any {
            if (cell && column._clientType === "c1templatefield") { // DOM parsing stage ( dataItem and domCell arguments are provided at the DOM parsing stage only)
                dataItem.html = dataItem.html || {}
                dataItem.html[column.dataKey] = value; // store original content to use it during cell rendering (value == cell.innerHTML)

                return wijmo.grid._getDOMText(cell); // now we can calculate agrregates (#64213).
            }

            return this.parse.apply(this, [column, value, forcedCulture]);
        }

        /** @ignore */
        parseCtxFailed(column: wijmo.grid.IColumn, value: any, dataItem: any, cell: HTMLTableCellElement, forcedCulture?: GlobalizeCulture): any {
            var newValue;

            dataItem.html = dataItem.html || {}; // contains original content of the cells that can't be parsed.

            if (cell) { // failed. Try to parse wijgrid-data attrubute\ inner content
                var dataAttr: string = cell.getAttribute("wijgrid-data"),
                    newValue = (dataAttr || (dataAttr === ""))
                        ? this._castValue(column, dataAttr)
                        : this._castValue(column, wijmo.grid._getDOMText(cell, 3, column._clientType === "c1checkboxfield"));

                if (!isNaN(newValue) && (column.dataType === "datetime") && this.mMobileSafari) {
                    // get rid of iOS phone format detection: 123456789012 -> <a href="tel:123456789012">123456789012</a>
                } else {
                    dataItem.html[column.dataKey] = cell.innerHTML; // save original content for later use
                }

                newValue = wijmo.grid.isNaN(newValue) ? value : newValue;
            }

            return newValue;
        }

        /** @ignore */
        parse(column: wijmo.grid.IColumn, value: any, forcedCulture?: GlobalizeCulture): any {
            var culture = forcedCulture || this._closestCulture(),
                parser = this._getDataParser(column),
                result = parser.parse(value, culture, this._getDefaultDataFormatString(column), this.options.nullString || "", true);

            if (wijmo.grid.isNaN(result)) {
                result = this._castValue(column, value);
            }

            return result;
        }

        /** @ignore */
        toStr(column: wijmo.grid.IColumn, value: any, forcedCulture?: GlobalizeCulture): string {
            var culture = forcedCulture || this._closestCulture(),
                parser = this._getDataParser(column);

            return parser.toStr(value, culture, this._getDefaultDataFormatString(column), this.options.nullString || "", true);
        }

        _addVirtualColumns(columns: wijmo.grid.IColumn[]): void {
            super._addVirtualColumns.apply(this, arguments); // append rowHeader

            if (this._hasDetails()) {
                columns.push(c1.gridview.c1detailheaderfield.getInitOptions());
            }
        }

        _prepareColumnsStage1(columns: wijmo.grid.IColumn[], dataLoaded: boolean, finalStage: boolean): void {
            if (dataLoaded && finalStage) {
                if (this.options.autoGenerateDeleteButton || this.options.autoGenerateEditButton ||
                    this.options.autoGenerateSelectButton || this.options.autoGenerateFilterButton) {
                    columns.splice(0, 0, wijmo.grid.createDynamicField({ innerState: { clientType: "c1commandfield" } }));
                }

                if (this.options.innerState && this.options.innerState.autogeneratedColumns) {
                    for (var i = 0, len = this.options.innerState.autogeneratedColumns.length; i < len; i++) {
                        var tmp = wijmo.grid.createDynamicField(this.options.innerState.autogeneratedColumns[i]);
                        columns.push(tmp);
                    }
                }
            }

            super._prepareColumnsStage1.apply(this, arguments);
        }

        _prepareColumnsStage2(columns: wijmo.grid.IColumn[], generationMode: string, fieldsInfo: wijmo.grid.IFieldInfoCollection, dataLoaded: boolean, finalStage: boolean, generateInstances = true): void {
            if (dataLoaded && finalStage) {
                var dataKey = 0;

                wijmo.grid.traverse(columns, function (column: wijmo.grid.IColumn) {
                    // map columns to DOMTable columns
                    if (column._isLeaf) {
                        column.dataKey = dataKey++;
                    }
                });
            }

            wijmo.grid.traverse(columns, function (column: wijmo.grid.IColumn) {
                var innerState = <c1.gridview.IC1GridViewFieldInnerState>(<any>column).innerState;

                // extract clientType
                if (innerState && innerState.clientType) {
                    column._clientType = innerState.clientType.split(":")[0].toLowerCase();
                }
            });

            super._prepareColumnsStage2.apply(this, arguments);
        }

        _hasDetails(): boolean {
            return this.mHasDetails;
        }

        _isRootGridView(): boolean {
            return !!(this.options.innerState && this.options.innerState._root);
        }

        _getRoot(): c1.c1gridview {
            if (this._isRootGridView()) {
                return this;
            } else {
                var root = this._parent();

                while (root._parent()) {
                    root = root._parent();
                }

                return root;
            }
        }

        _getFullPath(): string {
            if (this._isRootGridView()) {
                return "";
            } else {
                var path: number[] = [],
                    current = this;

                do {
                    path.unshift((<c1detailgridview>current).options.path);
                } while ((current = current._parent()) && !current._isRootGridView());

                return path.join("-");
            }
        }

        private _detailGrids: c1detailgridview[] = null;
        detailGrids(): c1detailgridview[] {
            if (this._hasDetails() && !this._detailGrids && this._view()) {
                var rows = this._rows(),
                    view = this._view(),
                    len = rows.length();

                if (len) {
                    this._detailGrids = [];
                    var dataRowCnt = -1;

                    for (var i = 0; i < rows.length(); i++) {
                        var row = view._getRowInfo(rows.item(i), false);

                        if (row.type & wijmo.grid.rowType.dataDetail) {
                            var childGrid = row.$rows.find("." + wijmo.grid.wijgrid.CSS.root).eq(0);
                            if (childGrid.length) {
                                var instance = <c1.c1detailgridview>childGrid.data("wijmo-" + c1.c1detailgridview.prototype.widgetEventPrefix);

                                if (instance) {
                                    instance.options.path = dataRowCnt;
                                    this._detailGrids.push(instance);
                                }


                            }
                        } else {
                            if (row.type & wijmo.grid.rowType.data) {
                                dataRowCnt++;
                            }
                        }
                    }
                }
            }

            return this._detailGrids || [];
        }

        _resetDetailGrids() {
            if (this._detailGrids) {
                $.each(this._detailGrids, (_, d) => {
                    d.options.path = undefined;
                });
            }

            this._detailGrids = null;

            // restore parents
            if (this._hasDetails() /*&& this._isRoot()*/) {
                $.each(this.detailGrids(), (idx, detail: c1detailgridview) => {
                    detail._parent(this);
                });
            }
        }

        _parent(value?: c1gridview) {
            if (arguments.length) {
                this.mParent = value;
            }

            return this.mParent;
        }

        _findDetailByPath(path: number): c1detailgridview {
            var result: c1detailgridview = null;

            $.each(this.detailGrids(), (_, d) => {
                if (d.options.path === path) {
                    result = d;
                    return true; // break
                }
            });

            return result;
        }

        _setSizeInternal(scrollValue: wijmo.grid.IScrollingState, rowsToAdjust?: wijmo.grid.cellRange, resizeDetails = true) {
            // #43727 (c1gridview placed inside the c1superpanel)
            var topmost = this._topmostElement();

            var hasNoInitialWidth = topmost.attr("has-no-initial-width");

            if (hasNoInitialWidth) {
                topmost.width("");
            }

            super._setSizeInternal.apply(this, arguments);

            if (hasNoInitialWidth) {
                topmost.css("width", this._view().getInlineTotalWidth() || ""); // set absolute width
            }

            if (this.options.innerState._hd && resizeDetails) { // has details
                var details = <c1.c1detailgridview[]>this.detailGrids();
                for (var i = 0; i < details.length; i++) {
                    details[i]._ignoreSizing(false);
                    details[i].setSize();
                    details[i]._ignoreSizing(true);
                }
            }
        }

        _onAfterCellUpdate(args: wijmo.grid.IAfterCellUpdateEventArgs) {
            if (this.mFirstEditedRowIndex === -1) {
                this.mFirstEditedRowIndex = args.cell.rowIndex();
            }

            super._onAfterCellUpdate.apply(this, args);
        }

        _onColumnDropping(args: wijmo.grid.IColumnDroppingEventArgs) {
            if (super._onColumnDropping.apply(this, arguments)) {

                var dragState = <c1.gridview.IC1GridViewFieldInnerState>(<any>args.drag).innerState,
                    dropState = <c1.gridview.IC1GridViewFieldInnerState>(<any>args.drop).innerState,

                    params: any[] = [
                        "move",
                        dragState._uid,
                        dropState._uid,
                        args.at
                    ];

                if (this._testCallback(gridview.callbackAction.colMove)) {
                    this._doCallback(params);
                } else {
                    this._doPostBack(params);
                }
            }

            return false;
        }

        _onColumnGrouping(args: wijmo.grid.IColumnGroupingEventArgs) {
            if (super._onColumnGrouping.apply(this, arguments)) {
                var dragState = <c1.gridview.IC1GridViewFieldInnerState>(<any>args.drag).innerState,
                    dropState = args.drop && <c1.gridview.IC1GridViewFieldInnerState>(<any>args.drop).innerState, // args.drop == null in case of groupArea

                    params: any[] = ["group",
                        dragState._uid,
                        args.drag.groupedIndex !== undefined ? args.drag.groupedIndex : -1,
                        args.drop ? dropState._uid : -1,
                        args.drop ? args.drop.groupedIndex : -1, args.at
                    ];

                if (this._testCallback(gridview.callbackAction.grouping)) {
                    this._doCallback(params);
                } else {
                    this._doPostBack(params);
                }
            }

            return false;
        }

        _onColumnUngrouping(args: wijmo.grid.IColumnUngroupingEventArgs) {
            if (super._onColumnUngrouping.apply(this, arguments)) {
                var params: any[] = ["ungroup", (<c1.gridview.IC1GridViewFieldInnerState>(<any>args.column).innerState)._uid];

                if (this._testCallback(gridview.callbackAction.grouping)) {
                    this._doCallback(params);
                } else {
                    this._doPostBack(params);
                }
            }

            return false;
        }

        _onColumnFiltering(args: wijmo.grid.IFilteringEventArgs) {
            if (super._onColumnFiltering.apply(this, arguments)) {
                if ((args.column.dataType === "datetime") && (args.value instanceof Date)) {
                    args.value = c1.gridview.truncDateTime(args.value, args.column.dataFormatString);
                }

                var innerState = <c1.gridview.IC1GridViewFieldInnerState>(<any>args.column).innerState;

                //Encode filterValue if column is of string dataType.
                if (args.value && !args.column.dataType) {
                    innerState.newFilterValue = c1common.htmlEncode(args.value);
                } else {
                    innerState.newFilterValue = args.value;
                }
                innerState.newFilterOperator = args.operator;

                if (this.options.filterSettings.filterOnSelect) {
                    var params = ["filter", <any>0];

                    if (this._testCallback(gridview.callbackAction.filtering)) {
                        this._doCallback(params);
                    } else {
                        this._doPostBack(params);
                    }
                }
            }

            return false;
        }

        _onColumnSorting(args: wijmo.grid.ISortingEventArgs) {
            if (super._onColumnSorting.apply(this, arguments)) {
                var column = <c1.gridview.c1gridviewtableboundfield><any>this._allColumns()[args.column._travIdx];

                if (column._isSortable()) { // only autogenerated and user-defined columns with sortExpression property explicitly defined can be sorted.
                    var params: any[] = [
                        "sort",
                        column.options.innerState._uid,
                        column.options.sortExpression
                    ];

                    if (this._testCallback(gridview.callbackAction.sorting)) {
                        this._doCallback(params);
                    } else {
                        this._doPostBack(params);
                    }
                }
            }

            return false;
        }

        _onCurrentCellChanged() {
            super._onCurrentCellChanged.apply(this, arguments);

            if (this.options.clientEditingUpdateMode === "autoRowEdit" && this.mFirstEditedRowIndex >= 0 && this.mFirstEditedRowIndex !== this.currentCell().rowIndex()) {
                this.mFirstEditedRowIndex = -1;
                this.update();
            }
        }

        _onPageIndexChanging(args: wijmo.grid.IPageIndexChangingEventArgs) {
            if (super._onPageIndexChanging.apply(this, arguments)) {
                var params: any[] = ["page", args.newPageIndex];

                if (this._testCallback(gridview.callbackAction.paging)) {
                    this._doCallback(params);
                } else {
                    this._doPostBack(params);
                }
            }

            return false;
        }

        _onBatchUpdate(args) {
            var params = ["batch"];

            this.options.batchValues = args.value;

            if (this._testCallback(gridview.callbackAction.editing)) {
                this._doCallback(params);
            } else {
                this._doPostBack(params);
            }
        }

        _onToggleHierarchy(e: JQueryEventObject, row: wijmo.grid.IRowInfo) {
            if (this._canInteract()) {
                var params = ["hierarchy", row.$rows.attr("id")];

                if (this._testCallback(gridview.callbackAction.hierarchy)) {
                    this._doCallback(params);
                } else {
                    this._doPostBack(params);
                }
            }
        }

        _postset_allowClientEditing(value, oldValue) {
            this._setOption("editingMode", value ? "cell" : "none"); // update wijgrid.editingMode option.
        }

        _postset_clientSelectionMode(value, oldValue) {
            this._setOption("selectionMode", value); // update wijgrid.selectionMode option.
        }

        _postset_displayVisible(value, oldValue) {
            if (value) {
                this.element.show();
                this._topmostElement().show();
                this.setSize();
            } else {
                this._topmostElement().hide();
                this.element.hide();
            }
        }

        _postset_showClientSelectionOnRender(value, oldValue) {
            this._setOption("showSelectionOnRender", value); // update wijgrid.showSelectionOnRender option.
        }

        _postset_selectedIndex(value, oldValue) {
            var params = ["select", value];

            if (this._testCallback(gridview.callbackAction.selection)) {
                this._doCallback(params);
            } else {
                this._doPostBack(params);
            }
        }

        onwijstatesaving(): any {

            var tmp: any = $.extend(true, {}, this.options),
                simplify = function (item) {
                    var key, value;
                    for (key in item) {
                        if (item.hasOwnProperty(key)) {
                            value = item[key];

                            if ((value === null) || (value === undefined)
                                || (typeof (value) === "function") || ((key + "").indexOf("_") === 0)
                                || (key === "data") || (key === "dataParser") || (key === "expandInfo")
                                || (key === "wijCSS") || (key === "wijMobileCSS")) {
                                delete item[key];
                            } else {
                                if ($.isPlainObject(value) || $.isArray(value)) {
                                    simplify(value);
                                }
                            }
                        }
                    }
                };

            // remove dynamic columns first (simplify will remove all underscored properties)
            tmp.columns = $.grep(tmp.columns, (column: wijmo.grid.IColumn, _) => {
                return !column._dynamic;
            });

            // remove redundant properties
            simplify(tmp);

            return tmp;
        }

        _castValue(column: wijmo.grid.IColumn, value: any): any {
            var foo;

            switch (column.dataType) {
                case "number":
                case "currency":
                    value = parseFloat(value);
                    break;

                case "datetime":
                    foo = value * 1;

                    if (isNaN(foo)) { // string value
                        value = new Date(value);

                        if (!wijmo.grid.validDate(value)) { // invalid string
                            value = NaN;
                        }
                    } else {
                        var offset = new Date(foo).getTimezoneOffset() * 60000; // local offset, depends on current value (because of DTS, #49810).
                        value = new Date(foo + offset); // foo is a timestamp (server timezone, utc + server offset) => (utc + server offset) + local offset - local offset === server datetime (#39285).
                    }

                    break;

                case "boolean":
                    foo = value.toLowerCase();
                    if (foo === "true" || foo === "false") {
                        value = (foo === "true");
                    } else {
                        value = NaN;
                    }

                    break;
            }

            return value; // value or NaN
        }

        _onBeforeCallback(argument): string {
            var args = argument.split(":"),
                result = "";

            if (args.length && args[0].toLowerCase() === "update") {
                var values = this._onBeforeUpdateCmdCallback(this.mOuterDiv.find("tr." + c1gridview.C1GridViewCss.editRow));
                result = c1common.JSON.stringify(values || []);
            }

            return result;
        }

        _doCallback(argumentsArray: any[], contextParam?: any): void {
            this._encodeFilterValues();
            var id = this.options.innerState._cid;

            new c1.gridview.cbp(id || "", //.replace(/\$/g, "_")
                argumentsArray.join(":"),
                contextParam);
        }

        _doPostBack(argumentsArray) {
            this._encodeFilterValues();
            var params = wijmo.grid.stringFormat(this.options.innerState._pbRef, argumentsArray.join(":"));

            if (!window.event) {
                // Workaround for an ASP.NET 4 issue: Sys$WebForms$PageRequestManager$_doPostBack function is trying to access arguments.callee if window.event is undefined.
                // Access to arguments.caller\ arguments.callee throw an exception when "strict mode" is used.
                window.event = <MSEventObj><any>{
                    target: null,
                    srcElement: null
                };
            }

            eval(params);
        }

        _encodeFilterValues() {
            var columns = this.columns(), filterValue: any;
            for (var i = 0; i < columns.length; i++) {
                filterValue = columns[i].option("filterValue");
                if (filterValue && !columns[i].option("dataType")) {
                    columns[i].option("filterValue", c1common.htmlEncode(filterValue));
                }
            }
        }

        _getC1InplaceEditorMarkup(column: c1.gridview.c1gridviewtableboundfield): string {
            if (column) {
                var gi = this.options.innerState,
                    ci = column.options.innerState;

                if (gi && ci) {
                    var markup = gi._c1InplaceEditorsMarkup;

                    if (markup && markup[ci._uid]) {
                        return wijmo.htmlDecode(markup[ci._uid]);
                    }
                }
            }

            return null;
        }

        _getDefaultDataFormatString(column: wijmo.grid.IColumn): string {
            if (!column.dataFormatString) {
                if (column.dataType === "datetime" && (<any>column).dataTypeQualifier === "timespan") {
                    return "T"; // Long Time h:mm:ss tt
                }

                if (column.dataType === "number" && !(<any>column).dataTypeQualifier) {
                    return "n0";
                }
            }

            return column.dataFormatString;
        }

        _onBeforeUpdateCmdCallback($row) {
            var view = this._view(),
                rowIndex = view.getAbsoluteRowIndex($row[0]),
                values = [],
                self = this,

                getEditorValue = function (domCell, cellIndex, param) {
                    $(domCell).find(":input").each(function (index, element) {
                        if ((<HTMLInputElement>element).type === "hidden") {
                            return true;
                        }

                        var $editor = $(element),
                            value = self._getEditorValue($editor);

                        if (value !== undefined) {
                            name = $editor.attr("name") || $editor.attr("id");
                            if (name) {
                                param.push([name.replace(/_/g, "$"), value]);
                            }
                        }
                    });

                    return true;
                };

            view.forEachRowCell(rowIndex, getEditorValue, values);

            getEditorValue = null;

            return values;
        }

        _getEditorValue($editor: JQuery): any {
            var result = null,
                data = $editor.data(),
                widgetName: string;

            if (data && (widgetName = data["widgetName"])) {
                var instance: wijmo.JQueryUIWidget = $editor.data("wijmo-" + widgetName);

                if (instance) {
                    if (instance instanceof $.wijmo.wijinputnumber) {
                        return instance.options.value
                    }

                    if (instance instanceof $.wijmo.wijinputdate) {
                        return instance.options.date;
                    }

                    if ((instance instanceof $.wijmo.wijinputtext) || (instance instanceof $.wijmo.wijinputmask)) {
                        return instance.options.text;
                    }
                }
            }

            return $editor.is(":checkbox")
                ? $editor.is(":checked")
                : $editor.val();
        }

        _processingPartialCallback(): boolean {
            return this.mInsertNewRow;
        }

        _updateUnderlyingInfo(sketchStartIndex, rowStartIndex, delta) {
            for (var i = sketchStartIndex, len = this.mSketchTable.count(); i < len; i++) {
                var row = this.mSketchTable.row(i);

                if (row instanceof wijmo.grid.SketchDataRow) {
                    (<wijmo.grid.SketchDataRow>row).originalRowIndex += delta;
                }
            }

            var rows = this._rows(),
                view = this._view();

            for (var i = rowStartIndex, len = rows.length(); i < len; i++) {
                var rowInfo = view._getRowInfo(rows.item(i));

                rowInfo.sectionRowIndex += delta;

                if (rowInfo.type & wijmo.grid.rowType.data) {
                    rowInfo.dataItemIndex += delta;
                    rowInfo.dataRowIndex += delta;
                    rowInfo.virtualDataItemIndex += delta;
                    rowInfo.sketchRowIndex += delta;
                }

                view._setRowInfo(rowInfo.$rows, rowInfo);
            }
        }

        _handleHierarchyCallback(entity: c1.gridview.IHierarchyCallbackEntity, params) {
            var foo: HTMLTableElement = <any>document.createElement("div");

            foo.innerHTML = "<table>" + entity.html + "</table>";
            foo = <HTMLTableElement>(foo.getElementsByTagName("table")[0]);

            var body = wijmo.grid.getTableSection(foo, wijmo.grid.rowScope.body),
                data = this._readTableSection(foo, wijmo.grid.rowScope.body, true),
                dataIndex = entity.rowIndex - this.mDataOffset,
                currentCell = this.currentCell()._clone();

            // after the new row will be inserted (or deleted) the current cell and selection object cells are become invalid. so clear them first.
            this._changeCurrentCell(null, new wijmo.grid.cellInfo(-1, -1, null, false, false), { changeSelection: true });

            var view = this._view(),
                leaves = this._leaves(),
                opts = this._instancesToOptions(leaves),
                dvw = this.mDataViewWrapper,
                row = view.bodyRows().find(entity.rowIndex, (row) => { return row.dataItemIndex === entity.rowIndex; }, false),
                sketchRowIndex = this.mSketchTable.findIndex(dataIndex, (row: wijmo.grid.SketchRow) => { return row.isDataRow() && (<wijmo.grid.SketchDataRow>row).originalRowIndex === dataIndex; });

            // remove rows
            for (var i = 0; i < (entity.expand ? 1 : 2); i++) {
                view._removeBodyRow(row.sectionRowIndex);
                this.mSketchTable.remove(sketchRowIndex);
                dvw._unsafeSplice(dataIndex, 1);
            }

            // parse
            dvw._parseOwnData(data, opts, body);

            // insert rows
            for (var i = 0; i < data.length; i++) {
                var sketchRow = this._buildSketchRow(dvw._wrapDataItem(data[i], dataIndex + i), leaves);

                dvw._unsafeSplice(dataIndex + i, 0, data[i]);
                this.mSketchTable.insert(sketchRowIndex + i, sketchRow);

                this.mInsertNewRow = true;
                try {
                    view._insertBodyRow(sketchRow, row.sectionRowIndex + i, row.dataItemIndex + i, row.virtualDataItemIndex + i, sketchRowIndex + i);
                } finally {
                    this.mInsertNewRow = false;
                }
            }

            if (entity.expand) {
                this._updateUnderlyingInfo(sketchRowIndex + 2, row.sectionRowIndex + 2, 1);
            } else {
                this._updateUnderlyingInfo(sketchRowIndex + 1, row.sectionRowIndex + 1, -1);
            }

            view._rebuildOffsets();
            view._ensureRenderBounds();
            this._rebuildRenderableRowsCollection();

            this._changeCurrentCell(null, currentCell, { changeSelection: true }); // restore current cell and selection
        }

        _handlePartialCallback(entity: c1.gridview.IPartialCallbackEntity, params) {
            var rowsAccessor = this._rows(),
                leaves = this._leaves(),
                opts = this._instancesToOptions(leaves),
                view = this._view(),
                allowVirtualScrolling = this._allowVVirtualScrolling(),
                dvw = this.mDataViewWrapper,
                sketchRow: wijmo.grid.SketchRow,
                invCulture = this._getDOMReaderCulture();

            // parse response, update data items						
            for (var i = 0; i <= 1; i++) {
                var rowEntity: c1.gridview.ICallbackRowEntity = i // starting from prevRow
                    ? entity.curRow
                    : entity.prevRow;

                if (rowEntity.rowIndex >= 0) {
                    // * build DOM from response *
                    //foo = $("<table>" + json.response[i + 1] + "</table>")[0]; // issues with the "checked" attribute

                    var dataIndex = rowEntity.rowIndex - this.mDataOffset;

                    if (dataIndex < 0) {
                        continue; // server-side scrolling?
                    }

                    var foo: HTMLTableElement = <any>document.createElement("div");
                    foo.innerHTML = "<table>" + rowEntity.html + "</table>";
                    foo = <HTMLTableElement>(foo.getElementsByTagName("table")[0]);

                    var rowData = this._readTableSection(foo, wijmo.grid.rowScope.body, true);

                    // * update underlying data *
                    var newDataItem = wijmo.grid.dataViewWrapper._parseDataItem(this, rowData[0], <any>foo.rows[0], opts, invCulture);
                    dvw._unsafeReplace(dataIndex, newDataItem); //  this.dataView().getSource() === newDataItem


                    var sketchRowIndex = this.mSketchTable.findIndex(dataIndex, (row: wijmo.grid.SketchRow) => {
                        return row.isDataRow() && (<wijmo.grid.SketchDataRow>row).originalRowIndex === dataIndex;
                    });

                    this.mSketchTable.replace(sketchRowIndex, sketchRow = this._buildSketchRow(dvw._wrapDataItem(newDataItem, dataIndex), leaves));
                    // update DOM * 

                    // find rowInfo by dataItemIndex
                    var rowInfo = view.bodyRows().find(0, (row: wijmo.grid.IRowInfo) => {
                        return row.dataItemIndex === dataIndex;
                    });

                    if (!rowInfo) {
                        if (!allowVirtualScrolling) { // do not raise exception when virtualScrolling is used - domRow is out of the view bounds.
                            throw "rowInfo is null";
                        }
                    } else {
                        view._removeBodyRow(rowInfo.sectionRowIndex); // remove old row

                        this.mInsertNewRow = true; // insert a new one
                        try {
                            rowInfo = view._insertBodyRow(sketchRow, rowInfo.sectionRowIndex, rowInfo.dataItemIndex, rowInfo.virtualDataItemIndex, sketchRowIndex);
                            //if (rowEntity.initializationScript) {
                            //	eval(rowEntity.initializationScript);
                            //}

                        } finally {
                            this.mInsertNewRow = false;
                        }
                    }
                }
            }

            dvw._refreshSilent();
        }

        _handleVirtualScrollingCallback(entity: c1.gridview.ICallbackEntity, param) {
            var foo = <Node>document.createElement("div"),
                leaves = this._leaves(),
                opts = this._instancesToOptions(leaves),
                rawData: any[],
                newData = [],
                invCulture = this._getDOMReaderCulture();

            (<HTMLElement>foo).innerHTML = "<table>" + entity.html + "</table>";
            foo = foo.firstChild; // foo.getElementsByTagName("table")[0];

            rawData = this._readTableSection(<HTMLTableElement>foo, wijmo.grid.rowScope.body, true); // get raw (unparsed) data

            // create an array of a new data items and parse.
            for (var i = 0; i < (<HTMLTableSectionElement>foo).rows.length; i++) {
                newData.push(wijmo.grid.dataViewWrapper._parseDataItem(this, rawData[i], (<any>foo).rows[i], opts, invCulture)); // parse
            }

            this.mInsertNewRow = true;
            try {
                this.mDataViewWrapper.makeDirty();

                // fallback to wijgrid with a new data items
                // Renew the selection. When server-side vertical virtual scrolling is used the sketchTable contains only currently rendered rows (there is no "detached" rows covering all available rows in this case), so the _onRendered->_ensureSelection() method call fails to renew selection properly.
                if (this._serverSideVirtualScrolling()) {
                    var selectionMode = this.options.selectionMode.toLowerCase();
                    if (selectionMode === "singlecolumn" || selectionMode === "multicolumn") {
                        var originalCompleteCallback = param.completeCallback;

                        param.completeCallback = () => {
                            if (originalCompleteCallback) {
                                originalCompleteCallback.apply(this, arguments);
                            }


                            this._updateSelection(null, wijmo.grid.cellRangeExtendMode.none);
                        };
                    }
                }
                super._onVirtualScrolling.call(this, param.newBounds, param.request, param.mode, param.scrollIndex, param.completeCallback, newData);

                //if (entity.initializationScript) { // edit row was rendered into view, initialize child controls
                //	eval(entity.initializationScript);
                //}
            }
            finally {
                this.mInsertNewRow = false;
            }
        }

        _instancesToOptions(instances: wijmo.grid.c1basefield[]): wijmo.grid.IColumn[] {
            var result: wijmo.grid.IColumn[] = [];

            $.each(instances, (_, value: wijmo.grid.c1basefield) => {
                if (!(value instanceof wijmo.grid.c1rowheaderfield)) {
                    result.push(<wijmo.grid.IColumn>value.options);
                }
            });

            return result;
        }

        _beginEditInternal(e): boolean {
            var row = this.currentCell().row();
            if (row && (row.type & wijmo.grid.rowType.dataDetail)) {
                return false; // a cell contains child grid.
            }

            var success = super._beginEditInternal.apply(this, arguments);

            if (success) {
                this._batchValues().addValue(this.currentCell());
            }

            return success;
        }

        _endEditInternal(e): boolean {
            var success = super._endEditInternal.apply(this, arguments);

            if (success) {
                this._batchValues().addValue(this.currentCell());
            }

            return success;
        }

        /// <summary>
        /// Sends edits done by user to server in client editing mode.
        /// </summary>
        /// <remarks>
        /// Works only if the <see cref="allowClientEditing"/> option is set to True.
        /// </remarks>
        update() {
            if (this._allowCellEditing() && (this.endEdit() !== false)) {
                var batchValues = this._batchValues().simplify(),
                    leaves = this._leaves(),
                    data = this.data();

                this.mBatchValues = null; // clear

                if (batchValues) {
                    var values = {};

                    for (var key in batchValues) {
                        if (batchValues.hasOwnProperty(key)) {

                            values[key] = {};

                            $.each(leaves, (_, leaf) => {
                                if (leaf instanceof c1.gridview.c1gridviewboundfield) {
                                    var boundField = <c1.gridview.c1gridviewboundfield>leaf,
                                        opt = boundField.options;

                                    if (!opt.readOnly && wijmo.grid.validDataKey(opt.dataField)) {
                                        if (batchValues[key][opt.dataField]) { // value is changed
                                            values[key][opt.dataField] = batchValues[key][opt.dataField];
                                        } else {
                                            values[key][opt.dataField] = [data[key][opt.dataKey], data[key][opt.dataKey]];
                                        }
                                    }
                                }
                            });
                        }
                    }

                    this._onBatchUpdate({ value: values }); // sends all changed row's values (to enable automatic updating)
                }
            }
        }

        doFilter(headerText: string, filterExpression: any) {
            var column: c1.gridview.c1gridviewtemplatefield,
                opt: wijmo.grid.IColumn,
                args: wijmo.grid.IFilteringEventArgs,
                cols = this.columns().filter(function (col) {
                    return col instanceof c1.gridview.c1gridviewtemplatefield && col.options.headerText == headerText;
                });
            if (cols.length > 0) {
                column = <c1.gridview.c1gridviewtemplatefield>cols[0];
                column.options.filterExpression = filterExpression;
                opt = <wijmo.grid.IColumn>(<wijmo.grid.c1field>column).options;
                args = { column: opt, operator: "contains", value: "" };
                this._onColumnFiltering(args);
            }
        }

        _onVirtualScrolling(newBounds: wijmo.grid.IRenderBounds, request: wijmo.grid.IVirtualScrollingRequest, mode: wijmo.grid.intersectionMode, scrollIndex: number, completeCallback) {
            if (request !== null) {
                this._doCallback(["vscroll", request.index + "", request.maxCount + ""], { newBounds: newBounds, request: request, mode: mode, scrollIndex: scrollIndex, completeCallback: completeCallback }); // request a new data page.
            } else {
                this.mInsertNewRow = true;
                try {
                    super._onVirtualScrolling.apply(this, arguments);
                } finally {
                    this.mInsertNewRow = false;
                }
            }
        }

        _totalRowsCount(): number {
            if (this._serverSideVirtualScrolling()) {
                return this.options.totalRows;
            }

            return super._totalRowsCount.apply(this, arguments);
        }

        _trackScrollingPosition(x, y) {
            super._trackScrollingPosition.apply(this, arguments);

            this.options.innerState.scrollingState = this.mScrollingState;
        }

        _trackScrollingIndex(index) {
            super._trackScrollingIndex.apply(this, arguments);

            this.options.innerState.scrollingState = this.mScrollingState;
        }

        doRefresh(userData?: wijmo.grid.IUserDataArgs): void {
            if (userData) {
                super.doRefresh(userData);
            } else {
                this._doCallback(["refresh"]);
            }
        }
    }

    c1gridview.prototype.widgetEventPrefix = "c1gridview";

    /** @ignore */
    export interface IC1GridViewInnerState {
        _cid: string; // clientID
        _uid: string; // uniqueID
        _cbEditRowInitScript: string;
        _dc?: string; // aspNetDisbledClass (ASP.NET4 only)
        _hd: boolean; // has details
        _root: boolean; // indicates the tompost grid in the hierarchy.
        autogeneratedColumns: wijmo.grid.IColumn[];
        _culture: string;
        _filterDisplayNames: { name: string; displayName: string; }[];
        scrollingState: wijmo.grid.IScrollingState;
        _bodyEditIndex: number; // The options.editIndex property mapped over the server-side body rows.
        _bodySelectedIndex: number; // The options.selectedIndex property mapped over the server-side body rows.
        _c1InplaceEditorsMarkup: { [index: string]: string }; // A collection of { column.uid, html } pairs. Contains html markup of C1Input controls of the row being edited, available only if client-side virtual scrolling is used.
        _pbRef: string;
    }

    /** @ignore */
    export interface IC1GridViewPagerSettings extends wijmo.grid.IPagerSettings {
        visible: boolean;
    }

    /** @ignore */
    export interface IC1GridViewOptions extends wijmo.grid.IWijgridOptions {
        allowCustomContent: boolean;
        allowC1InputEditors: boolean;
        autoGenerateDeleteButton: boolean;
        autoGenerateEditButton: boolean;
        autoGenerateSelectButton: boolean;
        autoGenerateFilterButton: boolean;
        callbackSettings: { action: string; mode: string; };
        clientEditingUpdateMode: string;
        editIndex: number;
        filterSettings: { filterOnSelect: boolean; mode: string; };
        innerState: IC1GridViewInnerState;
        pageCount: number;
        pagerSettings: IC1GridViewPagerSettings; // extend
        selectedIndex: number;
        showHeader: boolean;

        // server-side "virtual" properties
        allowClientEditing: boolean; // editingMode
        clientSelectionMode: string; // selectionMode
        showClientSelectionOnRender: boolean; // showSelectionOnRender

        batchValues: any;
    }

    c1gridview.prototype.options = <any>$.extend(true, {}, $.wijmo.wijgrid.prototype.options, {
        _htmlTrimMethod: wijmo.grid.trimMethod.control, // leave spaces, trim control characters only (#94176).

        columnsAutogenerationMode: "none",
        allowCustomContent: true,
        allowC1InputEditors: false,
        autoGenerateDeleteButton: false,
        autoGenerateEditButton: false,
        autoGenerateSelectButton: false,
        autoGenerateFilterButton: false,

        callbackSettings: {
            action: "none",
            mode: "partial"
        },

        clientEditingUpdateMode: "autoRowEdit",

        filterSettings: {
            filterOnSelect: true,
            mode: "auto"
        },

        innerState: {
        },

        pagerSettings: {
            visible: true // add new option to the pagerSettings
        },

        selectionMode: "none",
        selectedIndex: -1,

        showHeader: true,

        readAttributesFromData: true // to support virtual scrolling.
    });

    $.wijmo.registerWidget("c1gridview", $.wijmo.wijgrid, c1gridview.prototype);
}

module c1.gridview {


    var $ = jQuery;

    export enum callbackAction {
        /// <summary>
        /// All actions are performed via postback.
        /// </summary>
        none = 0x0,

        /// <summary>
        /// Column moving is performed via callbacks.
        /// </summary>
        colMove = 0x01,

        /// <summary>
        /// Editing is performed via callbacks.
        /// </summary>
        editing = 0x02,

        /// <summary>
        /// Paging is performed via callbacks.
        /// </summary>
        paging = 0x08,

        /// <summary>
        /// Row selection is performed via callbacks.
        /// </summary>
        selection = 0x10,

        /// <summary>
        /// Sorting is performed via callbacks.
        /// </summary>
        sorting = 0x20,

        /// <summary>
        /// Filtering is performed via callbacks.
        /// </summary>
        filtering = 0x80,

        /// <summary>
        /// Grouping is performed via callbacks.
        /// </summary>
        grouping = 0x100,

        /// <summary>
        /// Scrolling is performed via callbacks.
        /// </summary>
        scrolling = 0x200,

        hierarchy = 0x400,

        /// <summary>
        /// Refresh is performed via callbacks.
        /// </summary>
        refresh = 0x800,

        /// <summary>
        /// All actions are performed via callbacks.
        /// </summary>
        all = 0x01 | 0x02 | 0x08 | 0x10 | 0x20 | 0x80 | 0x100 | 0x200 | 0x400 | 0x800
    }

    /** @ignore */
    export class batchValues {
        private _gridView: c1.c1gridview;
        private _rows = {};

        constructor(gridView: c1.c1gridview) {
            this._gridView = gridView;
        }

        addValue(cellInfo: wijmo.grid.cellInfo) {
            if (cellInfo && cellInfo._isValid()) {
                var value = cellInfo.value(),
                    rowKey = cellInfo.rowIndex(),
                    cellKey = cellInfo.cellIndex(),
                    rowInfo;

                if (rowInfo = this._rows[rowKey]) {
                    if (!rowInfo[cellKey]) { // new value
                        rowInfo[cellKey] = {
                            initValue: value
                        };
                    }
                } else { // new row, new value
                    rowInfo = this._rows[rowKey] = {};
                    rowInfo[cellKey] = {
                        initValue: value
                    };
                }
            } // if
        }

        simplify() {
            var rows = {};

            for (var rowKey in this._rows) {
                if (this._rows.hasOwnProperty(rowKey)) {
                    var row = this._rows[rowKey],
                        dataItemIndex = -1,
                        values = {};

                    for (var cellKey in row) {
                        var cell = row[cellKey];
                        if (row.hasOwnProperty(cellKey) && (!wijmo.grid.isNaN(cell.initValue))) {
                            var cellInfo = new wijmo.grid.cellInfo(parseInt(cellKey, 10), parseInt(rowKey, 10));

                            cellInfo._setGridView(this._gridView);

                            if (dataItemIndex < 0) {
                                //TFS-430737:
                                dataItemIndex = cellInfo.row().virtualDataItemIndex;
                                if (dataItemIndex < 0)
                                    dataItemIndex = cellInfo.row().dataItemIndex;
							              }

                            var actualDataValue = cellInfo.value();
                            if (cell.initValue !== actualDataValue) {
                                var opt = <c1.gridview.IC1GridViewBoundFieldOptions><any>cellInfo.column();
                                values[opt.dataField] = [cell.initValue, actualDataValue];
                            }
                        }
                    }

                    if (!$.isEmptyObject(values) && dataItemIndex >= 0) {
                        rows[dataItemIndex] = values;
                    }
                }
            }

            return ($.isEmptyObject(rows))
                ? null
                : rows;
        }
    }

    /** @ignore */
    export class enumConverter {
        static fromString(enumObj: any, value: string): number {
            var values = value.split(","),
                success = true,
                result = 0;

            for (var i = 0, len = values.length; i < len && success; i++) {
                var cc = this._camelCase($.trim(values[i])),
                    test: number;

                if (success = ((test = enumObj[cc]) !== undefined)) {
                    result |= test;
                }
            }

            return success ? result : undefined;
        }

        static toString(enumObj: any, value: number): string {
            if (enumObj[value] !== undefined) {
                return enumObj[value];
            }

            var key,
                result: string[] = [];

            for (key in enumObj) {
                if (enumObj.hasOwnProperty(key)) {
                    var numKey = parseInt(key, 10);

                    if (numKey !== undefined) {
                        if (value & numKey) {
                            value &= ~numKey;
                            result.push(enumObj[key]);
                        }
                    }
                }
            }

            return (result.length && value === 0)
                ? result.join(", ")
                : undefined;
        }

        private static _camelCase(value: string): string {
            return (value && value.length)
                ? value.substring(0, 1).toLowerCase() + value.substring(1)
                : value;
        }
    }

    // dateTime utilites *
    /** @ignore */
    export function truncDateTime(value: Date, dataFormatString: string): Date {
        var timeUnits = wijmo.grid.TimeUnitConverter.convertFormatString(dataFormatString || "d");
        value = wijmo.grid.TimeUnitConverter.cutDate(value, timeUnits);

        return value;
    }

    /** @ignore */
    export function dateToInvariantString(date): string {
        if (date) {
            return date.format("MM/dd/yyyy");
        }

        return date;
    }
    // * dateTime utilites
}

module c1.gridview {


    var $ = jQuery;

    /** @ignore */
    export interface ICallbackContext {
        root: c1.c1gridview; // a topmost grid in a hierarchy, callback event target
        source: c1.c1gridview; // a grid triggered callback
        param: any; // callback parameters
    }

    /** @ignore */
    export interface ICallbackEntityMarker {
    }

    /* mapped from server-side */
    /** @ignore */
    export interface ICallbackEntity extends ICallbackEntityMarker {
        html: string;
        initializationScript: string;
    }

    /* mapped from server-side */
    /** @ignore */
    export interface ICallbackRowEntity extends ICallbackEntity {
        rowIndex: number;
    }

    /* mapped from server-side */
    /** @ignore */
    export interface IPartialCallbackEntity extends ICallbackEntityMarker {
        prevRow: ICallbackRowEntity;
        curRow: ICallbackRowEntity;
    }

    /* mapped from server-side */
    /** @ignore */
    export interface IHierarchyCallbackEntity extends ICallbackEntity {
        path: number[];
        //dataItemIndex: number;
        rowIndex: number;
        expand: boolean;
    }

    /* mapped from server-side */
    /** @ignore */
    export interface ICallbackResponse {
        command: string;
        mode: string;
        entity: ICallbackEntityMarker;
        options: IC1GridViewOptions;
        rootInnerState: IC1GridViewInnerState;
    }

    /** @ignore */
    export class cbp {
        constructor(gridID: string, argument, contextParam) {
            var source = this.getGridInstance(jQuery("#" + gridID));

            if (!source) {
                throw "invalid id";
            }

            var root = source._getRoot();

            if (!source._canInteract() || !root._canInteract()) {
                return;
            }

            var context: ICallbackContext = {
                source: source,
                root: root,
                param: contextParam
            };

            this.callbackInProggress(context, true);

            argument = this.prepareArgument(argument, root, source);

            var prmi: Sys.WebForms.PageRequestManager,
                doCallback = () => {
                    // root grid is ALWAYS acts as an eventTarget.
                    WebForm_DoCallback(root.options.innerState._uid, argument, $.proxy(this.eventCallback, this), context, $.proxy(this.errorCallback, this), true);
                };

            // An async postback is currently running by PageRequestManager. Wait until it will be completed otherwise the WebForm_DoCallback() function call will be omitted (#112510).
            if ((typeof (Sys) !== "undefined") && Sys.WebForms && Sys.WebForms.PageRequestManager && (prmi = Sys.WebForms.PageRequestManager.getInstance()) && prmi.get_isInAsyncPostBack()) {
                var endRequestHandler = $.proxy(() => {
                    prmi.remove_endRequest(endRequestHandler);
                    doCallback.call(this);  // and run
                }, this);

                prmi.add_endRequest(endRequestHandler);
            } else {
                doCallback.call(this); // run immediately
            }
        }

        // { command, mode, options, response }
        private eventCallback(value, context: ICallbackContext) {

            try {
                var response: c1.gridview.ICallbackResponse = jQuery.parseJSON(value);

                c1common.updateWebFormsInternals(context.source.options.innerState._cid, response.options); // response.options is a string here

                if ((context.root !== context.source) && response.rootInnerState) {
                    context.root.options.innerState = jQuery.parseJSON(<string><any>response.rootInnerState); // do not updateWebFormsInternals here. All callbacks are targeted to the root so _doCallback will call it automatically.
                }

                response.options = this.adjustFuncOptions(context, jQuery.parseJSON(<string><any>response.options));

                var view = context.source._view();
                if (view && (view instanceof wijmo.grid.fixedView)) {
                    (<wijmo.grid.fixedView>view).resetRowsHeightsCache();
                }

                if (response.mode === "partial") {
                    switch (response.command) {
                        case "vscroll":
                            this.handleVirtualScrollingCallback(response, context);
                            break;

                        case "hierarchy":
                            this.handleHierarchyCallback(response, context);
                            break;

                        default:
                            this.handlePartialCallback(response, context);
                            break;

                    }
                } else {
                    this.handleFullCallback(response, context);
                }
            }
            finally {
                this.callbackInProggress(context, false);
            }
        }

        private errorCallback(value, context: ICallbackContext) {
            try {
                alert(value);
            }
            finally {
                this.callbackInProggress(context, false);
            }
        }

        private handlePartialCallback(response: c1.gridview.ICallbackResponse, context: ICallbackContext) {
            var grid = context.source;

            grid.options.batchValues = null;

            if (response.command === "batch") {
                return;
            }

            grid.options.editIndex = response.options.editIndex;
            grid.options.selectedIndex = response.options.selectedIndex;
            grid.options.innerState = response.options.innerState;

            try {
                grid.mPartialCallback = true;
                grid._handlePartialCallback(<c1.gridview.IPartialCallbackEntity>response.entity, context.param);
            }
            finally {
                grid.mPartialCallback = false;
            }
        }

        private handleFullCallback(response: c1.gridview.ICallbackResponse, context: ICallbackContext) {
            var grid = context.source,
                $parentDiv = grid.element.closest("#" + context.source.options.innerState._cid + "_div"),
                entity: c1.gridview.ICallbackEntity = <any>response.entity;

            $parentDiv
                .empty() // destroy old widget
                .html(entity.html) // set inner html

            if (entity.initializationScript) { // initialize child widgets
                eval(entity.initializationScript);
            }

            var table = $parentDiv.find("> table"),
                options = wijmoASPNetParseOptions(response.options);

            // create new widget
            if (grid instanceof $.wijmo.c1detailgridview) {
                table.c1detailgridview(options);

                // restore parent
                var parent = grid._parent();
                if (parent) {
                    parent._resetDetailGrids();

                    // recalculate sizes starting from parent
                    do {
                        parent._setSizeWithoutDetails();
                    }
                    while (parent = parent._parent());
                }
            } else {
                table.c1gridview(options);
            }
        }

        private handleHierarchyCallback(response: c1.gridview.ICallbackResponse, context: ICallbackContext) {
            var grid = context.source,
                entity: c1.gridview.IHierarchyCallbackEntity = <any>response.entity;

            try {
                grid.options.innerState = response.options.innerState; // store tree hierarachy state between callbacks 

                grid.mPartialCallback = true;
                grid._handleHierarchyCallback(entity, context.param);
                eval(entity.initializationScript);

                grid._resetDetailGrids();

                // recalculate sizes
                var current = grid;
                do {
                    current._setSizeWithoutDetails();
                } while (current = current._parent())
            } finally {
                grid.mPartialCallback = false;
            }
        }

        private handleVirtualScrollingCallback(response: c1.gridview.ICallbackResponse, context: ICallbackContext) {
            var grid = context.source,
                entity: c1.gridview.ICallbackEntity = <any>response.entity;

            grid.options.pageIndex = response.options.pageIndex;
            grid.options.selectedIndex = response.options.selectedIndex;
            grid.options.editIndex = response.options.editIndex;
            grid.options.innerState = response.options.innerState;

            try {
                grid.mPartialCallback = true;
                grid._handleVirtualScrollingCallback(entity, context.param);
            } finally {
                grid.mPartialCallback = false;
            }
        }

        private adjustFuncOptions(context: ICallbackContext, options) {
            var test,
                grid = context.source;

            if (options && grid && grid._funcOptions && ($.isArray(test = grid._funcOptions()))) {
                for (var key in options) {
                    if (options.hasOwnProperty(key)) {
                        if (($.inArray(key, test) !== -1) && (typeof (options[key]) === "string")) {
                            options[key] = window[options[key]] || options[key];
                        }
                    }
                }
            }

            return options;
        }

        private callbackInProggress(context: ICallbackContext, value: boolean): void {
            context.source.mCallbackInProgress = value;
            context.root.mCallbackInProgress = value;

            if (value) {
                context.source._activateSpinner();
            } else {
                context.source._deactivateSpinner();
            }
        }

        private prepareArgument(argument: string, root: c1gridview, source: c1gridview): string {
            var extraArument = source._onBeforeCallback(argument);

            if (extraArument) {
                argument += ":" + extraArument;
            }

            this.saveSettings(root);

            if (source !== root) { // callback from the detail (inner) grid
                this.saveSettings(source);

                argument = "#" + c1.gridview.stringUtils.encodeValues([
                    argument, // original argument
                    "p" + source._getFullPath(), // hierarchy path of the source grid
                    __JSONC1.stringify(source.onwijstatesaving())
                ]);
            }

            return argument;
        }

        private saveSettings(grid: c1gridview): void {
            (<any>grid.element).wijSaveState(grid.widgetName, "onwijstatesaving");
            c1common.updateWebFormsInternals(grid.options.innerState._cid, __JSONC1.stringify(grid.onwijstatesaving()));
        }

        private getGridInstance(element: JQuery): c1.c1gridview {
            if (element && element.length) {
                return element.data("wijmo-c1gridview") || element.data("wijmo-c1detailgridview");
            }

            return null;
        }
    }

    /** @ignore */
    export class stringUtils {
        public static encodeValues(values: string[]): string {
            var res = "";

            if (values) {
                for (var i = 0; i < values.length; i++) {
                    res += values[i].length + "," + values[i];
                }
            }

            return res
        }

        public static decodeValue(value: string): string[] {
            var res = [];

            while (value) {
                var delim = value.indexOf(","),
                    len = parseInt(value.substr(0, delim), 10);

                res.push(value.substr(delim + 1, len));

                value = value.substr(delim + 1 + len);
            }

            return res;
        }
    }
}

module c1.gridview {
    /** @ignore */
    export class SketchHierarchyDetailRow extends wijmo.grid.SketchDataRow {
        constructor(originalRowIndex: number, renderState: wijmo.grid.renderState, attrs) {
            super(originalRowIndex, renderState, attrs);
            this.rowType = wijmo.grid.rowType.data | wijmo.grid.rowType.dataDetail; // no alternating rows
        }
    }
}

// columns
module c1.gridview {


    var $ = jQuery;

    // @ignore */
    export interface IC1GridViewFieldInnerState {
        // c1basefield
        _uid: number;
        oi: number;
        clientType: string;

        // c1field
        _resolvedHeaderImageUrl: string;

        // c1boundfield
        newFilterValue: any;
        newFilterOperator: string;

        // c1templatefield
        _htmpl: boolean;
        _itmpl: boolean;
        _ftmpl: boolean;
    }

    function getHeaderContent(column: wijmo.grid.c1basefield) {
        var innerState = <IC1GridViewFieldInnerState>(<any>column.options).innerState;

        return (innerState && innerState._resolvedHeaderImageUrl)
            ? "<img src=\"" + innerState._resolvedHeaderImageUrl + "\" />"
            : column.options.headerText || "&nbsp;";
    }

    //#region c1gridviewband
    /** @ignore */
    export class c1gridviewbandfield extends wijmo.grid.c1bandfield {
        public options: IC1GridViewBandFieldOptions;

        _getHeaderContent(): string {
            return getHeaderContent(this);
        }
    }

    /** @ignore */
    export interface IC1GridViewBandFieldOptions extends wijmo.grid.IC1BandFieldOptions {
        innerState?: IC1GridViewFieldInnerState;
    }

    /** @ignore */
    export class c1gridviewbandfield_options extends wijmo.grid.c1bandfield_options implements IC1GridViewBandFieldOptions {
    }

    c1gridviewbandfield.prototype.options = wijmo.grid.extendWidgetOptions(wijmo.grid.c1bandfield.prototype.options, new c1gridviewbandfield_options());
    //#endregion

    //#region c1gridviewtableboundfield
    /** @ignore */
    export class c1gridviewtableboundfield extends wijmo.grid.c1field {
        public options: IC1GridViewTableBoundFieldOptions;
        private mWrappedField: wijmo.grid.c1field;

        constructor(wijgrid: wijmo.grid.wijgrid, options: wijmo.grid.IColumn, widgetName?: string) {
            super(wijgrid, options, widgetName);

            this.mWrappedField = (options && options.dataType === "boolean")
                ? new wijmo.grid.c1booleanfield(wijgrid, options, widgetName)
                : new wijmo.grid.c1field(wijgrid, options, widgetName);
        }

        _provideDefaults() {
            super._provideDefaults.apply(this, arguments);
            this.options.ensurePxWidth = true;
        }

        _setupDataCell(container: JQuery, formattedValue, row: wijmo.grid.IRowInfo) {
            var grid = <c1.c1gridview>this._owner(),
                editIndex = grid.options.innerState._bodyEditIndex;

            // сheck renderCounter because original cells are becomes empty after the first rendering
            if (!grid.mRenderCounter && (row.state === wijmo.grid.renderState.rendering)) {
                if (row.type & wijmo.grid.rowType.dataDetail) {
                    return; // Intercept changing of the cell content
                } else {
                    if ((editIndex >= 0) && (this.options.readOnly === false) && !grid._processingPartialCallback()) {
                        if (row.dataItemIndex + grid.mDataOffset === editIndex) {
                            return; // Intercept changing of the cell content
                        }
                    }
                }
            }

            //super._setupDataCell.apply(this, arguments);
            this.mWrappedField._setupDataCell.apply(this, arguments);
        }

        _cellRendered(cell: JQuery, container: JQuery, row: wijmo.grid.IRowInfo) {
            this.mWrappedField._cellRendered.apply(this, arguments); // to enable single-click behaviour for c1checkboxfield.
        }

        _getHeaderContent(): string {
            return getHeaderContent(this);
        }

        _canDropTo(column: wijmo.grid.c1basefield): boolean {
            return super._canDropTo.apply(this, arguments) && !column.options._dynamic; // field can't be dropped onto the autogenerated fields
        }

        _isSortable(): boolean {
            return super._isSortable.apply(this, arguments) && (this.options.sortExpression !== undefined); // only user-defined columns with the sortExpression property explicitly defined can be sorted.
        }

        _needToEncodeValue(row: wijmo.grid.IRowInfo): boolean {
            var grid = <c1.c1gridview>this._owner();

            if (grid) {
                if (grid._processingPartialCallback()) {
                    return false; // in this mode we are updating DOM with the raw HTML markup.
                }

                if (!this.options.readOnly && grid._isServerSideEditRow(row)) {
                    return false; // suppose that corresponding cell contains server-side editor, so value consists of html markup.
                }
            }

            return super._needToEncodeValue.apply(this, arguments);
        }

        _postset_width(value, oldValue) {
            var grid = <c1detailgridview>this._owner(), // fake, compile-time casting
                flag = grid instanceof $.wijmo.c1detailgridview;

            try {
                if (flag) {
                    grid._ignoreSizing(false);
                }
                super._postset_width.apply(this, arguments);
            }
            finally {
                if (flag) {
                    grid._ignoreSizing(true);
                }
            }
        }
    }

    /** @ignore */
    export interface IC1GridViewTableBoundFieldOptions extends wijmo.grid.IC1FieldOptions {
        innerState?: IC1GridViewFieldInnerState;
        sortExpression?: string;
    }

    /** @ignore */
    export class c1gridviewtableboundfield_options extends wijmo.grid.c1field_options implements IC1GridViewTableBoundFieldOptions {
    }

    c1gridviewtableboundfield.prototype.options = wijmo.grid.extendWidgetOptions(wijmo.grid.c1field.prototype.options, new c1gridviewtableboundfield_options());
    //#endregion

    //#region c1gridviewboundfield
    /** @ignore */
    export class c1gridviewboundfield extends c1gridviewtableboundfield {
        public options: IC1GridViewBoundFieldOptions;

        constructor(wijgrid: wijmo.grid.wijgrid, options: wijmo.grid.IColumn, widgetName?: string) {
            super(wijgrid, options, widgetName);

            options.encodeHtml = this.options.htmlEncode;
        }

        _provideDefaults() {
            super._provideDefaults.apply(this, arguments);
            wijmo.grid.shallowMerge(this.options, c1gridviewboundfield.prototype.options);
        }

        _onFilterEditorValueChanged(args: { value: any; }) {
            if (args.value instanceof Date) {
                args.value = c1.gridview.truncDateTime(args.value, this.options.dataFormatString);
            }

            this.options.innerState.newFilterValue = args.value;
        }
    }

    /** @ignore */
    export interface IC1GridViewBoundFieldOptions extends IC1GridViewTableBoundFieldOptions {
        applyFormatInEditMode: boolean;
        convertEmptyStringToNull: boolean;
        dataField: string;
        htmlEncode: boolean;
    }
    /** @ignore */
    export interface IC1GridViewTemplateFieldOptions extends IC1GridViewBoundFieldOptions {
        filterExpression: string;
    }


    /** @ignore */
    export class c1gridviewboundfield_options extends c1gridviewtableboundfield_options implements IC1GridViewBoundFieldOptions {
        applyFormatInEditMode = false;
        dataField = undefined;
        convertEmptyStringToNull = true;
        htmlEncode = true;
    }
    /** @ignore */
    export class c1gridviewtemplatefield_options extends c1gridviewboundfield_options implements IC1GridViewTemplateFieldOptions {
        filterExpression = "";
    }


    c1gridviewboundfield.prototype.options = wijmo.grid.extendWidgetOptions(c1gridviewtableboundfield.prototype.options, new c1gridviewboundfield_options());
    //#endregion

    /** @ignore */
    export class c1gridviewautogeneratedfield extends c1gridviewboundfield {

        _provideDefaults() {
            super._provideDefaults.apply(this, arguments);
            this.options._dynamic = true;
            this.options.showFilter = false;
        }

        _isSortable(): boolean {
            return true; // autogenerated fields are always sortable
        }

        _canDrag(): boolean {
            return false;
        }
    }

    /** @ignore */
    export class c1gridviewcheckboxfield extends c1gridviewboundfield {
        _provideDefaults() {
            super._provideDefaults.apply(this, arguments);

            this.options.applyFormatInEditMode = false;
            this.options.convertEmptyStringToNull = false;
            this.options.htmlEncode = false;
        }

        _setupDataCell(container: JQuery, formattedValue, row: wijmo.grid.IRowInfo) {
            return; // Don't fallback to the wrappedField implementation because content of the cell will be overwritten later with the original HTML (speed optimization).
        }
    }

    /** @ignore */
    export class c1gridviewtemplatefield extends c1gridviewtableboundfield {
        public options: IC1GridViewTemplateFieldOptions;
        _setupHeaderCell() {
            if (!this.options.innerState._htmpl) { // Intercept changing of the cell content
                super._setupHeaderCell.apply(this, arguments);
            }
        }

        _setupDataCell(container: JQuery, formattedValue, rowInfo: wijmo.grid.IRowInfo) {
            if ((rowInfo.type & wijmo.grid.rowType.dataDetail) || !this.options.innerState._itmpl) { // Intercept changing of the cell content
                super._setupDataCell.apply(this, arguments);
            }
        }

        _setupFooterCell(container: JQuery) {
            if (!this.options.innerState._ftmpl) { // Intercept changing of the cell content
                super._setupFooterCell.apply(this, arguments);
            }
        }

        _onFilterEditorValueChanged(args: { value: any; }) {
            if (args.value instanceof Date) {
                args.value = c1.gridview.truncDateTime(args.value, this.options.dataFormatString);
            }

            this.options.innerState.newFilterValue = args.value;
        }
    }
    c1gridviewtemplatefield.prototype.options = wijmo.grid.extendWidgetOptions(c1gridviewboundfield.prototype.options, new c1gridviewtemplatefield_options());

    /** @ignore */
    export class c1gridviewhyperlinkfield extends c1gridviewtableboundfield {
    }

    /** @ignore */
    export class c1gridviewimagefield extends c1gridviewtableboundfield {
    }

    /** @ignore */
    export class c1gridviewbuttonfield extends c1gridviewtableboundfield {
    }

    /** @ignore */
    export class c1gridviewcommandfield extends c1gridviewtableboundfield {
        _setupFilterCell(container: JQuery) {
            if (this.options.dataIndex >= 0) { // dataIndex is the index of the filter cell in original table
                var grid = this._owner(),
                    tHead = grid._tHead();

                if (tHead && tHead.length) {
                    container.html(tHead[tHead.length - 1][this.options.dataIndex]);
                }
                // Intercept changing of the cell content (A "Filter" command button).
            }
        }
    }

    export class c1detailheaderfield extends wijmo.grid.c1rowheaderfield {
        /** @ignore */
        public static CLIENT_TYPE = "c1detailheaderfield";
        /** @ignore */
        public static WIDGET_NAME = "wijmo.c1detailheaderfield";

        /** @ignore */
        public static getInitOptions(): wijmo.grid.IColumn {
            var opts = wijmo.grid.c1rowheaderfield.getInitOptions();
            opts._clientType = c1detailheaderfield.CLIENT_TYPE;
            return opts;
        }

        public static test(column: wijmo.grid.IColumn): boolean {
            return column._clientType === c1detailheaderfield.CLIENT_TYPE;
        }

        constructor(wijgrid: wijmo.grid.wijgrid, options: any, widgetName?: string) {
            super(wijgrid, options, widgetName || c1detailheaderfield.WIDGET_NAME);
        }

        _setupDataCell(container: JQuery, formattedValue, row: wijmo.grid.IRowInfo) {

            if (row.type & wijmo.grid.rowType.dataDetail) {
                this._updateHTML(row, container, "&nbsp;");
            } else {
                var html = this._getToggleButtonHtml((row._extInfo.state & wijmo.grid.renderStateEx.collapsed) != 0);

                this._updateHTML(row, container, html);

                container.find("." + this._defCSS().groupToggleVisibilityButton).click((e: JQueryEventObject) => {
                    (<c1.c1gridview>this._owner())._onToggleHierarchy(e, row);
                });
            }
        }
    }

    wijmo.grid.registerColumnFactory((grid, column) => {
        var instance: wijmo.grid.c1basefield = null;

        switch (column._clientType) {
            case "c1band":
                column.showFilter = false;
                column.readOnly = true;
                instance = new c1gridviewbandfield(grid, column);
                break;

            case "c1boundfield":
                instance = new c1gridviewboundfield(grid, column);
                break;

            case "c1hyperlinkfield":
                column.showFilter = false;
                column.readOnly = true;
                instance = new c1gridviewhyperlinkfield(grid, column);
                break;

            case "c1imagefield":
                column.showFilter = false;
                column.readOnly = true;
                instance = new c1gridviewimagefield(grid, column);
                break;

            case "c1templatefield":
                column.showFilter = column["dataField"] ? column.showFilter : false;
                column.readOnly = true;
                instance = new c1gridviewtemplatefield(grid, column);
                break;

            case "c1buttonfield":
                column.showFilter = false;
                column.readOnly = true;
                instance = new c1gridviewbuttonfield(grid, column);
                break;

            case "c1autogeneratedfield":
                instance = new c1gridviewautogeneratedfield(grid, column);
                break;

            case "c1checkboxfield":
                instance = new c1gridviewcheckboxfield(grid, column);
                break;

            case "c1commandfield":
                column.allowSort = false;
                column.showFilter = false;
                column.readOnly = true;

                if (column._dynamic) {
                    column.allowMoving = false;
                }

                instance = new c1gridviewcommandfield(grid, column);
                break;

            case "c1detailheaderfield":
                instance = new c1detailheaderfield(grid, column);
                break;
        }

        return instance;
    });

    if (wijmo.exporter && wijmo.exporter.exportContext) {
        var exportContextPrototype = wijmo.exporter.exportContext.prototype,
            wijIsCheckBoxCol = exportContextPrototype.isCheckBoxCol,
            wijIsHtmlCol = exportContextPrototype.isHtmlCol,
            wijIsImageCol = exportContextPrototype.isImageCol,
            wijIsLinkCol = exportContextPrototype.isLinkCol,
            gridExporter = wijmo.exporter.gridExporter,
            originCreateGridWithExport = gridExporter.createGridWithExport;

        exportContextPrototype.isCheckBoxCol = function (col: wijmo.grid.c1basefield): boolean {
            return col instanceof c1gridviewcheckboxfield || wijIsCheckBoxCol.apply(this, arguments);
        }

        exportContextPrototype.isHtmlCol = function (col: wijmo.grid.c1basefield): boolean {
            return col instanceof c1gridviewtemplatefield || wijIsHtmlCol.apply(this, arguments);
        }

        exportContextPrototype.isImageCol = function (col: wijmo.grid.c1basefield): boolean {
            return col instanceof c1gridviewimagefield || wijIsImageCol.apply(this, arguments);
        }

        exportContextPrototype.isLinkCol = function (col: wijmo.grid.c1basefield): boolean {
            return col instanceof c1gridviewhyperlinkfield || col instanceof c1gridviewcommandfield
                || col instanceof c1gridviewbuttonfield || wijIsLinkCol.apply(this, arguments);
        }

        if (originCreateGridWithExport) {
            gridExporter.createGridWithExport =
                (setting: wijmo.exporter.GridExportSetting, newOptions, exportAction: (setting: wijmo.exporter.GridExportSetting) => void): void => {
                    if (setting.grid instanceof wijmo.grid.wijgrid) {
                        originCreateGridWithExport(setting, newOptions, exportAction);
                        return;
                    }

                    var grid = <c1.c1gridview>setting.grid,
                        $gridElement = $(grid.element),
                        id = $gridElement.attr("id"),
                        oldOptions = grid.options,
                        cloneElement = $element => $($element[0].outerHTML).removeAttr("id").empty(),
                        container = cloneElement(grid.element.closest(".c1gridview-marker")).addClass("wijmo-wijgrid");

                    //change options to make all data show in a single table
                    grid.options = newOptions;

                    //requst all data
                    (<any>$gridElement).wijSaveState("c1gridview", "onwijstatesaving");
                    c1common.updateWebFormsInternals(id, __JSONC1.stringify(grid.onwijstatesaving()));
                    WebForm_DoCallback(oldOptions.innerState._uid, newOptions.allowPaging ? ("page:" + newOptions.pageIndex) : "render",
                        result => {
                            // generate a temp grid which shows all data in a single table
                            var response: ICallbackResponse = jQuery.parseJSON(result),
                                options: IC1GridViewOptions = wijmoASPNetParseOptions(jQuery.parseJSON(<any>response.options));

                            options.rendered = gridExporter.createGridRendered(setting, exportAction, container);

                            // For fixing bug 73562: scrollMode's (de)serialization has problem after it is obsoleted.
                            // So force to set it to "none" here.
                            options.scrollMode = "none";

                            container.appendTo($(document.body));

                            $((<ICallbackEntity>response.entity).html).filter("table").eq(0).appendTo(container).c1gridview(options);
                        }, null, null, true);

                    //change the option back
                    grid.options = oldOptions;
                }
        }
    }
}

// grouped column header
module c1.gridview {


    var $ = jQuery;

    export class c1gridviewgroupedfield extends wijmo.grid.c1groupedfield {
        constructor(grid: c1.c1gridview, options: wijmo.grid.IColumn, element: JQuery) {
            super(grid, options, element);
        }

        _isSortable(): boolean {
            var grid = this._owner(),
                original = grid._findInstance(this.options);

            return super._isSortable.apply(this, arguments) &&
                // only autogenerated and user-defined columns with the sortExpression property explicitly defined can be sorted.
                (((<IC1GridViewTableBoundFieldOptions>this.options).sortExpression !== undefined) || (original instanceof c1gridviewautogeneratedfield));
        }
    }
}

module c1 {
    /** @ignore */
    export interface IC1DetailGridViewOptions extends IC1GridViewOptions {
        path: number;
    }

    /** @ignore */
    export class c1detailgridview extends c1gridview {
        public options: IC1DetailGridViewOptions;

        //#region sizing
        _onWindowResize() {
            // reset size, so the root grid can calculate its sizes correctly
            this._topmostElement().width("");
            this._view()._resetWidth();
            return; // The root grid triggers resizing of the children grids.
        }

        _setSizeInternal(scrollValue: wijmo.grid.IScrollingState) {
            if (this._ignoreSizing()) {
                return;
            }
            this._topmostElement().width(""); // reset 
            super._setSizeInternal.apply(this, arguments);
        }
        //#endregion

        _callbackAction(): gridview.callbackAction {
            var parent = this._parent();

            return parent
                ? parent._callbackAction()
                : gridview.callbackAction.none;
        }
    }

    c1detailgridview.prototype.widgetEventPrefix = "c1detailgridview";

    c1detailgridview.prototype.options = <any>$.extend(true, {}, $.wijmo.c1gridview.prototype.options, {
        path: undefined
    });

    $.wijmo.registerWidget("c1detailgridview", $.wijmo.c1gridview, c1detailgridview.prototype);
}
