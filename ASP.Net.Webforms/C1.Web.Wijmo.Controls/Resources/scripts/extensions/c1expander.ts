/// <reference path="../../../../../Widgets/Wijmo/wijexpander/jquery.wijmo.wijexpander.ts"/>

declare var __doPostBack, c1common;

module c1 {


	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1expander extends wijmo.expander.wijexpander {
		_init() {
			var height = this.options.height, content, header;
			if (height != "") {
				content = this.element.find("> .ui-widget-content");
				header = this.element.find("> .ui-expander-header");
				content.outerHeight(this.element.height() - header.height());
			}

			this.element.removeClass("ui-helper-hidden-accessible");
			$.wijmo.wijexpander.prototype._init.apply(this, arguments);
			this.element.bind("c1expanderaftercollapse c1expanderafterexpand", jQuery.proxy(this._onExpandedStateChanged, this));
		}
		_onExpandedStateChanged() {
			if (this.options.postBackEventReference) {
				c1common.nonStrictEval(this.options.postBackEventReference.replace("{0}", this.options.expanded));
			}
		}
	}

	c1expander.prototype.widgetEventPrefix = "c1expander";

	c1expander.prototype.options = $.extend(true, {}, $.wijmo.wijexpander.prototype.options, {
		height: "",
		postBackEventReference: null
	});

	$.wijmo.registerWidget("c1expander", c1expander.prototype);
}