/// <reference path="../../../../../Widgets/Wijmo/wijsuperpanel/jquery.wijmo.wijsuperpanel.ts"/>

declare var __doPostBack, c1common;

module c1 {

	var $ = jQuery, hiddenCss = "ui-helper-hidden-accessible";

	export class c1superpanel extends wijmo.superpanel.wijsuperpanel {
		_setOption (key, value) {
			var self = this;
			if (key === "disabled") {
				self._disableGestureScrolling(value);
			}
			$.wijmo.wijsuperpanel.prototype._setOption.apply(self, arguments);
		}

		_disableGestureScrolling (value) {
			var self = this, f = self._fields(),
			templateWrapper = f.templateWrapper;
			if (templateWrapper &&
				templateWrapper.is(":ui-draggable")) {
				templateWrapper.draggable(value ? "disable" : "enable");
			}
		}

		_create () {
			var self = this;

			self.element.removeClass("ui-helper-hidden-accessible");
			$.wijmo.wijsuperpanel.prototype._create.apply(self, arguments);
		}

		paintPanel(unfocus) { 
			var self = this;
			super.paintPanel(unfocus);
			if (this.element.is(":visible")) {
				if (self.options.enableGestureScrolling) {
					self._enableGestureScrolling();
				}
				if (self.options.disabled) {
					self._disableGestureScrolling(true);
				}
			}
		}

		_enableGestureScrolling () {
			var self = <any>this,
				f = self._fields(),
				hbarContainer = f.hbarContainer,
				hbarDrag = f.hbarDrag,
				vbarContainer = f.vbarContainer,
				vbarDrag = f.vbarDrag,
				templateWrapper = f.templateWrapper,
				contentWrapper = f.contentWrapper,
				w = contentWrapper.width(),
				h = contentWrapper.height(),
				offset = templateWrapper && templateWrapper.offset(),
				ox = offset && offset.left,
				oy = offset && offset.top,
				contentWidth = f.contentWidth,
				contentHeight = f.contentHeight,
				o = self.options,
				hScroller = o.hScroller,
				vScroller = o.vScroller,
				hScrollValue = hScroller.scrollValue,
				vSCrollValue = vScroller.scrollValue,
				x1, y1;

			// If init superpanel with scrollValue, the drag range is not start from the offset position.
			if (hScrollValue) { 
				ox += this.scrollValueToPx(hScrollValue, "h");
			}

			if (vSCrollValue) {
				oy += this.scrollValueToPx(vSCrollValue, "v");
			}

			if (templateWrapper) {
				if (contentWidth < w) {
					x1 = ox;
				}
				else { 
					x1 = ox - contentWidth + w;
				}
				if (contentHeight < h) {
					y1 = oy;
				}
				else { 
					y1 = oy - contentHeight + h;
				}
				templateWrapper.draggable({
					drag: function (event, ui) {
						var pos = ui.position,
							x = pos.left,
							y = pos.top;

						if (x <= 0 && x >= w - contentWidth) {
							o.hScroller.scrollValue = self.scrollPxToValue(-x, "h");
							self._scrollDrag("h", hbarContainer, hbarDrag, true);
						}

						if (y <= 0 && y >= h - contentHeight) {
							o.vScroller.scrollValue = self.scrollPxToValue(-y, "v");
							self._scrollDrag("v", vbarContainer, vbarDrag, true);
						}
					},
					containment: [x1, y1, ox, oy]
				});
			}
		}

		_scrollDrag (dir, hbarContainer, hbarDrag,
			fireScrollEvent) {
			var self = <any>this,
				o = self.options,
				v = dir === "v",
				scroller = v ? o.vScroller : o.hScroller,
				hMin = scroller.scrollMin,
				hMax = scroller.scrollMax,
				hValue = scroller.scrollValue === undefined ?
				hMin : (scroller.scrollValue - hMin),
				hLargeChange = self._getLargeChange(dir),
				max = hMax - hMin - hLargeChange + 1,
				dragleft = -1,
				track, drag, padding;

			if (hValue > max) {
				hValue = max;
			}

			if (hbarContainer !== undefined) {
				track = self._getTrackLen(dir);
				drag = hbarDrag[v ? "outerHeight" : "outerWidth"](true);
				padding = self._getScrollContainerPadding(v ? "top" : "left");
				dragleft = (hValue / max) * (track - drag) + padding;
			}

			if (dragleft >= 0) {
				hbarDrag.css(v ? "top" : "left", dragleft + "px");
			}
			
			self._scrollEnd(fireScrollEvent, self, dir);
		}
	}

	c1superpanel.prototype.widgetEventPrefix = "c1superpanel";

	c1superpanel.prototype.options = $.extend(true, {}, $.wijmo.wijsuperpanel.prototype.options, {
		enableGestureScrolling: true
	});

	$.wijmo.registerWidget("c1superpanel", c1superpanel.prototype);
}