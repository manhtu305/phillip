﻿/// <reference path="../../../../../Widgets/Wijmo/Base/jquery.wijmo.widget.ts"/>

declare var __doPostBack, WebForm_DoCallback, c1common;

// Module
module c1 {


	// Class
	var $ = jQuery;

	export class c1sitemap extends wijmo.JQueryUIWidget {

		_nodes: any;

		_create() {
			var self = this;

			self._nodes = self.element.find(".wijmo-c1sitemap-node");
			self._nodes.mouseenter(this._onMouseEnterNode).mouseleave(this._onMouseLeaveNode);
		}

		_init() {
			super._init();
		}

		_setOption(key, value) {
			super._setOption(key, value);
		}

		destroy() {
			this._nodes.unbind();

			super.destroy();
		}

		_onMouseEnterNode() {
			$(this).addClass("ui-state-hover");
		}

		_onMouseLeaveNode() {
			$(this).removeClass("ui-state-hover");
		}

	}

	c1sitemap.prototype.widgetEventPrefix = "c1sitemap";

	c1sitemap.prototype.options = <any> $.extend({}, {
		nodes: null
	});

	$.wijmo.registerWidget("c1sitemap", c1sitemap.prototype);
}

