﻿/// <reference path="../../../../../Widgets/Wijmo/wijcandlestickchart/jquery.wijmo.wijcandlestickchart.ts"/>
module c1 {
    class c1candlestickchart extends wijmo.chart.wijcandlestickchart {
		hintContent: any;
        formater: any;
        _create() {
			window["c1chart"].initChartExport();
            var self = this,
                o = self.options,
                content = o.hint.content,
                isContentFunc;

            self.hintContent = content;
            if (o.hint && o.hint.enable) {
                isContentFunc = $.isFunction(content);
                o.hint.content = function () {
                	return window["c1chart"].handleHintContents(content, this, self, (data) => {
                		var hintContents = data.hintContents,
							index = data.index;
                		if (hintContents && hintContents.length &&
								index < hintContents.length) {
                			return self._fotmatTooltip(hintContents[index]);
                		}
                		return null;
                	});
                };
            }
            
            self.element.removeClass("ui-helper-hidden-accessible");
            super._create();
        }

        _hasAxes() {
            return true;
        }

        adjustOptions() {
            var self = this,
                o = self.options,
                axis:any = o.axis;
            o.hint.content = self.hintContent;
            if (this.timeUtil) {
                $.each(o.seriesList, (i, series) => {
                    if (series.data && series.data.x) {
                        $.each(series.data.x, (j, val) => {
                            if (typeof (val) === "number") {
                                series.data.x[j] = this.timeUtil.getTime(val);
                            }
                        });
                    }
                });
            }
            if (axis && axis.x) {
                if (axis.x.autoMin) {
                    axis.x.min = null;
                }
                if (axis.x.autoMax) {
                    axis.x.max = null;
                }
            }
            if (axis && axis.y) {
                if ($.isArray(axis.y)) {
                    $.each(axis.y, (i, yaxis) => {
                        if (yaxis.autoMin) {
                            yaxis.min = null;
                        }
                        if (yaxis.autoMax) {
                            yaxis.max = null;
                        }
                    });
                } else {
                    if (axis.y.autoMin) {
                        axis.y.min = null;
                    }
                    if (axis.y.autoMax) {
                        axis.y.max = null;
                    }
                }
            }
        }
    }

    c1candlestickchart.prototype.widgetEventPrefix = "c1candlestickchart";
    $.wijmo.registerWidget("c1candlestickchart", c1candlestickchart.prototype);
}