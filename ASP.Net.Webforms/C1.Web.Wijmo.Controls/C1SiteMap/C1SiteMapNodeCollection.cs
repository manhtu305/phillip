using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Collections;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
	/// <summary>
	/// Represents the structure that manages the <see cref="C1SiteMapNode"/> nodes.
	/// </summary>
    public class C1SiteMapNodeCollection : C1ObservableItemCollection<IC1SiteMapNodeCollectionOwner, C1SiteMapNode>
    {
        #region Fields

        private C1SiteMap _siteMap;

        #endregion

        #region Constructor

        /// <summary>
		/// Explicit constructor for <see cref="C1SiteMapNodeCollection"/>.
		/// </summary>
		/// <param name="owner"></param>
        public C1SiteMapNodeCollection(IC1SiteMapNodeCollectionOwner owner)
			: base(owner)
		{ }

		#endregion

        #region Properties

        /// <summary>
        /// Gets the SiteMap control.
        /// </summary>
        private C1SiteMap SiteMap
        {
            get
            {
                if (_siteMap == null)
                {
                    if (Owner is C1SiteMap)
                    {
                        _siteMap = Owner as C1SiteMap;
                    }
                    else if (Owner is C1SiteMapNode)
                    {
                        _siteMap = (Owner as C1SiteMapNode).SiteMap;
                    }
                }

                return _siteMap;
            }
        }

        #endregion

        #region Methods

        /// <summary>
		/// Adds <see cref="C1SiteMapNode"/> node to the collection.
		/// </summary>
		/// <param name="node">node to add</param>
		public new void Add(C1SiteMapNode node)
		{
			if (node != null)
			{
				Insert(base.Count, node);
			}
		}

		/// <summary>
		/// Inserts <see cref="C1SiteMapNode"/> node to the collection.
		/// </summary>
		/// <param name="index">collection index</param>
		/// <param name="node">node to insert</param>
		public new void Insert(int index, C1SiteMapNode node)
		{
            node.SetOwner(this.Owner);
			base.InsertItem(index, node);

            //fire OnNodeCreated event.
            if (SiteMap != null)
            {
                SiteMap.OnNodeCreated(new C1SiteMapNodeEventArgs(node));
            }
		}

		/// <summary>
		/// Removes <see cref="C1SiteMapNode"/> node from the collection.
		/// <param name="node">node to remove</param>
		/// </summary>
		public new void Remove(C1SiteMapNode node)
		{
			int index = IndexOf(node);
            node.SetOwner(null);
			this.RemoveItem(index);
		}

        #endregion		
	}
}
