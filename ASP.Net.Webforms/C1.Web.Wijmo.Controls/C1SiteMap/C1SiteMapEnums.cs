﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    /// <summary>
    /// Define the site map node's layout type.
    /// </summary>
    public enum SiteMapLayoutType
    {
        /// <summary>
        /// The site map nodes will be shown as list.
        /// </summary>
        List,
        /// <summary>
        /// The site map nodes will be shown horizontally one by one, with a separator text (default is "|").
        /// </summary>
        Flow
    }

    /// <summary>
    /// The column repeat direction.
    /// </summary>
    public enum RepeatDirection
    {
        /// <summary>
        /// Repeat site map nodes horizontally.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Repeat site map nodes vertically.
        /// </summary>
        Vertical
    }

}
