﻿using C1.Web.Wijmo.Controls.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class C1SiteMapListLayoutSetting : Settings
    {
        /// <summary>
        /// Gets or sets the column count for the level.
        /// </summary>
        [NotifyParentProperty(true)]
        [DefaultValue(1)]
        [C1Description("C1SiteMapListLayoutSetting.RepeatColumns")]
        [WidgetOption]
        public int RepeatColumns
        {
            get
            {
                return GetPropertyValue<int>("RepeatColumns", 1);
            }
            set
            {
                SetPropertyValue<int>("RepeatColumns", value);
            }
        }

        /// <summary>
        /// gets or sets the column repeat direction if showing with multiple columns.
        /// </summary>
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(typeof(RepeatDirection), "Horizontal")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public RepeatDirection RepeatDirection
        {
            get
            {
                return GetPropertyValue<RepeatDirection>("RepeatDirection", RepeatDirection.Horizontal);
            }
            set
            {
                SetPropertyValue<RepeatDirection>("RepeatDirection", value);
            }
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize()
        {
            return RepeatColumns != 1 || RepeatDirection != Controls.C1SiteMap.RepeatDirection.Horizontal;
        }
    }
}
