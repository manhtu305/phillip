﻿using C1.Web.Wijmo.Controls.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    [ParseChildren(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class C1SiteMapLevelSetting : Settings
    {
        #region Fields

        private C1SiteMapListLayoutSetting _listLayout = new C1SiteMapListLayoutSetting();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the level of the setting.
        /// </summary>
        [C1Description("C1SiteMapLevelSetting.Level")]
        [DefaultValue(0)]
        [WidgetOption]
        [NotifyParentProperty(true)]
        public virtual int Level
        {
            get
            {
                return GetPropertyValue<int>("Level", 0);
            }
            set
            {
                SetPropertyValue<int>("Level", value);
            }
        }

        /// <summary>
        /// Gets or sets the maximum node of the level.
        /// </summary>
        [DefaultValue(-1)]
        [C1Description("C1SiteMapLevelSetting.MaxNodes")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        public int MaxNodes
        {
            get
            {
                return GetPropertyValue<int>("MaxNodes", -1);
            }
            set
            {
                SetPropertyValue<int>("MaxNodes", value);
            }
        }

        /// <summary>
        /// Gets or sets the layout type.
        /// </summary>
        [DefaultValue(typeof(SiteMapLayoutType), "List")]
        [NotifyParentProperty(true)]
        [C1Description("C1SiteMapLevelSetting.Layout")]
        [WidgetOption]
        public SiteMapLayoutType Layout
        {
            get
            {
                return GetPropertyValue<SiteMapLayoutType>("LayoutType", SiteMapLayoutType.List);
            }
            set
            {
                SetPropertyValue<SiteMapLayoutType>("LayoutType", value);
            }
        }

        /// <summary>
        /// Gets the ListLayout, which can set the repeat columns.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
        [C1Description("C1SiteMapLevelSetting.ListLayout")]
        [WidgetOption]
        public C1SiteMapListLayoutSetting ListLayout
        {
            get
            {
                return _listLayout;
            }
        }

        /// <summary>
        /// Gets or sets the separator text, which is used to separate nodes in flow mode.
        /// </summary>
        [DefaultValue("|")]
        [NotifyParentProperty(true)]
        [C1Description("C1SiteMapLevelSetting.SeparatorText")]
        [WidgetOption]
        public string SeparatorText
        {
            get
            {
                return GetPropertyValue<string>("SeparateText", "|");
            }
            set
            {
                SetPropertyValue<string>("SeparateText", value);
            }
        }

        /// <summary>
        /// Gets or sets the image url which will be applied to all nodes of the level.
        /// </summary>
        [DefaultValue("")]
        [NotifyParentProperty(true)]
        [C1Description("C1SiteMapLevelSetting.ImageUrl")]
        [Editor(typeof(System.Web.UI.Design.ImageUrlEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [WidgetOption]
        public string ImageUrl
        {
            get
            {
                return GetPropertyValue<string>("ImageUrl", string.Empty);
            }
            set
            {
                SetPropertyValue<string>("ImageUrl", value);
            }
        }

        /// <summary>
        /// Gets or sets the template which will be applied to all nodes of the level.
        /// </summary>
        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [RefreshProperties(RefreshProperties.All)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate NodeTemplate
        {
            get
            {
                return this.GetPropertyValue<ITemplate>("Template", null);
            }
            set
            {
                this.SetPropertyValue<ITemplate>("Template", value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the value indicate whether need to seriazlie.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize()
        {
            if (ListLayout.ShouldSerialize())
                return true;

            if (Level != 0)
                return true;

            if (MaxNodes != -1)
                return true;

            if (Layout != SiteMapLayoutType.List)
                return true;

            if (!string.IsNullOrEmpty(ImageUrl))
                return true;

            if (string.Compare(SeparatorText, "|") != 0)
                return true;

            return false;
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize ListLayout.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeListLayout()
        {
            return ListLayout.ShouldSerialize();
        }

        #endregion

    }

}
