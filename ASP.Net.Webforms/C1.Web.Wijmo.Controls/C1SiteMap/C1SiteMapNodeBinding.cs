﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Security.Permissions;
using System.Drawing.Design;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    /// <summary>
    /// Defines the relationship between a data item and the node it is binding to in a <see cref="C1SiteMap"/> control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class C1SiteMapNodeBinding : Settings, IDataSourceViewSchemaAccessor, ICloneable
    {
        #region Constructor

        /// <summary>
		/// Initializes a new instance of the <see cref="C1SiteMapNodeBinding"/> class.
        /// </summary>
        public C1SiteMapNodeBinding()
        {
        }

        /// <summary>
		/// Initializes a new instance of the <see cref="C1SiteMapNodeBinding"/> class.
        /// </summary>
        /// <param name="dataMember"></param>
        public C1SiteMapNodeBinding(string dataMember)
        {
            this.DataMember = dataMember;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the value to match against a <see cref="IHierarchyData"/>. Type property for a data item to determine whether to apply the SiteMap node binding.
        /// </summary>
        [C1Description("C1SiteMapNodeBinding.DataMember")]
        [DefaultValue("")]
        [WidgetOption]
        [C1Category("Category.Data")]
        public string DataMember
        {
            get
            {
                return this.GetPropertyValue<string>("DataMember", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("DataMember", value);
            }
        }

        /// <summary>
        /// Gets or sets the node level at which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Data")]
        [DefaultValue(0)]
        [WidgetOption]
        [C1Description("C1SiteMapNodeBinding.Level")]
        public int Level
        {
            get
            {
                return this.GetPropertyValue<int>("Level", 0);
            }
            set
            {
                this.SetPropertyValue<int>("Level", value);
            }
        }

        /// <summary>
        /// Gets or sets the string that specifies the display format 
        /// for the text of the <see cref="C1SiteMapNode"/> node to which the <see cref="C1SiteMapNodeBinding"/>object is applied.
        /// </summary>
        [C1Description("C1SiteMapNodeBinding.FormatString")]
        [DefaultValue("")]
        [WidgetOption]
        [Localizable(true)]
        [C1Category("Category.Databindings")]
        public string FormatString
        {
            get
            {
                return this.GetPropertyValue<string>("FormatString", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("FormatString", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Text property of a <see cref="C1SiteMapNode"/> object to which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
        [C1Description("C1SiteMapNodeBinding.TextField")]
        [DefaultValue("")]
        [WidgetOption]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string TextField
        {
            get
            {
                return this.GetPropertyValue<string>("TextField", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("TextField", value);
            }
        }      

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Value property of a <see cref="C1SiteMapNode"/> object to which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [C1Category("Category.Databindings")]
        [C1Description("C1SiteMapNodeBinding.ValueField")]
        [DefaultValue("")]
        [WidgetOption]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string ValueField
        {
            get
            {
                return this.GetPropertyValue<string>("ValueField", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("ValueField", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Enabled property of a <see cref="C1SiteMapNode"/> object to which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [WidgetOption]
        [C1Description("C1SiteMapNodeBinding.EnabledField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string EnabledField
        {
            get
            {
                return this.GetPropertyValue<string>("EnabledField", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("EnabledField", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the TooTip property of a <see cref="C1SiteMapNode"/> object to which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [WidgetOption]
        [C1Description("C1SiteMapNodeBinding.TooTipField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
        public string TooTipField
        {
            get
            {
                return this.GetPropertyValue<string>("TooTipField", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("TooTipField", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the NavigateUrl property of a <see cref="C1SiteMapNode"/> object to which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [WidgetOption]
        [C1Category("Category.Databindings")]
        [C1Description("C1SiteMapNodeBinding.NavigateUrlField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string NavigateUrlField
        {
            get
            {
                return this.GetPropertyValue<string>("NavigateUrlField", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("NavigateUrlField", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the ImageUrl property of a <see cref="C1SiteMapNode"/> object to which the <see cref="C1SiteMapNodeBinding"/> object is applied.
        /// </summary>
        [DefaultValue("")]
        [WidgetOption]
        [C1Category("Category.Databindings")]
        [C1Description("C1SiteMapNodeBinding.ImageUrlField")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string ImageUrlField
        {
            get
            {
                return this.GetPropertyValue<string>("ImageUrlField", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("ImageUrlField", value);
            }
        }

        #endregion

        #region IDataSourceViewSchemaAccessor

        object IDataSourceViewSchemaAccessor.DataSourceViewSchema
        {
            get
            {
                return this.GetPropertyValue<object>("DataSourceViewSchema", string.Empty);
            }
            set
            {
                this.SetPropertyValue<object>("DataSourceViewSchema", value);
            }
        }

        #endregion

        #region ICloneable

        /// <summary>
        /// Clone this object.
        /// </summary>
        /// <returns></returns>
        object ICloneable.Clone()
        {
            C1SiteMapNodeBinding binding = new C1SiteMapNodeBinding();
            binding.DataMember = this.DataMember;
            binding.Level = this.Level;
            binding.TextField = this.TextField;
            binding.ValueField = this.ValueField;
            binding.TooTipField = this.TooTipField;
            binding.NavigateUrlField = this.NavigateUrlField;
            binding.EnabledField = this.EnabledField;
            binding.ImageUrlField = this.ImageUrlField;

            return binding;
        }

        #endregion

    }
}
