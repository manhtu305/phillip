using System;
using System.Collections.Generic;
using System.Text;


namespace C1.Web.Wijmo.Controls.C1SiteMap
{
	/// <summary>
	/// Used to establish a relationship between the <see cref="C1SiteMapNodeCollection"/> nodes collection 
	/// and its <see cref="IC1SiteMapNodeCollectionOwner"/> owner.
	/// </summary>
	public interface IC1SiteMapNodeCollectionOwner
	{
		/// <summary>
		/// Collection that contains <see cref="C1SiteMapNode"/> nodes.
		/// </summary>
		C1SiteMapNodeCollection Nodes
		{
			get;
		}

		/// <summary>
		/// Owner of <see cref="C1SiteMapNodeCollection"/> collection.
		/// </summary>
		IC1SiteMapNodeCollectionOwner Owner
		{
			get;
		}
	}
}
