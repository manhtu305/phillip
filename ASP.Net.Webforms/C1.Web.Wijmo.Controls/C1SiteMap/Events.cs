﻿using C1.Web.Wijmo.Controls.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    /// <summary>
    /// Provides data for the <see cref="C1SiteMap.NodeDataBound"/>, <see cref="C1SiteMap.NodeCreated" /> events of the
    /// <see cref="C1SiteMap"/> control.
    /// </summary>
    public class C1SiteMapNodeEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="C1SiteMapNodeEventArgs"/> class.
        /// </summary>
        /// <param name="node">The referenced node.</param>
        public C1SiteMapNodeEventArgs(C1SiteMapNode node)
        {
            Node = node;
        }

        /// <summary>
        /// Gets or sets the referenced node in the <see cref="C1SiteMap"/> control when the event is raised.
        /// </summary>
        /// <value>The referenced node in the <see cref="C1SiteMap"/> control when the event is raised.</value>
        public C1SiteMapNode Node { get; set; }
    }

    /// <summary>
    /// Represents the method that handles the <see cref="C1SiteMap.NodeDataBound" /> and <see cref="C1SiteMap.NodeCreated"/> events.
    /// of the <see cref="C1SiteMap"/> control.
    /// </summary>
    public delegate void C1SiteMapNodeEventHandler(object sender, C1SiteMapNodeEventArgs e);

}
