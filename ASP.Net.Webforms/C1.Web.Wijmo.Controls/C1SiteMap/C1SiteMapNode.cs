﻿using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    [ToolboxItem(false)]
    [ParseChildren(true)]
    [DefaultProperty("Nodes")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class C1SiteMapNode : UIElement, INamingContainer, IC1SiteMapNodeCollectionOwner, IDataItemContainer
    {
        #region Fields

        //the max named CSS level, if node's level exceed this value, the css will be named as "wijmo-c1sitemap-levelother".
        private static int MaxCSSLevel = 3;

        private C1SiteMapNodeCollection _nodes;
        private IC1SiteMapNodeCollectionOwner _owner;

        private C1SiteMap _siteMap;

        //for data binding
        private object _dataItem;
        private int _dataItemIndex;
        private int _displayIndex;

        //for rendering
        private HtmlGenericControl _templateContainer;
        private HtmlControl _nodesContainer = new HtmlGenericControl("ul");

        #endregion

        #region Constructor

        public C1SiteMapNode()
        {
            _nodes = new C1SiteMapNodeCollection(this);
        }

        public C1SiteMapNode(object dataItem, int index, int displayIndex)
        {
            _dataItem = dataItem;
            _dataItemIndex = index;
            _displayIndex = displayIndex;

            _nodes = new C1SiteMapNodeCollection(this);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the text of the node.
        /// </summary>
        [C1Description("C1SiteMapNode.Text")]
        [DefaultValue("")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [Layout(LayoutType.Behavior)]
        public string Text
        {
            get
            {
                return this.GetPropertyValue<String>("Text", string.Empty);
            }
            set
            {
                this.SetPropertyValue<String>("Text", value);
            }
        }

        /// <summary>
        /// Gets or sets the value of the node.
        /// </summary>
        [C1Description("C1SiteMapNode.Value")]
        [DefaultValue("")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [Layout(LayoutType.Behavior)]
        public string Value
        {
            get
            {
                return this.GetPropertyValue<String>("Value", string.Empty);
            }
            set
            {
                this.SetPropertyValue<String>("Value", value);
            }
        }

        /// <summary>
        /// Gets or sets the NavigateUrl of the node.
        /// </summary>
        [C1Description("C1SiteMapNode.NavigateUrl")]
        [DefaultValue("")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [Layout(LayoutType.Behavior)]
        public string NavigateUrl
        {
            get
            {
                return this.GetPropertyValue<string>("NavigateUrl", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("NavigateUrl", value);
            }
        }

        /// <summary>
        /// Gets or sets the target to show the content specified by NavigateUrl.
        /// Values can be: _blank, _parent, _search, _self, _top.
        /// </summary>
        [C1Description("C1SiteMapNode.Target")]
        [DefaultValue("")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [Layout(LayoutType.Behavior)]
        public string Target
        {
            get
            {
                return this.GetPropertyValue<string>("Target", string.Empty);
            }
            set
            {
                this.SetPropertyValue<string>("Target", value);
            }
        }

        /// <summary>
        /// Gets or sets the ImageUrl of the node.
        /// </summary>
        [C1Description("C1SiteMapNode.ImageUrl")]
        [DefaultValue("")]
        [WidgetOption]
        [Editor(typeof(System.Web.UI.Design.ImageUrlEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [NotifyParentProperty(true)]
        [Layout(LayoutType.Behavior)]
        public string ImageUrl
        {
            get
            {
                return this.GetPropertyValue<String>("ImageUrl", string.Empty);
            }
            set
            {
                this.SetPropertyValue<String>("ImageUrl", value);
            }
        }

        /// <summary>
        /// Gets or sets the level of the node.
        /// </summary>
        [Browsable(false)]
        [WidgetOption]
        [DefaultValue(0)]
        [Layout(LayoutType.Behavior)]
        public int Level
        {
            get
            {
                return this.GetPropertyValue<int>("Level", 0);
            }
            internal set
            {
                this.SetPropertyValue<int>("Level", value);
            }
        }

        /// <summary>
        /// Gets a <see cref="C1SiteMapNodeCollection"/> that contains the first-level child nodes of the current node.
        /// </summary>
        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [WidgetOption]
        [CollectionItemType(typeof(C1SiteMapNode))]
        [Layout(LayoutType.Data)]
        public C1SiteMapNodeCollection Nodes
        {
            get { return _nodes; }
        }

        /// <summary>
        ///  Gets or sets the template that will be used for the specified C1SiteMapNodes.
        /// </summary>
        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        [RefreshProperties(RefreshProperties.All)]
        public virtual ITemplate Template
        {
            get
            {
                return this.GetPropertyValue<ITemplate>("Template", null);
            }
            set
            {
                this.SetPropertyValue<ITemplate>("Template", value);
            }
        }

        /// <summary>
        /// Gets or sets the HtmlGenericControl which is the template container.
        /// </summary>
        internal HtmlGenericControl TemplateContainer
        {
            get
            {
                return this._templateContainer;
            }
            set
            {
                this._templateContainer = value;
            }
        }

        /// <summary>
        /// Gets or sets the value indicate whether this node is templated.
        /// </summary>
        internal bool Templated { get; set; }

        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this web server control.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Li;
            }
        }

        /// <summary>
        /// Gets the SiteMap control.
        /// </summary>
        internal C1SiteMap SiteMap
        {
            get
            {
                return _siteMap;
            }
            set
            {
                _siteMap = value;
            }
        }

        /// <summary>
        /// The key used to backup/restore node's template.
        /// </summary> 
        [WidgetOption]
        [Json(true)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string StaticKey
        {
            get;
            set;
        }

        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            if (this.Templated && this.TemplateContainer != null)
            {
                this.Controls.Add(TemplateContainer);

                //must call this method to bind properties.
                this.DataBind();
            }

            _nodesContainer.Controls.Clear();
            this.Controls.Add(_nodesContainer);

            //check whether next level setting is flow, if true, add the separator control
            C1SiteMapLevelSetting nextLevelSetting = SiteMap.GetLevelSetting(Level + 1);
            int count = 0;

            foreach (C1SiteMapNode node in this.Nodes)
            {
                count++;

                if (!_nodesContainer.Controls.Contains(node))
                {
                    _nodesContainer.Controls.Add(node);
                }

                //check the max nodes.
                if (nextLevelSetting.MaxNodes >= 0 && count >= nextLevelSetting.MaxNodes)
                    return;

                //add separator
                if (nextLevelSetting.Layout == SiteMapLayoutType.Flow && count < Nodes.Count)
                {
                    var sep = new HtmlGenericControl("li");
                    sep.Attributes.Add("class", "wijmo-c1sitemap-separator");
                    sep.InnerText = nextLevelSetting.SeparatorText;

                    _nodesContainer.Controls.Add(sep);
                }
            }

            this.EnsureChildControls();
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            string cssClass = "";

            if (this.Nodes != null && this.Nodes.Count > 0)
            {
                cssClass += "wijmo-c1sitemap-parent";
            }
            else
            {
                cssClass += "wijmo-c1sitemap-item";
            }

            //check whether need to display with multiple columns
            C1SiteMapLevelSetting setting = SiteMap.GetLevelSetting(Level);
            if (setting.Layout == SiteMapLayoutType.List)
            {
                if (setting.ListLayout.RepeatColumns > 1)
                {
                    //design mode the width is not 100%.
                    int width = (IsDesignMode ? 95 : 100) / setting.ListLayout.RepeatColumns;

                    //if current node is the root node (only 1 node at level 0), then the width should be 100%
                    if (Level == 0 && SiteMap.Nodes.Count == 1)
                    {
                        width = 100;
                    }

                    writer.AddStyleAttribute(HtmlTextWriterStyle.Width, string.Format("{0}%", width));
                }
            }

            if (!this.Enabled)
            {
                cssClass += " ui-state-disabled";
            }

            this.CssClass = string.Format("{0} {1}", this.CssClass, cssClass);

            base.AddAttributesToRender(writer);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            EnsureChildControls();

            base.Render(writer);
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            if (!Templated)
            {
                RenderNode(writer);
            }
            else if (TemplateContainer != null)
            {
                TemplateContainer.RenderControl(writer);
            }

            var levelSetting = SiteMap.GetLevelSetting(Level);
            if (levelSetting.Layout == SiteMapLayoutType.Flow)
            {
                //if current level layout is Flow, do not render child.
                return;
            }

            //prepare child containers and css
            if (Nodes.Count > 0)
            {
                string strClass = "wijmo-c1sitemap-list wijmo-c1sitemap-child";

                //check whether next level is display as flow.
                C1SiteMapLevelSetting nextLevelSetting = SiteMap.GetLevelSetting(Level + 1);
                if (nextLevelSetting.Layout == SiteMapLayoutType.Flow
                    || (nextLevelSetting.Layout == SiteMapLayoutType.List && nextLevelSetting.ListLayout.RepeatColumns > 1))
                {
                    strClass += " wijmo-c1sitemap-flowlist";
                }

                _nodesContainer.Attributes.Add("class", strClass);
                _nodesContainer.RenderControl(writer);
            }
        }

        private void RenderNode(HtmlTextWriter writer)
        {
            StringBuilder sbCssClass = new StringBuilder("wijmo-c1sitemap-node ui-corner-all");
            if (this.Level < MaxCSSLevel)
            {
                sbCssClass.Append(string.Format(" wijmo-c1sitemap-node-level{0}", this.Level));
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, sbCssClass.ToString());
            writer.RenderBeginTag(HtmlTextWriterTag.Div);//<div>

            //add image, do not add image at design time.
            if (!IsDesignMode)
            {
                string imgUrl = this.ImageUrl;
                if (string.IsNullOrEmpty(imgUrl))
                {
                    C1SiteMapLevelSetting setting = SiteMap.GetLevelSetting(Level);
                    imgUrl = setting.ImageUrl;
                }

                if (!string.IsNullOrEmpty(imgUrl))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, ResolveClientUrl(imgUrl));
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag();
                }
            }

            //add text link
            if (this.Enabled && this.SiteMap.Enabled)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-c1sitemap-link");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, ResolveClientUrl(this.NavigateUrl));
                writer.AddAttribute(HtmlTextWriterAttribute.Target, this.Target);
                writer.RenderBeginTag(HtmlTextWriterTag.A);//<a href=>
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(this.Text);
            writer.RenderEndTag();

            if (this.Enabled && this.SiteMap.Enabled)
            {
                writer.RenderEndTag(); //<a>
            }

            writer.RenderEndTag(); //</div>
        }

        /// <summary>
        /// Generate nodes from datasource if using data binding.
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="level"></param>
        /// <param name="count"></param>
        internal void GenerateChildNodesFromDataSource(IHierarchicalEnumerable dataSource, int level, int count)
        {
            foreach (object item in dataSource)
            {
                IHierarchyData data = dataSource.GetHierarchyData(item);

                int index = count++;
                C1SiteMapNode node = new C1SiteMapNode(item, index, index);
                node.Level = level;
                node.SiteMap = this.SiteMap;

                C1SiteMapNodeBinding binding = SiteMap.GetLevelBinding(level);
                node.DataBind(binding, dataSource, item);

                //fire OnNodeDataBound event.
                C1SiteMapNodeEventArgs arg = new C1SiteMapNodeEventArgs(node);
                SiteMap.OnNodeDataBound(arg);

                this.Nodes.Add(node);

                if (data.HasChildren)
                {
                    IHierarchicalEnumerable childEnum = data.GetChildren();
                    if (childEnum != null)
                    {
                        node.GenerateChildNodesFromDataSource(childEnum, level + 1, count);
                    }
                }
            }
        }

        /// <summary>
        /// Bind node properties from given binding.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="dataSource"></param>
        /// <param name="data"></param>
        internal void DataBind(C1SiteMapNodeBinding binding, IHierarchicalEnumerable dataSource, object data)
        {
            //use default binding if no binding specified.
            if (binding == null)
            {
                INavigateUIData tempData = data as INavigateUIData;
                if (tempData != null)
                {
                    this.Text = tempData.Name;
                    this.Value = tempData.Value;
                    this.NavigateUrl = tempData.NavigateUrl;
                }

                return;
            }

            //bind node with specified binding.
            IHierarchyData hdata = dataSource.GetHierarchyData(data);
            string hType = hdata.Type;

            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(data);

            //bind Text
            string text = null;
            if (!string.IsNullOrEmpty(binding.TextField))
            {
                var result = GetFieldValue(data, pdc, binding.TextField);
                text = result == null ? null : result as string;
            }

            // Format with current culture.
            if (text != null)
            {
                if (binding.FormatString.Length > 0)
                {
                    Text = string.Format(CultureInfo.CurrentCulture, binding.FormatString, new object[] { text });
                }
                else
                {
                    Text = text;
                }
            }

            //bind Value
            if (!string.IsNullOrEmpty(binding.ValueField))
            {
                var val = GetFieldValue(data, pdc, binding.ValueField);
                this.Value = val == null ? string.Empty : val as string;
            }

            //bind NavigateUrl
            if (!string.IsNullOrEmpty(binding.NavigateUrlField))
            {
                var url = GetFieldValue(data, pdc, binding.NavigateUrlField);
                this.NavigateUrl = url == null ? string.Empty : url as string;
            }

            //bind EnabledField
            if (!string.IsNullOrEmpty(binding.EnabledField))
            {
                var result = GetFieldValue(data, pdc, binding.EnabledField);
                this.Enabled = result == null ? false : (bool)result;
            }

            //bind TooTipField
            if (!string.IsNullOrEmpty(binding.TooTipField))
            {
                var url = GetFieldValue(data, pdc, binding.TooTipField);
                this.NavigateUrl = url == null ? string.Empty : url as string;
            }

            //bind ImageUrlField
            if (!string.IsNullOrEmpty(binding.ImageUrlField))
            {
                var url = GetFieldValue(data, pdc, binding.ImageUrlField);
                this.NavigateUrl = url == null ? string.Empty : url as string;
            }
        }

        private object GetFieldValue(object data, PropertyDescriptorCollection propCollection, string propName)
        {
            PropertyDescriptor descr = propCollection.Find(propName, true);

            return descr == null ? null : descr.GetValue(data);
        }

        /// <summary>
        /// Due to IC1SiteMapNodeCollectionOwner.Owner doesn't support set, we provide a method here, only for internal use.
        /// </summary>
        /// <param name="owner"></param>
        internal void SetOwner(IC1SiteMapNodeCollectionOwner owner)
        {
            this._owner = owner;
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize Nodes.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeNodes()
        {
            return Nodes != null && Nodes.Count > 0;
        }

        #endregion

        #region IDataItemContainer

        object IDataItemContainer.DataItem
        {
            get { return _dataItem; }
        }

        int IDataItemContainer.DataItemIndex
        {
            get { return _dataItemIndex; }
        }

        int IDataItemContainer.DisplayIndex
        {
            get { return _displayIndex; }
        }

        #endregion

        #region IC1SiteMapNodeCollectionOwner

        /// <summary>
        /// Gets or sets the IC1SiteMapNodeCollectionOwner.Owner.
        /// </summary>
        IC1SiteMapNodeCollectionOwner IC1SiteMapNodeCollectionOwner.Owner
        {
            get { return _owner; }
        }

        #endregion
    }
}
