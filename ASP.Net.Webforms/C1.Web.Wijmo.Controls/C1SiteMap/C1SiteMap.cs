﻿using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Localization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1SiteMap", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1SiteMap
{
#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1SiteMap.C1SiteMapDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1SiteMap.C1SiteMapDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1SiteMap.C1SiteMapDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1SiteMap.C1SiteMapDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1SiteMap runat=server></{0}:C1SiteMap>")]
    [ToolboxBitmap(typeof(C1SiteMap), "C1SiteMap.png")]
    [ParseChildren(true)]
    [LicenseProviderAttribute()]
    [DefaultProperty("Nodes")]
    [WidgetDependencies(
        "extensions.c1sitemap.css",
        "extensions.c1sitemap.js",
		ResourcesConst.WIDGETS_CONTROL,
		ResourcesConst.C1WRAPPER_CONTROL)]
    public class C1SiteMap : C1TargetHierarchicalDataBoundControlBase, IC1SiteMapNodeCollectionOwner,
        INamingContainer, IPostBackDataHandler, IC1Serializable
    {
        #region Fields

        C1SiteMapNodeCollection _nodes;
        List<C1SiteMapNodeBinding> _bindings;

        List<C1SiteMapLevelSetting> _levelSettings = new List<C1SiteMapLevelSetting>();
        C1SiteMapLevelSetting _defaultLevelSetting = new C1SiteMapLevelSetting();

        //controls for render
        private HtmlGenericControl _nodesContainer = new HtmlGenericControl("ul");
        private HtmlGenericControl _clearDiv = new HtmlGenericControl("div"); 

        //for license.
        private bool _productLicensed = false;
        private bool _shouldNag;

        //for template restore
        private Dictionary<string, C1SiteMapNode> _templateNodes = new Dictionary<string, C1SiteMapNode>();

        #endregion

        #region Constructor

        public C1SiteMap()
        {
            VerifyLicense();

            _nodes = new C1SiteMapNodeCollection(this);
            _bindings = new List<C1SiteMapNodeBinding>();
        }

        #endregion

        #region Licensing

        internal virtual void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1SiteMap), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a <see cref="C1SiteMapNodeCollection"/> object that contains the root nodes of the current <see cref="C1SiteMap"/> control.
        /// </summary>
        [Browsable(false)]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [MergableProperty(false)]
        [CollectionItemType(typeof(C1SiteMapNode))]
        [Layout(LayoutType.Data)]
        public C1SiteMapNodeCollection Nodes
        {
            get
            {
                return _nodes;
            }
        }

        /// <summary>
        /// Defines the level settings for site map nodes.
        /// </summary>
        [C1Description("C1SiteMap.LevelSettings")]
        [C1Category("Category.Layout")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [MergableProperty(false)]
        [WidgetOption]
        [CollectionItemType(typeof(C1SiteMapLevelSetting))]
        public List<C1SiteMapLevelSetting> LevelSettings
        {
            get
            {
                return _levelSettings;
            }
        }

        /// <summary>
        /// Defines the default level settings if not specified for a level of nodes.
        /// </summary>
        [C1Description("C1SiteMap.DefaultLevelSetting")]
        [C1Category("Category.Layout")]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [WidgetOption]
        [RefreshProperties(RefreshProperties.All)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public C1SiteMapLevelSetting DefaultLevelSetting
        {
            get
            {
                return _defaultLevelSetting;
            }
        }

        /// <summary>
        /// Defines the data bindings for ndoes in the site map.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [C1Description("C1SiteMap.DataBindings")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [MergableProperty(false)]
        [WidgetOption]
        [CollectionItemType(typeof(C1SiteMapNodeBinding))]
        public List<C1SiteMapNodeBinding> DataBindings
        {
            get
            {
                return _bindings;
            }
        }

        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this web server control.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs after a node is data bound.
        /// </summary>
        [C1Category("Category.Action"), C1Description("C1SiteMap.NodeDataBound")]
        public event C1SiteMapNodeEventHandler NodeDataBound;

        /// <summary>
        /// Occurs after a node is created.
        /// </summary>
        [C1Category("Category.Action"), C1Description("C1SiteMap.NodeCreated")]
        public event C1SiteMapNodeEventHandler NodeCreated;

        protected internal virtual void OnNodeDataBound(C1SiteMapNodeEventArgs e)
        {
            if (this.NodeDataBound != null)
            {
                this.NodeDataBound(this, e);
            }
        }

        protected internal virtual void OnNodeCreated(C1SiteMapNodeEventArgs e)
        {
            if (this.NodeCreated != null)
            {
                this.NodeCreated(this, e);
            }
        }

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }

            if (!IsDesignMode && (!string.IsNullOrEmpty(this.DataSourceID) || this.DataSource != null))
            {
                this.RequiresDataBinding = true;
            }

            BackupNodesWithTemplate();
        }

        /// <summary>
        /// Due to node's template is not serialized, we need to remember them, and retore after Deserialized.
        /// </summary>
        private void BackupNodesWithTemplate()
        {
            int index = 0;
            foreach (C1SiteMapNode node in this.Nodes)
            {
                node.StaticKey = string.Format("{0}_{1}", this.ID, index);
                _templateNodes.Add(node.StaticKey, node);

                BackupNodesWithTemplate(node);

                index++;
            }
        }

        private void BackupNodesWithTemplate(C1SiteMapNode node)
        {
            int i = 0;
            foreach (C1SiteMapNode childNode in node.Nodes)
            {
                childNode.StaticKey = string.Format("{0}_{1}", node.StaticKey, i);
                _templateNodes.Add(childNode.StaticKey, childNode);

                BackupNodesWithTemplate(childNode);

                i++;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            //do data bound if bind to any datasource
            if (base.RequiresDataBinding && (!string.IsNullOrEmpty(this.DataSourceID) || (this.DataSource != null)))
            {
                this.EnsureDataBound();
            }

            //no need to create any child controls if no nodes there.
			if (this.Nodes.Count == 0)
				return;

            //prepare nodes template
            foreach (var node in Nodes)
            {
                PrepareNodesTemplate(node, 0);
            }

            _nodesContainer.Controls.Clear();
            this.Controls.Add(_nodesContainer);

            //add the first level
            foreach (C1SiteMapNode node in Nodes)
            {
                if (!_nodesContainer.Controls.Contains(node))
                {
                    _nodesContainer.Controls.Add(node);
                }
            }

            //add the clear fix div
            this.Controls.Add(_clearDiv);

            this.EnsureChildControls();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            EnsureChildControls();

            //if no nodes, no need to render anything, which is helpfull for deisgner to generate design time html.
			if (!IsDesignMode || Nodes.Count > 0)
			{
				base.Render(writer);
			}
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            string cssClass = "wijmo-c1sitemap ui-widget ui-widget-content ui-corner-all ui-helper-clearfix";

            if (!this.Enabled)
            {
                cssClass += " ui-state-disabled";
            }

            this.CssClass = string.Format("{0} {1}", this.CssClass, cssClass);

            base.AddAttributesToRender(writer);
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            _nodesContainer.Attributes.Add("class", "wijmo-c1sitemap-list");
            _nodesContainer.RenderControl(writer);

            _clearDiv.Attributes.Add("class", "ui-helper-clearfix"); 
            _clearDiv.RenderControl(writer);
        }

        protected override void PerformDataBinding()
        {
            base.PerformDataBinding();

            //at design time, the method will still be called if not bound to any data source.
            if (IsDesignMode && DataSource.GetType().Name == "HierarchicalSampleData")
                return;

            HierarchicalDataSourceView view = this.GetData(null);
            IHierarchicalEnumerable dataSource = view.Select();

            //Generate Nodes from datasource.
            if (dataSource != null)
            {
                GenerateChildNodesFromDataSource(dataSource);
            }
        }

        /// <summary>
        /// Binds a data source to the invoked server control and all its child controls.
        /// </summary>
        /// <param name="dataSource">Enumerable object to bind</param>
        private void GenerateChildNodesFromDataSource(IHierarchicalEnumerable dataSource)
        {
            this.Nodes.Clear();
            int index = 0;

            foreach (object item in dataSource)
            {
                index++;

                C1SiteMapNode newNode = new C1SiteMapNode(item, index, index);
                newNode.Level = 0;
                newNode.SiteMap = this;

                C1SiteMapNodeBinding binding = GetLevelBinding(0);
                newNode.DataBind(binding, dataSource, item);

                //fire OnNodeDataBound event.
                C1SiteMapNodeEventArgs arg = new C1SiteMapNodeEventArgs(newNode);
                OnNodeDataBound(arg);

                this.Nodes.Add(newNode);

                IHierarchyData childItems = dataSource.GetHierarchyData(item);
                if (childItems.HasChildren)
                {
                    IHierarchicalEnumerable childEnum = childItems.GetChildren();
                    if (childEnum != null)
                    {
                        newNode.GenerateChildNodesFromDataSource(childEnum, 1, index);
                    }
                }
            }
        }

        /// <summary>
        /// Prepare nodes tempalte and initialize nodes' template container. 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="level"></param>
        private void PrepareNodesTemplate(C1SiteMapNode node, int level)
        {
            node.Templated = false;
            node.Level = level;
            //better to initialize node's SiteMap here.
            node.SiteMap = this;

            ITemplate template = node.Template;

            if (!string.IsNullOrEmpty(node.StaticKey) && _templateNodes.ContainsKey(node.StaticKey))
            {
                template = _templateNodes[node.StaticKey].Template;
            }

            if (template == null)
            {
                //try get template from level setting
                C1SiteMapLevelSetting setting = GetLevelSetting(level);
                if (setting.NodeTemplate != null)
                {
                    template = setting.NodeTemplate;
                }
            }

            if (template != null)
            {
                node.Templated = true;
                HtmlGenericControl container = new HtmlGenericControl("div");
                node.TemplateContainer = container;
                template.InstantiateIn(container);
            }

            if (node.Nodes.Count > 0)
            {
                foreach (var curNode in node.Nodes)
                {
                    PrepareNodesTemplate(curNode, level + 1);
                }
            }
        }

        /// <summary>
        /// Get level settings for given level. If not set will return the default setting.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        internal C1SiteMapLevelSetting GetLevelSetting(int level)
        {
            foreach (var setting in LevelSettings)
            {
                if (setting.Level == level)
                    return setting;
            }

            return DefaultLevelSetting;
        }

        /// <summary>
        /// Get databinding for given level.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        internal C1SiteMapNodeBinding GetLevelBinding(int level)
        {
            foreach (var bind in _bindings)
            {
                if (bind.Level == level)
                    return bind;
            }

            return null;
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize Nodes.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeNodes()
        {
            return Nodes != null;
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize DataBindings.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeDataBindings()
        {
            return DataBindings != null && DataBindings.Count > 0;
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize LevelSettings.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeLevelSettings()
        {
            if (LevelSettings == null || LevelSettings.Count == 0)
                return false;

            foreach (C1SiteMapLevelSetting ls in LevelSettings)
            {
                if (ls.ShouldSerialize())
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the value indicate whether need to serialize DefaultLevelSetting.
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerializeDefaultLevelSetting()
        {
            return DefaultLevelSetting.ShouldSerialize();
        }

        #endregion

        #region IPostBackDataHandler

        public void RaisePostDataChangedEvent()
        {
        }

        public bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            //as LevelSettings.Template is not serialized/deserialized, we need to remember them and restore after json restore.
            Dictionary<int, C1SiteMapLevelSetting> backSettings = BackupLevelSettingsTemplate();

            //deserialize by json.
            Hashtable data = this.JsonSerializableHelper.GetJsonData(Page.Request.Form);
            this.RestoreStateFromJson(data);

            //restore LevelSetting.Template
            RestoreLevelSetingsTemplate(backSettings);

            return true;
        }

        private void RestoreLevelSetingsTemplate(Dictionary<int, C1SiteMapLevelSetting> backSettings)
        {
            foreach (var setting in this.LevelSettings)
            {
                if (backSettings.ContainsKey(setting.Level))
                {
                    setting.NodeTemplate = backSettings[setting.Level].NodeTemplate;
                }
            }
        }

        private Dictionary<int, C1SiteMapLevelSetting> BackupLevelSettingsTemplate()
        {
            Dictionary<int, C1SiteMapLevelSetting> backSettings = new Dictionary<int, C1SiteMapLevelSetting>();

            foreach (var setting in this.LevelSettings)
            {
                C1SiteMapLevelSetting newSetting = new C1SiteMapLevelSetting();
                newSetting.Level = setting.Level;
                newSetting.NodeTemplate = setting.NodeTemplate;

                backSettings[newSetting.Level] = newSetting;
            }

            return backSettings;
        }

        #endregion

        #region IC1Serializable Layout

        /// <summary>
        /// Saves the control layout properties to the file.
        /// </summary>
        /// <param name="filename">
        /// The file where the values of the layout properties will be saved.
        /// </param> 
        public void SaveLayout(string filename)
        {
            C1SiteMapSerializer sz = new C1SiteMapSerializer(this);
            sz.SaveLayout(filename);
        }

        /// <summary>
        /// Saves control layout properties to the stream.
        /// </summary>
        /// <param name="stream">
        /// The stream where the values of layout properties will be saved.
        /// </param> 
        public void SaveLayout(Stream stream)
        {
            C1SiteMapSerializer sz = new C1SiteMapSerializer(this);
            sz.SaveLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties from the file.
        /// </summary>
        /// <param name="filename">
        /// The file where the values of layout properties will be loaded.
        /// </param> 
        public void LoadLayout(string filename)
        {
            LoadLayout(filename, LayoutType.All);
        }

        /// <summary>
        /// Load control layout properties from the stream.
        /// </summary>
        /// <param name="stream">
        /// The stream where the values of layout properties will be loaded.
        /// </param> 
        public void LoadLayout(Stream stream)
        {
            LoadLayout(stream, LayoutType.All);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the file.
        /// </summary>
        /// <param name="filename">
        /// The file where the values of layout properties will be loaded.
        /// </param> 
        /// <param name="layoutTypes">
        /// The layout types to load.
        /// </param>
        public void LoadLayout(string filename, LayoutType layoutTypes)
        {
            C1SiteMapSerializer sz = new C1SiteMapSerializer(this);
            sz.LoadLayout(filename, layoutTypes);
        }

        /// <summary>
        /// Loads the control layout properties with specified types from the stream.
        /// </summary>
        /// <param name="stream">
        /// The stream where the values of the layout properties will be loaded.
        /// </param> 
        /// <param name="layoutTypes">
        /// The layout types to load.
        /// </param>
        public void LoadLayout(Stream stream, LayoutType layoutTypes)
        {
            C1SiteMapSerializer sz = new C1SiteMapSerializer(this);
            sz.LoadLayout(stream, layoutTypes);
        }


        #endregion

        #region IC1SiteMapNodeCollectionOwner

        /// <summary>
        /// There is no such owner for <see cref="C1SiteMap"/>.
        /// </summary>
        IC1SiteMapNodeCollectionOwner IC1SiteMapNodeCollectionOwner.Owner
        {
            get { return null; }
        }

        #endregion

    }
}
