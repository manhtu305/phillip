using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using System.Web.UI;
using System.Web;
using System.Globalization;
using System.ComponentModel.Design;


namespace C1.Web.Wijmo.Controls.C1SiteMap
{
    /// <summary>
    /// Represent serializer used by <see cref="C1SiteMap"/>. 
    /// </summary>
    public class C1SiteMapSerializer: C1BaseSerializer<C1SiteMap, C1SiteMapNode, IC1SiteMapNodeCollectionOwner>
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="C1SiteMapSerializer"/> class.
        /// </summary>
        public C1SiteMapSerializer(Object obj)
            : base(obj)
        {
        }

        #region ** protected override

		/// <summary>
		/// Override this method if you want to add deserialized object 
		/// to array or collection using custom way.
		/// </summary>
		/// <example>
		/// Override example:
		/// <code>
		/// protected override bool OnAddItem(object collection, object item)
		/// {
		///     if (collection is C1MenuItemCollection)
		///     {
		///         ((C1MenuItemCollection)collection).Add((C1MenuItem)item);
		///         return true;
		///     }
		///     return false;
		/// }
		/// </code>
		/// </example>
		/// <remarks>This method preliminary used by control developers.</remarks>
		/// <param name="collection"></param>
		/// <param name="item"></param>
		/// <returns>Return true if object is added to array or collection.</returns>
        protected override bool OnAddItem(object collection, object item)
        {
            if (collection is C1SiteMapNodeCollection)
            {
                ((C1SiteMapNodeCollection)collection).Add((C1SiteMapNode)item);
                return true;
            }
            return base.OnAddItem(collection, item);
        }

		/// <summary>
		/// Override this method to implement custom clear for deserialized array.
		/// </summary>
		/// <example>
		/// Override example:
		/// <code>
		/// protected override bool OnClearItems(object collection)
		/// {
		///     if (collection is C1MenuItemCollection)
		///     {
		///         ((C1MenuItemCollection)collection).Clear();
		///         return true;
		///     }
		///     return false;
		/// }
		/// </code>
		/// </example>
		/// <remarks>This method preliminary used by control developers.</remarks>
		/// <param name="collection"></param>
		/// <returns>Return true if array or collection is cleared.</returns>
        protected override bool OnClearItems(object collection)
        {
            if (collection is C1SiteMapNodeCollection)
            {
                ((C1SiteMapNodeCollection)collection).Clear();
                return true;
            }
            return base.OnClearItems(collection);
        }

        #endregion
    }
}