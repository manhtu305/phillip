﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	/// <summary>
	/// Defines the relationship between data item and the series data it is binding to.
	/// </summary>
	public class C1PieChartBinding : C1Chart.C1ChartBinding
	{

		#region fields

		private string _dataField = string.Empty;
		private string _offsetField = string.Empty;
		private string _labelField = string.Empty;
		#endregion

		#region constructors

		/// <summary>
		/// Create new instance of the C1PieChartBinding class.
		/// </summary>
		public C1PieChartBinding() 
			: base()
		{ }

		/// <summary>
		/// Create new instance of the C1PieChartBinding class.
		/// </summary>
		/// <param name="dataMember">Data member to bind chart data.</param>
		public C1PieChartBinding(string dataMember)
			: base(dataMember)
		{ }
		#endregion

		#region properties

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1PieChartSeries data.
		/// </summary>
		[C1Description("C1Chart.PieChartBinding.DataField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public string DataField
		{
			get
			{
				return _dataField;
			}
			set
			{
				_dataField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1PieChartSeries offset.
		/// </summary>
		[C1Description("C1Chart.PieChartBinding.OffsetField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public string OffsetField
		{
			get
			{
				return _offsetField;
			}
			set
			{
				_offsetField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1PieChartSeries label.
		/// </summary>
		[C1Description("C1Chart.PieChartBinding.LabelField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public string LabelField
		{
			get
			{
				return _labelField;
			}
			set
			{
				_labelField = value;
			}
		}

		#region hidden properties
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string XField
		{
			get
			{
				return base.XField;
			}
			set
			{
				base.XField = value;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string YField
		{
			get
			{
				return base.YField;
			}
			set
			{
				base.YField = value;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ChartDataXFieldType XFieldType
		{
			get
			{
				return base.XFieldType;
			}
			set
			{
				base.XFieldType = value;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ChartDataYFieldType YFieldType
		{
			get
			{
				return base.YFieldType;
			}
			set
			{
				base.YFieldType = value;
			}
		}

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool IsTrendline
        {
            get
            {
                return base.IsTrendline;
            }
            set
            {
                base.IsTrendline = value;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override TrendlineFitType TrendlineFitType
        {
            get
            {
                return base.TrendlineFitType;
            }
            set
            {
                base.TrendlineFitType = value;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int TrendlineOrder
        {
            get
            {
                return base.TrendlineOrder;
            }
            set
            {
                base.TrendlineOrder = value;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int TrendlineSampleCount
        {
            get
            {
                return base.TrendlineSampleCount;
            }
            set
            {
                base.TrendlineSampleCount = value;
            }
        }

		#endregion

		#endregion

		#region ICloneable
		/// <summary>
		/// Clone the chart binding.
		/// </summary>
		/// <returns>
		/// Returns the copy of a chart binding object.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override object Clone()
		{
			C1PieChartBinding binding = new C1PieChartBinding();
			binding.DataMember = this.DataMember;
			binding.DataField = this.DataField;
			binding.OffsetField = this.OffsetField;
			binding.LabelField = this.LabelField;
			return binding;
		}
		#endregion
	}
}
