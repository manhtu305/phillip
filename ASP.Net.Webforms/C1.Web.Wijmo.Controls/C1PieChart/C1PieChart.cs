﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using System.Collections;
using System.IO;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Reflection;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	/// <summary>
	/// The C1PieChart class contains properties specific to the C1PieChart.
	/// Pie charts are commonly used to display simple values.
	/// Each slice in the C1PieChart is represented by the PieChartSeries.
	/// </summary>
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1PieChart.C1PieChartDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1PieChart.C1PieChartDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1PieChart.C1PieChartDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1PieChart.C1PieChartDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1PieChart runat=server ></{0}:C1PieChart>")]
	[ToolboxBitmap(typeof(C1PieChart), "C1PieChart.png")]
	[LicenseProviderAttribute()]
	public partial class C1PieChart : C1ChartCore<PieChartSeries, PieChartAnimation, C1PieChartBinding>, IC1Serializable
	{

		private bool _productLicensed = false;
		private bool _shouldNag;

		/// <summary>
		/// Initializes a new instance of the C1PieChart class.
		/// </summary>
		public C1PieChart()
			: base()
		{
			VerifyLicense();
			this.InitChart();
		}

		private void VerifyLicense() 
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1PieChart), this, false);
			_shouldNag = licinfo.ShouldNag;
			#if GRAPECITY 
				_productLicensed = licinfo.IsValid; 
			#else 
				_productLicensed = licinfo.IsValid || licinfo.IsLocalHost; 
			#endif 
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this); 
			base.OnPreRender(e); 
		}

		#region options
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override List<AnnotationBase> Annotations
		{
			get
			{
				return new List<AnnotationBase>();
			}
		}
		#endregion options

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1PieChartSerializer sz = new C1PieChartSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1PieChartSerializer sz = new C1PieChartSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1PieChartSerializer sz = new C1PieChartSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1PieChartSerializer sz = new C1PieChartSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations

		#region DataBinding
		/// <summary>
		/// Binds data from the data source to the chart control.
		/// </summary>
		/// <param name="dataSource">
		/// The <see cref ="T:System.Collections.IEnumerable"> list of data returned from 
		/// a <see cref = "M:System.Web.UI.WebControls.DataBoundControl.PerformSelect()"> method call.
		/// </param>
		protected override void PerformDataBinding(IEnumerable dataSource)
		{
			if (dataSource == null || DataBindings.Count == 0)
			{
				return;
			}
			if (IsDesignMode)
			{
				return;
			}
			IEnumerator e = dataSource.GetEnumerator();
			SeriesList.Clear();
			int i = 0;
			while (e.MoveNext())
			{
				object o = e.Current;
				if (o != null)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
					if (i >= SeriesList.Count)
					{
						SeriesList.Add(new PieChartSeries());
					}
					InternalDataBind(o, pdc, SeriesList[i], DataBindings[0]);
					i++;
				}
			}
		}

		protected override void InternalDataBind(object data, PropertyDescriptorCollection pdc, PieChartSeries series, C1PieChartBinding binding)
		{
			if (!String.IsNullOrEmpty(binding.DataField))
			{
				object dataValue = GetFieldValue(data, pdc, binding.DataField);
				if (dataValue != null)
				{
					series.Data = Double.Parse(dataValue.ToString());
				}
			}
			if (!String.IsNullOrEmpty(binding.LabelField))
			{
				object labelValue = GetFieldValue(data, pdc, binding.LabelField);
				if (labelValue != null)
				{
					series.Label = labelValue.ToString();
				}
			}
			if (!String.IsNullOrEmpty(binding.OffsetField))
			{
				object offsetValue = GetFieldValue(data, pdc, binding.OffsetField);
				if (offsetValue != null)
				{
					series.Offset = Int32.Parse(offsetValue.ToString());
				}
			}
			if (!String.IsNullOrEmpty(binding.HintField))
			{
				object hintValue = GetFieldValue(data, pdc, binding.HintField);
				if (hintValue != null)
				{
					series.HintContent = hintValue.ToString();
				}
			}
		}
		#endregion
	}
}
