﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Drawing;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.IO;
using System.Reflection;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Chart
{
    /// <summary>
    /// The C1ChartNavigator class contains properties specific to the C1ChartNavigator.
    /// </summary>
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1ChartNavigator.C1ChartNavigatorDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1ChartNavigator.C1ChartNavigatorDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer("C1.Web.Wijmo.Controls.Design.C1ChartNavigator.C1ChartNavigatorDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
    [Designer("C1.Web.Wijmo.Controls.Design.C1ChartNavigator.C1ChartNavigatorDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1ChartNavigator runat=server ></{0}:C1ChartNavigator>")]
    [ToolboxBitmap(typeof(C1ChartNavigator), "C1ChartNavigator.png")]
    [LicenseProviderAttribute()]
    [WidgetDependencies(
        typeof(WijChartNavigator),
        "extensions.c1chartnavigator.js",
        ResourcesConst.C1WRAPPER_PRO
    )]
    public partial class C1ChartNavigator : C1ChartCore<LineChartSeries, LineChartAnimation, C1ChartBinding>
    {
        private bool _productLicensed = false;
        private bool _shouldNag;

        /// <summary>
        /// Initializes a new instance of the C1ChartNavigator class.
        /// </summary>
        public C1ChartNavigator()
            : base()
        {
            VerifyLicense();
            XAxis = new ChartAxis();
            XAxis.GridMajor.Visible = false;
        }

        private void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ChartNavigator), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY 
                _productLicensed = licinfo.IsValid; 
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">
        /// An <see cref="T:System.EventArgs"/> object that contains the event data.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }
            base.OnInit(e);
        }

        /// <summary>
        /// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
        /// </summary>
        /// <param name="e">
        /// An <see cref="T:System.EventArgs"/> object that contains the event data.
        /// </param>
        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }

        /// <summary>
        /// Gets the series data of the specified series.
        /// </summary>
        /// <param name="series">
        /// The specified series.
        /// </param>
        /// <returns>
        /// Returns the series data which retrieved from the specified series.
        /// </returns>
        protected override ChartSeriesData GetSeriesData(LineChartSeries series)
        {
            return series.Data;
        }

        internal override bool IsWijMobileControl
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Name of the theme that will be used to style the widgets. 
        /// Available embedded themes include: aristo, midnight, and ui-lightness. 
        /// Note that only one theme can be used for the whole page at one time. 
        /// Please make sure that all widget extenders have the same Theme value. 
        /// </summary>
        [C1Description("C1Base.Theme")]
        [C1Category("Category.Behavior")]
        [DefaultValue("aristo")]
        [TypeConverter(typeof(WijmoThemeNameConverter))]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [RefreshProperties(RefreshProperties.All)]
        public override string Theme
        {
            get
            {
                return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
            }
            set
            {
                if (this.Theme != value)
                {
                    WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
                }
            }
        }

        #region Hidden Chart Options

        [Browsable(false)]
        [Json(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool AutoResize
        {
            get
            {
                return base.AutoResize;
            }
            set
            {
                base.AutoResize = value;
            }
        }

        [Browsable(false)]
        [Json(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override LineChartAnimation Animation
        {
            get
            {
                return base.Animation;
            }
            set
            {
                base.Animation = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override List<ChartStyle> SeriesHoverStyles
        {
            get
            {
                return base.SeriesHoverStyles;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int MarginTop
        {
            get
            {
                return base.MarginTop;
            }
            set
            {
                base.MarginTop = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int MarginRight
        {
            get
            {
                return base.MarginRight;
            }
            set
            {
                base.MarginRight = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int MarginBottom
        {
            get
            {
                return base.MarginBottom;
            }
            set
            {
                base.MarginBottom = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int MarginLeft
        {
            get
            {
                return base.MarginLeft;
            }
            set
            {
                base.MarginLeft = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartStyle TextStyle
        {
            get
            {
                return base.TextStyle;
            }
            set
            {
                base.TextStyle = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartTitle Header
        {
            get
            {
                return base.Header;
            }
            set
            {
                base.Header = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartTitle Footer
        {
            get
            {
                return base.Footer;
            }
            set
            {
                base.Footer = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartLegend Legend
        {
            get
            {
                return base.Legend;
            }
            set
            {
                base.Legend = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartAxes Axis
        {
            get;
            set;
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartHint Hint
        {
            get
            {
                return base.Hint;
            }
            set
            {
                base.Hint = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool ShowChartLabels
        {
            get
            {
                return base.ShowChartLabels;
            }
            set
            {
                base.ShowChartLabels = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartStyle ChartLabelStyle
        {
            get
            {
                return base.ChartLabelStyle;
            }
            set
            {
                base.ChartLabelStyle = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override string ChartLabelFormatString
        {
            get
            {
                return base.ChartLabelFormatString;
            }
            set
            {
                base.ChartLabelFormatString = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool DisableDefaultTextStyle
        {
            get
            {
                return base.DisableDefaultTextStyle;
            }
            set
            {
                base.DisableDefaultTextStyle = value;
            }
        }

        [Json(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool Shadow
        {
            get
            {
                return base.Shadow;
            }
            set
            {
                base.Shadow = value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets an object value that indicates the X-Axis of the inner chart layout.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.XAxis")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        [C1Category("Category.Misc")]
        [Layout(LayoutType.Appearance)]
        public ChartAxis XAxis { get; set; }

        /// <summary>
        /// Gets or sets a string value that indicates the jquery selector of target charts.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.TargetSelector")]
        [Json(false)]
        [DefaultValue("")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public string TargetSelector
        {
            get
            {
                return GetPropertyValue<string>("TargetSelector", "");
            }
            set
            {
                SetPropertyValue<string>("TargetSelector", value);
            }
        }

        [WidgetOption]
        [WidgetOptionName("targetSelector")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ClientTargetSelector
        {
            get
            {
                return GetClientSelector(this.TargetSelector);
            }
        }

        /// <summary>
        /// Gets or sets the left value of navigator range.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.RangeMin")]
        [WidgetOption]
        [DefaultValue(null)]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Data)]
        public double? RangeMin
        {
            get
            {
                return GetPropertyValue<double?>("RangeMin", null);
            }
            set
            {
                SetPropertyValue<double?>("RangeMin", value);
            }
        }

        /// <summary>
        /// Gets or sets the right value of navigator range.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.RangeMax")]
        [WidgetOption]
        [DefaultValue(null)]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Data)]
        public double? RangeMax
        {
            get
            {
                return GetPropertyValue<double?>("RangeMax", null);
            }
            set
            {
                SetPropertyValue<double?>("RangeMax", value);
            }
        }

        /// <summary>
        /// Gets or sets a value to determine the size of each changing of navigator range.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.Step")]
        [WidgetOption]
        [DefaultValue(null)]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Data)]
        public double? Step
        {
            get
            {
                return GetPropertyValue<double?>("Step", null);
            }
            set
            {
                SetPropertyValue<double?>("Step", value);
            }
        }

        /// <summary>
        /// Gets or sets an object that used to set the indicator tooltip beside the range handlers.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.Indicator")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public new NavigatorIndicator Indicator { get; set; }

        #endregion

        #region Client Events

        /// <summary>
        /// This event is raised when range of navigator is changing.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.OnClientUpdating")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("updating")]
        [DefaultValue("")]
        [C1Category("Category.ClientSideEvents")]
        [Layout(LayoutType.Behavior)]
        public string OnClientUpdating
        {
            get
            {
                return GetPropertyValue("OnClientUpdating", "");
            }
            set
            {
                SetPropertyValue("OnClientUpdating", value);
            }
        }

        /// <summary>
        /// This event is raised when range of navigator has been changed.
        /// </summary>
        [C1Description("C1Chart.ChartNavigator.OnClientUpdated")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("updated")]
        [DefaultValue("")]
        [C1Category("Category.ClientSideEvents")]
        [Layout(LayoutType.Behavior)]
        public string OnClientUpdated
        {
            get
            {
                return GetPropertyValue("OnClientUpdated", "");
            }
            set
            {
                SetPropertyValue("OnClientUpdated", value);
            }
        }

        #endregion

        protected override void ConvertAxisOADate()
        {
            if (this.SeriesList.Count > 0)
            {
                if (IsDateTimeSeries("x"))
                {
                    if (RangeMin != null && NeedConvertOADate((double)RangeMin))
                    {
                        RangeMin *= UnitRate;
                    }
                    if (RangeMax != null && NeedConvertOADate((double)RangeMax))
                    {
                        RangeMax *= UnitRate;
                    }
                    ConvertAxisByOADate(this.XAxis);
                }
            }
        }

        private string GetClientSelector(string selector)
        {
            string[] selectors = selector.Split(',');
            for (int i = 0; i < selectors.Length; i++)
            {
                string s = selectors[i];
                if (s.StartsWith("#"))
                {
                    Control c = this.NamingContainer.FindControl(s.Substring(1));
                    if (c != null)
                    {
                        selectors[i] = string.Format("#{0}", c.ClientID);
                    }
                }
            }
            return string.Join(",", selectors);
        }
    }
}
