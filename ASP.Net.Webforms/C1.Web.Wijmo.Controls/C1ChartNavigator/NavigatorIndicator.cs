﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1Chart
{
    /// <summary>
    /// Represents the class for the Indicator object. The indicators appear besides the slider hanlders.
    /// </summary>
    public class NavigatorIndicator : Settings, IJsonEmptiable
    {
        #region Fields

        private ContentOption _content;

        #endregion

        #region Constructor

        public NavigatorIndicator()
        {

        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value that will be shown in the content part of the tooltip.
        /// </summary>
        [C1Description("C1Chart.NavigatorIndicator.Content")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Data)]
        public ContentOption Content
        {
            get
            {
                if (_content == null)
                {
                    _content = new ContentOption();
                }
                return _content;
            }
            set
            {
                _content = value;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates the format of indicator content.
        /// </summary>
        [C1Description("C1Chart.NavigatorIndicator.Format")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue("")]
        [C1Category("Category.Styles")]
        [Layout(LayoutType.Styles)]
        public string Format
        {
            get
            {
                return this.GetPropertyValue<string>("Format", "");
            }
            set
            {
                this.SetPropertyValue<string>("Format", value);
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether to show the tooltip.
        /// </summary>
        [C1Description("C1Chart.NavigatorIndicator.Visible")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(true)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public bool Visible
        {
            get
            {
                return this.GetPropertyValue<bool>("Visible", true);
            }
            set
            {
                this.SetPropertyValue<bool>("Visible", value);
            }
        }

        #endregion

        public bool IsEmpty
        {
            get { return !ShouldSerialize(); }
        }

        internal bool ShouldSerialize()
        {
            if (!Visible)
                return true;
            if (!String.IsNullOrEmpty(Format))
                return true;
            if (!((IJsonEmptiable)Content).IsEmpty)
                return true;
            return false;
        }
    }
}
