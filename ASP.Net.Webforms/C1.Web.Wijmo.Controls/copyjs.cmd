
@echo off
setlocal enabledelayedexpansion
cd /d "%~dp0"

set binariesRoot=%1
if '%binariesRoot%' == '' goto :EOF

set binariesRoot=%binariesRoot:"=%
if exist "%binariesRoot%\Resources" rd /s /q "%binariesRoot%\Resources"

xcopy /y /r ..\..\Widgets\Wijmo\External\jquery.bgiframe.js "%binariesRoot%\Resources\scripts\base\"
xcopy /y /r ..\..\Widgets\Wijmo\External\jquery.ui.position.js "%binariesRoot%\Resources\scripts\base\"
xcopy /y /r ..\..\Widgets\Wijmo\External\cultures\globalize.cultures.js "%binariesRoot%\Resources\scripts\"
xcopy /y /r ..\..\Widgets\Wijmo\External\globalize.min.js "%binariesRoot%\Resources\scripts\base\"
xcopy /y /r ..\..\Widgets\Wijmo\External\raphael-min.js "%binariesRoot%\Resources\scripts\base\"
xcopy /y /r ..\..\Widgets\Wijmo\External\jquery.mousewheel.min.js "%binariesRoot%\Resources\scripts\base\"

xcopy /y /r ..\C1.Web.Wijmo.Extenders\Resources\scripts\bootstrap.wijmo.js "%binariesRoot%\Resources\scripts\"
xcopy /y /r ..\C1.Web.Wijmo.Extenders\Resources\scripts\c1json2.js "%binariesRoot%\Resources\scripts\"
xcopy /y /r ..\C1.Web.Wijmo.Extenders\Resources\scripts\framework.js "%binariesRoot%\Resources\scripts\"

xcopy /y /r Resources\scripts\swfobject.min.js "%binariesRoot%\Resources\scripts\"
xcopy /y /r Resources\scripts\swfupload.min.js "%binariesRoot%\Resources\scripts\"
xcopy /y /r Resources\scripts\widgets\wijreportviewer.js "%binariesRoot%\Resources\scripts\widgets\"

set flag=0
for /f "tokens=1* delims=" %%i in (C1.Web.Wijmo.Controls.3.csproj) do (
  set row=%%i
  set replace1=!row:Release=!
  set replace2=!row:GrapeCity=!

  if not '!row!' == '!replace1!' (
    if not '!row!' == '!replace2!' (
      set flag=1
    )
  )

  if '!flag!' == '1' (    
    set replace3=!row:^<EmbeddedResource Include=!
    set replace4=!row:cultures=!
    set replace5=!row:/^>=!

    if not '!row!' == '!replace3!' (
      if '!row!' == '!replace4!' (
        if '!row!' == '!replace5!' (
          set jsfile=!replace3: =!
          set jsfile=!jsfile:"=!
          set jsfile=!jsfile:~1,-1!
          set jsfile=!jsfile:.min.js=.js!
          set prefix=!jsfile:~0,2!

          if '!prefix!' == '..' (
            call :copyWidgets !jsfile!
          )

          if not '!prefix!' == '..' (        
            call :copyResource !jsfile!
          )
        )
      )
    )
  )
)

del /f /q "%binariesRoot%\Resources\scripts\wijmo\bootstrap.wijmo.js"
del /f /q "%binariesRoot%\Resources\scripts\wijmo\c1json2.js"
del /f /q "%binariesRoot%\Resources\scripts\wijmo\framework.js"
del /f /q "%binariesRoot%\Resources\scripts\wijmo\jquery.wijmo.mobile.js"
goto :EOF

:copyWidgets
  @echo copy "%1" ...
  xcopy /y /r "%1" "%binariesRoot%\Resources\scripts\wijmo\"
goto :EOF

:copyResource
  @echo copy "%1" ...
  if not exist "%binariesRoot%\Resources\scripts\extensions" md "%binariesRoot%\Resources\scripts\extensions"
  copy /y "%1" "%binariesRoot%\%1"
goto :EOF

