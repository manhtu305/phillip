﻿using System;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	public class C1CandlestickChartSerializer : C1ChartSerializer<C1CandlestickChart>
	{
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1BubbleChartSerializer"/> class.
		/// </summary>
		/// <param name="obj"></param>
		public C1CandlestickChartSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
