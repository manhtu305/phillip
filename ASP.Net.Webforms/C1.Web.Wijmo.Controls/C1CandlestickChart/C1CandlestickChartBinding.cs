﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	/// <summary>
	/// Defines the relationship between data item and the series data it is binding to.
	/// </summary>
	public class C1CandlestickChartBinding : C1ChartBinding
	{
		#region  Fields
		private string _highField = string.Empty;
		private string _lowField = string.Empty;
		private string _openField = string.Empty;
		private string _closeField = string.Empty;
		private string _hintField = string.Empty;
		private ChartDataXFieldType _xFieldType = ChartDataXFieldType.DateTime;
		#endregion

		#region Ctors
		/// <summary>
		/// Initializes a new instance of the C1CandlestickChartBinding class.
		/// </summary>
		public C1CandlestickChartBinding()
			: base()
		{
		}

		/// <summary>
		/// Initializes a new instance of the C1CandlestickChartBinding class.
		/// </summary>
		/// <param name="dataMember">Data member to bind chart data.</param>
		public C1CandlestickChartBinding(string dataMember)
			: base(dataMember)
		{
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the type of x field from the data source that indicates C1Chart x value type.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.XFieldType")]
		[DefaultValue(ChartDataXFieldType.DateTime)]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public override ChartDataXFieldType XFieldType
		{
			get
			{
				return _xFieldType;
			}
			set
			{
				_xFieldType = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1CandlestickChart High values.
		/// </summary>
		[C1Description("C1CandlestickChartBinding.HighField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
		public virtual string HighField
		{
			get
			{
				return _highField;
			}
			set
			{
				_highField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1CandlestickChart Low values.
		/// </summary>
		[C1Description("C1CandlestickChartBinding.LowField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
		public virtual string LowField
		{
			get
			{
				return _lowField;
			}
			set
			{
				_lowField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1CandlestickChart Open values.
		/// </summary>
		[C1Description("C1CandlestickChartBinding.OpenField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
		public virtual string OpenField
		{
			get
			{
				return _openField;
			}
			set
			{
				_openField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1CandlestickChart Close values.
		/// </summary>
		[C1Description("C1CandlestickChartBinding.CloseField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
		public virtual string CloseField
		{
			get
			{
				return _closeField;
			}
			set
			{
				_closeField = value;
			}
		}

		#region Hidden
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string YField
		{
			get
			{
				return base.YField;
			}
			set
			{
				base.YField = value;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ChartDataYFieldType YFieldType
		{
			get
			{
				return base.YFieldType;
			}
			set
			{
				base.YFieldType = value;
			}
		}
		#endregion
		#endregion

		#region ICloneable
		/// <summary>
		/// Clone the chart binding.
		/// </summary>
		/// <returns>
		/// Returns the copy of a chart binding.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override object Clone()
		{
			C1CandlestickChartBinding binding = new C1CandlestickChartBinding();
			binding.DataMember = this.DataMember;
			binding.XField = this.XField;
			binding.HighField = this.HighField;
			binding.LowField = this.LowField;
			binding.OpenField = this.OpenField;
			binding.CloseField = this.CloseField;
			binding.HintField = this.HintField;
			binding.XFieldType = this.XFieldType;

			binding.Visible = this.Visible;
			binding.LegendEntry = this.LegendEntry;
			binding.Label = this.Label;

			return binding;
		}
		#endregion		
	}
}
