﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Web.UI;
using System.Collections;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Data;
using System.Collections.Generic;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1CandlestickChart.C1CandlestickChartDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1CandlestickChart.C1CandlestickChartDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1CandlestickChart.C1CandlestickChartDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1CandlestickChart.C1CandlestickChartDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1CandlestickChart runat=server ></{0}:C1CandlestickChart>")]
	[ToolboxBitmap(typeof(C1CandlestickChart), "C1CandlestickChart.png")]
	[LicenseProviderAttribute()]
	public partial class C1CandlestickChart : C1ChartCore<CandlestickChartSeries, ChartAnimation, C1CandlestickChartBinding>, IC1Serializable
	{
		#region ** Fields
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion end ** Fields

		#region ** Ctor
		/// <summary>
		/// Initializes a new instance of the C1CandlestickChart class.
		/// </summary>
		public C1CandlestickChart()
			:base()
		{
			VerifyLicense();

			this.InitChart();
		}		

		private void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1CandlestickChart), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY 
				_productLicensed = licinfo.IsValid; 
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion end ** Ctor

		#region ** Override Methods
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
		#endregion end ** Override Methods

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1CandlestickChartSerializer sz = new C1CandlestickChartSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1CandlestickChartSerializer sz = new C1CandlestickChartSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1CandlestickChartSerializer sz = new C1CandlestickChartSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1CandlestickChartSerializer sz = new C1CandlestickChartSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations

		#region ** DataBinding
		private int _dataBindingIndex = -1;

		/// <summary>
		/// Binds data from the data source to the chart control.
		/// </summary>
		/// <param name="dataSource">
		/// The <see cref ="T:System.Collections.IEnumerable"> list of data returned from 
		/// a <see cref = "M:System.Web.UI.WebControls.DataBoundControl.PerformSelect()"> method call.
		/// </param>
		protected override void PerformDataBinding(IEnumerable dataSource)
		{
			if (dataSource == null || DataBindings.Count == 0)
			{
				return;
			}
			if (IsDesignMode)
			{
				return;
			}

			ResetSeriesList();

			for (int i = 0; i < DataBindings.Count; i++)
			{
				_dataBindingIndex = i;
				C1CandlestickChartBinding binding = DataBindings[i];
				if (DataSource != null && DataSource.GetType() == typeof(DataSet) && !string.IsNullOrEmpty(binding.DataMember) && !IsBoundUsingDataSourceID)
				{
					this.DataMember = binding.DataMember;
					var ov = this.GetData();
					DataSourceViewSelectCallback cv = new DataSourceViewSelectCallback(OnDataSourceViewSelectCallBack);
					ov.Select(DataSourceSelectArguments.Empty, cv);
				}
				else
				{
					OnDataSourceViewSelectCallBack(dataSource);
				}
			}
		}

		private void OnDataSourceViewSelectCallBack(IEnumerable obj)
		{
			IEnumerator e = obj.GetEnumerator();
			C1CandlestickChartBinding binding = this.DataBindings[_dataBindingIndex];
			while (e.MoveNext())
			{
				object o = e.Current;
				if (o != null)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
					if (_dataBindingIndex >= SeriesList.Count)
					{
						SeriesList.Add(new CandlestickChartSeries());
					}
					InternalDataBind(o, pdc, SeriesList[_dataBindingIndex], binding);
				}
			}
		}

		protected override void InternalDataBind(object data, PropertyDescriptorCollection pdc, CandlestickChartSeries series, C1CandlestickChartBinding binding)
		{
            if (binding.IsTrendline)
            {
                base.InternalDataBind(data, pdc, series, binding);
                return;
            }

            CandlestickChartSeriesData hlcsData = series.Data;

            if (!String.IsNullOrEmpty(binding.XField))
            {
                object xValue = GetFieldValue(data, pdc, binding.XField);
                AddDataToChart(binding.XFieldType.ToString(), xValue, hlcsData.X);
            }

            if (!String.IsNullOrEmpty(binding.HighField))
            {
                object highValue = GetFieldValue(data, pdc, binding.HighField);
                AddDoubleDataToChart(highValue, hlcsData.High);
            }

            if (!String.IsNullOrEmpty(binding.LowField))
            {
                object lowValue = GetFieldValue(data, pdc, binding.LowField);
                AddDoubleDataToChart(lowValue, hlcsData.Low);
            }

            if (!String.IsNullOrEmpty(binding.OpenField))
            {
                object openValue = GetFieldValue(data, pdc, binding.OpenField);
                AddDoubleDataToChart(openValue, hlcsData.Open);
            }

            if (!String.IsNullOrEmpty(binding.CloseField))
            {
                object closeValue = GetFieldValue(data, pdc, binding.CloseField);
                AddDoubleDataToChart(closeValue, hlcsData.Close);
            }

            if (!String.IsNullOrEmpty(binding.Label))
            {
                series.Label = binding.Label;
            }
            series.LegendEntry = binding.LegendEntry;
            series.Visible = binding.Visible;

            if (!String.IsNullOrEmpty(binding.HintField))
            {
                object hintValue = GetFieldValue(data, pdc, binding.HintField);
                if (hintValue != null)
                {
                    List<string> values = series.HintContents.ToList<string>();
                    values.Add(hintValue.ToString());
                    series.HintContents = values.ToArray();
                }
            }
		}
		#endregion end ** DataBinding
	}
}
