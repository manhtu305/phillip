﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	/// <summary>
	/// Defines the relationship between data item and the series data it is binding to.
	/// </summary>
	public class C1BubbleChartBinding : C1ChartBinding
	{
		#region ** fields
		private string _y1Field = string.Empty;
		#endregion

		#region ** properties
		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1BubbleChart Y1 values.
		/// </summary>
		[C1Description("C1BubbleChartBinding.Y1Field")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
        [C1Category("Category.Databindings")]
		public virtual string Y1Field
		{
			get
			{
				return _y1Field;
			}
			set
			{
				_y1Field = value;
			}
		}

		#endregion

		#region ** constructor
		/// <summary>
		/// Create new instance of the C1ChartBinding class.
		/// </summary>
		public C1BubbleChartBinding()
			: base()
		{
		}

		/// <summary>
		/// Create new instance of the C1ChartBinding class.
		/// </summary>
		/// <param name="dataMember">Data member to bind chart data.</param>
		public C1BubbleChartBinding(string dataMember)
			: base(dataMember)
		{

		}
		#endregion

		#region ** override methods
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override object Clone()
		{
			C1BubbleChartBinding binding = new C1BubbleChartBinding();
			binding.DataMember = this.DataMember;
			binding.HintField = this.HintField;
			binding.XField = this.XField;
			binding.XFieldType = this.XFieldType;
			binding.Y1Field = this.Y1Field;
			binding.YField = this.YField;
			binding.YFieldType = this.YFieldType;
			return binding;
		}
		#endregion
	}
}
