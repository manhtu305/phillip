﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;
using System.Collections;
using System.Data;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1BubbleChart.C1BubbleChartDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1BubbleChart.C1BubbleChartDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1BubbleChart.C1BubbleChartDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1BubbleChart.C1BubbleChartDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1BubbleChart runat=server ></{0}:C1BubbleChart>")]
	[ToolboxBitmap(typeof(C1BubbleChart), "C1BubbleChart.png")]
	[LicenseProviderAttribute()]
	public partial class C1BubbleChart : C1ChartCore<BubbleChartSeries, ChartAnimation, C1BubbleChartBinding>, IC1Serializable
	{
		#region ** fields
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion
		
		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1BubbleChart"/> class.
		/// </summary>
		public C1BubbleChart()
			: base()
		{
			VerifyLicense();
			this.InitChart();
		}

		private void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1BubbleChart), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY 
				_productLicensed = licinfo.IsValid; 
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		#region ** override methods
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this); 
			base.OnPreRender(e);
		}
		

		#endregion

		#region ** internal and private methods
		//private void ShowAbout()
		//{
		//    Type t = typeof(C1BubbleChart).Assembly.GetType("C1.Util.Licensing.ProviderInfo");
		//    MethodInfo _about = t.GetMethod("ShowAboutBox", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic,
		//        null, new Type[] { typeof(object) }, null);
		//    //_about =  t.GetMethod("ShowAboutBox", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
		//    _about.Invoke(this, new object[] { this });
		//}		
		#endregion

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1BubbleChartSerializer sz = new C1BubbleChartSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1BubbleChartSerializer sz = new C1BubbleChartSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1BubbleChartSerializer sz = new C1BubbleChartSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1BubbleChartSerializer sz = new C1BubbleChartSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion

		#region ** DataBinding
		private int _dataBindingIndex = -1;
		/// <summary>
		/// When overridden in a derived class, binds data from the data source to the
		///	control.
		/// </summary>
		/// <param name="dataSource"></param>
		protected override void PerformDataBinding(System.Collections.IEnumerable dataSource)
		{
			if (dataSource == null || DataBindings.Count == 0)
			{
				return;
			}
			if (IsDesignMode)
			{
				return;
			}

			ResetSeriesList();

			// fixed the multiple table databind issue.
			for (int i = 0; i < DataBindings.Count; i++)
			{
				_dataBindingIndex = i;
				C1BubbleChartBinding binding = DataBindings[i];
				// just handle the dataset datasource, I don't know the datamember can use the other types.
				if (DataSource != null && DataSource.GetType() == typeof(DataSet) && !string.IsNullOrEmpty(binding.DataMember) && !IsBoundUsingDataSourceID)
				{
					this.DataMember = binding.DataMember;
					var ov = this.GetData();
					DataSourceViewSelectCallback cv = new DataSourceViewSelectCallback(OnDataSourceViewSelectCallBack);
					ov.Select(DataSourceSelectArguments.Empty, cv);
				}
				else 
				{
					IEnumerator e = dataSource.GetEnumerator();
					while (e.MoveNext())
					{
						object o = e.Current;
						if (o != null)
						{
							PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
							if (i >= SeriesList.Count)
							{
								SeriesList.Add(new BubbleChartSeries());
							}
							InternalDataBind(o, pdc, SeriesList[i], binding);
						}
					}
				}
			}
			
			//base.PerformDataBinding(dataSource);
		}

		private void OnDataSourceViewSelectCallBack(IEnumerable obj)
		{
			IEnumerator e = obj.GetEnumerator();
			C1BubbleChartBinding binding = this.DataBindings[_dataBindingIndex];
			while (e.MoveNext())
			{
				object o = e.Current;
				if (o != null)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
					if (_dataBindingIndex >= SeriesList.Count)
					{
						SeriesList.Add(new BubbleChartSeries());
					}
					InternalDataBind(o, pdc, SeriesList[_dataBindingIndex], binding);
				}
			}
		}

		protected override void InternalDataBind(object data, System.ComponentModel.PropertyDescriptorCollection pdc, BubbleChartSeries series, C1BubbleChartBinding binding)
		{
            if (binding.IsTrendline)
            {
                base.InternalDataBind(data, pdc, series, binding);
                return;
            }

			BubbleChartSeriesData d = series.Data;
			object xValue = GetFieldValue(data, pdc, binding.XField);
			AddDataToChart(binding.XFieldType.ToString(), xValue, d.X);
			object yValue = GetFieldValue(data, pdc, binding.YField);
			AddDataToChart(binding.YFieldType.ToString(), yValue, d.Y);
			object y1Value = GetFieldValue(data, pdc, binding.Y1Field);
			AddDoubleDataToChart(y1Value, d.Y1);

			if (!String.IsNullOrEmpty(binding.HintField))
			{
				object hintValue = GetFieldValue(data, pdc, binding.HintField);
				if (hintValue != null)
				{
					List<string> values = series.HintContents.ToList<string>();
					values.Add(hintValue.ToString());
					series.HintContents = values.ToArray();
				}
			}

			if (String.IsNullOrEmpty(series.Label))
			{
				series.Label = binding.YField;
			}
		}
		#endregion
	}
}
