﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	public class C1BubbleChartSerializer : C1ChartSerializer<C1BubbleChart>
	{
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1BubbleChartSerializer"/> class.
		/// </summary>
		/// <param name="obj"></param>
		public C1BubbleChartSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
