﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Collections.Specialized;
using System.Collections;
using System.IO;
using System.Reflection;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Rating
{

	/// <summary>
	/// The C1Rating class contains properties specific to the C1Rating.
	/// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Rating.C1RatingDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Rating.C1RatingDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Rating.C1RatingDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Rating.C1RatingDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Rating runat=server></{0}:C1Rating>")]
	[ToolboxBitmap(typeof(C1Rating), ("C1Rating.png"))]
	[LicenseProviderAttribute()]
	public partial class C1Rating: C1TargetControlBase, IPostBackDataHandler, IC1Serializable
	{
		#region Fields

		#region constant css class

		private const string CONST_WIDGETCLASS = "wijmo-wijrating";
		private const string CONST_STARCONTAINERCLASS = "wijmo-wijrating-starcontainer";
		private const string CONST_RESETBUTTONCLASS = "wijmo-wijrating-reset ui-corner-all ui-state-default";
		private const string CONST_RESETLABELCLASS = "ui-icon ui-icon-close";
		private const string CONST_STARCLASS = "wijmo-wijrating-star";
		private const string CONST_NORMALSTARCLASS = "wijmo-wijrating-normal";
		private const string CONST_RATEDSTARCLASS = "wijmo-wijrating-rated";
		private const string CONST_VERTICALCLASS = "wijmo-wijrating-vertical";

		#endregion

		#region license
		private bool _productLicensed = false;
		private bool _shouldNag;

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the C1Rating class.
		/// </summary>
		public C1Rating()
			: base()
		{
			VerifyLicense();
			this.InitRating();
		}

		private void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Rating), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"> value that
		/// corresponds to this chart control.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			this.CssClass = string.Format("{0} {1}", this.GetCssClass(), cssClass);

			if (this.IsDesignMode)
			{
				string width = GetWidth();
				writer.AddStyleAttribute("width", width + "px");
			}

			base.AddAttributesToRender(writer);

			this.CssClass = cssClass;
		}

		private string GetWidth()
		{
			int width = IconWidth;
			if (Orientation == Orientation.Horizontal)
			{
				//reset button's width + star's width
				width = IconWidth * Count;
				if (!ResetButton.Disabled)
				{
					width += 20;
				}
			}
			return width.ToString();
		}

		private string GetCssClass()
		{
			string css = "";
			css = "ui-widget " + CONST_WIDGETCLASS;
			if (!this.IsDesignMode)
			{
				css += String.Format(" {0}", Utils.GetHiddenClass());
			}
			if (Orientation == Orientation.Vertical)
			{
				css += " " + CONST_VERTICALCLASS;
			}
			return css;
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (this.IsDesignMode)
			{
				this.Controls.Clear();
				this.Controls.Add(CreateStarContainer());
				Control resetButton = CreateResetButton();
				if (resetButton != null)
				{
					if (ResetButton.Position == ResetButtonPosition.LeftOrTop)
					{
						this.Controls.AddAt(0, resetButton);
					}
					else
					{
						this.Controls.Add(resetButton);
					}
				}
			}

			base.Render(writer);
		}

		private Control CreateStarContainer()
		{
			Panel starContainer = new Panel();
			starContainer.Attributes.Add("class", CONST_STARCONTAINERCLASS);
			this.CreateStars(starContainer);

			return starContainer as Control;
		}

		private void CreateStars(Panel starContainer)
		{
			int splitIdx = 0;
			int starWidth = (int)Math.Ceiling((double)IconWidth/Split);
			int starHeight = (int)Math.Ceiling((double)IconHeight / Split);
			bool isCustomizedClass = !String.IsNullOrEmpty(Icons.IconsClass.IconClass)
				|| Icons.IconsClass.IconsClass.Length > 0;
			bool isRatedCustomizedClass = !String.IsNullOrEmpty(Icons.RatedIconsClass.IconClass)
				|| Icons.RatedIconsClass.IconsClass.Length > 0;
			for (var idx = 0; idx < Count * Split; idx++, splitIdx++)
			{
				var val = Math.Round((double)(idx + 1) * TotalValue / Count / Split, 2);
				if (splitIdx == Split)
				{
					splitIdx = 0;
				}
				Panel star = new Panel();
				star.Attributes.Add("class", CONST_STARCLASS);
				if (Orientation == Orientation.Vertical)
				{
					star.Width = IconWidth;
					star.Height = Unit.Pixel(starHeight);
				}
				else
				{
					star.Width = Unit.Pixel(starWidth);
					star.Height = IconHeight;
				}
				Panel content = new Panel();
				Label lb = new Label();
				lb.Text = val.ToString();
				content.Controls.Add(lb);
				string contentClass = CONST_NORMALSTARCLASS;
				content.Height = Unit.Pixel(IconHeight);
				content.Width = Unit.Pixel(IconWidth);
				if (splitIdx > 0 && Direction == Direction.Normal)
				{
					if (Orientation == Orientation.Vertical)
					{
						content.Style["margin-top"] = "-" + splitIdx * starHeight + "px";
					}
					else
					{
						content.Style["margin-left"] = "-" + splitIdx * starWidth + "px";
					}
				}
				else if (splitIdx < Split - 1 && Direction == Direction.Reversed)
				{
					if (Orientation == Orientation.Vertical)
					{
						content.Style["margin-top"] = "-" + (Split - 1 - splitIdx) * starHeight + "px";
					}
					else
					{
						content.Style["margin-left"] = "-" + (Split - 1 - splitIdx) * starWidth + "px";
					}
				}
				if (isCustomizedClass)
				{
					string customizedIconClass = "";
					int customizedIconIdx = 0;
					if (!String.IsNullOrEmpty(Icons.IconsClass.IconClass))
					{
						customizedIconClass = Icons.IconsClass.IconClass;
					}
					else
					{
						if (Direction == Direction.Reversed)
						{
							customizedIconIdx = (Count * Split - 1 - idx) / Split;
							//customizedIconIdx = (int)Math.Floor((double)(Count * Split - 1 - idx) / Split);
						}
						else
						{
							customizedIconIdx = idx / Split;
							//customizedIconIdx = (int)Math.Floor((double)idx / Split);
						}
						if (Icons.IconsClass.IconsClass.Length > customizedIconIdx)
						{
							customizedIconClass = Icons.IconsClass.IconsClass[customizedIconIdx];
						}
					}
					contentClass += " " + customizedIconClass;
				}
				string reatedCustomizedIconClass = "";
				if (isRatedCustomizedClass)
				{
					int ratedCustomizedIconIdx = 0;
					if (!String.IsNullOrEmpty(Icons.RatedIconsClass.IconClass))
					{
						reatedCustomizedIconClass = Icons.RatedIconsClass.IconClass;
					}
					else
					{
						if (Direction == Direction.Reversed)
						{
							ratedCustomizedIconIdx = (Count * Split - 1 - idx) / Split;
							//customizedIconIdx = (int)Math.Floor((double)(Count * Split - 1 - idx) / Split);
						}
						else
						{
							ratedCustomizedIconIdx = idx / Split;
							//customizedIconIdx = (int)Math.Floor((double)idx / Split);
						}
						if (Icons.RatedIconsClass.IconsClass.Length > ratedCustomizedIconIdx)
						{
							reatedCustomizedIconClass = Icons.RatedIconsClass.IconsClass[ratedCustomizedIconIdx];
						}
					}
				}
				if (val == Value)
				{
					contentClass += " " + CONST_RATEDSTARCLASS ;
					if (isRatedCustomizedClass)
					{
						contentClass += " " + reatedCustomizedIconClass;
					}
				}
				else if (val < Value && RatingMode == RatingMode.Continuous)
				{
					contentClass += " " + CONST_RATEDSTARCLASS;
					if (isRatedCustomizedClass)
					{
						contentClass += " " + reatedCustomizedIconClass;
					}
				}
				content.Attributes.Add("class", contentClass);
				star.Controls.Add(content);
				if (Direction == Direction.Reversed)
				{
					starContainer.Controls.AddAt(0, star);
				}
				else
				{
					starContainer.Controls.Add(star);
				}
			}
		}

		private Control CreateResetButton()
		{
			if (ResetButton.Disabled || !this.Enabled)
			{
				return null;
			}
			Panel resetButton = new Panel();
			string css = CONST_RESETBUTTONCLASS;
			if (!String.IsNullOrEmpty(ResetButton.CustomizedClass))
			{
				css += " " + ResetButton.CustomizedClass;
			}
			resetButton.Attributes.Add("class", css);
			Label label = new Label();
			label.Attributes.Add("class", CONST_RESETLABELCLASS);
			resetButton.Controls.Add(label);

			return resetButton as Control;
		}

		#endregion

		#region ** IPostBackDataHandler interface implementations
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1RatingSerializer sz = new C1RatingSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1RatingSerializer sz = new C1RatingSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1RatingSerializer sz = new C1RatingSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1RatingSerializer sz = new C1RatingSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations
	}
}
