﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Rating
{
	/// <summary>
	/// Serializes a <see cref="C1Rating"/> control.
	/// </summary>
	public class C1RatingSerializer : C1BaseSerializer<C1Rating, object, object>
	{
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the C1RatingSerializer class.
		/// </summary>
		/// <param name="obj">
		/// Serializable Object.
		/// </param>
		public C1RatingSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
