﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.Localization;
using System.Drawing;
using C1.Web.Wijmo.Controls.Base;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using C1.Web.Wijmo;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1Expander
{
	/// <summary>
	/// Represents an expanding panel that shows or hides embedded or external content.
	/// </summary>
	[ParseChildren(true)]
	[ToolboxData("<{0}:C1Expander runat=server></{0}:C1Expander>")]
	[ToolboxBitmap(typeof(C1Expander), "C1Expander.png")]
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Expander.C1ExpanderDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Expander.C1ExpanderDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Expander.C1ExpanderDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Expander.C1ExpanderDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[LicenseProviderAttribute()]
	public partial class C1Expander : C1TargetControlBase, ICompositeControlDesignerAccessor, IPostBackEventHandler, IPostBackDataHandler
	{

		#region ** fields

		private ITemplate HeaderTemplate;
		private ITemplate ContentTemplate;
		private C1ExpanderHeaderContainer _headerPanelControl;
		private C1ExpanderContentContainer _contentPanelControl;
		internal bool _productLicensed = false;
		private bool _shouldNag;
		private string _headerText;

		#endregion

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1Expander"/> class.
		/// </summary>
		public C1Expander()
			: base(HtmlTextWriterTag.Div)
		{
			VerifyLicense();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1Expander"/> class.
		/// </summary>
		/// <param name="headerText">Text that will be rendered into header panel.</param>
		public C1Expander(string headerText)
			: base(HtmlTextWriterTag.Div)
		{
			_headerText = headerText;
			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Expander), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region ** properties

        [RefreshProperties(RefreshProperties.All)]
        [WidgetOption]
        [Layout(LayoutType.Sizes)]
        public override Unit Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                base.Height = value;
            }
        }

		/// <summary>
		/// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[C1Description("C1Expander.AutoPostBack", "Indicates whether or not the control posts back to the server each time a user interacts with the control.")]
		[Layout(LayoutType.Behavior)]
		public bool AutoPostBack
		{
			get
			{
				return this.ViewState["AutoPostBack"] == null ? false : (bool)this.ViewState["AutoPostBack"];
			}
			set
			{
				this.ViewState["AutoPostBack"] = value;
			}
		}

		/// <summary>
		/// AutoPostBack Event Reference. Empty string if AutoPostBack property is false.
		/// </summary>
		[WidgetOption()]
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string PostBackEventReference
		{
			get
			{
				if (!IsDesignMode && (AutoPostBack || ExpandedChanged != null))
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "expanded={0}");
				}
				return "";
			}
		}

		/// <summary>
		/// Gets or sets the header section template.
		/// </summary>
		[C1Description("C1Expander.Header", "Gets or sets the header section template.")]
		[Browsable(false)]
		[DefaultValue(null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateContainer(typeof(C1ExpanderHeaderContainer))]
		[TemplateInstance(TemplateInstance.Single)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Misc)]
		public ITemplate Header
		{
			get { return HeaderTemplate; }
			set { HeaderTemplate = value; }
		}


		/// <summary>
		/// Gets or sets the content section template.
		/// </summary>
		[C1Description("C1Expander.Content", "Gets or sets the content section template.")]
		[Browsable(false)]
		[DefaultValue(null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateContainer(typeof(C1ExpanderContentContainer))]
		[TemplateInstance(TemplateInstance.Single)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Misc)]
		public ITemplate Content
		{
			get { return ContentTemplate; }
			set { ContentTemplate = value; }
		}

		/// <summary>
		/// Gets the control that is used as the header panel.
		/// </summary>
		[C1Description("C1Expander.HeaderPanel", "Gets the control that is used as the header panel.")]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1ExpanderHeaderContainer HeaderPanel
		{
			get
			{
				if (_headerPanelControl == null)
				{
					_headerPanelControl = new C1ExpanderHeaderContainer(this);
					//_headerPanelControl.ID = "hdr";
					if (this.IsDesignMode)
					{
						// fix for designer regions overflow
						// TFS issues: 1986
						//[C1Expander] Header and Content regions is not correctly drawn C1Expander when 'ExpandDirection' is set to 'Right'/'Left'
						_headerPanelControl.Attributes.Add("style", "overflow:hidden;margin:0;padding:0;");
					}
				}
				return _headerPanelControl;
			}
		}

		/// <summary>
		/// Gets the control that is used as the content panel.
		/// </summary>
		[C1Description("C1Expander.ContentPanel", "Gets the control that is used as the content panel.")]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1ExpanderContentContainer ContentPanel
		{
			get
			{
				if (_contentPanelControl == null)
				{
					_contentPanelControl = new C1ExpanderContentContainer(this);
					//_contentPanelControl.ID = "cnt";
					//_contentPanelControl.ScrollBars = this.ContentScrollBars;

					if (this.IsDesignMode)
					{
						// fix for designer regions overflow, 
						// TFS issues: 1986
						//[C1Expander] Header and Content regions is not correctly drawn C1Expander when 'ExpandDirection' is set to 'Right'/'Left'
						_contentPanelControl.Attributes.Add("style", "overflow:auto;margin:0;padding:0;");
					}
				}
				return _contentPanelControl;
			}
		}

		#endregion

		#region ** events

		/// <summary>
		/// Fires on the server side after an expanded state has been changed.
		/// </summary>
		[C1Description("C1Expander.ExpandedChanged", "Fires on the server side after an expanded state has been changed.")]
		[C1Category("Category.Behavior")]
		public event EventHandler ExpandedChanged;

		/// <summary>
		/// Called when selected pane is changed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnExpandedChanged(EventArgs e)
		{
			if (this.ExpandedChanged != null)
			{
				ExpandedChanged(this, e);

			}
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Called before control initialization.
		/// </summary>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
			EnsureChildControls();
		}

		/// <summary>
		/// Gets the controls collection.
		/// </summary>
		public override ControlCollection Controls
		{
			get
			{
				EnsureChildControls();
				return base.Controls;
			}
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			// Create the controls
			Controls.Clear();
			if (!string.IsNullOrEmpty(this._headerText))
			{
				LiteralControl literal = new LiteralControl(this._headerText);
				HeaderPanel.Controls.Add(literal);
			}
			// Load the templates into the controls
			if (HeaderTemplate != null)
				HeaderTemplate.InstantiateIn(HeaderPanel);
			else if (this.IsDesignMode)
				CreateDefaultHeaderTemplateForDesign();

			if (string.IsNullOrEmpty(this.ContentUrl)) //ABC191
			{
				if (ContentTemplate != null)
					ContentTemplate.InstantiateIn(ContentPanel);
				else if (this.IsDesignMode)
					CreateDefaultContentTemplateForDesign();
			}
			if (IsDesignMode)
			{
				if (this.ExpandDirection == Wijmo.Controls.C1Expander.ExpandDirection.Left
					|| this.ExpandDirection == Wijmo.Controls.C1Expander.ExpandDirection.Top)
				{
					this.Controls.Add(this.ContentPanel);
					this.Controls.Add(this.HeaderPanel);
				}
				else
				{
					this.Controls.Add(this.HeaderPanel);
					this.Controls.Add(this.ContentPanel);
				}
			}
			else
			{
				this.Controls.Add(this.HeaderPanel);
				this.Controls.Add(this.ContentPanel);
			}
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = string.IsNullOrEmpty(this.CssClass) ? "" : (this.CssClass + " ");
			if (!IsDesignMode)
			{
				cssClass += String.Format(" {0} ", Utils.GetHiddenClass());
			}
			else
			{
				cssClass += string.Format("wijmo-wijexpander ui-expander ui-widget ui-helper-reset ui-expander-icons ui-expander-{0}{1}",
						this.ExpandDirection.ToString().ToLowerInvariant(),
						(this.ExpandDirection == ExpandDirection.Left
						|| this.ExpandDirection == ExpandDirection.Right) ? " ui-helper-horizontal" : "");
			}
			writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.RenderLicenseWebComment(writer);
			EnsureChildControls();
			if (IsDesignMode)
			{
				Wijmo.Controls.C1Expander.ExpandDirection dir = this.ExpandDirection;
				string uiCornerDir = "ui-corner-bottom";
				switch (dir)
				{
					case Wijmo.Controls.C1Expander.ExpandDirection.Top:
						uiCornerDir = "ui-corner-bottom";
						break;
					case Wijmo.Controls.C1Expander.ExpandDirection.Bottom:
						uiCornerDir = "ui-corner-top";
						break;
					case Wijmo.Controls.C1Expander.ExpandDirection.Left:
						uiCornerDir = "ui-corner-right";
						break;
					case Wijmo.Controls.C1Expander.ExpandDirection.Right:
						uiCornerDir = "ui-corner-left";
						break;
				}
				this.ContentPanel.Attributes["class"] = string.Format("ui-expander-content ui-helper-reset ui-widget-content {0} ui-expander-content-active", uiCornerDir);
			}
			base.Render(writer);
		}

		/// <summary>
		/// Renders the contents.
		/// </summary>
		/// <param name="output">The output.</param>
		protected override void RenderContents(HtmlTextWriter output)
		{
			base.RenderContents(output);
		}

		#endregion

		#region ** IPostBackEventHandler interface implementations

		/// <summary>
		/// Process an event
		/// raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument">A System.String that represents an optional event argument to be passed to
		/// the event handler.</param>
		public void RaisePostBackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("expanded="))
			{
				bool newExp = bool.Parse(eventArgument.Remove(0, "expanded=".Length));
				if (newExp != this.Expanded)
				{
					this.Expanded = newExp;
					OnExpandedChanged(new EventArgs());
				}
			}
		}

		#endregion

		#region ** ICompositeControlDesignerAccessor interface implementation

		/// <summary>
		/// Recreates child controls.
		/// </summary>
		public void RecreateChildControls()
		{
			CreateChildControls();
		}

		#endregion

		#region ** private implementation 

		private static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}

		private void CreateDefaultContentTemplateForDesign()
		{
			ISite site = GetSite(this);
			if (site != null)
			{
				IDesignerHost host = (IDesignerHost)site.GetService(typeof(IDesignerHost));
				if (host != null)
				{
					this.ContentTemplate = ControlParser.ParseTemplate(host, " ");
				}
			}
		}

		private void CreateDefaultHeaderTemplateForDesign()
		{
			ISite site = GetSite(this);
			if (site != null)
			{
				IDesignerHost host = (IDesignerHost)site.GetService(typeof(IDesignerHost));
				if (host != null)
				{
					this.HeaderTemplate = ControlParser.ParseTemplate(host, " ");
				}
			}
		}

		#endregion

		#region ** IPostBackDataHandler

		private bool _raiseExpandedChanged = false;

		/// <summary>
		/// When implemented by a class, processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>
		/// true if the server control's state changes as a result of the postback; otherwise, false.
		/// </returns>
		public bool LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			return this.LoadClientData(data);	
		}

		/// <summary>
		/// When implemented by a class, signals the server control to notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		public void RaisePostDataChangedEvent()
		{
			if (_raiseExpandedChanged)
			{
				_raiseExpandedChanged = false;
				this.OnExpandedChanged(new EventArgs());				
			}
		}

		internal bool LoadClientData(Hashtable data)
		{
			bool isViewStateChanged = false;
			object o = o = data["expanded"];
			if (o != null)
			{
				if (this.Expanded != (bool)o)
				{
					isViewStateChanged = true;
					this.Expanded = (bool)o;
					_raiseExpandedChanged = true;
					
				}
			}
			return isViewStateChanged;
		}

		#endregion
	}
}
