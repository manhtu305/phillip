﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1Expander
{
	/// <summary>
	/// Represents the expander's content container.
	/// </summary>
    [ToolboxItem(false)]
    public class C1ExpanderContentContainer : Panel, INamingContainer
	{

		#region ** fields

		internal string _literalContent = "";
		private C1Expander _owner;
		private System.Web.UI.AttributeCollection _contentElementAttributes;
		private bool IsDesignMode = HttpContext.Current == null;

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ExpanderContentContainer"/> class.
		/// </summary>
		/// <param name="owner">The owner.</param>
		public C1ExpanderContentContainer(C1Expander owner)
			: base()
		{
			this._owner = owner;
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Gets a collection of all attribute name and value pairs 
		/// expressed on the content element tag within the ASP.NET page.
		/// </summary>
		/// <value>The content element attributes.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.UI.AttributeCollection ContentElementAttributes
		{
			get
			{
				if (_contentElementAttributes == null)
					_contentElementAttributes = new System.Web.UI.AttributeCollection(new StateBag());
				return _contentElementAttributes;
			}
		}

		#endregion

		#region ** methods

		#region ** render methods

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			if (IsDesignMode)
			{
				this.Attributes["role"] = "tabpanel";
				if (this._owner.Expanded)
				{
					this.Attributes["class"] =
						"ui-expander-content ui-helper-reset ui-widget-content ui-corner-bottom ui-expander-content-active";
				}
				else
				{

					this.Attributes["class"] =
						"ui-expander-content ui-helper-reset ui-widget-content ui-corner-bottom";
				}
				
				this.ContentElementAttributes.AddAttributes(writer);
			}

			base.RenderBeginTag(writer);
		}

		/// <summary>
		/// Renders the HTML closing tag of the control into the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{
			base.RenderEndTag(writer);
		}

		/// <summary>
		/// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			base.RenderContents(writer);
			if (!string.IsNullOrEmpty(_literalContent))
			{
				writer.Write(_literalContent);
			}
		}

		#endregion

		#endregion

	}
}
