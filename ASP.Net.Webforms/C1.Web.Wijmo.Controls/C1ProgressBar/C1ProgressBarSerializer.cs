﻿#region File Header
//==============================================================================
//  FileName    :   C1ProgressbarSerializer.cs
//
//  Description :   
//
//  Copyright (c) 2001 - 2011 GrapeCity, Inc.
//  All rights reserved.
//
//                                                          ... Ryan Wu
//==============================================================================

//------------------------------------------------------------------------------
//  Update Log:
//
//  Status          Date            Name                    BUG-ID
//  ----------------------------------------------------------------------------
//  Created         2011/03/23      Ryan Wu                 None
//
//------------------------------------------------------------------------------
#endregion end of File Header.

using System;
using System.Collections.Generic;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1ProgressBar
{

	/// <summary>
    /// Serializes the C1ProgressBar control.
	/// </summary>
	public class C1ProgressBarSerializer : C1BaseSerializer<C1ProgressBar, object, object>
	{

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ProgressBarSerializer"/> class.
		/// </summary>
		/// <param name="obj">object to serialize</param>
		public C1ProgressBarSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor.
	}
}
