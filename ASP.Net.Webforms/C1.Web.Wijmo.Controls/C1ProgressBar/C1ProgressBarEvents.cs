using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
 

namespace C1.Web.Wijmo.Controls.C1ProgressBar
{
	/// <summary>
	/// Delegate type for handling events that are related to the RunTask event.
	/// </summary>
	/// <param name="sender">
    /// The <see cref="C1ProgressBar"/> instance which raises the RunTask event.
	/// </param>
	/// <param name="e">
	/// The <see cref="C1ProgressBarTaskEventArgs"/> event arguments for the RunTask event.
	/// </param>
	public delegate void C1ProgressBarTaskEventHandler(object sender, C1ProgressBarTaskEventArgs e);

	/// <summary>
	/// Represents the C1ProgressBarTaskEventArgs class which contains all of the settings for the RunTask event argument.
	/// </summary>
	public class C1ProgressBarTaskEventArgs : EventArgs
	{

		private string _extraData;
		private int _value;
		private C1ProgressBar _progressbar;

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ProgressBarTaskEventArgs"/> class.
		/// </summary>
		/// <param name="progressbar">
		/// The instance of the <seealso cref="C1ProgressBar"/> class.
		/// </param>
		public C1ProgressBarTaskEventArgs(C1ProgressBar progressbar)
		{
			this._progressbar = progressbar;
		}

		/// <summary>
        /// Gets the extra data which is stored by the user. Can you provide a more detailed description, including what kind of extra data
		/// </summary>
		public string ExtraData
		{
			get
			{
				return _extraData;
			}
		}

		/// <summary>
		/// Gets the progress value.
		/// </summary>
		public int Value
		{
			get
			{
				return _value;
			}
		}

		/// <summary>
        /// Updates the progress for the C1ProgressBar control using an integer for the new progress value.
		/// </summary>
		/// <param name="newValue">
		/// The new progress value.
		/// </param>
		public void UpdateProgress(int newValue)
		{
			this.UpdateProgress(newValue, string.Empty);
		}

		/// <summary>
        /// Updates the progress for the C1ProgressBar control using an integer for the new progress value and a string for the extra data stored by the user. 
		/// </summary>
		/// <param name="newValue">
		/// The new progress value.
		/// </param>
		/// <param name="extraData">
		/// The extra data which is stored by user.
		/// </param>
		public void UpdateProgress(int newValue, string extraData)
		{
			_value = newValue;
			_extraData = extraData;
			this._progressbar.RegisterWidgetSettingStatement("value", newValue);
		}
	}
}
