﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Drawing;

namespace C1.Web.Wijmo.Controls.C1ProgressBar
{

	/// <summary>
	/// The C1ProgressBar is designed to simply display the current percentage complete for a process.
	/// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1ProgressBar.C1ProgressBarDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1ProgressBar.C1ProgressBarDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1ProgressBar.C1ProgressBarDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1ProgressBar.C1ProgressBarDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[DefaultEvent("RunTask")]
	[ToolboxData("<{0}:C1ProgressBar runat=server></{0}:C1ProgressBar>")]
	[ToolboxBitmap(typeof(C1ProgressBar), "C1ProgressBar.png")]
	[LicenseProviderAttribute()]
	public partial class C1ProgressBar : C1TargetControlBase, IPostBackDataHandler, IPostBackEventHandler
	{

		#region ** fields
		#region ** private members
		private HtmlGenericControl progress = null;
		private HtmlGenericControl label = null;
		private Hashtable _corners = null;
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion end of ** private members.

		#region ** const members
		private const string CONST_JQUERYPB_VARIABLE = "jqpb";
		private const int CONST_DEFAULT_HEIGHT = 16;
		private const int CONST_DEFAULT_WIDTH = 200;
		private const string CONST_WIJPROGRESSBAR = "wijmo-wijprogressbar";
		private const string CONST_PROGRESSBAR = "ui-progressbar";
		private const string CONST_PROGRESSBAR_VALUE = CONST_PROGRESSBAR + "-value";
		private const string CONST_PROGRESSBAR_LABEL = CONST_PROGRESSBAR + "-label";
		private const string CONST_LABEL_ALIGN_PREFIX = CONST_WIJPROGRESSBAR + "-lb-";
		private const string CONST_WIDGET = "ui-widget";
		private const string CONST_WIDGET_CONTENT = CONST_WIDGET + "-content";
		private const string CONST_WIDGET_HEADER = CONST_WIDGET + "-header";
		private const string CONST_CORNER_PREFIX = "ui-corner-";
		private const string CONST_CORNER_ALL = CONST_CORNER_PREFIX + "all";
		private const string CONST_CORNER_LEFT = CONST_CORNER_PREFIX + "left";
		private const string CONST_CORNER_RIGHT = CONST_CORNER_PREFIX + "right";
		private const string CONST_CORNER_TOP = CONST_CORNER_PREFIX + "top";
		private const string CONST_CORNER_BOTTOM = CONST_CORNER_PREFIX + "bottom";
		#endregion end of ** const members.

		#region ** events
		/// <summary>
		/// Fires when a server side task is running.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1ProgressBar.RunTask", "Raised when a server side task is running.")]
		public event C1ProgressBarTaskEventHandler RunTask; 
		#endregion end of ** events.
		#endregion end of ** fields.
		
		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ProgressBar"/> class.
		/// </summary>
		public C1ProgressBar()
		{
			VerifyLicense();
			this.InitProgressBar();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ProgressBar), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

//        internal C1ProgressBar(string key)
//        {
//#if GRAPECITY
//            _productLicensed = true;
//#else
//            // ttZzKzJ5mwNr/IihpBl2pA==
//            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ProgressBar), this,
//                Assembly.GetExecutingAssembly(), key);
//            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
//#endif
//            this.InitProgressBar();
//        }

		#region ** initialize progressbar
		private void InitProgressBar()
		{
			//_JsonSerializableImpl = new JsonSerializableHelper(this, this);
		}
		#endregion end of ** initialize progressbar.
		#endregion end of ** constructors.

		#region ** properties

		#region ** public properties
		/// <summary>
		/// Gets or sets the ID of the button that starts a server-side task.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1ProgressBar.StartTaskButton", "Gets or Sets ID of the button which starts a server side task.")]
		[DefaultValue("")]
		[IDReferenceProperty(typeof(Button))]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		#if ASP_NET45
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.3")]
#else
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.2")]
#endif
		public string StartTaskButton
		{
			get
			{
				return this.GetPropertyValue<string>("StartTaskButton", "");
			}
			set
			{
				this.SetPropertyValue<string>("StartTaskButton", value);
			}
		}

		/// <summary>
		/// Gets or sets the ID of the button that stops a server-side task.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1ProgressBar.StopTaskButton", "Gets or Sets ID of the button which stops a server side task.")]
		[DefaultValue("")]
		[IDReferenceProperty(typeof(Button))]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		#if ASP_NET45
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.3")]
#else
		[TypeConverter("C1.Web.UI.Design.ButtonIDConverter, C1.Web.Wijmo.Controls.Design.2")]
#endif
		public string StopTaskButton
		{
			get
			{
				return this.GetPropertyValue<string>("StopTaskButton", "");
			}
			set
			{
				this.SetPropertyValue<string>("StopTaskButton", value);
			}
		}

		/// <summary>
		///  Gets or sets the height of the progress bar.
		/// </summary>
		[Layout(LayoutType.Sizes)]
		[WidgetOption]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the width of the progress bar.
		/// </summary>
		[Layout(LayoutType.Sizes)]
		[WidgetOption]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		//public bool IsRunning
		//{
		//    get;
		//    internal set;
		//}

		///// <summary>
		///// The tooltip displayed when the mouse is over the control. 
		///// </summary>
		//[DefaultValue("{1}%")]
		//[C1Description("C1ProgressBar.ToolTip", "String that will be used as a format for the progress bar labels.six different values may be inserted in C# style string formatting: {0} or {ProgressValue} - Current Progress Value, {1} or {PercentProgress}- Current Percent Progress, {2} or {RemainingProgress}- Remaining progress to be completed, {3} or {PercentageRemaining}- Percentage remaining, {4} or {Min}- Minimum value, {5} or {Max}- Maximum value.")]
		//[Layout(LayoutType.Behavior)]
		//[WidgetOption]
		//public override string ToolTip
		//{
		//    get
		//    {
		//        return base.ToolTip;
		//    }
		//    set
		//    {
		//        base.ToolTip = value;
		//    }
		//}

		/// <summary>
		/// Gets the PostBack event reference. 
		/// Returns and empty string if the StartTaskButton or StopTaskButton property is string.empty.
		/// </summary>
		[Browsable(false)]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption()]
		public string PostBackEventReference
		{
			get
			{
				if (!this.IsDesignMode && (!string.IsNullOrEmpty(this.StartTaskButton) && 
					!string.IsNullOrEmpty(this.StopTaskButton)))
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "{0}");
				}

				return "";
			}
		}
		#endregion end of ** public properties.

		#region ** protected properties
		/// <summary>
		/// Gets the <see cref = "System.Web.UI.HtmlTextWriterTag"/> value that corresponds to 
		/// the C1ProgressBar control.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		} 
		#endregion end of ** protected properties.

		#region ** private properties
		private Hashtable Corners
		{
			get
			{
				if (this._corners == null)
				{
					this._corners = new Hashtable();
					this._corners.Add("East", "left");
					this._corners.Add("West", "right");
					this._corners.Add("North", "top");
					this._corners.Add("South", "bottom");
				}

				return this._corners;
			}
		}

		private bool IsVertical
		{
			get
			{
				return this.FillDirection == FillDirection.North ||
					this.FillDirection == FillDirection.South;
			}
		}
		#endregion end of ** private properties.
		#endregion end of ** properties.

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region ** methods
		#region ** private methods
		private Control CreateProgress()
		{
			progress = new HtmlGenericControl("div");

			return progress;
		}

		private Control CreateLabel()
		{
			label = new HtmlGenericControl("span");
			return label;
		}

		private Control CreateIframeForCallback()
		{
			HtmlGenericControl runTaskIframe = new HtmlGenericControl("iframe");

			runTaskIframe.Attributes["name"] = "RunTaskTarget_" + this.ClientID;
			runTaskIframe.Style.Add(HtmlTextWriterStyle.Position, "absolute");
			runTaskIframe.Style.Add(HtmlTextWriterStyle.Width, "1px");
			runTaskIframe.Style.Add(HtmlTextWriterStyle.Height, "1px");
			runTaskIframe.Style.Add(HtmlTextWriterStyle.Left, "-1000px");

			return runTaskIframe;
		}

		private string DetermineCompoundCssClass()
		{
			if (!this.IsDesignMode)
			{
				return Utils.GetHiddenClass();
			}

			return string.Format("{0} {1} {2} {3} {4} {0}-{5}", CONST_WIJPROGRESSBAR,
				CONST_PROGRESSBAR, CONST_WIDGET, CONST_WIDGET_CONTENT,
				CONST_CORNER_ALL, this.FillDirection.ToString().ToLower());
		}

		private void ApplyStyle2Progress()
		{
			string cssClass = string.Format("{0} {1} {2}{3}",
				CONST_PROGRESSBAR_VALUE, CONST_WIDGET_HEADER, CONST_CORNER_PREFIX,
				this.Corners[FillDirection.ToString()]);

			double percent = (Value - MinValue) * 100.0 / (MaxValue - MinValue);

			progress.Attributes["class"] = cssClass;
			progress.Style.Clear();
			progress.Style.Add(IsVertical ? "height" : "width", string.Format("{0}%", percent));

            if (!string.IsNullOrEmpty(IndicatorImage)) {
                progress.Style.Add("background-image", string.Format("url({0});", base.ResolveClientUrl(IndicatorImage)));
                progress.Style.Add("background-attachment", "fixed");
                progress.Style.Add("background-color", "transparent");
                progress.Style.Add("background-repeat", "repeat repeat");
                progress.Style.Add("background-position-x", "0%");
                progress.Style.Add("background-position-y", "0%");
            }
		}

		private void ApplyStyle2Label()
		{
			string cssClass = string.Format("{0} {1}{2} {3}{4}", CONST_PROGRESSBAR_LABEL,
				CONST_CORNER_PREFIX, this.Corners[FillDirection.ToString()],
				CONST_LABEL_ALIGN_PREFIX, LabelAlign.ToString().ToLower());
			double percent = (Value - MinValue) * 100.0 / (MaxValue - MinValue);

			label.Attributes["class"] = cssClass;
			label.Style.Clear();

			if (this.LabelAlign != LabelAlign.North && this.LabelAlign != LabelAlign.South &&
				!(this.LabelAlign == LabelAlign.Running && this.IsVertical))
			{
				label.Style.Add("line-height", this.Height.IsEmpty ?
					(this.IsVertical ? CONST_DEFAULT_WIDTH : CONST_DEFAULT_HEIGHT) + "px" :
					this.Height.ToString());
			}

			label.InnerText = string.Format(LabelFormatString, Value, percent,
				(MaxValue - Value), (100 - percent), MinValue, MaxValue);
		}

		private string GetClientID(string id)
		{
			if (string.IsNullOrEmpty(id))
			{
				return string.Empty;
			}

			Control ctrl = this.NamingContainer.FindControl(id);
			
			if (ctrl == null)
			{
				return id;
			}

			return ctrl.ClientID;
		}
		#endregion end of ** private methods.

		#region ** override methods
		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for
		/// posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();
			base.CreateChildControls();

			if (this.IsDesignMode)
			{
				this.Controls.Add(this.CreateProgress());
				this.Controls.Add(this.CreateLabel());
			}
			else
			{
				this.Controls.Add(this.CreateIframeForCallback());
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified HtmlTextWriterTag.
		/// </summary>
		/// <param name="writer">A HtmlTextWriter that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass);

			base.AddAttributesToRender(writer);

			this.CssClass = cssClass;
		}

		/// <summary>
		/// Raises the PreRender event.
		/// </summary>
		/// <param name="e">An EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);

			this.Page.RegisterRequiresRaiseEvent(this);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();

			if (this.IsDesignMode)
			{
				this.ApplyStyle2Progress();
				this.ApplyStyle2Label();
			}
			else
			{
				if (!string.IsNullOrEmpty(this.StartTaskButton))
				{
					this.StartTaskButton = this.GetClientID(this.StartTaskButton);
				}

				if (!string.IsNullOrEmpty(this.StopTaskButton))
				{
					this.StopTaskButton = this.GetClientID(this.StopTaskButton);
				}
			}

			base.Render(writer);
		} 
		#endregion end of ** override methods.

		#region ** events
		/// <summary>
		/// Rises RunTask event.
		/// </summary>
		/// <param name="e">A C1ProgressBarTaskEventArgs object that contains the event data.</param>
		protected void OnRunTask(C1ProgressBarTaskEventArgs e)
		{
			if (RunTask != null)
			{
				RunTask(this, e);
			}
		}
		#endregion end of ** events.
		#endregion end of ** methods.

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1ProgressBarSerializer sz = new C1ProgressBarSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1ProgressBarSerializer sz = new C1ProgressBarSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1ProgressBarSerializer sz = new C1ProgressBarSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1ProgressBarSerializer sz = new C1ProgressBarSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}

		#endregion end ** IC1Serializable interface implementations.

		#region ** IPostBackDataHandler interface implementations
		internal void RegisterWidgetSettingStatement(string key, object value)
		{
			this.Page.Response.Write(string.Format("<script>{0}.{1}('{2}', {3});</script>",
				CONST_JQUERYPB_VARIABLE, this.WidgetName, key, value));
			this.Page.Response.Flush();
		}

		/// <summary>
		/// Processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			if (postCollection["__EVENTTARGET"] != null && 
				postCollection["__EVENTTARGET"].Equals(this.ClientID))
			{
				return false;
			}

			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		/// <summary>
		/// Notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations

		#region ** IPostBackEventHandler interface implementations
		/// <summary>
		/// Enables a <see cref = "C1ProgressBar"/> control to process an event
		/// raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument">
		/// A String that represents an optional event argument to be passed to
		/// the event handler.
		/// </param>
		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (!this.Enabled)
			{
				return;
			}

			Page.Response.Buffer = true;
			Page.Response.Clear();
			Page.Response.Write("<head>");
			
			#region Buffer
			Page.Response.Write("<title>Buffer 256 bit</title>");
			Page.Response.Write("<script>function c1(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			Page.Response.Write("<script>function c2(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			Page.Response.Write("<script>function c3(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			Page.Response.Write("<script>function c4(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			Page.Response.Write("<script>function c5(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			Page.Response.Write("<script>function c6(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			Page.Response.Write("<script>function c7(){alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');alert('');}</script>");
			#endregion
			
			Page.Response.Write("</head>");
			Page.Response.Write("<body>");
			Page.Response.Write(string.Format("<script>var {0} = parent.$('#{1}');</script>",
				CONST_JQUERYPB_VARIABLE, this.ClientID));
			this.RegisterWidgetSettingStatement("value", 0);

			if (eventArgument.Equals("Start"))
			{
				//this.IsRunning = true;
				C1ProgressBarTaskEventArgs args = new C1ProgressBarTaskEventArgs(this);
				this.OnRunTask(args);
			}
			else
			{
				//this.IsRunning = false;
				string[] items = eventArgument.Split('|');
				this.RegisterWidgetSettingStatement("value", items[1]);
			}

			Page.Response.Write("</body>");
			Page.Response.End();
		}
		#endregion end of ** IPostBackEventHandler interface implementations.
	}
}
