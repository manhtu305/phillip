﻿C1ProgressBar

Differences from Palomino:
---------------------------------
Server side:
	Removed properties:
		All VisualStyle-related properties removed
(EmbeddedVisualStyles/VisualStyle/VisualStylePath/UseEmbeddedVisualStyles).
		AnimationDuration removed.
		Easing removed.
		OnClientClick removed.
		OnClientMouseDown removed.
		OnClientMouseUp removed.
		OnClientMouseOver removed.
		OnClientMouseOut removed.
		Orientation removed.

	Renamed Properties
		Tooltip -> ToolTipFormatString
		FillDirection (type) ProgressBarFillDirection(FromLeftOrTop,FromRightOrBottom) -> FillDirection(East,West,North,South)
		LabelAlign (type) ProgressBarLabelAlign(None,LeftOrTop,Center,RightOrBottom,Running) -> LabelAlign(East,West,Center,North,South,Running)