﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1AppView
{
	public class C1AppViewSerializer : C1BaseSerializer<C1AppView, C1AppViewItem, C1AppView>
	{
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the C1AppViewSerializer class.
		/// </summary>
		public C1AppViewSerializer(Object obj)
			: base(obj)
		{
		}

		#endregion

		#region ** protected override

		/// <summary>
		/// Provides a custom implementation to add deserialized objects to the items collection.
		/// </summary>        
		/// <remarks>This method is primarily used by control developers.</remarks>
		/// <param name="collection">Target collection to add items.</param>
		/// <param name="item">Item to be added to the collection.</param>
		/// <returns>Returns True if the "item" object is added to the target collection. Otherwise, False.</returns>
		protected override bool OnAddItem(object collection, object item)
		{
			if (collection is C1AppViewItemCollection)
			{
				try
				{
					if (item != null && item is C1AppViewItem)
					{
						((C1AppViewItemCollection)collection).Add((C1AppViewItem)item);
						return true;
					}
					else
					{
						if (item != null)
						{
							//   throw new HttpException("item is not AccordionPane?? " + item.GetType());
						}
						else
						{
							//throw new HttpException("item is NULL. strange...");
						}
						return true;
					}
				}
				catch (Exception ex)
				{
					throw new HttpException("OnAddItem error:" + ex.Message + "," + ex.StackTrace);
				}
			}
			return base.OnAddItem(collection, item);
		}

		/// <summary>
		/// Removes all the items from the <see cref="C1AppViewItemCollection"/>.
		/// </summary>
		/// <param name="collection">Target collection to remove items from.</param>
        /// <returns>Returns True if all the items are successfully removed from the collection. Otherwise, False.</returns>
		protected override bool OnClearItems(object collection)
		{
			if (collection is C1AppViewItemCollection)
			{
				((C1AppViewItemCollection)collection).Clear();
				return true;
			}
			return base.OnClearItems(collection);
		}

		#endregion
	}
}
