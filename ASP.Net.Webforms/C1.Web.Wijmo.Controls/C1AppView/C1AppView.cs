﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Collections;
using System.Reflection;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.Localization;
using System.IO;

namespace C1.Web.Wijmo.Controls.C1AppView
{

	/// <summary>
	/// Represents a Appview control in an ASP.NET Web page.
	/// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.C1AppViewDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.C1AppViewDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.C1AppViewDesigner, C1.Web.Wijmo.Controls.Design.3" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.C1AppViewDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1AppView runat=server></{0}:C1AppView>")]
	[ToolboxBitmap(typeof(C1AppView), "C1AppView.png")]
	[LicenseProvider()]
	public partial class C1AppView : C1TargetDataBoundControlBase,
		INamingContainer, IPostBackEventHandler, IC1Serializable
	{

		#region ** fields
		internal string _postBackEventReference;
		private bool _productLicensed = false;
		private bool _shouldNag;

		private C1AppViewItemCollection _items;
		private PlaceHolder _defaultContent;
		#endregion

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1AppView"/> class.
		/// </summary>
		public C1AppView()
		{
			VerifyLicense();
			this.LoadSettings = new LoadSettings();

			base.Height = new Unit("300px");
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1AppView), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region ** properties

		internal override bool IsWijWebControl
		{
			get
			{
				return false;
			}
		}

		private string DataTheme
		{
			get 
			{
				return this.ThemeSwatch == String.Empty ? "a": this.ThemeSwatch;
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the control, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[Browsable(true)]
		[DefaultValue("")]
		[WidgetOption]
		[TypeConverter(typeof(WijmoThemeSwatchConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string ThemeSwatch
		{
			get
			{
				return base.ThemeSwatch;
			}
			set
			{
				base.ThemeSwatch = value;
			}
		}

		/// <summary>
		/// Gets the app view item collection.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1AppView.Items")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[RefreshProperties(RefreshProperties.All)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[MergableProperty(false)]
		[Layout(LayoutType.Appearance)]
		[CollectionItemType(typeof(C1AppViewItem))]
		public C1AppViewItemCollection Items
		{
			get
			{
				if (_items == null)
					_items = new C1AppViewItemCollection(this);
				return _items;
			}
		}

		/// <summary>
		/// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
		/// </summary>
		[DefaultValue(false)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1AppView.AutoPostBack")]
		public bool AutoPostBack
		{
			get
			{
				return this.GetPropertyValue<bool>("AutoPostBack", false);
			}
			set
			{
				this.SetPropertyValue<bool>("AutoPostBack", value);
			}
		}

		/// <summary>
		/// Defines the default content of the AppView control Header.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[C1Description("C1AppView.HeaderTitle")]
		public string HeaderTitle
		{
			get
			{
				return this.GetPropertyValue<string>("HeaderTitle", "Appview");
			}
			set
			{
				this.SetPropertyValue<string>("HeaderTitle", value);
			}
		}

		///<summary>
		/// Gets or sets the field in the data source from which to load text values.
		///</summary>
		/// <example>
		/// <code lang="C#">
		/// C1AppView c = new C1AppView();
		/// c.DataSource = data;
		/// c.DataTextField = "textField";
		/// c.AppViewPageUrlField = "valueField";
		/// c.DataBind();
		/// </code>
		/// </example>
		[DefaultValue("")]
		[C1Description("C1AppView.DataTextField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string DataTextField
		{
			get
			{
				return this.GetPropertyValue<string>("DataTextField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataTextField", value);
			}
		}

		///<summary>
		/// Gets or sets the field in the data source from which to load appview page url values.
		///</summary>
		/// <example>
		/// <code lang="C#">
		/// C1AppView c = new C1AppView();
		/// c.DataSource = data;
		/// c.DataTextField = "textField";
		/// c.AppViewPageUrlField = "valueField";
		/// c.DataBind();
		/// </code>
		/// </example>
		[DefaultValue("")]
		[C1Description("C1AppView.AppViewPageUrlField")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Misc)]
		public string AppViewPageUrlField
		{
			get
			{
				return this.GetPropertyValue<string>("AppViewPageUrlField", "");
			}
			set
			{
				this.SetPropertyValue<string>("AppViewPageUrlField", value);
			}
		}

		/// <summary>
		/// Gets or sets the default content displays in the appview control.
		/// </summary>
		[C1Description("C1AppView.DefaultConent")]
		[C1Category("Category.Behavior")]
		[TemplateContainer(typeof(PlaceHolder))]
		[Browsable(false)]
		[Bindable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public PlaceHolder DefaultContent
		{
			get
			{
				if (_defaultContent == null)
					_defaultContent = new PlaceHolder();

				return _defaultContent;
			}
			set
			{
				_defaultContent = value;
			}

		}

		/// <summary>
		/// Gets the postback event reference. send it to client side. 
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(true)]
		public string PostBackEventReference
		{
			get
			{
				return _postBackEventReference;
			}
		}

		#region ** hidden properties

		/// <summary>
		/// Height of C1AppView.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Width of C1AppView.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		#endregion

		#endregion

		#region ** events

		/// <summary>
		/// Postback event.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public event EventHandler Postback;

		/// <summary>
		/// To Do handle postback event.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnPostback(EventArgs e)
		{
			if (this.Postback != null)
			{
				Postback(this, e);
			}
		}

		#endregion

		#region ** override methods
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
			if (!IsDesignMode)
			{
				if (this.Page != null)
				{
					this.Page.RegisterRequiresRaiseEvent(this);
					_postBackEventReference = this.Page.ClientScript.GetPostBackEventReference(this, "_Args");
				}
			}
		}

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This
		/// method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			writer.AddAttribute("id", this.ClientID);
			if (IsDesignMode)
				writer.AddAttribute("class", "wijmo-wijappview wijmo-wijappview-with-fixed-header");

			base.RenderBeginTag(writer);
		}

		/// <summary>
		/// Render the AppView Control
		/// </summary>
		/// <param name="writer"></param>
		protected override void RenderContents(HtmlTextWriter writer)
		{
			writer.AddAttribute("data-role", "appviewpage");
			if (IsDesignMode)
				writer.AddAttribute("class", "wijmo-wijappview-page wijmo-wijappview-page-active");
			writer.RenderBeginTag("div");

			RenderHeader(writer);

			RenderContent(writer);

			writer.RenderEndTag();

			RenderMenu(writer);
		}

		/// <summary>
		/// Render the Header part of the Appview control.
		/// </summary>
		/// <param name="writer"></param>
		private void RenderHeader(HtmlTextWriter writer)
		{
			if (IsDesignMode)
				writer.AddAttribute("class", "wijmo-wijappview-header ui-header-fixed ui-header ui-bar-" + DataTheme);
			writer.AddAttribute("data-role", "header");
			writer.AddAttribute("data-theme", DataTheme);
			writer.RenderBeginTag("div");
			if (IsDesignMode)
				writer.AddAttribute("class", "ui-title");
			writer.RenderBeginTag("h2");
			writer.Write(HeaderTitle);
			writer.RenderEndTag();
			writer.RenderEndTag();
		}

		/// <summary>
		/// Render the content part of the AppView control.
		/// </summary>
		/// <param name="writer"></param>
		private void RenderContent(HtmlTextWriter writer)
		{
			if (IsDesignMode)
			{
				CreatePageContainer(writer);
				writer.AddAttribute("class", "wijmo-wijappview-content ui-content ui-body-" + DataTheme);
			}
			writer.AddAttribute("data-role", "content");
			writer.AddAttribute("data-theme", DataTheme);
			writer.RenderBeginTag("div");
			DefaultContent.RenderControl(writer);
			writer.RenderEndTag();

			if (IsDesignMode)
				writer.RenderEndTag();
		}

		/// <summary>
		/// Render the menu part of the AppView control.
		/// </summary>
		/// <param name="writer"></param>
		private void RenderMenu(HtmlTextWriter writer)
		{
			if (IsDesignMode)
				writer.AddAttribute("class", "ui-body-" + DataTheme + " wijmo-wijappview-menu");
			writer.AddAttribute("data-role", "menu");
			writer.AddAttribute("class", "ui-body-" + DataTheme);
			writer.RenderBeginTag("div");
			HtmlGenericControl ul = new HtmlGenericControl("ul");
			if (IsDesignMode)
				ul.Attributes.Add("class", "ui-listview");
			ul.Attributes.Add("data-role", "listview");
			ul.Attributes.Add("data-theme", this.DataTheme);
			for (int i = 0; i < this.Items.Count; i++)
			{
				C1AppViewItem item = this.Items[i];
				HtmlGenericControl li = new HtmlGenericControl("li");
				li.Attributes.Add("data-theme", string.IsNullOrEmpty(item.ThemeSwatch) ? DataTheme : item.ThemeSwatch);
				if (IsDesignMode)
				{
					li.Attributes.Add("class", "ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-first-child ui-btn-up-" +
						(string.IsNullOrEmpty(item.ThemeSwatch) ? DataTheme : item.ThemeSwatch) +
                        (item.Visible ? "" : " ui-screen-hidden"));
					li.Controls.Add(CreatChildElements(item));
				}
				else
				{
					HyperLink link = new HyperLink();
					link.NavigateUrl = item.AppViewPageUrl;
					link.Text = item.Text;
					li.Controls.Add(link);
					string cssClass = "";
					if (!item.Visible) 
					{
						cssClass += " ui-screen-hidden";
					}
					if (!item.Enabled) 
					{
						cssClass += " ui-state-disabled";
					}
					if (!string.IsNullOrEmpty(cssClass))
					{
						cssClass = cssClass.Substring(1);
						li.Attributes.Add("class", cssClass);
					}
				}
                ul.Controls.Add(li);
			}
			ul.RenderControl(writer);
			writer.RenderEndTag();
		}

		/// <summary>
		/// Create the appview page container for design mode.
		/// </summary>
		/// <param name="writer"></param>
		private void CreatePageContainer(HtmlTextWriter writer)
		{
			writer.AddAttribute("class", "wijmo-wijappview-page-container");
			writer.RenderBeginTag("div");
		}

		/// <summary>
		/// Create child elements of the menu part for design mode.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private Control CreatChildElements(C1AppViewItem item)
		{
			HtmlGenericControl liDiv = new HtmlGenericControl("div");
			liDiv.Attributes.Add("class", "ui-btn-inner ui-li");
			HtmlGenericControl textDiv = new HtmlGenericControl("div");
			textDiv.Attributes.Add("class", "ui-btn-text");
			HyperLink link = new HyperLink();
			string cssClass = "ui-link-inherit";
			if (!item.Enabled) 
			{
				cssClass += " ui-state-disabled";
			}
			link.Attributes.Add("class", cssClass);
			link.NavigateUrl = item.AppViewPageUrl;
			link.Text = item.Text;
			textDiv.Controls.Add(link);
			liDiv.Controls.Add(textDiv);
			HtmlGenericControl iconSpan = new HtmlGenericControl("span");
			iconSpan.Attributes.Add("class", "ui-icon ui-icon-arrow-r ui-icon-shadow");
			liDiv.Controls.Add(iconSpan);

			return liDiv;
		}

		#endregion

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			if (base.RequiresDataBinding
			&& (!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				this.EnsureDataBound();
			}

			base.CreateChildControls();
		}

		/// <summary>
		/// When overridden in a derived class, binds data from the data source to the control.
		/// </summary>
		/// <param name="data">The <see cref="T:System.Collections.IEnumerable"/> list of data returned from a <see cref="M:System.Web.UI.WebControls.DataBoundControl.PerformSelect"/> method call.</param>
		protected override void PerformDataBinding(IEnumerable data)
		{
			base.PerformDataBinding(data);

			if (DesignMode || data == null)
			{
				return;
			}

			IEnumerator e = data.GetEnumerator();
			bool hasTextField = !this.DataTextField.Trim().Equals(string.Empty);
			bool hasAppViewPageUrlField = !this.AppViewPageUrlField.Trim().Equals(string.Empty);

			while (e.MoveNext())
			{
				object itemdata = e.Current;
				C1AppViewItem item = new C1AppViewItem(this);
				if (hasTextField)
				{
					item.Text = DataBinder.GetPropertyValue(itemdata, this.DataTextField.Trim()).ToString();
				}
				else
				{
					item.Text = itemdata.ToString();
				}
				if (hasAppViewPageUrlField)
				{
					item.AppViewPageUrl = DataBinder.GetPropertyValue(itemdata, this.AppViewPageUrlField.Trim()).ToString();
				}

				this.Items.Add(item);
			}
		}

		#region ** IPostBackEventHandler interface implementations

		/// <summary>
		/// Process an event
		/// raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument">A System.String that represents an optional event argument to be passed to the event hander.</param>
		public void RaisePostBackEvent(string eventArgument)
		{
			OnPostback(new EventArgs());
		}

		#endregion

		#region ** IC1Serializable interface implementations

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1AppViewSerializer sz = new C1AppViewSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1AppViewSerializer sz = new C1AppViewSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1AppViewSerializer sz = new C1AppViewSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1AppViewSerializer sz = new C1AppViewSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion
	}
}
