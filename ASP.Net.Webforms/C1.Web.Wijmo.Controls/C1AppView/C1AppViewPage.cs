﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1AppView
{
	/// <summary>
	/// Represents a Appview page in an ASP.NET Web page.
	/// </summary>
	[ToolboxData("<{0}:C1AppViewPage runat=server></{0}:C1AppViewPage>")]
	[ToolboxBitmap(typeof(C1AppViewPage), "C1AppViewPage.png")]
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.AppViewPageDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.AppViewPageDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.AppViewPageDesigner, C1.Web.Wijmo.Controls.Design.3" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1AppView.AppViewPageDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	public class C1AppViewPage : WebControl, INamingContainer
	{
		#region ** fields
		private Hashtable _properties = new Hashtable();
		#endregion

		#region constructor
		public C1AppViewPage() 
		{ 
		}
		#endregion

		#region ** properties

		/// <summary>
		/// Defines the header title of the Appview page.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[C1Description("C1AppViewPage.HeaderTitle")]
		public string HeaderTitle
		{
			get
			{
				return _properties["HeaderTitle"] == null ? "Overview" : (string)_properties["HeaderTitle"];
			}
			set
			{
				_properties["HeaderTitle"] = value;
			}
		}

		/// <summary>
		/// Defines the header container of the appview page.
		/// </summary>
		[DefaultValue(null)]
		[Browsable(false)]
		[Layout(LayoutType.Misc)]
		[C1Description("C1AppViewPage.Header")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public C1AppViewTemplateContent Header
		{
			get
			{
				if (_properties["Header"] == null)
				{
					C1AppViewTemplateContent header = new C1AppViewTemplateContent();
					_properties["Header"] = header;
				}
				return (C1AppViewTemplateContent)_properties["Header"];
			}
			set
			{
				_properties["Header"] = value;
			}
		}

		/// <summary>
		/// Defines the content container of the appview page.
		/// </summary>
		[DefaultValue(null)]
		[Browsable(false)]
		[Layout(LayoutType.Misc)]
		[C1Description("C1AppViewPage.Content")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public C1AppViewTemplateContent Content
		{
			get
			{
				if (_properties["Content"] == null)
				{
					C1AppViewTemplateContent content = new C1AppViewTemplateContent();
					_properties["Content"] = content;
				}
				return (C1AppViewTemplateContent)_properties["Content"];
			}
			set
			{
				_properties["Content"] = value;
			}
		}

		#endregion

		#region ** override methods
		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// add data-role and data-title for the appviewpage container.
		/// </summary>
		/// <param name="writer"></param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			writer.AddAttribute("data-role", "appviewpage");
			writer.AddAttribute("data-title", HeaderTitle);
			base.RenderBeginTag(writer);
		}

		/// <summary>
		/// Render the AppView page 
		/// </summary>
		/// <param name="writer"></param>
		protected override void RenderContents(HtmlTextWriter writer)
		{
			Header.RenderControl(writer);

			Content.RenderControl(writer);
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			if (Header.Template != null)
			{
				ITemplate template = Header.Template;
				Header.TemplateContainer.Attributes.Add("data-role", "header");				//template.InstantiateIn(Header.TemplateContainer);
				Header.Template.InstantiateIn(Header.TemplateContainer);
				this.Controls.Add(Header.TemplateContainer);
			}

			if (Content.Template != null)
			{
				ITemplate template = Content.Template;
				Content.TemplateContainer.Attributes.Add("data-role", "content");
				Content.Template.InstantiateIn(Content.TemplateContainer);
				this.Controls.Add(Content.TemplateContainer);
			}

		}

		#endregion
	}

	[ToolboxItem(false)]
	[ParseChildren(true)]
	public class C1AppViewTemplateContent : UIElement, INamingContainer
	{
		#region ** fields
		private HtmlGenericControl _container;
		private ITemplate _template;
		#endregion

		public C1AppViewTemplateContent() 
		{
			this.TemplateContainer = new HtmlGenericControl("div");
		}

		#region ** properties

		/// <summary>
		/// Gets or sets the content template used to display the content of the header or content container of appview page.
		/// </summary>
		[C1Description("C1AppViewTemplateContent.Template")]
		[Browsable(false)]
		[DefaultValue(null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Misc)]
		public ITemplate Template
		{
			get
			{
				return this._template;
			}
			set
			{
				this._template = value;
			}
		}

		/// <summary>
		/// Gets or sets the template container.
		/// </summary>
		/// <value>The template container.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public HtmlGenericControl TemplateContainer
		{
			get
			{
				return this._container;
			}
			set
			{
				this._container = value;
			}
		}

		#endregion

		#region ** Override Methods

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (Template != null)
			{
				_container.RenderControl(writer);
			}
		}

		#endregion
	}
}
