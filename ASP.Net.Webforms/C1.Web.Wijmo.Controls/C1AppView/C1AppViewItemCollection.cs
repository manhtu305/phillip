﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Data;
using System.Diagnostics;
using System.Drawing.Design;

namespace C1.Web.Wijmo.Controls.C1AppView
{
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;
	using C1.Web.Wijmo.Controls.Base.Collections;

	/// <summary>
	/// Represents a collection of Appview item.
	/// </summary>
	public class C1AppViewItemCollection : C1ObservableItemCollection<C1AppView, C1AppViewItem>
	{

		internal ulong Version = 1;

		/// <summary>
		/// Initializes a new instance of the AccordionPaneCollection class.
		/// </summary>
		/// <param name="owner">Parent C1AppView class instance.</param>
		public C1AppViewItemCollection(C1AppView owner)
			: base(owner)
		{
			Debug.Assert(owner != null, "C1AppViewItemCollection - owner is null");
		}

		/// <summary>
		/// Removes all items from collection.
		/// </summary>
		public new void Clear()
		{
			Version++;
			base.ClearItems();
		}

		/// <summary>
		/// Adds a new pane item to the end of the list.
		/// </summary>
		/// <param name="child"></param>
		public new void Add(C1AppViewItem child)
		{
			if (child != null)
			{
				this.Insert(this.Count, child);
			}
		}

		/// <summary>
		/// Insert a <see cref="C1AppViewItem"/> item into a specific position in the collection.
		/// </summary>
		/// <param name="index">Item position. Value should be greater or equal to 0</param>
		/// <param name="child">The new <see cref="C1AppViewItem"/> item.</param>
		public new void Insert(int index, C1AppViewItem child)
		{
			Version++;
			child.Owner = Owner;
			if (this.Count == 0)
				this.InsertItem(0, child);
			else
				this.InsertItem(index, child);
		}

		/// <summary>
		/// Removes the specified item from the list.
		/// </summary>
		/// <param name="child">The <see cref="C1AppViewItem"/> item to be removed.</param>
		public new void Remove(C1AppViewItem child)
		{
			RemoveAt(IndexOf(child));
		}

		/// <summary>
		/// Removes an item from the list using its index.
		/// </summary>
		/// <param name="index">The index of the <see cref="C1AppViewItem"/> item to be removed.</param>
		public new void RemoveAt(int index)
		{
			Version++;
			this.RemoveItem(index);
		}
	}
}
