﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1AppView
{
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Represents a single tab page in a C1Tabs. 
	/// </summary>
	[ToolboxItem(false)]
	[PersistChildren(true)]
	[ParseChildren(false)]
	public class C1AppViewItem : UIElement
	{
		#region Fields

		private C1AppView _owner;
		private Hashtable _properties = new Hashtable();

        #endregion

        #region Consturctor

        /// <summary>
		/// Constructor of the C1AppViewItem class.
        /// </summary>
		public C1AppViewItem()
			: base()
		{
		}

		/// <summary>
		/// Constructor of the C1AppViewItem class.
		/// </summary>
		/// <param name="appView"></param>
		public C1AppViewItem(C1AppView appView)
			: base()
        {
			this._owner = appView;
        }

        #endregion

        #region Properties

		internal C1AppView Owner
        {
            get { return this._owner; }
			set { this._owner = value; }
        }

        /// <summary>
        /// Gets or sets the text displayed in the menu of the appview.
        /// </summary>
        [Localizable(true)]
        [DefaultValue("")]
		[C1Category("Category.Behavior")]
		[C1Description("C1AppViewItem.Text")]
		[Layout(LayoutType.Behavior)]
        public string Text
        {
            get
            {
				return _properties["Text"] == null ? "" : (String)_properties["Text"];
            }
            set
            {
				_properties["Text"] = value;
            }
        }

		/// <summary>
		/// Gets or sets the page url of the menu that loaded by the appview.
		/// </summary>
		[Localizable(true)]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
		[C1Description("C1AppViewItem.AppViewPageUrl")]
		[Layout(LayoutType.Behavior)]
		public string AppViewPageUrl
		{
			get
			{
				return _properties["AppViewPageUrl"] == null ? "" : (String)_properties["AppViewPageUrl"];
			}
			set
			{
				_properties["AppViewPageUrl"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the DataTheme setting of the Appview item.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
		[C1Description("C1AppViewItem.DataTheme")]
		[Layout(LayoutType.Appearance)]
		public String ThemeSwatch
		{
			get
			{
				return _properties["ThemeSwatch"] == null ? "" : (String)_properties["ThemeSwatch"];
			}
			set
			{
				_properties["ThemeSwatch"] = value;
			}
		}

        #endregion
	}
}