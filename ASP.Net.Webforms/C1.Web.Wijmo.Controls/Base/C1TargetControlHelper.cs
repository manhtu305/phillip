﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using C1.Wijmo.Licensing;

namespace C1.Web.Wijmo.Controls
{
	class C1TargetControlHelper
	{
        public static string ResolveEmbeddedResourceUrl(string resourceName, Page page, bool combinedScript)
        {
            if (!combinedScript)
            {
                return page.ClientScript.GetWebResourceUrl(typeof(C1TargetControlBase), resourceName);
            }
            else
            {
                return resourceName;
            }
        }

		internal static string ResolvePhysicalPath(System.Web.UI.Control control, string url)
		{
			System.ComponentModel.ISite site = GetSite(control);
			if (site == null) return url;

			System.Web.UI.Design.IWebApplication service = (System.Web.UI.Design.IWebApplication)site.GetService(typeof(System.Web.UI.Design.IWebApplication));
			System.Web.UI.Design.IProjectItem item = service.GetProjectItemFromUrl(url);
			if (item == null)
				return url.Replace("~/", service.RootProjectItem.PhysicalPath);

			string path = item.PhysicalPath;
			return path;
		}

		internal static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}
			}
    class C1TargetControlHelper<T> : C1TargetControlHelper
        where T : WebControl, IC1TargetControl
    {
        public T Control { get; private set; }
        public Page Page
        {
            get { return Control.Page; }
        }
        ScriptManager _scriptManager;

        public C1TargetControlHelper(T control)
        {
            if (control == null)
                throw new ArgumentNullException("control");
            this.Control = control;
        }

        #region Json

        JsonSerializableHelper _jsonSerializableHelper;
        public JsonSerializableHelper JsonSerializableHelper
        {
            get
            {
                if (_jsonSerializableHelper == null)
                    _jsonSerializableHelper = new JsonSerializableHelper(Control, Control);
                return _jsonSerializableHelper;
            }
        }

        public void RenderJsonDataField(HtmlTextWriter writer)
        {
            JsonSerializableHelper.RenderJsonDataField(writer);
        }

        #endregion

        private void UpdateDisabledByParentState(bool enabledState)
        {
            // fix for [27824] [ASPNET Wijmo][C1EventsCalendar] Unlike other C1Controls , C1EventsCalendar placed in C1TabPage remain dim after setting "Enabled" property of C1Tabs  OFF and ON
            if (!enabledState)
            {
                if (Control.ViewState["DisabledByParentOriginalState"] == null)
                {
                    // Store original enabled state.
                    Control.ViewState["DisabledByParentOriginalState"] = Control.Enabled;
                }
                Control.Enabled = false;
            }
            else if (Control.ViewState["DisabledByParentOriginalState"] != null)
            {
                // restore original enabled state
                Control.Enabled = (bool)Control.ViewState["DisabledByParentOriginalState"];
                Control.ViewState["DisabledByParentOriginalState"] = null;
            }
            else
            {
                // do nothing when parent is enabled and DisabledByParentOriginalState is null.
            }
        }
        public void EnsureEnabledState()
        {
            // check to see if any of our parents are disabled
            Control ctl = Control.Parent;
            bool enabled = Control.Enabled;
            while (ctl != null)
            {
                if (ctl is WebControl && !((WebControl)ctl).Enabled)
                {
                    UpdateDisabledByParentState(false);
                    Control.Enabled = false;
                    return;
                }
                ctl = ctl.Parent;
            }
            UpdateDisabledByParentState(true);
        }

        public ScriptManager ScriptManager
        {
            get
            {
                if (this._scriptManager == null)
                {
                    Page page = Control.Page;
                    if (page == null)
                    {
                        throw new InvalidOperationException("Page cannot be null");
                    }
                    this._scriptManager = ScriptManager.GetCurrent(page);
                }
                return this._scriptManager;
            }
        }

        public void RegisterOnSubmitStatement(string callback = null)
        {
            string script = string.IsNullOrEmpty(callback)
                ? string.Format("$('#{0}').wijSaveState('{1}');", Control.ClientID, Control.WidgetName)
                : string.Format("$('#{0}').wijSaveState('{1}', '{2}');", Control.ClientID, Control.WidgetName, callback);

            string scriptId = Control.ClientID + "_SubmitScript";

            if (this.ScriptManager != null)
            {
                ScriptManager.RegisterOnSubmitStatement(Control, typeof(C1TargetControlBase), scriptId, script);
            }
            else
            {
                Control.Page.ClientScript.RegisterOnSubmitStatement(typeof(C1TargetControlBase), scriptId, script);
            }
        }

        /// <summary>
        /// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
        /// </summary>
        /// <param name="targetControl">The server control to which the extender is associated.</param>
        /// <returns>
        /// An enumeration of WidgetDescriptor objects.
        /// </returns>
        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            WidgetDescriptor descriptor = new WidgetDescriptor(Control.WidgetName, ("#" + Control.ClientID));
            WidgetObjectBuilder.DescribeWidget(Control, descriptor);
            return new List<ScriptDescriptor>(new WidgetDescriptor[] { descriptor });
        }

        /// <summary>
        /// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
        /// </summary>
        /// <param name="targetSelector">The target selector.</param>
        /// <returns>
        /// An enumeration of WidgetDescriptor objects.
        /// </returns>
        public IEnumerable<ScriptDescriptor> GetScriptDescriptors(string targetSelector)
        {
            WidgetDescriptor descriptor = new WidgetDescriptor(Control.WidgetName, targetSelector);
            WidgetObjectBuilder.DescribeWidget(Control, descriptor);
            return new List<ScriptDescriptor>(new WidgetDescriptor[] { descriptor });
        }

        /// <summary>
        /// Show about message box.
        /// </summary>
        public void ShowAbout()
        {
            Type t = typeof(C1TargetControlBase).Assembly.GetType("C1.Util.Licensing.ProviderInfo");
            System.Reflection.MethodInfo _about = t.GetMethod("ShowAboutBox", System.Reflection.BindingFlags.Public |
                                              System.Reflection.BindingFlags.Static |
                System.Reflection.BindingFlags.NonPublic, null,
                  new Type[] { typeof(object) }, null);

            _about.Invoke(Control, new object[] { Control });
        }

        #region ** register include/startup scripts

        #region register extender scripts via script manager

        void RegisterIncludesWithScriptManager()
        {
            WijmoResourceManager.RegisterScriptResource(Control);
            ScriptManager.RegisterScriptControl<T>(Control);

            // Fixed the memory leak issue when the control is inside of updatepanel. 
            // when init the page, added the dispose callback to the global dispose callbacks. in _updatepaenl method, 
            // call these cached dispose callbacks.
            ScriptManager.RegisterStartupScript(Control, typeof(C1TargetControlBase), "disposeCallbackData" + Control.ClientID,
                string.Format("$.disposeCallbackDatas['{0}']='{1}';", Control.ClientID, Control.WidgetName), true);
        }

        void RegisterIncludesWithoutScriptManager()
        {
            WijmoResourceManager.RegisterScriptResource(Control);
        }

        public void RegisterIncludes()
        {
            if (this.ScriptManager != null)
            {
                // Scripts registered by ExtenderControl class.
                RegisterIncludesWithScriptManager();
            }
            else
            {
                // ScriptManager does not exists, register scripts without script manager.
                RegisterIncludesWithoutScriptManager();
            }
        }

        #endregion

        #region register extender scripts without script manager
        
        public void RegisterScriptDescriptorsWithScriptManager()
		{
            bool isCallback = Page != null && Page.IsCallback;

            //TFS: 398360
            int RegisterScript = ScriptManager.GetRegisteredStartupScripts().Count();

            if (!isCallback && RegisterScript > 0) // To avoid the "Script controls must be registered using RegisterScriptControl() before calling RegisterScriptDescriptors()." exception when the Render() method is called directly during CallBack && PostBack.
            {
                ScriptManager.RegisterScriptDescriptors(Control);
            }
		}
        
        public void RegisterScriptDescriptorsWithoutScriptManager()
		{
			List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)Control.GetScriptDescriptors();
			foreach (WidgetDescriptor desc in scriptDescr)
			{
				ScriptManager.RegisterStartupScript(Control, Control.GetType(), "c" + Control.ClientID, desc.GetScriptInternal(Control.WijmoControlMode, WijmoResourceManager.IsControlInAppviewPage(Control)), true);
			}
		}

		public void RegisterScriptDescriptors()
		{
			if (this.ScriptManager != null)
			{
				// Scripts registered by ExtenderControl class.
				RegisterScriptDescriptorsWithScriptManager();
			}
			else
			{
				// ScriptManager does not exists, register scripts without script manager.
				RegisterScriptDescriptorsWithoutScriptManager();
			}
		}


		#endregion
		#endregion

		#region ** register css stylesheets


        public void RegisterRunTimeStylesheets()
        {
			WijmoResourceManager.RegisterCssResource(Control);
            }

		public void RegisterDesignTimeStyleSheets(HtmlTextWriter writer)
		{
			if (HttpContext.Current != null)
				return;

			string theme = Control.Theme;
			string lowerCaseTheme = theme.ToLower();
			bool useDefaultTheme = false;
			if (lowerCaseTheme.StartsWith("~/") ||
				lowerCaseTheme.StartsWith("http://") ||
					lowerCaseTheme.StartsWith("https://") ||
					lowerCaseTheme.EndsWith(".css"))
			{
				try
				{
					/*
					string filePath = C1TargetControlBase.ResolvePhysicalPath(Control, theme);
					FileInfo fileInfo = new FileInfo(filePath);
					if (fileInfo.Exists)
					{

						string css = "<link href=\"" + ResolveClientUrl(theme) + "\" id=\"" +
							"wijmo_theme_designtime" + "\" type=\"text/css\" rel=\"stylesheet\" />";
						writer.Write(css);
						theme = string.Empty;
						useDefaultTheme = false;
					}
					else
					{
						useDefaultTheme = true;
					}*/
                    
                    string clientUrl = Control.ResolveClientUrl(theme);
                    if (lowerCaseTheme.StartsWith("~/") && lowerCaseTheme.EndsWith(".css"))
                    {
                        string css = string.Empty;
                        try
                        {
                            int pos = clientUrl.LastIndexOf("/");
                            string preFixedPath = clientUrl.Substring(0, pos + 1);
                            writer.Write("<style media=\"all\" type=\"text/css\">");
                            css = File.ReadAllText(ResolvePhysicalPath(Control, clientUrl));
                            if (!string.IsNullOrEmpty(preFixedPath))
                            {
                                css = Regex.Replace(css, @"url\((.+)\)", new MatchEvaluator(match =>
                                {
                                    string value = match.Groups[0].Value;
                                    string relUrl = match.Groups[1].Value;

                                    return value.Replace(relUrl, preFixedPath + relUrl);
                                }));
                            }
                            writer.Write(css);
                            writer.Write("</style>");
                        }
                        catch
                        {
                            css = "<link href=\"" + clientUrl + "\" id=\"" +
                        "wijmo_theme_designtime" + "\" type=\"text/css\" rel=\"stylesheet\" />";
                            writer.Write(css);
                        }
                    }
                    else
                    {
                        string css = "<link href=\"" + clientUrl + "\" id=\"" +
						"wijmo_theme_designtime" + "\" type=\"text/css\" rel=\"stylesheet\" />";
                        writer.Write(css);
                    }
					
					theme = string.Empty;
					useDefaultTheme = false;
				}
				catch
				{
					//System.Windows.Forms.MessageBox.Show(ex.Message+"?" + ex.StackTrace);
					useDefaultTheme = true;
				}
			}
			useDefaultTheme = useDefaultTheme || Control.UseCDN && !string.IsNullOrEmpty(Control.CDNPath);
			if (useDefaultTheme)
			{
				theme = "aristo";
			}
			
			//wijmo_theme
			if (!string.IsNullOrEmpty(theme))
			{
				if (Control.WijmoControlMode == WijmoControlMode.Mobile)
				{
					//Uncomment this line after wijmo-mobile theme css is finished.
					/*
					RegisterDesignTimeStyleSheet(writer, string.Format("C1.Web.Wijmo.Controls.Resources.themes.jquery_mobile.{0}",
						theme.Replace("-", "_") + "-mobile.", GetThemeFileNameByThemeName(theme)));
					 * */
					RegisterDesignTimeStyleSheet(writer, string.Format("C1.Web.Wijmo.Controls.Resources.themes.wijmo.{0}",
						ResourcesConst.WIJMO_MOBILE_CSS));
				}
				else
				{
					RegisterDesignTimeStyleSheet(writer, string.Format("C1.Web.Wijmo.Controls.Resources.themes.{0}.{1}",
						theme.Replace("-", "_"), WijmoResourceManager.WijmoThemeFileName));
				}
			}
			OrderedDictionary cssReferences = WidgetDependenciesResolver.GetCssReferences(Control);
			IDictionaryEnumerator ien = cssReferences.GetEnumerator();
			while (ien.MoveNext())
			{
				RegisterDesignTimeStyleSheet(writer, (string)ien.Value);
			}
		}

		/// <summary>
		/// Register design-time CSS stylesheet.
		/// </summary>
		/// <param name="cssResourceName">CSS resource name.</param>
		/// <remarks>
		/// The cssResourceName is the link to an resource element.
		/// </remarks>
		public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		{
			//string css = "<link href=\"" + cssUrl + "\" id=\"" +  cssID  + "\" type=\"text/css\" rel=\"stylesheet\" />";
			string s = GetResourceCssContent(cssResourceName, typeof(C1TargetControlBase).Assembly);
			writer.Write("<style media=\"all\" type=\"text/css\">");
			writer.Write(s);
			writer.Write("</style>");
		}

		/// <summary>
		/// Returns content of the css resource embedded into assembly.
		/// </summary>
		/// <param name="name">The case-sensitive name of the manifest resource being requested.</param>
		/// <param name="assembly">The assembly that contains the resource.</param>
		/// <returns></returns>
		internal string GetResourceCssContent(string name, Assembly assembly)
		{

			//Assembly.GetAssembly(_control.GetType());
			string input = string.Empty;
			try
			{
				using (var reader = new StreamReader(assembly.GetManifestResourceStream(name)))
				{
					input = reader.ReadToEnd();
				}
			}
			catch (Exception)
			{
				throw new HttpException("Please, make sure you are using correct theme name. (Resource " + name + " does not exists in assembly).");
			}
			return Regex.Replace(input, "<%\\s*=\\s*WebResource\\(\"(?<resourceName>[^\"]*)\"\\)\\s*%>", new MatchEvaluator(WebResourceEvaluator));
		}

		private string WebResourceEvaluator(Match match)
		{
			return GetPage(Control).ClientScript.GetWebResourceUrl(Control.GetType(), match.Groups["resourceName"].Value);
		}

		private Page GetPage(Control control)
		{
			Page page = null;
			if (control == null)
			{
				throw new HttpException("(e9324) Can't get Page for null control.");
			}
			if (control.Page != null)
				page = control.Page;
			for (Control c = control.Parent; c != null; c = c.Parent)
			{
				if (c.Page != null)
				{
					page = c.Page;
					break;
				}
			}
			if (page != null)
				_vs2005DesignModePage = page;
			else
				page = _vs2005DesignModePage;
#if DEBUG
			if (page == null)
			{
				throw new HttpException("(e9325) Can't get Page for WebControl with id '" + control.ID + "'.");
			}
#endif
			return page;
		}

		Page _vs2005DesignModePage = null;

		#endregion
	}
}