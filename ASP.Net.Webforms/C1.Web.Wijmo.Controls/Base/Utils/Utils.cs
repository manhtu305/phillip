﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace C1.Web.Wijmo
{
	internal static class Utils
	{
		private const string _hiddenClass = "ui-helper-hidden-accessible";

		public static string GetHiddenClass()
		{
			//fix tfs issue 27988
			string userAgent = (HttpContext.Current == null ? string.Empty :
			HttpContext.Current.Request.UserAgent ?? string.Empty);
			bool isSafari = userAgent.IndexOf("safari", StringComparison.OrdinalIgnoreCase) > -1
                && userAgent.IndexOf("chrome", StringComparison.OrdinalIgnoreCase) < 0;
			return isSafari ? string.Empty : _hiddenClass;
		}
	}
}
