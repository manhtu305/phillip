﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls
{
    /// <summary>
    /// UTFSupport
    /// </summary>
    public class UTFSupport
    {

        /// <summary>
        /// Encode string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        static public string EncodeString(string s)
        {
            for (int i = 0; i < c__escapeArr1.Length; i++)
            {
                s = s.Replace(c__escapeArr1[i] + "", c__escapeArr2[i] + "");
            }
            // s = System.Web.HttpUtility.UrlEncode(s, Encoding.UTF8);
            // Don't restore line above ^^^^
            return s;
        }

        /// <summary>
        /// Decode string.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        static public string DecodeString(string s)
        {
            s = System.Web.HttpUtility.UrlDecode(s, Encoding.UTF8);
            for (int i = 0; i < c__escapeArr2.Length; i++)
            {
                s = s.Replace(c__escapeArr2[i] + "", c__escapeArr1[i] + "");
            }
            return s;
        }

        // fields:
        static private char[] c__escapeArr1 = new char[] { '\n', '\r', '"', '@', '+', '\'', '<', '>', '%', '{', '}' };
        static private string[] c__escapeArr2 = new string[] { "!ESC!NN!", "!ESC!RR!", "!ESC!01!", "!ESC!02!", "!ESC!03!", "!ESC!04!", "!ESC!05!", "!ESC!06!", "!ESC!07!", "!ESC!08!", "!ESC!09!" };
        //static private string[] c__escapeArr3 = new string[] {"(\n)"     , "(\r)"    , "(\")"    , "(@)"     , "(\\+)" , "(')", "(<)", "(>)" };
    }
}
