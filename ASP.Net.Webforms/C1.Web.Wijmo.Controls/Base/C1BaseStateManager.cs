﻿using System;
using System.Web.UI;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls
{

	/// <summary>
	/// Base class to support view state management 
	/// for a server control.
	/// </summary>
	public abstract class C1BaseStateManager : IStateManager
	{

		#region ** fields
		private StateBag _stateBag = null;
		private bool _marked = false;
		#endregion

		#region ** constructor
		/// <summary>
		/// Constructor.
		/// </summary>
		public C1BaseStateManager()
		{
		}
		#endregion end of ** constructor.

		#region ** protected properties
		/// <summary>
		/// Gets a value indicating whether a server control
		/// is tracking its view state changes.
		/// </summary>
		protected bool IsTrackingViewState
		{
			get
			{
				return _marked;
			}
		}

		/// <summary>
		/// Manages the view state.
		/// </summary>
		protected StateBag ViewState
		{
			get
			{
				if (_stateBag == null)
				{
					_stateBag = new StateBag();
					if (IsTrackingViewState)
						((IStateManager)_stateBag).TrackViewState();
				}

				return _stateBag;
			}
		}
		#endregion end of ** protected properties.

		#region ** protected methods
		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		protected V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.ViewState[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.ViewState[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		protected void SetPropertyValue<V>(string propertyName, V value)
		{
			this.ViewState[propertyName] = value;
		}

		/// <summary>
		/// Sets the state of the System.Web.UI.StateBag object as well as the System.Web.SessionState.ISessionStateItemCollection.Dirty
		/// property of each of the System.Web.UI.StateItem objects contained by it.
		/// </summary>
		protected internal virtual void SetDirty()
		{
			if (_stateBag != null)
				_stateBag.SetDirty(true);
		}

		/// <summary>
		/// Loads the server control's previously saved
		///  view state to the control.
		/// </summary>
		/// <param name="savedState">An System.Object that contains the saved view state values for the control.</param>
		protected virtual void LoadViewState(object savedState)
		{
			if (savedState != null)
				((IStateManager)ViewState).LoadViewState(savedState);
		}

		/// <summary>
        /// Saves the changes to a server control's view
		///     state to an System.Object.
		/// </summary>
		/// <returns>The System.Object that contains the view state changes.</returns>
		protected virtual object SaveViewState()
		{
			return (_stateBag != null) ? ((IStateManager)_stateBag).SaveViewState() : null;
		}

		/// <summary>
		/// Instructs the server control to track changes
		/// to its view state.
		/// </summary>
		protected virtual void TrackViewState()
		{
			_marked = true;

			if (_stateBag != null)
				((IStateManager)_stateBag).TrackViewState();
		}
		#endregion end of ** protected methods.

		#region ** IStateManager implementations
		bool IStateManager.IsTrackingViewState
		{
			get
			{
				return IsTrackingViewState;
			}
		}

		void IStateManager.LoadViewState(object savedState)
		{
			LoadViewState(savedState);
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}
		#endregion end of ** IStateManager implementations
	}
}