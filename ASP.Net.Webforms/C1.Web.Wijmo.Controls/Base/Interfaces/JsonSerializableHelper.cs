using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections;

namespace C1.Web.Wijmo.Controls
{
    /// <summary>
    /// Implementation of the IClientSerializable interface.
    /// </summary>
    public class JsonSerializableHelper : IJsonSerializable
    {        

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="control"></param>
        public JsonSerializableHelper(WebControl control)
        {
            this.control = control;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="control">WebControl.</param>
        /// <param name="urlResolutionService">IUrlResolutionService.</param>
        public JsonSerializableHelper(Control control, IUrlResolutionService urlResolutionService)
        {
            this.control = control;
            this._urlResolutionService = urlResolutionService;
        }

        private Control control;
        private IUrlResolutionService _urlResolutionService;

        #region IClientSerializable Members
        /// <summary>
        /// Gets the ID of the hiiden field that contains the Json data.
        /// </summary>
        public string DataFieldClientId
        {
            get
            {
                return this.control.ClientID + "__jsonserverstate";
            }
        }        

        /// <summary>
        /// Outputs the control state serialized to Json to a hidden field.
        /// </summary>
        /// <param name="writer"></param>
        public void RenderJsonDataField(System.Web.UI.HtmlTextWriter writer)
        {
            //string s = JsonHelper.ObjectToString(this.control, this._urlResolutionService);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, DataFieldClientId + "_ffcache");// fix for FF cache!
            writer.AddAttribute(HtmlTextWriterAttribute.Id, DataFieldClientId);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            //writer.AddAttribute(HtmlTextWriterAttribute.Value, s);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
        }
        #endregion

        /// <summary>
        /// Get JSON data.
        /// </summary>
        /// <param name="postCollection"></param>
        /// <returns></returns>
        public Hashtable GetJsonData(NameValueCollection postCollection)
        {
            if (string.IsNullOrEmpty(postCollection[DataFieldClientId]))
            {
                if (!string.IsNullOrEmpty(postCollection[DataFieldClientId + "_ffcache"]))
                {
                    return JsonHelper.StringToObject(postCollection[DataFieldClientId + "_ffcache"]);
                }
                else
                {
                    return new Hashtable();
                }
            }
            return JsonHelper.StringToObject(postCollection[DataFieldClientId]);
        }
    }
}
