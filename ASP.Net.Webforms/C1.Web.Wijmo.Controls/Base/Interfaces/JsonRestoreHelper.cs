using System;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;

namespace C1.Web.Wijmo.Controls
{

	/// <summary>
	/// Implementation of the IJsonRestore interface.
	/// </summary>
	public class JsonRestoreHelper
	{

		#region ** private methods
		private static string GetWidgetOptionName(PropertyDescriptor pd)
		{
			AttributeCollection attribs = pd.Attributes;
			WidgetOptionNameAttribute attrib = attribs[typeof(WidgetOptionNameAttribute)] as WidgetOptionNameAttribute;

			if (attrib != null && !string.IsNullOrEmpty(attrib.Name))
			{
				return attrib.Name;
			}

			string name = pd.Name;
			WidgetEventAttribute eAttrib = attribs[typeof(WidgetEventAttribute)] as WidgetEventAttribute;

			if (eAttrib != null)
			{
				name = name.Replace("OnClient", "").ToCamel(); // events names are changed to camel-case format since Wijmo 1.3.0.
			}

			return name.ToCamel();
		}

		private static void SetValue(PropertyDescriptor pd, object instance, object value, Hashtable states)
		{
			object instanceValue = pd.GetValue(instance);

			if (typeof(IFunctionType).IsInstanceOfType(instanceValue))
			{
				SetIFunctionOptionValue(pd, instance, value, instanceValue, states);
			}
			else if (typeof(ICustomOptionType).IsInstanceOfType(instanceValue))
			{
				SetICustomOptionValue(pd, instance, value, instanceValue);
			}
			else if (typeof(IJsonRestore).IsInstanceOfType(instanceValue))
			{
				SetIJsonRestoreValue(pd, instance, value, instanceValue);
			}
			else if (typeof(IDictionary).IsInstanceOfType(instanceValue))
			{
				SetIDictionaryValue(pd, instance, value, instanceValue);
			}
			else if (typeof(IList).IsInstanceOfType(instanceValue))
			{
				SetIListValue(pd, instance, value, instanceValue);
			}
			else if (typeof(Enum).IsInstanceOfType(instanceValue))
			{
				SetEnumValue(pd, instance, value);
			}
			else if (typeof(System.Web.UI.WebControls.Unit).IsInstanceOfType(instanceValue))
			{
				SetUnitValue(pd, instance, value);
			}
			else if(typeof(Color).IsInstanceOfType(instanceValue))
			{
				SetColorValue(pd, instance, value);
			}
			else if (typeof(Point).IsInstanceOfType(instanceValue))
			{
				SetPointValue(pd, instance, value);
			}
			else
			{
				pd.SetValue(instance, value);
			}
		}

		private static void SetIFunctionOptionValue(PropertyDescriptor pd, object instance, object value, object instanceValue, Hashtable states)
		{
			IFunctionType content = instanceValue as IFunctionType;
			object val = value;
			
			if (content == null)
			{
				return;
			}
			Hashtable ht = (Hashtable)states["innerStates"];
			
			if (ht != null)
			{
				string helper = (string)ht[pd.Name.ToLowerInvariant()];
				if (!string.IsNullOrEmpty(helper))
				{
					content.Function = helper;
				}
			}
			else
			{
				Hashtable items = val as Hashtable;
				if (items == null)
				{
					content.Function = "";
				}
				else
				{
					if (((string)items["type"]) == "fun")
					{
						val = items["value"];
						content.Function = (string)val;
					}
					else
					{
						content.Function = "";
					}
				}
			}
			content.DeSerializeValue(val);
			SetInstanceValue(pd, instance, instanceValue);
		}

		private static void SetIJsonRestoreValue(PropertyDescriptor pd, object instance, object value, object instanceValue)
		{
			(instanceValue as IJsonRestore).RestoreStateFromJson(value);
			SetInstanceValue(pd, instance, instanceValue);
		}

		private static void SetICustomOptionValue(PropertyDescriptor pd, object instance, object value, object instanceValue)
		{
			(instanceValue as ICustomOptionType).DeSerializeValue(value);
			SetInstanceValue(pd, instance, instanceValue);
		}

		private static void SetInstanceValue(PropertyDescriptor pd, object instance, object instanceValue)
		{
			if (pd.PropertyType.IsValueType)
			{
				pd.SetValue(instance, instanceValue);
			}
		}

		private static void SetIDictionaryValue(PropertyDescriptor pd, object instance, object value, object instanceValue)
		{
			if (!(value is IDictionary))
			{
				return;
			}

			IDictionary option = instanceValue as IDictionary;
			option.Clear();

			foreach (DictionaryEntry de in value as IDictionary)
			{
				option.Add(de.Key, de.Value);
			}
		}

		private static void SetIListValue(PropertyDescriptor pd, object instance, object value, object instanceValue)
		{
			if (!(value is IList))
			{
				return;
			}

			IList option = instanceValue as IList;

			if (option.IsFixedSize)
			{
				//pd.SetValue(instance, value);
				ArrayItemTypeAttribute attrib = pd.Attributes[typeof(ArrayItemTypeAttribute)] as ArrayItemTypeAttribute;
				Type itemType = attrib == null ? typeof(string) : attrib.ItemType;
				Type type = Type.GetType("System.Collections.Generic.List`1[" + itemType.ToString() + "]");
				IList items = System.Activator.CreateInstance(type) as IList;

				foreach (object item in value as IList)
				{
					items.Add(item);
				}
				object val = type.GetMethod("ToArray").Invoke(items, null);

				pd.SetValue(instance, val);
			}
			else
			{
				option.Clear();

				CollectionItemTypeAttribute attrib = pd.Attributes[typeof(CollectionItemTypeAttribute)] as CollectionItemTypeAttribute;

				foreach (object item in value as IList)
				{
					if (attrib == null || attrib.ItemType == null)
					{
						try
						{
							option.Add(item);
						}
						catch {
							try
							{
								Type itemType = option.GetType().GetGenericArguments()[0];
								TypeConverter converter = TypeDescriptor.GetConverter(itemType);
								if (converter.CanConvertFrom(typeof(string)))
								{
									option.Add(converter.ConvertFromString(item.ToString()));
								}
							}
							catch { }
						}
						continue;
					}

					object newItem = System.Activator.CreateInstance(attrib.ItemType);
					if (typeof(ICustomOptionType).IsInstanceOfType(newItem))
					{
						((ICustomOptionType)newItem).DeSerializeValue(item);
					}
					else
					{
						RestoreStateFromJson(newItem, item);
					}
					option.Add(newItem);
				}
			}
		}

		private static void SetEnumValue(PropertyDescriptor pd, object instance, object value)
		{
			if (!(value is string))
			{
				pd.SetValue(instance, value);
				return;
			}

			string[] values = ((string)value).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			int enumVal = 0;

			foreach (string val in values)
			{
				enumVal |= (int)Enum.Parse(pd.PropertyType, val, true);
			}

			pd.SetValue(instance, enumVal);
		}

		private static void SetUnitValue(PropertyDescriptor pd, object instance, object value)
		{
			pd.SetValue(instance, System.Web.UI.WebControls.Unit.Parse(value.ToString()));
		}

		private static void SetColorValue(PropertyDescriptor pd, object instance, object value)
		{
			pd.SetValue(instance, C1WebColorConverter.ToColor(value.ToString()));
		}

		private static void SetPointValue(PropertyDescriptor pd, object instance, object value)
		{
			if (!(value is Hashtable))
			{
				return;
			}

			Hashtable pointValue = value as Hashtable;
			int x;
			int y;
			if (pointValue.Count == 2 &&
				(pointValue.ContainsKey("x") && int.TryParse(pointValue["x"].ToString(), out x))
				&& (pointValue.ContainsKey("y") && int.TryParse(pointValue["y"].ToString(), out y)))
			{
				pd.SetValue(instance, new Point(x, y));
			}
		}
		
		#endregion end of ** private methods.

		/// <summary>
		/// Restore the states from the json string.
		/// </summary>
		/// <param name="instance">The instance whose properties need be resored.</param>
		/// <param name="state">The state which is parsed from the json string.</param>
		public static void RestoreStateFromJson(object instance, object state)
		{
			Hashtable states = state as Hashtable;

			if (instance == null || states == null)
			{
				return;
			}

			PropertyDescriptorCollection props = TypeDescriptor.GetProperties(instance);
			MethodInfo[] mis = instance.GetType().GetMethods();
			ArrayList methods = new ArrayList();

			foreach (MethodInfo mi in mis)
			{
				if (mi.Name.StartsWith("C1Deserialize"))
				{
					methods.Add(mi);
				}
			}

			foreach (PropertyDescriptor pd in props)
			{
				if (pd.Name == "Enabled" && 
					states["Enabled"] == null &&
					states["disabled"] != null)
				{
					pd.SetValue(instance, !((bool)states["disabled"]));
					continue;
				}

				if (pd.Attributes[typeof(ObsoleteAttribute)] != null) 
				{
					continue;
				}

				string optionName = GetWidgetOptionName(pd);

				if (states[optionName] == null)
				{
					continue;
				}

				object value = states[optionName];

				try
				{
					bool deserialized = false;

					foreach (MethodInfo mi in methods)
					{
						//Here we will invoke the C1DeserializeXXX which definied by every control if
						//it has some properties that need be deserialize by itself.
						if (mi.Name.Equals("C1Deserialize" + pd.Name))
						{
							mi.Invoke(instance, new object[] { value });
							deserialized = true;
							break;
						}
					}

					if (!deserialized)
					{
						SetValue(pd, instance, value, states);
					}
				}
				catch
				{
					try
					{
						if (pd.Converter.CanConvertFrom(typeof(string)))
						{
							pd.SetValue(instance, pd.Converter.ConvertFromString(value.ToString()));
						}
					}
					catch { }
				}
			}
		}
	}

	/// <summary>
	///  Converts a predefined color name or an RGB color value to and 
	///  from a System.Drawing.Color object.
	/// </summary>
	public class C1WebColorConverter : WebColorConverter
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public C1WebColorConverter()
			: base()
		{

		}

		#region --- public override ---

		/// <summary>
		/// ConvertFrom override.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			try
			{
				return base.ConvertFrom(context, culture, value);
			}
			catch (Exception)
			{
				return null;
			}
		}

		/// <summary>
		/// ConvertTo override
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			try
			{
				return base.ConvertTo(context, culture, value, destinationType);
			}
			catch (Exception)
			{
				return null;
			}
		}
		#endregion

		#region --- public static ---

		/// <summary>
		/// Converts Color type to string representation.
		/// </summary>
		/// <param name="c">Color to convert.</param>
		/// <returns></returns>
		public static string ToStringHexColor(Color c)
		{
			if (c != Color.Empty)
			{
				if (!c.IsSystemColor && c.IsNamedColor)
				{
					string ret = c.Name;
					ret = ret.Replace("LightGray", "LightGrey");
					return ret;
				}

				return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
			}
			return "";
		}

		/// <summary>
		/// Convert string to Color type.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Color ToColor(string s)
		{
			if (s != null)
				s = s.Replace("LightGrey", "LightGray");
			return (Color)new WebColorConverter().ConvertFromString(s);
		}

		#endregion
	}
}
