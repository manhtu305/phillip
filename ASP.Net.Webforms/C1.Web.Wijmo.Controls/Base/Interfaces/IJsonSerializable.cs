using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls
{

    /// <summary>
    /// Interface for serializing controls to client.
    /// </summary>
    public interface IJsonSerializable
    {

        /// <summary>
        /// Client ID of the field what contains serialized data.
        /// </summary>
        string DataFieldClientId
        {
            get;
        }

        /// <summary>
        /// Renders hidden field with json string what stores properties values of this control.
        /// </summary>
        /// <remarks>
        /// Note, current ClientID of the control will be used as prefix for the rendered field.
        /// </remarks>
        /// <param name="writer"></param>
        void RenderJsonDataField(HtmlTextWriter writer);
    }
}
