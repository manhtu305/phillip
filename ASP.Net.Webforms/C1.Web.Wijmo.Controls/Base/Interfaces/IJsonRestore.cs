﻿using System.Collections;

namespace C1.Web.Wijmo.Controls
{
    /// <summary>
    /// Interface for restore controls from client.
    /// </summary>
    public interface IJsonRestore
    {
        /// <summary>
        /// Restore control state from json object.
        /// </summary>
        /// <param name="state"></param>
        void RestoreStateFromJson(object state);
    }
}