﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using C1.Web.Wijmo;
using System.Web;
using System.Reflection;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls
{
	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	[WidgetDependenciesAttribute(typeof(WijmoBase), "framework.js", "c1json2.js")]
	public abstract partial class C1TargetControlBase : WebControl, IC1TargetControl
	{

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.C1TargetControlBase" /> class that represents a Span HTML tag.
		/// </summary>
		protected C1TargetControlBase()
		{
			this.InitializeControl();
		}

		private void InitializeControl()
		{
			_helper =  new C1TargetControlHelper<C1TargetControlBase>(this);
		}

		#endregion end of ** constructors

		#region ** fields

		C1TargetControlHelper<C1TargetControlBase> _helper;
		/// <summary>
		/// A value that indicates whether the control is in design mode.
		/// </summary>
		protected bool IsDesignMode = HttpContext.Current == null;

		protected bool? _enableCombinedJS = null;

		#endregion

		#region ** properties

		#region ** Theme / UseCDN / CDNPath

		/// <summary>
		/// Name of the theme that will be used to style the widgets. 
		/// Available embedded themes include: aristo, midnight, and ui-lightness. 
		/// Note that only one theme can be used for the whole page at one time. 
		/// Please make sure that all widget extenders have the same Theme value. 
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string Theme
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
			}
			set
			{
				if (this.Theme != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual bool UseCDN
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "WijmoUseCDN", "false"));

			}
			set
			{
				if (this.UseCDN != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoUseCDN", value.ToString());
				}
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string CDNPath
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				if (this.CDNPath != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCDNPath", value);
				}
			}
		}

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		/// <remarks>
		/// Using this property, user can specify the CDN dependencies(such as jQuery, jQuery UI, jQuery mobile, bootstrap).
		/// If some dependencies are set in this property, wijmo controls will find dependencies path in this property.  If found, use the path to load dependency, 
		/// otherwise, use the default CDN path.
		/// For example, if user wants to specify the dependencies of jQuery and jQuery UI, 
		/// he can set the value like this: ["http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.1/jquery-ui.js"]
		/// </remarks>
		[C1Description("C1Base.CDNDependencyPaths")]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string[] CDNDependencyPaths
		{
			get 
			{
				string paths = WebConfigWorker.ReadAppSetting(this, "CDNDependencyPaths", "");
				if (string.IsNullOrEmpty(paths)) 
				{
					return new string[0];
				}
				return paths.Split('|');
			}
			set 
			{
				WebConfigWorker.WriteAppSetting(this, "CDNDependencyPaths", string.Join("|", value));
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCssAdapter", value);
				}
			}
		}

		#endregion ** Theme / UseCDN / CDNPath

		internal virtual bool IsWijMobileControl
		{
			get
			{
				return true;
			}
		}

		internal virtual bool IsWijWebControl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// A value that indicates mode of the control, whether it is a mobile or web control.
		/// Note that only one value can be used for the whole website or project. 
		/// </summary>
		[C1Description("C1Base.WijmoControlMode")]
		[C1Category("Category.Behavior")]
		[DefaultValue(WijmoControlMode.Web)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual WijmoControlMode WijmoControlMode
		{
			get
			{
				return (WijmoControlMode)Enum.Parse(typeof(WijmoControlMode), WebConfigWorker.ReadAppSetting(this, "WijmoMode", "Web"));
			}
			set
			{
				if (this.WijmoControlMode != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoMode", value.ToString());
				}
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the control, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		[TypeConverter(typeof(WijmoThemeSwatchConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string ThemeSwatch
		{
			get
			{
				return GetPropertyValue<string>("ThemeSwatch", "");
			}
			set
			{
				this.SetPropertyValue<string>("ThemeSwatch", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[C1Description("C1Base.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
        /// Enable JavaScripts files combined into one file.
		/// In order to combined enabled, you must register the WijmoHttpHandler in web.config.
        /// </summary>
        [C1Description("C1Base.EnableCombinedJavaScripts")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool EnableCombinedJavaScripts
        {
            get
            {
				if(_enableCombinedJS == null)
				{
                    if (HttpContext.Current == null)//design time
                    {
                        _enableCombinedJS = bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", "false"));
                    }
                    else
                    {
                        string result = WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", null);
                        bool registedHandler = checkWijmoHttpHandlerRegisted();

                        if (string.IsNullOrEmpty(result))
                        {
                            //if user not set EnableCombinedJS, then check whether registered httphandler.
                            _enableCombinedJS = registedHandler;
                        }
                        else
                        {
                            _enableCombinedJS = bool.Parse(result);
                        }
                    }
				}

				return _enableCombinedJS.Value;
            }
            set
            {
				_enableCombinedJS = value;
                WebConfigWorker.WriteAppSetting(this, "EnableCombinedJavaScripts", value.ToString());
            }
        }

		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[C1Description("C1Base.EnableConditionalDependencies")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool EnableConditionalDependencies
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableConditionalDependencies", "false"));
			}
			set
			{
				if (this.EnableConditionalDependencies != value)
				{
					WebConfigWorker.WriteAppSetting(this, "EnableConditionalDependencies", value.ToString());
				}
			}
		}

		/// <summary>
		/// IsEnabled status
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[WidgetOption]
		public virtual bool DisabledState
		{
			get
			{
				return !this.IsParentEnabled();
			}
			set
			{
			}
		}
		
		/// <summary>
		/// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
		/// </summary>
		/// <remarks>
		/// Note that a server control is created and invisible if DisplayVisible is set to false.
		/// </remarks>
		[C1Description("C1Controls.DisplayVisible")]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue(true)]
		[WidgetOption]
		public bool DisplayVisible
		{
			get
			{
				return this.GetPropertyValue("DisplayVisible", true);
			}
			set
			{
				this.SetPropertyValue("DisplayVisible", value);
			}
		}

		#region ** hidden properties
		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor 
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor 
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.BorderStyle BorderStyle 
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.Unit BorderWidth 
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.FontInfo Font 
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor 
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Boolean Visible 
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}


		#endregion end of ** hidden properties.

		#endregion end of ** properties

		#region ** methods
		
		/// <summary>
		/// Ensure control's enabled state, check to see if any of control's parents are disabled.
		/// </summary>
		protected virtual void EnsureEnabledState()
		{
			if (!this.DesignMode)
			{
				_helper.EnsureEnabledState();
			}
		}

		protected override void OnInit(EventArgs e)
		{
            if (!this.DesignMode && 
                this.EnableCombinedJavaScripts &&
                !this.UseCDN && 
                !this.checkWijmoHttpHandlerRegisted())
            {
                throw new Exception("To Combined the JavaScripts, the Wijmo require WijmoHttpHandler in the web.config.");
            }
			base.OnInit(e);
			if (!this.DesignMode) 
			{
				WijmoResourceManager.RegisterResourceControl(this);
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (!base.DesignMode)
			{
				_helper.RegisterRunTimeStylesheets();
				base.OnPreRender(e);

				if (this is IPostBackDataHandler)
				{
					this.Page.RegisterRequiresPostBack(this);
					this.RegisterOnSubmitStatement();
				}

				_helper.RegisterIncludes();
			}
			else
			{
				base.OnPreRender(e);
			}

			this.EnsureEnabledState();
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.IsWijMobileControl && this.WijmoControlMode == WijmoControlMode.Mobile)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.MobileNotImp"));
			}
			else if (!this.IsWijWebControl && this.WijmoControlMode == WijmoControlMode.Web)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.WebNotImp"));
			}
			_helper.RegisterDesignTimeStyleSheets(writer);
			base.Render(writer);
			if (!this.IsDesignMode)
			{
				_helper.RenderJsonDataField(writer);
				this.RegisterScriptDescriptors();
			}
		}

		/// <summary>
		/// Provide a method for control to override the behavior of "RegisterScriptDescriptors"
		/// </summary>
		protected virtual void RegisterScriptDescriptors()
		{
			_helper.RegisterScriptDescriptors();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (!DisplayVisible && !this.IsDesignMode)
			{
				this.Style.Add(HtmlTextWriterStyle.Display, "none");
			}
			else
			{
				this.Style.Remove(HtmlTextWriterStyle.Display);
			}

			base.AddAttributesToRender(writer);
		}

		#endregion end of ** methods

		#region ** protected virtual and protected methods (wijmo extenders specific)

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected virtual void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(null);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget to json hidden input.
		/// </summary>
		/// <param name="callback">callback function name</param>
		protected void RegisterOnSubmitStatement(string callback) 
		{
			_helper.RegisterOnSubmitStatement(callback);
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return _helper.GetScriptDescriptors();
		}

		/// <summary>
		/// When overridden in a derived class, registers an additional script libraries for the extender control.
		/// </summary>
		/// <returns>
		/// An object that implements the <see cref="T:System.Collections.IEnumerable"/> interface and that contains ECMAScript (JavaScript) files that have been registered as embedded resources.
		/// </returns>
		public virtual IEnumerable<ScriptReference> GetScriptReferences()
		{
			return WijmoResourceManager.GetScriptReferences(this);
		}

		/// <summary>
		/// Resolves the embedded resource URL.
		/// </summary>
		/// <param name="resourceName">Name of the resource.</param>
		/// <returns></returns>
		protected virtual string ResolveEmbeddedResourceUrl(string resourceName)
		{
			return C1TargetControlHelper.ResolveEmbeddedResourceUrl(resourceName, Page, !this.UseCDN && this.EnableCombinedJavaScripts);
		}

		/// <summary>
		/// Show about message box.
		/// </summary>
		protected void ShowAbout()
		{
			_helper.ShowAbout();
		}
		
		/// <summary>
		/// Container of properties.
		/// </summary>
		protected virtual IDictionary PropertiesContainer
		{
			get { return ViewState; }
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		public V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.PropertiesContainer[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.PropertiesContainer[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public void SetPropertyValue<V>(string propertyName, V value)
		{
			this.PropertiesContainer[propertyName] = value;
			OnPropertyValueChanged(propertyName, value);
		}
		
		/// <summary>
		/// Raises the OnPropertyValueChanged event when property value is changed.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property</param>
		/// <param name="value">The value.</param>
		protected virtual void OnPropertyValueChanged<V>(string propertyName, V value)
		{
		}

		/// <summary>
		/// Get the culture setting of this control. 
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="culture"></param>
		/// <returns></returns>
		protected virtual CultureInfo GetCulture(CultureInfo culture) 
		{
			if (culture != null && !string.IsNullOrEmpty(culture.Name)) 
			{
				return culture;
			}
#if GRAPECITY
			return CultureInfo.GetCultureInfo("ja-JP");
#else
			return CultureInfo.CurrentCulture;
#endif
		}
		#endregion

		#region ** private implementation
		private bool checkWijmoHttpHandlerRegisted() 
        {
			if (HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] == null)
			{
				HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] = WebConfigWorker.WijmoHttpHandlerExists(this.Context, C1TargetControlBase.WIJMOCONTROLSRESOURCEHANDLER, typeof(WijmoHttpHandler), this.Page.Request.ApplicationPath);
			}
			return (bool)HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"];
        }
		/// <summary>
		/// Control's widget name.
		/// </summary>
		protected virtual string WidgetName
		{
			get { return this.GetType().Name.ToLower(CultureInfo.InvariantCulture); }
		}
		string IC1TargetControl.WidgetName
		{
			get { return WidgetName; }
		}

		IDictionary IC1TargetControl.ViewState
		{
			get { return ViewState; }
		}


		#endregion ** private implementation

		#region ** register css stylesheets		
		
		/// <summary>
		/// Register design time stylesheet
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		/// <param name="cssResourceName">resource name of stylesheet file</param>
		public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		{
			_helper.RegisterDesignTimeStyleSheet(writer, cssResourceName);
		}

		#endregion end of ** register css stylesheets
		
		/// <summary>
		/// JSON serializable helper.
		/// </summary>
		protected JsonSerializableHelper JsonSerializableHelper
		{
			get { return _helper.JsonSerializableHelper; }
		}

		#region ** IJsonRestore interface implementations
		void IJsonRestore.RestoreStateFromJson(object state)
		{
			this.RestoreStateFromJson(state);
		}
		
		/// <summary>
		/// Restore control's state from json
		/// </summary>
		/// <param name="state"></param>
		protected virtual void RestoreStateFromJson(object state)
		{
			JsonRestoreHelper.RestoreStateFromJson(this, state);
		}
		#endregion end of ** IJsonRestore interface implementations.
	}
}

namespace C1.Web.Wijmo.Controls.Base
{
	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	[WidgetDependenciesAttribute(typeof(WijmoBase), "framework.js", "c1json2.js")]
	public abstract partial class C1TargetDataBoundControlBase : DataBoundControl, IC1TargetControl
	{

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.C1TargetDataBoundControlBase" /> class that represents a Span HTML tag.
		/// </summary>
		protected C1TargetDataBoundControlBase()
		{
			this.InitializeControl();
		}

		private void InitializeControl()
		{
			_helper =  new C1TargetControlHelper<C1TargetDataBoundControlBase>(this);
		}

		#endregion end of ** constructors

		#region ** fields

		C1TargetControlHelper<C1TargetDataBoundControlBase> _helper;
		/// <summary>
		/// A value that indicates whether the control is in design mode.
		/// </summary>
		protected bool IsDesignMode = HttpContext.Current == null;

		protected bool? _enableCombinedJS = null;

		#endregion

		#region ** properties

		#region ** Theme / UseCDN / CDNPath

		/// <summary>
		/// Name of the theme that will be used to style the widgets. 
		/// Available embedded themes include: aristo, midnight, and ui-lightness. 
		/// Note that only one theme can be used for the whole page at one time. 
		/// Please make sure that all widget extenders have the same Theme value. 
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string Theme
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
			}
			set
			{
				if (this.Theme != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual bool UseCDN
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "WijmoUseCDN", "false"));

			}
			set
			{
				if (this.UseCDN != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoUseCDN", value.ToString());
				}
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string CDNPath
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				if (this.CDNPath != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCDNPath", value);
				}
			}
		}

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		/// <remarks>
		/// Using this property, user can specify the CDN dependencies(such as jQuery, jQuery UI, jQuery mobile, bootstrap).
		/// If some dependencies are set in this property, wijmo controls will find dependencies path in this property.  If found, use the path to load dependency, 
		/// otherwise, use the default CDN path.
		/// For example, if user wants to specify the dependencies of jQuery and jQuery UI, 
		/// he can set the value like this: ["http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.1/jquery-ui.js"]
		/// </remarks>
		[C1Description("C1Base.CDNDependencyPaths")]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string[] CDNDependencyPaths
		{
			get 
			{
				string paths = WebConfigWorker.ReadAppSetting(this, "CDNDependencyPaths", "");
				if (string.IsNullOrEmpty(paths)) 
				{
					return new string[0];
				}
				return paths.Split('|');
			}
			set 
			{
				WebConfigWorker.WriteAppSetting(this, "CDNDependencyPaths", string.Join("|", value));
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCssAdapter", value);
				}
			}
		}

		#endregion ** Theme / UseCDN / CDNPath

		internal virtual bool IsWijMobileControl
		{
			get
			{
				return true;
			}
		}

		internal virtual bool IsWijWebControl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// A value that indicates mode of the control, whether it is a mobile or web control.
		/// Note that only one value can be used for the whole website or project. 
		/// </summary>
		[C1Description("C1Base.WijmoControlMode")]
		[C1Category("Category.Behavior")]
		[DefaultValue(WijmoControlMode.Web)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual WijmoControlMode WijmoControlMode
		{
			get
			{
				return (WijmoControlMode)Enum.Parse(typeof(WijmoControlMode), WebConfigWorker.ReadAppSetting(this, "WijmoMode", "Web"));
			}
			set
			{
				if (this.WijmoControlMode != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoMode", value.ToString());
				}
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the control, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		[TypeConverter(typeof(WijmoThemeSwatchConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string ThemeSwatch
		{
			get
			{
				return GetPropertyValue<string>("ThemeSwatch", "");
			}
			set
			{
				this.SetPropertyValue<string>("ThemeSwatch", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[C1Description("C1Base.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
        /// Enable JavaScripts files combined into one file.
		/// In order to combined enabled, you must register the WijmoHttpHandler in web.config.
        /// </summary>
        [C1Description("C1Base.EnableCombinedJavaScripts")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool EnableCombinedJavaScripts
        {
            get
            {
				if(_enableCombinedJS == null)
				{
                    if (HttpContext.Current == null)//design time
                    {
                        _enableCombinedJS = bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", "false"));
                    }
                    else
                    {
                        string result = WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", null);
                        bool registedHandler = checkWijmoHttpHandlerRegisted();

                        if (string.IsNullOrEmpty(result))
                        {
                            //if user not set EnableCombinedJS, then check whether registered httphandler.
                            _enableCombinedJS = registedHandler;
                        }
                        else
                        {
                            _enableCombinedJS = bool.Parse(result);
                        }
                    }
				}

				return _enableCombinedJS.Value;
            }
            set
            {
				_enableCombinedJS = value;
                WebConfigWorker.WriteAppSetting(this, "EnableCombinedJavaScripts", value.ToString());
            }
        }

		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[C1Description("C1Base.EnableConditionalDependencies")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool EnableConditionalDependencies
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableConditionalDependencies", "false"));
			}
			set
			{
				if (this.EnableConditionalDependencies != value)
				{
					WebConfigWorker.WriteAppSetting(this, "EnableConditionalDependencies", value.ToString());
				}
			}
		}

		/// <summary>
		/// IsEnabled status
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[WidgetOption]
		public virtual bool DisabledState
		{
			get
			{
				return !this.IsParentEnabled();
			}
			set
			{
			}
		}
		
		/// <summary>
		/// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
		/// </summary>
		/// <remarks>
		/// Note that a server control is created and invisible if DisplayVisible is set to false.
		/// </remarks>
		[C1Description("C1Controls.DisplayVisible")]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue(true)]
		[WidgetOption]
		public bool DisplayVisible
		{
			get
			{
				return this.GetPropertyValue("DisplayVisible", true);
			}
			set
			{
				this.SetPropertyValue("DisplayVisible", value);
			}
		}

		#region ** hidden properties
		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor 
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor 
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.BorderStyle BorderStyle 
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.Unit BorderWidth 
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.FontInfo Font 
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor 
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Boolean Visible 
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}


		#endregion end of ** hidden properties.

		#endregion end of ** properties

		#region ** methods
		
		/// <summary>
		/// Ensure control's enabled state, check to see if any of control's parents are disabled.
		/// </summary>
		protected virtual void EnsureEnabledState()
		{
			if (!this.DesignMode)
			{
				_helper.EnsureEnabledState();
			}
		}

		protected override void OnInit(EventArgs e)
		{
            if (!this.DesignMode && 
                this.EnableCombinedJavaScripts &&
                !this.UseCDN && 
                !this.checkWijmoHttpHandlerRegisted())
            {
                throw new Exception("To Combined the JavaScripts, the Wijmo require WijmoHttpHandler in the web.config.");
            }
			base.OnInit(e);
			if (!this.DesignMode) 
			{
				WijmoResourceManager.RegisterResourceControl(this);
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (!base.DesignMode)
			{
				_helper.RegisterRunTimeStylesheets();
				base.OnPreRender(e);

				if (this is IPostBackDataHandler)
				{
					this.Page.RegisterRequiresPostBack(this);
					this.RegisterOnSubmitStatement();
				}

				_helper.RegisterIncludes();
			}
			else
			{
				base.OnPreRender(e);
			}

			this.EnsureEnabledState();
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.IsWijMobileControl && this.WijmoControlMode == WijmoControlMode.Mobile)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.MobileNotImp"));
			}
			else if (!this.IsWijWebControl && this.WijmoControlMode == WijmoControlMode.Web)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.WebNotImp"));
			}
			_helper.RegisterDesignTimeStyleSheets(writer);
			base.Render(writer);
			if (!this.IsDesignMode)
			{
				_helper.RenderJsonDataField(writer);
				this.RegisterScriptDescriptors();
			}
		}

		/// <summary>
		/// Provide a method for control to override the behavior of "RegisterScriptDescriptors"
		/// </summary>
		protected virtual void RegisterScriptDescriptors()
		{
			_helper.RegisterScriptDescriptors();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (!DisplayVisible && !this.IsDesignMode)
			{
				this.Style.Add(HtmlTextWriterStyle.Display, "none");
			}
			else
			{
				this.Style.Remove(HtmlTextWriterStyle.Display);
			}

			base.AddAttributesToRender(writer);
		}

		#endregion end of ** methods

		#region ** protected virtual and protected methods (wijmo extenders specific)

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected virtual void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(null);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget to json hidden input.
		/// </summary>
		/// <param name="callback">callback function name</param>
		protected void RegisterOnSubmitStatement(string callback) 
		{
			_helper.RegisterOnSubmitStatement(callback);
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return _helper.GetScriptDescriptors();
		}

		/// <summary>
		/// When overridden in a derived class, registers an additional script libraries for the extender control.
		/// </summary>
		/// <returns>
		/// An object that implements the <see cref="T:System.Collections.IEnumerable"/> interface and that contains ECMAScript (JavaScript) files that have been registered as embedded resources.
		/// </returns>
		public virtual IEnumerable<ScriptReference> GetScriptReferences()
		{
			return WijmoResourceManager.GetScriptReferences(this);
		}

		/// <summary>
		/// Resolves the embedded resource URL.
		/// </summary>
		/// <param name="resourceName">Name of the resource.</param>
		/// <returns></returns>
		protected virtual string ResolveEmbeddedResourceUrl(string resourceName)
		{
			return C1TargetControlHelper.ResolveEmbeddedResourceUrl(resourceName, Page, !this.UseCDN && this.EnableCombinedJavaScripts);
		}

		/// <summary>
		/// Show about message box.
		/// </summary>
		protected void ShowAbout()
		{
			_helper.ShowAbout();
		}
		
		/// <summary>
		/// Container of properties.
		/// </summary>
		protected virtual IDictionary PropertiesContainer
		{
			get { return ViewState; }
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		public V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.PropertiesContainer[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.PropertiesContainer[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public void SetPropertyValue<V>(string propertyName, V value)
		{
			this.PropertiesContainer[propertyName] = value;
			OnPropertyValueChanged(propertyName, value);
		}
		
		/// <summary>
		/// Raises the OnPropertyValueChanged event when property value is changed.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property</param>
		/// <param name="value">The value.</param>
		protected virtual void OnPropertyValueChanged<V>(string propertyName, V value)
		{
		}

		/// <summary>
		/// Get the culture setting of this control. 
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="culture"></param>
		/// <returns></returns>
		protected virtual CultureInfo GetCulture(CultureInfo culture) 
		{
			if (culture != null && !string.IsNullOrEmpty(culture.Name)) 
			{
				return culture;
			}
#if GRAPECITY
			return CultureInfo.GetCultureInfo("ja-JP");
#else
			return CultureInfo.CurrentCulture;
#endif
		}
		#endregion

		#region ** private implementation
		private bool checkWijmoHttpHandlerRegisted() 
        {
			if (HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] == null)
			{
				HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] = WebConfigWorker.WijmoHttpHandlerExists(this.Context, C1TargetControlBase.WIJMOCONTROLSRESOURCEHANDLER, typeof(WijmoHttpHandler), this.Page.Request.ApplicationPath);
			}
			return (bool)HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"];
        }
		/// <summary>
		/// Control's widget name.
		/// </summary>
		protected virtual string WidgetName
		{
			get { return this.GetType().Name.ToLower(CultureInfo.InvariantCulture); }
		}
		string IC1TargetControl.WidgetName
		{
			get { return WidgetName; }
		}

		IDictionary IC1TargetControl.ViewState
		{
			get { return ViewState; }
		}


		#endregion ** private implementation

		#region ** register css stylesheets		
		
		/// <summary>
		/// Register design time stylesheet
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		/// <param name="cssResourceName">resource name of stylesheet file</param>
		public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		{
			_helper.RegisterDesignTimeStyleSheet(writer, cssResourceName);
		}

		#endregion end of ** register css stylesheets
		
		/// <summary>
		/// JSON serializable helper.
		/// </summary>
		protected JsonSerializableHelper JsonSerializableHelper
		{
			get { return _helper.JsonSerializableHelper; }
		}

		#region ** IJsonRestore interface implementations
		void IJsonRestore.RestoreStateFromJson(object state)
		{
			this.RestoreStateFromJson(state);
		}
		
		/// <summary>
		/// Restore control's state from json
		/// </summary>
		/// <param name="state"></param>
		protected virtual void RestoreStateFromJson(object state)
		{
			JsonRestoreHelper.RestoreStateFromJson(this, state);
		}
		#endregion end of ** IJsonRestore interface implementations.
	}
}

namespace C1.Web.Wijmo.Controls.Base
{
	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	[WidgetDependenciesAttribute(typeof(WijmoBase), "framework.js", "c1json2.js")]
	public abstract partial class C1TargetHierarchicalDataBoundControlBase : HierarchicalDataBoundControl, IC1TargetControl
	{

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.C1TargetHierarchicalDataBoundControlBase" /> class that represents a Span HTML tag.
		/// </summary>
		protected C1TargetHierarchicalDataBoundControlBase()
		{
			this.InitializeControl();
		}

		private void InitializeControl()
		{
			_helper =  new C1TargetControlHelper<C1TargetHierarchicalDataBoundControlBase>(this);
		}

		#endregion end of ** constructors

		#region ** fields

		C1TargetControlHelper<C1TargetHierarchicalDataBoundControlBase> _helper;
		/// <summary>
		/// A value that indicates whether the control is in design mode.
		/// </summary>
		protected bool IsDesignMode = HttpContext.Current == null;

		protected bool? _enableCombinedJS = null;

		#endregion

		#region ** properties

		#region ** Theme / UseCDN / CDNPath

		/// <summary>
		/// Name of the theme that will be used to style the widgets. 
		/// Available embedded themes include: aristo, midnight, and ui-lightness. 
		/// Note that only one theme can be used for the whole page at one time. 
		/// Please make sure that all widget extenders have the same Theme value. 
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string Theme
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
			}
			set
			{
				if (this.Theme != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual bool UseCDN
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "WijmoUseCDN", "false"));

			}
			set
			{
				if (this.UseCDN != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoUseCDN", value.ToString());
				}
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string CDNPath
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				if (this.CDNPath != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCDNPath", value);
				}
			}
		}

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		/// <remarks>
		/// Using this property, user can specify the CDN dependencies(such as jQuery, jQuery UI, jQuery mobile, bootstrap).
		/// If some dependencies are set in this property, wijmo controls will find dependencies path in this property.  If found, use the path to load dependency, 
		/// otherwise, use the default CDN path.
		/// For example, if user wants to specify the dependencies of jQuery and jQuery UI, 
		/// he can set the value like this: ["http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.1/jquery-ui.js"]
		/// </remarks>
		[C1Description("C1Base.CDNDependencyPaths")]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string[] CDNDependencyPaths
		{
			get 
			{
				string paths = WebConfigWorker.ReadAppSetting(this, "CDNDependencyPaths", "");
				if (string.IsNullOrEmpty(paths)) 
				{
					return new string[0];
				}
				return paths.Split('|');
			}
			set 
			{
				WebConfigWorker.WriteAppSetting(this, "CDNDependencyPaths", string.Join("|", value));
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCssAdapter", value);
				}
			}
		}

		#endregion ** Theme / UseCDN / CDNPath

		internal virtual bool IsWijMobileControl
		{
			get
			{
				return true;
			}
		}

		internal virtual bool IsWijWebControl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// A value that indicates mode of the control, whether it is a mobile or web control.
		/// Note that only one value can be used for the whole website or project. 
		/// </summary>
		[C1Description("C1Base.WijmoControlMode")]
		[C1Category("Category.Behavior")]
		[DefaultValue(WijmoControlMode.Web)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual WijmoControlMode WijmoControlMode
		{
			get
			{
				return (WijmoControlMode)Enum.Parse(typeof(WijmoControlMode), WebConfigWorker.ReadAppSetting(this, "WijmoMode", "Web"));
			}
			set
			{
				if (this.WijmoControlMode != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoMode", value.ToString());
				}
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the control, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		[TypeConverter(typeof(WijmoThemeSwatchConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string ThemeSwatch
		{
			get
			{
				return GetPropertyValue<string>("ThemeSwatch", "");
			}
			set
			{
				this.SetPropertyValue<string>("ThemeSwatch", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[C1Description("C1Base.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
        /// Enable JavaScripts files combined into one file.
		/// In order to combined enabled, you must register the WijmoHttpHandler in web.config.
        /// </summary>
        [C1Description("C1Base.EnableCombinedJavaScripts")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool EnableCombinedJavaScripts
        {
            get
            {
				if(_enableCombinedJS == null)
				{
                    if (HttpContext.Current == null)//design time
                    {
                        _enableCombinedJS = bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", "false"));
                    }
                    else
                    {
                        string result = WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", null);
                        bool registedHandler = checkWijmoHttpHandlerRegisted();

                        if (string.IsNullOrEmpty(result))
                        {
                            //if user not set EnableCombinedJS, then check whether registered httphandler.
                            _enableCombinedJS = registedHandler;
                        }
                        else
                        {
                            _enableCombinedJS = bool.Parse(result);
                        }
                    }
				}

				return _enableCombinedJS.Value;
            }
            set
            {
				_enableCombinedJS = value;
                WebConfigWorker.WriteAppSetting(this, "EnableCombinedJavaScripts", value.ToString());
            }
        }

		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[C1Description("C1Base.EnableConditionalDependencies")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool EnableConditionalDependencies
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableConditionalDependencies", "false"));
			}
			set
			{
				if (this.EnableConditionalDependencies != value)
				{
					WebConfigWorker.WriteAppSetting(this, "EnableConditionalDependencies", value.ToString());
				}
			}
		}

		/// <summary>
		/// IsEnabled status
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[WidgetOption]
		public virtual bool DisabledState
		{
			get
			{
				return !this.IsParentEnabled();
			}
			set
			{
			}
		}
		
		/// <summary>
		/// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
		/// </summary>
		/// <remarks>
		/// Note that a server control is created and invisible if DisplayVisible is set to false.
		/// </remarks>
		[C1Description("C1Controls.DisplayVisible")]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue(true)]
		[WidgetOption]
		public bool DisplayVisible
		{
			get
			{
				return this.GetPropertyValue("DisplayVisible", true);
			}
			set
			{
				this.SetPropertyValue("DisplayVisible", value);
			}
		}

		#region ** hidden properties
		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor 
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor 
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.BorderStyle BorderStyle 
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.Unit BorderWidth 
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.FontInfo Font 
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor 
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Boolean Visible 
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}


		#endregion end of ** hidden properties.

		#endregion end of ** properties

		#region ** methods
		
		/// <summary>
		/// Ensure control's enabled state, check to see if any of control's parents are disabled.
		/// </summary>
		protected virtual void EnsureEnabledState()
		{
			if (!this.DesignMode)
			{
				_helper.EnsureEnabledState();
			}
		}

		protected override void OnInit(EventArgs e)
		{
            if (!this.DesignMode && 
                this.EnableCombinedJavaScripts &&
                !this.UseCDN && 
                !this.checkWijmoHttpHandlerRegisted())
            {
                throw new Exception("To Combined the JavaScripts, the Wijmo require WijmoHttpHandler in the web.config.");
            }
			base.OnInit(e);
			if (!this.DesignMode) 
			{
				WijmoResourceManager.RegisterResourceControl(this);
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (!base.DesignMode)
			{
				_helper.RegisterRunTimeStylesheets();
				base.OnPreRender(e);

				if (this is IPostBackDataHandler)
				{
					this.Page.RegisterRequiresPostBack(this);
					this.RegisterOnSubmitStatement();
				}

				_helper.RegisterIncludes();
			}
			else
			{
				base.OnPreRender(e);
			}

			this.EnsureEnabledState();
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.IsWijMobileControl && this.WijmoControlMode == WijmoControlMode.Mobile)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.MobileNotImp"));
			}
			else if (!this.IsWijWebControl && this.WijmoControlMode == WijmoControlMode.Web)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.WebNotImp"));
			}
			_helper.RegisterDesignTimeStyleSheets(writer);
			base.Render(writer);
			if (!this.IsDesignMode)
			{
				_helper.RenderJsonDataField(writer);
				this.RegisterScriptDescriptors();
			}
		}

		/// <summary>
		/// Provide a method for control to override the behavior of "RegisterScriptDescriptors"
		/// </summary>
		protected virtual void RegisterScriptDescriptors()
		{
			_helper.RegisterScriptDescriptors();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (!DisplayVisible && !this.IsDesignMode)
			{
				this.Style.Add(HtmlTextWriterStyle.Display, "none");
			}
			else
			{
				this.Style.Remove(HtmlTextWriterStyle.Display);
			}

			base.AddAttributesToRender(writer);
		}

		#endregion end of ** methods

		#region ** protected virtual and protected methods (wijmo extenders specific)

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected virtual void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(null);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget to json hidden input.
		/// </summary>
		/// <param name="callback">callback function name</param>
		protected void RegisterOnSubmitStatement(string callback) 
		{
			_helper.RegisterOnSubmitStatement(callback);
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return _helper.GetScriptDescriptors();
		}

		/// <summary>
		/// When overridden in a derived class, registers an additional script libraries for the extender control.
		/// </summary>
		/// <returns>
		/// An object that implements the <see cref="T:System.Collections.IEnumerable"/> interface and that contains ECMAScript (JavaScript) files that have been registered as embedded resources.
		/// </returns>
		public virtual IEnumerable<ScriptReference> GetScriptReferences()
		{
			return WijmoResourceManager.GetScriptReferences(this);
		}

		/// <summary>
		/// Resolves the embedded resource URL.
		/// </summary>
		/// <param name="resourceName">Name of the resource.</param>
		/// <returns></returns>
		protected virtual string ResolveEmbeddedResourceUrl(string resourceName)
		{
			return C1TargetControlHelper.ResolveEmbeddedResourceUrl(resourceName, Page, !this.UseCDN && this.EnableCombinedJavaScripts);
		}

		/// <summary>
		/// Show about message box.
		/// </summary>
		protected void ShowAbout()
		{
			_helper.ShowAbout();
		}
		
		/// <summary>
		/// Container of properties.
		/// </summary>
		protected virtual IDictionary PropertiesContainer
		{
			get { return ViewState; }
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		public V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.PropertiesContainer[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.PropertiesContainer[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public void SetPropertyValue<V>(string propertyName, V value)
		{
			this.PropertiesContainer[propertyName] = value;
			OnPropertyValueChanged(propertyName, value);
		}
		
		/// <summary>
		/// Raises the OnPropertyValueChanged event when property value is changed.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property</param>
		/// <param name="value">The value.</param>
		protected virtual void OnPropertyValueChanged<V>(string propertyName, V value)
		{
		}

		/// <summary>
		/// Get the culture setting of this control. 
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="culture"></param>
		/// <returns></returns>
		protected virtual CultureInfo GetCulture(CultureInfo culture) 
		{
			if (culture != null && !string.IsNullOrEmpty(culture.Name)) 
			{
				return culture;
			}
#if GRAPECITY
			return CultureInfo.GetCultureInfo("ja-JP");
#else
			return CultureInfo.CurrentCulture;
#endif
		}
		#endregion

		#region ** private implementation
		private bool checkWijmoHttpHandlerRegisted() 
        {
			if (HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] == null)
			{
				HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] = WebConfigWorker.WijmoHttpHandlerExists(this.Context, C1TargetControlBase.WIJMOCONTROLSRESOURCEHANDLER, typeof(WijmoHttpHandler), this.Page.Request.ApplicationPath);
			}
			return (bool)HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"];
        }
		/// <summary>
		/// Control's widget name.
		/// </summary>
		protected virtual string WidgetName
		{
			get { return this.GetType().Name.ToLower(CultureInfo.InvariantCulture); }
		}
		string IC1TargetControl.WidgetName
		{
			get { return WidgetName; }
		}

		IDictionary IC1TargetControl.ViewState
		{
			get { return ViewState; }
		}


		#endregion ** private implementation

		#region ** register css stylesheets		
		
		/// <summary>
		/// Register design time stylesheet
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		/// <param name="cssResourceName">resource name of stylesheet file</param>
		public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		{
			_helper.RegisterDesignTimeStyleSheet(writer, cssResourceName);
		}

		#endregion end of ** register css stylesheets
		
		/// <summary>
		/// JSON serializable helper.
		/// </summary>
		protected JsonSerializableHelper JsonSerializableHelper
		{
			get { return _helper.JsonSerializableHelper; }
		}

		#region ** IJsonRestore interface implementations
		void IJsonRestore.RestoreStateFromJson(object state)
		{
			this.RestoreStateFromJson(state);
		}
		
		/// <summary>
		/// Restore control's state from json
		/// </summary>
		/// <param name="state"></param>
		protected virtual void RestoreStateFromJson(object state)
		{
			JsonRestoreHelper.RestoreStateFromJson(this, state);
		}
		#endregion end of ** IJsonRestore interface implementations.
	}
}

namespace C1.Web.Wijmo.Controls.Base
{
	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	[WidgetDependenciesAttribute(typeof(WijmoBase), "framework.js", "c1json2.js")]
	public abstract partial class C1TargetCompositeControlBase : CompositeControl, IC1TargetControl
	{

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.C1TargetCompositeControlBase" /> class that represents a Span HTML tag.
		/// </summary>
		protected C1TargetCompositeControlBase()
		{
			this.InitializeControl();
		}

		private void InitializeControl()
		{
			_helper =  new C1TargetControlHelper<C1TargetCompositeControlBase>(this);
		}

		#endregion end of ** constructors

		#region ** fields

		C1TargetControlHelper<C1TargetCompositeControlBase> _helper;
		/// <summary>
		/// A value that indicates whether the control is in design mode.
		/// </summary>
		protected bool IsDesignMode = HttpContext.Current == null;

		protected bool? _enableCombinedJS = null;

		#endregion

		#region ** properties

		#region ** Theme / UseCDN / CDNPath

		/// <summary>
		/// Name of the theme that will be used to style the widgets. 
		/// Available embedded themes include: aristo, midnight, and ui-lightness. 
		/// Note that only one theme can be used for the whole page at one time. 
		/// Please make sure that all widget extenders have the same Theme value. 
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string Theme
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
			}
			set
			{
				if (this.Theme != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual bool UseCDN
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "WijmoUseCDN", "false"));

			}
			set
			{
				if (this.UseCDN != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoUseCDN", value.ToString());
				}
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string CDNPath
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				if (this.CDNPath != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCDNPath", value);
				}
			}
		}

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		/// <remarks>
		/// Using this property, user can specify the CDN dependencies(such as jQuery, jQuery UI, jQuery mobile, bootstrap).
		/// If some dependencies are set in this property, wijmo controls will find dependencies path in this property.  If found, use the path to load dependency, 
		/// otherwise, use the default CDN path.
		/// For example, if user wants to specify the dependencies of jQuery and jQuery UI, 
		/// he can set the value like this: ["http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.1/jquery-ui.js"]
		/// </remarks>
		[C1Description("C1Base.CDNDependencyPaths")]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string[] CDNDependencyPaths
		{
			get 
			{
				string paths = WebConfigWorker.ReadAppSetting(this, "CDNDependencyPaths", "");
				if (string.IsNullOrEmpty(paths)) 
				{
					return new string[0];
				}
				return paths.Split('|');
			}
			set 
			{
				WebConfigWorker.WriteAppSetting(this, "CDNDependencyPaths", string.Join("|", value));
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCssAdapter", value);
				}
			}
		}

		#endregion ** Theme / UseCDN / CDNPath

		internal virtual bool IsWijMobileControl
		{
			get
			{
				return true;
			}
		}

		internal virtual bool IsWijWebControl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// A value that indicates mode of the control, whether it is a mobile or web control.
		/// Note that only one value can be used for the whole website or project. 
		/// </summary>
		[C1Description("C1Base.WijmoControlMode")]
		[C1Category("Category.Behavior")]
		[DefaultValue(WijmoControlMode.Web)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual WijmoControlMode WijmoControlMode
		{
			get
			{
				return (WijmoControlMode)Enum.Parse(typeof(WijmoControlMode), WebConfigWorker.ReadAppSetting(this, "WijmoMode", "Web"));
			}
			set
			{
				if (this.WijmoControlMode != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoMode", value.ToString());
				}
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the control, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		[TypeConverter(typeof(WijmoThemeSwatchConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string ThemeSwatch
		{
			get
			{
				return GetPropertyValue<string>("ThemeSwatch", "");
			}
			set
			{
				this.SetPropertyValue<string>("ThemeSwatch", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[C1Description("C1Base.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
        /// Enable JavaScripts files combined into one file.
		/// In order to combined enabled, you must register the WijmoHttpHandler in web.config.
        /// </summary>
        [C1Description("C1Base.EnableCombinedJavaScripts")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool EnableCombinedJavaScripts
        {
            get
            {
				if(_enableCombinedJS == null)
				{
                    if (HttpContext.Current == null)//design time
                    {
                        _enableCombinedJS = bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", "false"));
                    }
                    else
                    {
                        string result = WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", null);
                        bool registedHandler = checkWijmoHttpHandlerRegisted();

                        if (string.IsNullOrEmpty(result))
                        {
                            //if user not set EnableCombinedJS, then check whether registered httphandler.
                            _enableCombinedJS = registedHandler;
                        }
                        else
                        {
                            _enableCombinedJS = bool.Parse(result);
                        }
                    }
				}

				return _enableCombinedJS.Value;
            }
            set
            {
				_enableCombinedJS = value;
                WebConfigWorker.WriteAppSetting(this, "EnableCombinedJavaScripts", value.ToString());
            }
        }

		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[C1Description("C1Base.EnableConditionalDependencies")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool EnableConditionalDependencies
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableConditionalDependencies", "false"));
			}
			set
			{
				if (this.EnableConditionalDependencies != value)
				{
					WebConfigWorker.WriteAppSetting(this, "EnableConditionalDependencies", value.ToString());
				}
			}
		}

		/// <summary>
		/// IsEnabled status
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[WidgetOption]
		public virtual bool DisabledState
		{
			get
			{
				return !this.IsParentEnabled();
			}
			set
			{
			}
		}
		
		/// <summary>
		/// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
		/// </summary>
		/// <remarks>
		/// Note that a server control is created and invisible if DisplayVisible is set to false.
		/// </remarks>
		[C1Description("C1Controls.DisplayVisible")]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue(true)]
		[WidgetOption]
		public bool DisplayVisible
		{
			get
			{
				return this.GetPropertyValue("DisplayVisible", true);
			}
			set
			{
				this.SetPropertyValue("DisplayVisible", value);
			}
		}

		#region ** hidden properties
		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor 
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor 
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.BorderStyle BorderStyle 
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.Unit BorderWidth 
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.FontInfo Font 
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor 
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Boolean Visible 
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}


		#endregion end of ** hidden properties.

		#endregion end of ** properties

		#region ** methods
		
		/// <summary>
		/// Ensure control's enabled state, check to see if any of control's parents are disabled.
		/// </summary>
		protected virtual void EnsureEnabledState()
		{
			if (!this.DesignMode)
			{
				_helper.EnsureEnabledState();
			}
		}

		protected override void OnInit(EventArgs e)
		{
            if (!this.DesignMode && 
                this.EnableCombinedJavaScripts &&
                !this.UseCDN && 
                !this.checkWijmoHttpHandlerRegisted())
            {
                throw new Exception("To Combined the JavaScripts, the Wijmo require WijmoHttpHandler in the web.config.");
            }
			base.OnInit(e);
			if (!this.DesignMode) 
			{
				WijmoResourceManager.RegisterResourceControl(this);
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (!base.DesignMode)
			{
				_helper.RegisterRunTimeStylesheets();
				base.OnPreRender(e);

				if (this is IPostBackDataHandler)
				{
					this.Page.RegisterRequiresPostBack(this);
					this.RegisterOnSubmitStatement();
				}

				_helper.RegisterIncludes();
			}
			else
			{
				base.OnPreRender(e);
			}

			this.EnsureEnabledState();
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.IsWijMobileControl && this.WijmoControlMode == WijmoControlMode.Mobile)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.MobileNotImp"));
			}
			else if (!this.IsWijWebControl && this.WijmoControlMode == WijmoControlMode.Web)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.WebNotImp"));
			}
			_helper.RegisterDesignTimeStyleSheets(writer);
			base.Render(writer);
			if (!this.IsDesignMode)
			{
				this.RegisterScriptDescriptors();
			}
		}

		/// <summary>
		/// Provide a method for control to override the behavior of "RegisterScriptDescriptors"
		/// </summary>
		protected virtual void RegisterScriptDescriptors()
		{
			_helper.RegisterScriptDescriptors();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (!DisplayVisible && !this.IsDesignMode)
			{
				this.Style.Add(HtmlTextWriterStyle.Display, "none");
			}
			else
			{
				this.Style.Remove(HtmlTextWriterStyle.Display);
			}

			base.AddAttributesToRender(writer);
		}

		#endregion end of ** methods

		#region ** protected virtual and protected methods (wijmo extenders specific)

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected virtual void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(null);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget to json hidden input.
		/// </summary>
		/// <param name="callback">callback function name</param>
		protected void RegisterOnSubmitStatement(string callback) 
		{
			_helper.RegisterOnSubmitStatement(callback);
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return _helper.GetScriptDescriptors();
		}

		/// <summary>
		/// When overridden in a derived class, registers an additional script libraries for the extender control.
		/// </summary>
		/// <returns>
		/// An object that implements the <see cref="T:System.Collections.IEnumerable"/> interface and that contains ECMAScript (JavaScript) files that have been registered as embedded resources.
		/// </returns>
		public virtual IEnumerable<ScriptReference> GetScriptReferences()
		{
			return WijmoResourceManager.GetScriptReferences(this);
		}

		/// <summary>
		/// Resolves the embedded resource URL.
		/// </summary>
		/// <param name="resourceName">Name of the resource.</param>
		/// <returns></returns>
		protected virtual string ResolveEmbeddedResourceUrl(string resourceName)
		{
			return C1TargetControlHelper.ResolveEmbeddedResourceUrl(resourceName, Page, !this.UseCDN && this.EnableCombinedJavaScripts);
		}

		/// <summary>
		/// Show about message box.
		/// </summary>
		protected void ShowAbout()
		{
			_helper.ShowAbout();
		}
		
		/// <summary>
		/// Container of properties.
		/// </summary>
		protected virtual IDictionary PropertiesContainer
		{
			get { return ViewState; }
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		public V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.PropertiesContainer[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.PropertiesContainer[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public void SetPropertyValue<V>(string propertyName, V value)
		{
			this.PropertiesContainer[propertyName] = value;
			OnPropertyValueChanged(propertyName, value);
		}
		
		/// <summary>
		/// Raises the OnPropertyValueChanged event when property value is changed.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property</param>
		/// <param name="value">The value.</param>
		protected virtual void OnPropertyValueChanged<V>(string propertyName, V value)
		{
		}

		/// <summary>
		/// Get the culture setting of this control. 
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="culture"></param>
		/// <returns></returns>
		protected virtual CultureInfo GetCulture(CultureInfo culture) 
		{
			if (culture != null && !string.IsNullOrEmpty(culture.Name)) 
			{
				return culture;
			}
#if GRAPECITY
			return CultureInfo.GetCultureInfo("ja-JP");
#else
			return CultureInfo.CurrentCulture;
#endif
		}
		#endregion

		#region ** private implementation
		private bool checkWijmoHttpHandlerRegisted() 
        {
			if (HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] == null)
			{
				HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] = WebConfigWorker.WijmoHttpHandlerExists(this.Context, C1TargetControlBase.WIJMOCONTROLSRESOURCEHANDLER, typeof(WijmoHttpHandler), this.Page.Request.ApplicationPath);
			}
			return (bool)HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"];
        }
		/// <summary>
		/// Control's widget name.
		/// </summary>
		protected virtual string WidgetName
		{
			get { return this.GetType().Name.ToLower(CultureInfo.InvariantCulture); }
		}
		string IC1TargetControl.WidgetName
		{
			get { return WidgetName; }
		}

		IDictionary IC1TargetControl.ViewState
		{
			get { return ViewState; }
		}


		#endregion ** private implementation

		#region ** register css stylesheets		
		
		/// <summary>
		/// Register design time stylesheet
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		/// <param name="cssResourceName">resource name of stylesheet file</param>
		public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		{
			_helper.RegisterDesignTimeStyleSheet(writer, cssResourceName);
		}

		#endregion end of ** register css stylesheets
		
		/// <summary>
		/// JSON serializable helper.
		/// </summary>
		protected JsonSerializableHelper JsonSerializableHelper
		{
			get { return _helper.JsonSerializableHelper; }
		}

		#region ** IJsonRestore interface implementations
		void IJsonRestore.RestoreStateFromJson(object state)
		{
			this.RestoreStateFromJson(state);
		}
		
		/// <summary>
		/// Restore control's state from json
		/// </summary>
		/// <param name="state"></param>
		protected virtual void RestoreStateFromJson(object state)
		{
			JsonRestoreHelper.RestoreStateFromJson(this, state);
		}
		#endregion end of ** IJsonRestore interface implementations.
	}
}

namespace C1.Web.Wijmo.Controls.Base
{
	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	[WidgetDependenciesAttribute(typeof(WijmoBase), "framework.js", "c1json2.js")]
	public abstract partial class C1TargetCompositeDataBoundControlBase : CompositeDataBoundControl, IC1TargetControl
	{

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.C1TargetCompositeDataBoundControlBase" /> class that represents a Span HTML tag.
		/// </summary>
		protected C1TargetCompositeDataBoundControlBase()
		{
			this.InitializeControl();
		}

		private void InitializeControl()
		{
			_helper =  new C1TargetControlHelper<C1TargetCompositeDataBoundControlBase>(this);
		}

		#endregion end of ** constructors

		#region ** fields

		C1TargetControlHelper<C1TargetCompositeDataBoundControlBase> _helper;
		/// <summary>
		/// A value that indicates whether the control is in design mode.
		/// </summary>
		protected bool IsDesignMode = HttpContext.Current == null;

		protected bool? _enableCombinedJS = null;

		#endregion

		#region ** properties

		#region ** Theme / UseCDN / CDNPath

		/// <summary>
		/// Name of the theme that will be used to style the widgets. 
		/// Available embedded themes include: aristo, midnight, and ui-lightness. 
		/// Note that only one theme can be used for the whole page at one time. 
		/// Please make sure that all widget extenders have the same Theme value. 
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string Theme
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
			}
			set
			{
				if (this.Theme != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual bool UseCDN
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "WijmoUseCDN", "false"));

			}
			set
			{
				if (this.UseCDN != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoUseCDN", value.ToString());
				}
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string CDNPath
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				if (this.CDNPath != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCDNPath", value);
				}
			}
		}

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		/// <remarks>
		/// Using this property, user can specify the CDN dependencies(such as jQuery, jQuery UI, jQuery mobile, bootstrap).
		/// If some dependencies are set in this property, wijmo controls will find dependencies path in this property.  If found, use the path to load dependency, 
		/// otherwise, use the default CDN path.
		/// For example, if user wants to specify the dependencies of jQuery and jQuery UI, 
		/// he can set the value like this: ["http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.1/jquery-ui.js"]
		/// </remarks>
		[C1Description("C1Base.CDNDependencyPaths")]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string[] CDNDependencyPaths
		{
			get 
			{
				string paths = WebConfigWorker.ReadAppSetting(this, "CDNDependencyPaths", "");
				if (string.IsNullOrEmpty(paths)) 
				{
					return new string[0];
				}
				return paths.Split('|');
			}
			set 
			{
				WebConfigWorker.WriteAppSetting(this, "CDNDependencyPaths", string.Join("|", value));
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCssAdapter", value);
				}
			}
		}

		#endregion ** Theme / UseCDN / CDNPath

		internal virtual bool IsWijMobileControl
		{
			get
			{
				return true;
			}
		}

		internal virtual bool IsWijWebControl
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// A value that indicates mode of the control, whether it is a mobile or web control.
		/// Note that only one value can be used for the whole website or project. 
		/// </summary>
		[C1Description("C1Base.WijmoControlMode")]
		[C1Category("Category.Behavior")]
		[DefaultValue(WijmoControlMode.Web)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual WijmoControlMode WijmoControlMode
		{
			get
			{
				return (WijmoControlMode)Enum.Parse(typeof(WijmoControlMode), WebConfigWorker.ReadAppSetting(this, "WijmoMode", "Web"));
			}
			set
			{
				if (this.WijmoControlMode != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoMode", value.ToString());
				}
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the control, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		[TypeConverter(typeof(WijmoThemeSwatchConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string ThemeSwatch
		{
			get
			{
				return GetPropertyValue<string>("ThemeSwatch", "");
			}
			set
			{
				this.SetPropertyValue<string>("ThemeSwatch", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[C1Description("C1Base.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
        /// Enable JavaScripts files combined into one file.
		/// In order to combined enabled, you must register the WijmoHttpHandler in web.config.
        /// </summary>
        [C1Description("C1Base.EnableCombinedJavaScripts")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool EnableCombinedJavaScripts
        {
            get
            {
				if(_enableCombinedJS == null)
				{
                    if (HttpContext.Current == null)//design time
                    {
                        _enableCombinedJS = bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", "false"));
                    }
                    else
                    {
                        string result = WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", null);
                        bool registedHandler = checkWijmoHttpHandlerRegisted();

                        if (string.IsNullOrEmpty(result))
                        {
                            //if user not set EnableCombinedJS, then check whether registered httphandler.
                            _enableCombinedJS = registedHandler;
                        }
                        else
                        {
                            _enableCombinedJS = bool.Parse(result);
                        }
                    }
				}

				return _enableCombinedJS.Value;
            }
            set
            {
				_enableCombinedJS = value;
                WebConfigWorker.WriteAppSetting(this, "EnableCombinedJavaScripts", value.ToString());
            }
        }

		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[C1Description("C1Base.EnableConditionalDependencies")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool EnableConditionalDependencies
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableConditionalDependencies", "false"));
			}
			set
			{
				if (this.EnableConditionalDependencies != value)
				{
					WebConfigWorker.WriteAppSetting(this, "EnableConditionalDependencies", value.ToString());
				}
			}
		}

		/// <summary>
		/// IsEnabled status
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[WidgetOption]
		public virtual bool DisabledState
		{
			get
			{
				return !this.IsParentEnabled();
			}
			set
			{
			}
		}
		
		/// <summary>
		/// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
		/// </summary>
		/// <remarks>
		/// Note that a server control is created and invisible if DisplayVisible is set to false.
		/// </remarks>
		[C1Description("C1Controls.DisplayVisible")]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue(true)]
		[WidgetOption]
		public bool DisplayVisible
		{
			get
			{
				return this.GetPropertyValue("DisplayVisible", true);
			}
			set
			{
				this.SetPropertyValue("DisplayVisible", value);
			}
		}

		#region ** hidden properties
		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor 
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor 
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.BorderStyle BorderStyle 
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.Unit BorderWidth 
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Web.UI.WebControls.FontInfo Font 
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor 
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Boolean Visible 
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}


		#endregion end of ** hidden properties.

		#endregion end of ** properties

		#region ** methods
		
		/// <summary>
		/// Ensure control's enabled state, check to see if any of control's parents are disabled.
		/// </summary>
		protected virtual void EnsureEnabledState()
		{
			if (!this.DesignMode)
			{
				_helper.EnsureEnabledState();
			}
		}

		protected override void OnInit(EventArgs e)
		{
            if (!this.DesignMode && 
                this.EnableCombinedJavaScripts &&
                !this.UseCDN && 
                !this.checkWijmoHttpHandlerRegisted())
            {
                throw new Exception("To Combined the JavaScripts, the Wijmo require WijmoHttpHandler in the web.config.");
            }
			base.OnInit(e);
			if (!this.DesignMode) 
			{
				WijmoResourceManager.RegisterResourceControl(this);
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			if (!base.DesignMode)
			{
				_helper.RegisterRunTimeStylesheets();
				base.OnPreRender(e);

				if (this is IPostBackDataHandler)
				{
					this.Page.RegisterRequiresPostBack(this);
					this.RegisterOnSubmitStatement();
				}

				_helper.RegisterIncludes();
			}
			else
			{
				base.OnPreRender(e);
			}

			this.EnsureEnabledState();
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.IsWijMobileControl && this.WijmoControlMode == WijmoControlMode.Mobile)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.MobileNotImp"));
			}
			else if (!this.IsWijWebControl && this.WijmoControlMode == WijmoControlMode.Web)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.WebNotImp"));
			}
			_helper.RegisterDesignTimeStyleSheets(writer);
			base.Render(writer);
			if (!this.IsDesignMode)
			{
				_helper.RenderJsonDataField(writer);
				this.RegisterScriptDescriptors();
			}
		}

		/// <summary>
		/// Provide a method for control to override the behavior of "RegisterScriptDescriptors"
		/// </summary>
		protected virtual void RegisterScriptDescriptors()
		{
			_helper.RegisterScriptDescriptors();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (!DisplayVisible && !this.IsDesignMode)
			{
				this.Style.Add(HtmlTextWriterStyle.Display, "none");
			}
			else
			{
				this.Style.Remove(HtmlTextWriterStyle.Display);
			}

			base.AddAttributesToRender(writer);
		}

		#endregion end of ** methods

		#region ** protected virtual and protected methods (wijmo extenders specific)

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected virtual void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement(null);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget to json hidden input.
		/// </summary>
		/// <param name="callback">callback function name</param>
		protected void RegisterOnSubmitStatement(string callback) 
		{
			_helper.RegisterOnSubmitStatement(callback);
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public virtual IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return _helper.GetScriptDescriptors();
		}

		/// <summary>
		/// When overridden in a derived class, registers an additional script libraries for the extender control.
		/// </summary>
		/// <returns>
		/// An object that implements the <see cref="T:System.Collections.IEnumerable"/> interface and that contains ECMAScript (JavaScript) files that have been registered as embedded resources.
		/// </returns>
		public virtual IEnumerable<ScriptReference> GetScriptReferences()
		{
			return WijmoResourceManager.GetScriptReferences(this);
		}

		/// <summary>
		/// Resolves the embedded resource URL.
		/// </summary>
		/// <param name="resourceName">Name of the resource.</param>
		/// <returns></returns>
		protected virtual string ResolveEmbeddedResourceUrl(string resourceName)
		{
			return C1TargetControlHelper.ResolveEmbeddedResourceUrl(resourceName, Page, !this.UseCDN && this.EnableCombinedJavaScripts);
		}

		/// <summary>
		/// Show about message box.
		/// </summary>
		protected void ShowAbout()
		{
			_helper.ShowAbout();
		}
		
		/// <summary>
		/// Container of properties.
		/// </summary>
		protected virtual IDictionary PropertiesContainer
		{
			get { return ViewState; }
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		public V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.PropertiesContainer[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.PropertiesContainer[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public void SetPropertyValue<V>(string propertyName, V value)
		{
			this.PropertiesContainer[propertyName] = value;
			OnPropertyValueChanged(propertyName, value);
		}
		
		/// <summary>
		/// Raises the OnPropertyValueChanged event when property value is changed.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property</param>
		/// <param name="value">The value.</param>
		protected virtual void OnPropertyValueChanged<V>(string propertyName, V value)
		{
		}

		/// <summary>
		/// Get the culture setting of this control. 
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="culture"></param>
		/// <returns></returns>
		protected virtual CultureInfo GetCulture(CultureInfo culture) 
		{
			if (culture != null && !string.IsNullOrEmpty(culture.Name)) 
			{
				return culture;
			}
#if GRAPECITY
			return CultureInfo.GetCultureInfo("ja-JP");
#else
			return CultureInfo.CurrentCulture;
#endif
		}
		#endregion

		#region ** private implementation
		private bool checkWijmoHttpHandlerRegisted() 
        {
			if (HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] == null)
			{
				HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"] = WebConfigWorker.WijmoHttpHandlerExists(this.Context, C1TargetControlBase.WIJMOCONTROLSRESOURCEHANDLER, typeof(WijmoHttpHandler), this.Page.Request.ApplicationPath);
			}
			return (bool)HttpContext.Current.Application["WijmoControlHttpHandlerRegistered"];
        }
		/// <summary>
		/// Control's widget name.
		/// </summary>
		protected virtual string WidgetName
		{
			get { return this.GetType().Name.ToLower(CultureInfo.InvariantCulture); }
		}
		string IC1TargetControl.WidgetName
		{
			get { return WidgetName; }
		}

		IDictionary IC1TargetControl.ViewState
		{
			get { return ViewState; }
		}


		#endregion ** private implementation

		#region ** register css stylesheets		
		
		/// <summary>
		/// Register design time stylesheet
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		/// <param name="cssResourceName">resource name of stylesheet file</param>
		public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		{
			_helper.RegisterDesignTimeStyleSheet(writer, cssResourceName);
		}

		#endregion end of ** register css stylesheets
		
		/// <summary>
		/// JSON serializable helper.
		/// </summary>
		protected JsonSerializableHelper JsonSerializableHelper
		{
			get { return _helper.JsonSerializableHelper; }
		}

		#region ** IJsonRestore interface implementations
		void IJsonRestore.RestoreStateFromJson(object state)
		{
			this.RestoreStateFromJson(state);
		}
		
		/// <summary>
		/// Restore control's state from json
		/// </summary>
		/// <param name="state"></param>
		protected virtual void RestoreStateFromJson(object state)
		{
			JsonRestoreHelper.RestoreStateFromJson(this, state);
		}
		#endregion end of ** IJsonRestore interface implementations.
	}
}
