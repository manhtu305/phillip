﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Diagnostics;
using System.Text;
using System.Web.UI.Design;
using System.Web;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Base.Collections;

namespace C1.Web.Wijmo.Controls.Base
{
	/// <summary>
	/// Serializes Control.
	/// </summary>
	/// <typeparam name="TControl">Control type.</typeparam>
	/// <typeparam name="TItem">Control Items's type.</typeparam>
	/// <typeparam name="TICollectionOwner">Items collection type.</typeparam>
	public class C1BaseSerializer<TControl, TItem, TICollectionOwner> : IC1Serializable
	{
		#region ** fields

		private System.Type _serObjectType;
		private BindingFlags _bindFlag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;
		private Hashtable _inheritedProps = CreateInheritedPropsList();
		private IComponentChangeService _componentChangeService = null;
#if ASP_NET45
		private string _assemblyName = "C1.Web.Wijmo.Controls.45";
		private string _namespaceURI = "urn:C1.Web.Wijmo.Controls.45";
		private string _namespaceAlternativeURI = "urn:C1.Web.Wijmo.Controls.4";
#elif ASP_NET4
		private string _assemblyName = "C1.Web.Wijmo.Controls.4";
		private string _namespaceURI = "urn:C1.Web.Wijmo.Controls.4";
		private string _namespaceAlternativeURI = "urn:C1.Web.Wijmo.Controls.3";
#elif ASP_NET35
		private string _assemblyName = "C1.Web.Wijmo.Controls.3";
		private string _namespaceURI = "urn:C1.Web.Wijmo.Controls.3";
		private string _namespaceAlternativeURI = "urn:C1.Web.Wijmo.Controls.2";
#else
		private string _assemblyName = "C1.Web.Wijmo.Controls.2";    
		private string _namespaceURI = "urn:C1.Web.Wijmo.Controls.2";
		private string _namespaceAlternativeURI = "urn:C1.Web.Wijmo.Controls.3";
#endif


		private string _defaultControlNamespace = "C1.Web.Wijmo.Controls";

		#endregion

		#region ** protected fields

		protected readonly Object SerializableObject;

		#endregion

		#region  ** constructors

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="serializableObject">Serializable Object.</param>
		public C1BaseSerializer(Object serializableObject)
		{
			this.SerializableObject = serializableObject;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="componentChangeService"></param>
		/// <param name="serializableObject"></param>
		public C1BaseSerializer(IComponentChangeService componentChangeService, Object serializableObject)
		{
			this.SerializableObject = serializableObject;
			this._componentChangeService = componentChangeService;
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Serializable object that will be serialized or deserialized.
		/// </summary>
		public Object Serializable
		{
			get
			{
				return this.SerializableObject;
			}
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Load object layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load object layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Saves the object layout properties to the XML file.
		/// </summary>
		/// <param name="filename">Path to the file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			if ((filename != null) && (filename.Length > 0))
			{
				FileStream fs = null;
				try
				{
					fs = new FileStream(filename, FileMode.Create);
					SaveLayout(fs);
				}
				finally
				{
					if (fs != null)
						fs.Close();
				}
			}
		}


		/// <summary>
		/// Saves object layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			if (this.SerializableObject != null)
			{
				XmlTextWriter writer = new XmlTextWriter(stream, null);
				writer.Formatting = Formatting.Indented;
				writer.WriteStartDocument();
				writer.WriteStartElement(this.SerializableObject.GetType().ToString(), this._namespaceURI);
				_serObjectType = this.SerializableObject.GetType();
				InnerSerialize(writer, this.SerializableObject);
				writer.WriteEndElement();
				writer.WriteEndDocument();
				writer.Flush();
			}
		}


		/// <summary>
		/// Loads object layout properties with specified types from the XML file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			XmlTextReader reader = null;
			try
			{
				reader = new XmlTextReader(filename);
				Deserialize(reader, this.SerializableObject, layoutTypes);
			}
			finally
			{
				if (reader != null)
					reader.Close();
			}
		}

		/// <summary>
		/// Loads the object layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			XmlTextReader reader = new XmlTextReader(stream);
			Deserialize(reader, this.SerializableObject, layoutTypes);
		}



		#endregion

		#region ** protected virtual methods

		/// <summary>
		/// Override this method if you want to add deserialized object 
		/// to array or collection using custom way.
		/// </summary>
		/// <example>
		/// Override example:
		/// <code>
		/// protected override bool OnAddItem(object collection, object item)
		/// {
		///     if (collection is C1MenuItemCollection)
		///     {
		///         ((C1MenuItemCollection)collection).Add((C1MenuItem)item);
		///         return true;
		///     }
		///     return false;
		/// }
		/// </code>
		/// </example>
		/// <remarks>This method preliminary used by control developers.</remarks>
		/// <param name="collection"></param>
		/// <param name="item"></param>
		/// <returns>Return true if object is added to array or collection.</returns>
		protected virtual bool OnAddItem(object collection, object item)
		{
			return false;
		}

		/// <summary>
		/// Override this method to implement custom clear for deserialized array.
		/// </summary>
		/// <example>
		/// Override example:
		/// <code>
		/// protected override bool OnClearItems(object collection)
		/// {
		///     if (collection is C1MenuItemCollection)
		///     {
		///         ((C1MenuItemCollection)collection).Clear();
		///         return true;
		///     }
		///     return false;
		/// }
		/// </code>
		/// </example>
		/// <remarks>This method preliminary used by control developers.</remarks>
		/// <param name="collection"></param>
		/// <returns>Return true if array or collection is cleared.</returns>
		protected virtual bool OnClearItems(object collection)
		{
			return false;
		}

		#endregion

		#region ** private implementation

		private static Hashtable CreateInheritedPropsList()
		{
			Hashtable ht = new Hashtable();
			ht.Add("BorderStyle", LayoutType.Appearance);
			ht.Add("BackColor", LayoutType.Appearance);
			ht.Add("BackImageUrl", LayoutType.Appearance);
			ht.Add("BorderColor", LayoutType.Appearance);
			ht.Add("BorderWidth", LayoutType.Appearance);
			ht.Add("Font", LayoutType.Appearance);
			ht.Add("ForeColor", LayoutType.Appearance);
			ht.Add("Height", LayoutType.Sizes);
			ht.Add("Width", LayoutType.Sizes);
			ht.Add("CssClass", LayoutType.Appearance);
			ht.Add("Wrap", LayoutType.Appearance);
			ht.Add("HorizontalAlign", LayoutType.Appearance);
			return ht;
		}

		private void Deserialize(XmlTextReader reader, object control, LayoutType deserializeTypes)
		{
			reader.WhitespaceHandling = WhitespaceHandling.None;
			reader.MoveToContent();
			CheckTypeMatch(reader, control);
			_serObjectType = control.GetType();
			try
			{
				RootControl = control;
				InnerDeserialize(reader, control, control.GetType(), deserializeTypes);
			}
			catch (Exception ex)
			{
				string errorText = "Deserialize error: " + ex.Message;
				if (errorText.Contains("Unknown server tag"))
				{
					errorText += " Please, register server tag before load layout from XML.";
				}
				//
				throw new HttpException(errorText);
			}
		}

		/// <summary>
		/// List of property names that will be serialized
		/// </summary>
		/// <param name="o">Control object</param>
		/// <param name="p">Control property to be matched</param>
		/// <param name="layout">Layout</param>
		/// <returns>A boolean value that indicates if control property should be serialized</returns>
		private bool CheckPropMatch(object o, System.Reflection.PropertyInfo p, LayoutType layout)
		{
			if (o is TControl || o is TItem)
			{
				switch (p.Name)
				{
					case "ScrollSettings":
					case "BackColor":
					case "BorderColor":
					case "BorderWidth":
					case "BorderStyle":
					case "Font":
					case "ForeColor":
					case "CssClass":
					case "SkinID":
						return layout == LayoutType.All || layout == LayoutType.Appearance;
					case "Height":
					case "Width":
						return layout == LayoutType.All || layout == LayoutType.Sizes;
				}
			}
			return false;
		}

		private bool GetIsActualVersion(XmlTextReader reader)
		{
			if (reader.NamespaceURI != null && (reader.NamespaceURI.Equals(this._namespaceURI, StringComparison.InvariantCultureIgnoreCase)
				|| reader.NamespaceURI.Equals(this._namespaceAlternativeURI, StringComparison.InvariantCultureIgnoreCase)))
				return true;
			return false;
		}

		private void CheckTypeMatch(XmlTextReader reader, object o)
		{
			if (((reader.Name == o.GetType().Namespace + "." + o.GetType().Name)
				|| (reader.Name == _defaultControlNamespace + "." + o.GetType().Name)) && GetIsActualVersion(reader))
				return;

			//TODO
			throw new Exception(string.Format("Xml file does not contain information for current {0} version.", o.GetType().Name)); ;
			//throw new Exception(string.Format(C1WebCommandStrings.c_xmlNotContainInformation, o.GetType().Name)); ;
		}


		private bool GetLayoutType(object[] layoutAttributes, out LayoutType layoutTypes)
		{
			layoutTypes = LayoutType.None;

			if (layoutAttributes != null && layoutAttributes.Length > 0)
			{
				LayoutAttribute atr = layoutAttributes[0] as LayoutAttribute;
				if (atr != null)
				{
					layoutTypes = atr.LayoutType;
					return true;
				}
			}
			return false;
		}
		
		private bool NeedSerializeType(System.Type type)
		{
			object[] atr = type.GetCustomAttributes((typeof(LayoutAttribute)), false);

			LayoutType lType;
			bool hasType = GetLayoutType(atr, out lType);

			return (hasType) ? (lType != LayoutType.None) : true;
		}

		private bool GetInheritedPropType(string propName, out LayoutType layoutTypes)
		{
			layoutTypes = LayoutType.None;

			if (_inheritedProps != null)
				if (_inheritedProps[propName] != null)
				{
					layoutTypes = (LayoutType)_inheritedProps[propName];
					return true;
				}

			return false;
		}

		// for fix the issue 32241, in chart series data, the stringValues, numberValues, datetimeValues should only one exist in the serialized xml.
		/// <summary>
		/// Check whether property needs seriazlie.
		/// </summary>
		/// <param name="o">object value</param>
		/// <param name="p">Property Info</param>
		/// <returns></returns>
		protected virtual bool NeedSerializeProp(object o, PropertyInfo p)
		{
			bool inh = CheckPropMatch(o, p, LayoutType.All);
			if (inh)
			{
				return true;
			}
			
			LayoutAttribute layoutAttr = (LayoutAttribute)Attribute.GetCustomAttribute(p, typeof(LayoutAttribute));
			if (layoutAttr != null)
			{
				return (layoutAttr.LayoutType != LayoutType.None);
			}
			else
			{
				object[] attributes = o.GetType().GetCustomAttributes((typeof(SerializeAllAttribute)), true);
				return (attributes.Length != 0 && ((SerializeAllAttribute)attributes[0]).Value) || (attributes.Length == 0 && !(o is WebControl));
			}
		}

		private PropertyInfo[] GetProps(object o)
		{
			if (o != null)
			{
				Type type = o.GetType();
				PropertyInfo[] pi = type.GetProperties(_bindFlag);
				return pi;
			}
			else
				return new PropertyInfo[] { };
		}

		private bool IsSimple(PropertyInfo p, object o)
		{
			return (p.PropertyType.IsPrimitive || p.PropertyType.IsValueType || o is ICollection || o is string
				 || (o.GetType() == System.Type.GetType("System.RuntimeType"))

				);
		}

		private void SerializeCollection(XmlTextWriter writer, object o)
		{
			foreach (object obj in (ICollection)o)
			{
				Type type = obj.GetType();

				if (NeedSerializeType(type))
				{
					TypeConverter tc = TypeDescriptor.GetConverter(type);

					writer.WriteStartElement(XmlConvert.EncodeName(type.FullName));

					if (type.IsSerializable && tc.CanConvertTo(typeof(string)))
						writer.WriteString(tc.ConvertToString(null, CultureInfo.InvariantCulture, obj));
					else
						InnerSerialize(writer, obj);

					writer.WriteEndElement();
				}
			}
		}

		private void InnerSerialize(XmlTextWriter writer, object o)
		{

			PropertyInfo[] pi = GetProps(o);

			foreach (PropertyInfo p in pi)
			{
				if (NeedSerializeProp(o, p) && NeedSerializeType(p.PropertyType) && p.CanRead)
				{
					object obj = p.GetValue(o, null);

					if (obj == null)
						continue;

					if (obj.Equals(o))
						break;

					if (obj is ITemplate)
					{
						try
						{
							string sTemplateContent = GetTemplateContent((ITemplate)obj);// this must goes in 
							// first line! in order to get CantHandleITemplateProperty exception
							// until any actions.
							writer.WriteStartElement(p.Name);
							//writer.WriteRaw("<!--template:");
							sTemplateContent = System.Web.HttpUtility.UrlEncode(sTemplateContent, Encoding.UTF32);
							writer.WriteRaw(sTemplateContent);
							//writer.WriteRaw("-->");
							//SerializeCollection(writer, obj);
							writer.WriteEndElement();
						}
						catch (CantHandleITemplateProperty)
						{
							continue;
						}
					}
					else if (!IsSimple(p, obj) && (GetProps(obj).Length > 0))
					{
						writer.WriteStartElement(p.Name);
						InnerSerialize(writer, obj);
						writer.WriteEndElement();
					}
					else if (obj is ICollection)
					{
						writer.WriteStartElement(p.Name);
						SerializeCollection(writer, obj);
						writer.WriteEndElement();
					}
					else
					{
						TypeConverter tc = TypeDescriptor.GetConverter(obj.GetType());
						if (tc.CanConvertTo(typeof(string)) && p.CanWrite)
						{
							writer.WriteStartElement(p.Name);
							writer.WriteString(tc.ConvertToString(null, CultureInfo.InvariantCulture, obj));
							writer.WriteEndElement();
						}
					}
				}
			}
		}
		private object RootControl;
		private object InnerDeserialize(XmlTextReader reader, object control, Type type, LayoutType deserializeTypes)
		{
			
			if (type == null)
				return null;
			if (type == typeof(ITemplate))
			{
				string sTemplateContent = reader.ReadString();
				sTemplateContent = System.Web.HttpUtility.UrlDecode(sTemplateContent, Encoding.UTF32);

				IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
				if (host != null)
				{
					/*
					try
					{
						*/
					ITemplate template = ControlParser.ParseTemplate(host, sTemplateContent);
					return template;
					/*
				}
				catch (Exception)
				{
					// Probably control tag that located inside template is not registered within page.
					return null;    
				}
				*/
				}
				else
				{
					// TODO: deserialize to ITemplate at runtime.
					return null;
				}
			}
			else if (type.IsArray)
			{
				ArrayList objectArray = new ArrayList();
				DeserializeArray(reader, objectArray, deserializeTypes);
				return objectArray.ToArray(type.GetElementType());
			}
			else if (type == typeof(ControlCollection))
			{
				if (control == null)
					return null;

				if (control is IStateManager)
					((IStateManager)control).TrackViewState();
				DeserializeArray(reader, (ControlCollection)control, deserializeTypes);
				return control;
			}
			else if (type.GetInterface("IList") != null)
			{
				if (control == null)
					control = Activator.CreateInstance(type);
				if (control is IStateManager)
					((IStateManager)control).TrackViewState();
				DeserializeArray(reader, (IList)control, deserializeTypes);
				return control;
			}

			if (reader.NodeType == XmlNodeType.Element)
				reader.MoveToElement();

			if (reader.IsEmptyElement)
				//return (o == null) ? DeserializeText(reader, type) : o;
				return DeserializeText(reader, type);

			while (reader.Read())
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						if (control == null)
						{
							control = Activator.CreateInstance(type);
						}

						PropertyInfo pi = type.GetProperty(reader.LocalName, _bindFlag);

						if (pi != null && NeedDeserializeProp(pi, deserializeTypes) &&
							NeedDeserializeType(pi.PropertyType, deserializeTypes))
						{
							try
							{
								object obj = pi.GetValue(control, null);
								object v = InnerDeserialize(reader, obj, pi.PropertyType, deserializeTypes);

								//if (pi.CanWrite && v != null)
								if (pi.CanWrite && (v != null) && !v.Equals(obj))
								{
									object old_v = null;
									if (_componentChangeService != null)
									{
										old_v = pi.GetValue(control, null);
										_componentChangeService.OnComponentChanging(control, TypeDescriptor.GetProperties(control)[reader.LocalName]);
									}
									pi.SetValue(control, v, null);
									if (_componentChangeService != null)
										_componentChangeService.OnComponentChanged(control, TypeDescriptor.GetProperties(control)[reader.LocalName], old_v, v);
								}
							}
							catch (Exception ex)
							{
								throw new HttpException(ex.Message);
								//+ "," + ex.StackTrace);
							}
						}
						else if (!reader.IsEmptyElement)
						{
							int level = 1;
							while (level > 0 && reader.Read())
								switch (reader.NodeType)
								{
									case XmlNodeType.Element:
										if (!reader.IsEmptyElement)
											level++;
										break;
									case XmlNodeType.EndElement:
										level--;
										break;
								}
						}
						break;
					case XmlNodeType.Text:
						return DeserializeText(reader, type);
					case XmlNodeType.EndElement:
						return control;
				}

			return control;

		}

		/// <summary>
		/// Check whether property need deserialize.
		/// </summary>
		/// <param name="pi">Property info</param>
		/// <param name="deserializeTypes">Deserialize types</param>
		/// <returns>Whether property need deserialize</returns>
		protected virtual bool NeedDeserializeProp(PropertyInfo pi, LayoutType deserializeTypes)
		{
			LayoutAttribute layoutAttr = (LayoutAttribute)Attribute.GetCustomAttribute(pi, typeof(LayoutAttribute));
			if (layoutAttr == null)
			{
				if (pi.ReflectedType == _serObjectType)
				{
					LayoutType type;
					bool hasType = GetInheritedPropType(pi.Name, out type);

					return (hasType)
						? ((deserializeTypes & type) != 0 && type != LayoutType.None)
						: true;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return ((deserializeTypes & layoutAttr.LayoutType) != 0 && layoutAttr.LayoutType != LayoutType.None);
			}
		}

		private bool NeedDeserializeType(System.Type type, LayoutType deserializeTypes)
		{
			LayoutType lType;
			bool hasType = GetLayoutType(type.GetCustomAttributes((typeof(LayoutAttribute)), false), out lType);

			if (!hasType)
				return true;
			else
				return ((deserializeTypes & lType) != 0 && lType != LayoutType.None);
		}

		private object DeserializeText(XmlTextReader reader, Type type)
		{
			object res = null;
			TypeConverter tc = TypeDescriptor.GetConverter(type);
			if (tc != null && tc.CanConvertTo(typeof(string)))
			{
				string val = reader.IsEmptyElement ? String.Empty : reader.Value;
				try
				{
					res = tc.ConvertFromString(null, CultureInfo.InvariantCulture, val);
				}
				catch
				{
				}

			}

			if (reader.NodeType == XmlNodeType.Text)
				while (reader.Read() && reader.NodeType != XmlNodeType.EndElement) ;

			return res;
		}

		private void DeserializeArray(XmlTextReader reader, ControlCollection container, LayoutType deserializeTypes)
		{//

			if (deserializeTypes != LayoutType.None)
			{
				if (!OnClearItems(container))
				{
					container.Clear();
				}
			}
			else
				return;
			//
			if (reader.IsEmptyElement || reader.NodeType != XmlNodeType.Element)
				return;

			reader.MoveToElement();
			while (reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						Type type = System.Type.GetType(XmlConvert.DecodeName(reader.LocalName));
						if (type == null)
							type = typeof(object);

						object obj = (Control)InnerDeserialize(reader, null, type, deserializeTypes);
						if (!OnAddItem(container, obj))
						{
							container.Add((Control)obj);
						}
						break;
					case XmlNodeType.EndElement:
						return;
				}
			}
		}

		/// <summary>
		/// Deserialize an array element of the control
		/// </summary>
		/// <param name="reader">XML reader</param>
		/// <param name="container">Control list</param>
		/// <param name="deserializeTypes">Types to be deserialized</param>
		private void DeserializeArray(System.Xml.XmlTextReader reader,
			System.Collections.IList container,
			LayoutType deserializeTypes)
		{

			if (!OnClearItems(container))
			{
				if (container is C1ObservableItemCollection<TICollectionOwner, TItem>)
					((C1ObservableItemCollection<TICollectionOwner, TItem>)container).Clear();
				else
					container.Clear();
			}

			//
			if (reader.IsEmptyElement || reader.NodeType != XmlNodeType.Element)
				return;

			reader.MoveToElement();
			while (reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						string name = reader.LocalName.Replace(this._namespaceURI, this._defaultControlNamespace);
						Type type = System.Type.GetType(XmlConvert.DecodeName(name) + "," + XmlConvert.DecodeName(this._assemblyName));

						//Add comments by RyanWu@20110628.
						//For fixing the issue#15893.
						//if (type == null)
						//{
						//    type = typeof(object);
						//}
						if (type == null)
						{
							type = System.Type.GetType(name);

							if (type == null)
							{
								type = typeof(object);
							}
						}
						//end by RyanWu@20110628.

						object obj = this.InnerDeserialize(reader, null, type, deserializeTypes);

						if (!OnAddItem(container, obj))
						{
							if (container is C1ObservableItemCollection<TICollectionOwner, TItem>)
								((C1ObservableItemCollection<TICollectionOwner, TItem>)container).Add((TItem)obj);
							else
								container.Add(obj);
						}
						break;
					case XmlNodeType.EndElement:
						return;
				}
			}
		}

		/// <summary>
		/// Helper method to instantiate the given template into a control
		/// an slurp out the markup.
		/// </summary>
		/// <param name="template"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		private string GetTemplateContent(ITemplate template)//, string id
		{
			DesignerPanel contentPanel = new DesignerPanel();
			contentPanel.ID = "_itemplate";// id;

			template.InstantiateIn(contentPanel);

			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
			//TODO: handle ITemplate properties at runtime?
			if (host == null)
				throw new CantHandleITemplateProperty();


			StringBuilder persistedControl = new StringBuilder(1024);
			foreach (System.Web.UI.Control c in contentPanel.Controls)
			{
				persistedControl.Append(ControlPersister.PersistControl(c, host));
			}
			return persistedControl.ToString();
		}

		private object GetService(Type type)
		{
			if (((WebControl)SerializableObject).Site != null)
				return ((WebControl)SerializableObject).Site.GetService(type);
			else
				return null;
		}

		/// <summary>
		/// Simple class to use for template instantiation
		/// </summary>
		internal class DesignerPanel : System.Web.UI.WebControls.Panel, INamingContainer
		{
		}

		#endregion

	}

	internal class CantHandleITemplateProperty : Exception
	{
		internal CantHandleITemplateProperty()
			: base()
		{

		}
	}
}
