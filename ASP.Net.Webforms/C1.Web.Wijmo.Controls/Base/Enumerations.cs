﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls
{

	/// <summary>
	/// Layouts for XML serialization.
	/// </summary>
	[Flags]
	public enum LayoutType
	{
		/// <summary>
		/// No properties save/load during serialization process.
		/// </summary>
		None = 0x0,
		/// <summary>
		/// Appearance properties save/load during serialization process.
		/// </summary>
		Appearance = 0x1,
		/// <summary>
		/// Behavior properties save/load during serialization process.
		/// </summary>
		Behavior = 0x2,
		/// <summary>
		/// Misc properties save/load during serialization process.
		/// </summary>
		Misc = 0x4,
		/// <summary>
		/// Style properties save/load during serialization process.
		/// </summary>
		Styles = 0x8,
		/// <summary>
		/// Sizes save/load during serialization process.
		/// </summary>
		Sizes = 0x10,
		/// <summary>
		/// Accessibility save/load during serialization process.
		/// </summary>
		Accessibility = 0x20,
		/// <summary>
		/// Data save/load during serialization process.
		/// </summary>
		Data = 0x40,
		/// <summary>
		/// All groups of properties save/load during serialization process.
		/// </summary>
		All = Appearance | Behavior | Misc | Styles | Sizes | Accessibility | Data
	}

}
