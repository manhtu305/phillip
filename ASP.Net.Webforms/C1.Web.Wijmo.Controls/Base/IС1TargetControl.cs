﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls
{
	interface IC1TargetControl : IWijmoWidgetSupport, IScriptControl, IJsonRestore
	{
		string WidgetName { get; }
		IDictionary ViewState { get; }
	}
}
