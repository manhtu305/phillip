﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel;


namespace C1.Web.Wijmo.Controls.Base.Collections
{
	/// <summary>
	/// Provides the class for a generic collection.  Represents a collection of C1ObservableItem.
	/// </summary>
	/// <typeparam name="TOwner">The type of owner of the collection.</typeparam>
	/// <typeparam name="TItem">The type of elements in the collection.</typeparam>
	public class C1ObservableItemCollection<TOwner, TItem> : Collection<TItem>
	{
		TOwner _owner;

		private EventHandlerList _events = new EventHandlerList();
		private static readonly object EVENT_Insert = new object();
		private static readonly object EVENT_Remove = new object();
		private static readonly object EVENT_Clear = new object();
		private static readonly object EVENT_Set = new object();

		/// <summary>
		/// Initializes a new instance of the C1ObservableItemCollection<TOwner, TItem> class.
		/// </summary>
		/// <param name="owner">The type of owner of the collection.</param>
		public C1ObservableItemCollection(TOwner owner)
			: base()
		{
			_owner = owner;
		}

		/// <summary>
		/// Gets the number of elements actually contained in the collection.
		/// </summary>
		public new virtual int Count
		{
			get
			{
				return this.Items.Count;
			}
		}

		/// <summary>
		/// Gets the owner of the collection.
		/// </summary>
		public TOwner Owner
		{
			get { return _owner; }
		}

		/// <summary>
		/// Gets the events of the collection.
		/// </summary>
		protected EventHandlerList Events
		{
			get { return _events; }
		}

		/// <summary>
		/// Inserts an element into the collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which item should be inserted.</param>
		/// <param name="item">The object to insert.</param>
		protected override void InsertItem(int index, TItem item)
		{
			base.InsertItem(index, item);

			CollectionChangedEventHandler eh = Events[EVENT_Insert] as CollectionChangedEventHandler;
			if (eh != null)
			{
				eh(this, new CollectionChangedEventArgs(ChangeType.Added, item, index));
			}
		}

		/// <summary>
		/// Removes the element at the specified index of the collection.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		protected override void RemoveItem(int index)
		{
			TItem item = this[index];
			base.RemoveItem(index);

			CollectionChangedEventHandler eh = Events[EVENT_Remove] as CollectionChangedEventHandler;
			if (eh != null)
			{
				eh(this, new CollectionChangedEventArgs(ChangeType.Removed, item, index));
			}
		}

		/// <summary>
		/// Removes all elements from the collection.
		/// </summary>
		protected override void ClearItems()
		{
			base.ClearItems();

			CollectionChangedEventHandler eh = Events[EVENT_Clear] as CollectionChangedEventHandler;
			if (eh != null)
			{
				eh(this, new CollectionChangedEventArgs(ChangeType.Removed, null, -1));
			}

		}

		/// <summary>
		/// Replaces the element at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to replace.</param>
		/// <param name="item">The new value for the element at the specified index.</param>
		protected override void SetItem(int index, TItem item)
		{
			base.SetItem(index, item);
			CollectionChangedEventHandler eh = Events[EVENT_Set] as CollectionChangedEventHandler;
			if (eh != null)
			{
				eh(this, new CollectionChangedEventArgs(ChangeType.Replaced, item, index));
			}
		}
		#region events

		/// <summary>
		/// Fires after the item is replaced.
		/// </summary>
		public event CollectionChangedEventHandler ItemReplaced
		{
			add { Events.AddHandler(EVENT_Set, value); }
			remove { Events.RemoveHandler(EVENT_Set, value); }
		}

		/// <summary>
		/// Fires after the item is removed..
		/// </summary>
		public event CollectionChangedEventHandler ItemRemoved
		{
			add { Events.AddHandler(EVENT_Remove, value); }
			remove { Events.RemoveHandler(EVENT_Remove, value); }
		}

		/// <summary>
		/// Fires after the item is added.
		/// </summary>
		public event CollectionChangedEventHandler ItemAdded
		{
			add { Events.AddHandler(EVENT_Insert, value); }
			remove { Events.RemoveHandler(EVENT_Insert, value); }
		}

		/// <summary>
		/// Fires after the item is cleared.
		/// </summary>
		public event CollectionChangedEventHandler Cleared
		{
			add { Events.AddHandler(EVENT_Clear, value); }
			remove { Events.RemoveHandler(EVENT_Clear, value); }
		}

		/// <summary>
		/// Specifies how the collection was changed.
		/// </summary>
		public enum ChangeType
		{
			/// <summary>
			/// Item was added to the collection.
			/// </summary>
			Added,
			/// <summary>
			/// Item was removed from the collection.
			/// </summary>
			Removed,
			/// <summary>
			/// Item was replaced in the collection.
			/// </summary>
			Replaced,
			/// <summary>
			/// The collection was cleared.
			/// </summary>
			Cleared
		};

		/// <summary>
        /// Occurs when the collection is changed.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void CollectionChangedEventHandler(object sender, CollectionChangedEventArgs e);

		/// <summary>
		/// Provides data for the CollectionChanged events.
		/// </summary>
		public class CollectionChangedEventArgs : EventArgs
		{
			ChangeType _type = ChangeType.Added;
			object _item;
			int _index;

			internal CollectionChangedEventArgs(ChangeType type, object item, int index)
			{
				_type = type;
				_item = item;
				_index = index;
			}

			/// <summary>
			/// Indicates how the collection was changed.
			/// </summary>
			public ChangeType ChangeType
			{
				get { return _type; }
			}

			/// <summary>
			/// The item associated with the change in the collection.
			/// </summary>
			public object Item
			{
				get { return _item; }
			}

			/// <summary>
			/// The index in the collection associated with the item.
			/// </summary>
			public int Index
			{
				get { return _index; }
			}
		}

		#endregion
	}
}
