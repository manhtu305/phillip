﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.Base
{
	/// <summary>
	/// Base class for all elements, handles layout and styles. 
	/// </summary>
	//[ParseChildren(true, "Children")] //needed by the page parser
	[ParseChildren(true)]
	[PersistChildren(false)] // Needed by the designer
	[ToolboxItemAttribute(false)]
	public class UIElement : WebControl, IJsonRestore
    {
        #region Fields

        private Hashtable _props;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates what control locates on design surface.
        /// </summary>
        protected bool IsDesignMode = (System.Web.HttpContext.Current == null);

        /// <summary>
        /// Gets a hashtable to store property info.
        /// </summary>
        protected Hashtable Props
        {
            get
            {
                if (_props == null) _props = new Hashtable();
                return _props;
            }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
        /// </summary>
        /// <remarks>
        /// Note that a server control is created and invisible if DisplayVisible is set to false.
        /// </remarks>
        [C1Description("C1Controls.DisplayVisible")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        [DefaultValue(true)]
        [WidgetOption]
        public bool DisplayVisible
        {
            get
            {
                return this.GetPropertyValue("DisplayVisible", true);
            }
            set
            {
                this.SetPropertyValue("DisplayVisible", value);
            }
        }

        // hide the Visible property
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
            }
        }
        #endregion

        #region Constructor

        /// <summary>
		/// UIElement constructor
		/// </summary>
		public UIElement()
		{
		}

		/// <summary>
		/// UIElement constructor
		/// </summary>
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public UIElement(HtmlTextWriterTag tag)
			: base(tag)
		{
		}

        #endregion

        #region --- object model ---
        [WidgetOption]
        public override string CssClass
        {
            get
            {
                return base.CssClass;
            }
            set
            {
                base.CssClass = value;
            }
        }

        /// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override BorderStyle BorderStyle
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit BorderWidth
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override FontInfo Font
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// Gets or sets the Height property of the UIElement.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the Width property of the UIElement.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool EnableTheming
		{
			get
			{
				return base.EnableTheming;
			}
			set
			{
				base.EnableTheming = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override bool EnableViewState
		{
			get
			{
				return base.EnableViewState;
			}
			set
			{
				base.EnableViewState = value;
			}
		}
		#endregion

        #region Methods

        /// <summary>
        /// Gets the property value by property name.
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="nullValue">The null value.</param>
        /// <returns></returns>
        protected V GetPropertyValue<V>(string propertyName, V nullValue)
        {
            if (this.Props[propertyName] == null)
            {
                return nullValue;
            }
            return (V)this.Props[propertyName];
        }

        /// <summary>
        /// Sets the property value by property name.
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        protected void SetPropertyValue<V>(string propertyName, V value)
        {
            this.Props[propertyName] = value;
        }

        /// <summary>
        /// Tests the property value by property name.
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="testValue">The test value.</param>
        /// <returns></returns>
        protected bool TestPropertyValue<V>(string propertyName, V testValue)
        {
            object value;
            return (_props != null && ((value = _props[propertyName]) != null) && value.Equals(testValue));
        }

        /// <summary>
        /// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (!DisplayVisible && !this.IsDesignMode)
            {
                this.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            else
            {
                this.Style.Remove(HtmlTextWriterStyle.Display);
            }

            base.AddAttributesToRender(writer);
        }
        #endregion

#if !EXTENDER
        #region ** IJsonRestore interfact implementations

        void IJsonRestore.RestoreStateFromJson(object state)
        {
            this.RestoreStateFromJson(state);
        }

        /// <summary>
        /// Restore the states from the JSON object.
        /// </summary>
        /// <param name="state">The state which is parsed from the JSON string.</param>
        protected virtual void RestoreStateFromJson(object state)
        {
            JsonRestoreHelper.RestoreStateFromJson(this, state);
        }

        #endregion end of ** IJsonRestore interfact implementations.
#endif
	}
}
