﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using C1.Web.Wijmo;
using System.Web;
using System.Reflection;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Converters;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls
{

	/// <summary>
	///  Class to be used as base for the wijmo target controls.
	/// </summary>
	public abstract partial class C1TargetControlBase
	{

		#region ** constructors


		/// <summary>
		/// Initializes a new instance of the C1.Web.Wijmo.Controls.C1TargetControlBase class using the specified HTML tag.
		/// </summary>
		/// <param name="tag">One of the System.Web.UI.HtmlTextWriterTag values..</param>
		public C1TargetControlBase(HtmlTextWriterTag tag)
			: base(tag)
		{
			this.InitializeControl();
		}

		/// <summary>
		/// Initializes a new instance of the C1.Web.Wijmo.Controls.C1TargetControlBase class using the specified HTML tag.
		/// </summary>
		/// <param name="tag">An HTML tag.</param>
		protected C1TargetControlBase(string tag)
			: base(tag)
		{
			this.InitializeControl();
		}

		#endregion

		#region ** fields
		private Hashtable _innerStates;

		[SmartAssembly.Attributes.DoNotObfuscate]
		internal const string WIJMOCONTROLSRESOURCEHANDLER = "~/WijmoControlsResource.axd";
		#endregion

		#region ** properties

		/// <summary>
		/// A hashtable value contains all information which need transfer from server to client.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DefaultValue(false)]
		public virtual Hashtable InnerStates
		{
			get
			{
				if (this._innerStates == null)
				{
					this._innerStates = new Hashtable();
				}

				return this._innerStates;
			}
		}

		#endregion

	}
}
