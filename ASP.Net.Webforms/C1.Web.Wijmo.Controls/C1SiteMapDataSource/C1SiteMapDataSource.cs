﻿using C1.Web.Wijmo.Controls.Localization;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1SiteMapDataSource", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1SiteMapDataSource
{
#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource.C1SiteMapDataSourceDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource.C1SiteMapDataSourceDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer("C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource.C1SiteMapDataSourceDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
    [Designer("C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource.C1SiteMapDataSourceDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxBitmap(typeof(C1SiteMapDataSource), "C1SiteMapDataSource.png")]
    [ToolboxData("<{0}:C1SiteMapDataSource runat=server></{0}:C1SiteMapDataSource>")]
    [LicenseProviderAttribute()]
    [DefaultProperty("SiteMapFile")]
    public class C1SiteMapDataSource : SiteMapDataSource
    {
        //for license.
        private bool _productLicensed = false;
        private bool _shouldNag;

        public C1SiteMapDataSource()
        {
            VerifyLicense();
        }

        #region Licensing

        internal virtual void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1SiteMapDataSource), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #endregion

        /// <summary>
        /// Gets or sets the relative path to the .sitemap file from which to load SiteMap data.
        /// </summary>
        /// <value>The site map file.</value>
        [Editor("C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource.C1SiteMapFileUrlEditor", "System.Drawing.Design.UITypeEditor")]
        [C1Description("C1SiteMapDataSource.SiteMapFile")]
        [C1Category("Category.Data")]
        [DefaultValue("")]
        [UrlProperty("*.sitemap")]
        public string SiteMapFile
        {
            get 
            {
                return (string)ViewState["SiteMapFile"];;
            }
            set
            {
                ViewState["SiteMapFile"] = value;

                UpdateProvider(value);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (HttpContext.Current == null && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }

            base.OnInit(e);
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

            SiteMapFile = (string)ViewState["SiteMapFile"];
        }

        private void ShowAbout()
        {
            Type t = typeof(C1TargetControlBase).Assembly.GetType("C1.Util.Licensing.ProviderInfo");
            System.Reflection.MethodInfo _about = t.GetMethod("ShowAboutBox", System.Reflection.BindingFlags.Public |
                                              System.Reflection.BindingFlags.Static |
                System.Reflection.BindingFlags.NonPublic, null,
                  new Type[] { typeof(object) }, null);

            _about.Invoke(this, new object[] { this });
        }

        private void UpdateProvider(string siteMapFile)
        {
            if (!DesignMode)
            {
                if (!string.IsNullOrEmpty(siteMapFile))
                {
                    XmlSiteMapProvider xmlSiteMapProvider = new XmlSiteMapProvider();
                    xmlSiteMapProvider.Initialize("TemporaryRunTimeXmlSiteMapProvider", new NameValueCollection() { { "siteMapFile", siteMapFile } });

                    Provider = xmlSiteMapProvider;
                }
            }
        }
    }
}
