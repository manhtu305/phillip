﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	public class C1ChartSerializer<TControl> : C1BaseSerializer<TControl, object, object>
	{
		public C1ChartSerializer(Object obj) : base(obj) { 
		
		}

		protected override bool NeedSerializeProp(object o, System.Reflection.PropertyInfo p)
		{			
			if (p.Name == "StringValues" || p.Name == "DoubleValues" || p.Name == "DateTimeValues")
			{
				Array arr = p.GetValue(o, null) as Array;
				if (arr == null || arr.Length == 0) {
					return false;
				}
			}
			return base.NeedSerializeProp(o, p);
		}
	}
}
