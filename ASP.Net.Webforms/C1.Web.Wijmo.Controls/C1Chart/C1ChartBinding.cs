﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartBinding.XFieldType property.
	/// </summary>
	public enum ChartDataXFieldType
	{
		/// <summary>
		/// Applies the string data type to the chart.
		/// </summary>
		String = 0,
		/// <summary>
		/// Applies the number data type to the chart.
		/// </summary>
		Number = 1,
		/// <summary>
		/// Applies the DateTime data type to the chart.
		/// </summary>
		DateTime = 2
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartBinding.YFieldType property.
	/// </summary>
	public enum ChartDataYFieldType
	{
		/// <summary>
		/// Applies the number data type to the chart.
		/// </summary>
		Number = 0,
		/// <summary>
		/// Applies the DateTime data type to the chart.
		/// </summary>
		DateTime = 1
	}

	/// <summary>
	/// Defines the relationship between data item and the series data it is binding to.
	/// </summary>
	public class C1ChartBinding : ICloneable, IDataSourceViewSchemaAccessor
	{

		#region fields
		private string _dataMember = string.Empty;
		private string _xField = string.Empty;
		private string _yField = string.Empty;
		private string _hintField = string.Empty;
		private ChartDataXFieldType _xFieldType = ChartDataXFieldType.String;
		private ChartDataYFieldType _yFieldType = ChartDataYFieldType.Number;
		private object _dataSourceViewSchema;

		private bool _visible = true;
		private bool _legendEntry = true;
		private string _label=string.Empty;
		//private ChartStyle _textStyle;

        private bool _isTrendline = false;
        private TrendlineFitType _trendlineFitType = TrendlineFitType.Polynom;
        private int _trendlineSampleCount = 100;
        private int _trendlineOrder = 4;

		#endregion

		#region constructors
		/// <summary>
		/// Initializes a new instance of the C1ChartBinding class.
		/// </summary>
		public C1ChartBinding() { }

		/// <summary>
		/// Initializes a new instance of the C1ChartBinding class.
		/// </summary>
		/// <param name="dataMember">Data member to bind chart data.</param>
		public C1ChartBinding(string dataMember)
		{
			this.DataMember = dataMember;
		}
		#endregion

		#region properties

		/// <summary>
		/// Gets or sets the data member to bind chart data.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.DataMember")]
		[DefaultValue("")]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public string DataMember
		{
			get
			{
				return _dataMember;
			}
			set
			{
				_dataMember = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1Chart X values.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.XField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public virtual string XField
		{
			get
			{
				return _xField;
			}
			set
			{
				_xField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1Chart Y values.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.YField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public virtual string YField
		{
			get
			{
				return _yField;
			}
			set
			{
				_yField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates C1Chart Hint values.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.HintField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public virtual string HintField
		{
			get
			{
				return _hintField;
			}
			set
			{
				_hintField = value;
			}
		}

		/// <summary>
		/// Gets or sets the type of x field from the data source that indicates C1Chart x value type.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.XFieldType")]
		[DefaultValue(ChartDataXFieldType.String)]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public virtual ChartDataXFieldType XFieldType
		{
			get
			{
				return _xFieldType;
			}
			set
			{
				_xFieldType = value;
			}
		}

		/// <summary>
		/// Gets or sets the type of y field from the data source that indicates C1Chart y value type.
		/// </summary>
		[C1Description("C1Chart.ChartBinding.YFieldType")]
		[DefaultValue(ChartDataYFieldType.Number)]
		[C1Category("Category.Databindings")]
		[Layout(LayoutType.Data)]
		public virtual ChartDataYFieldType YFieldType
		{
			get
			{
				return _yFieldType;
			}
			set
			{
				_yFieldType = value;
			}
		}

        /// <summary>
        /// Gets or sets the value indicates whether it's trendline.
        /// </summary>
        [C1Description("C1Chart.ChartBinding.IsTrendline")]
        [DefaultValue(false)]
        [C1Category("Category.Databindings")]
        [Layout(LayoutType.Data)]
        public virtual bool IsTrendline
        {
            get { return _isTrendline; }
            set { _isTrendline = value; }
        }

        /// <summary>
        /// Gets or sets the trendline fit type.
        /// </summary>
        [C1Description("C1Chart.ChartBinding.TrendLineFitType")]
        [DefaultValue(TrendlineFitType.Polynom)]
        [C1Category("Category.Databindings")]
        [Layout(LayoutType.Data)]
        public virtual TrendlineFitType TrendlineFitType
        {
            get { return _trendlineFitType; }
            set { _trendlineFitType = value; }
        }

        /// <summary>
        /// Gets or sets the sammple count used to draw trendline.
        /// </summary>
        [C1Description("C1Chart.ChartBinding.TrendLineSampleCount")]
        [DefaultValue(100)]
        [C1Category("Category.Databindings")]
        [Layout(LayoutType.Data)]
        public virtual int TrendlineSampleCount
        {
            get { return _trendlineSampleCount; }
            set { _trendlineSampleCount = value; }
        }

        /// <summary>
        /// Gets or sets the order value used to draw trendline.
        /// </summary>
        [C1Description("C1Chart.ChartBinding.TrendLineOrder")]
        [DefaultValue(4)]
        [C1Category("Category.Databindings")]
        [Layout(LayoutType.Data)]
        public virtual int TrendlineOrder
        {
            get { return _trendlineOrder; }
            set { _trendlineOrder = value; }
        }

		//add comments to add properties for tfs issue 16828
		public virtual bool Visible
		{
			get
			{
				return _visible;
			}
			set
			{
				_visible = value;
			}
		}

		public virtual bool LegendEntry
		{
			get
			{
				return _legendEntry;
			}
			set
			{
				_legendEntry = value;
			}
		}

		public virtual string Label
		{
			get
			{
				return _label;
			}
			set
			{
				_label = value;
			}
		}
		//end comments

		#endregion

		#region ICloneable
		/// <summary>
		/// Clone the chart binding.
		/// </summary>
		/// <returns>
		/// Returns the copy of a chart binding.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual object Clone()
		{
			C1ChartBinding binding = new C1ChartBinding();
			binding.DataMember = this.DataMember;
			binding.XField = this.XField;
			binding.YField = this.YField;
			binding.HintField = this.HintField;
			binding.XFieldType = this.XFieldType;
			binding.YFieldType = this.YFieldType;

			binding.Visible = this.Visible;
			binding.LegendEntry = this.LegendEntry;
			binding.Label = this.Label;

			return binding;
		}
		#endregion

		#region IDataSourceViewSchemaAccessor
		/// <summary>
		/// Gets or sets the schema associated with the chart.
		/// </summary>
		object IDataSourceViewSchemaAccessor.DataSourceViewSchema
		{
			get
			{
				return _dataSourceViewSchema;
			}
			set
			{
				this._dataSourceViewSchema = value;
			}
		}
		#endregion
	}
}
