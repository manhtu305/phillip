﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	public partial class ChartAnimation : ICustomOptionType, IJsonEmptiable
	{

		protected bool IsDesignMode = HttpContext.Current == null;

		#region Serialize
		/// <summary>
		/// Returns a boolean value that indicates whether the bar chart series
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this series should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal protected virtual bool ShouldSerialize()
		{
			//TODO: remove this after fix scatter's performance issue.
			return true;
			//if (IsDesignMode)
			//	return true;
			//if (!Enabled)
			//	return true;
			//if (Duration != 400)
			//	return true;
			//if (Easing != defaultEasing)
			//	return true;
			//return false;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}
		#endregion

		#region ICustomOptionType Members

		void ICustomOptionType.DeSerializeValue(object state)
		{
			Hashtable ht = state as Hashtable;
			if (ht != null)
			{
				if (ht["enabled"] != null)
				{
					this.Enabled = (bool)ht["enabled"];
				}
				if (ht["duration"] != null)
				{
					this.Duration = (int)ht["duration"];
				}
				if (ht["easing"] != null)
				{
					ConvertEasing(ht["easing"]);
					//this.Easing = (ChartEasing)ht["easing"];
				}
			}
		}

		private void ConvertEasing(object obj)
		{
			Hashtable easing = new Hashtable() {
				{">", "easeInCubic"}, 
				{"<", "easeOutCubic"},
				{"<>", "easeInOutCubic"},
				{"backIn", "easeInBack"},
				{"backOut", "easeOutBack"},
				{"elastic", "easeOutElastic"},
				{"bounce", "easeOutBounce"}
			};
			string value = (string)easing[obj.ToString()];
			if (String.IsNullOrEmpty(value))
			{
				this.Easing = (ChartEasing)obj;
			}
			else
			{
				this.Easing = (ChartEasing)Enum.Parse(typeof(ChartEasing), value, true);
			}
		}

		string ICustomOptionType.SerializeValue()
		{
			StringBuilder serializeValue = new StringBuilder();
			serializeValue.Append("{");
			serializeValue.Append(GetOptionValue());
			serializeValue.Append("}");
			return serializeValue.ToString();
		}

		/// <summary>
		/// Serializes the current animation object to a string value.
		/// </summary>
		/// <returns>
		/// Returns the string value that serialized from the current animation object.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual string GetOptionValue()
		{
			string optionValue = "";
			if (IsDesignMode)
			{
				optionValue += " enabled: false,";
			}
			else
			{
				//if (!this.Enabled)
				//	optionValue += " enabled: false,";
				//TODO: remove this after fix scatter's performance issue.
				if (this.Enabled)
					optionValue += " enabled: true,";
				else
					optionValue += " enabled: false,";
			}
			if (this.Duration != 400)
				optionValue += String.Format(" duration: {0},", this.Duration.ToString());
			if (this.Easing != defaultEasing)
				optionValue += String.Format(" easing: '{0}',", this.Easing.ToCamel());
			if (!String.IsNullOrEmpty(optionValue))
				optionValue = optionValue.Substring(0, optionValue.Length - 1);
			return optionValue;
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}
		#endregion
	}
}
