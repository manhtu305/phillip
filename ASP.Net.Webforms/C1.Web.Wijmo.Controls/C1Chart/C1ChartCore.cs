﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using C1.Web.Wijmo.Controls.Localization;
using System.Data;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	/// <summary>
	/// The C1ChartCore class contains common properties specific to the all charts.
	/// </summary>
	public abstract partial class C1ChartCore<Tseries, TAnimation, TBinding> : C1TargetDataBoundControlBase, IPostBackDataHandler
		where Tseries : ChartSeries, new()
		where TAnimation : ChartAnimation, new()
		where TBinding : C1ChartBinding, new()
	{

		List<TBinding> _dataBindings = null;

		/// <summary>
		/// Initializes a new instance of the C1ChartCore class.
		/// </summary>
		public C1ChartCore()
			: base()
		{
			this.Width = Unit.Pixel(600);
			this.Height = Unit.Pixel(400);
			this.InitChart();
		}

		/// <summary>
		/// A value that indicates the width of wijchart.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Width")]
        [C1Category("Category.Layout")]
		//[WidgetOption]
		[DefaultValue(typeof(Unit), "600")]
		[Layout(LayoutType.Sizes)]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// A value that indicates the height of wijchart.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Height")]
        [C1Category("Category.Layout")]
		//[WidgetOption]
		[DefaultValue(typeof(Unit), "400")]
		[Layout(LayoutType.Sizes)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		private Hashtable _innerStates;
		/// <summary>
		/// Gets the innerStates for tooltip. it send to client side. to deserialize the client function option.
		/// </summary>
		[Json(true)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public Hashtable InnerStates
		{
			get
			{
				if(_innerStates == null)
				{
					_innerStates = new Hashtable();
				}
				if (this.Hint.Content.Function != "")
				{
					if (_innerStates["Content"] == null)
					{
						_innerStates.Add("Content", this.Hint.Content.Function);
					}
					else
					{
						_innerStates["Content"] = this.Hint.Content.Function;
					}
				}
				if (this.Hint.Title.Function != "")
				{
					if (_innerStates["Title"] == null)
					{
						_innerStates.Add("Title", this.Hint.Title.Function);
					}
					else
					{
						_innerStates["Title"] = this.Hint.Title.Function;
					}
				}
				return _innerStates;
			}
		}

		#region DataBinding

		// hide the dataMember property
		/// <summary>
		/// Date Member of databinding.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string DataMember
		{
			get
			{
				return base.DataMember;
			}
			set
			{
				base.DataMember = value;
			}
		}

		//[Editor("C1.Web.Wijmo.Design.UITypeEditors.C1ChartBindingUITypeEditor, C1.Web.Wijmo.Design.2", typeof(UITypeEditor))]
		/// <summary>
		/// Gets a collection of C1ChartBinding objects that define the relationship 
		/// between a data item and the chart series it is binding to.
		/// </summary>
		[C1Description("C1Chart.DataBindings")]
        [C1Category("Category.Data")]
		[MergableProperty(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		public List<TBinding> DataBindings
		{
			get
			{
				if (_dataBindings == null)
				{
					_dataBindings = new List<TBinding>();
				}
				return _dataBindings;
			}
		}

		private int _dataBindingIndex = -1;

		/// <summary>
		/// Binds data from the data source to the chart control.
		/// </summary>
		/// <param name="dataSource">
		/// The <see cref ="T:System.Collections.IEnumerable" /> list of data returned from 
		/// a <see cref = "M:System.Web.UI.WebControls.DataBoundControl.PerformSelect()" /> method call.
		/// </param>
		protected override void PerformDataBinding(IEnumerable dataSource)
		{
			if (dataSource == null || DataBindings.Count == 0)
			{
				return;
			}
			if (IsDesignMode)
			{
				return;
			}

			ResetSeriesList();

			// fix issue 29463
			// if the databinding contains more than 1 dataMember. 

			for (int i = 0; i < DataBindings.Count; i++)
			{
				_dataBindingIndex = i;
				TBinding binding = DataBindings[i];

				// just handle the dataset datasource, I don't know the datamember can use the other types.
				if (DataSource != null && DataSource.GetType() == typeof(DataSet) && !string.IsNullOrEmpty(binding.DataMember) && !IsBoundUsingDataSourceID)
				{
					this.DataMember = binding.DataMember;
					var ov = this.GetData();
					DataSourceViewSelectCallback cv = new DataSourceViewSelectCallback(OnDataSourceViewSelectCallBack);
					ov.Select(DataSourceSelectArguments.Empty, cv);
				}
				else
				{
					IEnumerator e = dataSource.GetEnumerator();
					while (e.MoveNext())
					{
						object o = e.Current;
						if (o != null)
						{
							PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
							if (i >= SeriesList.Count)
							{
								SeriesList.Add(new Tseries());
							}
							InternalDataBind(o, pdc, SeriesList[i], binding);
						}
					}
				}
			}

				
			base.PerformDataBinding(dataSource);
		}

		protected virtual void ResetSeriesList()
		{
			SeriesList.Clear();
		}

		private void OnDataSourceViewSelectCallBack(IEnumerable obj)
		{
			IEnumerator e = obj.GetEnumerator();
			TBinding binding = this.DataBindings[_dataBindingIndex];
			while (e.MoveNext())
			{
				object o = e.Current;
				if (o != null)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
					if (_dataBindingIndex >= SeriesList.Count)
					{
						SeriesList.Add(new Tseries());
					}
					InternalDataBind(o, pdc, SeriesList[_dataBindingIndex], binding);
				}
			}
		}

		/// <summary>
		/// Internal binds data from the data source to the chart control.
		/// </summary>
		/// <param name="data">data of data source</param>
		/// <param name="pdc">property description collection</param>
		/// <param name="series">series of chart</param>
		/// <param name="binding">data binding info</param>
		protected virtual void InternalDataBind(object data, PropertyDescriptorCollection pdc, Tseries series, TBinding binding)
		{
            //firstly check wehther it's trendline, if true, the following code will add data to series.TrenlineSereis.Data.
            series.IsTrendline = binding.IsTrendline;
            if (series.IsTrendline)
            {
                series.TrendlineSeries.FitType = binding.TrendlineFitType;
                series.TrendlineSeries.Order = binding.TrendlineOrder;
                series.TrendlineSeries.SampleCount = binding.TrendlineSampleCount;
            }

			ChartSeriesData d = GetSeriesData(series);
			
            object xValue = GetFieldValue(data, pdc, binding.XField);
			AddDataToChart(binding.XFieldType.ToString(), xValue, d.X);
			
            object yValue = GetFieldValue(data, pdc, binding.YField);
			AddDataToChart(binding.YFieldType.ToString(), yValue, d.Y);
			
            if(!String.IsNullOrEmpty(binding.HintField))
			{
				object hintValue = GetFieldValue(data, pdc, binding.HintField);
				if (hintValue != null)
				{
					List<string> values = series.HintContents.ToList<string>();
					values.Add(hintValue.ToString());
					series.HintContents = values.ToArray();
				}
			}

			//add comments for tfs issue 16828
			if (!String.IsNullOrEmpty(binding.Label))
			{
				series.Label=binding.Label;
			}
			series.LegendEntry = binding.LegendEntry;
			series.Visible = binding.Visible;
			//end comments.

			if (String.IsNullOrEmpty(series.Label))
			{
				series.Label = binding.YField;
			}
		}
		

		internal void AddDataToChart(string type, object value, IChartYAxisList data)
		{
			if (value == null)
			{
				data.AddEmpty();
				return;
			}
			string val = value.ToString();
			if (type == "String")
			{
				//ChartXAxisList newData = (ChartXAxisList)data;
				//List<string> strValues = newData.StringValues.ToList<string>();
				//strValues.Add(val);
				//newData.StringValues = strValues.ToArray();
				((IChartXAxisList)data).Add(val);
			}
			else if (type == "Number")
			{
				//List<double> numValues = data.DoubleValues.ToList<double>();
				//numValues.Add(Double.Parse(val));
				//data.DoubleValues = numValues.ToArray();
				data.Add(Double.Parse(val));
			}
			else
			{
				//List<DateTime> dtValues = data.DateTimeValues.ToList<DateTime>();
				//dtValues.Add(DateTime.Parse(val));
				//data.DateTimeValues = dtValues.ToArray();
				data.Add(DateTime.Parse(val));
			}
		}

		internal void AddDoubleDataToChart(object value, IChartYAxisList data)
		{
			AddDataToChart("Number", value, data);
		}

		internal object GetFieldValue(object data, PropertyDescriptorCollection pdc, string propName)
		{
			PropertyDescriptor descr = pdc.Find(propName, true);
			if (descr != null)
			{
				return descr.GetValue(data);
			}
			else return null;
		}
		#endregion

		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag" /> value that
		/// corresponds to this chart control.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Deserialize the series list from the saved state.
		/// </summary>
		/// <param name="value">
		/// An object that contains the saved state values for the chart control.
		/// </param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeSeriesList(object value)
		{
			this.SeriesList.Clear();

			if (value is IList)
			{
				foreach (object item in (IList)value)
				{
					Hashtable hsItem = item as Hashtable;
					if (hsItem != null)
					{
						Tseries series = new Tseries();
						((IJsonRestore)series).RestoreStateFromJson(item);

                        if (series.IsTrendline)
                        {
                            ((IJsonRestore)series.TrendlineSeries).RestoreStateFromJson(item);
                        }

						this.SeriesList.Add(series);
					}
				}
			}
		}

		/// <summary>
		/// Deserialize the annotation list from the saved state.
		/// </summary>
		/// <param name="value">
		/// An object that contains the saved state values for the chart control.
		/// </param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeAnnotations(object value)
		{
			this.Annotations.Clear();

			if (value is IList)
			{
				foreach (object item in (IList)value)
				{
					Hashtable hsItem = item as Hashtable;
					if (hsItem != null)
					{
						AnnotationBase annoElement = null;
						string annoType = hsItem["type"].ToString();
						switch (annoType)
						{
							case "circle": 
								annoElement = new CircleAnnotation(); 
								break;
							case "ellipse": 
								annoElement = new EllipseAnnotation(); 
								break;
							case "image": 
								annoElement = new ImageAnnotation(); 
								break;
							case "line": 
								annoElement = new LineAnnotation(); 
								break;
							case "polygon": 
								annoElement = new PolygonAnnotation(); 
								break;
							case "rect": 
								annoElement = new RectAnnotation(); 
								break;
							case "square": 
								annoElement = new SquareAnnotation(); 
								break;
							case "text": 
								annoElement = new TextAnnotation(); 
								break;
							default: 
								break;
						}

						if (annoElement != null)
						{
							((IJsonRestore)annoElement).RestoreStateFromJson(item);
							this.Annotations.Add(annoElement);
						}
					}
				}
			}
		}


		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified
		/// System.Web.UI.HtmlTextWriterTag.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		///</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			string hiddenClass = "";
			if (!this.IsDesignMode)
			{
				hiddenClass = Utils.GetHiddenClass();
			}
			this.CssClass = string.Format("{0} {1}", hiddenClass, cssClass);

			base.AddAttributesToRender(writer);

			this.CssClass = cssClass;
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement("adjustOptions");
		}

        /// <summary>
        /// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            IEnumerable<ScriptDescriptor> sds = base.GetScriptDescriptors();

            foreach (WidgetDescriptor wsd in sds)
            {
                wsd.Options = wsd.Options.Replace("\"trendlineSeries\":", "");
            }

            return sds;
        }

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for all chart controls.
		/// </summary>
		/// <param name="postDataKey">
		/// The key identifier for the chart control.
		/// </param>
		/// <param name="postCollection">
		/// The collection of all incoming name values.
		/// </param>
		/// <returns>
		/// Returns true if the server control's state changes as a result of the postback;
		/// otherwise,false.
		/// </returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			this.RestoreStateFromJson(data);

			return false;
		}

		/// <summary>
		/// Notify the ASP.NET application that the state of the chart control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations
	}
}
