﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base.Collections;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
	/// <summary>
	/// Represents a collection of <see cref="C1ComboBoxItem"/> objects.
	/// </summary>
	public class C1ComboBoxItemCollection : C1ObservableItemCollection<C1ComboBox, C1ComboBoxItem>
	{

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ComboBoxItemCollection"/> class..
		/// </summary>
		/// <param name="owner">Parent ComboBox.</param>
		public C1ComboBoxItemCollection(C1ComboBox owner)
			: base(owner)
		{
		}
		#endregion end of ** constructor

		#region ** properties
		/// <summary>
		/// Gets value that indicates if the collection is empty or not.
		/// If collection is empty than it will be skipped during the serialization process.
		/// </summary>
		/// <example>
		/// <code lang="C#"> 
		/// C1ComboBox comboBox = new C1ComboBox();
		/// bool b = comboBox.Items.IsEmpty;
		/// // b == true;
		/// </code>
		/// </example>
		public bool IsEmpty
		{
			get
			{
				return (Count <= 0);
			}
		}

		/// <summary>
		/// Gets or sets the selected indices of <see cref="C1ComboBox"/>.
		/// </summary>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// c.Items.Add(new C1ComboBoxItem("test1"));
		/// c.Items.Add(new C1ComboBoxItem("test2"));
		/// c.SelectionMode = C1ComboBoxSelectionMode.Multiple;
		/// ArrayList indicesToSelect = new ArrayList();
		/// indicesToSelect.Add(0);
		/// indicesToSelect.Add(1);
		/// c.SelectedIndices = indicesToSelect;
		/// </code>
		/// </example>
		public ArrayList SelectedIndices
		{
			get
			{
				ArrayList indices = new ArrayList();
				for (int i = 0; i < Count; i++)
				{
					C1ComboBoxItem item = Items[i]; 
					if (item.Selected)
					{
						indices.Add(i);
					}
				}
				return indices;
			}
			set
			{
				InternalClearSelection();
				for (int i = 0; i < value.Count; i++ )
				{
					if (value[i] != null)
					{
						int index = (int)value[i];
						if (index >= 0 && index <= (Items.Count - 1))
						{
							C1ComboBoxItem item = Items[index];
							item.Selected = true;
						}
					}
				}
			}
		}
		#endregion end of ** properties

		#region ** methods

		/// <summary>
		/// Removes all <see cref="C1ComboBoxItem"/>s from the collection.
		/// </summary>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// c.Items.Add(new C1ComboBoxItem("test1"));
		/// c.Items.Add(new C1ComboBoxItem("test2"));
		/// // items are clear.
		/// c.Items.Clear();
		/// </code>
		/// </example>
		public new void Clear()
		{
            //Owner.Controls.Clear();
			base.ClearItems();
		}

		/// <summary>
		/// Adds a new <see cref="C1ComboBoxItem"/> to the end of the list.
		/// </summary>
		/// <param name="child">Item to add.</param>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// c.Items.Add(new C1ComboBoxItem("test1"));
		/// c.Items.Add(new C1ComboBoxItem("test2"));
		/// </code>
		/// </example>
		public new void Add(C1ComboBoxItem child)
		{
			if (child != null)
			{
				Insert(Count, child);
			}
		}

		/// <summary>
		/// Inserts a <see cref="C1ComboBoxItem"/> at the specified position in the collection.
		/// </summary>
		/// <param name="index">Position where the C1ComboBoxItem will be added. The value should be greater than or equal to 0.</param>
		/// <param name="child"><see cref="C1ComboBoxItem"/> instance.</param>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// c.Items.Add(new C1ComboBoxItem("test1"));
		/// c.Items.Insert(0,new C1ComboBoxItem("test2"));
		/// </code>
		/// </example>
		public new void Insert(int index, C1ComboBoxItem child)
		{
			if (Count == 0)
			{
				InsertItem(0, child);
			}
			else
			{
				InsertItem(index, child);
			}
			child.Owner = this.Owner;
		}

		/// <summary>
		/// Removes the specified <see cref="C1ComboBoxItem "/> from the list.
		/// </summary>
		/// <param name="child"><see cref="C1ComboBoxItem "/> to remove.</param>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// C1ComboBoxItem item = new C1ComboBoxItem("test1")
		/// c.Items.Add(item);
		/// c.Items.Remove(item);
		/// </code>
		/// </example>
		public new void Remove(C1ComboBoxItem child)
		{
			RemoveAt(IndexOf(child));
		}

		/// <summary>
		/// Removes the <see cref="C1ComboBoxItem"/> at the specified index from the list.
		/// </summary>
		/// <param name="index">Index of <see cref="C1ComboBoxItem"/> to remove.</param>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// c.Items.Add(new C1ComboBoxItem("test1"));
		/// c.Items.RemoveAt(0);
		/// </code>
		/// </example>
		public new void RemoveAt(int index)
		{
			RemoveItem(index);
		}

		/// <summary>
		/// InternalClearSelection
		/// </summary>
		internal void InternalClearSelection()
		{
			foreach (C1ComboBoxItem list in Items)
			{
				list.Selected = false;
			}
		}

		/// <summary>
		/// Gets selected items.
		/// </summary>
		/// <returns></returns>
		internal C1ComboBoxItem[] GetSelectedItems()
		{
			C1ComboBoxItem[] items = new C1ComboBoxItem[] { };
			foreach (C1ComboBoxItem i in Items)
			{
				if (i.Selected)
				{
					Array.Resize(ref items, items.Length + 1);
					items[items.Length - 1] = i;
				}
			}
			return items;
		}

		/// <summary>
		/// Sort
		/// </summary>
		internal void Sort(IComparer comparer)
		{
			ArrayList list = new ArrayList(Items.Count);

			foreach (C1ComboBoxItem controlItem in Items)
			{
				list.Add(controlItem);
			}
			if (comparer == null)
			{
				list.Sort();
			}
			else
			{
				list.Sort(comparer);
			}

			Clear();
			foreach (C1ComboBoxItem item in list)
			{
				Items.Add(item);
			}
		}

		/// <summary>
		/// Sort with default comparer.
		/// </summary>
		internal void Sort()
		{
			Sort(null);
		}

		/// <summary>
		/// Froms the json.
		/// </summary>
		/// <param name="arr">The al.</param>
		internal void FromJson(ArrayList arr)
		{
			ArrayList list = this.CreateCollectionFromJson(arr);
			// already have inline items.
			if (Items.Count > 0)
			{
				int count = Items.Count;
				int count2 = list.Count;
				if (list.Count > 0)
				{
					for (int i = 0; i < count; i++)
					{
						for (int j = 0; j < count2; j++)
						{
							C1ComboBoxItem c1ComboBoxItem = (C1ComboBoxItem)list[j];
							if (Items[i].StaticKey != null && c1ComboBoxItem.StaticKey != null &&
								Items[i].StaticKey == c1ComboBoxItem.StaticKey)
							{
								this.AssignPropToItem(Items[i], (Hashtable)arr[j]);
								list[j] = Items[i];
							}
						}
					}
				}
				Items.Clear();
				foreach (C1ComboBoxItem item in list)
				{
					item.Owner = this.Owner;
					Items.Add(item);
				}
			}
			else
			{
				foreach (C1ComboBoxItem item in list)
				{
					item.Owner = this.Owner;
					Items.Add(item);
				}
			}
		}

		private ArrayList CreateCollectionFromJson(ArrayList list)
		{
			ArrayList items = new ArrayList();
			if (list == null)
			{
				return items;
			}
			foreach (Hashtable hashtable in list)
			{
				C1ComboBoxItem item = new C1ComboBoxItem();
				AssignPropToItem(item, hashtable);
				items.Add(item);
			}
			return items;
		}

		private void AssignPropToItem(C1ComboBoxItem item,Hashtable ht)
		{
			if (item == null)
			{
				return ;
			}
			
			if (ht["label"] != null)
			{
				item.Text = ht["label"].ToString();
			}
			if (ht["value"] != null)
			{
				item.Value = ht["value"].ToString();
			}
			if (ht["selected"] != null)
			{
				item.Selected = (bool)ht["selected"];
			}
            if (ht["title"] != null)
            {
                item.ToolTip = ht["title"].ToString();
            }
			if (ht["cells"] != null)
			{
				ArrayList cells = ht["cells"] as ArrayList;
				List<string> list = new List<string>();
				for (int k = 0; k < cells.Count; k++)
				{
					list.Add(cells[k].ToString());
				}
				item.Cells = list.ToArray();
			}
			// StaticKey
			if (ht["staticKey"] is string)
			{
				item.StaticKey = ht["staticKey"] as string;
			}
		}
		#endregion end of ** methods

	}
}
