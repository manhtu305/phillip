﻿using System.Collections;
using System.Collections.ObjectModel;
using C1.Web.Wijmo.Controls.Base.Collections;

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
	/// <summary>
	/// Represents C1ComboColumn collection.
	/// </summary>
	public class C1ComboColumnCollection : C1ObservableItemCollection<C1ComboBox, C1ComboBoxColumn>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ComboColumnCollection"/> class.
		/// </summary>
		/// <param name="owner"></param>
		public C1ComboColumnCollection(C1ComboBox owner) : base(owner)
		{
		}

		/// <summary>
		/// Froms the json.
		/// </summary>
		/// <param name="arr">The al.</param>
		internal void FromJson(ArrayList arr)
		{
			this.Clear();
			for (int i = 0; i < arr.Count; i++)
			{
				Hashtable ht = arr[i] as Hashtable;
				C1ComboBoxColumn col = new C1ComboBoxColumn();
				if (ht["name"] != null)
				{
					col.Name = ht["name"].ToString();
				}
				if (ht["width"] != null)
				{
					col.Width = (int)ht["width"];
				}
                if (ht["dataField"] != null)
                {
                    col.DataField = ht["dataField"].ToString();
                }

                this.Add(col);
			}
		}
	}
}
