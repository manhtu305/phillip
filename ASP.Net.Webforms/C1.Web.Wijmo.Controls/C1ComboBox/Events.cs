﻿#region using

using System;

#endregion end of using

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
	///<summary>
	/// Represents the method that will handle <see cref="C1ComboBox.SelectedIndexChanged"/> event.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">An <see cref="C1ComboBoxEventArgs"/> that contains event data.</param>
	public delegate void C1ComboBoxEventHandler(object sender, C1ComboBoxEventArgs args);

	///<summary>
	/// Represents the method that will handle an data binding events of <see cref="C1ComboBox"/>.
	/// Such as <see cref="C1ComboBox.BoundItemCreated"/> event and <see cref="C1ComboBox.ItemDataBinding"/> event.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">An <see cref="C1ComboBoxItemEventArgs"/> that contains event data.</param>
	public delegate void C1ComboBoxItemEventHandler(object sender, C1ComboBoxItemEventArgs args);

	/// <summary>
	/// Contains event data for data binding events.
	/// Such as <see cref="C1ComboBox.BoundItemCreated"/> event and <see cref="C1ComboBox.ItemDataBinding"/> event.
	/// </summary>
	public class C1ComboBoxItemEventArgs : EventArgs
	{
		private C1ComboBoxItem _item;

		/// <summary>
		/// Gets the <see cref="C1ComboBoxItem"/> related with this event.
		/// </summary>
		public C1ComboBoxItem Item
		{
			get
			{
				return this._item;
			}
		}

		/// <summary>
		/// Initializes a new instance of the C1ComboBox class.
		/// </summary>
		/// <param name="item"><see cref="C1ComboBoxItem"/> related with this event</param>
		public C1ComboBoxItemEventArgs(C1ComboBoxItem item)
		{
			this._item = item;
		}
	}

	///<summary>
	/// Contains event data for <see cref="C1ComboBox.SelectedIndexChanged"/> event and <see cref="C1ComboBox.TextChanged"/> event.
	///</summary>
	public class C1ComboBoxEventArgs : EventArgs
	{

		#region ** fields
		private int _new;
		private int _old; 
		#endregion end of ** fields

		internal C1ComboBoxEventArgs(int oldIndex, int newIndex)
		{
			this._old = oldIndex; 
			this._new = newIndex;
		}


		#region ** properties
		/// <summary>
		/// Gets the new SelectedIndex.
		/// </summary>
		public int NewSelectedIndex
		{
			get
			{
				return _new;
			}
		}

		/// <summary>
		/// Gets the old SelectedIndex.
		/// </summary>
		public int OldSelectedIndex
		{
			get
			{
				return _old;
			}
			 
		} 
		#endregion end of ** properties

	}

	///<summary>
	/// Represents the method that will handle <see cref="C1ComboBox.ItemPopulate"/> event.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">An <see cref="C1ComboBoxItemPopulateEventArgs"/> that contains event data.</param>
	public delegate void C1ComboBoxItemPopulateEventHandler(object sender, C1ComboBoxItemPopulateEventArgs args);

	///<summary>
	/// Contains event data for <see cref="C1ComboBox.ItemPopulate"/> event.
	///</summary>
	public class C1ComboBoxItemPopulateEventArgs : EventArgs
	{
		private int _requestedItemCount = 0;
		private bool _endRequest = false;

		public C1ComboBoxItemPopulateEventArgs(int requestedItemCount)
		{
			this._requestedItemCount = requestedItemCount;
		}
		
		/// <summary>
		/// Gets the number of client items.
		/// </summary>
		public int RequestedItemCount
		{
			get { return _requestedItemCount; }
		}
		
		///<summary>
		/// Gets or sets a Boolean value indicating whether client side has rich the end of all items.
		///</summary>
		public bool EndRequest
		{
			get { return _endRequest; }
			set { _endRequest = value; }
		}

		//update for adding text property by wuhao at 2011/8/10
		/// <summary>
		/// Gets or sets the FilterText.
		/// </summary>
		public string FilterText
		{ get; set; }
	}

	///<summary>
	/// Represents the method that will handle <see cref="C1ComboBox.CallbackDataBind"/> event.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">An <see cref="C1ComboBoxCallbackEventArgs"/> that contains event data.</param>
	public delegate void C1ComboBoxCallbackEventHandler(object sender, C1ComboBoxCallbackEventArgs args);

	///<summary>
	/// Contains event data for <see cref="C1ComboBox.CallbackDataBind"/> event.
	///</summary>
	public class C1ComboBoxCallbackEventArgs : EventArgs
	{
		private object _dataSource;
		private int _pageSize = 10;

		/// <summary>
		///  Bind the DataSource to C1ComboBox in Callback mode.
		/// </summary>
		public object DataSource
		{
			get { return _dataSource; }
			set { _dataSource = value; }
		}

		/// <summary>
		/// Gets or sets the PageSize for DataSource.
		/// </summary>
		public int PageSize
		{
			get { return _pageSize; }
			set { _pageSize = value; }
		}

		//update for adding text property by wuhao at 2011/8/10
		/// <summary>
		/// Gets or sets the FilterText.
		/// </summary>
		public string FilterText
		{ get; set; }

	}
}
