﻿using System.ComponentModel.Design;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
    /// <summary>
    /// Represents C1ComboBoxSerializer which will serialize C1ComboBox.
    /// </summary>
    public class C1ComboBoxSerializer : C1BaseSerializer<C1ComboBox, object, object>
    {
        /// <summary>
        /// Initializes a new instance of the C1ComboBoxSerializer class..
        /// </summary>
        /// <param name="serializableObject">Object to serialize.</param>
        public C1ComboBoxSerializer(object serializableObject) : base(serializableObject)
        {
        }

        /// <summary>
        /// Initializes a new instance of the C1ComboBoxSerializer class.
        /// </summary>
        /// <param name="componentChangeService">IComponentChangeService</param>
        /// <param name="serializableObject">Object to serialize.</param>
        public C1ComboBoxSerializer(IComponentChangeService componentChangeService, object serializableObject)
            : base(componentChangeService, serializableObject)
        {
        }
    }
}