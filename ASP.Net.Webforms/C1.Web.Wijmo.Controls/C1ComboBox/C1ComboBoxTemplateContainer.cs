﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
    /// <summary>
    /// Template container for C1ComboBox.
    /// </summary>
	[ToolboxItem(false)]
    public class C1ComboBoxTemplateContainer : WebControl, IDataItemContainer
    {
        private C1ComboBox _parentComboBox;
        private C1ComboBoxItem _item;
        private object _dataItem;
        private int _dataItemIndex;

        /// <summary>
        /// Initializes a new instance of the C1ComboBoxTemplateContainer class.
        /// </summary>
        internal C1ComboBoxTemplateContainer()
        {
            
        }
        /// <summary>
        /// Initializes a new instance of the C1ComboBoxTemplateContainer class.
        /// </summary>
        /// <param name="itemIndex">Index of current item.</param>
        /// <param name="dataItem">Related data item.</param>
        internal C1ComboBoxTemplateContainer(int itemIndex, object dataItem)
        {
            _dataItemIndex = itemIndex;
            _dataItem = dataItem;
        }

        /// <summary>
        /// Parent ComboBox instance.
        /// </summary>
        public C1ComboBox ParentComboBox
        {
            get
            {
                return _parentComboBox;
            }
            set
            {
                _parentComboBox = value;
            }
        }

        /// <summary>
        /// C1ComboBoxItem with this template container.
        /// </summary>
        public C1ComboBoxItem ComboBoxItem
        {
            get
            {
                return _item;
            }
            set
            {
                _item = value;
            }
        }

        /// <summary>
        /// Gets the value that corresponds to this Web server control.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

         /// <summary>
        /// Gets an object that is used in simplified data-binding operations.
        /// </summary>
        [Browsable(false)]
        public object DataItem
        {
            get { return _dataItem; }
        }

        /// <summary>
        /// Gets the index of the data item.
        /// </summary>
        [Browsable(false)]
        public int DataItemIndex
        {
            get { return _dataItemIndex; }
        }

        /// <summary>
        ///  Gets the display order of the C1ComboBoxItem relative to the currently displayed C1ComboBoxItems.
        /// </summary>
        [Browsable(false)]
        public int DisplayIndex
        {
            get
            {
                return this.DataItemIndex;
            }
        }
    }
}
