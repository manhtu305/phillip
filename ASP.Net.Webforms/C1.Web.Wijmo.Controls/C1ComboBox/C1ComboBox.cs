﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;
using System.Drawing;
using System.IO;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Web.UI.HtmlControls;
using System.Web.UI.Design;
using System.Collections;
using System.Data;
using System.Collections.Specialized;
using System.Reflection;

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
    /// <summary>
    /// Represents a ComboBox in an ASP.NET Web page.
    /// </summary>
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1ComboBox.C1ComboBoxDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1ComboBox.C1ComboBoxDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1ComboBox.C1ComboBoxDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1ComboBox.C1ComboBoxDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1ComboBox runat=server></{0}:C1ComboBox>")]
    [ToolboxBitmap(typeof(C1ComboBox), "C1ComboBox.png")]
    [ControlValueProperty("SelectedValue")]
    [ValidationProperty("Text")]
    [LicenseProviderAttribute()]
    public partial class C1ComboBox : C1TargetDataBoundControlBase, IC1Serializable, ICallbackEventHandler, IPostBackEventHandler, IPostBackDataHandler
    {
        #region field
        private HtmlGenericControl _templateUL;
        private HtmlGenericControl _templateULContainer;
        private HtmlInputText _templateTextBox;
        private int cachedSelectedIndex = -1;
        private string cachedSelectedValue;
        private bool isDeserializing = false;
        private ArrayList cachedSelectedIndices;

        #endregion

        #region ** events

        /// <summary>
        /// Occurs when selected index of C1ComboBox is changed(with postback).
        /// </summary>
        [C1Category("Category.Misc")]
        [C1Description("C1ComboBox.SelectedIndexChanged")]
        public event C1ComboBoxEventHandler SelectedIndexChanged;
        private void OnSelectedIndexChanged(C1ComboBoxEventArgs e)
        {
            if (SelectedIndexChanged != null)
            {
                SelectedIndexChanged(this, e);
            }
        }

        /// <summary>
        /// Occurs when C1ComboBox requests data from client side.
        /// </summary>
        [C1Category("Category.Misc")]
        [C1Description("C1ComboBox.ItemPopulate")]
        public event C1ComboBoxItemPopulateEventHandler ItemPopulate;
        private void OnItemPopulate(C1ComboBoxItemPopulateEventArgs e)
        {
            if (ItemPopulate != null)
            {
                ItemPopulate(this, e);
            }
        }

        /// <summary>
        /// Occurs when C1ComboBox requests data from data scource.
        /// </summary>
        [C1Category("Category.Misc")]
        [C1Description("C1ComboBox.CallbackDataBind")]
        public event C1ComboBoxCallbackEventHandler CallbackDataBind;
        private void OnCallbackDataBind(C1ComboBoxCallbackEventArgs e)
        {
            if (CallbackDataBind != null)
            {
                CallbackDataBind(this, e);
            }
        }

        ///<summary>
        /// Occurs after <see cref="C1ComboBoxItem"/> is created in data binding.
        ///</summary>
        [C1Category("Category.Misc")]
        [C1Description("C1ComboBox.BoundItemCreated")]
        public event C1ComboBoxItemEventHandler BoundItemCreated;

        private void OnBoundItemCreated(C1ComboBoxItemEventArgs e)
        {
            if (BoundItemCreated != null)
            {
                BoundItemCreated(this, e);
            }
        }

        /// <summary>
        /// Occurs when <see cref="C1ComboBoxItem"/> is binding data.
        /// </summary>
        [C1Category("Category.Misc")]
        [C1Description("C1ComboBox.ItemDataBinding")]
        public event C1ComboBoxItemEventHandler ItemDataBinding;

        private void OnItemDataBinding(C1ComboBoxItemEventArgs e)
        {
            if (ItemDataBinding != null)
            {
                ItemDataBinding(this, e);
            }
        }

        #endregion

        #region ** Fields
        private bool _productLicensed;
        private bool _shouldNag;

        private C1ComboColumnCollection _columns;
        private C1ComboBoxItemCollection _items;
        private ITemplate _itemTemplate;
        #endregion

        #region ** constructor
        ///<summary>
        /// Initializes a new instance of the <seealso cref="C1ComboBox"/> class.
        ///</summary>
        public C1ComboBox()
        {
            VerifyLicense();

            base.Width = 160;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ComboBox"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public C1ComboBox(string key)
        {
#if GRAPECITY
			_productLicensed = true;
#else
            // ttZzKzJ5mwNr/IihpBl2pA==
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ComboBox), this,
                Assembly.GetExecutingAssembly(), key);
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
            base.Width = 160;
        }

        internal virtual void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ComboBox), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }
        #endregion end of ** constructor

        #region ** Properties
        ///<summary>
        /// Gets or sets a value that determines whether to enable callback to load <see cref="C1ComboBoxItem"/>s on demand.
        ///</summary>
        /// <example>
        /// <code>
        /// <![CDATA[<cc1:C1ComboBox ID="c1ComboBox2" runat="server" EnableCallBackMode="true"></cc1:C1ComboBox>]]>
        /// </code>
        /// </example>
        [C1Description("C1ComboBox.EnableCallBackMode")]
        [DefaultValue(false)]
        [Bindable(false)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption()]
        public bool EnableCallBackMode
        {
            get
            {
                return this.GetPropertyValue<bool>("EnableCallBackMode", false);
            }
            set
            {
                this.SetPropertyValue<bool>("EnableCallBackMode", value);
            }
        }

        /// <summary>
        ///  Gets or sets a value that determines which means to populate data for c1combobox(without posting back).
        /// </summary>
        [C1Description("C1ComboBox.CallbackDataPopulate")]
        [C1Category("Category.Behavior")]
        [DefaultValue(CallbackDataPopulate.DataBind)]
        [Layout(LayoutType.Behavior)]
        [WidgetOption()]
        public CallbackDataPopulate CallbackDataPopulate
        {
            get
            {
                return this.GetPropertyValue<CallbackDataPopulate>("CallbackDataPopulate", CallbackDataPopulate.DataBind);
            }
            set
            {
                this.SetPropertyValue<CallbackDataPopulate>("CallbackDataPopulate", value);
            }
        }

        /// <summary>
        /// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
        /// </summary>
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        [C1Description("C1ComboBox.AutoPostBack", "Indicates whether or not the control posts back to the server each time a user interacts with the control.")]
        [WidgetOption()]
        public bool AutoPostBack
        {
            get
            {
                return this.GetPropertyValue<bool>("AutoPostBack", false);
            }
            set
            {
                this.SetPropertyValue<bool>("AutoPostBack", value);
            }
        }

        ///<summary>
        /// Gets or sets a value determines whether to append bound items for <see cref="C1ComboBox"/>.
        ///</summary>
        /// <example>
        /// <code>
        /// <![CDATA[<cc1:C1ComboBox ID="c1ComboBox2" runat="server" AppendDataBoundItems="true"></cc1:C1ComboBox>]]>
        /// </code>
        /// </example>
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        [DefaultValue(false)]
        [C1Description("C1ComboBox.AppendDataBoundItems")]
        [Bindable(false)]
        public bool AppendDataBoundItems
        {
            get
            {
                return this.GetPropertyValue<bool>("AppendDataBoundItems", false);
            }
            set
            {
                this.SetPropertyValue<bool>("AppendDataBoundItems", value);
            }
        }

        ///<summary>
        /// The formatting applied to the text field. For example,"{0:d}".
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.DataSource = data;
        /// c.DataTextField = "textField";
        /// c.DataValueField = "valueField";
        /// c.DataBind();
        /// </code>
        /// </example>
        [DefaultValue("")]
        [C1Description("C1ComboBox.DataTextFormatString")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataTextFormatString
        {
            get
            {
                return this.GetPropertyValue<string>("DataTextFormatString", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataTextFormatString", value);
            }
        }

        ///<summary>
        /// Gets or sets the field in the data source from which to load text values.
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.DataSource = data;
        /// c.DataTextField = "textField";
        /// c.DataValueField = "valueField";
        /// c.DataBind();
        /// </code>
        /// </example>
        [DefaultValue("")]
        [C1Description("C1ComboBox.DataTextField")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataTextField
        {
            get
            {
                return this.GetPropertyValue<string>("DataTextField", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataTextField", value);
            }
        }

        ///<summary>
        /// Gets or sets the field in the data source from which to load item values.
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.DataSource = data;
        /// c.DataTextField = "textField";
        /// c.DataValueField = "valueField";
        /// c.DataBind();
        /// </code>
        /// </example>
        [DefaultValue("")]
        [C1Description("C1ComboBox.DataValueField")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataValueField
        {
            get
            {
                return this.GetPropertyValue<string>("DataValueField", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataValueField", value);
            }
        }

        ///<summary>
        /// Gets or sets the field in the data source from which to load item state.
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.DataSource = data;
        /// c.DataTextField = "textField";
        /// c.DataValueField = "valueField";
        /// c.DataSelectedField = "selectedField";
        /// c.DataBind();
        /// </code>
        /// </example>
        [DefaultValue("")]
        [C1Description("C1ComboBox.DataSelectedField")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataSelectedField
        {
            get
            {
                return this.GetPropertyValue<string>("DataSelectedField", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataSelectedField", value);
            }
        }

        //Add Json attribute by wuhao at 2011/10/9
        //Note:C1Combobox will resize when postback occurs
        /// <summary>
        /// Gets or sets the width of the combobox.
        /// </summary>
        [Layout(LayoutType.Sizes)]
        [Json(true)]
        public override Unit Width
        {
            get
            {
                return base.Width;
            }
            set
            {
                if (base.Width != value)
                {
                    base.Width = value;
                    if (this.IsDesignMode)
                    {
                        this.ChildControlsCreated = false;
                    }
                }
            }
        }

        //Add Json attribute by wuhao at 2012/12/3
        //Note:C1Combobox will resize when postback occurs
        /// <summary>
        /// Gets or sets the Height of the combobox.
        /// </summary>
        [Layout(LayoutType.Sizes)]
        [Json(true)]
        public override Unit Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                if (base.Height != value)
                {
                    base.Height = value;
                    if (this.IsDesignMode)
                    {
                        this.ChildControlsCreated = false;
                    }
                }
            }
        }

        /// <summary>
        /// Collection of columns in the combobox.
        /// </summary>
        [C1Description("C1ComboBox.Columns")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
        [Browsable(false)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public C1ComboColumnCollection Columns
        {
            get
            {
                if (_columns == null)
                {
                    _columns = new C1ComboColumnCollection(this);
                }
                return _columns;
            }
        }

        //update for adding text property by wuhao at 2011/8/10
        /// <summary>
        /// Text in the combobox.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1ComboBox.Text")]
        [C1Category("Category.Data")]
        [Browsable(true)]
        [Layout(LayoutType.Data)]
        [WidgetOption]
        public string Text
        {
            get
            {
                return GetPropertyValue<string>("Text", string.Empty);
            }
            set
            {
                SetPropertyValue<string>("Text", value);
                //update for fixing 19934 by wh at 2012/2/16
                if (this.SelectionMode == Wijmo.Controls.SelectionMode.Single && !isDeserializing)
                {
                    if (Items.Count > 0)
                    {
                        if (value != null && this.SelectedIndex > 0 &&
                            value.Equals(Items[this.SelectedIndex].Text))
                        {
                            return;
                        }

                        //SetPropertyValue("SelectedIndex", -1);
                        this.SelectedIndex = -1;
                        for (int i = 0; i < Items.Count; i++)
                        {
                            if (value != null && value.Equals(Items[i].Text))
                            {
                                //SetPropertyValue("SelectedIndex", i);
                                this.SelectedIndex = i;
                                return;
                            }
                        }
                    }
                }
                //end for fixing 19934
            }
        }

        // <summary>
        //     Gets or sets an optional argument.
        //</summary>
        [DefaultValue("")]
        [C1Description("C1ComboBox.CommandArgument")]
        [Browsable(true)]
        [Json(true)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public string CommandArgument
        {
            get
            {
                return GetPropertyValue<string>("CommandArgument", string.Empty);
            }
            set
            {
                SetPropertyValue<string>("CommandArgument", value);
            }
        }

        // <summary>
        //     Gets or sets the command name.
        //</summary>
        [DefaultValue("")]
        [C1Description("C1ComboBox.CommandName")]
        [Browsable(true)]
        [Json(true)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public string CommandName
        {
            get
            {
                return GetPropertyValue<string>("CommandName", string.Empty);
            }
            set
            {
                SetPropertyValue<string>("CommandName", value);
            }
        }

        ///<summary>
        /// Gets a value indicating whether C1ComboBox uses an item template.
        ///</summary>
        [Browsable(false)]
        [DefaultValue(false)]
        //[WidgetOption]
        internal bool IsTemplated
        {
            get
            {
                return ItemsTemplate != null;
            }
        }

        /// <summary>
        /// Collection of child items in the combobox.
        /// </summary>
        [C1Description("C1ComboBox.Items")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
        [Browsable(false)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        [WidgetOptionName("data")]
        [CollectionItemType(typeof(C1ComboBoxItem))]
        public C1ComboBoxItemCollection Items
        {
            get
            {
                if (_items == null)
                    _items = new C1ComboBoxItemCollection(this);
                return _items;
            }
        }

        /// <summary>
        /// Gets or sets the template of the items.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(C1ComboBoxTemplateContainer))]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Browsable(false)]
        [Bindable(false)]
        public virtual ITemplate ItemsTemplate
        {
            get
            {
                return _itemTemplate;
            }
            set
            {
                _itemTemplate = value;
            }
        }

        ///<summary>
        /// Gets the SelectedItem of <see cref="C1ComboBox"/>.
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.Items.Add(new C1ComboBoxItem("test1"));
        /// c.Items.Add(new C1ComboBoxItem("test2"));
        /// c.SelectedIndex = 1;
        /// // item will refer to test2.
        /// C1ComboBoxItem item = c.SelectedItem;
        /// </code>
        /// </example>
        [DefaultValue(null)]
        [Bindable(false)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual C1ComboBoxItem SelectedItem
        {
            get
            {
                return SelectedItems.Length > 0 ? SelectedItems[0] : null;
            }
        }

        //update for 20174: get selectedvalue option by wh at 2012/2/19
        ///<summary>
        /// Gets or sets the SelectedValue of <see cref="C1ComboBox"/>.
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.Items.Add(new C1ComboBoxItem("test1"));
        /// c.Items.Add(new C1ComboBoxItem("test2"));
        /// // select the test2 item.
        /// c.SelectedValue = "test2";
        /// </code>
        /// </example>
        [WidgetOption]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        [Bindable(true)]
        public virtual string SelectedValue
        {
            get
            {
                int selectedIndex = this.SelectedIndex;
                if (selectedIndex >= 0)
                {
                    return this.Items[selectedIndex].Value;
                }
                return String.Empty;
            }
            set
            {
                if (!isDeserializing)
                {
                    if (this.Items.Count != 0)
                    {
                        int index = FindItemIndexByValue(value);
                        if (this.SelectionMode == Wijmo.Controls.SelectionMode.Single)
                        {
                            this.SelectedIndex = index;
                        }
                        else
                        {
                            this.SelectedIndices.Add(index);
                        }
                    }
                    cachedSelectedValue = value;
                }
            }
        }
        //end for get selectedValue option

        ///<summary>
        /// Gets the SelectedItems of <see cref="C1ComboBox"/>.
        ///</summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.Items.Add(new C1ComboBoxItem("test1"));
        /// c.Items.Add(new C1ComboBoxItem("test2"));
        /// c.SelectionMode = C1ComboBoxSelectionMode.Multiple;
        /// ArrayList indicesToSelect = new ArrayList();
        /// indicesToSelect.Add(0);
        /// indicesToSelect.Add(1);
        /// // select 2 items.
        /// c.SelectedIndices = indicesToSelect;
        /// C1ComboBoxItem[] items = new C1ComboBoxItem[2];
        /// items = c.SelectedItems;
        /// </code>
        /// </example>
        [DefaultValue(null)]
        [Bindable(false)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual C1ComboBoxItem[] SelectedItems
        {
            get
            {
                return Items.GetSelectedItems();
            }
        }

        //update for issue 20253 by wh at 2012/3/1
        /// <summary>
        /// A value that specifies the index of the item to select when using single mode.
        /// If the selectionMode is "multiple", then this option could be set 
        /// to an array of Number which contains the indices of the items to select.
        /// Default: -1.
        /// Type: Number/Array.
        /// </summary>
        /// <remarks>
        /// If no item is selected, it will return -1.
        /// </remarks>
        [Browsable(false)]
        [C1Description("C1ComboBox.SelectedIndex")]
        [C1Category("Category.Behavior")]
        [DefaultValue(-1)]
        [Layout(LayoutType.Behavior)]
        [Bindable(true)]
        public int SelectedIndex
        {
            get
            {
                if (SelectedIndices.Count == 0)
                    return -1;
                else
                    return (int)SelectedIndices[0];
            }
            set
            {
                if (!isDeserializing)
                {
                    //if (this.SelectionMode == SelectionMode.Single) 
                    //{
                    ArrayList list = new ArrayList();
                    if (value >= 0 && value <= (Items.Count - 1))
                    {
                        list.Add(value);
                    }
                    else
                    {
                        list.Add(-1);
                    }
                    SelectedIndices = list;

                    cachedSelectedIndex = value;
                    //}
                }
            }
        }
        //end for issue 20253 by wh at 2012/3/1

        /// <summary>
        /// Gets or sets the SelectedIndices of <see cref="C1ComboBox"/>.
        /// </summary>
        /// <example>
        /// <code lang="C#">
        /// C1ComboBox c = new C1ComboBox();
        /// c.Items.Add(new C1ComboBoxItem("test1"));
        /// c.Items.Add(new C1ComboBoxItem("test2"));
        /// c.SelectionMode = C1ComboBoxSelectionMode.Multiple;
        /// ArrayList indicesToSelect = new ArrayList();
        /// indicesToSelect.Add(0);
        /// indicesToSelect.Add(1);
        /// // select 2 items.
        /// c.SelectedIndices = indicesToSelect;
        /// </code>
        /// </example>
        [WidgetOption]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual ArrayList SelectedIndices
        {
            get
            {
                return Items.SelectedIndices;
            }
            set
            {
                Items.SelectedIndices = value;
                cachedSelectedIndices = value;
            }
        }

        /// <summary>
        /// Changed the type of  control's  tag.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                if (this.IsDesignMode || this.ItemsTemplate != null)
                {
                    return HtmlTextWriterTag.Div;
                }
                else
                {
                    return HtmlTextWriterTag.Input;
                }
            }
        }
        #endregion

        internal override bool IsWijMobileControl
        {
            get
            {
                return false;
            }
        }

        #region ** methods

        /// <summary>
        /// Registers an OnSubmit statement in order to save the states of the widget
        /// to json hidden input.
        /// </summary>
        protected override void RegisterOnSubmitStatement()
        {
            this.RegisterOnSubmitStatement("adjustOptions");
        }

        protected override void EnsureEnabledState()
        {
            return;
        }

        #region ** CreateChildControls override

        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based
        /// implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            if (base.RequiresDataBinding
            && (!string.IsNullOrEmpty(this.DataSourceID)
                || (this.DataSource != null)))
            {
                this.EnsureDataBound();
            }

            if (this.IsDesignMode)
            {
                this.Controls.Clear();
                this.Controls.Add(CreateWrapper());
                if (this.ShowTrigger)
                {
                    this.Controls.Add(CreateTrigger());
                }
            }
            else if (this.ItemsTemplate != null)
            {
                this.Controls.Add(TemplateTextBox);
                AddItemsToCombo();
            }
            else
            {
                base.CreateChildControls();
            }

        }

        private Control CreateWrapper()
        {
            Panel wrapper = new Panel();
            wrapper.CssClass = "wijmo-wijcombobox-wrapper ui-state-default ui-corner-all";
            wrapper.Style["height"] = "20px";

            HtmlInputText input = new HtmlInputText();
            input.Attributes["class"] = "wijmo-wijcombobox-input";
            //input.Style["margin-right"] = "20px";
            if (this.ShowTrigger && this.TriggerPosition == TriggerPosition.Right)
            {
                input.Style["margin-right"] = "20px";
            }
            else if (this.ShowTrigger && this.TriggerPosition == TriggerPosition.Left)
            {
                input.Style["margin-left"] = "20px";
            }
            //update for fixing bug 17328 at 2011/10/18 by wuhao
            //input.Style["width"] = (this.Width.Value - 20) + "px";
            if (this.Width.Type != UnitType.Percentage)
            {
                if (this.ShowTrigger)
                {
                    input.Style["width"] = (this.Width.Value - 20) + "px";
                }
                else
                {
                    input.Style["width"] = this.Width.Value + "px";
                }
            }
            else
            {
                input.Style["width"] = this.Width.ToString();
                wrapper.Style["width"] = this.Width.ToString();
            }
            //end for fixing bug 17328
            input.Style["height"] = "20px";
            input.Style["padding"] = "0px";
            wrapper.Controls.Add(input);

            return wrapper;
        }

        private Control CreateTrigger()
        {
            Panel trigger = new Panel();
            trigger.CssClass = "wijmo-wijcombobox-trigger ui-state-default ui-corner-right";
            trigger.Style["height"] = "20px";
            if (this.TriggerPosition == TriggerPosition.Right)
            {
                trigger.Style["right"] = "-2px";
            }
            else
            {
                trigger.Style["left"] = "-2px";
            }
            trigger.Style["display"] = "block";

            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "ui-icon ui-icon-triangle-1-s";
            span.Style["margin-left"] = "2px";
            span.Style["margin-top"] = "3px";
            trigger.Controls.Add(span);

            return trigger;
        }

        /// <summary>
        /// Adds HTML attributes and styles that need to be rendered to the specified <see cref="HtmlTextWriter"/>
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the server control content.</param>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            string cssClass = this.CssClass;
            if (this.IsDesignMode)
            {
                this.CssClass = string.Format("{0} {1}", "wijmo-wijcombobox ui-widget ui-helper-clearfix", cssClass);
            }
            else
            {
				this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass);
            }
            bool enabled = this.Enabled;
            this.Enabled = true;
            base.AddAttributesToRender(writer);
            this.CssClass = cssClass;
            this.Enabled = enabled;
        }

        /// <summary>
        /// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            // remove the items which set DisplayVisible property to false.
            for (int i = this.Items.Count - 1; i >= 0; i--)
            {
                if (!this.Items[i].DisplayVisible)
                {
                    this.Items.RemoveAt(i);
                }
            }

            base.OnPreRender(e);
            if (this.EnableCallBackMode)
            {
                Page.ClientScript.GetCallbackEventReference(this, "", "", "");
            }
            if (this.AutoPostBack)
            {
                Page.ClientScript.GetPostBackEventReference(this, "");
            }
        }

        /// <summary>
        /// Renders the control to the specified HTML writer.
        /// </summary>
        /// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            this.EnsureChildControls();

            this.Attributes.Add("value", this.Text);

            base.Render(writer);
        }

        #endregion end of ** methods

        #region template helper

        internal HtmlGenericControl TemplateUL
        {
            get
            {
                if (_templateUL == null)
                {
                    _templateUL = new HtmlGenericControl("ul");
                    _templateUL.ID = this.ID + "_List";
                    //_ul.Attributes.Add("class", "C1cmbList");
                    //_ul.Attributes.Add("style", "margin: 0pt; padding: 0pt; list-style-position: outside; list-style-type: none; list-style-image: none;overflow:hidden;");
                }
                return _templateUL;
            }
        }

        internal HtmlGenericControl TemplateULContainer
        {
            get
            {
                if (_templateULContainer == null)
                {
                    _templateULContainer = new HtmlGenericControl("div");
                    _templateULContainer.ID = this.ID + "_ListContainer";
                    //_ulContainer.Attributes.Add("class", "C1cmbList");
                    //_ul.Attributes.Add("style", "margin: 0pt; padding: 0pt; list-style-position: outside; list-style-type: none; list-style-image: none;overflow:hidden;");
                }
                return _templateULContainer;
            }
        }

        private void AddItemsToCombo()
        {
            if (Items.Count > 0)
            {
                foreach (C1ComboBoxItem item in Items)
                {
                    if (!TemplateUL.Controls.Contains(item) && item.DisplayVisible)
                    {
                        TemplateUL.Controls.Add(item);
                        item.EnsureChild();
                    }
                }
            }
            TemplateULContainer.Controls.Add(TemplateUL);
            this.Controls.Add(TemplateULContainer);
        }

        private HtmlInputText TemplateTextBox
        {
            get
            {
                if (_templateTextBox == null)
                {
                    _templateTextBox = new HtmlInputText();
                    _templateTextBox.Attributes.Add("class", string.Format("{0}", this.CssClass));
                    //_templateTextBox.Style.Add("width", this.Width.ToString());
                    //_templateTextBox.Style.Add("height", this.Height.ToString());
                    _templateTextBox.ID = this.ID + "_Input";
                }
                return _templateTextBox;
            }
        }
        #endregion

        private IEnumerable GetResolvedDataSource(object source)
        {
            if (source is IEnumerable)
                return (IEnumerable)source;
            else if (source is IList)
                return (IEnumerable)source;
            else if (source is DataSet)
                return (IEnumerable)(((DataSet)source).Tables[0].DefaultView);
            else if (source is DataTable)
                return (IEnumerable)(((DataTable)source).DefaultView);
            else
                return null;
        }

        /// <summary>
        /// When overridden in a derived class, binds data from the data source to the control.
        /// </summary>
        /// <param name="dataSource">The <see cref="T:System.Collections.IEnumerable"/> list of data returned from a <see cref="M:System.Web.UI.WebControls.DataBoundControl.PerformSelect"/> method call.</param>
        protected override void PerformDataBinding(IEnumerable data)
        {
            base.PerformDataBinding(data);

            if (DesignMode || data == null)
            {
                return;
            }

            if (!AppendDataBoundItems)
            {
                Items.Clear();
            }

            IEnumerator e = data.GetEnumerator();
            int idx = 0;
            bool hasTextField = !this.DataTextField.Trim().Equals(string.Empty);
            bool hasValueField = !this.DataValueField.Trim().Equals(string.Empty);
            bool hasSelectedField = !this.DataSelectedField.Trim().Equals(string.Empty);

            while (e.MoveNext())
            {
                object itemdata = e.Current;
                C1ComboBoxItem item = new C1ComboBoxItem(idx++, itemdata);
                if (hasTextField)
                {
                    item.Text = DataBinder.GetPropertyValue(itemdata, this.DataTextField.Trim(), this.DataTextFormatString).ToString();
                }
                else
                {
                    item.Text = itemdata.ToString();
                }
                if (hasValueField)
                {
                    item.Value = DataBinder.GetPropertyValue(itemdata, this.DataValueField.Trim()).ToString();
                }
                else
                {
                    item.Value = itemdata.ToString();
                }

                if (hasSelectedField)
                {
                    item.Selected = (bool)DataBinder.GetPropertyValue(itemdata, this.DataSelectedField.Trim());
                }
                else
                {
                    item.Selected = false;
                }

                List<string> cells = new List<string>();
                for (int k = 0; k < this.Columns.Count; k++)
                {
                    bool hasField = !this.Columns[k].DataField.Trim().Equals(string.Empty);
                    if (hasField)
                    {
                        cells.Add(DataBinder.GetPropertyValue(itemdata, this.Columns[k].DataField.Trim(), this.Columns[k].DataFormatString).ToString());
                    }
                }
                item.Cells = cells.ToArray();
                C1ComboBoxItemEventArgs arg = new C1ComboBoxItemEventArgs(item);
                OnItemDataBinding(arg);
                this.Items.Add(item);
                item.Owner = this;
                OnBoundItemCreated(new C1ComboBoxItemEventArgs(item));
            }

            if (cachedSelectedValue != null)
            {
                SelectedValue = cachedSelectedValue;
                cachedSelectedValue = null;
                cachedSelectedIndex = -1;
                cachedSelectedIndices = null;
            }
            else if (cachedSelectedIndex > -1)
            {
                SelectedIndex = cachedSelectedIndex;
                cachedSelectedValue = null;
                cachedSelectedIndex = -1;
                cachedSelectedIndices = null;
            }
            else if (cachedSelectedIndices != null && cachedSelectedIndices.Count > 0)
            {
                SelectedIndices = cachedSelectedIndices;
                cachedSelectedValue = null;
                cachedSelectedIndex = -1;
                cachedSelectedIndices = null;
            }
        }

        /// <summary>
        /// Load composite properties from JsonData.
        /// </summary>
        /// <param name="data">Hashtable</param>
        private void LoadCompositePropertiesFromJsonData(Hashtable data)
        {
            if (data["data"] != null)
            {
                ArrayList arr = data["data"] as ArrayList;
                this.Items.FromJson(arr);
            }
            if (data["columns"] != null)
            {
                ArrayList arr = data["columns"] as ArrayList;
                this.Columns.FromJson(arr);
            }
            if (data["pageIndex"] != null)
            {
                this._pageIndex = (int)data["pageIndex"];
            }
            if (data["requestItemCount"] != null)
            {
                this._requestItemCount = (int)data["requestItemCount"];
            }
            if (data["endRequest"] != null)
            {
                this._endRequest = (bool)data["endRequest"];
            }
            //update for adding text property by wuhao at 2011/8/10
            if (data["text"] != null)
            {
                this.Text = (string)data["text"];
            }
            // when use the default deserialize, it just clear the SelectedIndices property and add it, it will not set the property, 
            // so that the items's selected state will not changed.
            if (data["selectedIndices"] != null)
            {
                this.SelectedIndices = (ArrayList)data["selectedIndices"];
            }
            //end for  adding text
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }
            base.OnInit(e);
            AddSysStaticKey();
            Page.RegisterRequiresControlState(this);

        }
        #endregion

        #region ** IC1Serializable Implementation

        /// <summary>
        /// Saves the control layout properties to the file.
        /// </summary>
        /// <param name="filename">The file where the values of the layout properties will be saved.</param> 
        /// <example>
        /// Following code shows how to use this method.
        /// <code lang="C#"> 
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("test1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("test2"));
        /// string path = "c:\comboBox.xml";
        /// // the comboBox is saved in comboBox.xml
        /// comboBox.SaveLayout(path);
        /// </code>
        /// </example>
        public void SaveLayout(string filename)
        {
            C1ComboBoxSerializer sz = new C1ComboBoxSerializer(this);
            sz.SaveLayout(filename);
        }

        /// <summary>
        /// Saves control layout properties to the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be saved.</param>
        /// <example>
        /// Following code shows how to use this method.
        /// <code lang="C#"> 
        /// // save comboBox properties to a MemoryStream object.
        /// MemoryStream m = new MemoryStream();
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("test1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("test2"));
        /// comboBox.SaveLayout(m);
        /// </code>
        /// </example>
        public void SaveLayout(Stream stream)
        {
            C1ComboBoxSerializer sz = new C1ComboBoxSerializer(this);
            sz.SaveLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        /// <example>
        /// Following code shows how to use this method.
        /// <code lang="C#"> 
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("test1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("test2"));
        /// string path = "c:\comboBox.xml";
        /// // the comboBox is saved in comboBox.xml
        /// comboBox.SaveLayout(path);
        /// 
        /// // load from comboBox.xml
        /// C1ComboBox comboBox2 = new C1ComboBox();
        /// comboBox2.LoadLayout(path);
        /// </code>
        /// </example>
        public void LoadLayout(string filename)
        {
            LoadLayout(filename, LayoutType.All);
        }

        /// <summary>
        /// Load control layout properties from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be loaded.</param>
        /// <example>
        /// Following code shows how to use this method.
        /// <code lang="C#"> 
        /// // save comboBox properties to a MemoryStream object.
        /// MemoryStream m = new MemoryStream();
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("test1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("test2"));
        /// comboBox.SaveLayout(m);
        /// // load from stream.
        /// C1ComboBox comboBox2 = new C1ComboBox();
        /// comboBox2.LoadLayout(m);
        /// </code>
        /// </example>
        public void LoadLayout(Stream stream)
        {
            LoadLayout(stream, LayoutType.All);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        /// <example>
        /// Following code shows how to use this method.
        /// <code lang="C#"> 
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("test1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("test2"));
        /// string path = "c:\comboBox.xml";
        /// // the comboBox is saved in comboBox.xml
        /// comboBox.SaveLayout(path);
        /// 
        /// // load from comboBox.xml, only properties with Appearance category is loaded.
        /// C1ComboBox comboBox2 = new C1ComboBox();
        /// comboBox2.LoadLayout(path, LayoutType.Appearance);
        /// </code>
        /// </example>
        public void LoadLayout(string filename, LayoutType layoutTypes)
        {
            Columns.Clear();
            Items.Clear();
            C1ComboBoxSerializer sz = new C1ComboBoxSerializer(this);
            sz.LoadLayout(filename, layoutTypes);
            SetItemOwner();
        }

        /// <summary>
        /// Loads the control layout properties with specified types from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        /// <example>
        /// Following code shows how to use this method.
        /// <code lang="C#"> 
        /// // save comboBox properties to a MemoryStream object.
        /// MemoryStream m = new MemoryStream();
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("test1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("test2"));
        /// comboBox.SaveLayout(m);
        /// // load from stream, only properties with Appearance category is loaded.
        /// C1ComboBox comboBox2 = new C1ComboBox();
        /// comboBox2.LoadLayout(m, LayoutType.Appearance);
        /// </code>
        /// </example>
        public void LoadLayout(Stream stream, LayoutType layoutTypes)
        {
            Columns.Clear();
            Items.Clear();
            C1ComboBoxSerializer sz = new C1ComboBoxSerializer(this);
            sz.LoadLayout(stream, layoutTypes);
            SetItemOwner();
        }

        private void SetItemOwner()
        {
            foreach (C1ComboBoxItem item in Items)
            {
                item.Owner = this;
            }
        }
        #endregion

        #region ** ICallbackEventHandler Implementation

        #region Client Properties for ajax.

        /// <summary>
        /// Gets the uniqueID of this control
        /// </summary>
        [Json(true)]
        [Browsable(false)]
        public override string UniqueID
        {
            get
            {
                return base.UniqueID;
            }
        }

        private int _requestItemCount = 0;
        [Json(true)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int RequestItemCount
        {
            get { return _requestItemCount; }
        }

        private bool _endRequest = false;
        [Json(true)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool EndRequest
        {
            get { return _endRequest; }
        }

        private int _pageIndex = 0;
        [Json(true)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int PageIndex
        {
            get { return _pageIndex; }
        }

        #endregion

        private string _fParams;

        /// <summary>
        /// Find a <see cref="C1ComboBoxItem"/>  by its <see cref="C1ComboBoxItem.Value"/> property.
        /// </summary>
        /// <param name="value">C1ComboBoxItem.Value used to match the item.</param>
        /// <returns>The index of first matched item. If no match is found, -1 is returned.</returns>
        /// <example>
        /// <code lang="C#"> 
        /// C1ComboBox comboBox = new C1ComboBox();
        /// comboBox.Items.Add(new C1ComboBoxItem("text1","value1"));
        /// comboBox.Items.Add(new C1ComboBoxItem("text2","value2"));
        /// // the first item is found.
        /// int matchIndex = comboBox.FindItemIndexByValue("value1");
        /// </code>
        /// </example>
        private int FindItemIndexByValue(string value)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (value != null && value.Equals(Items[i].Value))
                {
                    return i;
                }
            }
            return -1;
        }

        private string GetClientData_ScrollingToEnd_ItemPopulate(int curCount)
        {
            ArrayList arr = new ArrayList();
            for (int i = curCount; i < this.Items.Count; i++)
            {
                C1ComboBoxItem listBoxItem = this.Items[i];
                //
                string templateHtml = string.Empty;
                if (this.IsTemplated)
                {
                    StringWriter stringWriter = new StringWriter();
                    HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

                    listBoxItem.EnsureChild();
                    listBoxItem.RenderControl(htmlWriter);

                    templateHtml = htmlWriter.InnerWriter.ToString();
                }

                Hashtable item = new Hashtable();
                item["Text"] = listBoxItem.Text;
                item["Value"] = listBoxItem.Value;
                item["Selected"] = listBoxItem.Selected;
                if (!string.IsNullOrEmpty(templateHtml))
                {
                    item["TemplateHtml"] = templateHtml;
                }
                arr.Add(item);
            }

            Hashtable hash = new Hashtable();
            hash["ItemCount"] = this.Items.Count - curCount;
            hash["Items"] = arr;
            string res = JsonHelper.ObjectToString(hash, this);

            return res;
        }

        private string GetClientData_ScrollingToEnd_DataBind(IEnumerable dataSource, int pageIndex, int pageSize)
        {
            PagedDataSource pds = new PagedDataSource();
            pds.DataSource = dataSource;
            pds.AllowPaging = true;
            pds.PageSize = pageSize;
            pds.CurrentPageIndex = pageIndex;

            bool isEnd = pds.PageCount <= pds.CurrentPageIndex;
            if (this.EndRequest || isEnd)
            {
                return "";
            }

            dataSource = pds;

            ArrayList arr = new ArrayList();
            IEnumerator e = dataSource.GetEnumerator();
            bool hasTextField = !this.DataTextField.Trim().Equals(string.Empty);
            bool hasValueField = !this.DataValueField.Trim().Equals(string.Empty);
            bool hasSelectedField = !this.DataSelectedField.Trim().Equals(string.Empty);
            while (e.MoveNext())
            {
                object itemdata = e.Current;
                string text = "";
                string value = "";
                bool itemSelected = false;
                if (hasTextField)
                {
                    text = DataBinder.GetPropertyValue(itemdata, this.DataTextField.Trim(), this.DataTextFormatString).ToString();
                }
                else
                {
                    text = itemdata.ToString();
                }
                if (hasValueField)
                {
                    value = DataBinder.GetPropertyValue(itemdata, this.DataValueField.Trim()).ToString();
                }
                else
                {
                    value = itemdata.ToString();
                }
                if (hasSelectedField)
                {
                    itemSelected = (bool)DataBinder.GetPropertyValue(itemdata, this.DataSelectedField.Trim());
                }
                else
                {
                    itemSelected = false;
                }

                string templateHtml = string.Empty;
                if (this.IsTemplated)
                {
                    StringWriter stringWriter = new StringWriter();
                    HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
                    C1ComboBoxItem comboxItem = new C1ComboBoxItem();
                    comboxItem.Text = text;
                    comboxItem.Value = text;
                    this.Items.Add(comboxItem);

                    comboxItem.EnsureChild();
                    comboxItem.RenderControl(htmlWriter);

                    templateHtml = htmlWriter.InnerWriter.ToString();
                }

                Hashtable item = new Hashtable();
                item["Text"] = text;
                item["Value"] = text;
                item["Selected"] = itemSelected;

                if (!string.IsNullOrEmpty(templateHtml))
                {
                    item["TemplateHtml"] = templateHtml;
                }
                arr.Add(item);
            }

            Hashtable hash = new Hashtable();
            hash["PageIndex"] = pageIndex;
            hash["IsLastPage"] = pds.IsLastPage;
            hash["Items"] = arr;
            string res = JsonHelper.ObjectToString(hash, this);
            //string res = JsonHelper.WidgetToString(hash, this);

            return res;
        }

        string ICallbackEventHandler.GetCallbackResult()
        {
            StringBuilder sb = new StringBuilder();
            Hashtable data = JsonHelper.StringToObject(_fParams);
            string CommandName = data["CommandName"].ToString();
            //update for adding text property by wuhao at 2011/8/10
            this.Text = data["Text"].ToString();
            //end for add

            switch (CommandName)
            {
                case "ScrollingToEnd-ItemPopulate":
                    {
                        int curCount = this.Items.Count;
                        int requestItemCount = (int)data["RequestItemCount"];
                        requestItemCount = Math.Max(requestItemCount, this.RequestItemCount);

                        C1ComboBoxItemPopulateEventArgs args = new C1ComboBoxItemPopulateEventArgs(requestItemCount);
                        args.EndRequest = this.EndRequest;
                        //update for adding text property by wuhao at 2011/8/10
                        args.FilterText = data["Text"].ToString();
                        //end for add
                        this.OnItemPopulate(args);

                        if (args.EndRequest)
                        {
                            return "";
                        }

                        string res = GetClientData_ScrollingToEnd_ItemPopulate(curCount);
                        sb.Append(res);
                    }
                    break;
                case "ScrollingToEnd-DataBind":
                    {
                        int pageindex = (int)data["PageIndex"];
                        pageindex = Math.Max(pageindex, this.PageIndex);

                        C1ComboBoxCallbackEventArgs args = new C1ComboBoxCallbackEventArgs();
                        //update for adding text property by wuhao at 2011/8/10
                        args.FilterText = data["Text"].ToString();
                        //end for add
                        this.OnCallbackDataBind(args);

                        if (args.DataSource == null)
                        {
                            return "";
                        }

                        IEnumerable dataSource = this.GetResolvedDataSource(args.DataSource);
                        string res = GetClientData_ScrollingToEnd_DataBind(dataSource, pageindex, args.PageSize);
                        sb.Append(res);
                    }
                    break;
            }
            return sb.ToString();
        }

        void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
        {
            _fParams = eventArgument;
        }

        #endregion

        #region ** IPostBackEventHandler Implementation

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            //update for fixing issue 20257 by wh at 2012/3/2
            //int newIndex = Convert.ToInt32(eventArgument);
            //int oldIndex = this.SelectedIndex;
            //this.SelectedIndex = newIndex;
            string[] argArray = eventArgument.Split(',');
            int oldIndex = Convert.ToInt32(argArray[0]);
            int newIndex = Convert.ToInt32(argArray[1]);
            //end for 20257 issue
            OnSelectedIndexChanged(new C1ComboBoxEventArgs(oldIndex, newIndex));
        }

        #endregion

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void C1DeserializeItems(object obj)
        {

        }

        #region ** IPostBackDataHandler interface implementations
        /// <summary>
        /// Processes postback data for an ASP.NET server control.
        /// </summary>
        /// <param name="postDataKey">The key identifier for the control.</param>
        /// <param name="postCollection">The collection of all incoming name values.</param>
        /// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            isDeserializing = true;
            Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
            this.RestoreStateFromJson(data);
            this.LoadCompositePropertiesFromJsonData(data);
            isDeserializing = false;
            return false;
        }

        /// <summary>
        /// Notify the ASP.NET application that the state of the control has changed.
        /// </summary>
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }
        #endregion end of ** IPostBackDataHandler interface implementations

        #region template
        private void AddSysStaticKey()
        {
            int index = 0;
            foreach (C1ComboBoxItem item in Items)
            {
                item.StaticKey = "sk" + index;
                index++;
            }
            //index = 0;
            //foreach (C1ComboBoxColumn c in Columns)
            //{
            //    c.StaticKey = "sk" + index;
            //    index++;
            //}
        }
        #endregion


        #region  control state

        //update for case 21403
        //combobox can’t be restored after it is changed to be visible

        private string itemsInControlState;

        internal string ItemsInControlState
        {
            get
            {
                itemsInControlState = JsonHelper.WidgetToString(Items, this);
                return itemsInControlState;
            }
            set
            {
                itemsInControlState = value;
                Hashtable data = JsonHelper.StringToObject("{'data':" + itemsInControlState + "}");
                this.Items.FromJson(data["data"] as ArrayList);
            }
        }

        protected override object SaveControlState()
        {
            object obj = base.SaveControlState();

            if (ItemsInControlState != null)
            {
                if (obj != null)
                {
                    return new Pair(obj, ItemsInControlState);
                }
                else
                {
                    return (ItemsInControlState);
                }
            }
            else
            {
                return obj;
            }
        }

        protected override void LoadControlState(object state)
        {
            if (this.Page.Request[JsonSerializableHelper.DataFieldClientId + "_ffcache"] != null)
            {
                return;
            }

            if (state != null)
            {
                Pair p = state as Pair;
                if (p != null)
                {
                    base.LoadControlState(p.First);
                    ItemsInControlState = (string)p.Second;
                }
                else
                {
                    if (state is string)
                    {
                        ItemsInControlState = (string)state;
                    }
                    else
                    {
                        base.LoadControlState(state);
                    }
                }
            }
        }
        #endregion


    }
}