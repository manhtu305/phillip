﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base;
namespace C1.Web.Wijmo.Controls.C1ComboBox
{
	[DefaultProperty("Text")]
	public partial class C1ComboBoxItem : UIElement, IComparable, IDataItemContainer
	{
		#region ** fields
		private int _dataItemIndex;
		private object _dataItem;
		private C1ComboBoxTemplateContainer _container;
		private C1ComboBox _owner;
		private string _staticKey;
		#endregion end of ** fields.

		#region ** constructor
		///<summary>
		/// Initializes a new instance of the C1ComboBoxItem class.
		///</summary>
		public C1ComboBoxItem()
			: this(string.Empty)
		{
		}

		///<summary>
		/// Initializes a new instance of the C1ComboBoxItem class.
		///</summary>
		///<param name="text"><see cref="C1ComboBoxItem"/> text.</param>
		public C1ComboBoxItem(string text)
			: this(text, string.Empty)
		{
		}

		///<summary>
		/// Initializes a new instance of the <see cref="C1ComboBoxItem"/> class.
		///</summary>
		///<param name="text"><see cref="C1ComboBoxItem"/> Text.</param>
		///<param name="value"><see cref="C1ComboBoxItem"/> Value.</param>
		public C1ComboBoxItem(string text, string value)
		{
			this.Text = text;
			this.Value = value;
			//#if ASP_NET4
			//            this.ClientIDMode = ClientIDMode.AutoID;
			//#endif
		}

		/// <summary>
		/// Initializes a new instance of the C1ComboBoxItem class.
		/// </summary>
		/// <param name="itemIndex">Index of current item.</param>
		/// <param name="dataItem">Related data item.</param>
		internal C1ComboBoxItem(int itemIndex, object dataItem)
		{
			_dataItemIndex = itemIndex;
			_dataItem = dataItem;
			//#if ASP_NET4
			//            this.ClientIDMode = ClientIDMode.AutoID;
			//#endif
		}
		#endregion end of ** constructor

		#region ** properties
		/// <summary>
		/// item's label.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1ComboBox.C1ComboBoxItem.Text")]
		[WidgetOption]
		[WidgetOptionName("Label")]
		[Layout(LayoutType.Appearance)]
		public string Text
		{
			get
			{
				return _properties["Text"] == null ? "" : _properties["Text"].ToString();
			}
			set
			{
				_properties["Text"] = value;
			}
		}

        /// <summary>
        /// item's ToolTip
        /// </summary>
        [DefaultValue("")]
        [WidgetOptionName("title")]
        [WidgetOption]
        [Layout(LayoutType.Appearance)]
        public override string ToolTip
        {
            get
            {
                return base.ToolTip;
            }
            set
            {
                base.ToolTip = value;
            }
        }

		/// <summary>
		/// Key for C1ComboItem declared on aspx page.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(true)]
		public string StaticKey
		{
			get
			{
				return _staticKey;
			}
			set
			{
				_staticKey = value;
			}
		}

		//update for fixing issue 20255 by wh at 2011/3/5
		/// <summary>
		/// Key for C1ComboItem declared on aspx page.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				 base.Enabled = value;
			}
		}


		/// <summary>
		/// The visible of combobox item.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Visible
		{
			get
			{
				return base.Visible;
			}
			set
			{
				 base.Visible = value;
			}
		}
		//end for fixing issue 20255
		#endregion end of ** properties.

		#region uielement
		/// <summary>
		/// Gets the value that corresponds to this Web server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Li;
			}
		}
		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();
			if (_owner != null && _owner.ItemsTemplate != null)
			{
				TemplateContainer = new C1ComboBoxTemplateContainer(_owner.Items.IndexOf(this), _dataItem);
				TemplateContainer.ComboBoxItem = this;
				TemplateContainer.ParentComboBox = this._owner;
				//TemplateContainer.CssClass = "C1cmbItemTemplate";
				Controls.Add(TemplateContainer);
				_owner.ItemsTemplate.InstantiateIn(this.TemplateContainer);

				TemplateContainer.DataBind();
			}
		}

		#endregion

		#region help property
		/*///<summary>
		/// Gets or sets value indicating if this item uses template
		///</summary>
		[DefaultValue(false)]
		[Json(true, true, false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public bool Templated
		{
			get
			{
				return (_owner.ItemsTemplate != null);
			}
		}*/
		/// <summary>
		/// Specifies owner of this <see cref="C1ComboBoxItem"/>.
		/// </summary>
		/// <param name="owner"><see cref="C1ComboBox"/> object.</param>
		internal void SetParent(C1ComboBox owner)
		{
			_owner = owner;
		}

		[Browsable(false)]
		internal C1ComboBox Owner
		{
			get
			{
				return _owner;
			}
			set
			{
				_owner = value;
			}
		}

		internal void EnsureChild()
		{
			EnsureChildControls();
		}
		#endregion

		#region ** The IComparable interface implementations
		///<summary>
		/// IComparable.CompareTo implementation.
		///</summary>
		///<param name="obj"></param>
		///<returns></returns>
		///<exception cref="ArgumentException"></exception>
		///<exception cref="Exception"></exception>
		int IComparable.CompareTo(object obj)
		{
			if (!(obj is C1ComboBoxItem))
			{
				throw new ArgumentException();
			}

			C1ComboBoxItem comboBoxItem = (C1ComboBoxItem)obj;
			int i = String.Compare(Text, comboBoxItem.Text, true);

			return i;
		}
		#endregion end of ** The IComparable interface implementations.

		/// <summary>
		/// Gets or sets the template container.
		/// </summary>
		/// <value>The template container.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		internal C1ComboBoxTemplateContainer TemplateContainer
		{
			get
			{
				return this._container;
			}
			set
			{
				this._container = value;
			}
		}

		#region ** IDataItemContainer interface implementations
		/// <summary>
		/// Gets DataItem of this C1ComboBoxItem.
		/// </summary>
		[Browsable(false)]
		public object DataItem
		{
			get { return this._dataItem; }
		}

		/// <summary>
		/// Gets DataItemIndex of this C1ComboBoxItem.
		/// </summary>
		[Browsable(false)]
		public int DataItemIndex
		{
			get { return this._dataItemIndex; }
		}

		/// <summary>
		/// Gets DisplayIndex of this C1ComboBoxItem.
		/// </summary>
		[Browsable(false)]
		public int DisplayIndex
		{
			get { return this._dataItemIndex; }
		}
		#endregion end of ** IDataItemContainer interface implementations.

	}
}
