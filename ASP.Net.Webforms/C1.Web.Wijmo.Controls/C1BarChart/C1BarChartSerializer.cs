﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	/// <summary>
	/// Serializes C1BarChart Control.
	/// </summary>
	public class C1BarChartSerializer : C1ChartSerializer<C1BarChart>
	{

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the C1BarChartSerializer class.
		/// </summary>
		/// <param name="obj">
		/// The <see cref="C1BarChart"/> instance.
		/// </param>
		public C1BarChartSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
