﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.C1SuperPanel
{
    internal class C1SuperPanelSerializer : C1BaseSerializer<C1SuperPanel, object, object>
    {
        /// <summary>
		/// Initialize a new instance of <see cref="C1SuperPanelSerializer"/>.
        /// </summary>
        /// <param name="serializableObject">Serializable Object.</param>
        public C1SuperPanelSerializer(object serializableObject) : base(serializableObject)
        {
        }

        /// <summary>
		/// Initialize a new instance of <see cref="C1SuperPanelSerializer"/>.
        /// </summary>
        /// <param name="componentChangeService"></param>
        /// <param name="serializableObject"></param>
        public C1SuperPanelSerializer(IComponentChangeService componentChangeService, object serializableObject)
			: base(componentChangeService, serializableObject)
        {
        }
    }
}
