﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Util.Licensing;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Drawing;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.IO;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1SuperPanel
{
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1SuperPanel.C1SuperPanelDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1SuperPanel.C1SuperPanelDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1SuperPanel.C1SuperPanelDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1SuperPanel.C1SuperPanelDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[LicenseProviderAttribute()]
	[ToolboxData("<{0}:C1SuperPanel runat=server></{0}:C1SuperPanel>")]
	[ToolboxBitmap(typeof(C1SuperPanel), "C1SuperPanel.png")]
	public partial class C1SuperPanel : C1TargetControlBase, IC1Serializable, IPostBackDataHandler
	{

		#region ** fields
		#region ** private members
		private ITemplate _contentTemplate = null;
		private HtmlGenericControl _template = null;
		private HtmlGenericControl _stateContainer = null;
		private HtmlGenericControl _contentWrapper = null;

		private bool _productLicensed;
		private bool _shouldNag;
		#endregion end of ** private members.

		#region ** const members
		private const string CONST_WIJSUPERPANEL = "wijmo-wijsuperpanel";
		private const string CONST_WIJSUPERPANEL_STATECONTAINER = CONST_WIJSUPERPANEL + "-statecontainer";
		private const string CONST_WIJSUPERPANEL_CONTENTWRAPPER = CONST_WIJSUPERPANEL + "-contentwrapper";
		private const string CONST_WIJSUPERPANEL_TEMPLATEOUTERWRAPPER = CONST_WIJSUPERPANEL + "-templateouterwrapper";
		private const string CONST_WIDGET = "ui-widget";
		private const string CONST_WIDGET_CONTENT = "ui-widget-content";
		#endregion end of ** const members.
		#endregion end of ** fields.

		#region ** constructor
		/// <summary>
		/// Initialize a new instance of <see cref="C1SuperPanel"/>.
		/// </summary>
		public C1SuperPanel()
		{
			LicenseInfo licinfo = ProviderInfo.Validate(typeof(C1SuperPanel), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
			this.InitSuperPanel();
		}

		/// <summary>
		/// Initialize a new instance of <see cref="C1SuperPanel"/>.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public C1SuperPanel(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			// ttZzKzJ5mwNr/IihpBl2pA==
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1SuperPanel), this,
				Assembly.GetExecutingAssembly(), key);
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
			this.InitSuperPanel();
		}

		private void InitSuperPanel()
		{
			this.ResizableOptions.Handles = Compass.All;
			this.ResizableOptions.Helper = CONST_RESIZE_HELPER;
		}
		#endregion

		#region ** properties
		#region ** public properties
		/// <summary>
		/// Template for <see cref="C1SuperPanel"/> into which user could drag controls.
		/// </summary>
		[Browsable(false)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public ITemplate ContentTemplate
		{
			get
			{
				return _contentTemplate;
			}
			set
			{
				_contentTemplate = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether C1SuperPanel will maintain the scroll position on postback.
		/// </summary>
		[C1Description("C1SuperPanel.MaintainScrollPositionOnPostback")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public bool MaintainScrollPositionOnPostback
		{
			get
			{
				return this.GetPropertyValue<bool>("MaintainScrollPositionOnPostback", true);
			}
			set
			{
				this.SetPropertyValue<bool>("MaintainScrollPositionOnPostback", value);
			}
		}
		
		//TODO???
		/// <summary>
		/// Gets or sets a value indicating whether the content of C1SuperPanel can be dragged to scrolling when mousedown.
		/// </summary>
		[C1Description("C1SuperPanel.EnableGestureScrolling")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public bool EnableGestureScrolling
		{
			get
			{
				return this.GetPropertyValue<bool>("EnableGestureScrolling", true);
			}
			set
			{
				this.SetPropertyValue<bool>("EnableGestureScrolling", value);
			}
		}
		#region ** override properties
		/// <summary>
		/// Gets or sets the width of SuperPanel.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Web.UI.WebControls.Unit"/> that represents the width of the control. The default is <see cref="F:System.Web.UI.WebControls.Unit.Empty"/>.
		/// </returns>
		/// <exception cref="T:System.ArgumentException">
		/// The width of the Web server control was set to a negative value.
		/// </exception>
		[C1Description("C1SuperPanel.Width")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// Gets or sets the height of SuperPanel.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Web.UI.WebControls.Unit"/> that represents the height of the control. The default is <see cref="F:System.Web.UI.WebControls.Unit.Empty"/>.
		/// </returns>
		/// <exception cref="T:System.ArgumentException">
		/// The height was set to a negative value.
		/// </exception>
		[C1Description("C1SuperPanel.Height")]
		[C1Category("Category.Appearance")]
		[Layout(LayoutType.Appearance)]
		[WidgetOption]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the enable state of the C1SuperPanel.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[C1Description("C1SuperPanel.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}
		#endregion end of ** override properties.
		#endregion end of ** public properties.

		#region ** protected properties
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}
		#endregion end of ** protected properties.
		#endregion end of ** properties.

		#region ** methods
		#region ** private methods
		/// <summary>
		/// Determine compound css class.
		/// </summary>
		/// <returns>Returns the css string.</returns>
		private string DetermineCompoundCssClass()
		{
			if (!this.IsDesignMode)
			{
				const string contentClass = "ui-widget-content ui-corner-all";
				string hiddenClass = Utils.GetHiddenClass();
				if (string.IsNullOrEmpty(hiddenClass))
				{
					hiddenClass += " ";
				}
				return hiddenClass + contentClass;
			}

			string cssClass = string.Empty;

			return string.Format("{0} {1} {2}",
					CONST_WIJSUPERPANEL,
					CONST_WIDGET,
					CONST_WIDGET_CONTENT);
		}

		private HtmlGenericControl Template
		{
			get
			{
				if (this._template == null)
				{
					this._template = new HtmlGenericControl("div");
					this._template.Style.Add("width", "100%");
					this._template.Style.Add("height", "100%");
					this._template.Style.Add("overflow", "auto");
					this._template.Attributes["class"] = CONST_WIJSUPERPANEL_TEMPLATEOUTERWRAPPER;
				}

				return this._template;
			}
		}
		#endregion end of ** private methods.

		#region ** public methods
		/// <summary>
		/// Adds the control.
		/// </summary>
		/// <param name="control">The Control to add to SuperPanel.</param>
		public void AddControl(Control control)
		{
			if (IsDesignMode)
			{
				this.Template.Controls.Add(control);
			}
			else
			{
				this.Controls.Add(control);
			}
		}
		#endregion end of ** public methods.

		#region ** override methods
		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			base.CreateChildControls();

			this.Controls.Clear();

			if (this.IsDesignMode)
			{
				this._stateContainer = new HtmlGenericControl("div");
				this._stateContainer.Attributes["class"] = CONST_WIJSUPERPANEL_STATECONTAINER;
				this._stateContainer.Style.Add("width", "100%");
				this._stateContainer.Style.Add("height", "100%");
				this.Controls.Add(this._stateContainer);
				
				this._contentWrapper = new HtmlGenericControl("div");
				this._contentWrapper.Attributes["class"] = CONST_WIJSUPERPANEL_CONTENTWRAPPER;
				this._contentWrapper.Style.Add("width", "100%");
				this._contentWrapper.Style.Add("height", "100%");
				this._stateContainer.Controls.Add(this._contentWrapper);
				this._contentWrapper.Controls.Add(this.Template);

				this.Template.Attributes.Add("C1SuperPanelDesignerRegionAttributeName", "0");
			}
			else
			{
				if (this.ContentTemplate != null)
				{
					this.ContentTemplate.InstantiateIn(this);
				}
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
			EnsureChildControls();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to
		/// the specified <see cref="HtmlTextWriterTag"/>.
		/// </summary>
		/// <param name="writer">A <see cref="HtmlTextWriter"/>that represents
		/// the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = this.CssClass;
			this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass);

			base.AddAttributesToRender(writer);
			this.CssClass = cssClass;

			if (this.IsDesignMode)
			{
				writer.AddStyleAttribute("display", "block");
				writer.AddStyleAttribute("overflow", "visible");
			}
		}

		/// <summary>
		/// Raises the System.Web.UI.Control.PreRender event.
		/// </summary>
		/// <param name="e">An System.EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			//UI.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();

			if (this.Page != null && this.Page.IsPostBack)
			{
				if (!this.MaintainScrollPositionOnPostback)
				{
					this.HScroller.ScrollValue = 0;
					this.VScroller.ScrollValue = 0;
				}
			}
			
			base.Render(writer);
		}
		#endregion end of ** override methods.
		#endregion end of ** methods.

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1SuperPanelSerializer sz = new C1SuperPanelSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1SuperPanelSerializer sz = new C1SuperPanelSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1SuperPanelSerializer sz = new C1SuperPanelSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1SuperPanelSerializer sz = new C1SuperPanelSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end of ** IC1Serializable interface implementations.

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		///  Processes postback data for an ASP.NET server control
		/// </summary>
		/// <param name="postDataKey"> The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values</param>
		/// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			JsonRestoreHelper.RestoreStateFromJson(this, data);

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent() 
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations.
	}
}
