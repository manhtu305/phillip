﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;
using System.IO;
using System.Collections.Specialized;




namespace C1.Web.Wijmo.Controls.C1Wizard
{
    using C1.Web.Wijmo;
    using C1.Web.Wijmo.Controls.Base;
    using C1.Web.Wijmo.Controls.Localization;

    [ToolboxData("<{0}:C1Wizard runat=server></{0}:C1Wizard>")]
    [ToolboxBitmap(typeof(C1Wizard), "C1Wizard.png")]
    [ParseChildren(true)]
    [LicenseProviderAttribute()]
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1Wizard.C1WizardDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1Wizard.C1WizardDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer("C1.Web.Wijmo.Controls.Design.C1Wizard.C1WizardDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
    [Designer("C1.Web.Wijmo.Controls.Design.C1Wizard.C1WizardDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    public partial class C1Wizard : C1TargetControlBase,
        INamingContainer,
        IPostBackEventHandler,
        IPostBackDataHandler
    {
        #region Fields

        private bool _productLicensed = false;
		private bool _shouldNag;

        #endregion

        #region Constructor

         /// <summary>
        /// Construct a new instance of C1Wizard.
        /// </summary>
        [C1Description("C1Wizard.Constructor")]
        public C1Wizard()
            : base(HtmlTextWriterTag.Div)
        {
            VerifyLicense();
        }

        #endregion

        #region Licensing

        internal virtual void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Wizard), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #endregion

        #region Properties

        /// <summary>
        /// PostBack Event Reference. Empty string if AutoPostBack property is false.
        /// </summary>
        [WidgetOption()]
        [DefaultValue("")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string PostBackEventReference
        {
            get
            {
                if (!IsDesignMode && this.AutoPostBack)
                {
                    return this.Page.ClientScript.GetPostBackEventReference(this.GetPostBackOptions());
                }
                return "";
            }
        }

        /// <summary>
        /// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
        /// </summary>
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        [C1Description("C1Wizard.AutoPostBack")]
        public bool AutoPostBack
        {
            get
            {
                return GetPropertyValue<bool>("AutoPostBack", false);
            }
            set
            {
                SetPropertyValue<bool>("AutoPostBack", value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether validation is performed when a active index changed.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Wizard.CausesValidation")]
        [Layout(LayoutType.Behavior)]
        [DefaultValue(false)]
        public virtual bool CausesValidation
        {
            get
            {
                return GetPropertyValue<bool>("CausesValidation", false);
            }
            set
            {
                SetPropertyValue<bool>("CausesValidation", value);
            }
        }

        /// <summary>
        /// Gets or sets the group of controls for which the wizard causes validation when it posts back to the server.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Wizard.ValidationGroup")]
        [Layout(LayoutType.Behavior)]
        [DefaultValue("")]
        [Bindable(true)]
        public string ValidationGroup
        {
            get
            {
                return GetPropertyValue<string>("ValidationGroup", String.Empty);
            }
            set
            {
                SetPropertyValue<string>("ValidationGroup", value);
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether step headers are displayed.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Wizard.ShowStepHeader")]
        [Layout(LayoutType.Behavior)]
        [DefaultValue(true)]
        public virtual bool ShowStepHeader
        {
            get
            {
                return GetPropertyValue<bool>("ShowStepHeader", true);
            }
            set
            {
                SetPropertyValue<bool>("ShowStepHeader", value);
            }
        }

        /// <summary>
        /// Gets the current selected page.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public C1WizardStep ActivedStep
        {
            get
            {
                if (this.ActiveIndex < 0 || this.ActiveIndex >= this.Steps.Count) return null;
                return (C1WizardStep)this.Steps[this.ActiveIndex];
            }
        }

        /// <summary>
        /// Gets the tab step collection.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Browsable(false)]
        public C1WizardStepCollection Steps
        {
            get
            {
                return (C1WizardStepCollection)this.Controls;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Raised during postback when the selected index changed.
        /// </summary>
        [C1Description("C1Wizard.ActiveIndexChanged")]
        [C1Category("Category.Behavior")]
        public event EventHandler ActiveIndexChanged;

        /// <summary>
        /// Called when selected pane is changed.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnActiveIndexChanged(EventArgs e)
        {
            if (this.ActiveIndexChanged != null)
            {
                ActiveIndexChanged(this, e);
            }
        }

        #endregion

        #region IPostBackEventHandler Members

        /// <summary>
        /// Raises an event for the C1Wizard control when it posts back to the server.
        /// </summary>
        /// <param name="eventArgument">A String that represents the event argument passed to the event handler.</param>
        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.StartsWith("activeIndex="))
            {
                int newInd = int.Parse(eventArgument.Remove(0, "activeIndex=".Length));
                if (newInd != this.ActiveIndex)
                {
                    this.ActiveIndex = newInd;
                    OnActiveIndexChanged(new EventArgs());
                }
            }
        }

        #endregion

        #region ** IPostBackDataHandler interface implementations
        /// <summary>
        /// Processes postback data for an ASP.NET server control.
        /// </summary>
        /// <param name="postDataKey">The key identifier for the control.</param>
        /// <param name="postCollection">The collection of all incoming name values.</param>
        /// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
            this.RestoreStateFromJson(data);
            return false;
        }

        /// <summary>
        /// Notify the ASP.NET application that the state of the control has changed.
        /// </summary>
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        #endregion end of ** IPostBackDataHandler interface implementations

        #region  Render

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (this.ShowStepHeader)
                this.RenderHeaders(writer);

            this.RenderPages(writer);
            this.RenderButtons(writer);
        }

        private void RenderHeaders(HtmlTextWriter writer)
        {
            HtmlGenericControl ul = new HtmlGenericControl("ul");
            if (this.DesignMode)
            {
                ul.Attributes["class"] = "ui-widget ui-helper-reset wijmo-wijwizard-steps ui-helper-clearfix";
                ul.Style.Add(HtmlTextWriterStyle.Height, "80px");
                ul.Style.Add(HtmlTextWriterStyle.Width, "8000px");
            }

            // Render tabs
            for (int i = 0; i < this.Steps.Count; i++)
            {
                C1WizardStep step = ((C1WizardStep)this.Steps[i]);
                HtmlGenericControl li = new HtmlGenericControl("li");

                if (this.DesignMode)
                {
                    li.Attributes.Add("class", "ui-widget-header ui-corner-all" + (this.ActiveIndex == i ? " ui-priority-primary" : " ui-priority-secondary"));
                    li.Attributes.Add(System.Web.UI.Design.DesignerRegion.DesignerRegionAttributeName, string.Format("{0}", i));
                }
                else
                {
                    if (!step.Visible) continue;
                }

                string header = this.StepHeaderTemplate.Replace("#{title}", step.Title).Replace("#{desc}", step.Description);
                li.InnerHtml = header;
                ul.Controls.Add(li);
            }

            ul.RenderControl(writer);
        }

        private void RenderPages(HtmlTextWriter writer)
        {
            if (this.DesignMode)
            {
                if (this.ActiveIndex >= 0 && this.ActiveIndex < this.Steps.Count)
                {
                    C1WizardStep step = (C1WizardStep)this.Steps[this.ActiveIndex];
                    step._owner = this;

                    writer.WriteBeginTag("div");
                    writer.WriteAttribute("class", "wijmo-wijwizard-content ui-widget ui-widget-content ui-corner-all");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    step.RenderControl(writer);
                    writer.WriteEndTag("div");
                }
            }
            else
            {
                for (int i = 0; i < this.Steps.Count; i++)
                {
                    C1WizardStep step = ((C1WizardStep)this.Steps[i]);
                    if (step.Visible)
                        step.RenderControl(writer);
                }
            }
        }

        private void RenderButtons(HtmlTextWriter writer)
        {
            if (!this.DesignMode || this.NavButtons == NavButtonType.None) return;

            NavButtonType nbt = this.NavButtons;
            if (this.NavButtons == NavButtonType.Auto)
                nbt = NavButtonType.Common;

            Panel buttons = new Panel();
            buttons.CssClass = "wijmo-wijwizard-buttons";

            HyperLink prevButton = new HyperLink();
            HyperLink nextButton = new HyperLink();
            HtmlGenericControl prevSpan = new HtmlGenericControl("span");
            HtmlGenericControl nextSpan = new HtmlGenericControl("span");
            prevButton.Controls.Add(prevSpan);
            nextButton.Controls.Add(nextSpan);
            buttons.Controls.Add(prevButton);
            buttons.Controls.Add(nextButton);

            if (nbt == NavButtonType.Common)
            {
                prevButton.CssClass = "ui-widget ui-button ui-button-text-only ui-state-default ui-corner-all";
                prevSpan.Attributes["class"] = "ui-button-text";
                //prevSpan.InnerText = "back";
                prevSpan.InnerText=this.BackBtnText;

                nextButton.CssClass = "ui-widget ui-button ui-button-text-only ui-state-default ui-corner-all";
                nextSpan.Attributes["class"] = "ui-button-text";
                //nextSpan.InnerText = "next";
                nextSpan.InnerText=this.NextBtnText;
            }
            else 
            {
                prevButton.CssClass = "wijmo-wijwizard-prev ui-state-default ui-corner-right";
                prevSpan.Attributes["class"] = "ui-icon ui-icon-triangle-1-w";
                nextButton.CssClass = "wijmo-wijwizard-next ui-state-default ui-corner-left";
                nextSpan.Attributes["class"] = "ui-icon ui-icon-triangle-1-e";
            }

            buttons.RenderControl(writer);
        }

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.DesignMode)
            {
                if (!this.Visible)
                    return;

                writer.AddAttribute(HtmlTextWriterAttribute.Class, string.Format("{0} {1}", Utils.GetHiddenClass(), CssClass)); // hide markup during the client-side initialization.
            }

            if (this.Page != null)
                this.Page.VerifyRenderingInServerForm(this);

            base.Render(writer);
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.EnsureID();
            if (DesignMode)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijwizard ui-widget ui-helper-clearfix " + CssClass);
                writer.AddStyleAttribute(HtmlTextWriterStyle.Overflow, "hidden");
            }

            base.AddAttributesToRender(writer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override ControlCollection CreateControlCollection()
        {
            return new C1WizardStepCollection(this);
        }

        #endregion

        #region Internal Methods

        internal virtual PostBackOptions GetPostBackOptions()
        {
            PostBackOptions options = new PostBackOptions(this, this.UniqueID);
            options.ClientSubmit = true;
            options.Argument = "activeIndex={0}";
            if (this.Page != null)
            {
                if (this.CausesValidation && (this.Page.GetValidators(this.ValidationGroup).Count > 0))
                {
                    options.PerformValidation = true;
                    options.ValidationGroup = this.ValidationGroup;
                }
            }
            return options;
        }

        protected override void EnsureEnabledState()
        {
            return;
        }

        #endregion
    }
}
