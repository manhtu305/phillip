﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Data;
using System.Drawing.Design;



namespace C1.Web.Wijmo.Controls.C1Wizard
{
    using C1.Web.Wijmo.Controls.Base;
    using C1.Web.Wijmo.Controls.Localization;

    /// <summary>
    /// Represents a single tab page in a C1WizardStep. 
    /// </summary>
    [ToolboxItem(false)]
    [PersistChildren(true)]
    [ParseChildren(false)]
    public class C1WizardStep : Panel
    {
         #region Fields

        internal C1Wizard _owner;

        #endregion

        #region Consturctor

        /// <summary>
        /// Constructor of the C1WizardStep class.
        /// </summary>
        public C1WizardStep()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the title displayed on the wizard tab.
        /// </summary>
        [Localizable(true)]
        [DefaultValue("")]
        [C1Description("C1WizardStep.Title")]
        public string Title
        {
            get
            {
                object obj = this.ViewState["Title"];
                if (obj == null) return string.Empty;

                return (string)obj;
            }
            set
            {
                this.ViewState["Title"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the description displayed on the wizard tab.
        /// </summary>
        [Localizable(true)]
        [DefaultValue("")]
        [C1Description("C1WizardStep.Description")]
        public string Description
        {
            get
            {
                object obj = this.ViewState["Description"];
                if (obj == null) return string.Empty;

                return (string)obj;
            }
            set
            {
                this.ViewState["Description"] = value;
            }
        }

        #endregion

        #region Rendering

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.EnsureID();
            if (DesignMode)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijwizard-panel ui-widget-content wijmo-wijwizard-actived " + CssClass);
            }

            base.AddAttributesToRender(writer);
        }


        #endregion
    }
}
