﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Data;
using System.Drawing.Design;




namespace C1.Web.Wijmo.Controls.C1Wizard
{
    using C1.Web.Wijmo.Controls.Base;
    using C1.Web.Wijmo.Controls.Localization;

    /// <summary>
    /// Represents a collection of wizard steps.
    /// </summary>
    [Editor("System.Windows.Forms.Design.CollectionEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
    public class C1WizardStepCollection : ControlCollection
    {
        #region Constructor

        /// <summary>
        /// Creates an instance of C1WizardStepCollection class.
        /// </summary>
        /// <param name="owner">The owner of this collection.</param>
        public C1WizardStepCollection(C1Wizard owner)
            : base(owner)
        {
        }

        #endregion
    }
}
