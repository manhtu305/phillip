﻿namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;

	internal class EmptiableHashtable : Hashtable, IJsonEmptiable 
	{
		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return Count == 0; }
		}

		#endregion
	}
}
