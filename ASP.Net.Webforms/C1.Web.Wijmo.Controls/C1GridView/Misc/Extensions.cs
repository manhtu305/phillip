﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	internal static class TypeExtensions
	{
		private static long _jsDateBase = new DateTime(1970, 1, 1).Ticks;

		public static bool _IsBindable(this Type type)
		{
			if (type != null)
			{
				type = type._UnderlyingType();
			}

			return (type != null)
				? type.IsPrimitive || type == typeof(string) || type == typeof(decimal) || type == typeof(Guid) || type._IsDateTime()
				: false;
		}

		public static bool _IsNumeric(this Type type)
		{
			if (type != null)
			{
				type = type._UnderlyingType();
			}

			return (type != null)
				? ((type == typeof(decimal)) || (type.IsPrimitive && type != typeof(bool) && type != typeof(char) && type != typeof(IntPtr) && type != typeof(UIntPtr)))
				: false;
		}

		public static bool _IsReal(this Type type)
		{
			if (type != null && type._IsNumeric())
			{
				type = type._UnderlyingType();
				return (type == typeof(float) || type == typeof(double) || type == typeof(decimal));
			}

			return false;
		}

		public static bool _IsNatural(this Type type)
		{
			return (type != null && type._IsNumeric() && !type._IsReal());
		}

		public static bool _IsDateTime(this Type type)
		{
			if (type != null)
			{
				type = type._UnderlyingType();
			}

			return (type != null)
				? type == typeof(DateTime) || type == typeof(TimeSpan) || type == typeof(DateTimeOffset)
				: false;
		}

		public static long _jsGetTime(this DateTime value)
		{
			return (long)(new TimeSpan(value.Ticks - _jsDateBase).TotalMilliseconds); // (value - date(1970, 1, 1)).totalMilliseconds, local timezone (#39285).
			//return (long)(new TimeSpan(value.ToUniversalTime().Ticks - _jsDateBase).TotalMilliseconds); // (value.ToUniversalTime() - date(1970, 1, 1)).totalMilliseconds
		}

		public static string _jsGetTime2(this DateTime value)
		{
			return value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
		}

		public static Type _UnderlyingType(this Type type)
		{
			if (type != null)
			{
				Type underlyingType = Nullable.GetUnderlyingType(type);
				if (underlyingType != null)
				{
					type = underlyingType;
				}
			}

			return type;
		}
	}

	internal static class StyleExtensions
	{
		public static string ToStyleString(this Style value, WebControl resolver)
		{
			string result = string.Empty;

			if (value != null)
			{
				/*
				CssStyleCollection css = value.GetStyleAttributes(urlResolver);

				foreach (string key in css.Keys)
				{
					result += key + ":" + css[key] + ";";
				}

				if (!string.IsNullOrEmpty(value.CssClass))
				{
					result += "class:" + value.CssClass + ";";
				}
				*/

				using (StyleHtmlTextWriter writer = new StyleHtmlTextWriter())
				{
					value.AddAttributesToRender(writer, resolver);

					writer.RenderBeginTag(HtmlTextWriterTag.Div);
					writer.RenderEndTag();
					writer.Flush();

					result = writer.StyleString;
				}
			}

			return result;
		}
	}

	//internal static class DataKeyExtensions
	//{
	//	public static int _GetHash(this DataKey dataKey)
	//	{
	//		string result = string.Empty;

	//		if (dataKey != null)
	//		{
	//			foreach (System.Collections.DictionaryEntry entry in dataKey.Values)
	//			{
	//				object value = entry.Value;

	//				if (value == null || value == DBNull.Value)
	//				{
	//					result += "null";
	//				}
	//				else
	//				{
	//					result += (value is IConvertible)
	//						? ((IConvertible)value).ToString(System.Globalization.CultureInfo.InvariantCulture)
	//						: (value is IFormattable)
	//							? ((IFormattable)value).ToString(null, System.Globalization.CultureInfo.InvariantCulture)
	//							: value.ToString();
	//				}
	//			}
	//		}

	//		return result.GetHashCode();
	//	}
	//}

	internal class StyleHtmlTextWriter: HtmlTextWriter
	{
		private string _styleString = string.Empty;

		internal StyleHtmlTextWriter() : base(new StringWriter())
		{
		}

		public string StyleString
		{
			get { return _styleString; }
		}

		protected override bool OnAttributeRender(string name, string value, HtmlTextWriterAttribute key)
		{
			bool result = base.OnAttributeRender(name, value, key);

			if (result)
			{
				_styleString += "_" + name + ":" + value + ";"; // denote attribute using the underscore symbol.
			}

			return result;
		}

		protected override bool OnStyleAttributeRender(string name, string value, HtmlTextWriterStyle key)
		{
			bool result = base.OnStyleAttributeRender(name, value, key);

			if (result)
			{
				_styleString += name + ":" + value + ";";
			}

			return result;
		}
	}
}
