﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Collections.Specialized;
	using System.Web.UI;

	internal static class OrderedDictionaryHelper 
	{
		public static void LoadViewState(IOrderedDictionary dictionary, ArrayList state)
		{
			if (dictionary == null)
			{
				throw new ArgumentNullException("dictionary");
			}

			dictionary.Clear();

			if (state != null)
			{
				for (int i = 0, len = state.Count; i < len; i++)
				{
					Pair pair = (Pair)state[i];
					dictionary.Add(pair.First, pair.Second);
				}
			}
		}

		public static ArrayList SaveViewState(IOrderedDictionary dictionary)
		{
			ArrayList result = null;

			if (dictionary != null)
			{
				result = new ArrayList(dictionary.Count);

				foreach (DictionaryEntry entry in dictionary)
				{
					result.Add(new Pair(entry.Key, entry.Value));
				}
			}

			return (result != null && result.Count > 0)
				? result
				: null;
		}
	}
}
