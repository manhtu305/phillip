﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using C1.Web.Wijmo;
	
	/// <summary>
	/// MixedPersister.
	/// </summary>
	public class MixedPersister : Settings, IStateManager
	{
		private StateBag _viewState;
		private bool _marked = false;

		/// <summary>
		/// Constructor. Creates a new instance of the <b>MixedPersister</b> class.
		/// </summary>
		public MixedPersister() : this(true)
		{
		}

		/// <summary>
		/// Constructor. Creates a new instance of the <b>MixedPersister</b> class.
		/// </summary>
		/// <param name="trackViewState"></param>
		public MixedPersister(bool trackViewState)
		{
			if (trackViewState)
			{
				TrackViewState();
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(true, true, null)]
		[Layout(LayoutType.None)]
		public virtual Hashtable InnerState
		{
			get
			{
				Hashtable state = new EmptiableHashtable();

				object viewState = SaveViewState();
				if (viewState != null)
				{
					state["viewState"] = new ObjectStateFormatter().Serialize(viewState);
				}

				return state;
			}
		}


		#region protected members

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="name"></param>
		/// <param name="nullValue"></param>
		/// <returns></returns>
		protected V GetViewStatePropertyValue<V>(string name, V nullValue)
		{
			object value;

			return (_viewState != null && ((value = _viewState[name]) != null))
				? (V)value
				: nullValue;
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="name"></param>
		/// <param name="testValue"></param>
		/// <returns></returns>
		protected bool TestViewStatePropertyValue<V>(string name, V testValue)
		{
			object value;
			return (_viewState != null && ((value = _viewState[name]) != null) && _viewState.Equals(testValue));
		}

		/// <summary>
		/// Infrastructure
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="name"></param>
		/// <param name="value"></param>
		protected void SetViewStatePropertyValue<V>(string name, V value)
		{
			ViewState[name] = value;
		}

		/// <summary>
		/// Marks the state of the <see cref="MixedPersister"/> object as having been changed.
		/// </summary>
		protected internal virtual void SetDirty()
		{
			if (_viewState != null)
			{
				_viewState.SetDirty(true);
			}
		}

		#endregion

		#region IStateManager Members

		/// <summary>
		/// Gets a value indicating whether state changes are being tracked.
		/// </summary>
		protected bool IsTrackingViewState
		{
			get { return _marked; }
		}

		/// <summary>
		/// Manages the view state.
		/// </summary>
		protected StateBag ViewState
		{
			get
			{
				if (_viewState == null)
				{
					_viewState = new StateBag();
					if (IsTrackingViewState)
					{
						((IStateManager)_viewState).TrackViewState();
					}
				}

				return _viewState;
			}
		}

		/// <summary>
		/// Restores the previously saved view state of the <see cref="MixedPersister"/> object.
		/// </summary>
		/// <param name="state">An object that represents the <see cref="MixedPersister"/> state to restore.</param>
		protected virtual void LoadViewState(object state)
		{
			if (state != null)
			{
				((IStateManager)ViewState).LoadViewState(state);
			}
		}

		/// <summary>
		/// Saves the changes to the <see cref="MixedPersister"/> object since the time the page was posted back to the server.
		/// </summary>
		/// <returns>The object that contains the changes to the view state of the <see cref="MixedPersister"/>.</returns>
		protected virtual object SaveViewState()
		{
			return (_viewState != null)
				? ((IStateManager)_viewState).SaveViewState()
				: null;
		}

		/// <summary>
		/// Causes the <see cref="MixedPersister"/> object to track changes to its state so that it can be persisted across requests.
		/// </summary>
		protected virtual void TrackViewState()
		{
			_marked = true;

			if (_viewState != null)
			{
				((IStateManager)_viewState).TrackViewState();
			}

		}

		bool IStateManager.IsTrackingViewState
		{
			get { return IsTrackingViewState; }
		}

		void IStateManager.LoadViewState(object state)
		{
			LoadViewState(state);
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}

		#endregion

		#region IC1Cloneable Members

		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="MixedPersister"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);

			MixedPersister persister = copyFrom as MixedPersister;
			if (persister != null)
			{
				ViewState.SetDirty(true);

				IDictionaryEnumerator ien = persister.ViewState.GetEnumerator();
				while (ien.MoveNext())
				{
                    var stateItem = ien.Value as StateItem;
                    ViewState[(string)ien.Key] = stateItem == null ? ien.Value : stateItem.Value;
				}
			}
		}

		#endregion

		#region IJsonRestore memebers

		/// <summary>
		/// Restore the states from the JSON object.
		/// </summary>
		/// <param name="state">The state which is parsed from the JSON string.</param>
		protected override void RestoreStateFromJson(object state)
		{
			Hashtable ht = (Hashtable)state;

			if (ht != null)
			{
				Hashtable innerState;

				if ((innerState = ht["innerState"] as Hashtable) != null)
				{
					string viewState = (string)innerState["viewState"];

					if (viewState != null)
					{
						LoadViewState(new ObjectStateFormatter().Deserialize(viewState));
					}

					ht.Remove("innerState");
				}
			}

			base.RestoreStateFromJson(state);
		}
		#endregion
	}
}
