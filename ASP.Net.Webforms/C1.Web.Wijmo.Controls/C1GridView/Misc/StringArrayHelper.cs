﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	internal static class StringArrayHelper
	{
		public static bool Compare(string[] a, string[] b)
		{
			if (a != null || b != null)
			{
				if ((a == null || b == null) || (a.Length != b.Length))
				{
					return false;
				}

				for (int i = a.Length - 1; i >= 0; i--)
				{
					if (!string.Equals(a[i], b[i], StringComparison.Ordinal))
					{
						return false;
					}
				}
			}

			return true;
		}
	}

	internal static class StringHelper
	{
		public static string EncodeString(string[] values)
		{
			string res = string.Empty;

			foreach (string value in values)
				res += value.Length.ToString() + "," + value;

			return res;
		}

		public static string[] DecodeString(string value)
		{
			System.Collections.Generic.List<string> res = new System.Collections.Generic.List<string>();

			while (!string.IsNullOrEmpty(value))
			{
				int idx = value.IndexOf(',');
				int len = int.Parse(value.Substring(0, idx));
				res.Add(value.Substring(idx + 1, len));
				value = value.Substring(idx + 1 + len);
			}

			return res.ToArray();
		}
	}
}
