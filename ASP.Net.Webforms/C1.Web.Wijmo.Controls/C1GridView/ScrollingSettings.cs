﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	public partial class ScrollingSettings: Settings
	{
		/// <summary>
		/// Creates a new instance of the <see cref="ScrollingSettings"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="ScrollingSettings"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new ScrollingSettings();
		}
	}
}
