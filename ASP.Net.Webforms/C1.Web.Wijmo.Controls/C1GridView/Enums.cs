﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using System.Drawing.Design;

	/// <summary>
	/// Specifies the function of a row in the <see cref="C1GridView"/> control.
	/// </summary>
	public enum C1GridViewRowType
	{
		/// <summary>
		/// A header row of a data control. Header rows cannot be data-bound.
		/// </summary>
		Header,
		/// <summary>
		/// A footer row of a data control. Footer rows cannot be data-bound.
		/// </summary>
		Footer,
		/// <summary>
		/// A data row of a data control. Only DataRow rows can be data-bound.
		/// </summary>
		DataRow,
		/*
		/// <summary>
		/// A row that displays page buttons or a pager control. Pager rows cannot be data-bound.
		/// </summary>
		Pager,
		*/
		/// <summary>
		/// A row that displays page buttons or a pager control. Pager rows cannot be data-bound.
		/// </summary>
		EmptyDataRow,

		/// <summary>
		/// A row that displays the filter bar. Filter rows cannot be data-bound.
		/// </summary>
		Filter,
		/// <summary>
		/// A detail row of a data control.
		/// </summary>
		DetailRow
	}

	/// <summary>
	/// Specifies the state of a row in the <see cref="C1GridView"/> control.
	/// </summary>
	[Flags]
	public enum C1GridViewRowState
	{
		/// <summary>
		/// Indicates that the data control row is in a normal state. The Normal state is mutually exclusive with other states except the Alternate state.
		/// </summary>
		Normal = 0,
		/// <summary>
		/// Indicates that the data control row is an alternate row. 
		/// </summary>
		Alternate = 0x01,
		/// <summary>
		/// Indicates that the row has been selected by the user.
		/// </summary>
		Selected = 0x02,
		/// <summary>
		/// Indicates that the row is in an edit state, often the result of clicking an edit button for the row. Typically, the Edit and Insert states are mutually exclusive.
		/// </summary>
		Edit = 0x04
	}

	internal enum ItemStyleType
	{
		Control = 0,
		Filter,
		Footer,
		Header,
		Item
	}

	/// <summary>
	/// Indicates drop position.
	/// </summary>
	public enum C1GridViewDropPosition
	{
		/// <summary>
		/// Left.
		/// </summary>
		Left = 0,

		/// <summary>
		/// Right.
		/// </summary>
		Right,

		/// <summary>
		/// Center.
		/// </summary>
		Center
	}

	/// <summary>
	/// Filter mode.
	/// </summary>
	public enum FilterMode
	{
		/// <summary>
		/// Filtering is performed automatically.
		/// </summary>
		Auto = 0,

		/// <summary>
		/// Filtering is performed manually.
		/// </summary>
		Custom
	}

	/// <summary>
	/// Use the members of this enumeration to determine the actions that can be performed by the <see cref="C1GridView"/> control using callbacks.
	/// </summary>
	#if ASP_NET45
	[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.UI.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
	[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.UI.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
	[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.UI.Design.3", typeof(UITypeEditor))]
#endif
	[Flags]
	public enum CallbackAction
	{
		/// <summary>
		/// All actions are performed via postback.
		/// </summary>
		None = 0x0,

		/// <summary>
		/// Column moving is performed via callbacks.
		/// </summary>
		ColMove = 0x01,

		/// <summary>
		/// Editing is performed via callbacks.
		/// </summary>
		Editing = 0x02,

		/// <summary>
		/// Paging is performed via callbacks.
		/// </summary>
		Paging = 0x08,

		/// <summary>
		/// Row selection is performed via callbacks.
		/// </summary>
		Selection = 0x10,

		/// <summary>
		/// Sorting is performed via callbacks.
		/// </summary>
		Sorting = 0x20,

		/// <summary>
		/// Filtering is performed via callbacks.
		/// </summary>
		Filtering = 0x80,

		/// <summary>
		/// Grouping is performed via callbacks.
		/// </summary>
		Grouping = 0x100,

		/// <summary>
		/// Scrolling is performed via callbacks.
		/// </summary>
		/// <remarks>
		/// Used in associatation with the <see cref="C1GridView.ScrollingSettings.VirtualizationSettings.Mode"/> property. Please check the <see cref="VirtualizationMode"/> enumeration for more details.
		/// </remarks>
		Scrolling = 0x200,

		/// <summary>
		/// Expanding and collapsing of hierarcy nodes is performed via callbacks,
		/// </summary>
		Hierarchy = 0x400,

		/// <summary>
		/// Exporting is performed via callbacks.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		Exporting = 0x800,

		/// <summary>
		/// All actions are performed via callbacks.
		/// </summary>
		All = ColMove | Editing | Paging | Selection | Sorting | Filtering | Grouping | Scrolling | Hierarchy
	}

	/// <summary>
	/// Callback mode.
	/// </summary>
	public enum CallbackMode
	{
		/// <summary>
		/// Callback action is performed using partial update of control's HTML content.
		/// </summary>
		Partial = 0,

		/// <summary>
		/// Callback action is performed using full update of control's HTML content.
		/// </summary>
		Full = 1
	}

	/// <summary>
	/// Determines the method of sending client-side edited data to server.
	/// </summary>
	public enum ClientEditingUpdateMode
	{
		/// <summary>
		/// Edits done in a row are sent to server automatically when current row is changed.
		/// </summary>
		AutoRowEdit = 0,

		/// <summary>
		/// Edits done in a row are not automatically sent. Developer should explicitly call CSOM update() method to force sending the changes.
		/// </summary>
		Manual = 1
	}


}
