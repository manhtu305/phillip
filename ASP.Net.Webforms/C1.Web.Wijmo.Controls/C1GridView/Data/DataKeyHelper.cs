﻿using System;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1GridView.Data
{
	internal class DataKeyHelper
	{
		static internal string[] ParseKey(string keyFields)
		{
			if (keyFields == null || keyFields.Length == 0)
			{
				return new string[0];
			}

			string[] keyFieldNames = keyFields.Trim().Split(',');

			for (int i = 0; i < keyFieldNames.Length; i++)
			{
				string keyFieldName = keyFieldNames[i].Trim();
				if (keyFieldName.StartsWith("[") && keyFieldName.EndsWith("]"))
				{
					keyFieldName = keyFieldName.Substring(1, keyFieldName.Length - 2).Trim();
				}

				keyFieldNames[i] = keyFieldName;
			}

			return keyFieldNames;
		}
		static internal object EvaluateKey(object dataItem, string[] keyFields)
		{
			if (keyFields == null || keyFields.Length == 0)
			{
				return null;
			}

			if (keyFields.Length == 1)
			{
				return DataBinder.GetPropertyValue(dataItem, keyFields[0]);
			}

			object[] keyValues = new object[keyFields.Length];
			for (int i = 0; i < keyFields.Length; i++)
			{
				keyValues[i] = DataBinder.GetPropertyValue(dataItem, keyFields[i]);
			}

			return keyValues;
		}
	}
}
