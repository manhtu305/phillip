﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1GridView.Data
{
	internal class DataSourceHelper
	{
		private class FieldInfo
		{
			internal PropertyDescriptor Prop;
			internal object KeyValue;
			internal object Value;
			internal FieldInfo(PropertyDescriptor prop)
			{
				Prop = prop;
			}
		}

		// member fields
		private IList _list;
		private PropertyDescriptorCollection _props;
		private Hashtable _fieldInfos = new Hashtable();
		private int _position = -1;

		// constructor
		internal DataSourceHelper(object list)
		{
			_list = list as IList;
			if (_list == null)
			{
				throw new Exception("Invalid DataSource");
			}

			ITypedList typedList = _list as ITypedList;
			if (typedList == null)
			{
				throw new Exception("Invalid DataSource");
			}

			_props = typedList.GetItemProperties(new PropertyDescriptor[0]);
			if (_props == null || _props.Count == 0)
			{
				throw new Exception("Now fields in data source");
			}
		}

		// methods
		internal bool SetKeyValue(string[] fieldNames, DataKey value)
		{
			_position = -1;

			if (fieldNames == null || fieldNames.Length == 0)
			{
				return false;
			}

			FieldInfo[] keyInfos = new FieldInfo[fieldNames.Length];
			for (int i = 0; i < fieldNames.Length; i++)
			{
				FieldInfo fieldInfo = GetFieldInfo(fieldNames[i]);
				if (fieldInfo == null)
				{
					throw new Exception("Invalid field: " + fieldNames[i]);
				}

				fieldInfo.KeyValue = value.Values[i];

				keyInfos[i] = fieldInfo;
			}

			for (int rowNum = 0; rowNum < _list.Count; rowNum++)
			{
				object row = _list[rowNum];
				bool match = true;

				for (int i = 0; match && i < fieldNames.Length; i++)
				{
					FieldInfo keyInfo = keyInfos[i];
					object rowKeyVal = keyInfo.Prop.GetValue(row);
					if (!rowKeyVal.Equals(keyInfo.KeyValue))
					{
						match = false;
					}
				}

				if (match)
				{
					_position = rowNum;
					break;
				}
			}

			return _position != -1;
		}

		internal void SetFieldValue(string fieldName, object value)
		{
			FieldInfo fieldInfo = GetFieldInfo(fieldName);
			
			if (fieldInfo == null)
			{
				throw new Exception("Invalid field: " + fieldName);
			}

			if (fieldInfo.Prop.PropertyType != value.GetType())
			{
				value = Convert.ChangeType(value, fieldInfo.Prop.PropertyType);
			}

			fieldInfo.Value = value;
		}

		internal void Update()
		{
			if (_position < 0 || _position > _list.Count)
			{
				throw new Exception("Row index for update not found in the data source");
			}

			foreach (DictionaryEntry entry in _fieldInfos)
			{
				FieldInfo fieldInfo = (FieldInfo)entry.Value;
				if (fieldInfo.Value == null)
				{
					continue;
				}
				fieldInfo.Prop.SetValue(_list[_position], fieldInfo.Value);
			}
		}

		internal void Delete()
		{
			if (_position < 0 || _position > _list.Count)
			{
				throw new Exception("Row index for delete not found in the data source");
			}

			_list.RemoveAt(_position);
		}

		// private methods
		private FieldInfo GetFieldInfo(string fieldName)
		{
			FieldInfo fieldInfo = _fieldInfos[fieldName] as FieldInfo;
			
			if (fieldInfo == null)
			{
				PropertyDescriptor prop = _props.Find(fieldName, true);
				if (prop == null)
				{
					return null;
				}

				fieldInfo = new FieldInfo(prop);
				_fieldInfos.Add(fieldName, fieldInfo);
			}

			return fieldInfo;
		}

		// properties
		internal int Position { get { return _position; } }
	}
}
