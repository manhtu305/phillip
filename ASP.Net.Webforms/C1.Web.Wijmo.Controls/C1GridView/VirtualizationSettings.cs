﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	public partial class VirtualizationSettings : Settings
	{
		/// <summary>
		/// Creates a new instance of the <see cref="VirtualizationSettings"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="VirtualizationSettings"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new VirtualizationSettings();
		}
	}
}
