﻿using System;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1GridView
{
#if false
	using System.Collections;

	public abstract class ViewStatePersister : IStateManager, IC1Cloneable   
	{
		private StateBag _viewState;
		private bool _marked = false;

		public ViewStatePersister() : this(true)
		{
		}

		public ViewStatePersister(bool track)
		{
			if (track)
			{
				//TrackViewState();
			}
		}

		protected V GetPropertyValue<V>(string name, V nullValue)
		{
			object value;

			return (_viewState != null && ((value = _viewState[name]) != null))
				? (V)value
				: nullValue;
		}

		protected void SetPropertyValue<V>(string name, V value)
		{
			ViewState[name] = value;
		}

		protected internal virtual void SetDirty()
		{
			if (_viewState != null)
			{
				_viewState.SetDirty(true);
			}
		}

		protected bool IsTrackingViewState
		{
			get { return _marked; }
		}

		protected StateBag ViewState
		{
			get
			{
				if (_viewState == null)
				{
					_viewState = new StateBag();
					if (IsTrackingViewState)
					{
						((IStateManager)_viewState).TrackViewState();
					}
				}

				return _viewState;
			}
		}

		protected virtual void LoadViewState(object state)
		{
			if (state != null)
			{
				((IStateManager)ViewState).LoadViewState(state);
			}
		}

		protected virtual object SaveViewState()
		{
			return (_viewState != null)
				? ((IStateManager)_viewState).SaveViewState()
				: null;
		}

		protected virtual void TrackViewState()
		{
			_marked = true;

			if (_viewState != null)
			{
				((IStateManager)_viewState).TrackViewState();
			}

		}

		#region IStateManager Members

		bool IStateManager.IsTrackingViewState
		{
			get { return IsTrackingViewState; }
		}

		void IStateManager.LoadViewState(object state)
		{
			LoadViewState(state);
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}

		#endregion

		#region IC1Cloneable Members

		protected internal abstract ViewStatePersister CreateInstance();

		IC1Cloneable IC1Cloneable.CreateInstance()
		{
			return CreateInstance();
		}

		protected internal virtual void CopyFrom(ViewStatePersister copyFrom)
		{
			ViewState.SetDirty(true);

			IDictionaryEnumerator ien = copyFrom.ViewState.GetEnumerator();
			while (ien.MoveNext())
			{
				ViewState[(string)ien.Key] = ien.Value;
			}
		}

		void IC1Cloneable.CopyFrom(object copyFrom)
		{
			ViewStatePersister persiter = copyFrom as ViewStatePersister;
			if (persiter != null)
			{
				CopyFrom(persiter);
			}
		}

		#endregion

		#region ICloneable Members

		protected internal ViewStatePersister Clone()
		{
			ViewStatePersister persister = CreateInstance();
			persister.CopyFrom(this);
			return persister;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		#endregion
	}
#endif
}
