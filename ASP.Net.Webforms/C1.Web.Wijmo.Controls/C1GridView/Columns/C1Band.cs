﻿using System;
using System.Collections;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Collections.Generic;

	public partial class C1Band : C1BaseField 
	{
		internal static class Helper
		{
			public static C1Band GetVisibleParent(C1BaseField value)
			{
				IOwnerable tmp = value;

				while (tmp.Owner != null)
				{
					tmp = tmp.Owner;
					
					C1Band band = tmp as C1Band;
					if (band != null && band.ParentVisibility)
					{
						return band;
					}
				}

				return null;
			}
		}

		/*private TableItemStyle _childControlStyle;
		private TableItemStyle _childFilterStyle;
		private TableItemStyle _childFooterStyle;
		private TableItemStyle _childHeaderStyle;
		private TableItemStyle _childItemStyle;*/

		/// <summary>
		/// Constructor. Creates a new instance of the <b>C1Band</b> class.
		/// </summary>
		public C1Band()	: this(null)
		{
		}

		internal C1Band(IColumnsContainer owner) : base(owner)
		{
		}

		/*
		/// <summary>
		/// Gets the style properties to be merged with <see cref="C1BaseField.ControlStyle" /> of the child columns.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle ChildControlStyle
		{
			get
			{
				if (_childControlStyle == null)
				{
					_childControlStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_childControlStyle).TrackViewState();
					}
				}

				return _childControlStyle;
			}
		}

		/// <summary>
		/// Gets the style properties to be merged with <see cref="C1BoundField.FilterStyle" /> of the child columns.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the filter section of the child columns.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle ChildFilterStyle
		{
			get
			{
				if (_childFilterStyle == null)
				{
					_childFilterStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_childFilterStyle).TrackViewState();
					}
				}

				return _childFilterStyle;
			}
		}

		/// <summary>
		/// Gets the style properties to be merged with <see cref="C1BaseField.FooterStyle" /> of the child columns.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the footer section of the child columns.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle ChildFooterStyle
		{
			get
			{
				if (_childFooterStyle == null)
				{
					_childFooterStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_childFooterStyle).TrackViewState();
					}
				}

				return _childFooterStyle;
			}
		}

		/// <summary>
		/// Gets the style properties to be merged with <see cref="C1BaseField.HeaderStyle" /> of the child columns.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the header section of the child columns.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle ChildHeaderStyle
		{
			get
			{
				if (_childHeaderStyle == null)
				{
					_childHeaderStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_childHeaderStyle).TrackViewState();
					}
				}

				return _childHeaderStyle;
			}
		}

		/// <summary>
		/// Gets the style properties to be merged with <see cref="C1BaseField.ItemStyle" /> of the child columns.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the items of the child columns.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle ChildItemStyle
		{
			get
			{
				if (_childItemStyle == null)
				{
					_childItemStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_childItemStyle).TrackViewState();
					}
				}

				return _childItemStyle;
			}
		}
		*/

		#region ViewState

		/// <summary>
		/// Saves the changes to the <see cref="C1Band"/> object since the time the page was posted back to the server.
		/// </summary>
		/// <returns>The object that contains the changes to the view state of the <see cref="C1Band"/>.</returns>
		protected override object SaveViewState()
		{
			Pair state = new Pair(base.SaveViewState(), _columns != null ? ((IStateManager)_columns).SaveViewState() : null);

			return (state.First != null || state.Second != null)
				? state
				: null;

			/*object[] state = new object[] {
				base.SaveViewState(),
				_columns != null ? ((IStateManager)_columns).SaveViewState() : null,
				_childControlStyle != null ? ((IStateManager)_childControlStyle).SaveViewState() : null,
				_childFilterStyle != null ? ((IStateManager)_childFilterStyle).SaveViewState() : null,
				_childFooterStyle != null ? ((IStateManager)_childFooterStyle).SaveViewState() : null,
				_childHeaderStyle != null ? ((IStateManager)_childHeaderStyle).SaveViewState() : null,
				_childItemStyle != null ? ((IStateManager)_childItemStyle).SaveViewState() : null
			};

			return Array.TrueForAll(state, v => v == null)
				? null
				: state;*/
		}

		/// <summary>
		/// Restores the previously saved view state of the <see cref="C1Band"/> object.
		/// </summary>
		/// <param name="state">An object that represents the <see cref="C1Band"/> state to restore.</param>
		protected override void LoadViewState(object state)
		{
			if (state != null)
			{
				Pair stateObj = (Pair)state;

				if (stateObj.First != null)
				{
					base.LoadViewState(stateObj.First);
				}

				if (stateObj.Second != null)
				{
					((IStateManager)Columns).LoadViewState(stateObj.Second);
				}

				/*object[] stateObj = (object[])state;
				int len = stateObj.Length;

				if (len > 0)
				{
					base.LoadViewState(stateObj[0]);
				}

				if (len > 1)
				{
					((IStateManager)Columns).LoadViewState(stateObj[1]);
				}

				if (len > 2)
				{
					((IStateManager)ChildControlStyle).LoadViewState(stateObj[2]);
				}

				if (len > 3)
				{
					((IStateManager)ChildFilterStyle).LoadViewState(stateObj[3]);
				}

				if (len > 4)
				{
					((IStateManager)ChildFooterStyle).LoadViewState(stateObj[4]);
				}

				if (len > 5)
				{
					((IStateManager)ChildHeaderStyle).LoadViewState(stateObj[5]);
				}

				if (len > 6)
				{
					((IStateManager)ChildItemStyle).LoadViewState(stateObj[6]);
				}*/
			}
		}

		/// <summary>
		/// Causes the <see cref="C1Band"/> object to track changes to its state so that it can be persisted across requests.
		/// </summary>
		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (_columns != null)
			{
				((IStateManager)_columns).TrackViewState();
			}

			/*if (_childControlStyle != null)
			{
				((IStateManager)_childControlStyle).TrackViewState();
			}

			if (_childFilterStyle != null)
			{
				((IStateManager)_childFilterStyle).TrackViewState();
			}

			if (_childFooterStyle != null)
			{
				((IStateManager)_childFooterStyle).TrackViewState();
			}

			if (_childHeaderStyle != null)
			{
				((IStateManager)_childHeaderStyle).TrackViewState();
			}

			if (_childItemStyle != null)
			{
				((IStateManager)_childItemStyle).TrackViewState();
			}*/
		}

		#endregion

		internal override TableItemStyle GetCompoundStyle(ItemStyleType type)
		{
			TableItemStyle result = base.GetCompoundStyle(type);

			/*switch (type)
			{
				case StyleType.Control:
					if (_childControlStyle != null)
					{
						result.CopyFrom(_childControlStyle);
					}
					break;

				case StyleType.Footer:
					if (_childFooterStyle != null)
					{
						result.CopyFrom(_childFooterStyle);
					}
					break;

				case StyleType.Header:
					if (_childHeaderStyle != null)
					{
						result.CopyFrom(_childHeaderStyle);
					}
					break;

				case StyleType.Item:
					if (_childItemStyle != null)
					{
						result.CopyFrom(_childItemStyle);
					}
					break;

				case StyleType.Filter:
					if (_childFilterStyle != null)
					{
						result.CopyFrom(_childFilterStyle);
					}
					break;
			}*/

			return result;
		}

		/// <summary>
		/// Marks the state of the <see cref="C1Band"/> object as having been changed.
		/// </summary>
		protected internal override void SetDirty()
		{
			base.SetDirty();

			if (_columns != null)
			{
				_columns.SetDirty();
			}

			/*if (_childControlStyle != null)
			{
				_childControlStyle.SetDirty();
			}

			if (_childFilterStyle != null)
			{
				_childFilterStyle.SetDirty();
			}

			if (_childFooterStyle != null)
			{
				_childFooterStyle.SetDirty();
			}

			if (_childHeaderStyle != null)
			{
				_childHeaderStyle.SetDirty(); 
			}

			if (_childItemStyle != null)
			{
				_childItemStyle.SetDirty(); 
			}*/
		}

		#region IC1Cloneable

		/// <summary>
		/// Creates a new instance of the <see cref="C1Band"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1Band"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1Band();
		}


		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="C1Band"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);

			C1Band field = copyFrom as C1Band;
			if (field != null)
			{
				Columns.Clear();

				if (field._columns != null)
				{
					Columns.CopyFrom(field.Columns);
				}

				if (IsTrackingViewState)
				{
					((IStateManager)Columns).TrackViewState();
				}

				/*ChildControlStyle.Reset();
				ChildControlStyle.CopyFrom(field.ChildControlStyle);

				ChildFilterStyle.Reset();
				ChildFilterStyle.CopyFrom(field.ChildFilterStyle);

				ChildFooterStyle.Reset();
				ChildFooterStyle.CopyFrom(field.ChildFooterStyle);

				ChildHeaderStyle.Reset();
				ChildHeaderStyle.CopyFrom(field.ChildHeaderStyle);

				ChildItemStyle.Reset();
				ChildItemStyle.CopyFrom(field.ChildItemStyle);*/
			}
		}


		#endregion

		#region IJsonRestore members

		///// <summary>
		///// Infrastructure.
		///// </summary>
		//[Browsable(false)]
		//[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public override Hashtable InnerState
		//{
		//	get
		//	{
		//		Hashtable ht = base.InnerState;

		//		if (this.HeaderStyleCreated)
		//		{
		//			ht["headerattrstyle"] = HeaderStyle.ToStyleString(GridView);
		//		}

		//		return ht;
		//	}
		//}

		#endregion
	}
}
