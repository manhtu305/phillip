﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents a base class for all filterable fields.
	/// </summary>
	public abstract class C1FilterField : C1Field
	{
		private const string DEF_DATAFIELD = "";
		private const string DEF_FILTERCUSTOMOPERATOR = "";
		private const FilterOperator DEF_FILTEROPERATOR = FilterOperator.Contains;
		protected const string DEF_FILTERVALUE = "";
		private const string DEF_DATAFORMATSTRING = "";
		private const bool DEF_SHOWFILTER = true;

		private TableItemStyle _filterStyle;
		private string _newFilterValue = null;
		private FilterOperator? _newFilterOperator = null;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1FilterField"/> class.
		/// </summary>
		public C1FilterField() : this(null)
		{
		}

		internal C1FilterField(IOwnerable owner) : base(owner)
		{
		}


		/// <summary>
		/// Gets or sets the name of the field in the database table to which a grid column is bound.
		/// </summary>
		[DefaultValue(DEF_DATAFIELD)]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof(System.Web.UI.Design.DataSourceViewSchemaConverter))]
		[Json(true, true, DEF_DATAFIELD)]
		public virtual string DataField
		{
			get { return GetPropertyValue("DataField", DEF_DATAFIELD); }
			set
			{
				if (GetPropertyValue("DataField", DEF_DATAFIELD) != value)
				{
					SetPropertyValue("DataField", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Json(true, true, "string")]
		public string DataType
		{
			get
			{
				Type dataType = _DataType;

				return (dataType != null)
					? ConvertDataTypeToClientDataType(dataType)
					: string.Empty;
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Json(true, true, "")]
		public string DataTypeQualifier
		{
			get
			{
				if (_DataType == typeof(TimeSpan))
				{
					return "timespan";
				}

				if (_DataType._IsReal())
				{
					return "real";
				}

				return string.Empty;
			}
		}

		/// <summary>
		/// Gets or sets value that will be used as filter operator when the <see cref="FilterOperator"/> property
		/// is equal to the <see cref="C1.Web.Wijmo.Controls.C1GridView.FilterOperator.Custom"/> value.
		/// </summary>
		/// <value>
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		/// <remarks>
		/// The string can be any literal string and can contain two placeholders:
		/// <see cref="DataField"/> name and current <see cref="FilterValue"/> value.
		/// </remarks>
		/// <example>
		/// "[{0}] LIKE '{1}*'"
		/// </example>
		[DefaultValue(DEF_FILTERCUSTOMOPERATOR)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_FILTERCUSTOMOPERATOR)]
		public virtual string FilterCustomOperator
		{
			get { return GetPropertyValue("FilterCustomOperator", DEF_FILTERCUSTOMOPERATOR); }
			set
			{
				if (GetPropertyValue("FilterCustomOperator", DEF_FILTERCUSTOMOPERATOR) != value)
				{
					SetPropertyValue("FilterCustomOperator", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets filter operator used for filtering.
		/// </summary>
		/// <value>
		/// One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.FilterOperator"/> values.
		/// The default value is <see cref="C1.Web.Wijmo.Controls.C1GridView.FilterOperator.Contains"/>.
		/// </value>
		[DefaultValue(DEF_FILTEROPERATOR)]
		[NotifyParentProperty(true)]
		[Json(true, true, FilterOperator.NoFilter)] // <- don't use DEF_FILTEROPERATOR here because the default value of the wijmo.c1field.filterOperator is "nofilter"
		public virtual FilterOperator FilterOperator
		{
			get { return GetPropertyValue("FilterOperator", DEF_FILTEROPERATOR); }
			set
			{
				if (GetPropertyValue("FilterOperator", DEF_FILTEROPERATOR) != value)
				{
					SetPropertyValue("FilterOperator", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets the style properties of the filter item.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the filter item of the <see cref="C1GridView"/> control.
		/// </remarks>		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle FilterStyle
		{
			get
			{
				if (_filterStyle == null)
				{
					_filterStyle = new TableItemStyle();

					if (IsTrackingViewState)
					{
						((IStateManager)_filterStyle).TrackViewState();
					}
				}

				return _filterStyle;
			}
		}

		/// <summary>
		/// Gets or sets a value of the currently applied filter.   
		/// </summary>
		/// <value>
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// When filtering is done this property is automatically updated with the value inputted by user
		/// to edit box of current column in filter row.
		/// </value>
		[DefaultValue(DEF_FILTERVALUE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_FILTERVALUE)]
		public virtual string FilterValue
		{
			get { return GetPropertyValue("FilterValue", DEF_FILTERVALUE); }
			set
			{
				if (GetPropertyValue("FilterValue", DEF_FILTERVALUE) != value)
				{
					SetPropertyValue("FilterValue", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets value indicating whether filter text box will be shown in the filter row of the
		/// <see cref="C1GridView"/> control or not.
		/// </summary>
		/// <value>
		/// The default value is true.
		/// </value>
		[DefaultValue(DEF_SHOWFILTER)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_SHOWFILTER)]
		public virtual bool ShowFilter
		{
			get { return GetPropertyValue("ShowFilter", DEF_SHOWFILTER); }
			set
			{
				if (GetPropertyValue("ShowFilter", DEF_SHOWFILTER) != value)
				{
					SetPropertyValue("ShowFilter", value);
					OnFieldChanged();
				}
			}
		}

		#region internal properties

		internal string NewFilterValue
		{
			get { return _newFilterValue; }
			set { _newFilterValue = value; }
		}

		internal FilterOperator? NewFilterOperator
		{
			get { return _newFilterOperator; }
		}

		#endregion

		#region protected

		/// <summary>
		/// Gets value of the filter formatted according <see cref="FilterOperator"/>.
		/// </summary>
		/// <returns>Value of the filter formatted according <see cref="FilterOperator"/>.</returns>
		protected internal virtual string GetFilterExpression()
		{
			string result = string.Empty;

			Type dataType = _DataType;
			string dataField = DataField;
			FilterOperator filterOperator = FilterOperator;
			string filterValue = EscapeFilterValue(FilterValue);
			string operatorPattern = "{0}";

			if (dataType == typeof(string) || dataType == typeof(char))
			{
				operatorPattern = "'{0}'";
			}
			else if (dataType._IsDateTime())
			{
				operatorPattern = "#{0}#";

				if (filterOperator == FilterOperator.Contains ||
					filterOperator == FilterOperator.NotContain ||
					filterOperator == FilterOperator.BeginsWith ||
					filterOperator == FilterOperator.EndsWith)
				{
					filterOperator = FilterOperator.Equals;
				}
			}
			else
			{
				if (filterOperator == FilterOperator.Contains ||
					filterOperator == FilterOperator.NotContain ||
					filterOperator == FilterOperator.BeginsWith ||
					filterOperator == FilterOperator.EndsWith)
				{
					filterOperator = FilterOperator.Equals;
				}
			}

			if (!string.IsNullOrEmpty(FilterValue))
			{
				switch (filterOperator)
				{
					case FilterOperator.Contains:
						result = string.Format("[{0}] LIKE '*{1}*'", dataField, filterValue);
						break;

					case FilterOperator.NotContain:
						result = string.Format("[{0}] NOT LIKE '*{1}*'", dataField, filterValue);
						break;

					case FilterOperator.BeginsWith:
						result = string.Format("[{0}] LIKE '{1}*'", dataField, filterValue);
						break;

					case FilterOperator.EndsWith:
						result = string.Format("[{0}] LIKE '*{1}'", dataField, filterValue);
						break;

					case FilterOperator.Equals:
						result = string.Format("[{0}] = {1}", dataField, string.Format(operatorPattern, filterValue));
						break;

					case FilterOperator.NotEqual:
						result = string.Format("[{0}] <> {1}", dataField, string.Format(operatorPattern, filterValue));
						break;

					case FilterOperator.Greater:
						result = string.Format("[{0}] > {1}", dataField, string.Format(operatorPattern, filterValue));
						break;

					case FilterOperator.Less:
						result = string.Format("[{0}] < {1}", dataField, string.Format(operatorPattern, filterValue));
						break;

					case FilterOperator.GreaterOrEqual:
						result = string.Format("[{0}] >= {1}", dataField, string.Format(operatorPattern, filterValue));
						break;

					case FilterOperator.LessOrEqual:
						result = string.Format("[{0}] <= {1}", dataField, string.Format(operatorPattern, filterValue));
						break;

					case FilterOperator.Custom:
						result = string.Format(FilterCustomOperator, dataField, filterValue);
						break;
				}
			}

			switch (filterOperator)
			{
				case FilterOperator.IsEmpty:
					result = string.Format("([{0}] = '') OR ([{0}] is null)", dataField);
					break;

				case FilterOperator.NotIsEmpty:
					result = string.Format("([{0}] <> '') AND ([{0}] not is null)", dataField);
					break;

				case FilterOperator.IsNull:
					result = string.Format("[{0}] is null", dataField);
					break;

				case FilterOperator.NotIsNull:
					result = string.Format("[{0}] is not null", dataField);
					break;
			}

			return result;
		}

		#endregion

		#region IJsonRestore members, InnerState

		internal Type _DataType
		{
			get { return GetViewStatePropertyValue("DataType", typeof(string)); }
			set { SetViewStatePropertyValue("DataType", value); }
		}

		private string ConvertDataTypeToClientDataType(Type dataType)
		{
			// number, datetime, boolean, currency, string
			if (dataType != null)
			{
				dataType = dataType._UnderlyingType();

				if (dataType == typeof(bool))
				{
					return "boolean";
				}

				if (dataType == typeof(DateTime) || dataType == typeof(TimeSpan))
				{
					return "datetime";
				}

				if (dataType._IsNumeric())
				{
					return "number";
				}
			}

			return "string";
		}
		/// <summary>
		/// Restore the states from the JSON object.
		/// </summary>
		/// <param name="state">The state which is parsed from the JSON string.</param>
		protected override void RestoreStateFromJson(object state)
		{
			Hashtable ht = state as Hashtable;
			if (ht != null)
			{
				Hashtable innerState = ht["innerState"] as Hashtable;
				if (innerState != null)
				{
					object val;

					if ((val = innerState["newFilterValue"]) != null)
					{
						// use an invariant culture to build the DataView.RowFilter expression in future.
						_newFilterValue = (val is IFormattable)
							? ((IFormattable)val).ToString(string.Empty, System.Globalization.CultureInfo.InvariantCulture)
							: val.ToString();
					}

					if ((val = innerState["newFilterOperator"]) != null)
					{
						_newFilterOperator = (FilterOperator)Enum.Parse(typeof(FilterOperator), (string)val, true);
					}
				}
			}

			base.RestoreStateFromJson(state);
		}

		#endregion

		#region private members

		private string EscapeFilterValue(string filterValue)
		{
			if (!string.IsNullOrEmpty(filterValue))
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder();

				for (int i = 0; i < filterValue.Length; i++)
				{
					char ch = filterValue[i];
					switch (ch)
					{
						case '%':
						case '*':
						case '[':
						case ']':
							sb.Append('[' + ch + ']');
							break;

						case '\'':
							sb.Append("''");
							break;

						default:
							sb.Append(ch);
							break;
					}
				}

				return sb.ToString();
			}

			return filterValue;
		}

		#endregion

		#region IC1Cloneable
		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="C1FilterField"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);

			C1FilterField field = copyFrom as C1FilterField;
			if (field != null)
			{
				FilterStyle.Reset();
				if (field._filterStyle != null)
				{
					FilterStyle.CopyFrom(field.FilterStyle);
				}
			}
		}

		#endregion

		#region IStateManager members

		/// <summary>
		/// Restores the previously saved view state of the <see cref="C1FilterField"/> object.
		/// </summary>
		/// <param name="state">An object that represents the <see cref="C1FilterField"/> state to restore.</param>
		protected override void LoadViewState(object state)
		{
			if (state != null)
			{
				Pair stateObj = (Pair)state;

				base.LoadViewState(stateObj.First);

				if (stateObj.Second != null)
				{
					((IStateManager)FilterStyle).LoadViewState(stateObj.Second);
				}
			}
		}

		/// <summary>
		/// Saves the changes to the <see cref="C1FilterField"/> object since the time the page was posted back to the server.
		/// </summary>
		/// <returns>The object that contains the changes to the view state of the <see cref="C1FilterField"/>.</returns>
		protected override object SaveViewState()
		{
			Pair state = new Pair(base.SaveViewState(),
				_filterStyle != null ? ((IStateManager)_filterStyle).SaveViewState() : null);

			return (state.First != null || state.Second != null)
				? state
				: null;
		}

		/// <summary>
		/// Causes the <see cref="C1FilterField"/> object to track changes to its state so that it can be persisted across requests.
		/// </summary>
		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (_filterStyle != null)
			{
				((IStateManager)_filterStyle).TrackViewState();
			}
		}


		#endregion
	}
}
