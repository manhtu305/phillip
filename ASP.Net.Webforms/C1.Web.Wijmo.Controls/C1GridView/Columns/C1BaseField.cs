﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Collections.Specialized;
	using System.Drawing.Design;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.Web.UI.WebControls;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// This is the base class for all fields.
	/// </summary>
	public abstract partial class C1BaseField : MixedPersister, IDataSourceViewSchemaAccessor
	{
		private const string DEF_ACCESSIBLEHEADERTEXT = "";
		// private const bool DEF_FIXED = false;
		private const string DEF_HEADERIMAGEURL = "";

		private TableItemStyle _controlStyle;
		private TableItemStyle _footerStyle;
		private TableItemStyle _headerStyle;
		private TableItemStyle _itemStyle;

		private C1GridView _gridView;

		private bool _parentVisibility = true;
		private int _originalIndex = -1;

		internal static bool IsNull(object value) {
			return value == null || Convert.IsDBNull(value);
		}

		/// <summary>
		/// Constructor. Creates a new instance of the <b>C1BaseField</b> class.
		/// </summary>
		public C1BaseField() : this(null)
		{
		}

		internal C1BaseField(/*IColumnsContainer*/IOwnerable owner) : base()
		{
			_owner = owner;
		}

		#region properties
		/// <summary>
		/// Gets or sets text that is rendered as the <b>abbr</b> attribute of the column's header cell.
		/// </summary>
		[DefaultValue(DEF_ACCESSIBLEHEADERTEXT)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ACCESSIBLEHEADERTEXT)]
		//[WidgetOption()]
		public virtual string AccessibleHeaderText
		{
			get { return GetPropertyValue("AccessibleHeaderText", DEF_ACCESSIBLEHEADERTEXT); }
			set
			{
				if (GetPropertyValue("AccessibleHeaderText", DEF_ACCESSIBLEHEADERTEXT) != value)
				{
					SetPropertyValue("AccessibleHeaderText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets the style of any Web server controls contained by the <see cref="C1BaseField"/> object.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public TableItemStyle ControlStyle
		{
			get
			{
				if (_controlStyle == null)
				{
					_controlStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_controlStyle).TrackViewState();
					}
				}

				return _controlStyle;
			}
		}

		/* TODO
		/// <summary>
		/// Determines whether a column is fixed to the left of the grid when it is scrolled horizontally.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The default value of this property is false. When this property is set to true for a column,
		/// all columns to the left are also fixed. This property is usually used with the  <see cref="C1GridView.ScrollSettings"/> property.
		/// </para>
		/// <para>
		/// This property is ignored under group mode.
		/// </para>
		/// <para>
		/// If child column of some <see cref="C1Band"/> is fixed then the top
		/// and right-most column of the root <see cref="C1Band"/> contained current column will be fixed.
		/// </para>
		/// </remarks>
		[DefaultValue(DEF_FIXED)]
		[NotifyParentProperty(true)]
		//[WidgetOption()]
		[Json(true, true, DEF_FIXED)]
		public virtual bool Fixed
		{
			get { return GetPropertyValue("Fixed", DEF_FIXED); }
			set
			{
				if (GetPropertyValue("Fixed", DEF_FIXED) != value)
				{
					SetPropertyValue("Fixed", value);
					OnFieldChanged();
				}
			}
		}*/

		/// <summary>
		/// Gets the style properties of the column footer.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the footer section of the column.
		/// </remarks>		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public virtual TableItemStyle FooterStyle
		{
			get
			{
				if (_footerStyle == null)
				{
					_footerStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_footerStyle).TrackViewState();
					}
				}

				return _footerStyle;
			}
		}

		/// <summary>
		/// Gets or sets the location of the header image.
		/// </summary>
		[DefaultValue(DEF_HEADERIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_HEADERIMAGEURL)]
		public virtual string HeaderImageUrl
		{
			get { return GetPropertyValue("HeaderImageUrl", DEF_HEADERIMAGEURL); }
			set
			{
				if (GetPropertyValue("HeaderImageUrl", DEF_HEADERIMAGEURL) != value)
				{
					SetPropertyValue("HeaderImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets the style properties of the column header.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for the header section of the column.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public virtual TableItemStyle HeaderStyle
		{
			get
			{
				if (_headerStyle == null)
				{
					_headerStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_headerStyle).TrackViewState();
					}
				}

				return _headerStyle;
			}
		}

		/// <summary>
		/// Gets the style properties of the item.
		/// </summary>
		/// <remarks>
		/// This property is used to provide a custom style for items of the <see cref="C1GridView"/> component.
		/// </remarks>		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Browsable(false)] // << use property page for editing this
		public virtual TableItemStyle ItemStyle
		{
			get
			{
				if (_itemStyle == null)
				{
					_itemStyle = new TableItemStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_itemStyle).TrackViewState();
					}
				}

				return _itemStyle;
			}
		}

		#endregion

		#region members

		internal virtual bool IsLeaf
		{
			get;
			set;
		}

		internal virtual bool IsAutogenerated
		{
			get;
			set;
		}

		internal bool ParentVisibility
		{
			get { return _parentVisibility && Visible; }
			set { _parentVisibility = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		protected C1GridView GridView
		{
			get { return _gridView; }
		}

		/// <summary>
		/// Gets a value indicating whether a control is being used on a design surface.
		/// </summary>
		protected bool DesignMode
		{
			get { return (_gridView != null && _gridView.DesignMode); }
		}

		/// <summary>
		/// Fills the specified <see cref="IOrderedDictionary"/> object with values from the specified <see cref="C1GridViewCell"/> object.
		/// </summary>
		/// <param name="dictionary">A <see cref="IOrderedDictionary"/> used to store the values of the specified cell.</param>/// 
		/// <param name="cell">The <see cref="C1GridViewCell"/> object that contains the values to retrieve.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		/// <param name="includeReadOnly">True to include the values of read-only fields; otherwise, false.</param>
		public virtual void ExtractValuesFromCell(IOrderedDictionary dictionary, C1GridViewCell cell, C1GridViewRowState rowState, bool includeReadOnly)
		{
		}


		internal bool HeaderStyleCreated
		{
			get
			{
				return _headerStyle != null && !_headerStyle.IsEmpty;
			}
		}

		/// <summary>
		/// Performs basic initialization.
		/// </summary>
		/// <param name="gridView">C1GridView.</param>
		protected internal virtual void Initialize(C1GridView gridView)
		{
			_gridView = gridView;
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal virtual void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			switch (rowType)
			{
				case C1GridViewRowType.Header:
					InitializeHeaderCell(cell);
					break;

				case C1GridViewRowType.Footer:
					cell.Text = string.IsNullOrEmpty(FooterText) ? C1GridView.NON_BREAKING_SPACE : FooterText;
					break;
			}
		}

		/// <summary>
		/// Initializes the specified <see cref="TableCell"/> object.
		/// </summary>
		/// <param name="cell">Cell.</param>
		protected virtual void InitializeHeaderCell(TableCell cell)
		{
			if (!string.IsNullOrEmpty(HeaderImageUrl))
			{
				Image image = new Image();
				image.GenerateEmptyAlternateText = true;
				image.ImageUrl = HeaderImageUrl;
				cell.Controls.Add(image); 
			}
			else
			{

				cell.Text = string.IsNullOrEmpty(HeaderText) ? C1GridView.NON_BREAKING_SPACE : HeaderText;
			}
		}

		internal virtual TableItemStyle GetCompoundStyle(ItemStyleType type)
		{
			TableItemStyle result = new TableItemStyle();

			switch (type)
			{
				case ItemStyleType.Control:
					if (_controlStyle != null)
					{
						result.CopyFrom(_controlStyle);
					}
					break;

				case ItemStyleType.Footer:
					if (_footerStyle != null)
					{
						result.CopyFrom(_footerStyle);
					}
					break;

				case ItemStyleType.Header:
					if (_headerStyle != null)
					{
						result.CopyFrom(_headerStyle);
					}
					break;

				case ItemStyleType.Item:
					if (_itemStyle != null)
					{
						result.CopyFrom(_itemStyle);
					}
					break;
			}

			return result;
		}

		internal virtual TableItemStyle GetMergedStyle(ItemStyleType type)
		{
			TableItemStyle result = new TableItemStyle();

			IOwnerable owner = this;
			while (owner != null)
			{
				C1BaseField field = owner as C1BaseField;
				if (field != null)
				{
					result.MergeWith(field.GetCompoundStyle(type));
				}

				owner = owner.Owner;
			}

			return result;
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual string MonikerInternal()
		{
			return string.Empty;
		}

		/// <summary>
		/// This method is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Moniker()
		{
			string moniker = MonikerInternal();

			if (string.IsNullOrEmpty(moniker))
			{
				moniker = HeaderText;

				if (string.IsNullOrEmpty(moniker))
				{
					try
					{
						string typeName = ClientType.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[0];
						moniker = C1Localizer.GetString("C1GridView." + typeName, typeName);
					}
					catch
					{
					}
				}
			}

			return string.IsNullOrEmpty(moniker)
				? ToString()
				: moniker;
		}

		/// <summary>
		/// Helper method called by <see cref="C1GridView"/> control to set up the specified <see cref="C1GridViewCell"/> object.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to set up.</param>
		/// <param name="row">Row.</param>
		protected internal virtual void PrepareCell(C1GridViewCell cell, C1GridViewRow row)
		{
			TableItemStyle style = null;

			switch (row.RowType)
			{
				case C1GridViewRowType.DataRow:
					style = GetCompoundStyle(ItemStyleType.Control);

					if (style != null && !style.IsEmpty)
					{
						foreach (Control ctrl in cell.Controls)
						{
							WebControl webCtrl = ctrl as WebControl;
							if (webCtrl != null)
							{
								webCtrl.ControlStyle.CopyFrom(style); 
							}
						}
					}

					style = GetCompoundStyle(ItemStyleType.Item);

					break;

				case C1GridViewRowType.Filter:
					style = GetCompoundStyle(ItemStyleType.Filter );
					break;

				case C1GridViewRowType.Footer:
					style = GetCompoundStyle(ItemStyleType.Footer);
					break;

				case C1GridViewRowType.Header:
					style = GetCompoundStyle(ItemStyleType.Header);
					break;
			}

			cell.MergeStyle(style);
		}

		/// <summary>
		/// Marks the state of the <see cref="C1BaseField"/> object as having been changed.
		/// </summary>
		protected internal override void SetDirty()
		{
			base.SetDirty();

			if (_controlStyle != null)
			{
				_controlStyle.SetDirty();
			}

			if (_footerStyle != null)
			{
				_footerStyle.SetDirty();
			}

			if (_headerStyle != null)
			{
				_headerStyle.SetDirty();
			}

			if (_itemStyle != null)
			{
				_itemStyle.SetDirty();
			}
		}

		#endregion

		#region ViewState

		/// <summary>
		/// Restores the previously saved view state of the <see cref="C1BaseField"/> object.
		/// </summary>
		/// <param name="state">An object that represents the <see cref="C1BaseField"/> state to restore.</param>
		protected override void LoadViewState(object state)
		{
			if (state != null)
			{
				object[] stateObj = (object[])state;
				int len = stateObj.Length;

				if (len > 0)
				{
					base.LoadViewState(stateObj[0]);
				}

				if (len > 1)
				{
					((IStateManager)ControlStyle).LoadViewState(stateObj[1]);
				}

				if (len > 2)
				{
					((IStateManager)FooterStyle).LoadViewState(stateObj[2]);
				}

				if (len > 3)
				{
					((IStateManager)HeaderStyle).LoadViewState(stateObj[3]);
				}

				if (len > 4)
				{
					((IStateManager)ItemStyle).LoadViewState(stateObj[4]);
				}
			}
		}

		/// <summary>
		/// Saves the changes to the <see cref="C1BaseField"/> object since the time the page was posted back to the server.
		/// </summary>
		/// <returns>The object that contains the changes to the view state of the <see cref="C1BaseField"/>.</returns>
		protected override object SaveViewState()
		{
			object[] state = new object[] {
				base.SaveViewState(),
				_controlStyle != null ? ((IStateManager)ControlStyle).SaveViewState() : null,
				_footerStyle != null ? ((IStateManager)FooterStyle).SaveViewState() : null,
				_headerStyle != null ? ((IStateManager)HeaderStyle).SaveViewState() : null,
				_itemStyle != null ? ((IStateManager)ItemStyle).SaveViewState() : null
			};

			return Array.TrueForAll(state, v => v == null)
				? null
				: state;
		}

		/// <summary>
		/// Causes the <see cref="C1BaseField"/> object to track changes to its state so that it can be persisted across requests.
		/// </summary>
		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (_controlStyle != null)
			{
				((IStateManager)_controlStyle).TrackViewState();
			}

			if (_footerStyle != null)
			{
				((IStateManager)_footerStyle).TrackViewState();
			}

			if (_headerStyle != null)
			{
				((IStateManager)_headerStyle).TrackViewState();
			}

			if (_itemStyle != null)
			{
				((IStateManager)_itemStyle).TrackViewState();
			}
		}

		#endregion

		#region IC1Cloneable

		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="C1BaseField"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);

			C1BaseField field = copyFrom as C1BaseField;
			if (copyFrom != null)
			{
				ControlStyle.Reset();
				ControlStyle.CopyFrom(field.ControlStyle);

				FooterStyle.Reset();
				FooterStyle.CopyFrom(field.FooterStyle);

				HeaderStyle.Reset();
				HeaderStyle.CopyFrom(field.HeaderStyle);

				ItemStyle.Reset();
				ItemStyle.CopyFrom(field.ItemStyle);
			}
		}

		#endregion

		#region IJsonRestore members, InnerState

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable( EditorBrowsableState.Never)]
		public override Hashtable InnerState
		{
			get
			{
				// Note: properties prefixed with the "_" character are excluded from the callback request (see c1gridview.onwijstatesaving())

				Hashtable ht = base.InnerState;

				int uid = UID;
				ht["_uid"] = uid;

				ht["oi"] = (_originalIndex < 0)
					? _originalIndex = uid
					: _originalIndex;

				ht["clientType"] = ClientType;

				if (!string.IsNullOrEmpty(HeaderImageUrl) && (GridView != null)) {
					ht["_resolvedHeaderImageUrl"] = GridView.ResolveClientUrl(HeaderImageUrl);
				}

				return ht;
			}
		}

		/// <summary>
		/// Restore the states from the JSON object.
		/// </summary>
		/// <param name="state">The state which is parsed from the JSON string.</param>
		protected override void RestoreStateFromJson(object state)
		{
			Hashtable ht = state as Hashtable;
			if (ht != null)
			{
				Hashtable innerState;

				if ((innerState = ht["innerState"] as Hashtable) != null)
				{
					_originalIndex = (int)innerState["oi"];
				}

				innerState.Remove("oi");
			}

			base.RestoreStateFromJson(state);
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		internal int UID
		{
			get
			{
				C1GridView gridView = GridView;

				if (gridView == null) // no data is bounded to the grid so the C1BaseField.Intialize(gridView) method doesn't get called?
				{
					IOwnerable owner = this;
					while (owner.Owner != null)
					{
						owner = owner.Owner;
					}

					gridView = owner as C1GridView;
				}

				if (gridView != null)
				{
					int idx = 0;
					foreach (C1BaseField field in gridView.Columns.Traverse())
					{
						if (field == this)
						{
							return idx;
						}

						idx++;
					}
				}

				return -1;
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Json(true)]
		[Layout(LayoutType.None)]
		private string ClientType
		{
			get
			{
				Type fieldType = GetType();
				string name = fieldType.Name;
				string qualifiedName = string.Empty;

				while (Array.IndexOf<string>(C1BaseFieldCollection._knownTypeNames, name) < 0)
				{
					fieldType = fieldType.BaseType;

					if (fieldType == typeof(object))
					{
						throw new InvalidOperationException(string.Format(C1Localizer.GetString("C1Grid.EBaseTypeNotFound"), name));
					}

					name = fieldType.Name;
					qualifiedName = fieldType.AssemblyQualifiedName;
				}

				return (qualifiedName.Length > 0)
					? name + ":" + qualifiedName
					: name;
			}
		}

		#endregion

		#region nonpersistables
		
		/// <summary>
		/// Infrastucture.
		/// </summary>
		protected internal int OriginalIndex
		{
			get { return _originalIndex; }
		}

		/// <summary>
		/// Infrastucture.
		/// </summary>
		protected internal virtual bool NonPersistables
		{
			get { return false; }
		}

		/// <summary>
		/// Infrastucture.
		/// </summary>
		protected internal virtual void LoadNonPersistables(object state)
		{
		}

		/// <summary>
		/// Infrastucture.
		/// </summary>
		protected internal virtual object SaveNonPersistables()
		{
			return null;
		}

		#endregion

		#region IDataSourceViewSchemaAccessor Members

		object IDataSourceViewSchemaAccessor.DataSourceViewSchema
		{
			get;
			set;
		}

		#endregion

	}
}
