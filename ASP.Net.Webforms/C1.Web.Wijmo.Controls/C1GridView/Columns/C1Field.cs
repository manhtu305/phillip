﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Web.UI;

	/// <summary>
	/// Represents a base class for all data-bound fields.
	/// </summary>
	public abstract class C1Field : C1BaseField
	{
		private const Aggregate DEF_AGGREGATE = Aggregate.None;
		private const bool DEF_ALLOWGROUP = true;
		private const RowMerge DEF_ROWMERGE = RowMerge.None;
		private const C1SortDirection DEF_SORTDIRECTION = C1SortDirection.None;
		private const string DEF_SORTEXPRESSION = "";

		private GroupInfo _groupInfo;

		private float _groupedIndex = -1;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1Field"/> class.
		/// </summary>
		public C1Field() : this(null)
		{
		}

		internal C1Field(IOwnerable owner) : base(owner)
		{
		}

		#region public

		/*
		 * TODO
		/// <summary>
		/// Used to prevent client-side grouping of a column in a grouped grid.
		/// </summary>
		/// <value>
		/// When this property is set to True (default), you can drag columns to the grouping area.
		/// </value>
		[DefaultValue(DEF_ALLOWGROUP)]
		[NotifyParentProperty(true)]
		public virtual bool AllowGroup
		{
			get { return GetPropertyValue("AllowGroup", DEF_ALLOWGROUP); }
			set
			{
				if (GetPropertyValue("AllowGroup", DEF_ALLOWGROUP) != value)
				{
					SetPropertyValue("AllowGroup", value);
					OnFieldChanged();
				}
			}
		}*/

		/// <summary>
		/// Causes the grid to calculate aggregate values on the column and place them in the group header and footer rows. If the grid does not contain any groups, setting the Aggregate property has no effect.
		/// </summary>
		/// <value>
		/// One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.Aggregate"/> values. The default value is <see cref="C1.Web.Wijmo.Controls.C1GridView.Aggregate.None"/>.
		/// </value>
		/// <remarks>
		/// The aggregate value displayed in the group header/footer rows is formatted using the column's
		/// <see cref="C1.Web.Wijmo.Controls.C1GridView.C1BoundField.DataFormatString"/> property, except for Counts which are
		/// displayed as integers without decimals.
		/// </remarks>
		[DefaultValue(DEF_AGGREGATE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_AGGREGATE)]
		public Aggregate Aggregate
		{
			get { return GetPropertyValue("Aggregate", DEF_AGGREGATE); }
			set
			{
				if (GetPropertyValue("Aggregate", DEF_AGGREGATE) != value)
				{
					SetPropertyValue("Aggregate", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets the index that determines the position of grouped columns
		/// </summary>
		[Json(true, true, -1)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] 
		public float GroupedIndex
		{
			get { return _groupedIndex; }
			set { _groupedIndex = value; }
		}

		/// <summary>
		/// Gets a <see cref="GroupInfo"/> object that determines whether a column should be grouped on
		/// and how the group header and footer rows should be formatted and displayed.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[Json(true, true, null)]
		[Browsable(false)] // << use property page for editing this
		public virtual GroupInfo GroupInfo
		{
			get
			{
				if (_groupInfo == null)
				{
					_groupInfo = new GroupInfo(this);

					if (IsTrackingViewState)
					{
						((IStateManager)_groupInfo).TrackViewState();
					}
				}

				return _groupInfo;
			}
		}

		/// <summary>
		/// Determines whether rows are merged.
		/// </summary>
		/// <value>
		/// One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.RowMerge"/> values.
		/// The default value is <see cref="C1.Web.Wijmo.Controls.C1GridView.RowMerge.None"/>.
		/// </value>
		[DefaultValue(DEF_ROWMERGE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ROWMERGE)]
		public RowMerge RowMerge
		{
			get { return GetPropertyValue("RowMerge", DEF_ROWMERGE); }
			set
			{
				if (GetPropertyValue("RowMerge", DEF_ROWMERGE) != value)
				{
					SetPropertyValue("RowMerge", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets the direction of the sort.
		/// </summary>
		/// <value>
		/// One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.C1SortDirection"/> values. The default value is <see cref="C1.Web.Wijmo.Controls.C1GridView.C1SortDirection.None"/>.
		/// </value> 
		/// <remarks>
		/// This property provides a convenient method of tracking the column sort state when the grid is sorted.
		/// </remarks>
		[DefaultValue(DEF_SORTDIRECTION)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_SORTDIRECTION)]
		public C1SortDirection SortDirection
		{
			get { return GetPropertyValue("SortDirection", DEF_SORTDIRECTION); }
			set
			{
				if (GetPropertyValue("SortDirection", DEF_SORTDIRECTION) != value)
				{
					SetPropertyValue("SortDirection", value);
					OnFieldChanged();
				}
			}
		}


		/// <summary>
		/// Gets or sets the name of the field or expression to pass to the OnSortCommand method when a column is
		/// selected for sorting.
		/// </summary>
		/// <remarks>
		/// This property does not bind the column to a field in data source. To bind a column to a field,
		/// see the documentation for the specific column type.
		/// </remarks>
		[DefaultValue(DEF_SORTEXPRESSION)]
		[TypeConverter(typeof(System.Web.UI.Design.DataSourceViewSchemaConverter))]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_SORTEXPRESSION)]
		public virtual string SortExpression
		{
			get { return GetPropertyValue("SortExpression", DEF_SORTEXPRESSION); }
			set
			{
				if (GetPropertyValue("SortExpression", DEF_SORTEXPRESSION) != value)
				{
					SetPropertyValue("SortExpression", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		#region overrides

		internal override bool IsLeaf
		{
			get { return true; }
			set {} 
		}

		/// <summary>
		/// Marks the state of the <see cref="C1Field"/> object as having been changed.
		/// </summary>
		protected internal override void SetDirty()
		{
			base.SetDirty();

			if (_groupInfo != null)
			{
				_groupInfo.SetDirty();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected internal virtual string EnsureSortExpression()
		{
			string expression = SortExpression;

			return (string.IsNullOrEmpty(expression))
				? string.Empty
				: SortExpression.Trim();
		}

		#endregion

		#region IC1Cloneable


		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="C1Field"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);

			C1Field field = copyFrom as C1Field;
			if (field != null)
			{
				_groupInfo = null;
				if (field._groupInfo != null)
				{
					GroupInfo.CopyFrom((GroupInfo)field.GroupInfo);
				}
			}
		}

		/*protected internal override ViewStatePersister CreateInstance()
		{
			return new C1Field();
		}*/

		#endregion

		#region ViewState

		/// <summary>
		/// Restores the previously saved view state of the <see cref="C1Field"/> object.
		/// </summary>
		/// <param name="state">An object that represents the <see cref="C1Field"/> state to restore.</param>
		protected override void LoadViewState(object state)
		{
			if (state != null)
			{
				Pair stateObj = (Pair)state;

				base.LoadViewState(stateObj.First);

				if (stateObj.Second != null)
				{
					((IStateManager)GroupInfo).LoadViewState(stateObj.Second);
				}
			}
		}

		/// <summary>
		/// Saves the changes to the <see cref="C1Field"/> object since the time the page was posted back to the server.
		/// </summary>
		/// <returns>The object that contains the changes to the view state of the <see cref="C1Field"/>.</returns>
		protected override object SaveViewState()
		{
			Pair state = new Pair(base.SaveViewState(),
				(_groupInfo != null) ? ((IStateManager)_groupInfo).SaveViewState() : null);

			return (state.First != null || state.Second != null)
				? state
				: null;
		}

		/// <summary>
		/// Causes the <see cref="C1Field"/> object to track changes to its state so that it can be persisted across requests.
		/// </summary>
		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (_groupInfo != null)
			{
				((IStateManager)_groupInfo).TrackViewState();
			}
		}

		#endregion
	}
}
