﻿using System;
using System.ComponentModel;


namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Web.UI;
	using System.Web.UI.WebControls;

	/// <summary>
	/// Base class for all Button-based columns.
	/// </summary>
	public abstract class C1ButtonFieldBase : C1Field 
	{
		private const bool DEF_CAUSESVALIDATION = false;
		private const ButtonType DEF_BUTTONTYPE = ButtonType.Link;
		private const string DEF_VALIDATIONGROUP = "";

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1ButtonField"/> class.
		/// </summary>
		public C1ButtonFieldBase() : this(null)
		{
		}

		internal C1ButtonFieldBase(IOwnerable owner) : base(owner)
		{
		}

		#region properties

		/// <summary>
		/// Gets or sets a value indicating whether validation is performed when a button in a column is clicked.
		/// </summary>
		/// <returns>
		/// True to perform validation when a button in a column is clicked, otherwise false.
		/// </returns>
		/// <value>
		/// The default value is false.
		/// </value>
		[DefaultValue(DEF_CAUSESVALIDATION)]
		[Json(true, true, DEF_CAUSESVALIDATION)]
		public virtual bool CausesValidation
		{
			get { return GetPropertyValue("CausesValidation", DEF_CAUSESVALIDATION); }
			set
			{
				if (GetPropertyValue("CausesValidation", DEF_CAUSESVALIDATION) != value)
				{
					SetPropertyValue("CausesValidation", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the type of the button in the column.
		/// </summary>
		/// <value>
		/// One of the <see cref="ButtonType"/> values. The default value is <see cref="System.Web.UI.WebControls.ButtonType.Link"/>.
		/// </value>
		/// <remarks>
		/// This property is used to specify whether buttons in the column display as push buttons or hyperlinks.
		/// </remarks>	
		[DefaultValue(DEF_BUTTONTYPE)]
		[Json(true, true, DEF_BUTTONTYPE)]
		public virtual ButtonType ButtonType
		{
			get { return GetPropertyValue("ButtonType", DEF_BUTTONTYPE); }
			set
			{
				if (GetPropertyValue("ButtonType", DEF_BUTTONTYPE) != value)
				{
					SetPropertyValue("ButtonType", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>Gets or sets the name of the group of validation controls to validate when a button in a column is clicked.</summary>
		/// <returns>
		/// The name of the validation group to validate when a button in a column is clicked.
		/// </returns>
		/// <value>
		/// The default value is an empty string.
		/// </value>
		[DefaultValue(DEF_VALIDATIONGROUP)]
		[Json(true, true, DEF_VALIDATIONGROUP)]
		public virtual string ValidationGroup
		{
			get { return GetPropertyValue("ValidationGroup", DEF_VALIDATIONGROUP); }
			set
			{
				if (GetPropertyValue("ValidationGroup", DEF_VALIDATIONGROUP) != value)
				{
					SetPropertyValue("ValidationGroup", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		#region overrides, protected

		/// <summary>
		/// Creates and returns the control that will be contained in the cells of column, based on parameters and <see cref="ButtonType"/> value.
		/// </summary>
		/// <param name="commandArgument">Command argument.</param>
		/// <param name="commandName">Command name.</param>
		/// <param name="text">Text.</param>
		/// <param name="causesValidation">Indicates if validation is needed.</param>
		/// <param name="validationGroup">Validation group.</param>
		/// <param name="imageUrl">Image URL.</param>
		/// <param name="useCallback">Indicates if callback should be performed.</param>
		/// <returns>Control that will be placed to the cells of a column.</returns>
		protected Control GetButtonControl(string commandArgument, string commandName,
			string text, bool causesValidation, string validationGroup, string imageUrl, bool useCallback)
		{
			IButtonControl button = null;

			C1GridView grid = GridView;
			bool useCallbackBtn = (grid != null) && !grid.DesignMode && !grid.IsRoot() && grid.CallbackHierarchy; // force to use callback buttons in case of detail grid + CallbackHierarchy.

			switch (ButtonType)
			{
				case ButtonType.Button:
					if (useCallbackBtn)
					{
						button = new Controls.C1CallbackButton(grid); // to disable event validation.
					}
					else
					{
						button = new Controls.C1Button(useCallback ? grid : null, causesValidation ? null : grid);
					}

					button.Text = text;
					break;

				case ButtonType.Image:
					if (useCallbackBtn)
					{
						button = new Controls.C1CallbackImageButton(grid); // to disable event validation.
						((Controls.C1CallbackImageButton)button).ImageUrl = imageUrl;

					}
					else
					{
						if (useCallback || !causesValidation)
						{
							button = new Controls.C1ImageButton(useCallback ? grid : null, causesValidation ? null : grid);
							((ImageButton)button).ImageUrl = imageUrl;
						}
						else
						{
							button = new ImageButton();
							((ImageButton)button).ImageUrl = imageUrl;
						}
					}

					((WebControl)button).ToolTip = text;
					break;

				case ButtonType.Link:
					if (useCallbackBtn)
					{
						button = new Controls.C1CallbackLinkButton(grid); // to disable event validation.
					}
					else
					{
						button = new Controls.C1LinkButton(useCallback ? grid : null, causesValidation ? null : grid);
					}

					button.Text = text;
					break;
			}

			button.CausesValidation = causesValidation;
			if (causesValidation)
			{
				button.ValidationGroup = validationGroup;
			}

			button.CommandName = commandName;
			button.CommandArgument = commandArgument;

			return (Control)button;
		}

		/*protected internal override void CopyFrom(ViewStatePersister copyFrom)
		{
			base.CopyFrom(copyFrom);
		}*/

		#endregion
	}
}
