﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.Web.UI.WebControls;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Represents a column that is displayed as an image in a <see cref="C1GridView"/> control.
	/// </summary>
	public class C1ImageField : C1Field
	{
		private const string DEF_ALTERNATETEXT = "";
		private const bool DEF_CONVERTEMPTYSTRINGTONULL = true;
		private const string DEF_DATAALTERNATETEXTFIELD = "";
		private const string DEF_DATAALTERNATETEXTFORMATSTRING = "";
		private const string DEF_DATAIMAGEURLFIELD = "";
		private const string DEF_DATAIMAGEURLFORMATSTRING = "";
		private const string DEF_NULLDISPLAYTEXT = "";
		private const string DEF_NULLIMAGEURL = "";
		private const bool DEF_READONLY = false;

		private PropertyDescriptor _alternateTextColumnPD;
		private PropertyDescriptor _imageUrlColumnPD;

		#region public properties

		/// <summary>
		/// Gets or sets the alternate text displayed for an image in the <see cref="C1ImageField"/> object.
		/// </summary>
		/// <value>
		/// The alternate text for an image displayed in the <see cref="C1ImageField"/> object.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_ALTERNATETEXT)]
		[Json(true, true, DEF_ALTERNATETEXT)]
		public virtual string AlternateText
		{
			get { return GetPropertyValue("AlternateText", DEF_ALTERNATETEXT); }
			set
			{
				if (GetPropertyValue("AlternateText", DEF_ALTERNATETEXT) != value)
				{
					SetPropertyValue("AlternateText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether empty string ("") values are converted to null when the field
		/// values are returned from the data source.
		/// </summary>
		/// <value>
		/// True if empty values should be converted to null; otherwise, false.
		/// The default value is true.
		/// </value>
		[DefaultValue(DEF_CONVERTEMPTYSTRINGTONULL)]
		[Json(true, true, DEF_CONVERTEMPTYSTRINGTONULL)]
		public virtual bool ConvertEmptyStringToNull
		{
			get { return GetPropertyValue("ConvertEmptyStringToNull", DEF_CONVERTEMPTYSTRINGTONULL); }
			set
			{
				if (GetPropertyValue("ConvertEmptyStringToNull", DEF_CONVERTEMPTYSTRINGTONULL) != value)
				{
					SetPropertyValue("ConvertEmptyStringToNull", value);
				}
			}
		}

		/// <summary>
		/// Gets or sets the name of the column from the data source that contains the values to bind to the
		/// <see cref="AlternateText"/> property of each image in an <see cref="C1ImageField"/> object.
		/// </summary>
		/// <value>
		/// The name of the field to bind the <see cref="AlternateText"/> property of each image in an
		/// <see cref="ImageField"/> object.
		/// </value>
		[DefaultValue(DEF_DATAALTERNATETEXTFIELD)]
		[Json(true, true, DEF_DATAALTERNATETEXTFIELD)]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		public virtual string DataAlternateTextField
		{
			get { return GetPropertyValue("DataAlternateTextField", DEF_DATAALTERNATETEXTFIELD); }
			set
			{
				if (GetPropertyValue("DataAlternateTextField", DEF_DATAALTERNATETEXTFIELD) != value)
				{
					SetPropertyValue("DataAlternateTextField", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the string that specifies the format in which the alternate text for each image in an
		/// <see cref="C1ImageField"/> object is rendered.
		/// </summary>
		/// <value>
		/// A string that specifies the format in which the alternate text for each image in an
		/// <see cref="C1ImageField"/> object is rendered.
		/// The default value is an empty string (""), which indicates that now special formatting is applied to the
		/// alternate text.
		/// </value>
		[DefaultValue(DEF_DATAALTERNATETEXTFORMATSTRING)]
		[Json(true, true, DEF_DATAALTERNATETEXTFORMATSTRING)]
		public virtual string DataAlternateTextFormatString
		{
			get { return GetPropertyValue("DataAlternateTextFormatString", DEF_DATAALTERNATETEXTFORMATSTRING); }
			set
			{
				if (GetPropertyValue("DataAlternateTextFormatString", DEF_DATAALTERNATETEXTFORMATSTRING) != value)
				{
					SetPropertyValue("DataAlternateTextFormatString", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the name of the column from the data source that contains the values to bind to the
		/// <see cref="System.Web.UI.WebControls.Image.ImageUrl"/> property of each image in an
		/// <see cref="C1ImageField"/> object.
		/// </summary>
		/// <value>
		/// The name of the column to bind to the <see cref="System.Web.UI.WebControls.Image.ImageUrl"/> property
		/// of each image in an <see cref="C1ImageField"/> object.
		/// </value>
		[DefaultValue(DEF_DATAIMAGEURLFIELD)]
		[Json(true, true, DEF_DATAIMAGEURLFIELD)]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		public virtual string DataImageUrlField
		{
			get { return GetPropertyValue("DataImageUrlField", DEF_DATAIMAGEURLFIELD); }
			set
			{
				if (GetPropertyValue("DataImageUrlField", DEF_DATAIMAGEURLFIELD) != value)
				{
					SetPropertyValue("DataImageUrlField", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the string that specifies the format in which the URL for each image in an
		/// <see cref="C1ImageField"/> object is rendered.
		/// </summary>
		/// <value>
		/// A string that specifies the format in which the URL for each image in an <see cref="C1ImageField"/>
		/// object is rendered. The default value is the empty string ("") , which indicates that no special
		/// formatting is applied to the URLs.
		/// </value>
		[DefaultValue(DEF_DATAIMAGEURLFORMATSTRING)]
		[Json(true, true, DEF_DATAIMAGEURLFORMATSTRING)]
		public virtual string DataImageUrlFormatString
		{
			get { return GetPropertyValue("DataImageUrlFormatString", DEF_DATAIMAGEURLFORMATSTRING); }
			set
			{
				if (GetPropertyValue("DataImageUrlFormatString", DEF_DATAIMAGEURLFORMATSTRING) != value)
				{
					SetPropertyValue("DataImageUrlFormatString", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the text to display in an <see cref="C1ImageField"/> object when the value
		/// of the field specified by the <see cref="DataImageUrlField"/> property is null.
		/// </summary>
		/// <value>
		/// The text to display when the value of a column is null.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_NULLDISPLAYTEXT)]
		[Json(true, true, DEF_NULLDISPLAYTEXT)]
		public virtual string NullDisplayText
		{
			get { return GetPropertyValue("NullDisplayText", DEF_NULLDISPLAYTEXT); }
			set
			{
				if (GetPropertyValue("NullDisplayText", DEF_NULLDISPLAYTEXT) != value)
				{
					SetPropertyValue("NullDisplayText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL to an alternate image displayed in an <see cref="C1ImageField"/> object when the
		/// value of the field specified by the <see cref="DataImageUrlField"/> property is null.
		/// </summary>
		/// <value>
		/// The URL to an alternate image displayed when the value of a column is null.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_NULLIMAGEURL)]
		[Json(true, true, DEF_NULLIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public virtual string NullImageUrl
		{
			get { return GetPropertyValue("NullImageUrl", DEF_NULLIMAGEURL); }
			set
			{
				if (GetPropertyValue("NullImageUrl", DEF_NULLIMAGEURL) != value)
				{
					SetPropertyValue("NullImageUrl", value);
					OnFieldChanged();
				}
			}
		}
		/// <summary>
		/// Gets or sets a value indicating whether the values of the column specified by the
		/// <see cref="DataImageUrlField"/> property can be modified in edit mode.
		/// </summary>
		/// <value>
		/// True to indicate that the column values cannot be modified in edit mode; otherwise, false.
		/// The default value is false.
		/// </value>
		[DefaultValue(DEF_READONLY)]
		[Json(true, true, DEF_READONLY)]
		public virtual bool ReadOnly
		{
			get { return GetPropertyValue("ReadOnly", DEF_READONLY); }
			set
			{
				if (GetPropertyValue("ReadOnly", DEF_READONLY) != value)
				{
					SetPropertyValue("ReadOnly", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		/// <summary>
		/// Creates an empty <see cref="C1ImageField"/> object.
		/// </summary>
		/// <returns>An empty <see cref="C1ImageField"/> object.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1ImageField();
		}

		/// <summary>
		/// Fills the specified <see cref="IOrderedDictionary"/> object with values from the specified <see cref="C1GridViewCell"/> object.
		/// </summary>
		/// <param name="dictionary">A <see cref="IOrderedDictionary"/> used to store the values of the specified cell.</param>
		/// <param name="cell">The <see cref="C1GridViewCell"/> object that contains the values to retrieve.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		/// <param name="includeReadOnly">True to include the values of read-only fields; otherwise, false.</param>
		public override void ExtractValuesFromCell(IOrderedDictionary dictionary, C1GridViewCell cell, C1GridViewRowState rowState, bool includeReadOnly)
		{
			if (cell.Controls.Count > 0)
			{
				object urlValue = null;
				bool isSet = false;

				TextBox textBox = cell.Controls[0] as TextBox;
				if (textBox != null)
				{
					urlValue = textBox.Text;
					isSet = true;
				}
				else
				{
					Image image = cell.Controls[0] as Image;
					if (image != null && includeReadOnly)
					{
						if (image.Visible)
						{
							urlValue = image.ImageUrl;
						}
						isSet = true;
					}
				}

				if (isSet)
				{
					if (ConvertEmptyStringToNull && string.IsNullOrEmpty((string)urlValue))
					{
						urlValue = null;
					}

					string columnName = DataImageUrlField;
					if (dictionary.Contains(columnName))
					{
						dictionary[columnName] = urlValue;
					}
					else
					{
						dictionary.Add(columnName, urlValue);
					}
				}
			}
		}

		/// <summary>
		/// Initializes the <see cref="C1ImageField"/> object.
		/// </summary>
		/// <param name="gridView">C1GridView.</param>
		protected internal override void Initialize(C1.Web.Wijmo.Controls.C1GridView.C1GridView gridView)
		{
			base.Initialize(gridView);
			_alternateTextColumnPD = null;
			_imageUrlColumnPD = null;
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal override void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			base.InitializeCell(cell, columnIndex, rowType, rowState);

			if (rowType == C1GridViewRowType.DataRow && !string.IsNullOrEmpty(DataImageUrlField))
			{
				InitializeDataCell(cell, columnIndex, rowType, rowState);
			}
		}

		/// <summary>
		/// Initializes the specified <see cref="TableCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="TableCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected virtual void InitializeDataCell(TableCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			Control ctrl = null;

			if (((rowState & C1GridViewRowState.Edit) != 0) && !ReadOnly)
			{
				ctrl = new TextBox();
				cell.Controls.Add(ctrl);
			}
			else
			{
				ctrl = cell;

				if (!DesignMode)
				{
					cell.Controls.Add(new Image());
					cell.Controls.Add(new Label());
				}
			}

			ctrl.DataBinding += new EventHandler(OnDataBindField);
		}

		/// <summary>
		/// Applies the format specified by the <see cref="DataImageUrlFormatString"/> property to a field value.
		/// </summary>
		/// <param name="dataValue">The field value to format.</param>
		/// <returns>The formatted value.</returns>
		protected virtual string FormatImageUrlValue(object dataValue)
		{
			if (!C1BaseField.IsNull(dataValue))
			{
				string strValue = dataValue.ToString();

				if (string.IsNullOrEmpty(strValue))
				{
					return string.Empty;
				}

				return (!string.IsNullOrEmpty(DataImageUrlFormatString))
					? string.Format(DataImageUrlFormatString, dataValue)
					: strValue;
			}

			return null;
		}

		/// <summary>
		/// Applies the format specified by the <see cref="DataAlternateTextFormatString"/> property to the
		/// alternate text value contained in the specified <see cref="C1GridViewRow"/> object.
		/// </summary>
		/// <param name="row">The <see cref="C1GridViewRow"/> object that contains the alternate text value to format.</param>
		/// <returns>The formatted value.</returns>
		protected virtual string FormatAlternateText(C1GridViewRow row)
		{
			if (!string.IsNullOrEmpty(DataAlternateTextField))
			{
				object dataVal = GetValue(row, DataAlternateTextField, ref _alternateTextColumnPD);
				string strVal = string.Empty;

				if (!string.IsNullOrEmpty(DataAlternateTextFormatString))
				{
					strVal = string.Format(DataAlternateTextFormatString, dataVal);
				}
				else
					if (!C1BaseField.IsNull(dataVal))
					{
						strVal = dataVal.ToString();
					}

				return strVal;
			}

			return AlternateText;
		}

		/// <summary>
		/// Retrieves the value of the field bound to the <see cref="C1ImageField"/> object.
		/// </summary>
		/// <param name="row">The <see cref="C1GridViewRow"/> object that contains the field value.</param>
		/// <param name="fieldName">The name of the field for which to retrieve the value.</param>
		/// <param name="pd">A <see cref="System.ComponentModel.PropertyDescriptor"/>, passed by reference, that
		/// represents the properties of the field.</param>
		/// <returns>The value of the specified field.</returns>
		protected virtual object GetValue(C1GridViewRow row, string fieldName, ref PropertyDescriptor pd)
		{
			object dataItem = DataBinder.GetDataItem(row);

			if (dataItem == null && !DesignMode)
			{
				throw new HttpException(C1Localizer.GetString("C1Grid.EDataItemNotFound"));
			}

			if (pd == null)
			{
				pd = TypeDescriptor.GetProperties(dataItem).Find(fieldName, true);

				if (pd == null && !DesignMode)
				{
					throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataFieldNotFound"), fieldName));
				}
			}

			if (pd != null && dataItem != null)
			{
				return pd.GetValue(dataItem);
			}

			return (DesignMode)
				? null
				: dataItem;
		}

		/// <summary>
		/// Retrieves the value used for a field's value when rendering the <see cref="C1ImageField"/> object in a designer.
		/// </summary>
		/// <param name="rowState">One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.C1GridViewRowState"/> values.</param>
		/// <returns>The value to display in the designer as the field's value.</returns>
		protected virtual string GetDesignTimeValue(C1GridViewRowState rowState)
		{
			return C1Localizer.GetString("C1GridView.FieldDesignTimeValue");
		}

		/// <summary>
		/// Binds the value of a field to the <see cref="C1ImageField"/> object.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnDataBindField(object sender, EventArgs e)
		{
			C1GridViewRow row = (C1GridViewRow)((Control)sender).NamingContainer;
			TableCell cell = sender as TableCell;

			if (DesignMode)
			{
				cell.Text = GetDesignTimeValue(row.RowState);
			}
			else
			{
				object dataValue = GetValue(row, DataImageUrlField, ref _imageUrlColumnPD);
				string strVal = FormatImageUrlValue(dataValue);

				if (cell != null)
				{
					Image image = (Image)cell.Controls[0];
					Label label = (Label)cell.Controls[1];

					label.Visible = false;

					if ((strVal == null) || (ConvertEmptyStringToNull && strVal.Length == 0))
					{
						if (!string.IsNullOrEmpty(NullImageUrl))
						{
							strVal = NullImageUrl;
						}
						else
						{
							image.Visible = false;
							label.Visible = true;
							label.Text = NullDisplayText;
						}
					}

					image.ImageUrl = strVal;
					image.AlternateText = FormatAlternateText(row);
				}
				else
				{
					TextBox editBox = sender as TextBox;
					if (editBox != null)
					{
						if (dataValue != null)
						{
							editBox.Text = dataValue.ToString();
							editBox.ToolTip = FormatAlternateText(row);
						}
					}
				}
			}
		}
	}
}
