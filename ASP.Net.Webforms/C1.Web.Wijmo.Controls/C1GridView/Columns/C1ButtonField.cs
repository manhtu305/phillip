﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.Web.UI.WebControls;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// A <b>C1ButtonField</b> displays a command button for each item.
	/// </summary>
	public class C1ButtonField : C1ButtonFieldBase
	{
		private static readonly string[] _knownCommands = new string[] { C1GridView.CMD_CANCEL, C1GridView.CMD_EDIT, C1GridView.CMD_DELETE,
			C1GridView.CMD_SELECT, C1GridView.CMD_UPDATE };

		private const string DEF_COMMANDNAME = "";
		private const string DEF_DATATEXTFIELD = "";
		private const string DEF_DATATEXTFORMATSTRING = "";
		private const string DEF_IMAGEURL = "";
		private const string DEF_TEXT = "";

		private PropertyDescriptor _textFieldDesc;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1ButtonField"/> class.
		/// </summary>
		public C1ButtonField() : this(null)
		{
		}

		internal C1ButtonField(IOwnerable owner) : base(owner)
		{
		}

		#region public properties
		/// <summary>
		/// Gets or sets the command associated with the button.
		/// </summary>
		[DefaultValue(DEF_COMMANDNAME)]
		[Json(true, true, DEF_COMMANDNAME)]
		public virtual string CommandName
		{
			get { return GetPropertyValue("CommandName", DEF_COMMANDNAME); }
			set
			{
				if (GetPropertyValue("CommandName", DEF_COMMANDNAME) != value)
				{
					SetPropertyValue("CommandName", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the field name from a data source to bind to the <see cref="C1ButtonField"/>.
		/// </summary>
		[DefaultValue(DEF_DATATEXTFIELD)]
		[Json(true, true, DEF_DATATEXTFIELD)]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		public virtual string DataTextField
		{
			get { return GetPropertyValue("DataTextField", DEF_DATATEXTFIELD); }
			set
			{
				if (GetPropertyValue("DataTextField", DEF_DATATEXTFIELD) != value)
				{
					SetPropertyValue("DataTextField", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the field name from a data source to bind to the <see cref="C1ButtonField"/>.
		/// </summary>
		[DefaultValue(DEF_DATATEXTFORMATSTRING)]
		[Json(true, true, DEF_DATATEXTFORMATSTRING)]
		#if ASP_NET45
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.3")]
#endif
		public virtual string DataTextFormatString
		{
			get { return GetPropertyValue("DataTextFormatString", DEF_DATATEXTFIELD); }
			set
			{
				if (GetPropertyValue("DataTextFormatString", DEF_DATATEXTFIELD) != value)
				{
					SetPropertyValue("DataTextFormatString", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the image to display for each button in the <see cref="C1ButtonField"/> object.
		/// </summary>
		[DefaultValue(DEF_IMAGEURL)]
		[Json(true, true, DEF_IMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public virtual string ImageUrl
		{
			get { return GetPropertyValue("ImageUrl", DEF_IMAGEURL); }
			set
			{
				if (GetPropertyValue("ImageUrl", DEF_IMAGEURL) != value)
				{
					SetPropertyValue("ImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the static caption that is displayed for each button in the <see cref="C1ButtonField"/> object.
		/// </summary>
		[DefaultValue(DEF_TEXT)]
		[Json(true, true, DEF_TEXT)]
		public virtual string Text
		{
			get { return GetPropertyValue("Text", DEF_TEXT); }
			set
			{
				if (GetPropertyValue("Text", DEF_TEXT) != value)
				{
					SetPropertyValue("Text", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		#region overrides, protected

		/// <summary>
		/// Creates a new instance of the <see cref="C1ButtonField"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1ButtonField"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1ButtonField();
		}

		/// <summary>
		/// Formats the specified field value for a cell in the <see cref="C1ButtonField"/> object.
		/// </summary>
		/// <param name="dataTextValue">The field value to format.</param>
		/// <returns>The field value converted to the format specified by the <see cref="DataTextFormatString"/> property.</returns>
		protected virtual string FormatDataTextValue(object dataTextValue)
		{
			string formattedValue = String.Empty;

			if (!C1BaseField.IsNull(dataTextValue))
			{
				if (DataTextFormatString.Length == 0)
					formattedValue = dataTextValue.ToString();
				else
					formattedValue = String.Format(DataTextFormatString, dataTextValue);
			}
			return formattedValue;
		}

		/// <summary>
		/// Initializes the <see cref="C1ButtonField"/> object.
		/// </summary>
		/// <param name="gridView">C1GridView.</param>
		protected internal override void Initialize(C1GridView gridView)
		{
			base.Initialize(gridView); 
			_textFieldDesc = null;
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal override void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			base.InitializeCell(cell, columnIndex, rowType, rowState);

			string commandName = CommandName.ToLowerInvariant();

			if (rowType == C1GridViewRowType.DataRow)
			{
				bool useCallback = false;

				bool isKnownCommand = ((IList)_knownCommands).Contains(commandName);
				if (isKnownCommand)
				{
					if (commandName == C1GridView.CMD_SELECT)
					{
						if (GridView.CallbackSelection)
						{
							useCallback = true;
						}
					}
					else
					{
						if (GridView.CallbackEditing)
						{
							useCallback = true;
						}
					}
				}

				bool noValidation = (isKnownCommand && (commandName != C1GridView.CMD_UPDATE)) || useCallback;

				Control ctrl = GetButtonControl(GridView.Rows.Count.ToString(), CommandName, Text,
					noValidation ? false : CausesValidation,
					noValidation ? string.Empty : ValidationGroup,
					ImageUrl, useCallback);

				if (DataTextField.Length != 0)
					ctrl.DataBinding += new EventHandler(OnDataBindColumn);

				cell.Controls.Add(ctrl);
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <returns></returns>
		protected override string MonikerInternal()
		{
			string moniker = CommandName;

			if (!string.IsNullOrEmpty(moniker))
			{
				moniker = C1Localizer.GetString(moniker, moniker);
			}

			return moniker;
		}

		private void OnDataBindColumn(object sender, EventArgs e)
		{
			string formattedValue = C1.Web.Wijmo.Controls.Localization.C1Localizer.GetString("C1GridView.FieldDesignTimeValue");

			Control ctrl = (Control)sender;
			object dataItem = ((C1GridViewRow)ctrl.NamingContainer).DataItem;
			if (DataTextField.Length != 0)
			{
				_textFieldDesc = TypeDescriptor.GetProperties(dataItem).Find(DataTextField, true);
				if (_textFieldDesc == null)
				{
					if (!DesignMode)
					{
						throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataFieldNotFound"), DataTextField));
					}
				}
				else
				{
					formattedValue = FormatDataTextValue(_textFieldDesc.GetValue(dataItem));
				}
			}

			IButtonControl ibc = ctrl as IButtonControl;
			if (ibc != null)
			{
				ibc.Text = formattedValue;
			}
			else
			{
				((HyperLink)ctrl).Text = formattedValue;
			}
		}

		#endregion
	}
}
