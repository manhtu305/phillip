﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Drawing.Design;
	using System.Web.UI;

	/// <summary>
	/// A C1TemplateField displays each item in the column following a specified template.
	/// </summary>
	public class C1TemplateField : C1FilterField
	{
		private const bool DEF_CONVERTEMPTYSTRINGTONULL = true;

		private ITemplate _alternatingItemTemplate;
		private ITemplate _editItemTemplate;
		private ITemplate _footerTemplate;
		private ITemplate _headerTemplate;
		private ITemplate _itemTemplate;

		private C1GridViewUpdateBindingCollection _updateBindings;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1TemplateField"/> class.
		/// </summary>
		public C1TemplateField() : this(null)
		{
		}

		internal C1TemplateField(IOwnerable owner) : base(owner)
		{
		}

		#region public properties

		/// <summary>
		/// Gets or sets a value indicating whether the value that the <see cref="C1TemplateField"/> object is bound
		/// to should be converted to null if it is Empty.
		/// </summary>
		/// <value>
		/// True if the value that the <see cref="C1TemplateField"/> is bound to should be converted to null
		/// when it is empty; otherwise, false. The default value is true.
		/// </value>
		[DefaultValue(DEF_CONVERTEMPTYSTRINGTONULL)]
		[Json(true, true, DEF_CONVERTEMPTYSTRINGTONULL)]
		public virtual bool ConvertEmptyStringToNull
		{
			get { return GetPropertyValue("ConvertEmptyStringToNull", DEF_CONVERTEMPTYSTRINGTONULL); }
			set
			{
				if (GetPropertyValue("ConvertEmptyStringToNull", DEF_CONVERTEMPTYSTRINGTONULL) != value)
				{
					SetPropertyValue("ConvertEmptyStringToNull", value);
				}
			}
		}

		/// <summary>
		/// Gets or sets the template for displaying the alternating items in a <see cref="C1TemplateField"/> object.
		/// </summary>
		[TemplateContainer(typeof(IDataItemContainer), BindingDirection.TwoWay)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public virtual ITemplate AlternatingItemTemplate
		{
			get { return _alternatingItemTemplate; }
			set
			{
				_alternatingItemTemplate = value;
				OnFieldChanged();
			}
		}

		/// <summary>
		/// Gets or sets a value of the FilterExpression.
		/// </summary>
		[DefaultValue(DEF_FILTERVALUE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_FILTERVALUE)]
		public virtual string FilterExpression
		{
			get { return GetPropertyValue("FilterExpression", DEF_FILTERVALUE); }
			set
			{
				if (GetPropertyValue("FilterExpression", DEF_FILTERVALUE) != value)
				{
					SetPropertyValue("FilterExpression", value);
					OnFieldChanged();
				}
			}
		}


		/// <summary>
		/// Gets or sets the template to be used when editing an item.
		/// </summary>
		[TemplateContainer(typeof(IDataItemContainer), BindingDirection.TwoWay)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public virtual ITemplate EditItemTemplate
		{
			get { return _editItemTemplate; }
			set
			{
				_editItemTemplate = value;
				OnFieldChanged();
			}
		}

		/// <summary>
		/// Gets or sets the template used to show the footer of a <see cref="C1TemplateField"/>.
		/// </summary>
		[TemplateContainer(typeof(IDataItemContainer))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public virtual ITemplate FooterTemplate
		{
			get { return _footerTemplate; }
			set
			{
				_footerTemplate = value;
				OnFieldChanged();
			}
		}


		/// <summary>
		/// Gets or sets the template used to show the header of a <see cref="C1TemplateField"/>.
		/// </summary>
		[TemplateContainer(typeof(IDataItemContainer))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public virtual ITemplate HeaderTemplate
		{
			get { return _headerTemplate; }
			set
			{
				_headerTemplate = value;
				OnFieldChanged();
			}
		}

		/// <summary>
		/// Gets or sets the template used to show an item in a <see cref="C1TemplateField"/>.
		/// </summary>
		[TemplateContainer(typeof(IDataItemContainer), BindingDirection.TwoWay)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public virtual ITemplate ItemTemplate
		{
			get { return _itemTemplate; }
			set
			{
				_itemTemplate = value;
				OnFieldChanged();
			}
		}

		/// <summary>
		/// Returns the collection of <see cref="C1GridViewUpdateBinding"/> objects specifying bindings used
		/// in the <see cref="C1GridView.Update"/> method.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1GridViewUpdateBindingsEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1GridViewUpdateBindingsEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1GridViewUpdateBindingsEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		public C1GridViewUpdateBindingCollection UpdateBindings
		{
			get
			{
				if (_updateBindings == null)
				{
					_updateBindings = new C1GridViewUpdateBindingCollection();

					if (IsTrackingViewState)
					{
						((IStateManager)_updateBindings).TrackViewState();
					}
				}

				return _updateBindings;
			}
		}

		#endregion

		#region overrides

		/// <summary>
		/// Fills the specified <see cref="IOrderedDictionary"/> object with values from the specified <see cref="C1GridViewCell"/> object.
		/// </summary>
		/// <param name="dictionary">A <see cref="IOrderedDictionary"/> used to store the values of the specified cell.</param>
		/// <param name="cell">The <see cref="C1GridViewCell"/> object that contains the values to retrieve.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		/// <param name="includeReadOnly">True to include the values of read-only fields; otherwise, false.</param>
		public override void ExtractValuesFromCell(IOrderedDictionary dictionary, C1GridViewCell cell, C1GridViewRowState rowState, bool includeReadOnly)
		{
			if (cell != null)
			{
				ITemplate template = ItemTemplate;

				if (((rowState & C1GridViewRowState.Alternate) != 0) && AlternatingItemTemplate != null)
				{
					template = AlternatingItemTemplate;
				}

				if (((rowState & C1GridViewRowState.Edit) != 0) && EditItemTemplate != null)
				{
					template = EditItemTemplate;
				}

				IBindableTemplate bindableTemplate = template as IBindableTemplate;
				if (bindableTemplate != null)
				{
					bool convertEmptyStrings = ConvertEmptyStringToNull;

					foreach (DictionaryEntry entry in bindableTemplate.ExtractValues(cell.BindingContainer))
					{
						object val = entry.Value;

						if (convertEmptyStrings && (val is string) && (((string)val).Length == 0))
						{
							val = null;
						}

						if (dictionary.Contains(entry.Key))
						{
							dictionary[entry.Key] = val;
						}
						else
						{
							dictionary.Add(entry.Key, val);
						}
					}
				}
			}
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal override void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			base.InitializeCell(cell, columnIndex, rowType, rowState);

			ITemplate template = null;

			switch (rowType)
			{
				case C1GridViewRowType.DataRow:

					template = _itemTemplate;

					if ((rowState & C1GridViewRowState.Edit) == 0)
					{
						if  (((rowState & C1GridViewRowState.Alternate) != 0) && (_alternatingItemTemplate != null))
						{
							template = _alternatingItemTemplate;
						}
					}
					else
					{
						if (_editItemTemplate != null)
						{
							template = _editItemTemplate;
						}
					}

					break;

				case C1GridViewRowType.Footer:
					template = _footerTemplate;
					break;

				case C1GridViewRowType.Header:
					template = _headerTemplate;
					break;
			}

			if (template != null)
			{
				template.InstantiateIn(cell);
			}
		}

		/// <summary>
		/// Marks the state of the <see cref="C1TemplateField"/> object as having been changed.
		/// </summary>
		protected internal override void SetDirty()
		{
			base.SetDirty();

			if (_updateBindings != null)
			{
				_updateBindings.SetDirty();
			}
		}

		#endregion

		#region IC1Cloneable

		/// <summary>
		/// Creates a new instance of the <see cref="C1TemplateField"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1TemplateField"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1TemplateField();
		}

		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="C1TemplateField"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);

			C1TemplateField field = copyFrom as C1TemplateField;
			if (field != null)
			{
				_alternatingItemTemplate = field.AlternatingItemTemplate;
				_editItemTemplate = field.EditItemTemplate;
				_footerTemplate = field.FooterTemplate;
				_headerTemplate = field.HeaderTemplate;
				_itemTemplate = field.ItemTemplate;

				ConvertEmptyStringToNull = field.ConvertEmptyStringToNull;

				_updateBindings = null;
				if (field._updateBindings != null)
				{
					UpdateBindings.CopyFrom(field._updateBindings);
				}
			}
		}

		#endregion

		#region IJsonRestore members, InnerState

		/// <summary>
		/// Infrastructure.
		/// </summary>
		public override System.Collections.Hashtable InnerState
		{
			get
			{
				Hashtable innerState = base.InnerState;

				if (_headerTemplate != null)
				{
					innerState["_htmpl"] = true;
				}

				if (_itemTemplate != null)
				{
					innerState["_itmpl"] = true;
				}

				if (_footerTemplate != null)
				{
					innerState["_ftmpl"] = true;
				}

				return innerState;
			}
		}

		#endregion

		#region IStateManager

		/// <summary>
		/// Restores the previously saved view state of the <see cref="C1TemplateField"/> object.
		/// </summary>
		/// <param name="state">An object that represents the <see cref="C1TemplateField"/> state to restore.</param>
		protected override void LoadViewState(object state)
		{
			Pair stateObj = state as Pair;
			if (stateObj != null)
			{
				if (stateObj.First != null)
				{
					base.LoadViewState(stateObj.First);
				}

				if (stateObj.Second != null)
				{
					((IStateManager)UpdateBindings).LoadViewState(stateObj.Second);
				}
			}
		}

		/// <summary>
		/// Saves the changes to the <see cref="C1TemplateField"/> object since the time the page was posted back to the server.
		/// </summary>
		/// <returns>The object that contains the changes to the view state of the <see cref="C1TemplateField"/>.</returns>
		protected override object SaveViewState()
		{
			Pair pair = new Pair(base.SaveViewState(),
				(_updateBindings != null) ? ((IStateManager)_updateBindings).SaveViewState() : null);

			return (pair.First != null || pair.Second != null)
				? pair
				: null;
		}

		/// <summary>
		/// Causes the <see cref="C1TemplateField"/> object to track changes to its state so that it can be persisted across requests.
		/// </summary>
		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (_updateBindings != null)
			{
				((IStateManager)_updateBindings).TrackViewState();
			}
		}

		#endregion

		#region nonpersistables

		/// <summary>
		/// Infrastructure.
		/// </summary>
		protected internal override bool NonPersistables
		{
			get { return true; }
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		protected internal override void LoadNonPersistables(object state)
		{
			if (state != null)
			{
				object[] stateObj = (object[])state;

				base.LoadNonPersistables(stateObj[0]);

				_alternatingItemTemplate = (ITemplate)stateObj[1];
				_editItemTemplate = (ITemplate)stateObj[2];
				_footerTemplate = (ITemplate)stateObj[3];
				_headerTemplate = (ITemplate)stateObj[4];
				_itemTemplate = (ITemplate)stateObj[5];
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		protected internal override object SaveNonPersistables()
		{
			object[] state = new object[] {
				base.SaveNonPersistables(),
				_alternatingItemTemplate,
				_editItemTemplate,
				_footerTemplate,
				_headerTemplate,
				_itemTemplate
			};

			return Array.TrueForAll(state, v => v == null)
				? null
				: state;
		}

		#endregion
	}
}
