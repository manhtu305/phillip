﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.HtmlControls;
	using System.Web.UI.WebControls;

	using C1.Web.Wijmo.Controls.C1GridView.Controls;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// A C1BoundField is bound to a field in a data source.
	/// </summary>	
	public class C1BoundField : C1FilterField
	{
		/// <summary>
		/// Infrastructure.
		/// </summary>
		public static readonly string ThisExpression = "!";

		#region const

		private const bool DEF_APPLYFORMATINEDITMODE = false;
		private const bool DEF_CONVERTEMPTYSTRINGTONULL = true;
		private const string DEF_DATAFORMATSTRING = "";
		private const bool DEF_HTMLENCODE = true;
		private const bool DEF_HTMLENCODEFORMATSTRING = true;
		private const IMEMode DEF_IMEMODE = IMEMode.Auto;
		private const string DEF_NULLDISPLAYTEXT = "";
		private const bool DEF_READONLY = false;

		#endregion

		private PropertyDescriptor _pd;
		private IDictionary _valueList = null;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1BoundField"/> class.
		/// </summary>
		public C1BoundField() : this(null)
		{
		}

		internal C1BoundField(IOwnerable owner) : base(owner)
		{
		}

		#region public properties

		/// <summary>
		/// Gets or sets a value indicating whether the formatting string specified by the <see cref="DataFormatString"/>
		/// property is applied to field values when the data-bound control that contains the <see cref="C1BoundField"/> object is in edit mode.
		/// </summary>
		[DefaultValue(DEF_APPLYFORMATINEDITMODE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_APPLYFORMATINEDITMODE)]
		public virtual bool ApplyFormatInEditMode
		{
			get { return GetPropertyValue("ApplyFormatInEditMode", DEF_APPLYFORMATINEDITMODE); }
			set
			{
				if (GetPropertyValue("ApplyFormatInEditMode", DEF_APPLYFORMATINEDITMODE) != value)
				{
					SetPropertyValue("ApplyFormatInEditMode", value);
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether empty string ("") values are converted to null when the field values are retrieved from the data source.
		/// </summary>
		[DefaultValue(DEF_CONVERTEMPTYSTRINGTONULL)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_CONVERTEMPTYSTRINGTONULL)]
		public virtual bool ConvertEmptyStringToNull
		{
			get { return GetPropertyValue("ConvertEmptyStringToNull", DEF_CONVERTEMPTYSTRINGTONULL); }
			set
			{
				if (GetPropertyValue("ConvertEmptyStringToNull", DEF_CONVERTEMPTYSTRINGTONULL) != value)
				{
					SetPropertyValue("ConvertEmptyStringToNull", value);
				}
			}
		}



		/// <summary>
		/// Gets or sets the string that specifies the display format for items in the column.
		/// </summary>
		/// <value>
		/// This property is used to provide a custom format for the items in the column.
		/// </value> 
		/// <remarks>
		/// <para>
		/// Note: Formatting of values is done on the client-side using globalize.min.js library. Use formatting patterns supported by that library,
		/// for more information on the library please check https://github.com/jquery/globalize
		/// </para>
		/// <para>
		/// Examples:
		/// </para>
		/// <para>
		/// <ul>
		/// <li>"n" - displays numeric values in number format.</li>
		/// <li>"d" - displays numeric values in decimal format.</li>
		/// <li>"d" - displays date values in short date format.</li>/// 
		/// <li>"p" - displays numeric values in percentage format.</li>
		/// <li>"c" - displays numeric values in currency format.</li>
		/// </ul>
		/// </para>
		/// </remarks>
		[DefaultValue(DEF_DATAFORMATSTRING)]
		[NotifyParentProperty(true)]
#if ASP_NET4
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewJqueryGlobFormatConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewJqueryGlobFormatConverter, C1.Web.Wijmo.Controls.Design.3")]
#endif
		[Json(true, true, DEF_DATAFORMATSTRING)]
		public virtual string DataFormatString
		{
			get { return GetPropertyValue("DataFormatString", DEF_DATAFORMATSTRING); }
			set
			{
				if (GetPropertyValue("DataFormatString", DEF_DATAFORMATSTRING) != value)
				{
					SetPropertyValue("DataFormatString", value);
					OnFieldChanged();
				}
			}
		}


		/// <summary>
		/// Gets or sets a value indicating whether field values are HTML-encoded before they are displayed in a <see cref="C1BoundField"/> object.
		/// </summary>
		/// <value>
		/// True if field values are HTML-encoded before they are displayed in a <see cref="C1BoundField"/>  object; otherwise, false.
		/// The default value is true.
		/// </value>
		[DefaultValue(DEF_HTMLENCODE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_HTMLENCODE)]
		public virtual bool HtmlEncode
		{
			get { return GetPropertyValue("HtmlEncode", DEF_HTMLENCODE); }
			set
			{
				if (GetPropertyValue("HtmlEncode", DEF_HTMLENCODE) != value)
				{
					SetPropertyValue("HtmlEncode", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// A value indicating the state of the input method editor for text fields.
		/// </summary>
		[DefaultValue(DEF_IMEMODE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_IMEMODE)]
		public virtual IMEMode ImeMode
		{
			get { return GetPropertyValue("ImeMode", DEF_IMEMODE); }
			set
			{
				if (GetPropertyValue("ImeMode", ImeMode) != value)
				{
					SetPropertyValue("ImeMode", value);
					OnFieldChanged();
				}
			}
		}

#if SERVERFORMATTING
		/// <summary>
		/// Gets or sets a value that indicates whether formatted text should be HTML encoded when it is displayed.
		/// </summary>
		/// <value>
		/// True if the text should be HTML-encoded; otherwise, false. The default value is true.
		/// </value>
		[DefaultValue(DEF_HTMLENCODEFORMATSTRING)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_HTMLENCODEFORMATSTRING)]
		public virtual bool HtmlEncodeFormatString
		{
			get { return GetPropertyValue("HtmlEncodeFormatString", DEF_HTMLENCODEFORMATSTRING); }
			set
			{
				if (GetPropertyValue("HtmlEncodeFormatString", DEF_HTMLENCODEFORMATSTRING) != value)
				{
					SetPropertyValue("HtmlEncodeFormatString", value);
					OnFieldChanged();
				}
			}
		}
#endif

		/// <summary>
		/// Gets or sets the caption displayed for a field when the field's value is null.
		/// </summary>
		/// <value>
		/// The caption displayed for a field when the field's value is null.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_NULLDISPLAYTEXT)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_NULLDISPLAYTEXT)]
		public virtual string NullDisplayText
		{
			get { return GetPropertyValue("NullDisplayText", DEF_NULLDISPLAYTEXT); }
			set
			{
				if (GetPropertyValue("NullDisplayText", DEF_NULLDISPLAYTEXT) != value)
				{
					SetPropertyValue("NullDisplayText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Determines whether the items in the bound column can be edited.
		/// </summary>
		[DefaultValue(DEF_READONLY)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_READONLY)]
		public virtual bool ReadOnly
		{
			get { return GetPropertyValue("ReadOnly", DEF_READONLY); }
			set
			{
				if (GetPropertyValue("ReadOnly", DEF_READONLY) != value)
				{
					SetPropertyValue("ReadOnly", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the object that implements the IDictionary interface used for textual substitution.
		/// </summary>
		[Browsable(false)]
		public IDictionary ValueList
		{
			get { return _valueList; }
			set { this._valueList = value; }
		}

		#endregion

		#region protected

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected internal override string EnsureSortExpression()
		{
			string expression = base.EnsureSortExpression();
			if (string.IsNullOrEmpty(expression))
			{
				/* expression = DataField; */ // - SortExpression property must be explicitly defined by user to use sorting.
			}

			return expression;
		}

		/// <summary>
		/// Fills the specified <see cref="IOrderedDictionary"/> object with values from the specified <see cref="C1GridViewCell"/> object.
		/// </summary>
		/// <param name="dictionary">A <see cref="IOrderedDictionary"/> used to store the values of the specified cell.</param>
		/// <param name="cell">The <see cref="C1GridViewCell"/> object that contains the values to retrieve.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		/// <param name="includeReadOnly">True to include the values of read-only fields; otherwise, false.</param>
		public override void ExtractValuesFromCell(IOrderedDictionary dictionary, C1GridViewCell cell, C1GridViewRowState rowState, bool includeReadOnly)
		{
			base.ExtractValuesFromCell(dictionary, cell, rowState, includeReadOnly);

			if (cell != null)
			{
				object val = null;

				if (cell.Controls.Count > 0)
				{
					TextBox textBox = cell.Controls[0] as TextBox;
					if (textBox != null)
					{
						val = textBox.Text;
					}
					else
					{
						C1InplaceEditor c1editor = cell.Controls[0] as C1InplaceEditor;
						if (c1editor != null)
						{
							val = c1editor.ExtractValue();
						}
					}
				}
				else
				{
					if (includeReadOnly)
					{
						string cellText = cell.Text;

						if (cellText == C1GridView.NON_BREAKING_SPACE)
						{
							val = string.Empty;
						}
						else 
						{
							val = (HtmlEncode)
								? System.Web.HttpUtility.HtmlDecode(cellText)
								: cellText;
						}
					}
				}

				if (val != null)
				{
					string strVal = val as string;
					if (strVal != null)
					{
						if (((strVal.Length == 0) && ConvertEmptyStringToNull) || ((strVal.Length > 0) && (strVal == NullDisplayText)))
						{
							val = null;
						}
					}

					string key = DataField;

					if (dictionary.Contains(key))
					{
						dictionary[key] = val;
					}
					else
					{
						dictionary.Add(key, val);
					}
				}
			}
		}


		/// <summary>
		/// Formats the specified field value for a cell in the <see cref="C1BoundField"/> object. 
		/// </summary>
		/// <param name="dataValue">The field value to format.</param>
		/// <param name="encode">True to encode the value; otherwise, false.</param>
		/// <returns>The field value converted to the format specified by<see cref="DataFormatString"/>.</returns>
		protected virtual string FormatDataValue(object dataValue, bool encode)
		{
			if (!C1BaseField.IsNull(dataValue))
			{
#if SERVERFORMATTING
				string strVal = dataValue.ToString();
#else
				if (dataValue is TimeSpan)
				{
					dataValue = new DateTime(((TimeSpan)dataValue).Ticks);
				}

				string strVal = string.Empty;

				if (dataValue is DateTime)
				{
                    // TFS: 398360
                    // Fixing DateTime: not allow to convert to js timestamp number when Post Back
                    strVal = (GridView.Page.IsPostBack && string.IsNullOrEmpty(DataFormatString))
                        ? dataValue.ToString()
                        : ((DateTime)dataValue)._jsGetTime().ToString(); // convet date to the js timestamp (the number of milliseconds since midnight Jan 1, 1970).
                }
				else
				{
					IFormattable formattable = dataValue as IFormattable;

					strVal = (formattable != null)
						? formattable.ToString(string.Empty, System.Globalization.CultureInfo.InvariantCulture) // should be in sync with the c1gridview._getDOMReaderCulture method.
						: dataValue.ToString();
				}

#endif
				if (strVal.Length == 0 && ConvertEmptyStringToNull)
				{
					return NullDisplayText;
				}

#if SERVERFORMATTING
				if (HtmlEncodeFormatString)
				{
					if (!string.IsNullOrEmpty(DataFormatString))
					{
						strVal = string.Format(DataFormatString, dataValue);
					}

					if (!string.IsNullOrEmpty(strVal) && encode)
					{
						strVal = System.Web.HttpUtility.HtmlEncode(strVal);
					}
				}
				else
				{
					if (strVal.Length > 0 && encode)
					{
						strVal = System.Web.HttpUtility.HtmlEncode(strVal);
					}

					if (DataFormatString.Length > 0)
					{
						strVal = encode
							? string.Format(DataFormatString, strVal)
							: string.Format(DataFormatString, dataValue);
					}
				}
#else
				if (strVal.Length > 0 && encode)
				{
					strVal = System.Web.HttpUtility.HtmlEncode(strVal);
				}
#endif
				return strVal;
			}

			return (ConvertEmptyStringToNull)
				? NullDisplayText
				: string.Empty;
		}

		/// <summary>
		/// Retrieves the value used for a field's value when rendering the <see cref="C1BoundField"/> object in a designer.
		/// </summary>
		/// <param name="rowState">One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.C1GridViewRowState"/> values.</param>
		/// <returns>The value to display in the designer as the field's value.</returns>
		protected virtual object GetDesignTimeValue(C1GridViewRowState rowState)
		{
			return C1Localizer.GetString("C1GridView.FieldDesignTimeValue", "Databound");
		}

		/// <summary>
		/// Retrieves the value of the field bound to the <see cref="C1BoundField"/> object.
		/// </summary>
		/// <param name="row">The container for the field value.</param>
		/// <returns>The value of the field bound to the <see cref="C1BoundField"/>.</returns>
		protected object GetValue(C1GridViewRow row)
		{
			/*
			TODO
			if (ColumnInfo != null)
			{
				return ColumnInfo.GetValue(DataBinder.GetDataItem(row));
			}
			else
			{*/
			object dataItem = null;
			string dataField = DataField;

			if (!string.IsNullOrEmpty(dataField))
			{
				dataItem = DataBinder.GetDataItem(row);

				if (dataItem == null && !DesignMode)
				{
					throw new HttpException(C1Localizer.GetString("C1Grid.EDataItemNotFound"));
				}

				if (_pd == null)
				{
					_pd = TypeDescriptor.GetProperties(dataItem).Find(dataField, true);

					if (_pd == null && !DesignMode && (dataField != ThisExpression))
					{
						throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataFieldNotFound"), dataField));
					}
				}

				if (_pd != null && dataItem != null)
				{
					object obj = _pd.GetValue(dataItem);

					/* TODO
					if (!_values.ContainsKey(row.RowIndex))
					{
						_values.Add(row.RowIndex, (obj != null) ? obj.ToString() : null);
					}*/

					return obj;
				}

				if (DesignMode)
				{
					return GetDesignTimeValue(row.RowState);
				}
			}
			/*}*/

			return dataItem;
		}


		internal void GetC1InplaceEditorMarkupFromCell(IOrderedDictionary dictionary, C1GridViewCell cell)
		{
			if (!ReadOnly && cell != null && cell.Controls.Count > 0) 
			{
				C1InplaceEditor editor = cell.Controls[0] as C1InplaceEditor;
				if (editor != null)
				{
					string html = string.Empty;

					using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
					using (HtmlTextWriter htmlWriter = new C1GridView.NoFormatHtmlTextWriter(stringWriter))
					{
						editor.RenderControl(htmlWriter);
						htmlWriter.Flush();

						html = System.Web.HttpUtility.HtmlEncode(stringWriter.ToString()); // encode, otherwise a security exception will be thrown on postback (because of html tags).
					}

					dictionary.Add(UID.ToString(), html);
				}
			}
		}


		/// <summary>
		/// Initializes the <see cref="C1BoundField"/> object.
		/// </summary>
		/// <param name="gridView">C1GridView.</param>
		protected internal override void Initialize(C1GridView gridView)
		{
			base.Initialize(gridView);

			_pd = null;
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal override void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			base.InitializeCell(cell, columnIndex, rowType, rowState);

			switch (rowType)
			{
				case C1GridViewRowType.DataRow:
					InitializeDataCell(cell, columnIndex, rowType, rowState);
					break;
				case C1GridViewRowType.Filter:
					InitializeFilterCell(cell, columnIndex, rowType, rowState);
					break;
			}
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected virtual void InitializeFilterCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			if (DesignMode && ShowFilter)
			{
				HtmlGenericControl containerDiv = new HtmlGenericControl("div");
				containerDiv.Attributes.Add("class", "wijmo-wijgrid-filter ui-widget ui-state-default");
				HtmlGenericControl innerDiv = new HtmlGenericControl("div");
				innerDiv.Style.Add("width", "100%");
				innerDiv.Attributes.Add("class", "wijmo-wijinput ui-widget ui-state-default");
				HtmlGenericControl inputSpan = new HtmlGenericControl("span");
				inputSpan.Attributes.Add("class", "wijmo-wijinput-wrapper");
				HtmlInputText filterControl = new HtmlInputText();
				filterControl.Attributes.Add("class", "wijmo-wijgrid-filter-input wijmo-wijinput-input");
				filterControl.Style.Add("width", "100%");
				filterControl.Style.Add("height", "26px");
				inputSpan.Controls.Add(filterControl);
				innerDiv.Controls.Add(inputSpan);
				containerDiv.Controls.Add(innerDiv);
				HtmlAnchor drpButton = new HtmlAnchor();
				drpButton.Attributes.Add("class", "wijmo-wijgrid-filter-trigger ui-state-default");
				drpButton.Style.Add("z-index", "5");
				HtmlGenericControl aSpan = new HtmlGenericControl("span");
				aSpan.Attributes.Add("class", "ui-icon ui-icon-triangle-1-s");
				drpButton.Controls.Add(aSpan);
				containerDiv.Controls.Add(drpButton);
				cell.Controls.Add(containerDiv);
			}
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected virtual void InitializeDataCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			Control boundControl = null;

			if (((rowState & C1GridViewRowState.Edit) != 0) && !ReadOnly)
			{
				bool useRegularTextBox = this.GridView == null || !this.GridView.AllowC1InputEditors;

				if (useRegularTextBox)
				{
					boundControl = new TextBox();
					((WebControl)boundControl).Style.Add("ime-mode", ImeMode.ToString().ToLowerInvariant());
					((WebControl)boundControl).Attributes.Add("aria-label", "editor");
				}
				else
				{
					boundControl = new C1InplaceEditor(columnIndex, this._DataType, this.ImeMode, this.ApplyFormatInEditMode, this.DataFormatString, this.NullDisplayText);
				}

				cell.Controls.Add(boundControl);
			}
			else
			{
				boundControl = cell;
			}

			if (boundControl != null)
			{
				boundControl.DataBinding += new EventHandler(OnDataBindField);
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <returns></returns>
		protected override string MonikerInternal()
		{
			return DataField;
		}

		/// <summary>
		/// Binds the value of a field to the <see cref="C1BoundField"/> object.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnDataBindField(object sender, EventArgs e)
		{
			C1GridViewRow row = (C1GridViewRow)((Control)sender).NamingContainer;
			TableCell cell = sender as TableCell;

			object dataVal = GetValue(row);
			string strVal = FormatDataValue(dataVal, HtmlEncode && (cell != null));

			if (cell != null)
			{
				if (ValueList != null)
				{
					object obj = ValueList[strVal];
					if (obj != null)
					{
						strVal = obj.ToString();
					}
				}

				if (strVal.Length == 0)
				{
					strVal = C1GridView.NON_BREAKING_SPACE;
				}

				((TableCell)sender).Text = strVal;
			}
			else
			{
				TextBox editBox = sender as TextBox;

				if (editBox != null)
				{
					if (ApplyFormatInEditMode)
					{
						editBox.Text = strVal;
					}
					else
					{
						if (dataVal != null)
						{
							editBox.Text = dataVal.ToString();
						}

						cell = editBox.Parent as TableCell;
						if (cell != null)
						{
							cell.Attributes.Add("wijgrid-data", FormatDataValue(dataVal, false));
						}
					}
				}
				else
				{
					C1InplaceEditor c1editor = sender as C1InplaceEditor;

					if (c1editor != null)
					{
						c1editor.Value = dataVal;

						cell = c1editor.Parent as TableCell;
						if (cell != null)
						{
							cell.Attributes.Add("wijgrid-data", FormatDataValue(dataVal, false));
						}
					}
				}
			}
		}

		#endregion

		#region IC1Cloneable

		/// <summary>
		/// Creates a new instance of the <see cref="C1BoundField" /> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1BoundField" /> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1BoundField();
		}

		#endregion
	}
}
