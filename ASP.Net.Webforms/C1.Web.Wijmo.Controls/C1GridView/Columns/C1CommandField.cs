﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using System.Drawing.Design;
	using System.Web.UI;
	using System.Web.UI.Design;

	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// A special column used to display command buttons that perform delete, edit, select or filter operations.
	/// </summary>
	public class C1CommandField : C1ButtonFieldBase 
	{
		private const bool DEF_CAUSESVALIDATION = true;

		private const string DEF_CANCELIMAGEURL = "";
		private const string DEF_CANCELTEXT = "Cancel";
		private const string DEF_DELETEIMAGEURL = "";
		private const string DEF_DELETETEXT = "Delete";
		private const string DEF_EDITIMAGEURL = "";
		private const string DEF_EDITTEXT = "Edit";
		private const string DEF_FILTERTEXT = "Filter";
		private const string DEF_FILTERIMAGEURL = "";
		private const string DEF_SELECTIMAGEURL = "";
		private const string DEF_SELECTTEXT = "Select";
		private const string DEF_UPDATEIMAGEURL = "";
		private const string DEF_UPDATETEXT = "Update";
		private const bool DEF_SHOWCANCELBUTTON = true;
		private const bool DEF_SHOWDELETEBUTTON = false;
		private const bool DEF_SHOWEDITBUTTON = false;
		private const bool DEF_SHOWFILTERBUTTON = false;
		private const bool DEF_SHOWSELECTBUTTON = false;

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1CommandField"/> class.
		/// </summary>
		public C1CommandField() : this(null)
		{
		}

		internal C1CommandField(IOwnerable owner) : base(owner)
		{
			CancelText = C1Localizer.GetString("C1GridView.C1CommandField.CancelText", DEF_CANCELTEXT);
			DeleteText = C1Localizer.GetString("C1GridView.C1CommandField.DeleteText", DEF_DELETETEXT);
			EditText = C1Localizer.GetString("C1GridView.C1CommandField.EditText", DEF_EDITTEXT);
			FilterText = C1Localizer.GetString("C1GridView.C1CommandField.FilterText",  DEF_FILTERTEXT);
			SelectText = C1Localizer.GetString("C1GridView.C1CommandField.SelectText",  DEF_SELECTTEXT);
			UpdateText = C1Localizer.GetString("C1GridView.C1CommandField.UpdateText", DEF_UPDATETEXT);
		}

		#region properties

		/// <summary>
		/// Gets or sets a value indicating whether validation is performed when a button in a column is clicked.
		/// </summary>
		/// <returns>
		/// True to perform validation when a button in a column is clicked, otherwise false.
		/// </returns>
		/// <value>
		/// The default value is true.
		/// </value>
		[DefaultValue(DEF_CAUSESVALIDATION)]
		[Json(true, true, DEF_CAUSESVALIDATION)]
		public override bool CausesValidation
		{
			get	{ return GetPropertyValue("CausesValidation", DEF_CAUSESVALIDATION); }
			set	{ base.CausesValidation = value; }
		}

		/// <summary>
		/// Gets or sets the URL to the Cancel button image displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The URL to an image to display for the Cancel button in a <see cref="C1CommandField"/>.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_CANCELIMAGEURL)]
		[Json(true, true, DEF_CANCELIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		public virtual string CancelImageUrl
		{
			get { return GetPropertyValue("CancelImageUrl", DEF_CANCELIMAGEURL); }
			set
			{
				if (GetPropertyValue("CancelImageUrl", DEF_CANCELIMAGEURL) != value)
				{
					SetPropertyValue("CancelImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the Cancel button caption displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The caption for the Cancel button in a <see cref="C1CommandField"/>.
		/// The default value is "Cancel".
		/// </value>
		[DefaultValue(DEF_CANCELTEXT)]
		[Json(true, false, DEF_CANCELTEXT)]
		public virtual string CancelText
		{
			get { return GetPropertyValue("CancelText", DEF_CANCELTEXT); }
			set
			{
				if (GetPropertyValue("CancelText", DEF_CANCELTEXT) != value)
				{
					SetPropertyValue("CancelText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL to the Delete button image displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The URL to an image to display for the Delete button in a <see cref="C1CommandField"/>.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_DELETEIMAGEURL)]
		[Json(true, true, DEF_DELETEIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		public virtual string DeleteImageUrl
		{
			get { return GetPropertyValue("DeleteImageUrl", DEF_DELETEIMAGEURL); }
			set
			{
				if (GetPropertyValue("DeleteImageUrl", DEF_DELETEIMAGEURL) != value)
				{
					SetPropertyValue("DeleteImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the Delete button caption displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The caption for the Delete button in a <see cref="C1CommandField"/> field.
		/// The default value is "Delete".
		/// </value>
		[DefaultValue(DEF_DELETETEXT)]
		[Json(true, false, DEF_DELETETEXT)]
		public virtual string DeleteText
		{
			get { return GetPropertyValue("DeleteText", DEF_DELETETEXT); }
			set
			{
				if (GetPropertyValue("DeleteText", DEF_DELETETEXT) != value)
				{
					SetPropertyValue("DeleteText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL to the Edit button image displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The URL to an image to display for the Edit button in a <see cref="C1CommandField"/>.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_EDITIMAGEURL)]
		[Json(true, true, DEF_EDITIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		public virtual string EditImageUrl
		{
			get { return GetPropertyValue("EditImageUrl", DEF_EDITIMAGEURL); }
			set
			{
				if (GetPropertyValue("EditImageUrl", DEF_EDITIMAGEURL) != value)
				{
					SetPropertyValue("EditImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the Edit button caption displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The caption for the Edit button in a <see cref="C1CommandField"/>.
		/// The default value is "Edit".
		/// </value>
		[DefaultValue(DEF_EDITTEXT)]
		[Json(true, false, DEF_EDITTEXT)]
		public virtual string EditText
		{
			get { return GetPropertyValue("EditText", DEF_EDITTEXT); }
			set
			{
				if (GetPropertyValue("EditText", DEF_EDITTEXT) != value)
				{
					SetPropertyValue("EditText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the Filter button caption displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The caption for the Filter button in a <see cref="C1CommandField"/>.
		/// The default value is "Filter".
		/// </value>
		[DefaultValue(DEF_FILTERTEXT)]
		[Json(true, false, DEF_FILTERTEXT)]
		public virtual string FilterText
		{
			get { return GetPropertyValue("FilterText", DEF_FILTERTEXT); }
			set
			{
				if (GetPropertyValue("FilterText", DEF_FILTERTEXT) != value)
				{
					SetPropertyValue("FilterText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL to the Filter button image displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The URL to an image to display for the Filter button in a <see cref="C1CommandField"/>.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_FILTERIMAGEURL)]
		[Json(true, true, DEF_FILTERIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		public virtual string FilterImageUrl
		{
			get { return GetPropertyValue("FilterImageUrl", DEF_FILTERIMAGEURL); }
			set
			{
				if (GetPropertyValue("FilterImageUrl", DEF_FILTERIMAGEURL) != value)
				{
					SetPropertyValue("FilterImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL to the Select button image displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The URL to an image to display for the Select button in a <see cref="C1CommandField"/>.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_SELECTIMAGEURL)]
		[Json(true, true, DEF_SELECTIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		public virtual string SelectImageUrl
		{
			get { return GetPropertyValue("SelectImageUrl", DEF_SELECTIMAGEURL); }
			set
			{
				if (GetPropertyValue("SelectImageUrl", DEF_SELECTIMAGEURL) != value)
				{
					SetPropertyValue("SelectImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the Select button caption displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The caption for the Select button in a <see cref="C1CommandField"/>.
		/// The default value is "Select".
		/// </value>
		[DefaultValue(DEF_SELECTTEXT)]
		[Json(true, false, DEF_SELECTTEXT)]
		public virtual string SelectText
		{
			get { return GetPropertyValue("SelectText", DEF_SELECTTEXT); }
			set
			{
				if (GetPropertyValue("SelectText", DEF_SELECTTEXT) != value)
				{
					SetPropertyValue("SelectText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL to the Update button image displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The URL to an image to display for the Update button in a <see cref="C1CommandField"/>.
		/// The default value is an empty string (""), which indicates that this property is not set.
		/// </value>
		[DefaultValue(DEF_UPDATEIMAGEURL)]
		[Json(true, true, DEF_UPDATEIMAGEURL)]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		public virtual string UpdateImageUrl
		{
			get { return GetPropertyValue("UpdateImageUrl", DEF_UPDATEIMAGEURL); }
			set
			{
				if (GetPropertyValue("UpdateImageUrl", DEF_UPDATEIMAGEURL) != value)
				{
					SetPropertyValue("UpdateImageUrl", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the Update button caption displayed in a <see cref="C1CommandField"/> field.
		/// </summary>
		/// <value>
		/// The caption for the Update button in a <see cref="C1CommandField"/>.
		/// The default value is "Update".
		/// </value>
		[DefaultValue(DEF_UPDATETEXT)]
		[Json(true, false, DEF_UPDATETEXT)]
		public virtual string UpdateText
		{
			get { return GetPropertyValue("UpdateText", DEF_UPDATETEXT); }
			set
			{
				if (GetPropertyValue("UpdateText", DEF_UPDATETEXT) != value)
				{
					SetPropertyValue("UpdateText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether a Cancel button is displayed in a <see cref="C1CommandField"/> column.
		/// </summary>
		/// <value>
		/// True to display a Cancel button in a <see cref="C1CommandField"/>; otherwise, false. The default is true.
		/// </value>
		[DefaultValue(DEF_SHOWCANCELBUTTON)]
		[Json(true, true, DEF_SHOWCANCELBUTTON)]
		public bool ShowCancelButton
		{
			get { return GetPropertyValue("ShowCancelButton", DEF_SHOWCANCELBUTTON); }
			set
			{
				if (GetPropertyValue("ShowCancelButton", DEF_SHOWCANCELBUTTON) != value)
				{
					SetPropertyValue("ShowCancelButton", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether a Delete button is displayed in a <see cref="C1CommandField"/> column.
		/// </summary>
		/// <value>
		/// True to display a Delete button in a <see cref="C1CommandField"/>; otherwise, false. The default is false.
		/// </value>
		[DefaultValue(DEF_SHOWDELETEBUTTON)]
		[Json(true, true, DEF_SHOWDELETEBUTTON)]
		public bool ShowDeleteButton
		{
			get { return GetPropertyValue("ShowDeleteButton", DEF_SHOWDELETEBUTTON); }
			set
			{
				if (GetPropertyValue("ShowDeleteButton", DEF_SHOWDELETEBUTTON) != value)
				{
					SetPropertyValue("ShowDeleteButton", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether a Edit button is displayed in a <see cref="C1CommandField"/> column.
		/// </summary>
		/// <value>
		/// True to display a Edit button in a <see cref="C1CommandField"/>; otherwise, false. The default value is false.
		/// </value>
		[DefaultValue(DEF_SHOWEDITBUTTON)]
		[Json(true, true, DEF_SHOWEDITBUTTON)]
		public bool ShowEditButton
		{
			get { return GetPropertyValue("ShowEditButton", DEF_SHOWEDITBUTTON); }
			set
			{
				if (GetPropertyValue("ShowEditButton", DEF_SHOWEDITBUTTON) != value)
				{
					SetPropertyValue("ShowEditButton", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether a Filter button is displayed in a <see cref="C1CommandField"/> column.
		/// </summary>
		/// <value>
		/// True to display a Filter button in a <see cref="C1CommandField"/>; otherwise, false. The default value is false.
		/// </value>
		[DefaultValue(DEF_SHOWFILTERBUTTON)]
		[Json(true, true, DEF_SHOWFILTERBUTTON)]
		public bool ShowFilterButton
		{
			get { return GetPropertyValue("ShowFilterButton", DEF_SHOWFILTERBUTTON); }
			set
			{
				if (GetPropertyValue("ShowFilterButton", DEF_SHOWFILTERBUTTON) != value)
				{
					SetPropertyValue("ShowFilterButton", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether a Select button is displayed in a <see cref="C1CommandField"/> column.
		/// </summary>
		/// <value>
		/// True to display a Select button in a <see cref="C1CommandField"/>; otherwise, false. The default value is false.
		/// </value>
		[DefaultValue(DEF_SHOWSELECTBUTTON)]
		[Json(true, true, DEF_SHOWSELECTBUTTON)]
		public bool ShowSelectButton
		{
			get { return GetPropertyValue("ShowSelectButton", DEF_SHOWSELECTBUTTON); }
			set
			{
				if (GetPropertyValue("ShowSelectButton", DEF_SHOWSELECTBUTTON) != value)
				{
					SetPropertyValue("ShowSelectButton", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		#region overrides

		/// <summary>
		/// Creates a new instance of the <see cref="C1CommandField"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1CommandField"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1CommandField();
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal override void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			base.InitializeCell(cell, columnIndex, rowType, rowState);

			string cmdArgument = GridView.Rows.Count.ToString();

			switch (rowType)
			{
				case C1GridViewRowType.DataRow:

					if ((rowState & C1GridViewRowState.Edit) != 0)
					{
						if (ShowEditButton)
						{
							cell.Controls.Add(GetButtonControl(cmdArgument, C1GridView.CMD_UPDATE, UpdateText,
								GridView.CallbackEditing ? false : CausesValidation,
								ValidationGroup, UpdateImageUrl, GridView.CallbackEditing));

							if (ShowCancelButton)
							{
								cell.Controls.Add(new LiteralControl(C1GridView.NON_BREAKING_SPACE));
								cell.Controls.Add(GetButtonControl(cmdArgument, C1GridView.CMD_CANCEL, CancelText, false,
									string.Empty, CancelImageUrl, GridView.CallbackEditing));
							}
						}
					}
					else
					{
						bool insertNbsp = false;
						if (ShowEditButton)
						{
							cell.Controls.Add(GetButtonControl(cmdArgument, C1GridView.CMD_EDIT, EditText, false,
								string.Empty, EditImageUrl, GridView.CallbackEditing));

							insertNbsp = true;
						}

						if (ShowDeleteButton)
						{
							if (insertNbsp)
							{
								cell.Controls.Add(new LiteralControl(C1GridView.NON_BREAKING_SPACE));
							}

							cell.Controls.Add(GetButtonControl(cmdArgument, C1GridView.CMD_DELETE, DeleteText, false,
								string.Empty, DeleteImageUrl, GridView.CallbackEditing));

							insertNbsp = true;
						}

						if (ShowSelectButton)
						{
							if (insertNbsp)
							{
								cell.Controls.Add(new LiteralControl(C1GridView.NON_BREAKING_SPACE));
							}

							cell.Controls.Add(GetButtonControl(cmdArgument, C1GridView.CMD_SELECT, SelectText, false,
								string.Empty, SelectImageUrl, GridView.CallbackSelection));
						}
					}

					break;
				
				case C1GridViewRowType.Filter:
					if (ShowFilterButton)
					{
						cell.Controls.Add(GetButtonControl(cmdArgument, C1GridView.CMD_FILTER, FilterText, false,
							string.Empty, FilterImageUrl, GridView.CallbackFiltering));
							
					}
					break;
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <returns></returns>
		protected override string MonikerInternal()
		{
			string moniker = string.Empty;

			int compos = (ShowEditButton ? 1 : 0) | (ShowSelectButton ? 2 : 0) | (ShowDeleteButton ? 4 : 0);

			switch (compos)
			{
				case 1:
					moniker = C1Localizer.GetString("C1GridView.C1CommandField.EditUpdateCancelText", string.Format("{0}, {1}, {2}", DEF_EDITTEXT, DEF_UPDATETEXT, DEF_CANCELTEXT));
					break;

				case 2:
					moniker = C1Localizer.GetString("C1GridView.C1CommandField.SelectText", DEF_SELECTTEXT);
					break;

				case 4:
					moniker = C1Localizer.GetString("C1GridView.C1CommandField.DeleteText", DEF_UPDATETEXT);
					break;
			}

			return moniker;
		}

		#endregion
	}
}
