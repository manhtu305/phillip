﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using System.Web;
	using System.Web.UI.Design;
	using System.Web.UI.WebControls;

	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// A C1HyperLinkField displays each item as a hyperlink.
	/// </summary>
	public class C1HyperLinkField : C1Field
	{
		private const string[] DEF_DATANAVIGATEURLFIELDS = null;
		private const string DEF_DATANAVIGATEURLFORMATSTRING = "";
		private const string DEF_DATATEXTFIELD = "";
		private const string DEF_DATATEXTFORMATSTRING = "";
		private const string DEF_NAVIGATEURL = "";
		private const string DEF_TARGET = "";
		private const string DEF_TEXT = "";

		private PropertyDescriptor[] _urlFieldDesc;
		private PropertyDescriptor _textFieldDesc;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1HyperLinkField"/> class.
		/// </summary>
		public C1HyperLinkField() : this(null)
		{
		}

		internal C1HyperLinkField(IOwnerable owner)	: base(owner)
		{
		}

		#region public properties

		/// <summary>
		/// Gets or sets the names of the fields from the data source used to construct the URLs for the
		/// hyperlinks in the <see cref="C1HyperLinkField"/>.
		/// </summary>
		/// <remarks>
		/// If the DataNavigateUrlFields and the <see cref="NavigateUrl"/> properties are both set, the DataNavigateUrlFields property takes precedence.
		/// </remarks>
		/// <value>
		/// An array containing the names of the fields from the data source used to construct the URLs for the
		/// hyperlinks in the <see cref="C1HyperLinkField"/>.
		/// The default is an empty array, indicating that DataNavigateUrlFields is not set.
		/// </value>
		[DefaultValue(null)]
		[TypeConverter(typeof(StringArrayConverter))]
		[Json(true)]
		public virtual string[] DataNavigateUrlFields
		{
			get
			{
				string[] value = GetPropertyValue("DataNavigateUrlFields", (string[])null);

				return (value != null)
					? (string[])value.Clone()
					: new string[0];
			}
			set
			{
				if (!StringArrayHelper.Compare(GetPropertyValue("DataNavigateUrlFields", (string[])null), value))
				{
					SetPropertyValue("DataNavigateUrlFields", value != null ? (string[])value.Clone() : value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the display format for the URL of the hyperlinks in the <see cref="C1HyperLinkField"/>
		/// when the URL is data-bound to a field in a data source.
		/// </summary>
		/// <remarks>
		/// The specified format is only applied to the URL when the URL is data-bound to a field in a data source.
		/// Specify the fields to bind to the URL of the hyperlinks in the column by setting the
		/// <see cref="DataNavigateUrlFields"/> property.
		/// </remarks>
		[DefaultValue(DEF_DATANAVIGATEURLFORMATSTRING)]
		[Json(true, true, DEF_DATANAVIGATEURLFORMATSTRING)]
		#if ASP_NET45
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.3")]
#endif
		public virtual string DataNavigateUrlFormatString
		{
			get { return GetPropertyValue("DataNavigateUrlFormatString", DEF_DATANAVIGATEURLFORMATSTRING); }
			set
			{
				if (GetPropertyValue("DataNavigateUrlFormatString", DEF_DATANAVIGATEURLFORMATSTRING) != value)
				{
					SetPropertyValue("DataNavigateUrlFormatString", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the field to bind to the text of the hyperlink.
		/// </summary>
		/// <remarks>
		/// The DataTextField and <see cref="Text"/> properties cannot both be set at the same time.
		/// If both properties are set, the DataTextField property takes precedence.
		/// </remarks>
		[DefaultValue(DEF_DATATEXTFIELD)]
		[Json(true, true, DEF_DATATEXTFIELD)]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		public virtual string DataTextField
		{
			get { return GetPropertyValue("DataTextField", DEF_DATANAVIGATEURLFORMATSTRING); }
			set
			{
				if (GetPropertyValue("DataTextField", DEF_DATANAVIGATEURLFORMATSTRING) != value)
				{
					SetPropertyValue("DataTextField", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the display format for the text caption of the hyperlinks in the C1HyperLinkField column.
		/// </summary>
		/// <remarks>
		/// The specified format is applied to the text caption only when the text caption is data bound to a field in
		/// a data source. Specify the field to bind to the text caption of the hyperlinks in the column by setting
		/// the <see cref="DataTextField"/> property.
		/// </remarks>
		[DefaultValue(DEF_DATATEXTFORMATSTRING)]
		[Json(true, true, DEF_DATATEXTFORMATSTRING)]
		#if ASP_NET45
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewFormatConverter, C1.Web.Wijmo.Controls.Design.3")]
#endif
		public virtual string DataTextFormatString
		{
			get { return GetPropertyValue("DataTextFormatString", DEF_DATATEXTFORMATSTRING); }
			set
			{
				if (GetPropertyValue("DataTextFormatString", DEF_DATATEXTFORMATSTRING) != value)
				{
					SetPropertyValue("DataTextFormatString", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the URL when a hyperlink is clicked.
		/// </summary>
		/// <remarks>
		/// The <see cref="DataNavigateUrlFields"/> and NavigateUrl properties cannot both be set at the same time.
		/// If both properties are set, the <see cref="DataNavigateUrlFields"/> property takes precedence.
		/// </remarks>
		[DefaultValue(DEF_NAVIGATEURL)]
		[Json(true, true, DEF_NAVIGATEURL)]
		[Editor(typeof(UrlEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public virtual string NavigateUrl
		{
			get { return GetPropertyValue("NavigateUrl", DEF_NAVIGATEURL); }
			set
			{
				if (GetPropertyValue("NavigateUrl", DEF_NAVIGATEURL) != value)
				{
					SetPropertyValue("NavigateUrl", value);
					OnFieldChanged();
				}
			}
		}


		/// <summary>
		/// Gets or sets the window or frame where content appears when a hyperlink is clicked.
		/// </summary>
		/// <remarks>
		/// If this property is not set, the browser or window with focus refreshes when a hyperlink in the column is clicked.
		/// </remarks>
		[DefaultValue(DEF_TARGET)]
		[Json(true, true, DEF_TARGET)]
		[TypeConverter(typeof(TargetConverter))]
		public virtual string Target
		{
			get { return GetPropertyValue("Target", DEF_TARGET); }
			set
			{
				if (GetPropertyValue("Target", DEF_TARGET) != value)
				{
					SetPropertyValue("Target", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the text for the hyperlink.
		/// </summary>
		/// <remarks>
		/// The <see cref="DataTextField"/> and Text properties cannot both be set at the same time.
		/// If both properties are set, the<see cref="DataTextField"/> property takes precedence.
		/// </remarks>
		[DefaultValue(DEF_TEXT)]
		[Json(true, true, DEF_TEXT)]
		public virtual string Text
		{
			get { return GetPropertyValue("Text", DEF_TEXT); }
			set
			{
				if (GetPropertyValue("Text", DEF_TEXT) != value)
				{
					SetPropertyValue("Text", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		#region (de)serialization 

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <param name="value"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeDataNavigateUrlFields(object value)
		{
			System.Collections.ArrayList list = value as System.Collections.ArrayList;

			if (list != null)
			{
				DataNavigateUrlFields = (string[])list.ToArray(typeof(string));
			}
			else
			{
				DataNavigateUrlFields = null;
			}
		}

		#endregion

		#region overrides, protected

		/// <summary>
		/// Creates an empty <see cref="C1HyperLinkField"/> object.
		/// </summary>
		/// <returns>An empty <see cref="C1HyperLinkField"/> object.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1HyperLinkField();
		}

		/// <summary>
		/// Formats the navigation URL using the format string specified by the <see cref="DataNavigateUrlFormatString"/> property.
		/// </summary>
		/// <param name="dataUrlValues">An array of values to combine with the format string.</param>
		protected virtual string FormatDataNavigateUrlValue(params object[] dataUrlValues)
		{
			string formatedValue = string.Empty;

			if (dataUrlValues != null && dataUrlValues.Length > 0)
			{
				string dataNavigateUrlFormatString = DataNavigateUrlFormatString;
				if (string.IsNullOrEmpty(dataNavigateUrlFormatString))
				{
					object dataUrlVal = dataUrlValues[0];

					if (!C1BaseField.IsNull(dataUrlVal))
					{
						formatedValue = dataUrlVal.ToString();
					}
				}
				else
				{
					formatedValue = string.Format(dataNavigateUrlFormatString, dataUrlValues);
				}
			}

			return formatedValue;
		}

		/// <summary>
		/// Formats the caption text using the format string specified by the <see cref="DataTextFormatString"/> property.
		/// </summary>
		/// <param name="dataTextValue">The text value to format. </param>
		/// <returns>The formatted text value.</returns>
		protected virtual string FormatDataTextValue(object dataTextValue)
		{
			string formattedValue = String.Empty;

			if (!C1BaseField.IsNull(dataTextValue))
			{
				if (DataTextFormatString.Length == 0)
					formattedValue = dataTextValue.ToString();
				else
					formattedValue = String.Format(DataTextFormatString, dataTextValue);
			}
			return formattedValue;
		}

		/// <summary>
		/// Initializes the <see cref="C1HyperLinkField"/> object.
		/// </summary>
		/// <param name="gridView">C1GridView.</param>
		protected internal override void Initialize(C1GridView gridView)
		{
			base.Initialize(gridView);
			_textFieldDesc = null;
			_urlFieldDesc = null;
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected internal override void InitializeCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			base.InitializeCell(cell, columnIndex, rowType, rowState);

			if (rowType != C1GridViewRowType.Header && rowType != C1GridViewRowType.Footer /*&& rowType != C1GridViewRowType.GroupColumn*/)
			{
				HyperLink hyperLink = new HyperLink();

				hyperLink.Text = Text;
				hyperLink.NavigateUrl = NavigateUrl;
				hyperLink.Target = Target;

				if (DataNavigateUrlFields.Length != 0 || DataTextField.Length != 0)
				{
					hyperLink.DataBinding += new EventHandler(OnDataBindColumn);
				}

				cell.Controls.Add(hyperLink);
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		/// <returns></returns>
		protected override string MonikerInternal()
		{
			return Text;
		}

		#endregion

		#region private

		private void OnDataBindColumn(object sender, EventArgs e)
		{
			HyperLink hyperLink = (HyperLink)sender;
			object dataItem = ((C1GridViewRow)hyperLink.NamingContainer).DataItem;
			if (_textFieldDesc == null) //&& _urlFieldDesc == null) 
			{
				PropertyDescriptorCollection propertyDescriptorCollection = TypeDescriptor.GetProperties(dataItem);
				if (DataTextField.Length != 0)
				{
					_textFieldDesc = propertyDescriptorCollection.Find(DataTextField, true);
					if (_textFieldDesc == null)
					{
						if (!DesignMode)
						{
							throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataFieldNotFound"), DataTextField));
						}
					}
				}
			}
			if (this._urlFieldDesc == null)
			{
				int len = DataNavigateUrlFields.Length;
				if (len > 0)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(dataItem);

					string[] urlFields = DataNavigateUrlFields;
					_urlFieldDesc = new PropertyDescriptor[len];

					for (int i = 0; i < urlFields.Length; i++)
					{
						string urlField = urlFields[i];
						if (!string.IsNullOrEmpty(urlField))
						{
							_urlFieldDesc[i] = pdc.Find(urlField, true);
							if (_urlFieldDesc[i] == null && !DesignMode)
							{
								throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataFieldNotFound"), urlField));
							}
						}
					}
				}
			}
			if (_textFieldDesc != null)
			{
				hyperLink.Text = FormatDataTextValue(_textFieldDesc.GetValue(dataItem));
			}
			else
			{
				if (DesignMode && DataTextField.Length != 0)
				{
					hyperLink.Text = C1Localizer.GetString("C1GridView.FieldDesignTimeValue");
				}
			}

			string navigateUrl = string.Empty;
			int urlLen = 0;
			if (_urlFieldDesc != null && ((urlLen = _urlFieldDesc.Length) > 0))
			{
				object[] urlFieldValues = new object[urlLen];

				for (int i = 0; i < urlLen; i++)
				{
					if (_urlFieldDesc[i] != null)
					{
						urlFieldValues[i] = _urlFieldDesc[i].GetValue(dataItem);
					}
				}

				navigateUrl = FormatDataNavigateUrlValue(urlFieldValues);
				if (IsDangeriousUrl(navigateUrl))
				{
					navigateUrl = string.Empty;
				}
			}

			if (navigateUrl.Length == 0 && DataNavigateUrlFields.Length > 0 && DesignMode)
			{
				navigateUrl = "url";
			}

			if (navigateUrl.Length > 0)
			{
				hyperLink.NavigateUrl = navigateUrl;
			}
		}

		private bool IsDangeriousUrl(string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				string val = value.Trim();
				int len = val.Length;

				if ((len > 4 && val.StartsWith("http", StringComparison.InvariantCultureIgnoreCase)) &&
					((val[4] == ':') || (len > 5 && (val[4] == 's' || val[4] == 'S') && val[5] == ':')))
				{
					return false;
				}

				return val.IndexOf(':') >= 0;
			}

			return false;
		}

		#endregion
	}
}
