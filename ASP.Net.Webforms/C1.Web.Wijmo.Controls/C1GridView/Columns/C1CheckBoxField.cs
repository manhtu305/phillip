﻿using System;


namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Web.UI.Design;
	using System.Web.UI;
	using System.Web.UI.WebControls;

	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Represents a Boolean column that is displayed as a check box in a <see cref="C1GridView"/> control.
	/// </summary>
	public class C1CheckBoxField : C1BoundField
	{
		private const string DEF_TEXT = "";

		/// <summary>
		/// Initializes a new instance of the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		public C1CheckBoxField() : this(null)
		{
		}

		internal C1CheckBoxField(IOwnerable owner) : base(owner)
		{
		}

		#region public properties

		/// <summary>
		/// Overrides the <see cref="C1BoundField.ApplyFormatInEditMode"/> property.
		/// This property is not supported by the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		/// <value>
		/// False in all cases.
		/// </value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(false)]
		public override bool ApplyFormatInEditMode
		{
			get { return false; }
			set { }
		}

		/// <summary>
		/// Overrides the <see cref="C1CheckBoxField.ConvertEmptyStringToNull"/> property.
		/// This property is not supported by the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		/// <value>
		/// False in all cases.
		/// </value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(false)]
		public override bool ConvertEmptyStringToNull
		{
			get { return false; }
			set { }
		}

		/// <summary>
		/// Gets or sets the name of the field in the database table to which a grid column is bound.
		/// </summary>
		[TypeConverter(typeof(DataSourceBooleanViewSchemaConverter))]
		public override string DataField
		{
			get { return base.DataField; }
			set { base.DataField = value; }
		}

		/// <summary>
		/// Overrides the <see cref="C1BoundField.DataFormatString"/> property.
		/// This property is not supported by the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		/// <value>
		/// An empty string in all cases.
		/// </value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(false)]
		public override string DataFormatString
		{
			get { return string.Empty; }
			set { }
		}

		/// <summary>
		/// Overrides the <see cref="C1BoundField.HtmlEncode"/> property.
		/// This property is not supported by the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		/// <value>
		/// False in all cases.
		/// </value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(false)]
		public override bool HtmlEncode
		{
			get { return false; }
			set { }
		}

#if SERVERFORMATTING
		/// <summary>
		/// Overrides the <see cref="C1BoundField.HtmlEncodeFormatString"/> property.
		/// This property is not supported by the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		/// <value>
		/// False in all cases.
		/// </value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(false)]
		public override bool HtmlEncodeFormatString
		{
			get { return false; }
			set { }
		}
#endif

		/// <summary>
		/// Overrides the <see cref="C1BoundField.NullDisplayText"/> property.
		/// This property is not supported by the <see cref="C1CheckBoxField"/> class.
		/// </summary>
		/// <value>
		/// An empty string in all cases.
		/// </value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(false)]
		public override string NullDisplayText
		{
			get { return string.Empty; }
			set { }
		}

        /// <summary>
        /// Gets or sets the caption to display next to each check box in a <see cref="C1CheckBoxField"/>.
        /// </summary>
        [DefaultValue(DEF_TEXT)]
        [Json(true, true, DEF_TEXT)]
        public virtual string Text
        {
            get { return GetPropertyValue("Text", DEF_TEXT); }
            set
            {
                if (GetPropertyValue("Text", DEF_TEXT) != value)
                {
                    SetPropertyValue("Text", value);
                    OnFieldChanged();
                }
            }
        }
        
		#endregion

		/// <summary>
		/// Fills the specified <see cref="IOrderedDictionary"/> object with values from the specified <see cref="C1GridViewCell"/> object.
		/// </summary>
		/// <param name="dictionary">A <see cref="IOrderedDictionary"/> used to store the values of the specified cell.</param>/// 
		/// <param name="cell">The <see cref="C1GridViewCell"/> object that contains the values to retrieve.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		/// <param name="includeReadOnly">True to include the values of read-only fields; otherwise, false.</param>
		public override void ExtractValuesFromCell(IOrderedDictionary dictionary, C1GridViewCell cell, C1GridViewRowState rowState, bool includeReadOnly)
		{
			string dataField = this.DataField;
			object value = null;

			if (cell.Controls.Count > 0)
			{
				CheckBox checkBox = cell.Controls[0] as CheckBox;
				if ((checkBox != null) && (includeReadOnly || checkBox.Enabled))
				{
					value = checkBox.Checked;
				}
			}

			if (value != null)
			{
				if (dictionary.Contains(dataField))
				{
					dictionary[dataField] = value;
				}
				else
				{
					dictionary.Add(dataField, value);
				}
			}
		}

		/// <summary>
		/// Creates an empty <see cref="C1CheckBoxField"/> object.
		/// </summary>
		/// <returns>An empty <see cref="C1CheckBoxField"/> object.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1CheckBoxField();
		}

		/// <summary>
		/// Retrieves the value used for a field's value when rendering the <see cref="C1CheckBoxField"/> object in a designer.
		/// </summary>
		/// <param name="rowState">One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.C1GridViewRowState"/> values.</param>
		/// <returns>The value to display in the designer as the field's value.</returns>
		protected override object GetDesignTimeValue(C1GridViewRowState rowState)
		{
			return ((rowState & C1GridViewRowState.Alternate) != 0)
				? false
				: true;
		}

		/// <summary>
		/// Initializes the specified <see cref="C1GridViewCell"/> object to the specified row type and row state.
		/// </summary>
		/// <param name="cell">The <see cref="C1GridViewCell"/> to initialize.</param>
		/// <param name="columnIndex">The zero-based index of the column.</param>
		/// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
		/// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values.</param>
		protected override void InitializeDataCell(C1GridViewCell cell, int columnIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			CheckBox cb = null;
			bool bind = false;

			if (((rowState & C1GridViewRowState.Edit) != C1GridViewRowState.Normal) && !ReadOnly)
			{
				cb = new CheckBox();
				cb.ToolTip = HeaderText;
				bind = !string.IsNullOrEmpty(DataField);
			}
			else
			{
				if (bind = !string.IsNullOrEmpty(DataField))
				{
					cb = new CheckBox();
                    cb.Text = Text;
					if (this.GridView.AllowVirtualScrolling)
						cb.Enabled = false; //430737 it will not auto rebind when callback
                    else
						cb.Enabled = !ReadOnly && this.GridView.AllowClientEditing;
				}
			}
            
			if (cb != null)
			{
				cell.Controls.Add(cb);
				if (bind)
				{
					cb.DataBinding += new EventHandler(OnDataBindField);
				}
			}
		}

    /// <summary>
    /// Binds the value of a field to the <see cref="C1CheckBoxField"/> object.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
    protected override void OnDataBindField(object sender, EventArgs e)
		{
			CheckBox checkBox = sender as CheckBox;

			if (checkBox != null)
			{
				C1GridViewRow row = (C1GridViewRow)((Control)sender).NamingContainer;

				object dataValue = GetValue(row);
				bool boolValue = false;

				if (!C1BaseField.IsNull(dataValue))
				{
					if (dataValue is bool)
					{
						boolValue = (bool)dataValue;
					}
					else
					{
						try
						{
							boolValue = bool.Parse(dataValue.ToString());
						}
						catch (FormatException fe)
						{
							throw new System.Web.HttpException(string.Format(C1Localizer.GetString("C1Grid.EC1CheckBoxField_CouldntParseValue"), DataField), fe);
						}
					}
				}

                checkBox.Checked = boolValue;

                checkBox.Text = Text;
			}
		}
	}
}
