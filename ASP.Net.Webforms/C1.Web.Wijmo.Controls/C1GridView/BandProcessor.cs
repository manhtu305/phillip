﻿using System;
using System.Collections.Generic;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Helper class for supplementary table used to build multicolumn headers..
	/// </summary>
	internal class BandProcessor
	{
		private RowColSpan[,] table = null;
		private int shift;
		private int height;
		private int width;
		private int inc;
		private Dictionary<C1BaseField, int> savedXPos;

		public struct RowColSpan
		{
			public C1BaseField Column;
			public int ColSpan;
			public int RowSpan;

			public RowColSpan(C1BaseField band, int colspan, int rowspan)
			{
				Column = band;
				ColSpan = colspan;
				RowSpan = rowspan;
			}
		}

		/// <summary>
		/// Creates table to generate multicolumn headers.
		/// </summary>
		/// <param name="root">Root node. An instance of <see cref="C1BaseFieldCollection"/> or <see cref="C1Band"/>.</param>
		/// <param name="leaves">Collection of  ending nodes (leaves) of the tree.</param>
		/// <param name="parentVisibility">Determines if parent is visible.</param>
		/// <returns>Constructed table.</returns>
		/// <remarks>
		/// Table height is calculated as maximum size of the route in the tree (only visible items counted). Width
		/// is number of leaves, both visible and invisible. Table is generated thus each leaf has corresponding column.
		/// </remarks>
		public RowColSpan[,] GenerateTable(C1BaseFieldCollection root, List<C1BaseField> leaves, bool parentVisibility)
		{
			height = GetDimension(root, parentVisibility);

			foreach (C1BaseField field in root.Traverse())
			{
				if (field.IsLeaf)
				{
					leaves.Add(field);
				}
			}

			width = leaves.Count;
			table = new RowColSpan[height, width];
			shift = 0;
			inc = 0;
			savedXPos = new Dictionary<C1BaseField, int>();

			SetTableValues(root, 0, 0);
			return table;
		}


		/// <summary>
		/// Determines required sizes of the table which is required to hold multicolumn headers and builds collection
		/// of leaves. Leaf is either <see cref="C1Field"/> or <see cref="C1Band"/> with empty (containing no visible
		/// items) <see cref="C1Band.Columns"/> collection.
		/// </summary>
		/// <param name="root">Root node. An instance of <see cref="C1BaseFieldCollection"/> or <see cref="C1Band"/>.</param>
		/// <param name="parentVisibility">Determines if parent is visible.</param>
		/// <returns>Maximum deep.</returns>
		/// <remarks>
		/// Returning value does not count invisible leaves.
		/// Also sets <see cref="C1BaseField.IsLeaf"/> and <see cref="C1BaseField.ParentVisibility"/> properties of the nodes.
		/// </remarks>
		private int GetDimension(object root, bool parentVisibility)
		{
			int height = 0;

			C1BaseFieldCollection columns = root as C1BaseFieldCollection;
			if (columns != null)
			{
				int cnt = columns.Count;
				for (int i = 0; i < cnt; i++)
				{
					int tmp = GetDimension(columns[i], parentVisibility);
					height = Math.Max(height, tmp);
				}
			}
			else
			{
				C1Band band = root as C1Band;
				if (band != null)
				{
					band.ParentVisibility = parentVisibility;

					int cnt = band.Columns.Count;
					for (int i = 0; i < cnt; i++)
					{
						C1BaseField cur = band.Columns[i];
						int tmp = GetDimension(cur, band.ParentVisibility);
						height = Math.Max(height, tmp);
					}

					if (!band.ParentVisibility)
					{
						return height;
					}

					band.IsLeaf = (height == 0); // height == 0, i.e. sub nodes are invisible

					height++;
				}
				else //C1BaseField
				{
					C1BaseField col = (C1BaseField)root;
					col.IsLeaf = true;
					col.ParentVisibility = parentVisibility;
					if (col.ParentVisibility)
					{
						height = 1;
					}
				}
			}

			return height;
		}

		/*
		/// <summary>
		/// Sets items of the table cells, determining values of RowSpan\ColSpan.
		/// </summary>
		/// <param name="root">Root node. An instance of <see cref="C1FieldCollection"/> or <see cref="C1Band"/>.</param>
		/// <param name="y">Indicates the index of the table row.</param>
		/// <param name="x">Indicates the index of the table column.</param>
		*/
		private void SetTableValues(object root, int y, int x)
		{
			C1BaseFieldCollection cols = root as C1BaseFieldCollection;
			if (cols != null)
			{
				int cnt = cols.Count;
				for (int i = 0; i < cnt; i++)
				{
					SetTableValues(cols[i], y, x);
				}
			}
			else
			{
				C1BaseField bc = (C1BaseField)root;

				int tx = x + shift;
				if (bc.ParentVisibility)
				{
					int posX = tx + inc;
					table[y, posX].Column = bc;
					savedXPos.Add(bc, posX);
				}

				C1Band band = bc as C1Band;
				if (band != null)
				{
					int cnt = band.Columns.Count;
					for (int i = 0; i < cnt; i++)
					{
						SetTableValues(band.Columns[i], y + 1, x);
					}
				}

				if (bc.ParentVisibility)
				{
					if (shift - tx == 0) //bc is column or band without visible nodes
					{
						table[y, savedXPos[bc]].RowSpan = height - y;
						shift++;
					}
					else // band with visible nodes
					{
						table[y, savedXPos[bc]].ColSpan = shift - tx;
					}
				}
				else
					if (!(bc is C1Band) && height > 0)
					{
						C1Band visibleOwner = C1Band.Helper.GetVisibleParent(bc);
						
						bool isParentLeaf = (visibleOwner != null)
							? visibleOwner.IsLeaf 
							: false;

						if (isParentLeaf)
						{
							inc++;
						}

						if (y >= height)
						{
							y = height - 1;
						}

						int posX = x + shift + inc;

						table[y, posX].Column = bc;

						if (!isParentLeaf)
						{
							//parent is not leaf, try to put a value to the under cell, shift all the
							//upper items of a column one position to the right.
							if (visibleOwner != null && (savedXPos[visibleOwner] == posX))
							{
								ShiftTableElements(posX, y);
							}

							inc++;
						}
					}
			}
		}

		private void ShiftTableElements(int x, int untilY)
		{
			for (int i = 0; i < untilY; i++)
			{
				table[i, x + 1] = table[i, x];
				table[i, x].Column = null;
				table[i, x].RowSpan = 0;
				table[i, x].ColSpan = 0;

				if (table[i, x + 1].Column != null)
				{
					savedXPos[table[i, x + 1].Column]++;
				}
			}
		}
	}
}
