﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1GridView
{

	/// <summary>
	/// Represents an individual row in a <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewRow : TableRow, IDataItemContainer
	{
		#region fields

		private int _rowIndex;
		private int _dataItemIndex;
		private C1GridViewRowState _rowState;
		private C1GridViewRowType _rowType;

		private object _dataItem;

		private C1GridView _owner;

		#endregion

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewRow"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the <see cref="C1GridViewRow"/> object in the <see cref="C1GridView.Rows"/> collection.</param>
		/// <param name="dataItemIndex">The index of the DataItem in the underlying DataSet.</param>
		/// <param name="rowType">The row type of the <see cref="C1GridViewRow"/>.</param>
		/// <param name="rowState">The state of the <see cref="C1GridViewRow"/> object.</param>
		public C1GridViewRow(int rowIndex, int dataItemIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
		{
			_rowIndex = rowIndex;
			_dataItemIndex = dataItemIndex;
			_rowType = rowType;
			_rowState = rowState;
		}

		#region public properties

		/// <summary>
		/// Gets or sets the underlying data object to which the <see cref="C1GridViewRow"/> object is bound.
		/// </summary>
		/// <remarks>
		/// Use the DataItem property to access the properties of the underlying data object to which the 
		/// <see cref="C1GridViewRow"/> object is bound. The DataItem property is only available during and after the <see cref="C1GridView.RowDataBound"/> event of a <see cref="GridView"/> control.
		/// </remarks>
		public virtual object DataItem
		{
			get { return _dataItem; }
			protected internal set
			{
				_dataItem = value;
			}
		}

		/// <summary>
		/// Gets the index of the <see cref="DataItem"/> in the underlying DataSet.
		/// </summary>
		public virtual int DataItemIndex
		{
			get { return _dataItemIndex; }
		}

		/// <summary>
		/// Gets the display index of the <see cref="C1GridViewRow"/> object in the <see cref="C1GridView.Rows"/> collection of a <see cref="C1GridView"/> control.
		/// </summary>
		public int DisplayIndex
		{
			get { return _rowIndex; }
		}

		/// <summary>
		/// Gets the index of the <see cref="C1GridViewRow"/> object in the <see cref="C1GridView.Rows"/> collection of a <see cref="C1GridView"/> control.
		/// </summary>
		public virtual int RowIndex
		{
			get { return _rowIndex; }
		}

		/// <summary>
		/// Gets the state of the <see cref="C1GridViewRow"/> object.
		/// </summary>
		public virtual C1GridViewRowState RowState
		{
			get { return _rowState; }
			protected internal set { _rowState = value; }
		}

		/// <summary>
		/// Gets the row type of the <see cref="C1GridViewRow"/> object.
		/// </summary>
		public virtual C1GridViewRowType RowType
		{
			get { return _rowType; }
			protected internal set { _rowType = value; }
		}

		#endregion

		/// <summary>
		/// Gets the owner <see cref="C1GridView"/> object that contains this row.
		/// </summary>
		public C1GridView Owner
		{
			get { return _owner; }
			internal set { _owner = value; }
		}

		private int _displayDataItemIndex = -1;
		internal int DisplayDataItemIndex
		{
			get { return _displayDataItemIndex; }
			set { _displayDataItemIndex = value; }
		}

		private int _bodyRowIndex = -1;
		internal int BodyRowIndex
		{
			get { return _bodyRowIndex; }
			set { _bodyRowIndex = value; }
		}

		#region override

		/// <summary>
		/// Passes the event raised by a control within the container up the page's UI server control hierarchy.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="args">An <see cref="EventArgs"/> that contains event data.</param>
		/// <returns>True to indicate that this method is passing an event raised by a control within the container up the page's UI server control hierarchy; otherwise, false.</returns>
		protected override bool OnBubbleEvent(object source, EventArgs args)
		{
			if (args is CommandEventArgs)
			{
				C1GridViewCommandEventArgs c1args = new C1GridViewCommandEventArgs(this, source, (CommandEventArgs)args);
				base.RaiseBubbleEvent(this, c1args);
				return true;
			}

			return false;
		}

		#endregion

		#region IDataItemContainer Members

		object IDataItemContainer.DataItem
		{
			get { return DataItem; }
		}

		int IDataItemContainer.DataItemIndex
		{
			get { return DataItemIndex; }
		}

		int IDataItemContainer.DisplayIndex
		{
			get { return RowIndex; }
		}

		#endregion
	}

	/// <summary>
	/// Represents a data row in a <see cref="C1GridView"/> control.
	/// </summary>
	public sealed class C1GridViewDataRow : C1GridViewRow
	{
		private bool? _expanded = null;

		private C1GridViewDetailRow _detailRow;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewDataRow"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the <see cref="C1GridViewDataRow"/> object in the <see cref="C1GridView.Rows"/> collection.</param>
		/// <param name="dataItemIndex">The index of the DataItem in the underlying DataSet.</param>
		/// <param name="rowType">The row type of the <see cref="C1GridViewDataRow"/>.</param>
		/// <param name="rowState">The state of the <see cref="C1GridViewDataRow"/> object.</param>
		public C1GridViewDataRow(int rowIndex, int dataItemIndex, C1GridViewRowType rowType, C1GridViewRowState rowState): base(rowIndex, dataItemIndex, rowType, rowState)
		{
		}

		/// <summary>
		/// Gets an <see cref="C1GridViewDetailRow"/> object that represents an associated detail row in a hierarchy grid.
		/// </summary>
		public C1GridViewDetailRow DetailRow
		{
			get { return _detailRow; }
			internal set { _detailRow = value; }
		}

		internal bool HasDetails
		{
			get { return _detailRow != null; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether data row is expanded or not in a hierarchy grid.
		/// </summary>
		public bool Expanded
		{
			get
			{
				if (!_expanded.HasValue)
				{
					C1GridView owner = this.Owner;

					if (owner.HierarchyTree.ContainsKey(this.DisplayDataItemIndex))
					{
						_expanded = owner.HierarchyTree[this.DisplayDataItemIndex].Expanded;
					}
					else
					{
						C1DetailGridView detail = owner.FirstDetailItemInternal;

						bool value = detail != null
							? detail.StartExpanded // default value
							: true;

						owner.HierarchyTree.Add(this.DisplayDataItemIndex, new HierararchyState(value));

						_expanded = value;
					}
				}

				return _expanded.Value;
			}
			set
			{
				if (!_expanded.HasValue || _expanded.Value != value)
				{
					C1GridView owner = this.Owner;

					if (owner.FirstDetailItemInternal != null)
					{
						if (owner.HierarchyTree.ContainsKey(this.DisplayDataItemIndex))
						{
							owner.HierarchyTree[this.DisplayDataItemIndex].Expanded = value;
						}
						else
						{
							owner.HierarchyTree.Add(this.DisplayDataItemIndex, new HierararchyState(value));
						}
					}

					_expanded = value;
				}
			}
		}

		internal void MakeExpandedDirty()
		{
			_expanded = null;
		}
	}

	/// <summary>
	/// Represents a detail row in a <see cref="C1GridView"/> control.
	/// </summary>
	public sealed class C1GridViewDetailRow : C1GridViewRow
	{
		private C1GridViewDataRow _dataRow;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewDetailRow"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the <see cref="C1GridViewDetailRow"/> object in the <see cref="C1GridView.Rows"/> collection.</param>
		public C1GridViewDetailRow(int rowIndex): base(rowIndex, -1, C1GridViewRowType.DetailRow, C1GridViewRowState.Normal)
		{
		}

		/// <summary>
		/// Gets an <see cref="C1GridViewDataRow"/> object that represents an associated data row.
		/// </summary>
		public C1GridViewDataRow DataRow
		{
			get { return _dataRow; }
			internal set { _dataRow = value;}
		}

		/// <summary>
		/// Gets an <see cref="C1DetailGridView"/> object contained inside the row.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public C1DetailGridView DetailGridView
		{
			get
			{
				if (HasControls() && Cells.Count > 0 && Cells[0].Controls.Count > 0)
				{
					return (C1DetailGridView)Cells[0].Controls[0];
				}

				return null;
			}
		}
	}
}
