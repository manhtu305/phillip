﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using C1.Web.Wijmo.Controls.Localization;

	public partial class PagerSettings : Settings 
	{
		/// <summary>
		/// Creates a new instance of the <see cref="PagerSettings"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="PagerSettings"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new PagerSettings();
		}

		internal bool IsPagerOnBottom
		{
			get { return Position == PagerPosition.Bottom || Position == PagerPosition.TopAndBottom; }
		}

		internal bool IsPagerOnTop
		{
			get { return Position == PagerPosition.Top || Position == PagerPosition.TopAndBottom; }
		}
	}
}
