﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using System.Web.UI;
	using System.Web.UI.WebControls;

	/// <summary>
	/// Represents a cell in the rendered table of a <see cref="C1GridView"/>.
	/// </summary>
	[ToolboxItem(false)]
	public class C1GridViewCell : TableCell
	{
		private C1BaseField _containgField;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewCell"/> class.
		/// </summary>
		/// <param name="containingField">The <see cref="C1BaseField"/> that contains the current cell.</param>
		public C1GridViewCell(C1BaseField containingField)
		{
			_containgField = containingField;
		}

		/// <summary>
		/// Gets the <see cref="C1BaseField"/> object that contains the current cell.
		/// </summary>
		public C1BaseField ContainingField
		{
			get { return _containgField; }
		}

		/// <summary>
		/// Adds properties specific to the <see cref="C1GridViewCell"/> control to the list of attributes to render. (Inherited from <see cref="TableCell"/>)
		/// </summary>
		/// <param name="writer">The output stream that renders HTML content to the client. </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			if (DesignMode)
			{
				AddDesignTimeCss(writer);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (DesignMode)
			{
				writer.Write("<div class=\"wijmo-wijgrid-innercell\">");
			}

			base.RenderContents(writer);

			if (DesignMode)
			{
				writer.Write("</div>");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected virtual void AddDesignTimeCss(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijgridtd");
		}
	}
}
