﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using System.Web.UI;
	using System.Web.UI.WebControls;

	/// <summary>
	/// Represents a header cell in the rendered table of a <see cref="C1GridView"/>.
	/// </summary>
	[ToolboxItem(false)]
	public class C1GridViewHeaderCell : C1GridViewCell 
	{
		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewHeaderCell"/> class.
		/// </summary>
		/// <param name="containingField">The <see cref="C1BaseField"/> that contains the current cell.</param>
		public C1GridViewHeaderCell(C1BaseField containingField) : base(containingField)
		{
		}

		/// <summary>
		/// Gets or sets abbreviated text, which is rendered in an HTML abbr attribute and is used by screen readers.
		/// </summary>
		public virtual string AbbreviatedText
		{
			get	{ return ((string)ViewState["AbbreviatedText"] ?? string.Empty); }
			set { ViewState["AbbreviatedText"] = value; }
		}

		/// <summary>
		/// Gets or sets the header cell's scope within an HTML table.
		/// </summary>
		public virtual TableHeaderScope Scope
		{
			get { return (TableHeaderScope)(ViewState["Scope"] ?? TableHeaderScope.NotSet); }
			set { ViewState["Scope"] = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get	{ return HtmlTextWriterTag.Th; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected override void AddDesignTimeCss(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-widget wijmo-c1basefield ui-state-default wijmo-c1field");
		}
	}
}
