﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	public partial class GroupInfo : MixedPersister
	{
		#region IC1Cloneable Members

		/// <summary>
		/// Creates a new instance of the <see cref="GroupInfo"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="GroupInfo"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new GroupInfo();
		}

		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="GroupInfo"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		public new void CopyFrom(Settings copyFrom)
		{
			base.CopyFrom(copyFrom);
		}

		#endregion
	}
}
