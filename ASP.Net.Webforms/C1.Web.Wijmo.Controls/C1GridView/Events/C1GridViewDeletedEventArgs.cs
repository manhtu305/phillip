﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowDeleted"/> event of the
	/// <see cref="C1GridView"/>.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewDeletedEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewDeletedEventHandler(object sender, C1GridViewDeletedEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowDeleted"/> event.
	/// </summary>
	public class C1GridViewDeletedEventArgs : EventArgs
	{
		private int _affectedRows = -1;
		private Exception _exception;
		private bool _exceptionHandled;
		private IOrderedDictionary _keys;
		private IOrderedDictionary _vals;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewDeletedEventArgs"/> class.
		/// </summary>
		/// <param name="affectedRows">Number of rows affected by the delete operation.</param>
		/// <param name="e">Exception that was raised during the delete operation.</param>
		public C1GridViewDeletedEventArgs(int affectedRows, Exception e)
			: base()
		{
			_affectedRows = affectedRows;
			_exception = e;
		}

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewDeletedEventArgs"/> class.
		/// </summary>
		/// <param name="affectedRows">Number of rows affected by the delete operation.</param>
		/// <param name="e">Exception that was raised during the delete operation.</param>
		/// <param name="keys">An ordered dictionary of key field name/value pairs for the deleted record.</param>
		/// <param name="values">A dictionary of the non-key field name/value pairs for the deleted record.</param>
		internal C1GridViewDeletedEventArgs(int affectedRows, Exception e, IOrderedDictionary keys, IOrderedDictionary values)
			: this(affectedRows, e)
		{
		}


		/// <summary>
		/// Gets the number of rows affected by the delete operation.
		/// </summary>
		public int AffectedRows
		{
			get { return _affectedRows; }
		}

		/// <summary>
		/// Gets the exception that was raised during the delete operation.
		/// </summary>
		public Exception Exception
		{
			get { return _exception; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether an exception that was raised during the delete operation was handled
		/// in the event handler.
		/// </summary>
		public bool ExceptionHandled
		{
			get { return _exceptionHandled; }
			set { _exceptionHandled = value; }
		}

		/// <summary>
		/// Gets an ordered dictionary of key field name/value pairs for the deleted record.
		/// </summary>
		public IOrderedDictionary Keys
		{
			get
			{
				if (_keys == null)
				{
					_keys = new OrderedDictionary();
				}

				return _keys;
			}
		}

		/// <summary>
		/// Gets a dictionary of the non-key field name/value pairs for the deleted record.
		/// </summary>
		public IOrderedDictionary Values
		{
			get
			{
				if (_vals == null)
				{
					_vals = new OrderedDictionary();
				}

				return _vals;
			}
		}
	}
}
