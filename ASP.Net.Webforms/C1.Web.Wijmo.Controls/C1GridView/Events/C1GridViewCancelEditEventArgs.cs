﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowCancelingEdit"/> event of a
	/// <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewCancelEditEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewCancelEditEventHandler(object sender, C1GridViewCancelEditEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowCancelingEdit"/> event.
	/// </summary>
	public class C1GridViewCancelEditEventArgs : CancelEventArgs
	{
		private int _rowIndex;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewCancelEditEventArgs"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the row containing the cancel button.</param>
		public C1GridViewCancelEditEventArgs(int rowIndex)
			: base(false)
		{
			_rowIndex = rowIndex;
		}

		/// <summary>
		/// Gets the index of the row containing the Cancel button that raised the event.
		/// </summary>
		public int RowIndex
		{
			get { return _rowIndex; }
			set { _rowIndex = value; }
		}
	}
}
