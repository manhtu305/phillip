﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowUpdating"/> event of the
	/// <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewUpdateEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewUpdateEventHandler(object sender, C1GridViewUpdateEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowUpdating"/> event.
	/// </summary>
	public class C1GridViewUpdateEventArgs : CancelEventArgs
	{
		private IOrderedDictionary _keys;
		private IOrderedDictionary _newVals;
		private IOrderedDictionary _oldVals;
		private int _rowIndex;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewUpdateEventArgs"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the row being updated.</param>
		public C1GridViewUpdateEventArgs(int rowIndex)
			: base(false)
		{
			_rowIndex = rowIndex;
		}

		/// <summary>
		/// Gets a dictionary of field name/value pairs that represent the primary key of the row to update.
		/// </summary>
		public IOrderedDictionary Keys
		{
			get
			{
				if (_keys == null)
				{
					_keys = new OrderedDictionary();
				}

				return _keys;
			}
		}

		/// <summary>
		/// Gets a dictionary containing the revised values of the non-key field name/value pairs in the row to update.
		/// </summary>
		public IOrderedDictionary NewValues
		{
			get
			{
				if (_newVals == null)
				{
					_newVals = new OrderedDictionary();
				}

				return _newVals;
			}
		}

		/// <summary>
		/// Gets a dictionary containing the original field name/value pairs in the row to update.
		/// </summary>
		public IOrderedDictionary OldValues
		{
			get
			{
				if (_oldVals == null)
				{
					_oldVals = new OrderedDictionary();
				}

				return _oldVals;
			}
		}

		/// <summary>
		/// Gets the index of the row being updated.
		/// </summary>
		public int RowIndex
		{
			get { return _rowIndex; }
		}
	}
}