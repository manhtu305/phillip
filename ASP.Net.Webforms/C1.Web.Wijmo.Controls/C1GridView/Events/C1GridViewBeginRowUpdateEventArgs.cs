﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.BeginRowUpdate"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewBeginRowUpdateEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewBeginRowUpdateEventHandler(object sender, C1GridViewBeginRowUpdateEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.BeginRowUpdate"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewBeginRowUpdateEventArgs : EventArgs
	{
	}
}
