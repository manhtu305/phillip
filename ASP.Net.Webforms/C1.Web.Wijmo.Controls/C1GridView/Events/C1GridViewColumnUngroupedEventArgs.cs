﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.ColumnUngrouped"/> 
	/// event of the <see cref="C1GridView"/>.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewColumnUngroupedEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewColumnUngroupedEventHandler(object sender, C1GridViewColumnUngroupedEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.ColumnUngrouped"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewColumnUngroupedEventArgs
	{
		private C1BaseField _column;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewColumnUngroupedEventArgs"/> class. 
		/// </summary>
		/// <param name="column">The column being ungrouped.</param>
		public C1GridViewColumnUngroupedEventArgs(C1BaseField column)
		{
			_column = column;
		}

		/// <summary>
		/// The <see cref="C1BaseField"/> object being ungrouped.
		/// </summary>
		public C1BaseField Column
		{
			get { return _column; }
		}
	}
}
