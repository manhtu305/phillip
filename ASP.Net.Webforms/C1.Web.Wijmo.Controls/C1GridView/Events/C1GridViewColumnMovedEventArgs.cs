﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.ColumnMoved"/> 
	/// event of the <see cref="C1GridView"/>.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewColumnMoveEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewColumnMovedEventHandler(object sender, C1GridViewColumnMovedEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.ColumnMoving"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewColumnMovedEventArgs
	{
		private C1BaseField _drag;
		private C1BaseField _drop;
		private C1GridViewDropPosition _position;
		private int _destIndex;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewColumnMovedEventArgs"/> class. 
		/// </summary>
		/// <param name="drag">Drag source, column being dragged.</param>
		/// <param name="drop">Drop target, column on which drag source is dropped.</param>
		/// <param name="position">Position to drop relative to drop target.</param>
		/// <param name="destIndex">Index of the new position.</param>
		public C1GridViewColumnMovedEventArgs(C1BaseField drag, C1BaseField drop, C1GridViewDropPosition position, int destIndex)
		{
			_drag = drag;
			_drop = drop;
			_position = position;
			_destIndex = destIndex;
		}

		/// <summary>
		/// Drag source, the <see cref="C1BaseField"/> object being dragged.
		/// </summary>
		public C1BaseField Drag
		{
			get { return _drag; }
		}

		/// <summary>
		/// Drop target, the <see cref="C1BaseField"/> object on which drag source is dropped.
		/// </summary>
		public C1BaseField Drop
		{
			get { return _drop; }
		}

		/// <summary>
		/// Position to drop relative to drop target.
		/// </summary>
		public C1GridViewDropPosition Position
		{
			get { return _position; }
		}

		/// <summary>
		/// Index of the new position.
		/// </summary>
		public int DestIndex
		{
			get { return _destIndex; }
		}
	}
}
