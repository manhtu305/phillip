﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowDeleting"/> event of the
	/// <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewDeleteEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewDeleteEventHandler(object sender, C1GridViewDeleteEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowDeleting"/> event.
	/// </summary>
	public class C1GridViewDeleteEventArgs : CancelEventArgs
	{
		private IOrderedDictionary _keys;
		private IOrderedDictionary _vals;
		private int _rowIndex;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewDeleteEventArgs"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the row being deleted.</param>
		public C1GridViewDeleteEventArgs(int rowIndex)
			: base(false)
		{
			_rowIndex = rowIndex;
		}

		/// <summary>
		/// Gets a dictionary of field name/value pairs that represent the primary key of the item to delete.
		/// </summary>
		public IOrderedDictionary Keys
		{
			get
			{
				if (_keys == null)
				{
					_keys = new OrderedDictionary();
				}

				return _keys;
			}
		}

		/// <summary>
		/// Gets the index of the row being deleted.
		/// </summary>
		public int RowIndex
		{
			get { return _rowIndex; }
		}

		/// <summary>
		/// Gets a dictionary of the non-key field name/value pairs in the item to delete.
		/// </summary>
		public IOrderedDictionary Values
		{
			get
			{
				if (_vals == null)
				{
					_vals = new OrderedDictionary();
				}

				return _vals;
			}
		}
	}

}
