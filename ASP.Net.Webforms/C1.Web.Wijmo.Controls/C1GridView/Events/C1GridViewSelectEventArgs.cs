﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.SelectedIndexChanging"/> 
	/// event of the <see cref="C1GridView"/>.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewSelectEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewSelectEventHandler(object sender, C1GridViewSelectEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.SelectedIndexChanging"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewSelectEventArgs : CancelEventArgs
	{
		private int _newIndex;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewSelectEventArgs"/> class.
		/// </summary>
		/// <param name="newSelectedIndex">The index of the new row to select in the <see cref="C1GridView"/> control.</param>
		public C1GridViewSelectEventArgs(int newSelectedIndex)
			: base(false)
		{
			_newIndex = newSelectedIndex;
		}

		/// <summary>
		/// Gets or sets the index of the new row to select in the <see cref="C1GridView"/> control.
		/// </summary>
		public int NewSelectedIndex
		{
			get { return _newIndex; }
			set { _newIndex = value; }
		}
	}
}
