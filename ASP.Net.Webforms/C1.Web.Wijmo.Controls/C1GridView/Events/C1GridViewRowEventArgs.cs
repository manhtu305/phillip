﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowCreated"/> and <see cref="C1GridView.RowDataBound"/> 
	/// events of the <see cref="C1GridView"/>
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewRowEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewRowEventHandler(object sender, C1GridViewRowEventArgs e);


	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowCreated"/> and <see cref="C1GridView.RowDataBound"/> events.
	/// </summary>
	public class C1GridViewRowEventArgs : EventArgs
	{
		private C1GridViewRow _row;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewRowEventArgs"/> class.
		/// </summary>
		/// <param name="row">The row being created or data-bound.</param>
		public C1GridViewRowEventArgs(C1GridViewRow row)
		{
			_row = row;
		}

		/// <summary>
		/// Gets the row being created or data-bound.
		/// </summary>
		public C1GridViewRow Row
		{
			get { return _row; }
		}
	}

}
