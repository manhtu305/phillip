﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.Sorting"/> event of the
	/// <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewSortEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewSortEventHandler(object sender, C1GridViewSortEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.Sorting"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewSortEventArgs : CancelEventArgs
	{
		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewSortEventArgs"/> class. 
		/// </summary>
		/// <param name="sortExpression">The sort expression used to sort the items in the <see cref="C1GridView"/> control.</param>
		/// <param name="sortDirection">A <see cref="C1SortDirection"/>that indicates the direction in which to sort the items in the <see cref="C1GridView"/> control.</param>
		public C1GridViewSortEventArgs(string sortExpression, C1SortDirection sortDirection)
			: base(false)
		{
			SortExpression = sortExpression;
			SortDirection = sortDirection;
		}

		/// <summary>
		/// Gets or sets the direction in which to sort the <see cref="C1GridView"/> control.
		/// </summary>
		public C1SortDirection SortDirection
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the expression used to sort the items in the <see cref="C1GridView"/> control.
		/// </summary>
		public string SortExpression
		{
			get;
			set;
		}
	}
}
