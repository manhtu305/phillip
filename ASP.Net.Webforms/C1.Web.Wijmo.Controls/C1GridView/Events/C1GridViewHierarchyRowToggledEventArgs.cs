﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Collections.Specialized;
	using System.Web.UI.WebControls;

	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.HierarchyRowToggled"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewHierarchyRowToggledEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewHierarchyRowToggledEventHandler(object sender, C1GridViewHierarchyRowToggledEventArgs e);


	/// <summary>
	/// Provides data for the <see cref="C1GridView.HierarchyRowToggled"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewHierarchyRowToggledEventArgs : EventArgs
	{
		private int _rowIndex;
		private bool _expanded;
		private IOrderedDictionary _keys;

		internal C1GridViewHierarchyRowToggledEventArgs(int rowIndex, bool expanded, DataKey masterKey)
			: this(rowIndex, expanded)
		{
			if (masterKey != null)
			{
				IOrderedDictionary keys = Keys;

				foreach (DictionaryEntry entry in masterKey.Values)
				{
					keys.Add(entry.Key, entry.Value);
				}
			}
		}

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1GridViewHierarchyRowToggledEventArgs"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the row being toggled.</param>
		/// <param name="expanding">Indicates whether the row was expanded or collaped.</param>
		public C1GridViewHierarchyRowToggledEventArgs(int rowIndex, bool expanded)
		{
			_rowIndex = rowIndex;
			_expanded = expanded;
		}

		/// <summary>
		/// Indicates whether the row was expanded or collapsed.
		/// </summary>
		public bool Expanded
		{
			get { return _expanded; }
		}

		/// <summary>
		/// Gets a dictionary that represents an associated master data key value.
		/// </summary>
		public IOrderedDictionary Keys
		{
			get
			{
				if (_keys == null)
				{
					_keys = new OrderedDictionary();
				}

				return _keys;
			}
		}

		/// <summary>
		/// Gets the index of the row being toggled.
		/// </summary>		
		public int RowIndex
		{
			get { return _rowIndex; }
		}
	}
}
