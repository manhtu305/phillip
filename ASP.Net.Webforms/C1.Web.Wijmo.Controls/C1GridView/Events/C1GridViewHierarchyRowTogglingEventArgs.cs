﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using System.Collections;
	using System.Collections.Specialized;
	using System.Web.UI.WebControls;

	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.HierarchyRowToggling"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewHierarchyRowTogglingEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewHierarchyRowTogglingEventHandler(object sender, C1GridViewHierarchyRowTogglingEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.HierarchyRowToggling"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewHierarchyRowTogglingEventArgs : CancelEventArgs
	{
		private int _rowIndex;
		private bool _expanding;
		private IOrderedDictionary _keys;

		internal C1GridViewHierarchyRowTogglingEventArgs(int rowIndex, bool expanding, DataKey masterKey)
			: this(rowIndex, expanding)
		{
			if (masterKey != null)
			{
				IOrderedDictionary keys = Keys;

				foreach (DictionaryEntry entry in masterKey.Values)
				{
					keys.Add(entry.Key, entry.Value);
				}
			}
		}

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1GridViewHierarchyRowTogglingEventArgs"/> class.
		/// </summary>
		/// <param name="rowIndex">The index of the row being toggled.</param>
		/// <param name="expanding">Indicates whether the row is expanding or collapsing.</param>
		public C1GridViewHierarchyRowTogglingEventArgs(int rowIndex, bool expanding)
		{
			_rowIndex = rowIndex;
			_expanding = expanding;
		}

		/// <summary>
		/// Indicates whether the row is expanding or collapsing.
		/// </summary>
		public bool Expanding
		{
			get { return _expanding; }
		}

		/// <summary>
		/// Gets a dictionary that represents an associated master data key value.
		/// </summary>
		public IOrderedDictionary Keys
		{
			get
			{
				if (_keys == null)
				{
					_keys = new OrderedDictionary();
				}

				return _keys;
			}
		}

		/// <summary>
		/// The index of the row being toggled.
		/// </summary>		
		public int RowIndex
		{
			get { return _rowIndex; }
		}
	}

}
