﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Collections.Generic;
	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.Filtering"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewFilterEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewFilterEventHandler(object sender, C1GridViewFilterEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.Filtering"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewFilterEventArgs : CancelEventArgs
	{
		private IOrderedDictionary _values;
		private IOrderedDictionary _operators;

		/// <summary>
		/// Gets a dictionary containing field name / filter value pairs in the filter row.
		/// </summary>
		public IOrderedDictionary Values
		{
			get
			{
				if (_values == null)
				{
					_values = new OrderedDictionary();
				}

				return _values;
			}
		}

		/// <summary>
		/// Gets a dictionary containing field name / filter operator pairs.
		/// </summary>
		public IOrderedDictionary Operators
		{
			get
			{
				if (_operators == null)
				{
					_operators = new OrderedDictionary();
				}

				return _operators;
			}
		}

	}
}
