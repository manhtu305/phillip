﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Web.UI.WebControls;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowCommand"/> event of the
	/// <see cref="C1GridView"/>.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewCommandEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewCommandEventHandler(object sender, C1GridViewCommandEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowCommand"/> event.
	/// </summary>
	public class C1GridViewCommandEventArgs : CommandEventArgs
	{
		private C1GridViewRow _row;
		private object _commandSource;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewCommandEventArgs"/> class.
		/// </summary>
		/// <param name="commandSource">The source of the command.</param>
		/// <param name="originalArgs">A <see cref="CommandEventArgs"/> that contains the event data.</param>
		public C1GridViewCommandEventArgs(object commandSource, CommandEventArgs originalArgs)
			: base(originalArgs)
		{
			_commandSource = commandSource;
		}

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewCommandEventArgs"/> class.
		/// </summary>
		/// <param name="row">A <see cref="C1GridViewRow"/> that contains the command source.</param>
		/// <param name="commandSource">The source of the command.</param>
		/// <param name="originalArgs">A <see cref="CommandEventArgs"/> that contains the event data.</param>
		public C1GridViewCommandEventArgs(C1GridViewRow row, object commandSource, CommandEventArgs originalArgs)
			: this(commandSource, originalArgs)
		{
			_row = row;
		}

		/// <summary>
		/// Gets the <see cref="C1GridViewRow"/> that contains the command source.
		/// </summary>
		/*public*/
		internal C1GridViewRow Row
		{
			get { return _row; }
		}

		/// <summary>
		/// Gets the source of the command.
		/// </summary>
		public object CommandSource
		{
			get { return _commandSource; }
		}
	}
}
