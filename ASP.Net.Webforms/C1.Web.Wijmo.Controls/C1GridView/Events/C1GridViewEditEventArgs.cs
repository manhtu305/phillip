﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.RowEditing"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewEditEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewEditEventHandler(object sender, C1GridViewEditEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowEditing"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewEditEventArgs : CancelEventArgs
	{
		private int _newEditIndex;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewEditEventArgs"/> class. 
		/// </summary>
		/// <param name="newEditIndex">The index of the row being edited.</param>
		public C1GridViewEditEventArgs(int newEditIndex)
			: base(false)
		{
			_newEditIndex = newEditIndex;
		}

		/// <summary>
		/// Gets or sets the index of the row being edited.
		/// </summary>
		public int NewEditIndex
		{
			get { return _newEditIndex; }
			set { _newEditIndex = value; }
		}
	}
}
