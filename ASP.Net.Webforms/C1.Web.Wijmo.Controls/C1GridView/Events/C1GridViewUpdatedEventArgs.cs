﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections.Specialized;
	using System.ComponentModel;

	/// <summary>
	/// Represents the method that will handle the <see cref="C1GridView.RowUpdated"/> event of the
	/// <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewUpdatedEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewUpdatedEventHandler(object sender, C1GridViewUpdatedEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.RowUpdated"/> event.
	/// </summary>
	public class C1GridViewUpdatedEventArgs : EventArgs
	{
		private int _affectedRows;
		private Exception _exception;
		private IOrderedDictionary _keys;
		private IOrderedDictionary _newVals;
		private IOrderedDictionary _oldVals;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewUpdatedEventArgs"/> class.
		/// </summary>
		/// <param name="affectedRows">Number of rows affected by the update operation.</param>
		/// <param name="e">The exception (if any) that was raised during the update operation.</param>
		public C1GridViewUpdatedEventArgs(int affectedRows, Exception e)
			: base()
		{
			_affectedRows = affectedRows;
			_exception = e;
		}

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewUpdatedEventArgs"/> class.
		/// </summary>
		/// <param name="affectedRows">Number of rows affected by the update operation.</param>
		/// <param name="e">The exception (if any) that was raised during the update operation.</param>
		/// <param name="keys">A dictionary that contains the key field name/value pairs for the updated record.</param>
		/// <param name="newValues">A dictionary that contains the new field name/value pairs for the updated record.</param>
		/// <param name="oldValues">A dictionary that contains the original field name/value pairs for the updated record.</param>
		internal C1GridViewUpdatedEventArgs(int affectedRows, Exception e, IOrderedDictionary keys, 	IOrderedDictionary newValues, IOrderedDictionary oldValues)
			: this(affectedRows, e)
		{
			_keys = keys;
			_newVals = newValues;
			_oldVals = oldValues;
		}

		/// <summary>
		/// Gets the number of rows affected by the update operation.
		/// </summary>
		public int AffectedRows
		{
			get { return _affectedRows; }
		}

		/// <summary>
		/// Gets the exception (if any) that was raised during the update operation.
		/// </summary>
		public Exception Exception
		{
			get { return _exception; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether an exception that was raised during the update operation was
		/// handled in the event handler.
		/// </summary>
		public bool ExceptionHandled
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets a value that indicates whether the <see cref="C1GridView"/> control should remain in edit mode after an update operation.
		/// </summary>
		public bool KeepInEditMode
		{
			get;
			set;
		}

		/// <summary>
		/// Gets a dictionary that contains the key field name/value pairs for the updated record.
		/// </summary>
		public IOrderedDictionary Keys
		{
			get
			{
				if (_keys == null)
				{
					_keys = new OrderedDictionary();
				}

				return _keys;
			}
		}

		/// <summary>
		/// Gets a dictionary that contains the new field name/value pairs for the updated record.
		/// </summary>
		public IOrderedDictionary NewValues
		{
			get
			{
				if (_newVals == null)
				{
					_newVals = new OrderedDictionary();
				}

				return _newVals;
			}
		}

		/// <summary>
		/// Gets a dictionary that contains the original field name/value pairs for the updated record.
		/// </summary>
		public IOrderedDictionary OldValues
		{
			get
			{
				if (_oldVals == null)
				{
					_oldVals = new OrderedDictionary();
				}

				return _oldVals;
			}
		}
	}
}
