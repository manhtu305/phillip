﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.DetailRequiresDataSource"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewDetailRequiresDataSourceEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewDetailRequiresDataSourceEventHandler(object sender, C1GridViewDetailRequiresDataSourceEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.DetailRequiresDataSource"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewDetailRequiresDataSourceEventArgs : EventArgs
	{
		private C1DetailGridView _detail;

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1GridView"/> class.
		/// </summary>
		/// <param name="detail">A detail grid instance.</param>
		public C1GridViewDetailRequiresDataSourceEventArgs(C1DetailGridView detail)
		{
			_detail = detail;
		}

		/// <summary>
		/// The <see cref="C1DetailGridView"/> object which is represents a detail grid instance.
		/// </summary>
		public C1DetailGridView Detail
		{
			get { return _detail; }
		}
	}
}