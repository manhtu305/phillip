﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	/// <summary>
	/// Represents the method that handles the <see cref="C1GridView.PageIndexChanging"/> event of a <see cref="C1GridView"/> control.
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="C1GridViewPageEventArgs"/> object that contains the event data.</param>
	public delegate void C1GridViewPageEventHandler(object sender, C1GridViewPageEventArgs e);

	/// <summary>
	/// Provides data for the <see cref="C1GridView.PageIndexChanging"/> event of the <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewPageEventArgs : CancelEventArgs
	{
		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewPageEventArgs"/> class. 
		/// </summary>
		/// <param name="newPageIndex">The index of the new page.</param>
		public C1GridViewPageEventArgs(int newPageIndex)
			: base(false)
		{
			NewPageIndex = newPageIndex;
		}

		/// <summary>
		/// Gets or sets the index of the new page.
		/// </summary>
		public int NewPageIndex { get; set; }
	}
}
