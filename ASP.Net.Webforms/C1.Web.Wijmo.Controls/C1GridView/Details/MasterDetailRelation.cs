﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Base.Collections;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// A class that determines a master-detail relation in a hierarchy grid.
	/// </summary>
	public sealed class MasterDetailRelation: Settings, IJsonEmptiable 
	{
		private const string DEF_DETAILDATAKEYNAME = "";
		private const string DEF_MASTERDATAKEYNAME = "";
		private EventHandler _propertyChanged;

		/// <summary>
		/// Gets or sets a value that determines the name of the detail key field.
		/// </summary>
		public string DetailDataKeyName
		{
			get { return GetPropertyValue("DetailDataKeyName", DEF_MASTERDATAKEYNAME); }
			set
			{
				if (GetPropertyValue("DetailDataKeyName", DEF_MASTERDATAKEYNAME) != value)
				{
					SetPropertyValue("DetailDataKeyName", value);
					OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value that determines the name of the master key field.
		/// </summary>
		public string MasterDataKeyName
		{
			get { return GetPropertyValue("MasterDataKeyName", DEF_MASTERDATAKEYNAME); }
			set
			{
				if (GetPropertyValue("MasterDataKeyName", DEF_MASTERDATAKEYNAME) != value)
				{
					SetPropertyValue("MasterDataKeyName", value);
					OnPropertyChanged();
				}
			}
		}

		internal event EventHandler RelationChanged
		{
			add { _propertyChanged = (EventHandler)Delegate.Combine(_propertyChanged, value); }
			remove { _propertyChanged = (EventHandler)Delegate.Remove(_propertyChanged, value); }
		}

		internal void OnPropertyChanged()
		{
			if (_propertyChanged != null)
			{
				_propertyChanged(this, EventArgs.Empty);
			}
		}

		internal bool IsDefined()
		{
			return !string.IsNullOrEmpty(MasterDataKeyName) && !string.IsNullOrEmpty(DetailDataKeyName);
		}

		internal void SetDirty()
		{
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return string.IsNullOrEmpty(MasterDataKeyName) && string.IsNullOrEmpty(DetailDataKeyName); }
		}

		#endregion

		#region IC1Cloneable Members

		/// <summary>
		/// Creates a new instance of the <see cref="MasterDetailRelation"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="MasterDetailRelation"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new MasterDetailRelation();
		}

		#endregion

	}

	/// <summary>
	/// Represents a collection of <see cref="MasterDetailRelation"/> objects.
	/// </summary>
	public class MasterDetailRelationCollection : StateManagedCollection, IOwnerable, IC1Cloneable, IJsonRestore, IEnumerable<MasterDetailRelation>
	{
		#region static

		private static readonly Type[] _knownTypes = new Type[] { typeof(MasterDetailRelation) };

		#endregion

		private IOwnerable _owner;
		private EventHandler _relationsChanged;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="MasterDetailRelationCollection"/> class.
		/// </summary>
		public MasterDetailRelationCollection() : this(null)
		{
		}

		internal MasterDetailRelationCollection(IOwnerable owner) : base()
		{
			_owner = owner;
		}

		/// <summary>
		/// Gets a <see cref="MasterDetailRelation"/> derived column object from the collection at the specified index.
		/// In C#, this property is the indexer for the <see cref="MasterDetailRelationCollection"/> class.
		/// </summary>
		/// <param name="index">
		/// The index of the <see cref="MasterDetailRelation"/> derived object in the collection to retrieve.
		/// </param>
		public MasterDetailRelation this[int index]
		{
			get { return (MasterDetailRelation)((IList)this)[index]; }
		}

		/// <summary>
		/// Adds the specified <see cref="MasterDetailRelation"/> object to the end of the collection.
		/// </summary>
		/// <param name="value">The <see cref="MasterDetailRelation"/> to append.</param>
		public void Add(MasterDetailRelation value)
		{
			((IList)this).Add(value);
		}

		internal void AddRange(IEnumerable<MasterDetailRelation> range)
		{
			if (range != null)
			{
				foreach (MasterDetailRelation relation in range)
				{
					Add(relation);
				}
			}
		}

		/// <summary>
		/// Determines whether the collection contains a specific <see cref="MasterDetailRelation"/> object.
		/// </summary>
		/// <param name="value">The <see cref="MasterDetailRelation"/> to locate.</param>
		/// <returns>true if the collection contains the specified field; otherwise, false.</returns>
		public bool Contains(MasterDetailRelation value)
		{
			return ((IList)this).Contains(value);
		}

		/// <summary>
		/// Determines the index of the specified <see cref="MasterDetailRelation"/> object in the collection.
		/// </summary>
		/// <param name="value">The <see cref="MasterDetailRelation"/> to locate. </param>
		/// <returns>
		/// The index of the value parameter, if it is found in the collection; otherwise, -1.
		/// </returns>
		public int IndexOf(MasterDetailRelation value)
		{
			return ((IList)this).IndexOf(value);
		}

		/// <summary>
		/// Inserts the specified <see cref="MasterDetailRelation"/> object into the collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which the <see cref="MasterDetailRelation"/> is inserted.</param>
		/// <param name="value">The <see cref="MasterDetailRelation"/> to insert.</param>
		public void Insert(int index, MasterDetailRelation value)
		{
			((IList)this).Insert(index, value);
		}

		/// <summary>
		/// Removes the specified <see cref="MasterDetailRelation"/> object from the collection.
		/// </summary>
		/// <param name="value">The <see cref="MasterDetailRelation"/> to remove.</param>
		public void Remove(MasterDetailRelation value)
		{
			((IList)this).Remove(value);
		}

		/// <summary>
		/// Removes the <see cref="MasterDetailRelation"/> object at the specified index from the collection.
		/// </summary>
		/// <param name="index">The index of the <see cref="MasterDetailRelation"/> to remove.</param>
		public void RemoveAt(int index)
		{
			((IList)this).RemoveAt(index);
		}


		private void OnRelationsChanged(object sender, EventArgs e)
		{
			OnRelationsChanged();
		}

		private void OnRelationsChanged()
		{
			if (_relationsChanged != null)
			{
				_relationsChanged(this, EventArgs.Empty);
			}
		}

		internal event EventHandler RelationsChanged
		{
			add { _relationsChanged = (EventHandler)Delegate.Combine(_relationsChanged, value); }
			remove { _relationsChanged = (EventHandler)Delegate.Remove(_relationsChanged, value); }
		}

		#region IOwnerable Members

		internal IOwnerable Owner
		{
			get { return _owner; }
			set { _owner = value; }
		}

		IOwnerable IOwnerable.Owner
		{
			get { return Owner; }
			set { Owner = (IOwnerable)value; }
		}

		#endregion

		#region override

		/// <summary>
		/// Creates a default instance of the <see cref="MasterDetailRelation"/> type object that is identified by the provided index.
		/// </summary>
		/// <param name="index">The index of the <see cref="MasterDetailRelation"/> type to create from the ordered list of types that are returned by the <see cref="GetKnownTypes"/> method.</param>
		/// <returns>
		/// An object that represents an instance of a class that is derived from the <see cref="MasterDetailRelation"/> class, according to the index provided.
		/// </returns>
		protected override object CreateKnownType(int index)
		{
			return new MasterDetailRelation();
		}

		/// <summary>
		/// Gets an array of <see cref="MasterDetailRelation"/> types that the collection can contain.
		/// </summary>
		/// <returns>
		/// An array of type objects that identify the types of <see cref="MasterDetailRelation"/> objects that the collection can contain.
		/// </returns>
		protected override Type[] GetKnownTypes()
		{
			return _knownTypes;
		}

		/// <summary>
		/// Instructs the specified object to record its complete state to view state, instead of recording only changed information.
		/// </summary>
		/// <param name="o">The object to mark as changed since the last time that view state was loaded or saved.</param>
		protected override void SetDirtyObject(object o)
		{
			((MasterDetailRelation)o).SetDirty();
		}

		/// <summary>
		/// Performs additional processing after all items are removed from the collection.
		/// </summary>
		protected override void OnClearComplete()
		{
			OnRelationsChanged();
		}

		/// <summary>
		/// Performs additional processing after an item is added to the collection.
		/// </summary>
		/// <param name="index">The index in the collection that the <see cref="MasterDetailRelation"/> object was inserted at.</param>
		/// <param name="value">The object that was inserted into the collection.</param>
		protected override void OnInsertComplete(int index, object value)
		{
			base.OnInsertComplete(index, value);

			MasterDetailRelation relation = (MasterDetailRelation)value;
			relation.RelationChanged += new EventHandler(OnRelationsChanged);

			OnRelationsChanged();
		}

		/// <summary>
		/// Performs additional processing after an item is removed from the collection.
		/// </summary>
		/// <param name="index">The index in the collection that the <see cref="MasterDetailRelation"/> object was removed from.</param>
		/// <param name="value">The object that was removed from the collection.</param>
		protected override void OnRemoveComplete(int index, object value)
		{
			base.OnRemoveComplete(index, value);

			MasterDetailRelation relation = (MasterDetailRelation)value;

			relation.RelationChanged -= new EventHandler(OnRelationsChanged);
			
			OnRelationsChanged();
		}

		/// <summary>
		/// Validates an element of the collection.
		/// </summary>
		/// <param name="value">An element to validate.</param>
		protected override void OnValidate(object value)
		{
			base.OnValidate(value);

			if (value == null || !(value is MasterDetailRelation))
			{
				throw new ArgumentException("value");
			}
		}

		#endregion

		#region IJsonRestore Members

		void IJsonRestore.RestoreStateFromJson(object state)
		{
			Clear();

			ArrayList list = state as ArrayList;
			if (list != null)
			{
				foreach (Hashtable obj in list)
				{
					MasterDetailRelation item = new MasterDetailRelation();
					((IJsonRestore)item).RestoreStateFromJson(obj);
					Add(item);
				}
			}
		}

		#endregion

		#region IEnumerable<MasterDetailRelation> Members

		IEnumerator<MasterDetailRelation> IEnumerable<MasterDetailRelation>.GetEnumerator()
		{
			IEnumerator ien = GetEnumerator();

			while (ien.MoveNext()) 
			{
				yield return (MasterDetailRelation)ien.Current;
			}
		}

		#endregion

		#region IC1Cloneable Members

		IC1Cloneable IC1Cloneable.CreateInstance()
		{
			return new MasterDetailRelationCollection();
		}

		internal void CopyFrom(MasterDetailRelationCollection copyFrom)
		{
			Clear();

			if (copyFrom != null)
			{
				foreach (MasterDetailRelation field in copyFrom)
				{
					Add((MasterDetailRelation)field.Clone());
				}
			}
		}

		void IC1Cloneable.CopyFrom(object copyFrom)
		{
			CopyFrom(copyFrom as MasterDetailRelationCollection);
		}

		#endregion

		#region ICloneable Members

		object ICloneable.Clone()
		{
			MasterDetailRelationCollection result = (MasterDetailRelationCollection)((IC1Cloneable)this).CreateInstance();
			result.CopyFrom(this);
			return result;
		}

		#endregion
	}
}