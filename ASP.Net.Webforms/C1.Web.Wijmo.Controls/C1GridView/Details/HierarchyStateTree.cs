﻿using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	internal class HierararchyState
	{
		private HierararchyStateTree _child = new HierararchyStateTree();

		public HierararchyState(bool expanded)
		{
			Expanded = expanded;
		}

		public HierararchyStateTree Child
		{
			get { return _child; }
		}

		public bool Expanded { get; set; }

		public C1DetailGridView Properties { get; set; }

		internal string Serialize()
		{
			string result = Expanded ? "1" : "0";

			if (Expanded && _child != null && _child.Count > 0) // skip children if node is collapsed
			{
				result += _child.Serialize();
			}

			return result;
		}

#if DEBUG
		public override string ToString()
		{
			return string.Format("Expanded: {0}, HasChildren: {1}", Expanded.ToString(), _child.Count > 0);
		}
#endif
	}

	internal class HierararchyStateTree : SortedDictionary<int, HierararchyState>
	{
		public HierararchyStateTree()
			: base()
		{
		}

		public void PersistProperties(List<C1DetailGridView> details)
		{

			for (int i = 0; i < details.Count; i++)
			{
				C1DetailGridView detail = details[i];

				if (this.ContainsKey(i))
				{
					HierararchyState item = this[i]; // TODO: use masterKey //DataKey masterKey = detail.MasterDataKey;

					item.Properties = detail.Clone(true, false); // UI properties only, no event handlers

					item.Child.PersistProperties(detail.Details);
				}
			}
		}

		internal string Serialize()
		{
			StringBuilder sb = new StringBuilder();

			if (Count > 0)
			{
				sb.AppendFormat("[{0}]", Count);

				foreach (KeyValuePair<int, HierararchyState> state in this)
				{
					sb.Append(state.Value.Serialize());
				}
			}

			return sb.ToString();
		}

		internal void Deserialize(string value)
		{
			this.Clear();
			_Deserialize(value, 0, this, -1);
		}

		private int _Deserialize(string value, int startFrom, HierararchyStateTree tree, int itemsToRead)
		{
			int i = startFrom;
			HierararchyState item = null;

			for (; i < value.Length; i++)
			{
				if (value[i] == '[')
				{
					int idx = value.IndexOf(']', i + 1);
					int len = int.Parse(value.Substring(i + 1, idx - (i + 1)));

					i = _Deserialize(value, idx + 1, item == null ? tree : item.Child, len);
				}
				else
				{
					tree.Add(tree.Count, item = new HierararchyState(value[i] == '1'));

					if (tree.Count == itemsToRead)
					{
						if ((i < value.Length - 1) && (value[i + 1] == '['))
						{
							continue;
						}
						else
						{
							break;
						}
					}
				}
			}

			return i;
		}
	}
}