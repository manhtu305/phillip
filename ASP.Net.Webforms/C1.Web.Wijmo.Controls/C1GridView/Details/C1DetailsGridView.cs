﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Represents the <b>C1DetailGridView</b> control.
	/// </summary>
	[ToolboxItem(false)]
	public class C1DetailGridView : C1GridView, IStateManager
	{
		#region static

		private static readonly Dictionary<string, object> EVENTS_KEYS = BuildEventsKeysList(); // Gather keys by inheritance chain

		private static Dictionary<string, object> BuildEventsKeysList()
		{
			Dictionary<string, object> result = new Dictionary<string, object>(40);

			Type srcType = typeof(C1DetailGridView);

			do
			{
				// Collect event keys using reflection, couldn't find a better solution.
				foreach (EventInfo eventInfo in srcType.GetEvents(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
				{
					FieldInfo eventKeyInfo = srcType.GetField("Event" + eventInfo.Name, BindingFlags.NonPublic | BindingFlags.Static);
					if (eventKeyInfo != null)
					{
						result.Add(eventInfo.Name, eventKeyInfo.GetValue(null));
					}
				}

			} while ((srcType != typeof(Control)) && ((srcType = srcType.BaseType) != null));

			return result;
		}

		#endregion

		private const bool DEF_STARTEXPANDED = false;
		private MasterDetailRelationCollection _relation;
		private C1GridViewDetailRow _detailRow;
		private bool _persistDetailProperty;
		private C1GridView _root = null;

		#region ctor

		public C1DetailGridView()
			: this(null, null, null, null, -1)
		{
			_persistDetailProperty = true;
		}

		// root is the topmost grid in a hierarchy.
		internal C1DetailGridView(C1GridView root, C1GridViewDetailRow detailRow, C1DetailGridView template, HierararchyState state, int path)
			: base()
		{
			_root = root;

			_persistDetailProperty = false; // don't persist because it will be populated from the Master.Detail

			_detailRow = detailRow;

			Path = path;

			if (template != null)
			{

				CopyFrom(template);
				//this.RequiresDataBinding = true;
			}

			if (state != null)
			{
				HierarchyTree = state.Child;
				if (state.Properties != null)
				{
					this.CopyFrom(state.Properties, true, false); // copy UI properties only
				}
			}
			
			if (detailRow != null)
			{
				Master = detailRow.Owner; // set Master AFTER the properties were copied. Otherwise we are fall into an infinite loop because of DetailsChanged notification + Master.OnRequiresDataBinding() method call.

				ID += detailRow.DataRow.DisplayDataItemIndex.ToString();
			}
		}

		#endregion

		#region public properties

		/// <summary>
		/// Gets the <see cref="CallbackSettings"/> object that enables you to determine actions that can be performed by the <see cref="C1DetailGridView"/> using callbacks mechanism.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Json(false)]
		public override CallbackSettings CallbackSettings
		{
			get
			{
				return (_root != null)
					? (CallbackSettings)_root.CallbackSettings.Clone()
					: new CallbackSettings();
			}
		}

		/// <summary>
		/// Gets a collection of <see cref="MasterDetailRelation"/> objects that represent master-detail relations in a hierarchical grid.
		/// </summary>
		/// <remarks>
		/// Collection elements must comply with following:
		/// Field which is used as property value <see cref="MasterDetailRelation.MasterDataKeyName"/>, should also be specified in <see cref="C1.Web.Wijmo.Controls.C1GridView.C1GridView.DataKeyNames"/> property of the master grid.
		/// Field which is used as property value <see cref="MasterDetailRelation.DetailDataKeyName"/>, should also be specified in <see cref="C1.Web.Wijmo.Controls.C1GridView.C1GridView.DataKeyNames"/> property.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public MasterDetailRelationCollection Relation
		{
			get
			{
				if (_relation == null)
				{
					_relation = new MasterDetailRelationCollection(this);
				}

				if (IsTrackingViewState)
				{
					//((IStateManager)_relation).TrackViewState();
				}

				return _relation;
			}
		}

		//public C1GridViewDetailRow DetailRow
		//{
		//	get { return _detailRow; }
		//}

		/// <summary>
		/// Gets an <see cref="DataKey"/> object that represents an associated master data key value.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public DataKey MasterDataKey
		{
			get
			{
				if (_detailRow != null && _detailRow.DataRow != null)
				{
					return this.Master.DataKeys[this._detailRow.DataRow.DisplayDataItemIndex]; 
				}

				return null;
			}
		}

		/// <summary>
		/// Gets or sets a value that determines whether the hierarchy will be expanded by default or not.
		/// </summary>
		/// <value>
		/// The default value is True.
		/// </value>
		[DefaultValue(DEF_STARTEXPANDED)]
		[NotifyParentProperty(true)]
		public bool StartExpanded
		{
			get { return GetPropertyValue("StartExpanded", DEF_STARTEXPANDED); }
			set
			{
				if (GetPropertyValue("StartExpanded", DEF_STARTEXPANDED) != value)
				{
					SetPropertyValue("StartExpanded", value);

					C1GridView master = Master;
					if (master != null)
					{
						if (master.IsChildControlsCreated)
						{
							foreach (C1GridViewRow row in master.Rows)
							{
								C1GridViewDataRow dataRow = row as C1GridViewDataRow;
								if (dataRow != null)
								{
									dataRow.Expanded = value;
								}
							}
						}

						master.OnRequiresDataBinding();
					}
					else
					{
						OnRequiresDataBinding();
					}
				}
			}
		}

		#endregion

		#region overrides

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)] 
		public override bool EnableViewState
		{
			get	{ return base.EnableViewState; }
			set	{ base.EnableViewState = value;	}
		}

		internal override bool SerializeStateProperties
		{
			get
			{
				return EnableViewState
					? base.SerializeStateProperties 
					: true; // default
			}
		}

		internal override bool PersistDetailProperty
		{
			get	{ return _persistDetailProperty; }
		}

		internal override C1GridView Root
		{
			get { return _root; }
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void EnsureDataBound()
		{
			if (RequiresDataBinding && !this.IsBoundUsingDataSourceID && !IsChildControlsCreated && HasDetails() && (Master != null) && (Page != null) && Page.IsCallback)
			{
				// relates with the RaiseCallbackEvent() method
				// force automatic databinding on callback in case of run-time databinding to create detail grids
				DataBind(); 
			}
			else
			{
				base.EnsureDataBound();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public override void DataBind()
		{
			if (IsBoundUsingDataSourceID)
			{
				RestrictDataSourceControl();
			}

			base.DataBind();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void OnDataSourceViewChanged(object sender, EventArgs e)
		{
			if (Master != null)
			{
				return;
			}

			base.OnDataSourceViewChanged(sender, e);
		}

		#endregion

		#region private members

		private Control FindDataSourceControl(string controlID)
		{
			Control result = Page.FindControl(controlID);

			if (result == null) // MasterPage? Find closest ContentPlaceHolder.
			{
				Control placeHolder = this.NamingContainer;

				while (placeHolder != null && placeHolder != this.Page && !(placeHolder is ContentPlaceHolder))
				{
					placeHolder = placeHolder.NamingContainer;
				}

				if (placeHolder != null)
				{
					result = placeHolder.FindControl(controlID);
				}
			}

			if (result == null)
			{
				throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataSourceControlNotFound"), controlID));
			}

			return result;
		}

		private void RestrictDataSourceControl()
		{
			if (_detailRow == null && _detailRow.DataRow == null)
			{
				return;
			}

			C1GridViewDataRow dataRow = _detailRow.DataRow;

			if (dataRow != null)
			{
				if (dataRow.DisplayDataItemIndex < 0 || dataRow.DisplayDataItemIndex >= Master.DataKeys.Count)
				{
					return;
				}

				DataKey masterKey = Master.DataKeys[dataRow.DisplayDataItemIndex];

				foreach (MasterDetailRelation relation in Relation)
				{
					if (relation.IsDefined())
					{
						object masterKeyValue = masterKey.Values[relation.MasterDataKeyName];

						if (masterKeyValue != null)
						{
							SqlDataSource dataSourceControl = this.FindDataSourceControl(this.DataSourceID) as SqlDataSource;

							if (dataSourceControl != null)
							{
								ParameterCollection parameters = GetDataSourceControlSelectParameters(dataSourceControl);

								if (parameters != null)
								{
									foreach (Parameter param in parameters)
									{
										if (string.Equals(param.Name, relation.DetailDataKeyName, StringComparison.OrdinalIgnoreCase))
										{
											param.DefaultValue = masterKeyValue.ToString();
										}
									}
								}
							}
						}
					}
				}
			}
		}

		private ParameterCollection GetDataSourceControlSelectParameters(Control ctrl)
		{
			Type ctrlType = ctrl.GetType();

			PropertyInfo propInfo = ctrlType.GetProperty("SelectParameters", typeof(ParameterCollection))
				?? ctrlType.GetProperty("WhereParameters", typeof(ParameterCollection));

			return (propInfo != null)
				? (ParameterCollection)propInfo.GetValue(ctrl, null)
				: null;
		}

		#endregion


		#region IC1Cloneable Members

		/// <summary>
		/// Creates a new instance of the <see cref="C1DetailGridView"/> class.
		/// </summary>
		/// <returns>
		/// A new instance of the <see cref="C1DetailGridView"/> class.
		/// </returns>
		internal override C1GridView CreateInstance()
		{
			return new C1DetailGridView();
		}

		internal void CopyFrom(C1GridView source, bool uiPropertiesOnly, bool includingEventHandlers)
		{
			base.CopyFrom(source, uiPropertiesOnly);

			if (uiPropertiesOnly)
			{
				return;
			}

			if (source != null)
			{
				if (source.DetailInternal != null)
				{
					Detail.CopyFrom(source.DetailInternal);
				}

				C1DetailGridView detailSource = (C1DetailGridView)source;

				Relation.CopyFrom(detailSource.Relation);

				if (includingEventHandlers)
				{
					CloneEvents(detailSource, this, detailSource.Events, this.Events);
				}
			}
		}

		/// <summary>
		/// Copies the properties of the specified <see cref="C1DetailGridView"/> into the instance of the <see cref="C1DetailGridView"/> class that this method is called from.
		/// </summary>
		/// <param name="source">A <see cref="C1DetailGridView"/> that represents the properties to copy.</param>
		internal override void CopyFrom(C1GridView source, bool uiPropertiesOnly)
		{
			this.CopyFrom(source, uiPropertiesOnly, true);
		}

		internal C1DetailGridView Clone(bool uiPropertiesOnly, bool includingEventHandlers)
		{
			C1DetailGridView instance = (C1DetailGridView)this.CreateInstance();
			instance.CopyFrom(this, uiPropertiesOnly, includingEventHandlers);
			return instance;
		}

		internal override C1GridView Clone(bool uiPropertiesOnly)
		{
			return this.Clone(uiPropertiesOnly, true);
		}

		// clone event handlers
		private void CloneEvents(Control src, Control target, EventHandlerList srcEvents, EventHandlerList targetEvents)
		{
	
			foreach(KeyValuePair<string, object> value in EVENTS_KEYS)
			{
				object eventKey = value.Value;

				if (srcEvents[eventKey] != null)
				{
					Delegate[] delegates = srcEvents[eventKey].GetInvocationList();
					foreach (Delegate dlgt in delegates)
					{
						targetEvents.AddHandler(eventKey, dlgt);
					}
				}
			}
		}

		#endregion

		#region IStateManager Members

		bool IStateManager.IsTrackingViewState
		{

			get { return IsTrackingViewState; }
		}

		void IStateManager.LoadViewState(object state)
		{
			LoadViewState(state);
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}

		#endregion
	}
}