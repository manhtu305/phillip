﻿using System;
using System.Collections.Generic;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Web.UI;

	public sealed class C1DetailGridViewCollection : StateManagedCollection, IOwnerable, IC1Cloneable, IJsonRestore
	{
		private static readonly Type[] _knownTypes = new Type[] { typeof(C1DetailGridView) };

		private C1GridView _owner = null;
		private EventHandler _detailsChanged;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1DetailGridViewCollection"/> class.
		/// </summary>
		public C1DetailGridViewCollection()	: this(null)
		{
		}

		internal C1DetailGridViewCollection(C1GridView owner)
			: base()
		{
			_owner = owner;
		}

		/// <summary>
		/// Gets a <see cref="C1DetailGridView"/> object from the <see cref="C1DetailGridViewCollection"/>
		/// collection at the specified index.
		/// In C#, this property is the indexer for the <see cref="C1DetailGridViewCollection"/> class.
		/// </summary>
		/// <param name="index">
		/// The index of the <see cref="C1DetailGridView"/> derived object in the <see cref="C1DetailGridViewCollection"/> collection to retrieve.
		/// </param>
		public C1DetailGridView this[int index]
		{
			get { return (C1DetailGridView)((IList)this)[index]; }
		}

		/// <summary>
		/// Adds the specified <see cref="C1DetailGridView"/> object to the end of the <see cref="C1DetailGridViewCollection"/>.
		/// </summary>
		/// <param name="value">The <see cref="C1DetailGridView"/> to append.</param>
		public void Add(C1DetailGridView value)
		{
			((IList)this).Add(value);
		}

		/// <summary>
		/// Determines whether the <see cref="C1DetailGridViewCollection"/> collection contains a specific <see cref="C1DetailGridView"/> object.
		/// </summary>
		/// <param name="value">The <see cref="C1DetailGridView"/> to locate.</param>
		/// <returns>true if the <see cref="C1DetailGridViewCollection"/> contains the specified value; otherwise, false.</returns>
		public bool Contains(C1DetailGridView value)
		{
			return ((IList)this).Contains(value);
		}

		/// <summary>
		/// Determines the index of the specified <see cref="C1DetailGridView"/> object in the collection.
		/// </summary>
		/// <param name="value">The <see cref="C1DetailGridView"/> to locate. </param>
		/// <returns>
		/// The index of the value parameter, if it is found in the collection; otherwise, -1.
		/// </returns>
		public int IndexOf(C1DetailGridView value)
		{
			return ((IList)this).IndexOf(value);
		}

		/// <summary>
		/// Removes the specified <see cref="C1DetailGridView"/> object from the <see cref="C1DetailGridViewCollection"/> collection.
		/// </summary>
		/// <param name="value">The <see cref="C1DetailGridView"/> to remove.</param>
		public void Remove(C1DetailGridView value)
		{
			((IList)this).Remove(value);
		}

		private void OnDetailChanged(object sender, EventArgs e)
		{
			OnDetailsChanged();
		}

		private void OnDetailsChanged()
		{
			if (_detailsChanged != null)
			{
				_detailsChanged(this, EventArgs.Empty);
			}
		}

		internal event EventHandler DetailsChanged
		{
			add { _detailsChanged = (EventHandler)Delegate.Combine(_detailsChanged, value); }
			remove { _detailsChanged = (EventHandler)Delegate.Remove(_detailsChanged, value); }
		}

		#region override

		/// <summary>
		/// Creates a default instance of the <see cref="C1DetailGridView"/> type object that is identified by the provided index.
		/// </summary>
		/// <param name="index">The index of the <see cref="C1DetailGridView"/> type to create from the ordered list of types that are returned by the <see cref="GetKnownTypes"/> method.</param>
		/// <returns>
		/// An object that represents an instance of a class that is derived from the <see cref="C1DetailGridView"/> class, according to the index provided.
		/// </returns>
		protected override object CreateKnownType(int index)
		{
			return new C1DetailGridView();
		}

		/// <summary>
		/// Gets an array of <see cref="C1DetailGridView"/> types that the <see cref="C1DetailGridViewCollection"/> collection can contain.
		/// </summary>
		/// <returns>
		/// An array of type objects that identify the types of <see cref="C1DetailGridView"/> objects that the collection can contain.
		/// </returns>
		protected override Type[] GetKnownTypes()
		{
			return _knownTypes;
		}

		/// <summary>
		/// Instructs the specified object to record its complete state to view state, instead of recording only changed information.
		/// </summary>
		/// <param name="o">The object to mark as changed since the last time that view state was loaded or saved.</param>
		protected override void SetDirtyObject(object o)
		{
			//TODO
		}

		/// <summary>
		/// Performs additional processing after all items are removed from the collection.
		/// </summary>
		protected override void OnClearComplete()
		{
			OnDetailsChanged();
		}

		/// <summary>
		/// Performs additional processing after an item is added to the collection.
		/// </summary>
		/// <param name="index">The index in the collection that the <see cref="C1DetailGridView"/> object was inserted at.</param>
		/// <param name="value">The object that was inserted into the <see cref="C1DetailGridViewCollection"/> collection.</param>
		protected override void OnInsertComplete(int index, object value)
		{
			base.OnInsertComplete(index, value);

			C1DetailGridView detail = (C1DetailGridView)value;

			detail.Master = this._owner;

			//field.FieldChanged += new EventHandler(OnFieldChanged);

			OnDetailsChanged();
		}

		/// <summary>
		/// Performs additional processing after an item is removed from the collection.
		/// </summary>
		/// <param name="index">The index in the collection that the <see cref="C1DetailGridView"/> object was removed from.</param>
		/// <param name="value">The object that was removed from the <see cref="C1DetailGridViewCollection"/> collection.</param>
		protected override void OnRemoveComplete(int index, object value)
		{
			base.OnRemoveComplete(index, value);

			C1DetailGridView detail = (C1DetailGridView)value;

			//field.FieldChanged -= new EventHandler(OnDetailChanged);

			detail.Master = null;

			OnDetailsChanged();
		}

		/// <summary>
		/// Validates an element of the <see cref="C1DetailGridViewCollection"/> collection.
		/// </summary>
		/// <param name="value">An element to validate.</param>
		protected override void OnValidate(object value)
		{
			base.OnValidate(value);

			if (value == null || !(value is C1DetailGridView))
			{
				throw new ArgumentException("value");
			}
		}

		#endregion


		#region IOwnerable Members

		internal C1GridView Owner
		{
			get { return _owner; }
			set { _owner = value; }
		}

		IOwnerable IOwnerable.Owner
		{
			get { return Owner; }
			set { Owner = (C1GridView)value; }
		}

		#endregion

		#region IJsonRestore Members

		void IJsonRestore.RestoreStateFromJson(object state)
		{
			Clear();

			ArrayList list = state as ArrayList;
			if (list != null)
			{
				foreach (Hashtable detailState in list)
				{
					C1DetailGridView detail = null;

					detail = (C1DetailGridView)CreateKnownType(0);

					((IJsonRestore)detail).RestoreStateFromJson(detailState);

					Add(detail);
				}
			}
		}

		#endregion

		#region IC1Cloneable Members

		IC1Cloneable IC1Cloneable.CreateInstance()
		{
			return new C1DetailGridViewCollection(null);
		}

		internal void CopyFrom(C1DetailGridViewCollection copyFrom)
		{
			Clear();

			if (copyFrom != null)
			{
				foreach (C1DetailGridView detail in copyFrom)
				{
					Add((C1DetailGridView)detail.Clone());
				}
			}
		}

		void IC1Cloneable.CopyFrom(object copyFrom)
		{
			CopyFrom(copyFrom as C1DetailGridViewCollection);
		}

		#endregion

		#region ICloneable Members

		object ICloneable.Clone()
		{
			C1DetailGridViewCollection result = (C1DetailGridViewCollection)((IC1Cloneable)this).CreateInstance();
			result.CopyFrom(this);
			return result;
		}

		#endregion
	}
}
