﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView.Controls
{
	using System.Web.UI;
	using System.Web.UI.WebControls;

	internal class C1ImageButton : ImageButton 
	{
		private ICallbackContainer _cbContainer;
		private IPostBackContainer _pbContainer;

		public C1ImageButton(ICallbackContainer cbContainer, IPostBackContainer pbContainer) : base()
		{
			_cbContainer = cbContainer;
			_pbContainer = pbContainer;
		}

		protected override PostBackOptions GetPostBackOptions()
		{
			return (_pbContainer != null)
				? _pbContainer.GetPostBackOptions(this)
				: base.GetPostBackOptions();
		}

		public override bool CausesValidation
		{
			get { return false; }
			set { }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (_cbContainer != null)
			{
				string argument = string.Empty;

				PostBackOptions pbo = GetPostBackOptions();
				if (pbo != null)
				{
					//argument = string.Format("\"{0}\"", pbo.Argument);
					argument = pbo.Argument;
				}

				OnClientClick = _cbContainer.GetCallbackScript(this, argument);
			}

			base.Render(writer);
		}
	}
}
