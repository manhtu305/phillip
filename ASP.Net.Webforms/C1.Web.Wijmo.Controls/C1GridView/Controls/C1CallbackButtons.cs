﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView.Controls
{
	using System.Web.UI;
	using System.Web.UI.WebControls;

	internal class C1CallbackButtonControlBase : WebControl, IButtonControl
	{
		internal static string EnsureEndWithSemiColon(string value)
		{
			if (value != null)
			{
				int length = value.Length;
				if (length > 0 && value[length - 1] != ';')
				{
					return value + ";";
				}
			}
			return value;
		}

		private ICallbackContainer _container;

		protected C1CallbackButtonControlBase(ICallbackContainer container, HtmlTextWriterTag tag)
			: base(tag)
		{
			_container = container;
		}

		protected C1CallbackButtonControlBase(ICallbackContainer container, string tag)
			: base(tag)
		{
			_container = container;
		}

		public string OnClientClick
		{
			get { return (string)ViewState["OnClientClick"] ?? string.Empty; }
			set { ViewState["OnClientClick"] = value; }
		}

		protected ICallbackContainer Container
		{
			get { return _container; }
		}

		#region IButtonControl Members

		public string CommandArgument
		{
			get { return (string)ViewState["CommandArgument"] ?? string.Empty; }
			set { ViewState["CommandArgument"] = value; }
		}

		public string CommandName
		{
			get { return (string)ViewState["CommandName"] ?? string.Empty; }
			set { ViewState["CommandName"] = value; }
		}

		public string Text
		{
			get { return (string)ViewState["Text"] ?? string.Empty; }
			set { ViewState["Text"] = value; }
		}

		bool IButtonControl.CausesValidation
		{
			get { return false; }
			set { }
		}

		string IButtonControl.PostBackUrl
		{
			get { return string.Empty; }
			set { }
		}

		string IButtonControl.ValidationGroup
		{
			get { return string.Empty; }
			set { }
		}

		event EventHandler IButtonControl.Click
		{
			add { throw new NotSupportedException(); }
			remove { throw new NotSupportedException(); }
		}

		event CommandEventHandler IButtonControl.Command
		{
			add { throw new NotSupportedException(); }
			remove { throw new NotSupportedException(); }
		}

		#endregion

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (Page != null)
			{
				Page.VerifyRenderingInServerForm(this);
			}

			string clickScript = C1CallbackButtonControlBase.EnsureEndWithSemiColon(OnClientClick);
			if (HasAttributes)
			{
				string customClickScript = Attributes["onclick"];
				if (customClickScript != null)
				{
					clickScript = clickScript + C1CallbackButtonControlBase.EnsureEndWithSemiColon(customClickScript);
					Attributes.Remove("onclick");
				}
			}

			if (clickScript.Length > 0)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Onclick, clickScript);
			}

#if ASP_NET4
			bool suportsDisabledAttr = SupportsDisabledAttribute;
#else
			bool suportsDisabledAttr = true;
#endif
			if (Enabled && !IsEnabled && suportsDisabledAttr) // test parent control

			{
				writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "disabled");
			}

			base.AddAttributesToRender(writer);
		}

		protected override void Render(HtmlTextWriter writer)
		{
			ICallbackContainer container = Container;

			if (container != null)
			{
				string argument = ((IPostBackContainer)container).GetPostBackOptions(this).Argument;
				OnClientClick = container.GetCallbackScript(this, argument);
			}

			base.Render(writer);
		}
	}

	internal class C1CallbackButton : C1CallbackButtonControlBase
	{
		public C1CallbackButton(ICallbackContainer container)
			: base(container, HtmlTextWriterTag.Input)
		{
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
		}
	}

	internal class C1CallbackImageButton : C1CallbackButtonControlBase
	{
		public C1CallbackImageButton(ICallbackContainer container)
			: base(container, HtmlTextWriterTag.Input)
		{
		}

		public string ImageUrl
		{
			get { return (string)ViewState["ImageUrl"] ?? string.Empty; }
			set { ViewState["ImageUrl"] = value; }
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			writer.AddAttribute(HtmlTextWriterAttribute.Type, "image");

			string url = this.ResolveClientUrl(ImageUrl);
			if (!string.IsNullOrEmpty(url))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Src, url);
			}
		}

	}

	internal class C1CallbackLinkButton : C1CallbackButtonControlBase
	{
		public C1CallbackLinkButton(ICallbackContainer container)
			: base(container, HtmlTextWriterTag.A)
		{
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			writer.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:void(0)");
		}

		protected override void Render(HtmlTextWriter writer)
		{
			SetForeColor();
			base.Render(writer);
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			writer.Write(Text);
		}

		private void SetForeColor()
		{
			if (ForeColor.IsEmpty)
			{
				for (Control parent = this.Parent; parent != null && !(parent is C1GridView); parent = parent.Parent)
				{
					WebControl wc = parent as WebControl;
					if (wc != null && !wc.ForeColor.IsEmpty)
					{
						this.ForeColor = wc.ForeColor;
						return;
					}
				}
			}
		}
	}
}
