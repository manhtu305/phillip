﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView.Controls
{
	using System.Text.RegularExpressions;
	using System.Web.UI;
	using System.Web.UI.WebControls;

	using C1.Web.Wijmo.Controls.C1Input;

	internal class C1InplaceEditor : WebControl, INamingContainer
	{
		private const string CLASS_NAME = "c1gridview-inplace-editor";

		private const string ID_CONST = "c1gvie"; // "c1gv_inplace_editor
		private const string ID_FORMAT = ID_CONST + "{0}";
		private readonly static Regex _rgTestID = new Regex(ID_CONST + "{1}(\\d+)", RegexOptions.Compiled);

		internal static bool TestID(string id)
		{
			return _rgTestID.Match(id).Success;
		}

		internal static int ParseID(string id)
		{
			Match match = _rgTestID.Match(id);
			if (match.Success)
			{
				return int.Parse(match.Groups[1].Value);
			}

			throw new InvalidOperationException("Invalid ID");
		}

		internal static string GenerateID(int columnIndex)
		{
			return string.Format(ID_FORMAT, columnIndex.ToString());
		}

		private enum NumberFormatType
		{
			Number,
			Decimal,
			Percentage,
			Currency
		}

		private struct NumberFormatInfo
		{
			public NumberFormatType FormatType;
			public int DecimalPlaces;

			public NumberFormatInfo(NumberFormatType formatType, int decimalPlaces)
			{
				FormatType = formatType;
				DecimalPlaces = decimalPlaces;
			}
		}

		private readonly static Regex _rgNumberFormat = new Regex(@"^(n|d|p|c){1}(\d*)$", RegexOptions.Compiled);

		private object _value;
		private string _formattedValue;

		private bool _applyFormat;
		private Type _dataType;
		private IMEMode _imeMode;
		private string _dataFormatString;
		private string _nullDisplayText;

		private WebControl _control;

		internal C1InplaceEditor(int columnIndex, Type dataType, IMEMode imeMode, bool applyFormat, string dataFormatString, string nullDisplayText)
		{
			_applyFormat = applyFormat;
			_dataType = dataType;
			_imeMode = imeMode;
			_dataFormatString = dataFormatString;
			_nullDisplayText = nullDisplayText;

			this.ID = GenerateID(columnIndex);
		}

		internal object Value
		{
			set
			{
				_value = value;
			}
			get
			{
				return _value;
			}
		}

		internal string FormattedValue
		{
			set { _formattedValue = value; }
		}

		internal object ExtractValue()
		{
			this.EnsureChildControls();

			if (_control != null)
			{
				C1InputText inputText = _control as C1InputText;
				if (inputText != null)
				{
					return inputText.Text;
				}
				else
				{
					C1InputNumber inputNumber = _control as C1InputNumber; // cast to a base class
					if (inputNumber != null)
					{
						object value;

						if (inputNumber is C1InputPercent)
						{
							value = inputNumber.Value / 100; // backward convsersion. See the _CreateInputNumber method
						}
						else
						{
							value = inputNumber.Value;
						}

						value = Convert.ChangeType(value, _dataType); // convert to the underlying data type.

						return value.ToString();
					}
					else
					{
						C1InputDate inputDate = _control as C1InputDate;
						if (inputDate != null)
						{
							if (inputDate.Date.HasValue)
							{
								return inputDate.Date.Value.ToString();
							}
						}
					}
				}
			}

			return null;
		}

		#region override

		protected override HtmlTextWriterTag TagKey
		{
			get { return HtmlTextWriterTag.Div; }
		}

		protected override void CreateChildControls()
		{
			if (_dataType._IsNumeric())
			{
				NumberFormatInfo nfi = GetNumberFormatInfo(this._dataType, this._dataFormatString);

				switch (nfi.FormatType)
				{
					case NumberFormatType.Currency:
						_control = _CreateInputNumber<C1InputCurrency>();
						break;

					case NumberFormatType.Decimal:
					case NumberFormatType.Number:
						_control = _CreateInputNumber<C1InputNumeric>();
						break;

					case NumberFormatType.Percentage:
						_control = _CreateInputNumber<C1InputPercent>();
						break;
				}

				((C1InputNumber)_control).DecimalPlaces = nfi.DecimalPlaces;
				((C1InputNumber)_control).ShowSpinner = true;

				Controls.Add(_control);
			}
			else
			{
				if (_dataType._IsDateTime() && (_dataType == typeof(DateTime)))
				{
					_control = _CreateInputDate();
					((C1InputDate)_control).DisplayFormat = this._dataFormatString;
				}
				else
				{
					_control = _CreateInputText();
				}
			}


			C1InputBase c1Editor = _control as C1InputBase;
			if (c1Editor != null)
			{
				c1Editor.Width = Unit.Percentage(100); // #57219
				c1Editor.ImeMode = ConvertIMEModeToImeMode(this._imeMode);
			}

			Controls.Add(_control);

			base.CreateChildControls();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();

			this.CssClass = C1InplaceEditor.CLASS_NAME;

			base.Render(writer);
		}

		#endregion

		private C1InputText _CreateInputText()
		{
			C1InputText editor = new C1InputText();

			if (_applyFormat)
			{
				editor.Text = _formattedValue;
			}
			else
			{
				if (!C1BaseField.IsNull(_value))
				{
					editor.Text = _value.ToString();
				}
				else
				{
					editor.Text = null;
					editor.Placeholder = _nullDisplayText;
				}
			}

			return editor;
		}

		private T _CreateInputNumber<T>()
			where T : C1InputNumber, new()
		{
			T editor = new T();

			if (!C1BaseField.IsNull(_value))
			{
				double value = Convert.ToDouble(_value);

				if (editor is C1InputPercent)
				{
					editor.Value = value * 100; // To support globalize.js requirements ("p"). See the ExtractValue method.
				}
				else
				{
					editor.DbValue = value;
				}
			}
			else
			{
				editor.Placeholder = _nullDisplayText;
				editor.DbValue = _value;
			}

			return editor;
		}

		private C1InputDate _CreateInputDate()
		{
			C1InputDate editor = new C1InputDate();

			if (!C1BaseField.IsNull(_value))
			{
				editor.Date = (DateTime)_value;
			}
			else
			{
				editor.Placeholder = _nullDisplayText;
				editor.Date = null;
			}

			return editor;
		}

		private ImeMode ConvertIMEModeToImeMode(IMEMode mode)
		{
			switch (mode)
			{
				case IMEMode.Active:
					return ImeMode.Active;

				case IMEMode.Auto:
					return ImeMode.Auto;

				case IMEMode.Disabled:
					return ImeMode.Disabled;

				case IMEMode.Inactive:
					return ImeMode.InActive;
			}

			return ImeMode.Auto;
		}

		// dataType must be a number type
		private NumberFormatInfo GetNumberFormatInfo(Type dataType, string dataFormatString)
		{
			NumberFormatInfo result = new NumberFormatInfo(NumberFormatType.Number, 2); // default

			if (!string.IsNullOrEmpty(dataFormatString))
			{
				Match match = _rgNumberFormat.Match(dataFormatString);
				if (match.Success)
				{
					// determine format type
					switch (match.Groups[1].Value.ToLowerInvariant())
					{
						case "d":
							result.FormatType = NumberFormatType.Decimal;
							break;

						case "c":
							result.FormatType = NumberFormatType.Currency;
							break;

						case "p":
							result.FormatType = NumberFormatType.Percentage;
							break;

						case "n":
							result.FormatType = NumberFormatType.Number;
							break;
					}

					// determine decimal places
					if (match.Groups[2].Success && !string.IsNullOrEmpty(match.Groups[2].Value))
					{
						result.DecimalPlaces = int.Parse(match.Groups[2].Value);
					}
				}
			}

			// check consistency between format and datatype 
			if (!dataType._IsReal()
				|| (result.FormatType == NumberFormatType.Decimal)) // padding is not supported
			{
				result.DecimalPlaces = 0;
			}

			return result;
		}
	}
}
