﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Web.UI;
	using System.Web.UI.WebControls;

	internal class ChildTable : Table
	{
		private readonly string _parentClientID;

		public ChildTable(string parentClientID)
		{
			_parentClientID = parentClientID;
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);

			if (DesignMode)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijgrid-root wijmo-wijgrid-table");
				writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "100%");
			}
		}

		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			if (DesignMode)
			{
				base.RenderBeginTag(writer);
			}

			// Don't call a base method here. We want to render only content when the Render() method is called.
		}

		public override void RenderEndTag(HtmlTextWriter writer)
		{
			if (DesignMode)
			{
				base.RenderEndTag(writer);
			}

			// Don't call a base method here. We want to render only content when the Render() method is called.
		}
	}
}
