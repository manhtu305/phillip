﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView.Controls
{
	using System.Web.UI;
	using System.Web.UI.WebControls;

	internal class C1LinkButton : LinkButton 
	{
		private ICallbackContainer _cbContainer;
		private IPostBackContainer _pbContainer;

		public C1LinkButton(ICallbackContainer cbContainer, IPostBackContainer pbContainer) : base()
		{
			_cbContainer = cbContainer;
			_pbContainer = pbContainer;
		}

		public override bool  CausesValidation
		{
			get
			{
				if (_pbContainer != null)
				{
					return false;
				}

				return base.CausesValidation;
			}
			set
			{
				if (_pbContainer != null)
				{
					base.CausesValidation = value;
				}
			}
		}

		protected override PostBackOptions GetPostBackOptions()
		{
			return (_pbContainer != null)
				? _pbContainer.GetPostBackOptions(this)
				: base.GetPostBackOptions();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (_cbContainer != null)
			{
				string argument = string.Empty;

				PostBackOptions pbo = GetPostBackOptions();
				if (pbo != null)
				{
					//argument = string.Format("\"{0}\"", pbo.Argument);
					argument = pbo.Argument;
				}

				OnClientClick = _cbContainer.GetCallbackScript(this, argument);
			}

			SetForeColor();
			base.Render(writer);
		}

		private void SetForeColor()
		{
			if (ForeColor.IsEmpty)
			{
				for (Control parent = this.Parent; parent != null && !(parent is C1GridView); parent = parent.Parent)
				{
					WebControl wc = parent as WebControl;
					if (wc != null && !wc.ForeColor.IsEmpty)
					{
						this.ForeColor = wc.ForeColor;
						return;
					}
				}
			}
		}
	}
}
