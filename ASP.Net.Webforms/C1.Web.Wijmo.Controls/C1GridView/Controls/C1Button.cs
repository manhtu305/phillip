﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView.Controls
{
	using System.Web.UI;
	using System.Web.UI.WebControls;

	internal class C1Button : Button
	{
		private ICallbackContainer _cbContainer;
		private IPostBackContainer _pbContainer;

		public C1Button(ICallbackContainer cbContainer, IPostBackContainer pbContainer)	: base()
		{
			_cbContainer = cbContainer;
			_pbContainer = pbContainer;
		}

		public override bool UseSubmitBehavior
		{
			get { return false;	}
			set {}
		}

		protected override PostBackOptions GetPostBackOptions()
		{
			return (_pbContainer != null)
				? _pbContainer.GetPostBackOptions(this)
				: base.GetPostBackOptions();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (_cbContainer != null)
			{
				string argument = string.Empty;

				PostBackOptions pbo = GetPostBackOptions();
				if (pbo != null)
				{
					//argument = string.Format("\"{0}\"", pbo.Argument);
					argument = pbo.Argument;
				}

				OnClientClick = _cbContainer.GetCallbackScript(this, argument);
			}

			base.Render(writer);
		}

	}
}
