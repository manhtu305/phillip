﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	/// <summary>
	/// Represents a collection of <see cref="C1GridViewRow"/> objects in a <see cref="C1GridView"/> control.
	/// </summary>
	public class C1GridViewRowCollection : ICollection, IEnumerable
	{
		private ArrayList _list;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewRowCollection"/> class using the specified <see cref="ArrayList"/>.
		/// </summary>
		/// <param name="rows">An <see cref="ArrayList"/> object that contains the <see cref="C1GridViewRow"/> objects with which to initialize the collection.</param>
		public C1GridViewRowCollection(ArrayList rows)
		{
			_list = rows;
		}

		/// <summary>
		/// Gets or sets the <see cref="C1GridViewRow"/> at the specified index within the <see cref="C1GridViewRowCollection"/>.
		/// In C#, this property is the indexer for the <see cref="C1GridViewRowCollection"/> class.
		/// </summary>
		public C1GridViewRow this[int index]
		{
			get { return (C1GridViewRow)_list[index]; }
		}

		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		#endregion

		#region ICollection Members

		void ICollection.CopyTo(Array array, int index)
		{
			_list.CopyTo(array, index);
		}

		/// <summary>
		/// Gets the number of items in the <see cref="C1GridViewRowCollection"/>.
		/// </summary>
		public int Count
		{
			get { return _list.Count; }
		}

		/// <summary>
		/// Gets a value indicating whether access to the <see cref="C1GridViewRowCollection"/> is synchronized (thread-safe).
		/// </summary>
		public bool IsSynchronized
		{
			get { return _list.IsSynchronized; }
		}

		/// <summary>
		/// Gets the object that can be used to synchronize access to the <see cref="C1GridViewRowCollection"/>.
		/// </summary>
		public object SyncRoot
		{
			get { return _list.SyncRoot ; }
		}

		#endregion
	}
}
