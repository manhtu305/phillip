﻿namespace C1.Web.Wijmo.Controls.C1GridView
{
	using C1.Web.Wijmo.Controls.Base;

	internal interface IFooOwnerCollection
	{
	}

	/// <summary>
	/// Represent serializer used by <see cref="C1GridView"/>. 
	/// </summary>
	internal class C1GridViewSerializer : C1BaseSerializer<C1GridView, C1BaseField, IFooOwnerCollection>
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="serializableObject"></param>
		public C1GridViewSerializer(object serializableObject)
			: base(serializableObject)
		{
		}

		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="collection"></param>
		/// <param name="item"></param>
		/// <returns></returns>
		protected override bool OnAddItem(object collection, object item)
		{
			C1BaseFieldCollection columns = collection as C1BaseFieldCollection;
			if (columns != null)
			{
				columns.Add((C1BaseField)item);
				return true;
			}

			return base.OnAddItem(collection, item);
		}

		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="collection"></param>
		/// <returns></returns>
		protected override bool OnClearItems(object collection)
		{
			C1BaseFieldCollection columns = collection as C1BaseFieldCollection;
			if (columns != null)
			{
				columns.Clear();
				return true;
			}

			return base.OnClearItems(collection);
		}
	}
}