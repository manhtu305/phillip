﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Represents a callback settings of a <see cref="C1GridView"/> control.
	/// </summary>
	public sealed class CallbackSettings : Settings, IJsonEmptiable
	{
		private const CallbackAction DEF_CALLBACKACTION = CallbackAction.None;
		private const CallbackMode DEF_CALLBACKMODE = CallbackMode.Partial;

		#region properties

		/// <summary>
		/// Determines the actions that can be performed using callbacks mechanism.
		/// </summary>
		/// <value>
		/// The default value is <see cref="CallbackAction.None"/>
		/// </value>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_CALLBACKACTION)]
		[C1Description("CallbackSettings.Action")]
		[Layout(LayoutType.Behavior)]
		[Json(true, true, DEF_CALLBACKACTION)]
		[NotifyParentProperty(true)]
		public CallbackAction Action
		{
			get { return GetPropertyValue("Action", DEF_CALLBACKACTION); }
			set
			{
				if (GetPropertyValue("Action", DEF_CALLBACKACTION) != value)
				{
					SetPropertyValue("Action", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Determines if partial or full update of control's HTML content will take place during callback.
		/// </summary>
		/// <value>
		/// The default value is <see cref="CallbackMode.Partial" />.
		/// </value>
		/// <remarks>
		/// Partial rendering is used with callbacks. When enabled, server sends only necessary HTML code
		/// portions to a client as a result of callback thus performing smart update. Partial rendering can be
		/// used for edit, update, cancel and select actions.
		/// If for any reason you need full update of control's HTML content, set this property to <see cref="CallbackMode.Full"/>.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_CALLBACKMODE)]
		[C1Description("CallbackSettings.Mode")]
		[Layout(LayoutType.Behavior)]
		[Json(true, true, DEF_CALLBACKMODE)]
		[NotifyParentProperty(true)]
		public CallbackMode Mode
		{
			get { return GetPropertyValue("Mode", DEF_CALLBACKMODE); }
			set
			{
				if (GetPropertyValue("Mode", DEF_CALLBACKMODE) != value)
				{
					SetPropertyValue("Mode", value);
					_OnPropertyChanged();
				}
			}
		}

		#endregion

		/// <summary>
		/// Occurs when a property of a <see cref="FilterSettings"/> object changes values.
		/// </summary>
		[Browsable(false)]
		public event EventHandler PropertyChanged;

		private void _OnPropertyChanged()
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, EventArgs.Empty);
			}
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return Action == DEF_CALLBACKACTION && Mode == DEF_CALLBACKMODE; }
		}

		#endregion
		#region IC1loneable Members

		/// <summary>
		/// Creates a new instance of the <see cref="CallbackSettings"/> class.
		/// </summary>
		/// <returns>
		/// A new instance of the <see cref="CallbackSettings"/> class.
		/// </returns>
		protected internal override Settings CreateInstance()
		{
			return new CallbackSettings();
		}

		/// <summary>
		/// Copies the properties of the specified <see cref="CallbackSettings"/> into the instance of the <see cref="CallbackSettings"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="CallbackSettings"/> that represents the properties to copy.</param>
		protected internal override void CopyFrom(Settings copyFrom)
		{
			this.Props.Clear();
			base.CopyFrom(copyFrom);
		}

		#endregion
	}
}