﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Web.UI;

	/// <summary>
	/// Specifies binding used in <see cref="C1GridView.Update"/> method.
	/// </summary>
	public class C1GridViewUpdateBinding : MixedPersister
	{
		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewUpdateBinding"/> class.
		/// </summary>
		public C1GridViewUpdateBinding()
		{
		}

		/// <summary>
		/// ControlProperty.
		/// </summary>
		public string ControlProperty
		{
			get { return GetViewStatePropertyValue("ControlProperty", string.Empty); }
			set
			{
				if (GetViewStatePropertyValue("ControlProperty", string.Empty) != value)
				{
					SetViewStatePropertyValue("ControlProperty", value);
				}
			}
		}

		/// <summary>
		/// UpdateField.
		/// </summary>
		public string UpdateField
		{
			get { return GetViewStatePropertyValue("UpdateField", string.Empty); }
			set
			{
				if (GetViewStatePropertyValue("UpdateField", string.Empty) != value)
				{
					SetViewStatePropertyValue("UpdateField", value);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return string.Format("{0} <- {1}", UpdateField, ControlProperty);
		}

		/// <summary>
		/// Creates a new instance of the <see cref="C1GridViewUpdateBinding"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1GridViewUpdateBinding"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1GridViewUpdateBinding();
		}
	}
}
