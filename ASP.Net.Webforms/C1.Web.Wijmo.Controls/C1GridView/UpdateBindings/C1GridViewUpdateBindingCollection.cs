﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.Collections;
	using System.Web.UI;

	/// <summary>
	///  Contains <see cref="C1GridViewUpdateBinding"/> objects specifying bindings used in <see cref="C1GridView.Update"/> method.
	/// </summary>
	public class C1GridViewUpdateBindingCollection : CollectionBase, IStateManager, IC1Cloneable
	{
		private bool _isTrackingViewState;

		/// <summary>
		/// Constructor. Initializes a new instance of the <see cref="C1GridViewUpdateBindingCollection"/> class.
		/// </summary>
		internal C1GridViewUpdateBindingCollection()
		{
		}

		/// <summary>
		/// Gets an <see cref="C1GridViewUpdateBinding"/> object from the collection at the specified index.
		/// In C#, this property is the indexer for the <see cref="C1GridViewUpdateBindingCollection"/> class.
		/// </summary>
		/// <param name="index">
		/// The index of the <see cref="C1GridViewUpdateBinding"/> derived object in the <see cref="C1GridViewUpdateBindingCollection"/> collection to retrieve.
		/// </param>
		public C1GridViewUpdateBinding this[int index]
		{
			get	{ return (C1GridViewUpdateBinding)List[index]; }
			set	{ List[index] = value; }
		}

		/// <summary>
		/// Adds the specified <see cref="C1GridViewUpdateBinding"/> object to the end of the collection.
		/// </summary>
		/// <param name="value">The <see cref="C1GridViewUpdateBinding"/> to append.</param>
		public void Add(C1GridViewUpdateBinding value)
		{
			List.Add(value);
		}

		/// <summary>
		/// Inserts the specified <see cref="C1GridViewUpdateBinding"/> object into the <see cref="C1GridViewUpdateBindingCollection"/> collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which the <see cref="C1GridViewUpdateBinding"/> is inserted.</param>
		/// <param name="value">The <see cref="C1GridViewUpdateBinding"/> to insert.</param>
		public void Insert(int index, C1GridViewUpdateBinding value)
		{
			List.Insert(index, value);
		}

		/// <summary>
		/// Determines the index of the specified <see cref="C1GridViewUpdateBinding"/> object in the collection.
		/// </summary>
		/// <param name="value">The <see cref="C1GridViewUpdateBinding"/> to locate. </param>
		/// <returns>
		/// The index of the value parameter, if it is found in the collection; otherwise, -1.
		/// </returns>
		public int IndexOf(C1GridViewUpdateBinding value)
		{
			return List.IndexOf(value);
		}

		#region IStateManager Members

		bool IStateManager.IsTrackingViewState
		{
			get { return _isTrackingViewState; }
		}

		void IStateManager.LoadViewState(object state)
		{
			Clear();

			if (state != null)
			{
				object[] stateObj = (object[])state;

				for (int i = 0, len = stateObj.Length; i < len; i++)
				{
					C1GridViewUpdateBinding binding = new C1GridViewUpdateBinding();
					((IStateManager)binding).LoadViewState(stateObj[i]);
					Add(binding);
				}
			}
		}

		object IStateManager.SaveViewState()
		{
			object[] state = new object[List.Count];
			bool hasState = false;

			for (int i = 0, len = List.Count; i < len; i++)
			{
				state[i] = ((IStateManager)List[i]).SaveViewState();
				hasState = hasState || (state[i] != null);
			}

			return (hasState)
				? state
				: null;
		}

		void IStateManager.TrackViewState()
		{
			_isTrackingViewState = true;

			foreach (C1GridViewUpdateBinding value in List)
			{
				((IStateManager)value).TrackViewState();
			}
		}

		#endregion

		/// <summary>
		/// Marks the state of the collection as having been changed.
		/// </summary>
		protected internal void SetDirty()
		{
			foreach (C1GridViewUpdateBinding value in List)
			{
				value.SetDirty();
			}
		}


		#region IC1Cloneable Members

		IC1Cloneable IC1Cloneable.CreateInstance()
		{
			return new C1GridViewUpdateBindingCollection();
		}

		/// <summary>
		/// Copies the properties of the specified <see cref="C1GridViewUpdateBindingCollection"/> into the instance of the <see cref="C1GridViewUpdateBindingCollection"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="C1GridViewUpdateBindingCollection"/> that represents the properties to copy.</param>
		internal void CopyFrom(C1GridViewUpdateBindingCollection copyFrom)
		{
			Clear();

			if (copyFrom != null)
			{
				foreach (C1GridViewUpdateBinding value in copyFrom)
				{
					Add((C1GridViewUpdateBinding)value.Clone());
				}
			}
		}

		void IC1Cloneable.CopyFrom(object copyFrom)
		{
			CopyFrom(copyFrom as C1GridViewUpdateBindingCollection);
		}

		#endregion

		#region ICloneable Members

		object ICloneable.Clone()
		{
			C1GridViewUpdateBindingCollection result = (C1GridViewUpdateBindingCollection)((IC1Cloneable)this).CreateInstance();
			result.CopyFrom(this);
			return result;
		}

		#endregion
	}
}
