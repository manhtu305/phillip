﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
	using System.ComponentModel;
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Class used for setting filtering behavior in the <see cref="C1GridView"/> control.
	/// </summary>
	public sealed class FilterSettings : Settings, IJsonEmptiable
	{
		private const bool DEF_FILTERONSELECT = true;
		private const FilterMode DEF_FILTERMODE = FilterMode.Auto;

		#region ** other properties

		/// <summary>
		/// Gets or sets a value indicating whether the filtering will be performed immediately once a filter operator is selected.
		/// </summary>
		/// <value>
		/// The default value is true.
		/// </value>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_FILTERONSELECT)]
		[C1Description("FilterSettings.FilterOnSelect")]
		[Layout(LayoutType.Behavior)]
		[Json(true, true, DEF_FILTERONSELECT)]
		[NotifyParentProperty(true)]
		public bool FilterOnSelect
		{
			get { return GetPropertyValue("FilterOnSelect", DEF_FILTERONSELECT); }
			set
			{
				if (GetPropertyValue("FilterOnSelect", DEF_FILTERONSELECT) != value)
				{
					SetPropertyValue("FilterOnSelect", value);
				}
			}
		}

		/// <summary>
		/// Gets or sets value indicating whether filtering will be performed automatically or not.
		/// </summary>
		/// <value>
		/// One of the <see cref="C1.Web.Wijmo.Controls.C1GridView.FilterMode"/> values.
		/// The default value is <see cref="C1.Web.Wijmo.Controls.C1GridView.FilterMode.Auto"/>.
		/// </value>
		[C1Category("Category.Behavior")]
		[C1Description("FilterSettings.Mode")]
		[DefaultValue(DEF_FILTERMODE)]
		[Json(true, true, DEF_FILTERMODE)]
		[Layout(LayoutType.Behavior)]
		public FilterMode Mode
		{
			get { return GetPropertyValue("Mode", DEF_FILTERMODE); }
			set
			{
				if (GetPropertyValue("Mode", DEF_FILTERMODE) != value)
				{
					SetPropertyValue("Mode", value);
				}
			}
		}

		#endregion


		/// <summary>
		/// Occurs when a property of a <see cref="FilterSettings"/> object changes values.
		/// </summary>
		[Browsable(false)]
		public event EventHandler PropertyChanged;

		private void _OnPropertyChanged()
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, EventArgs.Empty);
			}
		}


		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return FilterOnSelect == DEF_FILTERONSELECT && Mode == DEF_FILTERMODE; }
		}

		#endregion
	}
}
