﻿using System;

namespace C1.Web.Wijmo.Controls.C1GridView
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Design;
    using System.Globalization;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using C1.Web.Wijmo.Controls.Base;
    using C1.Web.Wijmo.Controls.C1GridView.Controls;
    using C1.Web.Wijmo.Controls.C1GridView.Data;
    using C1.Web.Wijmo.Controls.Localization;
    using System.Linq;

    /// <summary>
    /// Represents the <b>C1GridView</b> control.
    /// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [SupportsEventValidation]
    [ToolboxBitmap(typeof(C1GridView), "C1GridView.png")]
    [LicenseProvider()]
    public partial class C1GridView : C1TargetCompositeDataBoundControlBase, ICallbackContainer, ICallbackEventHandler,
        IPostBackContainer, IPostBackEventHandler, IPostBackDataHandler, IPersistedSelector,
        Base.Interfaces.IC1Serializable, IC1Cloneable
    {

        #region private classes
        private static class CssClasses
        {
            public const string C1GridView_EditRow = "c1-c1gridview-editrow";
            public const string C1GridView_SelectedRow = "c1-c1gridview-selectedrow";
            public const string C1GridView_Marker = "c1gridview-marker";
            public const string C1GridView_CollapsedRow = "c1-c1gridview-collapsedrow";
            public const string C1GridView_ExpandedRow = "c1-c1gridview-expandedrow";

            public const string Wijgrid = "wijmo-wijgrid";
            public const string Wijgrid_Row = "wijmo-wijgrid-row";
            public const string Wijgrid_AltRow = "wijmo-wijgrid-alternatingrow";
            public const string Wijgrid_DataRow = "wijmo-wijgrid-datarow";
            public const string Wijgrid_EmptyDataRow = "wijmo-wijgrid-emptydatarow";
            public const string Wijgrid_FilterRow = "wijmo-wijgrid-filterrow";
            public const string Wijgrid_FooterRow = "wijmo-wijgrid-footerrow";
            public const string Wijgrid_HeaderRow = "wijmo-wijgrid-headerrow";
            public const string Wijgrid_GroupArea = "wijmo-wijgrid-group-area";
            public const string Wijgrid_Header = "wijmo-wijgrid-header";
            public const string Wijgrid_Footer = "wijmo-wijgrid-footer";
            public const string Wijgrid_HierarchyDetailRow = "wijmo-wijgrid-hierarchy-detailrow";

            public const string Wijsuperpanel_Header = "wijmo-wijsuperpanel-header";
            public const string Wijsuperpanel_Footer = "wijmo-wijsuperpanel-footer";

            public const string Wijpager = "wijmo-wijpager";

            public const string Jui_Widget = "ui-widget";
            public const string JqueryUI_WidgetContent = "ui-widget-content";
            public const string JqueryUI_WidgetHeader = "ui-widget-header";
            public const string JqueryUI_CornerAll = "ui-corner-all";
            public const string JqueryUI_HelperClearfix = "ui-helper-clearfix";
            public const string JqueryUI_StateDefault = "ui-state-default";
        }

        private class ScrollingState : ICustomOptionType
        {
            private double? _x;
            private double? _y;
            private int _index;

            private int _maxCount = -1;

            public ScrollingState() : this(null, null, 0)
            {
            }

            public ScrollingState(int? x, int? y, int index)
            {
                _x = x;
                _y = y;
                _index = index;
            }

            public double? X { get { return _x; } }
            public double? Y { get { return _y; } }

            public int Index {
				get { return _index; }
				set { _index = value; }
			}

			public int MaxCount {
				get { return _maxCount; }
				set { _maxCount = value; }
			}

            public void Reset()
            {
                _x = null;
                _y = null;
                _index = 0;
                _maxCount = -1;
            }

            #region ICustomOptionType Members

            string ICustomOptionType.SerializeValue()
            {
                System.Globalization.CultureInfo invariant = System.Globalization.CultureInfo.InvariantCulture;

                return string.Format("{{\"x\":{0},\"y\":{1},\"index\":{2}}}",
                    _x.HasValue? _x.Value.ToString(invariant) : "null",
                    _y.HasValue ? _y.Value.ToString(invariant) : "null",
                    _index.ToString(invariant));
            }

            public void DeSerializeValue(object state)
            {
                Hashtable ht = (Hashtable)state;
                object val;

                _x = null;
                _y = null;

                if ((val = ht["x"]) != null)
                {
                    _x = (val is int) ? (double)(int)val : (double)val;
                }

                if ((val = ht["y"]) != null)
                {
                    _y = (val is int) ? (double)(int)val : (double)val;
                }

                this._index = (int)ht["index"];
            }

            bool ICustomOptionType.IncludeQuotes
            {
                get { return false; }
            }

            #endregion
        }

        private interface ICallbackEntity
        {
        }

        private class CallbackEntity : ICallbackEntity
        {
            [Json(true)]
            public string html { get; set; }
            [Json(true)]
            public string initializationScript { get; set; }
        }

        private class CallbackRowEnitity : CallbackEntity
        {
            [Json(true)]
            public int rowIndex { get; set; }
        }

        private class PartialCallbackEntity : ICallbackEntity
		{
			[Json(true)]
			public CallbackRowEnitity prevRow { get; set;}
			[Json(true)]
			public CallbackRowEnitity curRow { get; set; }
		}

        private class CallbackHierarchyEntity : CallbackEntity {
			[Json(true)]
			public int[] path { get; set; }
			[Json(true)]
			public int rowIndex { get; set; }
			[Json(true)]
			public bool expand { get; set; }
		}

        private class CallbackResponse
        {
            [Json(true)]
            public string command { get; set; }
            [Json(true)]
            public string mode { get; set; }
            [Json(true)]
            public ICallbackEntity entity { get; set; }
            [Json(true)]
            public string options { get; set; }
            [Json(true)]
            public string rootInnerState { get; set; }
        }

        private class HierarchyPath
        {
            internal static string MakeID(C1GridViewRow row)
            {
                return "p" + new HierarchyPath(row).ToString();
            }

            public HierarchyPath(string id)
            {
                Match match = Regex.Match(id, @"p((\d|-)+)$");
                if (match.Success)
                {
                    string[] strPath = match.Groups[1].Value.Split('-');

                    _path = new int[strPath.Length - 1];
                    _dataItemIndex = int.Parse(strPath[strPath.Length - 1]);

                    for (int i = 0; i < strPath.Length - 1; i++)
                    {
                        _path[i] = int.Parse(strPath[i]);
                    }
                }
                else
                {
                    throw new ArgumentException();
                }
            }

            public HierarchyPath(C1GridViewRow row)
            {
                C1GridView grid = row.Owner;

                List<int> path = new List<int>();

                while (grid != null && grid.Path >= 0)
                {
                    path.Insert(0, grid.Path);
                    grid = grid.Master;
                }

                _path = path.ToArray();
                _dataItemIndex = row.DisplayDataItemIndex;
            }


            private int[] _path;
            private int _dataItemIndex;

            public int[] Path
			{
				get	{ return _path;	}
			}

            public int DataItemIndex
            {
                get { return _dataItemIndex; }
            }

            public new string ToString()
            {
                string separator = "-";
                NumberFormatInfo invf = CultureInfo.InvariantCulture.NumberFormat;
                string result = string.Empty;

                if (_path != null && _path.Length > 0)
                {
                    // int[] -> string[]
                    string[] strPath = new string[_path.Length];

                    for (int i = 0; i < _path.Length; i++)
                    {
                        strPath[i] = _path[i].ToString(invf);
                    }

                    result = string.Join(separator, strPath);
                }

                result = string.IsNullOrEmpty(result)
                    ? _dataItemIndex.ToString(invf)
                    : result + separator + _dataItemIndex.ToString(invf);

                return result;
            }
        }

        private class CallbackRequest
        {
            public CallbackRequest(string request)
            {
                string eventArgument = request;

                if (eventArgument.StartsWith("#")) // the real source of callback is a detail grid.
                {
                    string[] eventValues = StringHelper.DecodeString(eventArgument.Substring(1));

                    eventArgument = eventValues[0];

                    DetailPath  = new HierarchyPath(eventValues[1]);
                    DetailJsonState = JsonHelper.StringToObject(eventValues[2]);
                }

                if (!string.IsNullOrEmpty(eventArgument))
                {
                    string[] args = eventArgument.Split(new char[] { ':' }, 2);
                    if (args.Length > 0)
                    {
                        Command = args[0].ToLowerInvariant();
                        if (args.Length > 1)
                        {
                            Argument = args[1];
                        }
                    }
                }
            }

            public CallbackRequest(string command, string argument)
            {
                Command = command;
                Argument = argument;
            }

            public string Command { get; private set; }
            public string Argument { get; set; }

            public HierarchyPath DetailPath { get; private set; }
            public Hashtable DetailJsonState { get; private set; }
        }

        #endregion

        #region static

        // don't obfuscate the event keys, they will be searched by name in the C1DetailGridView.
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventBeginRowUpdate = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventColumnGrouping = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventColumnGrouped = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventColumnMoving = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventColumnMoved = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventColumnUngrouping = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventColumnUngrouped = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventDetailRequiresDataSource = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventEndRowUpdated = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventFiltering = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventFiltered = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventHierarchyRowToggling = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventHierarchyRowToggled = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventPageIndexChanging = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventPageIndexChanged = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowCancelingEdit = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowCommand = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowCreated = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowDataBound = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowDeleting = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowDeleted = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowEditing = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventSorting = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventSorted = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowUpdating = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventRowUpdated = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventSelectedIndexChanged = new object();
        [SmartAssembly.Attributes.DoNotObfuscate]
        private static readonly object EventSelectedIndexChanging = new object();

        #endregion

        #region const

        internal const string NON_BREAKING_SPACE = "&nbsp;";

        internal const string CMD_CANCEL = "cancel";
        internal const string CMD_DELETE = "delete";
        internal const string CMD_EDIT = "edit";
        internal const string CMD_FILTER = "filter";
        internal const string CMD_MOVE = "move";
        internal const string CMD_PAGE = "page";
        internal const string CMD_SELECT = "select";
        internal const string CMD_SORT = "sort";
        internal const string CMD_UPDATE = "update";
        internal const string CMD_GROUP = "group";
        internal const string CMD_UNGROUP = "ungroup";
        internal const string CMD_BATCH = "batch";
        internal const string CMD_VSCROLL = "vscroll";
        internal const string CMD_HIERARCHY = "hierarchy";
        internal const string CMD_RENDER = "render"; //Get the rendering layout by callback, only used for exporting
        internal const string CMD_REFRESH = "refresh";

        private Regex _eventArgumentTest = new Regex(string.Format("^{0}|^{1}|^{2}|^{3}|^{4}|^{5}|^{6}|^{7}|^{8}|^{9}|^{10}|^{11}|^{12}|^{13}|^{14}", CMD_CANCEL, CMD_DELETE, CMD_EDIT, CMD_FILTER, CMD_MOVE, CMD_GROUP, CMD_UNGROUP, CMD_PAGE, CMD_SELECT, CMD_SORT, CMD_UPDATE, CMD_BATCH, CMD_VSCROLL, CMD_HIERARCHY, CMD_REFRESH));
        private const string CALLBACK_EXPRESSION = "new c1.gridview.cbp('{0}','{1}');";
        private const int INITVIRTUALPAGESIZE = 1; // the number of rows to render during initalizaton. Render a single row to calculate proper row height (to evaluate virtualPageSize())

        private const bool DEF_ALLOWCLIENTEDITING = false;
        private const bool DEF_ALLOWCUSTOMCONTENT = true;
        private const bool DEF_ALLOWCUSTOMPAGING = false;
        private const bool DEF_ALLOWC1INPUTEDITORS = false;
        private const bool DEF_AUTOGENERATECOLUMNS = true;
        private const bool DEF_AUTOGENERATEDELETEBUTTON = false;
        private const bool DEF_AUTOGENERATEEDITBUTTON = false;
        private const bool DEF_AUTOGENERATEFILTERBUTTON = false;
        private const bool DEF_AUTOGENERATESELECTBUTTON = false;
        private const bool DEF_ALLOWAUTOSORT = true;
        private const int DEF_EDITINDEX = -1;
        private const string DEF_EMPTYDATATEXT = "";
        private const string DEF_ROWHEADERCOLUMN = "";
        private const int DEF_SELECTEDINDEX = -1;
        private const SelectionMode DEF_CLIENTSELECTIONMODE = SelectionMode.None;
        private const ClientEditingUpdateMode DEF_CLIENTEDITINGUPDATEMODE = ClientEditingUpdateMode.AutoRowEdit;
        private const bool DEF_SHOWCLIENTSELECTIONONRENDER = true;
        private const bool DEF_SHOWHEADER = true;
        private const bool DEF_USEACCESSIBLEHEADER = true;
        private const int DEF_VIRTUALITEMCOUNT = 0;

        private const C1SortDirection _DEF_AUTOSORTDIR = C1SortDirection.None;
        private const string _DEF_AUTOSORTEXPR = "";
        private const int _DEF_PAGECOUNT = -1;

        private Hashtable _batchValues;
        #endregion

        #region fields
        private bool _dataBindFlag = false;

        private bool _jsonControlStateLoading = false;
        private bool _jsonControlStateLoaded = false;

        private bool _jsonViewStateLoading = false;
        private bool _jsonViewStateLoaded = false;
        private bool _ignoreDataBinding = false;
        private bool _jsonStateLoaded = false;

        private object _fieldInfoViewState;
        private C1DataSource _c1ds;
        private BandProcessor.RowColSpan[,] _headerTable;

        private IAutoFieldGenerator _fieldsGenerator;
        private DataKeyArray _dataKeys;
        private ArrayList _dataKeysInternal;
        private DataKey _persistedDataKey;

        //private C1GridViewRow _topPagerRow;
        //private C1GridViewRow _bottomPagerRow;
        private C1GridViewRow _headerRow;
        private C1GridViewRow _filterRow;
        private C1GridViewRow _footerRow;

        private C1BaseFieldCollection _autogeneratedColumns;
        private C1BaseFieldCollection _userColumns;
        private List<C1BaseField> _leaves;

        private C1GridViewRowCollection _rows;
        private ArrayList _rowsInternal;
        private CallbackSettings _callbackSettings;

        private FilterSettings _filterSettings;

        private ITemplate _emptyDataTemplate;

        private int _editIndex = DEF_EDITINDEX;
        private int _selectedIndex = DEF_SELECTEDINDEX;
        private int _pageCount = _DEF_PAGECOUNT;
        private int _bodyRowsCount = 0;
        private string[] _dataKeyNames = null;

        private TableItemStyle _alternatingRowStyle;
        private TableItemStyle _editRowStyle;
        private TableItemStyle _emptyDataRowStyle;
        private TableItemStyle _filterStyle;
        private TableItemStyle _footerStyle;
        private TableItemStyle _headerStyle;
        private TableItemStyle _rowStyle;
        private TableItemStyle _selectedRowStyle;

        private IOrderedDictionary _editRowValues;

        private C1SortDirection _autoSortDirection = _DEF_AUTOSORTDIR;
        private string _autoSortExpression = _DEF_AUTOSORTEXPR;

        private bool _isProductLicensed = false;
        private bool _shouldNag;

        private int _tmpRowCommandIndex = int.MinValue; // persist current row index during callback
        private ScrollingState _scrollingState = new ScrollingState();

        private C1DetailGridViewCollection _detail;
        private List<C1DetailGridView> _detailGrids;
        private C1GridView _owner;
        private HierararchyStateTree _hierarchyTree;

        private bool _createInnerState = false;
        private Hashtable _innerState = null;


        #endregion

        #region ctor

        /// <summary>
        /// Constructor. Creates a new instance of the <see cref="C1GridView"/> class.
        /// </summary>
        public C1GridView()
            : base()
        {
            VerifyLicense();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1GridView"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public C1GridView(string key)
            : base()
        {
#if GRAPECITY
			_isProductLicensed = true;
#else
            // ttZzKzJ5mwNr/IihpBl2pA==
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1GridView), this,
                Assembly.GetExecutingAssembly(), key);
            _isProductLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        #endregion

        #region properties

        /// <summary>
        /// Gets or sets a value that determines whether automatic sorting is enabled.
        /// </summary>
        /// <value>
        /// The default value is True.
        /// </value>
        /// <remarks>
        /// Set this property to True to enable automatic sorting. To enable sorting you must also
        /// set the <see cref="C1GridView.AllowSorting"/> to true.
        /// </remarks>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AllowAutoSort")]
        /*[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]*/
        [DefaultValue(DEF_ALLOWAUTOSORT)]
        [Layout(LayoutType.Behavior)]
        public virtual bool AllowAutoSort
        {
            get { return GetPropertyValue("AllowAutoSort", DEF_ALLOWAUTOSORT); }
            set
            {
                if (GetPropertyValue("AllowAutoSort", DEF_ALLOWAUTOSORT) != value)
                {
                    SetPropertyValue("AllowAutoSort", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// A value indicating whether client editing is enabled.
        /// </summary>
        /// <value>
        /// The default value is False.
        /// </value>
        /// <remarks>
        /// <para>
        /// In client-side editing mode underlying dataset can be automatically updated only if the control is bound to DataSource control.
        /// Otherwise developer should implement dataset update using <see cref="C1GridView.BeginRowUpdate"/>, <see cref="C1GridView.RowUpdating"/> and <see cref="C1GridView.EndRowUpdated"/> event handlers.
        /// </para>
        /// <para>
        /// Note: only visible data are available on the client, hence after refresh values of the hidden columns used in UPDATE statement can be set to NULL.
        /// </para>
        /// </remarks> 
        [DefaultValue(DEF_ALLOWCLIENTEDITING)]
        [C1Description("C1Grid.AllowClientEditing")]
        [C1Category("Category.Behavior")]
        [Json(true, true, DEF_ALLOWCLIENTEDITING)]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public virtual bool AllowClientEditing
        {
            get { return GetPropertyValue("AllowClientEditing", DEF_ALLOWCLIENTEDITING); }
            set
            {
                if (GetPropertyValue("AllowClientEditing", DEF_ALLOWCLIENTEDITING) != value)
                {
                    SetPropertyValue("AllowClientEditing", value);
                }
            }
        }

        /// <summary>
        /// Infrastructure.
        /// </summary>
        [DefaultValue(DEF_ALLOWCUSTOMCONTENT)]
        [Json(true, true, DEF_ALLOWCUSTOMCONTENT)]
        [WidgetOption]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual bool AllowCustomContent
        {
            get { return GetPropertyValue("AllowCustomContent", DEF_ALLOWCUSTOMCONTENT); }
            set
            {
                if (GetPropertyValue("AllowCustomContent", DEF_ALLOWCUSTOMCONTENT) != value)
                {
                    SetPropertyValue("AllowCustomContent", value);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether custom paging is enabled.
        /// </summary>
        /// <value>
        /// The default value is False.
        /// </value>
        /// <remarks>
        /// <para>
        /// Set this property to True to enable custom paging. 
        /// </para>
        /// <para>
        /// Paging allows you to display the contents of the <see cref="C1GridView"/> component in page segments.
        /// To enable custom paging, set both the <see cref="C1GridView.AllowPaging"/> and AllowCustomPaging properties to True.
        /// Next, provide code to handle the <see cref="C1GridView.PageIndexChanged"/> event. Create a data source that contains
        /// the data to display on a single page and then use the DataBind() method to bind the data to
        /// the <see cref="C1GridView"/> component.
        /// </para>
        /// <para>
        /// Because only a segment of the data is loaded, you must set the <see cref="VirtualItemCount"/> property to the total number of items
        /// in the <see cref="C1GridView"/> component. This allows the control to determine the total number of pages
        /// needed to display every item in the <see cref="C1GridView"/> component. This property is normally
        /// programmatically set once the total number of items in the <see cref="C1GridView"/> component is determined.
        /// </para>
        /// <para>
        /// When paging is enabled with the AllowCustomPaging property set to False, the <see cref="C1GridView"/>
        /// component assumes that the data source contains all the items to be displayed. The <see cref="C1GridView"/>
        /// component calculates the indexes of the items on the displayed page based on the page index, specified
        /// by the <see cref="C1GridView.PageIndex"/> property, and the number of items on a page, specified by the
        /// <see cref="C1GridView.PageSize"/> property.
        /// </para>
        /// <para>
        /// When the AllowCustomPaging property is set to True, the <see cref="C1GridView"/> component assumes that
        /// the data source only contains the items as determined by the <see cref="C1GridView.VirtualItemCount"/> property.
        /// All items up to the number of items specified by the <see cref="C1GridView.PageSize"/> property are displayed.
        /// </para>
        /// </remarks>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AllowCustomPaging")]
        [DefaultValue(DEF_ALLOWCUSTOMPAGING)]
        [Layout(LayoutType.Behavior)]
        [Json(true, true, DEF_ALLOWCUSTOMPAGING)]
        public bool AllowCustomPaging
        {
            get { return GetPropertyValue("AllowCustomPaging", DEF_ALLOWCUSTOMPAGING); }
            set
            {
                if (GetPropertyValue("AllowCustomPaging", DEF_ALLOWCUSTOMPAGING) != value)
                {
                    SetPropertyValue("AllowCustomPaging", value);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether regular textboxes or C1Input controls will be used as cell editors during server-side editing.
        /// </summary>
        /// <value>
        /// The default value is False.
        /// </value>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AllowC1InputEditors")]
        [DefaultValue(DEF_ALLOWC1INPUTEDITORS)]
        [Layout(LayoutType.Behavior)]
        [Json(true, true, DEF_ALLOWC1INPUTEDITORS)]
        public virtual bool AllowC1InputEditors
        {
            get { return GetPropertyValue("AllowC1InputEditors", DEF_ALLOWC1INPUTEDITORS); }
            set
            {
                if (GetPropertyValue("AllowC1InputEditors", DEF_ALLOWC1INPUTEDITORS) != value)
                {
                    SetPropertyValue("AllowC1InputEditors", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value that determines whether <see cref="C1BoundField"/> objects are generated and displayed automatically.
        /// </summary>
        /// <value>
        /// Set this property to True to automatically display and create <see cref="C1BoundField"/> objects.
        /// The default value is True.
        /// </value>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AutogenerateColumns")]
        [Json(true, true, DEF_AUTOGENERATECOLUMNS)]
        [Layout(LayoutType.Behavior)]
        public virtual bool AutogenerateColumns
        {
            get { return GetPropertyValue("AutogenerateColumns", DEF_AUTOGENERATECOLUMNS); }
            set
            {
                if (GetPropertyValue("AutogenerateColumns", DEF_AUTOGENERATECOLUMNS) != value)
                {
                    SetPropertyValue("AutogenerateColumns", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a <see cref="C1CommandField"/> field column with a Delete button for each data row is automatically added to a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AutoGenerateDeleteButton")]
        [DefaultValue(DEF_AUTOGENERATEDELETEBUTTON)]
        [Json(true, true, DEF_AUTOGENERATEDELETEBUTTON)]
        [Layout(LayoutType.Behavior)]
        public virtual bool AutoGenerateDeleteButton
        {
            get { return GetPropertyValue("AutoGenerateDeleteButton", DEF_AUTOGENERATEDELETEBUTTON); }
            set
            {
                if (GetPropertyValue("AutoGenerateDeleteButton", DEF_AUTOGENERATEDELETEBUTTON) != value)
                {
                    SetPropertyValue("AutoGenerateDeleteButton", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a <see cref="C1CommandField"/> field column with an Edit button for each data row is automatically added to a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AutoGenerateEditButton")]
        [DefaultValue(DEF_AUTOGENERATEEDITBUTTON)]
        [Json(true, true, DEF_AUTOGENERATEEDITBUTTON)]
        [Layout(LayoutType.Behavior)]
        public virtual bool AutoGenerateEditButton
        {
            get { return GetPropertyValue("AutoGenerateEditButton", DEF_AUTOGENERATEEDITBUTTON); }
            set
            {
                if (GetPropertyValue("AutoGenerateEditButton", DEF_AUTOGENERATEEDITBUTTON) != value)
                {
                    SetPropertyValue("AutoGenerateEditButton", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a <see cref="C1CommandField"/> field column with an Filter button for the filter row is automatically added to a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AutoGenerateFilterButton")]
        [DefaultValue(DEF_AUTOGENERATEFILTERBUTTON)]
        [Json(true, true, DEF_AUTOGENERATEFILTERBUTTON)]
        [Layout(LayoutType.Behavior)]
        public virtual bool AutoGenerateFilterButton
        {
            get { return GetPropertyValue("AutoGenerateFilterButton", DEF_AUTOGENERATEFILTERBUTTON); }
            set
            {
                if (GetPropertyValue("AutoGenerateFilterButton", DEF_AUTOGENERATEFILTERBUTTON) != value)
                {
                    SetPropertyValue("AutoGenerateFilterButton", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a <see cref="C1CommandField"/> field column with an Select button for each data row is automatically added to a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.AutoGenerateSelectButton")]
        [DefaultValue(DEF_AUTOGENERATESELECTBUTTON)]
        [Json(true, true, DEF_AUTOGENERATESELECTBUTTON)]
        [Layout(LayoutType.Behavior)]
        public virtual bool AutoGenerateSelectButton
        {
            get { return GetPropertyValue("AutoGenerateSelectButton", DEF_AUTOGENERATESELECTBUTTON); }
            set
            {
                if (GetPropertyValue("AutoGenerateSelectButton", DEF_AUTOGENERATESELECTBUTTON) != value)
                {
                    SetPropertyValue("AutoGenerateSelectButton", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="CallbackSettings"/> object that enables you to determine actions that can be performed by the <see cref="C1.Web.Wijmo.Controls.C1GridView"/> using callbacks mechanism.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.CallbackSettings")]
        [Json(true, true, null)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
#if ASP_NET45
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewObjectConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
        [TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewObjectConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewObjectConverter, C1.Web.Wijmo.Controls.Design.3")]
#endif
        [Layout(LayoutType.Behavior)]

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public virtual CallbackSettings CallbackSettings
        {
            get
            {
                if (_callbackSettings == null)
                {
                    _callbackSettings = new CallbackSettings();

                    _callbackSettings.PropertyChanged += delegate(object o, EventArgs e)
                    {
                        OnRequiresDataBinding();
                    };
                }

                return _callbackSettings;
            }
        }

        /// <summary>
        /// Determines the method of sending client-side edited data to server.
        /// </summary>
        /// <value>
        /// The default value is <see cref="C1.Web.Wijmo.Controls.C1GridView.ClientEditingUpdateMode.AutoRowEdit"/>.
        /// </value>
        [DefaultValue(DEF_CLIENTEDITINGUPDATEMODE)]
        [C1Description("C1Grid.ClientEditingUpdateMode")]
        [C1Category("Category.Behavior")]
        [Json(true, true, DEF_CLIENTEDITINGUPDATEMODE)]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public virtual ClientEditingUpdateMode ClientEditingUpdateMode
        {
            get { return GetPropertyValue("ClientEditingUpdateMode", DEF_CLIENTEDITINGUPDATEMODE); }
            set
            {
                if (GetPropertyValue("ClientEditingUpdateMode", DEF_CLIENTEDITINGUPDATEMODE) != value)
                {
                    SetPropertyValue("ClientEditingUpdateMode", value);
                }
            }
        }

#if ASP_NET4
        private ClientIDMode _clientIDMode = ClientIDMode.AutoID;


        /// <summary>
        /// Gets or sets the algorithm that is used to generate the value of the System.Web.UI.Control.ClientID property.
        /// </summary>
        /// <value>
        /// The default is System.Web.UI.ClientIDMode.AutoID.
        /// </value>
        /// <returns>
        /// A value that indicates how the System.Web.UI.Control.ClientID property is generated. 
        /// </returns>
        [DefaultValue(ClientIDMode.AutoID)]
        public override ClientIDMode ClientIDMode
        {
            get { return _clientIDMode; }
            set
            {
                if (_clientIDMode != value)
                {
                    _clientIDMode = value;
                    ClearEffectiveClientIDMode();
                    ClearCachedClientID();
                }
            }
        }
#endif

        /// <summary>
        /// Represents client-side selection behavior.
        /// </summary>
        [DefaultValue(DEF_CLIENTSELECTIONMODE)]
        [C1Description("C1Grid.ClientSelectionMode")]
        [C1Category("Category.Behavior")]
        [Json(true, true, DEF_CLIENTSELECTIONMODE)]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public virtual SelectionMode ClientSelectionMode
        {
            get { return GetPropertyValue("ClientSelectionMode", DEF_CLIENTSELECTIONMODE); }
            set
            {
                if (GetPropertyValue("ClientSelectionMode", DEF_CLIENTSELECTIONMODE) != value)
                {
                    SetPropertyValue("ClientSelectionMode", value);
                }
            }
        }

        /// <summary>
        /// Gets a collection of objects representing the columns of the <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Default")]
        [C1Description("C1Grid.Columns")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Layout(LayoutType.Behavior)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
#if ASP_NET5
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1BaseFieldCollectionEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1BaseFieldCollectionEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1BaseFieldCollectionEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
        [WidgetOption]
        public virtual C1BaseFieldCollection Columns
        {
            get
            {
                if (_userColumns == null)
                {
                    _userColumns = new C1BaseFieldCollection(this);

                    _userColumns.FieldsChanged += delegate(object o, EventArgs e)
                    {
                        OnRequiresDataBinding();
                    };

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_userColumns).TrackViewState();
                    }
                }

                return _userColumns;
            }
        }

        /// <summary>
        /// Gets or sets the control that will automatically generate the columns for
        /// a System.Web.UI.WebControls.GridView control that uses ASP.NET Dynamic Data
        /// features.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public IAutoFieldGenerator ColumnsGenerator
        {
            get { return _fieldsGenerator; }
            set { _fieldsGenerator = value; }
        }

        /// <summary>
        /// Gets or sets an array that contains the names of the primary key fields for the items displayed in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Data")]
        [C1Description("C1Grid.DataKeyNames")]
#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1GridViewFieldNamesEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1GridViewFieldNamesEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors.C1GridViewFieldNamesEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
        [TypeConverter(typeof(StringArrayConverter))]
        public virtual string[] DataKeyNames
        {
            get
            {
                return (_dataKeyNames == null)
                    ? new string[0]
                    : (string[])_dataKeyNames.Clone();
            }
            set
            {
                if (!StringArrayHelper.Compare(_dataKeyNames, value))
                {
                    _dataKeyNames = value == null
                        ? (string[])null
                        : (string[])value.Clone();

                    ClearDataKeys();

                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets a collection of <see cref="DataKey"/> objects that represent the data key value of each row in a <see cref="C1GridView"/> control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DataKeyArray DataKeys
        {
            get
            {
                if (_dataKeys == null)
                {
                    _dataKeys = new DataKeyArray(DataKeysInternal);

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_dataKeys).TrackViewState();
                    }
                }

                return _dataKeys;
            }
        }

        internal virtual bool SerializeStateProperties
		{
			get	{ return Page != null && Page.IsCallback; }
		}

        internal virtual bool PersistDetailProperty
        {
            get { return true; }
        }

        private int _path = -1;
        internal int Path
        {
            get { return _path; }
            set { _path = value; }
        }

        /// <summary>
        /// Gets an <see cref="C1GridView"/> object that represents a master grid that contains this grid.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public C1GridView Master
        {
            get { return _owner; }
            internal set { _owner = value; }
        }

        internal virtual C1GridView Root
        {
            get { return null; }
        }

        /// <summary>
        /// Gets a collection of <see cref="C1DetailGridView"/> objects which are used as a template for populating detail grids in a hierarchical grid.
        /// </summary>
        /// <remarks>
        /// Only the first item of the collection is used.
        /// </remarks>
        /// <remarks>
        /// This is supported only when the data source for the GridView control is a DataSourceControl.
        /// </remarks>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Browsable(false)] // Hide property to avoid VS designer "not responding" issue because of nested Detail propertis. Need to provide a custom editor.
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public C1DetailGridViewCollection Detail
        {
            get
            {
                if (_detail == null)
                {
                    _detail = new C1DetailGridViewCollection(this);

                    _detail.DetailsChanged += delegate(object o, EventArgs e)
                    {
                        if (Master != null)
                        {
                            Master.OnRequiresDataBinding();
                        }
                        else
                        {
                            OnRequiresDataBinding();
                        }
                    };

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_detail).TrackViewState();
                    }
                }

                return _detail;
            }
        }

        /// <summary>
        /// Gets a collection of <see cref="C1DetailGridView"/> objects that contains populated detail grids in a hierarchical grid.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public List<C1DetailGridView> Details
        {
            get
            {
                if (_detailGrids == null)
                {
                    _detailGrids = new List<C1DetailGridView>();
                }

                return _detailGrids;
            }
        }

        /// <summary>
        /// Gets or sets the index of the item to be edited.
        /// </summary>
        /// <value>
        /// The default value is -1, which indicates that no item in the <see cref="C1GridView"/> component is being edited.
        /// </value>
        [C1Category("Category.Default")]
        [C1Description("C1Grid.EditIndex")]
        [DefaultValue(DEF_EDITINDEX)]
        [Browsable(false)]
        [Json(true, true, DEF_EDITINDEX)]
        public virtual int EditIndex
        {
            get { return _editIndex; }
            set
            {
                if (value < -1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                if (_editIndex != value)
                {
                    if (value == -1)
                    {
                        EditRowValues.Clear();
                    }

                    _editIndex = value;
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets the user-defined content for the empty data row rendered when a <see cref="C1GridView"/> control is bound to a data source that does not contain any records.
        /// </summary>
        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(C1GridViewRow))]
        public virtual ITemplate EmptyDataTemplate
        {
            get { return _emptyDataTemplate; }
            set { _emptyDataTemplate = value; }
        }

        /// <summary>
        /// Gets or sets the text to display in the empty data row rendered when a <see cref="C1GridView"/> control is bound to a data source that does not contain any records.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Grid.EmptyDataText")]
        [DefaultValue(DEF_EMPTYDATATEXT)]
        [Json(true, true, DEF_EMPTYDATATEXT)]
        [Layout(LayoutType.Appearance)]
        public virtual string EmptyDataText
        {
            get { return GetPropertyValue("EmptyDataText", DEF_EMPTYDATATEXT); }
            set
            {
                if (GetPropertyValue("EmptyDataText", DEF_EMPTYDATATEXT) != value)
                {
                    SetPropertyValue("EmptyDataText", value);
                }
            }
        }

        /// <summary>
        /// Gets a <see cref="C1GridViewRow"/> object that represents the filter row in a <see cref="C1GridView"/> control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public virtual C1GridViewRow FilterRow
        {
            get
            {
                if (_filterRow == null)
                {
                    EnsureChildControls();
                }

                return _filterRow;
            }
        }


        /// <summary>
        /// Gets the <see cref="FilterSettings"/> object that defines the filter behaviors of the column at client-side.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.FilterSettings")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Json(true, true, null)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
#if ASP_NET45
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewObjectConverter, C1.Web.Wijmo.Controls.Design.45")]
#elif ASP_NET4
        [TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewObjectConverter, C1.Web.Wijmo.Controls.Design.4")]
#elif ASP_NET35
		[TypeConverter("C1.Web.Wijmo.Controls.Design.C1GridView.C1GridViewObjectConverter, C1.Web.Wijmo.Controls.Design.3")]
#endif
        [Layout(LayoutType.Behavior)]
        public FilterSettings FilterSettings
        {
            get
            {
                if (_filterSettings == null)
                {
                    _filterSettings = new FilterSettings();

                    _filterSettings.PropertyChanged += delegate(object o, EventArgs e)
                    {
                        OnRequiresDataBinding();
                    };
                }

                return _filterSettings;
            }
        }

        /// <summary>
        /// Gets a <see cref="C1GridViewRow"/> object that represents the footer row in a <see cref="C1GridView"/> control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual C1GridViewRow FooterRow
        {
            get
            {
                if (_footerRow == null)
                {
                    EnsureChildControls();
                }

                return _footerRow;
            }
        }

        /// <summary>
        /// Gets a <see cref="C1GridViewRow"/> object that represents the header row in a <see cref="C1GridView"/> control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual C1GridViewRow HeaderRow
        {
            get
            {
                if (_headerRow == null)
                {
                    EnsureChildControls();
                }

                return _headerRow;
            }
        }

        /// <summary>
        /// Gets the total number of pages needed to display the items in the <see cref="C1GridView"/> component.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Json(true)]
        public virtual int PageCount
        {
            get { return _pageCount; }
        }

        /// <summary>
        /// Gets or sets the name of the column to use as the column header for the <see cref="C1GridView"/> control.
        /// This property is provided to make the control more accessible to users of assistive technology devices.
        /// </summary>
        /// <value>
        /// The name of the column to use as the column header.
        /// The default value is an empty string (""), which indicates that this property is not set.
        /// </value>
        [C1Category("Category.Accessibility")]
        [C1Description("C1Grid.RowHeaderColumn")]
        [DefaultValue(DEF_ROWHEADERCOLUMN)]
        [Json(true, true, DEF_ROWHEADERCOLUMN)]
        [Layout(LayoutType.Accessibility)]
        public string RowHeaderColumn
        {
            get { return GetPropertyValue("RowHeaderColumn", DEF_ROWHEADERCOLUMN); }
            set
            {
                if (GetPropertyValue("RowHeaderColumn", DEF_ROWHEADERCOLUMN) != value)
                {
                    SetPropertyValue("RowHeaderColumn", value);
                }
            }
        }

        /// <summary>
        /// Gets a collection of objects representing the items in <see cref="C1GridView"/>.
        /// </summary>
        /// <value>
        /// A <see cref="C1GridViewRowCollection"/> that contains a collection of <see cref="C1GridViewRow"/> objects representing
        /// the individual rows in the <see cref="C1GridView"/> control.
        /// </value>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [Themeable(false)]
        public virtual C1GridViewRowCollection Rows
        {
            get
            {
                if (_rows == null)
                {
                    if (_rowsInternal == null)
                    {
                        EnsureChildControls();
                    }

                    if (_rowsInternal == null)
                    {
                        _rowsInternal = new ArrayList();
                    }

                    _rows = new C1GridViewRowCollection(_rowsInternal);
                }

                return _rows;
            }
        }

        /// <summary>
        /// Gets the <see cref="DataKey"/> object that contains the data key value for the selected row in a <see cref="C1GridView"/> control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual DataKey SelectedDataKey
        {
            get
            {
                string[] dataKeyNames = DataKeyNamesInternal;
                if (dataKeyNames.Length == 0)
                {
                    throw new InvalidOperationException(string.Format(C1Localizer.GetString("C1Grid.EMissedDataKeyNames"), ID));
                }

                return (SelectedIndex >= 0 && SelectedIndex < DataKeys.Count)
                    ? DataKeys[SelectedIndex]
                    : null;
            }
        }

        /// <summary>
        /// Gets or sets the index of the selected row.
        /// </summary>
        /// <value>
        /// The default value is -1. Set this property to -1 to deselect an item in the <see cref="C1GridView"/> component.
        /// </value>
        [C1Description("C1Grid.SelectedIndex")]
        [Bindable(true)]
        [DefaultValue(DEF_SELECTEDINDEX)]
        [Json(true, true, DEF_SELECTEDINDEX)]
        public virtual int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (value < -1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                int oldIIndex = _selectedIndex;

                _selectedIndex = value;

                if (_rowsInternal != null)
                {
                    if (oldIIndex >= 0 && _rowsInternal.Count > oldIIndex)
                    {
                        C1GridViewRow row = (C1GridViewRow)_rowsInternal[oldIIndex];
                        row.RowState &= ~C1GridViewRowState.Selected;
                    }

                    if (value >= 0 && _rowsInternal.Count > value)
                    {
                        C1GridViewRow row = (C1GridViewRow)_rowsInternal[value];
                        row.RowState |= C1GridViewRowState.Selected;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the data-key value for the persisted selected item in a System.Web.UI.WebControls.GridView control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Layout(LayoutType.None)]
        public virtual DataKey SelectedPersistedDataKey
        {
            get { return _persistedDataKey; }
            set
            {
                _persistedDataKey = value;

                if (IsTrackingViewState && _persistedDataKey != null)
                {
                    ((IStateManager)_persistedDataKey).TrackViewState();
                }
            }
        }

        /// <summary>
        /// Gets a <see cref="C1GridViewRow"/> object that represents the selected item in a <see cref="C1GridView"/> control.
        /// </summary>
        /// <remarks>
        /// This <see cref="C1GridViewRow"/> object can be used to access the properties of the selected item.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual C1GridViewRow SelectedRow
        {
            get
            {
                int index = SelectedIndex;

                return (index < 0)
                    ? null
                    : Rows[index];
            }
        }

        /// <summary>
        /// Gets the data key value of the selected row in a <see cref="C1GridView"/> control.
        /// </summary>
        [Browsable(false)]
        public object SelectedValue
        {
            get
            {
                return (SelectedDataKey != null)
                    ? SelectedDataKey.Value
                    : null;
            }
        }

        /// <summary>
        /// A value indicating whether a selection will be automatically displayed at the current cell position when the wijgrid is rendered.
        /// </summary>
        /// <value>
        /// The default value is true.
        /// </value>
        /// <remarks>
        /// Set this property to false if you want to prevent C1GridView from selecting the currentCell automatically.
        /// </remarks>
        [C1Category("Category.Appearance")]
        [C1Description("C1Grid.ShowClientSelectionOnRender")]
        [DefaultValue(DEF_SHOWCLIENTSELECTIONONRENDER)]
        [Layout(LayoutType.Appearance)]
        [Json(true, false, DEF_SHOWCLIENTSELECTIONONRENDER)] // always serialize
        [WidgetOption]
        public virtual bool ShowClientSelectionOnRender
		{
			get {
				return GetPropertyValue("ShowClientSelectionOnRender", DEF_SHOWCLIENTSELECTIONONRENDER);
			}
			set
			{
				if (GetPropertyValue("ShowClientSelectionOnRender", DEF_SHOWCLIENTSELECTIONONRENDER) != value)
				{
					SetPropertyValue("ShowClientSelectionOnRender", value);
				}
			}
		}

        /// <summary>
        /// Determines whether the header is displayed.
        /// </summary>
        /// <value>
        /// The default value is true.
        /// </value>
        /// <remarks>
        /// Set this property to true to display the header.
        /// </remarks>
        [C1Category("Category.Appearance")]
        [C1Description("C1Grid.ShowHeader")]
        [DefaultValue(DEF_SHOWHEADER)]
        [Layout(LayoutType.Appearance)]
        [Json(true, true, DEF_SHOWHEADER)]
        public virtual bool ShowHeader
        {
            get { return GetPropertyValue("ShowHeader", DEF_SHOWHEADER); }
            set
            {
                if (GetPropertyValue("ShowHeader", DEF_SHOWHEADER) != value)
                {
                    SetPropertyValue("ShowHeader", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a <see cref="C1GridView"/> control renders its header in an accessible format.
        /// This property is provided to make the control more accessible to users of assistive technology devices.
        /// </summary>
        [C1Category("Category.Accessibility")]
        [C1Description("C1Grid.UseAccessibleHeader")]
        [DefaultValue(DEF_USEACCESSIBLEHEADER)]
        [Json(true, true, DEF_USEACCESSIBLEHEADER)]
        [Layout(LayoutType.Accessibility)]
        public virtual bool UseAccessibleHeader
        {
            get { return GetPropertyValue("UseAccessibleHeader", DEF_USEACCESSIBLEHEADER); }
            set
            {
                if (GetPropertyValue("UseAccessibleHeader", DEF_USEACCESSIBLEHEADER) != value)
                {
                    SetPropertyValue("UseAccessibleHeader", value);
                    OnRequiresDataBinding();
                }
            }
        }

        /// <summary>
        /// Gets or sets the virtual number of items when custom paging is used.
        /// </summary>
        /// <remarks>
        /// This property is only used when the <see cref="C1GridView.AllowCustomPaging"/> property is set to True.
        /// </remarks>
        [Browsable(false)]
        [DefaultValue(DEF_VIRTUALITEMCOUNT)]
        [Json(true, true, DEF_VIRTUALITEMCOUNT)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual int VirtualItemCount
        {
            get { return GetPropertyValue("VirtualItemCount", DEF_VIRTUALITEMCOUNT); }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                SetPropertyValue("VirtualItemCount", value);
            }
        }

        #endregion

        #region styles

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of alternating data rows in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.AlternatingRowStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle AlternatingRowStyle
        {
            get
            {
                if (_alternatingRowStyle == null)
                {
                    _alternatingRowStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_alternatingRowStyle).TrackViewState();
                    }
                }

                return _alternatingRowStyle;
            }
        }


        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the row selected for editing in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.EditRowStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle EditRowStyle
        {
            get
            {
                if (_editRowStyle == null)
                {
                    _editRowStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_editRowStyle).TrackViewState();
                    }
                }

                return _editRowStyle;
            }
        }

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the empty data row rendered when a <see cref="C1GridView"/> control is bound to a data source that does not contain any records.
        /// </summary>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.EmptyDataRowStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle EmptyDataRowStyle
        {
            get
            {
                if (_emptyDataRowStyle == null)
                {
                    _emptyDataRowStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_emptyDataRowStyle).TrackViewState();
                    }
                }

                return _emptyDataRowStyle;
            }
        }

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the filter row in a <see cref="C1GridView"/> control.
        /// </summary>
        /// <remarks>
        /// This property is used to provide a custom style for the filter section of the column.
        /// </remarks>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.FilterStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle FilterStyle
        {
            get
            {
                if (_filterStyle == null)
                {
                    _filterStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_filterStyle).TrackViewState();
                    }
                }

                return _filterStyle;
            }
        }

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the footer row in a <see cref="C1GridView"/> control.
        /// </summary>
        /// <remarks>
        /// This property is used to provide a custom style for the footer section of the column.
        /// </remarks>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.FooterStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle FooterStyle
        {
            get
            {
                if (_footerStyle == null)
                {
                    _footerStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_footerStyle).TrackViewState();
                    }
                }

                return _footerStyle;
            }
        }

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the header row in a <see cref="C1GridView"/> control.
        /// </summary>
        /// <remarks>
        /// This property is used to provide a custom style for the header section of the column.
        /// </remarks>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.HeaderStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle HeaderStyle
        {
            get
            {
                if (_headerStyle == null)
                {
                    _headerStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_headerStyle).TrackViewState();
                    }
                }

                return _headerStyle;
            }
        }

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the data rows in a <see cref="C1GridView"/> control.
        /// </summary>
        /// <remarks>
        /// This property is used to provide a custom style for the items of the <see cref="C1GridView"/> control.
        /// </remarks>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.RowStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle RowStyle
        {
            get
            {
                if (_rowStyle == null)
                {
                    _rowStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_rowStyle).TrackViewState();
                    }
                }

                return _rowStyle;
            }
        }

        /// <summary>
        /// Gets a reference to the <see cref="TableItemStyle"/> object that enables you to set the appearance of the selected row in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Style")]
        [C1Description("C1Grid.SelectedRowStyle")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Styles)]
        public TableItemStyle SelectedRowStyle
        {
            get
            {
                if (_selectedRowStyle == null)
                {
                    _selectedRowStyle = new TableItemStyle();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_selectedRowStyle).TrackViewState();
                    }
                }

                return _selectedRowStyle;
            }
        }

        #endregion

        #region events

        /// <summary>
        /// Occurs in client editing mode when edits done by user should be persisted to underlying dataset.
        /// </summary>
        /// <remarks>
        /// Fires at the very beginning of the update procedure.
        /// You can perform any actions preceding update here, for example, establish database connectivity.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.BeginRowUpdate")]
        public event C1GridViewBeginRowUpdateEventHandler BeginRowUpdate
        {
            add { base.Events.AddHandler(C1GridView.EventBeginRowUpdate, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventBeginRowUpdate, value); }
        }

        /// <summary>
        /// Raises the <see cref="BeginRowUpdate"/> event.
        /// </summary>
        /// <param name="e">A <see cref="C1GridViewBeginRowUpdateEventArgs"/> that contains event data.</param>
        protected virtual void OnBeginRowUpdate(C1GridViewBeginRowUpdateEventArgs e)
        {
            C1GridViewBeginRowUpdateEventHandler handler = (C1GridViewBeginRowUpdateEventHandler)base.Events[C1GridView.EventBeginRowUpdate];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when a column has been grouped.
        /// </summary>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.ColumnGrouped")]
        public event C1GridViewColumnGroupedEventHandler ColumnGrouped
        {
            add { base.Events.AddHandler(C1GridView.EventColumnGrouped, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventColumnGrouped, value); }
        }

        /// <summary>
        /// Raises the <see cref="ColumnGrouped"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewColumnGroupedEventArgs"/> that contains event data.</param>
        protected virtual void OnColumnGrouped(C1GridViewColumnGroupedEventArgs args)
        {
            C1GridViewColumnGroupedEventHandler handler = (C1GridViewColumnGroupedEventHandler)base.Events[C1GridView.EventColumnGrouped];

            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "ColumnGrouped"));
                }
            }
        }

        /// <summary>
        /// Occurs when a column is grouped, but before the <see cref="C1GridView"/> handles the operation.
        /// </summary>
        /// <remarks>
        /// To cancel the action set the Cancel property of the <see cref="C1GridViewColumnGroupEventArgs"/> object to true.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.ColumnGrouping")]
        public event C1GridViewColumnGroupEventHandler ColumnGrouping
        {
            add { base.Events.AddHandler(C1GridView.EventColumnGrouping, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventColumnGrouping, value); }
        }

        /// <summary>
        /// Raises the <see cref="ColumnGrouping"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewColumnGroupEventArgs"/> that contains event data.</param>
        protected virtual void OnColumnGrouping(C1GridViewColumnGroupEventArgs args)
        {
            C1GridViewColumnGroupEventHandler handler = (C1GridViewColumnGroupEventHandler)base.Events[C1GridView.EventColumnGrouping];

            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a column has been moved.
        /// </summary>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.ColumnMoved")]
        public event C1GridViewColumnMovedEventHandler ColumnMoved
        {
            add { base.Events.AddHandler(C1GridView.EventColumnMoved, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventColumnMoved, value); }
        }

        /// <summary>
        /// Raises the <see cref="ColumnMoved"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewColumnMovedEventArgs"/> that contains event data.</param>
        protected virtual void OnColumnMoved(C1GridViewColumnMovedEventArgs args)
        {
            C1GridViewColumnMovedEventHandler handler = (C1GridViewColumnMovedEventHandler)base.Events[C1GridView.EventColumnMoved];

            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "ColumnMoved"));
                }
            }
        }

        /// <summary>
        /// Occurs when a column is moved, but before the <see cref="C1GridView"/> handles the operation.
        /// </summary>
        /// <remarks>
        /// To cancel the action set the Cancel property of the <see cref="C1GridViewColumnMoveEventArgs"/> object to true.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.ColumnMoving")]
        public event C1GridViewColumnMoveEventHandler ColumnMoving
        {
            add { base.Events.AddHandler(C1GridView.EventColumnMoving, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventColumnMoving, value); }
        }

        /// <summary>
        /// Raises the <see cref="ColumnMoving"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewColumnMoveEventArgs"/> that contains event data.</param>
        protected virtual void OnColumnMoving(C1GridViewColumnMoveEventArgs args)
        {
            C1GridViewColumnMoveEventHandler handler = (C1GridViewColumnMoveEventHandler)base.Events[C1GridView.EventColumnMoving];

            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a column has been ungrouped.
        /// </summary>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.ColumnUngrouped")]
        public event C1GridViewColumnUngroupedEventHandler ColumnUngrouped
        {
            add { base.Events.AddHandler(C1GridView.EventColumnUngrouped, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventColumnUngrouped, value); }
        }

        /// <summary>
        /// Raises the <see cref="ColumnGrouped"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewColumnGroupedEventArgs"/> that contains event data.</param>
        protected virtual void OnColumnUngrouped(C1GridViewColumnUngroupedEventArgs args)
        {
            C1GridViewColumnUngroupedEventHandler handler = (C1GridViewColumnUngroupedEventHandler)base.Events[C1GridView.EventColumnUngrouped];

            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "ColumnUngrouped"));
                }
            }
        }

        /// <summary>
        /// Occurs when a detail grid requires the <see cref="DataSource"/> property to be set.
        /// </summary>
        /// <remarks>
        /// You must implement this event if the detail grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.DetailRequiresDataSource")]
        public event C1GridViewDetailRequiresDataSourceEventHandler DetailRequiresDataSource
        {
            add { base.Events.AddHandler(C1GridView.EventDetailRequiresDataSource, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventDetailRequiresDataSource, value); }
        }

        /// <summary>
        /// Raises the <see cref="DetailRequiresDataSource"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewDetailRequiresDataSourceEventArgs"/> that contains event data.</param>
        protected virtual void OnDetailRequiresDataSource(C1GridViewDetailRequiresDataSourceEventArgs args)
        {
            C1GridViewDetailRequiresDataSourceEventHandler handler = (C1GridViewDetailRequiresDataSourceEventHandler)base.Events[C1GridView.EventDetailRequiresDataSource];

            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!args.Detail.IsBoundUsingDataSourceID)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "DetailRequiresDataSource"));
                }
            }
        }

        /// <summary>
        /// Occurs when a column is ungrouped, but before the <see cref="C1GridView"/> handles the operation.
        /// </summary>
        /// <remarks>
        /// To cancel the action set the Cancel property of the <see cref="C1GridViewColumnUngroupEventArgs"/> object to true.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.ColumnUngrouping")]
        public event C1GridViewColumnUngroupEventHandler ColumnUngrouping
        {
            add { base.Events.AddHandler(C1GridView.EventColumnUngrouping, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventColumnUngrouping, value); }
        }

        /// <summary>
        /// Raises the <see cref="ColumnUngrouping"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewColumnUngroupEventArgs"/> that contains event data.</param>
        protected virtual void OnColumnUngrouping(C1GridViewColumnUngroupEventArgs args)
        {
            C1GridViewColumnUngroupEventHandler handler = (C1GridViewColumnUngroupEventHandler)base.Events[C1GridView.EventColumnUngrouping];

            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs in client editing mode when edits done by user should be persisted to underlying dataset.
        /// </summary>
        /// <remarks>
        /// Fires at the very end of the update procedure.
        /// You can perform finalization steps of update procedure here, for example, close database connection.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.EndRowUpdated")]
        public event C1GridViewEndRowUpdatedEventHandler EndRowUpdated
        {
            add { base.Events.AddHandler(C1GridView.EventEndRowUpdated, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventEndRowUpdated, value); }
        }

        /// <summary>
        /// Raises the <see cref="EndRowUpdated"/> event.
        /// </summary>
        /// <param name="e">A <see cref="C1GridViewEndRowUpdatedEventArgs"/> that contains event data.</param>
        protected virtual void OnEndRowUpdated(C1GridViewEndRowUpdatedEventArgs e)
        {
            C1GridViewEndRowUpdatedEventHandler handler = (C1GridViewEndRowUpdatedEventHandler)base.Events[C1GridView.EventEndRowUpdated];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs after filter expression is applied to the underlying <see cref="DataView"/>'s <see cref="DataView.RowFilter"/> property.
        /// </summary>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.Filtered")]
        public event EventHandler Filtered
        {
            add { base.Events.AddHandler(C1GridView.EventFiltered, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventFiltered, value); }
        }

        /// <summary>
        /// Raises the <see cref="Filtered"/> event.
        /// </summary>
        /// <param name="args">A <see cref="EventArgs"/> that contains event data.</param>
        protected virtual void OnFiltered(EventArgs args)
        {
            EventHandler handler = (EventHandler)base.Events[C1GridView.EventFiltered];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a hierarchy row is toggled, but before the <see cref="C1GridView"/> handles the operation.
        /// </summary>
        /// <remarks>
        /// To cancel the action set the Cancel property of the <see cref="C1GridViewHierarchyRowTogglingEventHandler"/> object to true.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.HierarchyRowToggling")]
        public event C1GridViewHierarchyRowTogglingEventHandler HierarchyRowToggling
        {
            add { base.Events.AddHandler(C1GridView.EventHierarchyRowToggling , value); }
            remove { base.Events.RemoveHandler(C1GridView.EventHierarchyRowToggling, value); }
        }

        /// <summary>
        /// Raises the <see cref="HierarchyRowToggling"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewHierarchyRowTogglingEventArgs"/> that contains event data.</param>
        protected virtual void OnHierarchyRowToggling(C1GridViewHierarchyRowTogglingEventArgs args)
        {
            C1GridViewHierarchyRowTogglingEventHandler handler = (C1GridViewHierarchyRowTogglingEventHandler)base.Events[C1GridView.EventHierarchyRowToggling];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a hierarchy row has been toggled.
        /// </summary>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.HierarchyRowToggled")]
        public event C1GridViewHierarchyRowToggledEventHandler HierarchyRowToggled
        {
            add { base.Events.AddHandler(C1GridView.EventHierarchyRowToggled, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventHierarchyRowToggled, value); }
        }

        /// <summary>
        /// Raises the <see cref="C1GridViewHierarchyRowToggledEventArgs"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewHierarchyRowToggledEventArgs"/> that contains event data.</param>
        protected virtual void OnHierarchyRowToggled(C1GridViewHierarchyRowToggledEventArgs args)
        {
            C1GridViewHierarchyRowToggledEventHandler handler = (C1GridViewHierarchyRowToggledEventHandler)base.Events[C1GridView.EventHierarchyRowToggled];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "HierarchyRowToggled"));
                }
            }
        }

        /// <summary>
        /// Occurs when the preparation for filtering is started but before the <see cref="C1GridView"/>
        /// instance handles the filter operation.
        /// </summary>
        /// <remarks>
        /// To stop the operation set the Cancel property of the <see cref="C1GridViewFilterEventArgs"/> object to true.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.Filtering")]
        public event C1GridViewFilterEventHandler Filtering
        {
            add { base.Events.AddHandler(C1GridView.EventFiltering, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventFiltering, value); }
        }

        /// <summary>
        /// Raises the <see cref="Filtering"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewFilterEventArgs"/> that contains event data.</param>
        protected virtual void OnFiltering(C1GridViewFilterEventArgs args)
        {
            C1GridViewFilterEventHandler handler = (C1GridViewFilterEventHandler)base.Events[C1GridView.EventFiltering];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID && !args.Cancel)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "Filtering"));
                }
            }
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked, but after the <see cref="C1GridView"/> control handles the paging operation.
        /// </summary>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.PageIndexChanged")]
        public event EventHandler PageIndexChanged
        {
            add { base.Events.AddHandler(C1GridView.EventPageIndexChanged, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventPageIndexChanged, value); }
        }

        /// <summary>
        /// Raises the <see cref="PageIndexChanged"/> event.
        /// </summary>
        /// <param name="args">A <see cref="EventArgs"/> that contains event data.</param>
        protected virtual void OnPageIndexChanged(EventArgs args)
        {
            EventHandler handler = (EventHandler)base.Events[C1GridView.EventPageIndexChanged];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked, but before the <see cref="C1GridView"/> control handles the paging operation.
        /// </summary>
        /// <remarks>
        /// To cancel the operation set the Cancel property of the <see cref="C1GridViewPageEventArgs"/> object to true.
        /// </remarks>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.PageIndexChanging")]
        public event C1GridViewPageEventHandler PageIndexChanging
        {
            add { base.Events.AddHandler(C1GridView.EventPageIndexChanging, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventPageIndexChanging, value); }
        }

        /// <summary>
        /// Raises the <see cref="PageIndexChanging"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewPageEventArgs"/> that contains event data.</param>
        protected virtual void OnPageIndexChanging(C1GridViewPageEventArgs args)
        {
            C1GridViewPageEventHandler handler = (C1GridViewPageEventHandler)base.Events[C1GridView.EventPageIndexChanging];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID && !args.Cancel)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "PageIndexChanging"));
                }
            }
        }

        /// <summary>
        /// Occurs when the Cancel button of a item in edit mode is clicked, but before the item exits edit mode.
        /// </summary>
        /// <remarks>
        /// To stop the operation set the Cancel property of the <see cref="C1GridViewCancelEditEventArgs"/> object to true.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowCancelingEdit")]
        public event C1GridViewCancelEditEventHandler RowCancelingEdit
        {
            add { base.Events.AddHandler(C1GridView.EventRowCancelingEdit, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowCancelingEdit, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowCancelingEdit"/> event.
        /// </summary>
        /// <param name="e">A <see cref="C1GridViewCancelEditEventArgs"/> that contains event data.</param>
        protected virtual void OnRowCancelingEdit(C1GridViewCancelEditEventArgs e)
        {
            C1GridViewCancelEditEventHandler handler = (C1GridViewCancelEditEventHandler)base.Events[C1GridView.EventRowCancelingEdit];
            if (handler != null)
            {
                handler(this, e);
            }
            else
            {
                if (!IsBoundUsingDataSourceID)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "RowCancelingEdit"));
                }
            }
        }

        /// <summary>
        /// Occurs when a button is clicked in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowCommand")]
        public event C1GridViewCommandEventHandler RowCommand
        {
            add { base.Events.AddHandler(C1GridView.EventRowCommand, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowCommand, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowCommand"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewCommandEventArgs"/> that contains event data.</param>
        protected virtual void OnRowCommand(C1GridViewCommandEventArgs args)
        {
            C1GridViewCommandEventHandler handler = (C1GridViewCommandEventHandler)Events[C1GridView.EventRowCommand];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a row is created in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.RowCreated")]
        public event C1GridViewRowEventHandler RowCreated
        {
            add { base.Events.AddHandler(C1GridView.EventRowCreated, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowCreated, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowCreated"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewRowEventArgs"/> that contains event data.</param>
        protected virtual void OnRowCreated(C1GridViewRowEventArgs args)
        {
            C1GridViewRowEventHandler handler = (C1GridViewRowEventHandler)Events[C1GridView.EventRowCreated];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a row is bound to data in a <see cref="C1GridView"/> control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Grid.RowDataBound")]
        public event C1GridViewRowEventHandler RowDataBound
        {
            add { base.Events.AddHandler(C1GridView.EventRowDataBound, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowDataBound, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowDataBound"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewRowEventArgs"/> that contains event data.</param>
        protected virtual void OnRowDataBound(C1GridViewRowEventArgs args)
        {
            C1GridViewRowEventHandler handler = (C1GridViewRowEventHandler)base.Events[C1GridView.EventRowDataBound];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when the Delete button is clicked, but before the <see cref="C1GridView"/> instance handles the delete operation.
        /// </summary>
        /// <remarks>
        /// To cancel the operation set the Cancel property of the <see cref="C1GridViewDeleteEventArgs"/> object to true.
        /// </remarks>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowDeleting")]
        public event C1GridViewDeleteEventHandler RowDeleting
        {
            add { base.Events.AddHandler(C1GridView.EventRowDeleting, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowDeleting, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowDeleting"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewDeleteEventArgs"/> that contains event data.</param>
        protected virtual void OnRowDeleting(C1GridViewDeleteEventArgs args)
        {
            C1GridViewDeleteEventHandler handler = (C1GridViewDeleteEventHandler)base.Events[C1GridView.EventRowDeleting];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID && !args.Cancel)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "RowDeleting"));
                }
            }
        }

        /// <summary>
        /// Occurs when the Delete button is clicked.
        /// </summary>
        /// <remarks>
        /// The event occurs only if the grid is bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowDeleted")]
        public event C1GridViewDeletedEventHandler RowDeleted
        {
            add { base.Events.AddHandler(C1GridView.EventRowDeleted, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowDeleted, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowDeleted"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewDeletedEventArgs"/> that contains event data.</param>
        protected virtual void OnRowDeleted(C1GridViewDeletedEventArgs args)
        {
            C1GridViewDeletedEventHandler handler = (C1GridViewDeletedEventHandler)base.Events[C1GridView.EventRowDeleted];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when the Edit button is clicked, but before the <see cref="C1GridView"/> instance handles the operation.
        /// </summary>
        /// <remarks>
        /// To cancel the operation set the Cancel property of the <see cref="C1GridViewEditEventArgs"/> object to true.
        /// </remarks>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowEditing")]
        public event C1GridViewEditEventHandler RowEditing
        {
            add { base.Events.AddHandler(C1GridView.EventRowEditing, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowEditing, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowEditing"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewEditEventArgs"/> that contains event data.</param>
        protected virtual void OnRowEditing(C1GridViewEditEventArgs args)
        {
            C1GridViewEditEventHandler handler = (C1GridViewEditEventHandler)base.Events[C1GridView.EventRowEditing];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID && !args.Cancel)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "RowEditing"));
                }
            }
        }

        /// <summary>
        /// Occurs when a row's Update button is clicked or when edits done by user in a client-side editing mode should be persisted to underlying dataset, but before the <see cref="C1GridView"/> control handles the update operation.
        /// </summary>
        /// <remarks>
        /// To cancel the update operation set the Cancel property of the <see cref="C1GridViewUpdateEventArgs"/> object to true.
        /// </remarks>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowUpdating")]
        public event C1GridViewUpdateEventHandler RowUpdating
        {
            add { base.Events.AddHandler(C1GridView.EventRowUpdating, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowUpdating, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowUpdating"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewUpdateEventArgs"/> that contains event data.</param>
        protected virtual void OnRowUpdating(C1GridViewUpdateEventArgs args)
        {
            C1GridViewUpdateEventHandler handler = (C1GridViewUpdateEventHandler)base.Events[C1GridView.EventRowUpdating];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID && !args.Cancel)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "RowUpdating"));
                }
            }
        }

        /// <summary>
        /// Occurs when a row's Update button is clicked or when edits done by user in a client-side editing mode should be persisted to underlying dataset, but after the <see cref="C1GridView"/> control handles the update operation.
        /// </summary>
        /// <remarks>
        /// The event occurs only if the grid is bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.RowUpdated")]
        public event C1GridViewUpdatedEventHandler RowUpdated
        {
            add { base.Events.AddHandler(C1GridView.EventRowUpdated, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventRowUpdated, value); }
        }

        /// <summary>
        /// Raises the <see cref="RowUpdated"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewUpdatedEventArgs"/> that contains event data.</param>
        protected virtual void OnRowUpdated(C1GridViewUpdatedEventArgs args)
        {
            C1GridViewUpdatedEventHandler handler = (C1GridViewUpdatedEventHandler)base.Events[C1GridView.EventRowUpdated];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a row's Select button is clicked, but after the <see cref="C1GridView"/> control handles the select operation.
        /// </summary>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.SelectedIndexChanged")]
        public event EventHandler SelectedIndexChanged
        {
            add { base.Events.AddHandler(C1GridView.EventSelectedIndexChanged, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventSelectedIndexChanged, value); }
        }

        /// <summary>
        /// Raises the <see cref="SelectedIndexChanged"/> event.
        /// </summary>
        /// <param name="args">A <see cref="EventArgs"/> that contains event data.</param>
        protected virtual void OnSelectedIndexChanged(EventArgs args)
        {
            if (DataKeyNamesInternal.Length > 0)
            {
                SelectedPersistedDataKey = SelectedDataKey;
            }

            EventHandler handler = (EventHandler)base.Events[C1GridView.EventSelectedIndexChanged];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when a row's Select button is clicked, but before the <see cref="C1GridView"/> control handles the select operation.
        /// </summary>
        /// <remarks>
        /// To cancel the operation set the Cancel property of the <see cref="C1GridViewSelectEventArgs"/> object to true.
        /// </remarks>
        /*
		/// <remarks>
		/// You must implement this event if the grid is not bound to a DataSource control.
		/// </remarks>*/
        [C1Category("Category.Action")]
        [C1Description("C1Grid.SelectedIndexChanging")]
        public event C1GridViewSelectEventHandler SelectedIndexChanging
        {
            add { base.Events.AddHandler(C1GridView.EventSelectedIndexChanging, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventSelectedIndexChanging, value); }
        }

        /// <summary>
        /// Raises the <see cref="SelectedIndexChanging"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewSelectEventArgs"/> that contains event data.</param>
        protected virtual void OnSelectedIndexChanging(C1GridViewSelectEventArgs args)
        {
            C1GridViewSelectEventHandler handler = (C1GridViewSelectEventHandler)base.Events[C1GridView.EventSelectedIndexChanging];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked, but after the <see cref="C1GridView"/> control handles the sort operation.
        /// </summary>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.Sorted")]
        public event EventHandler Sorted
        {
            add { base.Events.AddHandler(C1GridView.EventSorted, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventSorted, value); }
        }

        /// <summary>
        /// Raises the <see cref="Sorted"/> event.
        /// </summary>
        /// <param name="args">A <see cref="EventArgs"/> that contains event data.</param>
        protected virtual void OnSorted(EventArgs args)
        {
            EventHandler handler = (EventHandler)base.Events[C1GridView.EventSorted];
            if (handler != null)
            {
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked, but before the <see cref="GridView"/>  control handles the sort operation.
        /// </summary>
        /// <remarks>
        /// To cancel the operation set the Cancel property of the <see cref="C1GridViewSortEventArgs"/> object to true.
        /// </remarks>
        /// <remarks>
        /// You must implement this event if the grid is not bound to a DataSource control.
        /// </remarks>
        [C1Category("Category.Action")]
        [C1Description("C1Grid.Sorting")]
        public event C1GridViewSortEventHandler Sorting
        {
            add { base.Events.AddHandler(C1GridView.EventSorting, value); }
            remove { base.Events.RemoveHandler(C1GridView.EventSorting, value); }
        }

        /// <summary>
        /// Raises the <see cref="Sorting"/> event.
        /// </summary>
        /// <param name="args">A <see cref="C1GridViewSortEventArgs"/> that contains event data.</param>
        protected virtual void OnSorting(C1GridViewSortEventArgs args)
        {
            C1GridViewSortEventHandler handler = (C1GridViewSortEventHandler)base.Events[C1GridView.EventSorting];
            if (handler != null)
            {
                handler(this, args);
            }
            else
            {
                if (!IsBoundUsingDataSourceID && !args.Cancel)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EEventNotHandled"), ID, "Sorting"));
                }
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Resets the virtual scrolling state of the grid view.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The scroll state stores the client scroll position if the virtual scrolling is enabled.
        /// When the data source is changed on postback, if the new data source items count does not 
        /// cover the scroll position, there's no data row be rendered. It may cause the data row 
        /// does not align with the column header.
        /// </para><para>
        /// Call this method to reset the scroll state, to force the grid view renders the first data row.
        /// </para>
        /// </remarks>
        public void ResetScrollState()
        {
            _scrollingState.Reset();
        }

        /// <summary>
        /// Saves end user changes in the data source.
        /// </summary>
        public void Update()
        {
            _update(EditIndex);
        }

        private void _update(int idx)
        {
            if (idx < 0 || idx > Rows.Count)
            {
                throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ERowIndexNotFound"), ID));
            }

            C1GridViewRow item = Rows[idx];

            IList dataKeysArray = DataKeysInternal;

            if (dataKeysArray == null || idx >= dataKeysArray.Count)
            {
                throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ERowKeyValueNotFound"), ID));
            }

            PerformSelect();

            if (_c1ds == null || _c1ds.DataSource == null)
            {
                throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EInvalidDataSource"), ID));
            }

            DataSourceHelper dsHelper = new DataSourceHelper(_c1ds.DataSource);

            if (!dsHelper.SetKeyValue(DataKeyNamesInternal, (DataKey)dataKeysArray[idx]))
            {
                throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ERowIndexNotFoundInDataSource"), ID));
            }

            TableCellCollection cells = item.Cells;
            for (int col = 0; col < _leaves.Count; col++)
            {
                C1Field field = _leaves[col] as C1Field;
                if ((field == null)/* || (column != null && column.Grouped)*/)
                {
                    continue;
                }

                TableCell cell = item.Cells[col];
                C1TemplateField templateField = field as C1TemplateField;
                if (templateField != null)
                {
                    C1GridViewUpdateBindingCollection updateBindings = templateField.UpdateBindings;
                    foreach (C1GridViewUpdateBinding binding in updateBindings)
                    {
                        if (string.IsNullOrEmpty(binding.UpdateField))
                        {
                            continue;
                        }

                        int pos = binding.ControlProperty.IndexOf('.');
                        if (pos <= 0 || pos >= binding.ControlProperty.Length - 1)
                        {
                            throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EInvalidUpdateBindingControlProperty"), ID, binding.ControlProperty));
                        }

                        string ctrlName = binding.ControlProperty.Substring(0, pos);
                        string ctrlProp = binding.ControlProperty.Substring(pos + 1);
                        Control updateCtrl = cell.FindControl(ctrlName);

                        if (updateCtrl == null && cell.Controls.Count > 0 && cell.Controls[0] is UserControl)
                        {
                            updateCtrl = cell.Controls[0].FindControl(ctrlName);
                        }

                        if (updateCtrl == null)
                        {
                            throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EControlNotFound"), ID, ctrlName));
                        }

                        object propVal = DataBinder.Eval(updateCtrl, ctrlProp);
                        try
                        {
                            dsHelper.SetFieldValue(binding.UpdateField, propVal);
                        }
                        catch (Exception ex)
                        {
                            throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EErrorSettingField"), ID, binding.UpdateField), ex);
                        }
                    }
                }

                C1BoundField boundCol = field as C1BoundField;
                if (boundCol != null && !boundCol.ReadOnly)
                {
                    string dataField = boundCol.DataField;
                    if (!string.IsNullOrEmpty(dataField))
                    {
                        foreach (Control ctrl in cell.Controls)
                        {
                            TextBox txtBox = ctrl as TextBox;
                            if (txtBox != null)
                            {
                                string text = txtBox.Text;

                                IDictionary valueList = boundCol.ValueList;
                                if (valueList != null)
                                {
                                    bool found = false;
                                    for (int ignoreCase = 0; !found && ignoreCase < 2; ignoreCase++)
                                    {
                                        foreach (DictionaryEntry entry in valueList)
                                        {
                                            if (String.Compare(entry.Value.ToString(), text, ignoreCase == 1) == 0)
                                            {
                                                text = entry.Key.ToString();
                                                found = true;
                                                break;
                                            }
                                        }
                                    }

                                    if (!found)
                                    {
                                        text = string.Empty;
                                    }
                                }
                                try
                                {
                                    dsHelper.SetFieldValue(dataField, text);
                                }
                                catch (Exception ex)
                                {
                                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EErrorSettingField"), ID, dataField), ex);
                                }
                                break;
                            }
                        }
                    }
                }
            }

            dsHelper.Update();
        }

        #endregion

        #region overrides, protected

        /// <summary>
        /// Registers an OnSubmit statement in order to save the states of the widget
        /// to json hidden input.
        /// </summary>
        protected override void RegisterOnSubmitStatement()
        {
            this.RegisterOnSubmitStatement("onwijstatesaving");
        }

        /// <summary>
        /// Gets the HTML tag that is used to render the <see cref="C1GridView"/> control.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return DesignMode
                    ? HtmlTextWriterTag.Div
                    : HtmlTextWriterTag.Table;
            }
        }

        /// <summary>
        /// Creates the control hierarchy used to render the <see cref="GridView"/> control.
        /// </summary>
        /// <param name="dataSource">An <see cref="IEnumerable"/> that contains the data source for the <see cref="C1GridView"/> control.</param>
        /// <param name="dataBinding">True to indicate that the child controls are bound to data; otherwise, false.</param>
        /// <returns></returns>
        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            EnsureDataSource(dataSource, dataBinding);

            CreateLeaves(_c1ds, true);

            ICollection collection = _c1ds.DataSource as ICollection;

            ArrayList dataKeysArrayList = this.DataKeysInternal;

            int capacity = 0;
            if (dataBinding)
            {
                dataKeysArrayList.Clear();
                if (((_c1ds.DataSource != null) && (collection == null)) && (!AllowCustomPaging && _c1ds.IsPagingEnabled && !_c1ds.IsServerPagingEnabled))
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EServerPagingNotSupported"), ID));
                }
            }
            else
            {
                if (collection == null)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EDataSourceMustBeCollectionWhenNotDataBinding"), ID));
                }
            }

            _pageCount = 0;

            //List<C1BaseField> columns = new List<C1BaseField>(); // empty list

            if (_c1ds.DataSource != null)
            {
                if (_c1ds.IsPagingEnabled)
                {
                    int pageCount = _c1ds.PageCount;
                    if (_c1ds.CurrentPageIndex >= pageCount)
                    {
                        int lastPageIndex = pageCount - 1;
                        _pageIndex = lastPageIndex;
                        _c1ds.CurrentPageIndex = lastPageIndex;
                    }
                }

                //columns = this.CreateLeaves(dataBinding ? _c1ds : null, dataBinding);

                if (collection != null)
                {
                    capacity = _c1ds.IsPagingEnabled
                        ? _c1ds.PageSize
                        : collection.Count;

                    if (dataBinding)
                    {
                        dataKeysArrayList.Capacity = capacity;
                    }

                    _pageCount = _c1ds.DataSourceCount == 0
                        ? 0
                        : _c1ds.PageCount;
                }
            }

            this._rowsInternal = new ArrayList(capacity);
            this._detailGrids = null;
            this._rows = null;
            this._dataKeys = null;
            _bodyRowsCount = 0;
            //ClearDataKeys();

            Table child = this.CreateChildTable();
            this.Controls.Add(child);
            TableRowCollection rows = child.Rows;

            /*if (dataSource == null)
			{
				if ((this.EmptyDataTemplate != null) || (this.EmptyDataText.Length > 0))
				{
					CreateRow(-1, -1, C1GridViewRowType.EmptyDataRow, C1GridViewRowState.Normal, dataBinding, null, _leaves, rows, null);
				}
				else
				{
					this.Controls.Clear();
				}

				return 0;
			}*/

            // initialize columns (except non-empty bands, they are not contained in the _leaves collection)
            if (_leaves.Count > 0)
            {
                for (int i = 0, len = _leaves.Count; i < len; i++)
                {
                    _leaves[i].Initialize(this);
                    /*if (this.DetermineRenderClientScript())
					{
						columns[i].ValidateSupportsCallback();
					}*/
                }
            }

            // initialize non-empty bands
            if (_userColumns != null)
            {
                foreach (C1BaseField field in _userColumns.Traverse())
                {
                    C1Band band = field as C1Band;
                    if (band != null && !band.IsLeaf)
                    {
                        band.Initialize(this);
                    }
                }
            }

            IEnumerator enumerator = null;

            int dataRowsCount = 0;
            int dataSourceIndex = 0;
            int detailRowIndex = -100; // negative values
            bool isPagingEnabled = _c1ds.IsPagingEnabled;
            int editIndex = this.EditIndex;
            int selectedIndex = this.SelectedIndex;

            if (_leaves.Count > 0)
            {
                if (_c1ds.IsPagingEnabled)
                {
                    dataSourceIndex = _c1ds.FirstIndexInPage;
                }

                if (DesignMode && (isPagingEnabled && this.PagerSettings.Visible) && this._pagerSettings.IsPagerOnTop)
                {
                    InitializePager(true);
                }

                if (DesignMode && this.ShowGroupArea)
                {
                    InitializeGroupArea();
                }

                CreateHeaderRows(dataBinding, _leaves, rows);

                _filterRow = CreateRow(-1, -1, -1, C1GridViewRowType.Filter, C1GridViewRowState.Normal, false, null, _leaves, rows, null);

                if (!this.ShowFilter)
                {
                    _filterRow.Visible = false;
                }

                string[] dataKeyNamesInternal = this.DataKeyNamesInternal;
                bool storeDataKeys = dataBinding && (dataKeyNamesInternal.Length != 0) && !DesignMode;
                bool hasDetails = HasDetails();
                C1GridViewDataRow row;
                C1GridViewRowType rowType = C1GridViewRowType.DataRow;
                C1GridViewRowState rowState;

                if (_c1ds.DataSource != null)
                {
                    enumerator = _c1ds.PagedDataSource.GetEnumerator();
                }

                if (enumerator != null)
                {
                    while (enumerator.MoveNext())
                    {
                        object dataItem = enumerator.Current;
                        if (storeDataKeys)
                        {
                            OrderedDictionary keyTable = new OrderedDictionary(dataKeyNamesInternal.Length);
                            foreach (string dataKeyName in dataKeyNamesInternal)
                            {
                                keyTable.Add(dataKeyName, DataBinder.GetPropertyValue(dataItem, dataKeyName));
                            }

                            dataKeysArrayList.Add(new DataKey(keyTable, dataKeyNamesInternal));
                        }

                        rowState = C1GridViewRowState.Normal;

                        if (dataRowsCount == editIndex)
                        {
                            rowState |= C1GridViewRowState.Edit;
                        }

                        if (dataRowsCount == selectedIndex)
                        {
                            rowState |= C1GridViewRowState.Selected;
                        }

                        if ((dataRowsCount % 2) != 0)
                        {
                            rowState |= C1GridViewRowState.Alternate;
                        }

                        row = (C1GridViewDataRow)this.CreateRow(dataRowsCount, dataSourceIndex, dataRowsCount, rowType, rowState, dataBinding, dataItem, _leaves, rows, this._rowsInternal);

                        if (hasDetails && row.Expanded)
                        {
                            C1GridViewDetailRow detailRow = (C1GridViewDetailRow)this.CreateRow(detailRowIndex--, -1, dataRowsCount, C1GridViewRowType.DetailRow, C1GridViewRowState.Normal, dataBinding, dataItem, _leaves, rows, null);
                            row.DetailRow = detailRow;
                        }

                        dataRowsCount++;
                        dataSourceIndex++;
                    }
                }

                if (dataRowsCount == 0)
                {
                    this.CreateRow(-1, -1, -1, C1GridViewRowType.EmptyDataRow, C1GridViewRowState.Normal, dataBinding, null, _leaves, rows, null);
                }

                // create footer row
                _footerRow = CreateRow(-1, -1, -1, C1GridViewRowType.Footer, C1GridViewRowState.Normal, false, null, _leaves, rows, null);

                if (!ShowFooter)
                {
                    _footerRow.Visible = false;
                }

                if (DesignMode && (isPagingEnabled && this.PagerSettings.Visible) && this._pagerSettings.IsPagerOnBottom)
                {
                    InitializePager(false);
                }
            }

            int dataSourceCount = -1;

            if (dataBinding)
            {
                if (enumerator != null)
                {
                    if (_c1ds.IsPagingEnabled)
                    {
                        _pageCount = _c1ds.PageCount;
                        dataSourceCount = _c1ds.DataSourceCount;
                    }
                    else
                    {
                        _pageCount = 1;
                        dataSourceCount = dataRowsCount;
                    }
                }
                else
                {
                    _pageCount = 0;
                }
            }

            // hide pager rows in case of single page
            if (this.PageCount == 1)
            {
                /*if (_topPagerRow != null)
				{
					_topPagerRow.Visible = false;
				}

				if (_bottomPagerRow != null)
				{
					_bottomPagerRow.Visible = false;
				}*/
            }

            ChildControlsCreated = true;

            return dataSourceCount;
        }

        /// <summary>
        /// Creates the <see cref="DataSourceSelectArguments"/> object that contains the arguments that get passed to the data source for processing.
        /// </summary>
        /// <returns>A <see cref="DataSourceSelectArguments"/> that contains the arguments that get passed to the data source.</returns>
        protected override DataSourceSelectArguments CreateDataSourceSelectArguments()
        {
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataSourceView data = GetData();

            if (AllowAutoSort /*&& AllowSorting*/ && data.CanSort)
            {
                args.SortExpression = BuildSortExpression();
            }

            if (AllowPaging && data.CanPage)
            {
                args.StartRowIndex = this.PageSize * this.PageIndex;

                if (data.CanRetrieveTotalRowCount)
                {
                    args.RetrieveTotalRowCount = true;
                    args.MaximumRows = this.PageSize;
                }
                else
                {
                    args.MaximumRows = -1;
                }
            }

            return args;
        }

        /// <summary>
        /// Creates a <see cref="C1GridViewRow"/> object using the specified parameters.
        /// </summary>
        /// <param name="rowIndex">The index of the row to create.</param>
        /// <param name="dataSourceIndex">The index of the data source item to bind to the row.</param>
        /// <param name="rowType">One of the <see cref="C1GridViewRowType"/> values.</param>
        /// <param name="rowState">One of the <see cref="C1GridViewRowState"/> values. </param>
        /// <returns>A <see cref="GridViewRow"/> created using the specified parameters.</returns>
        protected virtual C1GridViewRow CreateRow(int rowIndex, int dataSourceIndex, C1GridViewRowType rowType, C1GridViewRowState rowState)
        {
            C1GridViewRow row;

            switch (rowType)
            {
                case C1GridViewRowType.DataRow:
                    row = new C1GridViewDataRow(rowIndex, dataSourceIndex, rowType, rowState);
                    break;

                case C1GridViewRowType.DetailRow:
                    row = new C1GridViewDetailRow(rowIndex);
                    break;

                default:
                    row = new C1GridViewRow(rowIndex, dataSourceIndex, rowType, rowState);
                    break;

            }

            row.Owner = this;

            return row;
        }

        /// <summary>
        /// Retrieves the values of each field declared within the specified row and stores them in the specified <see cref="IOrderedDictionary"/>  object.
        /// </summary>
        /// <param name="fieldValues">An <see cref="IOrderedDictionary"/> used to store the field values.</param>
        /// <param name="row">The <see cref="C1GridViewRow"/> from which to retrieve the field values.</param>
        /// <param name="includeReadOnlyFields">true to include read-only fields; otherwise, false.</param>
        /// <param name="includePrimaryKey">true to include the primary key field or fields; otherwise, false.</param>
        protected virtual void ExtractRowValues(IOrderedDictionary fieldValues, C1GridViewRow row, bool includeReadOnlyFields, bool includePrimaryKey)
        {
            if (fieldValues != null)
            {
                string[] dataKeyNames = DataKeyNamesInternal;

                List<C1BaseField> columns = _leaves;// CreateLeaves(null, false);

                for (int i = 0, len = columns.Count, cellsLen = row.Cells.Count; i < len && i < cellsLen; i++)
                {
                    C1BaseField column = columns[i];

                    if (column.ParentVisibility)
                    {
                        IOrderedDictionary values = new OrderedDictionary();
                        column.ExtractValuesFromCell(values, (C1GridViewCell)row.Cells[i], row.RowState, includeReadOnlyFields);

                        foreach (DictionaryEntry entry in values)
                        {
                            if (includePrimaryKey || (Array.IndexOf(dataKeyNames, entry.Key) < 0))
                            {
                                fieldValues[entry.Key] = entry.Value;

                                //TODO: check
                                /*if ((entry.Value == DBNull.Value || entry.Value == null) && (col != null && col.Grouped))
								{
									values[entry.Key] = EditRowValues[entry.Key];
								}*/
                            }
                        }
                    }
                }
            }
        }

        private IOrderedDictionary GetC1InplaceEditorsMarkup(C1GridViewRow editRow)
        {
            IOrderedDictionary result = new OrderedDictionary();

            if (editRow != null)
            {
                foreach (TableCell cell in editRow.Cells)
                {
                    C1GridViewCell gridCell = cell as C1GridViewCell;
                    if (gridCell != null)
                    {
                        C1BoundField boundField = gridCell.ContainingField as C1BoundField;
                        if (boundField != null)
                        {
                            boundField.GetC1InplaceEditorMarkupFromCell(result, gridCell);
                        }
                    }
                }
            }

            return result;
        }

        private void InitializeDetailRow(C1GridViewDetailRow detailRow, List<C1BaseField> leaves)
        {
            C1GridViewDataRow dataRow = detailRow.DataRow;
            HierararchyState state = HierarchyTree.ContainsKey(dataRow.DisplayDataItemIndex)
                ? HierarchyTree[dataRow.DisplayDataItemIndex]
                : null;

            detailRow.ID = "d" + dataRow.ID; // provide an unique ID to avoid autogeneration of the "_ctlNN" suffixes for the detail grids (#111571).

            C1DetailGridView detailGrid = new C1DetailGridView(Root == null ? this : Root, detailRow, this.FirstDetailItemInternal, state, dataRow.DisplayDataItemIndex);

            detailGrid.EnableViewState = false; // Do not save view state. When originally expanded detail grid is collapsed via callback then on next callback or postback the page will try to load view state of the nonexistent internal controls and hangs up (94844, 94850).

            C1GridViewCell detailCell = new C1GridViewCell(null);

            detailCell.Width = Unit.Percentage(100); //#109693

            detailCell.Controls.Add(detailGrid);
            detailRow.Cells.Add(detailCell);

            Details.Add(detailGrid);

            if (!detailGrid.IsBoundUsingDataSourceID)
            {
                OnDetailRequiresDataSource(new C1GridViewDetailRequiresDataSourceEventArgs(detailGrid));
                detailGrid.DataBind();
            }
        }

        private void InitializeEmptyDataRow(C1GridViewRow row, List<C1BaseField> leaves)
        {
            if (EmptyDataTemplate != null || !string.IsNullOrEmpty(EmptyDataText))
            {
                C1GridViewCell emptyDataCell = new C1GridViewCell(null);

                /* emptyDataCell.ColumnSpan = leaves.Count;  */
                // wijgrid doesn't handle spanned cells

                if (EmptyDataTemplate != null)
                {
                    EmptyDataTemplate.InstantiateIn(emptyDataCell);
                }
                else
                {
                    emptyDataCell.Text = EmptyDataText;
                }

                row.Cells.Add(emptyDataCell);
            }
        }

        /// <summary>
        /// Initializes a row in the <see cref="C1GridView"/> control.
        /// </summary>
        /// <param name="row">A <see cref="GridViewRow"/> that represents the row to initialize.</param>
        /// <param name="leaves">An collection of <see cref="C1BaseField"/> objects that represent the column fields in the <see cref="C1GridView"/> control.</param>
        protected virtual void InitializeRow(C1GridViewRow row, List<C1BaseField> leaves)
        {
            bool useAccessiableHeader = false;
            string rowHeaderColumn = RowHeaderColumn.ToLower();

            if ((row.RowType == C1GridViewRowType.DataRow) && HasDetails())
            {
                row.ID = HierarchyPath.MakeID(row);
            }

            if (row.RowType == C1GridViewRowType.Header)
            {
                useAccessiableHeader = UseAccessibleHeader;
            }

            C1GridViewCell cell;

            TableCellCollection cells = row.Cells;
            for (int i = 0, len = leaves.Count; i < len; i++)
            {
                C1BaseField field = leaves[i];

                if (row.RowType == C1GridViewRowType.Header)
                {
                    cell = new C1GridViewHeaderCell(field);
                    ((C1GridViewHeaderCell)cell).Scope = TableHeaderScope.Column;
                    ((C1GridViewHeaderCell)cell).AbbreviatedText = leaves[i].AccessibleHeaderText;
                }
                else
                {
                    C1BoundField boundField = field as C1BoundField;
                    if (boundField != null && !string.IsNullOrEmpty(rowHeaderColumn) && (string.Compare(rowHeaderColumn, boundField.DataField, true) == 0))
                    {
                        cell = new C1GridViewHeaderCell(field);
                        ((C1GridViewHeaderCell)cell).Scope = TableHeaderScope.Row;
                    }
                    else
                    {
                        cell = new C1GridViewCell(field);
                    }
                }

                leaves[i].InitializeCell(cell, i, row.RowType, row.RowState);
                cells.Add(cell);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isPagerHeader"></param>
        protected virtual void InitializePager(bool isPagerHeader)
        {
            C1Pager.C1Pager pager = new C1Pager.C1Pager();
            pager.Mode = PagerSettings.Mode;
            pager.Site = this.Site;
            if (isPagerHeader)
            {
                pager.Attributes.Add("class", CssClasses.Wijgrid_Header + " " + CssClasses.Wijsuperpanel_Header + " " + CssClasses.JqueryUI_WidgetHeader + " " + CssClasses.Jui_Widget + " " + CssClasses.Wijpager);
                this.Controls.AddAt(0, pager);
            }
            else
            {
                pager.Attributes.Add("class", CssClasses.Wijgrid_Footer + " " + CssClasses.Wijsuperpanel_Footer + " " + CssClasses.JqueryUI_StateDefault + " " + CssClasses.Jui_Widget + " " + CssClasses.Wijpager);
                this.Controls.Add(pager);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void InitializeGroupArea()
        {
            HtmlGenericControl containerDiv = new HtmlGenericControl("div");
            containerDiv.Attributes.Add("class", CssClasses.JqueryUI_WidgetContent + " " + CssClasses.JqueryUI_HelperClearfix + " " + CssClasses.Wijgrid_GroupArea);
            containerDiv.InnerText = this.GroupAreaCaption;
            this.Controls.AddAt(0, containerDiv);
        }

        /// <summary>
        /// Passes the event raised by a control within the container up the page's UI server control hierarchy.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="args">An <see cref="EventArgs"/> that contains event data.</param>
        /// <returns>True to indicate that this method is passing an event raised by a control within the container up the page's UI server control hierarchy; otherwise, false.</returns>
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            bool causesValidation = false;
            string validationGroup = string.Empty;

            C1GridViewCommandEventArgs c1args = args as C1GridViewCommandEventArgs;
            if (c1args != null)
            {
                IButtonControl btnControl = c1args.CommandSource as IButtonControl;
                if (btnControl != null)
                {
                    causesValidation = btnControl.CausesValidation;
                    validationGroup = btnControl.ValidationGroup;
                }
            }

            return this.HandleEvent(args, causesValidation, validationGroup);
        }

        /// <summary>
        /// Raises the <see cref="System.Web.UI.DataSourceView.DataSourceViewChanged"/> event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
        protected override void OnDataSourceViewChanged(object sender, EventArgs e)
        {
            _c1ds = null;
            ClearDataKeys();
            base.OnDataSourceViewChanged(sender, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_isProductLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }

            base.OnInit(e);

            if (Page != null)
            {
                if (DataKeyNamesInternal.Length > 0 && !AutogenerateColumns)
                {
                    Page.RegisterRequiresViewStateEncryption();
                }

                Page.RegisterRequiresControlState(this);

                if (Page.IsPostBack || Page.IsCallback)
                {
                    Page.InitComplete += new EventHandler(Page_InitComplete);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnPagePreLoad(object sender, EventArgs e)
        {
            if (Page != null && Page.IsPostBack && !Page.IsCallback && _dataBindFlag)
            {
                _dataBindFlag = false;
                RequiresDataBinding = true;
                EnsureDataBound(); //#34006: On setting CallbackSettings.Action to Paging, the value of previous page gets fetched in SelectedIndexChanging event
            }

            base.OnPagePreLoad(sender, e);
        }

        /// <summary>
        /// Binds data from the data source to the control. 
        /// </summary>
        /// <param name="data">An <see cref="IEnumerable"/> that contains the data source.</param>
        protected override void PerformDataBinding(IEnumerable data)
        {
            EnsureDataSource(data, true);

            DataView dataView = data as DataView;

            if (!IsBoundUsingDataSourceID && AllowAutoSort/* && AllowSorting*/ /* && !AutogenerateColumns*/)
            {
                string sortExpression = BuildSortExpression();

                if (data != null && dataView == null && !string.IsNullOrEmpty(sortExpression))
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EAutosortingNotSupported"), ID));
                }

                if (dataView != null)
                {
                    if (!string.IsNullOrEmpty(sortExpression))
                    {
                        dataView.Sort = sortExpression;
                    }
                }
            }

            // apply filtering
            if (FilterSettings.Mode == FilterMode.Auto)
            {
                string filterExpression = BuildFilterExpression();
                if ((filterExpression != null) &&
                    !(dataView == null && string.IsNullOrEmpty(filterExpression))) // no dataView + filtering is not used.
                {
                    if (data != null && dataView == null)
                    {
                        throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EAutofilteringNotSupported"), ID));
                    }

                    if (dataView != null)
                    {
                        dataView.RowFilter = filterExpression;
                    }
                }
            }

            if (Page != null && Page.IsPostBack && HasDetails() && HierarchyTree.Count > 0)
            {
                // save properties of the real detail instances to restore them later when detail grids will be repopulated from the Detail template.
                // It is necessary to simulate that each detail grid of the same hierarchy level can be configured individually via UI actions (sorting, pageing, column moving etc).
                HierarchyTree.PersistProperties(this.Details);
            }

            base.PerformDataBinding(data);

            if (IsBoundUsingDataSourceID && (EditIndex >= 0) && (EditIndex <= Rows.Count))
            {
                EditRowValues.Clear();
                ExtractRowValues(EditRowValues, Rows[EditIndex], true, false);
            }

            if (SelectedPersistedDataKey == null && DataKeys.Count > 0)
            {
                SelectedPersistedDataKey = DataKeys[0];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_isProductLicensed, Page, this);

            base.OnPreRender(e);

            if (Page != null && !IsDesignMode)
            {
                // Always register Callback js reference for exporting. Because when scrolling or paging is used,
                // exporting requires all data with the layout by callback.
                Page.ClientScript.GetCallbackEventReference(this, string.Empty, string.Empty, string.Empty); // brings WebForms.js
            }
        }

        /// <summary>
        /// Renders the Web server control content to the client's browser using the specified <see cref="HtmlTextWriter"/> object.
        /// </summary>
        /// <param name="writer">The <see cref="HtmlTextWriter"/> used to render the server control content on the client's browser.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            AllowInnerStateCreation();
            Render(writer, true); // Render(writer, !DesignMode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
            base.RenderEndTag(writer);
        }

        /// <summary>
        /// Sets up the control hierarchy for the <see cref="C1GridView"/> control.
        /// </summary>
        protected virtual void PrepareControlHierarchy()
        {
            if (!HasControls())
            {
                return;
            }

            if (DesignMode)
            {
                CssClass += " ui-widget wijmo-wijgrid ui-widget-content ui-corner-all";
            }

            TableRowCollection rows = TableRows;

            TableItemStyle compositeAltRowStyle = new TableItemStyle();
            compositeAltRowStyle.CopyFrom(_alternatingRowStyle);
            compositeAltRowStyle.MergeWith(_rowStyle);

            // render all rows by default
            //int bodyRowsOffset = GetBodyRowsOffset();
            int start = 0; // int start = bodyRowsOffset ?
            int end = rows.Count; // int end = rows.Count - bodyRowsOffset ?
            bool isPartialCallback = (Page != null) && Page.IsCallback && (ActualCallbackMode == CallbackMode.Partial);

            if (ServerSideRowsScrollingAllowed()) // restrict render area
            {
                start = _scrollingState.Index;
                end = start + (_scrollingState.MaxCount < 0 ? INITVIRTUALPAGESIZE : _scrollingState.MaxCount) - 1;
            }

            bool hasDetails = HasDetails();

            foreach (C1GridViewRow row in rows)
            {
                //if (!((row.RowType != C1GridViewRowType.DataRow) ||
                //	(row.RowIndex >= start && row.RowIndex <= end) ||
                //	(isPartialCallback && (row.RowIndex == _partialCurRowIndex || row.RowIndex == _partialPrevRowIndex))))

                // filter body rows
                if (!((row.TableSection != TableRowSection.TableBody) || // DataRow, DetailRow
                    (row.BodyRowIndex >= start && row.BodyRowIndex <= end) ||
                    (isPartialCallback && (row.RowIndex == _partialCurRowIndex || row.RowIndex == _partialPrevRowIndex))))
                {
                    row.Visible = false;
                    continue;
                }

                TableItemStyle compositeRowStyle = new TableItemStyle();

                switch (row.RowType)
                {
                    case C1GridViewRowType.DataRow:
                        if ((row.RowState & C1GridViewRowState.Edit) != 0)
                        {
                            compositeRowStyle.MergeWith(_editRowStyle);
                            compositeRowStyle.CssClass += " " + CssClasses.C1GridView_EditRow; // infrastructure.
                        }

                        if ((row.RowState & C1GridViewRowState.Selected) != 0)
                        {
                            compositeRowStyle.MergeWith(_selectedRowStyle);
                            if (_selectedRowStyle == null || string.IsNullOrEmpty(_selectedRowStyle.CssClass))
                            {
                                compositeRowStyle.CssClass += " " + CssClasses.C1GridView_SelectedRow; // infrastructure.
                            }
                        }

                        if ((row.RowState & C1GridViewRowState.Alternate) != 0)
                        {
                            compositeRowStyle.MergeWith(compositeAltRowStyle);

                            if (DesignMode)
                            {
                                compositeRowStyle.CssClass += CssClasses.Wijgrid_Row + " " + CssClasses.JqueryUI_WidgetContent + " " + CssClasses.Wijgrid_DataRow + " " + CssClasses.Wijgrid_AltRow;
                            }
                        }
                        else
                        {
                            compositeRowStyle.MergeWith(_rowStyle);

                            if (DesignMode)
                            {
                                compositeRowStyle.CssClass += CssClasses.Wijgrid_Row + " " + CssClasses.JqueryUI_WidgetContent + " " + CssClasses.Wijgrid_DataRow;
                            }
                        }

                        if (hasDetails)
                        {
                            compositeRowStyle.CssClass += " " + ((((C1GridViewDataRow)row).Expanded)
                                ? CssClasses.C1GridView_ExpandedRow
                                : CssClasses.C1GridView_CollapsedRow);
                        }

                        break;

                    case C1GridViewRowType.DetailRow:
                        compositeRowStyle.CssClass += " " + CssClasses.Wijgrid_HierarchyDetailRow;
                        break;

                    case C1GridViewRowType.EmptyDataRow:
                        if (row.Cells.Count == 0) // both EmptyDataTemplate and EmptyDataText are empty.
                        {
                            row.Visible = false;
                            continue;
                        }

                        compositeRowStyle.CopyFrom(_emptyDataRowStyle);

                        compositeRowStyle.CssClass += " " + CssClasses.Wijgrid_EmptyDataRow;
                        break;

                    case C1GridViewRowType.Filter:
                        if (!ShowFilter)
                        {
                            row.Visible = false;
                            continue;
                        }

                        compositeRowStyle.CopyFrom(_filterStyle);

                        if (DesignMode)
                        {
                            compositeRowStyle.CssClass += " " + CssClasses.Wijgrid_FilterRow;
                        }

                        break;

                    case C1GridViewRowType.Footer:
                        if (!ShowFooter)
                        {
                            row.Visible = false;
                            continue;
                        }

                        compositeRowStyle.CopyFrom(_footerStyle);

                        if (DesignMode)
                        {
                            compositeRowStyle.CssClass += " " + CssClasses.Wijgrid_FooterRow;
                        }

                        break;

                    case C1GridViewRowType.Header:
                        if (!ShowHeader)
                        {
                            row.Visible = false;
                            continue;
                        }

                        compositeRowStyle.CopyFrom(_headerStyle);

                        if (DesignMode)
                        {
                            compositeRowStyle.CssClass += " " + CssClasses.Wijgrid_HeaderRow;
                        }

                        break;
                }

                // apply composite style to the row
                row.MergeStyle(compositeRowStyle);

                // ** apply column styles to the cells
                foreach (C1GridViewCell cell in row.Cells)
                {
                    if (cell.ContainingField != null)
                    {
                        // Original html table treated as client data source so we must render invisible cells in run-time to handle grouping of the invisible columns properly.
                        // invisible cells will be hidden on the client.
                        if (DesignMode && !cell.ContainingField.ParentVisibility)
                        {
                            cell.Visible = false;
                            continue;
                        }

                        cell.ContainingField.PrepareCell(cell, row);
                    }
                }
            } // for each row
        }

        #endregion

        #region internal members

        internal C1DetailGridViewCollection DetailInternal
        {
            get { return _detail; }
        }

        internal C1DetailGridView FirstDetailItemInternal
        {
            get
            {
                return (_detail != null && _detail.Count > 0)
                    ? _detail[0]
                    : null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether a control is being used on a design surface.
        /// </summary>
        new protected internal bool DesignMode
        {
            get { return base.DesignMode; }
        }

        internal bool IsChildControlsCreated
        {
            get { return ChildControlsCreated; }
        }

        #endregion

        #region private members

        private string[] DataKeyNamesInternal
		{
			get	{ return _dataKeyNames ?? new string[0]; }
		}

        private ArrayList DataKeysInternal
        {
            get
            {
                if (_dataKeysInternal == null)
                {
                    _dataKeysInternal = new ArrayList();
                }

                return _dataKeysInternal;
            }
        }

        private IOrderedDictionary EditRowValues
        {
            get
            {
                if (_editRowValues == null)
                {
                    _editRowValues = new OrderedDictionary(Columns.Count);
                }

                return _editRowValues;
            }
        }

        internal HierararchyStateTree HierarchyTree
        {
            get
            {
                if (_hierarchyTree == null)
                {
                    _hierarchyTree = new HierararchyStateTree();
                }

                return _hierarchyTree;
            }
            set { _hierarchyTree = value; }
        }

        internal bool IgnoreDataBinding
        {
            get { return _ignoreDataBinding; }
            set { _ignoreDataBinding = value; }
        }

        private bool IsJsonStateLoaded
        {
            get { return this._jsonStateLoaded; }
        }

        private TableRowCollection TableRows
        {
            get
            {
                if (ChildControlsCreated)
                {
                    Table childTable = GetChildTable();

                    if (childTable != null)
                    {
                        return childTable.Rows;
                    }
                }

                return null;
            }
        }

        private void AllowInnerStateCreation()
        {
            _createInnerState = true;
        }

        private string BuildFilterExpression()
        {
            if (DesignMode)
            {
                return string.Empty;
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (C1BaseField field in Columns.Traverse())
            {
                C1FilterField boundField = field as C1FilterField;
                if (boundField != null)
                {
                    string filterExpression = boundField.GetFilterExpression();
                    if (!string.IsNullOrEmpty(filterExpression))
                    {
                        sb.AppendFormat("({0}) AND ", filterExpression);
                    }
                }
            }

            return (sb.Length > 0)
                ? sb.Remove(sb.Length - 5, 5).ToString() // remove trailing " AND "
                : sb.ToString();
        }

        private string BuildSortExpression()
        {
            if (DesignMode)
            {
                return string.Empty;
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            List<C1Field> groupCollection = this.GetGroupedColumns();
            foreach (C1Field c1Field in groupCollection)
            {
                string expression = c1Field.EnsureSortExpression();
                if (!string.IsNullOrEmpty(expression))
                {
                    sb.Append(expression);

                    if (c1Field.SortDirection == C1SortDirection.Descending)
                    {
                        sb.Append(" desc");
                    }

                    sb.Append(",");
                }
            }

            foreach (C1BaseField field in Columns.Traverse())
            {
                C1Field c1Field = field as C1Field;
                if (c1Field != null && c1Field.SortDirection != C1SortDirection.None && c1Field.GroupInfo.Position == GroupPosition.None)
                {
                    string expression = c1Field.EnsureSortExpression();
                    if (!string.IsNullOrEmpty(expression))
                    {
                        sb.Append(expression);

                        if (c1Field.SortDirection == C1SortDirection.Descending)
                        {
                            sb.Append(" desc");
                        }

                        sb.Append(",");
                    }
                }
            }

            if (!string.IsNullOrEmpty(_autoSortExpression))
            {
                sb.Append(string.Format("{0} {1},", _autoSortExpression, _autoSortDirection == C1SortDirection.Descending ? "desc" : string.Empty));
            }

            return (sb.Length > 0)
                ? sb.ToString(0, sb.Length - 1)
                : string.Empty;
        }

        private void ClearDataKeys(bool clearHierarchy = true)
        {
            if (clearHierarchy)
            {
                if (_hierarchyTree != null)
                {
                    _hierarchyTree.Clear();
                }
            }

            _dataKeysInternal = null;
        }

        private Table CreateChildTable()
        {
            string id = string.IsNullOrEmpty(ID)
                ? null
                : ClientID;

            return new ChildTable(id);
        }

        private int GetBodyRowsOffset()
        {
            int result = 0;

            Table childTable = GetChildTable();

            if (childTable != null)
            {
                for (int i = 0; i < childTable.Rows.Count; i++)
                {
                    if (childTable.Rows[i].TableSection != TableRowSection.TableBody)
                    {
                        result++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return result;
        }

        private Table GetChildTable()
        {
            Table childTable = Controls[0] as Table;

            if (childTable == null) // Design-time
            {
                childTable = Controls[1] as Table;
                if (childTable == null)
                {
                    childTable = Controls[2] as Table;
                }
            }

            return childTable;
        }

        private void CreateHeaderRows(bool dataBinding, List<C1BaseField> leaves, TableRowCollection rows)
        {
            bool useAccessibleHeader = UseAccessibleHeader;

            int height = _headerTable.GetLength(0);
            int width = _headerTable.GetLength(1);
            int startFrom = 0;

            // bool renderLeavesOnly = !DesignMode;
            bool renderLeavesOnly = false;// #63597 

            if (renderLeavesOnly && height > 0)
            {
                startFrom = height - 1; // render only the last row in run-time (wijgrid doesn't support multiheaders and spanned cells).

                // check whether all the cells in the last row are linked with the corresponding leaves.
                for (int j = 0; j < width; j++)
                {
                    if (_headerTable[startFrom, j].Column == null) // not linked (spanned column is placed somewhere above)
                    {
                        for (int row = startFrom - 1; row >= 0; row--)
                        {
                            _headerTable[startFrom, j].Column = _headerTable[row, j].Column;

                            if (_headerTable[startFrom, j].Column != null)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            for (int i = startFrom; i < height; i++)
            {
                _headerRow = CreateRow(-1, -1, C1GridViewRowType.Header, C1GridViewRowState.Normal);
                _headerRow.TableSection = TableRowSection.TableHeader;

                for (int j = 0; j < width; j++)
                {
                    BandProcessor.RowColSpan span = _headerTable[i, j];

                    if (span.Column != null)
                    {
                        C1GridViewHeaderCell cell = new C1GridViewHeaderCell(span.Column);

                        if (useAccessibleHeader)
                        {
                            cell.Scope = TableHeaderScope.Column;
                            cell.AbbreviatedText = span.Column.AccessibleHeaderText;
                        }

                        span.Column.InitializeCell(cell, j, C1GridViewRowType.Header, C1GridViewRowState.Normal);

                        if (span.ColSpan > 1)
                        {
                            cell.ColumnSpan = span.ColSpan;
                        }

                        if (span.RowSpan > 1)
                        {
                            cell.RowSpan = span.RowSpan;
                        }

                        _headerRow.Cells.Add(cell);
                    }
                }

                OnRowCreated(new C1GridViewRowEventArgs(_headerRow));
                rows.Add(_headerRow);

                if (dataBinding)
                {
                    _headerRow.DataBind();
                }
            }
        }

        private List<C1BaseField> CreateLeaves(C1DataSource dataSource, bool useDataSource)
        {
            if (useDataSource)
            {
                _autogeneratedColumns = null;
            }

            C1BaseFieldCollection allColumns = new C1BaseFieldCollection(true); // contains both user-defined and autogenerated columns
                                                                                // TODO? RowHeader

            // * append autogenerated command column
            if (AutoGenerateDeleteButton || AutoGenerateEditButton || AutoGenerateSelectButton || AutoGenerateFilterButton)
            {
                C1CommandField cmdField = new C1CommandField();

                if (AutoGenerateDeleteButton)
                {
                    cmdField.ShowDeleteButton = true;
                }

                if (AutoGenerateEditButton)
                {
                    cmdField.ShowEditButton = true;
                }

                if (AutoGenerateFilterButton)
                {
                    cmdField.ShowFilterButton = true;
                }

                if (AutoGenerateSelectButton)
                {
                    cmdField.ShowSelectButton = true;
                }

                allColumns.Add(cmdField);
            }

            // * append user columns
            allColumns.AddRange(_userColumns);

            // * build _autogeneratedColumns collection
            if (AutogenerateColumns || (DesignMode && (_userColumns == null || _userColumns.Count == 0))) // if we're at design time with no columns defined, then act like we're autogenerated
            {
                if (useDataSource)
                {
                    _autogeneratedColumns = new C1BaseFieldCollection(this);

                    if (ColumnsGenerator != null)
                    {
                        foreach (C1BaseField field in ColumnsGenerator.GenerateFields(this))
                        {
                            field.IsAutogenerated = true;
                            _autogeneratedColumns.Add(field);
                        }
                    }
                    else
                    {
                        if (dataSource != null)
                        {
                            _autogeneratedColumns.AddRange(dataSource.AutogenerateFields(DataKeyNamesInternal));
                        }
                    }
                }

                if (dataSource != null)
                {
                    MapColumnDataTypes(_autogeneratedColumns, dataSource.FieldInfo);
                }

                if (_autogeneratedColumns != null)
                {
                    foreach (C1BaseField field in _autogeneratedColumns)
                    {
                        C1Field c1field = field as C1Field;
                        if (c1field != null && _autoSortExpression.Length > 0 && c1field.SortExpression == _autoSortExpression)
                        {
                            c1field.SortDirection = _autoSortDirection;
                        }

                        allColumns.Add(field); // append autogenerated column
                    }
                }
            }

            // * build leaves and headers table
            _leaves = new List<C1BaseField>();
            _headerTable = new BandProcessor().GenerateTable(allColumns, _leaves, true);

            return _leaves;
        }

        private C1GridViewRow CreateRow(int rowIndex, int dataSourceIndex, int displayDataItemIndex, C1GridViewRowType rowType, C1GridViewRowState rowState, bool dataBind, object dataItem,
            List<C1BaseField> leaves, TableRowCollection rows, ArrayList rowsProp)
        {
            C1GridViewRow row = CreateRow(rowIndex, dataSourceIndex, rowType, rowState);

            row.DisplayDataItemIndex = displayDataItemIndex;

            switch (row.RowType)
            {
                case C1GridViewRowType.Filter:
                    row.TableSection = TableRowSection.TableHeader;
                    break;

                case C1GridViewRowType.Header:
                    row.TableSection = TableRowSection.TableHeader;
                    break;

                case C1GridViewRowType.Footer:
                    row.TableSection = TableRowSection.TableFooter;
                    break;

                default:
                    row.TableSection = TableRowSection.TableBody;
                    row.BodyRowIndex = _bodyRowsCount;
                    _bodyRowsCount++;
                    break;
            }

            switch (row.RowType)
            {
                case C1GridViewRowType.DetailRow:
                    ((C1GridViewDetailRow)row).DataRow = (C1GridViewDataRow)this.Rows[displayDataItemIndex];
                    InitializeDetailRow((C1GridViewDetailRow)row, leaves);
                    break;

                case C1GridViewRowType.EmptyDataRow:
                    InitializeEmptyDataRow(row, leaves);
                    break;

                default:
                    InitializeRow(row, leaves);
                    break;
            }

            if (dataBind)
            {
                row.DataItem = dataItem;
            }

            C1GridViewRowEventArgs args = new C1GridViewRowEventArgs(row);

            OnRowCreated(args);

            rows.Add(row);

            if (rowsProp != null)
            {
                rowsProp.Add(row);
            }

            if (dataBind)
            {
                row.DataBind();

                if (rowType != C1GridViewRowType.DetailRow)
                {
                    OnRowDataBound(args);
                }

                row.DataItem = null;
            }

            return row;
        }

        private C1DataSource EnsureDataSource(IEnumerable data, bool dataBinding)
        {
            bool mapDataTypes = false;

            if (dataBinding) // always recreate
            {
                _c1ds = C1DataSource.CreateDataSource(this, data, GetData(), SelectArguments, DataKeyNamesInternal);
                mapDataTypes = true;
            }
            else
            {
                if (_c1ds == null)
                {
                    _c1ds = C1DataSource.CreateDataSource(this, data, DataKeyNamesInternal);
                }

                if (_fieldInfoViewState != null) // restore FieldInfo from viewState
                {
                    ((IStateManager)_c1ds.FieldInfo).LoadViewState(_fieldInfoViewState);
                    mapDataTypes = true;
                }
            }

            if (mapDataTypes && (_userColumns != null))
            {
                MapColumnDataTypes(_userColumns.Traverse(), _c1ds.FieldInfo);
            }

            return _c1ds;
        }

        private List<FilterLocalization> GetLocalizedFilterDisplayNames()
        {
            List<FilterLocalization> result = new List<FilterLocalization>();
            string val;

            // NoFilter
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.NoFilter", string.Empty)))
            {
                result.Add(new FilterLocalization("NoFilter", val));
            }

            // Contains
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.Contains", string.Empty)))
            {
                result.Add(new FilterLocalization("Contains", val));
            }

            // NotContain
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.NotContain", string.Empty)))
            {
                result.Add(new FilterLocalization("NotContain", val));
            }

            // BeginsWith
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.BeginsWith", string.Empty)))
            {
                result.Add(new FilterLocalization("BeginsWith", val));
            }

            // EndsWith
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.EndsWith", string.Empty)))
            {
                result.Add(new FilterLocalization("EndsWith", val));
            }

            // Equals
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.Equals", string.Empty)))
            {
                result.Add(new FilterLocalization("Equals", val));
            }

            // NotEqual
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.NotEqual", string.Empty)))
            {
                result.Add(new FilterLocalization("NotEqual", val));
            }

            // Greater
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.Greater", string.Empty)))
            {
                result.Add(new FilterLocalization("Greater", val));
            }

            // Less
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.Less", string.Empty)))
            {
                result.Add(new FilterLocalization("Less", val));
            }

            // GreaterOrEqual
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.GreaterOrEqual", string.Empty)))
            {
                result.Add(new FilterLocalization("GreaterOrEqual", val));
            }

            // LessOrEqual
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.LessOrEqual", string.Empty)))
            {
                result.Add(new FilterLocalization("LessOrEqual", val));
            }

            // IsEmpty
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.IsEmpty", string.Empty)))
            {
                result.Add(new FilterLocalization("IsEmpty", val));
            }

            // NotIsEmpty
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.NotIsEmpty", string.Empty)))
            {
                result.Add(new FilterLocalization("NotIsEmpty", val));
            }

            // IsNull
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.IsNull", string.Empty)))
            {
                result.Add(new FilterLocalization("IsNull", val));
            }

            // NotIsNull
            if (!string.IsNullOrEmpty(val = C1Localizer.GetString("C1Grid.Filters.NotIsNull", string.Empty)))
            {
                result.Add(new FilterLocalization("NotIsNull", val));
            }

            return result;
        }

        private List<C1Field> GetGroupedColumns()
        {
            bool needRest = false;

            foreach (C1BaseField field in Columns.Traverse())
            {
                C1Field c1Field = field as C1Field;

                if (c1Field != null)
                {
                    if (c1Field.GroupInfo.Position != GroupPosition.None)
                    {
                        if (c1Field.GroupedIndex == -1)
                        {
                            needRest = true;
                        }

                        if (c1Field.SortDirection == C1SortDirection.None)
                        {
                            c1Field.SortDirection = C1SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        if (c1Field.GroupedIndex != -1)
                        {
                            c1Field.GroupedIndex = -1;
                        }
                    }
                }
            }

            List<C1Field> groupCollection = new List<C1Field>();
            if (needRest)
            {
                int groupLength = 0;

                foreach (C1BaseField field in Columns.Traverse())
                {
                    C1Field c1Field = field as C1Field;
                    if (c1Field != null && c1Field.GroupInfo.Position != GroupPosition.None)
                    {
                        c1Field.GroupedIndex = groupLength++;
                        groupCollection.Add(c1Field);
                    }
                }
            }
            else
            {
                foreach (C1BaseField field in Columns.Traverse())
                {
                    C1Field c1Field = field as C1Field;
                    if (c1Field != null && c1Field.GroupedIndex != -1)
                    {
                        groupCollection.Add(c1Field);
                    }
                }

                groupCollection.Sort(delegate(C1Field a, C1Field b)
                {
                    if (a.GroupedIndex == b.GroupedIndex)
                    {
                        return 0;
                    }

                    return (a.GroupedIndex > b.GroupedIndex)
                        ? 1
                        : -1;
                });
            }

            return groupCollection;
        }

        private int GetRowIndex(C1GridViewCommandEventArgs args)
        {
            return (args.Row != null)
                ? args.Row.RowIndex
                : Convert.ToInt32(args.CommandArgument, CultureInfo.InvariantCulture);
        }

        internal bool IsRoot()
        {
            return !(this is C1DetailGridView);
            //return this._owner == null;
        }

        internal bool HasDetails()
        {
            C1DetailGridView detail = FirstDetailItemInternal;

            return (detail != null && detail.Relation.Count > 0);
        }

        //private void MapColumnDataTypes(IEnumerable<C1BaseField> enumerable, List<FieldInfo> fieldsInfo)
        private void MapColumnDataTypes(IEnumerable columns, List<DataFieldInfo> fieldsInfo)
        {
            if (columns != null && fieldsInfo != null && fieldsInfo.Count > 0)
            {
                foreach (C1BaseField field in columns)
                {
                    C1FilterField boundField = field as C1FilterField;
                    if (boundField != null)
                    {
                        DataFieldInfo fieldInfo = fieldsInfo.Find(info => info.DataField.ToLower() == boundField.DataField.ToLower());
                        if (fieldInfo != null)
                        {
                            boundField._DataType = fieldInfo.Type;
                        }
                    }
                }
            }
        }

        private void LoadDataKeysState(object state)
        {
            if (state != null)
            {
                object[] objState = (object[])state;

                ClearDataKeys(false);

                string[] dataKeyNames = DataKeyNamesInternal;
                for (int i = 0, len = objState.Length; i < len; i++)
                {
                    DataKeysInternal.Add(new DataKey(new OrderedDictionary(len), dataKeyNames));
                    ((IStateManager)DataKeysInternal[i]).LoadViewState(objState[i]);
                }
            }
        }

        /// <summary>
        /// </summary>
        protected internal void OnRequiresDataBinding()
        {
            if (Initialized && !_ignoreDataBinding)
            {
                RequiresDataBinding = true;
            }
        }

        /// <summary>
        /// Adds the HTML attributes and styles of a <see cref="C1GridView"/> control to be rendered to
        /// the specified output stream.
        /// </summary>
        /// <param name="writer">An <see cref="HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            Style controlStyle = new Style();
            bool resetControlStyle = ControlStyleCreated || DesignMode;

            if (resetControlStyle)
            {
                Unit height = Height;

                controlStyle.CopyFrom(ControlStyle); // save
                ControlStyle.Reset(); // clear control style. All style properties (class, sizes etc) will be rendered to the outer div later.

                // table css
                ControlStyle.Width = Unit.Percentage(100);

                if (!height.IsEmpty) // stretch only if height is set (112832).
                {
                    ControlStyle.Height = Unit.Percentage(100);
                }
            }

            base.AddAttributesToRender(writer);

            if (resetControlStyle)
            {
                ControlStyle.Reset();
                ControlStyle.CopyFrom(controlStyle); // restore
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            AllowInnerStateCreation();

            return base.GetScriptDescriptors();
        }

        /// <summary>
        /// </summary>
        protected override void RegisterScriptDescriptors()
        {
            AllowInnerStateCreation();

            bool isCallback = Page != null && Page.IsCallback;

            if (!isCallback) // To avoid the "Script controls must be registered using RegisterScriptControl() before calling RegisterScriptDescriptors()." exception when the Render() method is called directly during callback.
            {
                base.RegisterScriptDescriptors();
            }
        }

        private void Page_InitComplete(object sender, EventArgs e)
        {
            Page.InitComplete -= new EventHandler(Page_InitComplete);
            //RestoreJsonState();
        }

        private void Render(HtmlTextWriter writer, bool renderPanel)
        {
            if (!DesignMode)
            {
                if (AllowClientEditing && EditIndex >= 0)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EMixedEditingModes"), ID));
                }

                if (HasDetails() && HasGrouping())
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EMixedGroupingAndMasterDetail"), ID));
                }

                if (this._InternalAllowVVirtualScrolling())
                {
                    if (AllowPaging || _InternalStaticRowIndex() >= 0 || HasMerging() || HasDetails())
                    {
                        throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EClientSideRowsVirtualScrollingRestrictions"), ID));
                    }

                    if (CallbackScrolling && HasGrouping())
                    {
                        throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.EServerSideRowsVirtualScrollingRestrictions"), ID));
                    }
                }
            }

            if (Page != null)
            {
                Page.VerifyRenderingInServerForm(this);
            }

            PrepareControlHierarchy();

            if (renderPanel)
            {
                // render control style properties to the outer div.

                Style outerDivControlStyle = new Style();
                outerDivControlStyle.CopyFrom(ControlStyle);

                string hiddenClass = Utils.GetHiddenClass();

                outerDivControlStyle.CssClass = string.Format("{0} {1} {2} {3}",
                    DesignMode ? "" : hiddenClass, // hide markup during the client-side initialization.
                    DesignMode ? "" : CssClasses.Wijgrid, // Fixing issue: C1Combobox controls in the template columns look broken.
                    DesignMode ? "" : CssClasses.C1GridView_Marker,
                    outerDivControlStyle.CssClass);

                outerDivControlStyle.AddAttributesToRender(writer);

                // render outer div

                if (!DesignMode && !DisplayVisible)
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID + "_div");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
            }

            base.Render(writer); //RenderContents(writer);

            if (renderPanel)
            {
                writer.RenderEndTag();
            }
        }

        private void RestoreJsonState()
        {
            try
            {
                if (Page != null)
                {
                    Hashtable data = this.JsonSerializableHelper.GetJsonData(Page.Request.Form);
                    this.RestoreStateFromJson(data);
                    CreateLeaves(null, false); // process user-defined columns only
                }
            }
            finally
            {
                _jsonStateLoaded = true;
            }
        }

        private C1SortDirection ToggleSortDirection(C1SortDirection value)
        {
            if (value == C1SortDirection.Ascending)
            {
                return C1SortDirection.Descending;
            }

            return C1SortDirection.Ascending;
        }

        private int ViewStateItemCount() {
			return (int)(ViewState["_!ItemCount"] ?? 0);
		}

        #endregion

        #region event handling

        private bool HandleEvent(EventArgs args, bool causesValidation, string validationGroup)
        {
            if (causesValidation && Page != null)
            {
                Page.Validate(validationGroup);
            }

            bool handled = false;

            C1GridViewCommandEventArgs c1args = args as C1GridViewCommandEventArgs;
            if (c1args != null)
            {
                string command = c1args.CommandName.ToLowerInvariant();
                bool isGridCommand = IsGridCommand(command);

                handled = true;

                if ((isGridCommand && IsRowCommand(command) || !isGridCommand)) // row command or user command
                {
                    OnRowCommand(c1args);
                }

                switch (command)
                {
                    case CMD_BATCH:
                        HandleBatch(c1args);
                        break;

                    case CMD_CANCEL:
                        _tmpRowCommandIndex = EditIndex;
                        HandleCancel(c1args);
                        break;

                    case CMD_DELETE:
                        HandleDelete(c1args);
                        break;

                    case CMD_EDIT:
                        _tmpRowCommandIndex = EditIndex;
                        HandleEdit(c1args);
                        break;

                    case CMD_FILTER:
                        HandleFilter(c1args);
                        break;

                    case CMD_MOVE:
                        HandleMove(c1args);
                        break;

                    case CMD_PAGE:
                        HandlePage(c1args);
                        break;

                    case CMD_SELECT:
                        _tmpRowCommandIndex = SelectedIndex;
                        HandleSelect(c1args);
                        break;

                    case CMD_SORT:
                        HandleSort(c1args);
                        break;

                    case CMD_UPDATE:
                        _tmpRowCommandIndex = EditIndex;
                        HandleUpdate(c1args, causesValidation);
                        break;

                    case CMD_GROUP:
                        HandleGroup(c1args);
                        break;

                    case CMD_UNGROUP:
                        HandleUngroup(c1args);
                        break;

                    case CMD_HIERARCHY:
                        HandleToggleHierarchy(c1args);
                        break;
                }
            }

            return handled;
        }

        private void HandleBatch(C1GridViewCommandEventArgs args)
        {
            if (_batchValues != null && _batchValues.Count > 0)
            {
                OnBeginRowUpdate(new C1GridViewBeginRowUpdateEventArgs());

                DataSourceView dataVew = null;

                if (IsBoundUsingDataSourceID)
                {
                    dataVew = GetData();

                    if (dataVew == null)
                    {
                        throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ENullView"), ID));
                    }
                }

                EnsureChildControls();
                TableRowCollection tableRows = TableRows;
                IList dataKeysArray = DataKeysInternal;
                int bodyRowsOffset = GetBodyRowsOffset();

                foreach (object dataIndexKey in _batchValues.Keys)
                {
                    Hashtable values = (Hashtable)_batchValues[dataIndexKey];
                    if (values.Count > 0)
                    {
                        int dataRowIndex = int.Parse((string)dataIndexKey); // including Data and Details rows
                        dataRowIndex = ((C1GridViewRow)tableRows[dataRowIndex + bodyRowsOffset]).RowIndex; // convert to DataItem index.

                        if (dataKeysArray == null || dataRowIndex >= dataKeysArray.Count)
                        {
                            throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ERowKeyValueNotFound"), ID));
                        }

                        DataKey key = (DataKey)dataKeysArray[dataRowIndex];

                        C1GridViewUpdateEventArgs ea = new C1GridViewUpdateEventArgs(dataRowIndex);

                        foreach (DictionaryEntry entry in ((DataKey)dataKeysArray[dataRowIndex]).Values)
                        {
                            ea.Keys.Add(entry.Key, entry.Value);
                        }

                        foreach (object valueKey in values.Keys)
                        {
                            ea.OldValues.Add(valueKey, ((ArrayList)values[valueKey])[0]);
                            ea.NewValues.Add(valueKey, ((ArrayList)values[valueKey])[1]);
                        }

                        OnRowUpdating(ea);

                        if (!ea.Cancel && IsBoundUsingDataSourceID)
                        {
                            dataVew.Update(ea.Keys, ea.NewValues, ea.OldValues, (affectedRows, ex) =>
                            {
                                C1GridViewUpdatedEventArgs uea = new C1GridViewUpdatedEventArgs(affectedRows, ex, ea.Keys, ea.NewValues, ea.OldValues);

                                OnRowUpdated(uea);

                                if (ex != null && !uea.ExceptionHandled)
                                {
                                    return false;
                                }

                                return true;
                            });
                        }
                    }
                }

                OnEndRowUpdated(new C1GridViewEndRowUpdatedEventArgs());

                if (IsBoundUsingDataSourceID)
                {
                    RequiresDataBinding = true;
                }
            }
        }

        private void HandleCancel(C1GridViewCommandEventArgs args)
        {
            C1GridViewCancelEditEventArgs c1args = new C1GridViewCancelEditEventArgs(GetRowIndex(args));

            OnRowCancelingEdit(c1args);

            if (!c1args.Cancel)
            {
                if (IsBoundUsingDataSourceID)
                {
                    EditIndex = -1;
                }

                RequiresDataBinding = true;
            }
        }

        private void HandleDelete(C1GridViewCommandEventArgs args)
        {
            int rowIndex = GetRowIndex(args);
            C1GridViewRow row = args.Row;

            if (row == null && rowIndex < Rows.Count)
            {
                row = Rows[rowIndex];
            }

            DataSourceView dataView = null;

            C1GridViewDeleteEventArgs c1args = new C1GridViewDeleteEventArgs(rowIndex);
            if (IsBoundUsingDataSourceID)
            {
                if ((dataView = GetData()) == null)
                {
                    throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ENullView"), ID));
                }

                if (row != null)
                {
                    ExtractRowValues(c1args.Values, row, true, false);
                }

                ArrayList dataKeys = DataKeysInternal;
                if (rowIndex < dataKeys.Count)
                {
                    foreach (DictionaryEntry entry in ((DataKey)dataKeys[rowIndex]).Values)
                    {
                        c1args.Keys.Add(entry.Key, entry.Value);

                        if (c1args.Values.Contains(entry.Key))
                        {
                            c1args.Values.Remove(entry.Key);
                        }
                    }
                }
            }

            OnRowDeleting(c1args);

            if (!c1args.Cancel)
            {
                if (IsBoundUsingDataSourceID)
                {
                    dataView.Delete(c1args.Keys, c1args.Values, (affectedRows, ex) =>
                    {
                        C1GridViewDeletedEventArgs deletedArgs = new C1GridViewDeletedEventArgs(affectedRows, ex, c1args.Keys, c1args.Values);
                        OnRowDeleted(deletedArgs);

                        if (ex != null && !deletedArgs.ExceptionHandled)
                        {
                            return false;
                        }

                        EditIndex = -1;

                        if (affectedRows > 0)
                        {
                            int rowsCount = ViewStateItemCount();

                            if (AllowPaging)
                            {
                                rowsCount += PageSize * (PageCount - 1);
                            }

                            int lastIndex = (rowsCount - affectedRows) - 1;

                            if (AllowPaging)
                            {
                                if (lastIndex < 0)
                                {
                                    lastIndex = 0;
                                }

                                int lastPage = lastIndex / PageSize;

                                if (lastPage < PageIndex)
                                {
                                    PageIndex = lastPage;
                                }
                            }

                            if (SelectedIndex > lastIndex)
                            {
                                SelectedIndex = lastIndex;
                            }
                        }

                        RequiresDataBinding = true;

                        return true;
                    });
                }
            }
        }

        private void HandleEdit(C1GridViewCommandEventArgs args)
        {
            C1GridViewEditEventArgs c1args = new C1GridViewEditEventArgs(GetRowIndex(args));

            OnRowEditing(c1args);

            if (!c1args.Cancel)
            {
                if (IsBoundUsingDataSourceID)
                {
                    EditIndex = c1args.NewEditIndex;
                }

                RequiresDataBinding = true;
            }
        }

        private void HandleFilter(C1GridViewCommandEventArgs args)
        {
            C1GridViewFilterEventArgs filteringArgs = new C1GridViewFilterEventArgs();
            Dictionary<C1FilterField, object[]> oldFilterInfo = new Dictionary<C1FilterField, object[]>();

            foreach (C1BaseField field in Columns.Traverse())
            {
                C1FilterField boundField = field as C1FilterField;
                if (boundField != null)
                {
                    oldFilterInfo.Add(boundField, new object[] { boundField.FilterOperator, boundField.FilterValue }); // store filter properties

                    // collect changes

                    string filterValue = boundField.NewFilterValue != null
                        ? boundField.NewFilterValue.ToString()
                        : boundField.FilterValue;

                    if (filteringArgs.Values.Contains(boundField.DataField))
                    {
                        filteringArgs.Values[boundField.DataField] = filterValue;
                    }
                    else
                    {
                        filteringArgs.Values.Add(boundField.DataField, filterValue);
                    }

                    boundField.FilterValue = filterValue; // update filterValue property


                    FilterOperator filterOperator = boundField.NewFilterOperator != null
                        ? boundField.NewFilterOperator.Value
                        : boundField.FilterOperator;

                    if (filteringArgs.Operators.Contains(boundField.DataField))
                    {
                        filteringArgs.Operators[boundField.DataField] = filterOperator;
                    }
                    else
                    {
                        filteringArgs.Operators.Add(boundField.DataField, filterOperator);
                    }

                    boundField.FilterOperator = filterOperator; // update filterOperator property.
                }
            }

            OnFiltering(filteringArgs);

            if (!filteringArgs.Cancel)
            {
                foreach (C1BaseField field in Columns.Traverse())
                {
                    C1FilterField boundField = field as C1FilterField;
                    if (boundField != null)
                    {
                        object filterOperator = filteringArgs.Operators[boundField.DataField];
                        if (filterOperator != null)
                        {
                            boundField.FilterOperator = (FilterOperator)filterOperator;
                        }

                        object filterValue = filteringArgs.Values[boundField.DataField];
                        if (filterValue != null)
                        {
                            boundField.FilterValue = (string)filterValue;
                        }
                    }
                }

                if (IsBoundUsingDataSourceID)
                {
                    _editIndex = DEF_EDITINDEX;
                    _pageIndex = DEF_PAGEINDEX;

                    ClearDataKeys();
                }

                //_scrollingState = new ScrollingState(); // reset
                _scrollingState.Reset();

                RequiresDataBinding = true;

                OnFiltered(EventArgs.Empty);
            }
            else
            {
                // restore filter properties
                foreach (KeyValuePair<C1FilterField, object[]> info in oldFilterInfo)
                {
                    info.Key.FilterOperator = (FilterOperator)info.Value[0];
                    info.Key.FilterValue = (string)info.Value[1];
                }
            }
        }

        private void HandleGroup(C1GridViewCommandEventArgs args)
        {
            string[] arguments = ((string)args.CommandArgument).Split(':');

            C1Field drag = (C1Field)Columns.GetColumnByTravIdx(int.Parse(arguments[0]));
            C1Field drop = null;
            int dropGroupIndex = int.Parse(arguments[3]);
            C1GridViewDropPosition dropPosition = C1GridViewDropPosition.Left;
            float resultIndex = 0;
            int destIdx = 0;
            if (dropGroupIndex != -1)
            {
                int dragGroupIndex = int.Parse(arguments[1]);
                drop = (C1Field)Columns.GetColumnByTravIdx(int.Parse(arguments[2]));
                dropPosition = (C1GridViewDropPosition)Enum.Parse(typeof(C1GridViewDropPosition), arguments[4], true);
                switch (dropPosition)
                {
                    case C1GridViewDropPosition.Left:
                        resultIndex = dropGroupIndex - 0.5f;
                        destIdx = dropGroupIndex - 1;
                        if (dragGroupIndex == -1 || dragGroupIndex >= dropGroupIndex)
                        {
                            destIdx++;
                        }
                        break;
                    case C1GridViewDropPosition.Right:
                        resultIndex = dropGroupIndex + 0.5f;
                        destIdx = dropGroupIndex;
                        if (dragGroupIndex == -1 || dragGroupIndex >= dropGroupIndex)
                        {
                            destIdx++;
                        }
                        break;
                }
            }

            C1GridViewColumnGroupEventArgs c1args = new C1GridViewColumnGroupEventArgs(drag, drop, dropPosition, destIdx);

            OnColumnGrouping(c1args);

            if (!c1args.Cancel)
            {
                drag.GroupedIndex = resultIndex;
                drag.GroupInfo.Position = GroupPosition.Header;

                OnColumnGrouped(new C1GridViewColumnGroupedEventArgs(drag, drop, dropPosition, destIdx));

                RequiresDataBinding = true;
            }
        }

        private void HandleMove(C1GridViewCommandEventArgs args)
        {
            string[] arguments = ((string)args.CommandArgument).Split(':');

            C1BaseField drag = Columns.GetColumnByTravIdx(int.Parse(arguments[0]));
            C1BaseField drop = Columns.GetColumnByTravIdx(int.Parse(arguments[1]));
            C1GridViewDropPosition dropPosition = (C1GridViewDropPosition)Enum.Parse(typeof(C1GridViewDropPosition), arguments[2], true);

            C1BaseFieldCollection dragFrom = ((IColumnsContainer)drag.Owner).Columns;
            C1BaseFieldCollection dropTo = (dropPosition == C1GridViewDropPosition.Center)
                ? ((IColumnsContainer)drop).Columns
                : ((IColumnsContainer)drop.Owner).Columns;

            int destIdx = -1;
            int dragIndex = dragFrom.IndexOf(drag);
            int dropIndex = dropTo.IndexOf(drop);
            switch (dropPosition)
            {
                case C1GridViewDropPosition.Center:
                    destIdx = dropTo.Count;
                    break;

                case C1GridViewDropPosition.Left:
                    destIdx = dropIndex - 1;
                    if (dragFrom != dropTo || dragIndex >= dropIndex)
                    {
                        destIdx++;
                    }
                    break;

                case C1GridViewDropPosition.Right:
                    destIdx = dropIndex;
                    if (dragFrom != dropTo || dragIndex >= dropIndex)
                    {
                        destIdx++;
                    }
                    break;
            }

            C1GridViewColumnMoveEventArgs c1args = new C1GridViewColumnMoveEventArgs(drag, drop, dropPosition, destIdx);

            OnColumnMoving(c1args);

            if (!c1args.Cancel)
            {
                dragFrom.Remove(drag);
                dropTo.Insert(destIdx, drag);

                OnColumnMoved(new C1GridViewColumnMovedEventArgs(drag, drop, dropPosition, destIdx));

                RequiresDataBinding = true;
            }
        }

        private void HandlePage(C1GridViewCommandEventArgs args)
        {
            int newIndex = PageIndex;
            string pageAction = ((string)args.CommandArgument).ToLowerInvariant();

            switch (pageAction)
            {
                /*case "first":
					newIndex = 0;
					break;

				case "prev":
					newIndex--;
					break;

				case "next":
					newIndex++;
					break;

				case "last":
					newIndex = PageCount;
					break;*/

                default:
                    newIndex = int.Parse(pageAction)/* - 1*/;
                    break;
            }

            // TFS 431829
            // Need place before OnPageIndexChanging()
            if (!IsBoundUsingDataSourceID)
                ClearDataKeys();

            C1GridViewPageEventArgs ea = new C1GridViewPageEventArgs(newIndex);
            OnPageIndexChanging(ea);

            if (!ea.Cancel)
            {
                if (IsBoundUsingDataSourceID)
                {
                    ClearDataKeys();
                    EditIndex = -1;
                    PageIndex = ea.NewPageIndex;
                }

                OnPageIndexChanged(EventArgs.Empty);
                RequiresDataBinding = true;
            }
        }

        private void HandleSelect(C1GridViewCommandEventArgs args)
        {
            C1GridViewSelectEventArgs c1args = new C1GridViewSelectEventArgs(GetRowIndex(args));

            OnSelectedIndexChanging(c1args);

            if (!c1args.Cancel)
            {
                SelectedIndex = c1args.NewSelectedIndex;
                OnSelectedIndexChanged(EventArgs.Empty);
            }
        }

        private void HandleSort(C1GridViewCommandEventArgs args)
        {
            string[] eventArgs = ((string)args.CommandArgument).Split(new char[] { ':' }, 2); // A field name can contain ':' character.

            int travIdx = int.Parse(eventArgs[0]);
            string sortExpression = eventArgs[1];

            C1Field fieldToSort = (travIdx >= 0)
                ? (C1Field)Columns.GetColumnByTravIdx(travIdx)
                : null;

            C1SortDirection sortDirection = (fieldToSort != null)
                ? ToggleSortDirection(fieldToSort.SortDirection)
                : ToggleSortDirection(_autoSortDirection);

            Dictionary<C1Field, object[]> oldSortInfo = new Dictionary<C1Field, object[]>();
            string oldAutoSortExpression = _DEF_AUTOSORTEXPR;
            C1SortDirection oldAutoSortDirection = _DEF_AUTOSORTDIR;

            if (!IsBoundUsingDataSourceID)
            {
                // store old settings
                oldAutoSortExpression = _autoSortExpression;
                oldAutoSortDirection = _autoSortDirection;

                foreach (C1BaseField test in Columns.Traverse())
                {
                    C1Field field = test as C1Field;
                    if (field != null)
                    {
                        oldSortInfo.Add(field, new object[] { field.SortDirection, field.SortExpression });
                    }
                }

                // apply new settings before re-binding in Sorting event handler in case of custom databinding.
                ApplySortingProperties(fieldToSort, sortExpression, sortDirection);
            }

            C1GridViewSortEventArgs sortingArguments = new C1GridViewSortEventArgs(sortExpression, sortDirection);
            OnSorting(sortingArguments);

            if (!sortingArguments.Cancel)
            {
                // apply new settings
                ApplySortingProperties(fieldToSort, sortingArguments.SortExpression, sortingArguments.SortDirection);

                if (IsBoundUsingDataSourceID)
                {
                    ClearDataKeys();

                    if (this.GetData() == null)
                    {
                        throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ENullView"), ID));
                    }

                    EditIndex = -1;
                    PageIndex = 0;
                }

                OnSorted(EventArgs.Empty);
                RequiresDataBinding = true;
            }
            else
            {
                if (!IsBoundUsingDataSourceID)
                {
                    // rollback changes
                    _autoSortDirection = oldAutoSortDirection;
                    _autoSortExpression = oldAutoSortExpression;

                    foreach (KeyValuePair<C1Field, object[]> info in oldSortInfo)
                    {
                        info.Key.SortDirection = (C1SortDirection)info.Value[0];
                        info.Key.SortExpression = (string)info.Value[1];
                    }
                }
            }
        }

        private C1DetailGridView FindDetail(HierarchyPath path, bool moveDeeperWithItem = false)
        {
            C1GridView result = this;

            foreach (int value in path.Path)
            {
                result = ((C1GridViewDataRow)result.Rows[value]).DetailRow.DetailGridView;
            }

            if (moveDeeperWithItem)
            {
                result = ((C1GridViewDataRow)result.Rows[path.DataItemIndex]).DetailRow.DetailGridView;
            }

            return (C1DetailGridView)result;
        }

        private void HandleToggleHierarchy(C1GridViewCommandEventArgs args)
        {
            HierarchyPath path = new HierarchyPath((string)args.CommandArgument);
            C1GridViewDataRow toggleRow = (C1GridViewDataRow)this.Rows[path.DataItemIndex];

            if (toggleRow != null)
            {
                DataKey dataKey = this is C1DetailGridView
                    ? ((C1DetailGridView)this).MasterDataKey
                    : DataKeys[toggleRow.RowIndex];

                C1GridViewHierarchyRowTogglingEventArgs togglingArgs = new C1GridViewHierarchyRowTogglingEventArgs(toggleRow.RowIndex,
                    !toggleRow.Expanded,
                    dataKey);

                OnHierarchyRowToggling(togglingArgs);

                if (!togglingArgs.Cancel)
                {
                    toggleRow.Expanded = !toggleRow.Expanded;

                    OnHierarchyRowToggled(new C1GridViewHierarchyRowToggledEventArgs(toggleRow.RowIndex, toggleRow.Expanded, dataKey));

                    this.RequiresDataBinding = true;
                }
            }
        }
        private void HandleUngroup(C1GridViewCommandEventArgs args)
        {
            string[] arguments = ((string)args.CommandArgument).Split(':');

            C1Field column = (C1Field)Columns.GetColumnByTravIdx(int.Parse(arguments[0]));

            C1GridViewColumnUngroupEventArgs c1args = new C1GridViewColumnUngroupEventArgs(column);

            OnColumnUngrouping(c1args);

            if (!c1args.Cancel)
            {
                column.GroupedIndex = -1;
                column.GroupInfo.Position = GroupPosition.None;

                OnColumnUngrouped(new C1GridViewColumnUngroupedEventArgs(column));

                RequiresDataBinding = true;
            }
        }

        private void HandleUpdate(C1GridViewCommandEventArgs args, bool causesValidation)
        {
            if (!causesValidation || Page == null || Page.IsValid)
            {
                DataSourceView dataVew = null;

                if (IsBoundUsingDataSourceID)
                {
                    dataVew = GetData();

                    if (dataVew == null)
                    {
                        throw new HttpException(string.Format(C1Localizer.GetString("C1Grid.ENullView"), ID));
                    }
                }

                C1GridViewUpdateEventArgs ea = new C1GridViewUpdateEventArgs(GetRowIndex(args));

                if (IsBoundUsingDataSourceID)
                {
                    foreach (DictionaryEntry entry in EditRowValues)
                    {
                        ea.OldValues.Add(entry.Key, entry.Value);
                    }

                    if (ea.RowIndex < DataKeys.Count)
                    {
                        foreach (DictionaryEntry entry in DataKeys[ea.RowIndex].Values)
                        {
                            ea.Keys.Add(entry.Key, entry.Value);
                        }
                    }

                    ExtractRowValues(ea.NewValues, args.Row, false, true);
                }

                OnRowUpdating(ea);

                if (!ea.Cancel && IsBoundUsingDataSourceID)
                {
                    dataVew.Update(ea.Keys, ea.NewValues, ea.OldValues, (affectedRows, ex) =>
                    {
                        C1GridViewUpdatedEventArgs uea = new C1GridViewUpdatedEventArgs(affectedRows, ex, ea.Keys, ea.NewValues, ea.OldValues);

                        OnRowUpdated(uea);

                        if (ex != null && !uea.ExceptionHandled)
                        {
                            return false;
                        }

                        if (!uea.KeepInEditMode)
                        {
                            EditIndex = -1;
                            RequiresDataBinding = true;
                        }

                        return true;
                    });
                }
            }
        }

        private void ApplySortingProperties(C1Field fieldToSort, string sortExpression, C1SortDirection sortDirection)
        {
            if (fieldToSort != null)
            {
                fieldToSort.SortDirection = sortDirection;
                fieldToSort.SortExpression = sortExpression;

                _autoSortExpression = _DEF_AUTOSORTEXPR;
                _autoSortDirection = _DEF_AUTOSORTDIR;
            }
            else
            {
                _autoSortExpression = sortExpression;
                _autoSortDirection = sortDirection;
            }

            // ** reset sortDirection for all columns except sorted one and grouped ones.
            /*if (fieldToSort == null || !fieldToSort.Grouped)
			{*/
            for (int i = 0; i < _leaves.Count; i++)
            {
                C1Field cur = _leaves[i] as C1Field;
                if (cur != null && cur != fieldToSort && cur.GroupInfo.Position == GroupPosition.None)
                {
                    if (fieldToSort != null)
                    {
                        cur.SortDirection = C1SortDirection.None;
                    }
                    else
                    {
                        if (!cur.IsAutogenerated || (cur.SortExpression != sortExpression))
                        {
                            cur.SortDirection = C1SortDirection.None;
                        }
                    }
                }
            }
            /*}*/
        }

        #endregion

        #region Control state

        /// <summary>
        /// Infrastructure.
        /// Loads the state of the properties in the <see cref="C1GridView"/> control that need to be persisted, even when <b>Control.EnableViewState</b> property is set to false.
        /// </summary>
        /// <param name="savedState"></param>
        protected override void LoadControlState(object savedState)
        {
            if (_jsonControlStateLoaded)
            {
                return; // don't overwrite the properties if controlState was loaded from json.
            }

            _dataKeyNames = new string[0];
            _editIndex = DEF_EDITINDEX;
            _pageIndex = DEF_PAGEINDEX;
            _pageCount = _DEF_PAGECOUNT;
            _selectedIndex = DEF_SELECTEDINDEX;
            _autoSortExpression = _DEF_AUTOSORTEXPR;
            _autoSortDirection = _DEF_AUTOSORTDIR;

            object[] stateObj = savedState as object[];
            if (stateObj != null)
            {
                base.LoadControlState(stateObj[0]);

                object obj;

                if ((obj = stateObj[1]) != null)
                {
                    _dataKeyNames = (string[])obj;
                }

                if ((obj = stateObj[2]) != null)
                {
                    LoadDataKeysState(obj);
                }

                if ((obj = stateObj[3]) != null)
                {
                    _editIndex = (int)obj;
                }

                if ((obj = stateObj[4]) != null)
                {
                    _pageIndex = (int)obj;
                }

                if ((obj = stateObj[5]) != null)
                {
                    _pageCount = (int)obj;
                }

                if ((obj = stateObj[6]) != null)
                {
                    _selectedIndex = (int)obj;
                }

                if ((obj = stateObj[7]) != null)
                {
                    _autoSortExpression = (string)obj;
                }

                if ((obj = stateObj[8]) != null)
                {
                    _autoSortDirection = (C1SortDirection)obj;
                }

                if (((obj = stateObj[9]) != null) && (_dataKeyNames != null) && (_dataKeyNames.Length > 0))
                {
                    _persistedDataKey = new DataKey(new OrderedDictionary(_dataKeyNames.Length), _dataKeyNames);
                    ((IStateManager)_persistedDataKey).LoadViewState(obj);
                }

                _fieldInfoViewState = stateObj[10];

                if ((obj = stateObj[11]) != null) // only if grid is a root.
                {
                    HierarchyTree.Deserialize((string)obj);
                }

                if ((obj = stateObj[12]) != null)
                {
                    OrderedDictionaryHelper.LoadViewState(EditRowValues, (ArrayList)obj);
                }
            }
            else
            {
                base.LoadControlState(null);
            }

            if (!_jsonStateLoaded
                && !_jsonControlStateLoading) // to avoid recursion.
            {
                RestoreJsonState();
            }
        }

        /// <summary>
        /// Infrastructure.
        /// Saves the state of the properties in the <see cref="C1GridView"/> control that need to be persisted, even when the <b>Control.EnableViewState</b> property is set to false.
        /// </summary>
        /// <returns></returns>
        protected override object SaveControlState()
        {
            string hierarchy = IsRoot() ? HierarchyTree.Serialize() : null;

            object baseState = base.SaveControlState();

            if ((baseState == null) &&
                (_dataKeyNames == null || _dataKeyNames.Length == 0) &&
                (_dataKeysInternal == null || _dataKeysInternal.Count == 0) &&
                (_editIndex == DEF_EDITINDEX) &&
                (_pageIndex == DEF_PAGEINDEX) &&
                (_pageCount == _DEF_PAGECOUNT) &&
                (_selectedIndex == DEF_SELECTEDINDEX) &&
                (_autoSortExpression == _DEF_AUTOSORTEXPR) &&
                (_autoSortDirection == _DEF_AUTOSORTDIR) &&
                (_c1ds == null || _c1ds.FieldInfo == null || _c1ds.FieldInfo.Count == 0) &&
                (hierarchy == null) &&
                (_editRowValues == null || _editRowValues.Count == 0))
            {
                return true; // trigger LoadControlState anyway.
            }
            else
            {
                return new object[] {
                    baseState,
                    (_dataKeyNames == null || _dataKeyNames.Length == 0) ? null : _dataKeyNames,
                    (_dataKeysInternal == null || _dataKeysInternal.Count == 0) ? null :  ((IStateManager)DataKeys).SaveViewState(),
                    _editIndex == DEF_EDITINDEX ? null : (object)_editIndex,
                    _pageIndex == DEF_PAGEINDEX ? null : (object)_pageIndex,
                    _pageCount == _DEF_PAGECOUNT ? null : (object)_pageCount,
                    _selectedIndex == DEF_SELECTEDINDEX ? null : (object)_selectedIndex,
                    _autoSortExpression == _DEF_AUTOSORTEXPR ? null : _autoSortExpression,
                    _autoSortDirection == _DEF_AUTOSORTDIR ? null : (object)_autoSortDirection,
                    _persistedDataKey == null ? null : ((IStateManager)_persistedDataKey).SaveViewState(),
                    _c1ds == null || _c1ds.FieldInfo == null || _c1ds.FieldInfo.Count == 0 ? null : ((IStateManager)_c1ds.FieldInfo).SaveViewState(),
                    hierarchy,
                    _editRowValues == null || _editRowValues.Count == 0 ? null : OrderedDictionaryHelper.SaveViewState(_editRowValues)
                };
            }
        }

        #endregion

        #region IStateManager

        /// <summary>
        /// Restores the previously saved view state of the <see cref="C1GridView"/> control.
        /// </summary>
        /// <param name="savedState">An object that represents the <see cref="C1GridView"/> view state to restore.</param>
        protected override void LoadViewState(object savedState)
        {
            if (_jsonViewStateLoaded)
            {
                return; // don't overwrite the properties if viewState was loaded from json.
            }

            // important! same properties can exist both in view state and json.
            // So we need to restore the control state from viewstate and then overwrite it with json version.

            if (savedState != null)
            {
                object[] stateObj = (object[])savedState;
                object state;

                base.LoadViewState(stateObj[0]);

                if ((state = stateObj[1]) != null)
                {
                    ((IStateManager)AlternatingRowStyle).LoadViewState(state);
                }

                if ((state = stateObj[2]) != null)
                {
                    ((IStateManager)EditRowStyle).LoadViewState(state);
                }

                if ((state = stateObj[3]) != null)
                {
                    ((IStateManager)EmptyDataRowStyle).LoadViewState(state);
                }

                if ((state = stateObj[4]) != null)
                {
                    ((IStateManager)FilterStyle).LoadViewState(state);
                }

                if ((state = stateObj[5]) != null)
                {
                    ((IStateManager)FooterStyle).LoadViewState(state);
                }

                if ((state = stateObj[6]) != null)
                {
                    ((IStateManager)HeaderStyle).LoadViewState(state);
                }

                if ((state = stateObj[7]) != null)
                {
                    ((IStateManager)RowStyle).LoadViewState(state);
                }

                if ((state = stateObj[8]) != null)
                {
                    ((IStateManager)SelectedRowStyle).LoadViewState(state);
                }

                if (((state = stateObj[9]) != null) && PersistDetailProperty)
                {
                    ((IStateManager)Detail).LoadViewState(state);
                }
            }

            if (!_jsonStateLoaded
                && !_jsonViewStateLoading) // to avoid recursion.
            {
                RestoreJsonState();
            }
        }

        /// <summary>
        /// Saves the changes to the <see cref="C1GridView"/> object since the time the page was posted back to the server.
        /// </summary>
        /// <returns>The object that contains the changes to the view state of the <see cref="C1GridView"/>.</returns>
        protected override object SaveViewState()
        {
            System.Diagnostics.Debug.WriteLine("SaveViewState");

            object[] state = new object[] {
                base.SaveViewState(),
                (_alternatingRowStyle != null) ? ((IStateManager)_alternatingRowStyle).SaveViewState() : null,
                (_editRowStyle != null) ? ((IStateManager)_editRowStyle).SaveViewState() : null,
                (_emptyDataRowStyle != null) ? ((IStateManager)_emptyDataRowStyle).SaveViewState() : null,
                (_filterStyle != null) ? ((IStateManager)_filterStyle).SaveViewState() : null,
                (_footerStyle != null) ? ((IStateManager)_footerStyle).SaveViewState() : null,
                (_headerStyle != null) ? ((IStateManager)_headerStyle).SaveViewState() : null,
                (_rowStyle != null) ? ((IStateManager)_rowStyle).SaveViewState() : null,
                (_selectedRowStyle != null) ? ((IStateManager)_selectedRowStyle).SaveViewState() : null,
                (_detail != null && PersistDetailProperty) ? ((IStateManager)_detail).SaveViewState(): null
            };

            return Array.TrueForAll(state, v => v == null)
                ? null
                : state;
        }

        /// <summary>
        /// Causes the <see cref="C1GridView"/> object to track changes to its state so that it can be persisted across requests.
        /// </summary>
        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (_userColumns != null)
            {
                ((IStateManager)_userColumns).TrackViewState();
            }

            if (_dataKeys != null)
            {
                ((IStateManager)_dataKeys).TrackViewState();
            }

            if (_autogeneratedColumns != null)
            {
                foreach (C1BaseField field in _autogeneratedColumns)
                {
                    ((IStateManager)field).TrackViewState();
                }
            }

            if (_alternatingRowStyle != null)
            {
                ((IStateManager)_alternatingRowStyle).TrackViewState();
            }

            if (_editRowStyle != null)
            {
                ((IStateManager)_editRowStyle).TrackViewState();
            }

            if (_emptyDataRowStyle != null)
            {
                ((IStateManager)_emptyDataRowStyle).TrackViewState();
            }

            if (_filterStyle != null)
            {
                ((IStateManager)_filterStyle).TrackViewState();
            }

            if (_footerStyle != null)
            {
                ((IStateManager)_footerStyle).TrackViewState();
            }

            if (_headerStyle != null)
            {
                ((IStateManager)_headerStyle).TrackViewState();
            }

            if (_rowStyle != null)
            {
                ((IStateManager)_rowStyle).TrackViewState();
            }

            if (_selectedRowStyle != null)
            {
                ((IStateManager)_selectedRowStyle).TrackViewState();
            }

            if (_detail != null && PersistDetailProperty)
            {
                ((IStateManager)_detail).TrackViewState();
            }
        }

        #endregion

        #region IPostBackContainer Members

        PostBackOptions IPostBackContainer.GetPostBackOptions(IButtonControl buttonControl)
        {
            if (buttonControl != null)
            {
                PostBackOptions pbo = new PostBackOptions(this, string.Format("{0}:{1}", buttonControl.CommandName, buttonControl.CommandArgument));
                pbo.RequiresJavaScriptProtocol = true;
                return pbo;
            }

            throw new ArgumentNullException("buttonControl");
        }

        #endregion

        #region IPostBackEventHandler Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventArgument"></param>
        protected virtual void RaisePostBackEvent(string eventArgument)
        {
            if (Page != null)
            {
                if (!IsGridCommand(eventArgument))
                {
                    Page.ClientScript.ValidateEvent(UniqueID, eventArgument);
                }
            }

            string[] arguments = eventArgument.Split(new char[] { ':' }, 2);

            if (arguments[0].Length > 0)
            {
                CommandEventArgs originalArgs = new CommandEventArgs(arguments[0], arguments.Length > 1 ? arguments[1] : string.Empty);
                C1GridViewCommandEventArgs c1args = new C1GridViewCommandEventArgs(null, this, originalArgs);
                HandleEvent(c1args, false, string.Empty);
            }
        }

        private bool IsGridCommand(string commandName)
        {
            return _eventArgumentTest.IsMatch(commandName.ToLower());
        }

        private bool IsRowCommand(string commandName)
        {
            return commandName == CMD_CANCEL || commandName == CMD_DELETE || commandName == CMD_EDIT || commandName == CMD_SELECT || commandName == CMD_UPDATE;
        }

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            this.RaisePostBackEvent(eventArgument);
        }

        #endregion

        #region IPostBackDataHandler Members

        /// <summary>
        /// Processes post-back data from a server control.
        /// </summary>
        /// <param name="postDataKey">Required. The key identifier for the control.</param>
        /// <param name="postCollection">Required. The collection of all incoming name values.</param>
        /// <returns>
        /// True if the server control's state changes as a result of the post back; otherwise False.
        /// </returns>
        protected virtual bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            if (!_jsonStateLoaded) // control was added to the page after initialization stage.
            {
                RestoreJsonState();
                CreateLeaves(_c1ds, true);
            }

            return false;
        }

        /// <summary>
        /// Enables a server control to process an event raised when a form is posted to the server.
        /// </summary>
        protected virtual void RaisePostDataChangedEvent()
        {
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

        #endregion

        #region IJsonRestore Members

        /// <summary>
        /// Infrastructure.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Json(true, true, null)]
        [Layout(LayoutType.None)]
        public Hashtable InnerState
        {
            get
            {
                // ** performance improvement
                // we don't need to deserialize this property automatically because the needful properties will be deserialized manually in the this.RestoreStateFromJson().
                // so make sure that the real object will be created only once, during JSON serialazation (rendering + callback rendering).
                if (!_createInnerState)
                {
                    return null;
                }

                if (_innerState == null) {
                    // Note: properties prefixed with the "_" character are excluded from the callback request (see c1gridview.onwijstatesaving())

                    _innerState = new EmptiableHashtable();
                    Hashtable ht = _innerState; // shortcut

                    if (_autogeneratedColumns != null && _autogeneratedColumns.Count > 0)
                    {
                        ht["autogeneratedColumns"] = _autogeneratedColumns;
                    }

                    ht["_culture"] = System.Globalization.CultureInfo.CurrentCulture.Name;

                    ht["_uid"] = UniqueID;
                    ht["_cid"] = ClientID;
                    ht["dbf"] = _dataBindFlag;
                    ht["scrollingState"] = _scrollingState;
#if ASP_NET4
                    if (WebControl.DisabledCssClass != "AspNetDisabled")
                    {
                        ht["_dc"] = WebControl.DisabledCssClass;
                    }
#endif
                    if (HasDetails())
                    {
                        ht["_hd"] = 1;
                    }

                    ht["_bodyEditIndex"] = (ChildControlsCreated && EditIndex >= 0 && EditIndex < Rows.Count)
                        ? Rows[EditIndex].BodyRowIndex
                        : -1;

                    ht["_bodySelectedIndex"] = (ChildControlsCreated && SelectedIndex >= 0 && SelectedIndex < Rows.Count)
                        ? Rows[SelectedIndex].BodyRowIndex
                        : -1;

                    if (IsRoot())
                    {
                        ht["_root"] = 1;
                    }

                    List<FilterLocalization> filterDisplayNames = GetLocalizedFilterDisplayNames();
                    if (filterDisplayNames.Count > 0)
                    {
                        ht["_filterDisplayNames"] = filterDisplayNames;
                    }

                    if (Page != null && !DesignMode)
                    {
                        ht["_pbRef"] = Page.ClientScript.GetPostBackEventReference(this, "{0}", false);

                        if (SerializeStateProperties)
                        {
                            object state = SaveControlState();
                            if (state != null)
                            {
                                ht["controlState"] = new ObjectStateFormatter().Serialize(state); // transfer controlState between callbacks
                            }

                            state = SaveViewState();
                            if (state != null)
                            {
                                ht["viewState"] = new ObjectStateFormatter().Serialize(state); // transfer viewState between callbacks
                            }
                        }
                    }

                    int editIndex = this.EditIndex;
                    if (AllowC1InputEditors && editIndex >= 0 && ChildControlsCreated && (_InternalAllowVVirtualScrolling() || (IsCallbacksEnabled && ActualCallbackMode != CallbackMode.Full)))
                    {
                        C1GridViewRow editRow = this.Rows[editIndex];

                        string editRowInitScript = GetChildInitializationScripts<Control>(editRow); // to initialize server-side c1input widgets during partial callbacks or virtual scrolling (both client and server-side modes)

                        ht["_cbEditRowInitScript"] = editRowInitScript;

                        if (_InternalAllowVVirtualScrolling() && !CallbackScrolling) // client-side virtual scrolling
                        {
                            IOrderedDictionary c1editorMarkup = GetC1InplaceEditorsMarkup(editRow); // 79648: store C1Inputs markup to recreate them properly during client-side rendering 
                            ht["_c1InplaceEditorsMarkup"] = c1editorMarkup;
                        }
                    }
                }

                return _innerState;
            }
        }

        /// <summary>
        /// Restore the states from the JSON object.
        /// </summary>
        /// <param name="state">The state which is parsed from the JSON string.</param>
        protected override void RestoreStateFromJson(object state)
        {
            Hashtable ht = state as Hashtable;
            if (ht != null)
            {
                object val;

                if ((val = ht["innerState"]) != null)
                {
                    Hashtable innerState = (Hashtable)val;

                    _dataBindFlag = (bool)innerState["dbf"];

                    if ((val = innerState["scrollingState"]) != null)
                    {
                        this._scrollingState.DeSerializeValue(val);
                    }

                    // restore autogenerated columns
                    if ((val = innerState["autogeneratedColumns"]) != null)
                    {
                        _autogeneratedColumns = new C1BaseFieldCollection(this);
                        ((IJsonRestore)_autogeneratedColumns).RestoreStateFromJson(val);
                    }

                    // restore control state
                    if (!string.IsNullOrEmpty((string)innerState["controlState"]))
                    {
                        try
                        {
                            _jsonControlStateLoading = true;
                            LoadControlState(new ObjectStateFormatter().Deserialize((string)innerState["controlState"]));
                        }
                        finally
                        {
                            _jsonControlStateLoading = false;
                            _jsonControlStateLoaded = true;
                        }
                    }

                    // restore view state
                    if (!string.IsNullOrEmpty((string)innerState["viewState"]))
                    {
                        try
                        {
                            _jsonViewStateLoading = true;
                            TrackViewState();
                            LoadViewState(new ObjectStateFormatter().Deserialize((string)innerState["viewState"]));
                        }
                        finally
                        {
                            _jsonViewStateLoading = false;
                            _jsonViewStateLoaded = true;
                        }
                    }
                }

                _batchValues = ht["batchValues"] as Hashtable;
            }

            try
            {
                _ignoreDataBinding = true;

                Dictionary<int, object> nonpersistables = new Dictionary<int, object>();

                int traverseIndex = 0;
                foreach (C1BaseField field in Columns.Traverse())
                {
                    if (field.NonPersistables)
                    {
                        // at this step columns are just restored from an aspx, so traverse index is equal to original index.
                        nonpersistables.Add(traverseIndex, field.SaveNonPersistables());
                    }
                    traverseIndex++;
                }

                base.RestoreStateFromJson(state);

                foreach (C1BaseField field in Columns.Traverse())
                {
                    if (nonpersistables.ContainsKey(field.OriginalIndex))
                    {
                        field.LoadNonPersistables(nonpersistables[field.OriginalIndex]);
                    }
                    if (typeof(C1FilterField).IsAssignableFrom(field.GetType()))
                    {
                        var filterField = (C1FilterField)field;
                        if (filterField.DataType == "string")
                        {
                            filterField.FilterValue = Page.Server.HtmlDecode(filterField.FilterValue);
                            filterField.NewFilterValue = Page.Server.HtmlDecode(filterField.NewFilterValue);
                        }
                    }
                }
            }
            finally
            {
                _ignoreDataBinding = false;
            }
        }

        #endregion

        #region IPersistedSelector Members

        DataKey IPersistedSelector.DataKey
        {
            get { return SelectedPersistedDataKey; }
            set { SelectedPersistedDataKey = value; }
        }

        #endregion

        #region IC1Serializable Members

        /// <summary>
        /// Saves control layout properties to the specified file.
        /// </summary>
        /// <param name="filename">XML file to save.</param>
        public void SaveLayout(string filename)
        {
            C1GridViewSerializer serializer = new C1GridViewSerializer(this);
            serializer.SaveLayout(filename);
        }

        /// <summary>
        /// Saves control layout properties to the specified stream.
        /// </summary>
        /// <param name="stream">Stream to save.</param>
        public void SaveLayout(System.IO.Stream stream)
        {
            C1GridViewSerializer serializer = new C1GridViewSerializer(this);
            serializer.SaveLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties from the file.
        /// </summary>
        /// <param name="filename">XML file to load.</param>
        public void LoadLayout(string filename)
        {
            LoadLayout(filename, LayoutType.All);
        }

        /// <summary>
        /// Loads control layout properties from the stream.
        /// </summary>
        /// <param name="stream">Stream to load.</param>
        public void LoadLayout(System.IO.Stream stream)
        {
            LoadLayout(stream, LayoutType.All);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the stream.
        /// </summary>
        /// <param name="filename">XML file to load.</param>
        /// <param name="layoutTypes">Layout types to load.</param>
        public void LoadLayout(string filename, LayoutType layoutTypes)
        {
            C1GridViewSerializer serializer = new C1GridViewSerializer(this);
            serializer.LoadLayout(filename, layoutTypes);

            CreateLeaves(null, false);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the file.
        /// </summary>
        /// <param name="stream">Stream to load.</param>
        /// <param name="layoutTypes">Layout types to load.</param> 
        public void LoadLayout(System.IO.Stream stream, LayoutType layoutTypes)
        {
            C1GridViewSerializer serializer = new C1GridViewSerializer(this);
            serializer.LoadLayout(stream, layoutTypes);

            CreateLeaves(null, false);
        }

        #endregion

        #region licensing support

        private void VerifyLicense()
        {
            var licInfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1GridView), this, false);
            _shouldNag = licInfo.ShouldNag;
#if GRAPECITY
			_isProductLicensed = licInfo.IsValid;
#else
            _isProductLicensed = licInfo.IsValid || licInfo.IsLocalHost;
#endif
        }

        #endregion

        #region native callbacks

        private CallbackMode ActualCallbackMode
        {
            get
            {
                if (_callbackRequest != null)
                {
                    switch (_callbackRequest.Command)
                    {
                        // can be completed with full update of grid's HTML content only.
                        case CMD_DELETE:
                        case CMD_FILTER:
                        case CMD_MOVE:
                        case CMD_GROUP:
                        case CMD_UNGROUP:
                        case CMD_PAGE:
                        case CMD_SORT:
                        case CMD_RENDER:
                            return CallbackMode.Full;

                        case CMD_VSCROLL:
                            return CallbackMode.Partial;

                        case CMD_HIERARCHY:
                            return CallbackMode.Partial;
                    }
                }

                return CallbackSettings.Mode;
            }
        }

        internal bool CallbackEditing
        {
            get { return ((CallbackSettings.Action & CallbackAction.Editing) != 0); }
        }

        internal bool CallbackFiltering
        {
            get { return ((CallbackSettings.Action & CallbackAction.Filtering) != 0); }
        }

        internal bool CallbackHierarchy
        {
            get { return ((CallbackSettings.Action & CallbackAction.Hierarchy) != 0); }
        }
        internal bool CallbackPaging
        {
            get { return (CallbackSettings.Action & CallbackAction.Paging) != 0; }
        }

        internal bool CallbackSelection
        {
            get { return ((CallbackSettings.Action & CallbackAction.Selection) != 0); }
        }

        internal bool CallbackSorting
        {
            get { return (CallbackSettings.Action & CallbackAction.Sorting) != 0; }
        }

        internal bool CallbackScrolling
        {
            get { return (CallbackSettings.Action & CallbackAction.Scrolling) != 0; }
        }

        private bool IsCallbacksEnabled
        {
            get
            {
                bool callbackSupported = !DesignMode && Page != null && Context != null && Page.Request.Browser.SupportsCallback;
                return callbackSupported && (CallbackSettings.Action != CallbackAction.None);
            }
        }

        internal bool ServerSideRowsScrollingAllowed()
        {
            return !AllowPaging && _InternalAllowVVirtualScrolling() && CallbackScrolling;
        }

        private bool HasGrouping()
        {
            if (_leaves != null)
            {
                C1BaseField groupingField = _leaves.Find(leaf =>
                {
                    C1Field field = leaf as C1Field;
                    return field != null
                        ? field.GroupInfo.Position != GroupPosition.None
                        : false;
                });

                return groupingField != null;
            }

            return false;
        }

        private bool HasMerging()
        {
            if (_leaves != null)
            {
                C1BaseField mergingField = _leaves.Find(leaf =>
                {
                    C1Field field = leaf as C1Field;
                    return field != null
                        ? field.RowMerge != RowMerge.None
                        : false;
                });

                return mergingField != null;
            }

            return false;
        }

        #region ICallbackContainer Members

        string ICallbackContainer.GetCallbackScript(IButtonControl buttonControl, string argument)
        {
            if (buttonControl == null)
            {
                throw new ArgumentNullException("buttonControl");
            }

            if (Page != null)
            {
                try
                {
                    Page.ClientScript.RegisterForEventValidation(this.UniqueID, argument); //#101254
                }
                catch { }
            }

            return string.Format(CALLBACK_EXPRESSION + "return false;", ClientID, argument);
        }

        #endregion

        #region ICallbackEventHandler Members

        /// <summary>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="argument"></param>
        /// <returns></returns>
        protected virtual string GetCallbackResult(string command, string argument)
        {
            ICallbackEntity responseEntity = null;

            if (command == CMD_VSCROLL)
            {
                responseEntity =  (CallbackEntity)GetVirtualScrollingCallbackResult(command, argument);
            }
            else
            {
                switch (ActualCallbackMode)
                {
                    case CallbackMode.Partial: // partial
                        if (command == CMD_HIERARCHY)
                        {
                            responseEntity = (CallbackHierarchyEntity)GetHierarchyCallbackResult(command, argument);
                        }
                        else
                        {
                            responseEntity = (PartialCallbackEntity)GetPartialCallbackResult(command, argument);
                        }

                        break;

                    case CallbackMode.Full: // full
                        responseEntity = (CallbackEntity)GetFullCallbackResult(command, argument);
                        break;
                }
            }

            AllowInnerStateCreation();

            C1GridView root = Root;
            if (root != null)
            {
                root.AllowInnerStateCreation();
            }

            CallbackResponse resultObj = new CallbackResponse
            {
                command = command,
                mode = ActualCallbackMode.ToString().ToLower(),
                entity = responseEntity,
                options = JsonHelper.WidgetToStringQuoteClientEvents(this, this),
                rootInnerState = IsRoot() ? null : JsonHelper.WidgetToStringQuoteClientEvents(root.InnerState, root) // transfer innerstate only
            };

            string result = JsonHelper.ObjectToString(resultObj, null);

            return result;
        }

        private int _partialPrevRowIndex = -1;
        private int _partialCurRowIndex = -1;

        /// <summary>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="argument"></param>
        protected virtual void RaiseCallbackEvent(string command, string argument)
        {
            string[] args;

            switch (command.ToLowerInvariant())
            {
                case CMD_CANCEL:
                case CMD_EDIT:
                    _partialPrevRowIndex = (_tmpRowCommandIndex != int.MinValue) // HandleEvent() has been called. This may happen if the row contains an element without ID (a checkbox inside the CheckboxField, for example) and during callback ASP.NET considers one of the CommandColumn buttons as an event target.
                        ? _tmpRowCommandIndex
                        : EditIndex;

                    RaisePostBackEvent(command + ":" + argument);
                    _partialCurRowIndex = Convert.ToInt32(argument, CultureInfo.InvariantCulture); // _partialCurRowIndex = EditIndex;

                    _dataBindFlag = true;
                    break;

                case CMD_BATCH:
                case CMD_DELETE:
                case CMD_FILTER:
                case CMD_MOVE:
                case CMD_GROUP:
                case CMD_UNGROUP:
                case CMD_PAGE:
                case CMD_SORT:
                    RaisePostBackEvent(command + ":" + argument);
                    _dataBindFlag = true;
                    break;

                case CMD_SELECT:
                    _partialPrevRowIndex = (_tmpRowCommandIndex != int.MinValue) // HandleEvent() has been called
                        ? _tmpRowCommandIndex
                        : SelectedIndex;

                    RaisePostBackEvent(command + ":" + argument);
                    _partialCurRowIndex = Convert.ToInt32(argument, CultureInfo.InvariantCulture); // _partialCurRowIndex = SelectedIndex;
                    break;

                case CMD_UPDATE:
                    args = argument.Split(new char[] { ':' }, 2);

                    int rowIndex = int.Parse(args[0]);

                    ArrayList values = (ArrayList)new JsonReader(new System.IO.StringReader(args[1])).ReadValue();
                    PatchRowWithValues(Rows[rowIndex], values);

                    argument = _callbackRequest.Argument = args[0]; // hide additional parameters

                    _partialPrevRowIndex = (_tmpRowCommandIndex != int.MinValue) // HandleEvent() has been called
                        ? _tmpRowCommandIndex
                        : EditIndex;

                    OnBubbleEvent(this, new C1GridViewCommandEventArgs(Rows[rowIndex], this, new CommandEventArgs(command, argument)));
                    _partialCurRowIndex = Convert.ToInt32(argument, CultureInfo.InvariantCulture); // _partialCurRowIndex = EditIndex;

                    _dataBindFlag = true;
                    break;

                case CMD_VSCROLL:
                    args = argument.Split(new char[] { ':' });
                    _scrollingState.Index = int.Parse(args[0]);
                    _scrollingState.MaxCount = int.Parse(args[1]);
                    break;

                case CMD_HIERARCHY:
                    RaisePostBackEvent(command + ":" + argument);
                    _dataBindFlag = true;
                    break;
                case CMD_RENDER:
                    _dataBindFlag = true;
                    break;

                case CMD_REFRESH:
                    _dataBindFlag = true;
                    break;

                default:
                    throw new NotImplementedException(string.Format("{0}: command \"{1}\" is not supported.", ID, command));
            }

            if (_dataBindFlag)
            {
                RequiresDataBinding = true;
                EnsureDataBound();
            }

            if (_partialPrevRowIndex >= Rows.Count)
            {
                _partialPrevRowIndex = -1;
            }
        }

        private Exception _raiseCallbackEventException = null;
        private CallbackRequest _callbackRequest = null;

        // Note: To restore hierarchy tree properly callbacks are always targeted to the topmost grid in the hierarchy.
        // So "this" points to the root grid.
        string ICallbackEventHandler.GetCallbackResult()
        {
            if (_raiseCallbackEventException != null)
            {
                throw _raiseCallbackEventException;
            }

            // Determine the real callback target
            C1GridView realTarget = _callbackRequest.DetailPath == null
                ? this // root grid
                : this.FindDetail(_callbackRequest.DetailPath, true); // detail grid

            return realTarget.GetCallbackResult(_callbackRequest.Command, _callbackRequest.Argument); // redirect to the real target
        }

        // Note: To restore hierarchy tree properly callbacks are always targeted to the topmost grid in the hierarchy.
        // So "this" points to the root grid.
        void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
        {
            _callbackRequest = new CallbackRequest(eventArgument);

            C1GridView realTarget = null;

            // Determine the real callback target
            if (_callbackRequest.DetailPath == null)
            {
                realTarget = this; // root grid
            }
            else // detail grid
            {
                realTarget = FindDetail(_callbackRequest.DetailPath, true);
                realTarget._callbackRequest = new CallbackRequest(_callbackRequest.Command, _callbackRequest.Argument); // set request parameters

                if (!realTarget.IsJsonStateLoaded) // grid was created dynamically during one of the previous callbacks, so it goes out of the normal page life cycle and it's properties are reseted to the original ones.
                {
                    realTarget.RestoreStateFromJson(_callbackRequest.DetailJsonState); // restore state manually
                    realTarget.CreateLeaves(null, false);
                }
            }

            try
            {
                realTarget.RaiseCallbackEvent(_callbackRequest.Command, _callbackRequest.Argument); // redirect to the real target
            }
            catch (Exception e)
            {
                _raiseCallbackEventException = e;
            }
        }

        #endregion

        private PartialCallbackEntity GetPartialCallbackResult(string command, string argument)
        {
            PartialCallbackEntity result = new PartialCallbackEntity();

            string prevRowHhtml = string.Empty;
            string curRowHhtml = string.Empty;
            string prevInitScript = string.Empty;
            string curInitScript = string.Empty;

            PrepareControlHierarchy();

            if (_partialPrevRowIndex >= 0)
            {
                //if (AllowC1InputEditors)
                //{
                //	prevInitScript = GetChildInitializationScripts(this.Rows[_partialPrevRowIndex]);
                //}

                using (System.IO.StringWriter sw = new System.IO.StringWriter())
                using (HtmlTextWriter hw = new NoFormatHtmlTextWriter(sw))
                {
                    Rows[_partialPrevRowIndex].RenderControl(hw);
                    sw.Flush();
                    prevRowHhtml = sw.ToString();
                }
            }

            if (_partialCurRowIndex >= 0)
            {
                if (AllowC1InputEditors)
                {
                    GetChildInitializationScripts<Control>(this.Rows[_partialCurRowIndex]); // force to generate the ClientID property value of the c1input controls.
                                                                                            // curInitScript = GetChildInitializationScripts(this.Rows[_partialCurRowIndex]);
                }

                using (System.IO.StringWriter sw = new System.IO.StringWriter())
                using (HtmlTextWriter hw = new NoFormatHtmlTextWriter(sw))
                {
                    Rows[_partialCurRowIndex].RenderControl(hw);
                    sw.Flush();
                    curRowHhtml = sw.ToString();
                }
            }

            return new PartialCallbackEntity
            {
                curRow = new CallbackRowEnitity
                {
                    html = curRowHhtml,
                    initializationScript = curInitScript,
                    rowIndex =  _partialCurRowIndex >= 0
                        ? Rows[_partialCurRowIndex].BodyRowIndex
                        : -1
                },
                prevRow = new CallbackRowEnitity
                {
                    html = prevRowHhtml,
                    initializationScript = prevInitScript,
                    rowIndex = _partialPrevRowIndex >= 0
                        ? Rows[_partialPrevRowIndex].BodyRowIndex
                        : -1
                }
            };
        }

        private CallbackEntity GetFullCallbackResult(string command, string argument)
        {
            string html = string.Empty;
            string initScripts = string.Empty;

            if (EditIndex >= 0)
            {
                if (AllowC1InputEditors)
                {
                    // GetChildInitializationScripts(this.Rows[EditIndex]); // force to generate the ClientID property value of the c1input controls.
                    initScripts = GetChildInitializationScripts<Control>(this.Rows[EditIndex]);
                }
            }

            if (HasDetails())
            {
                StringBuilder sb = new StringBuilder();
                TableRowCollection tableRows = TableRows;

                for (int i = 0; i < tableRows.Count; i++)
                {
                    sb.Append(GetChildInitializationScripts<C1GridView>(tableRows[i]));
                }

                initScripts = sb.ToString();
            }

            using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
            using (HtmlTextWriter htmlWriter = new NoFormatHtmlTextWriter(stringWriter))
            {
                Render(htmlWriter, false);

                htmlWriter.Flush();
                html = stringWriter.ToString();
            }

            return new CallbackEntity
            {
                html = html,
                initializationScript = initScripts
            };
        }

        private CallbackEntity GetHierarchyCallbackResult(string command, string argument)
        {
            HierarchyPath path = new HierarchyPath(argument);

            PrepareControlHierarchy();

            C1GridViewDataRow toggleRow = (C1GridViewDataRow)Rows[path.DataItemIndex];
            if (toggleRow != null)
            {
                int rowIndex = toggleRow.BodyRowIndex;
                string html = string.Empty;
                string initScript = string.Empty;
                TableRowCollection tableRows = TableRows;

                using (System.IO.StringWriter sw = new System.IO.StringWriter())
                using (HtmlTextWriter hw = new NoFormatHtmlTextWriter(sw))
                {
                    int i = rowIndex + GetBodyRowsOffset();

                    // render toggled DataRow + next detail rows 
                    do
                    {
                        initScript += GetChildInitializationScripts<Control>(tableRows[i]);
                        tableRows[i].RenderControl(hw);
                    } while ((++i < tableRows.Count) && (((C1GridViewRow)tableRows[i]).RowType != C1GridViewRowType.DataRow)
                        && (tableRows[i].TableSection == TableRowSection.TableBody)); // ignore footer.

                    hw.Flush();
                    html = sw.ToString();
                }

                return new CallbackHierarchyEntity
                {
                    html = html,
                    initializationScript = initScript,
                    path = path.Path,
                    rowIndex = rowIndex,
                    expand = toggleRow.Expanded
                };
            }

            return null;
        }

        private CallbackEntity GetVirtualScrollingCallbackResult(string command, string argument)
        {
            string[] args = argument.Split(':');
            string html = string.Empty;
            string editRowInitScripts = string.Empty;

            PrepareControlHierarchy();

            int start = _scrollingState.Index;
            int end = Math.Min(start + _scrollingState.MaxCount, _bodyRowsCount) - 1;

            if (AllowC1InputEditors && EditIndex >= 0 && EditIndex >= start && EditIndex <= end)
            {
                this.GetChildInitializationScripts<Control>(Rows[EditIndex]); // to generate the ClientID property value of the c1input controls.
                //editRowInitScripts = this.GetChildInitializationScripts(Rows[EditIndex]);
            }

            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            using (HtmlTextWriter hw = new NoFormatHtmlTextWriter(sw))
            {
                TableRowCollection tableRows = TableRows;
                int bodyRowsOffset = GetBodyRowsOffset();
                // TFS 430737
                int[] cbfIndexs = null;
                if (AllowClientEditing)
                {
                    List<int> cbfList = new List<int>();
                    // get the column position of C1CheckBoxField
                    for (int j = 0; j < tableRows[0].Cells.Count; j++)
                    {
                        C1CheckBoxField cbf = tableRows[0].Cells[j] is C1GridViewCell ?
                                  ((C1GridViewCell)tableRows[0].Cells[j]).ContainingField as C1CheckBoxField : null;
                        if (cbf != null && !cbf.ReadOnly)
                        {
                            cbfList.Add(j);
                        }
                    }
                    cbfIndexs = cbfList.ToArray();
                }

                for (int i = start; i <= end; i++)
                {
                    TableRow row = tableRows[i + bodyRowsOffset];
                    if (cbfIndexs != null)
                    {
                        for (int k = 0; k < cbfIndexs.Length; k++)
                        {
                            CheckBox cb = row.Cells[cbfIndexs[k]].Controls[0] as CheckBox;
                            if (cb != null)
                                cb.Enabled = true;
                        }
                    }
                    row.RenderControl(hw);
                }

                hw.Flush();
                html = sw.ToString();
            }

            return new CallbackEntity
            {
                html = html,
                initializationScript = editRowInitScripts
            };
        }

        private void PatchRowWithValues(C1GridViewRow row, ArrayList values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                ArrayList value = (ArrayList)values[i];
                string updateCtrlName = (string)value[0];
                object updateCtrlValue = value[1];

                if (C1InplaceEditor.TestID(updateCtrlName)) { // test for C1InplaceEditor's c1input editor control
                    int cellNum = C1InplaceEditor.ParseID(updateCtrlName);

                    C1InplaceEditor c1Editor = FindControlByID(row.Cells[cellNum], C1InplaceEditor.GenerateID(cellNum), false) as C1InplaceEditor; // find a parent C1InplaceEditor by ID
                    if (c1Editor != null)
                    {
                        c1Editor.Value = updateCtrlValue;
                        continue;
                    }

                    // shouldn't be there. fallback...
                }

                Control ctrl = FindControlByID(row, updateCtrlName); // find control by UniqueID

                if (ctrl != null)
                {
                    object[] attrs = ctrl.GetType().GetCustomAttributes(typeof(ControlValuePropertyAttribute), true);
                    if (attrs.Length > 0)
                    {
                        ControlValuePropertyAttribute ctrlValueAttr = (ControlValuePropertyAttribute)attrs[0];

                        System.Reflection.PropertyInfo pi = ctrl.GetType().GetProperty(ctrlValueAttr.Name);

                        object valueToUpdate = updateCtrlValue;

                        try
                        {
                            valueToUpdate = Convert.ChangeType(updateCtrlValue, pi.PropertyType);
                        }
                        catch
                        {
                        }

                        pi.SetValue(ctrl, valueToUpdate, null);
                    }
                    else
                    {
                        ListControl listCtrl = ctrl as ListControl;
                        if (listCtrl != null)
                        {
                            listCtrl.SelectedValue = updateCtrlValue.ToString();
                        }
                        else
                        {
                            IEditableTextControl editableTxtCtrl = ctrl as IEditableTextControl;
                            if (editableTxtCtrl != null)
                            {
                                editableTxtCtrl.Text = updateCtrlValue.ToString();
                            }
                            else
                            {
                                ICheckBoxControl chkCtrl = ctrl as ICheckBoxControl;
                                bool boolValue;
                                if (chkCtrl != null && bool.TryParse(updateCtrlValue.ToString(), out boolValue))
                                {
                                    chkCtrl.Checked = boolValue;
                                }
                            }
                        }
                    }
                }
            }
        }

        private Control FindControlByID(Control ctl, string name, bool uniqueID = true)
        {
            Control result = null;

            if (ctl != null)
            {
                if ((uniqueID && ctl.UniqueID == name) || (!uniqueID && ctl.ID == name))
                {
                    return ctl;
                }

                for (int i = 0; ((i < ctl.Controls.Count) && (result == null)); i++)
                {
                    result = FindControlByID(ctl.Controls[i], name, uniqueID);
                }
            }

            return result;
        }

        private string GetChildInitializationScripts<T>(Control root)
        {
            if (root != null)
            {
                StringBuilder sb = new StringBuilder();
                List<ScriptDescriptor> descriptors = new List<ScriptDescriptor>();

                GetChildScriptDescriptors<T>(root, descriptors);

                foreach (ScriptDescriptor descriptor in descriptors)
                {
                    string script = (string)typeof(ScriptDescriptor).GetMethod("GetScript", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.NonPublic).Invoke(descriptor, null);
                    sb.Append(script);
                }

                return sb.ToString();
            }

            return string.Empty;
        }

        private void GetChildScriptDescriptors<T>(Control control, List<ScriptDescriptor> descriptors)
        {
            // post-order walk, to initialise child controls first
            if (control != null)
            {
                for (int i = 0; i < control.Controls.Count; i++)
                {
                    GetChildScriptDescriptors<T>(control.Controls[i], descriptors);
                }

                IScriptControl scriptControl = control as IScriptControl;
                if (scriptControl != null && (scriptControl is T))
                {
                    if (control.ID == null)
                    {
                        string clientID = control.ClientID; // force control to create an ID.
                    }

                    descriptors.AddRange(scriptControl.GetScriptDescriptors());
                }
            }
        }

        #endregion

        internal class NoFormatHtmlTextWriter : HtmlTextWriter
        {
            public NoFormatHtmlTextWriter(System.IO.TextWriter writer)
                : base(writer, string.Empty)
            {
                NewLine = string.Empty;
                Indent = 0;
            }
        }

        private class FilterLocalization
        {
            private string _name;
            private string _displayName;

            public FilterLocalization(string name, string displayName)
            {
                _name = name;
                _displayName = displayName;
            }

            [Json(true)]
            public string Name
            {
                get { return _name; }
            }

            [Json(true)]
            public string DisplayName
            {
                get { return _displayName; }
            }
        }

        #region IC1Cloneable Members

        /// <summary>
        /// Creates a new instance of the <see cref="C1GridView"/> class.
        /// </summary>
        /// <returns>
        /// A new instance of the <see cref="C1GridView"/> class.
        /// </returns>
        internal virtual C1GridView CreateInstance()
        {
            return new C1GridView();
        }

        /// <summary>
        /// Copies the properties of the specified <see cref="C1GridView"/> into the instance of the <see cref="C1GridView"/> class that this method is called from.
        /// </summary>
        /// <param name="source">A <see cref="C1GridView"/> that represents the properties to copy.</param>
        /// <param name="uiPropertiesOnly"></param>
        internal virtual void CopyFrom(C1GridView source, bool uiPropertiesOnly)
        {
            if (source != null)
            {
                if (uiPropertiesOnly)
                {
                    // properties that can be changed via UI actionss
                    _autoSortExpression = source._autoSortExpression;
                    _autoSortDirection = source._autoSortDirection;
                    _editIndex = source._editIndex;
                    _pageIndex = source._pageIndex;
                    _selectedIndex = source._selectedIndex;
                    Columns.CopyFrom(source.Columns);

                    return;
                }

                EnableViewState = source.EnableViewState;
                EnableTheming = source.EnableTheming;
                Enabled = source.Enabled;
                ID = source.ID;

                // * primitive properties *
                _autoSortExpression = source._autoSortExpression;
                _autoSortDirection = source._autoSortDirection;
                _editIndex = source._editIndex;
                _pageIndex = source._pageIndex;
                _selectedIndex = source._selectedIndex;

                IDictionaryEnumerator ien = source.PropertiesContainer.GetEnumerator();
                while (ien.MoveNext())
                {
                    string key = (string)ien.Key;
                    object val = ien.Value;

                    if (val is StateItem)
                    {
                        val = ((StateItem)val).Value;
                    }

                    if (!(key.StartsWith("_") || val == null))
                    {
                        Type type = val.GetType();
                        if (type.IsPrimitive || type.IsValueType || type == typeof(string))
                        {
                            PropertiesContainer[key] = val;
                        }
                    }
                }

                // * complex properties *
                DataKeyNames = source.DataKeyNames;
                DataSource = source.DataSource;
                CallbackSettings.CopyFrom(source.CallbackSettings);
                Columns.CopyFrom(source.Columns);
                ColumnsGenerator = source.ColumnsGenerator;
                FilterSettings.CopyFrom(source.FilterSettings);
                PagerSettings.CopyFrom(source.PagerSettings);

                // * styles *

                if (source.ControlStyleCreated)
                {
                    ControlStyle.CopyFrom(source.ControlStyle);
                }

                CopyBaseAttributes(source);

                if (_alternatingRowStyle != null)
                {
                    AlternatingRowStyle.CopyFrom(_alternatingRowStyle);
                }

                if (_editRowStyle != null)
                {
                    EditRowStyle.CopyFrom(_editRowStyle);
                }

                if (_emptyDataRowStyle != null)
                {
                    EmptyDataRowStyle.CopyFrom(_emptyDataRowStyle);
                }

                if (_filterStyle != null)
                {
                    FilterStyle.CopyFrom(_filterStyle);
                }

                if (_footerStyle != null)
                {
                    FooterStyle.CopyFrom(_footerStyle);
                }

                if (_headerStyle != null)
                {
                    HeaderStyle.CopyFrom(_headerStyle);
                }

                if (_rowStyle != null)
                {
                    RowStyle.CopyFrom(_rowStyle);
                }

                if (_selectedRowStyle != null)
                {
                    SelectedRowStyle.CopyFrom(_selectedRowStyle);
                }
            }
        }

        internal void CopyFrom(C1GridView source)
        {
            this.CopyFrom(source, false);
        }

        IC1Cloneable IC1Cloneable.CreateInstance()
        {
            return this.CreateInstance();
        }

        void IC1Cloneable.CopyFrom(object copyFrom)
        {
            this.CopyFrom((C1GridView)copyFrom);
        }

        #endregion

        #region ICloneable Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uiPropertiesOnly"></param>
        /// <returns></returns>
        internal virtual C1GridView Clone(bool uiPropertiesOnly)
        {
            C1GridView clone = CreateInstance();
            clone.CopyFrom(this, false);
            return clone;
        }

        internal C1GridView Clone()
        {
            return this.Clone(false);
        }


        object ICloneable.Clone()
        {
            return this.Clone();
        }

        #endregion
    }
}
