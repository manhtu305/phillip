//----------------------------------------------------------------------------
// C1.C1Schedule.Localization.StringTables
//----------------------------------------------------------------------------
//
// Populates localized string tables used by the Localizer class.
//
// This file is project-specific (except for licensing-related resources
// such as About Box).
//
//----------------------------------------------------------------------------
// Copyright (C) 2001 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status	Date			By			Comments
//----------------------------------------------------------------------------
// Created	May 2002		Bernardo	-
// Modified	Apr 29, 2007	IrinaP		- only common strings for all scheduling projects
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Globalization;

namespace C1.C1Schedule.Localization
{
	/// <summary>
	/// StringTables
	/// this class contains a single static method InitTables that populates the
	/// tables used by the Localizer class.
	/// </summary>
	internal class StringTables
	{
		internal static void InitTables(Hashtable htDesc, Hashtable htCat, Hashtable htGetStr)
		{
#if GRAPECITY
			InitTables(htDesc, htCat, htGetStr, "ja");
#else
			InitTables(htDesc, htCat, htGetStr, CultureInfo.CurrentCulture.Name);
#endif
		}
		internal static void InitTables(Hashtable htDesc, Hashtable htCat, Hashtable htGetStr, string locale)
		{
			// english? no work is needed (use fallback strings)
			if (locale.StartsWith("en"))
				return;
#if false // use these strings as a start point for new culture
				// C1Description table
				//

				htDesc.Add("C1BindingSource.AllowNew", "Indicates whether the AddNew method can be used to add items to the list.");
				htDesc.Add("C1BindingSource.DataMember", "The specific list in the data source to which the connector currently binds to.");
				htDesc.Add("C1BindingSource.DataSource", "The data source that the connector binds to.");
				htDesc.Add("C1BindingSource.Filter", "The expression used to filter which rows are viewed.");
				htDesc.Add("C1BindingSource.Sort", "Column names used for sorting, and the sort order for viewing the rows in the data source.");

				htDesc.Add("Mapping.Body", "Allows the Body property of the appointment to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.End", "Allows the End property of the appointment to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.Location", "Allows the Location property of the appointment to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.Start", "Allows the Start property of the appointment to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.Subject", "Allows the Subject property of the appointment to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.AppointmentProperties", "Allows the other properties of the appointment to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.Text", "Allows the Text property of the object to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.Color", "Allows the Color property of the object to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.MenuCaption", "Allows the MenuCaption property of the object to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.DataType", "Type of data field or property which can be bound to this object.");
				htDesc.Add("Mapping.MappingName", "Name of data field or property to be bound to this object.");
				htDesc.Add("Mapping.IsMapped", "Indicates if this objects is mapped to data field or property.");
				htDesc.Add("Mapping.Required", "Indicates if mapping for this property is required or optional.");
				htDesc.Add("Mapping.PropertyName", "Name of the property which should be mapped.");
				htDesc.Add("Mapping.Id", "Allows the Id property of BasePersistableObject to be bound to appropriate field in the data source.");
				htDesc.Add("Mapping.Index", "Allows the Index property of BasePersistableObject to be bound to appropriate field in the data source.");

				htDesc.Add("CalendarInfo.SelectedDaysChanged", "Fired when the range of selected days was changed.");
				htDesc.Add("CalendarInfo.PropertyChanged", "Fired when the property of CalendarInfo object was changed.");
				htDesc.Add("CalendarInfo.DateFormatString", "Format string to display dates in appointments and built-in dialogs.");
				htDesc.Add("CalendarInfo.TimeFormatString", "Format string to display times in appointments and built-in dialogs.");
				htDesc.Add("CalendarInfo.WeekStart", "The first working day of the week.");
				htDesc.Add("CalendarInfo.WorkDays", "The list of working days of a week.");
				htDesc.Add("CalendarInfo.TimeInterval", "The default time interval for displaying time slots.");
				htDesc.Add("CalendarInfo.StartDayTime", "The start time of the working day.");
				htDesc.Add("CalendarInfo.EndDayTime", "The end time of the working day.");
				htDesc.Add("CalendarInfo.FirstDate", "The minimum date allowed.");
				htDesc.Add("CalendarInfo.LastDate", "The maximum date allowed.");
				htDesc.Add("CalendarInfo.Holidays", "The list of holidays.");
				htDesc.Add("CalendarInfo.WeekendExceptions", "The list of working weekend days.");
				htDesc.Add("CalendarInfo.DateTimeKind", "DateTime kind.");

				htDesc.Add("Storage.Appointments", "The collection of appointments.");
				htDesc.Add("Storage.Statuses", "The collection of statuses.");
				htDesc.Add("Storage.Resources", "The collection of resources.");
				htDesc.Add("Storage.Contacts", "The collection of contacts.");
				htDesc.Add("Storage.Categories", "The collection of categories.");
				htDesc.Add("Storage.Labels", "The collection of labels.");
				htDesc.Add("Storage.Mappings", "The collection of mappings.");
				htDesc.Add("Storage.ContactStorage", "The ContactStorage object.");
				htDesc.Add("Storage.AppointmentStorage", "The AppointmentStorage object.");
				htDesc.Add("Storage.LabelStorage", "The LabelStorage object.");
				htDesc.Add("Storage.ResourceStorage", "The ResourceStorage object.");
				htDesc.Add("Storage.StatusStorage", "The StatusStorage object.");
				htDesc.Add("Storage.CategoryStorage", "The CategoryStorage object.");
#endif

			// japanese
			if (locale.StartsWith("ja"))
			{
				// -------------------------------------------				
				#region ** C1Category
				//
				htCat.Add("Appearance", "表示");
				htCat.Add("Appointment", "予定");
				htCat.Add("Behavior", "動作");
				htCat.Add("Calendar Info", "カレンダー情報");
				htCat.Add("Data", "データ");
				htCat.Add("Design", "デザイン");
				htCat.Add("Layout", "レイアウト");
				htCat.Add("Mapping", "マッピング");
				htCat.Add("Misc", "その他");
				htCat.Add("Reminder", "再通知");
				htCat.Add("Selection", "選択");
				#endregion

				// -------------------------------------------				
				#region ** C1Description
				//
				htDesc.Add("C1BindingSource.AllowNew", "リストに項目を追加するために AddNew メソッドを使用できるかどうかを指定します。 ");
				htDesc.Add("C1BindingSource.DataMember", "コネクタが連結するデータソース内のリストを指定します。");
				htDesc.Add("C1BindingSource.DataSource", "コネクタが連結するデータソースを指定します。");
				htDesc.Add("C1BindingSource.Filter", "表示する列を指定する表現式です。");
				htDesc.Add("C1BindingSource.Sort", "データソースに表示する列の名前および行のソート順を指定します。");

				htDesc.Add("Mapping.Body", "予定の Body プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.End", "予定の End プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.Location", "予定の Location プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.Start", "予定の Start プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.Subject", "予定の Subject プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.AppointmentProperties", "その他のプロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.Text", "予定の Text プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.Color", "予定の Color プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.MenuCaption", "予定の MenuCaption プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.DataType", "このオブジェクトに連結するデータフィールドもしくはプロパティの種類を指定します。");
				htDesc.Add("Mapping.MappingName", "このオブジェクトに連結するデータフィールドもしくはプロパティの名前を指定します。");
				htDesc.Add("Mapping.IsMapped", "このオブジェクトがデータフィールドもしくはプロパティにマップされているかどうかを指定します。");
				htDesc.Add("Mapping.Required", "このプロパティのマッピングが必須かどうかを指定します。");
				htDesc.Add("Mapping.PropertyName", "マップするプロパティ名を指定します。");
				htDesc.Add("Mapping.Id", "BasePersistableObject の Id プロパティにマップするフィールドを指定します。");
				htDesc.Add("Mapping.Index", "BasePersistableObject の Index プロパティにマップするフィールドを指定します。");

				htDesc.Add("CalendarInfo.SelectedDaysChanged", "選択された日付範囲が変更されたときに発生します。");
				htDesc.Add("CalendarInfo.PropertyChanged", "CalendarInfo オブジェクトの値が変更されたときに発生します。");
				htDesc.Add("CalendarInfo.DateFormatString", "予定および組み込みのダイアログボックスで表示する日付を表す文字列の形式です。");
				htDesc.Add("CalendarInfo.TimeFormatString", "予定および組み込みのダイアログボックスで表示する時間を表す文字列の形式です。");
				htDesc.Add("CalendarInfo.WeekStart", "最初の稼働日です。");
				htDesc.Add("CalendarInfo.WorkDays", "週の稼動日のコレクションです。");
				htDesc.Add("CalendarInfo.TimeInterval", "間隔の規定値です。");
				htDesc.Add("CalendarInfo.StartDayTime", "平日の開始時刻です。");
				htDesc.Add("CalendarInfo.EndDayTime", "平日の終了時刻です。");
				htDesc.Add("CalendarInfo.FirstDate", "予定表の最初の日付です");
				htDesc.Add("CalendarInfo.LastDate", "予定表の最後の日付です");
				htDesc.Add("CalendarInfo.Holidays", "休日のコレクションです。");
				htDesc.Add("CalendarInfo.WeekendExceptions", "稼働日の週末のコレクションです。");
				htDesc.Add("CalendarInfo.DateTimeKind", "DateTime の種類です。");

				htDesc.Add("Storage.Appointments", "予定のコレクションです。");
				htDesc.Add("Storage.Statuses", "ステータスのコレクションです。");
				htDesc.Add("Storage.Resources", "リソースのコレクションです。");
				htDesc.Add("Storage.Contacts", "連絡先のコレクションです。");
				htDesc.Add("Storage.Categories", "分類項目のコレクションです。");
				htDesc.Add("Storage.Labels", "ラベルのコレクションです。");
				htDesc.Add("Storage.Mappings", "マッピングのコレクションです。");
				htDesc.Add("Storage.ContactStorage", "ContactStorage オブジェクトです。");
				htDesc.Add("Storage.AppointmentStorage", "AppointmentStorage オブジェクトです。");
				htDesc.Add("Storage.LabelStorage", "LabelStorage オブジェクトです。");
				htDesc.Add("Storage.ResourceStorage", "ResourceStorage オブジェクトです。");
				htDesc.Add("Storage.StatusStorage", "StatusStorage オブジェクトです。");
				htDesc.Add("Storage.CategoryStorage", "CategoryStorage オブジェクトです。");
				#endregion

				// -------------------------------------------	
				#region ** Strings
				// -------------------------------------------	
				#region ** Exceptions
				htGetStr.Add("The required MappingName for the {0} is not specified.", "{0} に必要な MappingName は指定されていません。");
				htGetStr.Add("Format {0} is not supported", "書式 {0} はサポートされていません。");
				htGetStr.Add("It is not valid VCALENDAR information", "有効な VCALENDAR 情報ではありません。");
				htGetStr.Add("Not all information was imported due to not valid data.", "データは無効であるため、すべての情報をインポートできませんでした。");
				htGetStr.Add("The End value should be greater than Start value.", "開始値より大きい値を指定してください。");
				htGetStr.Add("Cannot find C1Schedule node.", "C1Schedule ノードが見つかりません。");
				htGetStr.Add("This method is not supported on this operating system", "このメソッドは、このオペレーシングシステムではサポートされていません。");
				#endregion

				// -------------------------------------------	
				#region ** Recurrence pattern
				htGetStr.Add("1 hour", "1 時間");
				htGetStr.Add("{0} minutes", "{0} 分");
				htGetStr.Add("day", "日");
				htGetStr.Add("first", "第1");
				htGetStr.Add("second", "第2");
				htGetStr.Add("third", "第3");
				htGetStr.Add("fourth", "第4");
				htGetStr.Add("last", "最終");
				htGetStr.Add("week day", "平日"); // in the meaning of working day of the week
				htGetStr.Add("weekend", "週末");
				htGetStr.Add("Occurs every day", "毎日");
				htGetStr.Add("Occurs every {0} days", "{0} 日ごと"); // template for phrases such as "every 3 day"
				htGetStr.Add("Occurs every week day", "稼働日");
				htGetStr.Add("Occurs day {0} of every 1 month", "1 月ごとの {0} 日"); // template for phrases such as "monthly on the 24"
				htGetStr.Add("Occurs day {1} of every {0} months", "{0} か月ごとの {1} 日"); // template for phrases such as "every 2 months on the 24"
				htGetStr.Add("Occurs the {0} {1} of every 1 month", "1 か月ごとの{0}{1}"); // template for phrases such as "monthly on the 2 Friday"
				htGetStr.Add("Occurs the {1} {2} of every {0} months", " {0}か月ごとの{1}{2}"); // template for phrases such as "every 2 months on the 2 Friday")
				htGetStr.Add("Occurs every {0}", "{0} ごと"); // template for phrases such as "weekly every Monday"
				htGetStr.Add("Occurs every {0} weeks on {1}", "{0} 週間ごとの {1}"); // template for phrases such as "every 2 week every Monday"
				htGetStr.Add("incorrect reccurrence pattern", "定期的なパターンが正しくありません");
				htGetStr.Add("Occurs every {1} {0}", "毎年 {1} {0} 日"); // template for phrases such as "annually on the 31 of December"
				htGetStr.Add("Occurs the {0} {1} of {2}", "毎年{2}の{0}{1}"); // template for phrases such as "annually on the 2 Friday of December"
				htGetStr.Add(" effective {0} from {1} to {2}.", "、{1} から {2}。{0} から開始"); // template for phrases such as ", since 4.12.2007 from 8:00 to 8:20."
				htGetStr.Add(" effective {0} from {1} to {2}, {3} times.", "、{1} から {2}。{0} から開始。{3} 回"); // template for phrases such as ", since 4.12.2007 from 8:00 to 8:20, 5 times."
				htGetStr.Add(" effective {0} until {1} from {2} to {3}.", "、{2} から {3}。{0} から {1} まで有効"); // template for phrases such as ", since 4.12.2007 until 10.12.2007 from 8:00 to 8:20."
				#endregion

				// -------------------------------------------	
				#region ** Labels
				htGetStr.Add("None", "なし");
				htGetStr.Add("Important", "重要");
				htGetStr.Add("Business", "ビジネス");
				htGetStr.Add("Personal", "個人用");
				htGetStr.Add("Vacation", "休暇");
				htGetStr.Add("Deadline", "期日");
				htGetStr.Add("Must Attend", "要出席");
				htGetStr.Add("Travel Required", "出張あり");
				htGetStr.Add("Needs Preparation", "要準備");
				htGetStr.Add("Birthday", "誕生日");
				htGetStr.Add("Anniversary", "記念日");
				htGetStr.Add("Phone Call", "通話記録");
				#endregion

				// -------------------------------------------	
				#region ** Statuses
				htGetStr.Add("Busy", "予定あり");
				htGetStr.Add("Free", "空き時間");
				htGetStr.Add("Out of Office", "外出中");
				htGetStr.Add("Tentative", "仮の予定");
				#endregion

				// -------------------------------------------	
				#region ** Categories
		//		htGetStr.Add("Business", "ビジネス");
				htGetStr.Add("Competition", "会社関係 (競合相手)");
				htGetStr.Add("Favorites", "お気に入 り");
				htGetStr.Add("Gifts", "プレゼント/ギフト");
				htGetStr.Add("Goals/Objectives", "ゴール/目標");
				htGetStr.Add("Holiday", "祝日");
				htGetStr.Add("Holiday Cards", "年賀状/暑中見舞い");
				htGetStr.Add("Hot Contacts", "会社関係 (取引先)");
				htGetStr.Add("Ideas", "アイデア/ヒント");
				htGetStr.Add("International", "インターナショナル");
				htGetStr.Add("Key Customer", "顧客");
				htGetStr.Add("Miscellanneous", "その他");
			//	htGetStr.Add("Personal", "個人用");
				htGetStr.Add("Phone Calls", "通話記録");
				htGetStr.Add("Status", "進捗状況");
				htGetStr.Add("Strategies", "戦略");
				htGetStr.Add("Suppliers", "業者");
				htGetStr.Add("Time & Expenses", "時間/経費");
				htGetStr.Add("VIP", "VIP");
				htGetStr.Add("Waiting", "待機中");
				#endregion
				#endregion
			}

			// japanese
			if (locale.StartsWith("ru"))
			{
				// -------------------------------------------				
				#region ** C1Description
				//
				htDesc.Add("C1BindingSource.AllowNew", "Показывает, можно ли использовать метод AddNew для добавления новых элементов.");
				htDesc.Add("C1BindingSource.DataMember", "Конкретный объект в источнике данных, к которому выполняется байндинг.");
				htDesc.Add("C1BindingSource.DataSource", "Источник данных.");
				htDesc.Add("C1BindingSource.Filter", "Выражение, используемое для фильтрации.");
				htDesc.Add("C1BindingSource.Sort", "Имена столбцов, используемые для сортировки, и порядок сортировки.");

				htDesc.Add("Mapping.Body", "Устанавливает мэппинг для свойства Appointment.Body.");
				htDesc.Add("Mapping.End", "Устанавливает мэппинг для свойства Appointment.End.");
				htDesc.Add("Mapping.Location", "Устанавливает мэппинг для свойства Appointment.Location.");
				htDesc.Add("Mapping.Start", "Устанавливает мэппинг для свойства Appointment.Start.");
				htDesc.Add("Mapping.Subject", "Устанавливает мэппинг для свойства Appointment.Subject.");
				htDesc.Add("Mapping.AppointmentProperties", "Устанавливает мэппинг для поля, в котором будут храниться сериализованные свойства аппойнтмента.");
				htDesc.Add("Mapping.Text", "Устанавливает мэппинг для свойства Text.");
				htDesc.Add("Mapping.Color", "Устанавливает мэппинг для свойства Color.");
				htDesc.Add("Mapping.MenuCaption", "Устанавливает мэппинг для свойства MenuCaption.");
				htDesc.Add("Mapping.DataType", "Тип поля данных или свойства, к которому может быть осуществлен мэппинг.");
				htDesc.Add("Mapping.MappingName", "Name of data field or property to be bound to this object.");
				htDesc.Add("Mapping.IsMapped", "Показывает наличие мэппинга.");
				htDesc.Add("Mapping.Required", "Показывает, обязательно ли устанавливать мэппинг для данного поля.");
				htDesc.Add("Mapping.PropertyName", "Имя свойства.");
				htDesc.Add("Mapping.Id", "Устанавливает мэппинг для свойства Id.");
				htDesc.Add("Mapping.Index", "Устанавливает мэппинг для свойства Index.");

				htDesc.Add("CalendarInfo.SelectedDaysChanged", "Событие возникает при изменении диапазона дат.");
				htDesc.Add("CalendarInfo.PropertyChanged", "Событие возникает при изменении какого-либо свойства объекта CalendarInfo.");
				htDesc.Add("CalendarInfo.DateFormatString", "Формат отображения дат во встроенных диалогах.");
				htDesc.Add("CalendarInfo.TimeFormatString", "Формат отображения времени во встроенных диалогах.");
				htDesc.Add("CalendarInfo.WeekStart", "Первый рабочий день недели.");
				htDesc.Add("CalendarInfo.WorkDays", "Список рабочих дней недели.");
				htDesc.Add("CalendarInfo.TimeInterval", "Масштаб времени.");
				htDesc.Add("CalendarInfo.StartDayTime", "Начало рабочего дня.");
				htDesc.Add("CalendarInfo.EndDayTime", "Окончание рабочего дня.");
				htDesc.Add("CalendarInfo.FirstDate", "Минимальная допустимая дата.");
				htDesc.Add("CalendarInfo.LastDate", "Максимальная допустимая дата.");
				htDesc.Add("CalendarInfo.Holidays", "Список праздников.");
				htDesc.Add("CalendarInfo.WeekendExceptions", "Список рабочих выходных дней.");
				htDesc.Add("CalendarInfo.DateTimeKind", "Тип времени.");

				htDesc.Add("Storage.Appointments", "Коллекция аппойнтментов.");
				htDesc.Add("Storage.Statuses", "Коллекция статусов.");
				htDesc.Add("Storage.Resources", "Коллекция ресурсов.");
				htDesc.Add("Storage.Contacts", "Коллекция контактов.");
				htDesc.Add("Storage.Categories", "Коллекция категорий.");
				htDesc.Add("Storage.Labels", "Коллекция меток.");
				htDesc.Add("Storage.Mappings", "Коллекция всеэ мэппингов.");
				htDesc.Add("Storage.ContactStorage", "ContactStorage.");
				htDesc.Add("Storage.AppointmentStorage", "AppointmentStorage.");
				htDesc.Add("Storage.LabelStorage", "LabelStorage.");
				htDesc.Add("Storage.ResourceStorage", "ResourceStorage.");
				htDesc.Add("Storage.StatusStorage", "StatusStorage.");
				htDesc.Add("Storage.CategoryStorage", "CategoryStorage.");
				#endregion

				// -------------------------------------------	
				#region ** Strings
				// -------------------------------------------	
				#region ** Exceptions
				htGetStr.Add("The required MappingName for the {0} is not specified.", "Обязательный мэппинг для {0} не задан.");
				htGetStr.Add("Format {0} is not supported", "{0} формат не поддерживается");
				htGetStr.Add("It is not valid VCALENDAR information", "Данные не являются корректной информацией в VCALENDAR формате или повреждены");
				htGetStr.Add("Not all information was imported due to not valid data.", "Информация была импортирована не полностью из-за некорректных данных.");
				htGetStr.Add("The End value should be greater than Start value.", "Время окончания должно быть больше либо равно времени начала.");
				htGetStr.Add("Cannot find C1Schedule node.", "Узел C1Schedule не найден.");
				htGetStr.Add("This method is not supported on this operating system", "Этот метод не поддерживается данной операционной системой");
				#endregion

				// -------------------------------------------	
				#region ** Recurrence pattern
				htGetStr.Add("last", "последний");
				htGetStr.Add("day", "день");
				htGetStr.Add("week day", "рабочий день"); // in the meaning of working day of the week
				htGetStr.Add("weekend", "выходной");
				htGetStr.Add("daily", "ежедневно");
				htGetStr.Add("every {0} day", "каждый {0} день"); // template for phrases such as "every 3 day"
				htGetStr.Add("on working days", "по рабочим дням");
				htGetStr.Add("monthly on the {0}", "{0} числа каждого месяца"); // template for phrases such as "monthly on the 24"
				htGetStr.Add("every {0} months on the {1}", "каждый {0} месяц {1} числа"); // template for phrases such as "every 2 months on the 24"
				htGetStr.Add("monthly on the {0} {1}", "{0} {1} каждого месяца"); // template for phrases such as "monthly on the 2 Friday"
				htGetStr.Add("every {0} months on the {1} {2}", "каждый {0} месяц {1} {2}"); // template for phrases such as "every 2 months on the 2 Friday")
				htGetStr.Add("weekly every {0}", "еженедельно, {0}"); // template for phrases such as "weekly every Monday"
				htGetStr.Add("every {0} week every {1}", "каждую {0} неделю, {1}"); // template for phrases such as "every 2 week every Monday"
				htGetStr.Add("incorrect reccurrence pattern", "неправильный шаблон повторения");
				htGetStr.Add("annually on the {0} of {1}", "ежегодно {0} {1}"); // template for phrases such as "annually on the 31 of December"
				htGetStr.Add("annually on the {0} {1} of {2}", "ежегодно в {0} {1} {2}"); // template for phrases such as "annually on the 2 Friday of December"
				htGetStr.Add(", since {0} from {1} to {2}.", ", начиная с {0} с {1} до {2}."); // template for phrases such as ", since 4.12.2007 from 8:00 to 8:20."
				htGetStr.Add(", since {0} from {1} to {2}, {3} times.", ", начиная с {0} с {1} до {2}, {3} раз(а)."); // template for phrases such as ", since 4.12.2007 from 8:00 to 8:20, 5 times."
				htGetStr.Add(", since {0} until {1} from {2} to {3}.", ", начиная с {0} по {1} с {2} до {3}."); // template for phrases such as ", since 4.12.2007 until 10.12.2007 from 8:00 to 8:20."
				#endregion
				#endregion
			}
		}
	}
}
