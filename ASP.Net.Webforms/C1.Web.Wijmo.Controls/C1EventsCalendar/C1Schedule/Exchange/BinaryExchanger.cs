﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace C1.C1Schedule
{
	/// <summary>
	/// 
	/// </summary>
	internal class BinaryExchanger : C1ScheduleExchanger
	{
		#region ctor
		public BinaryExchanger(C1ScheduleStorage storage)
			: base(storage)
		{
		}
		#endregion

		#region overrides
		/// <summary>
		/// Exports the appointments's data to a stream in the XML format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the appointments's data will be exported.</param>
		protected override void ExportInternal(Stream stream)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.TypeFormat = System.Runtime.Serialization.Formatters.FormatterTypeStyle.TypesWhenNeeded;

			// don't serialize collection's classes, only items

			// serialize labels, resources, statuses, contacts, owners and categories
			formatter.Serialize(stream, _labels.ToArray());
			formatter.Serialize(stream, _resources.ToArray());
			formatter.Serialize(stream, _statuses.ToArray());
			formatter.Serialize(stream, _contacts.ToArray());
			formatter.Serialize(stream, _categories.ToArray());
            if (_owners.Count > 0)
            {
                formatter.Serialize(stream, _owners.ToArray());
            }

			// serialize appointments
			Appointment[] array = new Appointment[_appointments.Count];
			_appointments.CopyTo(array, 0);
			formatter.Serialize(stream, array);
		}

		/// <summary>
		/// Imports the scheduler's data from a stream whose data is in the XML format.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		protected override void ImportInternal(Stream stream)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Binder = new DeserializationBinder();

			// deserialize labels, resources, statuses, contacts, owners and categories
			_labels.AddRange((Label[])formatter.Deserialize(stream));
			_resources.AddRange((Resource[])formatter.Deserialize(stream));
			_statuses.AddRange((Status[])formatter.Deserialize(stream));
			_contacts.AddRange((Contact[])formatter.Deserialize(stream));
			_categories.AddRange((Category[])formatter.Deserialize(stream));

            object obj = formatter.Deserialize(stream);

            if (obj is Contact[])
            {
                // if owners present
                _owners.AddRange((Contact[])obj);
                obj = formatter.Deserialize(stream);
            }
			// deserialize appointments
            ((List<Appointment>)_appointments).AddRange((Appointment[])obj);

			foreach (Appointment app in _appointments)
			{
				app.EndEdit();
				if ((app.RecurrenceState == RecurrenceStateEnum.Exception
					|| app.RecurrenceState == RecurrenceStateEnum.Occurrence)
					&& app.GetRecurrencePattern() == null)
				{
					app.RecurrenceState = RecurrenceStateEnum.NotRecurring;
				}
			}
		}
		#endregion

		sealed class DeserializationBinder : SerializationBinder
		{
            public static string[] SupportedAssemblies = new string[] 
                    { 
                        "C1.Win.C1Schedule",
                        "C1.WPF.Schedule",
                        "C1.Web.UI.Controls",
                        "C1.WPF.C1Schedule",
                        "C1.Web.C1Schedule"
                    };

			public override Type BindToType(string assemblyName, string typeName)
			{
				Type typeToDeserialize = null;

				// For each assemblyName/typeName that you want to deserialize to
				// a different type, set typeToDeserialize to the desired type.
				AssemblyName name = Assembly.GetExecutingAssembly().GetName();
                if (assemblyName.Contains(name.Name))
                {
                    // To use a type from a different assembly version, 
                    // change the version number.
                    // To do this, uncomment the following line of code.
                    // assemblyName = assemblyName.Replace("1.0.0.0", "2.0.0.0");

                    // The following line of code returns the type.
                    assemblyName = Assembly.GetExecutingAssembly().FullName;
                }
                else
                {
                    foreach (string asName in SupportedAssemblies)
                    {
                        if (assemblyName.Contains(asName))
                        {
                            assemblyName = Assembly.GetExecutingAssembly().FullName;
                            break;
                        }
                    }
                }
				if (typeName.Contains("System.Collections.Generic.List"))
				{
					// To use a type from a different assembly version, 
					// change the version number.
					// To do this, uncomment the following line of code.
					// assemblyName = assemblyName.Replace("1.0.0.0", "2.0.0.0");

					// To use a different type from the same assembly, 
					// change the type name.
					typeName = "System.Collections.Generic.List`1[[C1.C1Schedule.Appointment]]";
				}

				// The following line of code returns the type.
				typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
					typeName, assemblyName));

				return typeToDeserialize;
			}
		}
	}
}



