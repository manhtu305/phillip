using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Runtime.InteropServices;
using Microsoft.Win32; 

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="ICalTimeZoneInfo"/> class determines time zone properties.
	/// </summary>
	internal class ICalTimeZoneInfo : TimeZoneInfo
	{
		#region fields
		private SortedList<DateTime, HistoricalZone> _historicalZones;
		private DateTime _lastModified = DateTime.MinValue;
		private string _tzid;
		private C1ScheduleStorage _storage;
		#endregion

		#region ctor
		internal ICalTimeZoneInfo(C1ScheduleStorage storage)
		{
			_storage = storage;
			_historicalZones = new SortedList<DateTime, HistoricalZone>();
		}

		internal static bool ReadTimeZone(StreamReader sr, C1ScheduleStorage storage)
		{
			ICalTimeZoneInfo info = new ICalTimeZoneInfo(storage);

			// read from iCal
			string str = sr.ReadLine();
			while (str != null)
			{
				if (str.Length != 0)
				{
					string[] strs = str.Split(':');
					if (strs.Length < 2)
					{
						return false;
					}
					switch (strs[0].ToUpper())
					{
						case "TZID":
							info._tzid = strs[1];
							if (!_zones.ContainsKey(info._tzid))
							{
								TimeZoneInfo systemZone = TimeZoneInfo.GetTimeZone(info._tzid);
								if (systemZone != null)
								{
									info.CopyFrom(systemZone);
									return true;
								}
							}
							break;
						case "LAST-MODIFIED":
							DateTime dt;
							if (ICalUtils.ParseTime(strs[1], "", out dt))
							{
								info.LastModified = dt;
							}
							break;
						case "BEGIN":
							HistoricalZone z = null;
							if (strs[1].ToUpper() == "STANDARD")
							{
								z = HistoricalZone.Read(sr, false);
							}
							else if (strs[1].ToUpper() == "DAYLIGHT")
							{
								z = HistoricalZone.Read(sr, true);
							}
							if (z != null )
							{
								info._historicalZones[z._start] = z;
							}
							else
							{
								return false;
							}
							break;
						case "END":
							if (strs[1].ToUpper().Contains("VTIMEZONE"))
							{
								if (_zones.ContainsKey(info._tzid))
								{
									if (_zones[info._tzid].LastModified < info.LastModified)
									{
										_zones.Remove(info._tzid);
									}
									else
									{
										return true;
									}
								}
								// count end times for s
								_zones.Add(info._tzid, info);
								return true;
							}
							break;
						default:
							break;
					}
				}
				str = sr.ReadLine();
			}
			return false;
		}

		internal static Dictionary<string, ICalTimeZoneInfo> _zones = new Dictionary<string, ICalTimeZoneInfo>();
		#endregion

		#region object model
		internal string TZID
		{
			get
			{
				return _tzid;
			}
		}

		/// <summary>
		/// A UTC value that specifies the date and time that this 
		/// time zone definition was last updated.
		/// It's unknown (has value DateTime.MinValue) for time zones,
		/// registered in the local system.
		/// But it makes sense for time zones, imported from iCal format.
		/// </summary>
		internal DateTime LastModified
		{
			get
			{
				return _lastModified;
			}
			set
			{
				_lastModified = value;
			}
		}
		#endregion

		#region interface
		internal HistoricalZone GetValidZone(DateTime date)
		{
			DateTime zoneKey = DateTime.MinValue;
			for (int i = 0; i <  _historicalZones.Count; i++)
			{
				if (date >= _historicalZones.Keys[i])
				{
					zoneKey = _historicalZones.Keys[i];
				}
				else
				{
					if (zoneKey == DateTime.MinValue)
					{
						break;
					}
					else
					{
						return _historicalZones[zoneKey];
					}
				}
			}
			GenerateZones(date, _storage.Info);

			foreach (KeyValuePair<DateTime, HistoricalZone> pair in _historicalZones)
			{
				if (date >= pair.Key)
				{
					zoneKey = pair.Key;
				}
				else
				{
					return _historicalZones[zoneKey];
				}
			}
			if (zoneKey == DateTime.MinValue)
			{
				return null;
			}
			else
			{
				return _historicalZones[zoneKey];
			}
		}

		internal DateTime GetUTCTime(DateTime time)
		{
			// find valid historical zone
			HistoricalZone zone = GetValidZone(time);
			
			// count UTC time = time - tzoffsetto
			DateTime result = time.Subtract(zone._offsetto);
			result = DateTime.SpecifyKind(result, DateTimeKind.Utc);
			return result;
		}

		internal DateTime GetLocalTime(DateTime time)
		{
			return GetUTCTime(time).ToLocalTime();
		}

		//internal bool IsValidZone(DateTime
		// use only for yearly patterns to determine the start date of the time zone
		private void GenerateZones(DateTime date, CalendarInfo info)
		{
			int count = _historicalZones.Count;
			for (int i = 0; i < count; i++)
			{
				// generate zones for rdates and clear rdates
				while (_historicalZones[_historicalZones.Keys[i]]._rdates.Count > 0)
				{
					DateTime rdate = _historicalZones[_historicalZones.Keys[i]]._rdates[0];
					if (!_historicalZones.ContainsKey(rdate))
					{
						_historicalZones.Add(rdate, new HistoricalZone(_historicalZones[_historicalZones.Keys[i]], rdate));
					}
					_historicalZones[_historicalZones.Keys[i]]._rdates.RemoveAt(0);
				}
			}
			for (int i = 0; i < count; i++)
			{
				foreach (ICalPattern pattern in _historicalZones[_historicalZones.Keys[i]]._rules)
				{
					DateTime pdate = pattern.PatternStartDate;
					while (pdate < date.AddYears(2))
					{
						if (!pattern.NoEndDate && pdate > pattern.PatternEndDate)
						{
							break;
						}
						if (pattern.RecurrenceType == RecurrenceTypeEnum.Yearly)
						{
							DateTime d = new DateTime(pdate.Year, pattern.MonthOfYear, pattern.DayOfMonth);
							if (!(!pattern.NoEndDate && d > pattern.PatternEndDate))
							{
								if (d >= pattern.PatternStartDate && !_historicalZones.ContainsKey(d))
								{
									_historicalZones.Add(d, new HistoricalZone(_historicalZones[_historicalZones.Keys[i]], d));
								}
							}
						}
						else if (pattern.RecurrenceType == RecurrenceTypeEnum.YearlyNth)
						{
							DateTime start = new DateTime(pdate.Year, pattern.MonthOfYear, 1);
							DateTime end = start.AddMonths(1);
							while (start < end)
							{
								if (start >= pdate && pattern.IsValid(start, _storage.Info))
								{
									_historicalZones.Add(start, new HistoricalZone(_historicalZones[_historicalZones.Keys[i]], start));
									break;
								}
								start = start.AddDays(1);
							}
						}
						pdate = pdate.AddYears(1);
					}
				}
			}
		}
		#endregion

		#region write
		internal static void WriteTimeZone(StreamWriter sw)
		{
			// add timezone information, it is needed for recurring appointments
			sw.WriteLine("BEGIN:VTIMEZONE");

			TimeZone localZone = TimeZone.CurrentTimeZone;

			System.Globalization.DaylightTime dlt = localZone.GetDaylightChanges(DateTime.UtcNow.ToLocalTime().Year);
			TimeSpan offsetStandart;
			TimeSpan offsetDayLight;
			TimeSpan offsetNow = localZone.GetUtcOffset(DateTime.UtcNow.ToLocalTime());
			DateTime startStandart;
			DateTime startDaylight = dlt.Start;

			if (localZone.IsDaylightSavingTime(DateTime.UtcNow.ToLocalTime()))
			{
				startStandart = dlt.End;
				offsetDayLight = offsetNow;
				offsetStandart = offsetDayLight.Subtract(dlt.Delta);
			}
			else
			{
				offsetStandart = offsetNow;
				offsetDayLight = offsetStandart.Add(dlt.Delta);
				if (dlt.End < DateTime.UtcNow.ToLocalTime())
				{
					startStandart = dlt.End;
				}
				else
				{
					startStandart = localZone.GetDaylightChanges(DateTime.UtcNow.ToLocalTime().Year - 1).End;
				}
			}
			sw.WriteLine("TZID:" + ICalUtils.LocalTZID);
			sw.WriteLine("BEGIN:STANDARD");
			sw.WriteLine("TZNAME:Standard Time"); 
			sw.WriteLine("DTSTART:" + ICalUtils.GetDTString(startStandart));
#if (!DOTNET20)
            System.TimeZoneInfo.AdjustmentRule[] rules = System.TimeZoneInfo.Local.GetAdjustmentRules();
            foreach (System.TimeZoneInfo.AdjustmentRule rule in rules)
			{
				string rrule = "RRULE:FREQ=YEARLY;INTERVAL=1;";
				if (rule.DateEnd != DateTime.MaxValue.Date)
				{
					rrule += "UNTIL=" + ICalUtils.GetDTString(rule.DateEnd) + ";"; 
				}
				System.TimeZoneInfo.TransitionTime trTime = rule.DaylightTransitionEnd;
				if (trTime.IsFixedDateRule)
				{
					rrule += "BYMONTHDAY=" + trTime.Day.ToString(CultureInfo.InvariantCulture) + ";";
					rrule += "BYMONTH=" + trTime.Month.ToString(CultureInfo.InvariantCulture) + ";";
				}
				else
				{
					int num = trTime.Week;
					if (num > 4)
					{
						num = -1;
					}
					rrule += "BYDAY=" + num.ToString(CultureInfo.InvariantCulture) + PatternInfo.GetByDay(trTime.DayOfWeek) + ";";
					rrule += "BYMONTH=" + trTime.Month.ToString(CultureInfo.InvariantCulture) + ";";
				}
				sw.WriteLine(rrule);
			}
#endif
			sw.WriteLine("TZOFFSETFROM:" + GetTZOffset(offsetDayLight));
			sw.WriteLine("TZOFFSETTO:" + GetTZOffset(offsetStandart));
			sw.WriteLine("END:STANDARD");
			sw.WriteLine("BEGIN:DAYLIGHT");
			sw.WriteLine("TZNAME:Daylight Savings Time"); 
			sw.WriteLine("DTSTART:" + ICalUtils.GetDTString(startDaylight));
#if (!DOTNET20)
			foreach (System.TimeZoneInfo.AdjustmentRule rule in rules)
			{
				string rrule = "RRULE:FREQ=YEARLY;INTERVAL=1;";
				if (rule.DateEnd != DateTime.MaxValue.Date)
				{
					rrule += "UNTIL=" + ICalUtils.GetDTString(rule.DateEnd) + ";";
				}
				System.TimeZoneInfo.TransitionTime trTime = rule.DaylightTransitionStart;
				if (trTime.IsFixedDateRule)
				{
					rrule += "BYMONTHDAY=" + trTime.Day.ToString(CultureInfo.InvariantCulture) + ";";
					rrule += "BYMONTH=" + trTime.Month.ToString(CultureInfo.InvariantCulture) + ";";
				}
				else
				{
					int num = trTime.Week;
					if (num > 4)
					{
						num = -1;
					}
					rrule += "BYDAY=" + num.ToString(CultureInfo.InvariantCulture) + PatternInfo.GetByDay(trTime.DayOfWeek) + ";";
					rrule += "BYMONTH=" + trTime.Month.ToString(CultureInfo.InvariantCulture) + ";";
				}
				sw.WriteLine(rrule);
			}
#endif
            sw.WriteLine("TZOFFSETFROM:" + GetTZOffset(offsetStandart));
			sw.WriteLine("TZOFFSETTO:" + GetTZOffset(offsetDayLight));
			sw.WriteLine("END:DAYLIGHT");
			sw.WriteLine("END:VTIMEZONE");
		}

		private static string GetTZOffset(TimeSpan value)
		{
			string result = "";
			if (value.Duration() != value)
			{
				result += "-";
				value = value.Duration();
			}
			else
			{
				result += "+";
			}
			result += value.Hours.ToString("d2") + value.Minutes.ToString("d2");
			return result;
		}
		#endregion
	}

	internal class HistoricalZone
	{
		#region fields
		internal DateTime _start;		// the effective onset date and
										// local time for the time zone sub-component definition
		internal TimeSpan _offsetto;	// the UTC offset for the time zone sub-component 
										// (Standard Time or Daylight Saving Time) when
										// this observance is in use
		internal TimeSpan _offsetfrom;	// the UTC offset which is in use when 
										// the onset of this time zone observance begins
		internal List<DateTime> _rdates;// the onset of the observance by giving 
										// the individual onset date and times
		internal List<ICalPattern> _rules;		// defines the recurrence rule for the onset of the
										// observance defined by this time zone sub-component
		#endregion

		#region ctor
		// private ctor
		private HistoricalZone()
		{
			_rules = new List<ICalPattern>();
			_rdates = new List<DateTime>();
		}

		internal HistoricalZone(HistoricalZone zone, DateTime start)
		{
			_start = start;
			_offsetfrom = zone._offsetfrom;
			_offsetto = zone._offsetto;
		}

		internal static HistoricalZone Read(StreamReader sr, bool daylight)
		{
			HistoricalZone zone = new HistoricalZone();
			string str = sr.ReadLine();
			List<ICalPattern> patterns = new List<ICalPattern>();
			List<DateTime> rdates = new List<DateTime>();
			while (str != null)
			{
				if (str.Length != 0)
				{
					string[] strs = str.Split(':');
					if (strs.Length < 2)
					{
						return null;
					}
					switch (strs[0].ToUpper())
					{
						case "DTSTART":
							DateTime dt;
							if (ICalUtils.ParseTime(strs[1], "", out dt))
							{
								zone._start = dt;
							}
							else
							{
								return null;
							}
							break;
						case "TZOFFSETFROM":
							TimeSpan offrom;
							if (ParseTZOffset(strs[1], out offrom))
							{
								zone._offsetfrom = offrom;
							}
							else
							{
								return null;
							}
							break;
						case "TZOFFSETTO":
							TimeSpan ofto;
							if (ParseTZOffset(strs[1], out ofto))
							{
								zone._offsetto = ofto;
							}
							else
							{
								return null;
							}
							break;
						case "RRULE":
							// it is possible that time zone has multiple RRULE entries
							if (!PatternInfo.ReadPattern(strs[1], patterns))
							{
								return null;
							}
							break;
						case "RDATE":
							// according to the specification RDATE can't contain periods 
							// if applied to time zone description
							if (!ICalUtils.ParseTimeList(strs[1], rdates, ""))
							{
								return null;
							}
							break;
						case "END":
							// check if any patterns exist
							if (patterns.Count > 0)
							{
								for (int i = 0; i < patterns.Count; i++)
								{
									// don't know what to do with non yearly patterns.
									// remove them
									if (patterns[i].RecurrenceType != RecurrenceTypeEnum.Yearly
										&& patterns[i].RecurrenceType != RecurrenceTypeEnum.YearlyNth)
									{
										patterns.RemoveAt(i);
									}
									else
									{
										if (patterns[i].RecurrenceType == RecurrenceTypeEnum.Yearly
											&& patterns[i].DayOfMonth == 0)
										{
											patterns[i].DayOfMonth = zone._start.Day;
										}
										if (patterns[i].RecurrenceType == RecurrenceTypeEnum.YearlyNth
											&& patterns[i].DayOfWeekMask == WeekDaysEnum.None)
										{
											patterns[i].DayOfWeekMask = PatternInfo.GetDayOfWeekMask(zone._start.DayOfWeek);
										}
										if (patterns[i].MonthOfYear == 0)
										{
											patterns[i].MonthOfYear = zone._start.Month;
										}
									}
								}
								zone._rules = patterns;
							}
							if (rdates.Count > 0)
							{
								rdates.Sort();
								zone._rdates = rdates;
							}
							return zone;
						default:
							break;
					}
				}
				str = sr.ReadLine();
			}
			return null;
		}

		private static bool ParseTZOffset(string str, out TimeSpan value)
		{
			value = TimeSpan.FromMinutes(0);
			int hours = 0;
			int minutes = 0;
			str = str.Trim();
			bool negate = str.StartsWith("-");
			if (str.StartsWith("-") || str.StartsWith("+"))
			{
				str = str.Substring(1);
			}
			if (str.Length < 4)
			{
				return false;
			}
			if (!int.TryParse(str.Substring(0, 2), out hours))
			{
				return false;
			}
			if (!int.TryParse(str.Substring(2, 2), out minutes))
			{
				return false;
			}
			value = TimeSpan.FromMinutes(hours * 60 + minutes);
			if (negate)
			{
				value = value.Negate();
			}
			return true;
		}
		#endregion

		#region interface

		#endregion
	}
}