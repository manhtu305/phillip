﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;

namespace C1.C1Schedule
{
	/// <summary>
	/// 
	/// </summary>
	internal static class ICalUtils
	{
		//----------------------------------------------------------------
		#region ** folding
		internal static void Fold(Stream inStream, Stream outStream)
		{
			StreamWriter sw = new StreamWriter(outStream);
			StreamReader reader = new StreamReader(inStream);
			inStream.Position = 0;
			outStream.Position = 0;
			string str;
			while ((str = reader.ReadLine()) != null)
			{
				while (str.Length > 75)
				{
					sw.WriteLine(str.Substring(0, 74));
					str = " " + str.Substring(74);
				}
				sw.WriteLine(str);
			}
			sw.Flush();
		}

		internal static void Unfold(Stream inStream, Stream outStream)
		{
			StreamWriter sw = new StreamWriter(outStream);
			StreamReader reader = new StreamReader(inStream);
			inStream.Position = 0;
			outStream.Position = 0;
			string str = reader.ReadToEnd();
			str = str.Replace("\r\n ", "");
			str = str.Replace("\r\n\t", "");
			sw.Write(str);
			sw.Flush();
			outStream.Position = 0;
		}
		#endregion

		//----------------------------------------------------------------
		#region ** misc
		internal static string LocalTZID
		{
			get
			{
#if (DOTNET20)
                string id = TimeZone.CurrentTimeZone.StandardName;
#elif (SILVERLIGHT)
				string id = System.TimeZoneInfo.Local.DisplayName;
				while (true)
				{
					int index1 = id.IndexOf("(");
					int index2 = id.IndexOf(")");
					if (index1 < 0 || index2 < 0)
					{
						break;
					}
					if (index2 > index1)
					{
						id = id.Remove(index1, index2 - index1 + 1).Trim();
					}
				}
#else
                string id = System.TimeZoneInfo.Local.Id;
#endif
				if (id.Contains("Standard Time"))
				{
					id = id.Substring(0, id.IndexOf("Standard Time")).Trim();
				}
				return id;
			}
		}

		internal static void WriteFooter(StreamWriter sw)
		{
			sw.WriteLine("END:VCALENDAR");
			sw.Flush();
		}

		internal static string EscapeString(string str)
		{
			string result = str.Replace(",", "\\,");
			result = result.Replace(";", "\\;");
			result = result.Replace("\n", "\\n");
			result = result.Replace("\r", "\\r");
			return result;
		}
		#endregion

		//----------------------------------------------------------------
		#region ** Convert values to string
		internal static string GetDurationString(TimeSpan value)
		{
			string result = "";
			if (value.Duration() != value)
			{
				value = value.Duration();
				result += "-";
			}
			result += "P";
			if ( value.TotalDays >= 1 )
			{
				result += ((int)value.TotalDays).ToString() + "D";
			}
			result += "T" + ((int)value.Hours).ToString() + "H"
					+ ((int)value.Minutes).ToString() + "M"
					+ ((int)value.Seconds).ToString() + "S";
			return result;
		}
		internal static string GetDateString(DateTime date)
		{
			return "VALUE=DATE:"+ date.ToString("yyyyMMdd");
		}
		internal static string GetUTCDTString(DateTime date)
		{
			DateTime d = date.ToUniversalTime();
			return GetDTString(d) + "Z";
		}
		internal static string GetDTString(DateTime date)
		{
			return date.ToString("yyyyMMdd") + "T" + date.ToString("HHmmss");
		}
		#endregion

		//----------------------------------------------------------------
		#region ** Parse strings
		internal static bool ParseTime(string value, string tzid, out DateTime date)
		{
			value = value.ToUpper().Trim();
			int year = 0;
			int month = 0;
			int day = 0;
			int hour = 0;
			int minute = 0;
			int second = 0;
			date = DateTime.MinValue;

			if (!int.TryParse(value.Substring(0, 4), out year))
			{
				return false;
			}
			if (!int.TryParse(value.Substring(4, 2), out month))
			{
				return false;
			}
			if (!int.TryParse(value.Substring(6, 2), out day))
			{
				return false;
			}
			if (value.Contains("T"))
			{
				if (!int.TryParse(value.Substring(9, 2), out hour))
				{
					return false;
				}
				if (!int.TryParse(value.Substring(11, 2), out minute))
				{
					return false;
				}
				if (value.Length <= 13 || !int.TryParse(value.Substring(13, 2), out second))
				{
					return false;
				}
			}
			if (value.EndsWith("Z"))
			{
				date = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc).ToLocalTime();
			}
			else
			{
				date = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Local);
#if (!SILVERLIGHT)
				// process time zone information
				if (tzid.Contains("TZID="))
				{
					tzid = tzid.Substring(tzid.IndexOf('=') + 1);
					if (ICalTimeZoneInfo._zones.ContainsKey(tzid))
					{
						date = ICalTimeZoneInfo._zones[tzid].GetLocalTime(date);
					}
					else 
					{
						TimeZoneInfo info = TimeZoneInfo.GetTimeZone(tzid);
						if (info != null)
						{
							// convert from timezone local time to universal time
							// and then to the current system local time
							date = info.ToUniversalTime(date).ToLocalTime();
						}
					}
				}
#endif
			}
			return true;
		}

		internal static bool ParseDuration(string value, out TimeSpan duration)
		{
			value = value.Trim().ToUpper();
			bool sign = value.StartsWith("-"); // todo: use that somewhere !!!
			value = value.Substring(value.IndexOf('P') + 1);
			int days = 0;
			int hours = 0;
			int minutes = 0;
			int seconds = 0;
			int v = 0;
			int index = 0;
			// count days
			if (value.Contains("W"))
			{
				index = value.IndexOf('W');
				if (int.TryParse(value.Substring(0, index), out v))
				{
					days = v * 7;
				}
				value = value.Substring(index + 1);
			}
			if (value.Contains("D"))
			{
				index = value.IndexOf('D');
				if (int.TryParse(value.Substring(0, index), out v))
				{
					days += v;
				}
				value = value.Substring(index + 1);
			}
			// count time
			value = value.Substring(value.IndexOf('T') + 1);
			if (value.Contains("H"))
			{
				index = value.IndexOf('H');
				if (int.TryParse(value.Substring(0, index), out v))
				{
					hours = v;
				}
				value = value.Substring(index + 1);
			}
			if (value.Contains("M"))
			{
				index = value.IndexOf('M');
				if (int.TryParse(value.Substring(0, index), out v))
				{
					minutes = v;
				}
				value = value.Substring(index + 1);
			}
			if (value.Contains("S"))
			{
				index = value.IndexOf('S');
				if (int.TryParse(value.Substring(0, index), out v))
				{
					seconds = v;
				}
			}
			duration = new TimeSpan(days, hours, minutes, seconds);
			return true;
		}

		internal static bool ParseTimeList(string value, List<DateTime> list, string tzid)
		{
			bool result = true;
			string[] tokens = value.Split(',');
			foreach (string token in tokens)
			{
				DateTime date;
				if ( ParseTime(token, tzid, out date))
				{
					list.Add(date);
				}
				else
				{
					result = false;
				}
			}
			return result;
		}		
		#endregion

		//----------------------------------------------------------------
		#region ** Appointment
		internal static void WriteAppointment(StreamWriter sw, Appointment app)
		{
			sw.WriteLine("BEGIN:VEVENT");
			sw.WriteLine("UID:" + app.Id.ToString() + "@componentone.com");
			DateTime appStart = app.Start;
			DateTime appEnd = app.End;
			if (app.RecurrenceState == RecurrenceStateEnum.Master)
			{
				appStart = app.GetRecurrencePattern().PatternStartDate.Date.Add(app.Start.TimeOfDay);
				appEnd = appStart.Add(app.Duration);
			}
			if (app.AllDayEvent)
			{
				sw.WriteLine("DTSTART;" + GetDateString(appStart));
				sw.WriteLine("DTEND;" + GetDateString(appEnd));
			}
			else
			{
				sw.WriteLine("DTSTART:" + GetUTCDTString(appStart));
				sw.WriteLine("DTEND:" + GetUTCDTString(appEnd));
			}
			sw.WriteLine("SEQUENCE:0");

			if (!string.IsNullOrEmpty(app.Body))
			{
				sw.WriteLine("DESCRIPTION:" + EscapeString(app.Body)); 
			}
			if ( !string.IsNullOrEmpty(app.Subject))
			{
				sw.WriteLine("SUMMARY:" + EscapeString(app.Subject)); 
			}
			if (app.Private)
			{
				sw.WriteLine("CLASS:PRIVATE");
			}
			else
			{
				sw.WriteLine("CLASS:PUBLIC");
			}
			if ( app.Categories.Count > 0 )
			{
				string cats = "CATEGORIES:";
				foreach ( Category cat in app.Categories )
				{
					cats += cat.Text + ",";
				}
				sw.WriteLine(cats.Substring(0, cats.Length - 1));
			}
			if (app.Resources.Count > 0)
			{
				string ress = "RESOURCES:";
				foreach (Resource res in app.Resources)
				{
					ress += res.Text + ",";
				}
				sw.WriteLine(ress.Substring(0, ress.Length - 1));
			}
			if ( !string.IsNullOrEmpty(app.Location))
			{
				sw.WriteLine("LOCATION:" + EscapeString(app.Location));
			}
            if (!string.IsNullOrEmpty(app.CustomData))
            {
                sw.WriteLine("X-C1-CUSTOMDATA:" + EscapeString(app.CustomData));
            }
            switch (app.Importance)
			{
				case ImportanceEnum.High:
					sw.WriteLine("PRIORITY:" + ((int)1).ToString());
					break;
				case ImportanceEnum.Low:
					sw.WriteLine("PRIORITY:" + ((int)9).ToString());
					break;
				case ImportanceEnum.Normal:
					sw.WriteLine("PRIORITY:" + ((int)5).ToString());
					break;
			}
            if (app.BusyStatus != null)
            {
                sw.WriteLine("X-C1-STATUS;" + app.BusyStatus.Id.ToString() + ":" + Status.GetValidString(app.BusyStatus.Text));
                if (app.BusyStatus.StatusType == StatusTypeEnum.Busy)
                {
                    sw.WriteLine("TRANSP:OPAQUE");
                }
                else
                {
                    sw.WriteLine("TRANSP:TRANSPARENT");
                }
            }

            if (app.Label != null && app.Label.Id != LabelCollection.NoneLabelId)
            {
                sw.WriteLine("X-C1-LABEL;" + app.Label.Id.ToString() + ":" + Label.GetValidString(app.Label.Text));
            }

			foreach (Contact cont in app.Links)
			{
				sw.WriteLine("CONTACT:" + cont.Text);
			}
			
			if ( app.ReminderSet )
			{
				WriteAlarm(sw, app);
			}

			if (app.IsRecurring)
			{
				PatternInfo.WriteReccurrence(sw, app.GetRecurrencePattern());
			}
		/*	RECURRENCE-ID
			SEQUENCE
			RELATED-TO*/
			sw.WriteLine("DTSTAMP:" + GetUTCDTString(DateTime.UtcNow)); 
			sw.WriteLine("END:VEVENT"); 
		}
		#endregion

		//----------------------------------------------------------------
		#region ** Alarm
		internal static void WriteAlarm(StreamWriter sw, Appointment app)
		{
			sw.WriteLine("BEGIN:VALARM");
			// actions EMAIL and PROCEDURE are not supported yet
			if ( app.ReminderPlaySound)
			{
				sw.WriteLine("ACTION:AUDIO");
			}
			else
			{
				sw.WriteLine("ACTION:DISPLAY");
				sw.WriteLine("DESCRIPTION:" + app.Subject);
			}

			// REPEAT and DURATION  are not supported yet
			sw.WriteLine("TRIGGER:" + GetDurationString(app.ReminderTimeBeforeStart));
			sw.WriteLine("END:VALARM"); 
		}
		internal static bool ReadAlarm(StreamReader sr, Appointment app)
		{
			app.SetReminder();
			string str = sr.ReadLine();
			while (str != null)
			{
				if (str.Length != 0)
				{
					string[] strs = str.Split(':');
					if (strs.Length < 2)
					{
						return false;
					}
					switch (strs[0].ToUpper())
					{
						case "ACTION":
							if (strs[1].ToUpper().Contains("AUDIO"))
							{
								app.ReminderPlaySound = true;
							}
							break;
						case "TRIGGER":
							TimeSpan duration;
							ParseDuration(strs[1], out duration);
							app.ReminderTimeBeforeStart = duration;
							break;
						case "END":
							if (strs[1].ToUpper().Contains("VALARM"))
							{
								return true;
							}
							break;
						default:
							break;
					}
				}
				str = sr.ReadLine();
			}
			return false;
		}
		#endregion
	}	
}
