﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// 
	/// </summary>
	internal class ICalExchanger : C1ScheduleExchanger
	{
		#region fields
#if (!SILVERLIGHT)
		private bool _tzNeeded;
#endif
		private bool _importErrorPresent;
		private bool _componentone;
		#endregion

		#region ctor
		public ICalExchanger(C1ScheduleStorage storage)
			: base (storage)
		{
		}
		#endregion

		#region overrides
		/// <summary>
		/// Exports the appointments's data to a stream in the iCal format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the appointments's data will be exported.</param>
		protected override void ExportInternal(Stream stream)
		{
			MemoryStream ms = new MemoryStream();

			StreamWriter sw = new StreamWriter(ms, Encoding.UTF8);
			WriteHeader(sw);
			foreach (Appointment app in _appointments)
			{
				ICalUtils.WriteAppointment(sw, app);
			}
			ICalUtils.WriteFooter(sw);
			ICalUtils.Fold(ms, stream);
		}

		/// <summary>
		/// Imports the scheduler's data from a stream whose data is in the ICal format.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		protected override void ImportInternal(Stream stream)
		{
			MemoryStream ms = new MemoryStream();
			ICalUtils.Unfold(stream, ms);
			StreamReader sr = new StreamReader(ms);
			// read in to pass:
			// 1. read only time zones and put all other info into other stream
			MemoryStream ms1 = ReadTimeZones(sr);
			// 2. read all other information
			ms1.Seek(0, SeekOrigin.Begin);
			sr = new StreamReader(ms1);
			Read(sr);
			ms1.Close();
		}

		protected override void Preprocess(bool full)
		{
#if (!SILVERLIGHT)
			_tzNeeded = false;
			// check if TimeZone information should be included
			foreach (Appointment app in _appointments)
			{
				if (app.IsRecurring)
				{
					_tzNeeded = true;
				}
				break;
			}
#endif
		}
		#endregion

		#region write
		private void WriteHeader(StreamWriter sw)
		{
			sw.WriteLine("BEGIN:VCALENDAR");
			sw.WriteLine("PRODID:-//ComponentOne.com//C1Schedule.2.0.20071.0//EN");
			sw.WriteLine("VERSION:2.0");
			sw.WriteLine("METHOD:PUBLISH");

#if (!SILVERLIGHT)
			if (_tzNeeded)
			{
				ICalTimeZoneInfo.WriteTimeZone(sw);
			}
#endif
		}
		#endregion

		#region read
		private void ThrowInalidVCalInfo()
		{
#if END_USER_LOCALIZATION
			throw new System.ApplicationException(Strings.C1Schedule.Exceptions.Item("InvalidVCALInformation", _storage.Info.CultureInfo));
#elif WINFX
			throw new System.ApplicationException(C1Localizer.GetString("Exceptions", "InvalidVCALInformation", "It is not valid VCALENDAR information.", _storage.Info.CultureInfo));
#elif SILVERLIGHT
			throw new System.Exception(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.InvalidVCALInformation);
#else
			throw new System.ApplicationException(C1Localizer.GetString("It is not valid VCALENDAR information."));
#endif
		}

		private MemoryStream ReadTimeZones(StreamReader sr)
		{
			MemoryStream outStream = new MemoryStream();
			StreamWriter sw = new StreamWriter(outStream);
			_importErrorPresent = false;
			string str;
			str = sr.ReadLine();
			if (str == null || !str.ToUpper().Contains("BEGIN:VCALENDAR"))
			{
				ThrowInalidVCalInfo();
			}

			while (true)
			{
				str = sr.ReadLine();
				if (str == null)
				{
					break;
				}
				else if (str.Length == 0 )
				{
					continue;
				}
				string[] strs = str.Split(':');
				if (strs.Length < 2)
				{
					continue;
				}
				if (strs[0].ToUpper() == "BEGIN" && strs[1].ToUpper() == "VTIMEZONE")
				{
#if (!SILVERLIGHT)
					if (!ICalTimeZoneInfo.ReadTimeZone(sr, _storage))
					{
						_importErrorPresent = true;
					}
#endif
				}
				else
				{
					sw.WriteLine(str);
				}
			}
			sw.Flush();
			return outStream;
		}

		private void Read(StreamReader sr)
		{
			_importErrorPresent = false;
			_componentone = false;
			string str;
			str = sr.ReadLine();
			if (str.Contains("PRODID") && str.Contains("ComponentOne"))
			{
				_componentone = true;
			}

			while (true)
			{
				str = sr.ReadLine();
				if (str == null)
				{
					break;
				} 
				else if (str.Length == 0 )
				{
					continue;
				}
				string[] strs = str.Split(':');
				if ( strs.Length < 2)
				{
					ThrowInalidVCalInfo();
				}
				switch (strs[0].ToUpper())
				{
					case "BEGIN":
						if (strs[1].ToUpper() == "VEVENT" )
						{
							if (!ReadAppointment(sr))
							{
								_importErrorPresent = true;
							}
						}
						break;
					case "END":
						if (strs[1].ToUpper() == "VCALENDAR")
						{
							if (_importErrorPresent)
							{
#if END_USER_LOCALIZATION
								throw new System.ApplicationException(Strings.C1Schedule.Exceptions.Item("NotAllInforImported", _storage.Info.CultureInfo));
#elif WINFX
								throw new System.ApplicationException(C1Localizer.GetString("Exceptions", "NotAllInforImported", "Not all information was imported due to not valid data.", _storage.Info.CultureInfo));
#elif SILVERLIGHT
								throw new System.Exception(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.NotAllInforImported);
#else
								throw new System.ApplicationException(C1Localizer.GetString("Not all information was imported due to not valid data."));
#endif
							}
							return;
						}
						break;
					default:
						continue;
				}
			}
			ThrowInalidVCalInfo();
		}

		private bool ReadAppointment(StreamReader sr)
		{
			Appointment app = new Appointment();
			app.ParentCollection = _storage.AppointmentStorage.Appointments;
			List<ICalPattern> patterns = new List<ICalPattern>();
			List<ICalPattern> exrules = new List<ICalPattern>();
			List<DateTime> rdates = new List<DateTime>();
			List<DateTime> exdates = new List<DateTime>();
			List<Period> rperiods = new List<Period>();
			List<Period> experiods = new List<Period>();
			DateTime start = app.Start;
			DateTime end = DateTime.MinValue;
			bool date = false;
			string str = sr.ReadLine();
            string comment = string.Empty;
            bool isStatusSet = false;
            string status = string.Empty;
			while (str != null)
			{
				if (str.Length != 0)
				{
					string[] strs;
					if (str.IndexOf(';') > 0 && str.IndexOf(';') < str.IndexOf(':'))
					{
						strs = str.Split(new char[] { ':', ';' }
#if (!SILVERLIGHT)
							, 3
#endif
							);
						if (strs.Length < 2)
						{
							return false;
						}
					}
					else
					{
						strs = str.Split(new char[] { ':' }
#if (!SILVERLIGHT)
							, 2
#endif
);
						if (strs.Length < 2)
						{
							return false;
						}
						// replace escape sequences
						strs[1] = ReplaceEscapeChars(strs[1]);
					}
					switch (strs[0].ToUpper())
					{
						// ------------------------------------
						#region ** UID
						case "UID":
							string st = strs[1].ToUpper();
							if (st.Contains("@"))
							{
								st = strs[1].Remove(st.IndexOf('@'));
							}
							try 
							{
								app.Id = new Guid(st);
							}
							catch {}
							break;
						#endregion
						// ------------------------------------
						#region ** DTSTART
						case "DTSTART":
							if (strs.Length == 2)
							{
								if (!ICalUtils.ParseTime(strs[1].ToUpper().Trim(), "", out start))
								{
									return false;
								}
							}
							else
							{
								if (strs[1].Contains("VALUE=DATE:"))
								{
									date = true;
								}
								if (!ICalUtils.ParseTime(strs[2].ToUpper().Trim(), strs[1], out start))
								{
									return false;
								}
							}
							break;
						#endregion
						// ------------------------------------
						#region ** DTEND
						case "DTEND":
							if (strs.Length == 2)
							{
								if (!ICalUtils.ParseTime(strs[1].ToUpper().Trim(), "", out end))
								{
									return false;
								}
							}
							else
							{
								if (!ICalUtils.ParseTime(strs[2].ToUpper().Trim(),  strs[1], out end))
								{
									return false;
								}
							}
							break;
						#endregion
						// ------------------------------------
						#region ** DESCRIPTION
						case "DESCRIPTION":
                            app.Body = ReplaceEscapeChars(str.Substring(str.IndexOf(':') + 1));
							break;
						#endregion
						// ------------------------------------
						#region ** SUMMARY
						case "SUMMARY":
                            app.Subject = ReplaceEscapeChars(str.Substring(str.IndexOf(':') + 1));
							break;
						#endregion
						// ------------------------------------
						#region ** CLASS
						case "CLASS":
							if (strs[1] == "PUBLIC")
							{
								app.Private = false;
							}
							else
							{
								app.Private = true;
							}
							break;
						#endregion
						// ------------------------------------
						#region ** CATEGORIES
						case "CATEGORIES":
							string[] cats;
							if (strs.Length == 2)
							{
								cats = strs[1].Split(',');
							}
							else
							{
								cats = strs[2].Split(',');
							}
							foreach (string s in cats)
							{
								Category cat = new Category();
								cat.Text = s.Replace('\\', ' ').Trim();
                                cat = MergeItem<Category, BaseObjectMappingCollection<Category>>
                                    (_storage.CategoryStorage, cat);
                                app.Categories.Add(cat);
							}
							break;
						#endregion
						// ------------------------------------
						#region ** RESOURCES
						case "RESOURCES":
							string[] ress;
							if (strs.Length == 2)
							{
								ress = strs[1].Split(',');
							}
							else
							{
								ress = strs[2].Split(',');
							}
							foreach (string s in ress)
							{
								Resource res = new Resource();
								res.Text = s.Replace('\\', ' ').Trim();
								res = MergeItem<Resource, BaseObjectMappingCollection<Resource>>
									(_storage.ResourceStorage, res);
								app.Resources.Add(res);
							}
							break;
						#endregion
						// ------------------------------------
						#region ** LOCATION
						case "LOCATION":
                            app.Location = ReplaceEscapeChars(str.Substring(str.IndexOf(':') + 1));
							break;
						#endregion
                        // ------------------------------------
                        #region ** X-C1-CUSTOMDATA
                        case "X-C1-CUSTOMDATA":
                            app.CustomData = ReplaceEscapeChars(str.Substring(str.IndexOf(':') + 1));
                            break;
                        #endregion
                        // ------------------------------------
                        #region ** COMMENT
                        case "COMMENT":
                            comment = ReplaceEscapeChars(str.Substring(str.IndexOf(':') + 1));
                            break;
                        #endregion
                        // ------------------------------------
                        #region ** X-C1-LABEL
                        case "X-C1-LABEL":
                            Label l = new Label(strs[2]);
                            l.SetId(new Guid(strs[1]));
                            l = MergeItem<Label, BaseObjectMappingCollection<Label>>
                                (_storage.LabelStorage, l);
                            app.Label = l;
                            break;
                        #endregion
                        // ------------------------------------
						#region ** PRIORITY
						case "PRIORITY":
							int pr = 5;
							if ( int.TryParse(strs[1].Trim(), out pr) )
							{
								if (pr < 5)
								{
									app.Importance = ImportanceEnum.High;
								}
								else if (pr == 5)
								{
									app.Importance = ImportanceEnum.Normal;
								}
								else
								{
									app.Importance = ImportanceEnum.Low;
								}
							}
							else
							{
								return false;
							}
							break;
						#endregion
						// ------------------------------------
						#region ** STATUS
                        case "X-C1-STATUS":
                            Status stat = new Status(strs[2]);
                            stat.SetId(new Guid(strs[1]));
                            stat = MergeItem<Status, BaseObjectMappingCollection<Status>>
                                (_storage.StatusStorage, stat);
                            app.BusyStatus = stat;
                            isStatusSet = true;
                            break;
                        case "STATUS":
                            status = strs[1];
                            break;
                        case "TRANSP":
                            if (!_componentone)
                            {
                                if (strs[1].ToUpper() == "OPAQUE")
                                {
                                    status = "Busy";
                                }
                                else
                                {
                                    status = "Free";
                                }
                            }
                            break;
                        #endregion
						// ------------------------------------
						#region ** CONTACT
						case "CONTACT":
							Contact cont = new Contact();
							if (strs.Length == 2)
							{
								cont.Text = strs[1];
							}
							else
							{
								cont.Text = strs[2];
							}
							cont = MergeItem<Contact, BaseObjectMappingCollection<Contact>>
								(_storage.ContactStorage, cont);
							app.Links.Add(cont);
							break;
						#endregion
						// ------------------------------------
						#region ** RRULE
						case "RRULE":
							// it is possible that appointment has multiple RRULE entries
							if (!PatternInfo.ReadPattern(strs[1], patterns))
							{
								return false;
							}
							break;
						#endregion
						// ------------------------------------
						#region ** RDATE
						case "RDATE":
							if (strs.Length == 3 )
							{
								if (strs[2].Contains("/"))
								{
									if (!Period.ParsePeriodList(strs[2], rperiods, strs[1]))
									{
										return false;
									}
								}
								else
								{
									if (!ICalUtils.ParseTimeList(strs[2], rdates, strs[1]))
									{
										return false;
									}
								}
							}
							else
							{
								if (!ICalUtils.ParseTimeList(strs[1], rdates, ""))
								{
									return false;
								}
							}
							break;
						#endregion
						// ------------------------------------
						#region ** EXDATE
						case "EXDATE":
							if (strs.Length == 3)
							{
								if (strs[2].Contains("/"))
								{
									if (!Period.ParsePeriodList(strs[2], experiods, strs[1]))
									{
										return false;
									}
								}
								else
								{
									if (!ICalUtils.ParseTimeList(strs[2], exdates, strs[1]))
									{
										return false;
									}
								}
							}
							else
							{
								if (!ICalUtils.ParseTimeList(strs[1], exdates, ""))
								{
									return false;
								}
							}
							List<DateTime> exdatesCorrected = new List<DateTime>();
							foreach (DateTime d in exdates)
							{
								exdatesCorrected.Add(d.Date);
							}
							exdates = exdatesCorrected;
							break;
						#endregion
						// ------------------------------------
						#region ** EXRULE
						case "EXRULE":
							// count all exceptions for limited time (not more that a year)
							if (!PatternInfo.ReadPattern(strs[1], exrules))
							{
								return false;
							}
							break;
						#endregion
						// ------------------------------------
						#region ** BEGIN
						case "BEGIN":
							switch (strs[1].ToUpper())
							{
								case "VALARM":
									if (!ICalUtils.ReadAlarm(sr, app))
									{
										return false;
									}
									break;
								default:
									break;
							}
							break;
						#endregion
						// ------------------------------------
						#region ** END
						case "END":
							if (strs[1].ToUpper().Contains("VEVENT"))
							{
								// end event
								app.Start = start;
								if (end == DateTime.MinValue)
								{
									if (date)
									{
										app.End = app.Start.AddDays(1);
									}
									else
									{
										app.End = app.Start;
									}
								}
								else
								{
									app.End = end;
								}
								if (app.Start.TimeOfDay.TotalMinutes == 0 && 
										app.End.TimeOfDay.TotalMinutes == 0	&& app.Duration.TotalDays >= 1)
								{
									app.AllDayEvent = true;
								}

								// check if any patterns exist
								if (patterns.Count > 0)
								{
									List<Appointment> list = GetPatterns(app, patterns, exrules,
																rdates, exdates, rperiods, experiods);
									foreach (Appointment ap in list)
									{
										_appointments.Add(ap);
									}
								}
								else if (rdates.Count > 0)
								{
									List<Appointment> list =
										GetAppointments(app, exrules, rdates, exdates, rperiods, experiods);
									foreach (Appointment ap in list)
									{
										_appointments.Add(ap);
									}
								}
                                if ( !string.IsNullOrEmpty(comment))
                                {
                                    // todo: add Appointment.Comment property
                                    if (string.IsNullOrEmpty(app.CustomData))
                                    {
                                        app.CustomData = comment;
                                    }
                                    else if (app.Tag == null)
                                    {
                                        app.Tag = comment;
                                    }
                                }
                                if (!isStatusSet && !string.IsNullOrEmpty(status))
                                {
                                    Status sta = new Status();
                                    sta.Text = status;
                                    sta = MergeItem<Status, BaseObjectMappingCollection<Status>>
                                        (_storage.StatusStorage, sta);
                                    app.BusyStatus = sta;
                                }

								_appointments.Add(app);
								return true;
							}
							break;
						#endregion
						default:
							break;
					}
				}
				str = sr.ReadLine();
			}
			return false;
		}

        private string ReplaceEscapeChars(string source)
        {
            string result = source.Replace("\\,", ",");
            result = result.Replace("\\;", ";");
            result = result.Replace("\\n", "\n");
            result = result.Replace("\\r", "\r");
            return result;
        }

		internal List<Appointment> GetPatterns(Appointment master,
												List<ICalPattern> patterns, List<ICalPattern> exrules,
												List<DateTime> rdates, List<DateTime> exdates,
												List<Period> rperiods, List<Period> experiods)
		{
			// ---------------------------------------
			#region ** create additional appointments for multiple patterns
			List<Appointment> list = new List<Appointment>();
			List<RecurrencePattern> filledpatterns = new List<RecurrencePattern>();
			RecurrencePattern pattern;
			while (patterns.Count > 1)
			{
				Appointment appNew = new Appointment();
				appNew.CopyFrom(master, false);
				pattern = appNew.GetRecurrencePattern();
				patterns[1].FillPattern(pattern, _storage.Info);
				filledpatterns.Add(pattern);
				list.Add(appNew);
				patterns.RemoveAt(1);
			}
			#endregion

			// ---------------------------------------
			#region ** create exception patterns
			List<Appointment> exlist = new List<Appointment>();
			List<RecurrencePattern> expatterns = new List<RecurrencePattern>();
			while (exrules.Count > 0)
			{
				Appointment appNew = new Appointment();
				appNew.CopyFrom(master, false);
				pattern = appNew.GetRecurrencePattern();
				exrules[0].FillPattern(pattern, _storage.Info);
				expatterns.Add(pattern);
				exlist.Add(appNew);
				exrules.RemoveAt(0);
			}
			#endregion

			// ---------------------------------------
			#region ** fill pattern for master appointment
			pattern = master.GetRecurrencePattern();
			patterns[0].FillPattern(pattern, _storage.Info);
			filledpatterns.Add(pattern);
			#endregion

			// ---------------------------------------
			#region ** count additional exdates from ex patterns
			while (expatterns.Count > 0)
			{
				List<DateTime> ex = expatterns[0].GetValidDates(pattern.PatternStartDate,
								pattern.PatternEndDate, _storage.Info);
				foreach (DateTime date in ex)
				{
					if (!exdates.Contains(date))
					{
						exdates.Add(date);
					}
				}
				expatterns.RemoveAt(0);
			}
			#endregion

			// ---------------------------------------
			#region ** count additional exdates from ex periods
			while (experiods.Count > 0)
			{
				DateTime start = experiods[0].Start.Date;
				DateTime end = experiods[0].End.Date;
				while (start <= end)
				{
					if (!exdates.Contains(start))
					{
						exdates.Add(start);
					}
					start = start.AddDays(1);
				}
				experiods.RemoveAt(0);
			}
			#endregion

			// ---------------------------------------
			#region ** count additional rdates from r periods
			while (rperiods.Count > 0)
			{
				DateTime start = rperiods[0].Start.Date;
				DateTime end = rperiods[0].End.Date;
				while (start <= end)
				{
					if (!rdates.Contains(start))
					{
						rdates.Add(start);
					}
					start = start.AddDays(1);
				}
				rperiods.RemoveAt(0);
			}
			#endregion

			// ---------------------------------------
			#region ** exclude exdates
			foreach (DateTime r in rdates)
			{
				if (exdates.Contains(r.Date))
				{
					exdates.Remove(r.Date);
				}
			}
			foreach (DateTime ex in exdates)
			{
				foreach (RecurrencePattern pat in filledpatterns)
				{
					if (pat.IsValid(ex, _storage.Info))
					{
						Appointment app = pat.ParentAppointment.CopyForPattern();
						app.SetStart(ex);
						app.ParentRecurrence = pat.ParentAppointment;
						app.RecurrenceState = RecurrenceStateEnum.Removed;
						pat.RemovedOccurrences.Add(app);
					}
				}
			}
			#endregion

			// ---------------------------------------
			#region ** include rdates as exceptions
			foreach (DateTime r in rdates)
			{
				Appointment app = master.CopyForPattern();
				app.SetStart(r);
				app.SetDuration(master.Duration);
				app.ParentRecurrence = master;
				app.RecurrenceState = RecurrenceStateEnum.Exception;
				if (master.Reminder != null)
				{
					app.SetReminder();
				}
				pattern.Exceptions.Add(app);
			}
			#endregion

			foreach (RecurrencePattern pat in filledpatterns)
			{
				pat.SetDirty();
			}

			return list;
		}

		internal List<Appointment> GetAppointments(Appointment master,
												List<ICalPattern> exrules,
												List<DateTime> rdates, List<DateTime> exdates,
												List<Period> rperiods, List<Period> experiods)
		{
			List<Appointment> list = new List<Appointment>();

			// ---------------------------------------
			#region ** create exception patterns
			List<Appointment> exlist = new List<Appointment>();
			List<RecurrencePattern> expatterns = new List<RecurrencePattern>();
			while (exrules.Count > 0)
			{
				Appointment appNew = new Appointment();
				appNew.CopyFrom(master, false);
				RecurrencePattern pattern = appNew.GetRecurrencePattern();
				exrules[0].FillPattern(pattern, _storage.Info);
				expatterns.Add(pattern);
				exlist.Add(appNew);
				exrules.RemoveAt(0);
			}
			#endregion

			// ---------------------------------------
			#region ** count additional exdates from ex periods
			while (experiods.Count > 0)
			{
				DateTime start = experiods[0].Start.Date;
				DateTime end = experiods[0].End.Date;
				while (start <= end)
				{
					if (!exdates.Contains(start))
					{
						exdates.Add(start);
					}
					start = start.AddDays(1);
				}
				experiods.RemoveAt(0);
			}
			#endregion

			// ---------------------------------------
			#region ** count additional rdates from r periods
			while (rperiods.Count > 0)
			{
				DateTime start = rperiods[0].Start.Date;
				DateTime end = rperiods[0].End.Date;
				while (start <= end)
				{
					if (!rdates.Contains(start))
					{
						rdates.Add(start);
					}
					start = start.AddDays(1);
				}
				rperiods.RemoveAt(0);
			}
			#endregion		
	
			// ---------------------------------------
			#region ** exclude exdates
			foreach (DateTime e in exdates)
			{
				if (rdates.Contains(e))
				{
					rdates.Remove(e);
				}
			}
			#endregion

			// ---------------------------------------
			#region ** remove dates from ex patterns
			foreach (DateTime date in rdates)
			{
				foreach (RecurrencePattern pat in expatterns)
				{
					if (pat.IsValid(date, _storage.Info))
					{
						rdates.Remove(date);
					}
				}
			}
			#endregion

			// ---------------------------------------
			#region ** create a new appointment for each rdate
			foreach (DateTime r in rdates)
			{
				Appointment app = master.CopyForPattern();
				app.SetStart(r.Add(master.Start.TimeOfDay));
				app.SetDuration(master.Duration);
				app.RecurrenceState = RecurrenceStateEnum.NotRecurring;
				if (master.Reminder != null)
				{
					app.SetReminder();
				}
				list.Add(app);
			}
			#endregion
			return list;
		}
		#endregion	

		#region merge
		private static T MergeItem<T, TMappingCollection>(BaseStorage<T, TMappingCollection> storage, T obj)
			where T : BaseObject, new()
			where TMappingCollection : MappingCollectionBase<T>, new()
		{
			// find if the object already exists in the collection
			T o = null;
			if (!obj.Id.Equals(Guid.Empty)
				&& storage.Objects.Contains(obj.Id))
			{
				o = storage.Objects[obj.Id];
			}
			else if (obj.Index != -1
				&& storage.Objects.Contains(obj.Index))
			{
				o = storage.Objects.FindByIndex(obj.Index);
			}
			else
			{
				// if there is no id and index,
				// try to find the object with the same text
				foreach (T l in storage.Objects)
				{
					if (l.Text.ToUpper().Equals(obj.Text.ToUpper()))
					{
						o = l;
						break;
					}
				}
			}
			if (o != null)
			{
				return o;
			}
			else
			{
				if (obj.Id.Equals(Guid.Empty)
					&& storage.Mappings.IdMapping.IsMapped)
				{
					obj.Id = Guid.NewGuid();
				}
				storage.Objects.Add(obj);
				return obj;
			}
		}
		#endregion
	}	
}
