﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

namespace C1.C1Schedule
{
	/// <summary>
	/// 
	/// </summary>
	internal class ICalPattern
	{
		//----------------------------------------------------------------
		#region ** fields
		private RecurrenceTypeEnum _recurrenceType;
		private int _interval;
		private int _occurrences;
		private WeekDaysEnum _dayOfWeekMask;
		private int _dayOfMonth;
		private int _monthOfYear;
		private bool _noEndDate;
		private WeekOfMonthEnum _instance;
		private DateTime _start;
		private TimeSpan _duration;
		private DateTime _patternStartDate;
		private DateTime _patternEndDate = DateTime.MaxValue;
		private bool _changed;
		private List<DateTime> _countedOccurrences;
		private DayOfWeek _weekStart = DayOfWeek.Monday;

		// lazy 
		private List<DayOfWeek> _listDays;
		#endregion

		//----------------------------------------------------------------
		#region ** ctor
		internal ICalPattern()
		{
			_recurrenceType = RecurrenceTypeEnum.Weekly;
			_interval = 1;
			_dayOfWeekMask = WeekDaysEnum.None;
			_noEndDate = true;
			_changed = true;
			_start = DateTime.MinValue;
			_duration = TimeSpan.FromMinutes(0);
			_listDays = new List<DayOfWeek>();
			_countedOccurrences = new List<DateTime>();
		}

		internal ICalPattern(ICalPattern source)
		{
			_recurrenceType = source.RecurrenceType;
			_interval = source.Interval;
			_dayOfMonth = source.DayOfMonth;
			_monthOfYear = source.MonthOfYear;
			_noEndDate = source.NoEndDate;
			_occurrences = source.Occurrences;
			_start = source.StartTime;
			_duration = source.Duration;
			_dayOfWeekMask = source.DayOfWeekMask;
			_weekStart = source.WeekStart;
			_patternStartDate = source.PatternStartDate;
			_patternEndDate = source.PatternEndDate;
			_listDays = new List<DayOfWeek>();
			_countedOccurrences = new List<DateTime>();
			_changed = true;
		}
		#endregion

		//----------------------------------------------------------------
		#region ** object model
		public DayOfWeek WeekStart
		{
			get
			{
				return _weekStart;
			}
			set
			{
				_weekStart = value;
				_changed = true;
			}
		}

		public RecurrenceTypeEnum RecurrenceType
		{
			get
			{
				return _recurrenceType;
			}
			set
			{
				_recurrenceType = value;
				_changed = true;
			}
		}

		public int Interval
		{
			get
			{
				return _interval;
			}
			set
			{
				_interval = value;
				_changed = true;
			}
		}

		public DateTime EndTime
		{
			get
			{
				return _start + _duration;
			}
			set
			{
				if (value < _start)
				{
					ThrowInvalidTime();
				}
				_duration = value - _start;
				_changed = true;
			}
		}

		public DateTime StartTime
		{
			get
			{
				return _start;
			}
			set
			{
				_start = value;
				_changed = true;
			}
		}

		public TimeSpan Duration
		{
			get
			{
				return _duration;
			}
			set
			{
				_duration = value;
				_changed = true;
			}
		}

		public DateTime PatternStartDate
		{
			get
			{
				return _patternStartDate;
			}
			set
			{
				_patternStartDate = value;
				_changed = true;
			}
		}
		public DateTime PatternEndDate
		{
			get
			{
				return _patternEndDate;
			}
			set
			{
				if (value < _patternStartDate)
				{
					ThrowInvalidTime();
				}
				_patternEndDate = value;
				_occurrences = 0;
				_noEndDate = false;
				_changed = true;
			}
		}
		public int Occurrences
		{
			get
			{
				return _occurrences;
			}
			set
			{
				_occurrences = value;
				if (_occurrences <= 0)
				{
					_noEndDate = true;
				}
				else
				{
					_noEndDate = false;
				}
				_changed = true;
			}
		}
		// positive or negative number
		// translate it into ?WeekOfMonthEnum for month patterns and to DayOfMonth
		// for year pattern?
		public WeekOfMonthEnum Instance
		{
			get
			{

				return _instance;
			}
			set
			{
				_instance = value;
				_changed = true;
			}
		}
		public WeekDaysEnum DayOfWeekMask
		{
			get
			{
				return _dayOfWeekMask;
			}
			set
			{
				if (_dayOfWeekMask != value)
				{
					_listDays.Clear();
					_dayOfWeekMask = value;
					_changed = true;
				}
			}
		}

		public int DayOfMonth
		{
			get
			{
				return _dayOfMonth;
			}
			set
			{
				_dayOfMonth = value;
				_changed = true;
			}
		}
		public int MonthOfYear
		{
			get
			{
				return _monthOfYear;
			}
			set
			{
				_monthOfYear = value;
				_changed = true;
			}
		}
		public bool NoEndDate
		{
			get
			{
				return _noEndDate;
			}
			set
			{
				_noEndDate = value;
				if (value)
				{
					_occurrences = 0;
				}
				_changed = true;
			}
		}
		#endregion

		//----------------------------------------------------------------
		#region interface
		// converts DayOfWeekMask into list of DayOfWeek constants
		internal List<DayOfWeek> GetDaysOfWeek(CalendarInfo info)
		{
			if (_listDays.Count == 0)
			{
				if (_dayOfWeekMask == WeekDaysEnum.EveryDay)
				{
					for (int i = 0; i < 7; i++)
					{
						_listDays.Add((DayOfWeek)i);
					}
				}
				else if (_dayOfWeekMask == WeekDaysEnum.WorkDays)
				{
					foreach (DayOfWeek day in info.WorkDays)
					{
						_listDays.Add(day);
					}
				}
				else if (_dayOfWeekMask == WeekDaysEnum.WeekendDays)
				{
					for (int i = 0; i < 7; i++)
					{
						if (!info.WorkDays.Contains((DayOfWeek)i))
						{
							_listDays.Add((DayOfWeek)i);
						}
					}
				}
				else
				{
					if ((_dayOfWeekMask & WeekDaysEnum.Friday) != 0)
					{
						_listDays.Add(DayOfWeek.Friday);
					}
					if ((_dayOfWeekMask & WeekDaysEnum.Monday) != 0)
					{
						_listDays.Add(DayOfWeek.Monday);
					}
					if ((_dayOfWeekMask & WeekDaysEnum.Saturday) != 0)
					{
						_listDays.Add(DayOfWeek.Saturday);
					}
					if ((_dayOfWeekMask & WeekDaysEnum.Sunday) != 0)
					{
						_listDays.Add(DayOfWeek.Sunday);
					}
					if ((_dayOfWeekMask & WeekDaysEnum.Thursday) != 0)
					{
						_listDays.Add(DayOfWeek.Thursday);
					}
					if ((_dayOfWeekMask & WeekDaysEnum.Tuesday) != 0)
					{
						_listDays.Add(DayOfWeek.Tuesday);
					}
					if ((_dayOfWeekMask & WeekDaysEnum.Wednesday) != 0)
					{
						_listDays.Add(DayOfWeek.Wednesday);
					}
				}
				_listDays.Sort();
			}
			return _listDays;
		}

		internal bool IsValid(DateTime date, CalendarInfo info)
		{
			GetDaysOfWeek(info);
			date = date.Date;
			if (!NoEndDate && date > PatternEndDate.Date)
			{
				return false;
			}
			if (_changed)
			{
				_countedOccurrences.Clear();
				_changed = false;
			}
			if (_countedOccurrences.Contains(date.Date))
			{
				return true;
			}
			bool valid = false;
			// check pattern
			switch (RecurrenceType)
			{
				// -----------------------------------------------
				#region ** Daily
				case RecurrenceTypeEnum.Daily:
					if (((TimeSpan)(date - PatternStartDate.Date)).TotalDays % _interval == 0)
					{
						valid = true;
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Monthly
				case RecurrenceTypeEnum.Monthly:
					if ((date.Month - PatternStartDate.Month + 1) % _interval == 0)
					{
						// if pattern set to the 28-31 day of month, then for shot months 
						// the last day of month is valid
						int daysInMonth = CalendarInfo.Culture.Calendar.GetDaysInMonth(date.Year, date.Month);
						int validDay = (_dayOfMonth > daysInMonth) ? daysInMonth : _dayOfMonth;
						if (date.Day == validDay)
						{
							valid = true;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** MonthlyNth
				case RecurrenceTypeEnum.MonthlyNth:
					if (_listDays.Count == 0)
					{
						return false;
					}
					if ((date.Month - PatternStartDate.Month + 1) % _interval == 0
						&& _listDays.Contains(date.DayOfWeek))
					{
						valid = IsValidInstance(date);
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Weekly
				case RecurrenceTypeEnum.Weekly:
					if ((((TimeSpan)(date - _patternStartDate)).TotalDays) % (7 * _interval) < 7)
					{
						if (_listDays.Contains(date.DayOfWeek))
						{
							valid = true;
							break;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Yearly
				case RecurrenceTypeEnum.Yearly:
					if (date.Month == _monthOfYear)
					{
						// if pattern set to the 28-31 day of month, then for shot months 
						// the last day of month is valid
						int daysInMonth = CalendarInfo.Culture.Calendar.GetDaysInMonth(date.Year, date.Month);
						int validDay = (_dayOfMonth > daysInMonth) ? daysInMonth : _dayOfMonth;
						if (date.Day == validDay)
						{
							valid = true;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** YearlyNth
				case RecurrenceTypeEnum.YearlyNth:
					if (_listDays.Count == 0)
					{
						return false;
					}
					if ((date.Year - PatternStartDate.Year + 1) % _interval == 0
						&& date.Month == _monthOfYear
						&& _listDays.Contains(date.DayOfWeek))
					{
						valid = IsValidInstance(date);
					}
					break;
				#endregion
			}
			if (valid && Occurrences > 0 && !NoEndDate)
			{
				_countedOccurrences.Add(date.Date);
				_occurrences--;
			}
			return valid;
		}

		private bool IsValidInstance(DateTime date)
		{
			bool valid = false;
			if ((int)_instance == 5)
			{
				DateTime lastDay = new DateTime(date.AddMonths(1).Year, date.AddMonths(1).Month, 1);
				DateTime firstDay = lastDay.Subtract(TimeSpan.FromDays(7));

				if (date >= firstDay)
				{
					valid = true;
					firstDay = date.AddDays(1);
				}
				while (firstDay < lastDay)
				{
					if (_listDays.Contains(firstDay.DayOfWeek))
					{
						valid = false;
						break;
					}
					firstDay = firstDay.AddDays(1);
				}
			}
			else
			{
				DateTime firstDay;
				DateTime lastDay;
				if (DayOfWeekMask == WeekDaysEnum.EveryDay)
				{
					firstDay = new DateTime(date.Year, date.Month, (int)_instance);
					lastDay = firstDay.AddDays(1);
				}
				else if (DayOfWeekMask == WeekDaysEnum.WorkDays || DayOfWeekMask == WeekDaysEnum.WeekendDays)
				{
					int c = 0;
					firstDay = new DateTime(date.Year, date.Month, 1).AddDays(-1);
					while (c < (int)_instance)
					{
						firstDay = firstDay.AddDays(1);
						if (_listDays.Contains(firstDay.DayOfWeek))
						{
							c++;
						}
					}
					lastDay = firstDay.AddDays(1);
				}
				else
				{
					firstDay = new DateTime(date.Year, date.Month, 1).AddDays(((int)_instance - 1) * 7);
					lastDay = firstDay.AddDays(7);
				}
				if (date >= firstDay && date < lastDay)
				{
					valid = true;
					lastDay = date.AddDays(-1);
				}
				while (firstDay < lastDay)
				{
					if (_listDays.Contains(lastDay.DayOfWeek))
					{
						valid = false;
						break;
					}
					lastDay = lastDay.AddDays(-1);
				}
			}
			return valid;
		}

		internal void FillPattern(RecurrencePattern pattern, CalendarInfo info)
		{
			// get missing parameters from Appointment.Start
			switch (_recurrenceType)
			{
				case RecurrenceTypeEnum.Monthly:
					if (_dayOfMonth == 0)
					{
						_dayOfMonth = pattern.ParentAppointment.Start.Day;
					}
					break;
				case RecurrenceTypeEnum.MonthlyNth:
				case RecurrenceTypeEnum.Workdays:
				case RecurrenceTypeEnum.Weekly:
					if (_dayOfWeekMask == WeekDaysEnum.None)
					{
						_dayOfWeekMask = PatternInfo.GetDayOfWeekMask(pattern.ParentAppointment.Start.DayOfWeek);
					}
					CorrectDayOfWeekMask(info);
					break;
				case RecurrenceTypeEnum.Yearly:
					if (_dayOfMonth == 0)
					{
						_dayOfMonth = pattern.ParentAppointment.Start.Day;
					}
					if (_monthOfYear == 0)
					{
						_monthOfYear = pattern.ParentAppointment.Start.Month;
					}
					break;
				case RecurrenceTypeEnum.YearlyNth:
					if (_dayOfWeekMask == WeekDaysEnum.None)
					{
						_dayOfWeekMask = PatternInfo.GetDayOfWeekMask(pattern.ParentAppointment.Start.DayOfWeek);
					}
					CorrectDayOfWeekMask(info);
					if (_monthOfYear == 0)
					{
						_monthOfYear = pattern.ParentAppointment.Start.Month;
					}
					break;
			}			
			
			// fill C1Schedule's pattern from this class
			pattern.RecurrenceType = _recurrenceType;
			pattern.Interval = _interval;
			pattern.Instance = _instance;
			pattern.Occurrences = _occurrences;
			pattern.DayOfWeekMask = _dayOfWeekMask;
			pattern.DayOfMonth = _dayOfMonth;
			pattern.MonthOfYear = _monthOfYear;
			pattern.NoEndDate = _noEndDate;
			TimeSpan duration = pattern.ParentAppointment.Duration;
			pattern.StartTime = pattern.ParentAppointment.Start;
			if (_start != DateTime.MinValue && pattern.StartTime.TimeOfDay != _start.TimeOfDay)
			{
				pattern.StartTime =	pattern.ParentAppointment.Start = 
					pattern.ParentAppointment.Start.Date.Add(_start.TimeOfDay);
			}
			pattern.Duration = duration;
			pattern.PatternStartDate = pattern.ParentAppointment.Start.Date;
			if (_patternEndDate != DateTime.MaxValue)
			{
				pattern.PatternEndDate = _patternEndDate;
			}
		}

		private void CorrectDayOfWeekMask(CalendarInfo info)
		{
			List<DayOfWeek> list = GetDaysOfWeek(info);
			if (list.Count == 7)
			{
				_dayOfWeekMask = WeekDaysEnum.EveryDay;
			}
			else if (list.Count == info.WorkDays.Count)
			{
				bool weekdaysPattern = true;
				foreach (DayOfWeek d in list)
				{
					if (!info.WorkDays.Contains(d))
					{
						weekdaysPattern = false;
						break;
					}
				}
				if (weekdaysPattern)
				{
					_dayOfWeekMask = WeekDaysEnum.WorkDays;
				}
			}
			else if (list.Count == 7 - info.WorkDays.Count)
			{
				bool weekendPattern = true;
				foreach (DayOfWeek d in list)
				{
					if (info.WorkDays.Contains(d))
					{
						weekendPattern = false;
						break;
					}
				}
				if (weekendPattern)
				{
					_dayOfWeekMask = WeekDaysEnum.WeekendDays;
				}
			}
			if (_dayOfWeekMask == WeekDaysEnum.WorkDays && _recurrenceType == RecurrenceTypeEnum.Weekly)
			{
				_recurrenceType = RecurrenceTypeEnum.Workdays;
			}
		}
		#endregion

		#region ** private
		private void ThrowInvalidTime()
		{
#if END_USER_LOCALIZATION
			throw new System.ApplicationException(C1.Win.C1Schedule.Localization.Strings.C1Schedule.Exceptions.StartEndValidationFailed);
#elif WINFX
			throw new System.ApplicationException(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "StartEndValidationFailed", "The End value should be greater than Start value.", null));
#elif SILVERLIGHT
            throw new System.ArgumentException(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.StartEndValidationFailed);
#else
			throw new System.ArgumentException(C1Localizer.GetString("The End value should be greater than Start value."));
#endif
		}
		#endregion
	}

	internal class PatternInfo
	{
		//----------------------------------------------------------------
		#region ** fields
		private RecurrenceTypeEnum _recurrenceType;
		private int _interval = 1;
		private int _occurrences;
		private DateTime _patternEndDate = DateTime.MaxValue;
		private DayOfWeek _weekStart = DayOfWeek.Monday;

		private List<DateTime> _times;
		private List<ByDayPattern> _byDayList;
		private List<int> _byMonthDayList;
		private List<int> _byMonthList;
		private List<int> _bySetPosList;
		#endregion

		//----------------------------------------------------------------
		#region ** ctor
		private PatternInfo()
		{
			_recurrenceType = RecurrenceTypeEnum.Weekly;
			_times = new List<DateTime>();
			_byDayList = new List<ByDayPattern>();
			_byMonthDayList =  new List<int>();
			_byMonthList = new List<int>();
			_bySetPosList = new List<int>();
		}

		internal static bool ReadPattern(string str, List<ICalPattern> list)
		{
			PatternInfo pattern = new PatternInfo();
			string[] elements = str.Split(';');
			foreach (string s in elements)
			{
				string[] strs = s.Split('=');
				switch (strs[0].ToUpper())
				{
					// -------------------------------------------------------------
					#region ** FREQ
					case "FREQ":
						{
							switch (strs[1].ToUpper())
							{
								case "WEEKLY":
									pattern._recurrenceType = RecurrenceTypeEnum.Weekly;
									break;
								case "MONTHLY":
									pattern._recurrenceType = RecurrenceTypeEnum.Monthly;
									break;
								case "YEARLY":
									pattern._recurrenceType = RecurrenceTypeEnum.Yearly;
									break;
								case "DAILY":
									pattern._recurrenceType = RecurrenceTypeEnum.Daily;
									break;
								default:
									// "SECONDLY", "MINUTELY", "HOURLY" patterns are not supported
									// TODO: write in readme about this limitation
									return false;
							}
						}
						break;
					#endregion
					case "UNTIL":
						if (!ICalUtils.ParseTime(strs[1].ToUpper().Trim(), "", out pattern._patternEndDate))
						{
							return false;
						}
						break;
					case "COUNT":
						if (!int.TryParse(strs[1], out pattern._occurrences))
						{
							return false;
						}
						break;
					case "INTERVAL":
						if (!int.TryParse(strs[1], out pattern._interval))
						{
							return false;
						}
						break;
					// -------------------------------------------------------------
					#region ** BYSECOND
					case "BYSECOND":
						string[] seconds = strs[1].Split(',');
						int sec = 0;
						List<int> secs = new List<int>();
						foreach (string s1 in seconds)
						{
							if (ParseNumber(s1, 0, 59, false, out sec))
							{
								secs.Add(sec);
							}
							else
							{
								return false;
							}
						}
						if (secs.Count > 0)
						{
							int count = pattern._times.Count;
							for (int i = 0; i <  count; i ++)
							{
								for ( int j = 0; j < secs.Count; j++ )
								{
									pattern._times.Add(pattern._times[i].AddSeconds(secs[j]));
								}
								pattern._times[i] = pattern._times[i].AddSeconds(secs[0]);
							}
						}
						break;
					#endregion
					// -------------------------------------------------------------
					#region ** BYMINUTE
					case "BYMINUTE":
						string[] minutes = strs[1].Split(',');
						int min = 0;
						List<int> mins = new List<int>();
						foreach (string s1 in minutes)
						{
							if (ParseNumber(s1, 0, 59, false, out min))
							{
								mins.Add(min);
							}
							else
							{
								return false;
							}
						}
						if (mins.Count > 0)
						{
							int count = pattern._times.Count;
							for (int i = 0; i < count; i++)
							{
								for (int j = 0; j < mins.Count; j++)
								{
									pattern._times.Add(pattern._times[i].AddMinutes(mins[j]));
								}
								pattern._times[i] = pattern._times[i].AddMinutes(mins[0]);
							}
						}
						break;
					#endregion
					// -------------------------------------------------------------
					#region ** BYHOUR
					case "BYHOUR":
						string[] hours = strs[1].Split(',');
						int hour = 0;
						List<int> hs = new List<int>();
						foreach (string s1 in hours)
						{
							if (ParseNumber(s1, 0, 23, false, out hour))
							{
								hs.Add(hour);
							}
							else
							{
								return false;
							}
						}
						if (hs.Count > 0)
						{
							int count = pattern._times.Count;
							for (int i = 0; i < count; i++)
							{
								for (int j = 0; j < hs.Count; j++)
								{
									pattern._times.Add(pattern._times[i].AddHours(hs[j]));
								}
								pattern._times[i] = pattern._times[i].AddHours(hs[0]);
							}
						}
						break;
					#endregion
					// -------------------------------------------------------------
					#region ** BYDAY
					case "BYDAY":
						// only for month or year patterns
						string[] weekdays = strs[1].Split(',');
						foreach (string weekday in weekdays)
						{
							ByDayPattern byDay = ByDayPattern.FromString(weekday);
							if (byDay != null)
							{
								pattern._byDayList.Add(byDay);
							}
						}
						break;
					#endregion
					case "BYMONTHDAY":
						if (!ParseNumberList(strs[1], 1, 31, pattern._byMonthDayList, true))
						{
							return false;
						}
						break;
					case "BYYEARDAY":
					case "BYWEEKNO":
						// BYYEARDAY, BYWEEKNO are not supported
						// TODO: write in readme about this limitation
						return false;
					case "BYMONTH":
						if (!ParseNumberList(strs[1], 1, 12, pattern._byMonthList, false))
						{
							return false;
						}
						break;
					case "BYSETPOS":
						if (!ParseNumberList(strs[1], 1, 5, pattern._bySetPosList, true)
							|| pattern._bySetPosList.Count > 1 )
						{
							return false;
						}
						break;
					case "WKST":
						DayOfWeek day;
						if (!GetDayOfWeekFromICalStr(strs[1], out day))
						{
							return false;
						}
						pattern._weekStart = day;
						break;
					default:
						break;
				}
			}
			return GeneratePatterns(pattern, list);
		}

		private static bool ValidatePattern(PatternInfo info)
		{
			switch (info._recurrenceType)
			{
				case RecurrenceTypeEnum.Weekly:
					if (info._byMonthDayList.Count > 0
						|| info._byMonthList.Count > 0
						|| info._bySetPosList.Count > 0)
					{
						return false;
					}
					foreach (ByDayPattern byDay in info._byDayList)
					{
						if (byDay.Instance != 1)
						{
							return false;
						}
					}
					break;
				case RecurrenceTypeEnum.Daily:
					if (info._byDayList.Count > 0
						|| info._byMonthDayList.Count > 0
						|| info._byMonthList.Count > 0
						|| info._bySetPosList.Count > 0)
					{
						return false;
					}
					break;
				case RecurrenceTypeEnum.Monthly:
					if (info._byMonthList.Count > 0)
					{
						return false;
					}
					if (info._bySetPosList.Count > 0)
					{
						foreach (ByDayPattern byDay in info._byDayList)
						{
							if (byDay.Instance != 1)
							{
								return false;
							}
						}
					}
					break;
				case RecurrenceTypeEnum.Yearly:
					if (info._bySetPosList.Count > 0)
					{
						foreach (ByDayPattern byDay in info._byDayList)
						{
							if (byDay.Instance != 1)
							{
								return false;
							}
						}
					}
					break;
			}
			return true;
		}

		private static bool GeneratePatterns(PatternInfo info, List<ICalPattern> list)
		{
			if (!ValidatePattern(info))
			{
				return false;
			}
			List<ICalPattern> newList = new List<ICalPattern>();
			ICalPattern pattern = new ICalPattern();
			newList.Add(pattern);
			if (info._occurrences != 0)
			{
				pattern.Occurrences = info._occurrences;
			}
			else if ( info._patternEndDate != DateTime.MaxValue)
			{
				pattern.PatternEndDate = info._patternEndDate;
			}
			pattern.Interval = info._interval;
			pattern.WeekStart = info._weekStart;
			pattern.RecurrenceType = info._recurrenceType;

			switch (info._recurrenceType)
			{
				case RecurrenceTypeEnum.Weekly:
					pattern.DayOfWeekMask = ByDayPattern.GetDayOfWeekMask(info._byDayList);
					break;
				case RecurrenceTypeEnum.Monthly:
					if (info._bySetPosList.Count > 0)
					{
						// if instance present
						pattern.RecurrenceType = RecurrenceTypeEnum.MonthlyNth;
						pattern.Instance = (WeekOfMonthEnum)info._bySetPosList[0];
						pattern.DayOfWeekMask = ByDayPattern.GetDayOfWeekMask(info._byDayList);
					}
					else
					{
						if (info._byDayList.Count > 0)
						{
							pattern.DayOfWeekMask = GetDayOfWeekMask(info._byDayList[0].DayOfWeek);
							pattern.Instance = (WeekOfMonthEnum)info._byDayList[0].Instance;
							if ((int)pattern.Instance != 1)
							{
								pattern.RecurrenceType = RecurrenceTypeEnum.MonthlyNth;
							}
							info._byDayList.RemoveAt(0);
							while (info._byDayList.Count > 0)
							{
								ICalPattern pat = new ICalPattern(pattern);
								pat.DayOfWeekMask = GetDayOfWeekMask(info._byDayList[0].DayOfWeek);
								pat.Instance = (WeekOfMonthEnum)info._byDayList[0].Instance;
								if ((int)pat.Instance != 1)
								{
									pat.RecurrenceType = RecurrenceTypeEnum.MonthlyNth;
								}
								newList.Add(pat);
								info._byDayList.RemoveAt(0);
							}
						}
						else if (info._byMonthDayList.Count > 0)
						{
							pattern.DayOfMonth = info._byMonthDayList[0];
							info._byMonthDayList.RemoveAt(0);
							while (info._byMonthDayList.Count > 0)
							{
								ICalPattern pat = new ICalPattern(pattern);
								pat.DayOfMonth = info._byMonthDayList[0];
								newList.Add(pat);
								info._byMonthDayList.RemoveAt(0);
							}
						}
					}
					break;
				case RecurrenceTypeEnum.Yearly:
					if (info._bySetPosList.Count > 0)
					{
						// if instance present
						pattern.RecurrenceType = RecurrenceTypeEnum.YearlyNth;
						pattern.Instance = (WeekOfMonthEnum)info._bySetPosList[0];
						pattern.DayOfWeekMask = ByDayPattern.GetDayOfWeekMask(info._byDayList);
					}
					else
					{
						if (info._byDayList.Count > 0)
						{
							pattern.DayOfWeekMask = GetDayOfWeekMask(info._byDayList[0].DayOfWeek);
							pattern.Instance = (WeekOfMonthEnum)info._byDayList[0].Instance;
							if ((int)pattern.Instance != 1)
							{
								pattern.RecurrenceType = RecurrenceTypeEnum.MonthlyNth;
							}
							info._byDayList.RemoveAt(0);
							while (info._byDayList.Count > 0)
							{
								ICalPattern pat = new ICalPattern(pattern);
								pat.DayOfWeekMask = GetDayOfWeekMask(info._byDayList[0].DayOfWeek);
								pat.Instance = (WeekOfMonthEnum)info._byDayList[0].Instance;
								if ((int)pat.Instance != 1)
								{
									pat.RecurrenceType = RecurrenceTypeEnum.MonthlyNth;
								}
								newList.Add(pat);
								info._byDayList.RemoveAt(0);
							}
						}
					}
					if (info._byMonthDayList.Count > 0)
					{
						pattern.DayOfMonth = info._byMonthDayList[0];
						info._byMonthDayList.RemoveAt(0);
						while (info._byMonthDayList.Count > 0)
						{
							ICalPattern pat = new ICalPattern(pattern);
							pat.DayOfMonth = info._byMonthDayList[0];
							newList.Add(pat);
							info._byMonthDayList.RemoveAt(0);
						}
					}
					if (info._byMonthList.Count > 0)
					{
						pattern.MonthOfYear = info._byMonthList[0];
						info._byMonthList.RemoveAt(0);
						while (info._byMonthList.Count > 0)
						{
							ICalPattern pat = new ICalPattern(pattern);
							pat.MonthOfYear = info._byMonthList[0];
							newList.Add(pat);
							info._byMonthList.RemoveAt(0);
						}
					}			
					break;
			}
			if (info._times.Count > 0)
			{
				foreach (ICalPattern pat in list)
				{
					pat.StartTime = info._times[0];
				}
			}
			if (info._times.Count > 1)
			{
				// create separate pattern for each time value
				int count = list.Count;
				for (int i = 1; i < info._times.Count; i++)
				{
					for (int j = 0; j < count; j++)
					{
						ICalPattern newPat = new ICalPattern(list[j]);
						newPat.StartTime = info._times[i];
						list.Add(newPat);
					}
				}
			}
			if (list.Count > 1 && info._occurrences > 0)
			{
				return false;
			}
			list.AddRange(newList);
			return true;
		}
		#endregion

		//----------------------------------------------------------------
		#region ** private helpers
		private class ByDayPattern
		{
			private ByDayPattern()
			{
			}

			public static ByDayPattern FromString(string str)
			{
				ByDayPattern byDay = new ByDayPattern();
				if (str.Length > 2)
				{
					int num = 0;
					string number = str.Substring(0, str.Length - 2);
					if (PatternInfo.ParseNumber(number, 1, 4, true, out num))
					{
						byDay._instance = num;
					}
					else
					{
						return null;
					}
					str = str.Substring(str.Length - 2, 2);
				}
				DayOfWeek day;
				if ( !PatternInfo.GetDayOfWeekFromICalStr(str, out day))
				{
					return null;
				}
				byDay._dayOfWeek = day;
				return byDay;
			}

			private int _instance = 1;
			private DayOfWeek _dayOfWeek;

			public int Instance
			{
				get
				{
					return _instance;
				}
			}

			public DayOfWeek DayOfWeek
			{
				get
				{
					return _dayOfWeek;
				}
			}

			public static WeekDaysEnum GetDayOfWeekMask(List<ByDayPattern> list)
			{
				WeekDaysEnum mask = WeekDaysEnum.None;
				foreach (ByDayPattern byDay in list)
				{
					mask |= PatternInfo.GetDayOfWeekMask(byDay.DayOfWeek);
				}
				return mask;
			}
		}

		public static WeekDaysEnum GetDayOfWeekMask(DayOfWeek day)
		{
			switch (day)
			{
				case DayOfWeek.Friday:
					return WeekDaysEnum.Friday;
				case DayOfWeek.Monday:
					return WeekDaysEnum.Monday;
				case DayOfWeek.Saturday:
					return WeekDaysEnum.Saturday;
				case DayOfWeek.Sunday:
					return WeekDaysEnum.Sunday;
				case DayOfWeek.Thursday:
					return WeekDaysEnum.Thursday;
				case DayOfWeek.Tuesday:
					return WeekDaysEnum.Tuesday;
				case DayOfWeek.Wednesday:
					return WeekDaysEnum.Wednesday;
			}
			return WeekDaysEnum.None;
		}

		private static bool ParseNumber(string str, int lowLimit, int highLimit, bool last, out int result)
		{
			str = str.ToLower().Trim();
			if (int.TryParse(str, out result) &&
				((last && result == -1 )|| (result >= lowLimit && result <= highLimit)))
			{
				if (result == -1)
				{
					result = 5;
				}
				return true;
			}
			return false;
		}

		private static bool ParseNumberList(string str, int lowLimit, int highLimit, List<int> list, bool last)
		{
			string[] positions = str.Split(',');
			int position = 0;
			foreach (string s in positions)
			{
				if (ParseNumber(s, lowLimit, highLimit, last, out position))
				{
					list.Add(position);
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		private static bool GetDayOfWeekFromICalStr(string str, out DayOfWeek day)
		{
			day = DayOfWeek.Monday;
			str = str.ToUpper().Trim();
			switch (str)
			{
				case "MO":
					day = DayOfWeek.Monday;
					break;
				case "SU":
					day = DayOfWeek.Sunday;
					break;
				case "TU":
					day = DayOfWeek.Tuesday;
					break;
				case "WE":
					day = DayOfWeek.Wednesday;
					break;
				case "TH":
					day = DayOfWeek.Thursday;
					break;
				case "FR":
					day = DayOfWeek.Friday;
					break;
				case "SA":
					day = DayOfWeek.Saturday;
					break;
				default:
					return false;
			}
			return true;
		}
		#endregion
		
		//----------------------------------------------------------------
		#region ** Write
		internal static void WriteReccurrence(StreamWriter sw, RecurrencePattern pat)
		{
			string result = "RRULE:FREQ=";

			switch (pat.RecurrenceType)
			{
				case RecurrenceTypeEnum.Daily:
					result += "DAILY";
					break;
				case RecurrenceTypeEnum.Monthly:
					result += "MONTHLY";
					result += ";BYMONTHDAY=" + pat.DayOfMonth.ToString();
					break;
				case RecurrenceTypeEnum.MonthlyNth:
					result += "MONTHLY";
					result += GetPosition(pat);
					result += GetByDayList(pat.GetDaysOfWeek());
					break;
				case RecurrenceTypeEnum.Workdays:
					result += "WEEKLY";
					result += GetByDayList(pat.Info.WorkDays);
					break;
				case RecurrenceTypeEnum.Weekly:
					result += "WEEKLY";
					result += GetByDayList(pat.GetDaysOfWeek());
					break;
				case RecurrenceTypeEnum.Yearly:
					result += "YEARLY";
					result += ";BYMONTHDAY=" + pat.DayOfMonth.ToString();
					result += ";BYMONTH=" + pat.MonthOfYear.ToString();
					break;
				case RecurrenceTypeEnum.YearlyNth:
					result += "YEARLY";
					result += ";BYMONTH=" + pat.MonthOfYear.ToString();
					result += GetPosition(pat);
					result += GetByDayList(pat.GetDaysOfWeek());
					break;
			}
			if (pat.Interval > 1)
			{
				result += ";INTERVAL=" + pat.Interval.ToString();
			}
			if (!pat.NoEndDate)
			{
				if (pat.Occurrences > 0)
				{
					result += ";COUNT=" + pat.Occurrences.ToString();
				}
				else
				{
					result += ";UNTIL=" + ICalUtils.GetDTString(pat.PatternEndDate);
				}
			}
			sw.WriteLine(result);
			// write exceptions and removed items
			if (pat.RemovedOccurrences.Count > 0 || pat.Exceptions.Count > 0)
			{
				string rem = "EXDATE;TZID=" + ICalUtils.LocalTZID + ":";
				foreach (Appointment app in pat.RemovedOccurrences)
				{
					rem += ICalUtils.GetDTString(app.Start.Date.Add(pat.ParentAppointment.Start.TimeOfDay)) + ",";
				}
				foreach (Appointment app in pat.Exceptions)
				{
					rem += ICalUtils.GetDTString(app.Start.Date.Add(pat.ParentAppointment.Start.TimeOfDay)) + ",";
				}
				sw.WriteLine(rem.Substring(0, rem.Length - 1));
			}
			if (pat.Exceptions.Count > 0)
			{
				string ex = "RDATE;TZID=" + ICalUtils.LocalTZID + ":";
				foreach (Appointment app in pat.Exceptions)
				{
					ex += ICalUtils.GetDTString(app.Start) + ",";
				}
				sw.WriteLine(ex.Substring(0, ex.Length - 1));
			}
		}

		private static string GetByDayList(List<DayOfWeek> list)
		{
			string byday = ";BYDAY=";
			if (list.Contains(DayOfWeek.Friday))
			{
				byday += "FR,";
			}
			if (list.Contains(DayOfWeek.Monday))
			{
				byday += "MO,";
			}
			if (list.Contains(DayOfWeek.Saturday))
			{
				byday += "SA,";
			}
			if (list.Contains(DayOfWeek.Sunday))
			{
				byday += "SU,";
			}
			if (list.Contains(DayOfWeek.Thursday))
			{
				byday += "TH,";
			}
			if (list.Contains(DayOfWeek.Tuesday))
			{
				byday += "TU,";
			}
			if (list.Contains(DayOfWeek.Wednesday))
			{
				byday += "WE,";
			}
			byday = byday.Substring(0, byday.Length - 1);
			return byday;
		}
		internal static string GetByDay(DayOfWeek day)
		{
			switch (day)
			{
				case DayOfWeek.Friday:
					return "FR";
				case DayOfWeek.Monday:
					return "MO";
				case DayOfWeek.Saturday:
					return "SA";
				case DayOfWeek.Sunday:
					return "SU";
				case DayOfWeek.Thursday:
					return "TH";
				case DayOfWeek.Tuesday:
					return "TU";
				case DayOfWeek.Wednesday:
					return "WE";
			}
			return string.Empty;
		}
		private static string GetPosition(RecurrencePattern pattern)
		{
			string result;
			if (pattern.Instance == WeekOfMonthEnum.Last)
			{
				result = ";BYSETPOS=-1";
			}
			else
			{
				result = ";BYSETPOS=" + ((int)pattern.Instance).ToString();
			}
			return result;
		}
		#endregion	
	}

	internal class Period
	{
		private DateTime _start;
		private DateTime _end;

		private Period()
		{
			_start = _end = DateTime.MinValue;
		}

		internal static Period FromString(string value, string tzid)
		{
			Period period= new Period();
			string[] strs = value.Split('/');
			if (!ICalUtils.ParseTime(strs[0], tzid, out period._start))
			{
				return null;
			}
			if (!ICalUtils.ParseTime(strs[1], tzid, out period._end))
			{
				TimeSpan duration;
				if (!ICalUtils.ParseDuration(strs[1], out duration))
				{
					return null;
				}
				else
				{
					period._end = period._start.Add(duration);
				}
			}
			return period;
		}

		internal static bool ParsePeriodList(string value, List<Period> list, string tzid)
		{
			bool result = true;
			string[] tokens = value.Split('/');
			foreach (string token in tokens)
			{
				Period period = FromString(token, tzid);
				if (period != null)
				{
					list.Add(period);
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		public DateTime Start
		{
			get
			{
				return _start;
			}
		}

		public DateTime End
		{
			get
			{
				return _end;
			}
		}
	}
}
