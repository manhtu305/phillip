﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace C1.C1Schedule
{
	/// <summary>
	/// Specifies the format of the exported or imported file.
	/// </summary>
	public enum FileFormatEnum
	{
		/// <summary>
		/// XML format (ComponentOne's own format).
		/// </summary>
		XML,
		/// <summary>
		/// iCal format according to RFC 2445.
		/// Format specification could be found here:
		/// http://tools.ietf.org/rfc/rfc2445.txt
		/// </summary>
		iCal,
#if (!SILVERLIGHT)
		/// <summary>
		/// Binary format.
		/// </summary>
		Binary
#endif
	}

	/// <summary>
	/// The base class for all importers/exporters in C1Schedule
	/// </summary>
	internal abstract class C1ScheduleExchanger
	{
		#region fields
		protected C1ScheduleStorage _storage;

		protected List<Category> _categories;
		protected List<Label> _labels;
		protected List<Resource> _resources;
		protected List<Status> _statuses;
		protected List<Contact> _contacts;
        protected List<Contact> _owners;
        protected IList<Appointment> _appointments;
		#endregion

		#region ctor
		public C1ScheduleExchanger(C1ScheduleStorage storage)
		{
			_storage = storage;

			_categories = new List<Category>();
			_labels = new List<Label>();
			_resources = new List<Resource>();
			_statuses = new List<Status>();
			_contacts = new List<Contact>();
            _owners = new List<Contact>();
        }
		#endregion

		#region interface
		/// <summary>
		/// Exports the scheduler's data to a file. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export the scheduler's data to. </param>
		public void Export(string path)
		{
			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				Export(stream);
			}
		}
		
		/// <summary>
		/// Exports the scheduler's data to a stream. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the scheduler's data will be exported.</param>
		public void Export(Stream stream)
		{
			_appointments = _storage.AppointmentStorage.Appointments;
			Preprocess(true);
			ExportInternal(stream);
            Cleanup();
        }

#if (!SILVERLIGHT)
		/// <summary>
		/// Exports the appointments's data to a file. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export. </param>
		/// <param name="appointments">A <see cref="IList{Appointment}"/> object.</param>
		public void Export(string path, IList<Appointment> appointments)
		{
			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				Export(stream, appointments);
			}
		}
#endif
		
		/// <summary>
		/// Exports the appointments's data to a stream. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the appointments's data will be exported.</param>
		/// <param name="appointments">A <see cref="IList{Appointment}"/> object.</param>
		public void Export(Stream stream, IList<Appointment> appointments)
		{
			_appointments = appointments;
			Preprocess(false);
			ExportInternal(stream);
            Cleanup();
        }
		
#if (!SILVERLIGHT)
		/// <summary>
		/// Imports data into the scheduler from a file.
		/// </summary>
		/// <param name="path">A <see cref="String"/> value containing the full path 
		/// (including the file name and extension) to a file which contains 
		/// the data to be imported into the scheduler.</param>
		public void Import(string path)
		{
			using (FileStream stream = new FileStream(path, FileMode.Open))
			{
				Import(stream);
			}
		}
#endif

		/// <summary>
		/// Imports the scheduler's data from a stream.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		public void Import(Stream stream)
		{
            Cleanup();
            _appointments = new List<Appointment>();
			ImportInternal(stream);
			UpdateStorages();
		}
		#endregion

		#region protected
		protected abstract void ExportInternal(Stream stream);
		protected abstract void ImportInternal(Stream stream);
		protected virtual void Preprocess(bool full)
		{
			_categories.Clear();
			_labels.Clear();
			_resources.Clear();
			_statuses.Clear();
			_contacts.Clear();
            _owners.Clear();
            if (full)
			{
				_categories.AddRange(_storage.CategoryStorage.Categories);
				_labels.AddRange(_storage.LabelStorage.Labels);
				_resources.AddRange(_storage.ResourceStorage.Resources);
				_statuses.AddRange(_storage.StatusStorage.Statuses);
				_contacts.AddRange(_storage.ContactStorage.Contacts);
                _owners.AddRange(_storage.OwnerStorage.Contacts);
            }
			else
			{
				foreach (Appointment app in _appointments)
				{
					foreach (Category cat in app.Categories)
					{
						if (!_categories.Contains(cat))
						{
							_categories.Add(cat);
						}
					}
					if (app.Label != null && !_labels.Contains(app.Label))
					{
						_labels.Add(app.Label);
					}
					foreach (Resource res in app.Resources)
					{
						if (!_resources.Contains(res))
						{
							_resources.Add(res);
						}
					}
					if (app.BusyStatus != null && !_statuses.Contains(app.BusyStatus))
					{
						_statuses.Add(app.BusyStatus);
					}
					foreach (Contact cont in app.Links)
					{
						if (!_contacts.Contains(cont))
						{
							_contacts.Add(cont);
						}
					}
                    if (app.Owner != null && !_owners.Contains(app.Owner))
                    {
                        _owners.Add(app.Owner);
                    }
                }
			}
		}
		#endregion

		#region private stuff
        private void Cleanup()
        {
            _categories.Clear();
            _labels.Clear();
            _resources.Clear();
            _statuses.Clear();
            _contacts.Clear();
            _owners.Clear();
            _appointments = null;
        }
		private void UpdateStorages()
		{
			// merge loaded labels, resources, statuses, contacts, owners and categories
			MergeStorage<Label, BaseObjectMappingCollection<Label>>(_storage.LabelStorage, _labels);
			MergeStorage<Resource, BaseObjectMappingCollection<Resource>>(_storage.ResourceStorage, _resources);
			MergeStorage<Status, BaseObjectMappingCollection<Status>>(_storage.StatusStorage, _statuses);
			MergeStorage<Contact, BaseObjectMappingCollection<Contact>>(_storage.ContactStorage, _contacts);
            MergeStorage<Contact, BaseObjectMappingCollection<Contact>>(_storage.OwnerStorage, _owners);
            MergeStorage<Category, BaseObjectMappingCollection<Category>>(_storage.CategoryStorage, _categories);

			// merge loaded appointments
			foreach (Appointment appointment in _appointments)
			{
				// find if the object already exists in the collection
				Appointment app = null;
				if (!appointment.Id.Equals(Guid.Empty)
					&& _storage.AppointmentStorage.Appointments.Contains(appointment.Id))
				{
					app = _storage.AppointmentStorage.Appointments[appointment.Id];
				}
				else if (_storage.AppointmentStorage.Mappings.IndexMapping.IsMapped 
					&& appointment.Index != -1
					&& _storage.AppointmentStorage.Appointments.Contains(appointment.Index))
				{
					app = _storage.AppointmentStorage.Appointments.FindByIndex(appointment.Index);
				}
				if (app != null)
				{
					appointment.ParentCollection = app.ParentCollection;
					app.CopyFrom(appointment, false);
					// TODO: add sequence number or check last modified date
				}
				else
				{
					if (appointment.Id.Equals(Guid.Empty)
					   && _storage.AppointmentStorage.Mappings.IdMapping.IsMapped)
					{
						appointment.Id = Guid.NewGuid();
					}
					_storage.AppointmentStorage.Appointments.Add(appointment);
				}
			}

			_storage.OnAppointmentsLoaded();

            Cleanup();
		}

		private static void MergeStorage<T, TMappingCollection>(BaseStorage<T, TMappingCollection> storage, List<T> list) 
			where T : BaseObject, new()
			where TMappingCollection : MappingCollectionBase<T>, new()
		{
			foreach (T obj in list)
			{
				// find if the object already exists in the collection
				T o = null;
				if (!obj.Id.Equals(Guid.Empty)
					&& storage.Objects.Contains(obj.Id))
				{
					o = storage.Objects[obj.Id];
				}
				else if (storage.Mappings.IndexMapping.IsMapped 
					&& obj.Index != -1 
					&& storage.Objects.Contains(obj.Index))
				{
					o = storage.Objects.FindByIndex(obj.Index);
				}
				else
				{
					// if there is no id and index,
					// try to find the object with the same text
					foreach (T l in storage.Objects)
					{
						if (l.Text.ToUpper().Equals(obj.Text.ToUpper()))
						{
							o = l;
							break;
						}
					}
				}
				if (o != null)
				{
					o.BeginEdit();
					o.Color = obj.Color;
					o.Brush = obj.Brush;
					o.MenuCaption = obj.MenuCaption;
					o.Text = obj.Text;
					o.EndEdit();
				}
				else
				{
					if (obj.Id.Equals(Guid.Empty)
						&& storage.Mappings.IdMapping.IsMapped)
					{
						obj.Id = Guid.NewGuid();
					}
					storage.Objects.Add(obj);
				}
			}
		}
		#endregion
	}
}
