﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

namespace C1.C1Schedule
{
	/// <summary>
	/// 
	/// </summary>
	internal class XmlExchanger : C1ScheduleExchanger
	{
		#region static
		// reader and writer settings can be safely shared between many XmlReaders and Writers
		internal static XmlWriterSettings _writerSettings = new XmlWriterSettings();
		internal static XmlReaderSettings _readerSettings = new XmlReaderSettings();

		// Simplified version of XmlReader.ReadString(). 
		// Silverlight doesn't have XmlReader.ReadString() method.
		public static string ReadStringValue(XmlReader reader)
		{
			if (reader.ReadState != ReadState.Interactive)
			{
				return string.Empty;
			}
			if (reader.NodeType == XmlNodeType.Element)
			{
				if (reader.IsEmptyElement || !reader.Read() || reader.NodeType == XmlNodeType.EndElement)
				{
					return string.Empty;
				}
			}
			string str = string.Empty;
			if (reader.NodeType == XmlNodeType.Text)
			{
				str = reader.Value;
				reader.Read();
			}
			return str;
		}

		static XmlExchanger()
		{
			_writerSettings.OmitXmlDeclaration = true;
#if !SILVERLIGHT 
#if (CLR40)
            _readerSettings.DtdProcessing = DtdProcessing.Prohibit;
#else
			_readerSettings.ProhibitDtd = true;
#endif
#endif
			_readerSettings.IgnoreWhitespace = true;
			_readerSettings.IgnoreComments = true;
			_readerSettings.IgnoreProcessingInstructions = true;
		}
		#endregion

		#region ctor
		public XmlExchanger(C1ScheduleStorage storage)
			: base(storage)
		{
		}
		#endregion

		#region overrides
		/// <summary>
		/// Exports the appointments's data to a stream in the XML format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the appointments's data will be exported.</param>
		protected override void ExportInternal(Stream stream)
		{
			using (XmlWriter writer = XmlWriter.Create(stream, _writerSettings))
			{
				WriteHeader(writer);
				if (_appointments.Count > 0)
				{
					writer.WriteStartElement("Appointments");
					foreach (Appointment app in _appointments)
					{
						app.ToXml(writer);
					}
					writer.WriteEndElement();
				}
				WriteFooter(writer);
			}
		}

		/// <summary>
		/// Imports the scheduler's data from a stream whose data is in the XML format.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		protected override void ImportInternal(Stream stream)
		{
			using (XmlReader reader = XmlReader.Create(stream, _readerSettings))
			{
				if (!reader.Read() || reader.Name != "C1Schedule")
				{
#if END_USER_LOCALIZATION
					throw new ApplicationException(C1.Win.C1Schedule.Localization.Strings.C1Schedule.Exceptions.Item("XmlImportError", _storage.Info.CultureInfo));
#elif WINFX
					throw new ApplicationException(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "XmlImportError", "Cannot find C1Schedule node.", _storage.Info.CultureInfo));
#elif SILVERLIGHT
					throw new Exception(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.XmlImportError);
#else
					throw new ApplicationException(C1Localizer.GetString("Cannot find C1Schedule node."));
#endif
				}

				while (reader.Read() && reader.NodeType == XmlNodeType.Element)
				{
					using (XmlReader reader1 = reader.ReadSubtree())
					{
						switch (reader.Name)
						{
							case "Categories":
								ReadCollection<Category>(reader1, _categories);
								break;
							case "Labels":
								ReadCollection<Label>(reader1, _labels);
								break;
							case "Resources":
								ReadCollection<Resource>(reader1, _resources);
								break;
							case "Statuses":
								ReadCollection<Status>(reader1, _statuses);
								break;
							case "Contacts":
								ReadCollection<Contact>(reader1, _contacts);
								break;
                            case "Owners":
                                ReadCollection<Contact>(reader1, _owners);
                                break;
                            case "Appointments":
								ReadAppointmentCollection(reader1);
								break;
						}
					}
				}
			}
		}

		private void ReadCollection<T>(XmlReader reader, List<T> list) where T : BaseObject, new()
		{
			reader.Read();
			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				using (XmlReader reader1 = reader.ReadSubtree())
				{
					T baseObject = new T();
					baseObject.FromXml(reader1);
					list.Add(baseObject);
				}
			}
		}

		private void ReadAppointmentCollection(XmlReader reader)
		{
			reader.Read();
			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				using (XmlReader reader1 = reader.ReadSubtree())
				{
					Appointment app = new Appointment();
					app.ParentCollection = _storage.AppointmentStorage.Appointments;
					app.FromXml(reader1);
					_appointments.Add(app);
				}
			}
		}
		#endregion

		#region private stuff
		private void WriteHeader(XmlWriter writer)
		{
			writer.WriteStartElement("C1Schedule");
			writer.WriteAttributeString("version", Assembly.GetExecutingAssembly().FullName);

			if ( _categories.Count > 0 )
			{
				writer.WriteStartElement("Categories");
				foreach (Category cat in _categories)
				{
					cat.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if ( _labels.Count > 0 )
			{
				writer.WriteStartElement("Labels");
				foreach (Label lab in _labels)
				{
					lab.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if ( _resources.Count > 0 )
			{
				writer.WriteStartElement("Resources");
				foreach (Resource res in _resources)
				{
					res.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if ( _statuses.Count > 0 )
			{
				writer.WriteStartElement("Statuses");
				foreach (Status st in _statuses)
				{
					st.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if ( _contacts.Count > 0 )
			{
				writer.WriteStartElement("Contacts");
				foreach (Contact con in _contacts)
				{
					con.ToXml(writer);
				}
				writer.WriteEndElement();
			}
            if (_owners.Count > 0)
            {
                writer.WriteStartElement("Owners");
                foreach (Contact con in _owners)
                {
                    con.ToXml(writer);
                }
                writer.WriteEndElement();
            }
        }

		private static void WriteFooter(XmlWriter writer)
		{
			writer.WriteEndElement();
			writer.Flush();
		}
		#endregion
	}
}



