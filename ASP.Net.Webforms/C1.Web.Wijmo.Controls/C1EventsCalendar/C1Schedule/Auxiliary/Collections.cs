using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml;

// TODO: add comments

namespace C1.C1Schedule   //alex
{
#if (!WINFX && !SILVERLIGHT)
    #region WinFX INotifyCollectionChanged stuff for FW 2.0
    /// <summary>
	/// Describes the action that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event. 
    /// </summary>
	public enum NotifyCollectionChangedAction
    {
		/// <summary>
		/// One or more items were added to the collection. 
		/// </summary>
        Add,
		/// <summary>
		/// One or more items were removed from the collection. 
		/// </summary>
        Remove,
		/// <summary>
		/// One or more items were replaced in the collection.
		/// </summary>
        Replace,
		/// <summary>
		/// One or more items were moved within the collection.
		/// </summary>
        Move,
		/// <summary>
		/// The content of the collection changed dramatically.
		/// </summary>
        Reset
    }

	/// <summary>
	/// Provides data for the <see cref="INotifyCollectionChanged.CollectionChanged"/> event.
	/// </summary>
    public class NotifyCollectionChangedEventArgs : EventArgs
    {
        private NotifyCollectionChangedAction _action;
        private IList _newItems;
        private int _newStartingIndex;
        private IList _oldItems;
        private int _oldStartingIndex;

		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action)
        {
            Initialize(action, null, null, -1, -1);
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="changedItems">The <see cref="IList"/> list containing
		/// changed items.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            IList changedItems): this(action, changedItems, -1)
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="changedItem">The changed item.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            object changedItem): this(action, ConvertToReadOnlyList(changedItem))
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="newItems">The <see cref="IList"/> list containing added items.</param>
		/// <param name="oldItems">The <see cref="IList"/> list containing removed items.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            IList newItems, IList oldItems): this(action, newItems, oldItems, -1)
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="changedItems">The <see cref="IList"/> list containing
		/// changed items.</param>
		/// <param name="startingIndex">The zero-based starting index.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            IList changedItems, int startingIndex)
        {
            IList oldItems = changedItems;
            IList newItems = changedItems;
            int oldIndex = startingIndex;
            int newIndex = startingIndex;
            if (action == NotifyCollectionChangedAction.Add)
            {
                oldItems = null;
                oldIndex = -1;
            }
            else if (action == NotifyCollectionChangedAction.Remove)
            {
                newItems = null;
                newIndex = -1;
            }
            Initialize(action, newItems, oldItems, -1, -1);
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="changedItem">The changed item.</param>
		/// <param name="index">The zero-based index of the changed item.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            object changedItem, int index)
            :
            this(action, ConvertToReadOnlyList(changedItem), index)
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="newItem">The new item.</param>
		/// <param name="oldItem">The old item.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            object newItem, object oldItem)
            :
            this(action, ConvertToReadOnlyList(newItem), ConvertToReadOnlyList(oldItem))
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="newItems">The <see cref="IList"/> list containing added items.</param>
		/// <param name="oldItems">The <see cref="IList"/> list containing removed items.</param>
		/// <param name="startingIndex">The zero-based starting index.</param>
		public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            IList newItems, IList oldItems, int startingIndex)
        {
            Initialize(action, newItems, oldItems, startingIndex, startingIndex);
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="changedItems">The <see cref="IList"/> list containing
		/// changed items.</param>
		/// <param name="index">The new index.</param>
		/// <param name="oldIndex">The old index.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            IList changedItems, int index, int oldIndex)
        {
            Initialize(action, changedItems, changedItems, index, oldIndex);
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="changedItem">The changed item.</param>
		/// <param name="index">The new index.</param>
		/// <param name="oldIndex">The old index.</param>
		public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            object changedItem, int index, int oldIndex):
            this(action, ConvertToReadOnlyList(changedItem), index, oldIndex)
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
		/// </summary>
		/// <param name="action">The <see cref="NotifyCollectionChangedAction"/> action 
		/// that caused a <see cref="INotifyCollectionChanged.CollectionChanged"/> event.</param>
		/// <param name="newItem">The new item.</param>
		/// <param name="oldItem">The old item.</param>
		/// <param name="index">The zero-based index of the item.</param>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action,
            object newItem, object oldItem, int index)
            :
            this(action, ConvertToReadOnlyList(newItem), ConvertToReadOnlyList(oldItem), index)
        {
        }

        private void Initialize(NotifyCollectionChangedAction action,
            object newItems, object oldItems, int newIndex, int oldIndex)
        {
            _action = action;
            _newItems = ConvertToReadOnlyList(newItems);
            _oldItems = ConvertToReadOnlyList(oldItems);
            _newStartingIndex = newIndex;
            _oldStartingIndex = oldIndex;
            switch (action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        // note: newIndex < 0 is allowable
                        if (_newItems == null || _oldItems != null || _oldStartingIndex >= 0)
                            throw new ArgumentException("Add");
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    {
                        if (_oldItems == null || _newItems != null || _newStartingIndex >= 0)
                            throw new ArgumentException("Remove");
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    {
                        if (_oldItems == null || _newItems == null || _newStartingIndex < 0
                            || _oldStartingIndex < 0)
                        {
                            throw new ArgumentException("Move");
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    {
                        if (_oldItems == null || _newItems == null)
                            throw new ArgumentException("Replace");
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    {
                        if (_newItems != null || _oldItems != null || newIndex >= 0 ||
                            oldIndex >= 0)
                        {
                            throw new ArgumentException("Reset");
                        }
                    }
                    break;
                default:
                    throw new NotSupportedException(action.ToString());

            }
        }

		/// <summary>
		/// Gets the action that caused the event. 
		/// </summary>
        public NotifyCollectionChangedAction Action 
        {
            get { return _action; }
        }
		/// <summary>
		/// Gets the list of new items involved in the change.
		/// </summary>
        public IList NewItems 
        {
            get { return _newItems; }
        }
		/// <summary>
		/// Gets the index at which the change occurred.
		/// </summary>
        public int NewStartingIndex 
        {
            get { return _newStartingIndex; } 
        }
		/// <summary>
		/// Gets the list of items affected by a Replace, Remove, or Move action.
		/// </summary>
        public IList OldItems 
        {
            get { return _oldItems; }
        }
		/// <summary>
		/// Gets the index at which a Move, Remove, ore Replace action occurred.
		/// </summary>
        public int OldStartingIndex 
        {
            get { return _oldStartingIndex; }
        }

        private static IList ConvertToReadOnlyList(object obj)
        {
            IList ret;
            if (obj == null)
                return null;
            else
            {
                ret = obj as IList;
                if (ret != null)
                {
                    if (!ret.IsReadOnly)
                        ret = ArrayList.ReadOnly(ret);
                }
                else
                {
                    ret = ArrayList.ReadOnly(new object[]{obj});
                }
            }
            return ret;
        }

    }

	/// <summary>
	/// The delegate type for the event handlers of the <see cref="INotifyCollectionChanged.CollectionChanged"/> event. 
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">The <see cref="C1.C1Schedule.NotifyCollectionChangedEventArgs"/> 
	/// that contains the event data.</param>
    public delegate void NotifyCollectionChangedEventHandler(object sender, 
        NotifyCollectionChangedEventArgs e);

    /// <summary>
	/// Notifies listeners of dynamic changes, such as when items get added 
	/// and removed or the whole list is refreshed.
    /// </summary>
	public interface INotifyCollectionChanged
    {
		/// <summary>
		/// Occurs when the collection changes.
		/// </summary>
        event NotifyCollectionChangedEventHandler CollectionChanged;
    }
    #endregion
#endif

    /// <summary>
	/// The <see cref="C1ObservableCollection{T}"/> is the base class for all 
	/// <see cref="Collection{T}"/> derived collections. Supports notification.
    /// </summary>
    /// <typeparam name="T">The type of objects to keep in collection.</typeparam>
	public class C1ObservableCollection<T> : Collection<T>, INotifyCollectionChanged
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ObservableCollection{T}"/> class.
		/// </summary>
        public C1ObservableCollection(): base()
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ObservableCollection{T}"/> class.
		/// </summary>
		/// <param name="list">The <see cref="IList{T}"/> list that is wrapped 
		/// by the new collection.</param>
        public C1ObservableCollection(IList<T> list): base(list)
        {
        }

        private event NotifyCollectionChangedEventHandler CollectionChanged;
        event NotifyCollectionChangedEventHandler
            INotifyCollectionChanged.CollectionChanged
        {
            add { this.CollectionChanged += value; }
            remove { this.CollectionChanged -= value; }
        }
		/// <summary>
		/// 
		/// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Reset));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Add, item, index));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
        protected override void RemoveItem(int index)
        {
            T item = this[index];
            base.RemoveItem(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Remove, item, index));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
        protected override void SetItem(int index, T item)
        {
            T oldItem = this[index];
            base.SetItem(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Replace, item, oldItem, index));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
                this.CollectionChanged(this, e);
        }
    }

	/// <summary>
	/// The <see cref="C1ObservableCollection{T}"/> is the base class for all 
	/// <see cref="KeyedCollection{TKey, TItem}"/> derived collections. Supports notification.
	/// </summary>
	/// <typeparam name="TKey">The type of keys in the collection.</typeparam>
	/// <typeparam name="TItem">The type of objects to keep in the collection.</typeparam>
    public abstract class C1ObservableKeyedCollection<TKey, TItem> : 
        KeyedCollection<TKey, TItem>, INotifyCollectionChanged
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ObservableKeyedCollection{TKey, TItem}"/> class.
		/// </summary>
        protected C1ObservableKeyedCollection()
            : this(null, 0)
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ObservableKeyedCollection{TKey, TItem}"/> class.
		/// </summary>
		/// <param name="comparer">The implementation of the <see cref="IEqualityComparer"/> generic interface 
		/// to use when comparing keys, or a null reference (Nothing in Visual Basic) 
		/// to use the default equality comparer for the type of the key, obtained 
		/// from EqualityComparer.Default.</param>
        protected C1ObservableKeyedCollection(IEqualityComparer<TKey> comparer)
            : this(comparer, 0)
        {
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="comparer"></param>
		/// <param name="dictionaryCreationThreshold"></param>
        protected C1ObservableKeyedCollection(IEqualityComparer<TKey> comparer,
            int dictionaryCreationThreshold): base(comparer, dictionaryCreationThreshold)
        {
        }

        private event NotifyCollectionChangedEventHandler CollectionChanged;
        event NotifyCollectionChangedEventHandler
            INotifyCollectionChanged.CollectionChanged
        {
            add { this.CollectionChanged += value; }
            remove { this.CollectionChanged -= value; }
        }

		/// <summary>
		/// 
		/// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Reset));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
        protected override void InsertItem(int index, TItem item)
        {
            base.InsertItem(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Add, item, index));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
        protected override void RemoveItem(int index)
        {
            TItem item = this[index];
            base.RemoveItem(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Remove, item, index));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
        protected override void SetItem(int index, TItem item)
        {
            TItem oldItem = this[index];
            base.SetItem(index, item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(
                NotifyCollectionChangedAction.Replace, item, oldItem, index));
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
                this.CollectionChanged(this, e);
        }
    }
}