using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
#if (WINFX)
using System.Collections.Specialized;
#else
using System.Windows.Forms.Design;
#endif
#if (ASP_NET_2)
using ASP = System.Web;
using ASP_UI = System.Web.UI;
#endif

#if (GRID)
namespace C1.WPF.C1DataGrid
#else
namespace C1.C1Schedule
#endif
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="C1BindingSource"/> encapsulates the data source. 
	/// </summary>
	[ToolboxItem(false)]
    [ComplexBindingProperties("DataSource", "DataMember")]
    //[TypeConverter(typeof(ExpandableObjectConverter))]
    public class C1BindingSource : Component, IBindingListView, IBindingList, IList,
        ICollection, IEnumerable, ITypedList, /*ICancelAddNew, */ 
        ISupportInitializeNotification, ISupportInitialize
	{
		#region fields
		private IList _listInternal;
        private IBindingList _bindingListInternal;
        private IBindingListView _bindingListViewInternal;
        private INotifyCollectionChanged _sourceCollectionChanged;  
        private string _dataMember = "";
        private object _dataSource;
        private string _filter;
        private bool _raiseListChangedEvents = true;
        private string _sort;
        private ConstructorInfo _itemCtor;
        private bool _allowNewExplicit = true;
        private bool _initializing;
        private bool _sourceLoadDeferred;
        private IList _resolvedDeferredList;
        //private int _addingNewItemIdx = -1;
        private string[] _dataKeyNamesInternal = new string[0];
        private bool _suppressOnListChanged;
        private object _addingNew;
        private bool _degradedSource = true;

#if (ASP_NET_2)
        [Flags()]
        private enum PropertyKindEnum
        {
            PrimaryKeyWritable  = 1 << 0,
            PrimaryKeyReadOnly  = 1 << 1,
            DataReadOnly            = 1 << 2,
            DataWritable            = 1 << 3,
            PrimaryKey          = PrimaryKeyWritable | PrimaryKeyReadOnly,
            Any                 = PrimaryKey | DataReadOnly | DataWritable,
            AnyData             = DataReadOnly | DataWritable,
            AnyWritable         = PrimaryKeyWritable | DataWritable,
            AnyReadonly         = PrimaryKeyReadOnly | DataReadOnly
        }
        private string _dataSourceID = "";
        private IEnumerable _aspSelectResult = null;
        private ASP_UI.Control _aspOwnerControl = null;
        private bool _ASPIsPagePreLoaded = false;
        private ASP_UI.DataSourceView _ASPDataView = null;
        private readonly Dictionary<object, Dictionary<string, object>> _oldValuesCache =
            new Dictionary<object, Dictionary<string, object>>();
#endif
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1BindingSource"/> class. 
		/// </summary>
		public C1BindingSource(): this(null, "")
        {
        }
		/// <summary>
		/// Initializes a new instance of the <see cref="C1BindingSource"/> class
		/// with the specified data source and data member values.
		/// </summary>
		/// <param name="dataSource">The <see cref="System.Object"/> object to use as data source.</param>
		/// <param name="dataMember">The data member name.</param>
        public C1BindingSource(object dataSource, string dataMember)
        {
            _dataSource = dataSource;
            _dataMember = dataMember;
#if (ASP_NET_2)
            _ASPIsPagePreLoaded = true;
#endif
            try
            {
                _suppressOnListChanged = true;
                try
                {
                    ReDetermineSourceRequest();
                }
                finally
                {
                    _suppressOnListChanged = false;
                }
            }
            finally
            {
#if (ASP_NET_2)
                _ASPIsPagePreLoaded = false;
#endif
            }
		}
		#endregion

		#region Object model // Properties
		/// <summary>
		/// Gets a value indicating whether items in the underlying list can be edited.
		/// </summary>
        [Browsable(false)]
        public virtual bool AllowEdit
        {
            get
            {
#if (ASP_NET_2)
                if (ASPDataView != null && !ASPDataView.CanUpdate)
                    return false;
#endif
                if (_bindingListInternal != null)
                    return _bindingListInternal.AllowEdit;
                else
                    return !_listInternal.IsReadOnly;
            }
        }
		/// <summary>
		/// Gets or sets a value indicating whether the <see cref="AddNew"/> method 
		/// can be used to add items to the list. 
		/// </summary>
		[C1Description("C1BindingSource.AllowNew", "Indicates whether the AddNew method can be used to add items to the list.")]
		[Browsable(false)]
		public virtual bool AllowNew
        {
            get
            {
                if (!(_allowNewExplicit && AllowAddItem))
                    return false;
                if (AddingNew != null)
                    return true;
                if (_degradedSource && (_itemCtor == null ||
                    _itemCtor.DeclaringType == typeof(object)))
                    return false;
                if (_bindingListInternal != null && _bindingListInternal.AllowNew)
                {
                    return true;
                }
                return _itemCtor != null;
            }
        }

		/// <summary>
		/// Gets a value indicating whether items can be removed from the underlying list. 
		/// </summary>
        [Browsable(false)]
        public virtual bool AllowRemove
        {
            get
            {
#if (ASP_NET_2)
                if (ASPDataView != null && !ASPDataView.CanDelete)
                    return false;
#endif
                if (_bindingListInternal != null)
                    return _bindingListInternal.AllowEdit;
                else
                    return !(_listInternal.IsReadOnly || _listInternal.IsFixedSize);
            }
        }

		/// <summary>
		/// Gets the total number of items in the underlying list.
		/// </summary>
        [Browsable(false)]
        public virtual int Count
        {
            get { return _listInternal.Count; }
        }

		/// <summary>
		/// Gets or sets the specific list in the data source to which 
		/// the connector currently binds to.
		/// </summary>
        [DefaultValue("")]
        [RefreshProperties(RefreshProperties.Repaint)]
#if (!WINFX)
        [Editor(typeof(C1DataMemberEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
		[C1Description("C1BindingSource.DataMember", "The specific list in the data source to which the connector currently binds to.")]
		public string DataMember
        {
            get { return _dataMember; }
            set
            {
                string newVal = value == null ? "" : value.Trim();
				if (_dataMember != newVal)
                {
					_dataMember = newVal;
                    ReDetermineSourceRequest();
                    OnDataMemberChanged(EventArgs.Empty);
                }
            }
        }

		/// <summary>
		/// Gets or sets the data source that the connector binds to. 
		/// </summary>
        [AttributeProvider(typeof(IListSource))]
        [RefreshProperties(RefreshProperties.Repaint)]
        [DefaultValue(null)]
		[C1Description("C1BindingSource.DataSource", "The data source that the connector binds to.")]
		public object DataSource
        {
            get { return _dataSource; }
            set
            {
                if (_dataSource != value)
                {
                    _dataSource = value;
                    if (IsInitialized)
                    {
                        object obj = GetResolvedSource(_dataSource, null);
                        if (GetItemProperties(obj, null, -1).Find(_dataMember, true) == null)
                            _dataMember = "";
                    }
                    ReDetermineSourceRequest();
                    OnDataSourceChanged(EventArgs.Empty);
                }
            }
        }

		/// <summary>
		/// Gets or sets the expression used to filter which rows are viewed. 
		/// </summary>
		[Browsable(false)]
        private string Filter
        {
            get { return _filter; }
            set
            {
                if (_filter != value)
                {
                    _filter = value;
                    ApplyFilter();
                }
            }
        }
		[C1Description("C1BindingSource.Filter", "The expression used to filter which rows are viewed.")]
		string IBindingListView.Filter
        {
            get { return this.Filter; }
            set { this.Filter = value; }
        }

        //public bool IsBindingSuspended { get; } //???

		/// <summary>
		/// Gets a value indicating whether the underlying list has a fixed size. 
		/// </summary>
        [Browsable(false)]
        public virtual bool IsFixedSize
        {
            get { return _listInternal.IsFixedSize; }
        }
		/// <summary>
		/// Gets a value indicating whether the underlying list is read-only.
		/// </summary>
        [Browsable(false)]
        public virtual bool IsReadOnly
        {
            get { return _listInternal.IsReadOnly; }
        }
		/// <summary>
		/// Gets a value indicating whether the items in the underlying list are sorted.
		/// </summary>
        [Browsable(false)]
        bool IBindingList.IsSorted
        {
            get
            {
                if (_bindingListInternal != null)
                    return _bindingListInternal.IsSorted;
                else
                    return false;
            }
        }
		/// <summary>
		/// Gets a value indicating whether access to the collection is synchronized (thread safe). 
		/// </summary>
        [Browsable(false)]
        public virtual bool IsSynchronized
        {
            get { return _listInternal.IsSynchronized; }
        }
		/// <summary>
		/// Gets or sets the list element at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to get or set.</param>
        [Browsable(false)]
        public virtual object this[int index]
        {
            get { return _listInternal[index]; }
            set 
            {
                _listInternal[index] = value;
                if (!SourceRaiseNotifications)
                    OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, index));
            }
        }
		/// <summary>
		/// Gets the list that the connector is bound to. 
		/// </summary>
        [Browsable(false)]
        public IList List
        {
            get { return _listInternal; }
        }
		/// <summary>
		/// Gets or sets a value indicating whether <see cref="ListChanged"/> events should be raised. 
		/// </summary>
        [DefaultValue(true)]
        internal bool RaiseListChangedEvents
        {
            get { return _raiseListChangedEvents; }
            set { _raiseListChangedEvents = value; }
        }
		/// <summary>
		/// Gets or sets the column names used for sorting, and the sort order 
		/// for viewing the rows in the data source.
		/// </summary>
		[C1Description("C1BindingSource.Sort", "Column names used for sorting, and the sort order for viewing the rows in the data source.")]
		[Browsable(false)]
		private string Sort
        {
            get { return _sort; }
            set
            {
                if (_sort != value)
                {
                    _sort = value;
                    ApplySort();
                }
            }
        }
		/// <summary>
		/// Gets the collection of sort descriptions applied to the data source. 
		/// </summary>
        [Browsable(false)]
        ListSortDescriptionCollection IBindingListView.SortDescriptions
        {
            get
            {
                if (_bindingListViewInternal != null)
                    return _bindingListViewInternal.SortDescriptions;
                else
                    return null;
            }
        }
		/// <summary>
		/// Gets the direction the items in the list are sorted.
		/// </summary>
        [Browsable(false)]
        ListSortDirection IBindingList.SortDirection
        {
            get
            {
                if (_bindingListInternal != null)
                    return _bindingListInternal.SortDirection;
                else
                    return ListSortDirection.Ascending;
            }
        }
		/// <summary>
		/// Gets the <see cref="PropertyDescriptor"/> that is being used for sorting the list. 
		/// </summary>
        [Browsable(false)]
        PropertyDescriptor IBindingList.SortProperty
        {
            get
            {
                if (_bindingListInternal != null)
                    return _bindingListInternal.SortProperty;
                else
                    return null;
            }
        }
		/// <summary>
		/// Gets a value indicating whether the data source supports multi-column sorting. 
		/// </summary>
        [Browsable(false)]
        private bool SupportsAdvancedSorting
        {
            get
            {
                if (_bindingListViewInternal != null)
                    return _bindingListViewInternal.SupportsAdvancedSorting;
                else
                    return false;
            }
        }
        bool IBindingListView.SupportsAdvancedSorting
        {
            get { return this.SupportsAdvancedSorting; }
        }
		/// <summary>
		/// Gets a value indicating whether the data source supports change notification.
		/// </summary>
        [Browsable(false)]
        public virtual bool SupportsChangeNotification
        {
            get { return true; }
        }
		/// <summary>
		/// Gets a value indicating whether the data source supports filtering.
		/// </summary>
        [Browsable(false)]
        private bool SupportsFiltering
        {
            get
            {
                if (_bindingListViewInternal != null)
                    return _bindingListViewInternal.SupportsFiltering;
                else
                    return false;
            }
        }
        bool IBindingListView.SupportsFiltering
        {
            get { return this.SupportsFiltering; }
        }
		/// <summary>
		/// Gets a value indicating whether the data source supports searching with 
		/// the Find method. 
		/// </summary>
        [Browsable(false)]
        public virtual bool SupportsSearching
        {
            get
            {
                if (_bindingListInternal != null)
                    return _bindingListInternal.SupportsSearching;
                else
                    return false;
            }
        }
		/// <summary>
		/// Gets a value indicating whether the data source supports sorting.
		/// </summary>
        [Browsable(false)]
        private bool SupportsSorting
        {
            get
            {
                if (_bindingListInternal != null)
                    return _bindingListInternal.SupportsSorting;
                else
                    return false;
            }
        }
        bool IBindingList.SupportsSorting
        {
            get { return this.SupportsSorting; }
        }
		/// <summary>
		/// Gets an object that can be used to synchronize access to the underlying list. 
		/// </summary>
        [Browsable(false)]
        public virtual object SyncRoot
        {
            get { return _listInternal.SyncRoot; }
        }

        /*
        protected internal string[] DataKeyNames
        {
            get { return (string[])DataKeyNamesInternal.Clone(); }
        }
         */
		/// <summary>
		/// 
		/// </summary>
        protected internal virtual string[] DataKeyNamesInternal
        {
            get { return _dataKeyNamesInternal; }
            set
            {
				_dataKeyNamesInternal = value ?? new string[0];
            }
		}
		#endregion

		#region Events // Events
		/// <summary>
		/// Occurs before an item is added to the underlying list. 
		/// </summary>
		public event AddingNewEventHandler AddingNew;
        //public event BindingManagerDataErrorEventHandler DataError;
		/// <summary>
		/// Occurs when the <see cref="DataMember"/> property value has changed.
		/// </summary>
        public event EventHandler DataMemberChanged;
		/// <summary>
		/// Occurs when the <see cref="DataSource"/> property value has changed. 
		/// </summary>
        public event EventHandler DataSourceChanged;
		/// <summary>
		/// Occurs when the underlying list changes or an item in the list changes.
		/// </summary>
        public event ListChangedEventHandler ListChanged;
		#endregion

		#region Interface // Methods
		/// <summary>
		/// Adds an existing item to the internal list.
		/// </summary>
		/// <param name="value">The <see cref="System.Object"/> to add.</param>
		/// <returns>The position into which the new element was inserted.</returns>
		public virtual int Add(object value)
        {
            int index = _listInternal.IndexOf(value);
            if (index < 0)
            {
                index = _listInternal.Count;
                Insert(index, value);
            }
            return index;
        }
		/// <summary>
		/// Adds a new item to the underlying list. 
		/// </summary>
		/// <returns>The item added to the list.</returns>
        public virtual object AddNew()
        {
            if (!AllowAddItem)
                throw new InvalidOperationException("AddNew");
            AddingNewEventArgs e = new AddingNewEventArgs();
            OnAddingNew(e);
            if (e.NewObject != null)
            {
                Add(e.NewObject);
                return e.NewObject;
            }
            else
            {
                if (!AllowNew)
                    throw new InvalidOperationException("AddNew");
                FinishAddNew();
                if (_bindingListInternal != null)
                    _addingNew = _bindingListInternal.AddNew();
                else
                {
                    if (_itemCtor != null)
                    {
                        _addingNew = _itemCtor.Invoke(null);
                        int idx = _listInternal.Add(_addingNew);
                        if (!SourceRaiseNotifications)
                            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded,
                                idx));
                    }
                    else
                        throw new InvalidOperationException("AddNew");
                }
                return _addingNew;
            }
        }
		/// <summary>
		/// Commits a pending new item to the collection. 
		/// </summary>
        public void FinishAddNew()
        {
            if (_addingNew == null)
                return;
            object addingNew = _addingNew;
            _addingNew = null;
            IEditableObject editable = addingNew as IEditableObject;
            try
            {
				if (editable != null)
				{
                    editable.EndEdit();
				}
            }
            catch
            {
                System.Data.DataRowView viewRow = editable as System.Data.DataRowView;
                if (viewRow != null)
                {
                    System.Data.DataTable tb = viewRow.DataView.Table;
                    System.Data.DataRow[] tbRows = new System.Data.DataRow[tb.Rows.Count];
                    tb.Rows.CopyTo(tbRows, 0);
                }
                throw;
            }
#if (ASP_NET_2)
            if (ASPHasSourceID)
                ASPProcessItemAdded(addingNew);
#endif
        }
		/// <summary>
		/// Starts an edit operation on the specified object.
		/// </summary>
		/// <param name="item">The object on which to start operation.</param>
        public void BeginEdit(object item)
        {
            if (item == null)
                return;
            IEditableObject editable = item as IEditableObject;
            if (editable != null)
                editable.BeginEdit();
#if (ASP_NET_2)
            StoreOldValues(item);
#endif
        }
		/// <summary>
		/// Applies pending changes to the underlying data source.
		/// </summary>
		/// <param name="item">The object on which to apply pending changes.</param>
        public void EndEdit(object item)
        {
            if (item == null)
                return;
            IEditableObject editable = item as IEditableObject;
            if (editable != null)
                editable.EndEdit();
#if (ASP_NET_2)
            if (ASPHasSourceID)
                ASPProcessItemUpdated(item);
#endif
        }
		/// <summary>
		/// Sorts the data source with the specified sort description or descriptions.
		/// </summary>
		/// <param name="sorts"></param>
        void IBindingListView.ApplySort(ListSortDescriptionCollection sorts)
        {
            if (!SupportsAdvancedSorting)
                throw new InvalidOperationException("ApplySort");
            if (_bindingListViewInternal != null)
            {
                _sort = GetSortStringFromDescriptions(sorts);
                _bindingListViewInternal.ApplySort(sorts);
            }
        }
		/// <summary>
		/// Sorts the data source using the specified property descriptor and sort direction.
		/// </summary>
		/// <param name="property"></param>
		/// <param name="direction"></param>
        void IBindingList.ApplySort(PropertyDescriptor property,
            ListSortDirection direction)
        {
            if (!SupportsSorting)
                throw new InvalidOperationException("ApplySort");
            if (_bindingListInternal != null)
            {

                _sort = GetSortStringFromDescription(property, direction);
                _bindingListInternal.ApplySort(property, direction);
            }
        }
		/// <summary>
		/// Removes all elements from the list. 
		/// </summary>
        public virtual void Clear()
        {
            _listInternal.Clear();
            if (!SourceRaiseNotifications)
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }
		/// <summary>
		/// Determines whether an object is an item in the list. 
		/// </summary>
		/// <param name="value">The <see cref="System.Object"/> to locate.</param>
		/// <returns>True if the <see cref="System.Object"/> is found in the list; 
		/// otherwise, false.</returns>
        public virtual bool Contains(object value)
        {
			try
			{
                return _listInternal.Contains(value);
			}
			catch (System.NotImplementedException)
			{
				int count = _listInternal.Count;
				for (int i = 0; i < count; i++)
				{
					if (value == _listInternal[i])
					{
						return true;
					}
				}
				return false;
			}
      }
		/// <summary>
		/// Copies the contents of the <see cref="List"/> to the specified array, 
		/// starting at the specified index value. 
		/// </summary>
		/// <param name="array">The one-dimensional <see cref="System.Array"/> that is the 
		/// destination of the elements copied from <see cref="System.Collections.ICollection"/>. 
		/// The <see cref="System.Array"/> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in the array at which the copying begins.</param>
        public virtual void CopyTo(Array array, int index)
        {
            _listInternal.CopyTo(array, index);
        }
		/// <summary>
		/// Releases all resources used by the <see cref="C1BindingSource"/>.
		/// </summary>
		/// <param name="disposing">True if managed resources should be disposed; 
		/// otherwise, false.</param>
        protected override void Dispose(bool disposing) 
        {
            if (disposing)
            {
                DetachFromList();
            }
            //base.di
        }
		/// <summary>
		/// Searches for the index of the item that has the given property descriptor. 
		/// </summary>
		/// <param name="prop">The <see cref="System.ComponentModel.PropertyDescriptor"/> 
		/// to search on.</param>
		/// <param name="key">The value of the property parameter to search for.</param>
		/// <returns>The index of the item that has the given 
		/// <see cref="System.ComponentModel.PropertyDescriptor"/>.</returns>
        public virtual int Find(PropertyDescriptor prop, object key)
        {
            if (!SupportsSearching)
                throw new InvalidOperationException("Find");
            if (_bindingListInternal != null)
                return _bindingListInternal.Find(prop, key);
            else
                return -1;
        }
		/// <summary>
		/// Returns the index of the item in the list with the specified property name and value. 
		/// </summary>
		/// <param name="propertyName">The property name to search on.</param>
		/// <param name="key">The value of the property parameter to search for.</param>
		/// <returns>
        /// The index of the item in the list with the specified property name and value.
        /// </returns>
        public int Find(string propertyName, object key)
        {
            PropertyDescriptor propDesc =
                GetListOrTypeItemProperties(_listInternal).Find(propertyName, true);
            if (propDesc == null)
                throw new InvalidOperationException("Find");
            else
                return Find(propDesc, key);
        }
		/// <summary>
		/// Retrieves an enumerator for the <see cref="List"/>. 
		/// </summary>
		/// <returns>An <see cref="System.Collections.IEnumerable"/> object that can 
		/// be used to iterate through the collection.</returns>
        public virtual IEnumerator GetEnumerator()
        {
            return _listInternal.GetEnumerator();
        }
		/// <summary>
		/// Retrieves an array of PropertyDescriptor objects representing 
		/// the bindable properties of the data source list type.
		/// </summary>
		/// <param name="listAccessors">An array of 
		/// <see cref="System.ComponentModel.PropertyDescriptor"/> objects to find in 
		/// the collection as bindable. This can be null.</param>
		/// <returns>The <see cref="System.ComponentModel.PropertyDescriptorCollection"/> 
		/// that represents the properties on each item used to bind data.</returns>
        public virtual PropertyDescriptorCollection GetItemProperties(
            PropertyDescriptor[] listAccessors)
        {
            return GetItemProperties(/*GetResolvedSource(DataSource, DataMember)*/
                _listInternal, listAccessors, 0);
        }
		/// <summary>
		/// Gets the name of the list supplying data for the binding. 
		/// </summary>
		/// <param name="listAccessors">An array of <see cref="System.ComponentModel.PropertyDescriptor"/> 
		/// objects, the list name for which is returned. This can be null.</param>
		/// <returns>The name of the list.</returns>
        public virtual string GetListName(PropertyDescriptor[] listAccessors)
        {
            object list = GetResolvedSource(ActualDataSource, DataMemberOfActualDataSource);
            if (list == null)
                return "";
            ITypedList typedList = list as ITypedList;
            if (typedList != null)
                return typedList.GetListName(listAccessors);
            if (listAccessors != null && listAccessors.Length > 0)
                list = listAccessors[0].PropertyType;
            return DetermineItemTypeForDataSource(list, null).Name;

        }
		/// <summary>
		/// Searches for the specified object and returns the index of the first 
		/// occurrence within the entire list.
		/// </summary>
		/// <param name="value">The <see cref="System.Object"/> to index.</param>
		/// <returns>The index of <i>value</i> if found in the list; otherwise, -1.</returns>
        public virtual int IndexOf(object value)
        {
			try
			{
				return _listInternal.IndexOf(value);
			}
			catch (System.NotImplementedException)
			{
				int count = _listInternal.Count;
				for (int i = 0; i < count; i++)
				{
					if (value == _listInternal[i])
					{
						return i;
					}
				}
				return -1; // throw; ??
			}
        }
		/// <summary>
		/// Inserts an item into the list at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which 
		/// <i>value</i> should be inserted.</param>
		/// <param name="value">The <see cref="System.Object"/> to insert.</param>
        public virtual void Insert(int index, object value)
        {
            if (!AllowAddItem)
                throw new InvalidOperationException("Add");
            FinishAddNew();
            if (AutoAdjustItemType && _listInternal.Count == 0)
            {
                Type itemType = value != null ? value.GetType() : typeof(object);
                AssignListInternal(CreateBindingList(itemType));
            }
            if (index == _listInternal.Count)
                _listInternal.Add(value); // because not all lists supports Insert, e.g. DataView
            else
                _listInternal.Insert(index, value);
            if (!SourceRaiseNotifications)
                OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index));
            _addingNew = value;
            FinishAddNew();

        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
        protected virtual void OnAddingNew(AddingNewEventArgs e)
        {
            if (AddingNew != null)
                AddingNew(this, e);
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
        protected virtual void OnDataMemberChanged(EventArgs e)
        {
            if (DataMemberChanged != null)
                DataMemberChanged(this, e);
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
        protected virtual void OnDataSourceChanged(EventArgs e)
        {
            if (DataSourceChanged != null)
                DataSourceChanged(this, e);
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
        protected virtual void OnListChanged(ListChangedEventArgs e)
        {
            if (RaiseListChangedEvents && ListChanged != null)
                ListChanged(this, e);
            //test
            /*
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                case ListChangedType.ItemChanged:
                case ListChangedType.ItemDeleted:
                    OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                    break;
            }
             */
        }

        private void AddNewFinished(object item)
        {
#if (ASP_NET_2)
            if (item != null)
                ASPProcessItemAdded(item);
#endif
        }
		/// <summary>
		/// Removes the specified item from the list. 
		/// </summary>
		/// <param name="value">The <see cref="System.Object"/> to remove.</param>
        public virtual void Remove(object value)
        {
            if (!AllowRemove)
                throw new InvalidOperationException("Remove");
#if (ASP_NET_2)
            if (ASPHasSourceID)
            {
                ASPProcessItemDeleted(value);
                return;
            }
#endif
            int idx = -1;
            if (!SourceRaiseNotifications)
                idx = IndexOf(value);
            _listInternal.Remove(value);
            if (idx >= 0)
                OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, idx));
        }
		/// <summary>
		/// Removes the item at the specified index in the list.
		/// </summary>
		/// <param name="index">The zero-based index of the item to remove.</param>
        public virtual void RemoveAt(int index)
        {
            if (!AllowRemove)
                throw new InvalidOperationException("RemoveAt");
#if (ASP_NET_2)
            if (ASPHasSourceID)
            {
                ASPProcessItemDeleted(this[index]);
                return;
            }
#endif
            _listInternal.RemoveAt(index);
            if (!SourceRaiseNotifications)
                OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, index));
        }
		/// <summary>
		/// Removes the filter associated with the <see cref="C1BindingSource"/>.
		/// </summary>
        void IBindingListView.RemoveFilter()
        {
            Filter = null;
        }
		/// <summary>
		/// Removes the sort associated with the <see cref="C1BindingSource"/>. 
		/// </summary>
        void IBindingList.RemoveSort()
        {
            Sort = null;
        }
		/// <summary>
		/// Reinitializes the <see cref="AllowNew"/> property. 
		/// </summary>
        private void ResetAllowNew()
        {
            _allowNewExplicit = true;
        }
		/// <summary>
		/// Causes a control bound to the <see cref="C1BindingSource"/> to reread 
		/// all the items in the list and refresh their displayed values. 
		/// </summary>
		/// <param name="metadataChanged">The <see cref="Boolean"/> value specifying
		/// whether the metadata has been changed.</param>
        public void ResetBindings(bool metadataChanged)
        {
            if (metadataChanged)
                OnListChanged(new ListChangedEventArgs(
                    ListChangedType.PropertyDescriptorChanged, null));
            OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }
		/// <summary>
		/// Causes a control bound to the <see cref="C1BindingSource"/> to re-read 
		/// the item at the specified index, and refresh its displayed value. 
		/// </summary>
		/// <param name="itemIndex">The zero-based index of item to re-read.</param>
        public void ResetItem(int itemIndex)
        {
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, itemIndex));
        }

        //public void ResumeBinding(); //???
		//public void SuspendBinding(); //???
		#endregion


		#region private stuff // private stuff
	/*	private IList ListInternal
        {
            get { return _listInternal; }
        }

        private IBindingList BindingListInternal
        {
            get { return _bindingListInternal; }
        }

        private IBindingListView BindingListViewInternal
        {
            get { return _bindingListViewInternal; }
        }

        private ConstructorInfo ItemCtor
        {
            get { return _itemCtor; }
        }*/

        private bool SourceRaiseNotifications 
        {
            get { return _bindingListInternal != null || _sourceCollectionChanged != null; }
        }

        private bool AutoAdjustItemType
        {
            get { return ActualDataSource == null; }
        }

        private bool AllowAddItem
        {
            get 
            {
#if (ASP_NET_2)
                if (ASPDataView != null && !ASPDataView.CanInsert)
                    return false;
#endif
                return !(IsReadOnly || IsFixedSize); 
            }
        }

        private object ActualDataSource
        {
            get
            {
#if (ASP_NET_2)
                if (ASPHasSourceID)
                    return _aspSelectResult;
                else
                    return DataSource;
#else
                return DataSource;
#endif
            }
        }

        private string DataMemberOfActualDataSource
        {
            get
            {
#if (ASP_NET_2)
                if (ASPHasSourceID)
                    return "";
                else
                    return DataMember;
#else
                return DataMember;
#endif
            }
        }

        private void ReDetermineSourceRequest()
        {
            if (!IsInitialized)
            {
                _sourceLoadDeferred = true;
                return;
            }
#if (ASP_NET_2)
            
            //ASPDetachFromView();
            _ASPDataView = null;
            if (ASPHasSourceID)
            {
                if (ASPIsDesignMode)
                    ASPReDetermineSourceCallback(null);
                else
                {
                    ASP_UI.DataSourceView view = ASPGetDataView(DataSourceID, DataMember);
                    if (view == null)
                        throw new ASP.HttpException("Invalid DataSourceID/DataMember");
                    _ASPDataView = view;

                    view.Select(ASPCreateSelectArguments(view),
                        new ASP_UI.DataSourceViewSelectCallback(ASPReDetermineSourceCallback));
                    
                    //ASPAttachToView();
                }
            }
            else
                ReDetermineSource();
#else
            ReDetermineSource();
#endif
        }

        private void ReDetermineSource()
        {
            ISupportInitializeNotification sourceInitNotif =
                ActualDataSource as ISupportInitializeNotification;
            if (sourceInitNotif != null && !sourceInitNotif.IsInitialized)
            {
                sourceInitNotif.Initialized += new EventHandler(Source_Initialized);
                return;
            }
            bool isDegradedSource = true;
            object resolvedSource = GetResolvedSource(ActualDataSource, 
                DataMemberOfActualDataSource);
            Type itemType = null;
            IList list;
            if (resolvedSource == null)
            {
                itemType = DetermineItemTypeForDataSource(ActualDataSource,
                    DataMemberOfActualDataSource);
                list = CreateBindingList(itemType);
            }
            else if (resolvedSource is IEnumerable)
            {
                isDegradedSource = !(resolvedSource is IBindingList);
                if (isDegradedSource)
                    itemType = DetermineItemTypeForDataSource(resolvedSource, null);
                if (resolvedSource is IList)
                {
                    list = (IList)resolvedSource;
                }
                else
                {
                    list = CreateBindingList(itemType);
                    IEnumerable en = (IEnumerable)resolvedSource;
                    foreach (object item in en)
                        list.Add(item);
                }
            }
            else
            {
                itemType = resolvedSource.GetType();
                list = CreateBindingList(itemType);
            }
            _degradedSource = isDegradedSource;
            AssignListInternal(list);
        }

        private void AssignListInternal(IList list)
        {
            Debug.Assert(list != null);
            if (list == null)
                return;
            ISupportInitializeNotification initNotif =
                list as ISupportInitializeNotification;
            if (initNotif != null && !initNotif.IsInitialized)
            {
                _resolvedDeferredList = list;
                initNotif.Initialized +=
                    new EventHandler(ResolvedSourceInitialized_Initialized);
                return;
            }
            DetachFromList();
            _listInternal = list;
            _bindingListInternal = _listInternal as IBindingList;
            _bindingListViewInternal = _listInternal as IBindingListView;
            _sourceCollectionChanged = _listInternal as INotifyCollectionChanged;   
            Type itemType = DetermineItemTypeForDataSource(list, null);
            _itemCtor = GetDefaultPublicCtor(itemType);
            ApplyFilter();
            ApplySort();
            AttachToList();
            if (!_suppressOnListChanged)
            {
                OnListChanged(new ListChangedEventArgs(
                    ListChangedType.PropertyDescriptorChanged, null));
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            }
        }

        void Source_Initialized(object sender, EventArgs e)
        {
            ISupportInitializeNotification initNotif =
                sender as ISupportInitializeNotification;
            if (initNotif != null)
                initNotif.Initialized -= new EventHandler(Source_Initialized);
            ReDetermineSourceRequest();
        }

        void ResolvedSourceInitialized_Initialized(object sender, EventArgs e)
        {
            ISupportInitializeNotification initNotif =
                sender as ISupportInitializeNotification;
            if (initNotif != null)
            {
                initNotif.Initialized -=
                    new EventHandler(ResolvedSourceInitialized_Initialized);
            }
            if (_resolvedDeferredList != null)
            {
                IList list = _resolvedDeferredList;
                _resolvedDeferredList = null;
                AssignListInternal(list);
            }
        }

        private void AttachToList()
        {
            if (_bindingListInternal != null)
            {
                _bindingListInternal.ListChanged += new ListChangedEventHandler(BindingListInternal_ListChanged);
            }
            else
            {
                if (_sourceCollectionChanged != null)
                {
                    _sourceCollectionChanged.CollectionChanged += new NotifyCollectionChangedEventHandler(_sourceCollectionChanged_CollectionChanged);
                    foreach (INotifyPropertyChanged item in _listInternal)
                    {
                        item.PropertyChanged += new PropertyChangedEventHandler(item_PropertyChanged);
                    }
                }
            }
        }

        private void DetachFromList()
        {
            if (_bindingListInternal != null)
            {
                _bindingListInternal.ListChanged -= new ListChangedEventHandler(BindingListInternal_ListChanged);
            }
            else
            {
                if (_sourceCollectionChanged != null)
                {
                    foreach (INotifyPropertyChanged item in _listInternal)
                    {
                        item.PropertyChanged -= new PropertyChangedEventHandler(item_PropertyChanged);
                    }
                    _sourceCollectionChanged.CollectionChanged -= new NotifyCollectionChangedEventHandler(_sourceCollectionChanged_CollectionChanged);
                }
            }
        }

        void BindingListInternal_ListChanged(object sender, ListChangedEventArgs e)
        {
            OnListChanged(e);
        }
        
        void _sourceCollectionChanged_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ListChangedEventArgs args;
            int index;
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    index = e.NewStartingIndex;
                    foreach (object newItem in e.NewItems)
                    {
                        AttachItem((INotifyPropertyChanged)newItem);
                        args = new ListChangedEventArgs(ListChangedType.ItemAdded, index);
                        OnListChanged(args);
                        index++;
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Replace:
                    index = e.OldStartingIndex;
                    foreach (object oldItem in e.OldItems)
                    {
                        DetachItem((INotifyPropertyChanged)oldItem);
                        args = new ListChangedEventArgs(ListChangedType.ItemDeleted, index);
                        OnListChanged(args);
                        index++;
                    }
                    index = e.NewStartingIndex;
                    foreach (object newItem in e.NewItems)
                    {
                        AttachItem((INotifyPropertyChanged)newItem);
                        args = new ListChangedEventArgs(ListChangedType.ItemAdded, index);
                        OnListChanged(args);
                        index++;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    index = e.OldStartingIndex;
                    foreach (object oldItem in e.OldItems)
                    {
                        DetachItem((INotifyPropertyChanged)oldItem);
                        args = new ListChangedEventArgs(ListChangedType.ItemDeleted, index);
                        OnListChanged(args);
                        index++;
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    args = new ListChangedEventArgs(ListChangedType.Reset, -1);
                    OnListChanged(args);
                    break;
            }
        }

        private void AttachItem(INotifyPropertyChanged item)
        {
            if (item == null)
            {
                return;
            }
            item.PropertyChanged += new PropertyChangedEventHandler(item_PropertyChanged);
        }

        private void DetachItem(INotifyPropertyChanged item)
        {
            if (item == null)
            {
                return;
            }
            item.PropertyChanged -= new PropertyChangedEventHandler(item_PropertyChanged);
        }

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ListChangedEventArgs args = new ListChangedEventArgs(ListChangedType.ItemChanged, _listInternal.IndexOf(sender));
            OnListChanged(args);
        }

        internal static object GetResolvedSource(object dataSource, string dataMember)
        {
            object resFromTypeSource = dataSource;
            if (resFromTypeSource is Type)
                resFromTypeSource = CreateListFromType((Type)resFromTypeSource);
            return GetActualSourceFromInstance(resFromTypeSource, dataMember);
        }

        private static IList CreateListFromType(Type type)
        {
            if (typeof(IListSource).IsAssignableFrom(type))
            {
                IListSource listSource = (IListSource)Activator.CreateInstance(type);
                return listSource.GetList();
            }
            else if (typeof(ITypedList).IsAssignableFrom(type) &&
                typeof(IList).IsAssignableFrom(type))
            {
                return (IList)Activator.CreateInstance(type);
            }
            else
            {
                Type itemType = DetermineItemTypeForDataSource(type, null);
                return CreateBindingList(itemType);
            }
        }

        internal static object GetActualSourceFromInstance(object instance, string dataMember)
        {
            Debug.Assert(!(instance is Type));
            if (instance == null || instance is Type)
                return null;

            object baseSource = instance is IListSource ? ((IListSource)instance).GetList() :
                instance;
            if (string.IsNullOrEmpty(dataMember))
            {
                return baseSource;
            }
            PropertyDescriptor propDesc =
                GetListOrTypeItemProperties(baseSource).Find(dataMember, true);
            if (propDesc == null)
            {
				return baseSource;
            }
			IEnumerable en = baseSource as IEnumerable;
			if (en != null)
				baseSource = GetFirstItem(en);
            if (baseSource != null)
            {
                object ret = propDesc.GetValue(baseSource);
				IListSource listSource = ret as IListSource;
				if (listSource != null)
					return listSource.GetList();
                else
                    return ret;
            }
            else
                return null;
        }

        internal static PropertyDescriptorCollection GetListOrTypeItemProperties(object list)
        {
            IListSource listSource = list as IListSource;
            object resolvedList = listSource != null ? listSource.GetList() : list;
            if (resolvedList == null)
                return new PropertyDescriptorCollection(null, true);
            if (resolvedList is ITypedList)
                return ((ITypedList)resolvedList).GetItemProperties(null);
            else if (resolvedList is Type || resolvedList is IEnumerable)
            {
                Type itemType = DetermineItemTypeForDataSource(resolvedList, null);
                return TypeDescriptor.GetProperties(itemType);
            }
            else
                return TypeDescriptor.GetProperties(resolvedList);
        }

        private static object GetFirstItem(IEnumerable enumerable)
        {
            if (enumerable == null)
                return null;
            IEnumerator enumerator = enumerable.GetEnumerator();
            if (enumerator.MoveNext())
                return enumerator.Current;
            else
                return null;
        }

        private static IBindingList CreateBindingList(Type itemType)
        {
            Type blType = typeof(BindingList<>).MakeGenericType(itemType);
            return (IBindingList)Activator.CreateInstance(blType);
        }

        internal static Type DetermineItemTypeForDataSource(object source, string dataMember)
        {
            if (source == null)
                return typeof(object);
            IListSource listSource = source as IListSource;
            if (listSource != null)
                source = listSource.GetList();
            object itemContainer = source;
            if (!string.IsNullOrEmpty(dataMember))
            {
                PropertyDescriptor propDesc =
                    GetListOrTypeItemProperties(source).Find(dataMember, true);
                if (propDesc == null)
                    return typeof(object);
				itemContainer = propDesc.GetValue(source) ?? propDesc.GetType();
            }

			Type itemContainerType = itemContainer as Type;
			if (itemContainerType != null)
            {
                itemContainer = null;
            }
            else
                itemContainerType = itemContainer.GetType();
            if (typeof(Array).IsAssignableFrom(itemContainerType))
                return itemContainerType.GetElementType();
            /* TBD: IEnumerable<T>
            else if (typeof(IEnumerable).IsAssignableFrom(itemContainerType) &&
                itemContainerType.isge
            {
            }
             */
			IEnumerable en = itemContainer as IEnumerable;
			if (en != null)
            {
                object item = GetFirstItem(en);
                if (item != null)
                    return item.GetType();
            }
            if (typeof(IList).IsAssignableFrom(itemContainerType))
            {
                PropertyInfo[] props = itemContainerType.GetProperties(
                    BindingFlags.Instance | BindingFlags.Public);
                if (props != null)
                {
                    foreach (PropertyInfo propInfo in props)
                    {
                        if (propInfo.Name == "Item" && propInfo.PropertyType != typeof(object)
                            && propInfo.GetIndexParameters().Length > 0)
                        {
                            return propInfo.PropertyType;
                        }
                    }
                }
            }
            return typeof(object);
        }

        private static ConstructorInfo GetDefaultPublicCtor(Type type)
        {
            if (type == null)
                return null;
            return type.GetConstructor(Type.EmptyTypes);
        }

        private static PropertyDescriptorCollection GetItemProperties(object obj,
            PropertyDescriptor[] listAccessors, int startIdx)
        {
            if (obj == null)
                return new PropertyDescriptorCollection(null);
            else if (obj is ITypedList)
            {
                PropertyDescriptor[] accessArr;
                if (listAccessors == null || listAccessors.Length == 0 || startIdx == 0)
                {
                    accessArr = listAccessors;
                }
                else if (startIdx >= listAccessors.Length)
                {
                    accessArr = null;
                }
                else
                {
                    accessArr = new PropertyDescriptor[listAccessors.Length - startIdx];
                    listAccessors.CopyTo(accessArr, startIdx);
                }
                return ((ITypedList)obj).GetItemProperties(accessArr);
            }
            else
            {
                PropertyDescriptorCollection props = GetListOrTypeItemProperties(obj);
                if (listAccessors == null || listAccessors.Length == 0 ||
                    startIdx >= listAccessors.Length)
                {
                    return props;
                }
                PropertyDescriptor propDesc = props.Find(listAccessors[startIdx].Name, true);
                if (propDesc == null)
                    return new PropertyDescriptorCollection(null);
                IEnumerable en = obj as IEnumerable;
                if (en != null)
                {
                    object item = GetFirstItem(en);
                    if (item != null)
                    {
                        object nextObj = propDesc.GetValue(item);
                        return GetItemProperties(nextObj, listAccessors, startIdx + 1);
                    }
                }
                return GetItemProperties(propDesc.GetValue(obj), listAccessors, startIdx + 1);
            }

        }

        private void ApplyFilter()
        {
            if (IsInitialized && _bindingListViewInternal != null &&
                _bindingListViewInternal.SupportsFiltering)
            {
                _bindingListViewInternal.Filter = Filter;
            }
        }

        private void ApplySort()
        {
            if (!IsInitialized)
                return;
            ListSortDescriptionCollection sortDescrs = GetSortDescriptionsFromString(Sort);
            if (_bindingListViewInternal != null &&
                _bindingListViewInternal.SupportsAdvancedSorting)
            {
                if (sortDescrs.Count != 0)
                    _bindingListViewInternal.ApplySort(sortDescrs);
                else
                    _bindingListViewInternal.RemoveSort();
            }
            else if (_bindingListInternal != null && _bindingListInternal.SupportsSorting)
            {
                if (sortDescrs.Count != 0)
                {
                    ListSortDescription descr = sortDescrs[0];
                    _bindingListInternal.ApplySort(descr.PropertyDescriptor,
                        descr.SortDirection);
                }
                else
                    _bindingListInternal.RemoveSort();
            }
        }

        private ListSortDescriptionCollection GetSortDescriptionsFromString(string sort)
        {
            Collection<ListSortDescription> ret = new Collection<ListSortDescription>();
			if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(sort.Trim()))
            {
                PropertyDescriptorCollection props = GetListOrTypeItemProperties(_listInternal);
                int pos = 0;
                pos = SkipWhitespace(sort, pos);
                for (; pos < sort.Length; )
                {
                    bool descending = false;
                    string fieldName = ExtractSortIdentifier(sort, ref pos, out descending);
                    if (pos < sort.Length && (sort[pos] == ',' || sort[pos] == ';'))
                    {
                        pos++;
                        pos = SkipWhitespace(sort, pos);
                    }
                    PropertyDescriptor propDesc = props.Find(fieldName, true);
                    if (propDesc == null)
                        throw new ArgumentNullException("Sort");
                    ListSortDescription sortDescr = new ListSortDescription(propDesc,
                        descending ? ListSortDirection.Descending : ListSortDirection.Ascending);
                    ret.Add(sortDescr);
                }
            }

            if (ret.Count > 0)
            {
                ListSortDescription[] retArr = new ListSortDescription[ret.Count];
                ret.CopyTo(retArr, 0);
                return new ListSortDescriptionCollection(retArr);
            }
            else
                return new ListSortDescriptionCollection();
        }

        private static string GetSortStringFromDescriptions(
            ListSortDescriptionCollection sortDescriptions)
        {
            if (sortDescriptions == null || sortDescriptions.Count == 0)
                return null;
            string ret = "";
            foreach (ListSortDescription sortField in sortDescriptions)
            {
                if (ret.Length != 0)
                    ret += ", ";
                ret += GetSortStringFromDescription(sortField.PropertyDescriptor,
                    sortField.SortDirection);
            }
            return ret;
        }

        private static string GetSortStringFromDescription(PropertyDescriptor property,
            ListSortDirection direction)
        {
            string ret = property.Name;
            if (direction == ListSortDirection.Descending)
                ret += " DESC";
            return ret;
        }

        static private int SkipWhitespace(string text, int pos)
        {
            if (pos < 0 || pos >= text.Length)
                return pos;
            int posEnd = pos;
            for (; posEnd < text.Length && IsWhiteSpace(text[posEnd]); posEnd++) ;
            return posEnd;
        }

        static private bool IsWhiteSpace(char c)
        {
            return Char.IsWhiteSpace(c);
        }

        //Processes sort field name definition. having a form:
        //<field name> <direction specifier>
        //, where <field name> may contain spaces and can be optionally framed in square 
        //brackets (existence of spaces doesn't require brackets), <direction specifier>
        //is optional and can take values "ASC" or "DESC".
        //If string has spaces and has no brackets and last word is "ASC" or "DESC" then it's
        //treated as direction specifier but not as a part of field name.
        //Returns field name (without brackets).
        //pos receive a position index beyond the processes string.
        //descending receives a value based on <direction specifier> specified or false
        //if not specified.
        static private string ExtractSortIdentifier(string text, ref int pos,
            out bool descending)
        {
            // Utility function used in parsing.
            // Parses an identifier (variable name).
            // Variable name is either a valid identifier (which may be not a requirement
            // in ADO.NET, but we need this restriction), or it is enclosed in brackets.
            // In latter case, if there are bracket characters inside the name, the closing bracket
            // is escaped with \, like this: [My Variable Name [yes it is!\]]
            // Rather weird escaping rules, but that's what ADO.NET exp.language does.
            descending = false;
            string id = "";
            int firstPos = pos;
            bool quote = text[pos] == '[';
            if (quote)
                pos++;
            int lastWordPos = -1;
            bool inSpace = true;
            bool done = false;
            for (; !done && pos < text.Length; pos++)
            {
                char c = text[pos];
                if (quote)
                {
                    if (c == '\\' && pos < text.Length - 1 && text[pos + 1] == ']')
                        continue;
                    if (c == ']' && pos > firstPos && text[pos - 1] != '\\')
                    {
                        done = true;
                        continue;
                    }
                }
                bool isWSP = IsWhiteSpace(c);
                bool isLetDig = IsLetterOrDigit(c);
                if (!quote && !(isLetDig || isWSP))
                    break;
                if (isLetDig && inSpace)
                    lastWordPos = pos;
                inSpace = isWSP;
                id += c;
            }

            string ascdesc = "";
            if (quote)
            {
                if (done) // ']' found && more than one word
                {
                    pos = SkipWhitespace(text, pos);
                    for (; pos < text.Length && IsLetterOrDigit(text[pos]); pos++)
                        ascdesc += text[pos];
                }
            }
            else if (firstPos != lastWordPos) //multiple words
            {
                ascdesc = text.Substring(lastWordPos, pos - lastWordPos).Trim();
            }
            descending = string.Compare(ascdesc, "DESC", true) == 0;
            bool ascending = !descending && string.Compare(ascdesc, "ASC", true) == 0;
            id = id.Trim();
            if ((descending || ascending) && !quote)
            {
                id = id.Substring(0, id.Length - ascdesc.Length).Trim();
            }
            return id;
        }

        static private bool IsLetterOrDigit(char c)
        {
            return Char.IsLetterOrDigit(c) || c == '_';
        }

		/*
		private void EnsureAddNewFinished(int itemIndex, bool cancel)
		{
			if (_addingNewItemIdx < 0 || itemIndex != _addingNewItemIdx ||
				_addingNewItemIdx >= ListInternal.Count)
				return;
			object item = ListInternal[itemIndex];
			_addingNewItemIdx = -1;
			if (item == null)
				return;
			IEditableObject editable = item as IEditableObject;
			if (editable != null)
			{
				if (cancel)
				{
					editable.CancelEdit();
					if (itemIndex < ListInternal.Count && item == ListInternal[itemIndex])
						RemoveAt(itemIndex);
				}
				else
				{
					editable.EndEdit();
				}
			}
		}
		 */
		#endregion

		#region ISupportInitialize
		void ISupportInitialize.BeginInit()
        {
            _initializing = true;
        }
        void ISupportInitialize.EndInit()
        {
            if (_initializing)
            {
                _initializing = false;
                if (_sourceLoadDeferred)
                {
                    _sourceLoadDeferred = false;
                    ReDetermineSourceRequest();
                }
                if (Initialized != null)
                    Initialized(this, EventArgs.Empty);
            }
        }
        #endregion

        #region ISupportInitializeNotification
        private EventHandler Initialized;
        event EventHandler ISupportInitializeNotification.Initialized
        {
            add { this.Initialized += value; }
            remove { this.Initialized -= value; }
        }

        bool IsInitialized
        {
            get 
            { 
#if (ASP_NET_2)
                return _ASPIsPagePreLoaded && !_initializing;
#else
                return !_initializing; 
#endif
            }
        }

        bool ISupportInitializeNotification.IsInitialized
        {
            get { return this.IsInitialized; }
        }
        #endregion

        #region IBindingList
        void IBindingList.AddIndex(PropertyDescriptor property)
        {
            if (_bindingListInternal != null)
                _bindingListInternal.AddIndex(property);
            else
                throw new NotSupportedException("AddIndex");
        }
        void IBindingList.RemoveIndex(PropertyDescriptor property)
        {
            if (_bindingListInternal != null)
                _bindingListInternal.RemoveIndex(property);
            else
                throw new NotSupportedException("RemoveIndex");
        }
        #endregion

        #region ICancelAddNew
        /*
        void ICancelAddNew.CancelNew(int itemIndex)
        {
        }
        void ICancelAddNew.EndNew(int itemIndex)
        {
        }
         */

        #endregion

        #region ShouldSerialize methods
        [Obfuscation(Exclude = true)]
        private bool ShouldSerializeAllowNew()
        {
            return !_allowNewExplicit;
        }
        [Obfuscation(Exclude = true)]
        private bool ShouldSerializeFilter()
        {
            return !string.IsNullOrEmpty(Filter);
        }
        [Obfuscation(Exclude = true)]
        private bool ShouldSerializeSort()
        {
            return !string.IsNullOrEmpty(Sort);
        }
        #endregion

        #region ASP2 stuff
#if (ASP_NET_2)

        // Public
#pragma warning disable 1591
        [DefaultValue("")]
        [NotifyParentProperty(true)]
        public string DataSourceID
        {
            get
            {
                if (_dataSourceID == null)
                    return "";
                else
                    return _dataSourceID;
            }
            set
            {
                _dataSourceID = value;
                if (!string.IsNullOrEmpty(_dataSourceID))
                    _dataSource = null;
                ReDetermineSourceRequest();
            }
        }
#pragma warning restore 1591

        // Internal
        internal ASP_UI.Control OwnerControl
        {
            get { return _aspOwnerControl; }
            set 
            {
                if (_aspOwnerControl != value)
                {
                    if (ASPIsPagePreLoaded)
                        throw new InvalidOperationException("Can't change OwnerControl at this stage of life cycle.");
                    ASPDetachFromOwnerControl();
                    _aspOwnerControl = value;
                    ASPAttachToOwnerControl();
                }
            }
        }


        // Private

        private bool ASPHasSourceID
        {
            get { return !string.IsNullOrEmpty(DataSourceID); }
        }

        private bool ASPIsDesignMode
        {
            get
            {
                if (OwnerControl == null)
                    return false;
                PropertyInfo prop = OwnerControl.GetType().GetProperty("DesignMode",
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (prop == null || !typeof(bool).Equals(prop.PropertyType))
                    return false;
                else
                    return (bool)prop.GetValue(OwnerControl, null);
            }

        }

        private ASP_UI.IDataSource ASPGetDataSource(string dataSourceID)
        {
            if (string.IsNullOrEmpty(dataSourceID))
                return null;
            return ASPFindControl(dataSourceID) as ASP_UI.IDataSource;
        }

        private bool ASPIsPagePreLoaded
        {
            get { return _ASPIsPagePreLoaded; }
        }

        private ASP_UI.DataSourceView ASPDataView
        {
            get { return _ASPDataView; }
        }

        private ASP_UI.DataSourceView ASPGetDataView(string dataSourceID, string dataMember)
        {
            ASP_UI.IDataSource dataSource = ASPGetDataSource(dataSourceID);
            if (dataSource == null)
                return null;
            // dma--> ANSCH000303 fix:
            if (dataSource is System.Web.UI.WebControls.SqlDataSource)
            {
                System.Web.UI.WebControls.SqlDataSource sqlds = dataSource as System.Web.UI.WebControls.SqlDataSource;
                if (sqlds != null)
                {
                    if (sqlds.OldValuesParameterFormatString == "{0}")
                    {
                        sqlds.ConflictDetection = System.Web.UI.ConflictOptions.OverwriteChanges;
                    }
                }
            }
            // <--
            return dataSource.GetView(dataMember);
        }

        private string FixOldValuesParameters(System.Web.UI.WebControls.ParameterCollection parameterCollection,
            string sFormatPrefix, string sCommand)
        {
            sCommand = sCommand.Replace(sFormatPrefix, "");
            for (int i = 0; i < parameterCollection.Count; i++)
            {
                System.Web.UI.WebControls.Parameter p = parameterCollection[i];
                p.Name = p.Name.Replace(sFormatPrefix, "");
            }
            return sCommand;
        }

        private ASP_UI.Control ASPFindControl(string dataSourceID)
        {
            if (string.IsNullOrEmpty(dataSourceID) || OwnerControl == null)
                return null;
            ASP_UI.Page page = OwnerControl.Page;
            ASP_UI.Control nameCont = OwnerControl;
            if (nameCont == null)
                return null;
            do
            {
                nameCont = nameCont.NamingContainer;
                if (nameCont == null)
                    return null;
                ASP_UI.Control ret = nameCont.FindControl(dataSourceID);
                if (ret != null)
                    return ret;
            }
            while (nameCont != page);

            return null;
        }

        private ASP_UI.DataSourceSelectArguments ASPCreateSelectArguments(
            ASP_UI.DataSourceView view)
        {
            ASP_UI.DataSourceSelectArguments ret = new ASP_UI.DataSourceSelectArguments();
            return ret;
        }

        private void ASPReDetermineSourceCallback(IEnumerable data)
        {
            _aspSelectResult = data;
            ReDetermineSource();
        }

        private void ASPAttachToOwnerControl()
        {
            if (OwnerControl != null)
            {
                OwnerControl.Init += new EventHandler(OwnerControl_Init);
            }
        }

        private void ASPDetachFromOwnerControl()
        {
            if (OwnerControl != null)
            {
                OwnerControl.Init -= new EventHandler(OwnerControl_Init);
                if (OwnerControl.Page != null)
                    OwnerControl.Page.PreLoad -= new EventHandler(Page_PreLoad);
            }
        }

        void OwnerControl_Init(object sender, EventArgs e)
        {
            ASPDetachFromOwnerControl();
            if (ASPIsDesignMode)
            {
                _ASPIsPagePreLoaded = true;
                ReDetermineSourceRequest();
            }
            else if (OwnerControl.Page != null)
                OwnerControl.Page.PreLoad += new EventHandler(Page_PreLoad);
        }

        void Page_PreLoad(object sender, EventArgs e)
        {
            ASPDetachFromOwnerControl();
            _ASPIsPagePreLoaded = true;
            ReDetermineSourceRequest();
        }

        private void ASPProcessItemAdded(object item)
        {
            if (item == null || ASPDataView == null || !ASPDataView.CanInsert)
                return;
            Dictionary<string, object> itemValues = GetItemValues(item,
                PropertyKindEnum.AnyWritable);
            itemValues = AdjustItemValuesForSource(itemValues);
            //itemValues["Body"] = "qqqqq";
            //bool bbb = itemValues.Remove("Id");

            ASPDataView.Insert(itemValues, 
                new ASP_UI.DataSourceViewOperationCallback(ASPInsertCallback));
        }
        private bool ASPInsertCallback (int affectedRecords, Exception ex)
        {
            if (ex != null)
                throw ex;
            if (affectedRecords == 0)
            {
                throw new Exception("DataBase Insert operation failed.");
            }
            ReDetermineSourceRequest();
            return true;
        }

        private void ASPProcessItemDeleted(object item)
        {
            if (item == null || ASPDataView == null || !ASPDataView.CanDelete)
                return;
            Dictionary<string, object> dataValues = AdjustItemValuesForSource(GetItemValues(item,
                PropertyKindEnum.AnyData));
            Dictionary<string, object> keyValues = AdjustItemValuesForSource(GetItemValues(item,
                PropertyKindEnum.PrimaryKey));
            ASPDataView.Delete(keyValues, dataValues,
                new ASP_UI.DataSourceViewOperationCallback(ASPDeleteCallback));
        }
        private bool ASPDeleteCallback(int affectedRecords, Exception ex)
        {
            if (ex != null)
                throw ex;
            if (affectedRecords == 0)
            {
                throw new Exception("DataBase Delete operation failed.");
            }
            return true;
        }

        private void ASPProcessItemUpdated(object item)
        {
            if (item == null || ASPDataView == null || !ASPDataView.CanDelete)
                return;
            Dictionary<string, object> newValues = AdjustItemValuesForSource(GetItemValues(item,
                PropertyKindEnum.AnyWritable));
            Dictionary<string, object> keyValues = AdjustItemValuesForSource(GetItemValues(item,
                PropertyKindEnum.PrimaryKey), false);
            Dictionary<string, object> oldValues = AdjustItemValuesForSource(GetOldItemValues(item,
                PropertyKindEnum.Any));
            ASPDataView.Update(keyValues, newValues, oldValues, 
                new ASP_UI.DataSourceViewOperationCallback(ASPUpdateCallback));
        }
        private bool ASPUpdateCallback(int affectedRecords, Exception ex)
        {
            if (ex != null)
                throw ex;
            if (affectedRecords == 0)
            {
                throw new Exception("DataBase Update operation failed.");
            }
            return true;
        }

        private Dictionary<string, object> GetItemValues(object item, 
            PropertyKindEnum propKinds)
        {
            if (item == null)
                return null;
            Debug.Assert(propKinds != 0);
            PropertyDescriptorCollection props = GetItemProperties(null);
            Dictionary<string, object> ret = new Dictionary<string, object>(props.Count);
            foreach (PropertyDescriptor propDesc in props)
            {
                if (IncludeProperty(propDesc, propKinds))
                {
                    object val = propDesc.GetValue(item);
                    val = DBNull.Value == val ? null : val;
                    ret[propDesc.Name] = val;
                }
            }
            
            return ret;
        }

        private Dictionary<string, object> FilterItemValues(
            Dictionary<string, object> itemValues, PropertyKindEnum propKinds)
        {
            if (itemValues == null)
                return null;
            Debug.Assert(propKinds != 0);
            PropertyDescriptorCollection props = GetItemProperties(null);
            Dictionary<string, object> ret = new Dictionary<string, object>(props.Count);
            foreach (KeyValuePair<string, object> entry in itemValues)
            {
                PropertyDescriptor propDesc = props.Find(entry.Key, true);
                if (propDesc != null && IncludeProperty(propDesc, propKinds))
                    ret[entry.Key] = entry.Value;
            }

            return ret;
        }

        private Dictionary<string, object> FilterNotNullItemValues(
            Dictionary<string, object> itemValues)
        {
            if (itemValues == null)
                return null;
            Dictionary<string, object> ret = new Dictionary<string, object>(itemValues.Count);
            foreach (KeyValuePair<string, object> entry in itemValues)
            {
                if (entry.Value != null && entry.Value != DBNull.Value)
                    ret.Add(entry.Key, entry.Value);
            }
            return ret;
        }

        private Dictionary<string, object> AdjustItemValuesForSource(
            Dictionary<string, object> itemValues, bool convertGUID)
        {
            if (itemValues == null)
                return null;
            Dictionary<string, object> ret = new Dictionary<string, object>(itemValues.Count);
            foreach (KeyValuePair<string, object> entry in itemValues)
            {
                object retVal = entry.Value;
// Fixed issue with DBNull exception (DMA):
//                if (retVal == null)
//                    retVal = DBNull.Value;
//                else
                if (retVal is Guid)
                {
                    if (convertGUID)
                        retVal = ((Guid)retVal).ToString("D");
                }
                ret.Add(entry.Key, retVal);
            }
            return ret;
        }
        private Dictionary<string, object> AdjustItemValuesForSource(
            Dictionary<string, object> itemValues)
        {
            return AdjustItemValuesForSource(itemValues, true);
        }

        private Dictionary<string, object> GetOldItemValues(object item,
            PropertyKindEnum propKinds)
        {
            if (item == null)
                return null;
            Debug.Assert(propKinds != 0);
            Dictionary<string, object> ret = GetOldValues(item);
            if (ret != null)
                return FilterItemValues(ret, propKinds);
            else
                return GetItemValues(item, propKinds);
        }

        private bool IncludeProperty(PropertyDescriptor propDesc, PropertyKindEnum propKinds)
        {
            if (IsPK(propDesc.Name))
            {
                if (propDesc.IsReadOnly)
                    return (propKinds & PropertyKindEnum.PrimaryKeyReadOnly) != 0;
                else
                    return (propKinds & PropertyKindEnum.PrimaryKeyWritable) != 0;
            }
            else if (propDesc.IsReadOnly)
                return (propKinds & PropertyKindEnum.DataReadOnly) != 0;
            else
                return (propKinds & PropertyKindEnum.DataWritable) != 0;
        }

        private bool IsPK(string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;
            foreach (string curName in DataKeyNamesInternal)
            {
                if (string.Compare(curName, name, true) == 0)
                    return true;
            }
            return false;
        }

        //private void ASPAttachToView()
        //{
        //    if (ASPDataView != null)
        //        ASPDataView.DataSourceViewChanged +=
        //            new EventHandler(ASPDataView_DataSourceViewChanged);
        //}

        //private void ASPDetachFromView()
        //{
        //    if (ASPDataView != null)
        //        ASPDataView.DataSourceViewChanged -=
        //            new EventHandler(ASPDataView_DataSourceViewChanged);
        //}

        //void ASPDataView_DataSourceViewChanged(object sender, EventArgs e)
        //{
        //    /*
        //    OnListChanged(new ListChangedEventArgs(ListChangedType.PropertyDescriptorChanged,
        //        null));
        //    OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        //     */
        //    ReDetermineSourceRequest();
        //}

        internal void ASPDataView_UpdateDataSourceView()
        {
            System.Threading.Thread.Sleep(650); // [DMA] update collections fix.
            ReDetermineSourceRequest();
        }

#region Old Values Management
        private void StoreOldValues(object item)
        {
            if (item != null)
                _oldValuesCache[item] = GetItemValues(item, PropertyKindEnum.Any);
        }

        private void RemoveOldValues(object item)
        {
            if (item != null)
                _oldValuesCache.Remove(item);
        }

        private void ClearOldValues()
        {
            _oldValuesCache.Clear();
        }

        private Dictionary<string, object> GetOldValues(object item)
        {
            if (item != null)
            {
                Dictionary<string, object> ret;
                if (_oldValuesCache.TryGetValue(item, out ret))
                    return ret;
            }

            return null;
        }
#endregion


#endif
        #endregion

    }

#if (!WINFX)

    #region Design-time support
    /// <summary>
    /// Base UITypeEditor for all dynamic enums.
    /// </summary>
    internal abstract class BaseEnumEditor : System.Drawing.Design.UITypeEditor
    {
        // member fields
        private IWindowsFormsEditorService _edSvc;

        // constructor
        protected BaseEnumEditor()
			: base()
        {
        }

        // virtual methods
        protected abstract void FillListBox(System.Windows.Forms.ListBox box,
            ITypeDescriptorContext context, IServiceProvider provider);
        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(
            ITypeDescriptorContext context)
        {
            if (context != null && context.Instance != null /*&& context.Container != null*/)
                return System.Drawing.Design.UITypeEditorEditStyle.DropDown;
            return base.GetEditStyle(context);
        }
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null || /*context.Container == null || */ provider == null)
                return value;
            _edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (_edSvc == null)
                return value;
            System.Windows.Forms.ListBox box = new System.Windows.Forms.ListBox();
            box.Click += new System.EventHandler(OnListBoxClick);
            box.BorderStyle = System.Windows.Forms.BorderStyle.None;
            FillListBox(box, context, provider);
            int idx = -1;
			if (value != null)
			{
				try
				{
					idx = box.Items.IndexOf(value);
				}
				catch
				{
					for (int i = 0; i < box.Items.Count; i++)
					{
						if (box.Items[i] == value)
						{
							idx = i;
							break;
						}
					}
				}
			}
			if (idx > -1)
			{
				box.SelectedIndex = idx;
			}
            _edSvc.DropDownControl(box);
            _edSvc = null;
            object selection = box.SelectedItem;
            if (selection == null)
                return value;
            else
                return box.Items[box.SelectedIndex];
        }

        // callbacks
        private void OnListBoxClick(object sender, System.EventArgs e)
        {
            _edSvc.CloseDropDown();
        }
    }

    internal class C1DataMemberEditor : BaseEnumEditor
    {
        private const string NoneItem = "(none)";

        protected override void FillListBox(System.Windows.Forms.ListBox box, 
            ITypeDescriptorContext context, IServiceProvider provider)
        {
            box.Items.Add(NoneItem);
            if (context == null || context.Instance == null)
                return;
            string dataSourcePropName = "DataSource";
            ComplexBindingPropertiesAttribute attr = 
                (ComplexBindingPropertiesAttribute)TypeDescriptor.GetAttributes(
                context.Instance)[typeof(ComplexBindingPropertiesAttribute)];
            if (attr != ComplexBindingPropertiesAttribute.Default)
                dataSourcePropName = attr.DataSource;
            PropertyDescriptor propDesc = TypeDescriptor.GetProperties(context.Instance).
                Find(dataSourcePropName, false);
            if (propDesc == null)
                return;
            object dataSource = propDesc.GetValue(context.Instance);
            if (dataSource == null)
                return;
            PropertyDescriptorCollection members =
                C1BindingSource.GetListOrTypeItemProperties(dataSource);
            foreach (PropertyDescriptor curProp in members)
                box.Items.Add(curProp.Name);
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (!(value is string))
                return value;
            string strVal = (string)value;
            if (string.IsNullOrEmpty(strVal))
                strVal = NoneItem;
            strVal = base.EditValue(context, provider, value) as string;
            if (strVal == NoneItem)
                strVal = "";
            return strVal;
        }
    }
    #endregion
#endif
}
