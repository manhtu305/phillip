using System;

namespace C1.C1Schedule   
{
    internal static class PlatformIndependenceHelper
    {
        /// <summary>
        /// Gets a platform specific Color object based on its integer representation.
        /// The byte ordering in the <paramref name="color"/> is ARGB, for example
        /// the constant 0x01020304 represents the color with the following components:
        /// A = 0x01
        /// R = 0x02
        /// G = 0x03
        /// B = 0x04
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static 
#if (WINFX || SILVERLIGHT)
            System.Windows.Media.Color
#else
            System.Drawing.Color
#endif
            GetColor(uint color)
		{
#if (WINFX || SILVERLIGHT)
			return System.Windows.Media.Color.FromArgb(
                (byte)((color >> 3 * 8) & 0xFF),
                (byte)((color >> 2 * 8) & 0xFF),
                (byte)((color >> 1 * 8) & 0xFF),
                (byte)(color & 0xFF));
#else
            return System.Drawing.Color.FromArgb((int)color);
#endif
        }

        /// <summary>
        /// Indicates whether the specified <paramref name="color"/> is transparent.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static bool IsEmptyColor(
#if (WINFX || SILVERLIGHT)
			System.Windows.Media.Color
#else
            System.Drawing.Color
#endif
            color)
        {
            return color.A == 0 && color.R == 0 && color.G == 0 && color.B == 0;
		}

#if (WINFX || SILVERLIGHT)
		public static System.Windows.Media.Color 
#else
        public static System.Drawing.Color
#endif
				ParseARGBString(string str)
		{

			int[] values = new int[]{0, 0, 0, 0};
			bool empty = true;
			
			if (!String.IsNullOrEmpty(str))
			{
				String[] strs = str.Split(',');
				if (strs.Length >= 3 && strs.Length <= 4)
				{
					int j = 3;
					for (int i = strs.Length - 1; i >= 0; i--, j--)
					{
						int c = 0;
						if (int.TryParse(strs[i].Trim(), out c))
						{
							values[j] = c;
							empty = false;
						}
					}
				}
			}

			if ( empty )
			{
#if (WINFX || SILVERLIGHT)
				return System.Windows.Media.Color.FromArgb(0, 0, 0, 0);
#else
				return System.Drawing.Color.Empty;
#endif
			}
			else
			{
#if (WINFX || SILVERLIGHT)
				return System.Windows.Media.Color.FromArgb((byte)values[0], (byte)values[1], (byte)values[2], (byte)values[3]);
#else
				return System.Drawing.Color.FromArgb(values[0], values[1], values[2], values[3]);
#endif
			}
		}
    }
}