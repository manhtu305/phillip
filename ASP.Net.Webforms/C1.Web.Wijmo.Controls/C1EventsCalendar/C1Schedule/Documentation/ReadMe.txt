/////////////////////////////////////////////////////////////////////////////////////////
//
// Readme file for C1.C1Schedule namespace
//
/////////////////////////////////////////////////////////////////////////////////////////

=========================================================================================       
Date: October 4, 2006
========================================================================================= 

  All classes and interfaces defined in namespace C1.C1Schedule 
  are shared between the next components:
    * C1.Win.C1Schedule for windows forms,
    * C1.Web.C1Schedule for ASP.Net 2.0,
    * C1.WPF.C1Schedule for WPF,
    * C1.WPF.Schedule for the new WPF version,
    * C1.Silverlight.Schedule for Silverlight.




