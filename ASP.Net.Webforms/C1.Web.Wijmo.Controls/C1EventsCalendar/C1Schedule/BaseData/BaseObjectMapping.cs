using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

#if (WINFX || SILVERLIGHT)
using System.Windows.Media;
#else
using System.Drawing;
#endif

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Represents a generic collection of mappings for properties 
	/// of the <see cref="BaseObject"/> derived objects to appropriate data fields. 
	/// </summary>
	/// <typeparam name="T">The type of the objects for mapping.
	/// It should be derived from the <see cref="BaseObject"/> class 
	/// and have the default parameter-less constructor.</typeparam>
	public class BaseObjectMappingCollection<T> : MappingCollectionBase<T> where T : BaseObject, new()
	{
		#region fields
		private MappingInfo _textMapping;
		private MappingInfo _colorMapping;
		private MappingInfo _captionMapping;
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="BaseObjectMappingCollection{T}"/> class.
		/// </summary>
		public BaseObjectMappingCollection()
		{
			// add mappings for contact's Properties
			_textMapping = Add(typeof(T), "Text", true, "");
			_colorMapping = Add(typeof(T), typeof(string), "Color", false, 
                C1Brush.GetARGBstring(PlatformIndependenceHelper.GetColor(0x00FFFFFF) //Transparent
                ));
			_captionMapping = Add(typeof(T), "MenuCaption", false, "");
		}
		#endregion

		#region object model
		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="BaseObject.Text"/> 
		/// property of the object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="BaseObject.Text"/> 
		/// property of the object to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.Text", "Allows the Text property of the object to be bound to appropriate field in the data source.")]
		public MappingInfo TextMapping
		{
			get
			{
				return _textMapping;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="BaseObject.Color"/> 
		/// property of the object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="BaseObject.Color"/> 
		/// property of the object to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.Color", "Allows the Color property of the object to be bound to appropriate field in the data source.")]
		public MappingInfo ColorMapping
		{
			get
			{
				return _colorMapping;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for <see cref="BaseObject.MenuCaption"/> 
		/// property of the object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="BaseObject.MenuCaption"/> 
		/// property of the object to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.MenuCaption", "Allows the MenuCaption property of the object to be bound to appropriate field in the data source.")]
		public MappingInfo CaptionMapping
		{
			get
			{
				return _captionMapping;
			}
		}
		#endregion

		#region Read Write
		/// <summary>
		/// Fills BaseObject object with values from specified boundObject.
		/// </summary>
		/// <param name="internalObject"></param>
		/// <param name="boundObject"></param>
		/// <param name="raiseChanged"></param>
		/// <returns></returns>
		internal override bool ReadObject(T internalObject, object boundObject, bool raiseChanged)
		{
			bool dirty = false;
			dirty = internalObject.SetText((string)_textMapping.GetPropertyValue(boundObject)) || dirty;
			
			if (_captionMapping.IsMapped)
			{
				dirty = internalObject.SetMenuCaption((string)_captionMapping.GetPropertyValue(boundObject)) || dirty;
			}
			if (_colorMapping.IsMapped)
			{
				string str = (string)_colorMapping.GetPropertyValue(boundObject);
				dirty = internalObject.SetColor((Color)PlatformIndependenceHelper.ParseARGBString(str))|| dirty;
			}
			return base.ReadObject(internalObject, boundObject, raiseChanged, dirty);
		}

		/// <summary>
		/// Fills boundObject with values from specified BaseObject object.
		/// </summary>
		/// <param name="internalObject">T object.</param>
		/// <param name="boundObject">Object from custom data source.</param>
		internal override void WriteObject(T internalObject, object boundObject)
		{
			base.WriteObject(internalObject, boundObject);
			_textMapping.SetPropertyValue(boundObject, internalObject.Text);
			_colorMapping.SetPropertyValue(boundObject, C1Brush.GetARGBstring(internalObject.Color));
			_captionMapping.SetPropertyValue(boundObject, internalObject.MenuCaption);
		}
		#endregion
	}
}
