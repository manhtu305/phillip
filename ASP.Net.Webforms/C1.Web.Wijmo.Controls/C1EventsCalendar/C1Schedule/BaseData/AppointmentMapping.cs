using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif
	/// <summary>
	/// Represents a generic collection of mappings for properties of 
	/// <see cref="BaseObject"/> derived objects to appropriate data fields. 
	/// </summary>
#if (!SILVERLIGHT)
	[ToolboxItem(false)]
#endif
	public class AppointmentMappingCollection : MappingCollectionBase<Appointment>
	{
		#region fields
		private AppointmentCollection _collection;
		private MappingInfo _body;
		private MappingInfo _end;
		private MappingInfo _location;
		private MappingInfo _start;
		private MappingInfo _subject;
		private MappingInfo _properties;
		private MappingInfo _ownerIdMapping;
		private MappingInfo _ownerIndexMapping;
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="AppointmentMappingCollection"/> class.
		/// </summary>
		public AppointmentMappingCollection()
		{
			// add mappings for simple properties
			_body = Add(typeof(Appointment), "Body", true, "");
			_end = Add(typeof(Appointment), "End", true, DateTime.UtcNow.AddMinutes(15));
			_location = Add(typeof(Appointment), "Location", true, "");
			_start = Add(typeof(Appointment), "Start", true, DateTime.UtcNow);
			_subject = Add(typeof(Appointment), "Subject", true, "");

			// add mappings for complex properties
			_properties = Add(typeof(Appointment), typeof(string), "AppointmentProperties", false, "");

			// add mappings for owner Id and Index
			_ownerIdMapping = Add(typeof(Appointment), "OwnerId", false, Guid.Empty);
			_ownerIndexMapping = Add(typeof(Appointment), "OwnerIndex", false, -1);
		}
		#endregion

		#region Read Write
		/// <summary>
		/// Fills BaseObject object with values from specified boundObject.
		/// </summary>
		/// <param name="internalObject"></param>
		/// <param name="boundObject"></param>
		/// <param name="raiseChanged"></param>
		/// <returns></returns>
		internal override bool ReadObject(Appointment internalObject, object boundObject, bool raiseChanged)
		{
			bool dirty = false;
			dirty = internalObject.SetBody((string)_body.GetPropertyValue(boundObject)) || dirty;

			dirty = internalObject.SetLocation((string)_location.GetPropertyValue(boundObject)) || dirty;

			dirty = internalObject.SetSubject((string)_subject.GetPropertyValue(boundObject)) || dirty;

			DateTime dt = _collection.CalendarInfo.ToLocalTime((DateTime)_start.GetPropertyValue(boundObject));
			dirty = internalObject.SetStart(dt) || dirty;

			dt = _collection.CalendarInfo.ToLocalTime((DateTime)_end.GetPropertyValue(boundObject));
			dirty = internalObject.SetEnd(dt) || dirty;

			if (_properties.IsMapped)
			{
				string sourceProps = _properties.GetPropertyValue(boundObject) as string;
                dirty = internalObject.SetAppointmentProperties(sourceProps) || dirty;
			}

			if (_ownerIdMapping.IsMapped)
			{
				object id = _ownerIdMapping.GetPropertyValue(boundObject);
				if (id != null && !(id is Guid))
				{
					id = new Guid((string)id);
				}
				if (id == null)
				{
					id = internalObject.Id;
				}
				else if (!id.Equals(internalObject.Id))
				{
					dirty = internalObject.SetOwnerId((Guid)id) || dirty;
				}
			}
			else if (_ownerIndexMapping.IsMapped)
			{
				object index = _ownerIndexMapping.GetPropertyValue(boundObject);
				if (index == null)
				{
					index = internalObject.Index;
				}
				else if (!index.Equals(internalObject.Index))
				{	// <<IS>> This conversion solves db conversion problem. For example Oracle db return decimal, it results in cast exception.
					dirty = internalObject.SetOwnerIndex(Convert.ToInt32(index)) || dirty;
				}
			}

			return base.ReadObject(internalObject, boundObject, raiseChanged, dirty);
		}

		/// <summary>
		/// Fills boundObject with values from specified BaseObject object.
		/// </summary>
        /// <param name="internalObject">Appointment object.</param>
		/// <param name="boundObject">object from custom data source/</param>
		internal override void WriteObject(Appointment internalObject, object boundObject)
		{
			base.WriteObject(internalObject, boundObject);

			_body.SetPropertyValue(boundObject, internalObject.Body);
			_end.SetPropertyValue(boundObject, _collection.CalendarInfo.ToDataSourceTime(internalObject.End));
			_location.SetPropertyValue(boundObject, internalObject.Location);
			_start.SetPropertyValue(boundObject, _collection.CalendarInfo.ToDataSourceTime(internalObject.Start));
			_subject.SetPropertyValue(boundObject, internalObject.Subject);
			if (_properties.IsMapped)
			{
                _properties.SetPropertyValue(boundObject, internalObject.GetAppointmentProperties());
			}
			if (internalObject.Owner != null)
			{
				_ownerIdMapping.SetPropertyValue(boundObject, internalObject.Owner.Id);
				_ownerIndexMapping.SetPropertyValue(boundObject, internalObject.Owner.Index);
			}
			else
			{
				_ownerIdMapping.SetPropertyValue(boundObject, Guid.Empty);
				_ownerIndexMapping.SetPropertyValue(boundObject, -1);
			}
		}

		internal override Appointment NewItem()
		{
			Appointment app = new Appointment();
			app.ParentCollection = _collection;
			return app;
		}
		#endregion

		#region object model
		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="Appointment.Body"/> 
		/// property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of the <see cref="Body"/> allow 
		/// the <see cref="Appointment.Body"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.Body", "Allows the Body property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo Body
		{
			get
			{
				return _body;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="Appointment.End"/> 
		/// property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="Appointment.End"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
		C1Description("Mapping.End", "Allows the End property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo End
		{
			get
			{
				return _end;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="Appointment.Location"/>
		/// property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="Appointment.Location"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.Location", "Allows the Location property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo Location		
		{
			get
			{
				return _location;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="Appointment.Start"/>
		/// property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="Appointment.Start"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.Start", "Allows the Start property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo Start		
		{
			get
			{
				return _start;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="Appointment.Subject"/>
		/// property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="Appointment.Subject"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.Subject", "Allows the Subject property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo Subject
		{
			get
			{
				return _subject;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the memory field 
		/// which is intended to store the other properties of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the other properties of 
		/// the <see cref="Appointment"/> object to be bound to 
		/// the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
	    C1Description("Mapping.AppointmentProperties", "Allows the other properties of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo AppointmentProperties
		{
			get
			{
				return _properties;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the Appointment.Owner.Id property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="Appointment.Owner"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
		C1Description("Mapping.OwnerId", "Allows the Owner property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo OwnerIdMapping
		{
			get
			{
				return _ownerIdMapping;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the Appointment.Owner.Index property of the <see cref="Appointment"/> object.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="Appointment.Owner"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
		C1Description("Mapping.OwnerIndex", "Allows the Owner property of the appointment to be bound to appropriate field in the data source.")]
		public MappingInfo OwnerIndexMapping
		{
			get
			{
				return _ownerIndexMapping;
			}
		}
		#endregion

		#region internal
		internal AppointmentCollection Collection
		{
			get
			{
				return _collection;
			}
			set
			{
				_collection = value;
			}
		}
		#endregion
	}
}
