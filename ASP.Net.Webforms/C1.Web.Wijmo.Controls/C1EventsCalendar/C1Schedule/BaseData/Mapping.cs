using System;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// This class provides information on the mapping of the single property 
	/// of the object to the appropriate data field. 
	/// </summary>
#if (!SILVERLIGHT)
	[ToolboxItem(false)]
	[TypeConverterAttribute(typeof(ExpandableObjectConverter)),
 	DefaultProperty("MappingName")]
#endif
	public class MappingInfo
#if (!SILVERLIGHT)
		: Component
#endif
	{
		#region fields
		private string _propertyName;

#if SILVERLIGHT
		private PropertyInfo _propertyInfo;
#else
		private PropertyDescriptor _propertyDescriptor;
#endif

		private Type _propertyDataType;
		private string _mappingName = string.Empty;
		private bool _required;
		private bool _isMapped;
		private object _defaultValue;
		#endregion

		#region ctor
		/// <summary>
		/// Use this ctor for simple properties that has the same types 
		/// in the internal object and in the bound object.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="propName"></param>
		/// <param name="required"></param>
		/// <param name="defaultValue"></param>
		internal MappingInfo(Type type, string propName, bool required, object defaultValue)
		{
			_propertyDataType = type.GetProperty(propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).PropertyType;
			_propertyName = propName;
			_mappingName = string.Empty;
			_required = required;
			_defaultValue = defaultValue;
		}

		/// <summary>
		/// Use this ctor when property of bound object has to have the type other than 
		/// type of the internal object's property.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="dataType"></param>
		/// <param name="propName"></param>
		/// <param name="required"></param>
		/// <param name="defaultValue"></param>
		internal MappingInfo(Type type, Type dataType, string propName, bool required, object defaultValue)
		{
			_propertyDataType = dataType;
			_propertyName = propName;
			_mappingName = string.Empty;
			_required = required;
			_defaultValue = defaultValue;
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="Type"/> value determining the type of the data field 
		/// or property which can be bound to this object.
		/// </summary>
		[C1Description("Mapping.DataType", "Type of data field or property which can be bound to this object.")]
		public Type DataType
		{
			get
			{
				return _propertyDataType;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="String"/> value determining the name of the data field 
		/// or property to be bound to the object.
		/// </summary>
#if (!SILVERLIGHT)
		[TypeConverter(typeof(StringConverter))]
		[RefreshProperties(RefreshProperties.All)]
#endif
		[DefaultValue(""),
	    C1Description("Mapping.MappingName", "Name of data field or property to be bound to this object.")]
		public string MappingName
		{
			get
			{
				return _mappingName;
			}
			set
			{
				_isMapped = value.Length > 0;
                Reset();
                _mappingName = value;
			}
		}

		/// <summary>
		/// Gets the <see cref="Boolean"/> value indicating if this objects is mapped 
		/// to the data field or property.
		/// </summary>
		[C1Description("Mapping.IsMapped", "Indicates if this objects is mapped to data field or property.")]
		public bool IsMapped
		{
			get
			{
				return _isMapped;
			}
		}

		/// <summary>
		/// Gets the <see cref="Boolean"/> value indicating if mapping for this property is required or optional.
		/// </summary>
		[C1Description("Mapping.Required", "Indicates if mapping for this property is required or optional.")]
		public bool Required
		{
			get
			{
				return _required;
			}
		}

		/// <summary>
		/// Gets the <see cref="String"/> value determining the name of the property 
		/// which should be mapped.
		/// </summary>
		[C1Description("Mapping.PropertyName", "Name of the property which should be mapped.")]
		public string PropertyName
		{
			get
			{
				return _propertyName;
			}
		}
		#endregion

		#region internal
#if (SILVERLIGHT)
		/// <summary>
		/// Returns the property of specified object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal object GetPropertyValue(object obj)
		{
			object val = null;
			if (IsMapped)
			{
                if (obj is C1.Silverlight.Data.DataRow)
                {
                    C1.Silverlight.Data.DataRow row = obj as C1.Silverlight.Data.DataRow;
                    return row[MappingName];
                }
                else
                {
				    _propertyInfo = _propertyInfo ?? FindPropertyInfo(obj);
				    try
				    {
					    val = _propertyInfo.GetValue(obj, null);
				    }
				    catch
				    {
					    _propertyInfo = FindPropertyInfo(obj);
					    val = _propertyInfo.GetValue(obj, null);
				    }
				    if (val == null || val is System.DBNull)
				    {
					    val = _defaultValue;
				    }
                }
            }
			else if (Required && !ScheduleStorage.DesignMode)
			{
				ThrowRequiredMappingException();
			}
			return val;
		}

		private PropertyInfo FindPropertyInfo(object source)
		{
			return C1BindingSource.GetListOrTypeItemProperty(source, MappingName);
		}

		/// <summary>
		/// Sets property of specified object to the specified value.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		internal void SetPropertyValue(object obj, object value)
		{
			if (IsMapped)
			{
                if (obj is C1.Silverlight.Data.DataRow)
                {
                    C1.Silverlight.Data.DataRow row = obj as C1.Silverlight.Data.DataRow;
                    row[MappingName] = value;
                }
                else
                {
				    _propertyInfo = _propertyInfo ?? FindPropertyInfo(obj);
				    if (_propertyInfo != null)
				    {
                        object val = _propertyInfo.GetValue(obj, null);
                        if ( (val != null && !val.Equals(value) ) || (val == null && value != null))
                        {
					        _propertyInfo.SetValue(obj, value, null);
                        }
				    }
                }
            }
			else if (Required && !ScheduleStorage.DesignMode)
			{
				ThrowRequiredMappingException();
			}
		}
#else
        /// <summary>
		/// Returns the property of specified object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal object GetPropertyValue(object obj)
		{
			object val = null;
			if (IsMapped)
			{
				_propertyDescriptor = _propertyDescriptor ?? FindPropertyDescriptor(obj);
				try
				{
					val = _propertyDescriptor.GetValue(obj);
				}
				catch
				{
					_propertyDescriptor = FindPropertyDescriptor(obj);
					val = _propertyDescriptor.GetValue(obj);
				}
				if (val == null || val is System.DBNull)
				{
					val = _defaultValue;
				}
			}
#if WINFX 
			else if (Required && !ScheduleStorage.DesignMode)
#else
			else if (Required && !DesignMode)
#endif
			{
				ThrowRequiredMappingException();
			}
			return val;
		}

		private PropertyDescriptor FindPropertyDescriptor(object source)
		{
			return C1BindingSource.GetListOrTypeItemProperties(source).Find(MappingName, true);
		}

		/// <summary>
		/// Sets property of specified object to the specified value.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		internal void SetPropertyValue(object obj, object value)
		{
			if (IsMapped)
			{
				_propertyDescriptor = _propertyDescriptor ?? FindPropertyDescriptor(obj);
				if (_propertyDescriptor != null)
				{
                    object val = _propertyDescriptor.GetValue(obj);
                    if ( (val != null && !val.Equals(value) ) || (val == null && value != null))
                    {
                        _propertyDescriptor.SetValue(obj, value);
                    }
				}
			}
#if WINFX 
			else if (Required && !ScheduleStorage.DesignMode)
#else
			else if (Required && !DesignMode)
#endif
			{
				ThrowRequiredMappingException();
			}
		}
#endif
        internal void Reset()
        {
            // clear cached values
#if SILVERLIGHT
            _propertyInfo = null;
#else
            _propertyDescriptor = null;
#endif
        }

		private void ThrowRequiredMappingException()
		{
#if END_USER_LOCALIZATION
			throw new System.ArgumentNullException(this.PropertyName, 
				String.Format(Strings.C1Schedule.Exceptions.MappingRequired, this.PropertyName));
#elif WINFX
			throw new System.ArgumentNullException(this.PropertyName,
				String.Format(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "MappingRequired",
				"The required MappingName for the {0} is not specified.", ScheduleStorage.Info.CultureInfo),
				this.PropertyName));
#elif SILVERLIGHT
            throw new System.ArgumentNullException(this.PropertyName,
                String.Format(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.MappingRequired, this.PropertyName));
#else
			throw new System.ArgumentNullException(this.PropertyName, 
				String.Format(C1Localizer.GetString("The required MappingName for the {0} is not specified."), 
				this.PropertyName));
#endif
		}

		#endregion
		#region WinFX specific
#if (WINFX || SILVERLIGHT)
		private C1ScheduleStorage _scheduleStorage;

		internal C1ScheduleStorage ScheduleStorage
		{
			get
			{
				return _scheduleStorage;
			}
			set
			{
				_scheduleStorage = value;
			}
		}
#endif
		#endregion	
	}

	/// <summary>
	/// Represents a collection of mappings for properties of the 
	/// <see cref="BasePersistableObject"/> object to the appropriate data fields. 
	/// Names of properties are keys in this collection.
	/// </summary>
	/// <typeparam name="T">The type of the objects for mapping.
	/// It should be derived from the <see cref="BasePersistableObject"/>class 
	/// and have the default parameter-less constructor.</typeparam>
#if !WINFX && !SILVERLIGHT
	[TypeConverterAttribute(typeof(System.ComponentModel.ComponentConverter))]
#endif
	public class MappingCollectionBase<T> : KeyedCollection<string, MappingInfo>
#if (!SILVERLIGHT)
		, IComponent
#endif
		where T : BasePersistableObject, new()
	{
		#region fields
		private MappingInfo _idMapping;
		private MappingInfo _indexMapping;
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="MappingCollectionBase{T}"/> class.
		/// </summary>
		public MappingCollectionBase()
		{
			// add mappings for Id and Index
			_idMapping = Add(typeof(BasePersistableObject), "Id", false, Guid.Empty);
			_indexMapping = Add(typeof(BasePersistableObject), "Index", false, -1);
		}
		#endregion

		#region object model
		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="BasePersistableObject.Id"/> property.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="BasePersistableObject.Id"/> property  
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
		///  <remarks> Each object derived from <see cref="BasePersistableObject"/> is identified 
		/// by either Id or Index property. 
		/// To allow data to be correctly restored from your data sources you should specify mapping 
		/// either for the <see cref="MappingCollectionBase{T}.IdMapping"/> or 
		/// for the <see cref="MappingCollectionBase{T}.IndexMapping"/>.
		/// It makes sense in case if you bind to your data storages of Resources, Labels, 
		/// Contacts or Categories. 
		/// C1Schedule saves only Ids or Indexes of these objects in AppointmentSorage. 
		/// If there is no mapping for IdMapping or IndexMapping, C1Schedule won't be able to 
		/// identify Appointment's properties correctly in storages initialized by your data.
		/// </remarks>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
		C1Description("Mapping.Id", "Allows the Id property of BasePersistableObject to be bound to appropriate field in the data source.")]
		public MappingInfo IdMapping
		{
			get
			{
				return _idMapping;
			}
			set
			{
				_idMapping = value;
			}
		}

		/// <summary>
		/// Returns <see cref="MappingInfo"/> object for the <see cref="BasePersistableObject.Index"/> property.
		/// If the storage object is bound to a data source via its DataSource property, 
		/// the properties of this object allow the <see cref="BasePersistableObject.Index"/> property 
		/// to be bound to the appropriate field in the data source. 
		/// </summary>
		///  <remarks> Each object derived from <see cref="BasePersistableObject"/> is identified 
		/// by either Id or Index property. 
		/// To allow data to be correctly restored from your data sources you should specify mapping 
		/// either for the <see cref="MappingCollectionBase{T}.IdMapping"/> or 
		/// for the <see cref="MappingCollectionBase{T}.IndexMapping"/>.
		/// It makes sense in case if you bind to your data storages of Resources, Labels, 
		/// Contacts or Categories. 
		/// C1Schedule saves only Ids or Indexes of these objects in AppointmentSorage. 
		/// If there is no mapping for IdMapping or IndexMapping, C1Schedule won't be able to 
		/// identify Appointment's properties correctly in storages initialized by your data.
		/// </remarks>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Mapping"),
		C1Description("Mapping.Index", "Allows the Index property of BasePersistableObject to be bound to appropriate field in the data source.")]
		public MappingInfo IndexMapping
		{
			get
			{
				return _indexMapping;
			}
			set
			{
				_indexMapping = value;
			}
		}
		#endregion

		#region internal methods
		internal MappingInfo Add(Type type, string propName, bool required, object defaulValue)
		{
			MappingInfo map = new MappingInfo(type, propName, required, defaulValue);
			Add(map);
			return map;
		}
		internal MappingInfo Add(Type type, Type dataType, string propName, bool required, object defaulValue)
		{
			MappingInfo map = new MappingInfo(type, dataType, propName, required, defaulValue);
			Add(map);
			return map;
		}
        internal void Reset()
        {
            foreach (MappingInfo info in Items)
            {
                info.Reset();
            }
        }
		#endregion

		#region overrides
		/// <summary>
		/// Returns property name as the key for collection.
		/// </summary>
		/// <param name="item">The <see cref="MappingInfo"/> object.</param>
		/// <returns>The property name.</returns>
		protected override string GetKeyForItem(MappingInfo item)
		{
			return item.PropertyName;
		}
		#endregion

		#region working with objects
		/// <summary>
		/// Fills internal object derived from BasePersistableObject 
		/// with values from specified bound object.
		/// </summary>
		/// <param name="internalObject"></param>
		/// <param name="boundObject"></param>
		/// <param name="raiseChanged">Specifies if internalObject should raise Changed 
		/// event during this method call</param>
		/// <returns>Value indicating if bound object was changed by this method.</returns>
		/// <remarks>Overriding method of derived class should call the base 
		/// class's ReadObject method or fill BasePersistableObject
		/// properties on it's own.</remarks>
		internal virtual bool ReadObject(T internalObject, object boundObject, bool raiseChanged)
		{
			return this.ReadObject(internalObject, boundObject, raiseChanged, false);
		}
		
		/// <summary>
		/// Fills internal object derived from BasePersistableObject 
		/// with values from specified bound object.
		/// </summary>
		/// <param name="internalObject"></param>
		/// <param name="boundObject"></param>
		/// <param name="raiseChanged">Specifies if internalObject should raise Changed 
		/// event during this method call</param>
		/// <param name="dirty">Specifies whether an object has been changed</param>
		/// <returns>Value indicating if bound object was changed by this method.</returns>
		/// <remarks>Overriding method of derived class should call the base 
		/// class's ReadObject method or fill BasePersistableObject
		/// properties on it's own.</remarks>
		internal virtual bool ReadObject(T internalObject, object boundObject, bool raiseChanged, bool dirty)
		{
			if (_idMapping.IsMapped)
			{
				object id = _idMapping.GetPropertyValue(boundObject);
                if (id != null && !(id is Guid))
                {
                    id = new Guid((string)id);
                }
				if (id == null)
				{
					id = internalObject.Id;
				}
				else if (!id.Equals(internalObject.Id))
				{
					internalObject.SetId((Guid)id);
					dirty = true;
				}
			}
			else if (_indexMapping.IsMapped)
			{
				object index = _indexMapping.GetPropertyValue(boundObject);
				if (index == null)
				{
					index = internalObject.Index;
				}
				else if (!index.Equals(internalObject.Index))
				{	// <<IS>> This conversion solve db conversion problem. For example Oracle db return decimal, it results in cast exception.
					internalObject.SetIndex(Convert.ToInt32(index));
					dirty = true;
				}
			}
			if (dirty && raiseChanged)
				internalObject.OnChanged();
			return dirty;
		}

		/// <summary>
		/// When implemented in a derived class fills boundObject 
		/// with values from specified internalObject.
		/// </summary>
		/// <param name="internalObject">object from C1Schedule collection.</param>
		/// <param name="boundObject">object from custom data source.</param>
		/// <remarks>Overriding method of derived class should call 
		/// the base class's WriteObject method or fill boundObject
		/// properties on it's own.</remarks>
		internal virtual void WriteObject(T internalObject, object boundObject)
		{
			_idMapping.SetPropertyValue(boundObject, internalObject.Id);
            if (_indexMapping.IsMapped && internalObject.Index == -1)
			{
                // fill index from data source if it is filled there
				object index = _indexMapping.GetPropertyValue(boundObject);
                if (index != null && (int)index != -1)
				{
					internalObject.SetIndex(Convert.ToInt32(index));
				}
			}
			_indexMapping.SetPropertyValue(boundObject, internalObject.Index);
		}

		/// <summary>
		/// Implementation in derived classes should return new object of type T.
		/// </summary>
		/// <returns>New object of type T.</returns>
		internal virtual T NewItem()
		{
			return new T();
		}
		#endregion

		#region design-time support
		/// <summary>
		/// Gets the number of elements actually contained in 
		/// the <see cref="MappingCollectionBase{T}"/> object.
		/// </summary>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}

		/// <summary>
		/// Gets the generic equality comparer that is used to determine equality 
		/// of keys in the collection.
		/// </summary>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		public new IEqualityComparer<string> Comparer
		{
			get
			{
				return base.Comparer;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected override void InsertItem(int index, MappingInfo item)
		{
#if (!SILVERLIGHT)
			item.Site = Site;
#endif
			base.InsertItem(index, item);
		}

		/// <summary>
		/// Represents the method that handles the Disposed event of a component. 
		/// </summary>
		public event EventHandler Disposed;

#if (!SILVERLIGHT)
		private ISite _iSite;

        /// <summary>
		/// Gets or sets the ISite associated with the IComponent. 
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public virtual ISite Site
		{
			get
			{
				return _iSite;
			}
			set
			{
				_iSite = value;
				foreach (MappingInfo item in Items)
				{
					item.Site = value;
				}
			}
		}
#endif
        /// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, 
		/// or resetting unmanaged resources. 
		/// </summary>
		public virtual void Dispose()
		{
			//There is nothing to clean.
			if (Disposed != null)
				Disposed(this, EventArgs.Empty);
		}
		#endregion

		#region WinFX specific
#if (WINFX || SILVERLIGHT)
		private C1ScheduleStorage _scheduleStorage;

		internal C1ScheduleStorage ScheduleStorage
		{
			get
			{
				return _scheduleStorage;
			}
			set
			{
				_scheduleStorage = value;
				foreach (MappingInfo item in Items)
				{
					item.ScheduleStorage = _scheduleStorage;
				}
			}
		}
#endif
		#endregion
	}
}
