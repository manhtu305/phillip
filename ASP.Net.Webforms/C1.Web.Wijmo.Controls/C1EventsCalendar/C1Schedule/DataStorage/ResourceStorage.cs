﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="ResourceStorage"/> is the storage for <see cref="Resource"/> objects.
	/// It allows binding to the data source and mapping data source fields 
	/// to the resource properties.
	/// </summary>
	public class ResourceStorage : BaseStorage<Resource, BaseObjectMappingCollection<Resource>>
	{
		#region ctor
		internal ResourceStorage(C1ScheduleStorage scheduleStorage): base(scheduleStorage) 
		{
            Objects = new ResourceCollection(this);
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="ResourceCollection"/> object 
		/// that contains resource related information. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true)]
#endif
		//[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Storage.Resources", "The collection of resources.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		[Browsable(false)]
#endif
		public ResourceCollection Resources
		{
			get
			{
				return (ResourceCollection)Objects;
			}
		}
		#endregion
	}
}
