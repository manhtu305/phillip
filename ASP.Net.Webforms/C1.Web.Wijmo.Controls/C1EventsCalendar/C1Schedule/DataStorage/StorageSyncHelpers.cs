using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

#if (SILVERLIGHT)
using C1.Silverlight.Data;
#else
using System.Data;
#endif


namespace C1.C1Schedule
{
    /// <summary>
    /// Represents a mapping between data source and collection items and holds a cache
    /// of source items in order as they are represented in a source list. The latter
    /// necessary to get an instance of a deleted item.
    /// </summary>
    internal class SourceToCollectionItemsMap
    {
  /*      /// <summary>
        /// Represents a Source Item - Collection Item pair.
        /// </summary>
        private class SourceCollectionItemPair
        {
            private object _sourceItem;
            private BasePersistableObject _collectionItem = null;

            public SourceCollectionItemPair(object sourceItem,
                BasePersistableObject collectionItem)
            {
                _sourceItem = sourceItem;
                _collectionItem = collectionItem;
            }

            public object SourceItem
            {
                get { return _sourceItem; }
                set { _sourceItem = value; }
            }

            public BasePersistableObject CollectionItem
            {
                get { return _collectionItem; }
                set { _collectionItem = value; }
            }
        }*/

        /// key - source item, value - SourceCollectionItemPair, 
        /// item[int].SourceItem - the same as source.item[int]
        //private OrderedDictionary _dict = new OrderedDictionary();
        private readonly ItemsMapTable _mapTable = new ItemsMapTable();

        /// <summary>
        /// Initialize a source items cache.
        /// </summary>
        /// <param name="sourceList"></param>
        public void SetSourceItems(IList sourceList)
        {
#if SILVERLIGHT
			MapTable.Rows.Clear();
#else
            MapTable.Clear();
#endif
            if (sourceList == null)
                return;
            for (int i = 0; i < sourceList.Count; i++)
            {
                MapTable.Rows.Add(MapTable.NewRow());
            }
        }

        /// <summary>
        /// Updates the source items cache based on the specified list change action in
        /// the specified source list.
        /// Returns a map row affected by this change.
        /// </summary>
		/// <param name="e"></param>
		/// <param name="sourceList"></param>
		/// <param name="duplicateAdd"></param>
		/// <returns></returns>
        public ItemsMapTableRow UpdateByListChange(ListChangedEventArgs e, IList sourceList,
            out bool duplicateAdd)
        {
            ItemsMapTableRow ret = null;
            duplicateAdd = false;
            if (e == null)
                return ret;
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                    if (e.NewIndex >= 0 && e.NewIndex <= MapTable.Rows.Count && 
                        sourceList != null && e.NewIndex < sourceList.Count)
                    {
                        if (MapTable.Rows.Count < sourceList.Count)
                        {
                            ret = MapTable.NewRow();
#if (SILVERLIGHT)
							MapTable.Rows.Insert(e.NewIndex, ret);
#else
                            MapTable.Rows.InsertAt(ret, e.NewIndex);
                            ret.AcceptChanges();
#endif
                        }
                        else
                        {
                            duplicateAdd = true;
                            ret = (ItemsMapTableRow)MapTable.Rows[e.NewIndex];
                        }
                    }
                    else
                        Debug.Assert(false);
                    break;
                case ListChangedType.ItemChanged:
                    if (IsValidIndex(e.NewIndex))
                    {
                        ret = (ItemsMapTableRow)MapTable.Rows[e.NewIndex];
                    }
                    else
                        Debug.Assert(false);
                    break;
                case ListChangedType.ItemDeleted:
                    if (IsValidIndex(e.NewIndex))
                    {
                        ret = (ItemsMapTableRow)MapTable.NewRow();
                        ret.ItemArray = MapTable.Rows[e.NewIndex].ItemArray;
                        MapTable.Rows.RemoveAt(e.NewIndex);
                    }
                    else
                        Debug.Assert(false);
                    break;
                case ListChangedType.ItemMoved:
                    if (IsValidIndex(e.OldIndex) && IsValidIndex(e.NewIndex))
                    {
                        if (e.NewIndex != e.OldIndex)
                        {
                            ItemsMapTableRow curRow = 
                                (ItemsMapTableRow)MapTable.Rows[e.OldIndex];
                            object[] values = curRow.ItemArray;
                            MapTable.Rows.Remove(curRow);
#if (!SILVERLIGHT)
                            curRow.AcceptChanges();
#endif
                            ret = MapTable.NewRow();
                            ret.ItemArray = values;
#if SILVERLIGHT
							MapTable.Rows.Insert(e.NewIndex, ret);
#else
                            MapTable.Rows.InsertAt(ret, e.NewIndex);
                            ret.AcceptChanges();
#endif
                        }
                        else
                            ret = (ItemsMapTableRow)MapTable.Rows[e.NewIndex];
                    }
                    else
                        Debug.Assert(false);
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }

            return ret;
        }
        public ItemsMapTableRow UpdateByListChange(ListChangedEventArgs e, IList sourceList)
        {
            bool duplAdd;
            return UpdateByListChange(e, sourceList, out duplAdd);
        }

        public void SetMapping(BasePersistableObject collectionItem, 
            ItemsMapTableRow mapRow)
        {
            if (mapRow == null)
                return;
            if (mapRow.Index >= 0)
                mapRow.CollectionItem = collectionItem;
        }

        public ItemsMapTableRow GetMapRowAt(int idx)
        {
            if (IsValidIndex(idx))
                return (ItemsMapTableRow)MapTable.Rows[idx];
            else
                return null;
        }

		/*
		/// <summary>
		/// Gets a source item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
	    public object GetSourceItemAt(int index)
		{
			return ((SourceCollectionItemPair)_dict[index]).SourceItem;
		}
		 */

		/*
		public BasePersistableObject GetCollectionItem(object sourceItem)
		{
			if (sourceItem == null)
				return null;
			SourceCollectionItemPair pair = _dict[sourceItem] as SourceCollectionItemPair;
			if (pair == null)
			{
				foreach (DictionaryEntry entry in _dict)
				{
					if (entry.Key == sourceItem)
					{
						pair = (SourceCollectionItemPair)entry.Value;
					}
				}
			}
			if (pair != null)
				return pair.CollectionItem;
			else
				return null;
		}
		 */

        /*
        public bool ContainsSourceItem(object sourceItem)
        {
            return sourceItem != null ? _dict[sourceItem] != null : false;
        }
         */

        /// <summary>
        /// Indicates whether the specified index is valid.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool IsValidIndex(int index)
        {
            return index >= 0 && index < MapTable.Rows.Count;
        }

        /*
        private SourceCollectionItemPair GetPairAt(int index)
        {
            return (SourceCollectionItemPair)_dict[index];
        }
         */

        private ItemsMapTable MapTable
        {
            get { return _mapTable; }
        }
    }

    internal class ItemsMapTable : DataTable
    {
        public const string CollectionItemColumnName = "CollectionItem";
        
        private readonly DataColumn _collectionItemColumn;
        
        internal ItemsMapTable()
        {
            _collectionItemColumn = new DataColumn(CollectionItemColumnName, typeof(object));
            Columns.Add(_collectionItemColumn);
        }

        public DataColumn CollectionItemColumn
        {
            get { return _collectionItemColumn; }
        }

        public new ItemsMapTableRow NewRow()
        {
            return (ItemsMapTableRow)base.NewRow();
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new ItemsMapTableRow(builder);
        }
    }

    internal class ItemsMapTableRow: DataRow
    {
        internal ItemsMapTableRow(DataRowBuilder builder): base(builder)
        {
        }

        public new ItemsMapTable Table
        {
            get { return (ItemsMapTable)base.Table; }
        }

        public BasePersistableObject CollectionItem
        {
            get 
            {
#if (!SILVERLIGHT)
                DataRowVersion version = DataRowVersion.Default;
                if (RowState == DataRowState.Detached &&
                    !HasVersion(DataRowVersion.Proposed))
                {
                    version = DataRowVersion.Original;
                }
                object val = Table != null ? this[Table.CollectionItemColumn, version] : 
                    this[ItemsMapTable.CollectionItemColumnName, version];
#else
				object val = Table != null ? this[Table.CollectionItemColumn] : 
                    this[ItemsMapTable.CollectionItemColumnName];
#endif
                if (val != DBNull.Value)
                    return (BasePersistableObject)val;
                else
                    return null;
            }
            set
            {
                if (Table != null)
                    this[Table.CollectionItemColumn] = value;
                else
                    this[ItemsMapTable.CollectionItemColumnName] = value;
            }
        }

        public int Index
        {
            get { return base.Table != null ? base.Table.Rows.IndexOf(this) : -1; }
        }
    }

    internal class SyncBatchInfo
    {
        public enum ActionEnum
        {
            None,
            Add,
            Change,
            Delete,
            Reset
        }

        public enum ProcessingStateEnum
        {
            Pending,
            Processing,
            Finished
        }

        public class SyncActionItem
        {
            private BasePersistableObject _collectionItem;
            //private SyncBatchInfo.ActionEnum _collectionAction = ActionEnum.None;
            private SyncBatchInfo.ActionEnum _action = ActionEnum.None;
            private ItemsMapTableRow _sourceItemMap;
            //private SyncBatchInfo.ActionEnum _sourceAction = ActionEnum.None;
            private SyncBatchInfo.ProcessingStateEnum _processingState =
                ProcessingStateEnum.Pending;
            private bool _initiatorIsCollection;

            public SyncBatchInfo.ActionEnum Action
            {
                get { return _action; }
                set { _action = value; }
            }
            
            public BasePersistableObject CollectionItem
            {
                get { return _collectionItem; }
                set { _collectionItem = value; }
            }

            /*
            public SyncBatchInfo.ActionEnum CollectionAction
            {
                get { return _collectionAction; }
                set { _collectionAction = value; }
            }
             */

            public ItemsMapTableRow SourceItemMap
            {
                get { return _sourceItemMap; }
                set { _sourceItemMap = value; }
            }

            /*
            public SyncBatchInfo.ActionEnum SourceAction
            {
                get { return _sourceAction; }
                set { _sourceAction = value; }
            }
             */

            public SyncBatchInfo.ProcessingStateEnum ProcessingState
            {
                get { return _processingState; }
                set { _processingState = value; }
            }

            public bool InitiatorIsCollection
            {
                get { return _initiatorIsCollection; }
                set { _initiatorIsCollection = value; }
            }
        }

        private readonly List<SyncActionItem> _actionItems = new List<SyncActionItem>();
        private int _deferredStateCounter;
        private ProcessingStateEnum _resetState = ProcessingStateEnum.Finished;


        public SyncActionItem AddSourceAction(ActionEnum action, 
            ItemsMapTableRow sourceItemMap, BasePersistableObject collectionItem)
        {
            if (action == ActionEnum.Reset)
            {
                Debug.Assert(false);
                return null;
            }
            SyncActionItem actionItem = new SyncActionItem();
            actionItem.InitiatorIsCollection = false;
            actionItem.Action = action;
            actionItem.CollectionItem = collectionItem;
            actionItem.SourceItemMap = sourceItemMap;
            ActionItems.Add(actionItem);
            return actionItem;
        }

        public SyncActionItem AddCollectionAction(ActionEnum action,
            BasePersistableObject collectionItem, ItemsMapTableRow sourceItemMap)
        {
            if (action == ActionEnum.Reset)
            {
                Debug.Assert(false);
                return null;
            }
            SyncActionItem actionItem = new SyncActionItem();
            actionItem.InitiatorIsCollection = true;
            actionItem.Action = action;
            actionItem.CollectionItem = collectionItem;
            actionItem.SourceItemMap = sourceItemMap;
            ActionItems.Add(actionItem);
            return actionItem;
        }

        public void NotifyCorrespondence(BasePersistableObject collectionItem,
            ItemsMapTableRow sourceItemMap)
        {
            if (collectionItem == null || sourceItemMap == null)
                return;
            int collItemIdx = -1;
            int sourceItemIdx = -1;
            for (int i = 0; i < ActionItems.Count && (collItemIdx < 0 || sourceItemIdx < 0); 
                i++)
            {
                SyncActionItem item = ActionItems[i];
                if (collItemIdx < 0 && item.CollectionItem == collectionItem)
                    collItemIdx = i;
                if (sourceItemIdx < 0 && item.SourceItemMap == sourceItemMap)
                    sourceItemIdx = i;
            }
            if (collItemIdx != sourceItemIdx)
            {
                if (collItemIdx >= 0)
                {
                    ActionItems[collItemIdx].SourceItemMap = sourceItemMap;
                    if (sourceItemIdx >= 0)
                        ActionItems.RemoveAt(sourceItemIdx);
                }
                else if (sourceItemIdx >= 0)
                {
                    ActionItems[sourceItemIdx].CollectionItem = collectionItem;
                }
            }
        }

        public SyncActionItem GetActionItemForCollectionItem(
            BasePersistableObject collectionItem)
        {
            if (collectionItem == null)
                return null;
            for (int i = 0; i < ActionItems.Count; i++)
            {
                if (ActionItems[i].CollectionItem == collectionItem)
                    return ActionItems[i];
            }
            return null;
        }

        public SyncActionItem GetActionItemForSourceItemMap(
            ItemsMapTableRow sourceItemMap)
        {
            if (sourceItemMap == null)
                return null;
            for (int i = 0; i < ActionItems.Count; i++)
            {
                if (ActionItems[i].SourceItemMap == sourceItemMap)
                    return ActionItems[i];
            }
            return null;
        }

        public void StartDeferredState()
        {
            _deferredStateCounter++;
        }

        public bool FinishDeferredState()
        {
            if (_deferredStateCounter > 0)
                _deferredStateCounter--;
            return _deferredStateCounter == 0;
        }

        public bool IsDeferred
        {
            get { return _deferredStateCounter > 0; }
        }

        public void Clear()
        {
            ActionItems.Clear();
        }

        public int Count
        {
            get { return ActionItems.Count; }
        }

        public SyncActionItem GetItemAt(int index)
        {
            return ActionItems[index];
        }

        public bool RemoveActionItem(SyncActionItem item)
        {
            return ActionItems.Remove(item);
        }

        /// <summary>
        /// Finished means not Reset state.
        /// </summary>
        public ProcessingStateEnum ResetState
        {
            get { return _resetState; }
        }

        public void NotifyReset()
        {
            ActionItems.Clear();
            _resetState = ProcessingStateEnum.Pending;
        }

        public void ResetProcessingStarted()
        {
            _resetState = ProcessingStateEnum.Processing;
        }

        public void ResetProcessingFinished()
        {
            _resetState = ProcessingStateEnum.Finished;
        }

        private List<SyncActionItem> ActionItems
        {
            get { return _actionItems; }
        }

    }

    /*
    internal class SyncBatchesManager
    {
        public SyncBatchInfo EnsureSyncBatch()
        {
            throw new NotImplementedException();
        }

        public SyncBatchInfo TryCloseSyncBatch()
        {
            throw new NotImplementedException();
        }
    }
     */
}