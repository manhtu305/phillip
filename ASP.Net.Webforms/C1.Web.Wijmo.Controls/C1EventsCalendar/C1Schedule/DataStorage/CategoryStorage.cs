﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="CategoryStorage"/> is the storage for <see cref="Category"/> objects.
	/// It allows binding to the data source and mapping data source fields 
	/// to the category properties.
	/// </summary>
    public class CategoryStorage : BaseStorage<Category, BaseObjectMappingCollection<Category>>
	{
		#region ctor
		internal CategoryStorage(C1ScheduleStorage scheduleStorage): base(scheduleStorage) 
		{
			Objects = new CategoryCollection(this);
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="CategoryCollection"/> object that contains 
		/// category related information. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true)]
#endif
		//[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Storage.Categories", "The collection of categories.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		[Browsable(false)]
#endif
		public CategoryCollection Categories
		{
			get
			{
				return (CategoryCollection)Objects;
			}
		}
		#endregion
	}
}
