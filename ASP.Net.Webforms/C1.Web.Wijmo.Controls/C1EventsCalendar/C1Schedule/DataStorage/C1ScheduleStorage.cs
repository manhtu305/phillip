﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Security;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="C1.C1Schedule.C1ScheduleStorage"/> component handles all data 
	/// operations for the scheduler control and contains specific data storages 
	/// for appointments, resources, contacts, owners, labels and statuses, 
	/// which are used for binding different collections to the data.
	/// The <see cref="C1.C1Schedule.C1ScheduleStorage"/> component also handles data 
	/// importing and exporting to other scheduling formats such as binary, iCal, and XML.
	/// </summary>
#if (!SILVERLIGHT)
	[DesignTimeVisible(false),
	DesignerCategory("Data"),
	ToolboxItem(false)]
#endif
    public partial class C1ScheduleStorage
#if (!SILVERLIGHT)
		: Component, ISite		
#endif
	{
		#region events
		/// <summary>
		/// Occurs when appointments were loaded from the file.
		/// </summary>
		internal event EventHandler AppointmentsLoaded;
		internal void OnAppointmentsLoaded()
		{
			if (AppointmentsLoaded != null)
			{
				AppointmentsLoaded(this, null);
			}
		}
		#if (!ASP_NET_2 && !SILVERLIGHT)
		/// <summary>
		/// Occurs when system time has been changed.
		/// </summary>
		internal event EventHandler SystemTimeChanged;
		internal void OnSystemTimeChanged()
		{
			if (SystemTimeChanged != null)
			{
				SystemTimeChanged(this, null);
			}
		}
		#endif
		#endregion

		#region ctor
		internal C1ScheduleStorage()
		{
			InitializeComponent();
#if (!SILVERLIGHT)
			_appointmentStorage.Site =
				_resourceStorage.Site =
				_statusStorage.Site =
				_labelStorage.Site =
				_categoryStorage.Site =
				_contactStorage.Site =
                _ownerStorage.Site = this;

			#if (!ASP_NET_2)
			if (!DesignMode)
			{
				Subscribe();
			}
			#endif
#endif
		}


        #region WinFX specific  //alex
#if (WINFX)
#if(WPF4)
        private readonly C1.WPF.Schedule.C1Scheduler _scheduler;
        internal C1ScheduleStorage(C1.WPF.Schedule.C1Scheduler scheduler)
#else
        private readonly C1.WPF.C1Schedule.C1Scheduler _scheduler;
        internal C1ScheduleStorage(C1.WPF.C1Schedule.C1Scheduler scheduler)//:this()
#endif
        {
            _scheduler = scheduler;
			InitializeComponent();
			
			_appointmentStorage.Site =
				_resourceStorage.Site =
				_statusStorage.Site =
				_labelStorage.Site =
				_categoryStorage.Site =
				_contactStorage.Site = 
                _ownerStorage.Site = this;

			if (!DesignMode)
			{
				// it's required in case if application has no permissions
				try
				{
					Subscribe();
				}
				catch{	}
			}
		}
		
#if(WPF4)
        /// <summary>
        /// Gets the reference to the parent <see cref="C1.WPF.Schedule.C1Scheduler"/> control.
        /// </summary>
        public C1.WPF.Schedule.C1Scheduler Scheduler
#else
		/// <summary>
		/// Gets the reference to the parent <see cref="C1.WPF.C1Schedule.C1Scheduler"/> control.
		/// </summary>
        public C1.WPF.C1Schedule.C1Scheduler Scheduler
#endif
        {
            get { return _scheduler; }
        }
#endif
        #endregion

        #region Silverlight specific  //alex
#if (SILVERLIGHT)
        private readonly C1.Silverlight.Schedule.C1Scheduler _scheduler;
		internal C1ScheduleStorage(C1.Silverlight.Schedule.C1Scheduler scheduler)//:this()
        {
            _scheduler = scheduler;
			InitializeComponent();
		}
		
		/// <summary>
		/// Gets the reference to the parent <see cref="C1.Silverlight.Schedule.C1Scheduler"/> control.
		/// </summary>
		public C1.Silverlight.Schedule.C1Scheduler Scheduler
        {
            get { return _scheduler; }
        }
#endif
        #endregion

        #endregion

        //------------------------------------------------------------------------------------------
		#region ** ISite Members
#if (!SILVERLIGHT)
		[Browsable(false)]
        IComponent ISite.Component
		{
			get
			{
				return this;
			}
		}
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		IContainer ISite.Container
        {
           get
           {
               return base.Container;
           }
        }
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		bool ISite.DesignMode
		{
			get
			{
				return this.DesignMode;
			}
		}
		internal new bool DesignMode
		{
			get
			{
#if (WINFX)
				if (Scheduler != null)
				{
					return DesignerProperties.GetIsInDesignMode(Scheduler);
				}
#endif
				return base.DesignMode;
			}
		}
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        string ISite.Name
		{
			get
			{
				return null;
			}
			set
			{
			}
		}
        object IServiceProvider.GetService(Type serviceType)
		{
			return null;
		}
#else
		internal bool DesignMode
		{
			get
			{
				return DesignerProperties.GetIsInDesignMode(new System.Windows.Controls.Border());
			}
		}
#endif
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="ContactStorage"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
	    [C1Description("Storage.ContactStorage", "The ContactStorage object.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
		public ContactStorage ContactStorage
		{
			get
			{
				return _contactStorage;
			}
		}

        /// <summary>
        /// Gets the <see cref="ContactStorage"/> object used to keep contacts which might be used as appointment owners. 
        /// </summary>
        /// <remarks><see cref="Appointment.Owner"/> property only accepts <see cref="Contact"/> objects which are present in this storage.</remarks>
#if (!SILVERLIGHT)
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
        [C1Description("Storage.OwnerStorage", "The ContactStorage object to keep appointment owners.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
        public ContactStorage OwnerStorage
        {
            get
            {
                return _ownerStorage;
            }
        }

		/// <summary>
		/// Gets the <see cref="AppointmentStorage"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Description("Storage.AppointmentStorage", "The AppointmentStorage object.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
		public AppointmentStorage AppointmentStorage
		{
			get
			{
				return _appointmentStorage;
			}
		}

		/// <summary>
		/// Gets the <see cref="LabelStorage"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Description("Storage.LabelStorage", "The LabelStorage object.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
		public LabelStorage LabelStorage
		{
			get
			{
				return _labelStorage;
			}
		}

		/// <summary>
		/// Gets the <see cref="ResourceStorage"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Description("Storage.ResourceStorage", "The ResourceStorage object.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
		public ResourceStorage ResourceStorage
		{
			get
			{
				return _resourceStorage;
			}
		}

		/// <summary>
		/// Gets the <see cref="StatusStorage"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Description("Storage.StatusStorage", "The StatusStorage object.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
		public StatusStorage StatusStorage
		{
			get
			{
				return _statusStorage;
			}
		}

		/// <summary>
		/// Gets the <see cref="CategoryStorage"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Description("Storage.CategoryStorage", "The CategoryStorage object.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
#if (CLR40)
        [Editor("C1.Win.C1Schedule.Design.StorageEditor, C1.Win.C1Schedule.4.Design", typeof(System.Drawing.Design.UITypeEditor))]
#else
		[Editor(typeof(C1.Win.C1Schedule.Design.StorageEditor), typeof(System.Drawing.Design.UITypeEditor))]
#endif
#endif
		public CategoryStorage CategoryStorage
		{
			get
			{
				return _categoryStorage;
			}
		}

		/// <summary>
		/// Gets the <see cref="ReminderCollection"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		public ReminderCollection Reminders
		{
			get
			{
				return _reminders;
			}
		}
		/// <summary>
		/// Gets the <see cref="ActionCollection"/> object. 
		/// </summary>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		public ActionCollection Actions
		{
			get
			{
				return _actions;
			}
		}

		internal CalendarInfo Info
		{
			get
			{
				return _info;
			}
		}


		internal DayCollection Days
		{
			get
			{
				return _days;
			}
		}
        #endregion

		#if (!ASP_NET_2 && !SILVERLIGHT)
		#region system time
		// on single time zone change this event fired some times
		// skip extra changes if possible
		private bool _isInChange;
		// handle system time changing
		void SystemEvents_TimeChanged(object sender, EventArgs e)
		{
			if (_isInChange)
			{
				return;
			}
			_isInChange = true;
			// reset CalendarInfo
			CalendarInfo.Culture.ClearCachedData();
			Info.CultureInfo.ClearCachedData();
			// update appointments
			if (_info.DateTimeKind == DateTimeKind.Utc)
			{
				// re-read from storage for getting Utc time
				_appointmentStorage.ResetBindings(false);
			}
			_appointmentStorage.Appointments.RefreshData(false);
			// update reminders
			_reminders.UpdateAll();
			_actions.UpdateAll();
			// fire SystemTimeChanged event
			OnSystemTimeChanged();
			_isInChange = false;
		}
		#endregion
		#endif

		#region Export/Import
		/// <summary>
		/// Clears all unbound storages. Loads default sets if any for unbound storages. 
		/// </summary>
		public void Clear()
		{
			_days.ClearAppointments();
			if (!_appointmentStorage.BoundMode )
			{
				_appointmentStorage.Appointments.Clear();
			}
			if (!_categoryStorage.BoundMode)
			{
				_categoryStorage.Categories.LoadDefaults();
			}
			if (!_contactStorage.BoundMode)
			{
				_contactStorage.Contacts.Clear();
			}
            if (!_ownerStorage.BoundMode)
            {
                _ownerStorage.Contacts.Clear();
            }
            if (!_labelStorage.BoundMode)
			{
				_labelStorage.Labels.LoadDefaults();
			}
			if (!_resourceStorage.BoundMode)
			{
				_resourceStorage.Resources.Clear();
			}
			if (!_statusStorage.BoundMode)
			{
				_statusStorage.Statuses.LoadDefaults();
			}
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Exports the scheduler's data to a file in the specified format. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export the scheduler's data to. </param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Export(string path, FileFormatEnum format)
		{
			CreateExchanger(format).Export(path);
		}
#endif

		/// <summary>
		/// Exports the scheduler's data to a stream in the specified format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the scheduler's data will be exported.</param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Export(Stream stream, FileFormatEnum format)
		{
			CreateExchanger(format).Export(stream);
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Exports the appointments' data to a file in the specified format. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export. </param>
		/// <param name="appointments"><see cref="IList{Appointment}"/> to be exported.</param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Export(string path, IList<Appointment> appointments, FileFormatEnum format)
		{
			CreateExchanger(format).Export(path, appointments);
		}
#endif

		/// <summary>
		/// Exports the appointments' data to a stream in the specified format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the appointments's data will be exported.</param>
		/// <param name="appointments"><see cref="IList{Appointment}"/> to be exported.</param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Export(Stream stream, IList<Appointment> appointments, FileFormatEnum format)
		{
			CreateExchanger(format).Export(stream, appointments);
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Imports data into the scheduler from a file of the specified format.
		/// </summary>
		/// <param name="path">A <see cref="String"/> value containing the full path 
		/// (including the file name and extension) to a file which contains 
		/// the data to be imported into the scheduler.</param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Import(string path, FileFormatEnum format)
		{
			try
			{
				_days.BeginUpdate();
				CreateExchanger(format).Import(path);
			}
			finally
			{
				_days.EndUpdate();
			}
		}
#endif

		/// <summary>
		/// Imports the scheduler's data from a stream whose data is in the specified format.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Import(Stream stream, FileFormatEnum format)
		{
			try
			{
				_days.BeginUpdate();
				CreateExchanger(format).Import(stream);
			}
			finally
			{
				_days.EndUpdate();
			}
		}

		private C1ScheduleExchanger CreateExchanger(FileFormatEnum format)
		{
			C1ScheduleExchanger ex = null;
			switch (format)
			{
				case FileFormatEnum.iCal:
					ex = new ICalExchanger(this);
					break;
				case FileFormatEnum.XML:
					ex = new XmlExchanger(this);
					break;
#if (!SILVERLIGHT)
				case FileFormatEnum.Binary:
					ex = new BinaryExchanger(this);
					break;
#endif
				default:
#if END_USER_LOCALIZATION
					throw new System.ArgumentException(Strings.C1Schedule.Exceptions.Item("FormatNotSupported", _info.CultureInfo), format.ToString());
#elif WINFX
					throw new System.ArgumentException(C1.WPF.Localization.C1Localizer.GetString(
						"Exceptions", "FormatNotSupported", "Format {0} is not supported.", _info.CultureInfo), format.ToString());
#elif SILVERLIGHT
                    throw new System.ArgumentException(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.FormatNotSupported, format.ToString());
#else
					throw new System.ArgumentException(C1Localizer.GetString("Format {0} is not supported."), format.ToString());
#endif
			}
			return ex;
		}
		#endregion

#if (!SILVERLIGHT)
		// Detect whether or not this application has the requested permission
		internal static bool IsPermissionGranted(CodeAccessPermission requestedPermission)
		{
			try
			{
				// Try and get this permission
				requestedPermission.Demand();
				return true;
			}
			catch
			{
				return false;
			}
		}
#endif

	}
}
