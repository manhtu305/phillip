﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="LabelStorage"/> is the storage for <see cref="Label"/> objects.
	/// It allows binding to the data source and mapping data source fields 
	/// to the label properties.
	/// </summary>
	public class LabelStorage : BaseStorage<Label, BaseObjectMappingCollection<Label>>
	{
		#region ctor
		internal LabelStorage(C1ScheduleStorage scheduleStorage): base(scheduleStorage) 
		{
            Objects = new LabelCollection(this); 
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="LabelCollection"/> object 
		/// that contains label related information. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true)]
#endif
		//[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Storage.Labels", "The collection of labels.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		[Browsable(false)]
#endif
		public LabelCollection Labels
		{
			get
			{
				return (LabelCollection)Objects;
			}
		}
		#endregion
	}
}
