﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="ContactStorage"/> is the storage for <see cref="Contact"/> objects.
	/// It allows binding to the data source and mapping data source fields 
	/// to the contact properties.
	/// </summary>
	public class ContactStorage : BaseStorage<Contact, BaseObjectMappingCollection<Contact>>
	{
		#region ctor
		internal ContactStorage(C1ScheduleStorage scheduleStorage): base(scheduleStorage) 
		{
            Objects = new ContactCollection(this); 
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="ContactCollection"/> object that contains 
		/// contact related information. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true)]
#endif
		[C1Description("Storage.Contacts", "The collection of contacts.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		[Browsable(false)]
#endif
		public ContactCollection Contacts
		{
			get
			{
				return (ContactCollection)Objects;
			}
		}
		#endregion
	}
}
