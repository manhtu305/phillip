using System;
using System.ComponentModel;
using System.Collections;
using System.Reflection;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="BaseStorage{T, TMappingCollection}"/> is the base generic component 
	/// for all storages in C1Schedule object model.
	/// Represents a storage of objects derived from <see cref="BasePersistableObject"/>.
	/// This component implements methods of operations over the collections of objects. 
	/// It allows the collection to be populated programmatically 
	/// or automatically by retrieving data from the data source which 
	/// is specified by the DataSource and DataMember properties. 
	/// It allows mapping of data source fields to the object properties.
	/// </summary>
	/// <typeparam name="T">The type of the objects which will be stored in the storage.
	/// It should be derived from the <see cref="BasePersistableObject"/> class
	/// and have the default parameter-less constructor.</typeparam>
	/// <typeparam name="TMappingCollection">The type of the collection which
	/// will be used for mapping. It should be derived from 
	/// the <see cref="MappingCollectionBase{T}"/> class
	/// and have the default parameter-less constructor.</typeparam>
#if (!SILVERLIGHT)
	[ComplexBindingProperties("DataSource", "DataMember")]
#endif
    public class BaseStorage<T, TMappingCollection> : C1BindingSource 
#if (!SILVERLIGHT)
		, ISite 
#endif
		where T : BasePersistableObject, new() 
		where TMappingCollection : MappingCollectionBase<T>, new()
	{
		#region fields
		private TMappingCollection _mappings;
		private BaseCollection<T> _objects;
#if (!SILVERLIGHT)
       [NonSerialized]
#endif
       private readonly C1ScheduleStorage _scheduleStorage = null;
       #region Data Synchronization fields
#if (!SILVERLIGHT)
       [NonSerialized]
#endif
	    private readonly SyncBatchInfo _syncBatch = new SyncBatchInfo();
        private readonly SourceToCollectionItemsMap _itemsMap = 
            new SourceToCollectionItemsMap();
        #endregion
        #endregion

        #region ctor
        internal BaseStorage(C1ScheduleStorage scheduleStorage) 
		{
            _scheduleStorage = scheduleStorage;
            _mappings = new TMappingCollection();
#if WINFX
			_mappings.ScheduleStorage = scheduleStorage;
#endif
#if (!SILVERLIGHT)
			_mappings.Site = this;
#endif
		}
		#endregion

		#region public interface
		/// <summary>
		/// Call this method each time after changing your data source 
		/// if it doesn't implement IBindingList interface.
		/// Or use BaseStorage{T, TMappingCollection}.Add 
		/// and BaseStorage{T, TMappingCollection}.Remove 
		/// instead of corresponding methods of your data source class.
		/// </summary>
		public void RefreshData()
		{
#if SILVERLIGHT
			if (!(DataSource is IList))
#else
			if (!(DataSource is IList) && !(DataSource is IListSource))
#endif
			{
				string oldMember = this.DataMember;
				object oldData = this.DataSource;
				this.DataSource = null;
				this.DataSource = oldData;
				this.DataMember = oldMember;
			}
			else
			{
				this.ResetBindings(true);
			}
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets a value indicating if the collection is bound to data. 
		/// </summary>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		public bool BoundMode
		{
			get
			{
				#if (ASP_NET_2)
					return ( !(string.IsNullOrEmpty(DataSourceID) && DataSource == null));
				#else
					return (DataSource != null);
				#endif
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="MappingCollectionBase{T}"/> derived object 
		/// that allows the properties of objects maintained by the current collection 
		/// to be bound to the appropriate fields in the data source. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true), 
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		Browsable(false)]
#endif
		[C1Category("Data"),
	    C1Description("Storage.Mappings", "The collection of mappings.")]
		public TMappingCollection Mappings
		{
			get
			{
				return _mappings;
			}
			set
			{
				_mappings = value;
				if (_mappings != null)
				{
					Mappings.IdMapping = _mappings["Id"];
					Mappings.IndexMapping = _mappings["Index"];
				}
				else
				{
					Mappings.IdMapping = null;
					Mappings.IndexMapping = null;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
#if (!SILVERLIGHT)
		[Obfuscation(Exclude = true)]
#endif
		protected internal bool ShouldSerializeMappings()
		{
			return BoundMode;
		}

        /// <summary>
        /// Gets the reference to the owning <see cref="C1ScheduleStorage"/> component.
        /// </summary>
#if (!SILVERLIGHT)
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
#endif
        public C1ScheduleStorage ScheduleStorage 
        {
            get { return _scheduleStorage; }
        }
		#endregion

		#region internal properties
		internal BaseCollection<T> Objects
		{
			get
			{
				return _objects;
			}
			set
			{
				if (_objects != null && value == null)
				{
					// unsubscribe from collection's events
					_objects.ItemAdded -= new EventHandler<BaseCollectionEventArgs<T>>(_objects_ItemAdded);
					_objects.ItemChanged -= new EventHandler<BaseCollectionEventArgs<T>>(_objects_ItemChanged);
					_objects.ItemRemoved -= new EventHandler<BaseCollectionEventArgs<T>>(_objects_ItemRemoved);
				}
				_objects = value;
				if (_objects != null)
				{
					// subscribe on collection's events
					_objects.ItemAdded += new EventHandler<BaseCollectionEventArgs<T>>(_objects_ItemAdded);
					_objects.ItemChanged += new EventHandler<BaseCollectionEventArgs<T>>(_objects_ItemChanged);
					_objects.ItemRemoved += new EventHandler<BaseCollectionEventArgs<T>>(_objects_ItemRemoved);
				}
			}
		}
		#endregion

		#region Object collection event handlers
		void _objects_ItemRemoved(object sender, BaseCollectionEventArgs<T> e)
		{
            if (SyncBatch.ResetState == SyncBatchInfo.ProcessingStateEnum.Finished)
            {
                ProcessChangeEvent(true, e.Item, e.Item.BaseSourceItemMap,
                    SyncBatchInfo.ActionEnum.Delete);
            }
		}

		void _objects_ItemChanged(object sender, BaseCollectionEventArgs<T> e)
		{
            if (SyncBatch.ResetState == SyncBatchInfo.ProcessingStateEnum.Finished &&
                !e.Item.IsEdit)
            {
                ProcessChangeEvent(true, e.Item, e.Item.BaseSourceItemMap,
                    SyncBatchInfo.ActionEnum.Change);
            }
		}

		void _objects_ItemAdded(object sender, BaseCollectionEventArgs<T> e)
		{
            if (SyncBatch.ResetState == SyncBatchInfo.ProcessingStateEnum.Finished)
            {
                ProcessChangeEvent(true, e.Item, e.Item.BaseSourceItemMap,
                    SyncBatchInfo.ActionEnum.Add);
            }
		}
		#endregion

		#region overrides
#if (!SILVERLIGHT)
		//------------------------------------------------------------------------------------------
		#region ** ISite Members
		/// <summary>
		/// 
		/// </summary>
		[Browsable(false)]
		IComponent ISite.Component
		{
			get
			{
				return _scheduleStorage;
			}
		}
		/// <summary>
		/// 
		/// </summary>
        IContainer ISite.Container
		{
			get
			{
				return _scheduleStorage.Container;
			}
		}
		/// <summary>
		/// 
		/// </summary>
        bool ISite.DesignMode
		{
			get
			{
				return _scheduleStorage.DesignMode;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
        string ISite.Name
		{
			get
			{
				return null;
			}
			set
			{
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceType"></param>
		/// <returns></returns>
		object IServiceProvider.GetService(Type serviceType)
		{
			return null;
		}
		#endregion
#endif
		private void ReFillObjects()
		{
#if SILVERLIGHT
			if ( ! DesignerProperties.GetIsInDesignMode(new System.Windows.Controls.Border()))
#else
			if (!DesignMode && ((ISupportInitializeNotification)this).IsInitialized)
#endif
            {
                // refill Objects collection
                Objects.Clear();
                _mappings.Reset();
                for (int i = 0; i < List.Count; i++)
                {
                    object obj = List[i];
                    ItemsMapTableRow mapRow = ItemsMap.GetMapRowAt(i);
                    T newObj = _mappings.NewItem();
                    SetCollectionItemBaseItem(newObj, obj, mapRow);
                    _mappings.ReadObject(newObj, obj, true);
                    Objects.Add(newObj);
                }
            }
		}

		/// <summary>
		/// Translates changes in the internal list to the Objects collection.
		/// </summary>
		/// <param name="e">A System.ComponentModel.ListChangedEventArgs object that contains the event data.</param>
		protected override void OnListChanged(ListChangedEventArgs e)
		{
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                    {
                        bool duplicated;
                        ItemsMapTableRow mapRow = ItemsMap.UpdateByListChange(e, List, out duplicated);
                        if (mapRow != null)
                        {
                            ProcessChangeEvent(false, (T)mapRow.CollectionItem, mapRow,
                                duplicated ? SyncBatchInfo.ActionEnum.Change :
                                    SyncBatchInfo.ActionEnum.Add);
                        }
                    }
                    break;
                case ListChangedType.ItemChanged:
                    {
                        ItemsMapTableRow mapRow = ItemsMap.UpdateByListChange(e, List);
                        if (mapRow != null)
                            ProcessChangeEvent(false, (T)mapRow.CollectionItem, mapRow,
                                SyncBatchInfo.ActionEnum.Change);
                    }
                    break;
                case ListChangedType.Reset:
                    ItemsMap.SetSourceItems(List);
                    ProcessChangeEvent(false, null, null, SyncBatchInfo.ActionEnum.Reset);
                    break;
                case ListChangedType.ItemDeleted:
                    {
                        ItemsMapTableRow mapRow = ItemsMap.UpdateByListChange(e, List);
                        if (mapRow != null)
                            ProcessChangeEvent(false, (T)mapRow.CollectionItem, mapRow,
                                SyncBatchInfo.ActionEnum.Delete);
                    }
                    break;
                case ListChangedType.ItemMoved:
                    ItemsMap.UpdateByListChange(e, List);
                    break;
            }
            
            base.OnListChanged(e);
#if (false)
			if (BoundMode)
			{
				switch (e.ListChangedType)
				{
					case ListChangedType.ItemAdded:
						// add new object to Objects collection and fill 
						// it from just added bound object
						if (Objects.Count < this.Count)
						{
							T newObj = _mappings.NewItem();
							_mappings.ReadObject(newObj, this[e.NewIndex], true);
							Objects.Add(newObj);
							if (newObj.Index == -1)
							{
								newObj.SetIndex(e.NewIndex);
							}
                            //this.MovePrevious(); //alexbind
						}
		/*				else
						{
							// just reread object
							_mappings.ReadObject(Objects[e.NewIndex], this[e.NewIndex], true);
						}*/
						break;
					case ListChangedType.ItemChanged:
						// check index
						if ( e.NewIndex < 0 
							|| e.NewIndex >= Count 
							|| !Objects.Contains(e.NewIndex) )
						{
							return;
						}
						// read object
						_mappings.ReadObject(Objects[e.NewIndex], this[e.NewIndex], false);
						break;
					case ListChangedType.Reset:
						ReFillObjects();
						break;
					case ListChangedType.ItemDeleted:
						// check index
						if (e.NewIndex < 0
							|| e.NewIndex >= Count
							|| !Objects.Contains(e.NewIndex))
						{
							return;
						}
						// if there is no mapped index - reset DataSource to
						// keep internal index in an actual state
						if (!Mappings.IdMapping.IsMapped && !Mappings.IndexMapping.IsMapped)
						{
							OnDataSourceChanged(null);
						}
						else
						{
							// be sure to remove corresponding object
							if (Objects.Count > List.Count)
							{
								Objects.RemoveAt(e.NewIndex);
							}
						}
						break;
					default:
						break;
				}
			}
			base.OnListChanged(e);
#endif
		}

		/// <summary>
		/// Removes object from the Objects collection.
		/// </summary>
		/// <param name="value">The <see cref="System.Object"/> to remove.</param>
		public override void Remove(object value)
		{
            base.Remove(value);
		}
		/// <summary>
		/// This property is for internal use only.
		/// </summary>
		protected internal override string[] DataKeyNamesInternal
        {
            get
            {   //TBD: optimize key storage
                if (!string.IsNullOrEmpty(Mappings.IdMapping.MappingName))
                    return new string[] { Mappings.IdMapping.MappingName };
                else if (!string.IsNullOrEmpty(Mappings.IndexMapping.MappingName))
                    return new string[] { Mappings.IndexMapping.MappingName };
                else
                    return new string[0];
            }
            set { }
		}
		#endregion

        #region Data Synchronization
        internal event DataSourceItemSynchronizedEventHandler DataSourceItemSynchronized;
		/// <summary>
		/// Raises the <see cref="DataSourceItemSynchronized"/> event.
		/// </summary>
		/// <param name="e">The <see cref="C1.C1Schedule.ItemSynchronizedEventArgs"/> 
		/// that contains the event data.</param>
        protected virtual void OnDataSourceItemSynchronized(ItemSynchronizedEventArgs e)
        {
            if (DataSourceItemSynchronized != null)
                DataSourceItemSynchronized(this, e);
        }
 
		private SyncBatchInfo SyncBatch
        {
            get { return _syncBatch; }
        }

        private SourceToCollectionItemsMap ItemsMap
        {
            get { return _itemsMap; }
        }

        private void ProcessChangeEvent(bool initiatorIsCollection,
            T collectionItem, ItemsMapTableRow mapRow,
            SyncBatchInfo.ActionEnum action)
        {
            //TBD: Reset
            if (action == SyncBatchInfo.ActionEnum.Reset)
            {
                SyncBatch.NotifyReset();
                SyncBatch.StartDeferredState();
                SyncBatch.ResetProcessingStarted();
                ReFillObjects();
                SyncBatch.ResetProcessingFinished();
                if (SyncBatch.FinishDeferredState())
                    ProcessSyncBatch();
                return;
            }
            object sourceItem = null;
            if (initiatorIsCollection)
            {
                if (collectionItem == null || !IsSynchronizable(collectionItem))
                    return;
                sourceItem = GetSourceItem(collectionItem.BaseSourceItemMap);
                if ((action == SyncBatchInfo.ActionEnum.Change ||
                    action == SyncBatchInfo.ActionEnum.Delete) &&
                    sourceItem == null)
                {
                    return;
                }
                if (action == SyncBatchInfo.ActionEnum.Change &&
                    !Objects.Contains(collectionItem))
                {
                    return;
                }
            }
            else
            {
                if (mapRow == null)
                    return;
                sourceItem = GetSourceItem(mapRow);
                collectionItem = (T)mapRow.CollectionItem;
            }
            SyncBatchInfo.SyncActionItem actionItem = initiatorIsCollection ?
                SyncBatch.GetActionItemForCollectionItem(collectionItem) :
                SyncBatch.GetActionItemForSourceItemMap(mapRow);
            bool performAction = false;
            if (actionItem != null)
            {
                if (actionItem.InitiatorIsCollection == initiatorIsCollection)
                {
                    switch (actionItem.ProcessingState)
                    {
                        case SyncBatchInfo.ProcessingStateEnum.Finished:
                            performAction = true;
                            actionItem.ProcessingState =
                                SyncBatchInfo.ProcessingStateEnum.Pending;
                            break;
                        case SyncBatchInfo.ProcessingStateEnum.Processing:
                            actionItem.ProcessingState =
                                SyncBatchInfo.ProcessingStateEnum.Pending;
                            break;
                        case SyncBatchInfo.ProcessingStateEnum.Pending:
                            performAction = true;
                            break;
                    }
                }
                else
                {
                    switch (actionItem.ProcessingState)
                    {
                        case SyncBatchInfo.ProcessingStateEnum.Finished:
                            performAction = true;
                            actionItem.ProcessingState =
                                SyncBatchInfo.ProcessingStateEnum.Pending;
                            actionItem.InitiatorIsCollection = initiatorIsCollection;
                            break;
                    }
                }
                actionItem.Action = GetComposedAction(actionItem.Action, action);
            }
            else //actionItem == null
            {
                if (initiatorIsCollection)
                {
                    actionItem = SyncBatch.AddCollectionAction(action, collectionItem,
                        mapRow);
                }
                else
                {
                    actionItem = SyncBatch.AddSourceAction(action, mapRow, 
                        collectionItem);
                }
                performAction = true;
            }
            if (!SyncBatch.IsDeferred && performAction)
            {
                actionItem.ProcessingState =
                    SyncBatchInfo.ProcessingStateEnum.Processing;
                if (initiatorIsCollection)
                {
                    switch (actionItem.Action)
                    {
                        case SyncBatchInfo.ActionEnum.Add:
                            {
                                if (!AllowNew || sourceItem != null)
                                    break;
                                SyncBatch.StartDeferredState();
                                try
                                {
                                    sourceItem = AddNew();
                                    SetCollectionItemBaseItem(collectionItem, sourceItem, null);
                                    if (sourceItem != null && SyncBatch.ResetState ==
                                        SyncBatchInfo.ProcessingStateEnum.Finished &&
                                        collectionItem.BaseSourceItemMap != null)
                                    {
                                        SyncBatch.NotifyCorrespondence(collectionItem,
                                            collectionItem.BaseSourceItemMap);
                                    }
                                    if (AllowNew) 
                                        _mappings.WriteObject(collectionItem, sourceItem);
                                    FinishAddNew();
                                    //TBD: update back
                                }
                                finally
                                {
                                    if (SyncBatch.FinishDeferredState())
                                        ProcessSyncBatch();
                                }
                            }
                            break;
                        case SyncBatchInfo.ActionEnum.Change:
                            {
                                if (AllowEdit)
                                {
                                    BeginEdit(sourceItem);
                                    try
                                    {
                                        _mappings.WriteObject(collectionItem,
                                            sourceItem);
                                    }
                                    finally
                                    {
                                        EndEdit(sourceItem);
                                    }
                                    //TBD: update back
                                }
                            }
                            break;
                        case SyncBatchInfo.ActionEnum.Delete:
                            {
                                if (AllowRemove && sourceItem != null)
                                {
                                    SetCollectionItemBaseItem(collectionItem, null, null);
                                    Remove(sourceItem);
                                }
                            }
                            break;
                    }
                }
                else // initiator is source
                {
                    switch (actionItem.Action)
                    {
                        case SyncBatchInfo.ActionEnum.Add:
                            {
                                if (collectionItem == null)
                                {
                                    T newCollItem = _mappings.NewItem();
                                    SetCollectionItemBaseItem(newCollItem, sourceItem, mapRow);
                                    actionItem.CollectionItem = newCollItem;
                                    _mappings.ReadObject(newCollItem, sourceItem, false);
                                    Objects.Add(newCollItem);
                                    //TBD: re-fetch
                                }
                            }
                            break;
                        case SyncBatchInfo.ActionEnum.Change:
                            if (collectionItem != null && Contains(sourceItem) &&
                                Objects.Contains(collectionItem))
                            {
								bool dirty = _mappings.ReadObject(collectionItem, sourceItem, true);
								if (dirty)
								{
									Appointment app = collectionItem as Appointment;
									if (app != null)
									{
										app.BeginEdit();
										app.SetDirty();
										app.EndEdit();
									}
								}
                                //TBD: re-fetch
                            }
                            break;
                        case SyncBatchInfo.ActionEnum.Delete:
                            if (collectionItem != null)
                            {
                                int idx = Objects.IndexOf(collectionItem);
                                if (idx >= 0)
                                    Objects.RemoveAt(idx);
                            }
                            break;
                    }
                }
                if (actionItem.ProcessingState ==
                    SyncBatchInfo.ProcessingStateEnum.Processing)
                {
                    //TBD: what about Pending
                    actionItem.ProcessingState =
                        SyncBatchInfo.ProcessingStateEnum.Finished;
                    SyncBatch.RemoveActionItem(actionItem);
                    if (actionItem.InitiatorIsCollection && sourceItem != null)
                    {
                        SynchronizationActionEnum? eventAction = 
                            GetSyncEventAction(actionItem.Action);
                        if (eventAction.HasValue)
                        {
                            OnDataSourceItemSynchronized(
                                new ItemSynchronizedEventArgs(this, actionItem.CollectionItem,
                                sourceItem, eventAction.Value));
                        }
                    }
                }

            }
        }

        private void ProcessSyncBatch()
        {
            //TBD: Reset
            bool somethingProcessed = true;
            while (somethingProcessed && !SyncBatch.IsDeferred)
            {
                somethingProcessed = false;
                for (int i = 0; i < SyncBatch.Count && !SyncBatch.IsDeferred && 
                    !somethingProcessed; i++)
                {
                    SyncBatchInfo.SyncActionItem actionItem = SyncBatch.GetItemAt(i);
                    if (actionItem.ProcessingState == SyncBatchInfo.ProcessingStateEnum.Pending)
                    {
                        ProcessChangeEvent(actionItem.InitiatorIsCollection,
                            (T)actionItem.CollectionItem, actionItem.SourceItemMap,
                            actionItem.Action);
                        somethingProcessed = true;
                    }
                }
            }
        }

        private static SyncBatchInfo.ActionEnum GetComposedAction(
            SyncBatchInfo.ActionEnum currentAction, SyncBatchInfo.ActionEnum newAction)
        {
            SyncBatchInfo.ActionEnum ret = currentAction;
            if (currentAction == SyncBatchInfo.ActionEnum.None)
                return newAction;
            else if (newAction == SyncBatchInfo.ActionEnum.None)
                return currentAction;
            else
            {
                switch (newAction)
                {
                    case SyncBatchInfo.ActionEnum.Delete:
                        ret = SyncBatchInfo.ActionEnum.Delete;
                        break;
                    case SyncBatchInfo.ActionEnum.Change:
                        ret = currentAction;
                        break;
                    case SyncBatchInfo.ActionEnum.Add:
                        if (currentAction == SyncBatchInfo.ActionEnum.Delete)
                            ret = SyncBatchInfo.ActionEnum.Add;
                        else
                            ret = currentAction;
                        break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Updates ItemsMap and BasePersistableObject.BaseSourceItemMap.
        /// <paramref name="mapRow"/> should be specified if it's known (for 
        /// performance purposes), if it's null then the one will be determined
        /// by <paramref name="sourceItem"/>
        /// </summary>
        /// <param name="collectionItem"></param>
        /// <param name="sourceItem"></param>
		/// <param name="mapRow"></param>
		private void SetCollectionItemBaseItem(BasePersistableObject collectionItem,
            object sourceItem, ItemsMapTableRow mapRow)
        {
            if (mapRow == null && sourceItem != null)
            {
				int idx = IndexOf(sourceItem);
                if (idx >= 0)
                    mapRow = ItemsMap.GetMapRowAt(idx);
            }
            if (collectionItem != null)
                collectionItem.SetBaseSourceItemMap(mapRow);
            if (mapRow != null)
                mapRow.CollectionItem = collectionItem;
        }

        private object GetSourceItem(ItemsMapTableRow mapRow)
        {
            if (mapRow == null)
                return null;
            int idx = mapRow.Index;
            if (idx < 0 || idx >= List.Count)
                return null;
            else
                return List[idx];
        }

        private static SynchronizationActionEnum? GetSyncEventAction(SyncBatchInfo.ActionEnum action)
        {
            SynchronizationActionEnum? syncAction = null;
            switch (action)
            {
                case SyncBatchInfo.ActionEnum.Add:
                    syncAction = SynchronizationActionEnum.Added;
                    break;
                case SyncBatchInfo.ActionEnum.Delete:
                    syncAction = SynchronizationActionEnum.Deleted;
                    break;
                case SyncBatchInfo.ActionEnum.Change:
                    syncAction = SynchronizationActionEnum.Changed;
                    break;
            }
            return syncAction;
        }

        /// <summary>
        /// Indicates whether the specified collection item should be synchronized.
        /// </summary>
		/// <param name="collectionItem">The <see cref="BasePersistableObject"/> object.</param>
		/// <returns>True if item should be synchronized; false, otherwise.</returns>
        protected virtual bool IsSynchronizable(BasePersistableObject collectionItem)
        {
            return collectionItem != null;
        }
        #endregion
    }

    #region Data Synchronization
	/// <summary>
	/// Describes the version of data in a data storage. 
	/// </summary>
    public enum SynchronizationActionEnum
    {
		/// <summary>
		/// A new item.
		/// </summary>
        Added,
		/// <summary>
		/// A deleted item.
		/// </summary>
        Deleted,
		/// <summary>
		/// A changed item.
		/// </summary>
        Changed
    }

	/// <summary>
	/// Provides data for the DataSourceItemSynchronized event.
	/// </summary>
    public class ItemSynchronizedEventArgs : EventArgs
    {
        private readonly object _dataSourceItem;
        private readonly SynchronizationActionEnum _action;
        private readonly C1BindingSource _storage;
        private readonly BasePersistableObject _collectionItem;

		/// <summary>
		/// Initializes a new instance of the <see cref="ItemSynchronizedEventArgs"/> class.
		/// </summary>
		/// <param name="storage">Reference to the changed <see cref="C1BindingSource"/> object.</param>
		/// <param name="collectionItem">Reference to the <see cref="BasePersistableObject"/> derived object.</param>
		/// <param name="dataSourceItem">Reference to the corresponding item in the data source.</param>
		/// <param name="action">The <see cref="SynchronizationActionEnum"/> value.</param>
        public ItemSynchronizedEventArgs(C1BindingSource storage, 
            BasePersistableObject collectionItem, object dataSourceItem, 
            SynchronizationActionEnum action)
        {
            _storage = storage;
            _collectionItem = collectionItem;
            _dataSourceItem = dataSourceItem;
            _action = action;
        }

		/// <summary>
		/// Gets the reference to the changed <see cref="C1BindingSource"/> object.
		/// </summary>
        public C1BindingSource Storage
        {
            get { return _storage; }
        }
        
		/// <summary>
		/// Gets the reference to the data source item.
		/// </summary>
        public object DataSourceItem
        {
            get { return _dataSourceItem; }
        }

		/// <summary>
		/// Gets the reference to the <see cref="BasePersistableObject"/> derived object.
		/// </summary>
        public BasePersistableObject CollectionItem
        {
            get { return _collectionItem; }
        }

		/// <summary>
		/// Gets the <see cref="SynchronizationActionEnum"/> value.
		/// </summary>
        public SynchronizationActionEnum Action
        {
            get { return _action; }
        }
    }

	/// <summary>
	/// The delegate type for the event handlers of the DataSourceItemSynchronized event. 
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">The <see cref="C1.C1Schedule.ItemSynchronizedEventArgs"/> 
	/// that contains the event data.</param>
    public delegate void DataSourceItemSynchronizedEventHandler(object sender, 
        ItemSynchronizedEventArgs e);
    #endregion
}
