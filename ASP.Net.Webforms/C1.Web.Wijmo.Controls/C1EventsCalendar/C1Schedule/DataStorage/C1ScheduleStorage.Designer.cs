﻿using System;
using System.ComponentModel;
using C1.C1Schedule;

namespace C1.C1Schedule
{
	partial class C1ScheduleStorage
	{
#if (SILVERLIGHT)
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		internal void Dispose()
		{
			if (!DesignMode)
			{
				_actions.Dispose();
				_reminders.Dispose();
                _resourceStorage.Dispose();
                _statusStorage.Dispose();
                _contactStorage.Dispose();
                _ownerStorage.Dispose();
                _labelStorage.Dispose();
                _categoryStorage.Dispose();
                _appointmentStorage.Dispose();
            }
		}
#else
        /// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null) && !DesignMode)
			{
				#if (!ASP_NET_2 )
				#if WINFX
				// it's required in case if application has no permissions
				try
				{
				#endif
					Unsubscribe();
				#if WINFX
				}
				catch{}
				#endif
				#endif
				_actions.Dispose();
				_reminders.Dispose();
				_resourceStorage.Dispose();
				_statusStorage.Dispose();
				_contactStorage.Dispose();
                _ownerStorage.Dispose();
				_labelStorage.Dispose(); 
				_categoryStorage.Dispose();
				_appointmentStorage.Dispose();
				components.Dispose();
			}
			base.Dispose(disposing);
		}
#endif

		#if (!ASP_NET_2 && !SILVERLIGHT)
		private void Subscribe()
		{
			Microsoft.Win32.SystemEvents.TimeChanged += new EventHandler(SystemEvents_TimeChanged);
		}

		private void Unsubscribe()
		{
			Microsoft.Win32.SystemEvents.TimeChanged -= new EventHandler(SystemEvents_TimeChanged);
		}
		#endif

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
#if (!SILVERLIGHT)
			this.components = new System.ComponentModel.Container();
#endif
			this._reminders = new ReminderCollection();
			this._actions = new ActionCollection();
			this._info = new CalendarInfo();
			this._days = new DayCollection(_info);
			this._resourceStorage = new ResourceStorage(this); 
            this._statusStorage = new StatusStorage(this); 
            this._contactStorage = new ContactStorage(this);
            this._ownerStorage = new ContactStorage(this);
            this._labelStorage = new LabelStorage(this); 
            this._categoryStorage = new CategoryStorage(this); 
			this._appointmentStorage = new AppointmentStorage(this, 
											_reminders, 
											_statusStorage.Statuses,
											_labelStorage.Labels,
											_categoryStorage.Categories,
											_resourceStorage.Resources,
											_contactStorage.Contacts,
                                            _ownerStorage.Contacts,
                                            _info, _actions);
            this._days._parentCollection = this._appointmentStorage.Appointments;
		}
		#endregion

		private AppointmentStorage _appointmentStorage;
		private ResourceStorage _resourceStorage;
		private StatusStorage _statusStorage;
		private LabelStorage _labelStorage;
		private CategoryStorage _categoryStorage;
		private ContactStorage _contactStorage;
        private ContactStorage _ownerStorage;
        private ActionCollection _actions;
		private ReminderCollection _reminders;
		private CalendarInfo _info;
		private DayCollection _days;

	}
}
