﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="StatusStorage"/> is the storage for <see cref="Status"/> objects.
	/// It allows binding to the data source and mapping data sources fields 
	/// to the status properties.
	/// </summary>
	public class StatusStorage : BaseStorage<Status, BaseObjectMappingCollection<Status>>
	{
		#region ctor
		internal StatusStorage(C1ScheduleStorage scheduleStorage): base(scheduleStorage) 
		{
			Objects = new StatusCollection(this);
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="StatusCollection"/> object 
		/// that contains status related information. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true)]
#endif
		//[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Storage.Statuses", "The collection of statuses.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		[Browsable(false)]
#endif
		public StatusCollection Statuses
		{
			get
			{
				return (StatusCollection)Objects;
			}
		}
		#endregion
	}
}
