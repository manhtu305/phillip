﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
// ToDo: 
// maybe change Action to collection of actions
// Attachments
// BillingInformation?
// Companies?
// IsConflict?
// UserProperties?

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="AppointmentStorage"/> is the storage 
	/// for <see cref="Appointment"/> objects. It allows binding to the data source 
	/// and mapping data source fields to the appointment properties.
	/// </summary>
	public class AppointmentStorage : BaseStorage<Appointment, AppointmentMappingCollection>
	{
		#region ctor
		internal AppointmentStorage(C1ScheduleStorage scheduleStorage, 
			ReminderCollection reminders,
			StatusCollection statuses,
			LabelCollection labels,
			CategoryCollection categories,
			ResourceCollection resources,
			ContactCollection contacts,
            ContactCollection owners,
            CalendarInfo info, ActionCollection actions)
            : base(scheduleStorage)

		{
			Objects = new AppointmentCollection(this, reminders, statuses,
								labels, categories, resources, contacts, owners, info, actions);
			((AppointmentMappingCollection)Mappings).Collection = (AppointmentCollection)Objects;
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets an <see cref="AppointmentCollection"/> object that contains appointment related information. 
		/// </summary>
#if (!SILVERLIGHT)
		[ParenthesizePropertyName(true)]
#endif
		[C1Description("Storage.Appointments", "The collection of appointments.")]
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		[Browsable(false)]
#endif
		public AppointmentCollection Appointments
		{
			get
			{
				return (AppointmentCollection)Objects;
			}
		}
		#endregion
	}
}

