using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.C1Schedule.Printing
{
	/// <summary>
	/// The <see cref="PrintContextType"/> defines the printing context for the <see cref="PrintStyle"/> object.
	/// </summary>
	[Flags]
	public enum PrintContextType
	{
		/// <summary>
		/// Document displays content of the <see cref="C1.C1Schedule.Appointment"/> object.
		/// </summary>
		Appointment = 0x01,
		/// <summary>
		/// Document displays appointments for the specified date range.
		/// </summary>
		DateRange = 0x02
	}
}
