using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

#if !WINFX
using System.Drawing.Printing;
using System.Windows.Forms;

using C1.Win.C1Schedule;
using C1.Win.C1Schedule.Printing;

#else
using C1.WPF.C1Schedule;
#endif

#if END_USER_LOCALIZATION
using C1.Win.C1Schedule.Localization;
#else
	using C1.Util.Localization;
#endif

namespace C1.C1Schedule.Printing
{
	using C1.C1Schedule;

	/// <summary>
	/// The object used to manage schedule printing.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class PrintInfo 
	{
		#region ** fields
#if WINFX
		internal C1.WPF.C1Schedule.C1Scheduler _schedule;
#else
		internal C1.Win.C1Schedule.C1Schedule _schedule;
#endif
		private PrintDocumentWrapper _printDoc = null;
		private PreviewWrapper _preview = null;

		private PrintStyleCollection _printStyles = null;
		
		private bool _showOptionsForm = true;
		private bool _hidePrivateAppointments = false;
		private bool _selectedGroupOnly = false;

		private PrintStyle _currentStyle = null;
		private List<Appointment> _currentAppointments = null;

		private bool _enablePrint = true;
		private bool _showProgressForm = true;
		#endregion

		#region ** events
		private void OnBeforeOptionsFormShow(CancelEventArgs args)
		{
			_schedule.OnBeforeOptionsFormShow(this, args);
		}
		private void OnBeforePrint(CancelEventArgs args)
		{
			_schedule.OnBeforePrint(this, args);
		}
		private void OnBeforePrintPreview(CancelEventArgs args)
		{
			_schedule.OnBeforePrintPreview(this, args);
		}
		#endregion

		#region ** ctor
		internal PrintInfo()
		{
			_printDoc = new PrintDocumentWrapper(this);
			_printStyles = new PrintStyleCollection();
			_printStyles.LoadDefaults();
			_printDoc.DocumentStarting += new EventHandler(_printDoc_DocumentStarting);
			_preview = new PreviewWrapper(this);
		}

#if WINFX
		internal PrintInfo(C1.WPF.C1Schedule.C1Scheduler schedule)
#else
		internal PrintInfo(C1.Win.C1Schedule.C1Schedule schedule)
#endif
		{
			_schedule = schedule;
			_printDoc = new PrintDocumentWrapper(this);
			_printStyles = new PrintStyleCollection();
			_printStyles.LoadDefaults();
			_printDoc.DocumentStarting += new EventHandler(_printDoc_DocumentStarting);
			_preview = new PreviewWrapper(this);
		}
		#endregion

		#region ** public properties
		/// <summary>
		/// Gets or sets a <see cref="Boolean"/> value determining whether printing is enabled.
		/// </summary>
		/// <remarks> At run-time this property returns false if
		/// C1PrintDocument control can't be initialized.</remarks>
		[DefaultValue(true)]
		[C1Description("Printing.IsPrintingEnabled", "Determines whether print operations are enabled.")]
		public bool IsPrintingEnabled
		{
			get
			{
				bool result = _enablePrint;
				if (!_schedule.IsDesignMode)
				{
					result = result && (_printDoc.C1PrintDocument != null);
				}
				return result;
			}
			set
			{
				_enablePrint = value;
			}
		}

		/// <summary>
		/// Gets a <see cref="Boolean"/> value determining whether preview is enabled.
		/// </summary>
		[Browsable(false)]
		public bool IsPreviewEnabled 
		{ 
			get 
			{
				return IsPrintingEnabled && (_preview.C1PrintPreviewDialog != null);
			} 
		}


		/// <summary>
		/// Gets the <see cref="PrintStyleCollection"/> collection, containing all available styles of printing.
		/// </summary>
		[ParenthesizePropertyName(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Printing.PrintStyles", "The collection of printing styles.")]
		public PrintStyleCollection PrintStyles
		{
			get
			{
				return _printStyles;
			}
		}

		/// <summary>
		/// Gets the <see cref="PrintDocumentWrapper"/> object.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Printing.PrintDocumentWrapper", "The C1PrintDocument wrapper.")]
		public PrintDocumentWrapper PrintDocumentHelper
		{
			get
			{
				return _printDoc;
			}
		}

		/// <summary>
		/// Gets the <see cref="PreviewWrapper"/> object.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Printing.PreviewWrapper", "The preview wrapper.")]
		public PreviewWrapper PreviewHelper
		{
			get
			{
				return _preview;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Boolean"/> value determining whether control should
		/// show options form before printing.
		/// </summary>
		[DefaultValue(true)]
		[C1Description("Printing.ShowOptionsForm", "Determines whether the Options form appears.")]
		public bool ShowOptionsForm
		{
			get
			{
#if WINFX
				return false;
#else
				return _showOptionsForm;
#endif
			}
			set
			{
				_showOptionsForm = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Progress Form is displayed.
		/// </summary>
		[DefaultValue(true)]
		[C1Description("Printing.ShowProgressForm", "Determines whether the Progress Form appears.")]
		public bool ShowProgressForm
		{
			get
			{
				return _showProgressForm;
			}
			set
			{
				_showProgressForm = value;
			}
		}

		/// <summary>
		/// Gets a <see cref="PrintStyle"/> object which is currently selected for printing.
		/// </summary>
		[Browsable(false)]
		public PrintStyle CurrentStyle
		{
			get
			{
				return _currentStyle;
			}
			internal set
			{
				if (_currentStyle != value)
				{
					if (_currentStyle != null)
					{
						_currentStyle.IsLoaded = false;
					}
					_currentStyle = value;
					HidePrivateAppointments = HidePrivateAppointments || _currentStyle.HidePrivateAppointments;
				}
                if (_currentStyle != null)
                {
                    _currentStyle.CalendarInfo = _schedule.CalendarInfo;
                }
            }
		}

		/// <summary>
		/// Gets or sets a <see cref="Boolean"/> value determining whether control should
		/// hide details of private appointments in the resulting document.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("Printing.HidePrivateAppointments", "Determines whether the private appointments should be printed.")]
		public bool HidePrivateAppointments
		{
			get
			{
				return _hidePrivateAppointments;
			}
			set
			{
				_hidePrivateAppointments = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Boolean"/> value determining whether control should only
		/// show appointments from the currently selected <see cref="ScheduleGroupItem"/> in the resulting document.
		/// By default resulting document contains all appointments regardless of the C1Schedule grouping.
		/// Set this property to true to show appointments from the currently selected <see cref="ScheduleGroupItem"/>.
		/// </summary>
		/// <remarks>This property only makes sense if the C1Schedule.GroupBy property is set.</remarks>
		[DefaultValue(false)]
		[C1Description("Printing.PrintSelectedCalendar", "Determines whether selected ScheduleGroupItem appointments should be printed.")]
		public bool PrintSelectedCalendar
		{
			get
			{
				return _selectedGroupOnly;
			}
			set
			{
				_selectedGroupOnly = value;
			}
		}
		#endregion

		#region ** public methods
		/// <summary>
		/// Sends default printing style for the currently selected view to the printer.
		/// </summary>
		public void Print()
		{
			if (!IsPrintingEnabled)
			{
				return;
			}
			if (SetupPrintContext(null))
			{
				InternalPrint();
			}
		}

		/// <summary>
		/// Sends the specified printing style to the printer.
		/// </summary>
		/// <param name="style">The <see cref="PrintStyle"/> object to print.</param>
		public void Print(PrintStyle style)
		{
			if (!IsPrintingEnabled)
			{
				return;
			}
#if !WINFX
			Print(style, _schedule.SelectedDates[0], _schedule.SelectedDates[_schedule.SelectedDates.Length - 1]);
#else
			Print(style, _schedule.VisibleDates[0], _schedule.VisibleDates[_schedule.VisibleDates.Count - 1]);
#endif
		}

		/// <summary>
		/// Sends the specified <see cref="Appointment"/> object to the printer.
		/// </summary>
		/// <param name="appointment">The <see cref="Appointment"/> object to print.</param>
		public void Print(Appointment appointment)
		{
			if (!IsPrintingEnabled)
			{
				return;
			}
			if (SetupPrintContext(appointment))
			{
				InternalPrint();
			}
		}

		/// <summary>
		/// Sends the specified printing style to the printer.
		/// </summary>
		/// <param name="style">The <see cref="PrintStyle"/> object to print.</param>
		/// <param name="start">The <see cref="DateTime"/> value specifying the beginning of the print range.</param>
		/// <param name="end">The <see cref="DateTime"/> value specifying the end of the print range.</param>
		public void Print(PrintStyle style, DateTime start, DateTime end)
		{
			if (!IsPrintingEnabled)
			{
				return;
			}
			if (style != null)
			{
				SetupPrintContext(style, start, end);
				InternalPrint();
			}
		}

		/// <summary>
		/// Opens a separate application window in which end users can preview 
		/// the output that would be generated by the print operation.
		/// </summary>
		public void Preview()
		{
			if (!IsPreviewEnabled)
			{
				return;
			}
			if (SetupPrintContext(null))
			{
				PreviewCurrentStyle();
			}
		}

		/// <summary>
		/// Opens a separate application window in which end users can preview 
		/// the output that would be generated by the print operation.
		/// </summary>
		/// <param name="style">The <see cref="PrintStyle"/> object to preview.</param>
		public void Preview(PrintStyle style)
		{
			if (!IsPreviewEnabled)
			{
				return;
			}
#if !WINFX
			Preview(style, _schedule.SelectedDates[0], _schedule.SelectedDates[_schedule.SelectedDates.Length - 1]);
#else
			Preview(style, _schedule.VisibleDates[0], _schedule.VisibleDates[_schedule.VisibleDates.Count - 1]);
#endif
		}

		/// <summary>
		/// Opens a separate application window in which end users can preview 
		/// the output that would be generated by the print operation.
		/// </summary>
		/// <param name="appointment">The <see cref="Appointment"/> object to preview.</param>
		public void Preview(Appointment appointment)
		{
			if (!IsPreviewEnabled)
			{
				return;
			}
			if (SetupPrintContext(appointment))
			{
				PreviewCurrentStyle();
			}
		}

		/// <summary>
		/// Opens a separate application window in which end users can preview 
		/// the output that would be generated by the print operation.
		/// </summary>
		/// <param name="style">The <see cref="PrintStyle"/> object to preview.</param>
		/// <param name="start">The <see cref="DateTime"/> value specifying the beginning of the print range.</param>
		/// <param name="end">The <see cref="DateTime"/> value specifying the end of the print range.</param>
		public void Preview(PrintStyle style, DateTime start, DateTime end)
		{
			if (!IsPreviewEnabled)
			{
				return;
			}
			if (style != null)
			{
				SetupPrintContext(style, start, end);
				PreviewCurrentStyle();
			}
		}

		/// <summary>
		/// Loads style definition to C1PrintDocument control.
		/// </summary>
		/// <param name="style">The <see cref="PrintStyle"/> for loading into print document object.</param>
		public void LoadStyle(PrintStyle style)
		{
			if (!IsPrintingEnabled)
			{
				return;
			}
			if (style.Load(_printDoc))
			{
				CurrentStyle = style;
			}
		}
		#endregion

		#region ** private stuff
		/// <summary>
		/// Shows a dialog window for editing the specified <see cref="PrintStyle"/> object.
		/// </summary>
		/// <param name="style">The <see cref="PrintStyle"/> object to edit.</param>
		/// <returns>Returns true if user pressed Ok in a Print Options dialog;
		/// false otherwise.</returns>
		internal bool EditPrintStyle(PrintStyle style)
		{
#if !WINFX
			PrintStyleOptionsForm form = new PrintStyleOptionsForm(style, _schedule.CalendarInfo.CultureInfo);
			if (form.ShowDialog() == DialogResult.OK)
			{
				return true;
			}
			return false;
#else
			return true;
#endif
		}

		/// <summary>
		/// Shows a dialog window for selecting printing style and setting it's properties.
		/// </summary>
		/// <returns>Returns true if user pressed Ok in a Print Options dialog;
		/// false otherwise.</returns>
		internal bool EditPrintStyle()
		{
#if !WINFX
			PrintContextType context = PrintContextType.DateRange;
			if (_schedule.SelectedAppointments.Count > 0)
			{
				context |= PrintContextType.Appointment;
			}
			CancelEventArgs args = new CancelEventArgs();
			OnBeforeOptionsFormShow(args);
			if (args.Cancel)
			{
				return true;
			}

			PrintOptionsForm form = new PrintOptionsForm(this, CurrentStyle, context);
			form.StartDate = _schedule.SelectedDates[0];
			form.EndDate = _schedule.SelectedDates[_schedule.SelectedDates.Length - 1];
			if (form.ShowDialog() == DialogResult.OK)
			{
				CurrentStyle = form.SelectedStyle;
				_hidePrivateAppointments = form.HidePrivateAppointments;
				_selectedGroupOnly = form.PrintSelectedCalendar;
				SetupPrintContext(CurrentStyle, form.StartDate, form.EndDate);
				return true;
			}
			return false;
#else
			return true;
#endif
		}

#if WINFX
		internal C1.WPF.C1Schedule.C1Scheduler Owner
#else
		internal C1.Win.C1Schedule.C1Schedule Owner
#endif
		{
			get
			{
				return _schedule;
			}
		}

		private void InternalPrint()
		{
			CancelEventArgs args = new CancelEventArgs();
			OnBeforePrint(args);
			if (args.Cancel)
			{
				return;
			}
			_printDoc.Print();
		}

		private bool SetupPrintContext(Appointment appointment)
		{
            // clear old value first
            _currentAppointments = null;
            bool showOptionsForm = _showOptionsForm;
			if (appointment != null)
			{
				_currentAppointments = new List<Appointment>();
				_currentAppointments.Add(appointment);
				showOptionsForm = false;
			}
			else
			{
#if !WINFX
				_currentAppointments = new List<Appointment>(_schedule.SelectedAppointments);
#else
				if ( _schedule.SelectedAppointment != null )
				{
					_currentAppointments = new List<Appointment>();
					_currentAppointments.Add(_schedule.SelectedAppointment);
				}
#endif
			}
			if (_printStyles.Contains("Memo") && 
				( _currentAppointments != null && _currentAppointments.Count > 0))
			{
				CurrentStyle = _printStyles["Memo"];
			}
			else
			{
#if !WINFX
				int daysNumber = _schedule.SelectedDates.Length;
#else
				int daysNumber = _schedule.VisibleDates.Count;
#endif
				if (daysNumber > 14 && _printStyles.Contains("Month"))
				{
					CurrentStyle = _printStyles["Month"];
				}
				else if (daysNumber == 7 && _printStyles.Contains("Week"))
				{
					CurrentStyle = _printStyles["Week"];
				}
				else if (_printStyles.Contains("Daily"))
				{
					CurrentStyle = _printStyles["Daily"];
				}
				else if (_printStyles.Contains("Details"))
				{
					CurrentStyle = _printStyles["Details"];
				}
				else
				{
					CurrentStyle = _printStyles[0];
				}
			}
			if (showOptionsForm)
			{
				if (!EditPrintStyle())
				{
					return false;
				}
			}
			else
			{
				if (CurrentStyle == null)
				{
					return false;
				}
#if !WINFX
				SetupPrintContext(CurrentStyle, _schedule.SelectedDates[0], _schedule.SelectedDates[_schedule.SelectedDates.Length - 1]);
#else
				SetupPrintContext(CurrentStyle, _schedule.VisibleDates[0], _schedule.VisibleDates[_schedule.VisibleDates.Count - 1]);
#endif
			}
			return true;
		}

		void _printDoc_DocumentStarting(object sender, EventArgs e)
		{
            if (_currentStyle == null)
            {
                return;
            }
#if WINFX
			DateTime start = _schedule.VisibleDates[0].Date;
			DateTime end = _schedule.VisibleDates[_schedule.VisibleDates.Count - 1].Date;
			CalendarInfo calendarInfo = _schedule.CalendarHelper.Info;
#else
			DateTime start = _schedule.SelectedDates[0].Date;
			DateTime end = _schedule.SelectedDates[_schedule.SelectedDates.Length - 1].Date;
			CalendarInfo calendarInfo = _schedule.CalendarInfo;
#endif

			_currentStyle.SetupTags(_schedule.DataStorage.AppointmentStorage.Appointments,
				_currentAppointments, start, end, HidePrivateAppointments, calendarInfo, 
				_selectedGroupOnly, 
				(_schedule.SelectedGroupItem == null ? null : _schedule.SelectedGroupItem.Owner),
				_schedule.GroupBy, _schedule.AppointmentComparison);

			_schedule.OnPrintDocumentStarting(sender, e);
		}

		internal void SetupPrintContext(PrintStyle style, DateTime start, DateTime end)
		{
			LoadStyle(style);
			style.SetDateRangeTags(start, end);
		}

		private void PreviewCurrentStyle()
		{
			CancelEventArgs args = new CancelEventArgs();
			OnBeforePrintPreview(args);
			if (args.Cancel)
			{
				return;
			}

#if WINFX
			_preview.Preview(_printDoc);
#else
			PageSettings ps = CurrentStyle.CurrentPageSettings;
			_preview.Preview(_printDoc, ref ps);
#endif
		}
		#endregion
	}
}
