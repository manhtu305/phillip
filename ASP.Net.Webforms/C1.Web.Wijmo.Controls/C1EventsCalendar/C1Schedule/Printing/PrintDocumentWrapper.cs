using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Collections.Specialized;

#if !WINFX
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.ComponentModel;
using C1.Win.C1Schedule.Localization;
using C1.Win.C1Schedule.Printing;
#else
using System.Windows.Documents;
#endif

namespace C1.C1Schedule.Printing
{
#if WINFX
	/// <summary>
	/// The <see cref="PrintDocumentHelper"/> class wraps the C1.C1Preview.C1PrintDocument functionality.
	/// It loads C1.C1Preview.C1PrintDocument control via reflection 
	/// from the C1.WPF.C1Report assembly.
	/// </summary>
#else
	/// <summary>
	/// The <see cref="PrintDocumentWrapper"/> class wraps the C1.C1Preview.C1PrintDocument functionality.
	/// It loads C1.C1Preview.C1PrintDocument component via reflection from the C1.C1Report.2 assembly.
	/// </summary>
#endif
	[TypeConverterAttribute(typeof(ExpandableObjectConverter))]
	public class PrintDocumentWrapper : IDisposable
	{
		#region ** fields
		internal static Type _tagType = null;
		internal static Type _tagInputParamsType = null;
		internal static Type _tagStringInputParamsType = null;
		internal static Type _tagDateTimeInputParamsType = null;
		internal static Type _tagListInputParamsType = null;
		internal static Type _tagBoolInputParamsType = null;
		internal static Type _tagNumericInputParamsType = null;
		internal static Type _tagListInputParamsItemType = null;

		private static Type _tagCollectionType = null;
		private static Assembly _previewAssembly = null;
		private static Type _documentType = null;
//		private static Type _exportProviderType = null;
//		private static Type _exportProvidersType = null;
		private static Type _documentFormatEnum = null;
		private static Type _refreshModeEnum = null;
		private static Type _c1PageSettingsType = null;
		private static Type _pageLayoutType = null;
		private static Type _documentInfoType;

		private static Type	_longOperationEventHandlerType = null;
		private static Type _longOperationEventArgsType = null;

		private static Type _scriptingOptionsType = null;

	//	private static object[] _availableProviders = null;
//		private static string _exportFilter = string.Empty;
		private static bool _failedLoad = false;

		private Component _document = null;

		private PrintInfo _printInfo;

		private Delegate _docStartingDelegate;
		private Delegate _longOperationDelegate;
		private PrintProgressForm _progressForm = null;
		private Form _mainForm = null;

		#endregion

		#region ** initialization
		internal PrintDocumentWrapper(PrintInfo printInfo)
		{
			_printInfo = printInfo;
			MethodInfo mi = GetType().GetMethod("c1PrintDocument1_DocumentStarting", BindingFlags.NonPublic | BindingFlags.Instance);
			_docStartingDelegate = Delegate.CreateDelegate(typeof(EventHandler), this, mi, false);
		}

		private bool Loaded
		{
			get
			{
				if (!_failedLoad && _previewAssembly == null)
				{
					_failedLoad = !LoadAssemblies();
				}
				return _previewAssembly != null;
			}
		}

		private bool LoadAssemblies()
		{
			if (_previewAssembly != null)
				return true;
#if (CLR40)
            string assName = "C1.C1Report.4";
#elif (WINFX)
            string assName = "C1.WPF.C1Report";
#else
            string assName = "C1.C1Report.2";
#endif
            try
			{
                _previewAssembly = TryLoadAssembly(assName, null);

				_documentType = _previewAssembly.GetType("C1.C1Preview.C1PrintDocument");
		//		_exportProviderType = _previewAssembly.GetType("C1.C1Preview.Export.ExportProvider");
		//		_exportProvidersType = _previewAssembly.GetType("C1.C1Preview.Export.ExportProviders");
				_documentFormatEnum = _previewAssembly.GetType("C1.C1Preview.C1DocumentFormatEnum");
				_refreshModeEnum = _previewAssembly.GetType("C1.C1Preview.RefreshModeEnum");
				_c1PageSettingsType = _previewAssembly.GetType("C1.C1Preview.C1PageSettings");
				_pageLayoutType = _previewAssembly.GetType("C1.C1Preview.PageLayout");
				_scriptingOptionsType = _previewAssembly.GetType("C1.C1Preview.Scripting.ScriptingOptions");
				_documentInfoType = _previewAssembly.GetType("C1.C1Preview.DocumentInfo");
				_longOperationEventHandlerType = _previewAssembly.GetType("C1.C1Preview.LongOperationEventHandler");
				_longOperationEventArgsType = _previewAssembly.GetType("C1.C1Preview.LongOperationEventArgs");
				
				_tagType = _previewAssembly.GetType("C1.C1Preview.Tag");
				_tagCollectionType = _previewAssembly.GetType("C1.C1Preview.TagCollection");
				_tagInputParamsType = _previewAssembly.GetType("C1.C1Preview.TagInputParams");
				_tagStringInputParamsType = _previewAssembly.GetType("C1.C1Preview.TagStringInputParams");
				_tagDateTimeInputParamsType = _previewAssembly.GetType("C1.C1Preview.TagDateTimeInputParams");
				_tagListInputParamsType = _previewAssembly.GetType("C1.C1Preview.TagListInputParams");
				_tagBoolInputParamsType = _previewAssembly.GetType("C1.C1Preview.TagBoolInputParams");
				_tagNumericInputParamsType = _previewAssembly.GetType("C1.C1Preview.TagNumericInputParams");
				_tagListInputParamsItemType = _previewAssembly.GetType("C1.C1Preview.TagListInputParamsItem");
			}
			catch
			{
#if WINFX
				MessageBox.Show("Could not initialize C1.WPF.C1Report.dll.\n" +
					"Please make sure that dll is available and properly installed.",
					"C1Schedule", MessageBoxButton.OK, MessageBoxImage.Error);
#else
				MessageBox.Show(String.Format(
                    Strings.MiscStrings.Item("PrintdocInitializationFailed", _printInfo._schedule.CalendarInfo.CultureInfo), assName), 
					Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
#endif
				_previewAssembly = null;
			}
			if (_previewAssembly == null)
				return false;
			return true;
		}

		internal static Assembly TryLoadAssembly(string assemblyName, string tokenString)
		{
			// check already loaded assemblies
			Assembly[] domainAssemblies = AppDomain.CurrentDomain.GetAssemblies();

			foreach (Assembly ass in domainAssemblies)
			{
				if (ass.FullName.Contains(assemblyName))
				{
					if (String.IsNullOrEmpty(tokenString)
						|| (!String.IsNullOrEmpty(tokenString) && ass.FullName.Contains(tokenString)))
					{
						return ass;
					}
				}
			}

			// Note: Assembly.Load does not work when the assembly
			// is in the GAC. LoadWithPartialName works...
			Assembly result = null;
			try
			{
				result = Assembly.Load(assemblyName);
			}
			catch
			{
				result = Assembly.LoadWithPartialName(assemblyName);
			}
			if (result != null)
			{
				if (String.IsNullOrEmpty(tokenString)
					|| (!String.IsNullOrEmpty(tokenString) && result.FullName.Contains(tokenString)))
				{
					return result;
				}
			}
			return null;
		}
		#endregion

		#region ** wrapped document
		/// <summary>
		/// Occurs when the document building is started.
		/// </summary>
		internal event EventHandler DocumentStarting;

		/// <summary>
		/// Gets or sets wrapped C1PrintDocument instance.
		/// </summary>
		/// <remarks>
		/// Set this property from code if your application contains reference 
		/// to the C1.WPF.C1Report or C1.C1Report.2 assembly.
		/// In such case you'll be able to work with C1PrintDocument directly from your code.
		/// </remarks>
		[DefaultValue(null)]
		[C1Description("Printing.C1PrintDocument", "C1.C1Preview.C1PrintDocument component.")]
		public Component C1PrintDocument
		{
			get
			{
#if WINFX
				if (_printInfo.Owner == null ||
					_printInfo.Owner.DesignMode)
				{
					return _document;
				}
#else
				if (_printInfo.Owner == null ||
					_printInfo.Owner.IsDesignMode)
				{
					return _document;
				}
#endif
				if (Loaded && _document == null)
				{
					try
					{
						_document = (Component)Activator.CreateInstance(
#if WINFX
						_documentType, new object[] { "EwHlAYMBngABAf4AtQDKAG0B" });
#else
						_documentType, new object[] { "CgGnAaIBJADOAcoAPABUAGsB" });
#endif
					}
					catch
					{
						_document = (Component)Activator.CreateInstance(_documentType);
					}
					if (_document != null)
					{
						Subscribe();
					}
				}
				return _document;
			}
			set
			{
				if (_document != value)
				{
					if (_document != null)
					{
						Unsubscribe();
					}
					_document = value;
					if (_document != null)
					{
						Subscribe();
					}
				}
			}
		}

		private void Subscribe()
		{
			if (C1PrintDocument == null || _printInfo.Owner == null || _printInfo.Owner.IsDesignMode)
			{
				return;
			}
			EventInfo ei = _documentType.GetEvent("DocumentStarting");
			if (ei != null)
			{
				ei.AddEventHandler(_document, _docStartingDelegate);
			}
			if ( _longOperationDelegate == null )
			{
				MethodInfo mi = GetType().GetMethod("c1PrintDocument1_LongOperation", BindingFlags.NonPublic | BindingFlags.Instance);
				_longOperationDelegate = Delegate.CreateDelegate(_longOperationEventHandlerType, this, mi, false);
			}
			if (_longOperationDelegate != null)
			{
				ei = _documentType.GetEvent("LongOperation");
				if (ei != null)
				{
					ei.AddEventHandler(_document, _longOperationDelegate);
				}
			}
		}

		private void Unsubscribe()
		{
			if (C1PrintDocument == null || _printInfo.Owner == null || _printInfo.Owner.IsDesignMode)
			{
				return;
			}
			EventInfo ei = _documentType.GetEvent("DocumentStarting");
			if (ei != null)
			{
				ei.RemoveEventHandler(_document, _docStartingDelegate);
			}
			if (_longOperationDelegate != null)
			{
				ei = _documentType.GetEvent("LongOperation");
				if (ei != null)
				{
					ei.RemoveEventHandler(_document, _longOperationDelegate);
				}
			}
		}

		[Obfuscation(Exclude = true)]
		private void c1PrintDocument1_DocumentStarting(object sender, EventArgs e)
		{
			if (DocumentStarting != null)
			{
				DocumentStarting(sender, e);
			}
		}
#if WINFX
		/// <summary>
		/// Get the <see cref="FixedDocumentSequence"/> object from the wrapped C1PrintDocument object.
		/// </summary>
		public FixedDocumentSequence Document 
		{
			get
			{
				if (C1PrintDocument == null)
				{
					return null;
				}
				return (FixedDocumentSequence)_documentType.GetProperty("FixedDocumentSequence").GetValue(_document, null);
			}
		}
#endif
		#endregion

		#region ** print progress
		[Obfuscation(Exclude = true)]
		private void c1PrintDocument1_LongOperation(object sender, EventArgs e)
		{
			Application.DoEvents();
			if (_progressForm == null)
			{
				return;
			}
			PropertyInfo pi = _longOperationEventArgsType.GetProperty("Complete");
			if (pi != null)
			{
				Double complete = (Double)pi.GetValue(e, null);
				_progressForm.Progress = complete;
			}

			if(	_progressForm.CancelClicked)
			{
				pi = _longOperationEventArgsType.GetProperty("CanCancel");
				if (pi != null)
				{
					bool canCancel = (bool)pi.GetValue(e, null);
					if (canCancel)
					{
						pi = _longOperationEventArgsType.GetProperty("Cancel");
						if (pi != null)
						{
							try
							{
								pi.SetValue(e, true, null);
							}
							catch
							{
							}
						}
					}
				}
			}
		}

		private bool ActivateProgressForm()
		{
			if (!_printInfo.ShowProgressForm)
			{
				return false;
			}
			bool result = false;
			if (_progressForm == null)
			{
				_progressForm = new PrintProgressForm(_printInfo);
				result = true;
			}
			_progressForm.Show();
			Application.DoEvents();
			if (_mainForm == null)
			{
				_mainForm = _printInfo.Owner.FindForm();
			}
			if (_mainForm != null)
			{
				// imitate modal dialog
				_mainForm.Activated += new EventHandler(frm_Activated);
			}
			return result;
		}

		private void HideProgressForm()
		{
			if (_mainForm != null)
			{
				// imitate modal dialog
				_mainForm.Activated += new EventHandler(frm_Activated);
			}
			DisposeProgressForm();
		}

		void frm_Activated(object sender, EventArgs e)
		{
			if (_progressForm != null)
			{
				_progressForm.Activate();
			}
		}

		void DisposeProgressForm()
		{
			// hide the progress dialog if it is shown
			if (_progressForm != null)
			{
				_progressForm.Close();
				_progressForm.Dispose();
				_progressForm = null;
			}
		}
		void SetProgressText(string text)
		{
			if (_progressForm != null)
			{
				_progressForm.TxtPrinting = text;
				Application.DoEvents();
				if (text == "")
				{
					HideProgressForm();
				}
			}
		}
		#endregion


		#region ** public methods
		/// <summary>
		/// Returns the value of the specified property of the document's DocumentInfo object.
		/// </summary>
		public object GetDocumentInfoProperty(string propertyName)
		{
			if (C1PrintDocument == null)
			{
				return null;
			}
			PropertyInfo pi = _documentType.GetProperty("DocumentInfo");
			if (pi != null)
			{
				object docInfo = pi.GetValue(_document, null);
				if (docInfo != null)
				{
					return _documentInfoType.GetProperty("propertyName").GetValue(docInfo, null).ToString();
				}
			}
			return null;
		}

		/// <summary>
		/// Adds specified external assembly reference to the currently loaded document.
		/// </summary>
		/// <param name="assemblyName">The <see cref="String"/> value specifying assembly name for adding.</param>
		public void AddExternalAssembly(string assemblyName)
		{
			if (C1PrintDocument == null)
			{
				return;
			}
			PropertyInfo pi = _documentType.GetProperty("ScriptingOptions");   
			if (pi != null)
			{
				object options = pi.GetValue(_document, null);
				if (options != null)
				{
					pi = _scriptingOptionsType.GetProperty("ExternalAssemblies");
					if (pi != null)
					{
						StringCollection extAssemblies = (StringCollection)pi.GetValue(options, null);
						if (extAssemblies != null && !extAssemblies.Contains(assemblyName))
						{
							extAssemblies.Add(assemblyName);
						}
					}
				}
			}
		}

		private void AddScheduleReference()
		{
#if (PUBLIC_STYLES) 
            // WinForms version
            AddExternalAssembly(GetType().Module.Name);
#else       
            // for WPF version PrintDocWrapper is not included into Scheduler's assembly,
            // so find Scheduler's assembly in app domain
            Assembly[] domainAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly ass in domainAssemblies)
            {
                if (ass.FullName.Contains("Schedule"))
                {
                    AddExternalAssembly(ass.ManifestModule.Name);
                }
            }
#endif
#if WINFX // add some more references
			string path = Assembly.GetAssembly(typeof(System.Windows.Media.Color)).Location;
			AddExternalAssembly(path);
			path = Assembly.GetAssembly(typeof(INotifyCollectionChanged)).Location;
			AddExternalAssembly(path);
		/*	path = Assembly.GetAssembly(typeof(System.Drawing.Font)).Location;
			AddExternalAssembly(path);*/
#endif
		}

		/// <summary>
        /// Loads the current document from a stream.
        /// The stream should contain a document in C1D format.
        /// </summary>
        /// <param name="stream">The stream from which to load the document.</param>
		public void Load(Stream stream)
		{
			if ( C1PrintDocument == null )
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Load", new Type[] { typeof(Stream) });
			if (mi == null)
				return;
			mi.Invoke(_document, new object[] { stream });
			AddScheduleReference();
		}

        /// <summary>
        /// Loads the current document from a stream.
        /// </summary>
        /// <param name="stream">The stream from which to load the document.</param>
        /// <param name="documentFormat">The document persistence format to use.</param>
        public void Load(Stream stream, object documentFormat)
        {
 			if ( C1PrintDocument == null || _documentFormatEnum == null)
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Load", new Type[] { typeof(Stream), _documentFormatEnum });
			if (mi == null)
				return;
			mi.Invoke(_document, new object[] { stream, documentFormat });
			AddScheduleReference();
		}

        /// <summary>
        /// Loads the current document from a file.
        /// The file format (C1D or C1DX) is determined by the file extension.
        /// The format defaults to C1D if the format cannot be determined from the extension.
        /// </summary>
        /// <param name="fileName">The file name.</param>
		public void Load(string fileName)
		{
			if ( C1PrintDocument == null )
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Load", new Type[] { typeof(String) });
			if (mi == null)
				return;
			mi.Invoke(_document, new object[] { fileName });
			AddScheduleReference();
		}

        /// <summary>
        /// Loads the current document from a file.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="documentFormat">The document persistence format to use.</param>
		public void Load(string fileName, object documentFormat)
        {
 			if ( C1PrintDocument == null || _documentFormatEnum == null)
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Load", new Type[] { typeof(String), _documentFormatEnum });
			if (mi == null)
				return;
			mi.Invoke(_document, new object[] { fileName, documentFormat });
			AddScheduleReference();
        }

		/// <summary>
        /// Prints the document.
		/// </summary>
		public void Print()
		{
			if (C1PrintDocument == null)
			{
				return;
			}
			MethodInfo mi = _documentType.GetMethod("Print", new Type[] { Type.GetType("System.Drawing.Printing.PrinterSettings, System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a") });
			if (mi == null)
				return;
			ActivateProgressForm();
			Generate();
			SetProgressText(Strings.PrintingStrings.Item("Printing", _printInfo._schedule.CalendarInfo.CultureInfo));
			mi.Invoke(_document, new object[] { null });
			HideProgressForm();
		}

#if !WINFX
		/// <summary>
        /// Prints the document.
        /// </summary>
        /// <param name="printerSettings">The printer settings to use.</param>
		public void Print(PrinterSettings printerSettings)
        {
 			if ( C1PrintDocument == null)
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Print", new Type[] { typeof(PrinterSettings) });
			if (mi == null)
				return;
			if (printerSettings == null)
			{
				printerSettings = new PrinterSettings();
			}
			ActivateProgressForm();
			Generate();
			SetProgressText(Strings.PrintingStrings.Item("Printing", _printInfo._schedule.CalendarInfo.CultureInfo));
			mi.Invoke(_document, new object[] { printerSettings });
			HideProgressForm();
		}

        /// <summary>
        /// Prints the document.
        /// </summary>
        /// <param name="printerSettings">The printer settings to use.</param>
        /// <param name="showProgress">Flag indicating whether to show the progress dialog.</param>
        public void Print(PrinterSettings printerSettings, bool showProgress)
        {
 			if ( C1PrintDocument == null)
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Print", new Type[] { typeof(PrinterSettings), typeof(Boolean) });
			if (mi == null)
				return;
			ActivateProgressForm();
			Generate();
			SetProgressText(Strings.PrintingStrings.Item("Printing", _printInfo._schedule.CalendarInfo.CultureInfo));
			mi.Invoke(_document, new object[] { printerSettings, showProgress });
			HideProgressForm();
		}
        
		/// <summary>
        /// Prints the document.
        /// </summary>
        /// <param name="printerSettings">The printer settings to use.</param>
        /// <param name="defaultPageSettings">The default page settings to use.</param>
        /// <param name="showProgress">Flag indicating whether to show the progress dialog.</param>
        public void Print(PrinterSettings printerSettings, PageSettings defaultPageSettings, bool showProgress)
        {
 			if ( C1PrintDocument == null)
			{
				return;
			}
 			MethodInfo mi = _documentType.GetMethod("Print", new Type[] { typeof(PrinterSettings), typeof(PageSettings), typeof(Boolean) });
			if (mi == null)
				return;
			ActivateProgressForm();
			Generate();
			SetProgressText(Strings.PrintingStrings.Item("Printing", _printInfo._schedule.CalendarInfo.CultureInfo));
			mi.Invoke(_document, new object[] { printerSettings, defaultPageSettings, showProgress });
			HideProgressForm();
		}
#endif

		/// <summary>
		/// Generates a document.
		/// </summary>
		/// <returns>Returns true if no warning occurs during generating.</returns>
		public bool Generate()
		{
			if (C1PrintDocument == null)
			{
				return false;
			}
			MethodInfo mi = _documentType.GetMethod("Generate", new Type[] { _refreshModeEnum });
			if (mi == null)
				return false;
			bool newForm = false;
			try
			{
				newForm = ActivateProgressForm();
				return (bool)mi.Invoke(_document, new object[] { 0x01 });
			}
			finally
			{
				if (newForm)
				{
					HideProgressForm();
				}
			}
		}

		/// <summary>
		/// Updates specified tag dictionary with the current document tags.
		/// </summary>
		/// <param name="tags">The <see cref="Dictionary{String, TagInfo}"/> dictionary of tags for updating.</param>
		/// <remarks>If specified dictionary doesn't contain one of document tags, this tag will be added to the dictionary.
		/// If the dictionary contains tag with the same name and type, this tag won't be changed.
		/// If the dictionary contains tag with the same name and different type, this tag will be replaced with the new one.</remarks>
		public void ReadTags(ref Dictionary<string, TagInfo> tags)
		{
			if (C1PrintDocument == null)
			{
				return;
			}
			PropertyInfo pi = _documentType.GetProperty("Tags");   
			if (pi != null)
			{
				object listtags = pi.GetValue(_document, null);
				if (listtags != null)
				{
					int count = (int)_tagCollectionType.GetProperty("Count").GetValue(listtags, null);
					for (int i = 0; i < count; i++)
					{
						TagInfo tag = new TagInfo(_tagCollectionType.GetProperty("Item", new Type[] { typeof(int) }).GetValue(listtags, new object[] { i }));
						if (!tags.ContainsKey(tag.Name))
						{
							tags.Add(tag.Name, tag);
						}
						else if (tags[tag.Name].Type == tag.Type)
						{
							tags[tag.Name].RefreshNativeTagReference(tag);
						}
						else
						{
							tags[tag.Name] = tag;
						}
					}
				}
			}
		}

		/// <summary>
		/// Sets the value of the specified document tag.
		/// </summary>
		/// <param name="name">The <see cref="String"/> value specifying the name of the document tag.</param>
		/// <param name="value">The value to be set.</param>
		public void SetDocumentTag(string name, object value)
		{
			if (C1PrintDocument == null)
			{
				return;
			}
			object tags = _documentType.GetProperty("Tags").GetValue(_document, null);
			object tag = _tagCollectionType.GetProperty("Item", _tagType, new Type[] { typeof(String) }).GetValue(tags, new object[] { name });
			_tagType.GetProperty("Value").SetValue(tag, value, null);
		}

		/// <summary>
		/// Adds the new tag with the specified value to the document.
		/// </summary>
		/// <param name="name">The <see cref="String"/> value specifying the name of the document tag.</param>
		/// <param name="value">The value to be set.</param>
		public void AddDocumentTag(string name, object value)
		{
			if (C1PrintDocument == null)
			{
				return;
			}
			object tag = Activator.CreateInstance(_tagType, new object[] { name, value });
 			MethodInfo mi = _tagCollectionType.GetMethod("Add", new Type[] {_tagType} );
			if (mi == null)
				return;
			mi.Invoke(_documentType.GetProperty("Tags").GetValue(_document, null), new object[] {tag});
		}

		/// <summary>
        /// Clears the document, all its properties will be set to default values
        /// </summary>
		public void Clear()
		{
			if (C1PrintDocument == null)
			{
				return;
			}
			MethodInfo mi = _documentType.GetMethod("Clear");
			if (mi == null)
				return;
			mi.Invoke(_document, null);
		}

		/*	internal void SetExporterDocFromClient(object exporter, C1.C1PreviewClient.C1PrintDocument doc)
			{
				System.Diagnostics.Debug.Assert(exporter != null);
				if (doc != null)
				{
					System.IO.Stream mystream = new System.IO.MemoryStream();
					_info.SetProgressText(C1Localizer.GetString("Preparing document"));
					doc.Save(mystream);
					mystream.Position = 0;
					exporter.GetType().GetProperty("Document").SetValue(exporter, _newC1PrintDocument(mystream), null);
					_info.SetProgressText("");
				}
				else
					exporter.GetType().GetProperty("Document").SetValue(exporter, null, null);
			}*/

		/*		public void Export(object exporter, string filename)
				{
					System.Type exporterType = exporter.GetType();
					if (exporterType.GetProperty("Document").GetValue(exporter, null) != null)
					{
						exporterType.GetProperty("ShowOptions").SetValue(exporter, false, null);
						MethodInfo expm = exporterType.GetMethod("Export", new Type[] { typeof(string) });
						if (expm != null)
							expm.Invoke(exporter, new object[] { filename });
					}
				}

				public static string ExportFilter()
				{
					if (!Loaded || _exportProvidersType == null)
						return string.Empty;

					if (_exportFilter == string.Empty)
					{
						if (_availableProviders == null)
							GetExportProviders();
						for (int i = 0; i < _availableProviders.Length; i++)
						{
							_exportFilter += _exportProviderType.GetProperty("FormatName").GetValue(_availableProviders[i], null)
							+ " (*." + _exportProviderType.GetProperty("DefaultExtension").GetValue(_availableProviders[i], null)
							+ ")|*." + _exportProviderType.GetProperty("DefaultExtension").GetValue(_availableProviders[i], null);
							if (i < _availableProviders.Length - 1)
								_exportFilter += "|";
						}
					}
					return _exportFilter;
				}

				public static string ExportFilter(object[] availableProviders)
				{
					if (!Loaded || _exportProvidersType == null)
						return string.Empty;
					string filter = string.Empty;
					for (int i = 0; i < availableProviders.Length; i++)
					{
						filter += _exportProviderType.GetProperty("FormatName").GetValue(availableProviders[i], null)
						+ " (*." + _exportProviderType.GetProperty("DefaultExtension").GetValue(availableProviders[i], null)
						+ ")|*." + _exportProviderType.GetProperty("DefaultExtension").GetValue(availableProviders[i], null);
						if (i < availableProviders.Length - 1)
							filter += "|";
					}
					return filter;
				}

				public static object[] GetExportProviders()
				{
					if (_availableProviders == null)
					{
						PropertyInfo mi = _exportProvidersType.GetProperty("RegisteredProviders");   // static
						if (mi != null)
						{
							object listproviders = mi.GetValue(null, null);
							if (listproviders != null)
							{
								int count = (int)_exportProvidersType.GetProperty("Count").GetValue(listproviders, null);
								_availableProviders = new object[count];
								for (int i = 0; i < count; i++)
								{
									// _availableProviders[i] = _exportProvidersType.GetProperty("Item").GetValue(listproviders, new object[] { i });
									_availableProviders[i] = _exportProvidersType.GetProperty("Item", new Type[] { typeof(int) }).GetValue(listproviders, new object[] { i });
								}
							}
						}
					}
					return _availableProviders;
				}

				public static object[] GetExportProviders(string formatExt)
				{
					if (_availableProviders == null)
						GetExportProviders();
					int j = 0;
					for (int i = 0; i < _availableProviders.Length; i++)
					{
						string provext = (string)_exportProviderType.GetProperty("DefaultExtension").GetValue(_availableProviders[i], null);
						if (formatExt == null || provext.ToLower() == formatExt.ToLower())
							j++;
					}
					object[] res = new object[j];
					j = 0;
					for (int i = 0; i < _availableProviders.Length; i++)
					{
						string provext = (string)_exportProviderType.GetProperty("DefaultExtension").GetValue(_availableProviders[i], null);
						if (formatExt == null || provext.ToLower() == formatExt.ToLower())
							res[j++] = _availableProviders[i];
					}
					return res;
				}*/
		#endregion
		
		#region IDisposable Members

		/// <summary>
		/// Releases all unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the
		// runtime from inside the finalizer and you should not reference
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			if (disposing && _document != null)
			{
				_document.Dispose();
				_document = null;
			}
		}
		#endregion
		#region ** private stuff
#if !WINFX
		internal void SetPageSettings(PageSettings pageSettings)
		{
			if (C1PrintDocument == null || pageSettings == null)
			{
				return;
			}
			object pageLayout = _documentType.GetProperty("PageLayout").GetValue(_document, null);
			_pageLayoutType.GetProperty("PageSettings").SetValue(pageLayout,
				Activator.CreateInstance(_c1PageSettingsType, new object[] { pageSettings }), null);
		}

		internal PageSettings GetPageSettings()
		{
			if (C1PrintDocument == null)
			{
				return null;
			}
			object pageLayout = _documentType.GetProperty("PageLayout").GetValue(_document, null);
			object c1PageSettings = _pageLayoutType.GetProperty("PageSettings").GetValue(pageLayout, null);
			MethodInfo mi = _c1PageSettingsType.GetMethod("ToPageSettings", new Type[] { _documentType });
			if (mi == null)
			{
				return null;
			}
			return mi.Invoke(c1PageSettings, new object[] { _document }) as PageSettings;
		}
#endif
		#endregion
	}
}

