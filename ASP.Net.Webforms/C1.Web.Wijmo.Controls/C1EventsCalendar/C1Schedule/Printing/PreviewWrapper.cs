using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Windows;

using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

#if END_USER_LOCALIZATION
using C1.Win.C1Schedule.Localization;
#else
	using C1.Util.Localization;
#endif

namespace C1.C1Schedule.Printing
{
	/// <summary>
	/// The <see cref="PreviewWrapper"/> class wraps the print preview dialog functionality.
	/// It allows showing C1.C1Preview.C1PrintDocument content in a C1.Win.C1Preview.C1PrintPreviewDialog, 
	/// loaded via reflection from C1.Win.C1Report.2 assembly.
	/// </summary>
	[TypeConverterAttribute(typeof(ExpandableObjectConverter))]
	public class PreviewWrapper: IDisposable
	{
		#region ** fields
		private string _caption = "Print Preview";

		private static Assembly _previewWinAssembly = null;
		private static Type _previewType = null;
		private static bool _failedLoad = false;

		private Form _preview = null;
		
		private PrintInfo _printInfo;
		#endregion

		#region ** initialization
		internal PreviewWrapper(PrintInfo printInfo)
		{
			_printInfo = printInfo;
		}

		private bool Loaded
		{
			get
			{
				if (!_failedLoad && _previewWinAssembly == null)
				{
					_failedLoad = !LoadAssemblies();
				}
				return _previewWinAssembly != null;
			}
		}

		private bool LoadAssemblies()
		{
			if (_previewWinAssembly != null)
				return true;
#if (CLR40)
            string assName = "C1.Win.C1Report.4";
#else
            string assName = "C1.Win.C1Report.2";
#endif
            try
			{
                _previewWinAssembly = PrintDocumentWrapper.TryLoadAssembly(assName, "PublicKeyToken=41780e2fc605e636");
				_previewType = _previewWinAssembly.GetType("C1.Win.C1Preview.C1PrintPreviewDialog");
			}
			catch
			{
				MessageBox.Show(String.Format(
					Strings.MiscStrings.Item("PrintdocInitializationFailed", 
					_printInfo._schedule.CalendarInfo.CultureInfo), assName),
					Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

				_previewWinAssembly = null;
			}
			if (_previewWinAssembly == null)
				return false;
			return true;
		}
		#endregion

		#region ** public interface
		/// <summary>
		/// Gets or sets the caption of the preview window.
		/// </summary>
		[DefaultValue("Print Preview")]
		[C1Description("Printing.PreviewFormCaption", "Gets or sets the caption of the preview window.")]
		public string Caption
		{
			get
			{
				return _caption;
			}
			set
			{
				_caption = value;
			}
		}

		/// <summary>
		/// Gets or sets the C1.Win.C1Preview.C1PrintPreviewDialog object, used for previewing.
		/// </summary>
		/// <remarks>
		/// You can set this property from code if your application contains reference 
		/// to the C1.Win.C1Report.2 assembly.
		/// In such case you'll be able to set various C1.Win.C1Preview.C1PrintPreviewDialog 
		/// properties before showing it to end-users.
		/// </remarks>
		[DefaultValue(null)]
		[C1Description("Printing.C1PrintPreviewDialog", "C1.Win.C1Preview.C1PrintPreviewDialog form, used for previewing.")]
		public Form C1PrintPreviewDialog
		{
			get
			{
				if (_printInfo.Owner == null ||
					_printInfo.Owner.IsDesignMode)
				{
					return _preview;
				}
				if (Loaded && _preview == null)
				{
					// C1PrintPreviewDialog hasn't got ctor with internal license
					_preview = Activator.CreateInstance(_previewType) as Form;
				}
				return _preview;
			}
			set
			{
				_preview = value;
			}
		}

		/// <summary>
		/// Shows the specified object in a print preview dialog.
		/// </summary>
		/// <param name="printDocument">An object to preview. 
		/// It might be one of the next types:
		///   - C1.C1Preview.C1PrintDocument, 
		///   - <see cref="PrintDocument"/>,
		///   - C1.C1Report.C1Report, 
		///   - <see cref="IBindingList"/>, 
		///   - <see cref="System.Collections.IEnumerable"/>.</param>
		public void Preview(object printDocument)
		{
			if (!Loaded || _previewType == null )
				return;

			if (printDocument != null && C1PrintPreviewDialog != null)
			{
				_previewType.GetProperty("Document").SetValue(_preview, printDocument, null);
                string caption = Caption;
                if (caption == "Print Preview")
                {
                    caption = Strings.PrintingStrings.Item("PrintPreview", _printInfo._schedule.CalendarInfo.CultureInfo);
                }

				_preview.Text = caption;
				_preview.ShowDialog();
			}
		}

		/// <summary>
		/// Shows the specified object in a print preview dialog using the specified <see cref="PageSettings"/> object.
		/// </summary>
		/// <param name="printDocument">The <see cref="PrintDocumentWrapper"/> object to preview.</param>
		/// <param name="pageSettings">The <see cref="PageSettings"/> object for using in preview.</param>
		public void Preview(PrintDocumentWrapper printDocument, ref PageSettings pageSettings)
		{
			printDocument.Generate();
			Preview(printDocument.C1PrintDocument);
			pageSettings = printDocument.GetPageSettings();
		}
		/// <summary>
		/// Shows the specified <see cref="PrintDocumentWrapper"/> object in a print preview dialog.
		/// </summary>
		/// <param name="printDocument">The <see cref="PrintDocumentWrapper"/> object to preview.</param>
		public void Preview(PrintDocumentWrapper printDocument)
		{
			printDocument.Generate();
			Preview(printDocument.C1PrintDocument);
		}
		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Releases all unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the
		// runtime from inside the finalizer and you should not reference
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			if (disposing && _preview != null)
			{
				_preview.Close();
				if (!_preview.IsDisposed)
				{
					_preview.Dispose();
				}
				_preview = null;
			}
		}
		#endregion
	}
}

