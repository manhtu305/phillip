using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace C1.C1Schedule.Printing
{
	/// <summary>
	/// The <see cref="DateAppointments"/> class represents the single calendar day
	/// with the <see cref="List{Appointment}"/> list of appointments.
	/// It is an auxiliary class for use in C1PrintDocument scripts.
	/// </summary>
	public class DateAppointments
	{
		#region ** fields
		private DateTime _date;
		private List<Appointment> _appointments = null;
		private Status _busyStatus = null;
		#endregion

		#region ** ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="DateAppointments"/> class.
		/// </summary>
		/// <param name="date">The <see cref="DateTime"/> value determining the represented date.</param>
		public DateAppointments(DateTime date)
		{
			_date = date;
		}
		#endregion

		#region ** object model
		/// <summary>
		/// Gets the <see cref="DateTime"/> value determining the represented date.
		/// </summary>
		public DateTime Date
		{
			get
			{
				return _date;
			}
		}

		/// <summary>
		/// Returns true if current day contains one or more appointments.
		/// </summary>
		public bool HasAppointments
		{
			get
			{
				return _appointments != null && _appointments.Count > 0;
			}
		}

		/// <summary>
		/// Returns availability status of the current day.
		/// </summary>
		/// <remarks>Availability status is determined according to availability statuses of
		/// AllDay events for the representing day.</remarks>
		public Status BusyStatus
		{
			get
			{
				return _busyStatus;
			}
		}

		/// <summary>
		/// Gets the <see cref="List{Appointment}"/> list of all appointments for the represented day.
		/// </summary>
		public List<Appointment> Appointments
		{
			get
			{
				if (_appointments == null)
				{
					_appointments = new List<Appointment>();
				}
				return _appointments;
			}
		}
		#endregion

		#region ** public methods
		/// <summary>
		/// Adds a new <see cref="Appointment"/> object to the current day.
		/// </summary>
		/// <remarks>The specified <see cref="Appointment"/> object will be added 
		/// only if some part of the appointment falls on the current day.</remarks>
		/// <param name="appointment">The <see cref="Appointment"/> object to add.</param>
		public void AddAppointment(Appointment appointment)
		{
			bool needRefreshStatus = false;
            DateTime start = appointment.Start;
            DateTime end = appointment.End;
            if (start == end)
            {
                end = end.AddMilliseconds(1);
            }

			if (start < _date.AddDays(1) &&
				((end.TimeOfDay == TimeSpan.Zero && end > _date) ||
				(end.TimeOfDay > TimeSpan.Zero && end >= _date)))
			{
				Appointments.Add(appointment);
				needRefreshStatus = true;
			}
			if (needRefreshStatus)
			{
				foreach (Appointment app in Appointments)
				{
					if (app.BusyStatus != null && app.BusyStatus.StatusType != StatusTypeEnum.Free)
					{
						if (app.AllDayEvent || app.Duration.TotalDays > 1)
						{
							_busyStatus = app.BusyStatus;
							return;
						}
					}
				}
			}
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="DateAppointmentsCollection"/> class represents 
	/// the <see cref="KeyedCollection{DateTime, DateAppointments}"/> collection 
	/// which can be used as a data source in C1PrintDocument documents.
	/// </summary>
	public class DateAppointmentsCollection : KeyedCollection<DateTime, DateAppointments>
	{
		#region ** ctor
		private DateAppointmentsCollection()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DateAppointmentsCollection"/> collection
		/// for the specified date range with appointment from the specified <see cref="AppointmentCollection"/> collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the date range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the date range.</param>
		/// <param name="appointmentCollection">The <see cref="AppointmentCollection"/> collection for getting appointments from.</param>
		public DateAppointmentsCollection(DateTime start, DateTime end, AppointmentCollection appointmentCollection)
			: this(start, end, appointmentCollection, null, true, true, true)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DateAppointmentsCollection"/> collection
		/// for the specified date range with appointment from the specified <see cref="AppointmentCollection"/> collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the date range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the date range.</param>
		/// <param name="appointmentCollection">The <see cref="AppointmentCollection"/> collection for getting appointments from.</param>
		/// <param name="includeEmptyDays">The <see cref="Boolean"/> value specifying whether to include days without appointments into collection.</param>
		/// <param name="includePrivateAppointments">The <see cref="Boolean"/> value specifying whether to include private appointments into collection.</param>
		public DateAppointmentsCollection(DateTime start, DateTime end, AppointmentCollection appointmentCollection, bool includeEmptyDays, bool includePrivateAppointments)
			: this(start, end, appointmentCollection, null, includeEmptyDays, true, includePrivateAppointments)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DateAppointmentsCollection"/> collection
		/// for the specified date range with appointment from the specified <see cref="AppointmentCollection"/> collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the date range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the date range.</param>
		/// <param name="appointmentCollection">The <see cref="AppointmentCollection"/> collection for getting appointments from.</param>
		/// <param name="calendarInfo">The <see cref="CalendarInfo"/> object containing calendar specific information.</param>
		/// <param name="includeEmptyDays">The <see cref="Boolean"/> value specifying whether to include days without appointments into collection.</param>
		/// <param name="includeWeekends">The <see cref="Boolean"/> value specifying whether to include weekend days into collection.</param>
		/// <param name="includePrivateAppointments">The <see cref="Boolean"/> value specifying whether to include private appointments into collection.</param>
		public DateAppointmentsCollection(DateTime start, DateTime end, AppointmentCollection appointmentCollection, CalendarInfo calendarInfo, bool includeEmptyDays, bool includeWeekends, bool includePrivateAppointments)
		{
			AppointmentList list = appointmentCollection.GetOccurrences(start, end.AddDays(1));
			if (!includePrivateAppointments)
			{
				for (int i = list.Count - 1; i >= 0; i--)
				{
					if (list[i].Private)
					{
						list.RemoveAt(i);
					}
				}
			}
			Init(start, end, list, calendarInfo, includeEmptyDays, includeWeekends);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DateAppointmentsCollection"/> collection
		/// for the specified date range with appointment from the specified <see cref="AppointmentCollection"/> collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the date range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the date range.</param>
		/// <param name="appointments">The <see cref="IList{Appointment}"/> list of appointments.</param>
		/// <param name="includeEmptyDays">The <see cref="Boolean"/> value specifying whether to include days without appointments into collection.</param>
		public DateAppointmentsCollection(DateTime start, DateTime end, IList<Appointment> appointments, bool includeEmptyDays)
			: this(start, end, appointments, null, includeEmptyDays, true)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DateAppointmentsCollection"/> collection
		/// for the specified date range with appointment from the specified <see cref="AppointmentCollection"/> collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the date range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the date range.</param>
		/// <param name="appointments">The <see cref="IList{Appointment}"/> list of appointments.</param>
		/// <param name="calendarInfo">The <see cref="CalendarInfo"/> object containing calendar specific information.</param>
		/// <param name="includeEmptyDays">The <see cref="Boolean"/> value specifying whether to include days without appointments into collection.</param>
		/// <param name="includeWeekends">The <see cref="Boolean"/> value specifying whether to include weekend days into collection.</param>
		public DateAppointmentsCollection(DateTime start, DateTime end, IList<Appointment> appointments, CalendarInfo calendarInfo, bool includeEmptyDays, bool includeWeekends)
		{
			Init(start, end, appointments, calendarInfo, includeEmptyDays, includeWeekends);
		}

		private void Init(DateTime start, DateTime end, IList<Appointment> appointments, CalendarInfo calendarInfo, bool includeEmptyDays, bool includeWeekends)
		{
			FillCollection(start, end);
			foreach (Appointment app in appointments)
			{
				AddAppointment(app);
			}
			RemoveExtraDays(calendarInfo, includeEmptyDays, includeWeekends);
		}
		#endregion

		#region ** interface
		/// <summary>
		/// Gets a new <see cref="DateAppointmentsCollection"/> collection containing 
		/// <see cref="DateAppointments"/> objects for the specified date range.
		/// </summary>
		/// <remarks>Resulting collection always contains the subset of the current collection.
		/// All days out of this collection date range are omitted.</remarks>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the date range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the date range.</param>
		/// <returns>The <see cref="DateAppointmentsCollection"/> collection containing 
		/// <see cref="DateAppointments"/> objects for the specified date range.</returns>
		public DateAppointmentsCollection this[DateTime start, DateTime end]
		{
			get
			{
				if (end < start)
				{
					DateTime tmp = start;
					start = end;
					end = tmp;
				}
				start = start.Date;
				DateAppointmentsCollection coll = new DateAppointmentsCollection();
				while (start <= end)
				{
					if (Contains(start))
					{
						coll.Add(this[start]);
					}
					start = start.AddDays(1);
				}
				return coll;
			}
		}
		
		/// <summary>
		/// Gets a <see cref="List{Appointment}"/> containing <see cref="Appointment"/>
		/// objects for the specified DateTime range.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value representing the beginning of the DateTime range.</param>
		/// <param name="end">The <see cref="DateTime"/> value representing the end of the DateTime range.</param>
		/// <param name="includeAll">Specifies whether the returned list contains all appointments 
		/// intercepting the specified DateTime range. If this value is False, the returned list
		/// will contain only appointments starting within the specified DateTime range.</param>
		/// <returns>The <see cref="List{Appointment}"/> list containing 
		/// <see cref="Appointment"/> objects for the specified DateTime range.</returns>
		public List<Appointment> GetIntervalAppointments(DateTime start, DateTime end, bool includeAll)
		{
			if (end < start)
			{
				DateTime tmp = start;
				start = end;
				end = tmp;
			}
			DateTime startDate = start.Date;
			List<Appointment> list = new List<Appointment>();
			while (startDate <= end)
			{
				if (Contains(startDate))
				{
					foreach (Appointment app in this[startDate].Appointments)
					{
						if (list.Contains(app))
						{
							continue;
						}
						if (includeAll)
						{
							if (app.End >= start && app.Start < end)
							{
								list.Add(app);
							}
						}
						else
						{
							if (app.Start >= start && app.Start < end)
							{
								list.Add(app);
							}
						}
					}
				}
				startDate = startDate.AddDays(1);
			}
			return list;
		}
		#endregion

		#region ** private stuff
		/// <summary>
		/// Returns key value for the specified item.
		/// </summary>
		/// <param name="item">A <see cref="DateAppointments"/> object.</param>
		/// <returns>The <see cref="DateTime"/> value used as a key.</returns>
		protected override DateTime GetKeyForItem(DateAppointments item)
		{
			return item.Date;
		}
		/// <summary>
		/// Returns DayCollection for all dates from the start till the end.
		/// </summary>
		/// <returns></returns>
		void FillCollection(DateTime start, DateTime end)
		{
			if (end < start)
			{
				DateTime tmp = start;
				start = end;
				end = tmp;
			}
			while (start <= end)
			{
				Add(new DateAppointments(start.Date));
				start = start.AddDays(1);
			}
		}

		void RemoveExtraDays(CalendarInfo calendarInfo, bool includeEmptyDays, bool includeWeekends)
		{
			if (!includeEmptyDays)
			{
				for (int i = Count - 1; i >= 0; i--)
				{
					if (!this[i].HasAppointments)
					{
						RemoveAt(i);
					}
				}
			}
			if (!includeWeekends && calendarInfo != null)
			{
				for (int i = Count - 1; i >= 0; i--)
				{
					if (!calendarInfo.WorkDays.Contains(this[i].Date.DayOfWeek))
					{
						RemoveAt(i);
					}
				}
			}
		}

		// adds appointment
		void AddAppointment(Appointment appointment)
		{
			DateTime start = appointment.Start.Date;
			DateTime end = appointment.End;
            if (end == start)
            {
                end = end.AddMilliseconds(1);
            }
			while (start < end)
			{
				if (this.Contains(start))
				{
					this[start].AddAppointment(appointment);
				}
				start = start.AddDays(1);
			}
		}
		#endregion
	}
}
