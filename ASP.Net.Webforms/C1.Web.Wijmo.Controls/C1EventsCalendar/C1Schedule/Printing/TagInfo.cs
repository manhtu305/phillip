using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace C1.C1Schedule.Printing
{
	/// <summary>
	/// The <see cref="TagInfo"/> class represents the facade for the C1.C1Preview.Tag class.
	/// Use this class for getting information about document tags and for 
	/// setting tag values before rendering the document.
	/// </summary>
	public class TagInfo
	{
		#region ** fields
		private object _value;
		private string _description;
		private string _name;
		private bool _showInDialog = false;
		private Type _type;
		object _sourceTag = null;
		private TagInputParams _inputParams = null;

		#endregion

		#region ** ctor
		internal TagInfo(object sourceTag)
		{
			Type tagType = PrintDocumentWrapper._tagType;
			if (tagType == null || sourceTag == null)
			{
				return;
			}
			_sourceTag = sourceTag;
			PropertyInfo pi = tagType.GetProperty("Name");   // static
			if (pi != null)
			{
				_name = pi.GetValue(sourceTag, null).ToString();
			}
			pi = tagType.GetProperty("Description");   // static
			if (pi != null)
			{
				_description = (string)pi.GetValue(sourceTag, null);
			}
			pi = tagType.GetProperty("ShowInDialog");   // static
			if (pi != null)
			{
				_showInDialog = (bool)pi.GetValue(sourceTag, null);
			}
			pi = tagType.GetProperty("Type");   // static
			if (pi != null)
			{
				_type = pi.GetValue(sourceTag, null) as Type;
			}
			pi = tagType.GetProperty("Value");   // static
			if (pi != null)
			{
				_value = pi.GetValue(sourceTag, null);
			}
			ReadInputParams();
		}

		private void ReadInputParams()
		{
			PropertyInfo pi = PrintDocumentWrapper._tagType.GetProperty("InputParams");
			if (pi != null)
			{
				object pars = pi.GetValue(_sourceTag, null);
				Type paramsType = null;
				if (pars != null)
				{
					switch (pars.GetType().Name)
					{
						case "TagStringInputParams":
							_inputParams = new TagStringInputParams();
							paramsType = PrintDocumentWrapper._tagStringInputParamsType;
							pi = paramsType.GetProperty("MaxLength");
							if (pi != null)
							{
								((TagStringInputParams)_inputParams).MaxLength = (int)pi.GetValue(pars, null);
							}
							break;
						case "TagDateTimeInputParams":
							_inputParams = new TagDateTimeInputParams();
							paramsType = PrintDocumentWrapper._tagDateTimeInputParamsType;
							/*	TODO: fill min and max dates in a form according to scheduler CalendarInfo settings
								pi = paramsType.GetProperty("MinDate");   
								if (pi != null)
								{
									((TagDateTimeInputParams)_inputParams).MinDate = (DateTime)pi.GetValue(pars, null);
								}
								pi = paramsType.GetProperty("MaxDate");   
								if (pi != null)
								{
									((TagDateTimeInputParams)_inputParams).MaxDate = (DateTime)pi.GetValue(pars, null);
								}*/
							pi = paramsType.GetProperty("Format");
							if (pi != null)
							{
								((TagDateTimeInputParams)_inputParams).Format = (int)pi.GetValue(pars, null);
							}
							break;
						case "TagListInputParams":
							_inputParams = new TagListInputParams();
							paramsType = PrintDocumentWrapper._tagListInputParamsType;
							pi = paramsType.GetProperty("Type");
							if (pi != null)
							{
								((TagListInputParams)_inputParams).Type = (int)pi.GetValue(pars, null);
							}
							pi = paramsType.GetProperty("Items");
							if (pi != null)
							{
								System.Collections.CollectionBase items = pi.GetValue(pars, null) as System.Collections.CollectionBase;
								if (items != null)
								{
									pi = PrintDocumentWrapper._tagListInputParamsItemType.GetProperty("Value");
									foreach (object item in items)
									{
										((TagListInputParams)_inputParams).Items.Add(pi.GetValue(item, null));
									}
								}
							}
							break;
						case "TagBoolInputParams":
							_inputParams = new TagBoolInputParams();
							break;
						case "TagNumericInputParams":
							_inputParams = new TagNumericInputParams();
							paramsType = PrintDocumentWrapper._tagNumericInputParamsType;
							pi = paramsType.GetProperty("Minimum");
							if (pi != null)
							{
								((TagNumericInputParams)_inputParams).Minimum = (int)pi.GetValue(pars, null);
							}
							pi = paramsType.GetProperty("Maximum");
							if (pi != null)
							{
								((TagNumericInputParams)_inputParams).Maximum = (int)pi.GetValue(pars, null);
							}
							pi = paramsType.GetProperty("Increment");
							if (pi != null)
							{
								((TagNumericInputParams)_inputParams).Increment = (int)pi.GetValue(pars, null);
							}
							break;
					}
					if (_inputParams != null)
					{
						paramsType = PrintDocumentWrapper._tagInputParamsType;
						MethodInfo mi = paramsType.GetMethod("IsShowLabel");
						if (mi != null)
						{
							_inputParams.IsShowLabel = (bool)mi.Invoke(pars, null);
						}
					}
				}
				else
				{
					// pars is null, initialize from tag type
					if (Type == typeof(string))
					{
						_inputParams = new TagStringInputParams();
					}
					else if (Type == typeof(bool))
					{
						_inputParams = new TagBoolInputParams();
					}
					else if (Type == typeof(DateTime))
					{
						_inputParams = new TagDateTimeInputParams();
					}
					else if (Type == typeof(int))
					{
						_inputParams = new TagNumericInputParams();
					}
				}
			}
		}

		internal void RefreshNativeTagReference(TagInfo tag)
		{
			_sourceTag = tag._sourceTag;
			Value = Value;
		}
		#endregion

		#region ** public properties
		/// <summary>
		/// Gets or sets the C1.C1Preview.Tag object value.
		/// </summary>
		public object Value
		{
			get
			{
				return _value;
			}
			set
			{
				PropertyInfo pi = PrintDocumentWrapper._tagType.GetProperty("Value");   // static
				if (pi != null)
				{
					pi.SetValue(_sourceTag, value, null);
				}
				_value = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether to show the current tag and
		/// allow the user to edit its <see cref="Value"/> in the tags input dialog.
		/// True by default.
		/// </summary>
		public bool ShowInDialog
		{
			get
			{
				return _showInDialog;
			}
		}

		/// <summary>
		/// Gets the unique name of the tag object as it is defined in a document.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}
		/// <summary>
		/// Gets or sets the description of the tag.
		/// If not empty, used as the label in the tag input dialog.
		/// (If Description is empty, the value of the <see cref="Name"/> property 
		/// will be used as default value.)
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}
		/// <summary>
		/// Gets the current tag type.
		/// </summary>
		[Browsable(false)]
		public Type Type
		{
			get
			{
				return _type;
			}
		}

        /// <summary>
        /// Gets a <see cref="TagInputParams"/> object that is used by the tag input dialog
        /// to customize the editor for the current tag. By default, this property is null.
        /// </summary>
        /// <remarks>
        /// The <see cref="TagInputParams"/> class is abstract, the following types derived from it
        /// can be used, depending on the current tag's type:
        /// <list type="bullet">
        /// <item><term><see cref="TagStringInputParams"/></term>
        /// <description>Used to customize input of a string value.</description></item>
        /// <item><term><see cref="TagDateTimeInputParams"/></term>
        /// <description>Used to customize input of a <see cref="DateTime"/> value.</description></item>
        /// <item><term><see cref="TagListInputParams"/></term>
        /// <description>Allows to use a ListBox or ComboBox for input of the tag's value.</description></item>
        /// <item><term><see cref="TagBoolInputParams"/></term>
        /// <description>Allows to input a Boolean value using a check box.</description></item>
        /// <item><term><see cref="TagNumericInputParams"/></term>
        /// <description>Used to customize input of a integer value.</description></item>
        /// </list>
        /// </remarks>
        [Browsable(false)]
        public TagInputParams InputParams
        {
            get 
			{
				return _inputParams;
			}
        }		
		#endregion
	}

	/// <summary>
	/// The abstract base class for specialized classes used by the
	/// tag input dialog to customize input of individual tags.
	/// </summary>
	public abstract class TagInputParams 
    {
		private bool _isShowLabel;

		/// <summary>
		/// Gets or sets a value indicating whether the input form should show the label with the tag's
		/// description adjacent to the input control.
		/// </summary>
		/// <returns>True if the label is to be shown, false otherwise.</returns>
		public bool IsShowLabel
		{
			get
			{
				return _isShowLabel;
			}
			set
			{
				_isShowLabel = value;
			}
		}
	}

    /// <summary>
    /// Represents customization parameters for input of string tag values.
    /// </summary>
    public partial class TagStringInputParams : TagInputParams
    {
        internal const int c_DefMaxLength = 32767;
        private int _maxLength = c_DefMaxLength;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagStringInputParams"/> class.
        /// </summary>
        public TagStringInputParams()
        {
        }

         /// <summary>
        /// Gets or sets the maximum length of the input string.
        /// </summary>
        public int MaxLength
        {
            get { return _maxLength; }
            set { _maxLength = value; }
        }
    }

    /// <summary>
    /// Represents customization parameters for input of <see cref="DateTime"/> tag values.
    /// </summary>
    public partial class TagDateTimeInputParams : TagInputParams
    {
        internal const int c_DefFormat = 1;
  /*      internal readonly static DateTime c_DefMinDate = DateTime.MinValue;
        internal readonly static DateTime c_DefMaxDate = DateTime.MaxValue;*/

    /*    private DateTime _minDate = c_DefMinDate;
        private DateTime _maxDate = c_DefMaxDate;*/
        private int _format = c_DefFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagDateTimeInputParams"/> class.
        /// </summary>
        public TagDateTimeInputParams()
        {
        }

  /*      /// <summary>
        /// Gets or sets the minimum allowed date.
        /// </summary>
        public DateTime MinDate
        {
            get { return _minDate; }
            set { _minDate = value; }
        }

        /// <summary>
        /// Gets or sets the maximum allowed date.
        /// </summary>
        public DateTime MaxDate
        {
            get { return _maxDate; }
            set { _maxDate = value; }
        }*/

        /// <summary>
        /// Gets or sets the format to use in the date/time picker.
		///		Long = 1 - the long date format set by the user's operating system,
		///		Short = 2 - the short date format set by the user's operating system,
		///		Time = 4 - the time format set by the user's operating system,
		///		Custom = 8 - the date/time value in a custom format.
        /// </summary>
        public int Format
        {
            get { return _format; }
            set { _format = value; }
        }
    }

    /// <summary>
    /// Represents customization parameters for input of tag values that can be selected from a list.
    /// </summary>
    public partial class TagListInputParams : TagInputParams
    {
        internal const int c_DefType = 1; //TagListInputParamsTypeEnum.ComboBox;

        private int _type = c_DefType;
        private List<object> _items;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagListInputParams"/> class.
        /// </summary>
        public TagListInputParams()
        {
			_items = new List<object>();
        }

        /// <summary>
        /// Gets or sets the type of list input to use.
		///   ListBox = 0,
        ///   ComboBox = 1/
        /// </summary>
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// Gets the collection of list items.
        /// </summary>
		public List<object> Items
        {
            get
            {
                return _items;
            }
        }
    }

	/// <summary>
	/// Represents customization parameters for input of Boolean tag values.
	/// </summary>
	public partial class TagBoolInputParams : TagInputParams
    {
    }

	/// <summary>
	/// Represents customization parameters for input of integer tag values.
	/// </summary>
	public partial class TagNumericInputParams : TagInputParams
    {
		internal const int c_DefMinimum = int.MinValue;
		internal const int c_DefMaximum = int.MaxValue;
		internal const int c_DefIncrement = 1;

		private int _minimum = c_DefMinimum;
		private int _maximum = c_DefMaximum;
		private int _increment = c_DefIncrement;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagNumericInputParams"/> class.
        /// </summary>
        public TagNumericInputParams()
        {
        }
 
        /// <summary>
        /// Gets or sets the minimum allowed value.
        /// </summary>
        public int Minimum
        {
            get { return _minimum; }
            set { _minimum = value; }
        }

        /// <summary>
        /// Gets or sets the maximum allowed value.
        /// </summary>
		public int Maximum
        {
            get { return _maximum; }
            set { _maximum = value; }
        }

        /// <summary>
        /// Gets or sets the increment for the spin buttons.
        /// </summary>
		public int Increment
        {
            get { return _increment; }
            set { _increment = value; }
        }
     }
}

