using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.IO;

#if !WINFX
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

using C1.Framework;
using C1.Win.C1Schedule;
#else
using C1.WPF.C1Schedule;
#endif

#if END_USER_LOCALIZATION
using C1.Win.C1Schedule.Localization;
#else
	using C1.Util.Localization;
#endif

namespace C1.C1Schedule.Printing
{
	/// <summary>
	/// The <see cref="PrintStyle"/> class represents the single printing style for a schedule control.
	/// </summary>
	[TypeConverterAttribute(typeof(ExpandableObjectConverter))]
	public class PrintStyle 
	{
		#region ** fields
		private PrintStyleCollection _owner = null;
        private CalendarInfo _ci = null;
		
		private Type _type = typeof(PrintStyle);
		private bool _isLoaded;
		private string _name = String.Empty;
		private string _description = String.Empty;
		private string _displayName = String.Empty;
		private string _styleSource = String.Empty;
		private int _documentFormat = 0;
		private bool _filled = false;
		private PrintContextType _context = PrintContextType.DateRange;
		private Dictionary<string, TagInfo> _tags = new Dictionary<string, TagInfo>();

		private string _headerLeft = "";
		private string _headerCenter = "";
		private string _headerRight = "";
		private string _footerLeft = "";
		private string _footerCenter = "";
		private string _footerRight = "";

		private bool _reverseOnEvenPages = false;
		private bool _hidePrivateAppointments = false;

#if !WINFX
		private PageSettings _currentPagesettings = null;
		private Image _previewImage = null;
		private Font _dateHeadingsFont = null;
		private Font _appointmentsFont = null;
		private Font _headerFont = null;
		private Font _footerFont = null;
		private Color _headerColor = Color.Black;
		private Color _footerColor = Color.Black;
#else
#endif
		#endregion

		#region ** ctor
		/// <summary>
		/// Initializes the new <see cref="PrintStyle"/> object.
		/// </summary>
		public PrintStyle()
		{
		}
		#endregion

		#region ** public properties
		/// <summary>
		/// Gets or sets the <see cref="String"/> value, determining the unique style name for using in
		/// <see cref="PrintStyleCollection"/>. 
		/// </summary>
		[ParenthesizePropertyName(true)]
		[C1Category("Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[DefaultValue("")]
		public string StyleName
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="String"/> value, determining style description.
		/// </summary>
		[C1Category("Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[DefaultValue("")]
		public string Description
		{
			get
			{
				if (String.IsNullOrEmpty(_description))
				{
					_description = _name;
				}
				return _description;
			}
			set
			{
				_description = value;
			}
		}
		
		/// <summary>
		/// Gets or sets the <see cref="String"/> value, determining style display name.
		/// </summary>
		/// <remarks>For default styles this value is initialized with the value of 
		/// the <see cref="Description"/> property localized according to the owning control's culture.</remarks>
		[C1Category("Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DefaultValue("")]
		[Browsable(false)]
		public string DisplayName
		{
			get
			{
				if (String.IsNullOrEmpty(_displayName))
				{
					_displayName = Description;
				}
				return _displayName;
			}
			set
			{
				_displayName = value;
			}
		}

		[Obfuscation(Exclude = true)]
		private bool ShouldSerializeDescription()
		{
			if ( String.IsNullOrEmpty(_description) || _name.Equals(_description))
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Gets an <see cref="int"/> value determining the format of source document.
		/// Returns 0 for .c1d and 1 for .c1dx documents.
		/// </summary>
		[Browsable(false)]
		public int DocumentFormat
		{
			get
			{
				return _documentFormat;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="PrintContextType"/> value, specifying whether the current
		/// <see cref="PrintStyle"/> objects displays the single appointment or appointments
		/// of the specified date range.
		/// </summary>
		[DefaultValue(PrintContextType.DateRange)]
		[C1Category("Data")]
		public PrintContextType Context
		{
			get
			{
				return _context;
			}
			set
			{
				_context = value;
			}
		}

#if !WINFX
		/// <summary>
		/// Gets or sets a <see cref="Font"/> object used for printing date headings.
		/// </summary>
		[DefaultValue(null)]
		[C1Category("Appearance")]
		public Font DateHeadingsFont
		{
			get
			{
				return _dateHeadingsFont;
			}
			set
			{
				_dateHeadingsFont = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Font"/> object used for printing appointments.
		/// </summary>
		[DefaultValue(null)]
		[C1Category("Appearance")]
		public Font AppointmentsFont
		{
			get
			{
				return _appointmentsFont;
			}
			set
			{
				_appointmentsFont = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Font"/> object used for printing page headers.
		/// </summary>
		[DefaultValue(null)]
		[C1Category("Appearance")]
		public Font HeaderFont
		{
			get
			{
				return _headerFont;
			}
			set
			{
				_headerFont = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Font"/> object used for printing page footers.
		/// </summary>
		[DefaultValue(null)]
		[C1Category("Appearance")]
		public Font FooterFont
		{
			get
			{
				return _footerFont;
			}
			set
			{
				_footerFont = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Color"/> value used for printing page headers.
		/// </summary>
		[DefaultValue(typeof(Color), "Black")]
		[C1Category("Appearance")]
		public Color HeaderColor
		{
			get
			{
				return _headerColor;
			}
			set
			{
				_headerColor = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Color"/> value used for printing page footers.
		/// </summary>
		[DefaultValue(typeof(Color), "Black")]
		[C1Category("Appearance")]
		public Color FooterColor
		{
			get
			{
				return _footerColor;
			}
			set
			{
				_footerColor = value;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Image"/> object, representing a small preview of the style 
		/// which can be used for displaying style information in UI..
		/// </summary>
		[DefaultValue(null)]
		[C1Category("Data")]
		public Image PreviewImage
		{
			get
			{
				return _previewImage;
			}
			set
			{
				_previewImage = value;
			}
		}

		/// <summary>
		/// Gets or sets the PageSettings for printing.
		/// </summary>
		/// <remarks>If end-user changes style PageSettings at run-time, 
		/// the changed settings will be used as default at every next printing or previewing.</remarks>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public PageSettings CurrentPageSettings
		{
			get
			{
				return _currentPagesettings;
			}
			set
			{
				_currentPagesettings = value;
			}
		}
#endif

		/// <summary>
		/// Gets or sets a <see cref="String"/> value used in the left part of the page header.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string HeaderLeft
		{
			get
			{
				return _headerLeft;
			}
			set
			{
				_headerLeft = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="String"/> value used in the center part of the page header.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string HeaderCenter
		{
			get
			{
				return _headerCenter;
			}
			set
			{
				_headerCenter = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="String"/> value used in the right part of the page header.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string HeaderRight
		{
			get
			{
				return _headerRight;
			}
			set
			{
				_headerRight = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="String"/> value used in the left part of the page footer.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string FooterLeft
		{
			get
			{
				return _footerLeft;
			}
			set
			{
				_footerLeft = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="String"/> value used in the center part of the page footer.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string FooterCenter
		{
			get
			{
				return _footerCenter;
			}
			set
			{
				_footerCenter = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="String"/> value used in the right part of the page footer.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string FooterRight
		{
			get
			{
				return _footerRight;
			}
			set
			{
				_footerRight = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Boolean"/> value determining whether 
		/// page headers and footers should be reversed on even pages.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Behavior")]
		public bool ReverseOnEvenPages
		{
			get
			{
				return _reverseOnEvenPages;
			}
			set
			{
				_reverseOnEvenPages = value;
			}
		}

		/// <summary>
		/// Gets or sets a <see cref="Boolean"/> value determining whether control should
		/// hide details of private appointments in resulting document.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Behavior")]
		[C1Description("Printing.HidePrivateAppointments", "Determines whether the private appointments should be printed.")]
		public bool HidePrivateAppointments
		{
			get
			{
				return _hidePrivateAppointments;
			}
			set
			{
				_hidePrivateAppointments = value;
			}
		}
		
		/// <summary>
		/// Gets or sets the <see cref="String"/> value determining the source of
		/// C1PrintDocument template. It might be the name of .c1d or .c1dx file or the name
		/// of resource.
		/// </summary>
		[DefaultValue("")]
		[C1Category("Data")]
		public string StyleSource
		{
			get
			{
				return _styleSource;
			}
			set
			{
				_styleSource = value;
				if (_styleSource.EndsWith(".c1dx", StringComparison.OrdinalIgnoreCase))
				{
					_documentFormat = 1;
				}
				else
				{
					_documentFormat = 0;
				}
			}
		}

		/// <summary>
		/// Returns true if the current style is loaded into C1PrintDocument control.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsLoaded
		{
			get
			{
				return _isLoaded;
			}
			internal set
			{
				_isLoaded = value;
			}
		}

		/// <summary>
		/// Gets a <see cref="Dictionary{String, TagInfo}"/> dictionary, containing information 
		/// about C1PrintDocument tags.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Dictionary<string, TagInfo> Tags
		{
			get
			{
				return _tags;
			}
		}
		#endregion

		#region ** public methods
		/// <summary>
		/// Loads style definition to the specified C1PrintDocument control.
		/// </summary>
		/// <param name="printDoc">The <see cref="PrintDocumentWrapper"/> object.</param>
		/// <returns>Returns true at successful loading; false - otherwise.</returns>
		public bool Load(PrintDocumentWrapper printDoc)
		{
			if (String.IsNullOrEmpty(StyleSource))
			{
				return false;
			}
			printDoc.Clear();
			
			// load document
			Stream sr = ResourceLoader.GetStream(StyleSource);
			if (sr != null)
			{
				printDoc.Load(sr, DocumentFormat);
				sr.Dispose();
			}
			else
			{
				printDoc.Load(StyleSource, DocumentFormat);
			}
			printDoc.ReadTags(ref _tags);

			if (String.IsNullOrEmpty(StyleName))
			{
				// fill name and description from document
				StyleName = (string)printDoc.GetDocumentInfoProperty("Title");
				if (String.IsNullOrEmpty(StyleName))
				{
					StyleName = _owner.GetUniqueStyleName();
				}
			}
			if (String.IsNullOrEmpty(_description))
			{
				_description = (string)printDoc.GetDocumentInfoProperty("Subject");
			}

#if !WINFX
			if (CurrentPageSettings != null)
			{
				// set page settings for document
				printDoc.SetPageSettings(CurrentPageSettings);
			}
#endif

			UpdateTagsFromStyle();
			IsLoaded = true;
			return true;
		}

		/// <summary>
		/// Returns a <see cref="Boolean"/> value, determining whether the specified tag in one of 
		/// the predefined tags used by the <see cref="PrintStyle"/> and PrintOptionsForm classes.
		/// </summary>
		/// <param name="tag"></param>
		/// <returns></returns>
		public bool IsStandardTag(TagInfo tag)
		{
			string name = tag.Name;
			PropertyInfo pi = _type.GetProperty(name);
			if (pi != null && pi.PropertyType == tag.Type)
			{
				return true;
			}
			return name == "StartDate" || name == "EndDate";
		}
		#endregion

		#region private stuff
		internal PrintStyleCollection Owner
		{
			get
			{
				return _owner;
			}
			set
			{
				_owner = value;
			}
		}

        internal CalendarInfo CalendarInfo
        {
            get
            {
                return _ci;
            }
            set
            {
                _ci = value;
            }
        }

		internal void FillStyleFromTags()
		{
			if (_filled)
			{
				return;
			}
			foreach (TagInfo tag in _tags.Values)
			{
				FillStylePropertyFromTag(tag);
			}

			_filled = true;
		}

		internal void UpdateTagsFromStyle()
		{
			if (_tags.Count == 0 )
			{
				return;
			}
			FillStyleFromTags();
			foreach (TagInfo tag in _tags.Values)
			{
				UpdateTagFromStyle(tag);
			}
		}

		// Set date range tags in a separate method, as it might
		// be useful to set them before editing style in print setup forms.
		internal void SetDateRangeTags(DateTime start, DateTime end)
		{
			TagInfo tag = null;
			if ((Context & PrintContextType.DateRange) != 0)
			{
				if (Tags.ContainsKey("StartDate"))
				{
					tag = Tags["StartDate"];
					if (tag != null && tag.Type == typeof(DateTime))
					{
						tag.Value = start;
					}
				}
				if (Tags.ContainsKey("EndDate"))
				{
					tag = Tags["EndDate"];
					if (tag != null && tag.Type == typeof(DateTime))
					{
						tag.Value = end;
					}
				}
			}
		}

		// Use this method to set all tags at document starting.
		// Note: start and end parameters are just default values. 
		// If document contains StartDate and EndDate tags, 
		// actual start and end values go from the tags.
		internal void SetupTags(AppointmentCollection appointmentCollection, 
			List<Appointment> appointmentList, DateTime start, DateTime end, 
			bool hidePrivateAppointments, CalendarInfo calendarInfo, 
			bool selectedGroup, BaseObject selectedGroupOwner, string groupBy, Comparison<Appointment> comparision)
		{
			TagInfo tag = null;
			if (Tags.ContainsKey("Appointment"))
			{
				tag = Tags["Appointment"];
				if (tag != null && tag.Type == typeof(Appointment)
					&& appointmentList != null && appointmentList.Count > 0)
				{
					tag.Value = appointmentList[0];
				}
			}
			if (Tags.ContainsKey("Appointments"))
			{
				tag = Tags["Appointments"];
				if (tag != null)
				{
					if (tag.Type == typeof(AppointmentCollection))
					{
						tag.Value = appointmentCollection;
					}
					else if (tag.Type == typeof(IList<Appointment>))
					{
						if ((Context & PrintContextType.Appointment) != 0
							&& appointmentList != null)
						{
                            appointmentList.Sort(comparision);
							tag.Value = appointmentList;
						}
						else
						{
							TagInfo tag1 = null;
							if (Tags.ContainsKey("StartDate"))
							{
								tag1 = Tags["StartDate"];
								if (tag1 != null && tag1.Type == typeof(DateTime))
								{
									start = (DateTime)tag1.Value;
								}
							}
							if (Tags.ContainsKey("EndDate"))
							{
								tag1 = Tags["EndDate"];
								if (tag1 != null && tag1.Type == typeof(DateTime))
								{
									end = (DateTime)tag1.Value;
								}
							}
							// get appointments for the currently selected SchedulerGroupItem if any, 
							// or all appointments otherwise.
							AppointmentList list;
							if (selectedGroup)
							{
								list = appointmentCollection.GetOccurrences(selectedGroupOwner,
									groupBy, start, end.AddDays(1), !hidePrivateAppointments);
							}
							else
							{
								list = appointmentCollection.GetOccurrences(
									null, string.Empty, start, end.AddDays(1), !hidePrivateAppointments);
							}
                            list.Sort(comparision);
							tag.Value = list;
						}
					}
				}
			}
			if (Tags.ContainsKey("CalendarInfo"))
			{
				tag = Tags["CalendarInfo"];
				if (tag != null && tag.Type == typeof(CalendarInfo))
				{
					tag.Value = calendarInfo;
				}
			}
			if (Tags.ContainsKey("HidePrivateAppointments"))
			{
				tag = Tags["HidePrivateAppointments"];
				if (tag != null && tag.Type == typeof(bool))
				{
					tag.Value = hidePrivateAppointments;
				}
			}
		}

		private void FillStylePropertyFromTag(TagInfo tag)
		{
			string name = tag.Name;
			PropertyInfo pi = _type.GetProperty(name);
			if (pi != null && pi.PropertyType == tag.Type)
			{
				object defaultValue = null;
				object[] attrs = pi.GetCustomAttributes(typeof(DefaultValueAttribute), false);
				if (attrs.Length > 0)
				{
					DefaultValueAttribute dva = attrs.GetValue(0) as DefaultValueAttribute;
					if (dva != null)
					{
						defaultValue = dva.Value;
					}
				}
				object actualValue = pi.GetValue(this, null);
				if (actualValue == null || (actualValue != null && actualValue.Equals(defaultValue)))
				{
					pi.SetValue(this, tag.Value, null);
				}
			}
		}

		private void UpdateTagFromStyle(TagInfo tag)
		{
			string name = tag.Name;
			PropertyInfo pi = _type.GetProperty(name);
            if (pi != null && pi.PropertyType == tag.Type)
            {
                tag.Value = pi.GetValue(this, null);
            }
            else
            {
                // string tags
                if (tag.Type == typeof(string))
                {
                    string str = Strings.PrintStyleTagStrings.TryFindValue((string)tag.Value, (_ci == null ? System.Globalization.CultureInfo.CurrentCulture : _ci.CultureInfo));
                    if (!string.IsNullOrEmpty(str))
                    {
                        tag.Value = str;
                    }
                }
            }
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="PrintStyleCollection"/> class represents the keyed collection of scheduler printing styles.
	/// The <see cref="PrintStyle.StyleName"/> property is used as a collection key.
	/// </summary>
	[TypeConverterAttribute(typeof(CollectionConverter))]
	public class PrintStyleCollection : KeyedCollection<string, PrintStyle>
	{
		#region ** fields
		private static string _defaultStyleName = "Unknown Style";
		#endregion

		#region ** public interface
		/// <summary>
		/// Fills collection with default printing styles.
		/// </summary>
		public void LoadDefaults()
		{
			Clear();

			PrintStyle st = new PrintStyle();
			st.StyleName = "Daily";
			st.Description = "Daily Style";
			st.StyleSource = "day.c1d";
			st.Owner = this;
#if !WINFX
			st.PreviewImage = C1.Framework.ResourceLoader.GetImage("daily.png");
#endif
			Add(st);

			st = new PrintStyle();
			st.StyleName = "Week";
			st.Description = "Weekly Style";
			st.StyleSource = "week.c1d";
			st.Owner = this;
#if !WINFX
			st.PreviewImage = C1.Framework.ResourceLoader.GetImage("week.png");
#endif
			Add(st);

			st = new PrintStyle();
			st.StyleName = "Month";
			st.Description = "Monthly Style";
			st.StyleSource = "month.c1d";
			st.Owner = this;
#if !WINFX
			st.PreviewImage = C1.Framework.ResourceLoader.GetImage("month.png");
#endif
			Add(st);

			st = new PrintStyle();
			st.StyleName = "Details";
			st.Description = "Details Style";
			st.StyleSource = "details.c1d";
			st.Owner = this;
#if !WINFX
			st.PreviewImage = C1.Framework.ResourceLoader.GetImage("details.png");
#endif
			Add(st);

			st = new PrintStyle();
			st.StyleName = "Memo";
			st.Context = PrintContextType.Appointment;
			st.Description = "Memo Style";
			st.StyleSource = "memo.c1d";
			st.Owner = this;
#if !WINFX
			st.PreviewImage = C1.Framework.ResourceLoader.GetImage("memo.png");
#endif
			Add(st);
		}

		/// <summary>
		/// Adds a set of <see cref="PrintStyle"/> objects to the collection.
		/// </summary>
		/// <param name="items">An array of type <see cref="PrintStyle"/>
		/// that contains the print styles to add. </param>
		public void AddRange(PrintStyle[] items)
		{
			Clear();
			foreach (PrintStyle item in items)
			{
				Add(item);
			}
		}

		/// <summary>
		/// Returns the <see cref="String"/> value which can be used
		/// as unique style name.
		/// </summary>
		public string GetUniqueStyleName()
		{
			string name = _defaultStyleName;
			int index = 0;
			while (Contains(name))
			{
				index++;
				name = _defaultStyleName + " " + index.ToString();
			}
			return name;
		}
		#endregion

		#region ** private stuff
		/// <summary>
		/// Extracts the key from the specified item.
		/// </summary>
		/// <param name="item">The <see cref="PrintStyle"/> object from which to extract the key.</param>
		/// <returns>The key for the specified item.</returns>
		protected override string GetKeyForItem(PrintStyle item)
		{
			return item.StyleName;
		}

		/// <summary>
		/// Inserts the specified item in the collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index where the item is to be inserted.</param>
		/// <param name="item">The item to insert in the collection.</param>
		protected override void InsertItem(int index, PrintStyle item)
		{
			item.Owner = this;
			base.InsertItem(index, item);
		}
		#endregion
	}
}