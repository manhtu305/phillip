﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="Resource"/> class represents the resource which can be
	/// associated with the <see cref="Appointment"/> object. 
	/// </summary>
#if (!SILVERLIGHT)
	[Serializable]
#endif
	public class Resource : BaseObject
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Resource"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Int32"/> value which should be used as resource key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Resource(int key)
            : this()
        {
            base.SetIndex(key);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Resource"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Guid"/> value which should be used as resource key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Resource(Guid key)
            : this()
        {
            base.SetId(key);
        }

        /// <summary>
		/// Creates the new <see cref="Resource"/> object.
		/// </summary>
		public Resource()
		{
		}

        // todo: add ResourceType property (string) ?
        // add CustomData

#if (!SILVERLIGHT)
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Resource(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
#endif
	}

	/// <summary>
	/// The <see cref="ResourceCollection"/> is a collection of <see cref="Resource"/> 
	/// objects which represents all available resources in C1Schedule object model.
	/// </summary>
	public class ResourceCollection : BaseCollection<Resource>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResourceCollection"/> class.
		/// </summary>
		public ResourceCollection(): this(null) 
		{
		}
        internal ResourceCollection(object owner): base(owner) 
        {
        }
    }

	/// <summary>
	/// The <see cref="ResourceList"/> is a list of <see cref="Resource"/> objects.
	/// Only objects existing in the owning <see cref="ResourceCollection"/> object 
	/// may be added to this list.
	/// Use the <see cref="ResourceList"/> to associate the set of <see cref="Resource"/> objects 
	/// with an <see cref="Appointment"/> object.
	/// </summary>
	public class ResourceList : BaseList<Resource>
	{
		internal ResourceList(ResourceCollection owner)
			: base(owner)
		{
		}

		internal ResourceList(ResourceList list)
			: base(list)
		{
		}

		internal new ResourceCollection Owner
		{
			get
			{
				return (ResourceCollection)base.Owner;
			}
		}
	}
}
