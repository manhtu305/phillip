using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml;
using System.ComponentModel;
using System.Reflection;
using System.Security.Permissions;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	#region enums
	/// <summary>
	/// Determines the types of recurrent appointments.
	/// </summary>
	public enum RecurrenceTypeEnum
	{
		/// <summary>
		/// The recurring appointment reoccurs on a daily basis.
		/// </summary>
		Daily,
		/// <summary>
		/// The recurring appointment reoccurs every working day.
		/// </summary>
		Workdays,
		/// <summary>
		/// The recurring appointment reoccurs on a monthly basis. 
		/// </summary>
		Monthly,
		/// <summary>
		/// The recurring appointment reoccurs every N months. 
		/// </summary>
		MonthlyNth,
		/// <summary>
		/// The recurring appointment reoccurs on a weekly basis.
		/// </summary>
		Weekly,
		/// <summary>
		/// The recurring appointment reoccurs on an yearly basis.
		/// </summary>
		Yearly,
		/// <summary>
		/// The recurring appointment reoccurs every N years.
		/// </summary>
		YearlyNth
	}

	/// <summary>
	/// Determines the set of days and groups of days for recurrence patterns. 
	/// </summary>
	[Flags]
	public enum WeekDaysEnum
	{
		/// <summary>
		/// No specific value; the actual value is obtained from 
		/// the root <see cref="Appointment"/> object. 
		/// </summary>
		None = 0,
		/// <summary>
		/// Specifies Sunday.
		/// </summary>
		Sunday = 1,
		/// <summary>
		/// Specifies Monday.
		/// </summary>
		Monday = 2,
		/// <summary>
		/// Specifies Tuesday. 
		/// </summary>
		Tuesday = 4,
		/// <summary>
		/// Specifies Wednesday.
		/// </summary>
		Wednesday = 8,
		/// <summary>
		/// Specifies Thursday.
		/// </summary>
		Thursday = 16,
		/// <summary>
		/// Specifies Friday.
		/// </summary>
		Friday = 32,
		/// <summary>
		/// Specifies Saturday.
		/// </summary>
		Saturday = 64,
		/// <summary>
		/// Specifies Saturday and Sunday (or what ever days according 
		/// to the settings of the C1Schedule).
		/// </summary>
		WeekendDays = 129,
		/// <summary>
		/// Specifies work days (all days except weekend).
		/// </summary>
		WorkDays = 128,
		/// <summary>
		/// Specifies every day of the week.
		/// </summary>
		EveryDay = 127
	}

	/// <summary>
	/// Determines the week in a month in which the appointment will occur.
	/// </summary>
	public enum WeekOfMonthEnum
	{
		/// <summary>
		/// The recurring appointment will occur on the specified 
		/// day or days of the first week in the month. 
		/// </summary>
		First = 1,
		/// <summary>
		/// The recurring appointment will occur on the specified 
		/// day or days of the second week in the month. 
		/// </summary>
		Second = 2,
		/// <summary>
		/// The recurring appointment will occur on the specified 
		/// day or days of the third week in the month. 
		/// </summary>
		Third = 3,
		/// <summary>
		/// The recurring appointment will occur on the specified 
		/// day or days of the fourth week in the month. 
		/// </summary>
		Fourth = 4,
		/// <summary>
		/// The recurring appointment will occur on the specified 
		/// day or days of the last week in the month. 
		/// </summary>
		Last = 5
	}
	#endregion

	/// <summary>
	/// The <see cref="RecurrencePattern"/> class contains information 
	/// that describes the recurrence pattern and range 
	/// of the associated <see cref="Appointment"/> object. 
	/// </summary>
#if (!SILVERLIGHT)
	[Serializable]
#endif
	public class RecurrencePattern : INotifyPropertyChanged
#if (!SILVERLIGHT)
		, ISerializable  
#endif
	{
        #region events
		// in order to avoid serialization of registered delegates for the next event
#if (!SILVERLIGHT)
		[field: NonSerialized()]
#endif
		private event PropertyChangedEventHandler PropertyChanged;
		event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { PropertyChanged += value; }
            remove { PropertyChanged -= value; }
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
			SetDirty(!_changed && propertyName != "Description");
			if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            if (propertyName != "Description")
            {
				_description = "";
                OnPropertyChanged("Description");
            }
        }
        #endregion

        #region fields
		private static DateTime MaxDate = DateTime.MaxValue.Date;

		private Appointment _owner;

		private RecurrenceTypeEnum _recurrenceType;
		private int _interval;
		private int _occurrences;// = 10;
		// _adjustedOccurrences stores the number of occurrences with the changed date
		// it's required to account occurrences
		internal int _adjustedOccurrences = 0;
		private WeekDaysEnum _dayOfWeekMask;
		private int _dayOfMonth;
		private int _monthOfYear;
		private bool _noEndDate;
		private AppointmentList _exceptions;
		private AppointmentList _removed;
		private WeekOfMonthEnum _instance;

		// appointment times
		private DateTime _start;
		private TimeSpan _duration;

		// pattern times
		private DateTime _patternStartDate;
		private DateTime _patternEndDate;

		// lazy 
		private DateTime _patternReferencePoint; // first day of week, month or year depending of recurrence type
		private List<DayOfWeek> _listDays;
		private bool _changed;
		private string _description;
		private CalendarInfo _info;
		private int count;
		bool _countingBeforeFirstDate = false;
		#endregion

		#region ctor
		internal RecurrencePattern(Appointment owner)
		{
			_owner = owner;
			_recurrenceType = RecurrenceTypeEnum.Weekly;
			_dayOfWeekMask = 0;
			_interval = 1;
			_instance = WeekOfMonthEnum.First;
			_dayOfMonth = 1;
			_monthOfYear = 1;
			_noEndDate = true;
			_start = owner.Start;
			_patternStartDate = _start.Date; 
			_patternEndDate = _patternStartDate.AddMonths(2);
			_dayOfWeekMask = (WeekDaysEnum)Enum.Parse(typeof(WeekDaysEnum), Enum.GetName(typeof(DayOfWeek), _start.DayOfWeek), true);
			_duration = owner.Duration;
			_occurrences = 0;
			_exceptions = new AppointmentList(owner.ParentCollection);
			_removed = new AppointmentList(owner.ParentCollection);
			_info = Info;
			_listDays = new List<DayOfWeek>();
			_description = "";
			_changed = true;
		}
		#endregion

		#region object model
		internal Appointment Owner
		{
			get
			{
				return _owner;
			}
			set
			{
				_owner = value;
				_exceptions.ParentCollection = 
					_removed.ParentCollection =	_owner == null ? null : _owner.ParentCollection;
				foreach (Appointment exception in _exceptions)
				{
					exception.ParentRecurrence = exception.ParentRecurrence ?? _owner;
				}
				foreach (Appointment removed in _removed)
				{
					removed.ParentRecurrence = removed.ParentRecurrence ?? _owner;
				}
			}
		}

		internal CalendarInfo Info
		{
			get
			{
				if (_info == null && _owner.ParentCollection != null)
				{
					_info = _owner.ParentCollection.CalendarInfo;
					SetDirty();
				}
                return _info;
			}
			set
			{
				if (_info != value)
				{
					_info = value;
					SetDirty();
				}
			}
		}
		
		/// <summary>
		/// Gets the <see cref="Appointment"/> object which represents
		/// the master appointment for this <see cref="RecurrencePattern"/> object.
		/// </summary>
		public Appointment ParentAppointment
		{
			get
			{
				return _owner;
			}
		}

		/// <summary>
		/// Gets the human-readable description of the <see cref="RecurrencePattern"/> object.
		/// </summary>
		public string Description
		{
			get
			{
				return GetDescription((Info ?? new CalendarInfo()).CultureInfo);
			}
		}

		/// <summary>
		/// Gets the human-readable description of the <see cref="RecurrencePattern"/> object
		/// according to the specified culture.
		/// </summary>
		/// <param name="culture">The <see cref="CultureInfo"/> object used for getting calendar specific strings.</param>
		/// <returns>The <see cref="String"/> value described the <see cref="RecurrencePattern"/> object.</returns>
		public string GetDescription(CultureInfo culture)
		{
			if (_changed)
			{
				Count();
			}
			if (_description.Length == 0)
			{
				switch (RecurrenceType)
				{
					case RecurrenceTypeEnum.Daily:
						if (_interval == 1)
						{
#if END_USER_LOCALIZATION
							_description = Strings.C1Schedule.Strings.Item("Daily", culture);
#elif WINFX
							_description = C1Localizer.GetString("MiscStrings", "Daily", "Occurs every day", culture);
#elif SILVERLIGHT
                            _description = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Daily;
#else
							_description = C1Localizer.GetString("Occurs every day");
#endif
						}
						else
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("EveryNDay", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "EveryNDay", "Occurs every {0} days", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.EveryNDay,
#else
								C1Localizer.GetString("Occurs every {0} days"),
#endif
								_interval.ToString(culture));
						}
						break;
					case RecurrenceTypeEnum.Workdays:
#if END_USER_LOCALIZATION
						_description = Strings.C1Schedule.Strings.Item("OnWorkingDays", culture);
#elif WINFX
						_description = C1Localizer.GetString("MiscStrings", "OnWorkingDays", "Occurs every week day", culture);
#elif SILVERLIGHT
                        _description = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.OnWorkingDays;
#else
						_description = C1Localizer.GetString("Occurs every week day");
#endif
						break;
					case RecurrenceTypeEnum.Monthly:
						if (_interval == 1)
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("Monthly", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "Monthly", "Occurs day {0} of every 1 month", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Monthly,
#else
								C1Localizer.GetString("Occurs day {0} of every 1 month"),
#endif
								_dayOfMonth.ToString(culture));
						}
						else
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("EveryNMonth", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "EveryNMonth", "Occurs day {1} of every {0} months", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.EveryNMonth,
#else
								C1Localizer.GetString("Occurs day {1} of every {0} months"),
#endif
								_interval.ToString(culture), _dayOfMonth.ToString(culture));
						}
						break;
					case RecurrenceTypeEnum.MonthlyNth:
						if (_listDays.Count == 0)
						{
							return GetIncorrectPatternMessage(culture);
						}
						if (_interval == 1)
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("MonthlyOnTheNWeekday", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "MonthlyOnTheNWeekday", "Occurs the {0} {1} of every 1 month", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.MonthlyOnTheNWeekday,
#else
								C1Localizer.GetString("Occurs the {0} {1} of every 1 month"),
#endif
								CalendarInfo.GetInstanceName(_instance, culture),
								GetDayOfWeekMaskDescription(culture));
						}
						else
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("EveryNMonthOnTheNWeekday", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "EveryNMonthOnTheNWeekday", "Occurs the {1} {2} of every {0} months", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.EveryNMonthOnTheNWeekday,
#else
								C1Localizer.GetString("Occurs the {1} {2} of every {0} months"),
#endif
								_interval.ToString(culture), CalendarInfo.GetInstanceName(_instance, culture),
								GetDayOfWeekMaskDescription(culture));
						}
						break;
					case RecurrenceTypeEnum.Weekly:
						if (_interval == 1)
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("Weekly", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "Weekly", "Occurs every {0}", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Weekly,
#else
								C1Localizer.GetString("Occurs every {0}"), 
#endif
								GetDayOfWeekMaskDescription(true, culture));
						}
						else
						{
							_description = string.Format(
#if END_USER_LOCALIZATION
								Strings.C1Schedule.Strings.Item("EveryNWeek", culture),
#elif WINFX
								C1Localizer.GetString("MiscStrings", "EveryNWeek", "Occurs every {0} weeks on {1}", culture),
#elif SILVERLIGHT
                                C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.EveryNWeek,
#else
								C1Localizer.GetString("Occurs every {0} weeks on {1}"),
#endif
								_interval.ToString(culture), GetDayOfWeekMaskDescription(true, culture));
						}
						break;
					case RecurrenceTypeEnum.Yearly:
						_description = string.Format(
#if END_USER_LOCALIZATION
							Strings.C1Schedule.Strings.Item("Yearly", culture),
#elif WINFX
							C1Localizer.GetString("MiscStrings", "Yearly", "Occurs every {1} {0}", culture),
#elif SILVERLIGHT
                            C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Yearly,
#else
							C1Localizer.GetString("Occurs every {1} {0}"),
#endif
                            // don't use genitive name as different platforms return different values
							_dayOfMonth.ToString(culture), CalendarInfo.GetMonthName(_monthOfYear, culture));
						break;
					case RecurrenceTypeEnum.YearlyNth:
						if (_listDays.Count == 0)
						{
							return GetIncorrectPatternMessage(culture);
						}
						_description = string.Format(
#if END_USER_LOCALIZATION
							Strings.C1Schedule.Strings.Item("YearlyNth", culture),
#elif WINFX
							C1Localizer.GetString("MiscStrings", "YearlyNth", "Occurs the {0} {1} of {2}", culture),
#elif SILVERLIGHT
                            C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.YearlyNth,
#else
							C1Localizer.GetString("Occurs the {0} {1} of {2}"),
#endif
							CalendarInfo.GetInstanceName(_instance, culture),
							GetDayOfWeekMaskDescription(culture),
							CalendarInfo.GetMonthName(_monthOfYear, culture));
                        // don't use genitive name as different platforms return different values
						break;
				}
				// add pattern timetable
				if (NoEndDate)
				{
					_description += string.Format(
#if END_USER_LOCALIZATION
						Strings.C1Schedule.Strings.Item("Since", culture),
#elif WINFX
						C1Localizer.GetString("MiscStrings", "Since", " effective {0} from {1} to {2}.", culture),
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Since,
#else
						C1Localizer.GetString(" effective {0} from {1} to {2}."),
#endif
						PatternStartDate.ToShortDateString(),
						StartTime.ToShortTimeString(),
						EndTime.ToShortTimeString());
				}
				else if (Occurrences > 0)
				{
					_description += string.Format(
#if END_USER_LOCALIZATION
						Strings.C1Schedule.Strings.Item("SinceNTimes", culture),
#elif WINFX
						C1Localizer.GetString("MiscStrings", "SinceNTimes", " effective {0} from {1} to {2}, {3} times.", culture),
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.SinceNTimes,
#else
						C1Localizer.GetString(" effective {0} from {1} to {2}, {3} times."),
#endif
						PatternStartDate.ToShortDateString(),
						StartTime.ToShortTimeString(),
						EndTime.ToShortTimeString(),
						Occurrences.ToString(culture));
				}
				else
				{
					_description += string.Format(
#if END_USER_LOCALIZATION
						Strings.C1Schedule.Strings.Item("SinceUntil", culture),
#elif WINFX
						C1Localizer.GetString("MiscStrings", "SinceUntil", " effective {0} until {1} from {2} to {3}.", culture),
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.SinceUntil,
#else
						C1Localizer.GetString(" effective {0} until {1} from {2} to {3}."),
#endif
						PatternStartDate.ToShortDateString(),
						PatternEndDate.ToShortDateString(),
						StartTime.ToShortTimeString(),
						EndTime.ToShortTimeString());
				}
			}
			return _description;
		}

		private string GetIncorrectPatternMessage(CultureInfo culture)
		{
#if END_USER_LOCALIZATION
			return Strings.C1Schedule.Strings.Item("IncorrectPattern", culture);
#elif WINFX
			return C1Localizer.GetString("MiscStrings", "IncorrectPattern", "incorrect recurrence pattern", culture);
#elif SILVERLIGHT
            return C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.IncorrectPattern;
#else
			return C1Localizer.GetString("incorrect recurrence pattern");
#endif
		}

		/// <summary>
		/// Gets the human-readable description of the <see cref="RecurrencePattern"/> object
		/// according to the specified <see cref="CalendarInfo"/> object.
		/// Use this method instead of <see cref="Description"/> property if the <see cref="RecurrencePattern"/> 
		/// object does not belong to any <see cref="C1Schedule"/> control.
		/// </summary>
		/// <param name="calendarInfo">The <see cref="CalendarInfo"/> object used for getting calendar specific strings.</param>
		/// <returns>The <see cref="String"/> value described the <see cref="RecurrencePattern"/> object.</returns>
		/// <remarks>If the <see cref="RecurrencePattern"/> object does not belong to the <see cref="C1Schedule"/> control,
		/// its Description property will return the default description using current user culture and calendar settings.</remarks>
		public string GetDescription(CalendarInfo calendarInfo)
		{
			Info = calendarInfo;
			return Description;
		}

		internal string GetDayOfWeekMaskDescription(CultureInfo culture)
		{
			return GetDayOfWeekMaskDescription(false, culture);
		}

		internal string GetDayOfWeekMaskDescription(bool enumeration, CultureInfo culture)
		{
			string result = "";
			if (_listDays.Count == 0)
			{
				return result;
			}
			string separator = ", ";
			if (culture.TwoLetterISOLanguageName == "ja")
			{
				separator = "/";
			}
			if (!enumeration)
			{
				if (_dayOfWeekMask == WeekDaysEnum.EveryDay || _listDays.Count == 7)
				{
#if END_USER_LOCALIZATION
					result = Strings.C1Schedule.Strings.Item("Day", culture);
#elif WINFX
					result = C1Localizer.GetString("MiscStrings", "Day", "day", culture);
#elif SILVERLIGHT
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Day;
#else
					result = C1Localizer.GetString("day");
#endif
				}
				else if (_dayOfWeekMask == WeekDaysEnum.WorkDays)
				{
#if END_USER_LOCALIZATION
					result = Strings.C1Schedule.Strings.Item("Weekday", culture);
#elif WINFX
					result = C1Localizer.GetString("MiscStrings", "Weekday", "week day", culture);
#elif SILVERLIGHT
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Weekday;
#else
					result = C1Localizer.GetString("week day");
#endif
				}
				else if (_dayOfWeekMask == WeekDaysEnum.WeekendDays)
				{
#if END_USER_LOCALIZATION
					result = Strings.C1Schedule.Strings.Item("Weekend", culture);
#elif WINFX
					result = C1Localizer.GetString("MiscStrings", "Weekend", "weekend", culture);
#elif SILVERLIGHT
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Weekend;
#else
					result = C1Localizer.GetString("weekend");
#endif
				}
				else
				{
					enumeration = true;
				}
			}
			if (enumeration)
			{
				foreach (DayOfWeek day in _listDays)
				{
					result += CalendarInfo.GetDayOfWeekName(day, culture) + separator;
				}
				result = result.Substring(0, result.Length - separator.Length);
			}
			return result;
		}

		/// <summary>
		/// Gets or sets the <see cref="RecurrenceTypeEnum"/> value
		/// determining the type of recurrence (daily, monthly, etc.).
		/// Default value is RecurrenceTypeEnum.Weekly.
		/// </summary>
		public RecurrenceTypeEnum RecurrenceType
		{
			get
			{
				return _recurrenceType;
			}
			set
			{
				if ( _recurrenceType != value)
				{
					_recurrenceType = value;
                    OnPropertyChanged("RecurrenceType"); 
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Int32"/> value specifying 
		/// the interval between occurrences of the recurrence. 
		/// The <see cref="Interval"/> property works in conjunction with 
		/// the <see cref="RecurrenceType"/> property to determine the cycle of the recurrence. 
		/// The maximum allowable value is 99 for weekly patterns and 999 for daily patterns.
		/// The default value is 1.
		/// </summary>
		///<remarks>For example, if the <see cref="RecurrenceType"/> is set 
		/// to <see cref="RecurrenceTypeEnum.Daily"/>, and the <see cref="Interval"/> is set to 3, 
		/// the recurrence will occur every third day.</remarks> 
		public int Interval
		{
			get
			{
				return _interval;
			}
			set
			{
				if (_interval != value)
				{
					_interval = (value < 1) ? 1 : value;
					if (_interval > 999)
					{
						_interval = 999;
					}
                    OnPropertyChanged("Interval"); 
                }
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="DateTime"/> value indicating 
		/// the end time for the given occurrence of the recurrence pattern. 
		/// </summary>
		public DateTime EndTime
		{
			get
			{
				return _start + _duration;
			}
			set
			{
				if (value < _start)
				{
					ThrowInvalidTime();
				}
				_duration = value - _start;
                OnPropertyChanged("EndTime"); 
                OnPropertyChanged("Duration"); 
            }
		}

		private void ThrowInvalidTime()
		{
#if END_USER_LOCALIZATION
			throw new System.ArgumentException(Strings.C1Schedule.Exceptions.Item("StartEndValidationFailed", (Info ?? new CalendarInfo()).CultureInfo));
#elif WINFX
			throw new System.ArgumentException(C1Localizer.GetString("Exceptions", "StartEndValidationFailed", "The End value should be greater than Start value.", (Info ?? new CalendarInfo()).CultureInfo));
#elif SILVERLIGHT
            throw new System.ArgumentException(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.StartEndValidationFailed);
#else
			throw new System.ArgumentException(C1Localizer.GetString("The End value should be greater than Start value."));
#endif
		}

		/// <summary>
		/// Gets or sets the <see cref="DateTime"/> value indicating 
		/// the start time for the given occurrence of the recurrence pattern. 
		/// </summary>
		public DateTime StartTime
		{
			get
			{
				return _start;
			}
			set
			{
                if (_start != value) 
                {
                    _start = value;
                    OnPropertyChanged("StartTime"); 
                    OnPropertyChanged("EndTime"); 
                }
            }
		}

		/// <summary>
		/// Gets or sets the <see cref="TimeSpan"/> value indicating 
		/// the duration of each occurrence in the recurrence pattern.
		/// </summary>
		public TimeSpan Duration
		{
			get
			{
				return _duration;
			}
			set
			{
                if (_duration != value) 
                {
					try
					{
						DateTime end = _start + value;
					}
					catch (System.ArgumentOutOfRangeException)
					{
#if END_USER_LOCALIZATION
						throw new System.ArgumentException(Strings.RecurrenceFormStrings.Item("IncorrectDuration", (Info ?? new CalendarInfo()).CultureInfo));
#elif WINFX
						throw new System.ArgumentException(C1Localizer.GetString("Exceptions", "IncorrectDuration", "The duration is incorrect.", (Info ?? new CalendarInfo()).CultureInfo));
#else
						throw new System.ArgumentException(C1Localizer.GetString("The duration is incorrect."));
#endif
					}
					_duration = value;
                    OnPropertyChanged("Duration"); 
                    OnPropertyChanged("EndTime"); 
                }
            }
		}

		/// <summary>
		/// Gets or sets the <see cref="DateTime"/> value indicating 
		/// the start date of the recurrence pattern. 
		/// </summary>
		public DateTime PatternStartDate
		{
			get
			{
				return _patternStartDate;
			}
			set
			{
				if (_patternStartDate != value.Date)
				{
					_patternStartDate = value.Date;
                    OnPropertyChanged("PatternStartDate");

					if (value > PatternEndDate)
					{
						SetPatternEndDate(_patternStartDate.AddMonths(2));
					}
                }
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="DateTime"/> value indicating 
		/// the end date of the recurrence pattern. 
		/// </summary>
		/// <remarks>This property is optional but must be coordinated 
		/// with other properties when setting up a recurrence pattern. 
		/// If this property or the <see cref="Occurrences"/> property is set, 
		/// the pattern is considered to be finite, and the <see cref="NoEndDate"/> 
		/// property is false. 
		/// If neither <see cref="PatternEndDate"/> nor <see cref="Occurrences"/> is set, 
		/// the pattern is considered infinite and <see cref="NoEndDate"/> is true. 
		/// The <see cref="Interval"/> property must be set before 
		/// setting <see cref="PatternEndDate"/>.</remarks>
		public DateTime PatternEndDate
		{
			get
			{
				if (Occurrences > 0 && !NoEndDate && _patternEndDate == MaxDate)
				{
					// count property so that it returns valid value
					Count();
				}
				return _patternEndDate;
			}
			set
			{
				if (value < _patternStartDate)
				{
					ThrowInvalidTime();
				}
				bool occurChanged = Occurrences != 0;
				_occurrences = 0;
				bool propChanged = _patternEndDate != value.Date;
                if (propChanged)
                {
                    bool noEndChanged = NoEndDate;
                    _noEndDate = false;
                    _patternEndDate = value.Date;
                    OnPropertyChanged("PatternEndDate");
                    if (noEndChanged)
                        OnPropertyChanged("NoEndDate");
                }
				if (occurChanged)
					OnPropertyChanged("Occurrences");
			}
		}

		private void SetPatternEndDate(DateTime value)
		{
			// set without dirtying the pattern
			if (_patternEndDate != value.Date)
			{
				_patternEndDate = value.Date;
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("PatternEndDate"));
			}
		}

		/// <summary>
		/// Gets or sets the number of occurrences for the recurrence pattern.
		/// This property allows the definition of a recurrence pattern that 
		/// is only valid for the specified number of subsequent occurrences. 
		/// For example, you can set this property to 10 for a formal training 
		/// course that will be held on the next ten Thursday evenings.
		/// The default value is 0.
		/// </summary>
		/// <remarks>This property must be coordinated with other properties 
		/// when setting up a recurrence pattern. 
		/// If the <see cref="PatternEndDate"/> property 
		/// or the <see cref="Occurrences"/> property is set, the pattern 
		/// is considered to be finite and the <see cref="NoEndDate"/> property is false. 
		/// If neither <see cref="PatternEndDate"/> nor <see cref="Occurrences"/> is set, 
		/// the pattern is considered infinite and <see cref="NoEndDate"/> is true.</remarks>
		public int Occurrences
		{
			get
			{
				return _occurrences;
			}
			set
			{
				if ( _occurrences != value)
				{
					_occurrences = value;
                    bool noEndChanged = _noEndDate; //alex
					if (_occurrences <= 0)
					{
						_noEndDate = true;
					}
					else
					{
						_noEndDate = false;
					}
					noEndChanged = noEndChanged != _noEndDate;
                    OnPropertyChanged("Occurrences"); 
                    if (noEndChanged)
                        OnPropertyChanged("NoEndDate"); //alex
				}
			}
		}

		/// <summary>
		/// Get or sets the <see cref="WeekOfMonthEnum"/> value specifying 
		/// the count for which the recurrence pattern is valid for a given interval. 
		/// This property is only valid for recurrences of 
		/// the <see cref="RecurrenceTypeEnum.MonthlyNth"/> and 
		/// <see cref="RecurrenceTypeEnum.YearlyNth"/> types and allows the definition 
		/// of a recurrence pattern that is only valid for the Nth occurrence, 
		/// such as "the 2nd Sunday in March" pattern. 
		/// The default value is WeekOfMonthEnum.First.
		/// </summary>
		public WeekOfMonthEnum Instance
		{
			get
			{
				
				return _instance;
			}
			set
			{
				if ( _instance != value)
				{
					_instance = value;
                    OnPropertyChanged("Instance");
				}
			}
		}

		// TODO: throw exceptions as it described in remarks.
		/// <summary>
		/// Gets or sets the <see cref="WeekDaysEnum"/> value representing the mask 
		/// for week days on which the recurring appointment occurs. 
		/// Monthly and yearly patterns are only valid for a single day. 
		/// Weekly patterns are only valid as the Or of the <see cref="DayOfWeekMask"/>.
		/// The default value is 0.
		/// </summary>
		/// <remarks>
		/// When the <see cref="RecurrenceType"/> property is set to 
		///   <see cref="RecurrenceTypeEnum.Daily"/>, the <see cref="DayOfWeekMask"/> property 
		///   can only be set to <see cref="WeekDaysEnum.EveryDay"/>; setting the property 
		///   to any other value will result in an exception.
		/// When the <see cref="RecurrenceType"/> property is set to 
		///   <see cref="RecurrenceTypeEnum.Workdays"/>, the <see cref="DayOfWeekMask"/> property 
		///   can only be set to <see cref="WeekDaysEnum.WorkDays"/>; setting the property 
		///   to any other value will result in an exception.
		/// When the <see cref="RecurrenceType"/> property is set to 
		///   <see cref="RecurrenceTypeEnum.Weekly"/>, the <see cref="DayOfWeekMask"/> property 
		///   cannot be set to <see cref="WeekDaysEnum.None"/>; doing so will result 
		///   in an exception being thrown.
		/// When the <see cref="RecurrenceType"/> property is set to 
		///   <see cref="RecurrenceTypeEnum.Monthly"/> or <see cref="RecurrenceTypeEnum.Yearly"/>
		///   the <see cref="DayOfWeekMask"/> property is not applicable.</remarks>
		public WeekDaysEnum DayOfWeekMask
		{
			get
			{
				return _dayOfWeekMask;
			}
			set
			{
				if ( _dayOfWeekMask != value)
				{
					_listDays.Clear();
					_dayOfWeekMask = value;

                    OnPropertyChanged("DayOfWeekMask");
				}
			}
		}

		/// <summary>
		/// Gets or sets the number of the day in its respective month on which 
		/// each occurrence will occur. Applicable only when the <see cref="RecurrenceType"/> 
		/// property is set to <see cref="RecurrenceTypeEnum.Monthly"/> 
		/// or <see cref="RecurrenceTypeEnum.Yearly"/>.
		/// The default value is 1.
		/// </summary>
		public int DayOfMonth  
		{
			get
			{
				return _dayOfMonth;
			}
			set
			{
				if ( _dayOfMonth != value)
				{
					_dayOfMonth = (value < 1) ? 1 : value;
					if (_dayOfMonth > 31)
					{
						_dayOfMonth = 31;
					}
                    OnPropertyChanged("DayOfMonth");
				}
			}
		}

		/// <summary>
		/// Gets or sets the value indicating which month of the year is valid 
		/// for the specified recurrence pattern. Can be a number from 1 to 12.
		/// This property is only valid for recurrence patterns whose <see cref="RecurrenceType"/> 
		/// property is set to <see cref="RecurrenceTypeEnum.YearlyNth"/> 
		/// or <see cref="RecurrenceTypeEnum.Yearly"/>.
		/// The default value is 1.
		/// </summary>
		public int MonthOfYear 
		{
			get
			{
				return _monthOfYear;
			}
			set
			{
				if ( _monthOfYear != value)
				{
					_monthOfYear = (value < 1) ? 1 : value;
					if (_monthOfYear > 12)
					{
						_monthOfYear = 12;
					}
					OnPropertyChanged("MonthOfYear");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Boolean"/> value indicating if
		/// the recurrence pattern is endless.
		/// The default value is True.
		/// </summary>
		/// <remarks>This property must be coordinated with other properties when 
		/// setting up a recurrence pattern. If the <see cref="PatternEndDate"/> property 
		/// or the <see cref="Occurrences"/> property is set, the pattern is considered 
		/// to be finite and the <see cref="NoEndDate"/> property is false. 
		/// If neither <see cref="PatternEndDate"/> nor <see cref="Occurrences"/> is set, 
		/// the pattern is considered infinite and <see cref="NoEndDate"/> is true.</remarks>
		public bool NoEndDate
		{
			get
			{
				return _noEndDate;
			}
			set
			{
				if ( _noEndDate != value)
				{
                    bool occursChanged = false; //alex
					//if (!value)  // alex
                    if (value)
					{
                        occursChanged = _occurrences != 0; //alex
						_occurrences = 0;
						
					}
					_noEndDate = value;
                    OnPropertyChanged("NoEndDate"); 
                    if (occursChanged)
                        OnPropertyChanged("Occurrences"); //alex
				}
			}
		}

		/// <summary>
		/// Gets the <see cref="AppointmentList"/> object which holds the list 
		/// of <see cref="Appointment"/> objects that define the exceptions to that series 
		/// of appointments. <see cref="Appointment"/> objects are added to 
		/// the <see cref="Exceptions"/> whenever a property in the corresponding 
		/// <see cref="Appointment"/> object is altered.
		/// </summary>
		public AppointmentList Exceptions
		{
			get
			{
				return _exceptions;
			}
		}

		/// <summary>
		/// Gets the <see cref="AppointmentList"/> object which holds the list 
		/// of <see cref="Appointment"/> objects removed from that series of appointments. 
		/// </summary>
		public AppointmentList RemovedOccurrences
		{
			get
			{
				return _removed;
			}
		}

		/// <summary>
		/// Returns a specific instance of the Appointment object on the specified date.
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="info">CalendarInfo</param>
		/// <returns></returns>
		/// <remarks>The GetOccurrence method generates an exception if no appointment 
		/// of that series exists on the specified date.</remarks>
		internal Appointment GetOccurrence(DateTime startDate, CalendarInfo info)
		{
			startDate = startDate.Date;
            foreach (Appointment app in _removed)
            {
                if (app.Start.Date == startDate)
                {
                    return null;
                }
            }
            Appointment result = null;
            AppointmentCollection parentCollection = _owner.ParentCollection;
            DayCollection days = parentCollection.Days;
			// find if it already exists
            foreach (Appointment app in days[startDate].Appointments)
			{
				if (app.Start.Date == startDate && app.ParentRecurrence != null && app.ParentRecurrence.Id.Equals(_owner.Id))
				{
					if (app.RecurrenceState == RecurrenceStateEnum.Removed)
					{
						return null;
					}
                    result = app;
                    break;
				}
			}
            if (result == null)
            {
                foreach (Appointment app in _exceptions)
                {
                    if (app.Start.Date == startDate)
                    {
                        // add occurrences to days (it caches them)
                        days.AddAppointment(app);
                        result = app;
                        break;
                    }
                }
            }
			// there is no such appointment
            if (result == null && IsValid(startDate, info))
			{
				Appointment app = _owner.CopyForPattern();
				app.SetStart(startDate.Date.Add(_start.TimeOfDay));
				app.SetDuration(_duration);
				app.ParentRecurrence = _owner;
				app.RecurrenceState = RecurrenceStateEnum.Occurrence;
				if (_owner.Reminder != null)
				{
					app.SetReminder();
				}
                // add occurrences to days (it caches them)
                days.AddAppointment(app);
                result = app;
			}
            if (result != null)
            {
                result.ParentCollection = result.ParentCollection ?? parentCollection;
                if (result.Reminder != null)
                {
                    parentCollection.Reminders.Add(result.Reminder);
                }
                if (result.Action != null)
                {
                    parentCollection.Actions.Add(result.Action);
                }
            }
            return result;
        }
		#endregion

		#region xml
#if (!SILVERLIGHT) 
		/// <summary>
		/// Reconstructs recurrence pattern properties from an <see cref="XmlNode"/>. 
		/// </summary>
		/// <param name="node"></param>
		/// <returns>True if pattern has been changed.</returns>
		internal bool FromXml(XmlNode node)
		{
			using (System.IO.TextReader tr = new System.IO.StringReader(node.OuterXml))
			using (XmlReader reader = XmlReader.Create(tr, XmlExchanger._readerSettings))
			{
				return FromXml(reader);
			}
        }
#endif

		/// <summary>
		/// Reconstructs recurrence pattern properties from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader"></param>
		/// <returns>True if pattern has been changed.</returns>
		internal bool FromXml(XmlReader reader)
		{
			bool dirty = false;

			_exceptions.Clear();
            _removed.Clear();

			reader.Read();

			RecurrenceTypeEnum recurrenceType = RecurrenceTypeEnum.Weekly;
			int interval = 1;
			int occurrences = 0;
			WeekDaysEnum dayOfWeekMask = WeekDaysEnum.None;
			int dayOfMonth = 1;
			int monthOfYear = 1;
			int adjustedOccurrences = 0;
			bool noEndDate = true;
			WeekOfMonthEnum instance = WeekOfMonthEnum.First;

			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				DateTime time;
				switch (reader.Name)
				{
					case "RecurrenceType":
						recurrenceType = (RecurrenceTypeEnum)int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "Interval":
						interval = int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "Occurrences":
						occurrences = int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "DayOfWeekMask":
						dayOfWeekMask = (WeekDaysEnum)int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "DayOfMonth":
						dayOfMonth = int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "MonthOfYear":
						monthOfYear = int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "NoEndDate":
						noEndDate = bool.Parse(XmlExchanger.ReadStringValue(reader));
						break;
					case "Instance":
						instance = (WeekOfMonthEnum)int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "Start":
						time = CalendarInfo.ParseTime(XmlExchanger.ReadStringValue(reader));
						if (time != _start)
						{
							_start = time;
							dirty = true;
						}
						break;
					case "Duration":
						long duration = long.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						if (_duration.Ticks != duration)
						{
							_duration = TimeSpan.FromTicks(duration);
							dirty = true;
						}
						break;
					case "StartDate":
						time = DateTime.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						if (time.Date != _patternStartDate)
						{
							_patternStartDate = time.Date;
							dirty = true;
						}
						break;
					case "EndDate":
						time = DateTime.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						if (time.Date != _patternEndDate)
						{
							_patternEndDate = time.Date;
							dirty = true;
						}
						break;
					case "AdjustedOccurrences":
						adjustedOccurrences = int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "Exceptions":
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							reader1.Read();
							while (reader1.Read() && reader1.NodeType == XmlNodeType.Element)
							{
								Appointment app = new Appointment();
								app.ParentCollection = _owner.ParentCollection;
								app.ParentRecurrence = _owner;
								app.RecurrenceState = RecurrenceStateEnum.Exception;
								using (XmlReader reader2 = reader1.ReadSubtree())
								{
									app.FromXml(reader2);
								}
								for (int j = Exceptions.Count - 1; j >= 0; j--)
								{
									if (Exceptions[j].Start.Date == app.Start.Date
										&& Exceptions[j].ParentRecurrence == _owner)
									{
										_owner.ParentCollection.Days.RemoveAppointment(Exceptions[j]);
										Exceptions.RemoveAt(j);
									}
								}
								_exceptions.Add(app);
								_owner.ParentCollection.Days.AddAppointment(app);
							}
						}
						break;
					case "Removed":
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							reader1.Read();
							while (reader1.Read() && reader1.NodeType == XmlNodeType.Element)
							{
								Appointment app = new Appointment();
								app.RecurrenceState = RecurrenceStateEnum.Removed;
								app.ParentCollection = _owner.ParentCollection;
								app.ParentRecurrence = _owner;
								using (XmlReader reader2 = reader1.ReadSubtree())
								{
									app.FromXml(reader2);
								}
								_removed.Add(app);
								_owner.ParentCollection.Days.AddAppointment(app);
							}
						}
						break;
				}
			}

			if ( recurrenceType != _recurrenceType)
			{
				_recurrenceType = recurrenceType;
				dirty = true;
			}
			if ( interval != _interval)
			{
				_interval = interval;
				dirty = true;
			}
			if (occurrences != _occurrences)
			{
				_occurrences = occurrences;
				dirty = true;
			}
			if (dayOfWeekMask != _dayOfWeekMask)
			{
				_dayOfWeekMask = dayOfWeekMask;
				dirty = true;
			}
			if (dayOfMonth != _dayOfMonth)
			{
				_dayOfMonth = dayOfMonth;
				dirty = true;
			}
			if (monthOfYear != _monthOfYear)
			{
				_monthOfYear = monthOfYear;
				dirty = true;
			}
			if (noEndDate != _noEndDate)
			{
				_noEndDate = noEndDate;
				dirty = true;
			}
			if (instance != _instance)
			{
				_instance = instance;
				dirty = true;
			}
			if (adjustedOccurrences != _adjustedOccurrences)
			{
				_adjustedOccurrences = adjustedOccurrences;
				dirty = true;
			}

			if (dirty)
			{
				OnDeserialized(
#if (!SILVERLIGHT)
					new System.Runtime.Serialization.StreamingContext()
#endif
					);
			}
			return dirty;
		}

		/// <summary>
		/// Creates an XML encoding of the recurrence pattern properties. 
		/// </summary>
		/// <param name="writer"></param>
		internal void ToXml(XmlWriter writer)
		{
			writer.WriteStartElement("RecurrencePattern");

			if (_recurrenceType != RecurrenceTypeEnum.Weekly)
			{
				writer.WriteElementString("RecurrenceType", ((int)_recurrenceType).ToString(CultureInfo.InvariantCulture));
			}
			if (_interval != 1)
			{
				writer.WriteElementString("Interval", _interval.ToString(CultureInfo.InvariantCulture));
			}
			if (_occurrences != 0)
			{
				writer.WriteElementString("Occurrences", _occurrences.ToString(CultureInfo.InvariantCulture));
			}
			if (_dayOfWeekMask != 0)
			{
				writer.WriteElementString("DayOfWeekMask", ((int)_dayOfWeekMask).ToString(CultureInfo.InvariantCulture));
			}
			if (_dayOfMonth != 1)
			{
				writer.WriteElementString("DayOfMonth", _dayOfMonth.ToString(CultureInfo.InvariantCulture));
			}
			if (_monthOfYear != 1)
			{
				writer.WriteElementString("MonthOfYear", _monthOfYear.ToString(CultureInfo.InvariantCulture));
			}
			if (!_noEndDate)
			{
				writer.WriteElementString("NoEndDate", _noEndDate.ToString(CultureInfo.InvariantCulture));
			}
			if (_instance != WeekOfMonthEnum.First)
			{
				writer.WriteElementString("Instance", ((int)_instance).ToString(CultureInfo.InvariantCulture));
			}

			if (Info != null && _info.DateTimeKind == DateTimeKind.Utc)
			{
				writer.WriteElementString("Start", _info.ToDataSourceTime(_start).ToString("r", CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteElementString("Start", _start.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteElementString("Duration", _duration.Ticks.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("StartDate", _patternStartDate.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("EndDate", _patternEndDate.ToString(CultureInfo.InvariantCulture));

			if (_adjustedOccurrences > 0)
			{
				writer.WriteElementString("AdjustedOccurrences", _adjustedOccurrences.ToString(CultureInfo.InvariantCulture));
			}
			if (_exceptions.Count > 0)
			{
				writer.WriteStartElement("Exceptions");
				foreach (Appointment app in _exceptions)
				{
					app.ToXml(writer);
				}
				writer.WriteEndElement();
			}

			if (_removed.Count > 0)
			{
				writer.WriteStartElement("Removed");
				foreach (Appointment app in _removed)
				{
					app.ToXml(writer);
				}
				writer.WriteEndElement();
			}

			writer.WriteEndElement();
		}
		#endregion

		#region ISerializable
#if (!SILVERLIGHT)
		[OnDeserialized]
#endif
		private void OnDeserialized(
#if (!SILVERLIGHT)
			StreamingContext context
#endif
			)
		{
			_listDays = new List<DayOfWeek>();
			SetDirty();
		}
#if (!SILVERLIGHT)
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected RecurrencePattern(SerializationInfo info, StreamingContext context)
		{
			_recurrenceType = (RecurrenceTypeEnum)info.GetValue("_recurrenceType", typeof(RecurrenceTypeEnum));
			_interval = info.GetInt32("_interval");
			_occurrences = info.GetInt32("_occurrences");
			_dayOfWeekMask = (WeekDaysEnum)info.GetValue("_dayOfWeekMask", typeof(WeekDaysEnum));
			_dayOfMonth = info.GetInt32("_dayOfMonth");
			_monthOfYear = info.GetInt32("_monthOfYear");
			_noEndDate = info.GetBoolean("_noEndDate");
			_exceptions = (AppointmentList)info.GetValue("_exceptions", typeof(AppointmentList));
			_removed = (AppointmentList)info.GetValue("_removed", typeof(AppointmentList));
			_instance = (WeekOfMonthEnum)info.GetValue("_instance", typeof(WeekOfMonthEnum));
			DateTime st = info.GetDateTime("_start");
			if (st.Kind == DateTimeKind.Utc)
			{
				st = st.ToLocalTime();
			}
			_start = st;
			_duration = (TimeSpan)info.GetValue("_duration", typeof(TimeSpan));
			_patternStartDate = info.GetDateTime("_patternStartDate").Date;
			_patternEndDate = info.GetDateTime("_patternEndDate").Date;
			
			try
			{
				_adjustedOccurrences = info.GetInt32("_adjustedOccurrences");
			}
			catch (SerializationException)
			{
			}
		}

		// A method called when serializing.
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
			Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_recurrenceType", _recurrenceType, typeof(RecurrenceTypeEnum));
			info.AddValue("_interval", _interval);
			info.AddValue("_occurrences", _occurrences);
			info.AddValue("_dayOfWeekMask", _dayOfWeekMask, typeof(WeekDaysEnum));
			info.AddValue("_dayOfMonth", _dayOfMonth);
			info.AddValue("_monthOfYear", _monthOfYear);
			info.AddValue("_noEndDate", _noEndDate);
			info.AddValue("_exceptions", _exceptions, typeof(AppointmentList));
			info.AddValue("_removed", _removed, typeof(AppointmentList));
			info.AddValue("_instance", _instance, typeof(WeekOfMonthEnum));

			if (Info != null)
			{
				info.AddValue("_start", _info.ToDataSourceTime(_start));
			}
			else
			{
				info.AddValue("_start", _start);
			}
			info.AddValue("_duration", _duration, typeof(TimeSpan));
			info.AddValue("_patternStartDate", _patternStartDate);
			info.AddValue("_patternEndDate", _patternEndDate);
			info.AddValue("_adjustedOccurrences", _adjustedOccurrences);
		}
#endif
		#endregion

		#region private helpers
		// converts DayOfWeekMask into list of DayOfWeek constants
		internal List<DayOfWeek> GetDaysOfWeek()
		{
			if (_changed)
			{
				Count();
			}
			return _listDays;
		}

		internal bool IsValid(DateTime date, CalendarInfo info)
		{
			Info = info;
			date = date.Date;
			if (_changed)
			{
				Count();
			}
			if ((date < PatternStartDate.Date) || (!NoEndDate && date > _patternEndDate.Date))
			{
				return false;
			}
			if (!_countingBeforeFirstDate && date < info.FirstDate)
			{
				return false;
			}
			if (Occurrences > 0 && !NoEndDate && count >= Occurrences + _adjustedOccurrences)
			{
				return false;
			}
			// check pattern
			bool valid = false;
			switch (RecurrenceType)
			{
				// -----------------------------------------------
				#region ** Daily
				case RecurrenceTypeEnum.Daily:
					if (((TimeSpan)(date - PatternStartDate.Date)).TotalDays % _interval == 0)
					{
						valid = true;
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Workdays
				case RecurrenceTypeEnum.Workdays:
					foreach (DateTime holiday in info.Holidays)
					{
						if (holiday.Date == date)
						{
							return false;
						}
					}
					if ( info.WorkDays.Contains(date.DayOfWeek))
					{
						valid = true;
						break;
					}
					if (!valid)
					{
						foreach (DateTime exception in info.WeekendExceptions)
						{
							if (exception.Date == date)
							{
								valid = true;
								break;
							}
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Monthly
				case RecurrenceTypeEnum.Monthly:
					if ( (date.Month - PatternStartDate.Month) % _interval == 0 )
					{
						// if pattern set to the 28-31 day of month, then for shot months 
						// the last day of month is valid
						int daysInMonth = info.CultureInfo.Calendar.GetDaysInMonth(date.Year, date.Month);
						int validDay = (_dayOfMonth > daysInMonth)? daysInMonth : _dayOfMonth; 
						if (date.Day == validDay)
						{
							valid = true;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** MonthlyNth
				case RecurrenceTypeEnum.MonthlyNth:
					if (_listDays.Count == 0)
					{
						return false;
					}
					if ((date.Month - PatternStartDate.Month) % _interval == 0
						&& _listDays.Contains(date.DayOfWeek))
					{
						valid = IsValidInstance(date);
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Weekly
				case RecurrenceTypeEnum.Weekly:
					if ((((TimeSpan)(date - _patternReferencePoint)).TotalDays) % (7 * _interval) < 7)
					{
						if ( _listDays.Contains(date.DayOfWeek))
						{
							valid = true;
							break;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Yearly
				case RecurrenceTypeEnum.Yearly:
					if (date.Month == _monthOfYear )
					{
						// if pattern set to the 28-31 day of month, then for shot months 
						// the last day of month is valid
						int daysInMonth = info.CultureInfo.Calendar.GetDaysInMonth(date.Year, date.Month);
						int validDay = (_dayOfMonth > daysInMonth)? daysInMonth : _dayOfMonth; 
						if (date.Day == validDay)
						{
							valid = true;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** YearlyNth
				case RecurrenceTypeEnum.YearlyNth:
					if (_listDays.Count == 0)
					{
						return false;
					}
					if (date.Month == _monthOfYear && _listDays.Contains(date.DayOfWeek))
					{
						valid = IsValidInstance(date);
					}
					break;
				#endregion
			}
			if (valid && Occurrences > 0 && !NoEndDate)
			{
				count++;
				if (Occurrences + _adjustedOccurrences == count)
				{
					_patternEndDate = date.Date;
				}
			}
			return valid;
		}

		// just checks the date against the pattern definition, doesn't change count and reference dates.
		internal bool IsValidDate(DateTime date, CalendarInfo info)
		{
			Info = info;
			date = date.Date;
			if (date < PatternStartDate.Date || (!NoEndDate && date > _patternEndDate.Date ))
			{
				return false;
			}
			bool valid = false;
			// check pattern
			switch (RecurrenceType)
			{
				// -----------------------------------------------
				#region ** Daily
				case RecurrenceTypeEnum.Daily:
					if (((TimeSpan)(date - PatternStartDate.Date)).TotalDays % _interval == 0)
					{
						valid = true;
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Workdays
				case RecurrenceTypeEnum.Workdays:
					foreach (DateTime holiday in info.Holidays)
					{
						if (holiday.Date == date)
						{
							return false;
						}
					}
					if (info.WorkDays.Contains(date.DayOfWeek))
					{
						valid = true;
						break;
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Monthly
				case RecurrenceTypeEnum.Monthly:
					if ((date.Month - PatternStartDate.Month) % _interval == 0)
					{
						// if pattern set to the 28-31 day of month, then for shot months 
						// the last day of month is valid
						int daysInMonth = info.CultureInfo.Calendar.GetDaysInMonth(date.Year, date.Month);
						int validDay = (_dayOfMonth > daysInMonth) ? daysInMonth : _dayOfMonth;
						if (date.Day == validDay)
						{
							valid = true;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** MonthlyNth
				case RecurrenceTypeEnum.MonthlyNth:
					if (_listDays.Count == 0)
					{
						return false;
					}
					if ((date.Month - PatternStartDate.Month) % _interval == 0
						&& _listDays.Contains(date.DayOfWeek))
					{
						valid = IsValidInstance(date);
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Weekly
				case RecurrenceTypeEnum.Weekly:
					if ((((TimeSpan)(date - _patternReferencePoint)).TotalDays) % (7 * _interval) < 7)
					{
						if (_listDays.Contains(date.DayOfWeek))
						{
							valid = true;
							break;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** Yearly
				case RecurrenceTypeEnum.Yearly:
					if (date.Month == _monthOfYear)
					{
						// if pattern set to the 28-31 day of month, then for shot months 
						// the last day of month is valid
						int daysInMonth = info.CultureInfo.Calendar.GetDaysInMonth(date.Year, date.Month);
						int validDay = (_dayOfMonth > daysInMonth) ? daysInMonth : _dayOfMonth;
						if (date.Day == validDay)
						{
							valid = true;
						}
					}
					break;
				#endregion
				// -----------------------------------------------
				#region ** YearlyNth
				case RecurrenceTypeEnum.YearlyNth:
					if (_listDays.Count == 0)
					{
						return false;
					}
					if (date.Month == _monthOfYear && _listDays.Contains(date.DayOfWeek))
					{
						valid = IsValidInstance(date);
					}
					break;
				#endregion
			}
			return valid;
		}

		internal List<DateTime> GetValidDates(DateTime start, DateTime end, CalendarInfo info)
		{
			List<DateTime> dates = new List<DateTime>();
			DateTime date = (start.Date < _patternStartDate) ? _patternStartDate : start.Date;
			end = (end.Date > _patternEndDate) ? _patternEndDate : end.Date;
			end = (date == end) ? date.AddMinutes(1) : end;
			if (end == MaxDate)
			{
				end = date.AddYears(1);
			}
			while (date < end)
			{
				if (IsValid(date, info))
				{
					dates.Add(date);
				}
				date = date.AddDays(1);
			}
			return dates;
		}

		private bool IsValidInstance(DateTime date)
		{
			bool valid = false;
			if ((int)_instance == 5)
			{
				DateTime lastDay = new DateTime(date.AddMonths(1).Year, date.AddMonths(1).Month, 1);
				DateTime firstDay = lastDay.Subtract(TimeSpan.FromDays(7));

				if (date >= firstDay)
				{
					valid = true;
					firstDay = date.AddDays(1);
				}
				while (firstDay < lastDay)
				{
					if (_listDays.Contains(firstDay.DayOfWeek))
					{
						valid = false;
						break;
					}
					firstDay = firstDay.AddDays(1);
				}
			}
			else
			{
				DateTime firstDay;
				DateTime lastDay;
				if (DayOfWeekMask == WeekDaysEnum.EveryDay)
				{
					firstDay = new DateTime(date.Year, date.Month, (int)_instance);
					lastDay = firstDay.AddDays(1);
				}
				else if (DayOfWeekMask == WeekDaysEnum.WorkDays || DayOfWeekMask == WeekDaysEnum.WeekendDays)
				{
					int c = 0;
					firstDay = new DateTime(date.Year, date.Month, 1).AddDays(-1);
					while (c < (int)_instance)
					{
						firstDay = firstDay.AddDays(1);
						if (_listDays.Contains(firstDay.DayOfWeek))
						{
							c++;
						}
					}
					lastDay = firstDay.AddDays(1);
				}
				else
				{
					firstDay = new DateTime(date.Year, date.Month, 1).AddDays(((int)_instance - 1) * 7);
					lastDay = firstDay.AddDays(7);
				}
				if (date >= firstDay && date < lastDay)
				{
					valid = true;
					lastDay = date.AddDays(-1);
				}
				while (firstDay < lastDay)
				{
					if (_listDays.Contains(lastDay.DayOfWeek))
					{
						valid = false;
						break;
					}
					lastDay = lastDay.AddDays(-1);
				}
			}
			return valid;
		}

		// recalculates pattern properties which depend on other pattern properties
		private void Count()
		{
			_changed = false;
			CalendarInfo info = Info ?? new CalendarInfo();
			// renew description
			_description = "";
			count = Exceptions.Count + RemovedOccurrences.Count;

			_listDays.Clear();
			switch (_dayOfWeekMask)
			{
				case WeekDaysEnum.EveryDay:
					for (int i = 0; i < 7; i++)
					{
						_listDays.Add((DayOfWeek)i);
					}
					break;
				case WeekDaysEnum.WorkDays:
					foreach (DayOfWeek day in info.WorkDays)
					{
						_listDays.Add(day);
					}
					break;
				case WeekDaysEnum.WeekendDays:
					for (int i = 0; i < 7; i++)
					{
						if (!info.WorkDays.Contains((DayOfWeek)i))
						{
							_listDays.Add((DayOfWeek)i);
						}
					}
					break;
				default:
					Type dayOfWeekType = typeof(DayOfWeek);
#if (SILVERLIGHT)
                    DayOfWeek[] days = {DayOfWeek.Sunday, DayOfWeek.Monday, DayOfWeek.Tuesday, 
                                        DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday};
                    foreach (DayOfWeek day in days)
#else
					foreach (DayOfWeek day in Enum.GetValues(dayOfWeekType))
#endif
					{
						if ((_dayOfWeekMask & (WeekDaysEnum)Enum.Parse(typeof(WeekDaysEnum), Enum.GetName(dayOfWeekType, day), true)) != 0)
						{
							_listDays.Add(day);
						}
					}
					break;
			}
			_listDays.Sort();

			// check pattern
			if (RecurrenceType == RecurrenceTypeEnum.Workdays || RecurrenceType == RecurrenceTypeEnum.Weekly)
			{
				_patternReferencePoint =
					PatternStartDate.Date.Subtract(TimeSpan.FromDays(PatternStartDate.DayOfWeek - info.WeekStart));
				if (Occurrences > 0 && !NoEndDate && _listDays.Count == 0 && RecurrenceType == RecurrenceTypeEnum.Weekly)
				{
					_patternEndDate = PatternStartDate.Date;
				}
			}
			if (Occurrences > 0 && PatternStartDate.Date < info.FirstDate && !_countingBeforeFirstDate)
			{
				_countingBeforeFirstDate = true;
				// count occurrences before the first date
				DateTime start = PatternStartDate.Date;
				DateTime end = info.FirstDate;
				while (start < end)
				{
					IsValid(start, info);
					start = start.AddDays(1);
				}
				_countingBeforeFirstDate = false;
			}
			if (Occurrences > 0 && !NoEndDate )
			{
				// count _patternEndDate
				int tmpCount = count;
				count -= Exceptions.Count + RemovedOccurrences.Count;
				DateTime start = PatternStartDate.Date < info.FirstDate ? info.FirstDate : PatternStartDate.Date;
				while (_patternEndDate == MaxDate)
				{
					IsValid(start, info);
					start = start.AddDays(1);
				}
				count = tmpCount;
			}
		}

		/// <summary>
		/// Creates the copy of the <see cref="RecurrencePattern"/> object.
		/// </summary>
		/// <returns>The new <see cref="RecurrencePattern"/> instance.</returns>
		internal RecurrencePattern Copy()
		{
			RecurrencePattern pattern = (RecurrencePattern)this.MemberwiseClone();
			pattern._exceptions = Exceptions.Clone();
			pattern._removed = RemovedOccurrences.Clone();
			pattern.Owner = null;
			pattern.SetDirty();
			return pattern;
		}

		internal void CopyFrom(RecurrencePattern pattern)
		{
			_listDays = new List<DayOfWeek>();
			_info = pattern.Info;
			_recurrenceType = pattern.RecurrenceType;
			_interval = pattern.Interval;
			_occurrences = pattern.Occurrences;
			_adjustedOccurrences = pattern._adjustedOccurrences;
			_dayOfWeekMask = pattern.DayOfWeekMask;
			_dayOfMonth = pattern.DayOfMonth;
			_monthOfYear = pattern.MonthOfYear;
			_noEndDate = pattern.NoEndDate;
			_instance = pattern.Instance;
			_start = pattern.StartTime;
			_duration = pattern.Duration;
			_patternStartDate = pattern.PatternStartDate;
			_patternEndDate = pattern.PatternEndDate;
            _exceptions.Clear();
            _removed.Clear();
			_exceptions = pattern.Exceptions.Clone();
			_removed = pattern.RemovedOccurrences.Clone();
			SetDirty();
			OnPropertyChanged("Description");
		}

		internal void SetDirty()
		{
			_changed = true;
			count = 0;
			if (Occurrences > 0 && !NoEndDate)
			{
				// end date should be counted according the new pattern settings
				_patternEndDate = MaxDate;
			}
		}
		
		// call this method to cleanup all information about exceptions and removed occurrences
		internal void SetDirty(bool cleanupExceptions)
		{
			if (cleanupExceptions)
			{
				_adjustedOccurrences = 0;
                _exceptions.Clear();
                _removed.Clear();
			}
			SetDirty();
		}
	#endregion
	}
}
