using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml;
using System.Globalization;
using System.IO;

#if (!WINFX && !SILVERLIGHT)
using System.Drawing;
#else
using System.Windows.Media;
#endif

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="BasePersistableObject"/> is a base class for persistable objects.
	/// </summary>
#if (!SILVERLIGHT)
	[Serializable]
#endif
	public class BasePersistableObject: INotifyPropertyChanged 
	{
		#region events
		internal void OnChanged()
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(String.Empty));
		}
        private event PropertyChangedEventHandler PropertyChanged;
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { PropertyChanged += value; }
            remove { PropertyChanged -= value; }
        }
		/// <summary>
		/// Overrides the default behavior.
		/// </summary>
		/// <param name="propertyName">The property name.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (IsEdit)
                _changedDuringEdit = true;
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
		#endregion

		#region fields
		private Guid _id;
		private int _index;
        #region Data Synchronization fields
#if (!SILVERLIGHT)
        [NonSerialized]
#endif
        private ItemsMapTableRow _baseSourceItemMap;
#if (!SILVERLIGHT)
        [NonSerialized]
#endif
        private bool _isEdit;
#if (!SILVERLIGHT)
        [NonSerialized]
#endif
		private bool _changedDuringEdit;
        #endregion
        private Stream _beforeEditState;
        #endregion

        #region ctor
        /// <summary>
		/// Creates new BasePersistableObject object.
		/// </summary>
		internal BasePersistableObject() 
		{
			_id = Guid.NewGuid();
			_index = -1;
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets an array of objects representing the key of the <see cref="BasePersistableObject"/>.
		/// </summary>
		/// <remarks><para>In the current version, the array always contains a single object.
		/// It can be either an <see cref="Int32"/> or <see cref="Guid"/> value depending on
		/// which one is actually used for binding this data.
		/// For example, if you bind AppointmentStorage to the database and set
		/// IndexMapping, this property will return an <see cref="Int32"/> value.
		/// If you set IdMapping, this property will return a <see cref="Guid"/> value.</para>
		/// <para>In future versions working with compound keys might be implemented.
		/// In such case this property will return array of objects composing the key.</para></remarks>
		public object[] Key
		{
			get
			{
				if (_index == -1)
				{
					return new object[]{_id};
				}
				else
				{
					return new object[] { _index };
				}
			}
		}

		/// <summary>
		/// Unique value for serialization purposes.
		/// </summary>
		internal Guid Id
		{
			get
			{
				return _id;
			}
			set
			{
				if (_id != value)
				{
					_id = value;
					OnPropertyChanged("Id");
				}
			}
		}

		internal void SetId(Guid id)
		{
			if (_id != id)
			{
				_id = id;
			}
		}

		/// <summary>
		/// User can specify mapping either for Id property or for Index property
		/// (depending on his database structure or object model).
		/// In unbound mode or if user doesn't specify mapping for any of these properties,
		/// Index will contain index of the object in containing collection.
		/// </summary>
		internal int Index
		{
			get
			{
				return _index;
			}
			set
			{
				if (_index != value)
				{
					_index = value;
					OnPropertyChanged("Index");
                }
			}
		}

		internal void SetIndex(int index)
		{
			if (_index != index)
			{
				_index = index;
			}
		}
		#endregion

#if (!SILVERLIGHT)
		#region serialization
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected BasePersistableObject(SerializationInfo info, StreamingContext context)
		{
			bool initEdit = !IsEdit;
			if (initEdit)
			{
				BeginEditInternal();
			}
			_id = (Guid)info.GetValue("id", typeof(Guid));
			_index = info.GetInt32("index");
			if (initEdit)
				EndEdit();
		}

		/// <summary>
		/// A method called when serializing.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/> 
		/// to populate with data.</param>
		/// <param name="context">The context information about the source or destination 
		/// of the serialization.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("id", _id, typeof(Guid));
			info.AddValue("index", _index);
		}
		#endregion
#endif

		#region Data Synchronization
		/// <summary>
        /// Brings the object in edit mode.
        /// </summary>
        /// <remarks>
        /// A corresponding item from a data source will not be updated on this object's
        /// property changes while the object is in edit mode. Data synchronization will
        /// be performed when the <see cref="EndEdit()"/> method will be called.
        /// </remarks>
        public virtual void BeginEdit()
        {
			if (!_isEdit)
			{
				_isEdit = true;
				_changedDuringEdit = false;
				if (_beforeEditState != null)
				{
					_beforeEditState.Close();
				}
				_beforeEditState = new MemoryStream();
				ToXml(_beforeEditState);
				_beforeEditState.Position = 0;
			}
		}
        /// <summary>
        /// Finishes edit mode started by the <see cref="BeginEdit"/> method and
        /// updates a corresponding data source item.
        /// </summary>
        public void EndEdit()
        {
            EndEdit(true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="canRaiseChangeNotification"></param>
        protected virtual void EndEdit(bool canRaiseChangeNotification)
        {
			if (_isEdit)
			{
				_isEdit = false;
				if (_beforeEditState != null)
				{
					_beforeEditState.Close();
					_beforeEditState = null;
				}
				if (_changedDuringEdit && canRaiseChangeNotification)
					OnPropertyChanged("");
			}
        }

		// only use in this and derived classes in FromXml methods and ctors
		// (in such case there is no need in saving state to xml
        #pragma warning disable 1591
		protected internal void BeginEditInternal()
		{
			if (!_isEdit)
			{
				_isEdit = true;
				_changedDuringEdit = false;
			}
        }
        #pragma warning restore 1591

        /// <summary>
        /// Discards changes since the last <see cref="BeginEdit"/> call.
        /// </summary>
        public virtual void CancelEdit()
        {
            if (_isEdit)
            {
                try
                {
                    bool changed = FromXml(_beforeEditState);
                    _beforeEditState.Close();
                    _beforeEditState = null;
                    if (changed)
                        OnPropertyChanged("");
                }
                finally
                {
                    _isEdit = false;
                }
            }
        }


        /// <summary>
		/// Indicates whether the object is in edit mode, 
		/// which is started using the <see cref="BeginEdit()"/> method.
        /// </summary>
        public bool IsEdit 
        {
            get { return _isEdit; }
        }

        /// <summary>
        /// Keeps a reference to a data source item that this object is based on.
        /// A null value means that there is no base source item.
        /// </summary>
        internal ItemsMapTableRow BaseSourceItemMap
        {
            get { return _baseSourceItemMap; }
        }

        /// <summary>
        /// WARNING: don't call this method! This a special method that must be called
        /// from the BaseStorage.SetCollectionItemBaseItem method only!
        /// </summary>
        /// <param name="sourceItemMap"></param>
        internal void SetBaseSourceItemMap(ItemsMapTableRow sourceItemMap)
        {
            _baseSourceItemMap = sourceItemMap;
        }
        #endregion

		#region XML
#if (!SILVERLIGHT) 
		/// <summary>
		/// Loads an object from the specified <see cref="XmlNode"/>.
		/// </summary>
		/// <param name="node">An <see cref="System.Xml.XmlNode"/> which contains the object data.</param>
		/// <returns>Returns true if object has been changed.</returns>
		public virtual bool FromXml(XmlNode node)
		{
			return false;
		}
#endif
		
		/// <summary>
		/// Loads an object from the specified <see cref="XmlReader"/>.
		/// </summary>
		/// <param name="reader">An <see cref="System.Xml.XmlReader"/> which contains the object data.</param>
		/// <returns>Returns true if object has been changed.</returns>
		public virtual bool FromXml(XmlReader reader)
		{
			return false;
		}

		/// <summary>
		/// Saves an object to the specified <see cref="XmlWriter"/>.
		/// </summary>
		/// <param name="writer">The <see cref="System.Xml.XmlWriter"/> that will receive 
		/// the object data.</param>
		public virtual void ToXml(XmlWriter writer)
		{
		}

		/// <summary>
		/// Loads an object from the specified <see cref="Stream"/>.
		/// </summary>
		/// <param name="stream">The <see cref="System.IO.Stream"/> that contains the object data.</param>
		/// <returns>Returns true if object has been changed.</returns>
		public bool FromXml(Stream stream)
        {
			if (stream == null)
				throw new ArgumentNullException("stream");
			if (!stream.CanRead)
			{
				// just in case
				return false;
			}
			using (XmlReader reader = XmlReader.Create(stream, XmlExchanger._readerSettings))
			{
				return this.FromXml(reader);
			}
        }

		/// <summary>
		/// Saves an object to the specified <see cref="Stream"/>.
		/// </summary>
		/// <param name="stream">The <see cref="System.IO.Stream"/> that contains the object data.</param>
		public void ToXml(Stream stream)
        {
			if (stream == null)
				throw new ArgumentNullException("stream");
			using (XmlWriter wr = XmlWriter.Create(stream, XmlExchanger._writerSettings))
			{

				this.ToXml(wr);
			}
		}
		#endregion

		//------------------------------------------------------------------------------------------
		#region ** string validation
		// remove forbidden XML characters
		// use this method to validate data which might be saved to xml
		// * Tfs issue 4496 - Opening an appointment that contains Unicode control 
		//  character in subject textbox, causes an unhandled exception
		internal static string GetValidString(string sourceString)
		{
            if (string.IsNullOrEmpty(sourceString))
            {
                return String.Empty;
            }
			StringBuilder sb = new StringBuilder(sourceString.Length);

			foreach (var c in sourceString)
			{
				var current = (int)c;
				if (Char.IsSurrogate(c) || current == 0x9 ||
					current == 0xA ||
					current == 0xD ||
					(current >= 0x20 && current <= 0xD7FF) ||
					(current >= 0xE000 && current <= 0xFFFD) ||
					(current >= 0x10000 && current <= 0x10FFFF))
				{
					sb.Append(c);
				}
			}
			return sb.ToString();
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="BaseObject"/> is a base class for resources, labels, statuses, 
	/// categories and contacts. It implements base UI properties and persistence.
	/// </summary>
#if (!SILVERLIGHT)
	[DefaultProperty("Text"),
	Serializable]
#endif
	public class BaseObject : BasePersistableObject
#if (!SILVERLIGHT)
		, ISerializable
#endif
	{
		#region fields
		private Color _color;
		private string _text;
		private string _caption;
		private C1Brush _brush;
		#endregion

		#region ctor
		/// <summary>
		/// Creates the new <see cref="BaseObject"/> object with default settings.
		/// </summary>
		public BaseObject() 
		{
			_color = PlatformIndependenceHelper.GetColor(0);
			_text = string.Empty;
			_caption = string.Empty;
			_brush = new C1Brush();
		}

		/// <summary>
		/// Creates the new <see cref="BaseObject"/> object with specified text.
		/// </summary>
		/// <param name="text">The <see cref="String"/> value.</param>
		public BaseObject(string text)
			: this(PlatformIndependenceHelper.GetColor(0), text, string.Empty, new C1Brush(PlatformIndependenceHelper.GetColor(0)))
		{
		}

		/// <summary>
		/// Creates the new <see cref="BaseObject"/> object with specified text and menu caption.
		/// </summary>
		/// <param name="text">The <see cref="String"/> value.</param>
		/// <param name="menuCaption">The <see cref="String"/> value.</param>
		public BaseObject(string text, string menuCaption)
			: this(PlatformIndependenceHelper.GetColor(0), text, menuCaption, new C1Brush(PlatformIndependenceHelper.GetColor(0)))
		{
		}

		/// <summary>
		/// Creates the new <see cref="BaseObject"/> object with specified color, 
		/// text and menu caption.
		/// </summary>
		/// <param name="color">The <see cref="Color"/> value.</param>
		/// <param name="text">The <see cref="String"/> value.</param>
		/// <param name="menuCaption">The <see cref="String"/> value.</param>
		public BaseObject(Color color, string text, string menuCaption)
			: this(color, text, menuCaption, new C1Brush(color))
		{
		}

		/// <summary>
		/// Creates the new <see cref="BaseObject"/> object with specified parameters.
		/// </summary>
		/// <param name="color">The <see cref="Color"/> value.</param>
		/// <param name="text">The <see cref="String"/> value.</param>
		/// <param name="menuCaption">The <see cref="String"/> value.</param>
		/// <param name="brush">The <see cref="Brush"/> object.</param>
		public BaseObject(Color color, string text, string menuCaption, C1Brush brush)
		{
			_color = color;
			_text =  GetValidString(text);
			_caption = GetValidString(menuCaption);
			_brush = brush;
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets or sets the color of the user interface object.
		/// </summary>
		public Color Color
		{
			get
			{
				return _color;
			}
			set
			{
				if (_color != value)
				{
					_color = value;
					Brush = new C1Brush(value);
					OnPropertyChanged("Color"); 
				}
			}
		}
		internal virtual bool SetColor(Color value)
		{
			if (_color != value)
			{
				_color = value;
				Brush = new C1Brush(value);
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Gets or sets the text which identifies a user interface object. 
		/// </summary>
		public string Text
		{
			get
			{
				return _text;
			}
			set
			{
				value = GetValidString(value);
				if (_text != value)
				{
					_text = value;
                    OnPropertyChanged("Text"); 
                }
			}
		}
		internal bool SetText(string value)
		{
			value = GetValidString(value);
			if (_text != value)
			{
				_text = value;
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Gets or sets the menu caption for the user interface object.
		/// </summary>
		public string MenuCaption
		{
			get
			{
				if (string.IsNullOrEmpty(_caption))
				{
					return _text;
				}
				return _caption;
			}
			set
			{
				value = GetValidString(value);
				if (_caption != value)
				{
					_caption = value;
                    OnPropertyChanged("MenuCaption"); 
                }
			}
		}
		internal bool SetMenuCaption(string value)
		{
			value = GetValidString(value);
			if (_caption != value)
			{
				_caption = value;
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Gets or sets the brush used to fill the user interface object's image.
		/// </summary>
		/// <remarks>Supported persistence only for System.Drawing.Drawing2D.HatchBrush
		/// and System.Drawing.SolidBrush</remarks>
		public C1Brush Brush
		{
			get
			{
				return _brush;
			}
			set
			{
				if (!_brush.Equals(value))
				{
					_brush = value;
                    OnPropertyChanged("Brush"); 
                }
			}
		}
		#endregion

#if (!SILVERLIGHT)
		#region ISerializable
		// A method called when serializing.
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("color", C1Brush.GetARGBstring(_color));
			info.AddValue("text", _text);
			info.AddValue("caption", _caption);

			info.AddValue("brush", _brush, typeof(C1Brush));
		}

		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected BaseObject(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			bool initEdit = !IsEdit;
			if (initEdit)
			{
				BeginEditInternal();
			}
			string str = info.GetString("color");
			_color = PlatformIndependenceHelper.ParseARGBString(str);
			
			_text = info.GetString("text");
			_caption = info.GetString("caption");

			_brush = (C1Brush)info.GetValue("brush", typeof(C1Brush));
			if (initEdit)
				EndEdit();
		}
		#endregion
#endif

		#region XML
#if (!SILVERLIGHT) 
		/// <summary>
		/// Loads an object from the specified <see cref="XmlNode"/>.
		/// </summary>
		/// <param name="node">An <see cref="System.Xml.XmlNode"/> 
		/// which contains the object data.</param>
		public override bool FromXml(XmlNode node)
		{
			using (System.IO.TextReader tr = new System.IO.StringReader(node.OuterXml))
			using (XmlReader reader = XmlReader.Create(tr, XmlExchanger._readerSettings))
			{
				return FromXml(reader);
			}
		}
#endif

		/// <summary>
		/// Loads an object from the specified <see cref="XmlReader"/>.
		/// </summary>
		/// <param name="reader">An <see cref="System.Xml.XmlReader"/> which contains the object data.</param>
		/// <returns>Returns true if object has been changed.</returns>
		public override bool FromXml(XmlReader reader)
		{
			bool dirty = false;
			bool initEdit = !IsEdit;
			if (initEdit)
			{
				BeginEditInternal();
			}

			try
			{
				if (reader.Read() && reader.IsStartElement() && reader.HasAttributes)
				{
					SetId(new Guid(reader.GetAttribute("Id")));
					SetIndex(int.Parse(reader.GetAttribute("Index"), CultureInfo.InvariantCulture));
				}
				else
				{
					return false;
				}

				Color color = PlatformIndependenceHelper.GetColor(0);
				string text = "";
				string menuCaption = "";
				C1Brush newBrush = _brush;
				bool brushChanged = false;

				while (reader.Read() && reader.NodeType == XmlNodeType.Element)
				{
					switch (reader.Name)
					{
						case "Color":
							color = PlatformIndependenceHelper.ParseARGBString(XmlExchanger.ReadStringValue(reader));
							break;
						case "Brush":
							if (newBrush == null)
							{
								newBrush = new C1Brush();
								dirty = true;
							}
							using (XmlReader reader1 = reader.ReadSubtree())
							{
								reader1.Read();
								while (reader1.Read() && reader.NodeType == XmlNodeType.Element)
								{
									Color c;
									switch (reader1.Name)
									{
										case "ForeColor":
											c = PlatformIndependenceHelper.ParseARGBString(XmlExchanger.ReadStringValue(reader1));
											if (newBrush.ForeColor != c)
											{
												newBrush.ForeColor = c;
												brushChanged = true;
											}
											break;
										case "BackColor":
											c = PlatformIndependenceHelper.ParseARGBString(XmlExchanger.ReadStringValue(reader1));
											if (newBrush.BackColor != c)
											{
												newBrush.BackColor = c;
												brushChanged = true;
											}
											break;
										case "Style":
											int ival = int.Parse(XmlExchanger.ReadStringValue(reader1), CultureInfo.InvariantCulture);
											if ((int)newBrush.Style != ival)
											{
												newBrush.Style = (C1BrushStyleEnum)ival;
												brushChanged = true;
											}
											break;
									}
								}
							}
							break;
						case "Text":
							text = XmlExchanger.ReadStringValue(reader);
							break;
						case "MenuCaption":
							menuCaption = XmlExchanger.ReadStringValue(reader);
							break;
					}
				}

				if (color != Color)
				{
					Color = color;
					dirty = true;
				}
				if (brushChanged)
				{
					_brush = newBrush;
					dirty = true;
					OnPropertyChanged("Brush");
				}
				if (!text.Equals(Text))
				{
					Text = text;
					dirty = true;
				}
				if (!menuCaption.Equals(MenuCaption))
				{
					MenuCaption = menuCaption;
					dirty = true;
				}
			}
			finally
			{
				if (initEdit)
					EndEdit();
			}
			return dirty;
		}

		/// <summary>
		/// Saves an object into specified <see cref="XmlWriter"/>.
		/// </summary>
		/// <param name="writer">The <see cref="System.Xml.XmlWriter"/> 
		/// that will receive the object data.</param>
		public override void ToXml(XmlWriter writer)
		{
			writer.WriteStartElement(GetType().Name);
			writer.WriteAttributeString("Id", Id.ToString());
			writer.WriteAttributeString("Index", Index.ToString(CultureInfo.InvariantCulture));
			if (!PlatformIndependenceHelper.IsEmptyColor(Color)) 
			{
				writer.WriteElementString("Color", C1Brush.GetARGBstring(Color));
			}

			if (_brush != null && _brush.Style != C1BrushStyleEnum.Transparent
				&& !(PlatformIndependenceHelper.IsEmptyColor(_brush.BackColor) &&
				PlatformIndependenceHelper.IsEmptyColor(_brush.ForeColor)))
			{
				writer.WriteStartElement("Brush");
				writer.WriteElementString("Style", ((int)_brush.Style).ToString(CultureInfo.InvariantCulture));
				writer.WriteElementString("BackColor", C1Brush.GetARGBstring(_brush.BackColor));
				if (!PlatformIndependenceHelper.IsEmptyColor(_brush.ForeColor))
				{
					writer.WriteElementString("ForeColor", C1Brush.GetARGBstring(_brush.ForeColor));
				}
				writer.WriteEndElement();
			}
				
			if (!String.IsNullOrEmpty(Text))
			{
				writer.WriteElementString("Text", Text);
			}
			if (!String.IsNullOrEmpty(MenuCaption))
			{
				writer.WriteElementString("MenuCaption", MenuCaption);
			}

			writer.WriteEndElement();
		}
		#endregion

		/// <summary>
		/// Overrides the default behavior.
		/// </summary>
		/// <returns>The text which identifies a user interface object.</returns>
		public override string ToString()
		{
			return MenuCaption;
		}
	}

	/// <summary>
	/// The <see cref="BaseCollection{T}"/> is a base generic class for all collections 
	/// in C1Schedule object model.
	/// </summary>
	/// <typeparam name="T">The type of the objects which will be stored in the collection.
	/// It should be derived from the <see cref="BasePersistableObject"/> class
	/// and have the default parameter-less constructor.</typeparam>
#if (!SILVERLIGHT)
	[TypeConverterAttribute(typeof(System.ComponentModel.CollectionConverter))] 
#endif
	public class BaseCollection<T> : C1ObservableKeyedCollection<Guid, T>  
		where T : BasePersistableObject
    {
        #region fields
 #if (!SILVERLIGHT)
       [NonSerialized]
#endif
        private readonly object _owner = null; 
        #endregion

        #region events
        internal event EventHandler<BaseCollectionEventArgs<T>> ItemAdded;
		private void OnItemAdded(object sender, BaseCollectionEventArgs<T> e)
		{
			if (ItemAdded != null)
			{
				ItemAdded(this, e);
			}
		}
		internal event EventHandler<BaseCollectionEventArgs<T>> ItemChanged;
		internal void OnItemChanged(object sender, BaseCollectionEventArgs<T> e)
		{
			if (ItemChanged != null)
			{
				ItemChanged(this, e);
			}
		}
		internal event EventHandler<BaseCollectionEventArgs<T>> ItemRemoved;
		private void OnItemRemoved(object sender, BaseCollectionEventArgs<T> e)
		{
			if (ItemRemoved != null)
			{
				ItemRemoved(this, e);
			}
		}
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="BaseCollection{T}"/> class.
		/// </summary>
		/// <param name="owner">The owning storage.</param>
		public BaseCollection(object owner) 
		{
            _owner = owner;
		}
		#endregion

		#region public interface
		/// <summary>
		/// Adds a new custom object to the collection.
		/// If the item already exists in the collection, its index is returned.
		/// </summary>
		/// <returns>Returns the index of newly added custom object.</returns>
		public new int Add(T item)
		{
            int index = IndexOf(item);
            if (index < 0)
            {
                base.Add(item);
                index = base.IndexOf(item);
            }
            return index;
		}

		/// <summary>
		/// Check if collection contains an item with specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the item to locate.</param>
		/// <returns>True if the item is found in the collection; otherwise, false.</returns>
		public bool Contains(int index)
		{
			foreach (T item in Items)
			{
				if (item.Index == index)
				{
					return true;
				}
			}
			return false;
		}

        /// <summary>
        /// Gets the element with the specified key if any. 
        /// </summary>
        /// <param name="key">The key of the element to get.</param>
        /// <returns>The element with the specified key. If an element with the specified key is not found, return null.</returns>
        public new T this[Guid key]
        {
            get
            {
                if (this.Contains(key))
                {
                    return base[key];
                }
                return null;
            }
        }

		#endregion

        #region internal properties
		/// <summary>
		/// 
		/// </summary>
        protected internal object OwnerInternal
        {
            get { return _owner; }
        }
        #endregion

        #region internal methods
        /// <summary>
		/// Call this method after removing items from the unbound storage.
		/// </summary>
		internal void RefreshIndices()
		{
			foreach (T obj in Items)
			{
				obj.SetIndex(IndexOf(obj));
			}
		}

		internal T FindByIndex(int index)
		{
			foreach (T obj in Items)
			{
				if (obj.Index == index)
				{
					return obj;
				}
			}
			return null;
		}
		#endregion

		#region Serialization
		internal T[] GetItems()
		{
			T[] array = new T[Items.Count];
			Items.CopyTo(array, 0);
			return array;
		}

		/// <summary>
		/// Adds an array of objects to the collection.
		/// This methods clears the collection before adding new items. 
		/// Don't use it if you want to keep preexisting items.
		/// </summary>
		/// <param name="items">An array of items.</param>
		public virtual void AddRange(T[] items)
		{
			Clear();
			foreach (T item in items)
			{
				Add(item);
			}
		}

		/// <summary>
		/// Adds an array of objects to the collection.
		/// This methods clears the collection before adding new items. 
		/// Don't use it if you want to keep preexisting items.
		/// </summary>
		/// <param name="items">An array of items.</param>
		public void AddRange(object[] items)
		{
			AddRange((T[])items);
		}
		#endregion

		#region overrides
		/// <summary>
		/// Overrides default behavior of collection at inserting new items.
		/// </summary>
		/// <param name="index">The zero-based index of the item to insert.</param>
		/// <param name="item">The object to insert.</param>
		protected override void InsertItem(int index, T item)
		{
			if (Contains(item.Id))
			{
				return;
			}
			base.InsertItem(index, item);
			((INotifyPropertyChanged)item).PropertyChanged += new PropertyChangedEventHandler(BaseCollection_PropertyChanged);
			OnItemAdded(this, new BaseCollectionEventArgs<T>(item));
		}

		void BaseCollection_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			OnItemChanged(this, new BaseCollectionEventArgs<T>((T)sender));
		}

		/// <summary>
		/// Extracts the key from the specified element.
		/// </summary>
		/// <param name="item">The element from which to extract the key.</param>
		/// <returns>The key for the specified element.</returns>
		protected override Guid GetKeyForItem(T item)
		{
			return item.Id;
		}

		/// <summary>
		/// Overrides base class implementation.
		/// </summary>
		/// <param name="index">The zero-based index of the item to remove.</param>
		protected override void RemoveItem(int index)
		{
			T item = this[index];
			((INotifyPropertyChanged)item).PropertyChanged -= new PropertyChangedEventHandler(BaseCollection_PropertyChanged);
			base.RemoveItem(index);
			OnItemRemoved(this, new BaseCollectionEventArgs<T>(item));
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="BaseList{T}"/> is a base generic class for all lists 
	/// in C1Schedule's object model.
	/// Only objects existing in the owning collection can be added to this list.
	/// </summary>
	/// <typeparam name="T">The type of the objects which will be stored in the list.
	/// It should be derived from the <see cref="BasePersistableObject"/> class.</typeparam>
	public class BaseList<T> : C1ObservableCollection<T>  
		where T : BasePersistableObject
	{
		/// <summary>
		/// 
		/// </summary>
		private BaseCollection<T> _owner;

		internal BaseList(BaseCollection<T> owner)
		{
			_owner = owner;
		}

		internal BaseList(BaseList<T> list)
		{
			_owner = list._owner;
			foreach (T item in list)
			{
				Add(item);
			}
		}

		internal void AddItems(Guid[] ids, int[] indices)
		{
			if (_owner != null)
			{
				foreach (Guid id in ids)
				{
					if (_owner.Contains(id))
					{
						Add(_owner[id]);
					}
				}
				foreach (int index in indices)
				{
					if (index != -1)
					{
						T item = _owner.FindByIndex(index);
						if (item != null)
						{
							Add(item);
						}
					}
				}
			}
		}

		internal static bool IsEquals(BaseList<T> list1, BaseList<T> list2)
		{
			if (list1 == list2)
			{
				return true;
			}
			else if (list1 == null || list1.Count == 0)
			{
				if (list2 == null || list2.Count == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else if (list2 == null || list2.Count == 0)
			{
				return false;
			}
			else if (list1.Count != list2.Count)
			{
				return false;
			}
			foreach (T item in list1)
			{
				if (!list2.Contains(item))
				{
					return false;
				}
			}
			return true;
		}

		#region overrides
		/// <summary>
		/// Overrides default behavior of Collection.
		/// Prevents from adding items non-existent in the parent collection.
		/// </summary>
		/// <param name="index">The zero-based index of the item to insert.</param>
		/// <param name="item">The object to insert.</param>
		protected override void InsertItem(int index, T item)
		{
			if (_owner.Contains(item) && !base.Contains(item))
			{
				base.InsertItem(index, item);
			}
		}

		/// <summary>
		/// Overrides default behavior of Collection.
		/// Prevents from adding items non-existent in the parent collection.
		/// </summary>
		/// <param name="index">The zero-based index of the item.</param>
		/// <param name="item">The object to set.</param>
		protected override void SetItem(int index, T item)
		{
			if (_owner.Contains(item))
			{
				base.SetItem(index, item);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			foreach (T item in Items)
			{
				sb.Append(item.ToString() + "; ");
			}
			return sb.ToString();
		}
		#endregion

		/// <summary>
		/// Gets the reference to the owning collection.
		/// </summary>
		protected BaseCollection<T> Owner
		{
			get
			{
				return _owner;
			}
		}
	}

	internal class BaseCollectionEventArgs<T> : EventArgs
	{
		private T _item;

		internal BaseCollectionEventArgs(T item)
		{
			_item = item;
		}

		internal T Item
		{
			get
			{
				return _item;
			}
		}
	}
}
