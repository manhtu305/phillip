﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ComponentModel;
using System.Globalization;

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="Category"/> class represents the category which can be
	/// associated with the <see cref="Appointment"/> object. 
	/// </summary>
#if (!SILVERLIGHT)
	[Serializable]
#endif
	public class Category : BaseObject
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Category"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Int32"/> value which should be used as category key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Category(int key)
            : this()
        {
            base.SetIndex(key);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Category"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Guid"/> value which should be used as category key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Category(Guid key)
            : this()
        {
            base.SetId(key);
        }

		/// <summary>
		/// Creates the new <see cref="Category"/> object.
		/// </summary>
		public Category()
		{
		}

		/// <summary>
		/// Creates custom <see cref="Category"/> object with specified text.
		/// </summary>
		/// <param name="text">The text of category.</param>
		public Category(string text)
			: base (text)
		{
		}

		internal Category(Guid id, int index, string text)
			: base(text)
		{
			base.SetId(id);
			base.SetIndex(index);
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Category(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
#endif
	}

	/// <summary>
	/// The <see cref="CategoryCollection"/> is a collection of <see cref="Category"/> 
	/// objects which represents all available categories in C1Schedule object model.
	/// By default it contains the set of predefined categories. 
	/// </summary>
	public class CategoryCollection : BaseCollection<Category>
	{
		CategoryStorage _storage;
		/// <summary>
		/// Initializes a new instance of the <see cref="CategoryCollection"/> class.
		/// </summary>
        public CategoryCollection(): this(null) 
		{
		}
        internal CategoryCollection(object owner): base(null) 
        {
			_storage = owner as CategoryStorage;
            LoadDefaults();
        }

		/// <summary>
		/// Restores the collection to its default state.
		/// </summary>
		/// <remarks>The <see cref="LoadDefaults"/> method removes all 
		/// custom categories from the collection and leaves only the standard ones.
		/// </remarks>
		public void LoadDefaults()
		{
			List<Category> defaultList = GetDefaults();
			Clear();
			this.AddRange(defaultList.ToArray());
		}

		/// <summary>
		/// Returns the list of predefined categories.
		/// </summary>
		/// <returns>The <see cref="List{Category}"/> object.</returns>
		public List<Category> GetDefaults()
		{
			CultureInfo info;
			if (_storage == null)
			{
				info = CalendarInfo.Culture;
			}
			else
			{
				info = _storage.ScheduleStorage.Info.CultureInfo;
			}

			List<Category> list = new List<Category>();

#if END_USER_LOCALIZATION
			list.Add(GetDefault(new Category(new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}"), 0, 
				Strings.Categories.Item("Business", info)))); 
			list.Add(GetDefault(new Category(new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}"), 1, 
				Strings.Categories.Item("Competition", info))));
			list.Add(GetDefault(new Category(new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}"), 2, 
				Strings.Categories.Item("Favorites", info))));
			list.Add(GetDefault(new Category(new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}"), 3, 
				Strings.Categories.Item("Gifts", info))));
			list.Add(GetDefault(new Category(new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}"), 4, 
				Strings.Categories.Item("Goals", info))));
			list.Add(GetDefault(new Category(new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}"), 5, 
				Strings.Categories.Item("Holiday", info))));
			list.Add(GetDefault(new Category(new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}"), 6, 
				Strings.Categories.Item("HolidayCards", info))));
			list.Add(GetDefault(new Category(new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}"), 7, 
				Strings.Categories.Item("HotContacts", info))));
			list.Add(GetDefault(new Category(new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}"), 8, 
				Strings.Categories.Item("Ideas", info))));
			list.Add(GetDefault(new Category(new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}"), 9, 
				Strings.Categories.Item("International", info))));
			list.Add(GetDefault(new Category(new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}"), 10, 
				Strings.Categories.Item("KeyCustomer", info))));
			list.Add(GetDefault(new Category(new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}"), 11, 
				Strings.Categories.Item("Miscellanneous", info))));
			list.Add(GetDefault(new Category(new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}"), 12, 
				Strings.Categories.Item("Personal", info))));
			list.Add(GetDefault(new Category(new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}"), 13, 
				Strings.Categories.Item("PhoneCalls", info))));
			list.Add(GetDefault(new Category(new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}"), 14, 
				Strings.Categories.Item("Status", info))));
			list.Add(GetDefault(new Category(new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}"), 15, 
				Strings.Categories.Item("Strategies", info))));
			list.Add(GetDefault(new Category(new Guid("{113A6732-1002-4b45-9944-D80252096F38}"), 16, 
				Strings.Categories.Item("Suppliers", info))));
			list.Add(GetDefault(new Category(new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}"), 17, 
				Strings.Categories.Item("Time", info))));
			list.Add(GetDefault(new Category(new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}"), 18, 
				Strings.Categories.Item("VIP", info))));
			list.Add(GetDefault(new Category(new Guid("{D5032327-1388-4552-A072-25C67218E938}"), 19,
				Strings.Categories.Item("Waiting", info))));
#elif WINFX
			list.Add(GetDefault(new Category(new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}"), 0,
				C1Localizer.GetString("Categories", "Business", "Business", info))));
			list.Add(GetDefault(new Category(new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}"), 1,
				C1Localizer.GetString("Categories", "Competition", "Competition", info))));
			list.Add(GetDefault(new Category(new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}"), 2,
				C1Localizer.GetString("Categories", "Favorites", "Favorites", info))));
			list.Add(GetDefault(new Category(new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}"), 3,
				C1Localizer.GetString("Categories", "Gifts", "Gifts", info))));
			list.Add(GetDefault(new Category(new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}"), 4,
				C1Localizer.GetString("Categories", "Goals", "Goals/Objectives", info))));
			list.Add(GetDefault(new Category(new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}"), 5,
				C1Localizer.GetString("Categories", "Holiday", "Holiday", info))));
			list.Add(GetDefault(new Category(new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}"), 6,
				C1Localizer.GetString("Categories", "HolidayCards", "Holiday Cards", info))));
			list.Add(GetDefault(new Category(new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}"), 7,
				C1Localizer.GetString("Categories", "HotContacts", "Hot Contacts", info))));
			list.Add(GetDefault(new Category(new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}"), 8,
				C1Localizer.GetString("Categories", "Ideas", "Ideas", info))));
			list.Add(GetDefault(new Category(new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}"), 9,
				C1Localizer.GetString("Categories", "International", "International", info))));
			list.Add(GetDefault(new Category(new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}"), 10,
				C1Localizer.GetString("Categories", "KeyCustomer", "Key Customer", info))));
			list.Add(GetDefault(new Category(new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}"), 11,
				C1Localizer.GetString("Categories", "Miscellanneous", "Miscellaneous", info))));
			list.Add(GetDefault(new Category(new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}"), 12,
				C1Localizer.GetString("Categories", "Personal", "Personal", info))));
			list.Add(GetDefault(new Category(new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}"), 13,
				C1Localizer.GetString("Categories", "PhoneCalls", "Phone Calls", info))));
			list.Add(GetDefault(new Category(new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}"), 14,
				C1Localizer.GetString("Categories", "Status", "Status", info))));
			list.Add(GetDefault(new Category(new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}"), 15,
				C1Localizer.GetString("Categories", "Strategies", "Strategies", info))));
			list.Add(GetDefault(new Category(new Guid("{113A6732-1002-4b45-9944-D80252096F38}"), 16,
				C1Localizer.GetString("Categories", "Suppliers", "Suppliers", info))));
			list.Add(GetDefault(new Category(new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}"), 17,
				C1Localizer.GetString("Categories", "Time", "Time & Expenses", info))));
			list.Add(GetDefault(new Category(new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}"), 18,
				C1Localizer.GetString("Categories", "VIP", "VIP", info))));
			list.Add(GetDefault(new Category(new Guid("{D5032327-1388-4552-A072-25C67218E938}"), 19,
				C1Localizer.GetString("Categories", "Waiting", "Waiting", info))));
#elif (SILVERLIGHT)
			list.Add(GetDefault(new Category(new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}"), 0,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Business)));
			list.Add(GetDefault(new Category(new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}"), 1,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Competition)));
			list.Add(GetDefault(new Category(new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}"), 2,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Favorites)));
			list.Add(GetDefault(new Category(new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}"), 3,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Gifts)));
			list.Add(GetDefault(new Category(new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}"), 4,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Goals)));
			list.Add(GetDefault(new Category(new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}"), 5,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Holiday)));
			list.Add(GetDefault(new Category(new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}"), 6,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.HolidayCards)));
			list.Add(GetDefault(new Category(new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}"), 7,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.HotContacts)));
			list.Add(GetDefault(new Category(new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}"), 8,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Ideas)));
			list.Add(GetDefault(new Category(new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}"), 9,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.International)));
			list.Add(GetDefault(new Category(new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}"), 10,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.KeyCustomer)));
			list.Add(GetDefault(new Category(new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}"), 11,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Miscellanneous)));
			list.Add(GetDefault(new Category(new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}"), 12,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Personal)));
			list.Add(GetDefault(new Category(new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}"), 13,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.PhoneCalls)));
			list.Add(GetDefault(new Category(new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}"), 14,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Status)));
			list.Add(GetDefault(new Category(new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}"), 15,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Strategies)));
			list.Add(GetDefault(new Category(new Guid("{113A6732-1002-4b45-9944-D80252096F38}"), 16,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Suppliers)));
			list.Add(GetDefault(new Category(new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}"), 17,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Time)));
			list.Add(GetDefault(new Category(new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}"), 18,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.VIP)));
			list.Add(GetDefault(new Category(new Guid("{D5032327-1388-4552-A072-25C67218E938}"), 19,
                C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Waiting)));
#else
			list.Add(GetDefault(new Category(new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}"), 0, 
				C1Localizer.GetString("Business"))));
			list.Add(GetDefault(new Category(new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}"), 1, 
				C1Localizer.GetString("Competition"))));
			list.Add(GetDefault(new Category(new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}"), 2, 
				C1Localizer.GetString("Favorites"))));
			list.Add(GetDefault(new Category(new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}"), 3, 
				C1Localizer.GetString("Gifts"))));
			list.Add(GetDefault(new Category(new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}"), 4, 
				C1Localizer.GetString("Goals/Objectives"))));
			list.Add(GetDefault(new Category(new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}"), 5, 
				C1Localizer.GetString("Holiday"))));
			list.Add(GetDefault(new Category(new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}"), 6, 
				C1Localizer.GetString("Holiday Cards"))));
			list.Add(GetDefault(new Category(new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}"), 7, 
				C1Localizer.GetString("Hot Contacts"))));
			list.Add(GetDefault(new Category(new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}"), 8, 
				C1Localizer.GetString("Ideas"))));
			list.Add(GetDefault(new Category(new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}"), 9, 
				C1Localizer.GetString("International"))));
			list.Add(GetDefault(new Category(new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}"), 10, 
				C1Localizer.GetString("Key Customer"))));
			list.Add(GetDefault(new Category(new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}"), 11, 
				C1Localizer.GetString("Miscellanneous"))));
			list.Add(GetDefault(new Category(new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}"), 12, 
				C1Localizer.GetString("Personal"))));
			list.Add(GetDefault(new Category(new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}"), 13, 
				C1Localizer.GetString("Phone Calls"))));
			list.Add(GetDefault(new Category(new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}"), 14, 
				C1Localizer.GetString("Status"))));
			list.Add(GetDefault(new Category(new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}"), 15, 
				C1Localizer.GetString("Strategies"))));
			list.Add(GetDefault(new Category(new Guid("{113A6732-1002-4b45-9944-D80252096F38}"), 16, 
				C1Localizer.GetString("Suppliers"))));
			list.Add(GetDefault(new Category(new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}"), 17, 
				C1Localizer.GetString("Time & Expenses"))));
			list.Add(GetDefault(new Category(new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}"), 18, 
				C1Localizer.GetString("VIP"))));
			list.Add(GetDefault(new Category(new Guid("{D5032327-1388-4552-A072-25C67218E938}"), 19,
				C1Localizer.GetString("Waiting"))));
#endif

			return list;
		}

		internal void RefreshDefaults()
		{
			CultureInfo culture;
			if (_storage == null)
			{
				culture = CalendarInfo.Culture;
			}
			else
			{
				culture = _storage.ScheduleStorage.Info.CultureInfo;
			}

#if END_USER_LOCALIZATION
			Category category = this[new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Business", culture);
			}
			category = this[new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Competition", culture);
			}
			category = this[new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Favorites", culture);
			}
			category = this[new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Gifts", culture);
			}
			category = this[new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Goals", culture);
			}
			category = this[new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Holiday", culture);
			}
			category = this[new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("HolidayCards", culture);
			}
			category = this[new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("HotContacts", culture);
			}
			category = this[new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Ideas", culture);
			}
			category = this[new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("International", culture);
			}
			category = this[new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("KeyCustomer", culture);
			}
			category = this[new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Miscellanneous", culture);
			}
			category = this[new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Personal", culture);
			}
			category = this[new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("PhoneCalls", culture);
			}
			category = this[new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Status", culture);
			}
			category = this[new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Strategies", culture);
			}
			category = this[new Guid("{113A6732-1002-4b45-9944-D80252096F38}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Suppliers", culture);
			}
			category = this[new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Time", culture);
			}
			category = this[new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("VIP", culture);
			}
			category = this[new Guid("{D5032327-1388-4552-A072-25C67218E938}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = Strings.Categories.Item("Waiting", culture);
			}
#elif WINFX
			Category category = this[new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Business", "Business", culture);
			}
			category = this[new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Competition", "Competition", culture);
			}
			category = this[new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Favorites", "Favorites", culture);
			}
			category = this[new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Gifts", "Gifts", culture);
			}
			category = this[new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Goals", "Goals/Objectives", culture);
			}
			category = this[new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Holiday", "Holiday", culture);
			}
			category = this[new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "HolidayCards", "Holiday Cards", culture);
			}
			category = this[new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "HotContacts", "Hot Contacts", culture);
			}
			category = this[new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Ideas", "Ideas", culture);
			}
			category = this[new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "International", "International", culture);
			}
			category = this[new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "KeyCustomer", "Key Customer", culture);
			}
			category = this[new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Miscellanneous", "Miscellaneous", culture);
			}
			category = this[new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Personal", "Personal", culture);
			}
			category = this[new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "PhoneCalls", "Phone Calls", culture);
			}
			category = this[new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Status", "Status", culture);
			}
			category = this[new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Strategies", "Strategies", culture);
			}
			category = this[new Guid("{113A6732-1002-4b45-9944-D80252096F38}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Suppliers", "Suppliers", culture);
			}
			category = this[new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Time", "Time & Expenses", culture);
			}
			category = this[new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "VIP", "VIP", culture);
			}
			category = this[new Guid("{D5032327-1388-4552-A072-25C67218E938}")];
			if (category != null)
			{
				category.Text = category.MenuCaption = C1Localizer.GetString("Categories", "Waiting", "Waiting", culture);
			}
#elif SILVERLIGHT
            Category category = this[new Guid("{F958FC94-4592-416d-8D7A-02D58D729E96}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Business;
            }
            category = this[new Guid("{DF734D68-F1FE-4702-8ED6-FC97F5449F39}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Competition;
            }
            category = this[new Guid("{B5E3EB3C-3DA3-429d-B7D7-0134F0F623C0}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Favorites;
            }
            category = this[new Guid("{9DBE4448-5A10-446a-9AA4-90FCCFC9ED66}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Gifts;
            }
            category = this[new Guid("{095B80E4-FDB3-44ad-83FF-1B1F0892B1F3}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Goals;
            }
            category = this[new Guid("{9EC4A041-AD4A-47ea-A9E6-86945E14EB15}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Holiday;
            }
            category = this[new Guid("{AF9B1B04-BF73-46f7-9E64-CA956FF7D8CC}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.HolidayCards;
            }
            category = this[new Guid("{F687E97A-9570-4b35-821A-011FD6CC69CF}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.HotContacts;
            }
            category = this[new Guid("{DFC83FD4-052A-4a56-8532-5C1958F20962}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Ideas;
            }
            category = this[new Guid("{780A68B2-D156-48b9-AD8A-429BF1DEE602}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.International;
            }
            category = this[new Guid("{0996DC5E-D62F-42af-83FE-7C73177CEDAA}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.KeyCustomer;
            }
            category = this[new Guid("{2F669DEC-4CA9-45cb-AC0E-7CD9A90658E3}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Miscellanneous;
            }
            category = this[new Guid("{2A638FEC-2C50-40db-92AE-B6DD365094EC}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Personal;
            }
            category = this[new Guid("{F1217A0B-43AD-4f44-B11A-637673E71E9D}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.PhoneCalls;
            }
            category = this[new Guid("{5274D772-1EBC-43ba-B249-2AA02FE9ADCE}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Status;
            }
            category = this[new Guid("{79B8A685-EA58-4cd8-AC51-5D87674E004F}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Strategies;
            }
            category = this[new Guid("{113A6732-1002-4b45-9944-D80252096F38}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Suppliers;
            }
            category = this[new Guid("{BA2E9460-8F47-45df-BA0A-D3CE9FA20967}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Time;
            }
            category = this[new Guid("{87A9BF82-2024-4c40-8AEA-D9B7E11AF6B5}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.VIP;
            }
            category = this[new Guid("{D5032327-1388-4552-A072-25C67218E938}")];
            if (category != null)
            {
                category.Text = category.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Categories.Waiting;
            }
#endif
		}

		private Category GetDefault(Category cat)
		{
			if (this.Contains(cat.Id))
			{
				Category def = this[cat.Id];
				def.Index = cat.Index;
				def.Text = cat.Text;
				return def;
			}
			return cat;
		}
	}

	/// <summary>
	/// The <see cref="CategoryList"/> is a list of <see cref="Category"/> objects.
	/// Only objects existing in the owning <see cref="CategoryCollection"/> object 
	/// may be added to this list.
	/// Use the <see cref="CategoryList"/> to associate the set of <see cref="Category"/> objects 
	/// with an <see cref="Appointment"/> object.
	/// </summary>
	public class CategoryList : BaseList<Category>
	{
		internal CategoryList(CategoryCollection owner)
			: base(owner)
		{
		}

		internal CategoryList(CategoryList list)
			:base(list)
		{
		}

		internal new CategoryCollection Owner
		{
			get
			{
				return (CategoryCollection)base.Owner;
			}
		}
	}
}
