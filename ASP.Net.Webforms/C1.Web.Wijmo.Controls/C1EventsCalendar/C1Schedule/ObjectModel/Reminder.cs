﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="Reminder"/> class represents an object which allows users to keep track 
	/// of upcoming appointments by scheduling a pop-up dialog box to appear at a given time. 
	/// </summary>
	public class Reminder : INotifyPropertyChanged 
	{
		#region fields
		private Appointment _owner;
		internal ReminderCollection _coll;

		private DateTime _originalReminderDate;
		private DateTime _nextReminderDate;

		private bool _isActive;
		private RegisteredWaitHandle _registeredWaitHandle;
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="Reminder"/> class.
		/// </summary>
		/// <param name="owner"></param>
		internal Reminder(Appointment owner)
		{
			_owner = owner;
            Update(7);
            if (_owner.ParentCollection != null)
            {
                _coll = _owner.ParentCollection.Reminders;
            }
		}
        // Updates properties from the owner appointment.
        internal void Update()
        {
            Update(3);
        }
        private void Update(int delayInSeconds)
        {
            DateTime now = DateTime.UtcNow.ToLocalTime();
            DateTime date = now;
            try
            {
                date = _owner.Start - _owner.ReminderTimeBeforeStart;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                date = now;
            }
            _originalReminderDate = date;
            SetNextReminderDate(_originalReminderDate, delayInSeconds);
            if (_coll != null)
            {
                if (_owner._inCollection ||
                    (_owner.ParentRecurrence != null && _owner.ParentRecurrence._inCollection))
                {
                    _coll.OnReminderChange(this, new ReminderEventArgs(this));
                }
            }
        }
        #endregion

		#region object model
		#region properties
		/// <summary>
		/// Gets the owning <see cref="Appointment"/> object. 
		/// </summary>
		public Appointment ParentAppointment
		{
			get
			{
				return _owner;
			}
		}

		/// <summary>
		/// Gets the <see cref="String"/> value representing the window caption text 
		/// of the <see cref="Appointment"/> object which 
		/// the <see cref="Reminder"/> object is applied to.
		/// </summary>
		public string Caption
		{
			get
			{
				return _owner.Subject;
			}
		}

		/// <summary>
		/// Gets a value that determines if the <see cref="Reminder"/> object 
		/// is currently active (visible). 
		/// </summary>
		public bool IsActive
		{
			get
			{
				return _isActive;
			}
			internal set
			{
				if (_isActive != value)
				{
					_isActive = value;
					OnPropertyChanged("IsActive");
				}
			}
		}

		/// <summary>
		/// Gets the <see cref="DateTime"/> value indicating the next time the specified 
		/// reminder will occur. 
		/// </summary>
		/// <remarks>The <see cref="NextReminderDate"/> property value changes every time 
		/// the object's <see cref="Snooze"/> method is executed or when the user 
		/// clicks the Snooze button.</remarks>
		public DateTime NextReminderDate
		{
			get
			{
				return _nextReminderDate;
			}
			private set // other classes must use SetNextReminderDate
            {
                if (_nextReminderDate != value)
                {
                    _nextReminderDate = value;
                    OnPropertyChanged("NextReminderDate");
                    _owner.DirtyReminderNextDate();
                }
            }
		}

        internal void SetNextReminderDate(DateTime sugestedDate, int delayInSeconds)
        {
            DateTime now = DateTime.UtcNow.ToLocalTime();
            NextReminderDate = (sugestedDate < now) ? now.AddSeconds(delayInSeconds) : sugestedDate;
        }

		/// <summary>
		/// Gets the <see cref="DateTime"/> value specifying the original date and time 
		/// that the specified reminder is set to occur. 
		/// It is calculated using the following formula: 
		/// <see cref="OriginalReminderDate"/> = <see cref="Appointment.Start"/> 
		///   - <see cref="Appointment.ReminderTimeBeforeStart"/>.
		/// </summary>
		/// <remarks>This value corresponds to the original date and time 
		/// value before the <see cref="Snooze"/> method is executed or the user 
		/// clicks the Snooze button.</remarks>
		public DateTime OriginalReminderDate
		{
			get
			{
				return _originalReminderDate;
			}
		}
		#endregion

		#region methods
		/// <summary>
		/// The <see cref="Snooze"/> method notifies the scheduler to defer the triggering 
		/// of a reminder by the specified interval.
		/// </summary>
		/// <param name="delay">The <see cref="TimeSpan"/>value specifying the interval
		/// of time to wait before displaying the reminder again.</param>
		public void Snooze(TimeSpan delay)
		{
            try
            {
                NextReminderDate = DateTime.UtcNow.ToLocalTime().Add(delay);
                IsActive = false;
                _coll.OnReminderSnooze(this, new ReminderEventArgs(this));
            }
            catch (System.ArgumentOutOfRangeException)
            {
                Dismiss();
            }
		}

		/// <summary>
		/// Dismisses the current <see cref="Reminder"/> object.
		/// </summary>
		public void Dismiss()
		{
            IsActive = false;
			_owner.ReminderSet = false;
		}
		#endregion
		#endregion

		#region internal
		internal RegisteredWaitHandle Handle
		{
			get { return _registeredWaitHandle; }
			set	{ _registeredWaitHandle = value; }
		}
		#endregion

        #region INotifyPropertyChanged
        private event PropertyChangedEventHandler PropertyChanged;
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { PropertyChanged += value; }
            remove { PropertyChanged -= value; }
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

	/// <summary>
	/// The <see cref="ReminderCollection"/> is a collection of all <see cref="Reminder"/> objects 
	/// that represents the reminders for all pending items.
	/// </summary>
	/// <remarks>Use the <see cref="C1ScheduleStorage.Reminders"/> property to get a reference  
	/// to the Reminders collection. 
	/// Reminders are created programmatically when a new <see cref="Appointment"/> object 
	/// is created with a reminder. For example, a reminder is created when 
	/// an <see cref="Appointment"/> object is created and the <see cref="Appointment.ReminderSet"/>
	/// property is set to true. 
	/// </remarks>
	public class ReminderCollection : C1ObservableCollection<Reminder>, IDisposable 
	{
		// For every Reminder in collection the new delegate that is waiting for Reminder's timeout
		// is registered. This delegate queues to the thread pool. A worker thread will execute 
		// the delegate when the time-out interval elapses.
		// Each time callback method WaitProc executes it fires ReminderFire event. 

		#region events
		/// <summary>
		/// Occurs after a reminder is added.
		/// </summary>
		internal event EventHandler<ReminderEventArgs> ReminderAdd;
		internal void OnReminderAdd(object sender, ReminderEventArgs e)
		{
			if (ReminderAdd != null)
			{
				ReminderAdd(sender, e);
			}
		}
		/// <summary>
		/// Occurs after a reminder has been modified.
		/// </summary>
		internal event EventHandler<ReminderEventArgs> ReminderChange;
		internal void OnReminderChange(object sender, ReminderEventArgs e)
		{
            ScheduleReminder(e.Reminder);
			if (ReminderChange != null)
			{
				ReminderChange(sender, e);
			}
		}
		/// <summary>
		/// Occurs before the reminder is executed.
		/// </summary>
		internal event EventHandler<ReminderEventArgs> ReminderFire;
		internal void OnReminderFire(object sender, ReminderEventArgs e)
		{
            if (ReminderFire != null)
			{
				ReminderFire(sender, e);
			}
		}
		/// <summary>
		/// Occurs when a reminder is dismissed using the Snooze button.
		/// </summary>
		internal event EventHandler<ReminderEventArgs> ReminderSnooze;
		internal void OnReminderSnooze(object sender, ReminderEventArgs e)
		{
            ScheduleReminder(e.Reminder);
			if (ReminderSnooze != null)
			{
				ReminderSnooze(sender, e);
			}
		}
		/// <summary>
		/// Occurs when a <see cref="Reminder"/>  object has been removed from the collection.
		/// </summary>
		/// <remarks>A reminder can be removed from the Reminders collection 
		/// by any of the following means:
		/// - The Reminders collection's Remove method. 
		/// - The Reminder object's Dismiss method. 
		/// - When the user clicks the Dismiss button. 
		/// - When a user turns off a meeting reminder from within the associated item.
		/// - When a user deletes an item that contains a reminder. </remarks>
		internal event EventHandler<ReminderEventArgs> ReminderRemove;
		internal void OnReminderRemove(object sender, ReminderEventArgs e)
		{
			if (ReminderRemove != null)
			{
				ReminderRemove(sender, e);
			}
		}
		#endregion

		#region fields
		private WaitOrTimerCallback _callBack; // represents a method to be called when a WaitHandle times out. 
		private AutoResetEvent _ev; // dummy event for using in RegisterWaitForSingleObject.
		private bool disposed = false;
		#endregion;

		#region ctor
		/// <summary>
		/// Initialize new <see cref="ReminderCollection"/> object.
		/// </summary>
		internal ReminderCollection()
		{
			_callBack = new WaitOrTimerCallback(WaitProc);
			_ev = new AutoResetEvent(false);
		}
		#endregion

		#region overrides
		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item.</param>
		/// <param name="item">The <see cref="C1.C1Schedule.Reminder"/> object to insert.</param>
		protected override void InsertItem(int index, Reminder item)
		{
			Appointment app = item.ParentAppointment;
			if (!app.IsRecurring && !Items.Contains(item))
			{
				DateTime now = DateTime.UtcNow.ToLocalTime();
				List<Reminder> remindersToDeactivate = new List<Reminder>();
				//check for occurrences
				if (app.Start < now && app.RecurrenceState != RecurrenceStateEnum.NotRecurring)
				{
					lock (Items)
					{
						foreach (Reminder rem in Items)
						{
							Appointment apWithRaisedRem = rem.ParentAppointment;
							if (apWithRaisedRem.RecurrenceState == RecurrenceStateEnum.Occurrence && apWithRaisedRem.Start < now && apWithRaisedRem.ParentRecurrence == app.ParentRecurrence)
							{
								if (apWithRaisedRem.Start < app.Start)
								{
									// deactivate old reminder
                                    remindersToDeactivate.Add(rem);
								}
								else
								{
									// don't add new reminder
									return;
								}
							}
						}
					}
				}
				item._coll = this;
                ScheduleReminder(item);
				lock (Items)
				{
					OnReminderAdd(this, new ReminderEventArgs(item));
					base.InsertItem(index, item);
				}
                foreach (Reminder r in remindersToDeactivate)
				{
                    DeactivateReminder(r);
				}
			}
		}

		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item to remove.</param>
		protected override void RemoveItem(int index)
		{
			Reminder rem;
			lock (Items)
			{
				rem = Items[index];
			}
            DeactivateReminder(rem);
			OnReminderRemove(this, new ReminderEventArgs(rem));
			base.RemoveItem(index);
		}

		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		protected override void ClearItems()
		{
			lock (Items)
			{
				for (int i = 0; i < Items.Count; i++)
				{
					Reminder rem = Items[i];
                    DeactivateReminder(rem);
					OnReminderRemove(this, new ReminderEventArgs(rem));
				}
				base.ClearItems();
			}
		}

		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item.</param>
		/// <param name="item">The <see cref="C1.C1Schedule.Reminder"/> object to set.</param>
		protected override void SetItem(int index, Reminder item)
		{
			OnReminderChange(this, new ReminderEventArgs(item));
			if (!item.ParentAppointment.IsRecurring)
			{
				base.SetItem(index, item);
			}
		}

        private void ScheduleReminder(Reminder reminder)
        {
            DeactivateReminder(reminder);
            if (!reminder.ParentAppointment.IsRecurring)
            {
                reminder.SetNextReminderDate(reminder.NextReminderDate, 7);
                TimeSpan timeout = reminder.NextReminderDate - DateTime.UtcNow.ToLocalTime();
                if (timeout.TotalMilliseconds < Int32.MaxValue)
                {
                    // register wait handle
                    reminder.Handle = ThreadPool.RegisterWaitForSingleObject(_ev, _callBack, reminder, timeout, true);
                }
            }
        }
        private void DeactivateReminder(Reminder reminder)
        {
            reminder.IsActive = false;
            // un-register wait handle
            if (reminder.Handle != null)
            {
                reminder.Handle.Unregister(null);
            }
        }
		#endregion

		#region internal stuff
		// update all reminders
		internal void UpdateAll()
		{
            foreach (Reminder rem in Items)
			{
				rem.Update();
			}
		}

		/// <summary>
		/// Method to be called when it is time to show reminder.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="timedOut"></param>
		internal void WaitProc(object state, bool timedOut)
		{
            // Fire ReminderFire event
			Reminder rem = (Reminder)state;
			OnReminderFire(this, new ReminderEventArgs(rem));
		}
		#endregion

		#region IDisposable Members
		/// <summary>
		/// Releases all unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the
		// runtime from inside the finalizer and you should not reference
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.disposed)
			{
				if (disposing)
				{
					Clear();
				}
				_ev.Close();
				_ev = null;
				disposed = true;
			}
		}
		#endregion
	}

	/// <summary>
	/// Describes event data for <see cref="Reminder"/> object events.
	/// </summary>
	public class ReminderEventArgs : EventArgs
	{
		private Reminder _reminder;

		/// <summary>
		/// Initializes a new instance of the <see cref="ReminderEventArgs"/> 
		/// class with the specified <see cref="Reminder"/> object. 
		/// </summary>
		/// <param name="reminder">The <see cref="Reminder"/> object.</param>
		public ReminderEventArgs(Reminder reminder)
		{
			_reminder = reminder;
		}

		/// <summary>
		/// Gets the reminder which the event was raised for. 
		/// </summary>
		public Reminder Reminder
		{
			get
			{
				return _reminder;
			}
		}
	}

	/// <summary>
	/// Delegate for handling the cancelable event involving a single <see cref="Reminder"/> object. 
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">The <see cref="C1.C1Schedule.CancelReminderEventArgs"/> 
	/// that contains the event data.</param>
	public delegate void CancelReminderEventHandler(object sender, CancelReminderEventArgs e);

	/// <summary>
	/// Describes event data for cancelable Reminder events.
	/// </summary>
	public class CancelReminderEventArgs : EventArgs
	{
		private Reminder _reminder;
		private bool _cancel;

		/// <summary>
		/// Initializes a new instance of the <see cref="CancelReminderEventArgs"/>
		/// class with the specified <see cref="Reminder"/> object. 
		/// </summary>
		/// <param name="reminder">The <see cref="C1.C1Schedule.Reminder"/> object.</param>
		public CancelReminderEventArgs(Reminder reminder)
		{
			_reminder = reminder;
		}
		/// <summary>
		/// Gets the reminder which the event was raised for. 
		/// </summary>
		public Reminder Reminder
		{
			get
			{
				return _reminder;
			}
		}

		/// <summary>
		/// Set to true to cancel the operation.
		/// </summary>
		public bool Cancel
		{
			get
			{
				return _cancel;
			}
			set
			{
				_cancel = value;
			}
		}
	}

}
