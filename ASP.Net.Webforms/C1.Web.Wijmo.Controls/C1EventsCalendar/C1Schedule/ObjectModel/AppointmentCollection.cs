﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

// ToDo: 
// Attachments
// BillingInformation?
// Companies?
// IsConflict?
// UserProperties?

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="AppointmentCollection"/> is a collection of all 
	/// <see cref="Appointment"/> objects in the C1Schedule component.
	/// </summary>
	public class AppointmentCollection : BaseCollection<Appointment>
	{
		#region events
		/// <summary>
		/// Occurs when RecurrenceState of an appointment is changed.
		/// </summary>
		internal event EventHandler<AppointmentEventArgs> AppointmentRecurrenceStateChanged;
		internal void OnAppointmentRecurrenceStateChanged(object sender, AppointmentEventArgs e)
		{
			Appointment app = e.Appointment;
            _days.RemoveAppointment(app, true);
            // get occurrences to fill days, reminders and Actions
            GetOccurrences(app, _info.FirstDate, _info.LastTime);
            if (!app.IsRecurring)
            {
                _days.AddAppointment(app);
            }
            base.OnItemChanged(sender, new BaseCollectionEventArgs<Appointment>(app));
			if (AppointmentRecurrenceStateChanged != null)
			{
				AppointmentRecurrenceStateChanged(sender, e);
			}
		}

		/// <summary>
		/// Occurs when a custom action of an appointment executes. 
		/// </summary>
		internal event CancelAppointmentEventHandler AppointmentCustomAction;
		internal void OnAppointmentCustomAction(object sender, CancelAppointmentEventArgs e)
		{
			if (AppointmentCustomAction != null)
			{
				AppointmentCustomAction(sender, e);
			}
		}
		/// <summary>
		/// Occurs when an appointment was added.
		/// </summary>
		internal event EventHandler<AppointmentEventArgs> AppointmentAdded;
		internal void OnAppointmentAdded(object sender, AppointmentEventArgs e)
		{
			if (AppointmentAdded != null)
			{
				AppointmentAdded(sender, e);
			}
		}
		/// <summary>
		/// Occurs when an appointment was removed.
		/// </summary>
		internal event EventHandler<AppointmentEventArgs> AppointmentRemoved;
		internal void OnAppointmentRemoved(object sender, AppointmentEventArgs e)
		{
            Appointment app = e.Appointment;
			_days.RemoveAppointment(app);
			if (app.RecurrenceState == RecurrenceStateEnum.Exception || app.RecurrenceState == RecurrenceStateEnum.Occurrence)
			{
				base.OnItemChanged(sender, new BaseCollectionEventArgs<Appointment>(app.ParentRecurrence));
			}
			if (AppointmentRemoved != null)
			{
				AppointmentRemoved(sender, e);
			}
		}
		/// <summary>
		/// Occurs when an appointment was changed.
		/// </summary>
		internal event EventHandler<AppointmentEventArgs> AppointmentChanged;
		internal void OnAppointmentChanged(object sender, AppointmentEventArgs e)
		{
			Appointment app = e.Appointment;
			if (app.IsRecurring)
			{
                _days.RemoveAppointment(app);
				app.GetRecurrencePattern().SetDirty();
                // get occurrences to fill days, reminders and Actions
                GetOccurrences(app, _info.FirstDate, _info.LastTime);
			}
			else
			{
				_days.ChangeAppointment(app);
			}
			if (app.RecurrenceState == RecurrenceStateEnum.Exception || app.RecurrenceState == RecurrenceStateEnum.Occurrence)
			{
				base.OnItemChanged(sender, new BaseCollectionEventArgs<Appointment>(app.ParentRecurrence));
			}
			else
			{
				base.OnItemChanged(sender, new BaseCollectionEventArgs<Appointment>(app));
			}
			if (AppointmentChanged != null)
			{
				AppointmentChanged(sender, e);
			}
		}
		#endregion

		#region fields
		private ActionCollection _actions;
		private ContactCollection _contacts;
        private ContactCollection _owners;
        private ReminderCollection _reminders;
		private StatusCollection _statuses;
		private LabelCollection _labels;
		private CategoryCollection _categories;
		private ResourceCollection _resources;
		private CalendarInfo _info;
		private bool _enableEvents;
		private DayCollection _days;
		#endregion

		#region ctor
		internal AppointmentCollection(object owner, 
			ReminderCollection reminders,
			StatusCollection statuses, 
			LabelCollection labels, 
			CategoryCollection categories, 
			ResourceCollection resources,
			ContactCollection contacts,
            ContactCollection owners,
            CalendarInfo info, ActionCollection actions)
            : base(owner)
		{
			_days = ((AppointmentStorage)owner).ScheduleStorage.Days;
			_actions = actions;
			_reminders = reminders;
			_statuses	= statuses;
			_labels		= labels;
			_categories = categories;
			_resources	= resources;
			_contacts	= contacts;
            _owners = owners;
            _info = info;
			_enableEvents = true;
		}
		#endregion

        #region object model
        /// <summary>
		/// Gets the reference to the owning <see cref="AppointmentStorage"/> object.
        /// </summary>
        public AppointmentStorage ParentStorage 
        {
            get { return OwnerInternal as AppointmentStorage; }
        }

		internal ActionCollection Actions
		{
			get
			{
				return _actions;
			}
		}
		internal StatusCollection Statuses
		{
			get
			{
				return _statuses;
			}
		}
		internal ContactCollection Contacts
		{
			get
			{
				return _contacts;
			}
		}
        internal ContactCollection Owners
        {
            get
            {
                return _owners;
            }
        }
        internal ReminderCollection Reminders
		{
			get
			{
				return _reminders;
			}
		}
		internal LabelCollection Labels
		{
			get
			{
				return _labels;
			}
		}
		internal CategoryCollection Categories
		{
			get
			{
				return _categories;
			}
		}
		internal ResourceCollection Resources
		{
			get
			{
				return _resources;
			}
		}
		internal CalendarInfo CalendarInfo
		{
			get
			{
				return _info;
			}
		}
		internal DayCollection Days
		{
			get
			{
				return _days;
			}
		}
		#endregion

        #region interface
		/// <summary>
		/// Creates the new <see cref="Appointment"/> object 
		/// with default settings and adds it to the collection.
		/// </summary>
		/// <returns>The <see cref="Appointment"/> object.</returns>
		public Appointment Add()
		{
			Appointment app = new Appointment();
			this.Add(app);
			return app;
		}

		/// <summary>
		/// Creates the new <see cref="Appointment"/> object 
		/// with specified parameters and adds it to the collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the appointment.</param>
		/// <returns>The <see cref="Appointment"/> object.</returns>
		public Appointment Add(DateTime start, DateTime end)
		{
			Appointment app = new Appointment(start, end);
			this.Add(app);
			return app;
		}

		/// <summary>
		/// Creates the new <see cref="Appointment"/> object 
		/// with specified parameters and adds it to the collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="duration">The <see cref="TimeSpan"/> value which specifies 
		/// the duration of the appointment.</param>
		/// <returns>The <see cref="Appointment"/> object.</returns>
		public Appointment Add(DateTime start, TimeSpan duration)
		{
			Appointment app = new Appointment(start, duration);
			this.Add(app);
			return app;
		}

		/// <summary>
		/// Creates the new <see cref="Appointment"/> object 
		/// with specified parameters and adds it to the collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the appointment.</param>
		/// <param name="subject">The <see cref="String"/> value which contains 
		/// the subject of the appointment.</param>
		/// <returns>The <see cref="Appointment"/> object.</returns>
		public Appointment Add(DateTime start, DateTime end, string subject)
		{
			Appointment app = new Appointment(start, end, subject);
			this.Add(app);
			return app;
		}

		/// <summary>
		/// Creates the new <see cref="Appointment"/> object 
		/// with specified parameters and adds it to the collection.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="duration">The <see cref="TimeSpan"/> value which specifies 
		/// the duration of the appointment.</param>
		/// <param name="subject">The <see cref="String"/> value which contains 
		/// the subject of the appointment.</param>
		/// <returns>The <see cref="Appointment"/> object.</returns>
		public Appointment Add(DateTime start, TimeSpan duration, string subject)
		{
			Appointment app = new Appointment(start, duration, subject);
			this.Add(app);
			return app;
		}

		/// <summary>
		/// Returns a specific instance of the Appointment object on the specified date.
		/// </summary>
		/// <param name="pattern"></param>
		/// <param name="startDate"></param>
		/// <param name="info"></param>
		/// <returns></returns>
		/// <remarks>The GetOccurrence method generates an exception if no appointment 
		/// of that series exists on the specified date.</remarks>
		internal static Appointment GetOccurrence(Appointment pattern, DateTime startDate, CalendarInfo info)
		{
			if (!pattern.IsRecurring)
			{
				return null;
			}
			return pattern.GetRecurrencePattern().GetOccurrence(startDate, info);
		}

		/// <summary>
		/// Retrieves the <see cref="AppointmentList"/> object which contains 
		/// the full list of <see cref="Appointment"/> objects in the specified time interval. 
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the interval.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the interval.</param>
		/// <returns>The <see cref="AppointmentList"/> object.</returns>
		/// <remarks>This method will create instances of the <see cref="Appointment"/>
		/// object for recurring appointments if they haven't been created yet.
		/// </remarks>
		public AppointmentList GetOccurrences(DateTime start, DateTime end)
		{
			AppointmentList apps = new AppointmentList(this);
			for (int i = 0; i < Count; i++)
			{
				GetOccurences(ref apps, Items[i], start, end);
			}
			return apps;
		}

		/// <summary>
		/// Retrieves the <see cref="AppointmentList"/> object which contains 
		/// the full list of <see cref="Appointment"/> objects in the specified time interval. 
		/// </summary>
		/// <param name="groupOwner">The <see cref="BaseObject"/> object determining the appointment group.</param>
		/// <param name="groupBy">The <see cref="String"/> value determining the type of grouping.</param>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the interval.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the interval.</param>
		/// <param name="includePrivateAppointments">The <see cref="Boolean"/> value determining whether 
		/// to include private appointments.</param>
		/// <returns>The <see cref="AppointmentList"/> object.</returns>
		/// <remarks>This method will create instances of the <see cref="Appointment"/>
		/// object for recurring appointments if they haven't been created yet.
		/// </remarks>
		public AppointmentList GetOccurrences(BaseObject groupOwner, string groupBy, 
			DateTime start, DateTime end, bool includePrivateAppointments)
		{
			AppointmentList apps = new AppointmentList(this);
			for (int i = 0; i < Count; i++)
			{
				GetOccurences(ref apps, Items[i], start, end);
			}
			if (!includePrivateAppointments)
			{
				for (int i = apps.Count - 1; i >= 0; i--)
				{
					if (apps[i].Private )
					{
						apps.RemoveAt(i);
					}
				}
			}
			groupBy = groupBy.Trim();
			if (!String.IsNullOrEmpty(groupBy))
			{
				for (int i = apps.Count - 1; i >= 0; i--)
				{
					if (!apps[i].IsGroupMember(groupOwner, groupBy))
					{
						apps.RemoveAt(i);
					}
				}
			}

			return apps;
		}

		/// <summary>
		/// Retrieves the <see cref="AppointmentList"/> object which contains 
		/// the list of occurrences of the specified master appointment 
		/// in the specified time interval. 
		/// </summary>
		/// <param name="appointment">The <see cref="Appointment"/> object 
		/// representing the master appointment.</param>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the interval.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the interval.</param>
		/// <returns>The <see cref="AppointmentList"/> object.</returns>
		/// <remarks>This method will create instances of the <see cref="Appointment"/>
		/// object for recurring appointments if they haven't been created yet.</remarks>
		public AppointmentList GetOccurrences(Appointment appointment, DateTime start, DateTime end)
		{
			AppointmentList apps = new AppointmentList(this);
			GetOccurences(ref apps, appointment, start, end);
			return apps;
		}

		private void GetOccurences(ref AppointmentList list, Appointment appointment, DateTime start, DateTime end)
		{
			if (appointment.IsRecurring)
			{
				RecurrencePattern pattern = appointment.GetRecurrencePattern();
				// adjust start

				DateTime date = pattern.PatternStartDate;
				if (date < start)
				{
					date = start.Subtract(appointment.Duration).AddDays(-6).Date;
				}
				foreach (Appointment exception in pattern.Exceptions)
				{
					if (exception.Start < date)
					{
						date = exception.Start.Date;
					}
				}
				if (!pattern.NoEndDate && end > pattern.PatternEndDate.Date.AddDays(1))
				{
					end = pattern.PatternEndDate;
					foreach (Appointment exception in pattern.Exceptions)
					{
						if (exception.Start > end)
						{
							end = exception.Start;
						}
					}
					end = end.Date.AddDays(1);
				}
				end = (date == end) ? date.AddMinutes(1) : end;
				while (date < end) 
				{
					Appointment occ = GetOccurrence(appointment, date, _info);
					if (occ != null)
					{
						if (!list.Contains(occ))
						{
							if ((occ.Start < end && occ.End > start)
								|| (occ.Duration.Ticks == 0 && occ.Start == start))
							{
								list.Add(occ);
							}
						}
					}
					date = date.AddDays(1);
				}
			}
			else
			{
				if (start < _info.FirstDate)
				{
					start = _info.FirstDate;
				}
				if ( (appointment.Start < end && appointment.End > start) 
					|| ( appointment.Duration.Ticks == 0 && appointment.Start == start) )
				{
					list.Add(appointment);
					if (appointment.Reminder != null)
					{
						_reminders.Add(appointment.Reminder);
					}
					if (appointment.Action != null)
					{
						_actions.Add(appointment.Action);
					}
				}
			}
		}

		internal void RefreshData(bool full)
		{
			try
			{
				_days.BeginUpdate();
				if (full)
				{
                    _actions.Clear();
                    _reminders.Clear();
					_days.Clear();
					_days.FillDayCollection();
				}
				else
				{
					_days.ClearAppointments();
				}
                DateTime start = _info.FirstDate;
                DateTime end = _info.LastTime;
                for (int i = 0; i < Count; i++)
                {
                    Appointment app = Items[i];
                    if (app.IsRecurring)
                    {
                        app.GetRecurrencePattern().SetDirty();
                    }
                    else 
                    {
                        _days.AddAppointment(app);
                    }
                    // get occurrences to fill days, reminders and Actions
                    GetOccurrences(app, start, end);
                }
            }
			finally
			{
				_days.EndUpdate();
			}
		}
		
		internal void RefreshData(DateTime start, DateTime end)
		{
			try
			{
				_days.BeginUpdate();
				_days.FillDayCollection(start, end);
                for (int i = 0; i < Count; i++)
                {
                    Appointment app = Items[i];
                    if (app.IsRecurring)
                    {
                        RecurrencePattern pattern = app.GetRecurrencePattern();
                        bool needUpdate = pattern.PatternStartDate >= start && pattern.PatternStartDate < end;
                        if (!pattern.NoEndDate && pattern.Occurrences == 0 && pattern.PatternEndDate < start)
                        {
                            needUpdate = false;
                        }
                        if (!needUpdate)
                        {
                            foreach (Appointment ex in pattern.Exceptions)
                            {
                                if (ex.Start < end && ex.End >= start)
                                {
                                    needUpdate = true;
                                    break;
                                }
                            }
                        }
                        if (needUpdate)
                        {
                            _days.RemoveAppointment(app, true);
                            app.GetRecurrencePattern().SetDirty();
                            // get occurrences to fill days, reminders and Actions
                            GetOccurrences(app, _info.FirstDate, _info.LastTime);
                        }
                    }
                    else if (app.Start < end && app.End >= start)
                    {
                        _days.AddAppointment(app);
                        // get occurrences to fill days, reminders and Actions
                        GetOccurrences(app, start, end);
                    }
                }
            }
			finally
			{
				_days.EndUpdate();
			}
		}

		/// <summary>
		/// Retrieves the <see cref="AppointmentList"/> object which contains 
		/// the list of occurrences of <see cref="Appointment"/> objects 
		/// which are in conflict with the specified appointment.
		/// </summary>
		/// <param name="appointment">The <see cref="Appointment"/> object.</param>
		/// <returns>The <see cref="AppointmentList"/> object.</returns>
		public AppointmentList GetConflicts(Appointment appointment)
		{
			AppointmentList list = new AppointmentList(this);
			for (int i = 0; i < Count; i++)
			{
				Appointment app = Items[i];
				if (!app.Equals(appointment) && IsConflict(app, appointment))
				{
					list.Add(app);
				}
			}
			return list;
		}

		private static bool IsConflict(Appointment app1, Appointment app2)
		{
			if ( (app1.Start >= app2.End || app2.Start >= app1.End) )
			{
				return false;
			}
			return true;
		}
		#endregion

		#region overrides
		/// <summary>
		/// Clears the collection.
		/// </summary>
		protected override void ClearItems()
		{
			_days.ClearAppointments();
			while (Items.Count > 0)
			{
				RemoveItem(0);
			}
		}

		/// <summary>
		/// Overrides the default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item to remove.</param>
		protected override void RemoveItem(int index)
		{
			Appointment app = Items[index];
			base.RemoveItem(index);
			app._inCollection = false;
			OnAppointmentRemoved(this, new AppointmentEventArgs(app));
		}

		/// <summary>
		/// Overrides the default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item.</param>
		/// <param name="item">The <see cref="C1.C1Schedule.Appointment"/> object to insert.</param>
		protected override void InsertItem(int index, Appointment item)
		{
			item.ParentCollection = this;
			item._inCollection = true;

            // get occurrences to fill days, reminders and Actions
            GetOccurrences(item, _info.FirstDate, _info.LastTime);
            if (!item.IsRecurring)
            {
                _days.AddAppointment(item);
            }

			base.InsertItem(index, item);
			if (_enableEvents)
			{
				OnAppointmentAdded(this, new AppointmentEventArgs(item));
			}
		}

		/// <summary>
		/// Overrides the default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item.</param>
		/// <param name="item">The <see cref="C1.C1Schedule.Appointment"/> object to set.</param>
		protected override void SetItem(int index, Appointment item)
		{
			base.SetItem(index, item);
		}
		#endregion

		#region Serialization
		/// <summary>
		/// Adds an array of <see cref="Appointment"/> objects to the collection.
		/// This methods clears collection before adding new items. 
		/// Don't use it if you want to save previously existed items.
		/// </summary>
		/// <param name="items">An array of <see cref="Appointment"/> objects.</param>
		public override void AddRange(Appointment[] items)
		{
			_enableEvents = false;
			_reminders.Clear();
			base.AddRange(items);
			_enableEvents = true;
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="AppointmentList"/> is a sortable collection of 
	/// <see cref="Appointment"/> objects.
	/// </summary>
	/// <remarks>The <see cref="AppointmentList"/> is not guaranteed to be sorted. 
	/// You must sort the <see cref="AppointmentList"/> before performing operations
	/// that require the <see cref="AppointmentList"/> to be sorted.</remarks>
#if (!SILVERLIGHT)
	[Serializable]
#endif
	public class AppointmentList : Collection<Appointment> 
	{
#if (!SILVERLIGHT)
		[NonSerialized]
#endif
		private Collection<Appointment> _owner;
#if (!SILVERLIGHT)
        [NonSerialized]
#endif
        private List<Appointment> _items;
#if (!SILVERLIGHT)
		[NonSerialized]
#endif
		private static AppointmentComparer _defaultComparer = AppointmentComparer.Default;

		internal AppointmentList(AppointmentCollection owner)
		{
			_owner = owner;
            _items = (List<Appointment>)Items;
		}

		internal Collection<Appointment> ParentCollection
		{
			get
			{
				return _owner;
			}
			set
			{
				_owner = value;
			}
		}

		/// <summary>
		/// Sorts the elements in the entire AppointmentList using the default comparer
		/// (by the Appointment.Start value).
		/// </summary>
		public void Sort ()
		{
            _items.Sort(_defaultComparer);
		}
 
		/// <summary>
		/// Sorts the elements in the entire AppointmentList using 
		/// the specified System.Comparison. 
		/// </summary>
		/// <param name="comparison">The <see cref="System.Comparison{Appointment}"/> 
		/// to use when comparing elements.</param>
		public void Sort (Comparison<Appointment> comparison)
		{
            _items.Sort(comparison);
		}
 
		/// <summary>
		/// Sorts the elements in the entire AppointmentList using 
		/// the specified comparer. 
		/// </summary>
		/// <param name="comparer">The IComparer{Appointment} implementation to use 
		/// when comparing elements, or a null reference (Nothing in Visual Basic) 
		/// to use the default comparer.</param>
		public void Sort(IComparer<Appointment> comparer)   
		{
            _items.Sort(comparer ?? _defaultComparer);
		}
 
		/// <summary>
		/// Sorts the elements in a range of elements in AppointmentList using 
		/// the specified comparer. 
		/// </summary>
		/// <param name="index">The zero-based starting index of the range to sort.</param>
		/// <param name="count">The length of the range to sort.</param>
		/// <param name="comparer">The IComparer{Appointment} implementation to use 
		/// when comparing elements, or a null reference (Nothing in Visual Basic) 
		/// to use the default comparer.</param>
		public void Sort(int index, int count, IComparer<Appointment> comparer) 
		{
            _items.Sort(index, count, comparer ?? _defaultComparer);
		}

		/// <summary>
		/// Creates a copy of the list by copying items to the new list.
		/// </summary>
		/// <returns></returns>
		internal AppointmentList Clone()
		{
			AppointmentList list = new AppointmentList((AppointmentCollection)_owner);
			for (int i = 0; i < Count; i++)
			{
				list.Add(Items[i]);
			}
			return list;
		}
	}

	/// <summary>
	/// Compares <see cref="Appointment"/> objects by their start time.
	/// </summary>
	public class AppointmentComparer : Comparer<Appointment>
	{
		private static AppointmentComparer _default;

		/// <summary>
		/// Returns an existing instance of <see cref="AppointmentComparer"/> class if any;
		/// or creates a new one.
		/// </summary>
		public static new AppointmentComparer Default
		{
			get
			{
				return _default ?? (_default = new AppointmentComparer());
			}
		}
		/// <summary>
		/// Performs a comparison of two <see cref="Appointment"/> objects and returns 
		/// a value indicating whether the one <see cref="Appointment"/> starts earlier, 
		/// at the same time, or later than the other. 
		/// </summary>
		/// <param name="x">The first <see cref="Appointment"/> to compare.</param>
		/// <param name="y">The second <see cref="Appointment"/> to compare.</param>
		/// <returns>Less than zero - x occurs earlier than y.
		/// Zero - x and y occur at the same time.
		/// Greater than zero - x occurs later than y.</returns>
		public override int Compare(Appointment x, Appointment y)
		{
			return DateTime.Compare(x.Start, y.Start);
		}
	}

	/// <summary>
	/// The <see cref="AppointmentEventArgs"/> class describes event data for <see cref="Appointment"/> events.
	/// </summary>
	public class AppointmentEventArgs : EventArgs
	{
		private Appointment _appointment;

		/// <summary>
		/// Initializes a new instance of the <see cref="AppointmentEventArgs"/>
		/// class with the specified appointment. 
		/// </summary>
		/// <param name="appointment">The <see cref="C1.C1Schedule.Appointment"/> object.</param>
		public AppointmentEventArgs(Appointment appointment)
		{
			_appointment = appointment;
		}

		/// <summary>
		/// Gets the appointment which the event was raised for. 
		/// </summary>
		public Appointment Appointment
		{
			get
			{
				return _appointment;
			}
		}
	}

	/// <summary>
	/// Delegate for handling the cancelable event involving a single 
	/// <see cref="Appointment"/> object. 
	/// </summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">The <see cref="C1.C1Schedule.CancelAppointmentEventArgs"/> 
	/// that contains the event data.</param>
	public delegate void CancelAppointmentEventHandler(object sender, CancelAppointmentEventArgs e);

	/// <summary>
	/// The <see cref="CancelAppointmentEventArgs"/> class describes event data 
	/// for <see cref="Appointment"/> events.
	/// </summary>
	public class CancelAppointmentEventArgs : EventArgs
	{
		private Appointment _appointment;
		private bool _cancel;

		/// <summary>
		/// Initializes a new instance of the <see cref="CancelAppointmentEventArgs"/>
		/// class with the specified appointment. 
		/// </summary>
		/// <param name="appointment">The <see cref="Appointment"/> object.</param>
		public CancelAppointmentEventArgs(Appointment appointment)
		{
			_appointment = appointment;
		}

		/// <summary>
		/// Gets the <see cref="Appointment"/> object which the event was raised for. 
		/// </summary>
		public Appointment Appointment
		{
			get
			{
				return _appointment;
			}
		}

		/// <summary>
		/// Set to true to cancel the operation.
		/// </summary>
		public bool Cancel
		{
			get
			{
				return _cancel;
			}
			set
			{
				_cancel = value;
			}
		}
	}
}

