﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ComponentModel;

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="Contact"/> class represents the person information.
	/// Not real class just sample for prototyping.
	/// </summary>
#if (!SILVERLIGHT)
	[DefaultProperty("Text"),
	Serializable]
#endif
	public class Contact : BaseObject
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Contact"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Int32"/> value which should be used as contact key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Contact(int key)
            : this()
        {
            base.SetIndex(key);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Contact"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Guid"/> value which should be used as contact key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Contact(Guid key)
            : this()
        {
            base.SetId(key);
        }
		/// <summary>
		/// Creates the new <see cref="Contact"/> object.
		/// </summary>
		public Contact()
		{
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Contact(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
#endif
	}

	/// <summary>
	/// The <see cref="ContactCollection"/> is a collection of <see cref="Contact"/> 
	/// objects which represents all available contacts in C1Schedule object model.
	/// </summary>
	public class ContactCollection : BaseCollection<Contact>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ContactCollection"/> class.
		/// </summary>
		public ContactCollection() : this(null) 
		{
		}
        internal ContactCollection(object owner): base(owner) 
        {
        }
    }

	/// <summary>
	/// The <see cref="ContactList"/> is a list of <see cref="Contact"/> objects.
	/// Only objects existing in the owning <see cref="ContactCollection"/> object 
	/// may be added to this list.
	/// Use the <see cref="ContactList"/> to associate the set of <see cref="Contact"/> objects
	/// with an <see cref="Appointment"/> object.
	/// </summary>
	public class ContactList : BaseList<Contact>
	{
		internal ContactList(ContactCollection owner)
			: base (owner)
		{
		}

		internal ContactList(ContactList list)
			: base(list)
		{
		}

		internal new ContactCollection Owner
		{
			get
			{
				return (ContactCollection)base.Owner;
			}
		}
	}
}
