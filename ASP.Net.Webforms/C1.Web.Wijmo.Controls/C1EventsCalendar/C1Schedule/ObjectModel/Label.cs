﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ComponentModel;
using System.Globalization;

#if (WINFX || SILVERLIGHT)
using System.Windows.Media;
#else
using System.Drawing;
#endif

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// The <see cref="Label"/> class represents the label which can be
	/// associated with the <see cref="Appointment"/> object. 
	/// </summary>
#if (!SILVERLIGHT)
	[Serializable]
#endif
	public class Label : BaseObject
	{
		#region ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Int32"/> value which should be used as label key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Label(int key)
            : this()
        {
            base.SetIndex(key);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Guid"/> value which should be used as label key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Label(Guid key)
            : this()
        {
            base.SetId(key);
        }

        /// <summary>
		/// Creates the new <see cref="Label"/> object with default parameters.
		/// </summary>
		public Label() : base ()
		{
		}

		/// <summary>
		/// Creates custom <see cref="Label"/> object with specified text.
		/// </summary>
		/// <param name="text">The text of the label.</param>
		public Label(string text)
			: base (text)
		{
		}

		/// <summary>
		/// Creates custom <see cref="Label"/> object with specified text and menu caption.
		/// </summary>
		/// <param name="text">The text of the label.</param>
		/// <param name="menuCaption">The menu caption for displaying label in dialogs.</param>
		public Label(string text, string menuCaption)
			: base(text, menuCaption)
		{
		}

		/// <summary>
		/// Creates custom <see cref="Label"/> object with specified color, text and menu caption.
		/// </summary>
		/// <param name="color">The color of the label.</param>
		/// <param name="text">The text of the label.</param>
		/// <param name="menuCaption">The menu caption for displaying label in dialogs.</param>
		public Label(Color color, string text, string menuCaption)
			: base(color, text, menuCaption)
		{
		}

		internal Label(Guid id, Color color, string text, string menuCaption)
			: base(color, text, menuCaption)
		{
			base.SetId(id);
		}

		internal Label(Guid id, Color color, string text)
			: base(color, text, text)
		{
			base.SetId(id);
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Label(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
#endif
		#endregion
	}

	/// <summary>
	/// The <see cref="LabelCollection"/> is a collection of <see cref="Label"/> 
	/// objects which represents all available labels in C1Schedule object model.
	/// </summary> 
	/// <remarks>
	/// <para>By default it contains the following set of predefined labels:</para>
	/// <list type="bullet">
	/// <item><term></term><description>None</description></item>
	/// <item><term></term><description>Important</description></item>
	/// <item><term></term><description>Business</description></item>
	/// <item><term></term><description>Personal</description></item>
	/// <item><term></term><description>Vacation</description></item>
	/// <item><term></term><description>Must Attend</description></item>
	/// <item><term></term><description>Deadline</description></item>
	/// <item><term></term><description>Travel Required</description></item>
	/// <item><term></term><description>Needs Preparation</description></item>
	/// <item><term></term><description>Birthday</description></item>
	/// <item><term></term><description>Anniversary</description></item>
	/// <item><term></term><description>Phone Call</description></item>
	/// <para>It also allows adding of custom labels.</para>
	/// </list>
	/// </remarks>
	public class LabelCollection : BaseCollection<Label>
	{
		LabelStorage _storage;
		/// <summary>
        /// Defines Id of the None label.
        /// </summary>
        public static readonly Guid NoneLabelId = new Guid("{D3F876F9-881B-478c-8594-4941B92D6424}");
        
        /// <summary>
		/// Initializes a new instance of the <see cref="LabelCollection"/> class.
		/// </summary>
		public LabelCollection(): this(null) 
		{
		}
        internal LabelCollection(object owner): base(owner) 
        {
			_storage = owner as LabelStorage;
			LoadDefaults();
        }

		/// <summary>
		/// Restores the collection to its default state.
		/// </summary>
		/// <remarks>The <see cref="LoadDefaults"/> method removes all 
		/// custom labels from the collection and leaves only the standard ones.
		/// </remarks>
		public void LoadDefaults()
		{
			CultureInfo info;
			if (_storage == null)
			{
				info = CalendarInfo.Culture;
			}
			else
			{
				info = _storage.ScheduleStorage.Info.CultureInfo;
			}
			Clear();

#if END_USER_LOCALIZATION
			Add(new Label(NoneLabelId,
				PlatformIndependenceHelper.GetColor(0xFFFFFFFF), // White
				Strings.Labels.Item("None", info)));
			Add(new Label(new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}"),
				PlatformIndependenceHelper.GetColor(0xffffb6c1), // LightPink
				Strings.Labels.Item("Important", info)));
			Add(new Label(new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}"),
				PlatformIndependenceHelper.GetColor(0xffb0c4de), // LightSteelBlue
				Strings.Labels.Item("Business", info)));
			Add(new Label(new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}"),
				PlatformIndependenceHelper.GetColor(0xff9acd32), // YellowGreen
				Strings.Labels.Item("Personal", info)));
			Add(new Label(new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}"),
				PlatformIndependenceHelper.GetColor(0xfff5f5dc), // Beige
				Strings.Labels.Item("Vacation", info)));
			Add(new Label(new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}"),
				PlatformIndependenceHelper.GetColor(0xffff7f50), // Coral
				Strings.Labels.Item("Deadline", info)));
			Add(new Label(new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}"),
				PlatformIndependenceHelper.GetColor(0xffffdead), // NavajoWhite
				Strings.Labels.Item("MustAttend", info)));
			Add(new Label(new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}"),
				PlatformIndependenceHelper.GetColor(0xff00ffff), // Cyan
				Strings.Labels.Item("TravelRequired", info)));
			Add(new Label(new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}"),
				PlatformIndependenceHelper.GetColor(0xffeee8aa), // PaleGoldenrod
				Strings.Labels.Item("NeedsPreparation", info)));
			Add(new Label(new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}"),
				PlatformIndependenceHelper.GetColor(0xffdda0dd), // Plum
				Strings.Labels.Item("Birthday", info)));
			Add(new Label(new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}"),
				PlatformIndependenceHelper.GetColor(0xffb0e0e6), // PowderBlue
				Strings.Labels.Item("Anniversary", info)));
			Add(new Label(new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}"),
				PlatformIndependenceHelper.GetColor(0xffffd700), // Gold
				Strings.Labels.Item("PhoneCall", info)));
#elif WINFX
			Add(new Label(NoneLabelId,
				PlatformIndependenceHelper.GetColor(0xFFFFFFFF), // White
				C1Localizer.GetString("Labels", "None", "None", info)));
			Add(new Label(new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}"),
				PlatformIndependenceHelper.GetColor(0xffffb6c1), // LightPink
				C1Localizer.GetString("Labels", "Important", "Important", info)));
			Add(new Label(new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}"),
				PlatformIndependenceHelper.GetColor(0xffb0c4de), // LightSteelBlue
				C1Localizer.GetString("Labels", "Business", "Business", info)));
			Add(new Label(new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}"),
				PlatformIndependenceHelper.GetColor(0xff9acd32), // YellowGreen
				C1Localizer.GetString("Labels", "Personal", "Personal", info)));
			Add(new Label(new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}"),
				PlatformIndependenceHelper.GetColor(0xfff5f5dc), // Beige
				C1Localizer.GetString("Labels", "Vacation", "Vacation", info)));
			Add(new Label(new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}"),
				PlatformIndependenceHelper.GetColor(0xffff7f50), // Coral
				C1Localizer.GetString("Labels", "Deadline", "Deadline", info)));
			Add(new Label(new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}"),
				PlatformIndependenceHelper.GetColor(0xffffdead), // NavajoWhite
				C1Localizer.GetString("Labels", "MustAttend", "Must Attend", info)));
			Add(new Label(new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}"),
				PlatformIndependenceHelper.GetColor(0xff00ffff), // Cyan
				C1Localizer.GetString("Labels", "TravelRequired", "Travel Required", info)));
			Add(new Label(new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}"),
				PlatformIndependenceHelper.GetColor(0xffeee8aa), // PaleGoldenrod
				C1Localizer.GetString("Labels", "NeedsPreparation", "Needs Preparation", info)));
			Add(new Label(new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}"),
				PlatformIndependenceHelper.GetColor(0xffdda0dd), // Plum
				C1Localizer.GetString("Labels", "Birthday", "Birthday", info)));
			Add(new Label(new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}"),
				PlatformIndependenceHelper.GetColor(0xffb0e0e6), // PowderBlue
				C1Localizer.GetString("Labels", "Anniversary", "Anniversary", info)));
			Add(new Label(new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}"),
				PlatformIndependenceHelper.GetColor(0xffffd700), // Gold
				C1Localizer.GetString("Labels", "PhoneCall", "Phone Call", info)));
#elif SILVERLIGHT
			Add(new Label(NoneLabelId,
				PlatformIndependenceHelper.GetColor(0xFFFFFFFF), // White
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.None));
			Add(new Label(new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}"),
				PlatformIndependenceHelper.GetColor(0xffffb6c1), // LightPink
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Important));
			Add(new Label(new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}"),
				PlatformIndependenceHelper.GetColor(0xffb0c4de), // LightSteelBlue
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Business));
			Add(new Label(new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}"),
				PlatformIndependenceHelper.GetColor(0xff9acd32), // YellowGreen
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Personal));
			Add(new Label(new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}"),
				PlatformIndependenceHelper.GetColor(0xfff5f5dc), // Beige
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Vacation));
			Add(new Label(new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}"),
				PlatformIndependenceHelper.GetColor(0xffff7f50), // Coral
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Deadline));
			Add(new Label(new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}"),
				PlatformIndependenceHelper.GetColor(0xffffdead), // NavajoWhite
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.MustAttend));
			Add(new Label(new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}"),
				PlatformIndependenceHelper.GetColor(0xff00ffff), // Cyan
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.TravelRequired));
			Add(new Label(new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}"),
				PlatformIndependenceHelper.GetColor(0xffeee8aa), // PaleGoldenrod
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.NeedsPreparation));
			Add(new Label(new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}"),
				PlatformIndependenceHelper.GetColor(0xffdda0dd), // Plum
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Birthday));
			Add(new Label(new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}"),
				PlatformIndependenceHelper.GetColor(0xffb0e0e6), // PowderBlue
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Anniversary));
			Add(new Label(new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}"),
				PlatformIndependenceHelper.GetColor(0xffffd700), // Gold
				C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.PhoneCall));
#else
			Add(new Label(NoneLabelId,
                PlatformIndependenceHelper.GetColor(0xFFFFFFFF), // White
                C1Localizer.GetString("None")));
			Add(new Label(new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}"),
                PlatformIndependenceHelper.GetColor(0xffffb6c1), // LightPink
                C1Localizer.GetString("Important")));
			Add(new Label(new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}"),
                PlatformIndependenceHelper.GetColor(0xffb0c4de), // LightSteelBlue
                C1Localizer.GetString("Business")));
			Add(new Label(new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}"),
                PlatformIndependenceHelper.GetColor(0xff9acd32), // YellowGreen
                C1Localizer.GetString("Personal")));
			Add(new Label(new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}"),
                PlatformIndependenceHelper.GetColor(0xfff5f5dc), // Beige
                C1Localizer.GetString("Vacation")));
			Add(new Label(new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}"),
                PlatformIndependenceHelper.GetColor(0xffff7f50), // Coral
                C1Localizer.GetString("Deadline")));
			Add(new Label(new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}"),
                PlatformIndependenceHelper.GetColor(0xffffdead), // NavajoWhite
                C1Localizer.GetString("Must Attend")));
			Add(new Label(new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}"),
                PlatformIndependenceHelper.GetColor(0xff00ffff), // Cyan
                C1Localizer.GetString("Travel Required")));
			Add(new Label(new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}"),
                PlatformIndependenceHelper.GetColor(0xffeee8aa), // PaleGoldenrod
                C1Localizer.GetString("Needs Preparation")));
			Add(new Label(new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}"),
                PlatformIndependenceHelper.GetColor(0xffdda0dd), // Plum
                C1Localizer.GetString("Birthday")));
			Add(new Label(new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}"),
                PlatformIndependenceHelper.GetColor(0xffb0e0e6), // PowderBlue
                C1Localizer.GetString("Anniversary")));
			Add(new Label(new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}"),
				PlatformIndependenceHelper.GetColor(0xffffd700), // Gold
                C1Localizer.GetString("Phone Call")));
#endif
		}
		
		internal void RefreshDefaults()
		{
			CultureInfo culture;
			if (_storage == null)
			{
				culture = CalendarInfo.Culture;
			}
			else
			{
				culture = _storage.ScheduleStorage.Info.CultureInfo;
			}

#if END_USER_LOCALIZATION
			Label label = this[NoneLabelId];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("None", culture);
			}
			label = this[new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Important", culture);
			}
			label = this[new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Business", culture);
			}
			label = this[new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Personal", culture);
			}
			label = this[new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Vacation", culture);
			}
			label = this[new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Deadline", culture);
			}
			label = this[new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("MustAttend", culture);
			}
			label = this[new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("TravelRequired", culture);
			}
			label = this[new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("NeedsPreparation", culture);
			}
			label = this[new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Birthday", culture);
			}
			label = this[new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("Anniversary", culture);
			}
			label = this[new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = Strings.Labels.Item("PhoneCall", culture);
			}
#elif WINFX
			Label label = this[NoneLabelId];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "None", "None", culture);
			}
			label = this[new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Important", "Important", culture);
			}
			label = this[new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Business", "Business", culture);
			}
			label = this[new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Personal", "Personal", culture);
			}
			label = this[new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Vacation", "Vacation", culture);
			}
			label = this[new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Deadline", "Deadline", culture);
			}
			label = this[new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "MustAttend", "Must Attend", culture);
			}
			label = this[new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "TravelRequired", "Travel Required", culture);
			}
			label = this[new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "NeedsPreparation", "Needs Preparation", culture);
			}
			label = this[new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Birthday", "Birthday", culture);
			}
			label = this[new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "Anniversary", "Anniversary", culture);
			}
			label = this[new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}")];
			if (label != null)
			{
				label.Text = label.MenuCaption = C1Localizer.GetString("Labels", "PhoneCall", "Phone Call", culture);
			}
#elif SILVERLIGHT
            Label label = this[NoneLabelId];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.None;
            }
            label = this[new Guid("{9EFA1881-A29D-461d-BC50-E8027B12DD4C}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Important;
            }
            label = this[new Guid("{DA09A26C-5AB3-460a-BD2C-E992B9E86E2B}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Business;
            }
            label = this[new Guid("{7669CE0F-888C-4c50-BB4C-52E592310DDB}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Personal;
            }
            label = this[new Guid("{BF0B75A3-031C-457a-9A21-6B0C095AF673}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Vacation;
            }
            label = this[new Guid("{002DFB0D-46D8-4b87-9B98-9B1D909B0249}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Deadline;
            }
            label = this[new Guid("{622D42F6-B114-4118-959A-E057DDD5C87E}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.MustAttend;
            }
            label = this[new Guid("{4AF15A3D-5D2E-4a3d-A3A0-F889DD7D3CAF}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.TravelRequired;
            }
            label = this[new Guid("{3AD374E6-D019-4dc8-BC80-3B40DF854F3B}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.NeedsPreparation;
            }
            label = this[new Guid("{DE9AEF44-72B3-4787-97B8-FB6BC4AFF6A0}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Birthday;
            }
            label = this[new Guid("{79DC5773-4DA2-465c-8CA0-A5842014D01A}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.Anniversary;
            }
            label = this[new Guid("{49A25D47-AFB1-4128-802C-371BFB18E65C}")];
            if (label != null)
            {
                label.Text = label.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Labels.PhoneCall;
            }
#endif
		}
	}

	/// <summary>
	/// The <see cref="LabelList"/> is a list of <see cref="Label"/> objects.
	/// Only objects existing in the owning <see cref="LabelCollection"/> object 
	/// may be added to this list.
	/// </summary>
	public class LabelList : BaseList<Label>
	{
		internal LabelList(LabelCollection owner)
			: base(owner)
		{
		}

		internal LabelList(LabelList list)
			: base(list)
		{
		}

		internal new LabelCollection Owner
		{
			get
			{
				return (LabelCollection)base.Owner;
			}
		}
	}
}
