using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace C1.C1Schedule
{
    /// <summary>
    /// Represents one day in the C1Schedule. 
    /// </summary>
    internal class Day
    {
        #region fields
        private DateTime _date;
        private bool _selected;
        private bool _isHoliday = false;
        private bool _isWeekend = false;
        private List<Appointment> _appointments;
        #endregion

        #region ctor
        /// <summary>
        /// Creates new Day object.
        /// </summary>
        internal Day(DateTime date)
        {
            _date = date.Date;
            _appointments = new List<Appointment>();
        }

#if (false)
		internal static Day CreateWeekend(DateTime date)
		{
			Day day = new Day(date);
			day._isWeekend = true;
			return day;
		}

		internal static Day CreateHoliday(DateTime date)
		{
			Day day = new Day(date);
			day._isHoliday = true;
			return day;
		}
#endif
        #endregion

        #region object model
        internal DateTime Date
        {
            get
            {
                return _date;
            }
        }

        internal bool HasActivity
        {
            get
            {
                return (_appointments.Count != 0);
            }
        }

        internal bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
            }
        }

        internal List<Appointment> Appointments
        {
            get
            {
                return _appointments;
            }
        }

        internal bool IsHoliday
        {
            get
            {
                return _isHoliday;
            }
            set
            {
                _isHoliday = value;
            }
        }

        internal bool IsWeekend
        {
            get
            {
                return _isWeekend;
            }
            set
            {
                _isWeekend = value;
            }
        }
        #endregion

        #region overrides
        public override string ToString()
        {
            return Date.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Collection of all days in the C1Schedule component.
    /// </summary>
    internal class DayCollection : KeyedCollection<DateTime, Day>
    {
        #region fields
        internal DateList _boldedDates; // list of dates with appointments
        private CalendarInfo _info;
        private bool _inUpdate = false;
        internal AppointmentCollection _parentCollection = null;
        #endregion

        #region events
        internal event EventHandler BoldedDatesChanged;
        void _boldedDates_ListChanged(object sender, EventArgs e)
        {
            // suppress events for batch operations
            if (!_inUpdate && BoldedDatesChanged != null)
            {
                BoldedDatesChanged(this, null);
            }
        }
        #endregion

        #region ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="DayCollection"/> class.
        /// </summary>
        internal DayCollection(CalendarInfo info)
        {
            _info = info;
            _boldedDates = new DateList();
            _boldedDates.ListChanged += new EventHandler(_boldedDates_ListChanged);
        }
        #endregion

        #region interface
        internal void BeginUpdate()
        {
            _inUpdate = true;
        }

        internal void EndUpdate()
        {
            _inUpdate = false;
            _boldedDates_ListChanged(this, null);
        }

        /// <summary>
        /// Returns DayCollection for all dates from the FirstDate till the LastDate.
        /// </summary>
        /// <returns></returns>
        internal void FillDayCollection()
        {
            FillDayCollection(_info.FirstDate, _info.LastTime);
        }

        /// <summary>
        /// Returns DayCollection for all dates from the start till the end.
        /// </summary>
        /// <returns></returns>
        internal void FillDayCollection(DateTime start, DateTime end)
        {
            start = start.Date;
            while (start <= end)
            {
                AddDay(start);
                start = start.AddDays(1);
            }
        }

        // returns the first day of week, containing specified date.
        internal DateTime GetFirstWeekDay(DateTime day)
        {
            day = day.Date;
            while (day.DayOfWeek != _info.WeekStart)
            {
                AddDay(day);
                day = day.AddDays(-1);
            }
            return day;
        }

        // returns the first day of week, containing specified date.
        internal DateTime GetLastWeekDay(DateTime day)
        {
            day = day.Date;
            while (true)
            {
                DateTime nextDay = day.AddDays(1);
                if (nextDay.DayOfWeek != _info.WeekStart)
                {
                    day = nextDay;
                    AddDay(day);
                }
                else
                {
                    break;
                }
            }
            return day;

        }

#if (false)
        internal void AddRange(List<Day> days)
		{
			int dayCount = days.Count;
			for ( int i = 0; i < dayCount; i++)
			{
				Day day = days[i];
				DateTime date = day.Date;
				if (!Contains(date))
				{
					Add(day);
					List<Appointment> appList = day.Appointments;
                    for (int j = 0; j < appList.Count; j++)
					{
						if (appList[j].RecurrenceState != RecurrenceStateEnum.Removed)
						{
							_boldedDates.Add(date);
							break;
						}
					}
				}
			}
		}
#endif

        // adds appointment
        internal void AddAppointment(Appointment appointment)
        {
            DateTime start = appointment.Start.Date;
            DateTime end = (appointment.End == start) ? start.AddMinutes(1) : appointment.End;
            while (start < end)
            {
                if (this.Contains(start))
                {
                    List<Appointment> appList = this[start].Appointments;
                    if (!appList.Contains(appointment))
                    {
                        appList.Add(appointment);
                    }
                }
                if (appointment.RecurrenceState != RecurrenceStateEnum.Removed)
                {
                    _boldedDates.Add(start);
                }
                start = start.AddDays(1);
            }
        }

        internal void ClearAppointments()
        {
            int boldedCount = _boldedDates.Count;
            if (boldedCount > 0)
            {
                for (int i = boldedCount - 1; i >= 0; i--)
                {
                    this[_boldedDates.Items[i]].Appointments.Clear();
                }
                _boldedDates.Clear();
            }
            if (_parentCollection != null)
            {
                _parentCollection.Reminders.Clear();
                _parentCollection.Actions.Clear();
            }
        }

        // removes appointment or all its' occurrences
        internal void RemoveAppointment(Appointment appointment)
        {
            RemoveAppointment(appointment, false);
        }

        // removes appointment or all its' occurrences
        internal void RemoveAppointment(Appointment appointment, bool recurrent)
        {
            if (recurrent || appointment.IsRecurring)
            {
                ReminderCollection reminders = _parentCollection.Reminders;
                ActionCollection actions = _parentCollection.Actions;
                if (appointment.Reminder != null)
                {
                    reminders.Remove(appointment.Reminder);
                }
                if (appointment.Action != null)
                {
                    actions.Remove(appointment.Action);
                }
                List<DateTime> boldedDates = _boldedDates.Items;
                for (int i = boldedDates.Count - 1; i >= 0; i--)
                {
                    List<Appointment> appList = this[boldedDates[i]].Appointments;
                    for (int j = appList.Count - 1; j >= 0; j--)
                    {
                        Appointment app = appList[j];
                        if (app.ParentRecurrence == appointment || app == appointment || app.Id == appointment.Id
                            || (app.ParentRecurrence != null && app.ParentRecurrence.Id == appointment.Id))
                        {
                            if (app.Reminder != null)
                            {
                                reminders.Remove(app.Reminder);
                            }
                            if (app.Action != null)
                            {
                                actions.Remove(app.Action);
                            }
                            appList.Remove(app);
                            if (appList.Count == 0)
                            {
                                _boldedDates.RemoveAt(i);
                            }
                        }
                    }
                }
            }
            else
            {
                RemoveSingleAppointment(appointment, true);
            }
        }

        internal void RemoveSingleAppointment(Appointment appointment, bool removeReminder)
        {
            if (removeReminder)
            {
                if (appointment.Reminder != null)
                {
                    _parentCollection.Reminders.Remove(appointment.Reminder);
                }
                if (appointment.Action != null)
                {
                    _parentCollection.Actions.Remove(appointment.Action);
                }
            }
            List<DateTime> boldedDates = _boldedDates.Items;
            for (int i = boldedDates.Count - 1; i >= 0; i--)
            {
                List<Appointment> appList = this[boldedDates[i]].Appointments;
                if (appList.Contains(appointment))
                {
                    appList.Remove(appointment);
                    if (appList.Count == 0)
                    {
                        _boldedDates.RemoveAt(i);
                    }
                }
            }
        }

        // changes an appointment
        internal void ChangeAppointment(Appointment appointment)
        {
            RemoveSingleAppointment(appointment, false);
            DateTime start = appointment.Start.Date;
            DateTime end = (appointment.End == start) ? start.AddMinutes(1) : appointment.End;
            while (start < end)
            {
                this[start].Appointments.Add(appointment);
                if (appointment.RecurrenceState != RecurrenceStateEnum.Removed)
                {
                    _boldedDates.Add(start);
                }
                start = start.AddDays(1);
            }
        }
        #endregion

        #region overrides
        public new Day this[DateTime value]
        {
            get
            {
                DateTime date = value.Date;
                AddDay(date);
                return base[date];
            }
        }

        protected override void ClearItems()
        {
            _boldedDates.Clear();
            base.ClearItems();
        }

        protected override void RemoveItem(int index)
        {
            _boldedDates.Remove(this[index].Date);
            base.RemoveItem(index);
        }

        /// <summary>
        /// Extracts the key from the specified element.
        /// </summary>
        /// <param name="item">The element from which to extract the key.</param>
        /// <returns>The key for the specified element.</returns>
        protected override DateTime GetKeyForItem(Day item)
        {
            return item.Date;
        }
        #endregion

        #region ** private stuff
        // Returns Day object for specified date.
        // Make sure that date.TimeOfDay is zero before call.
        private Day GetDay(DateTime date)
        {
            Day day = new Day(date);
            if (!_info.IsWorkingDay(date))
            {
                if (_info.Holidays.Contains(date))
                {
                    day.IsHoliday = true;
                }
                else
                {
                    day.IsWeekend = true;
                }
            }
            return day;
        }

        // Make sure that date.TimeOfDay is zero before call.
        private void AddDay(DateTime day)
        {
            if (!Contains(day))
            {
                Add(GetDay(day));
            }
        }
        #endregion
    }

    /// <summary>
    /// The <see cref="DateList"/> class is a wrapper for the 
    /// <see cref="List{DateTime}"/> instance.
    /// At addition of the new value, it cuts off the time part of the value
    /// and ensures that the value is not present in the list yet.
    /// </summary>
    /// <remarks>This class uses binary search for all operations.</remarks>
    public class DateList
    {
        private List<DateTime> _items;

        internal event EventHandler ListChanged;
        private void OnListChanged()
        {
            if (ListChanged != null)
            {
                ListChanged(this, null);
            }
        }
        /// <summary>
        /// Initializes a new instance of <see cref="DateList"/> class.
        /// </summary>
        public DateList()
        {
            _items = new List<DateTime>();
        }

        /// <summary>
        /// Adds new date to the list.
        /// </summary>
        /// <param name="item">The <see cref="DateTime"/> value to add.</param>
        public void Add(DateTime item)
        {
            item = item.Date;
            int index = _items.BinarySearch(item);
            if (index < 0)
            {
                index = ~index;
            }
            else
            {
                return;
            }
            _items.Insert(index, item);
            OnListChanged();
        }

        /// <summary>
        /// Clears all items from the wrapped list.
        /// </summary>
        public void Clear()
        {
            _items.Clear();
            OnListChanged();
        }

        /// <summary>
        /// Removes specified item from the wrapped list.
        /// </summary>
        /// <param name="item">The <see cref="DateTime"/> value to remove.</param>
        public void Remove(DateTime item)
        {
            _items.Remove(item.Date);
            OnListChanged();
        }

        /// <summary>
        /// Removes item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item remove to.</param>
        public void RemoveAt(int index)
        {
            _items.RemoveAt(index);
            OnListChanged();
        }

        /// <summary>
        /// Returns the number of items in the wrapped list.
        /// </summary>
        public int Count
        {
            get
            {
                return _items.Count;
            }
        }

        /// <summary>
        /// Returns the reference to the wrapped list.
        /// </summary>
        public List<DateTime> Items
        {
            get
            {
                return _items;
            }
        }

        /// <summary>
        /// Returns true if specified item exists in the wrapped list.
        /// </summary>
        /// <param name="item">An item to search for.</param>
        /// <returns>True if item exists in the wrapped list.</returns>
        public bool Contains(DateTime item)
        {
            return _items.BinarySearch(item.Date) >= 0;
        }

        /// <summary>
        /// Returns true if either of specified items exists in the wrapped list.
        /// </summary>
        /// <param name="days">An array of <see cref="DateTime"/> values to search for.</param>
        /// <returns>True if at least one of specified items exists in the wrapped list.</returns>
        public bool Contains(DateTime[] days)
        {
            foreach (DateTime day in days)
            {
                if (_items.BinarySearch(day) >= 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if either of specified items exists in the wrapped list.
        /// </summary>
        /// <param name="days">A list of <see cref="DateTime"/> values to search for.</param>
        /// <returns>True if at least one of specified items exists in the wrapped list.</returns>
        public bool Contains(IList<DateTime> days)
        {
            foreach (DateTime day in days)
            {
                if (_items.BinarySearch(day) >= 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the first item that is less than specified one.
        /// </summary>
        /// <param name="item">An item to search for.</param>
        /// <returns>The first item that is less than specified one; 
        /// or DateTime.MinValue if such item is not found.</returns>
        public DateTime GetPreviousDate(DateTime item)
        {
            int index = _items.BinarySearch(item.Date);
            if (index < 0)
            {
                index = ~index;
            }
            if (index == 0)
            {
                return DateTime.MinValue;
            }
            return _items[index - 1];
        }

        /// <summary>
        /// Returns the first item that is greater than specified one.
        /// </summary>
        /// <param name="item">An item to search for.</param>
        /// <returns>The first item that is greater than specified one; 
        /// or DateTime.MaxValue if such item is not found.</returns>
        public DateTime GetNextDate(DateTime item)
        {
            int index = _items.BinarySearch(item.Date.AddDays(1));
            if (index < 0)
            {
                index = ~index;
            }
            if (index == _items.Count)
            {
                return DateTime.MaxValue;
            }
            return _items[index];
        }
    }
}
