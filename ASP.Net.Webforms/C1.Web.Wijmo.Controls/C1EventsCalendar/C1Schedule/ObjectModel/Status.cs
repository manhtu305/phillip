﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Globalization;

#if (!WINFX && !SILVERLIGHT)
using System.Drawing;
#else
using System.Windows.Media;
#endif

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Determines the type of status.
	/// </summary>
	public enum StatusTypeEnum
	{
        /// <summary>
        ///	This status means that a person is in the office and available to others. 
        /// </summary>
        Free = 1,
        /// <summary>
        /// This status means that a person is in the office and tentatively available to others.
        /// </summary>
        Tentative = 2,
        /// <summary>
		///	This status means that a person is in the office but unavailable to others.
		/// </summary>
		Busy = 3,
		/// <summary>
		///	This status means that a person is out of the office and unavailable to others.
		/// </summary>
		OutOfOffice = 4,
		/// <summary>
		/// This status represents all custom statuses added to the StatusCollection.
		/// </summary>
		Custom = 5
	}

	/// <summary>
	/// The <see cref="Status"/> class represents an availability status 
	/// for the <see cref="Appointment"/> object. 
	/// </summary>
#if (!SILVERLIGHT)
	[DefaultProperty("Text"),
	Serializable]
#endif
	public class Status : BaseObject
	{
		#region fields
		private StatusTypeEnum _type;
		#endregion

		#region ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="Status"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Int32"/> value which should be used as status key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Status(int key)
            : this()
        {
            base.SetIndex(key);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Status"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Guid"/> value which should be used as status key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Status(Guid key)
            : this()
        {
            base.SetId(key);
        }

		/// <summary>
		/// Creates the new custom <see cref="Status"/> object with default parameters.
		/// </summary>
		public Status()
		{
			_type = StatusTypeEnum.Custom;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="Status"/> class 
		/// and initializes it with the specified type. 
		/// </summary>
		/// <param name="type">The <see cref="StatusTypeEnum"/> value.</param>
		public Status(StatusTypeEnum type)
			: this(type, null)
		{
		}
		
		/// <summary>
		/// Creates a new instance of the <see cref="Status"/> class 
		/// and initializes it with the specified type. 
		/// </summary>
		/// <param name="type">The <see cref="StatusTypeEnum"/> value.</param>
		/// <param name="culture">The <see cref="CultureInfo"/> value.</param>
		public Status(StatusTypeEnum type, CultureInfo culture)
		{
			culture = culture ?? CalendarInfo.Culture;
			
			_type = type;
			switch (type)
			{
				case StatusTypeEnum.Busy:
					Color = PlatformIndependenceHelper.GetColor(0xFF0000FF); //Blue
					Text = MenuCaption =
#if END_USER_LOCALIZATION
						Strings.Statuses.Item("Busy", culture);
#elif WINFX
						C1Localizer.GetString("Statuses", "Busy", "Busy", culture);
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.Busy;
#else
						C1Localizer.GetString("Busy");
#endif
					Brush = new C1Brush(Color);
					Id = new Guid("{ED87F852-8590-4aab-B728-A02203730791}");
					break;
				case StatusTypeEnum.Free:
					Color = PlatformIndependenceHelper.GetColor(0xFFFFFFFF); //White
					Text = MenuCaption =
#if END_USER_LOCALIZATION
						Strings.Statuses.Item("Free", culture);
#elif WINFX
						C1Localizer.GetString("Statuses", "Free", "Free", culture);
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.Free;
#else
						C1Localizer.GetString("Free");
#endif
					Brush = new C1Brush(Color);
					Id = new Guid("{BDA8B713-2EB9-4f34-AE9C-5D0C7ABC22D7}");
					break;
				case StatusTypeEnum.OutOfOffice:
					Color = PlatformIndependenceHelper.GetColor(0xFF800080); //Purple
                    Text = MenuCaption =
#if END_USER_LOCALIZATION
						Strings.Statuses.Item("OutOfOffice", culture);
#elif WINFX
						C1Localizer.GetString("Statuses", "OutOfOffice", "Out of Office", culture);
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.OutOfOffice;
#else
						C1Localizer.GetString("Out of Office");
#endif
					Brush = new C1Brush(PlatformIndependenceHelper.GetColor(0xFFEE82EE)); //Violet
					Id = new Guid("{7A18315F-8290-4c2c-AB0C-60FB527DC3E8}");
					break;
				case StatusTypeEnum.Tentative:

#if ASP_NET35||ASP_NET_2
                    // Color for Tentative status will be light blue(#00aeef) for ASP .NET version,
                    // since we can't paint text backbround by blue lines.
                    // PS. please, make sure you that you removed previous data file to test new default colors.
                    Color = PlatformIndependenceHelper.GetColor(0xFF00AEEF); // Light Blue
#else
                    Color = PlatformIndependenceHelper.GetColor(0xFF0000FF); //Blue
#endif
                    Text = MenuCaption =
#if END_USER_LOCALIZATION
						Strings.Statuses.Item("Tentative", culture);
#elif WINFX
						C1Localizer.GetString("Statuses", "Tentative", "Tentative", culture);
#elif SILVERLIGHT
                        C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.Tentative;
#else
						C1Localizer.GetString("Tentative");
#endif
					Brush = new C1Brush(Color,
					PlatformIndependenceHelper.GetColor(0xFFFFFFFF), //White
					C1BrushStyleEnum.BackwardDiagonal);
					Id = new Guid("{E6569EEF-845E-41fe-8772-C7A252D467F9}");
					break;
			}
		}

		/// <summary>
		/// Creates a new instance of the custom <see cref="Status"/> object with the specified text.
		/// </summary>
		/// <param name="text">The <see cref="String"/> value.</param>
		public Status(string text)
            : this(PlatformIndependenceHelper.GetColor(0), text, string.Empty, new C1Brush())
		{
		}

		/// <summary>
		/// Creates the new custom <see cref="Status"/> object with specified text and menu caption.
		/// </summary>
		/// <param name="text">The <see cref="String"/> value.</param>
		/// <param name="menuCaption">The menu caption for displaying status in dialogs.</param>
		public Status(string text, string menuCaption)
			: this(PlatformIndependenceHelper.GetColor(0), text, menuCaption, new C1Brush())
		{
		}

		/// <summary>
		/// Creates the new custom <see cref="Status"/> object with specified color, 
		/// text and menu caption.
		/// </summary>
		/// <param name="color">The color which will be used for displaying status.</param>
		/// <param name="text">The <see cref="String"/> value.</param>
		/// <param name="menuCaption">The menu caption for displaying status in dialogs.</param>
		public Status(Color color, string text, string menuCaption)
			: this(color, text, menuCaption, new C1Brush())
		{
		}

		/// <summary>
		/// Creates the new custom <see cref="Status"/> object with specified parameters.
		/// </summary>
		/// <param name="color">The color which will be used for displaying status.</param>
		/// <param name="text">The <see cref="String"/> value.</param>
		/// <param name="menuCaption">The menu caption for displaying status in dialogs.</param>
		/// <param name="brush">The brush which will be used for displaying status.</param>
		public Status(Color color, string text, string menuCaption, C1Brush brush)
			: base(color, text, menuCaption, brush)
		{
			_type = StatusTypeEnum.Custom;
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Status(SerializationInfo info, StreamingContext context)
			: base (info, context)
		{
		}
#endif
		#endregion

		#region object model
		/// <summary>
		/// Gets the <see cref="StatusTypeEnum"/> value determining the type of the status. 
		/// </summary>
		public StatusTypeEnum StatusType
		{
			get
			{
				return _type;
			}
		}
		#endregion

		#region internal stuff
		internal override bool SetColor(Color value)
		{
			if (Color != value)
			{
				Color = value;
				Brush = new C1Brush(value);
				return true;
			}
			else
			{
				return false;
			}
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="StatusCollection"/> is a collection of <see cref="Status"/> 
	/// objects which represents all available statuses in C1Schedule object model.
	/// By default it contains the set of predefined statuses. 
	/// </summary>
	public class StatusCollection : BaseCollection<Status>
	{
		StatusStorage _storage;

		/// <summary>
		/// Initializes a new instance of the <see cref="StatusCollection"/> class.
		/// </summary>
		public StatusCollection():this(null) 
		{
		}

        internal StatusCollection(object owner): base(owner) 
        {
			_storage = owner as StatusStorage;
			LoadDefaults();
        }

		/// <summary>
		/// Restores the collection to its default state.
		/// </summary>
		/// <remarks>The <see cref="LoadDefaults"/> method removes all the custom 
		/// appointment statuses from the collection and leaves only the standard ones.
		/// </remarks>
		public void LoadDefaults()
		{
			CultureInfo info;
			if (_storage == null)
			{
				info = CalendarInfo.Culture;
			}
			else
			{
				info = _storage.ScheduleStorage.Info.CultureInfo;
			}
			List<Status> list = new List<Status>();
			list.Add(GetDefault(new Status(StatusTypeEnum.Busy, info)));
			list.Add(GetDefault(new Status(StatusTypeEnum.Free, info)));
			list.Add(GetDefault(new Status(StatusTypeEnum.OutOfOffice, info)));
			list.Add(GetDefault(new Status(StatusTypeEnum.Tentative, info)));
			Clear();
			this.AddRange(list.ToArray());
		}

		internal void RefreshDefaults()
		{
			CultureInfo culture;
			if (_storage == null)
			{
				culture = CalendarInfo.Culture;
			}
			else
			{
				culture = _storage.ScheduleStorage.Info.CultureInfo;
			}

#if END_USER_LOCALIZATION
			Status status = this[StatusTypeEnum.Busy];
			if (status != null)
			{
				status.Text = status.MenuCaption = Strings.Statuses.Item("Busy", culture);
			}
			status = this[StatusTypeEnum.Free];
			if (status != null)
			{
				status.Text = status.MenuCaption = Strings.Statuses.Item("Free", culture);
			}
			status = this[StatusTypeEnum.OutOfOffice];
			if (status != null)
			{
				status.Text = status.MenuCaption = Strings.Statuses.Item("OutOfOffice", culture);
			}
			status = this[StatusTypeEnum.Tentative];
			if (status != null)
			{
				status.Text = status.MenuCaption = Strings.Statuses.Item("Tentative", culture);
			}
#elif WINFX
			Status status = this[StatusTypeEnum.Busy];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1Localizer.GetString("Statuses", "Busy", "Busy", culture);
			}
			status = this[StatusTypeEnum.Free];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1Localizer.GetString("Statuses", "Free", "Free", culture);
			}
			status = this[StatusTypeEnum.OutOfOffice];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1Localizer.GetString("Statuses", "OutOfOffice", "Out Of Office", culture);
			}
			status = this[StatusTypeEnum.Tentative];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1Localizer.GetString("Statuses", "Tentative", "Tentative", culture);
			}
#elif SILVERLIGHT
			Status status = this[StatusTypeEnum.Busy];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.Busy;
			}
			status = this[StatusTypeEnum.Free];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.Free;
			}
			status = this[StatusTypeEnum.OutOfOffice];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.OutOfOffice;
			}
			status = this[StatusTypeEnum.Tentative];
			if (status != null)
			{
				status.Text = status.MenuCaption = C1.Silverlight.Schedule.Resources.C1_Schedule_Statuses.Tentative;
			}
#endif
		}

		/// <summary>
		/// Gets the <see cref="Status"/> object specified by the appointment status type. 
		/// For custom statuses returns the first occurrence.
		/// If there is no such <see cref="Status"/> object in the collection, returns null.
		/// </summary>
		/// <param name="type">The <see cref="StatusTypeEnum"/> value.</param>
		/// <returns>The <see cref="Status"/> object.</returns>
		public Status this[StatusTypeEnum type]
		{
			get
			{
				foreach (Status item in Items)
				{
					if (item.StatusType == type)
					{
						return item;
					}
				}
				return null;
			}
		}

		private Status GetDefault(Status st)
		{
			if (this.Contains(st.Id))
			{
				Status def = this[st.Id];
				def.Index = st.Index;
				def.Text = st.Text;
				def.MenuCaption = st.MenuCaption;
				return def;
			}
			return st;
		}
	}

	/// <summary>
	/// The <see cref="StatusList"/> is a list of <see cref="Status"/> objects.
	/// Only objects existing in the owning <see cref="StatusCollection"/> object 
	/// may be added to this list.
	/// </summary>
	public class StatusList : BaseList<Status>
	{
		internal StatusList(StatusCollection owner)
			: base (owner)
		{
		}

		internal StatusList(StatusList list)
			: base(list)
		{
		}

		internal new StatusCollection Owner
		{
			get
			{
				return (StatusCollection)base.Owner;
			}
		}
	}
}
