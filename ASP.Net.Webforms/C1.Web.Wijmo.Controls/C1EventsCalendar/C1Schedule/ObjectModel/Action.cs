﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Globalization;
using System.Threading;

namespace C1.C1Schedule
{
	/// <summary>
	/// <see cref="ActionTypeEnum"/> determines the type of action. 
	/// </summary>
	public enum ActionTypeEnum
	{
		/// <summary>
		/// Execute local application with specified parameters.
		/// </summary>
		ExecuteApplication,
		/// <summary>
		/// Open specified target in default browser.
		/// </summary>
		NavigateToUrl
	}

	/// <summary>
	/// Represents an action to take when the current system time reaches 
	/// the start of the associated <see cref="Appointment"/> object.
	/// </summary>
#if (SILVERLIGHT)
	public class Action 
#else
	[Serializable]
	public class Action : ISerializable 
#endif
	{
		#region fields
		private Appointment _owner;
		private bool _enabled = true;
		private ActionTypeEnum _type = ActionTypeEnum.ExecuteApplication;
		private string _command;
		private string _parameters;
		private RegisteredWaitHandle _registeredWaitHandle;
		internal ActionCollection _coll;
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="Action"/> class.
		/// </summary>
		/// <param name="owner">The <see cref="Appointment"/> object.</param>
		public Action(Appointment owner)
		{
			_owner = owner;
			if (_owner.ParentCollection != null)
			{
				_coll = _owner.ParentCollection.Actions;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Action"/> class.
		/// </summary>
		public Action()
		{
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets or sets the owning <see cref="Appointment"/> object. 
		/// </summary>
		public Appointment ParentAppointment
		{
			get
			{
				return _owner;
			}
			set
			{
				_owner = value;
				if (_owner != null && _owner.ParentCollection != null)
				{
					_coll = _owner.ParentCollection.Actions;
				}
				else
				{
					_coll = null;
				}
			}
		}

		/// <summary>
		/// Gets or sets the command string for the action.
		/// </summary>
		public string Command 
		{
			get
			{
				return _command;
			}
			set
			{
				value = BasePersistableObject.GetValidString(value);
				if (_command != value)
				{
					_command = value;
					if (_owner != null)
					{
						_owner.PropertyChanged("Action");
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets parameters used when the action is invoked.
		/// </summary>
		public string Parameters
		{
			get
			{
				return _parameters;
			}
			set
			{
				value = BasePersistableObject.GetValidString(value);
				if (_parameters != value)
				{
					_parameters = value;
					if (_owner != null)
					{
						_owner.PropertyChanged("Action");
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Boolean"/> value indicating 
		/// whether the action is enabled. Default value is True.
		/// </summary>
		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				if (_enabled != value)
				{
					_enabled = value;
					if (_owner != null)
					{
						_owner.PropertyChanged("Action");
					}
					if ( _coll != null)
					{
						if (_enabled)
						{
							_coll.Add(this);
						}
						else
						{
							_coll.Remove(this);
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="ActionTypeEnum"/> value determining 
		/// the type of action that will occur. Default value is ActionTypeEnum.ExecuteApplication.
		/// </summary>
		public ActionTypeEnum Type
		{
			get
			{
				return _type;
			}
			set
			{
				if (_type != value)
				{
					_type = value;
					if (_owner != null)
					{
						_owner.PropertyChanged("Action");
					}
				}
			}
		}
		#endregion

		#region internal
		internal RegisteredWaitHandle Handle
		{
			get { return _registeredWaitHandle; }
			set	{ _registeredWaitHandle = value; }
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Reconstructs action properties from an <see cref="XmlNode"/>. 
		/// </summary>
		/// <param name="node"></param>
		/// <returns>True if an action has been changed.</returns>
		internal bool FromXml(XmlNode node)
		{
			using (System.IO.TextReader tr = new System.IO.StringReader(node.OuterXml))
			using (XmlReader reader = XmlReader.Create(tr, XmlExchanger._readerSettings))
			{
				return FromXml(reader);
			}
#if false
			bool dirty = false;
			XmlNode nd = node.SelectSingleNode("Enabled");
			if (nd != null)
			{
				bool val = bool.Parse(nd.InnerText);
				if (_enabled != val)
				{
					_enabled = val;
					dirty = true;
				}
			}
			nd = node.SelectSingleNode("Type");
			if (nd != null)
			{
				int ival = int.Parse(nd.InnerText, CultureInfo.InvariantCulture);
				if ((int)_type != ival)
				{
					_type = (ActionTypeEnum)ival;
					dirty = true;
				}
			}
			nd = node.SelectSingleNode("Command");
			if (nd != null && nd.InnerText != _command)
			{
				_command = nd.InnerText;
				dirty = true;
			}
			nd = node.SelectSingleNode("Parameters");
			if (nd != null && nd.InnerText != _parameters)
			{
				_parameters = nd.InnerText;
				dirty = true;
			}
			return dirty;
#endif
		}
#endif

		/// <summary>
		/// Reconstructs action properties from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader"></param>
		/// <returns>True if an action has been changed.</returns>
		internal bool FromXml(XmlReader reader)
		{
			bool dirty = false;
			reader.Read();
			bool enabled = true;
			ActionTypeEnum type = ActionTypeEnum.ExecuteApplication;
			string command = "";
			string parameters = "";

			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				switch (reader.Name)
				{
					case "Enabled":
						enabled = bool.Parse(XmlExchanger.ReadStringValue(reader));
						break;
					case "Type":
						type = (ActionTypeEnum)int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "Command":
						command = XmlExchanger.ReadStringValue(reader);
						break;
					case "Parameters":
						parameters = XmlExchanger.ReadStringValue(reader);
						break;
				}
			}
			if (_enabled != enabled)
			{
				_enabled = enabled;
				dirty = true;
			}
			if (type != _type)
			{
				_type = type;
				dirty = true;
			}
			if (!command.Equals(_command))
			{
				_command = command;
				dirty = true;
			}
			if (!parameters.Equals(_parameters))
			{
				_parameters = parameters;
				dirty = true;
			}
			return dirty;
		}

		/// <summary>
		/// Creates an XML encoding of the action properties. 
		/// </summary>
		/// <param name="writer"></param>
		internal void ToXml(XmlWriter writer)
		{
			if (string.IsNullOrEmpty(Command) && string.IsNullOrEmpty(Parameters))
			{
				// don't write empty actions
				return;
			}
			writer.WriteStartElement("Action");
			if (!_enabled)
			{
				writer.WriteElementString("Enabled", _enabled.ToString(CultureInfo.InvariantCulture));
			}
			if (_type != ActionTypeEnum.ExecuteApplication)
			{
				writer.WriteElementString("Type", ((int)_type).ToString(CultureInfo.InvariantCulture));
			}
			if (!string.IsNullOrEmpty(Command))
			{
				writer.WriteElementString("Command", Command);
			}
			if (!string.IsNullOrEmpty(Parameters))
			{
				writer.WriteElementString("Parameters", Parameters);
			}
			writer.WriteEndElement();
		}
		#endregion

#if (!SILVERLIGHT)
		#region ISerializable
		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Action(SerializationInfo info, StreamingContext context)
		{
			_enabled = info.GetBoolean("_enabled");
			_type = (ActionTypeEnum)info.GetValue("_type", typeof(ActionTypeEnum));
			_command = info.GetString("_command");
			_parameters = info.GetString("_parameters");
		}

		// A method called when serializing.
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
			Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_enabled", _enabled);
			info.AddValue("_type", _type, typeof(ActionTypeEnum));
			info.AddValue("_command", _command);
			info.AddValue("_parameters", _parameters);
		}
		#endregion
#endif
	}

	/// <summary>
	/// The <see cref="ActionCollection"/> is a collection of all <see cref="Action"/> objects 
	/// that represents the reminders for all pending items.
	/// </summary>
	public class ActionCollection : C1ObservableCollection<Action>, IDisposable
	{
		// For every Action in collection the new delegate that is waiting for Action's timeout
		// is registered. This delegate queues to the thread pool. A worker thread will execute 
		// the delegate when the time-out interval elapses.
		// Each time callback method WaitProc executes it fires 
		// AppointmentCollection.AppointmentCustomAction event. 

		#region fields
		private WaitOrTimerCallback _callBack; // represents a method to be called when a WaitHandle times out. 
		private AutoResetEvent _ev; // dummy event for using in RegisterWaitForSingleObject.
		private bool disposed = false;
		#endregion;

		#region ctor
		/// <summary>
		/// Initialize new <see cref="ReminderCollection"/> object.
		/// </summary>
		internal ActionCollection()
		{
			_callBack = new WaitOrTimerCallback(WaitProc);
			_ev = new AutoResetEvent(false);
		}
		#endregion

		#region overrides
		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item.</param>
		/// <param name="item">The <see cref="C1.C1Schedule.Action"/> object to insert.</param>
		protected override void InsertItem(int index, Action item)
		{
			Appointment app = item.ParentAppointment;
			if (app == null)
			{
				return;
			}
			if (!app.IsRecurring && !Items.Contains(item))
			{
				DateTime now = DateTime.UtcNow.ToLocalTime();
				if (app.Start < now)
				{
					return;
				}
				
				// calculate timeout
				TimeSpan timeout = app.Start - now;
				if (timeout.TotalMilliseconds < Int32.MaxValue)
				{
					// register wait handle
					item.Handle = ThreadPool.RegisterWaitForSingleObject(
						_ev, _callBack, item, timeout, true);
				}
				lock (Items)
				{
					base.InsertItem(index, item);
				}
			}
		}

		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		/// <param name="index">The zero-based index of the item to remove.</param>
		protected override void RemoveItem(int index)
		{
			Action action;
			lock (Items)
			{
				action = Items[index];
				base.RemoveItem(index);
			}
			// un-register wait handle
			if (action.Handle != null)
			{
				action.Handle.Unregister(null);
			}
		}

		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		protected override void ClearItems()
		{
			lock (Items)
			{
				for (int i = 0; i < Items.Count; i++)
				{
					Action action = Items[i];
					// un-register wait handles
					if (action.Handle != null)
					{
						action.Handle.Unregister(null);
					}
				}
				base.ClearItems();
			}
		}
		#endregion

		#region internal stuff
		// update all actions
		internal void UpdateAll()
		{
			List<Action> actions = new List<Action>();
			actions.AddRange(this);
			Clear();
			foreach (Action action in actions)
			{
				this.Add(action);
			}
		}

		/// <summary>
		/// Method to be called when it is time to execute action.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="timedOut"></param>
		internal void WaitProc(object state, bool timedOut)
		{
			// Fire AppointmentCustomAction event
			Action action = (Action)state;
			if (action.ParentAppointment != null && action.ParentAppointment.ParentCollection != null)
			{
				action.ParentAppointment.ParentCollection.OnAppointmentCustomAction(action, new CancelAppointmentEventArgs(action.ParentAppointment));
			}
			Remove(action);
		}
		#endregion

		#region IDisposable Members
		/// <summary>
		/// Releases all unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the
		// runtime from inside the finalizer and you should not reference
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.disposed)
			{
				if (disposing)
				{
					Clear();
				}
				_ev.Close();
				_ev = null;
				disposed = true;
			}
		}

		#endregion
	}


}
