using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Runtime.InteropServices;
using Microsoft.Win32;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

namespace C1.C1Schedule
{
	/// <summary>
	/// The <see cref="TimeZoneInfo"/> class determines time zone properties.
	/// It can be used for conversion to/from utc times and local times and respect DST.
	/// </summary>
	public class TimeZoneInfo
	{
		#region fields
		private TZI _tzi;				// time zone information.
		private string _tzId;			// time zone Id.
		private string _displayName;	// time zone display name.
		private string _standardName;	// time zone standard name.
		private string _daylightName;	// time zone daylight name (DST).
		private static readonly List<TimeZoneInfo> _timeZones; // static list of all time zones on machine.
		#endregion

		#region ctor
		// private ctor
		internal TimeZoneInfo()
		{
		}

		// fills the list of available time zones from the registry
		static TimeZoneInfo()
		{
			_timeZones = new List<TimeZoneInfo>();

			using (RegistryKey key =
				Registry.LocalMachine.OpenSubKey(
					@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones"))
			{
				string[] zoneNames = key.GetSubKeyNames();

				foreach (string zoneName in zoneNames)
				{
					using (RegistryKey subKey = key.OpenSubKey(zoneName))
					{
						TimeZoneInfo tzi = new TimeZoneInfo();
						tzi._displayName = (string)subKey.GetValue("Display");
						tzi._tzId = zoneName;
						tzi._standardName = (string)subKey.GetValue("Std");
						tzi._daylightName = (string)subKey.GetValue("Dlt");
						tzi.InitTzi((byte[])subKey.GetValue("Tzi"));
						_timeZones.Add(tzi);
					}
				}
			}
		}
		#endregion

		#region object model
		/// <summary>
		/// Gets a list of all time zones defined in the local system.
		/// </summary>
		public static List<TimeZoneInfo> TimeZones
		{
			get
			{
				// return a copy of the list.
				List<TimeZoneInfo> list = new List<TimeZoneInfo>(_timeZones);
				return list;
			}
		}

		/// <summary>
		/// Gets a string array of display time zone names supported in the local system.
		/// </summary>
		public static string[] TimeZoneNames
		{
			get
			{
				List<String> list = new List<String>();
				foreach (TimeZoneInfo tzi in _timeZones)
				{
					list.Add(tzi.DisplayName);
				}
				return list.ToArray();
			}
		}

		/// <summary>
		/// Gets the current time zone information.
		/// </summary>
		public static TimeZoneInfo CurrentTimeZone
		{
			get
			{
				string tzn = TimeZone.CurrentTimeZone.StandardName;
				TimeZoneInfo tzi = TimeZoneInfo.GetTimeZone(tzn);
				return tzi;
			}
		}

		/// <summary>
		/// The time zone's name during 'standard' time (i.e. not daylight savings).
		/// </summary>
		public string StandardName
		{
			get
			{
				return _standardName;
			}
		}

		/// <summary>
		/// The time zone's name during daylight savings time (DST).
		/// </summary>
		public string DaylightName
		{
			get
			{
				return _daylightName;
			}
		}

		/// <summary>
		/// The time zone's display name (e.g. '(GMT-05:00) Eastern Time (US and Canada)').
		/// </summary>
		public string DisplayName
		{
			get
			{
				return _displayName;
			}
		}

		/// <summary>
		/// Gets the standard offset from UTC as a TimeSpan.
		/// </summary>
		public TimeSpan StandardOffset
		{
			get
			{
				return TimeSpan.FromMinutes(StandardBias);
			}
		}

		/// <summary>
		/// Gets the daylight offset from UTC as a TimeSpan.
		/// </summary>
		public TimeSpan DaylightOffset
		{
			get
			{
				return TimeSpan.FromMinutes(DaylightBias);
			}
		}

		/// <summary>
		/// Gets the difference, in minutes, between UTC and local time.
		/// UTC = local time + bias.
		/// </summary>
		public int StandardBias
		{
			get
			{
				return -(_tzi.bias + _tzi.standardBias);
			}
		}

		/// <summary>
		/// Gets the difference, in minutes, between UTC and local time (in daylight savings time).
		/// UTC = local time + bias.
		/// </summary>
		public int DaylightBias
		{
			get
			{
				return -(_tzi.bias + _tzi.daylightBias);
			}
		}
		#endregion

		#region interface
		/// <summary>
		/// Gets display name of this time zone instance.
		/// </summary>
		/// <returns>Time zone standard name.</returns>
		public override string ToString()
		{
			return _displayName;
		}

		/// <summary>
		/// Returns a TimeZoneInfo instance for the time zone with supplied name.
		/// </summary>
		/// <remarks>This method searches by standard, daylight and display names.</remarks>
		/// <param name="name">A name of the time zone to search for.</param>
		/// <returns>TimeZoneInfo instance if any; null otherwise.</returns>
		public static TimeZoneInfo GetTimeZone(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				name = TimeZone.CurrentTimeZone.StandardName;
			}

			foreach (TimeZoneInfo tzi in TimeZoneInfo.TimeZones)
			{
				if (tzi._tzId.Contains(name) || tzi.DisplayName.Contains(name)
					|| tzi.StandardName.Contains(name) || tzi.DaylightName.Contains(name))
				{
					return tzi;
				}
			}
			return null;
		}

		/// <summary>
		/// Converts the value of the utc time to a local time in this time zone.
		/// </summary>
		/// <param name="utc">The UTC time to convert.</param>
		/// <returns>The local time.</returns>
		public DateTime ToLocalTime(DateTime utc)
		{
			// Convert to SYSTEMTIME
			SYSTEMTIME stUTC = DateTimeToSystemTime(utc);

			// Set up the TIME_ZONE_INFORMATION
			TIME_ZONE_INFORMATION tziNative = TziNative();
			SYSTEMTIME stLocal;
			NativeMethods.SystemTimeToTzSpecificLocalTime(ref tziNative, ref stUTC, out stLocal);

			// Convert back to DateTime
			return SystemTimeToDateTime(ref stLocal);
		}

		/// <summary>
		/// Converts the value of the local time to UTC time.
		/// Note that there may be different possible interpretations at the daylight time boundaries.
		/// </summary>
		/// <param name="local">The local time to convert.</param>
		/// <returns>The UTC DateTime.</returns>
		/// <exception cref="NotSupportedException">Thrown if the method failed due to missing platform support.</exception>
		public DateTime ToUniversalTime(DateTime local)
		{
			SYSTEMTIME stLocal = DateTimeToSystemTime(local);
			TIME_ZONE_INFORMATION tziNative = TziNative();
			SYSTEMTIME stUTC;

			try
			{
				NativeMethods.TzSpecificLocalTimeToSystemTime(ref tziNative, ref stLocal, out stUTC);
				DateTime result = DateTime.SpecifyKind(SystemTimeToDateTime(ref stUTC), DateTimeKind.Utc);
				return result;
			}
			catch (EntryPointNotFoundException e)
			{
#if END_USER_LOCALIZATION
				throw new NotSupportedException(C1.Win.C1Schedule.Localization.Strings.C1Schedule.Exceptions.MethodNotSupported, e);
#elif WINFX
				throw new NotSupportedException(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "MethodNotSupported", "This method is not supported on this operating system.", null), e);
#else
				throw new NotSupportedException(C1Localizer.GetString("This method is not supported on this operating system."), e);
#endif
			}
		}
		#endregion

		#region Private Methods
		/// <summary>
		/// Copies a time zone info form the specified <see cref="TimeZoneInfo"/> object into the current instance.
		/// </summary>
		/// <param name="info"></param>
		protected void CopyFrom(TimeZoneInfo info)
		{
			_tzi = info._tzi;
			_displayName = info._displayName;
			_standardName = info._standardName;
			_daylightName = info._daylightName;
		}

		private static SYSTEMTIME DateTimeToSystemTime(DateTime dt)
		{
			SYSTEMTIME st;
			System.Runtime.InteropServices.ComTypes.FILETIME ft = new System.Runtime.InteropServices.ComTypes.FILETIME();
			ft.dwHighDateTime = (int)(dt.Ticks >> 32);
			ft.dwLowDateTime = (int)(dt.Ticks & 0xFFFFFFFFL);
			NativeMethods.FileTimeToSystemTime(ref ft, out st);
			return st;
		}

		private static DateTime SystemTimeToDateTime(ref SYSTEMTIME st)
		{
			System.Runtime.InteropServices.ComTypes.FILETIME ft = new System.Runtime.InteropServices.ComTypes.FILETIME();
			NativeMethods.SystemTimeToFileTime(ref st, out ft);
			DateTime dt = new DateTime((((long)ft.dwHighDateTime) << 32) | (uint)ft.dwLowDateTime);
			return dt;
		}

		private TIME_ZONE_INFORMATION TziNative()
		{
			TIME_ZONE_INFORMATION tziNative = new TIME_ZONE_INFORMATION();
			tziNative.Bias = _tzi.bias;
			tziNative.StandardDate = _tzi.standardDate;
			tziNative.StandardBias = _tzi.standardBias;
			tziNative.DaylightDate = _tzi.daylightDate;
			tziNative.DaylightBias = _tzi.daylightBias;
			return tziNative;
		}

		/// <summary>
		/// The standard Windows SYSTEMTIME structure.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		private struct SYSTEMTIME
		{
			public UInt16 wYear;
			public UInt16 wMonth;
			public UInt16 wDayOfWeek;
			public UInt16 wDay;
			public UInt16 wHour;
			public UInt16 wMinute;
			public UInt16 wSecond;
			public UInt16 wMilliseconds;
		}

		// FILETIME is already declared in System.Runtime.InteropServices. 

		/// <summary>
		/// The layout of the Tzi value in the registry.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		private struct TZI
		{
			public int bias;
			public int standardBias;
			public int daylightBias;
			public SYSTEMTIME standardDate;
			public SYSTEMTIME daylightDate;
		}

		/// <summary>
		/// The standard Win32 TIME_ZONE_INFORMATION structure.
		/// Thanks to www.pinvoke.net.
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		private struct TIME_ZONE_INFORMATION
		{
			[MarshalAs(UnmanagedType.I4)]
			public Int32 Bias;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
			public string StandardName;
			public SYSTEMTIME StandardDate;
			[MarshalAs(UnmanagedType.I4)]
			public Int32 StandardBias;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
			public string DaylightName;
			public SYSTEMTIME DaylightDate;
			[MarshalAs(UnmanagedType.I4)]
			public Int32 DaylightBias;
		}

		/// <summary>
		/// A container for P/Invoke declarations.
		/// </summary>
		private struct NativeMethods
		{
			private const string KERNEL32 = "kernel32.dll";

			[DllImport(KERNEL32)]
			public static extern uint GetTimeZoneInformation(out TIME_ZONE_INFORMATION
			lpTimeZoneInformation);

			[DllImport(KERNEL32)]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool SystemTimeToTzSpecificLocalTime(
			[In] ref TIME_ZONE_INFORMATION lpTimeZone,
			[In] ref SYSTEMTIME lpUniversalTime,
			out SYSTEMTIME lpLocalTime);

			[DllImport(KERNEL32)]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool SystemTimeToFileTime(
			[In] ref SYSTEMTIME lpSystemTime,
			out System.Runtime.InteropServices.ComTypes.FILETIME lpFileTime);

			[DllImport(KERNEL32)]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool FileTimeToSystemTime(
			[In] ref System.Runtime.InteropServices.ComTypes.FILETIME lpFileTime,
			out SYSTEMTIME lpSystemTime);

			/// <summary>
			/// Convert a local time to UTC, using the supplied time zone information.
			/// Windows XP and Server 2003 and later only.
			/// </summary>
			/// <param name="lpTimeZone">The time zone to use.</param>
			/// <param name="lpLocalTime">The local time to convert.</param>
			/// <param name="lpUniversalTime">The resultant time in UTC.</param>
			/// <returns>true if successful, false otherwise.</returns>
			[DllImport(KERNEL32)]
			[return: MarshalAs(UnmanagedType.Bool)]
			public static extern bool TzSpecificLocalTimeToSystemTime(
			[In] ref TIME_ZONE_INFORMATION lpTimeZone,
			[In] ref SYSTEMTIME lpLocalTime,
			out SYSTEMTIME lpUniversalTime);
		}

		/// <summary>
		/// Initialize the _tzi member.
		/// </summary>
		/// <param name="info">The Tzi data from the registry.</param>
		private void InitTzi(byte[] info)
		{
			if (info.Length != Marshal.SizeOf(_tzi))
				throw new ArgumentException("Information size is incorrect", "info");

			// Could have sworn there's a Marshal operation to pack bytes into
			// a structure, but I can't see it. Do it manually.
			GCHandle h = GCHandle.Alloc(info, GCHandleType.Pinned);
			try
			{
				_tzi = (TZI)Marshal.PtrToStructure(h.AddrOfPinnedObject(), typeof(TZI));
			}
			finally
			{
				h.Free();
			}
		}
		#endregion
	}
}