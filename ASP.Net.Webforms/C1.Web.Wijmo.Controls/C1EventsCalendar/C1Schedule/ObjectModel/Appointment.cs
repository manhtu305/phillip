﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Collections.ObjectModel;
using System.Xml;
using System.Globalization;
using System.Reflection;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

// ToDo: 
// maybe change Action to collection of actions
// Attachments
// BillingInformation?
// Companies?
// IsConflict?
// UserProperties?

namespace C1.C1Schedule
{
	#region enums
	/// <summary>
	/// Determines the type of the <see cref="Appointment"/> object.
	/// </summary>
	public enum RecurrenceStateEnum
	{
		/// <summary>
		/// Represents a standard (non-recurring) appointment. 
		/// </summary>
		NotRecurring,
		/// <summary>
		/// Represents the appointment which serves as the pattern 
		/// for the other recurring appointments. 
		/// The pattern for recurring appointments is specified via 
		/// the Appointment.Recurrence property.
		/// </summary>
		Master,
		/// <summary>
		/// Represents one of recurring appointments whose pattern appointment 
		/// is specified via the Appointment.Recurrence property.
		/// </summary>
		Occurrence,
		/// <summary>
		/// Represents a recurring appointment, which was changed 
		/// and now doesn't satisfy the pattern of the recurring series.  
		/// </summary>
		Exception,
		/// <summary>
		/// Represents a recurring appointment, which was deleted
		/// and now doesn't satisfy the pattern of the recurring series.  
		/// </summary>
		Removed
	}

	/// <summary>
	/// Determines the importance of the <see cref="Appointment"/> object.
	/// </summary>
	public enum ImportanceEnum
	{
		/// <summary>
		/// Low
		/// </summary>
		Low = 0,
		/// <summary>
		/// Normal
		/// </summary>
		Normal = 1,
		/// <summary>
		/// High
		/// </summary>
		High = 2
	}

	/// <summary>
	/// Determines the sensitivity of the <see cref="Appointment"/> object.
	/// </summary>
	public enum SensitivityEnum
	{
		/// <summary>
		/// Confidential
		/// </summary>
		Confidential,
		/// <summary>
		/// Normal
		/// </summary>
		Normal,
		/// <summary>
		/// Personal
		/// </summary>
		Personal,
		/// <summary>
		/// Private
		/// </summary>
		Private
	}
	#endregion

	/// <summary>
	/// The <see cref="Appointment"/> object is a meeting, a one-time appointment, 
	/// or a recurring appointment or meeting in the C1Schedule. 
	/// The <see cref="Appointment"/> class includes properties 
	/// that specify meeting details such as the location and time.
	/// </summary>
#if SILVERLIGHT
	public class Appointment : BasePersistableObject
#else
	[Serializable]
	public class Appointment : BasePersistableObject, ISerializable
#endif
	{
		#region ** fields
		private AppointmentCollection _parentCollection = null;
		private Appointment _parentRecurrence = null;

		private Action _action = null;
		private object _tag = null;
        private string _customData = null;
		private bool _autoResolvedWinner = false;

		// owner
		private Contact _owner = null;
		private Guid _ownerId = Guid.Empty;
		private int _ownerIndex = -1;

        private bool _reminderChanged = false;
        private bool _reminderAdded = false;
        private bool _reminderRemoved = false;
        private bool _changed = false;
        private bool _recStateChanged = false;
		private bool _saved = false;

		// strings
        private string _subject = "";
        private string _body = "";
        private string _location = "";

		// times
		private DateTime _start;
		// the last saved start value (it doesn't changed between BeginEdit/EndEdit calls)
		internal DateTime _lastAcceptedStart;
		private TimeSpan _duration;
		internal TimeSpan _lastAcceptedDuration;
		private bool _allDayEvent = false;

		// status
        private ImportanceEnum _importance = ImportanceEnum.Normal;
        private SensitivityEnum _sensitivity = SensitivityEnum.Normal;
		private bool _private = false;
		private Status _busyStatus;
        private Guid _busyStatusId = Guid.Empty;
		private int _busyStatusIndex = -1;

		// reminder properties
		private bool _reminderOverrideDefault = false;
		private bool _reminderPlaySound = true;
        private string _reminderSoundFile = "";
		private TimeSpan _reminderTimeBeforeStart = TimeSpan.FromMinutes(15);
		private Reminder _reminder = null;

		// recurrence properties
        private RecurrenceStateEnum _recurrenceState = RecurrenceStateEnum.NotRecurring;
		private RecurrencePattern _recurrencePattern = null;

		// associated collections
		private Label _label;
		private Guid _labelId = Guid.Empty;
		private int _labelIndex = -1;
		private CategoryList _categories;
		private Guid[] _categoryIds;
		private int[] _categoryIndices;
		private ResourceList _resources;
		private Guid[] _resourceIds;
		private int[] _resourceIndices;
		private ContactList _links;
		private Guid[] _linkIds;
		private int[] _linkIndices;

		internal bool _inCollection = false;
		internal DateTime _previousStart; // old value of Start.Date - it is needed for excluding
										 // dates from Recurrence pattern
		#endregion

		#region ** ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="Appointment"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Int32"/> value which should be used as appointment key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Appointment(int key)
            : this()
        {
            base.SetIndex(key);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Appointment"/> class with the specified key.
        /// </summary>
        /// <param name="key">The <see cref="Guid"/> value which should be used as appointment key.</param>
        /// <remarks>Use this constructor if your business logic requires setting custom key value.
        /// Make sure that you use the correct constructor overload (with integer or Guid key value) and that key value is unique.</remarks>
        public Appointment(Guid key)
            : this()
        {
            base.SetId(key);
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="Appointment"/> class.
		/// </summary>
		/// <remarks>Creates an appointment with default properties.</remarks>
		public Appointment()
			: this(DateTime.UtcNow.ToLocalTime(), TimeSpan.FromHours(1)) 
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Appointment"/> 
		/// class with the specified parameters.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the appointment.</param>
		public Appointment(DateTime start, DateTime end)
		{
            _previousStart = _start = start;
            End = end;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Appointment"/> 
		/// class with the specified parameters.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="duration">The <see cref="TimeSpan"/> value which specifies 
		/// the duration of the appointment.</param>
		public Appointment(DateTime start, TimeSpan duration)
		{
            _previousStart = _start = start;
			_duration = duration;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Appointment"/> 
		/// class with the specified parameters.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="end">The <see cref="DateTime"/> value which specifies 
		/// the end date and time of the appointment.</param>
		/// <param name="subject">The <see cref="String"/> value which contains 
		/// the subject of the appointment.</param>
		public Appointment(DateTime start, DateTime end, string subject)
			: this(start, end)
		{
			_subject = GetValidString(subject);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Appointment"/> 
		/// class with the specified parameters.
		/// </summary>
		/// <param name="start">The <see cref="DateTime"/> value which specifies 
		/// the start date and time of the appointment.</param>
		/// <param name="duration">The <see cref="TimeSpan"/> value which specifies 
		/// the duration of the appointment.</param>
		/// <param name="subject">The <see cref="String"/> value which contains 
		/// the subject of the appointment.</param>
		public Appointment(DateTime start, TimeSpan duration, string subject)
			: this(start, duration)
		{
			_subject = GetValidString(subject);
		}
		#endregion

		#region ** object model
        #region * misc
        /// <summary>
		/// Gets the reference to the parent <see cref="AppointmentCollection"/> object.
		/// </summary>
		public AppointmentCollection ParentCollection
		{
			get { return _parentCollection; }
			internal set { _parentCollection = value; }
		}

        /// <summary>
        /// Gets an <see cref="Action"/> object for the <see cref="Appointment"/> object.
        /// Once this property is first requested, a new <see cref="Action"/> object 
        /// is created and assigned to the <see cref="Appointment.Action"/> property.
        /// </summary>
        /// <remarks>The <see cref="Action"/> object contains information regarding 
        /// the type of action to perform as well as the command 
        /// and parameters to pass to the action.
        /// If the action object is enabled, the action will be invoked 
        /// when the start date and time of the owning <see cref="Appointment"/> object has occurred.
        /// </remarks>
        public Action Action
        {
            get { return _action; }
            set
            {
                if (_action != value)
                {
                    _action = value;
                    if (_action != null)
                    {
                        _action.ParentAppointment = this;
                    }
                    OnPropertyChanged("Action");
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ImportanceEnum"/> value indicating 
        /// the relative importance level for the appointment.
        /// Default value is <see cref="ImportanceEnum.Normal"/>.
        /// </summary>
        [DefaultValue(ImportanceEnum.Normal)]
        public ImportanceEnum Importance
        {
            get { return _importance; }
            set
            {
                if (_importance != value)
                {
					BeforePropertyChange("Importance", _importance);
					_importance = value;
                    OnPropertyChanged("Importance");
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="Boolean"/> value indicating 
        /// if the current appointment is an All-day appointment.
        /// </summary>
        [DefaultValue(false)]
        public bool AllDayEvent
        {
            get { return _allDayEvent; }
            set
            {
                if (_allDayEvent != value)
                {
					BeforePropertyChange("AllDayEvent", _allDayEvent);
					// TODO: check events
                    _allDayEvent = value;
                    bool startChanged = false;
                    bool endChanged = false;
                    bool durationChanged = false;
                    if (value)
                    {
                        DateTime newStart = _start.Date;
                        DateTime newEnd = newStart + _duration;
                        if (newEnd.TimeOfDay.TotalMinutes != 0 || Duration.Ticks == 0)
                        {
                            newEnd = newEnd.Date.AddHours(24);
                        }
                        TimeSpan newDuration = newEnd - newStart;
                        startChanged = newStart != _start;
                        endChanged = newEnd != _start + _duration;
                        durationChanged = newDuration != _duration;
                        if (startChanged)
                        {
                            _previousStart = _start;
                            _start = newStart;
                        }
                        if (durationChanged)
                            _duration = newDuration;
                    }
                    if (endChanged)
                        OnPropertyChanged("End");
                    if (durationChanged)
                        OnPropertyChanged("Duration");
                    if (startChanged)
                        OnPropertyChanged("Start");
                    OnPropertyChanged("AllDayEvent");
                }
            }
        }

        /// <summary>
        /// Gets or sets custom information associated with this object.
        /// </summary>
        /// <remarks>This property is valid only for the lifetime of the object.</remarks>
        [DefaultValue(null)]
        public object Tag
        {
            get { return _tag; }
            set
            {
                if (_tag != value)
                {
					_tag = value;
                    OnPropertyChanged("Tag");
                }
            }
        }

        /// <summary>
        /// Gets or sets custom text associated with this object.
        /// </summary>
        /// <remarks>This property is serialized with other appointment properties.
        /// It isn't used by the scheduler, use it to keep your data associated with appointment.
        /// This property is serialized into xml and binary formats and it is saved into underlying 
        /// data source if mapping for Appointment Properties is set. 
        /// It isn't retained at export/import to iCal format.</remarks>
        [DefaultValue(null)]
        public string CustomData
        {
            get { return _customData; }
            set
            {
                if (_customData != value)
                {
					_customData = value;
                    OnPropertyChanged("CustomData");
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="Boolean"/> value indicating if the appointment 
        /// has not been modified since the last save.
        /// </summary>
        public bool Saved
        {
            get { return _saved; }
        }

        /// <summary>
        /// Gets or sets the <see cref="SensitivityEnum"/> value indicating 
        /// the sensitivity level of the appointment. 
        /// </summary>
        [DefaultValue(SensitivityEnum.Normal)]
        public SensitivityEnum Sensitivity
        {
            get { return _sensitivity; }
            set
            {
                if (_sensitivity != value)
                {
					BeforePropertyChange("Sensitivity", _sensitivity);
					_sensitivity = value;
                    OnPropertyChanged("Sensitivity");
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="Boolean"/> value determining 
        /// whether the calendar owner intends to keep the <see cref="Appointment"/>
        /// object private.
        /// </summary>
        [DefaultValue(false)]
        public bool Private
        {
            get { return _private; }
            set
            {
                if (_private != value)
                {
					BeforePropertyChange("Private", _private);
					_private = value;
                    OnPropertyChanged("Private");
                }
            }
        }
        #endregion

        #region * Subject
        /// <summary>
		/// Gets or sets the <see cref="String"/> value 
		/// representing the subject of the <see cref="Appointment"/> object. 
		/// </summary>
        [DefaultValue("")]
		public string Subject
		{
			get { return _subject; }
			set
			{
				if (SetSubject(value))
				{
                    OnPropertyChanged("Subject"); 
				}
			}
		}
		internal bool SetSubject(string value)
		{
			value = GetValidString(value);
			if (!_subject.Equals(value))
			{
				BeforePropertyChange("Subject", _subject);
				_subject = value;
				return true;
			}
			return false;
		}
        #endregion

        #region * Body
        /// <summary>
		/// Gets or sets the <see cref="String"/> value representing 
		/// the body of the <see cref="Appointment"/> object.
		/// </summary>
		[DefaultValue("")]
		public string Body
		{
			get { return _body; }
			set 
            {
                if (SetBody(value))
				{
                    OnPropertyChanged("Body"); 
                }
			}
		}
		internal bool SetBody(string value)
		{
			value = GetValidString(value);
			if (!_body.Equals(value))
			{
				BeforePropertyChange("Body", _body);
				_body = value;
				return true;
			}
			return false;
		}
        #endregion

        #region * Conflicts
        /// <summary>
		/// Gets the <see cref="AppointmentList"/> object representing 
		/// the list of <see cref="Appointment"/> objects which are in conflict 
		/// with this particular appointment.
		/// </summary>
		public AppointmentList Conflicts
		{
            //alex: TBD: find a way to raise PropertyChanged for this property
            get { return _parentCollection.GetConflicts(this); }
		}

		/// <summary>
		/// Gets the <see cref="Boolean"/> value determining if the item is 
		/// a winner of an automatic conflict resolution.
		/// </summary>
		/// <remarks>A value of false does not necessarily indicate that the item is 
		/// a loser of an automatic conflict resolution. 
		/// The item should be in conflict with another item.
		/// If an item has its Conflicts.Count property greater than zero 
		/// and if its <see cref="AutoResolvedWinner"/> property is true, 
		/// it is a winner of an automatic conflict resolution. 
		/// On the other hand, if the item is in conflict and has its 
		/// <see cref="AutoResolvedWinner"/> property as false, 
		/// it is a loser in an automatic conflict resolution.</remarks>
		[DefaultValue(false)]
		public bool AutoResolvedWinner
		{
			get { return _autoResolvedWinner; }
		}
        #endregion

        #region * Start
        /// <summary>
		/// Gets or sets the <see cref="DateTime"/> value determining the 
		/// start date and time of the appointment. 
		/// </summary>
		/// <remarks>
		/// <para>If the <see cref="AllDayEvent"/> property is set to true, 
		/// then the start date of the appointment is the <see cref="DateTime.Date"/> of 
		/// the <see cref="Start"/>, and the start time of the appointment is 0:00.</para>
		/// <para>At setting this property, if it's time is not equal 0:00, 
		/// the <see cref="AllDayEvent"/> property will be set to false.</para></remarks>
		public DateTime Start
		{
			get { return _start; }
			set
			{
                if (_start != value)
                {
					BeforePropertyChange("Start", _start);
					bool prevIsOneDay = IsOneDay;
                    if (!IsEdit)
                    {
                        _previousStart = _start;
                    }
                    _start = value;
                    if (ReminderSet)
                    {
                        if (IsEdit)
                        {
                            _reminderChanged = true;
                        }
                        else if (!IsRecurring)
                        {
                            _reminder.Update();
                        }
                        else if (_inCollection)
                        {
                            _parentCollection.Reminders.OnReminderChange(this, new ReminderEventArgs(_reminder));
                        }
                    }
                    // in cases when there is need to call some notifications,
                    // call one notification of this class (in order to rise appointment events only once)
                    // and then call notifications of the base class
                    OnPropertyChanged("Start");
                    base.OnPropertyChanged("End");
                    if (AllDayEvent && !SatisfyAllDayEventConditions())
                        AllDayEvent = false;
                    if (prevIsOneDay != IsOneDay)
                        base.OnPropertyChanged("IsOneDay");
                }
            }
		}
		internal bool SetStart(DateTime value)
		{
			if ( !_start.Equals(value))
			{
				_previousStart = _start = value;
                if (_allDayEvent && !SatisfyAllDayEventConditions())
                    _allDayEvent = false;
                return true;
			}
			else
			{
				_previousStart = value;
				return false;
			}
		}
        #endregion

        #region * End
        /// <summary>
		/// Gets or sets the <see cref="DateTime"/> value determining 
		/// the end date and time of the appointment. 
		/// </summary>
		/// <remarks> <para>The <see cref="End"/> property is usually specified 
		/// by the <see cref="Start"/> and <see cref="Duration"/> properties, 
		/// and is always calculated as <see cref="End"/> = <see cref="Start"/> + <see cref="Duration"/>. 
		/// When setting the <see cref="End"/> property, the <see cref="Start"/> property 
		/// retains its value, and the <see cref="Duration"/> is changed according 
		/// to the new value of the <see cref="End"/> property. 
		/// If the new <see cref="End"/> property's value is less than 
		/// the <see cref="Start"/> property's value, then an exception will be raised.</para>
		/// <para>At setting this property, if it's time is not equal 0:00, 
		/// or value is equal to the value of <see cref="Start"/> property, 
		/// the <see cref="AllDayEvent"/> property will be set to false.</para></remarks>
		public DateTime End
		{
			get { return _start + _duration; }
			set
			{
				if (value == _start + _duration)
				{
					return;
				}
				if (value < _start)
				{
#if END_USER_LOCALIZATION
					if (ParentCollection == null)
					{
						throw new System.ArgumentException(C1.Win.C1Schedule.Localization.Strings.C1Schedule.Exceptions.StartEndValidationFailed);
					}
					else
					{
						throw new System.ArgumentException(C1.Win.C1Schedule.Localization.Strings.C1Schedule.Exceptions.Item("StartEndValidationFailed", ParentCollection.CalendarInfo.CultureInfo));
					}
#elif WINFX
					if (ParentCollection == null)
					{
						throw new System.ArgumentException(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "StartEndValidationFailed", "The End value should be greater than Start value.", null));
					}
					else
					{
						throw new System.ArgumentException(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "StartEndValidationFailed", "The End value should be greater than Start value.", ParentCollection.CalendarInfo.CultureInfo));
					}
#elif SILVERLIGHT
                    throw new System.ArgumentException(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.StartEndValidationFailed);
#else
					throw new System.ArgumentException(C1Localizer.GetString("The End value should be greater than Start value."));
#endif
				}
				BeforePropertyChange("Duration", _duration);
				_duration = value - _start;
                OnPropertyChanged("End"); 
                base.OnPropertyChanged("Duration");
                if (AllDayEvent && !SatisfyAllDayEventConditions())
                    AllDayEvent = false;
            }
		}
        internal bool SetEnd(DateTime value)
        {
            if (!End.Equals(value))
            {
                _duration = value.Subtract(_start);
                if (_allDayEvent && !SatisfyAllDayEventConditions())
                    _allDayEvent = false;
                return true;
            }
            return false;
        }
        #endregion

        #region * IsOneDay
        /// <summary>
		/// Gets a <see cref="Boolean"/> value indicating either it is 
		/// a one day appointment or shares some days.
		/// </summary>
		public bool IsOneDay
		{
			get
			{
				if (_start == End)
				{
					return true;
				}
				else if (End.TimeOfDay == TimeSpan.Zero)
				{
					return _start.Date.Equals(End.Date.AddDays(-1));
				}
				else
				{
					return (_start.Date == End.Date);
				}
			}
		}


		internal bool IsAllDayOrMultiDay
		{
			get { return _duration.TotalDays >= 1; }
		}

		internal DateTime EndDate
		{
			get
			{
				DateTime end = End;
				if (_duration.Ticks > 0 && end.TimeOfDay == TimeSpan.Zero)
				{
					return end.AddDays(-1);
				}
				else
				{
					return end.Date;
				}
			}
		}
        #endregion

        #region * Duration
        /// <summary>
		/// Gets or sets the <see cref="TimeSpan"/> value indicating the 
		/// duration of the appointment.
		/// </summary>
		/// <remarks>At setting this property, if it's value is not equal to the whole 
		/// number of days, the <see cref="AllDayEvent"/> property will be set to false.</remarks>
		[DefaultValue(typeof(TimeSpan), "1:00:00")]
		public TimeSpan Duration
		{
			get { return _duration; }
			set
			{
				if (_duration != value)
				{
					BeforePropertyChange("Duration", _duration);
					_duration = value;
                    OnPropertyChanged("Duration");
					base.OnPropertyChanged("End");
                    if (AllDayEvent && !SatisfyAllDayEventConditions())
                        AllDayEvent = false;
                }
			}
		}
		internal bool SetDuration(TimeSpan value)
		{
			if (_duration != value)
			{
				_duration = value;
				return true;
			}
			return false;
		}
        #endregion

        #region * Location
        /// <summary>
		/// Gets or sets the <see cref="String"/> value specifying 
		/// where the appointment is going to be.
		/// </summary>
		[DefaultValue("")]
		public string Location
		{
			get { return _location; }
			set
			{
                if (SetLocation(value))
				{
                    OnPropertyChanged("Location");
                }
			}
		}
		internal bool SetLocation(string value)
		{
			value = GetValidString(value);
            if (!_location.Equals(value))
            {
				BeforePropertyChange("Location", _location);
				_location = value;
				return true;
			}
			return false;
		}
        #endregion

        #region * Reminder
        /// <summary>
		/// Gets the <see cref="Reminder"/> object which is associated with the appointment.
		/// To associate an appointment with the reminder, set its 
		/// <see cref="ReminderSet"/> property to true. 
		/// Once this property is set to true, a new <see cref="Reminder"/> object 
		/// is created and assigned to the <see cref="Reminder"/> property.
		/// </summary>
		public Reminder Reminder
		{
			get { return _reminder; }
		}

		internal void SetReminder()
		{
            _reminder = new Reminder(this);
            if (IsEdit)
            {
                _reminderAdded = true;
            }
        }

		/// <summary>
		/// Gets or sets the <see cref="Boolean"/> value indicating whether 
		/// a reminder is associated with the appointment. 
		/// Once this property is set to true, a new <see cref="Reminder"/> object 
		/// is created and assigned to the <see cref="Reminder"/> property.
		/// </summary>
		[DefaultValue(false)]
		public bool ReminderSet
		{
			get
			{
				if (_reminderRemoved)
				{
					return false;
				}
				return _reminder != null;
			}
			set
			{
                if (ReminderSet == value)
				{
					return;
				}
				BeforePropertyChange("ReminderSet", ReminderSet);
				if (value)
				{
					_reminder = new Reminder(this);
					if (IsEdit)
					{
						_reminderRemoved = false;
						_reminderChanged = true;
						_reminderAdded = true;
					}
					else if (_inCollection)
					{
						if (!IsRecurring)
						{
							_parentCollection.Reminders.Add(_reminder);
						}
						else
						{
							_parentCollection.Reminders.OnReminderAdd(this, new ReminderEventArgs(_reminder));
						}
					}
				}
				else
				{
					if (IsEdit)
					{
						_reminderAdded = false;
						_reminderChanged = true;
						_reminderRemoved = true;
					}
					else if (_inCollection)
					{
						if (!IsRecurring)
						{
							_parentCollection.Reminders.Remove(_reminder);
							_reminder = null;
						}
						else
						{
							_parentCollection.Reminders.OnReminderRemove(this, new ReminderEventArgs(_reminder));
							_reminder = null;
						}
					}
					else if (ParentRecurrence != null && ParentRecurrence._inCollection)
					{
						_parentCollection.Reminders.Remove(_reminder);
						_reminder = null;
					}
					else
					{
						_reminder = null;
					}
				}
                OnPropertyChanged("ReminderSet");
				base.OnPropertyChanged("Reminder"); 
            }
		}

		/// <summary>
		/// Gets or sets the <see cref="TimeSpan"/> value indicating the 
		/// interval of time the reminder should occur 
		/// prior to the start of the appointment. 
		/// </summary>
		[DefaultValue(typeof(TimeSpan), "0:15:00")]
		public TimeSpan ReminderTimeBeforeStart
		{
			get { return _reminderTimeBeforeStart; }
			set
			{
                if (_reminderTimeBeforeStart != value)
                {
					BeforePropertyChange("ReminderTimeBeforeStart", _reminderTimeBeforeStart);
					_reminderTimeBeforeStart = value;
                    if (ReminderSet)
                    {
                        if (IsEdit)
                        {
                            _reminderChanged = true;
                        }
                        else if (!IsRecurring)
                        {
                            _reminder.Update();
                        }
                        else if (_inCollection)
                        {
                            _parentCollection.Reminders.OnReminderChange(this, new ReminderEventArgs(_reminder));
                        }
                    }
                    OnPropertyChanged("ReminderTimeBeforeStart");
                }
            }
		}

        internal void DirtyReminderNextDate()
        {
            if (IsEdit)
            {
                _reminderChanged = true;
            }
			base.OnPropertyChanged("");
        }

		/// <summary>
		/// Gets or sets the <see cref="Boolean"/> value indicating if the associated
		/// <see cref="Reminder"/> object has non-default settings.
		/// True if the reminder overrides the default reminder behavior for the appointment.
		/// You must set the <see cref="ReminderOverrideDefault"/> property to validate 
		/// the <see cref="ReminderPlaySound"/> and 
		/// the <see cref="ReminderSoundFile"/> properties.
		/// </summary>
		[DefaultValue(false)]
		public bool ReminderOverrideDefault
		{
			get { return _reminderOverrideDefault; }
			set
			{
				if (_reminderOverrideDefault != value)
				{
					BeforePropertyChange("ReminderOverrideDefault", _reminderOverrideDefault);
					_reminderOverrideDefault = value;
                    OnPropertyChanged("ReminderOverrideDefault"); 
                }
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Boolean"/> value indicating if the reminder 
		/// should play a sound when it occurs for this appointment or task. 
		/// The <see cref="ReminderPlaySound"/> property must be set in order to validate 
		/// the <see cref="ReminderSoundFile"/> property. 
		/// This property is only valid if the <see cref="ReminderOverrideDefault"/> property 
		/// is set to true.
		/// </summary>
		[DefaultValue(true)]
		public bool ReminderPlaySound
		{
			get { return _reminderPlaySound; }
			set
			{
				if (_reminderPlaySound != value)
				{
					BeforePropertyChange("ReminderPlaySound", _reminderPlaySound);
					_reminderPlaySound = value;
                    OnPropertyChanged("ReminderPlaySound"); 
                }
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="String"/> value indicating the path and file name 
		/// of the sound file to play when the reminder occurs for the appointment. 
		/// This property is only valid if the <see cref="ReminderOverrideDefault"/> and 
		/// <see cref="ReminderPlaySound"/> properties are set to true.
		/// </summary>
		/// <remarks>The default sound is the system Exclamation sound.</remarks>
        [DefaultValue("")]
		public string ReminderSoundFile
		{
			get { return _reminderSoundFile; }
			set
			{
				value = GetValidString(value);
				if (!_reminderSoundFile.Equals(value))
				{
					BeforePropertyChange("ReminderSoundFile", _reminderSoundFile);
					_reminderSoundFile = value;
                    OnPropertyChanged("ReminderSoundFile"); 
                }
			}
		}
        #endregion

        #region * Recurrence
        /// <summary>
		/// Gets the <see cref="Boolean"/> value indicating if the appointment is recurring.
		/// </summary>
		public bool IsRecurring
		{
			get { return (_recurrenceState == RecurrenceStateEnum.Master); }
		}

		/// <summary>
		/// Gets the <see cref="Appointment"/> object that defines the recurrence 
		/// criteria for this <see cref="Appointment"/> object. 
		/// If this appointment is a not member of a recurrence, 
		/// or is itself a root appointment, null is returned. 
		/// </summary>
		/// <remarks>If an appointment is recurring (see the <see cref="IsRecurring"/> property) 
		/// it represents an occurrence in the series that is started and defined 
		/// by a specific pattern appointment. 
		/// Use the <see cref="ParentRecurrence"/> property to obtain the pattern 
		/// of the current appointment. A pattern appointment can be recognized by its 
		/// <see cref="RecurrenceState"/> property set 
		/// to the <see cref="RecurrenceStateEnum.Master"/> value. 
		/// The recurrence information defined by the pattern appointment can be accessed 
		/// via the <see cref="RecurrencePattern"/> property of the appointment. 
		/// </remarks>
		public Appointment ParentRecurrence
		{
			get { return _parentRecurrence; }
			internal set { _parentRecurrence = value; }
		}

		/// <summary>
		/// Gets the <see cref="RecurrenceStateEnum"/> value indicating 
		/// the recurrence state of the appointment. 
		/// </summary>
		public RecurrenceStateEnum RecurrenceState
		{
			get { return _recurrenceState; }
			internal set { _recurrenceState = value; }
		}
        #endregion

        #region * Label, Resources, Categories, Links, BusyStatus, Owner
        /// <summary>
		/// Gets or sets the <see cref="Label"/> object associated with this appointment.
		/// </summary>
		/// <remarks>Labels may be associated with an appointment 
		/// to effectively group appointments. </remarks>
        /// <remarks>This property only accepts <see cref="Label"/> objects which are present in the C1ScheduleStorage.LabelStorage storage.</remarks>
        public Label Label
		{
			get
			{
				if (_label == null && _parentCollection != null)
				{
                    LabelCollection labels = _parentCollection.Labels;
					if (_labelIndex != -1)
					{
						_label = labels.FindByIndex(_labelIndex);
						_labelId = Guid.Empty;
					}
					else if (!_labelId.Equals(Guid.Empty))
					{
						if (labels.Contains(_labelId))
						{
							_label = labels[_labelId];
						}
					}
                    else if (labels.Contains(LabelCollection.NoneLabelId))
                        _label = labels[LabelCollection.NoneLabelId];
				}
				return _label;
			}
			set
			{
				if (_label != value)
				{
					BeforePropertyChange("Label", _label);
					_labelId = Guid.Empty;
					_labelIndex = -1;
					_label = value;
					OnPropertyChanged("Label");
				}
			}
		}

		/// <summary>
		/// Gets the <see cref="ResourceList"/> object which holds the set of 
		/// <see cref="Resource"/> objects for this appointment.
		/// An appointment may be associated with multiple resources.
		/// </summary>
        /// <remarks>This property only accepts <see cref="Resource"/> objects which are present in the C1ScheduleStorage.ResourceStorage storage.</remarks>
        public ResourceList Resources
		{
			get
			{
				if (_resources == null && _parentCollection != null)
				{
					_resources = new ResourceList(_parentCollection.Resources);
					if (_resourceIds != null)
					{
						_resources.Clear();
						_resources.AddItems(_resourceIds, _resourceIndices);
						_resourceIndices = null;
						_resourceIds = null;
					}
				}
				return _resources;
			}
			internal set
			{
				if ((Resources == null && value != null) || (Resources != null && !Resources.Equals(value)))
				{
					BeforePropertyChange("Resources", Resources);
					_resourceIndices = null;
					_resourceIds = null;
					_resources = value;
					OnPropertyChanged("Resources");
				}
			}
		}

		/// <summary>
		/// Gets the <see cref="ContactList"/> object which holds the set 
		/// of <see cref="Contact"/> objects for this appointment.
		/// An appointment may be associated with multiple contacts.
		/// </summary>
        /// <remarks>This property only accepts <see cref="Contact"/> objects which are present in the C1ScheduleStorage.ContactStorage storage.</remarks>
        public ContactList Links
		{
			get
			{
				if (_links == null && _parentCollection != null)
				{
					_links = new ContactList(_parentCollection.Contacts);
					if (_linkIds != null)
					{
						_links.Clear();
						_links.AddItems(_linkIds, _linkIndices);
						_linkIndices = null;
						_linkIds = null;
					}
				}
				return _links;
			}
			internal set
			{
				if ((Links == null && value != null) || (Links != null && !Links.Equals(value)))
				{
					BeforePropertyChange("Links", Links);
					_linkIndices = null;
					_linkIds = null;
					_links = value;
					OnPropertyChanged("Links");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Status"/> object indicating the busy status 
		/// of the user for the appointment.
		/// </summary>
        /// <remarks>This property only accepts <see cref="Status"/> objects which are present in the C1ScheduleStorage.StatusStorage storage.</remarks>
        [DefaultValue(typeof(Status), "Free")]
		public Status BusyStatus
		{
			get
			{
				if (_busyStatus == null && _parentCollection != null)
				{
                    StatusCollection statuses = _parentCollection.Statuses;
					if (_busyStatusIndex != -1)
					{
						_busyStatus = statuses.FindByIndex(_busyStatusIndex);
						_busyStatusId = Guid.Empty;
					}
					else if (!_busyStatusId.Equals(Guid.Empty))
					{
						if (statuses.Contains(_busyStatusId))
						{
							_busyStatus = statuses[_busyStatusId];
						}
						else
						{
							_busyStatus = statuses[StatusTypeEnum.Busy];
						}
					}
					else
					{
						// default status
						_busyStatus = statuses[StatusTypeEnum.Busy];
					}
				}
				return _busyStatus; 
			}
			set
			{
				if (_busyStatus != value)
				{
					BeforePropertyChange("BusyStatus", _busyStatus);
					_busyStatusId = Guid.Empty;
					_busyStatusIndex = -1;
					_busyStatus = value;
                    OnPropertyChanged("BusyStatus"); 
                }
			}
		}

		/// <summary>
		/// Gets the <see cref="CategoryList"/> object which holds the set 
		/// of categories assigned to the appointment.
		/// Categories may be associated with an appointment to effectively group appointments. 
		/// An appointment may be associated with multiple categories.
		/// </summary>
        /// <remarks>This property only accepts <see cref="Category"/> objects which are present in the C1ScheduleStorage.CategoryStorage storage.</remarks>
        public CategoryList Categories
		{
			get
			{
				if (_categories == null && _parentCollection != null)
				{
					_categories = new CategoryList(_parentCollection.Categories);
					if (_categoryIds != null)
					{
						_categories.Clear();
						_categories.AddItems(_categoryIds, _categoryIndices);
						_categoryIndices = null;
						_categoryIds = null;
					}
				}
				return _categories;
			}
			internal set
			{
				if ((Categories == null && value != null) || (Categories != null && !Categories.Equals(value)))
				{
					BeforePropertyChange("Categories", Categories);
					_categoryIndices = null;
					_categoryIds = null;
					_categories = value;
					OnPropertyChanged("Categories");
				}
			}
		}
        #endregion

        #region * Owner
        /// <summary>
        /// Gets or sets the <see cref="Contact"/> object which owns current <see cref="Appointment"/> object.
        /// </summary>
        /// <remarks>This property only accepts <see cref="Contact"/> objects which are present in the C1ScheduleStorage.OwnerStorage storage.</remarks>
        /// <value>The default value is null.</value>
        public Contact Owner
        {
            get
            {
                if (_owner == null && _parentCollection != null)
                {
                    ContactCollection owners = _parentCollection.Owners;
                    if (_ownerIndex != -1)
                    {
                        _owner = owners.FindByIndex(_ownerIndex);
                        _ownerId = Guid.Empty;
                    }
                    else if (!_ownerId.Equals(Guid.Empty))
                    {
                        if (owners.Contains(_ownerId))
                        {
                            _owner = owners[_ownerId];
                        }
                    }
                }
                return _owner;
            }
            set
            {
                if (_owner != value)
                {
					BeforePropertyChange("Owner", Owner);
					_ownerId = Guid.Empty;
                    _ownerIndex = -1;
                    _owner = value;
                    OnPropertyChanged("Owner");
                }
            }
        }

#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        internal Guid OwnerId
        {
#if !EXTENDER
            [SmartAssembly.Attributes.DoNotObfuscate]
#endif
            get
            {
                return _ownerId;
            }
        }
        internal bool SetOwnerId(Guid id)
        {
            if ((_owner != null && _owner.Id != id) || _ownerId != id)
            {
                _ownerId = id;
                return true;
            }
            return false;
        }
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        internal int OwnerIndex
        {
#if !EXTENDER
            [SmartAssembly.Attributes.DoNotObfuscate]
#endif
            get
            {
                return _ownerIndex;
            }
        }
        internal bool SetOwnerIndex(int index)
        {
            if ((_owner != null && _owner.Index != index) || _ownerIndex != index)
            {
                _ownerIndex = index;
                return true;
            }
            return false;
        }
        #endregion
        #endregion

        #region ** public methods
        #region * BeginEdit/EndEdit
        /// <summary>
		/// Prevents the <see cref="Appointment"/> object from being updated until 
		/// the <see cref="EndEdit"/> method is called.
		/// </summary>
		/// <remarks>The <see cref="BeginEdit"/> and <see cref="EndEdit"/> methods 
		/// are designed to implement batch modifications. 
		/// This allows you to prevent generation of change notifications
		/// when changing multiple settings at the same time. 
		/// To do this enclose the code that changes the properties in calls to these methods.
		/// Each call to <see cref="BeginEdit"/> must be paired with a call to <see cref="EndEdit"/>. 
		/// To ensure that <see cref="EndEdit"/> is always called even if an exception occurs, 
		/// wrap any calls to these methods in a try...finally statement.</remarks>
        public override void BeginEdit()
		{
            if (IsEdit) 
                return;
			_lastAcceptedStart = _start;
			_lastAcceptedDuration = _duration;
			base.BeginEdit();
		}

		/// <summary>
		/// Unlocks the <see cref="Appointment"/> object after a call 
		/// to the <see cref="BeginEdit"/> method and causes an immediate update. 
		/// </summary>
		/// <param name="canRaiseChangeNotification">The <see cref="Boolean"/> value determining
		/// if change notifications can be generated when applying changes.</param>
		protected override void EndEdit(bool canRaiseChangeNotification)
		{
			if (!IsEdit)
			{
				_changed = false;
				return;
			}
			_lastAcceptedStart = _start;
			_lastAcceptedDuration = _duration;

			EndEditReminder(canRaiseChangeNotification);

			if (_recStateChanged)
			{
				_recStateChanged = false;
				_changed = true;
				if (canRaiseChangeNotification && _inCollection)
				{
					_parentCollection.OnAppointmentRecurrenceStateChanged(this, new AppointmentEventArgs(this));
				}
			}

			if (_changed)
			{
				if (!ValidateOccurrence(canRaiseChangeNotification))
				{
					CancelExceptionsHandling();
					return;
				}
				if (_recurrenceState == RecurrenceStateEnum.Occurrence )
				{
					RecurrencePattern pattern = GetRecurrencePattern();
					if (pattern != null && !pattern.Exceptions.Contains(this))
					{
					    _recurrenceState = RecurrenceStateEnum.Exception;
					    pattern.Exceptions.Add(this);
					    OnPropertyChanged("RecurrenceState");
					}
				}
				_changed = false;
				base.EndEdit(false);
				if (_inCollection || (ParentRecurrence != null && ParentRecurrence._inCollection))
				{
					foreach (Action action in _parentCollection.Actions)
					{
						if (action.ParentAppointment == this)
						{
							_parentCollection.Actions.Remove(action);
							break;
						}
					}
					if (_action != null)
					{
						_parentCollection.Actions.Add(_action);
					}
					if (canRaiseChangeNotification)
					{
						_parentCollection.OnAppointmentChanged(this, new AppointmentEventArgs(this));
					}
				}
			}
			else
			{
				base.EndEdit(false);
			}
			FinishPropertyChange(); 
		}
        #endregion

        #region * Grouping
        /// <summary>
        /// Returns True if the current <see cref="Appointment"/> object is a member of a group which is specified by the
        /// groupOwner and groupBy parameters; False otherwise. 
        /// </summary>
        /// <param name="groupOwner">The <see cref="BaseObject"/> object determining the appointment group.</param>
        /// <param name="groupBy">The <see cref="String"/> value determining the type of grouping.</param>
        /// <returns></returns>
        public bool IsGroupMember(BaseObject groupOwner, string groupBy)
        {
            if (string.IsNullOrEmpty(groupBy))
            {
                return true;
            }
            switch (groupBy)
            {
                case "Owner":
                    if (groupOwner == null)
                    {
                        return Owner == null;
                    }
                    return groupOwner == Owner;
                case "Contact":
                    if (groupOwner == null)
                    {
                        return Links.Count == 0;
                    }
                    Contact cnt = groupOwner as Contact;
                    if (cnt == null)
                    {
                        throw new ArgumentException("groupOwner");
                    }
                    return Links.Contains(cnt);
                case "Category":
                    if (groupOwner == null)
                    {
                        return Categories.Count == 0;
                    }
                    Category cat = groupOwner as Category;
                    if (cat == null)
                    {
                        throw new ArgumentException("groupOwner");
                    }
                    return Categories.Contains(cat);
                default:
                    if (groupOwner == null)
                    {
                        return Resources.Count == 0;
                    }
                    // search in Resources
                    Resource res = groupOwner as Resource;
                    if (res == null)
                    {
                        throw new ArgumentException("groupOwner");
                    }
                    return Resources.Contains(res);
            }
        }

        // sets a new group owner for the current appointment.
        internal void SetOwner(BaseObject newOwner, BaseObject oldOwner, string groupBy)
        {
            if (string.IsNullOrEmpty(groupBy) || newOwner == oldOwner)
            {
                return;
            }
            switch (groupBy)
            {
                case "Owner":
                    // single object, so we don't need oldOwner here
                    if (newOwner != null && !(newOwner is Contact))
                    {
                        throw new ArgumentException("newOwner");
                    }
                    Owner = newOwner as Contact;
                    break;
                case "Contact":
                    Contact cnt = newOwner as Contact;
                    if (newOwner != null && cnt == null)
                    {
                        throw new ArgumentException("newOwner");
                    }
                    Contact oldCnt = oldOwner as Contact;
                    if (oldOwner != null && oldCnt == null)
                    {
                        throw new ArgumentException("oldOwner");
                    }
                    if (InvalidateList<Contact>(Links, cnt, oldCnt))
                    {
                        SetDirty();
                    }
                    break;
                case "Category":
                    Category cat = newOwner as Category;
                    if (newOwner != null && cat == null)
                    {
                        throw new ArgumentException("newOwner");
                    }
                    Category oldCat = oldOwner as Category;
                    if (oldOwner != null && oldCat == null)
                    {
                        throw new ArgumentException("oldOwner");
                    }
                    if (InvalidateList<Category>(Categories, cat, oldCat))
                    {
                        SetDirty();
                    }
                    break;
                default:
                    Resource res = newOwner as Resource;
                    if (newOwner != null && res == null)
                    {
                        throw new ArgumentException("newOwner");
                    }
                    Resource oldRes = oldOwner as Resource;
                    if (oldOwner != null && oldRes == null)
                    {
                        throw new ArgumentException("oldOwner");
                    }
                    // todo: when resource groups are added, clear only resource group specified in groupBy
                    if (InvalidateList<Resource>(Resources, res, oldRes))
                    {
                        SetDirty();
                    }
                    break;
            }
        }
        #endregion

        #region * Recurrence
        /// <summary>
		/// Removes the recurrence settings and restores 
		/// the <see cref="RecurrenceStateEnum.NotRecurring"/> state for an appointment.
		/// </summary>
		public void ClearRecurrencePattern()
		{
			if (_recurrencePattern != null && _recurrenceState == RecurrenceStateEnum.Master)
			{
                DetachFromRecPattern();
				_recurrencePattern = null;
				_recurrenceState = RecurrenceStateEnum.NotRecurring;
                OnPropertyChanged("RecurrenceState");
                if (IsEdit)
				{
					_recStateChanged = true;
				}
				else
				{
					OnRecurrenceStateChanged();
				}
			}
		}

		/// <summary>
		/// Returns the <see cref="RecurrencePattern"/> object that represents 
		/// the recurrence attributes of an appointment. 
		/// If there is no existing recurrence pattern, a new 
		/// empty <see cref="RecurrencePattern"/> object is returned.
		/// </summary>
		/// <returns>The <see cref="RecurrencePattern"/> object.</returns>
		public RecurrencePattern GetRecurrencePattern()
		{
			if (_recurrencePattern == null && _recurrenceState == RecurrenceStateEnum.NotRecurring)
			{
				_recurrencePattern = new RecurrencePattern(this);
				_recurrenceState = RecurrenceStateEnum.Master;
                AttachToRecPattern();
                OnPropertyChanged("RecurrenceState");
                if (IsEdit)
				{
					_recStateChanged = true;
				}
				else
				{
					OnRecurrenceStateChanged();
				}
            }
			else if (_recurrencePattern!= null && _recurrencePattern.Owner.ParentCollection == null)
			{
				_recurrencePattern.Owner._parentCollection = this._parentCollection;
			}
			else if (_recurrencePattern == null && ParentRecurrence != null)
			{
				return ParentRecurrence.GetRecurrencePattern();
			}
			return _recurrencePattern;
		}
        #endregion

        #region * Copy
        /// <summary>
		/// Creates the copy of the <see cref="Appointment"/> object.
		/// </summary>
		/// <returns>The new <see cref="Appointment"/> instance.</returns>
		public Appointment Copy()
		{
            Appointment app = GetCopy();
			if (IsRecurring)
			{
				app._recurrencePattern = _recurrencePattern.Copy();
				app._recurrencePattern.Owner = app;
				app.AttachToRecPattern();
			}
			return app;
		}
        /// <summary>
        /// Creates the copy of the <see cref="Appointment"/> object. Doesn't copy recurrence pattern.
        /// </summary>
        /// <returns>The new <see cref="Appointment"/> instance.</returns>
        internal Appointment CopyForPattern()
        {
            Appointment app = GetCopy();
            app._reminder = null;
			app._exceptionsForUpdate = null;
            return app;
        }

		/// <summary>
		/// Copies properties from the specified <see cref="Appointment"/> object to this one. 
		/// </summary>
		/// <param name="app">The <see cref="Appointment"/> instance to copy.</param>
		/// <param name="includeKeys">Specifies whether to copy internal keys.</param>
		///<remarks><para>Set includeKeys to true if you want to receive exact copy 
		///of the specified <see cref="Appointment"/> object.</para>
		///<para>Set includeKeys to false in order to copy only descriptive <see cref="Appointment"/> 
		///properties to other <see cref="Appointment"/> object (for example, 
		///if you want to duplicate appointment from one day to another).</para></remarks>
		public void CopyFrom(Appointment app, bool includeKeys)
		{
			bool needEndEdit = !IsEdit;
			if (needEndEdit)
			{
				_lastAcceptedStart = _start;
				_lastAcceptedDuration = _duration;
				BeginEditInternal();
			}

			if (includeKeys)
			{
				Id = app.Id;
				Index = app.Index;
			}

			if (RecurrenceState != app.RecurrenceState)
			{
				_recStateChanged = true;
				if (RecurrenceState == RecurrenceStateEnum.Master)
				{
					DetachFromRecPattern();
					_recurrencePattern = null;
				}
				RecurrenceState = app.RecurrenceState;
			}
			if (RecurrenceState == RecurrenceStateEnum.Master)
			{
				if (_recurrencePattern != null)
				{
					DetachFromRecPattern();
				}
				_recurrencePattern = app.GetRecurrencePattern().Copy();
				_exceptionsForUpdate = app._exceptionsForUpdate;
				_recStateChanged = true;
				_recurrencePattern.Owner = this;
				AttachToRecPattern();
			}
			else if (RecurrenceState != RecurrenceStateEnum.NotRecurring)
			{
				_parentRecurrence = app.ParentRecurrence;
			}

			Subject = app.Subject;
			Body = app.Body;
			Location = app.Location;

			Start = app.Start;
			Duration = app.Duration;
			AllDayEvent = app.AllDayEvent;

			Importance = app.Importance;
			Sensitivity = app.Sensitivity;

			Tag = app.Tag;
			_action = app.Action;
			_autoResolvedWinner = app.AutoResolvedWinner;

			BusyStatus = app.BusyStatus;
			Label = app.Label;

			ReminderOverrideDefault = app.ReminderOverrideDefault;
			ReminderPlaySound = app.ReminderPlaySound;
			ReminderSoundFile = app.ReminderSoundFile;
			ReminderTimeBeforeStart = app.ReminderTimeBeforeStart;
			ReminderSet = app.ReminderSet;
			if (_reminder != null)
			{
				_reminder.SetNextReminderDate(app.Reminder.NextReminderDate, 3);
			}

            if (!BaseList<Category>.IsEquals(Categories, app.Categories))
            {
				BeforePropertyChange("Categories", Categories);
                _categories = null;
                _categoryIds = GetGuids(app.Categories);
                _categoryIndices = GetIndices(app.Categories);
                OnPropertyChanged("Categories");
            }
            if (!BaseList<Resource>.IsEquals(Resources, app.Resources))
            {
				BeforePropertyChange("Resources", Resources);
				_resources = null;
                _resourceIds = GetGuids(app.Resources);
                _resourceIndices = GetIndices(app.Resources);
                OnPropertyChanged("Resources");
            }
            if (!BaseList<Contact>.IsEquals(Links, app.Links))
            {
				BeforePropertyChange("Links", Links);
				_links = null;
                _linkIds = GetGuids(app.Links);
                _linkIndices = GetIndices(app.Links);
                OnPropertyChanged("Links");
            }
			Owner = app.Owner;
			Private = app.Private;
            CustomData = app.CustomData;

			if (needEndEdit)
			{
				this.EndEdit(true);
			}
		}
        #endregion

        /// <summary>
		/// Deletes an appointment and removes it from 
		/// the owning <see cref="AppointmentCollection"/> collection.
		/// </summary>
		public void Delete()
		{
			switch (RecurrenceState)
			{
				case RecurrenceStateEnum.Exception:
					 _parentRecurrence.GetRecurrencePattern().Exceptions.Remove(this);
                     _parentRecurrence.GetRecurrencePattern().RemovedOccurrences.Add(this);
                     _parentCollection.OnAppointmentRemoved(this, new AppointmentEventArgs(this));
					 break;
				case RecurrenceStateEnum.Occurrence:
                     _parentRecurrence.GetRecurrencePattern().RemovedOccurrences.Add(this);
                     _parentCollection.OnAppointmentRemoved(this, new AppointmentEventArgs(this));
					 break;
				default:
					if (_inCollection)
					{
						_parentCollection.Remove(this);
					}
					break;
			}
		}

        /// <summary>
        /// Returns a time term before the appointment start; a negative value means that 
        /// appointment is expired.
        /// </summary>
		/// <returns>A time term before the appointment starts; a negative value means 
		/// that the appointment has expired.</returns>
        public TimeSpan GetTimeTillEvent()
        {
            return DateTime.Now - Start;
        }

#if (!SILVERLIGHT) // in Silverlight you can open files form OpenFileDialog only,
		           // there is no way to open file by path
		/// <summary>
		/// Saves the appointment to the specified path in the format 
		/// of the specified file format. 
		/// </summary>
		/// <param name="path">Required string. The path in which to save the item.</param>
		/// <param name="fileFormat">The <see cref="FileFormatEnum"/> value indicating 
		/// the file format to save.</param>
		public void SaveAs(string path, FileFormatEnum fileFormat)
		{
			List<Appointment> list = new List<Appointment>();
			list.Add(this);
			ParentCollection.ParentStorage.ScheduleStorage.Export(path, list, fileFormat);
		}
#endif
		#endregion

		#region ** Serialization & Persistance
        #region * public stuff
#if (!SILVERLIGHT)
        /// <summary>
		/// Reconstructs appointment from an <see cref="XmlNode"/>. 
		/// </summary>
		/// <param name="node">An <see cref="System.Xml.XmlNode"/> 
		/// which contains the <see cref="Appointment"/> data.</param>
		/// <returns>True if appointment has been changed.</returns>
		public override bool FromXml(XmlNode node)
		{
			using (System.IO.TextReader tr = new System.IO.StringReader(node.OuterXml))
			using (XmlReader reader = XmlReader.Create(tr, XmlExchanger._readerSettings))
			{
				return FromXml(reader);
			}
		}
#endif

		/// <summary>
		/// Reconstructs appointment from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader">An <see cref="System.Xml.XmlReader"/> 
		/// which contains the <see cref="Appointment"/> data.</param>
		/// <returns>True if appointment has been changed.</returns>
		public override bool FromXml(XmlReader reader)
		{
			bool dirty = false;
			bool initEdit = !IsEdit;
			if (initEdit)
			{
				_lastAcceptedStart = _start;
				_lastAcceptedDuration = _duration;
				BeginEditInternal();
			}

			try
			{
				if (reader.Read() && reader.IsStartElement() && reader.HasAttributes)
				{
					SetId(new Guid(reader.GetAttribute("Id")));
					int index;
					if (int.TryParse(reader.GetAttribute("Index"), NumberStyles.Integer, CultureInfo.InvariantCulture, out index))
					{
						SetIndex(index);
					}
				}
				else
				{
					return false;
				}

				string body = "";
				string subject = "";
				string location = "";

				while (reader.Read() && reader.NodeType == XmlNodeType.Element)
				{
					switch (reader.Name)
					{
						case "Body":
							body = XmlExchanger.ReadStringValue(reader);
							break;
						case "Location":
							location = XmlExchanger.ReadStringValue(reader);
							break;
						case "Subject":
							subject = XmlExchanger.ReadStringValue(reader);
							break;
						case "Start":
							DateTime time = CalendarInfo.ParseTime(XmlExchanger.ReadStringValue(reader));
							if (time != _start)
							{
								_previousStart = _start = time;
								dirty = true;
							}
							break;
						case "Duration":
							TimeSpan duration = TimeSpan.FromTicks(long.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture));
							if (duration != _duration)
							{
								_duration = duration;
								dirty = true;
							}
							break;
						case "Owner":
							Guid newId = new Guid(reader.GetAttribute("Id"));
							int newInd = int.Parse(reader.GetAttribute("Index"), CultureInfo.InvariantCulture);
							if ((_owner != null && !_owner.Id.Equals(newId)) || (_owner == null && !_ownerId.Equals(newId)))
							{
								_ownerId = newId;
								_ownerIndex = newInd;
								dirty = true;
							}
							break;
						case "AppointmentProps":
							using (XmlReader reader1 = reader.ReadSubtree())
							{
								dirty = PropsFromXml(reader1) || dirty;
							}
							break;
					}
				}

				if (!body.Equals(_body))
				{
					_body = body;
					dirty = true;
				}
				if (!location.Equals(_location))
				{
					_location = location;
					dirty = true;
				}
				if (!subject.Equals(_subject))
				{
					_subject = subject;
					dirty = true;
				}

			}
			finally
			{
				if (dirty)
				{
					_changed = true;
				}
				if (initEdit)
					EndEdit();
			}
			return dirty; 
		}

		/// <summary>
		/// Creates an XML encoding of the appointment. 
		/// </summary>
		/// <param name="writer">The <see cref="System.Xml.XmlWriter"/> 
		/// that will receive the object data.</param>
		public override void ToXml(XmlWriter writer)
		{
			writer.WriteStartElement("Appointment");
			writer.WriteAttributeString("Id", Id.ToString());
			writer.WriteAttributeString("Index", Index.ToString(CultureInfo.InvariantCulture));

			if (!String.IsNullOrEmpty(Body))
			{
				writer.WriteElementString("Body", Body);
			}
			if (!String.IsNullOrEmpty(Location))
			{
				writer.WriteElementString("Location", Location);
			}
			if (!String.IsNullOrEmpty(Subject))
			{
				writer.WriteElementString("Subject", Subject);
			}
			CalendarInfo info = null;
			if (_parentCollection != null)
			{
				info = _parentCollection.CalendarInfo;
			}
			else if (ParentRecurrence != null && ParentRecurrence._parentCollection != null )
			{
				info = ParentRecurrence._parentCollection.CalendarInfo;
			}
			if (info != null && info.DateTimeKind == DateTimeKind.Utc )
			{
				writer.WriteElementString("Start",
					info.ToDataSourceTime(_start).ToString("r", CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteElementString("Start", _start.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteElementString("Duration", _duration.Ticks.ToString(CultureInfo.InvariantCulture));

			if (Owner != null)
			{
				writer.WriteStartElement("Owner");
				writer.WriteAttributeString("Id", _owner.Id.ToString());
				writer.WriteAttributeString("Index", _owner.Index.ToString(CultureInfo.InvariantCulture));
				writer.WriteEndElement();
			}

			PropsToXml(writer);

			writer.WriteEndElement();
		}

        /// <summary>
        /// Returns XML encoding of appointment properties.
        /// </summary>
        /// <remarks>Use this property to get the same string as serialized value of appointment properties, which you get
        /// when set mapping for AppointmentProperties.</remarks>
        /// <returns>The <see cref="String"/> value containing XML encoding of appointment properties.</returns>
        public string GetAppointmentProperties()
        {
            StringBuilder sb = new StringBuilder();
            using (System.IO.TextWriter writer = new System.IO.StringWriter(sb, System.Globalization.CultureInfo.InvariantCulture))
            using (XmlWriter xmlWriter = XmlWriter.Create(writer, XmlExchanger._writerSettings))
            {
                PropsToXml(xmlWriter);
            }
            return sb.ToString();
        }
        /// <summary>
        /// Reconstructs appointment properties from the specified string.
        /// </summary>
        /// <param name="source">The <see cref="String"/> value containing XML encoding of appointment properties.</param>
        /// <returns>True if appointment has been changed.</returns>
        public bool SetAppointmentProperties(string source)
        {
            source = source.Trim();
            if (!string.IsNullOrEmpty(source))
            {
                using (System.IO.TextReader tr = new System.IO.StringReader(source))
                using (XmlReader reader = XmlReader.Create(tr, XmlExchanger._readerSettings))
                {
                    return PropsFromXml(reader);
                }
            }
            return false;
        }
        #endregion

        #region * internal stuff
#if (!SILVERLIGHT)
        /// <summary>
		/// Reconstructs appointment properties from an <see cref="XmlNode"/>. 
		/// </summary>
		/// <param name="node"></param>
		/// <returns>True if appointment has been changed.</returns>
		internal bool PropsFromXml(XmlNode node)
		{
			using (System.IO.TextReader tr = new System.IO.StringReader(node.OuterXml))
			using (XmlReader reader = XmlReader.Create(tr, XmlExchanger._readerSettings))
			{
				return PropsFromXml(reader);
			}
		}
#endif

		/// <summary>
		/// Reconstructs appointment properties from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader"></param>
		/// <returns>True if appointment has been changed.</returns>
		internal bool PropsFromXml(XmlReader reader)
		{
			bool dirty = false;
			reader.Read();

			bool allDayEvent = false;
			bool autoResolvedWinner = false;
			bool private_ = false;
			ImportanceEnum importance = ImportanceEnum.Normal;
			SensitivityEnum sensitivity = SensitivityEnum.Normal;
			bool isPatternPresent = false;
			Action action = null;
			bool isReminderPresent = false;
			bool isCategoriesPresent = false;
			bool isResourcesPresent = false;
			bool isLinksPresent = false;
            string customData = null;

			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				Guid newId;
				int newInd;
				switch (reader.Name)
				{
					case "AllDayEvent":
						allDayEvent = bool.Parse(XmlExchanger.ReadStringValue(reader));
						break;
					case "AutoResolvedWinner":
						autoResolvedWinner = bool.Parse(XmlExchanger.ReadStringValue(reader));
						break;
					case "Private":
						private_ = bool.Parse(XmlExchanger.ReadStringValue(reader));
						break;
					case "Importance":
						importance = (ImportanceEnum)int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "Sensitivity":
						sensitivity = (SensitivityEnum)int.Parse(XmlExchanger.ReadStringValue(reader), CultureInfo.InvariantCulture);
						break;
					case "RecurrencePattern":
						DetachFromRecPattern();
						_recurrencePattern = new RecurrencePattern(this);
						_recurrenceState = RecurrenceStateEnum.Master;
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							this._recStateChanged = _recurrencePattern.FromXml(reader1);
						}
						AttachToRecPattern();
						isPatternPresent = true;
						break;
					case "Action":
						action = new Action();
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							action.FromXml(reader1);
						}
						action.ParentAppointment = this;
						break;
					case "Reminder":
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							bool reminderOverrideDefault = false;
							bool reminderPlaySound = true;
							string reminderSoundFile = "";
							TimeSpan reminderTimeBeforeStart = TimeSpan.FromMinutes(15);
							reader1.Read();
							while (reader1.Read() && reader1.NodeType == XmlNodeType.Element)
							{
								switch (reader1.Name)
								{
									case "OverrideDefault":
										reminderOverrideDefault = bool.Parse(XmlExchanger.ReadStringValue(reader1));
										break;
									case "PlaySound":
										reminderPlaySound = bool.Parse(XmlExchanger.ReadStringValue(reader1));
										break;
									case "SoundFile":
										reminderSoundFile = XmlExchanger.ReadStringValue(reader1);
										break;
									case "TimeBeforeStart":
										reminderTimeBeforeStart = TimeSpan.FromTicks(
											long.Parse(XmlExchanger.ReadStringValue(reader1), CultureInfo.InvariantCulture));
										break;
                                    case "Set":
										bool val = bool.Parse(XmlExchanger.ReadStringValue(reader1));
										if (val != ReminderSet)
										{
											dirty = true;
											if (val)
											{
												SetReminder();
											}
											else
											{
												_reminder = null;
												_reminderRemoved = true;
											}
										}
										else if (IsEdit)
										{
											if (_reminderAdded && !val)
											{
												_reminder = null;
												_reminderChanged = _reminderAdded = false;
											}
											if (_reminderRemoved && val)
											{
												SetReminder();
												_reminderChanged = _reminderRemoved = false;
											}
										}
										break;
                                    case "NextReminderDate":
                                        DateTime time = CalendarInfo.ParseTime(XmlExchanger.ReadStringValue(reader1));
                                        if (_reminder != null)
                                        {
                                            _reminder.SetNextReminderDate(time, 3);
                                        }
                                        break;
                                }
							}

							if ( reminderOverrideDefault != _reminderOverrideDefault)
							{
								_reminderOverrideDefault = reminderOverrideDefault;
								dirty = true;
							}
							if (reminderPlaySound != _reminderPlaySound)
							{
								_reminderPlaySound = reminderPlaySound;
								dirty = true;
							}
							if (!reminderSoundFile.Equals(_reminderSoundFile))
							{
								_reminderSoundFile = reminderSoundFile;
								dirty = true;
							}
							if (reminderTimeBeforeStart != _reminderTimeBeforeStart)
							{
								_reminderTimeBeforeStart = reminderTimeBeforeStart;
								dirty = true;
							}
						}
                        if (_reminder != null)
                        {
                            _reminder.Update();
                        }
						isReminderPresent = true;
						break;
					case "BusyStatus":
						newId = new Guid(reader.GetAttribute("Id"));
						newInd = int.Parse(reader.GetAttribute("Index"), CultureInfo.InvariantCulture);
						if ((_busyStatus != null && !_busyStatus.Id.Equals(newId))
							|| (_busyStatus == null && !_busyStatusId.Equals(newId)))
						{
                            _busyStatus = null;
							_busyStatusId = newId;
							_busyStatusIndex = newInd;
							dirty = true;
						}
						break;
					case "Label":
						newId = new Guid(reader.GetAttribute("Id"));
						newInd = int.Parse(reader.GetAttribute("Index"), CultureInfo.InvariantCulture);
						if ((_label != null && !_label.Id.Equals(newId))
							|| (_label == null && !_labelId.Equals(newId)))
						{
							_label = null;
							_labelId = newId;
							_labelIndex = newInd;
							dirty = true;
						}
						break;
					case "Categories":
						isCategoriesPresent = true;
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							List<Guid> ids = new List<Guid>();
							List<int> indices = new List<int>();
							FillIds(reader1, ids, indices);
							if (isDifferentGuids(ids, GetGuids(_categories)))
							{
								_categories = null;
								_categoryIds = ids.ToArray();
								_categoryIndices = indices.ToArray();
								dirty = true;
							}
						}
						break;
					case "Resources":
						isResourcesPresent = true;
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							List<Guid> ids = new List<Guid>();
							List<int> indices = new List<int>();
							FillIds(reader1, ids, indices);
							if (isDifferentGuids(ids, GetGuids(_resources)))
							{
								_resources = null;
								_resourceIds = ids.ToArray();
								_resourceIndices = indices.ToArray();
								dirty = true;
							}
						}
						break;
					case "Links":
						isLinksPresent = true;
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							List<Guid> ids = new List<Guid>();
							List<int> indices = new List<int>();
							FillIds(reader1, ids, indices);
							if (isDifferentGuids(ids, GetGuids(_links)))
							{
								_links = null;
								_linkIds = ids.ToArray();
								_linkIndices = indices.ToArray();
								dirty = true;
							}
						}
						break;
                    case "CustomData":
                        customData = XmlExchanger.ReadStringValue(reader);
                        break;
				}
			}
            if (_customData != customData)
            {
                _customData = customData;
                dirty = true;
            }
			if (_allDayEvent != allDayEvent)
			{
				_allDayEvent = allDayEvent;
                if (_allDayEvent && !SatisfyAllDayEventConditions())
                {
                    _allDayEvent = false;
                }
                dirty = true;
			}
			if (_autoResolvedWinner != autoResolvedWinner)
			{
				_autoResolvedWinner = autoResolvedWinner;
				dirty = true;
			}
			if (private_ != _private)
			{
				_private = private_;
				dirty = true;
			}
			if (_importance != importance)
			{
				_importance = importance;
				dirty = true;
			}
			if (_sensitivity != sensitivity)
			{
				_sensitivity = sensitivity;
				dirty = true;
			}
			if (!isPatternPresent)
			{
				ClearRecurrencePattern();
			}
			if (_action != action)
			{
				_action = action;
				dirty = true;
			}
			if (!isReminderPresent && _reminder != null)
			{
				dirty = true;
				_reminder = null;
				_reminderRemoved = true;
			}
			if (!isCategoriesPresent)
			{
				if (_categories != null)
				{
					_categories.Clear();
				}
				_categoryIds = null;
				_categoryIndices = null;
			}
			if (!isResourcesPresent)
			{
				if (_resources != null)
				{
					_resources.Clear();
				}
				_resourceIds = null;
				_resourceIndices = null;
			}
			if (!isLinksPresent)
			{
				if (_links != null)
				{
					_links.Clear();
				}
				_linkIds = null;
				_linkIndices = null;
			}

			return dirty;
		}

		private static void FillIds(XmlReader reader, List<Guid> ids, List<int> indices)
		{
			reader.Read();
			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				ids.Add(new Guid(reader.GetAttribute("Id")));
				indices.Add(int.Parse(reader.GetAttribute("Index"), CultureInfo.InvariantCulture));
			}
		}

		/// <summary>
		/// Creates an XML encoding of the appointment properties. 
		/// </summary>
		/// <param name="writer"></param>
		internal void PropsToXml(XmlWriter writer)
		{
			writer.WriteStartElement("AppointmentProps");

			if (_allDayEvent)
			{
				writer.WriteElementString("AllDayEvent", _allDayEvent.ToString(CultureInfo.InvariantCulture));
			}
			if (_autoResolvedWinner)
			{
				writer.WriteElementString("AutoResolvedWinner", _autoResolvedWinner.ToString(CultureInfo.InvariantCulture));
			}
			if (_private)
			{
				writer.WriteElementString("Private", _private.ToString(CultureInfo.InvariantCulture));
			}
			if (_importance != ImportanceEnum.Normal)
			{
				writer.WriteElementString("Importance", ((int)_importance).ToString(CultureInfo.InvariantCulture));
			}
			if (_sensitivity != SensitivityEnum.Normal)
			{
				writer.WriteElementString("Sensitivity", ((int)_sensitivity).ToString(CultureInfo.InvariantCulture));
			}
			
			if (IsRecurring && _recurrencePattern != null)
			{
				_recurrencePattern.ToXml(writer);
			}

			if (Reminder != null)
			{
				writer.WriteStartElement("Reminder");
				if (_reminderOverrideDefault)
				{
					writer.WriteElementString("OverrideDefault", _reminderOverrideDefault.ToString(CultureInfo.InvariantCulture));
				}
				if (!_reminderPlaySound)
				{
					writer.WriteElementString("PlaySound", _reminderPlaySound.ToString(CultureInfo.InvariantCulture));
				}
				if (!string.IsNullOrEmpty(_reminderSoundFile))
				{
					writer.WriteElementString("SoundFile", _reminderSoundFile);
				}
				if (_reminderTimeBeforeStart.TotalMinutes != 15)
				{
					writer.WriteElementString("TimeBeforeStart", _reminderTimeBeforeStart.Ticks.ToString(CultureInfo.InvariantCulture));
				}
				writer.WriteElementString("Set", ReminderSet.ToString(CultureInfo.InvariantCulture));
                CalendarInfo info = null;
                if (_parentCollection != null)
                {
                    info = _parentCollection.CalendarInfo;
                }
                else if (ParentRecurrence != null && ParentRecurrence._parentCollection != null)
                {
                    info = ParentRecurrence._parentCollection.CalendarInfo;
                }
                if (info != null && info.DateTimeKind == DateTimeKind.Utc)
                {
                    writer.WriteElementString("NextReminderDate",
                        info.ToDataSourceTime(_reminder.NextReminderDate).ToString("r", CultureInfo.InvariantCulture));
                }
                else
                {
                    writer.WriteElementString("NextReminderDate", _reminder.NextReminderDate.ToString(CultureInfo.InvariantCulture));
                }
                writer.WriteEndElement();
			}

			if (_action != null)
			{
				Action.ToXml(writer);
			}

			writer.WriteStartElement("BusyStatus");
			if (_busyStatus == null)
			{
				writer.WriteAttributeString("Id", _busyStatusId.ToString());
				writer.WriteAttributeString("Index", _busyStatusIndex.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteAttributeString("Id", _busyStatus.Id.ToString());
				writer.WriteAttributeString("Index", _busyStatus.Index.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();

			writer.WriteStartElement("Label");
			if (_label == null)
			{
				writer.WriteAttributeString("Id", _labelId.ToString());
				writer.WriteAttributeString("Index", _labelIndex.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteAttributeString("Id", _label.Id.ToString());
				writer.WriteAttributeString("Index", _label.Index.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();

			if (Categories != null && Categories.Count > 0)
			{
				writer.WriteStartElement("Categories");
				writeList(writer, Categories);
				writer.WriteEndElement();
			}
			if (Resources != null && Resources.Count > 0)
			{
				writer.WriteStartElement("Resources");
				writeList(writer, Resources);
				writer.WriteEndElement();
			}
			if (Links != null && Links.Count > 0)
			{
				writer.WriteStartElement("Links");
				writeList(writer, Links);
				writer.WriteEndElement();
			}
            if (!String.IsNullOrEmpty(_customData))
            {
                writer.WriteElementString("CustomData", _customData);
            }
            writer.WriteEndElement();
		}

		private static void writeList<T>(XmlWriter writer, BaseList<T> list) where T : BasePersistableObject
		{
			foreach (T obj in list)
			{
				writer.WriteStartElement("Obj");
				writer.WriteAttributeString("Id", obj.Id.ToString());
				writer.WriteAttributeString("Index", obj.Index.ToString(CultureInfo.InvariantCulture));
				writer.WriteEndElement();
			}
		}

		private static bool isDifferentGuids(List<Guid> ids1, Guid[] ids2)
		{
			if (ids1.Count != ids2.Length)
			{
				return true;
			}
			foreach (Guid id in ids2)
			{
				if (!ids1.Contains(id))
				{
					return true;
				}
			}
			return false;
		}

		static internal Guid[] GetGuids<T>(BaseList<T> list) where T : BasePersistableObject
		{
			Guid[] arr;
			if (list == null)
			{
				arr = new Guid[0];
			}
			else
			{
				arr = new Guid[list.Count];
				for (int i = 0; i < list.Count; i++)
				{
					arr[i] = list[i].Id;
				}
			}
			return arr;
		}

		static internal int[] GetIndices<T>(BaseList<T> list) where T : BasePersistableObject
		{
			List<int> indices = new List<int>();
			if (list != null)
			{
				foreach (T obj in list)
				{
					// don't add default '-1' values
					if (obj.Index >= 0)
					{
						indices.Add(obj.Index);
					}
				}
			}
			return indices.ToArray();
		}

#if(SILVERLIGHT)
		private void OnSerializing()
#else
        [OnSerializing]
		private void OnSerializing(StreamingContext context)
#endif
		{
			// fill Ids
			if (_busyStatus != null)
			{
				_busyStatusId = _busyStatus.Id;
				_busyStatusIndex = _busyStatus.Index;
			}
			if (_label != null)
			{
				_labelId = _label.Id;
				_labelIndex = _label.Index;
			}
			if (_owner != null)
			{
				_ownerId = _owner.Id;
				_ownerIndex = _owner.Index;
			}
			_categoryIds = GetGuids(_categories);
			_resourceIds = GetGuids(_resources);
			_linkIds = GetGuids(_links);

			// fill Indices
			_categoryIndices = GetIndices(_categories);
			_resourceIndices = GetIndices(_resources);
			_linkIndices = GetIndices(_links);
		}

#if (!SILVERLIGHT)
        [OnSerialized]
		private void OnSerialized(StreamingContext context)
        {
            bool prevSaved = _saved;
            _saved = true;
            if (!prevSaved)
                base.OnPropertyChanged("Saved");
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            if (_recurrencePattern != null)
            {
                _recurrencePattern.Owner = this;
            }
        }
        
        /// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected Appointment(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			bool initEdit = !IsEdit;
			if (initEdit)
			{
				_lastAcceptedStart = _start;
				_lastAcceptedDuration = _duration;
				base.BeginEditInternal();
			}
			try
			{
				_action = (Action)info.GetValue("action", typeof(Action));
				if (_action != null)
				{
					_action.ParentAppointment = this;
				}
			}
			catch (SerializationException)
			{
			}
			_autoResolvedWinner = info.GetBoolean("autoresolvedWinner");
			try
			{
				_private = info.GetBoolean("private");
			}
			catch (SerializationException )
			{
			}

			_subject = info.GetString("subject");
			_body = info.GetString("body");
			_location = info.GetString("location");

			DateTime st = info.GetDateTime("start");
			if (st.Kind == DateTimeKind.Utc)
			{
				st = st.ToLocalTime();
			}
			_previousStart = _start = st;
			_duration = (TimeSpan)info.GetValue("duration", typeof(TimeSpan));
			_allDayEvent = info.GetBoolean("allDayEvent");

			_importance = (ImportanceEnum)info.GetValue("importance", typeof(ImportanceEnum));
			_sensitivity = (SensitivityEnum)info.GetValue("sensitivity", typeof(SensitivityEnum));
			_busyStatusId = (Guid)info.GetValue("busyStatusId", typeof(Guid));
			_busyStatusIndex = info.GetInt32("busyStatusIndex");

			_recurrenceState = (RecurrenceStateEnum)info.GetValue("recurrenceState", typeof(RecurrenceStateEnum));
			_recurrencePattern = (RecurrencePattern)info.GetValue("recurrencePattern", typeof(RecurrencePattern));

            AttachToRecPattern();

			_reminderOverrideDefault = info.GetBoolean("reminderOverrideDefault");
			_reminderPlaySound = info.GetBoolean("reminderPlaySound");
			_reminderSoundFile = info.GetString("reminderSoundFile");
			_reminderTimeBeforeStart = (TimeSpan)info.GetValue("reminderTimeBeforeStart", typeof(TimeSpan));
			ReminderSet = info.GetBoolean("reminder");
			if (_reminder != null)
			{
				try
				{
					DateTime remStart = info.GetDateTime("reminderNextDate");
					if (remStart.Kind == DateTimeKind.Utc)
					{
						remStart = remStart.ToLocalTime();
					}
					_reminder.SetNextReminderDate(remStart, 3);
				}
				catch (SerializationException)
				{
				}
			}

			_labelId = (Guid)info.GetValue("labelId", typeof(Guid));
			_labelIndex = info.GetInt32("labelIndex");
			_categoryIds = (Guid[])info.GetValue("categoryIds", typeof(Guid[]));
			_resourceIds = (Guid[])info.GetValue("resourceIds", typeof(Guid[]));
			_linkIds = (Guid[])info.GetValue("linkIds", typeof(Guid[]));

			_labelIndex = (int)info.GetInt32("labelIndex");
			_categoryIndices = (int[])info.GetValue("categoryIndices", typeof(int[]));
			_resourceIndices = (int[])info.GetValue("resourceIndices", typeof(int[]));
			_linkIndices = (int[])info.GetValue("linkIndices", typeof(int[]));

            try
            {
                _customData = info.GetString("customData");
            }
            catch (SerializationException)
            {
            }
			try
			{
				_ownerId = (Guid)info.GetValue("ownerId", typeof(Guid));
			}
			catch (SerializationException)
			{
			}
			try
			{
				_ownerIndex = info.GetInt32("ownerIndex");
			}
			catch (SerializationException)
			{
			}

			if (initEdit)
				EndEdit();
		}

		// A method called when serializing.
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("action", _action, typeof(Action));
			info.AddValue("autoresolvedWinner", _autoResolvedWinner);
			info.AddValue("private", _private);
			info.AddValue("subject", _subject);
			info.AddValue("body", _body);
			info.AddValue("location", _location);

			CalendarInfo cinfo = null;
			if (_parentCollection != null)
			{
				cinfo = _parentCollection.CalendarInfo;
			}
			else if (ParentRecurrence != null && ParentRecurrence._parentCollection != null)
			{
				cinfo = ParentRecurrence._parentCollection.CalendarInfo;
			}
			if (cinfo != null)
			{
				info.AddValue("start", cinfo.ToDataSourceTime(_start));
			}
			else
			{
				info.AddValue("start", _start);
			}
			info.AddValue("duration", _duration, typeof(TimeSpan));
			info.AddValue("allDayEvent", _allDayEvent);

			info.AddValue("importance", _importance, typeof(ImportanceEnum));
			info.AddValue("sensitivity", _sensitivity, typeof(SensitivityEnum));
			info.AddValue("busyStatusId", _busyStatusId, typeof(Guid));
			info.AddValue("busyStatusIndex", _busyStatusIndex);

			info.AddValue("recurrenceState", _recurrenceState, typeof(RecurrenceStateEnum));
			if (IsRecurring)
			{
				info.AddValue("recurrencePattern", _recurrencePattern, typeof(RecurrencePattern));
			}
			else
			{
				info.AddValue("recurrencePattern", null, typeof(RecurrencePattern));
			}

			info.AddValue("reminderOverrideDefault", _reminderOverrideDefault);
			info.AddValue("reminderPlaySound", _reminderPlaySound);
			info.AddValue("reminderSoundFile", _reminderSoundFile);
			info.AddValue("reminderTimeBeforeStart", _reminderTimeBeforeStart, typeof(TimeSpan));
			info.AddValue("reminder", ReminderSet);
			if (_reminder != null)
			{
				if (cinfo != null)
				{
					info.AddValue("reminderNextDate", cinfo.ToDataSourceTime(_reminder.NextReminderDate));
				}
				else
				{
					info.AddValue("reminderNextDate", _reminder.NextReminderDate);
				}
			}

			info.AddValue("labelId", _labelId, typeof(Guid));
			info.AddValue("labelIndex", _labelIndex);
			info.AddValue("categoryIds", _categoryIds, typeof(Guid[]));
			info.AddValue("resourceIds", _resourceIds, typeof(Guid[]));
			info.AddValue("linkIds", _linkIds, typeof(Guid[]));

			info.AddValue("labelIndices", _labelIndex, typeof(int));
			info.AddValue("categoryIndices", _categoryIndices, typeof(int[]));
			info.AddValue("resourceIndices", _resourceIndices, typeof(int[]));
			info.AddValue("linkIndices", _linkIndices, typeof(int[]));
            if (!string.IsNullOrEmpty(_customData))
            {
                info.AddValue("customData", _customData);
            }
			if (Owner != null)
			{
				info.AddValue("ownerId", _ownerId, typeof(Guid));
				info.AddValue("ownerIndex", _ownerIndex);
			}
        }
#endif
        #endregion
        #endregion

		#region ** Handle changes of the Master appointment properties
#if (!SILVERLIGHT)
		[NonSerialized]
#endif
		Dictionary<string, List<Appointment>> _exceptionsForUpdate = null;

		private void BeforePropertyChange(string propertyName, object oldValue)
		{
			if (RecurrenceState == RecurrenceStateEnum.Master && _recurrencePattern.Exceptions.Count > 0)
			{
				switch (propertyName)
				{
					case "Categories":
						foreach (Appointment ex in _recurrencePattern.Exceptions)
						{
							if (BaseList<Category>.IsEquals(Categories, ex.Categories))
							{
								EnsurePropertyIncluded(propertyName);
								_exceptionsForUpdate[propertyName].Add(ex);
							}
						}
						break;
					case "Resources":
						foreach (Appointment ex in _recurrencePattern.Exceptions)
						{
							if (BaseList<Resource>.IsEquals(Resources, ex.Resources))
							{
								EnsurePropertyIncluded(propertyName);
								_exceptionsForUpdate[propertyName].Add(ex);
							}
						}
						break;
					case "Links":
						foreach (Appointment ex in _recurrencePattern.Exceptions)
						{
							if (BaseList<Contact>.IsEquals(Links, ex.Links))
							{
								EnsurePropertyIncluded(propertyName);
								_exceptionsForUpdate[propertyName].Add(ex);
							}
						}
						break;
					default:
						PropertyInfo pi = typeof(Appointment).GetProperty(propertyName);
						foreach (Appointment ex in _recurrencePattern.Exceptions)
						{
							object propValue = pi.GetValue(ex, null);
							if ( (propValue != null && propValue.Equals(oldValue)) 
								 || (propValue == oldValue) )
							{
								EnsurePropertyIncluded(propertyName);
								_exceptionsForUpdate[propertyName].Add(ex);
							}
						}
						break;
				}
			}
		}
		private void EnsurePropertyIncluded(string propertyName)
		{
			if ( _exceptionsForUpdate == null )
			{
				_exceptionsForUpdate = new Dictionary<string, List<Appointment>>();
			}
			if (!_exceptionsForUpdate.ContainsKey(propertyName))
			{
				_exceptionsForUpdate.Add(propertyName, new List<Appointment>());
			}
		}
		private void FinishPropertyChange()
		{
			if (_exceptionsForUpdate != null)
			{
				// don't use foreach loops by _exceptionsForUpdate or by exceptions, collections might be changed.
				// use foreach by collection copy or use loops without enumeration (while or for)
				string[] keys = new string[_exceptionsForUpdate.Count];
				_exceptionsForUpdate.Keys.CopyTo(keys, 0);

				foreach (string key in keys)
				{
					List<Appointment> exceptions;
					if (_exceptionsForUpdate.TryGetValue(key, out exceptions))
					{
						PropertyInfo pi = typeof(Appointment).GetProperty(key);
						while (exceptions.Count > 0)
						{
							Appointment exception = exceptions[0];
							pi.SetValue(exception, pi.GetValue(this, null), null);
							exceptions.Remove(exception);
						}
					}
				}
				CancelExceptionsHandling();
			}
		}
		private void CancelExceptionsHandling()
		{
			if (_exceptionsForUpdate != null)
			{
				_exceptionsForUpdate.Clear();
				_exceptionsForUpdate = null;
			}
		}
		/// <summary>
		/// Discards changes since the last <see cref="BeginEdit"/> call.
		/// </summary>
		public override void CancelEdit()
		{
			CancelExceptionsHandling();
			base.CancelEdit();
		}
		#endregion

		#region ** private methods
		/// <summary>
        /// Copies RecurrencePattern from the specified Appointment
        /// </summary>
        /// <param name="app"></param>
        internal void CopyPatternFrom(Appointment app)
        {
            bool needEndEdit = !IsEdit;
            if (needEndEdit)
            {
                _lastAcceptedStart = _start;
                _lastAcceptedDuration = _duration;
                BeginEditInternal();
            }

            if (RecurrenceState != app.RecurrenceState)
            {
                if (RecurrenceState == RecurrenceStateEnum.Master)
                {
                    DetachFromRecPattern();
                    _recurrencePattern = null;
                }
                _recStateChanged = true;
                RecurrenceState = app.RecurrenceState;
            }
            if (RecurrenceState == RecurrenceStateEnum.Master)
            {
                if (_recurrencePattern != null)
                {
                    DetachFromRecPattern();
                }
                _recurrencePattern = _recurrencePattern ?? new RecurrencePattern(this);
                AttachToRecPattern();
                _recurrencePattern.CopyFrom(app._recurrencePattern);
                _recurrencePattern.Owner = this;
                _recStateChanged = true;
            }
            else if (RecurrenceState != RecurrenceStateEnum.NotRecurring)
            {
                _recurrencePattern = null;
                _parentRecurrence = app.ParentRecurrence;
                OnPropertyChanged("ParentRecurrence");
            }

            if (_recStateChanged)
            {
                OnPropertyChanged("RecurrenceState");
            }

            if (needEndEdit)
            {
                this.EndEdit(true);
            }
        }

        private bool ValidateOccurrence(bool canRaiseChangeNotification)
        {
            if (_parentCollection != null)
            {
                if ((RecurrenceState == RecurrenceStateEnum.Occurrence || RecurrenceState == RecurrenceStateEnum.Exception)
                    && _start.Date != _previousStart.Date)
                {
                    // validate
                    RecurrencePattern pattern = GetRecurrencePattern();
                    bool invalid = pattern.Occurrences <= 0 && !pattern.NoEndDate && _start.Date > pattern.PatternEndDate;
                    Appointment app = pattern.GetOccurrence(_start.Date, _parentCollection.CalendarInfo);
                    if (invalid ||
                        (app != null && app != this && app.RecurrenceState != RecurrenceStateEnum.Removed))
                    {
                        // don't allow 2 occurrences in the same day
                        CancelEdit();
                        _changed = false;
                        if (canRaiseChangeNotification && (_inCollection
                            || (ParentRecurrence != null && ParentRecurrence._inCollection)))
                        {
                            _parentCollection.OnAppointmentChanged(this, new AppointmentEventArgs(this));
                        }
                        return false;
                    }
                    else
                    {
                        // if date has been changed, remove this date from pattern
                        foreach (Appointment removed in pattern.RemovedOccurrences)
                        {
                            if (removed.Start.Date == _start.Date)
                            {
                                pattern.RemovedOccurrences.Remove(removed);
                                if (pattern._adjustedOccurrences > 0)
                                {
                                    pattern._adjustedOccurrences--;
                                }
                                break;
                            }
                        }
                        if (pattern.IsValidDate(_previousStart, _parentCollection.CalendarInfo))
                        {
                            if (pattern.Occurrences > 0)
                            {
                                pattern._adjustedOccurrences++;
                            }
                            Appointment removed = CopyForPattern();
                            removed._start = _previousStart;
                            removed._recurrenceState = RecurrenceStateEnum.Removed;
                            pattern.RemovedOccurrences.Add(removed);
                        }
                        _previousStart = _start;
                    }
                }
            }
            return true;
        }

        private void EndEditReminder(bool canRaiseChangeNotification)
        {
            if (_parentCollection != null)
            {
                if (_reminderAdded && _reminder != null && (_inCollection
                    || (ParentRecurrence != null && ParentRecurrence._inCollection)))
                {
                    if (!IsRecurring)
                    {
                        _parentCollection.Reminders.Add(_reminder);
                    }
                    else if (canRaiseChangeNotification)
                    {
                        _parentCollection.Reminders.OnReminderAdd(this, new ReminderEventArgs(_reminder));
                    }
                }
                else if (_reminderRemoved)
                {
                    if (_inCollection
                        || (ParentRecurrence != null && ParentRecurrence._inCollection))
                    {
                        if (!IsRecurring)
                        {
                            _parentCollection.Reminders.Remove(_reminder);
                        }
                        else if (canRaiseChangeNotification)
                        {
                            _parentCollection.Reminders.OnReminderRemove(this, new ReminderEventArgs(_reminder));
                        }
                    }
                    _reminder = null;
                }
                else if (_reminderChanged && _reminder != null)
                {
                    if (!IsRecurring)
                    {
                        _reminder.Update();
                    }
                    else if (canRaiseChangeNotification && (_inCollection
                    || (ParentRecurrence != null && ParentRecurrence._inCollection)))
                    {
                        _parentCollection.Reminders.OnReminderChange(this, new ReminderEventArgs(_reminder));
                    }
                }
            }
            _reminderAdded = false;
            _reminderRemoved = false;
            _reminderChanged = false;
        }
        
        internal string GetFileName()
		{
			string result = Subject;
#if (!SILVERLIGHT)
			char[] invalidChars = System.IO.Path.GetInvalidFileNameChars();
#else
			char[] invalidChars = new char[] {(char)0x0022, (char)0x003c, (char)0x003e, 
									(char)0x007c, (char)0x0000, (char)0x0001, (char)0x0002, 
									(char)0x0003, (char)0x0004, (char)0x0005, (char)0x0006, 
									(char)0x0007, (char)0x0008, (char)0x0009, (char)0x000a, 
									(char)0x000b, (char)0x000c, (char)0x000d, (char)0x000e, 
									(char)0x000f, (char)0x0010, (char)0x0011, (char)0x0012,
									(char)0x0013, (char)0x0014, (char)0x0015, (char)0x0016, 
									(char)0x0017, (char)0x0018, (char)0x0019, (char)0x001a, 
									(char)0x001b, (char)0x001c, (char)0x001d, (char)0x001e,
									(char)0x001f, (char)0x003a, (char)0x002a, (char)0x003f, 
									(char)0x005c, (char)0x002f};
#endif
			foreach (char ch in invalidChars)
			{
				result = result.Replace(ch, '_');
			}
			return result;
		}

		// call this method when alive appointment has been added to C1Schedule in order 
		// to ensure, that Label, Status, Categories, Contacts and Resources 
		// contain valid objects from this C1Schedule control.
		internal void InvalidateProperties()
		{
			OnSerializing(
#if (!SILVERLIGHT)
				new System.Runtime.Serialization.StreamingContext()
#endif
				);
			_busyStatus = null;
			_label = null;
			_categories = null;
			_links = null;
			_owner = null;
			_resources = null;
		}

        internal void SetDirty()
		{
			_changed = true;
		}

		internal void PropertyChanged(string propertyName)
		{
			OnPropertyChanged(propertyName);
		}

		/// <summary>
		/// Overrides default behavior.
		/// </summary>
		/// <param name="propertyName">The property name.</param>
		protected override void OnPropertyChanged(string propertyName)
		{
            bool prevSaved = _saved;
            _saved = false;
            if (prevSaved)
                base.OnPropertyChanged("Saved");
            bool recStateChanged = false;
			if (IsEdit)
			{
				_changed = true;
			}
			else
			{
				_lastAcceptedStart = _start;
				_lastAcceptedDuration = _duration;
				if (propertyName != "RecurrenceState")
                {
					if ((RecurrenceState == RecurrenceStateEnum.Occurrence || RecurrenceState == RecurrenceStateEnum.Exception)
						&& _start.Date != _previousStart.Date)
					{
						RecurrencePattern pattern = ParentRecurrence.GetRecurrencePattern();
						// validate
						bool invalid = pattern.Occurrences <= 0 && !pattern.NoEndDate && _start.Date > pattern.PatternEndDate;
						Appointment app = pattern.GetOccurrence(_start.Date, _parentCollection.CalendarInfo);
						if (invalid || 
							(app != null && app != this && app.RecurrenceState != RecurrenceStateEnum.Removed))
						{
							// don't allow 2 occurrences in the same day
							_start = _previousStart;
							if (_inCollection || (ParentRecurrence != null && ParentRecurrence._inCollection))
							{
								_parentCollection.OnAppointmentChanged(this, new AppointmentEventArgs(this));
							}
							CancelExceptionsHandling();
							base.OnPropertyChanged(propertyName);
							base.OnPropertyChanged("RecurrenceState");
							return;
						}
						else
						{
							// if date has been changed, remove this date from pattern
							foreach (Appointment removed in pattern.RemovedOccurrences)
							{
								if (removed.Start.Date == _start.Date)
								{
									pattern.RemovedOccurrences.Remove(removed);
									if (pattern._adjustedOccurrences > 0)
									{
										pattern._adjustedOccurrences--;
									}
									break;
								}
							}
							if (pattern.IsValidDate(_previousStart, _parentCollection.CalendarInfo))
							{
								if (pattern.Occurrences > 0)
								{
									pattern._adjustedOccurrences++;
								}
								Appointment removed = CopyForPattern();
								removed._start = _previousStart;
								removed._recurrenceState = RecurrenceStateEnum.Removed;
								pattern.RemovedOccurrences.Add(removed);
							}
							_previousStart = _start;
						}
					}
					if (RecurrenceState == RecurrenceStateEnum.Occurrence)
                    {
						RecurrencePattern pattern = GetRecurrencePattern();
						if (!pattern.Exceptions.Contains(this))
						{
							_recurrenceState = RecurrenceStateEnum.Exception;
							pattern.Exceptions.Add(this);
							recStateChanged = true;
						}
                    }
					if (_inCollection || (ParentRecurrence != null && ParentRecurrence._inCollection))
                    {
						foreach (Action action in _parentCollection.Actions)
						{
							if (action.ParentAppointment == this)
							{
								_parentCollection.Actions.Remove(action);
								break;
							}
						}
						if (_action != null)
						{
							_parentCollection.Actions.Add(_action);
						}
						_parentCollection.OnAppointmentChanged(this, new AppointmentEventArgs(this));
                    }
                }
				FinishPropertyChange();
			}
			base.OnPropertyChanged(propertyName);
            if (recStateChanged)
                base.OnPropertyChanged("RecurrenceState");
        }

		private void OnRecurrenceStateChanged()
		{
            bool prevSaved = _saved;
            _saved = false;
            if (prevSaved)
                base.OnPropertyChanged("Saved");
			if (!IsEdit && _inCollection)
			{
				_recStateChanged = false;
				_parentCollection.OnAppointmentRecurrenceStateChanged(this, new AppointmentEventArgs(this));
			}
		}

        private void AttachToRecPattern()
        {
            if (_recurrencePattern != null)
                ((INotifyPropertyChanged)_recurrencePattern).PropertyChanged += 
                    new PropertyChangedEventHandler(RecPattern_PropertyChanged);
        }

        private void DetachFromRecPattern()
        {
            if (_recurrencePattern != null)
                ((INotifyPropertyChanged)_recurrencePattern).PropertyChanged -=
                    new PropertyChangedEventHandler(RecPattern_PropertyChanged);
        }

        void RecPattern_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (IsRecurring && sender == _recurrencePattern)
                OnPropertyChanged(""); // We have no a property for rec. pattern, so send it with empty name
        }

        internal bool SatisfyAllDayEventConditions()
        {
            return _start.TimeOfDay.Ticks == 0 && _duration.Ticks > 0 && End.TimeOfDay.Ticks == 0;
        }
        
        // adds new object to the list and removes old objects if any
        private bool InvalidateList<T>(BaseList<T> list, T newObj, T oldObj) where T : BasePersistableObject
        {
            bool hasChanges = false;
            // do nothing if newObj == oldObj
            if (newObj != oldObj && list.Count > 0)
            {
                list.Clear();
                hasChanges = true;
            }
            if (newObj != null && !list.Contains(newObj))
            {
                list.Add(newObj);
                hasChanges = true;
            }
            return hasChanges;
        }

        private Appointment GetCopy()
        {
            Appointment app = (Appointment)this.MemberwiseClone();
            app._categories = null;
            app._categoryIds = GetGuids(Categories);
            app._categoryIndices = GetIndices(Categories);
            app._resources = null;
            app._resourceIds = GetGuids(Resources);
            app._resourceIndices = GetIndices(Resources);
            app._links = null;
            app._linkIds = GetGuids(Links);
            app._linkIndices = GetIndices(Links);
            app._changed = false;
            app._inCollection = false;
            app._recurrencePattern = null;
            app._reminderChanged = false;
            app._reminderAdded = false;
            app._reminderRemoved = false;
            app.EndEdit(false);
            return app;
        }
        #endregion
 	}
}

