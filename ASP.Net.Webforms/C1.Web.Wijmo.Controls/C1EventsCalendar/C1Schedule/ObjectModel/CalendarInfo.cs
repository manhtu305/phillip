﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Reflection;

// TODO:
// 1. Add events to the public static methods (to allow overriding of default
//   behavior of these methods).
// 2. Add localization for all invariable string resources.
// 3. Change FirstDate to affect on first visible date
//    and add new field for first date of interval

namespace C1.C1Schedule
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Determines the time interval which is used for 
	/// displaying time slots in the DayView.
	/// </summary>
	public enum TimeScaleEnum
	{
	/*	/// <summary>
		/// 1 minute.
		/// </summary>
		OneMinute = 1,
		/// <summary>
		/// 2 minutes.
		/// </summary>
		TwoMinutes = 2,
		/// <summary>
		/// 3 minutes.
		/// </summary>
		ThreeMinutes = 3,
		/// <summary>
		/// 4 minutes.
		/// </summary>
		FourMinutes = 4,*/
		/// <summary>
		/// 5 minutes.
		/// </summary>
		FiveMinutes = 5,
		/// <summary>
		/// 6 minutes.
		/// </summary>
		SixMinutes = 6,
		/// <summary>
		/// 10 minutes.
		/// </summary>
		TenMinutes = 10,
	/*	/// <summary>
		/// 12 minutes.
		/// </summary>
		TwelveMinutes = 12,*/
		/// <summary>
		/// 15 minutes.
		/// </summary>
		FifteenMinutes = 15,
		/// <summary>
		/// 20 minutes.
		/// </summary>
		TwentyMinutes = 20,
		/// <summary>
		/// 30 minutes.
		/// </summary>
		ThirtyMinutes = 30,
		/// <summary>
		/// 60 minutes.
		/// </summary>
		OneHour = 60
	}

#if (!SILVERLIGHT)
	[Obfuscation(Exclude = true, ApplyToMembers = true)]
#endif
	internal class DayOfWeekConverter : EnumConverter
	{
		public static DayOfWeek[] AllWeekDays = {	DayOfWeek.Sunday, DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, 
									DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday};

		public DayOfWeekConverter()
			: base(typeof(DayOfWeek))
		{
		}
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			string str = value as String;
			if (str != null)
			{
				string newValue = str.Trim().ToLower();
                foreach (DayOfWeek d in AllWeekDays)
				{
                    if (culture.DateTimeFormat.GetDayName(d).ToLower() == newValue)
					{
						return d;
					}
				}
                culture = new CultureInfo("en-US");
                foreach (DayOfWeek d in AllWeekDays)
                {
                    if (culture.DateTimeFormat.GetDayName(d).ToLower() == newValue)
                    {
                        return d;
                    }
                }
            }
			return base.ConvertFrom(context, culture, value);
		}
		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
#if (SILVERLIGHT)
            // Blend uses converters in a different direction than for WPF
            // and saves day name with CurrentUI culture. That might fail at run-time.
            // so use English culture here
            culture = new CultureInfo("en-US");
#endif
			if (destinationType == typeof(string) && value is DayOfWeek)
			{
				return culture.DateTimeFormat.GetDayName((DayOfWeek)value);
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}

#if (!SILVERLIGHT)
	[Obfuscation(Exclude = true, ApplyToMembers = true)]
	internal class TimeScaleEnumConverter : EnumConverter
	{
		public TimeScaleEnumConverter()
			: base(typeof(TimeScaleEnum))
		{
		}
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			string str = value as string;
			if (str != null)
			{
				string newValue = str.Trim().ToLower();
#if END_USER_LOCALIZATION
				string anHour = Strings.TimeSpanStrings.OneHour.ToLower();
				string nMinutes = Strings.TimeSpanStrings.NMinutes.ToLower();
#elif WINFX
				string anHour = C1.WPF.Localization.C1Localizer.GetString("TimeStrings", "OneHour", "1 hour", culture).ToLower();
				string nMinutes = C1.WPF.Localization.C1Localizer.GetString("TimeStrings", "NMinutes", "{0} minutes", culture).ToLower();
#else
				string anHour = C1Localizer.GetString("1 hour").ToLower();
				string nMinutes = C1Localizer.GetString("{0} minutes").ToLower();
#endif
				if (anHour == newValue)
				{
					return TimeScaleEnum.OneHour;
				}
				else if (String.Format(nMinutes, 5) == newValue)
				{
					return TimeScaleEnum.FiveMinutes;
				}
				else if (String.Format(nMinutes, 6) == newValue)
				{
					return TimeScaleEnum.SixMinutes;
				}
				else if (String.Format(nMinutes, 10) == newValue)
				{
					return TimeScaleEnum.TenMinutes;
				}
				else if (String.Format(nMinutes, 15) == newValue)
				{
					return TimeScaleEnum.FifteenMinutes;
				}
				else if (String.Format(nMinutes, 20) == newValue)
				{
					return TimeScaleEnum.TwentyMinutes;
				}
				else if (String.Format(nMinutes, 30) == newValue)
				{
					return TimeScaleEnum.ThirtyMinutes;
				}
			}
			return base.ConvertFrom(context, culture, value);
		}
		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is TimeScaleEnum)
			{
#if END_USER_LOCALIZATION
				string anHour = Strings.TimeSpanStrings.OneHour.ToLower();
				string nMinutes = Strings.TimeSpanStrings.NMinutes.ToLower();
#elif WINFX
				string anHour = C1.WPF.Localization.C1Localizer.GetString("TimeStrings", "OneHour", "1 hour", culture).ToLower();
				string nMinutes = C1.WPF.Localization.C1Localizer.GetString("TimeStrings", "NMinutes", "{0} minutes", culture).ToLower();
#else
				string anHour = C1Localizer.GetString("1 hour").ToLower();
				string nMinutes = C1Localizer.GetString("{0} minutes").ToLower();
#endif
				switch ((TimeScaleEnum)value)
				{
					case TimeScaleEnum.OneHour:
						return anHour;
					case TimeScaleEnum.FiveMinutes:
						return String.Format(nMinutes, 5);
					case TimeScaleEnum.SixMinutes:
						return String.Format(nMinutes, 6);
					case TimeScaleEnum.TenMinutes:
						return String.Format(nMinutes, 10);
					case TimeScaleEnum.FifteenMinutes:
						return String.Format(nMinutes, 15);
					case TimeScaleEnum.TwentyMinutes:
						return String.Format(nMinutes, 20);
					case TimeScaleEnum.ThirtyMinutes:
						return String.Format(nMinutes, 30);
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
#endif

	/// <summary>
	/// The <see cref="CalendarInfo"/> class manages and exposes 
	/// all date-related data to C1Schedule controls. 
	/// </summary>
#if (!SILVERLIGHT)
	[DesignTimeVisible(false),
	ToolboxItem(false)]
#endif
	public class CalendarInfo : 
#if (!SILVERLIGHT)
		Component, 
#endif
		INotifyPropertyChanged
	{
		#region fields
		private int _weekStart;
		private TimeSpan _timeScale;
		private WorkDays _workDays;
		private TimeSpan _startDayTime;
		private TimeSpan _endDayTime;
		private List<DateTime> _holidays;
		private List<DateTime> _exceptions;
		
		private string _dateFormat;
		private string _timeFormat;

		private DateTime _firstDate;
		private bool _firstDateChaged = false;
		private DateTime _lastDate;
		private bool _lastDateChaged = false;
		private DateTime _lastTime;

		private DateTimeKind _dateTimeKind = DateTimeKind.Unspecified;

		private CultureInfo _cultureInfo;
		private static CultureInfo _ci;
		#endregion

		#region events
		/// <summary>
		/// Occurs when the range of selected days was changed.
		/// </summary>
		[C1Description("CalendarInfo.SelectedDaysChanged", "Fired when the range of selected days was changed.")]
		public event EventHandler SelectedDaysChanged;
		private void OnSelectedDaysChanged()
		{
			if (SelectedDaysChanged != null)
			{
				SelectedDaysChanged(this, null);
			}
		}
		/// <summary>
		/// Occurs when the property of <see cref="CalendarInfo"/> object was changed.
		/// </summary>
		[C1Description("CalendarInfo.PropertyChanged", "Fired when the property of CalendarInfo object was changed.")]
		public event PropertyChangedEventHandler PropertyChanged;
		internal void OnPropertyChanged(string name)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}
		#endregion

		#region ctor
		/// <summary>
		/// Initializes a new instance of the <see cref="CalendarInfo"/> component.
		/// </summary>
		internal CalendarInfo()
		{
			_weekStart = 7;
			_timeScale = TimeSpan.FromMinutes(30);
			// default values
			_workDays = new WorkDays(this);

			_workDays.Add(DayOfWeek.Monday);
			for (int i = 0; i < 4; i++)
			{
				if ((int)_workDays[i] == 6)
				{
					_workDays.Add(0);
				}
				else
				{
					_workDays.Add(_workDays[i] + 1);
				}
			}
			_startDayTime = TimeSpan.FromHours(7);
			_endDayTime = TimeSpan.FromHours(19);
			_firstDate = DateTime.Today.AddYears(-1);
			SetLastDate(DateTime.Today.AddYears(1));
			_holidays = new List<DateTime>();
			_exceptions = new List<DateTime>();

			_dateFormat = CultureInfo.DateTimeFormat.ShortDatePattern;
			_timeFormat = "t";
		}
		#endregion

		#region calendar properties
		/// <summary>
		/// Gets or sets the <see cref="CultureInfo"/> object which 
		/// holds culture-specific information used by C1Schedule components.
		/// </summary>
		public static CultureInfo Culture
		{
			get
			{
#if GRAPECITY
				return _ci ?? (_ci = new CultureInfo("ja-JP"));
#else
                return _ci ?? (_ci = CultureInfo.CurrentCulture);
#endif
			}
			set
			{
				_ci = value;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="CultureInfo"/> object which 
		/// holds culture-specific information used by C1Schedule components.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
		DefaultValue(typeof(CultureInfo), ""),
		C1Description("CalendarInfo.CultureInfo", "Holds culture-specific information used by C1Schedule components.")]
		public CultureInfo CultureInfo
		{
			get
			{
                return _cultureInfo ?? Culture;
			}
			set
			{
				_cultureInfo = value;
				OnPropertyChanged("CultureInfo");
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="String"/> value determining display format 
		/// of date values to use in appointments and built-in dialogs.
		/// </summary>
		/// <remarks>The actual representation depends on the value 
		/// of the <see cref="Culture"/> property.</remarks>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
		DefaultValue("d"),
		C1Description("CalendarInfo.DateFormatString", "Format string to display dates in appointments and built-in dialogs.")]
		public string DateFormatString
		{
			get
			{
				return _dateFormat;
			}
			set
			{
				if (_dateFormat != value)
				{
					_dateFormat = value;
					OnPropertyChanged("DateFormatString");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="String"/> value determining display format 
		/// of time values to use in appointments and built-in dialogs.
		/// </summary>
		/// <remarks>The actual representation depends on the value 
		/// of the <see cref="Culture"/> property.</remarks>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
	    DefaultValue("t"),
	    C1Description("CalendarInfo.TimeFormatString", "Format string to display times in appointments and built-in dialogs.")]
		public string TimeFormatString
		{
			get
			{
				return _timeFormat;
			}
			set
			{
				if (_timeFormat != value)
				{
					_timeFormat = value;
					OnPropertyChanged("TimeFormatString");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="DayOfWeek"/> value determining
		/// the first day of the week. Default is system settings.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[TypeConverter(typeof(DayOfWeekConverter))]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.WeekStart", "The first working day of the week.")]
		public System.DayOfWeek WeekStart
		{
			get
			{
				if ((int)_weekStart == 7)
				{
					return CultureInfo.DateTimeFormat.FirstDayOfWeek;
				}
				else
				{
					return (System.DayOfWeek)_weekStart;
				}
			}
			set
			{
				if (_weekStart != (int)value)
				{
					_weekStart = (int)value;
					OnPropertyChanged("WeekStart");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="WorkDays"/> object containing the 
		/// set of working days in one week.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[RefreshProperties(RefreshProperties.All)]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.WorkDays", "The list of working days of a week.")]
		public WorkDays WorkDays
		{
			get
			{
                return _workDays ?? (_workDays = new WorkDays(this));
			}
			set
			{
				if (_workDays != value)
				{
					_workDays = value;
					if (value != null)
					{
						_workDays.Sort();
						_workDays._info = this;
					}
					OnPropertyChanged("WorkDays");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="Boolean"/> value determining whether
		/// the <see cref="CalendarInfo"/> object has any working days.
		/// </summary>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DefaultValue(false)]
		public bool NoWorkingDays
		{
			get
			{
				return WorkDays.Count == 0;
			}
			set
			{
				if (value)
				{
					WorkDays.Clear();
					OnPropertyChanged("WorkDays");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="TimeSpan"/> value specifying the time interval 
		/// which is used for displaying time slots in the DayView.
		/// </summary>
		/// <remarks>The <see cref="TimeScale"/> property cannot be set to any 
		/// arbitrary value. When assigning a value to this property it is 
		/// automatically calculated so as to be equal to the nearest 
		/// <see cref="TimeScaleEnum"/> value.</remarks>
#if (!SILVERLIGHT)
		[Browsable(false)]
#endif
		public TimeSpan TimeScale
		{
			get
			{
				return _timeScale;
			}
			set
			{
				if (_timeScale != value)
				{
					double mins = value.TotalMinutes;
					int min = 60;
					if (mins < 5.5)
					{
						min = 5;
					}
					else if (mins < 8)
					{
						min = 6;
					}
					else if (mins < 13.5)
					{
						min = 10;
					}
					else if (mins < 17.5)
					{
						min = 15;
					}
					else if (mins < 25)
					{
						min = 20;
					}
					else if (mins < 45)
					{
						min = 30;
					}
					if (_timeScale.TotalMinutes != min)
					{
						_timeScale = TimeSpan.FromMinutes(min);
						OnPropertyChanged("TimeScale");
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="TimeScaleEnum"/> value specifying the time interval 
		/// which is used for displaying time slots in the DayView.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[RefreshProperties(RefreshProperties.All)]
		[TypeConverter(typeof(TimeScaleEnumConverter))]
#endif
		[C1Category("Calendar Info"),
		DefaultValue(TimeScaleEnum.ThirtyMinutes),
		C1Description("CalendarInfo.TimeInterval", "The default time interval for displaying time slots.")]
		public TimeScaleEnum TimeInterval
		{
			get
			{
				return (TimeScaleEnum)(int)_timeScale.TotalMinutes;
			}
			set
			{
				if (_timeScale.TotalMinutes != (int)value)
				{
					_timeScale = TimeSpan.FromMinutes((int)value);
					OnPropertyChanged("TimeScale");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="TimeSpan"/> value specifying the beginning 
		/// of the working time.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
		C1Description("CalendarInfo.StartDayTime", "The start time of the working day.")]
		public TimeSpan StartDayTime
		{
			get
			{
				return _startDayTime;
			}
			set
			{
				if (_startDayTime != value)
				{
					_startDayTime = value;
					OnPropertyChanged("StartDayTime");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="TimeSpan"/> value specifying 
		/// the end of the working time.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.EndDayTime", "The end time of the working day.")]
		public TimeSpan EndDayTime
		{
			get
			{
				return _endDayTime;
			}
			set
			{
				if (_endDayTime != value)
				{
					_endDayTime = value;
					OnPropertyChanged("EndDayTime");
				}
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="DateTime"/> value specifying minimum date allowed. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.FirstDate", "The minimum date allowed.")]
		public DateTime FirstDate
		{
			get
			{
				return _firstDate;
			}
			set
			{
				DateTime date = value.Date;
				if (_firstDate != date)
				{
					_firstDateChaged = value != DateTime.MinValue;
					if (value == DateTime.MinValue)
					{
						value = DateTime.Today.AddYears(-1);
					}
					if (LastDate <= date)
					{
						LastDate = date.AddMonths(1);
					}
					_firstDate = date;
					OnSelectedDaysChanged();
					OnPropertyChanged("FirstDate");
				}
			}
        }

#pragma warning disable 1591
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
		protected internal bool ShouldSerializeFirstDate()
		{
			return _firstDateChaged;
		}
#pragma warning restore 1591

        /// <summary>
		/// Gets or sets the <see cref="DateTime"/> value specifying maximum date allowed. 
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.LastDate", "The maximum date allowed.")]
		public DateTime LastDate
		{
			get
			{
				return _lastDate;
			}
			set
			{
				DateTime date = value.Date;
				if (_lastDate != date)
				{
					_lastDateChaged = value != DateTime.MinValue;
					if (date == DateTime.MinValue)
					{
						date = DateTime.Today.AddYears(1);
					}
					if (date < _firstDate)
					{
						date = _firstDate.AddMonths(1);
					}
					SetLastDate(date);
					OnSelectedDaysChanged();
					OnPropertyChanged("LastDate");
				}
			}
        }

#pragma warning disable 1591
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
		protected internal bool ShouldSerializeLastDate()
		{
			return _lastDateChaged;
        }
#pragma warning restore 1591

        private void SetLastDate(DateTime value)
		{
			_lastDate = value.Date;
			_lastTime = _lastDate.AddDays(1);
		}
		internal DateTime LastTime
		{
			get
			{
				return _lastTime;
			}
		}

		internal void SetRange(DateTime start, DateTime end)
		{
			bool dirty = false;
			if (_firstDate != start.Date)
			{
				_firstDate = start.Date;
				dirty = true;
			}
			if (_lastDate != end.Date)
			{
				SetLastDate(end);
				dirty = true;
			}
			if (dirty)
			{
				OnSelectedDaysChanged();
			}
		}

		/// <summary>
		/// Gets the <see cref="List{DateTime}"/> object which holds
		/// the list of holidays (non-working days in addition to weekends).
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.Holidays", "The list of holidays.")]
		public List<DateTime> Holidays
		{
			get
			{
				return _holidays;
			}
		}

		/// <summary>
		/// Gets the <see cref="List{DateTime}"/> object which holds
		/// the list of weekend days which should be working.
		/// </summary>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
#endif
		[C1Category("Calendar Info"),
	    C1Description("CalendarInfo.WeekendExceptions", "The list of working weekend days.")]
		public List<DateTime> WeekendExceptions
		{
			get
			{
				return _exceptions;
			}
		}

		/// <summary>
		/// Gets or sets <see cref="DateTimeKind"/> specifying DateTime kind
		/// used for saving <see cref="DateTime"/> values.
		/// </summary>
		/// <remarks>
		/// This property only have an influence on the process of saving data to
		/// the AppointmentStorage.DataSource object and on export operations.
		/// All properties of the <see cref="Appointment"/> and <see cref="RecurrencePattern"/>
		/// objects represent current system local time values.
		/// Set this property to DateTimeLind.Utc if you need to save
		/// data with universal time. In such case the data will be converted 
		/// to the local time and back at every read/write operation.
		/// </remarks>
#if (!SILVERLIGHT)
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
		[C1Category("Calendar Info"),
		DefaultValue(DateTimeKind.Unspecified),
		C1Description("CalendarInfo.DateTimeKind", "DateTime kind.")]
		public DateTimeKind DateTimeKind
		{
			get
			{
				return _dateTimeKind;
			}
			set
			{
				if (_dateTimeKind != value)
				{
					_dateTimeKind = value;
					OnPropertyChanged("DateTimeKind");
				}
			}
		}
		#endregion

		#region interface
		/// <summary>
		/// Returns true if specified date is not weekend or holiday.
		/// </summary>
		/// <param name="day">The <see cref="DateTime"/> value to check.</param>
		/// <returns>True if specified date is not weekend or holiday; false - otherwise.</returns>
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal bool IsWorkingDay(DateTime day)
		{
            day = day.Date;
            if (!WorkDays.Contains(day.DayOfWeek) && !_exceptions.Contains(day))
			{
				return false;
			}
            if (_holidays.Contains(day))
			{
				return false;
			}
			return true;
		}

		internal DateTime ToDataSourceTime(DateTime source)
		{
			if (_dateTimeKind == DateTimeKind.Utc)
			{
				if (source.Kind == DateTimeKind.Unspecified)
				{
					DateTime.SpecifyKind(source, DateTimeKind.Local);
				}
				return source.ToUniversalTime();
			}
			return source;
		}

		internal DateTime ToLocalTime(DateTime source)
		{
			DateTime result = source;
			if (source.Kind == DateTimeKind.Unspecified)
			{
				if (_dateTimeKind == DateTimeKind.Utc)
				{
					result = DateTime.SpecifyKind(source, DateTimeKind.Utc);
				}
				else
				{
					result = DateTime.SpecifyKind(source, DateTimeKind.Local);
				}
			}
			return result.ToLocalTime();
		}

		/// <summary>
		/// Copies properties from the specified <see cref="CalendarInfo"/> object to this one. 
		/// </summary>
		/// <param name="info">The <see cref="CalendarInfo"/> instance to copy.</param>
		internal void CopyFrom(CalendarInfo info)
		{
			_weekStart = (int)info.WeekStart;
			_timeScale = info.TimeScale;
			_dateTimeKind = info.DateTimeKind;
			WorkDays.Clear();
			WorkDays.AddRange(info.WorkDays);
			_startDayTime = info.StartDayTime;
			_endDayTime = info.EndDayTime;
			_firstDate = info.FirstDate;
			Holidays.Clear();
			Holidays.AddRange(info.Holidays);
			WeekendExceptions.Clear();
			WeekendExceptions.AddRange(info.WeekendExceptions);

			_dateFormat = info.DateFormatString;
			_timeFormat = info.TimeFormatString;

			SetLastDate(info.LastDate);
			_cultureInfo = info.CultureInfo;
			OnPropertyChanged("");
		}
		#endregion

		#region static methods
		/// <summary>
		/// Returns the culture-specific full name of the specified day of the week
		/// based on the current UI culture.
		/// </summary>
		/// <param name="day"></param>
		/// <returns></returns>
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetDayOfWeekName(DayOfWeek day)
		{
			return GetDayOfWeekName(day, Culture);
		}
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetDayOfWeekName(DayOfWeek day, CultureInfo culture)
		{
			return culture.DateTimeFormat.GetDayName(day);
		}

		/// <summary>
		/// Returns the culture-specific full name of the specified month based on the 
		/// current UI culture.
		/// </summary>
		/// <param name="month"></param>
		/// <returns></returns>
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetMonthName(int month)
		{
			return GetMonthName(month, Culture);
		}
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetMonthName(int month, CultureInfo culture)
		{
			return culture.DateTimeFormat.GetMonthName(month);
		}

#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetGenitiveMonthName(int month, CultureInfo culture)
		{
			return culture.DateTimeFormat.MonthGenitiveNames[month - 1];
		}
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal string GetGenitiveMonthName(int month)
		{
			return GetGenitiveMonthName(month, CultureInfo);
		}

		/// <summary>
		/// Returns the culture-specific full name of the specified instance based on the 
		/// current UI culture.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetInstanceName(WeekOfMonthEnum instance)
		{
#if END_USER_LOCALIZATION
			CultureInfo culture = Strings.UICulture;
#elif WINFX
			CultureInfo culture = C1.WPF.Localization.C1Localizer.DefaultCulture;
#elif ASP_NET_2            
            CultureInfo culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
#else
			CultureInfo culture = C1Localizer.CurrentUICulture;            
#endif
			return GetInstanceName(instance, culture);
		}

#if (!SILVERLIGHT)
        [Obfuscation(Exclude = true)]
#endif
        internal static string GetInstanceName(WeekOfMonthEnum instance, CultureInfo culture)
		{
			string cultureLanguageName = culture.TwoLetterISOLanguageName;
			string defaultValue = "";
			string result = "";
			switch (instance)
			{
				case WeekOfMonthEnum.First:
					defaultValue = "first";
					break;
				case WeekOfMonthEnum.Second:
					defaultValue = "second";
					break;
				case WeekOfMonthEnum.Third:
					defaultValue = "third";
					break;
				case WeekOfMonthEnum.Fourth:
					defaultValue = "fourth";
					break;
				case WeekOfMonthEnum.Last:
					defaultValue = "last";
					break;
				default:
					return "";
			}

			switch (instance)
			{
#if END_USER_LOCALIZATION
				case WeekOfMonthEnum.First:
					result = Strings.C1Schedule.Strings.Item("First", culture);
					break;
				case WeekOfMonthEnum.Second:
					result = Strings.C1Schedule.Strings.Item("Second", culture);
					break;
				case WeekOfMonthEnum.Third:
					result = Strings.C1Schedule.Strings.Item("Third", culture);
					break;
				case WeekOfMonthEnum.Fourth:
					result = Strings.C1Schedule.Strings.Item("Fourth", culture);
					break;
				case WeekOfMonthEnum.Last:
					result = Strings.C1Schedule.Strings.Item("Last", culture);
					break;
#elif WINFX
				case WeekOfMonthEnum.First:
					result = C1.WPF.Localization.C1Localizer.GetString("MiscStrings", "First", defaultValue, culture);
					break;
				case WeekOfMonthEnum.Second:
					result = C1.WPF.Localization.C1Localizer.GetString("MiscStrings", "Second", defaultValue, culture);
					break;
				case WeekOfMonthEnum.Third:
					result = C1.WPF.Localization.C1Localizer.GetString("MiscStrings", "Third", defaultValue, culture);
					break;
				case WeekOfMonthEnum.Fourth:
					result = C1.WPF.Localization.C1Localizer.GetString("MiscStrings", "Fourth", defaultValue, culture);
					break;
				case WeekOfMonthEnum.Last:
					result = C1.WPF.Localization.C1Localizer.GetString("MiscStrings", "Last", defaultValue, culture);
					break;
#elif SILVERLIGHT
                case WeekOfMonthEnum.First:
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.First;
                    break;
                case WeekOfMonthEnum.Second:
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Second;
                    break;
                case WeekOfMonthEnum.Third:
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Third;
                    break;
                case WeekOfMonthEnum.Fourth:
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Fourth;
                    break;
                case WeekOfMonthEnum.Last:
                    result = C1.Silverlight.Schedule.Resources.C1_Schedule_MiscStrings.Last;
                    break;
#else
				default:
					result = C1Localizer.GetString(defaultValue);
					break;
#endif
			}
            if (result == "second" && cultureLanguageName == "fr")
            {
                // hint for French culture
                return result;
            }
			if (cultureLanguageName != "en" && cultureLanguageName != "ja" && result.Equals(defaultValue))
			{
				return ((int)instance).ToString();
			}
			return result;
		}

		/// <summary>
		/// Returns DateTime value with filled DateTimeKind property.
		/// DateTimeKind = DateTimeKind.Utc if string was saved using RFC1123Pattern. 
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		internal static DateTime ParseTime(string source)
		{
			DateTime time = DateTime.Parse(source, CultureInfo.InvariantCulture);
			return DateTime.SpecifyKind(time, DateTimeKind.Local);
		}
		#endregion
	}

	/// <summary>
	/// The <see cref="WorkDays"/> class is a list of working days for the single week.
	/// </summary>
	public class WorkDays : List<System.DayOfWeek>
	{
		internal CalendarInfo _info;
		internal WorkDays(CalendarInfo info)
		{
			_info = info;
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="WorkDays"/> class.
		/// </summary>
		public WorkDays()
		{
		}

		/// <summary>
		/// Creates the new <see cref="WorkDays"/> object.
		/// </summary>
		/// <param name="items">The array of <see cref="DayOfWeek"/> objects.</param>
		public void AddRange(System.DayOfWeek[] items)
		{
			Clear();
			foreach (System.DayOfWeek day in items)
			{
				Add(day);
			}
			Sort();
			if (_info != null)
			{
				_info.OnPropertyChanged("WorkDays");
			}
        }

#pragma warning disable 1591
        public override string ToString()
        {
            char separator = ',';
            DayOfWeekConverter converter = new DayOfWeekConverter();
            CultureInfo culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            StringBuilder builder = new StringBuilder();
            foreach (DayOfWeek day in this)
            {
                builder.Append(converter.ConvertTo(null, culture, day, typeof(string)));
                builder.Append(separator);
            }
            if (builder.Length > 0)
            {
                builder.Remove(builder.Length - 1, 1);
            }
            return builder.ToString();
        }
#pragma warning restore 1591
    }
}
