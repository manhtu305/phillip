﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using System.ComponentModel;

#if (!WINFX && !SILVERLIGHT)
using System.Drawing;
using System.Drawing.Drawing2D;
#else
using System.Windows.Media;
#endif

namespace C1.C1Schedule
{
	/// <summary>
	/// Specifies the different patterns available for brushes.
	/// </summary>
	public enum C1BrushStyleEnum
	{
		/// <summary>
		/// Represents transparent brush.
		/// </summary>
		Transparent = 0,
		/// <summary>
		/// Represents solid brush.
		/// </summary>
		Solid = 1,
		/// <summary>
		/// A pattern of lines on a diagonal from upper right to lower left.
		/// </summary>
		BackwardDiagonal = 2,
#if !SILVERLIGHT
		/// <summary>
		/// Specifies horizontal and vertical lines that cross.  
		/// </summary>
		Cross = 3,
		/// <summary>
		/// A pattern of crisscross diagonal lines. 
		/// </summary>
		DiagonalCross = 4,
#endif
		/// <summary>
		/// A pattern of lines on a diagonal from upper left to lower right. 
		/// </summary>
		ForwardDiagonal = 5,
		/// <summary>
		/// A pattern of horizontal lines. 
		/// </summary>
		Horizontal = 6,
		/// <summary>
		/// A pattern of vertical lines. 
		/// </summary>
		Vertical = 7,
#if !SILVERLIGHT
		/// <summary>
		/// Specifies horizontal lines that are composed of tildes. 
		/// </summary>
		Wave = 8,
		/// <summary>
		/// Specifies horizontal lines that are composed of zigzags. 
		/// </summary>
		ZigZag = 9
#endif
	}

	/// <summary>
	/// Wrapper class for brushes. Only for serialization/deserialization purposes.
	/// </summary>
#if (!SILVERLIGHT)
	[Serializable]
	[Obfuscation(Exclude = true, ApplyToMembers = true)]
#endif
	public class C1Brush
#if (!SILVERLIGHT)
 : ISerializable
#endif
	{
		#region fields
		private Color _backColor = PlatformIndependenceHelper.GetColor(0);
		private Color _foreColor = PlatformIndependenceHelper.GetColor(0);
		private C1BrushStyleEnum _style = C1BrushStyleEnum.Transparent;
		private Brush _brush = null;
		#endregion

		#region ** ctor
		/// <summary>
		/// Creates the new <see cref="C1Brush"/> object.
		/// </summary>
		public C1Brush()
		{
		}

#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
		/// <summary>
		/// Creates the new <see cref="C1Brush"/> object.
		/// </summary>
		/// <param name="brush"></param>
		internal C1Brush(System.Drawing.Brush brush)
		{
			InitFromBrush(brush);
		}
#endif

		/// <summary>
		/// Creates the new <see cref="C1Brush"/> object.
		/// </summary>
		/// <param name="foreColor">The foreground <see cref="Color"/> value.</param>
		/// <param name="backColor">The background <see cref="Color"/> value.</param>
		/// <param name="style">The <see cref="C1BrushStyleEnum"/> value.</param>
		public C1Brush(Color foreColor, Color backColor, C1BrushStyleEnum style)
		{
			_foreColor = foreColor;
			_backColor = backColor;
			_style = style;
		}

		/// <summary>
		/// Creates a new solid <see cref="C1Brush"/> object.
		/// </summary>
		/// <param name="backColor">The background <see cref="Color"/> value.</param>
		public C1Brush(Color backColor)
		{
			_backColor = backColor;
			_style = C1BrushStyleEnum.Solid;
		}
		#endregion

		#region ** object model
		/// <summary>
		/// 
		/// </summary>
		public Color ForeColor
		{
			get
			{
				return _foreColor;
			}
			set
			{
				_foreColor = value;
				OnPropertyChanged();
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public Color BackColor
		{
			get
			{
				return _backColor;
			}
			set
			{
				_backColor = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public C1BrushStyleEnum Style
		{
			get
			{
				return _style;
			}
			set
			{
				_style = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Brush Brush
		{
			get
			{
#if (WINFX || SILVERLIGHT)
				return GetWPFBrush();
#else
				return BrushInternal;
#endif
			}
		}
		#endregion

		#region ** build actual brushes
#if (!WINFX && !SILVERLIGHT)
		/// <summary>
		/// Gets or sets WinForms/WebForms specific brush object based on the C1Brush definition. 
		/// </summary>
		internal Brush BrushInternal
		{
			get
			{
				if (_brush == null)
				{
					switch (_style)
					{
						case C1BrushStyleEnum.Transparent:
							_brush = Brushes.Transparent;
							break;
						case C1BrushStyleEnum.BackwardDiagonal:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.BackwardDiagonal,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.Cross:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.Cross,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.DiagonalCross:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.DiagonalCross,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.ForwardDiagonal:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.ForwardDiagonal,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.Horizontal:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.Horizontal,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.Vertical:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.Vertical,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.Wave:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.Wave,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.ZigZag:
							_brush = new System.Drawing.Drawing2D.HatchBrush(HatchStyle.ZigZag,
										_foreColor, _backColor);
							break;
						case C1BrushStyleEnum.Solid:
						default:
							_brush = new SolidBrush(_backColor);
							break;
					}
				}
				return _brush;
			}
			set
			{
				InitFromBrush(value);
			}
		}

		private void InitFromBrush(Brush brush)
		{
			OnPropertyChanged();
			_brush = brush;
			_style = C1BrushStyleEnum.Solid;
            _foreColor = PlatformIndependenceHelper.GetColor(0);
            _backColor = PlatformIndependenceHelper.GetColor(0);
			if (brush.Equals(Brushes.Transparent))
			{
				_style = C1BrushStyleEnum.Transparent;
			}
			else if (brush is SolidBrush)
			{
				_backColor = ((SolidBrush)brush).Color;
			}
			else if (brush is System.Drawing.Drawing2D.HatchBrush)
			{
				_foreColor = ((System.Drawing.Drawing2D.HatchBrush)brush).ForegroundColor;
				_backColor = ((System.Drawing.Drawing2D.HatchBrush)brush).BackgroundColor;
				switch (((System.Drawing.Drawing2D.HatchBrush)brush).HatchStyle)
				{
					case HatchStyle.BackwardDiagonal:
						_style = C1BrushStyleEnum.BackwardDiagonal;
						break;
					case HatchStyle.Cross:
						_style = C1BrushStyleEnum.Cross;
						break;
					case HatchStyle.DiagonalCross:
						_style = C1BrushStyleEnum.DiagonalCross;
						break;
					case HatchStyle.ForwardDiagonal:
						_style = C1BrushStyleEnum.ForwardDiagonal;
						break;
					case HatchStyle.Horizontal:
						_style = C1BrushStyleEnum.Horizontal;
						break;
					case HatchStyle.Vertical:
						_style = C1BrushStyleEnum.Vertical;
						break;
					case HatchStyle.Wave:
						_style = C1BrushStyleEnum.Wave;
						break;
					case HatchStyle.ZigZag:
						_style = C1BrushStyleEnum.ZigZag;
						break;
					default:
						_style = C1BrushStyleEnum.ForwardDiagonal;
						break;
				}
			}
		}
#endif

#if (WINFX || SILVERLIGHT)
		/// <summary>
		/// Returns the WPF brush object based on C1Brush definition.
		/// </summary>
		/// <returns></returns>
		private Brush GetWPFBrush()
		{
			if (_brush == null)
			{
				switch (Style)
				{
					case C1BrushStyleEnum.Transparent:
						_brush = new SolidColorBrush(Colors.Transparent);
						break;
					case C1BrushStyleEnum.Solid:
						_brush = new SolidColorBrush(BackColor);
						break;
					case C1BrushStyleEnum.BackwardDiagonal:
						LinearGradientBrush br = new LinearGradientBrush();
						br.StartPoint = new System.Windows.Point(0, 0);
						br.EndPoint = new System.Windows.Point(7, 7);
						FillGradientBrush(ref br);
						_brush = br;
						break;
					case C1BrushStyleEnum.ForwardDiagonal:
						LinearGradientBrush br1 = new LinearGradientBrush();
						br1.StartPoint = new System.Windows.Point(0, 7);
						br1.EndPoint = new System.Windows.Point(7, 0);
						FillGradientBrush(ref br1);
						_brush = br1;
						break;
					case C1BrushStyleEnum.Vertical:
						LinearGradientBrush br2 = new LinearGradientBrush();
						br2.StartPoint = new System.Windows.Point(0, 1);
						br2.EndPoint = new System.Windows.Point(10, 1);
						FillGradientBrush(ref br2);
						_brush = br2;
						break;
					case C1BrushStyleEnum.Horizontal:
						LinearGradientBrush br3 = new LinearGradientBrush();
						br3.StartPoint = new System.Windows.Point(1, 0);
						br3.EndPoint = new System.Windows.Point(1, 12);
						FillGradientBrush(ref br3);
						_brush = br3;
						break;
#if (!SILVERLIGHT)
					default:
						_brush = GetDrawingBrush();
						break;
#endif
				}
#if (!SILVERLIGHT)
				if (_brush != null)
				{
					_brush.Freeze();
				}
#endif
			}
#if (SILVERLIGHT)
			Brush newBrush = _brush;
#else
			// return clone. In other case memory leaks occur (brush keep references to dead objects).
			Brush newBrush = _brush.CloneCurrentValue();
			newBrush.Freeze();
#endif
			return newBrush;
		}

		private void FillGradientBrush(ref LinearGradientBrush brush)
		{
			// Silverlight only has parameter-less ctors for GradientStopCollection
			// and for GradientStop.
			GradientStopCollection coll = new GradientStopCollection();
			GradientStop stop = new GradientStop();
			stop.Offset = 0.1;
			stop.Color = BackColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.4;
			stop.Color = BackColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.4;
			stop.Color = ForeColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.45;
			stop.Color = ForeColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.45;
			stop.Color = BackColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.9;
			stop.Color = BackColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.9;
			stop.Color = ForeColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.95;
			stop.Color = ForeColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 0.95;
			stop.Color = BackColor;
			coll.Add(stop);
			stop = new GradientStop();
			stop.Offset = 1;
			stop.Color = BackColor;
			coll.Add(stop);
			brush.GradientStops = coll;
			brush.SpreadMethod = GradientSpreadMethod.Repeat;
			brush.MappingMode = BrushMappingMode.Absolute;
		}

#if (!SILVERLIGHT)
		private DrawingBrush GetDrawingBrush()
		{
			const double fullLength = 6;
			const double halfLength = 3;
			DrawingBrush ret = new DrawingBrush();
			ret.ViewportUnits = BrushMappingMode.Absolute;
			ret.TileMode = TileMode.Tile;
			ret.Stretch = Stretch.None;
			switch (Style)
			{
				case C1BrushStyleEnum.Wave:
				case C1BrushStyleEnum.ZigZag:
					ret.Viewport = new System.Windows.Rect(0, 0, fullLength, halfLength);
					break;
				default:
					ret.Viewport = new System.Windows.Rect(0, 0, fullLength, fullLength);
					break;
			}
			DrawingGroup backGr = GetBackgroundDrawing(ret);
			DrawingGroup hatchGr = GetHatchDrawing(ret);
			DrawingGroup dgr = new DrawingGroup();
			dgr.Children.Add(backGr);
			dgr.Children.Add(hatchGr);
			ret.Drawing = dgr;
			return ret;
		}

		private DrawingGroup GetBackgroundDrawing(DrawingBrush forBrush)
		{
			DrawingGroup ret = new DrawingGroup();
			GeometryDrawing geomDr = new GeometryDrawing();
			geomDr.Brush = new SolidColorBrush(BackColor);
			RectangleGeometry rectGeom = new RectangleGeometry(forBrush.Viewport);
			geomDr.Geometry = rectGeom;
			ret.Children.Add(geomDr);
			return ret;
		}

		private DrawingGroup GetHatchDrawing(DrawingBrush forBrush)
		{
			DrawingGroup ret = new DrawingGroup();
			GeometryDrawing geomDr = new GeometryDrawing();
			geomDr.Pen = new Pen(new SolidColorBrush(ForeColor), 0.5);
			Geometry geom = null;
			switch (Style)
			{
				case C1BrushStyleEnum.Cross:
					{
						GeometryGroup gr = new GeometryGroup();
						gr.Children.Add(new LineGeometry(new System.Windows.Point(0, 1),
							new System.Windows.Point(forBrush.Viewport.Width, 1)));
						gr.Children.Add(new LineGeometry(new System.Windows.Point(1, 0),
							new System.Windows.Point(1, forBrush.Viewport.Height)));
						geom = gr;
					}
					break;
				case C1BrushStyleEnum.DiagonalCross:
					{
						GeometryGroup gr = new GeometryGroup();
						gr.Children.Add(new LineGeometry(new System.Windows.Point(0, 0),
							new System.Windows.Point(forBrush.Viewport.Width,
								forBrush.Viewport.Height)));
						gr.Children.Add(new LineGeometry(
							new System.Windows.Point(forBrush.Viewport.Width, 0),
							new System.Windows.Point(0, forBrush.Viewport.Height)));
						geom = gr;
					}
					break;
				default: //TBD: this is Wave and ZigZag 
					geom = new LineGeometry(
						new System.Windows.Point(0, 1),
						new System.Windows.Point(forBrush.Viewport.Width, 1));
					break;
			}
			geomDr.Geometry = geom;
			ret.Children.Add(geomDr);
			return ret;
		}
#endif
#endif
		private void OnPropertyChanged()
		{
#if (!WINFX && !SILVERLIGHT)
			if (_brush != null)
			{
				_brush.Dispose();
			}
#endif
			_brush = null;
		}
		#endregion

#if (!SILVERLIGHT)
		#region ISerializable
		// A method called when serializing.
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("style", _style, typeof(C1BrushStyleEnum));
			if (_style != C1BrushStyleEnum.Transparent)
			{
				info.AddValue("backColor", GetARGBstring(_backColor));
				if (_style != C1BrushStyleEnum.Solid)
				{
					info.AddValue("foreColor", GetARGBstring(_foreColor));
				}
			}
		}

		/// <summary>
		/// Special constructor for deserialization.
		/// </summary>
		/// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/>.</param>
		/// <param name="context">The context information.</param>
		[SecurityPermissionAttribute(SecurityAction.LinkDemand,
		Flags = SecurityPermissionFlag.SerializationFormatter)]
		protected C1Brush(SerializationInfo info, StreamingContext context)
		{
			_style = (C1BrushStyleEnum)info.GetValue("style", typeof(C1BrushStyleEnum));
			if (_style != C1BrushStyleEnum.Transparent)
			{
				string str = info.GetString("backColor");
				_backColor = PlatformIndependenceHelper.ParseARGBString(str);

				if (_style != C1BrushStyleEnum.Solid)
				{
					str = info.GetString("foreColor");
					_foreColor = PlatformIndependenceHelper.ParseARGBString(str);
				}
			}
			OnPropertyChanged();
		}
		#endregion
#endif

		#region overrides
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
			return Brush.Equals(obj);
#else
			return base.Equals(obj);
#endif
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
#if (!ASP_NET_2 && !WINFX && !SILVERLIGHT)
			return Brush.GetHashCode();
#else
			return base.GetHashCode();
#endif
		}
		#endregion

		#region static helpers
		/// <summary>
		/// Returns string representation of color in ARGB format.
		/// </summary>
		/// <param name="color">The <see cref="C1.C1Schedule.BaseObject.Color"/>value.</param>
		/// <returns>The string representation of color in ARGB format</returns>
		public static string GetARGBstring(Color color)
		{
			return color.A.ToString() + ", " +
					color.R.ToString() + ", " +
					color.G.ToString() + ", " +
					color.B.ToString();
		}
		#endregion
	}

}
