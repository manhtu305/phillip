﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using C1.Web.Wijmo.Controls.Localization;
using System.Web;
using System.Reflection;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1EventsCalendar
{

	/// <summary>
	/// The C1EventsCalendar control is a fully functional schedule that 
	/// allows users to add, edit, and manage their appointments.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1EventsCalendar.C1EventsCalendarDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1EventsCalendar.C1EventsCalendarDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1EventsCalendar.C1EventsCalendarDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1EventsCalendar.C1EventsCalendarDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1EventsCalendar runat=\"server\"></{0}:C1EventsCalendar>")]
	[ToolboxBitmap(typeof(C1EventsCalendar), "C1EventsCalendar.png")]
	[ParseChildren(true)]
	[LicenseProvider()]
	public partial class C1EventsCalendar : C1TargetControlBase, INamingContainer,
		IPostBackDataHandler, IPostBackEventHandler, IUrlResolutionService, IC1Serializable
	{

		#region ** fields

		internal bool _productLicensed = false;
		private bool _shouldNag;
		private ITemplate _eventDialogTemplate;
		private HtmlGenericControl _eventDialogTemplateContainer;
		private ITemplate _eventTemplate;
		private HtmlGenericControl _eventTemplateContainer;

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ReportViewer"/> class.
		/// </summary>
		public C1EventsCalendar()
			: base("div")
		{
			VerifyLicense();
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.AddAttributesToRender(writer);
			if (IsDesignMode)
			{
				writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.DetermineDesignPixelWidth() + "px");
				writer.AddStyleAttribute(HtmlTextWriterStyle.Height, this.DetermineDesignPixelHeight() + "px");
			}
			else
			{
				if (!this.Width.IsEmpty)
				{
					writer.AddStyleAttribute(HtmlTextWriterStyle.Width, this.Width.ToString());
				}
				if (!this.Height.IsEmpty)
				{
					writer.AddStyleAttribute(HtmlTextWriterStyle.Height, this.Height.ToString());
				}
			}

			string cssClass = string.IsNullOrEmpty(this.CssClass) ? "" : (this.CssClass + " ");
			if (!IsDesignMode)
			{
				cssClass += String.Format(" {0} ", Utils.GetHiddenClass());
			}
			else
			{
				cssClass += string.Format("c1-c1eventscalendar wijmo-wijeventscalendar wijmo-wijev ui-widget ui-helper-reset ui-state-default{0}",
					"");
			}
#if GRAPECITY
				cssClass += " gc-c1eventscalender";
#endif
			writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement("adjustOptions");
		}
		#endregion

		#region ** licensing

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1EventsCalendar), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region ** IC1Serializable

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1EventsCalendarSerializer sz = new C1EventsCalendarSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1EventsCalendarSerializer sz = new C1EventsCalendarSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1EventsCalendarSerializer sz = new C1EventsCalendarSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1EventsCalendarSerializer sz = new C1EventsCalendarSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}

		#endregion

		#region ** IPostBackDataHandler interface implementation

		/// <summary>
		/// When implemented by a class, processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>
		/// true if the server control's state changes as a result of the postback; otherwise, false.
		/// </returns>
		public bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
            Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			return this.LoadClientData(data);
		}

		/// <summary>
		/// When implemented by a class, signals the server control to notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		public void RaisePostDataChangedEvent()
		{

		}

		internal bool LoadClientData(Hashtable data)
		{
			bool isViewStateChanged = false;
			object o = o = data["selectedDate"];
			if (o != null)
			{
				if (this.SelectedDate != (DateTime)o)
				{
					isViewStateChanged = true;
					this.SelectedDate = (DateTime)o;
					//_raiseSelectedDateChanged = true;
				}
			}
			var views = data["views"] as ArrayList;
			if (views != null)
			{
				LoadViewsFromJson(views);
			}
			return isViewStateChanged;
		}

		internal void LoadViewsFromJson(ArrayList views)
		{
			Views.Clear();
			for (int i = 0; i < views.Count; i++)
			{
				View view = new View();
				var viewType = views[i] as string;
				if (viewType != null)
				{
					view.Type = (C1EventsCalendarViewType)Enum.Parse(typeof(C1EventsCalendarViewType), viewType, true);
					Views.Add(view);
					continue;
				}
				var ht = views[i] as Hashtable;
				if (ht != null)
				{
					view.Type = C1EventsCalendarViewType.Custom;
					if (ht["name"] != null)
					{
						view.Name = Convert.ToString(ht["name"]);
					}
					if (ht["unit"] != null)
					{
						view.Unit = (C1EventsCalendarCustomViewUnit)Enum.Parse(typeof(C1EventsCalendarCustomViewUnit), Convert.ToString(ht["unit"]), true);
					}
					if (ht["count"] != null)
					{
						view.Count = Convert.ToInt32(ht["count"]);
					}
					Views.Add(view);
				}
			}
		}

		#endregion

		#region ** IPostBackEventHandler interface implementations

		/// <summary>
		/// Process an event
		/// raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument">A System.String that represents an optional event argument to be passed to
		/// the event handler.</param>
		public void RaisePostBackEvent(string eventArgument)
		{

		}

		#endregion

		#region ** IUrlResolutionService

		/// <summary>
		/// Resolves the client URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <returns></returns>
		string IUrlResolutionService.ResolveClientUrl(string url)
		{
			return ResolveClientUrl(url);
		}

		#endregion

		#region ** CreateChildControls override

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for
		/// posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if (EventDialogTemplate != null)
			{
				Controls.Add(EventDialogTemplateContainer);
				EventDialogTemplate.InstantiateIn(EventDialogTemplateContainer);
			}
			if (EventTemplate != null)
			{
				Controls.Add(EventTemplateContainer);
				EventTemplate.InstantiateIn(EventTemplateContainer);
			}
			base.CreateChildControls();
		}

		#endregion

		#region template

		/// <summary>
		/// Gets or sets the Event Dialog template used to display the content of the Event Dialog.
		/// </summary>
		[Browsable(false)]
		[DefaultValue(null)]
		[C1Description("C1Dialog.EventDialogTemplate")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		[Layout(LayoutType.Behavior)]
		[Json(false)]
		public ITemplate EventDialogTemplate
		{
			get
			{
				return _eventDialogTemplate;
			}
			set
			{
				_eventDialogTemplate = value;
			}
		}

		/// <summary>
		/// Gets or sets the Event template used to display the content of the Event.
		/// </summary>
		[Browsable(false)]
		[DefaultValue(null)]
		[C1Description("C1Dialog.EventTemplate")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		[Layout(LayoutType.Behavior)]
		[Json(false)]
		public ITemplate EventTemplate
		{
			get
			{
				return _eventTemplate;
			}
			set
			{
				_eventTemplate = value;
			}
		}

		private HtmlGenericControl EventDialogTemplateContainer
		{
			get
			{
				if (_eventDialogTemplateContainer == null)
				{
					_eventDialogTemplateContainer = new HtmlGenericControl("div");
					_eventDialogTemplateContainer.Style["display"] = "none";
					_eventDialogTemplateContainer.ID = "EventDialogTemplateContainer";
				}
				return _eventDialogTemplateContainer;
			}
		}

		private HtmlGenericControl EventTemplateContainer
		{
			get
			{
				if (_eventTemplateContainer == null)
				{
					_eventTemplateContainer = new HtmlGenericControl("div");
					_eventTemplateContainer.Style["display"] = "none";
					_eventTemplateContainer.ID = "EventTemplateContainer";
				}
				return _eventTemplateContainer;
			}
		}
		#endregion

		#region ** private implementation

		private int DetermineDesignPixelHeight()
		{
			if (this.Height.IsEmpty || this.Height.Type == System.Web.UI.WebControls.UnitType.Percentage)
			{
				return 600;
			}
			return (int)this.Height.Value;
		}

		private int DetermineDesignPixelWidth()
		{
			if (this.Width.IsEmpty || this.Width.Type == System.Web.UI.WebControls.UnitType.Percentage)
			{
				return 800;
			}
			return (int)this.Width.Value;
		}

        //private void RenderDesignTimeContent(HtmlTextWriter writer)
        //{
        //    base.RenderContents(writer);
        //    //writer.Write("Design time preview is not available for the C1EventsCalendar control.");
        //}

		internal string ResolvePhysicalPath(string url)
		{
			return C1TargetControlHelper.ResolvePhysicalPath(this, url);
		}

        internal string ResolvePhysicalPath(System.Web.UI.Control control, string url)
        {
            return C1TargetControlHelper.ResolvePhysicalPath(control, url);
        }

		#endregion

	}
}
