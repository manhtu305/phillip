﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Drawing.Design;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{

	/// <summary>
	/// Allows binding to DataSource and mapping DataSource's fields to Event's properties.
	/// </summary>
	[PersistenceMode(PersistenceMode.InnerProperty),
	TypeConverter(typeof(ExpandableObjectConverter))]
	[DefaultProperty("DataSourceID")]
	[Serializable]
	public class C1EventStorage : IDataSourceViewSchemaAccessor, IStateManager
	{

		#region ** fields

		C1EventStorageMappings _Mappings;
		[NonSerialized]
		private StateBag viewState;
		private bool isTrackingViewState;

		#endregion

		#region ** properties
		
		/// <summary>
		/// Use the Mappings property to bind the event object properties to the appropriate fields in the datasource specified by DataSourceID property.
		/// </summary>
		[C1Category("Data")]
		[C1Description("C1EventsCalendar.DataStorage.C1EventStorage.Mappings", "Use the Mappings property to bind the event object properties to the appropriate fields in the datasource specified by DataSourceID property.")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public C1EventStorageMappings Mappings
		{
			get
			{
				if (_Mappings == null)
				{
					_Mappings = new C1EventStorageMappings(this);
				}
				return _Mappings;
			}
		}

		/// <summary>
		/// The ID of the control from which the events calendar control retrieves its list of data items.
		/// </summary>
		[C1CategoryAttribute("Data")]
		[C1Description("C1EventsCalendar.DataStorage.C1EventStorage.DataSourceID", "The ID of the control from which the events calendar control retrieves its list of data items.")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[Editor("C1.Web.UI.Design.C1DataSourceIDEditor", typeof(UITypeEditor))]
		public string DataSourceID
		{
			get
			{
				object o = ViewState["DataSourceID"];
				return ((o == null) ? "" : (string)o);
			}

			set
			{
				if (value == "(None)" || value == "<Create new data source...>")
					value = "";
				ViewState["DataSourceID"] = value;
			}
		}

		/// <summary>
		/// The specific data member in a multimember data source.
		/// </summary>
		[C1CategoryAttribute("Data")]
		[C1Description("C1EventsCalendar.DataStorage.C1EventStorage.DataMember", "The specific data member in a multimember data source.")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[Bindable(false)]
		[Editor("C1.Web.UI.Design.C1DataMemberEditor", typeof(UITypeEditor))]
		public string DataMember
		{
			get
			{
				object o = ViewState["DataMember"];
				return ((o == null) ? "" : (string)o);
			}

			set
			{
				ViewState["DataMember"] = value;
			}
		}

		#endregion

		#region ** IDataSourceViewSchemaAccessor interface implementation

		object IDataSourceViewSchemaAccessor.DataSourceViewSchema
		{
			get
			{
				return ViewState["IDataSourceViewSchemaAccessor.DataSourceViewSchema"];
			}
			set
			{
				ViewState["IDataSourceViewSchemaAccessor.DataSourceViewSchema"] = value;
			}
		}

		#endregion

		#region ** ViewState implementation

		/// <summary>
		/// ViewState
		/// </summary>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		internal StateBag ViewState
		{
			get
			{
				if (this.viewState == null)
				{
					this.viewState = new StateBag(false);
					if (this.isTrackingViewState)
					{
						((IStateManager)this.viewState).TrackViewState();
					}
				}
				return this.viewState;
			}
		}

		bool IsTrackingViewState
		{
			get
			{
				return this.isTrackingViewState;
			}
		}

		bool IStateManager.IsTrackingViewState
		{
			get
			{
				return this.isTrackingViewState;
			}
		}

		void IStateManager.TrackViewState()
		{
			try
			{
				this.isTrackingViewState = true;
				if (this.viewState != null)
				{
					((IStateManager)this.viewState).TrackViewState();
				}
			}
			catch (Exception)
			{
			}
		}



		object IStateManager.SaveViewState()
		{
			try
			{
				object[] stateObj = new object[1];
				if ((this.viewState != null))
					stateObj[0] = ((IStateManager)this.viewState).SaveViewState();
				return stateObj;
			}
			catch (Exception)
			{
				return new object[1];
			}
		}

		void IStateManager.LoadViewState(object state)
		{
			try
			{
				if (state != null)
				{
					object[] stateObj = (object[])state;
					int len = stateObj.Length;

					if (len > 0 && stateObj[0] != null)
						((IStateManager)this.ViewState).LoadViewState(stateObj[0]);
				}
			}
			catch (Exception)
			{
			}
		}

		#endregion
	}
}
