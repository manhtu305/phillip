﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Collections;
using System.Xml.Serialization;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{
	/// <summary>
	/// Allows import/export events data to/from XML.
	/// </summary>
	[Serializable]
	public class C1EventsCalendarXmlExchanger
	{

		#region static
		// reader and writer settings can be safely shared between many XmlReaders and Writers
		internal static XmlWriterSettings _writerSettings = new XmlWriterSettings();
		internal static XmlReaderSettings _readerSettings = new XmlReaderSettings();

		// Simplified version of XmlReader.ReadString(). 
		// Silverlight doesn't have XmlReader.ReadString() method.
		public static string ReadStringValue(XmlReader reader)
		{
			if (reader.ReadState != ReadState.Interactive)
			{
				return string.Empty;
			}
			if (reader.NodeType == XmlNodeType.Element)
			{
				if (reader.IsEmptyElement || !reader.Read() || reader.NodeType == XmlNodeType.EndElement)
				{
					return string.Empty;
				}
			}
			string str = string.Empty;
			if (reader.NodeType == XmlNodeType.Text)
			{
				str = reader.Value;
				reader.Read();
			}
			return str;
		}

		static C1EventsCalendarXmlExchanger()
		{
			_writerSettings.OmitXmlDeclaration = true;
#if !SILVERLIGHT
			_readerSettings.ProhibitDtd = true;
#endif
			_readerSettings.IgnoreWhitespace = true;
			_readerSettings.IgnoreComments = true;
			_readerSettings.IgnoreProcessingInstructions = true;
		}

		#endregion

		#region fields

		//protected C1ScheduleStorage _storage;

		//protected List<Category> _categories;
		//protected List<Label> _labels;
		//protected List<Resource> _resources;
		//protected List<Status> _statuses;
		//protected List<Contact> _contacts;
		protected IList<Event> _Events = new List<Event>();
		protected IList<object> _Calendars = new List<object>();

		#endregion

		#region ** constructor
		public C1EventsCalendarXmlExchanger()
		{

		}

		#endregion

		#region ** properties


		public IList<Event> Events
		{
			get
			{
				return _Events;
			}
			set
			{
				_Events = value;
			}
		}

		public IList<object> Calendars
		{
			get
			{
				return _Calendars;
			}
			set
			{
				_Calendars = value;
			}
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Exports the scheduler's data to a file. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export the scheduler's data to. </param>
		public void Export(string path)
		{
			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				Export(stream);
			}
		}

		/// <summary>
		/// Exports the scheduler's data to a stream. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the scheduler's data will be exported.</param>
		public void Export(Stream stream)
		{
			//_Events = _storage.AppointmentStorage.Appointments;
			ExportInternal(stream);
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Exports the appointments's data to a file. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export. </param>
		/// <param name="appointments">A <see cref="IList{Appointment}"/> object.</param>
		public void Export(string path, IList<Event> appointments)
		{
			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				Export(stream, appointments);
			}
		}
#endif

		/// <summary>
		/// Exports the events's data to a stream. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the appointments's data will be exported.</param>
		/// <param name="appointments">A <see cref="IList{Appointment}"/> object.</param>
		public void Export(Stream stream, IList<Event> appointments)
		{
			_Events = appointments;
			ExportInternal(stream);
		}

#if (!SILVERLIGHT)
		/// <summary>
		/// Imports data into the scheduler from a file.
		/// </summary>
		/// <param name="path">A <see cref="String"/> value containing the full path 
		/// (including the file name and extension) to a file which contains 
		/// the data to be imported into the scheduler.</param>
		public void Import(string path)
		{
			using (FileStream stream = new FileStream(path, FileMode.Open))
			{
				Import(stream);
			}
		}
#endif

		/// <summary>
		/// Imports the scheduler's data from a stream.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		public void Import(Stream stream)
		{
			_Events = new List<Event>();
			_Calendars = new List<Object>();
			ImportInternal(stream);
		}

		#endregion

		#region overrides
		/// <summary>
		/// Exports the Events's data to a stream in the XML format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the Events's data will be exported.</param>
		protected virtual void ExportInternal(Stream stream)
		{
			using (XmlWriter writer = XmlWriter.Create(stream, _writerSettings))
			{
				WriteHeader(writer);
				if (_Events.Count > 0)
				{
					writer.WriteStartElement("Appointments");
					foreach (Event app in _Events)
					{
						app.ToXml(writer);
					}
					writer.WriteEndElement();
				}
				WriteFooter(writer);
			}
		}

		/// <summary>
		/// Imports the scheduler's data from a stream whose data is in the XML format.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the scheduler. </param>
		protected virtual void ImportInternal(Stream stream)
		{
			using (XmlReader reader = XmlReader.Create(stream, _readerSettings))
			{
				if (!reader.Read() || (reader.Name != "C1Schedule" && reader.Name != "C1EventsCalendar"))
				{
#if END_USER_LOCALIZATION
					throw new ApplicationException(C1.Win.C1Schedule.Localization.Strings.C1Schedule.Exceptions.Item("XmlImportError", _storage.Info.CultureInfo));
#elif WINFX
					throw new ApplicationException(C1.WPF.Localization.C1Localizer.GetString("Exceptions", "XmlImportError", "Cannot find C1Schedule node.", _storage.Info.CultureInfo));
#elif SILVERLIGHT
					throw new Exception(C1.Silverlight.Schedule.Resources.C1_Schedule_Exceptions.XmlImportError);
#else
					throw new ApplicationException(C1Localizer.GetString("Cannot find C1Schedule node."));
#endif
				}

				while (reader.Read() && reader.NodeType == XmlNodeType.Element)
				{
					using (XmlReader reader1 = reader.ReadSubtree())
					{
						switch (reader.Name)
						{
							/*case "Categories":
								ReadCollection<Category>(reader1, _categories);
								break;
							case "Labels":
								ReadCollection<Label>(reader1, _labels);
								break;
							case "Resources":
								ReadCollection<Resource>(reader1, _resources);
								break;
							case "Statuses":
								ReadCollection<Status>(reader1, _statuses);
								break;
							case "Contacts":
								ReadCollection<Contact>(reader1, _contacts);
								break;*/
							case "Calendars":
								//ReadEventCollection(reader1);
								break;
							case "Appointments":
								ReadEventCollection(reader1);
								break;
							case "Events":
								ReadEventCollection(reader1);
								break;
						}
					}
				}
			}
		}


		private void ReadEventCollection(XmlReader reader)
		{
			_Events.Clear();
			reader.Read();
			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				using (XmlReader reader1 = reader.ReadSubtree())
				{
					Event app = new Event();
					//app.ParentCollection = _storage.EventStorage.Events;
					app.FromXml(reader1);
					_addWithSort(app);
				}
			}
		}

		private void _addWithSort(Event app)
		{
			for (int i = 0; i < _Events.Count; i++)
			{
				if (_Events[i].Start > app.Start)
				{
					_Events.Insert(i, app);
					return;
				}
				else if (_Events[i].Start == app.Start)
				{
					if (_Events[i].Subject.CompareTo(app.Subject) == 1)
					{
						_Events.Insert(i, app);
						return;
					}
				}
			}
			_Events.Add(app);
		}
		#endregion

		#region private stuff
		private void WriteHeader(XmlWriter writer)
		{
			writer.WriteStartElement("C1Schedule");
			writer.WriteAttributeString("version", Assembly.GetExecutingAssembly().FullName);
			/*
			if (_categories.Count > 0)
			{
				writer.WriteStartElement("Categories");
				foreach (Category cat in _categories)
				{
					cat.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if (_labels.Count > 0)
			{
				writer.WriteStartElement("Labels");
				foreach (Label lab in _labels)
				{
					lab.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if (_resources.Count > 0)
			{
				writer.WriteStartElement("Resources");
				foreach (Resource res in _resources)
				{
					res.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if (_statuses.Count > 0)
			{
				writer.WriteStartElement("Statuses");
				foreach (Status st in _statuses)
				{
					st.ToXml(writer);
				}
				writer.WriteEndElement();
			}
			if (_contacts.Count > 0)
			{
				writer.WriteStartElement("Contacts");
				foreach (Contact con in _contacts)
				{
					con.ToXml(writer);
				}
				writer.WriteEndElement();
			}*/
		}

		private static void WriteFooter(XmlWriter writer)
		{
			writer.WriteEndElement();
			writer.Flush();
		}
		#endregion

	}

    #region ** events calendar data objects


	/// <summary>
	/// The <see cref="Event"/> object is a meeting, a one-time event, 
	/// or a recurring event or meeting in the C1EventsCalendar. 
	/// The <see cref="Event"/> class includes properties 
	/// that specify meeting details such as the location and time.
	/// </summary>
	[Serializable]
	public class Event
	{

		#region ** fields

		private string _id;
		private string _calendar;
		private string _tag;
		private string _location;
		public DateTime _start;
		public DateTime _end;
		public string _subject;
		public string _description;
		public string _color;
		public bool _allDayEvent;
		private string _recurrenceState;
		private string _parentRecurrenceId;
		private RecurrencePattern _recurrencePattern;
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="Event"/> class.
		/// </summary>
		public Event()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Event"/> class.
		/// </summary>
		public Event(Hashtable ht)
		{
			this.Data = ht;
		}

		#endregion

		#region ** properties

		/// <summary>
		/// The unique event id (this field generated automatically).
		/// </summary>
		/// <value>The calendar.</value>
		[WidgetOption()]
		[WidgetOptionName("id")]
		public string Id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		/// <summary>
		/// Calendar id to which the event belongs.
		/// </summary>
		/// <value>The calendar.</value>
		[WidgetOption()]
		[WidgetOptionName("calendar")]
		public string Calendar
		{
			get
			{
				return _calendar;
			}
			set
			{
				_calendar = value;
			}
		}

		/// <summary>
		/// Event title.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("subject")]
		public string Subject
		{
			get
			{
				return _subject;
			}
			set
			{
				_subject = value;
			}
		}

		/// <summary>
		/// Event location.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("location")]
		public string Location
		{
			get
			{
				return _location;
			}
			set
			{
				_location = value;
			}
		}

		/// <summary>
		/// Start date/time.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("start")]
		public DateTime Start
		{
			get
			{
				return _start;
			}
			set
			{
				_start = value;
			}
		}

		/// <summary>
		/// End date/time.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("end")]
		public DateTime End
		{
			get
			{
				return _end;
			}
			set
			{
				_end = value;
			}
		}

		/// <summary>
		/// Event description.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("description")]
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		/// <summary>
		/// Event color.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("color")]
		public string Color
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
			}
		}

		/// <summary>
		/// indicates all day event.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("allday")]
		public bool Allday
		{
			get
			{
				return _allDayEvent;
			}
			set
			{
				_allDayEvent = value;
			}
		}

		/// <summary>
		/// The tag property can be used to store any user-defined information
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("tag")]
		public string Tag
		{
			get
			{
				return _tag;
			}
			set
			{
				_tag = value;
			}
		}

		/// <summary>
		/// id of the event object that 
		///				defines the recurrence criteria for this event object. 
		///				If an event is recurring (see isRecurring)
		///				it represents an occurrence in the series that is 
		///				started and defined by a specific pattern event. 
		///				Use the getPatern method in order to obtain the pattern 
		///				of the current event. A pattern event can be recognized by its 
		///				recurrenceState field set to the "master" value. 
		///				The recurrence information defined by the pattern event
		///				can be accessed  via the recurrencePattern field of the event. 
		///				If this event is a not member of a recurrence, 
		///				or is itself a root event, this field will be null. 
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("parentRecurrenceId")]
		public string ParentRecurrenceId
		{
			get
			{
				return _parentRecurrenceId;
			}
			set
			{
				_parentRecurrenceId = value;
			}
		}

		/// <summary>
		/// Indicates the recurrence state of the event. 
		///				(possible values are "notRecurring"(or null), "master", "occurrence", 
		///				"exception", "removed")
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("recurrenceState")]
		public string RecurrenceState
		{
			get
			{
				return _recurrenceState;
			}
			set
			{
				_recurrenceState = value;
			}
		}

		/// <summary>
		/// Represents the recurrence attributes 
		///				of an event. Only master events can have this field
		///				(recurrenceState is "master")
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("recurrencePattern")]
		public RecurrencePattern RecurrencePattern
		{
			get
			{
				return _recurrencePattern;
			}
			set
			{
				_recurrencePattern = value;
			}
		}

		#endregion

		#region ** methods



		/// <summary>
		/// Creates an XML encoding of the event. 
		/// </summary>
		/// <param name="writer">The <see cref="System.Xml.XmlWriter"/> 
		/// that will receive the object data.</param>
		public virtual void ToXml(XmlWriter writer)
		{
			writer.WriteStartElement("Appointment");
			writer.WriteAttributeString("Id", Id.ToString());
			//writer.WriteAttributeString("Index", Index.ToString(CultureInfo.InvariantCulture));

			if (!String.IsNullOrEmpty(Description))
			{
				writer.WriteElementString("Body", Description);
			}
			if (!String.IsNullOrEmpty(Location))
			{
				writer.WriteElementString("Location", Location);
			}
			if (!String.IsNullOrEmpty(Subject))
			{
				writer.WriteElementString("Subject", Subject);
			}
			if (!String.IsNullOrEmpty(Color))
			{
				writer.WriteElementString("Color", Color);
			}
			if (!String.IsNullOrEmpty(Calendar))
			{
				writer.WriteElementString("Calendar", Calendar);
			}
			/*
			if (_DateTimeKind == DateTimeKind.Utc)
			{
				writer.WriteElementString("Start",
					info.ToDataSourceTime(_Start).ToString("r", CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteElementString("Start", _Start.ToString(CultureInfo.InvariantCulture));
			}*/
			writer.WriteElementString("Start", _start.ToString(CultureInfo.InvariantCulture));
			TimeSpan _duration = _end - _start;
			writer.WriteElementString("Duration", _duration.Ticks.ToString(CultureInfo.InvariantCulture));

			PropsToXml(writer);

			writer.WriteEndElement();
		}

		/// <summary>
		/// Reconstructs event from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader">An <see cref="System.Xml.XmlReader"/> 
		/// which contains the <see cref="Appointment"/> data.</param>
		/// <returns>True if event has been changed.</returns>
		public virtual bool FromXml(XmlReader reader)
		{
			bool dirty = false;
			try
			{
				if (reader.Read() && reader.IsStartElement() && reader.HasAttributes)
				{
					Id = reader.GetAttribute("Id");
					//SetId(new Guid(reader.GetAttribute("Id")));
					//SetIndex(int.Parse(reader.GetAttribute("Index"), CultureInfo.InvariantCulture));
				}
				else
				{
					return false;
				}

				string body = "";
				string subject = "";
				string location = "";
				string color = "";
				string calendar = "";

				while (reader.Read() && reader.NodeType == XmlNodeType.Element)
				{
					switch (reader.Name)
					{
						case "Body":
							body = XmlReadStringValue(reader);
							break;
						case "Color":
							color = XmlReadStringValue(reader);
							break;
						case "Calendar":
							calendar = XmlReadStringValue(reader);
							break;
						case "Location":
							location = XmlReadStringValue(reader);
							break;
						case "Subject":
							subject = XmlReadStringValue(reader);
							break;
						case "Start":
							DateTime time = DateTime.Parse(XmlReadStringValue(reader), CultureInfo.InvariantCulture);
							time = DateTime.SpecifyKind(time, DateTimeKind.Local);
							//DateTime time = CalendarInfo.ParseTime(XmlReadStringValue(reader));
							if (time != _start)
							{
								//_previousStart = _start = time;
								_start = time;
								dirty = true;
							}
							break;
						case "Duration":
							TimeSpan duration = TimeSpan.FromTicks(long.Parse(XmlReadStringValue(reader), CultureInfo.InvariantCulture));
							time = _start + duration;
							if (time != _end)
							{
								_end = time;
								dirty = true;
							}
							break;
						case "AppointmentProps":
							using (XmlReader reader1 = reader.ReadSubtree())
							{
								dirty = PropsFromXml(reader1) || dirty;
							}
							break;
					}
				}

				if (!body.Equals(_description))
				{
					_description = body;
					dirty = true;
				}
				if (!location.Equals(_location))
				{
					_location = location;
					dirty = true;
				}
				if (!subject.Equals(_subject))
				{
					_subject = subject;
					dirty = true;
				}
				if (!color.Equals(_color))
				{
					_color = color;
					dirty = true;
				}
				if (!calendar.Equals(_calendar))
				{
					_calendar = calendar;
					dirty = true;
				}

			}
			finally
			{
				/*
				if (dirty)
				{
					_changed = true;
				}*/
			}
			return dirty;
		}

		#endregion

		#region ** private implementation

		/// <summary>
		/// Creates an XML encoding of the event properties and returns it as string. 
		/// </summary>
		public string PropsToXml()
		{

			StringBuilder sb = new StringBuilder();
			using (TextWriter textWriter = new StringWriter(sb,
						System.Globalization.CultureInfo.InvariantCulture))
			using (XmlWriter writer = XmlWriter.Create(textWriter, C1EventsCalendarXmlExchanger._writerSettings))
			{
				PropsToXml(writer);
			}
			return sb.ToString();
		}

		private void PropsToXml(XmlWriter writer)
		{
			Event data = this;
			writer.WriteStartElement("AppointmentProps");
			writer.WriteElementString("AllDayEvent", data.Allday.ToString(CultureInfo.InvariantCulture));
			/*
			if (_autoResolvedWinner)
			{
				writer.WriteElementString("AutoResolvedWinner", _autoResolvedWinner.ToString(CultureInfo.InvariantCulture));
			}
			if (_private)
			{
				writer.WriteElementString("Private", _private.ToString(CultureInfo.InvariantCulture));
			}
			if (_importance != ImportanceEnum.Normal)
			{
				writer.WriteElementString("Importance", ((int)_importance).ToString(CultureInfo.InvariantCulture));
			}
			if (_sensitivity != SensitivityEnum.Normal)
			{
				writer.WriteElementString("Sensitivity", ((int)_sensitivity).ToString(CultureInfo.InvariantCulture));
			}
			 */
			if (_recurrenceState == "exception")
			{
				// fix for 26155
				writer.WriteElementString("RecurrenceState", _recurrenceState);
			}
			if (!string.IsNullOrEmpty(_parentRecurrenceId))
			{
				// fix for 26155
				writer.WriteElementString("ParentRecurrenceId", _parentRecurrenceId);
			}
			if (_recurrencePattern != null)
			{
				_recurrencePattern.ToXml(writer);
			}
			/*
			if (Reminder != null)
			{
				writer.WriteStartElement("Reminder");
				if (_reminderOverrideDefault)
				{
					writer.WriteElementString("OverrideDefault", _reminderOverrideDefault.ToString(CultureInfo.InvariantCulture));
				}
				if (!_reminderPlaySound)
				{
					writer.WriteElementString("PlaySound", _reminderPlaySound.ToString(CultureInfo.InvariantCulture));
				}
				if (!string.IsNullOrEmpty(_reminderSoundFile))
				{
					writer.WriteElementString("SoundFile", _reminderSoundFile);
				}
				if (_reminderTimeBeforeStart.TotalMinutes != 15)
				{
					writer.WriteElementString("TimeBeforeStart", _reminderTimeBeforeStart.Ticks.ToString(CultureInfo.InvariantCulture));
				}
				writer.WriteElementString("Set", ReminderSet.ToString(CultureInfo.InvariantCulture));
				writer.WriteEndElement();
			}

			if (_action != null)
			{
				Action.ToXml(writer);
			}

			writer.WriteStartElement("BusyStatus");
			if (_busyStatus == null)
			{
				writer.WriteAttributeString("Id", _busyStatusId.ToString());
				writer.WriteAttributeString("Index", _busyStatusIndex.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteAttributeString("Id", _busyStatus.Id.ToString());
				writer.WriteAttributeString("Index", _busyStatus.Index.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();

			writer.WriteStartElement("Label");
			if (_label == null)
			{
				writer.WriteAttributeString("Id", _labelId.ToString());
				writer.WriteAttributeString("Index", _labelIndex.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteAttributeString("Id", _label.Id.ToString());
				writer.WriteAttributeString("Index", _label.Index.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();

			if (Categories != null && Categories.Count > 0)
			{
				writer.WriteStartElement("Categories");
				writeList(writer, Categories);
				writer.WriteEndElement();
			}
			if (Resources != null && Resources.Count > 0)
			{
				writer.WriteStartElement("Resources");
				writeList(writer, Resources);
				writer.WriteEndElement();
			}
			if (Links != null && Links.Count > 0)
			{
				writer.WriteStartElement("Links");
				writeList(writer, Links);
				writer.WriteEndElement();
			}
			 */
			if (!String.IsNullOrEmpty(_tag))
			{
				writer.WriteElementString("CustomData", _tag);
			}
			writer.WriteEndElement();
		}

		internal static bool PropsFromXmlInternal(XmlReader reader, Hashtable data, Event refEventCanBeNull)
		{
			bool dirty = false;
			reader.Read();
			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				switch (reader.Name)
				{
					case "AllDayEvent":
						data["allday"] = bool.Parse(XmlReadStringValue(reader));
						if (refEventCanBeNull != null &&
							refEventCanBeNull.Allday != (bool)data["allday"])
						{
							refEventCanBeNull.Allday = (bool)data["allday"];
							dirty = true;
						}
						break;
					case "RecurrenceState":
						// fix for 26155
						data["recurrenceState"] = XmlReadStringValue(reader);
						if (refEventCanBeNull != null &&
							refEventCanBeNull.RecurrenceState != (string)data["recurrenceState"])
						{
							refEventCanBeNull.RecurrenceState = (string)data["recurrenceState"];
							dirty = true;
						}

						break;
					case "ParentRecurrenceId":
						// fix for 26155
						data["parentRecurrenceId"] = XmlReadStringValue(reader);
						if (refEventCanBeNull != null &&
							refEventCanBeNull.ParentRecurrenceId != (string)data["parentRecurrenceId"])
						{
							refEventCanBeNull.ParentRecurrenceId = (string)data["parentRecurrenceId"];
							dirty = true;
						}
						break;
					case "RecurrencePattern":
						RecurrencePattern recurrencePattern;
						if (refEventCanBeNull != null && refEventCanBeNull.RecurrencePattern != null)
						{
							recurrencePattern = refEventCanBeNull.RecurrencePattern;
						}
						else
						{
							recurrencePattern = new RecurrencePattern();
						}
						//_recurrencePattern.Info = _parentCollection.CalendarInfo;
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							bool recStateChanged = recurrencePattern.FromXml(reader1);
							dirty = recStateChanged ? recStateChanged : dirty;
						}
						if (refEventCanBeNull != null)
						{
							refEventCanBeNull.RecurrenceState = "master";
							refEventCanBeNull.RecurrencePattern = recurrencePattern;
						}
						data["recurrenceState"] = "master";
						data["recurrencePattern"] = recurrencePattern;
						break;
					case "CustomData":
						data["tag"] = XmlReadStringValue(reader);
						if (refEventCanBeNull != null &&
							refEventCanBeNull.Tag != (string)data["tag"])
						{
							refEventCanBeNull.Tag = (string)data["tag"];
							dirty = true;
						}
						break;
				}
			}
			return refEventCanBeNull == null || dirty;
		}
		/// <summary>
		/// Reconstructs appointment properties from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader"></param>
		internal bool PropsFromXml(XmlReader reader)
		{
			Hashtable data = new Hashtable();
			bool dirty = PropsFromXmlInternal(reader, data, this);


			return dirty;
		}

		private static string XmlReadStringValue(XmlReader reader)
		{
			if (reader.ReadState != ReadState.Interactive)
			{
				return string.Empty;
			}
			if (reader.NodeType == XmlNodeType.Element)
			{
				if (reader.IsEmptyElement || !reader.Read() || reader.NodeType == XmlNodeType.EndElement)
				{
					return string.Empty;
				}
			}
			string str = string.Empty;
			if (reader.NodeType == XmlNodeType.Text)
			{
				str = reader.Value;
				reader.Read();
			}
			return str;
		}

		#endregion


		/// <summary>
		/// Copies properties from event given by parameter ev.
		/// </summary>
		/// <param name="ev">The event.</param>
		public void CopyFrom(Event ev)
		{
			this.Id = ev.Id;
			this.Subject = ev.Subject;
			this.Location = ev.Location;
			this.Description = ev.Description;
			this.Start = ev.Start;
			this.End = ev.End;
			this.Color = ev.Color;
			this.Calendar = ev.Calendar;
			this.Allday = ev.Allday;
			this.Tag = ev.Tag;
			this.ParentRecurrenceId = ev.ParentRecurrenceId;
			this.RecurrencePattern = ev.RecurrencePattern;
			this.RecurrenceState = ev.RecurrenceState;
		}

		internal static Event _fromC1ScheduleAppointment(C1Schedule.Appointment c1Appt)
		{
			StringBuilder sb = new StringBuilder();
			using (System.IO.TextWriter tw = new System.IO.StringWriter(sb))
			using (XmlWriter writer = XmlWriter.Create(tw, C1EventsCalendarXmlExchanger._writerSettings))
			{
				c1Appt.ToXml(writer);
			}
			using (System.IO.TextReader tr = new System.IO.StringReader(sb.ToString()))
			using (XmlReader reader = XmlReader.Create(tr, C1EventsCalendarXmlExchanger._readerSettings))
			{
				Event ev = new Event();
				ev.FromXml(reader);
				return ev;
			}
		}

		internal static C1Schedule.Appointment _toC1ScheduleAppointment(Event ev)
		{
			StringBuilder sb = new StringBuilder();
			using (System.IO.TextWriter tw = new System.IO.StringWriter(sb))
			using (XmlWriter writer = XmlWriter.Create(tw, C1EventsCalendarXmlExchanger._writerSettings))
			{
				ev.ToXml(writer);
			}
			using (System.IO.TextReader tr = new System.IO.StringReader(sb.ToString()))
			using (XmlReader reader = XmlReader.Create(tr, C1EventsCalendarXmlExchanger._readerSettings))
			{
				C1Schedule.Appointment c1Appt = new C1Schedule.Appointment();
				c1Appt.FromXml(reader);
				return c1Appt;
			}
		}

		/// <summary>
		/// Gets or sets the raw event data.
		/// </summary>
		public Hashtable Data
		{
			get
			{
				Hashtable ht = new Hashtable();
				ht["id"] = this._id;
				ht["calendar"] = this._calendar;
				ht["subject"] = this._subject;
				ht["location"] = this._location;
				ht["start"] = this._start;
				ht["end"] = this._end;
				ht["description"] = this._description;
				ht["color"] = this._color;
				ht["allday"] = this._allDayEvent;
				ht["tag"] = this._tag;
				ht["parentRecurrenceId"] = this._parentRecurrenceId;
				ht["recurrenceState"] = this._recurrenceState;
				if (this._recurrencePattern != null)
				{
					ht["recurrencePattern"] = this._recurrencePattern.Data;
				}
				return ht;
			}
			set
			{
				Hashtable ht = value;
				if (ht.Contains("id"))
					this._id = (string)ht["id"];
				if (ht.Contains("calendar"))
					this._calendar = (string)ht["calendar"];
				if (ht.Contains("subject"))
					this._subject = (string)ht["subject"];
				if (ht.Contains("location"))
					this._location = (string)ht["location"];
				this._start = (DateTime)ht["start"];
				this._end = (DateTime)ht["end"];
				if (ht.Contains("description"))
					this._description = (string)ht["description"];
				if (ht.Contains("color"))
					this._color = (string)ht["color"];
				if (ht.Contains("allday"))
					this._allDayEvent = (bool)ht["allday"];
				if (ht.Contains("tag"))
					this._tag = (string)ht["tag"];
				if (ht.Contains("parentRecurrenceId"))
					this._parentRecurrenceId = (string)ht["parentRecurrenceId"];
				if (ht.Contains("recurrenceState"))
					this._recurrenceState = (string)ht["recurrenceState"];
				Hashtable ptrn = ht["recurrencePattern"] as Hashtable;
				if (ptrn != null)
				{
					this._recurrencePattern = new RecurrencePattern(this._id, ptrn);
				}
				else
				{
					RecurrencePattern ptrnObj = ht["recurrencePattern"] as RecurrencePattern;
					if (ptrnObj != null)
					{
						ptrnObj.SetParentRecurrenceIdInternal(this._id);
						this._recurrencePattern = ptrnObj;
					}
				}
			}
		}


	}


	/// <summary>
	/// Represents the recurrence attributes 
	///				of an event. Only master events can have this field
	///				(recurrenceState is "master").
	/// </summary>
	[Serializable]
	public class RecurrencePattern
	{
		#region ** fields

		private string _parentRecurrenceId;
		private string _recurrenceType;
		private int _interval;
		private DateTime _startTime;
		private DateTime _endTime;
		private DateTime _patternStartDate;
		private DateTime _patternEndDate;
		private int _occurrences;
		private string _instance;
		private string _dayOfWeekMask;
		private int _dayOfMonth;
		private int _monthOfYear;
		private bool _noEndDate = true;
		private List<string> _exceptions = new List<string>();
		private List<string> _removed = new List<string>();

		#endregion

		#region ** constructors

		public RecurrencePattern()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RecurrencePattern"/> class.
		/// </summary>
		/// <param name="parentRecurrenceId">The parent id.</param>
		/// <param name="rawData">The raw pattern data.</param>
		public RecurrencePattern(string parentRecurrenceId, Hashtable rawData)
		{
			this._parentRecurrenceId = parentRecurrenceId;
			this.Data = rawData;
		}

		internal void SetParentRecurrenceIdInternal(string parentRecurrenceId)
		{
			this._parentRecurrenceId = parentRecurrenceId;
		}
		#endregion

		#region ** properties

		/// <summary>
		/// Id of the event object
		///					which represents the master event for this 
		///					RecurrencePattern object.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("parentRecurrenceId")]
		public string ParentRecurrenceId
		{
			get
			{
				return _parentRecurrenceId;
			}
		}

		/// <summary>
		/// Determines the type of recurrence:
		///					daily - the recurring event reoccurs on a daily basis.
		///					workdays - the recurring event reoccurs every working day.
		///					monthly - the recurring event reoccurs on a monthly basis. 
		///					monthlyNth - the recurring event reoccurs every N months. 
		///					weekly - the recurring event reoccurs on a weekly basis.
		///					yearly - the recurring event reoccurs on an yearly basis.
		///					yearlyNth - the recurring event reoccurs every N years.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("recurrenceType")]
		public string RecurrenceType
		{
			get
			{
				return _recurrenceType;
			}
			set
			{
				_recurrenceType = value;
			}
		}

		/// <summary>
		/// Specifies the interval between occurrences 
		///					of the recurrence. 
		///					The interval field works in conjunction with 
		///					the "recurrenceType" field to determine the cycle of the recurrence. 
		///					The maximum allowable value is 99 for weekly patterns and 999 for daily patterns.
		///					The default value is 1.
		///					For example, if the recurrenceType is set 
		///					to daily, and the "interval" is set to 3, 
		///					the recurrence will occur every third day.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("interval")]
		public int Interval
		{
			get
			{
				return _interval;
			}
			set
			{
				_interval = value;
			}
		}

		/// <summary>
		/// Indicates the start time for the given 
		///	occurrence of the recurrence pattern. 
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("startTime")]
		public DateTime StartTime
		{
			get
			{
				return _startTime;
			}
			set
			{
				_startTime = value;
			}
		}
		/// <summary>
		/// Indicates the end time for the given 
		///	occurrence of the recurrence pattern.  
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("endTime")]
		public DateTime EndTime
		{
			get
			{
				return _endTime;
			}
			set
			{
				_endTime = value;
			}
		}

		/// <summary>
		/// Indicates the start date of the 
		///					recurrence pattern. 
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("patternStartDate")]
		public DateTime PatternStartDate
		{
			get
			{
				return _patternStartDate;
			}
			set
			{
				_patternStartDate = value;
			}
		}

		/// <summary>
		/// Indicates the end date of the 
		///					recurrence pattern.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("patternEndDate")]
		public DateTime PatternEndDate
		{
			get
			{
				return _patternEndDate;
			}
			set
			{
				_patternEndDate = value;
			}
		}

		/// <summary>
		/// The number of occurrences for the 
		///					recurrence pattern.	This field allows the definition of 
		///					a recurrence pattern that is only valid for the specified 
		///					number of subsequent occurrences. 
		///					For example, you can set this property to 10 for a formal 
		///					training  course that will be held on the next ten Thursday 
		///					evenings. The default value is 0. This field must be 
		///					coordinated with other fields when setting up a recurrence pattern. 
		///					If the "patternEndDate" field or the "occurrences" field 
		///					is set, the pattern is considered to be finite and the 
		///					"noEndDate" field is false. 
		///					If neither "patternEndDate" nor "occurrences" is set, 
		///					the pattern is considered infinite and "noEndDate" is true.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("occurrences")]
		public int Occurrences
		{
			get
			{
				return _occurrences;
			}
			set
			{
				_occurrences = value;
			}
		}

		/// <summary>
		///Determines the week in a month in which 
		///					the event will occur. This field is only valid for recurrences of 
		///					the "monthlyNth" and "yearlyNth" types and allows the definition 
		///					of a recurrence pattern that is only valid for the Nth occurrence, 
		///					such as "the 2nd Sunday in March" pattern. 
		///					The default value is "first".
		///					Possible values are:
		///						first - the recurring event will occur on the specified 
		///							day or days of the first week in the month. 
		///						second - The recurring event will occur on the specified 
		///							day or days of the second week in the month. 
		///						third = - The recurring event will occur on the specified 
		///							day or days of the third week in the month. 
		///						fourth - The recurring event will occur on the specified 
		///							day or days of the fourth week in the month.
		///						last - The recurring event will occur on the specified 
		///							day or days of the last week in the month. 
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("instance")]
		public string Instance
		{
			get
			{
				return _instance;
			}
			set
			{
				_instance = value;
			}
		}

		/// <summary>
		///	String, contains set of values representing the mask 
		///					for week days on which the recurring event occurs. 
		///					Monthly and yearly patterns are only valid for a single day. 
		///					The default value is "none".
		///					When the "RecurrenceType" field is set to "daily", 
		///					the "DayOfWeekMask" field can only be set to "everyDay"; 
		///					setting the field to any other value will result in an exception.
		///					When the "recurrenceType" field is set to 
		///					"workdays", the "dayOfWeekMask" field 
		///					can only be set to "workDays"; setting the field 
		///					to any other value will result in an exception.
		///					When the "recurrenceType" field is set to 
		///					"weekly", the "dayOfWeekMask" field 
		///					cannot be set to "none"; doing so will result 
		///					in an exception being thrown.
		///					When the recurrenceType" field is set to 
		///					"monthly" or "yearly" the "dayOfWeekMask" field is not applicable.
		///					Possible values are:
		///						none - no specific value.
		///						sunday - specifies Sunday.
		///						monday - Specifies Monday.
		///						tuesday - Specifies Tuesday.
		///						wednesday - Specifies Wednesday.
		///						thursday - Specifies Thursday.
		///						friday - Specifies Friday.
		///						saturday - Specifies Saturday.
		///						weekendDays - Specifies Saturday and Sunday.
		///						workDays - Specifies work days (all days except weekend).
		///						everyDay - Specifies every day of the week.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("dayOfWeekMask")]
		public string DayOfWeekMask
		{
			get
			{
				return _dayOfWeekMask;
			}
			set
			{
				_dayOfWeekMask = value;
			}
		}

		/// <summary>
		/// The number of the day in its respective month on which 
		///					each occurrence will occur. Applicable only when the recurrenceType 
		///					field is set to "monthly" or "yearly".
		///					The default value is 1.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("dayOfMonth")]
		public int DayOfMonth
		{
			get
			{
				return _dayOfMonth;
			}
			set
			{
				_dayOfMonth = value;
			}
		}

		/// <summary>
		/// Number, indicates which month of the year is valid 
		///					for the specified recurrence pattern. Can be a number from 1 to 12.
		///					This field is only valid for recurrence patterns whose recurrenceType" 
		///					field is set to "yearlyNth" or "yearly".
		///					The default value is 1.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("monthOfYear")]
		public int MonthOfYear
		{
			get
			{
				return _monthOfYear;
			}
			set
			{
				_monthOfYear = value;
			}
		}

		/// <summary>
		/// Indicates if the recurrence pattern is endless.
		///					The default value is True. This field must be coordinated with 
		///					other fields when setting up a recurrence pattern. If the patternEndDate field
		///					or the occurrences field is set, the pattern is considered 
		///					to be finite and the "noEndDate" field is false. 
		///					If neither patternEndDate nor occurrences is set, 
		///					the pattern is considered infinite and "noEndDate" is true.
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("noEndDate")]
		public bool NoEndDate
		{
			get
			{
				return _noEndDate;
			}
			set
			{
				_noEndDate = value;
			}
		}

		/// <summary>
		/// Holds the list of event object ids that 
		///	define the exceptions to that series of events. 
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("exceptions")]
		public List<string> Exceptions
		{
			get
			{
				return _exceptions;
			}
		}

		/// <summary>
		/// Holds the list of event object's ids
		///	removed from that series of events. 
		/// </summary>
		[WidgetOption()]
		[WidgetOptionName("removedOccurrences")]
		public List<string> RemovedOccurrences
		{
			get
			{
				return _removed;
			}
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Reconstructs recurrence pattern properties from an <see cref="XmlReader"/>. 
		/// </summary>
		/// <param name="reader"></param>
		/// <returns>True if pattern has been changed.</returns>
		internal bool FromXml(XmlReader reader)
		{
			bool dirty = false;

			_exceptions.Clear();
			_removed.Clear();

			reader.Read();

			string recurrenceType = "weekly";
			int interval = 1;
			int occurrences = 0;
			string dayOfWeekMask = "none";
			int dayOfMonth = 1;
			int monthOfYear = 1;
			int adjustedOccurrences = 0;
			bool noEndDate = true;
			string instance = "first";

			while (reader.Read() && reader.NodeType == XmlNodeType.Element)
			{
				DateTime time;
				switch (reader.Name)
				{
					case "RecurrenceType":
						recurrenceType = RecurrenceTypeEnumToString((RecurrenceTypeEnum)int.Parse(
								C1EventsCalendarXmlExchanger.ReadStringValue(reader),
														CultureInfo.InvariantCulture));
						break;
					case "Interval":
						interval = int.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
														CultureInfo.InvariantCulture);
						break;
					case "Occurrences":
						occurrences = int.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
														CultureInfo.InvariantCulture);
						break;
					case "DayOfWeekMask":
						dayOfWeekMask = C1EventsCalendarXmlExchanger.ReadStringValue(reader);
						break;
					case "DayOfMonth":
						dayOfMonth = int.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
														CultureInfo.InvariantCulture);
						break;
					case "MonthOfYear":
						monthOfYear = int.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
														CultureInfo.InvariantCulture);
						break;
					case "NoEndDate":
						noEndDate = bool.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader));
						break;
					case "Instance":
						instance = WeekOfMonthEnumToString((WeekOfMonthEnum)int.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
																		CultureInfo.InvariantCulture));
						break;
					case "Start":
						time = DateTime.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
																CultureInfo.InvariantCulture);
						time = DateTime.SpecifyKind(time, DateTimeKind.Local);
						if (time != _startTime)
						{
							_startTime = time;
							dirty = true;
						}
						break;
					case "Duration":
						TimeSpan duration = TimeSpan.FromTicks(long.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
																		CultureInfo.InvariantCulture));
						time = _startTime + duration;
						if (time != _endTime)
						{
							_endTime = time;
							dirty = true;
						}
						break;
					case "StartDate":
						time = DateTime.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
																		CultureInfo.InvariantCulture);
						if (time != _patternStartDate)
						{
							_patternStartDate = time;
							dirty = true;
						}
						break;
					case "EndDate":
						time = DateTime.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
																	CultureInfo.InvariantCulture);
						if (time != _patternEndDate)
						{
							_patternEndDate = time;
							dirty = true;
						}
						break;
					case "AdjustedOccurrences":
						adjustedOccurrences = int.Parse(C1EventsCalendarXmlExchanger.ReadStringValue(reader),
														CultureInfo.InvariantCulture);
						break;
					case "Exceptions":
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							reader1.Read();
							while (reader1.Read() && reader1.NodeType == XmlNodeType.Element)
							{
								Event app = new Event();
								//app.ParentCollection = _owner.ParentCollection;
								//app.ParentRecurrence = _owner;
								app.ParentRecurrenceId = this._parentRecurrenceId;
								app.RecurrenceState = "exception";
								using (XmlReader reader2 = reader1.ReadSubtree())
								{
									app.FromXml(reader2);
								}
								/*
								for (int j = Exceptions.Count - 1; j >= 0; j--)
								{
									if (Exceptions[j].Start.Date == app.Start.Date
										&& Exceptions[j].ParentRecurrence == _owner)
									{
										_owner.ParentCollection.Days.RemoveAppointment(Exceptions[j]);
										Exceptions.RemoveAt(j);
									}
								}*/
								_exceptions.Add(app.Id);
								//_owner.ParentCollection.Days.AddAppointment(app);
							}
						}
						break;
					case "Removed":
						using (XmlReader reader1 = reader.ReadSubtree())
						{
							reader1.Read();
							while (reader1.Read() && reader1.NodeType == XmlNodeType.Element)
							{
								Event app = new Event();
								app.RecurrenceState = "removed";
								app.ParentRecurrenceId = this._parentRecurrenceId;
								//app.ParentCollection = _owner.ParentCollection;
								//app.ParentRecurrence = _owner;
								using (XmlReader reader2 = reader1.ReadSubtree())
								{
									app.FromXml(reader2);
								}
								_removed.Add(app.Id);
								//_owner.ParentCollection.Days.AddAppointment(app);
							}
						}
						break;
				}
			}

			if (recurrenceType != _recurrenceType)
			{
				_recurrenceType = recurrenceType;
				dirty = true;
			}
			if (interval != _interval)
			{
				_interval = interval;
				dirty = true;
			}
			if (occurrences != _occurrences)
			{
				_occurrences = occurrences;
				dirty = true;
			}
			if (dayOfWeekMask != _dayOfWeekMask)
			{
				_dayOfWeekMask = dayOfWeekMask;
				dirty = true;
			}
			if (dayOfMonth != _dayOfMonth)
			{
				_dayOfMonth = dayOfMonth;
				dirty = true;
			}
			if (monthOfYear != _monthOfYear)
			{
				_monthOfYear = monthOfYear;
				dirty = true;
			}
			if (noEndDate != _noEndDate)
			{
				_noEndDate = noEndDate;
				dirty = true;
			}
			if (instance != _instance)
			{
				_instance = instance;
				dirty = true;
			}
			/*
			if (adjustedOccurrences != _adjustedOccurrences)
			{
				_adjustedOccurrences = adjustedOccurrences;
				dirty = true;
			}
			*/

			if (dirty)
			{
				/*			OnDeserialized(
			#if (!SILVERLIGHT)
			new System.Runtime.Serialization.StreamingContext()
			#endif
			);*/
			}
			return dirty;
		}

		private string WeekOfMonthEnumToString(WeekOfMonthEnum weekOfMonthEnum)
		{
			return weekOfMonthEnum.ToString().ToCamel();
		}

		/// <summary>
		/// Creates an XML encoding of the recurrence pattern properties. 
		/// </summary>
		/// <param name="writer"></param>
		internal void ToXml(XmlWriter writer)
		{
			writer.WriteStartElement("RecurrencePattern");

			if (_recurrenceType != "weekly")
			{
				writer.WriteElementString("RecurrenceType", ((int)StringToRecurrenceType(_recurrenceType)).ToString(CultureInfo.InvariantCulture));
			}
			if (_interval != 1)
			{
				writer.WriteElementString("Interval", _interval.ToString(CultureInfo.InvariantCulture));
			}
			if (_occurrences != 0)
			{
				writer.WriteElementString("Occurrences", _occurrences.ToString(CultureInfo.InvariantCulture));
			}
			if (_dayOfWeekMask != null && _dayOfWeekMask != "none")
			{
				writer.WriteElementString("DayOfWeekMask", _dayOfWeekMask);
			}
			if (_dayOfMonth != 1)
			{
				writer.WriteElementString("DayOfMonth", _dayOfMonth.ToString(CultureInfo.InvariantCulture));
			}
			if (_monthOfYear != 1)
			{
				writer.WriteElementString("MonthOfYear", _monthOfYear.ToString(CultureInfo.InvariantCulture));
			}
			if (!_noEndDate)
			{
				writer.WriteElementString("NoEndDate", _noEndDate.ToString(CultureInfo.InvariantCulture));
			}
			if (_instance != "first")
			{
				///				instance - String, determines the week in a month in which 
				///					the event will occur. This field is only valid for recurrences of 
				///					the "monthlyNth" and "yearlyNth" types and allows the definition 
				///					of a recurrence pattern that is only valid for the Nth occurrence, 
				///					such as "the 2nd Sunday in March" pattern. 
				///					The default value is "first".
				///					Possible values are:
				///						first - the recurring event will occur on the specified 
				///							day or days of the first week in the month. 
				///						second - The recurring event will occur on the specified 
				///							day or days of the second week in the month. 
				///						third = - The recurring event will occur on the specified 
				///							day or days of the third week in the month. 
				///						fourth - The recurring event will occur on the specified 
				///							day or days of the fourth week in the month.
				///						last - The recurring event will occur on the specified 
				///							day or days of the last week in the month. 
				if (_recurrenceType == "monthlyNth" || _recurrenceType == "yearlyNth")
				{
					writer.WriteElementString("Instance", ((int)StringToInstance(_instance)).ToString(CultureInfo.InvariantCulture));
				}
			}
			writer.WriteElementString("Start", _startTime.ToString(CultureInfo.InvariantCulture));
			TimeSpan duration = _endTime - _startTime;
			writer.WriteElementString("Duration", duration.Ticks.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("StartDate", _patternStartDate.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("EndDate", _patternEndDate.ToString(CultureInfo.InvariantCulture));

			/*if (_adjustedOccurrences > 0)
			{
				writer.WriteElementString("AdjustedOccurrences", _adjustedOccurrences.ToString(CultureInfo.InvariantCulture));
			}*/
			if (_exceptions.Count > 0)
			{
				writer.WriteStartElement("Exceptions");
				foreach (string appId in _exceptions)
				{
					Event app = new Event();
					app.Id = appId;
					//qq: C1EventsCalendar -> C1Schedule compatibility.
					app.ToXml(writer);
				}
				writer.WriteEndElement();
			}

			if (_removed.Count > 0)
			{
				writer.WriteStartElement("Removed");
				foreach (string appId in _removed)
				{
					Event app = new Event();
					app.Id = appId;
					//qq: C1EventsCalendar -> C1Schedule compatibility.
					app.ToXml(writer);
				}
				writer.WriteEndElement();
			}

			writer.WriteEndElement();
		}

		#endregion

		#region enums

		/// <summary>
		/// Determines the types of recurrent appointments.
		/// </summary>
		public enum RecurrenceTypeEnum
		{
			/// <summary>
			/// The recurring appointment reoccurs on a daily basis.
			/// </summary>
			Daily,
			/// <summary>
			/// The recurring appointment reoccurs every working day.
			/// </summary>
			Workdays,
			/// <summary>
			/// The recurring appointment reoccurs on a monthly basis. 
			/// </summary>
			Monthly,
			/// <summary>
			/// The recurring appointment reoccurs every N months. 
			/// </summary>
			MonthlyNth,
			/// <summary>
			/// The recurring appointment reoccurs on a weekly basis.
			/// </summary>
			Weekly,
			/// <summary>
			/// The recurring appointment reoccurs on an yearly basis.
			/// </summary>
			Yearly,
			/// <summary>
			/// The recurring appointment reoccurs every N years.
			/// </summary>
			YearlyNth
		}

		private string RecurrenceTypeEnumToString(RecurrenceTypeEnum recurrenceTypeEnum)
		{
			return recurrenceTypeEnum.ToString().ToCamel();
		}

		/// <summary>
		/// Determines the week in a month in which the appointment will occur.
		/// </summary>
		public enum WeekOfMonthEnum
		{
			/// <summary>
			/// The recurring appointment will occur on the specified 
			/// day or days of the first week in the month. 
			/// </summary>
			First = 1,
			/// <summary>
			/// The recurring appointment will occur on the specified 
			/// day or days of the second week in the month. 
			/// </summary>
			Second = 2,
			/// <summary>
			/// The recurring appointment will occur on the specified 
			/// day or days of the third week in the month. 
			/// </summary>
			Third = 3,
			/// <summary>
			/// The recurring appointment will occur on the specified 
			/// day or days of the fourth week in the month. 
			/// </summary>
			Fourth = 4,
			/// <summary>
			/// The recurring appointment will occur on the specified 
			/// day or days of the last week in the month. 
			/// </summary>
			Last = 5
		}

		private RecurrenceTypeEnum StringToRecurrenceType(string recurrenceType)
		{
			if (string.IsNullOrEmpty(recurrenceType))
			{
				return RecurrenceTypeEnum.Weekly;
			}
			switch (recurrenceType)
			{
				case "daily":
					return RecurrenceTypeEnum.Daily;
				case "monthly":
					return RecurrenceTypeEnum.Monthly;
				case "monthlyNth":
					return RecurrenceTypeEnum.MonthlyNth;
				case "weekly":
					return RecurrenceTypeEnum.Weekly;
				case "workdays":
					return RecurrenceTypeEnum.Workdays;
				case "yearly":
					return RecurrenceTypeEnum.Yearly;
				case "yearlyNth":
					return RecurrenceTypeEnum.YearlyNth;
				default:
					return RecurrenceTypeEnum.Weekly;
			}
		}

		private WeekOfMonthEnum StringToInstance(string instance)
		{
			if (string.IsNullOrEmpty(instance))
			{
				return WeekOfMonthEnum.First;
			}
			switch (instance.ToLowerInvariant())
			{
				case "first":
					return WeekOfMonthEnum.First;
				case "fourth":
					return WeekOfMonthEnum.Fourth;
				case "last":
					return WeekOfMonthEnum.Last;
				case "second":
					return WeekOfMonthEnum.Second;
				case "third":
					return WeekOfMonthEnum.Third;
				default:
					return WeekOfMonthEnum.First;
			}
		}

		#endregion

		/// <summary>
		/// Gets or sets the raw pattern data.
		/// </summary>
		/// <value>The data.</value>
		public Hashtable Data
		{
			get
			{
				Hashtable ht = new Hashtable();

				ht["recurrenceType"] = this._recurrenceType;

				ht["interval"] = this._interval;

				ht["startTime"] = this._startTime;

				ht["endTime"] = this._endTime;

				ht["patternStartDate"] = this._patternStartDate;

				ht["patternEndDate"] = this._patternEndDate;

				ht["occurrences"] = this._occurrences;

				ht["instance"] = this._instance;

				ht["dayOfWeekMask"] = this._dayOfWeekMask;
				ht["dayOfMonth"] = this._dayOfMonth;

				ht["monthOfYear"] = this._monthOfYear;

				ht["noEndDate"] = this._noEndDate;

				if (this._exceptions.Count > 0)
				{
					ht["exceptions"] = this._exceptions.ToArray<string>();
				}

				if (this._removed.Count > 0)
				{
					ht["removedOccurrences"] = this._removed.ToArray<string>();
				}
				return ht;
			}
			set
			{
				Hashtable ht = value;
				if (ht.Contains("recurrenceType"))
					this._recurrenceType = (string)ht["recurrenceType"];
				if (ht.Contains("interval"))
					this._interval = (int)ht["interval"];
				if (ht.Contains("startTime"))
					this._startTime = (DateTime)ht["startTime"];
				if (ht.Contains("endTime"))
					this._endTime = (DateTime)ht["endTime"];
				if (ht.Contains("patternStartDate"))
					this._patternStartDate = (DateTime)ht["patternStartDate"];
				if (ht.Contains("patternEndDate"))
					this._patternEndDate = (DateTime)ht["patternEndDate"];
				if (ht.Contains("occurrences"))
					this._occurrences = (int)ht["occurrences"];
				if (ht.Contains("instance"))
					this._instance = (string)ht["instance"];
				if (ht.Contains("dayOfWeekMask"))
					this._dayOfWeekMask = (string)ht["dayOfWeekMask"];
				if (ht.Contains("dayOfMonth"))
					this._dayOfMonth = (int)ht["dayOfMonth"];
				if (ht.Contains("monthOfYear"))
					this._monthOfYear = (int)ht["monthOfYear"];
				if (ht.Contains("noEndDate"))
					this._noEndDate = (bool)ht["noEndDate"];

				if (ht.Contains("exceptions"))
				{
					this._exceptions.Clear();
					ArrayList exceptions = _ensureArrayList(ht["exceptions"]);
					for (int i = 0; i < exceptions.Count; i++)
					{
						this._exceptions.Add((string)exceptions[i]);
					}
				}
				if (ht.Contains("removedOccurrences"))
				{
					this._removed.Clear();
					ArrayList removedOccurrences = _ensureArrayList(ht["removedOccurrences"]);
					for (int i = 0; i < removedOccurrences.Count; i++)
					{
						this._removed.Add((string)removedOccurrences[i]);
					}
				}
			}
		}

		private ArrayList _ensureArrayList(object arr)
		{
			// fix for 23782
			if (arr != null)
			{
				if (arr is string[])
				{
					return new ArrayList((string[])arr);
				}
				else if (arr is ArrayList)
				{
					return (ArrayList)arr;
				}
			}
			return new ArrayList();
		}



	}



	#endregion


	#region ** interfaces

	#endregion
}

#region ** docs

/*
block comments:
// util methods:
// format date/time:
// public methods
// _parseDateFromClass

// Event object fields:
		///     id - String, unique event id, this field generated automatically;
		///		calendar - String, calendar id to which the event belongs;
		///		subject - String, event title;
		///		location - String, event location;
		///		start - Date, start date/time;
		///		end - Date, end date/time;
		///		description - String, event description;
		///		color - String, event color;
		///		allday - Boolean, indicates all day event
		///		tag - String, this field can be used to store custom information.

		///		parentRecurrenceId - String, id of the event object that 
		///				defines the recurrence criteria for this event object. 
		///				If an event is recurring (see isRecurring)
		///				it represents an occurrence in the series that is 
		///				started and defined by a specific pattern event. 
		///				Use the getPatern method in order to obtain the pattern 
		///				of the current event. A pattern event can be recognized by its 
		///				recurrenceState field set to the "master" value. 
		///				The recurrence information defined by the pattern event
		///				can be accessed  via the recurrencePattern field of the event. 
		///				If this event is a not member of a recurrence, 
		///				or is itself a root event, this field will be null. 
		///		recurrenceState - String, indicates the recurrence state of the event. 
		///				(possible values are "notRecurring"(or null), "master", "occurrence", 
		///				"exception", "removed")
		///		recurrencePattern - Object, represents the recurrence attributes 
		///				of an event. Only master events can have this field
		///				(recurrenceState is "master")
		///				Object syntax:
		///				parentRecurrenceId - String, id of the event object
		///					which represents the master event for this 
		///					recurrencePattern object.
		///				recurrenceType - String, determines the type of recurrence:
		///					daily - the recurring event reoccurs on a daily basis.
		///					workdays - the recurring event reoccurs every working day.
		///					monthly - the recurring event reoccurs on a monthly basis. 
		///					monthlyNth - the recurring event reoccurs every N months. 
		///					weekly - the recurring event reoccurs on a weekly basis.
		///					yearly - the recurring event reoccurs on an yearly basis.
		///					yearlyNth - the recurring event reoccurs every N years.
		///				interval - Number, specifies the interval between occurrences 
		///					of the recurrence. 
		///					The interval field works in conjunction with 
		///					the "recurrenceType" field to determine the cycle of the recurrence. 
		///					The maximum allowable value is 99 for weekly patterns and 999 for daily patterns.
		///					The default value is 1.
		///					For example, if the recurrenceType is set 
		///					to daily, and the "interval" is set to 3, 
		///					the recurrence will occur every third day.
		///				startTime - Date, indicates the start time for the given 
		///					occurrence of the recurrence pattern. 
		///				endTime - Date, indicates the end time for the given 
		///					occurrence of the recurrence pattern. 
		///				patternStartDate - Date, indicates the start date of the 
		///					recurrence pattern.
		///				patternEndDate - Date, indicates the end date of the 
		///					recurrence pattern.
		///					This field is optional but must be coordinated 
		///					with other fields when setting up a recurrence pattern. 
		///					If this field or the "occurrences" field is set, 
		///					the pattern is considered to be finite, and the "noEndDate"
		///					field is false. 
		///					If neither "patternEndDate" nor "occurrences" is set, 
		///					the pattern is considered infinite and "noEndDate" is true. 
		///					The "interval" field must be set before 
		///					setting "patternEndDate".
		///				occurrences - Number, the number of occurrences for the 
		///					recurrence pattern.	This field allows the definition of 
		///					a recurrence pattern that is only valid for the specified 
		///					number of subsequent occurrences. 
		///					For example, you can set this property to 10 for a formal 
		///					training  course that will be held on the next ten Thursday 
		///					evenings. The default value is 0. This field must be 
		///					coordinated with other fields when setting up a recurrence pattern. 
		///					If the "patternEndDate" field or the "occurrences" field 
		///					is set, the pattern is considered to be finite and the 
		///					"noEndDate" field is false. 
		///					If neither "patternEndDate" nor "occurrences" is set, 
		///					the pattern is considered infinite and "noEndDate" is true.
		///				instance - String, determines the week in a month in which 
		///					the event will occur. This field is only valid for recurrences of 
		///					the "monthlyNth" and "yearlyNth" types and allows the definition 
		///					of a recurrence pattern that is only valid for the Nth occurrence, 
		///					such as "the 2nd Sunday in March" pattern. 
		///					The default value is "first".
		///					Possible values are:
		///						first - the recurring event will occur on the specified 
		///							day or days of the first week in the month. 
		///						second - The recurring event will occur on the specified 
		///							day or days of the second week in the month. 
		///						third = - The recurring event will occur on the specified 
		///							day or days of the third week in the month. 
		///						fourth - The recurring event will occur on the specified 
		///							day or days of the fourth week in the month.
		///						last - The recurring event will occur on the specified 
		///							day or days of the last week in the month. 
		///				dayOfWeekMask - String, contains set of values representing the mask 
		///					for week days on which the recurring event occurs. 
		///					Monthly and yearly patterns are only valid for a single day. 
		///					The default value is "none".
		///					When the "RecurrenceType" field is set to "daily", 
		///					the "DayOfWeekMask" field can only be set to "everyDay"; 
		///					setting the field to any other value will result in an exception.
		///					When the "recurrenceType" field is set to 
		///					"workdays", the "dayOfWeekMask" field 
		///					can only be set to "workDays"; setting the field 
		///					to any other value will result in an exception.
		///					When the "recurrenceType" field is set to 
		///					"weekly", the "dayOfWeekMask" field 
		///					cannot be set to "none"; doing so will result 
		///					in an exception being thrown.
		///					When the recurrenceType" field is set to 
		///					"monthly" or "yearly" the "dayOfWeekMask" field is not applicable.
		///					Possible values are:
		///						none - no specific value; the actual value is obtained from 
		///							the root event object. 
		///						sunday - specifies Sunday.
		///						monday - Specifies Monday.
		///						tuesday - Specifies Tuesday. 
		///						wednesday - Specifies Wednesday.
		///						thursday - Specifies Thursday.
		///						friday - Specifies Friday.
		///						saturday - Specifies Saturday.
		///						weekendDays - Specifies Saturday and Sunday (or what ever days according 
		///							to the settings of the C1EventsCalendar).
		///						workDays - Specifies work days (all days except weekend).
		///						everyDay - Specifies every day of the week.
		///				dayOfMonth - Number, the number of the day in its respective month on which 
		///					each occurrence will occur. Applicable only when the recurrenceType 
		///					field is set to "monthly" or "yearly".
		///					The default value is 1.
		///				monthOfYear - Number, indicates which month of the year is valid 
		///					for the specified recurrence pattern. Can be a number from 1 to 12.
		///					This field is only valid for recurrence patterns whose recurrenceType" 
		///					field is set to "yearlyNth" or "yearly".
		///					The default value is 1.
		///				noEndDate - Boolean, indicates if the recurrence pattern is endless.
		///					The default value is True. This field must be coordinated with 
		///					other fields when setting up a recurrence pattern. If the patternEndDate field
		///					or the occurrences field is set, the pattern is considered 
		///					to be finite and the "noEndDate" field is false. 
		///					If neither patternEndDate nor occurrences is set, 
		///					the pattern is considered infinite and "noEndDate" is true.
		///				exceptions - Array, holds the list of event objects that 
		///					define the exceptions to that series of events. 
		///					Event objects are added to the exceptions whenever a field 
		///					in the corresponding event object is altered.
		///				removedOccurrences - Array, holds the list of event objects 
		///					removed from that series of events. 





/// Calendar object fields:
		///     id - String, unique calendar id, this field generated automatically;
		///		name - String, calendar name;
		///		location - String, location field;
		///		description - String, calendar description;
		///		color - String, calendar color;
		///		tag - String, this field can be used to store custom information.
*/

#endregion