﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{

	/// <summary>
	/// C1CalendarStorage mappings configuration.
	/// </summary>
	[PersistenceMode(PersistenceMode.InnerProperty), TypeConverter(typeof(ExpandableObjectConverter))]
	[Serializable]
	public class C1CalendarStorageMappings : C1MappingsBase
	{

		#region ** fields

		C1CalendarStorage _c1CalendarStorage;
		private MappingInfo _name;
		private MappingInfo _location;
		private MappingInfo _description;
		private MappingInfo _color;
		private MappingInfo _tag;
		private MappingInfo _properties;

		/*
		/// Calendar object fields:
		///     id - String, unique calendar id, this field generated automatically;
		///		name - String, calendar name;
		///		location - String, location field;
		///		description - String, calendar description;
		///		color - String, calendar color;
		///		tag - String, this field can be used to store custom information.		 
		 */

		#endregion

		#region ** constructor

		internal C1CalendarStorageMappings(C1CalendarStorage c1CalendarStorage)
			: base(c1CalendarStorage.ViewState)
		{
			this._c1CalendarStorage = c1CalendarStorage;
			_name = Add("name", false, string.Empty);
			_location = Add("location", false, string.Empty);
			_description = Add("description", false, string.Empty);
			_color = Add("color", false, string.Empty);
			_tag = Add("tag", false, string.Empty);
			_properties = Add("properties", false, string.Empty);			
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Allows the name property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1CalendarStorageMappings.NameMapping", "Allows the name property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo NameMapping
		{
			get
			{

				return _name;

			}
		}

		/// <summary>
		/// Allows the location property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1CalendarStorageMappings.LocationMapping", "Allows the location property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo LocationMapping
		{
			get
			{

				return _location;

			}
		}

		/// <summary>
		/// Allows the description property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1CalendarStorageMappings.DescriptionMapping", "Allows the description property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo DescriptionMapping
		{
			get
			{

				return _description;

			}
		}


		/// <summary>
		/// Allows the color property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1CalendarStorageMappings.ColorMapping", "Allows the color property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo ColorMapping
		{
			get
			{

				return _color;

			}
		}

		/// <summary>
		/// Allows other calendar properties to be bound to appropriate field in the data source. 
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1CalendarStorageMappings.PropertiesMapping", "Allows other calendar properties to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo PropertiesMapping
		{
			get
			{
				return _properties;
			}
		}

		/// <summary>
		/// Allows the tag property to be bound to appropriate field in the data source (the tag property can be used to store any user-defined information).
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1CalendarStorageMappings.TagMapping", "Allows the tag property to be bound to appropriate field in the data source. (the tag property can be used to store any user-defined information)")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo TagMapping
		{
			get
			{
				return _tag;
			}
		}


		#endregion

	}
}
