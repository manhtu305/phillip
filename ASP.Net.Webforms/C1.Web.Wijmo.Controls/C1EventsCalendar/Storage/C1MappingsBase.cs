﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{



	/// <summary>
	/// Represents a collection of mappings for properties to the appropriate data fields. 
	/// Names of properties are keys in this collection.
	/// </summary>
	[Serializable]
	public class C1MappingsBase
	{
		#region ** fields
		[NonSerialized]
		StateBag _parentViewState;
		private MappingInfo _idMapping;
		protected KeyedCollection<string, MappingInfo> _mappings = new KeyedMappingInfoColleaction();

		#endregion
		
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1MappingsBase"/> class.
		/// </summary>
		public C1MappingsBase()
		{
			
			_parentViewState = new StateBag();
			_idMapping = Add("id", false, string.Empty);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MappingCollectionBase{T}"/> class.
		/// </summary>
		public C1MappingsBase(StateBag parentStateBag)
		{
			_parentViewState = parentStateBag;
			_idMapping = Add("id", false, string.Empty);
		}
		#endregion

		#region object model
		/// <summary>
		/// Allows the id property to be bound to appropriate field in the data source.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Category("Mapping"),
		C1Description("C1EventsCalendar.C1MappingsBase.Id", "Allows the id property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public MappingInfo IdMapping
		{
			get
			{
				return _idMapping;
			}
		}

		#region ** methods

		/// <summary>
		/// Finds the mapping info by mapping name (name of the data field).
		/// </summary>
		/// <param name="mappingName">Name of the mapping.</param>
		/// <returns></returns>
		public MappingInfo FindByMappingName(string mappingName)
		{
			for (int i = 0; i < _mappings.Count; i++)
			{
				if (_mappings[i].MappingName ==
						mappingName)
				/*
if (_mappings[i].MappingName.Replace("original_", "") == 
					mappingName.Replace("original_", ""))					 
				 */
				{
					return _mappings[i];
				}
			}
			return null;
		}


		/// <summary>
		/// Creates an array of MappingInfo objects.
		/// </summary>
		/// <returns></returns>
		public MappingInfo[] ToArray()
		{
			return this._mappings.ToArray<MappingInfo>();
		}
		#endregion

		#endregion

		#region internal methods
		internal MappingInfo Add(string propName, bool required, string defaulValue)
		{
			MappingInfo map = new MappingInfo(propName, required, defaulValue, this._parentViewState);
			_mappings.Add(map);
			return map;
		}


		#endregion

		#region design-time support


		/// <summary>
		/// Represents the method that handles the Disposed event of a component. 
		/// </summary>
		public event EventHandler Disposed;

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, 
		/// or resetting unmanaged resources. 
		/// </summary>
		public virtual void Dispose()
		{
			//There is nothing to clean.
			if (Disposed != null)
				Disposed(this, EventArgs.Empty);
		}
		#endregion



	}

	[Serializable]
	internal class KeyedMappingInfoColleaction : KeyedCollection<string, MappingInfo>
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="KeyedMappingInfoColleaction"/> class.
		/// </summary>
		public KeyedMappingInfoColleaction() : base() { }

		/// <summary>
		/// Gets the key for item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		protected override string GetKeyForItem(MappingInfo item)
		{
			return item.PropertyName;
		}
	}
}
