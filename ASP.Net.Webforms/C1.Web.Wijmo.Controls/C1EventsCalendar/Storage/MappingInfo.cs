﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{
	/// <summary>
	/// Information about mapping.
	/// </summary>
	[PersistenceMode(PersistenceMode.InnerProperty), TypeConverter(typeof(ExpandableObjectConverter))]
	[Serializable]
	public class MappingInfo
	{
		string _propName;
		bool _required;
		string _defaultValue;
		[NonSerialized]
		StateBag _stateBag;
		bool _isMapped;


		/// <summary>
		/// Initializes a new instance of the <see cref="MappingInfo"/> class.
		/// </summary>
		/// <param name="propName">Name of the prop.</param>
		/// <param name="required">if set to <c>true</c> [required].</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="stateBag">The state bag.</param>
		public MappingInfo(string propName, bool required, string defaultValue, StateBag stateBag)
		{
			if (string.IsNullOrEmpty(propName))
			{
				throw new Exception("Parameter propName can not be empty.");
			}
			this._propName = propName;
			this._required = required;
			this._defaultValue = defaultValue;
			this._stateBag = stateBag;
		}

		/// <summary>
		/// Name of the data field or property to be bound to this object.
		/// </summary>
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[C1Description("C1EventsCalendar.MappingInfo.MappingName", "Name of the data field or property to be bound to this object.")]
		[DefaultValue("")]
		public string MappingName
		{
			get
			{
				object obj = this._stateBag[this._propName + "MappingName"];
				return obj == null ? "" : (string)obj;
			}
			set
			{
				_isMapped = value.Length > 0;
				this._stateBag[this._propName + "MappingName"] = value;
			}
		}

		/// <summary>
		/// The default value which will be used when mapping is not mapped to a data field or data field value is empty.
		/// </summary>
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[C1Description("C1EventsCalendar.MappingInfo.DefaultValue", "The default value which will be used when mapping is not mapped to a data field or data field value is empty.")]
		public string DefaultValue
		{
			get
			{
				return _defaultValue;
			}
			set
			{
				_defaultValue = value;
			}
		}

		/// <summary>
		/// Name of the property which should be mapped. (read only)
		/// </summary>
		[C1Description("C1EventsCalendar.MappingInfo.PropertyName", "Name of the property which should be mapped. (read only)")]
		public string PropertyName
		{
			get
			{
				return this._propName;
			}
		}

		/// <summary>
		/// Indicates whether objects is mapped to the data field or property. (read only)
		/// </summary>
		[C1Description("C1EventsCalendar.MappingInfo.IsMapped")]
		public bool IsMapped
		{
			get
			{
				return this._isMapped;
			}
		}

		/// <summary>
		/// Indicates whether mapping for this property is required or optional. (read only)
		/// </summary>
		[C1Description("C1EventsCalendar.MappingInfo.Required")]
		public bool Required
		{
			get
			{
				return this._required;
			}
		}

		#region --- public override ( ToString() ) ---

		/// <summary>
		/// Returns a System.String that represents the current object.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			string s = "Not Mapped.";
			if (this.IsMapped)
				s = "Mapped.";
			return s;
		}

		#endregion

	}
}
