﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web;
using System.Collections;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Xml;
using System.Globalization;
using System.Text.RegularExpressions;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{

	/// <summary>
	/// The C1EventsCalendarStorage defines data storage configuration. 
	/// </summary>
	[PersistenceMode(PersistenceMode.InnerProperty), TypeConverter(typeof(ExpandableObjectConverter))]
	[Serializable]
	public class C1EventsCalendarStorage : IStateManager
	{
		#region ** fields

#if EXTENDER
		C1EventsCalendarExtender parentControl;
#else
		[NonSerialized]
		C1EventsCalendar parentControl;
#endif

		[NonSerialized]
		private StateBag viewState;
		private bool isTrackingViewState;

		private C1EventStorage _EventStorage;
		private C1CalendarStorage _CalendarStorage;

		C1EventsCalendarXmlExchanger _C1EventsCalendarXmlExchanger;
		protected C1EventCollection _Events;
		protected IList<object> _Calendars = new List<object>();
		private string _clientId;

		internal bool _readingData;
		internal bool _isUpdating;
		bool _initialized = false;

		List<Event> _pendingAddEvents = new List<Event>();
		List<Event> _pendingDeleteEvents = new List<Event>();


		#endregion

		#region ** constructor

		internal C1EventsCalendarStorage(Control parentControl)
			: this(parentControl.ClientID)
		{
#if EXTENDER
			this.parentControl = parentControl as C1EventsCalendarExtender;
#else
			this.parentControl = parentControl as C1EventsCalendar;
#endif
		}

		/// <summary>
		/// Initializes a new instance of <see cref="C1EventsCalendarStorage"/>
		/// </summary>
		/// <param name="clientID">The ID of the component which owns it.</param>
		public C1EventsCalendarStorage(string clientID)
		{
			_Events = new C1EventCollection(this);
			_C1EventsCalendarXmlExchanger = new C1EventsCalendarXmlExchanger();
			_clientId = clientID;
		}

		public C1EventsCalendarStorage()
		{
			_Events = new C1EventCollection(this);
			_C1EventsCalendarXmlExchanger = new C1EventsCalendarXmlExchanger();
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Gets the events collection.
		/// </summary>
		/// <value>The events.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1EventCollection Events
		{
			get
			{				
				this.Initialize();				
				return _Events;
			}
		}

		/// <summary>
		/// Gets the calendars collection.
		/// </summary>
		/// <value>The calendars.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]		
		public IList<object> Calendars
		{
			get
			{
				this.Initialize();
				return _Calendars;
			}
		}

		/// <summary>
		/// Gets or sets the visible calendars.
		/// </summary>
		/// <value>The visible calendars.</value>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		[TypeConverter(typeof(StringArrayConverter))]
		public string[] VisibleCalendars
		{
			get
			{
				object o = this.ViewState["VisibleCalendars"];
				if (o != null)
				{
					return (string[])o;
				}
				else
				{
					return parentControl != null ? parentControl.VisibleCalendars : null;
				}
			}
			set
			{
				this.ViewState["VisibleCalendars"] = value;
			}
		}

		/// <summary>
		/// The name of the xml file which will be used for unbound mode.
		/// </summary>
		/// <value>The data file.</value>
		[DefaultValue("c1evcaldata.xml")]
		[C1Description("C1EventsCalendarStorage.DataFile", "The name of the xml file which will be used for unbound mode.")]
		public string DataFile
		{
			get
			{
				return (string)this.ViewState["DataFile"] ?? "c1evcaldata.xml";
			}
			set
			{
				this.ViewState["DataFile"] = value;
				if (_initialized)
				{
					this.LoadData();
				}
			}
		}

		/// <summary>
		/// The EventStorage property defines data storage configuration for events..
		/// </summary>
		[C1Category("Storages")]
		[C1Description("C1EventsCalendarStorage.EventStorage", "The EventStorage property defines data storage configuration for events.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public C1EventStorage EventStorage
		{
			get
			{
				if (_EventStorage == null)
				{
					_EventStorage = new C1EventStorage();
					if (IsTrackingViewState)
					{
						((IStateManager)_EventStorage).TrackViewState();
					}
				}
				return _EventStorage;
			}
		}


		/// <summary>
		/// Gets the <see cref="WebStatusStorage"/> object.
		/// </summary>
		[C1Category("Storages")]
		[C1Description("C1EventsCalendarStorage.CalendarStorage", "The CalendarStorage property defines data storage configuration for calendars.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public C1CalendarStorage CalendarStorage
		{
			get
			{
				if (_CalendarStorage == null)
				{
					_CalendarStorage = new C1CalendarStorage();
					if (IsTrackingViewState)
						((IStateManager)_CalendarStorage).TrackViewState();
				}
				return _CalendarStorage;
			}
		}

		#endregion

		#region ** methods	

		/// <summary>
		/// Initialize data storage. 
		/// </summary>
		public void Initialize()
		{
			if (parentControl != null && !string.IsNullOrEmpty(parentControl.ClientID))
			{
				// fix for [21908] When you run Datasource.aspx it persists all data from Default.aspx’s calendar.
				_clientId = parentControl.ClientID;
			}
			if (!_initialized)
			{
				_initialized = true;
				this.LoadData();
			}
		}

		/// <summary>
		/// Prevents the events calendar storage from being updated until the EndUpdate method is called.
		/// </summary>
		public void BeginUpdate()
		{
			_isUpdating = true;
		}

		/// <summary>
		/// Unlocks the events calendar storage after a call to the BeginUpdate method call and causes an immediate update.
		/// </summary>
		public void EndUpdate()
		{
			if (_isUpdating)
			{
				_isUpdating = false;
				this.SaveData();
			}
		}

		/// <summary>
		/// Load events calendar data from data source or from xml file.
		/// </summary>
		public void LoadData()
		{
			if (HttpContext.Current == null)
			{
				// do not load data at design time
				return;
			}
			this._ReadData();
		}

		/// <summary>
		/// Save events data to data source or xml file depending on data storage settings.
		/// </summary>
		public void SaveData()
		{
			if (!_initialized)
			{
				throw new Exception("Data storage is not Initialized. Please, call Initialize first.");
			}
			if (_isUpdating)
			{
				return;
			}
			IDataSource ds1 = GetEventsDataSource();

			IDataSource ds2 = GetCalendarsDataSource();

			if (ds1 != null)
			{
				for (int i = 0; i < _pendingAddEvents.Count; i++)
				{
					AddEventToDataSource(_pendingAddEvents[i].Data);
				}
				_pendingAddEvents.Clear();
				for (int i = 0; i < _pendingDeleteEvents.Count; i++)
				{
					DeleteEventFromDataSource(_pendingDeleteEvents[i].Data);
				}
				_pendingDeleteEvents.Clear();
			}


			if (ds1 == null || ds2 == null)
			{
				_exportToXMLInternal(false, this.ResolvedDataFile);
			}
			// load updated data from xml or data source
			this.LoadData();
		}

		private IDataSource GetEventsDataSource()
		{
			if (!string.IsNullOrEmpty(this.EventStorage.DataSourceID))
			{
				return HttpContext.Current.Cache[_clientId +
								"_EventsDataSource"] as IDataSource;
			}
			return null;
		}

		private IDataSource GetCalendarsDataSource()
		{
			if (!string.IsNullOrEmpty(this.CalendarStorage.DataSourceID))
			{
				return HttpContext.Current.Cache[_clientId +
												"_CalendarsDataSource"] as IDataSource;
			}
			return null;
		}

		private void _exportToXMLInternal(bool forceWriteEvents, string path)
		{
			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				_exportToXMLInternal(forceWriteEvents, stream);
			}
		}

		private void _exportToXMLInternal(bool forceWriteEvents, Stream stream)
		{
			List<Event> events = new List<Event>();
			IDataSource ds1 = GetEventsDataSource();
			// only save events to xml when events data source is not present.
			if (ds1 == null || forceWriteEvents)
			{
				events.AddRange(_Events);
				events.AddRange(_pendingAddEvents);
				_pendingAddEvents.Clear();
				for (int i = 0; i < _pendingDeleteEvents.Count; i++)
				{
					RemoveEventFromList(events, _pendingDeleteEvents[i]);
				}
				_pendingDeleteEvents.Clear();
			}
			_C1EventsCalendarXmlExchanger.Events = events;
			_C1EventsCalendarXmlExchanger.Calendars = _Calendars;
			_C1EventsCalendarXmlExchanger.Export(stream);
		}

		/// <summary>
		/// Exports the events calendar's data to a file in the specified format. 
		/// </summary>
		/// <param name="path">A <see cref="String"/> containing the full path 
		/// (including the file name and extension) to export the events calendar's data to. </param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Export(string path, FileFormatEnum format)
		{
			using (FileStream stream = new FileStream(path, FileMode.Create))
			{
				Export(stream, format);
			}			
		}

		/// <summary>
		/// Imports data into the events calendar from a file of the specified format.
		/// </summary>
		/// <param name="path">A <see cref="String"/> value containing the full path 
		/// (including the file name and extension) to a file which contains 
		/// the data to be imported into the events calendar.</param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Import(string path, FileFormatEnum format)
		{

			using (FileStream stream = new FileStream(path, FileMode.Open))
			{
				Import(stream, format);
			}
		}

		/// <summary>
		/// Exports the events calendar's data to a stream in the specified format. 
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream into which the events calendar's data will be exported.</param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Export(Stream stream, FileFormatEnum format)
		{
			C1.C1Schedule.C1ScheduleStorage c1Storage = _createC1Storage();
			WriteEventsToC1Storage(c1Storage);
			CreateExchanger(format, c1Storage).Export(stream);			
		}

		private C1Schedule.C1ScheduleStorage _createC1Storage()
		{
			C1.C1Schedule.C1ScheduleStorage c1Storage = new C1.C1Schedule.C1ScheduleStorage();
			return c1Storage;
		}
		private void ReadEventsFromC1Storage(C1.C1Schedule.C1ScheduleStorage c1Storage)
		{
			try
			{
				_readingData = true;
				_Events.Clear();
				C1.C1Schedule.AppointmentCollection appts = c1Storage.AppointmentStorage.Appointments;
				for (int i = 0; i < appts.Count; i++)
				{
					Event ev = Event._fromC1ScheduleAppointment(appts[i]);
					_Events.Add(ev);
				}
			}
			finally
			{
				_readingData = false;
			}
		}
		private void WriteEventsToC1Storage(C1.C1Schedule.C1ScheduleStorage c1Storage)
		{
			C1.C1Schedule.AppointmentCollection appts = c1Storage.AppointmentStorage.Appointments;
			appts.Clear();
			Event[] events = _Events.ToArray<Event>();
			for (int i = 0; i < events.Length; i++)
			{				
				appts.Add(Event._toC1ScheduleAppointment(events[i]));
			}
		}

		/// <summary>
		/// Imports the events calendar's data from a stream whose data is in the specified format.
		/// </summary>
		/// <param name="stream">A <see cref="Stream"/> object which specifies 
		/// the stream that contains the data to import to the events calendar. </param>
		/// <param name="format">The <see cref="FileFormatEnum"/> value.</param>
		public void Import(Stream stream, FileFormatEnum format)
		{
			C1.C1Schedule.C1ScheduleStorage c1Storage = _createC1Storage();
			C1.C1Schedule.C1ScheduleExchanger ex = CreateExchanger(format, c1Storage);
			ex.Import(stream);
			ReadEventsFromC1Storage(c1Storage);
		}

		/// <summary>
		/// Adds the event.
		/// </summary>
		/// <param name="ev">The event.</param>
		/// <returns>Event object which will be added to data source or data file.</returns>
		public Event AddEvent(Event ev)
		{
			return AddEvent(ev.Data);
		}
		/// <summary>
		/// Adds the event.
		/// </summary>
		/// <param name="eventData">The raw event data.</param>
		/// <returns>Event object which will be added to data source or data file.</returns>
		public Event AddEvent(Hashtable eventData)
		{
			Event ev = new Event(eventData);
			if (string.IsNullOrEmpty(ev.Id))
			{
				ev.Id = Guid.NewGuid().ToString("D").ToUpperInvariant();
			}
			else
			{
				ev.Id = ev.Id.ToUpperInvariant();
			}

			if (this.FindEventByID(ev.Id) != null)
			{
				throw new Exception(String.Format( "Unable to add event, event with same id {0} already exists.", ev.Id));
			}
			_pendingAddEvents.Add(ev);
			if (!_isUpdating)
			{
				this.SaveData();
			}
			return ev;
		}

		/// <summary>
		/// Updates the event.
		/// </summary>
		/// <param name="ev">The event.</param>
		/// <returns></returns>
		public Event UpdateEvent(Event ev)
		{
			return UpdateEvent(ev.Data);
		}

		/// <summary>
		/// Updates the event.
		/// </summary>
		/// <param name="eventData">The raw event data.</param>
		/// <returns></returns>
		public Event UpdateEvent(Hashtable eventData)
		{
			try
			{
				IDataSource ds = GetEventsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					ParameterCollection commandParameters = _getCommandParameters("UpdateParameters", ds);
					Dictionary<string, object> newValues = GetItemValues(eventData,
						commandParameters, this.EventStorage.Mappings, false, true);
					Dictionary<string, object> keyValues = GetItemValues(eventData,
						commandParameters, this.EventStorage.Mappings, true, true);
					Dictionary<string, object> oldValues = null;					
					if (eventData.ContainsKey("prevData") && eventData["prevData"] is Hashtable)
					{
						oldValues = GetItemValues((Hashtable)eventData["prevData"],
						commandParameters, this.EventStorage.Mappings, false, true);
					}
					
					view.Update(keyValues, newValues, oldValues,
						new DataSourceViewOperationCallback(_genericDBActionCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
					return new Event(eventData);
				}
				else
				{
					Event evUpd = new Event(eventData);
					Event evTarget = this.FindEventByID(evUpd.Id);
					if (evTarget != null)
					{
						evTarget.CopyFrom(evUpd);
						this.SaveData();
						return evTarget;
					}
					else
					{
						throw new Exception(String.Format("Unable to update event, target event with id {0} not found.", evUpd.Id));
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Deletes the event.
		/// </summary>
		/// <param name="ev">The event.</param>
		public void DeleteEvent(Event ev)
		{
			DeleteEvent(ev.Data);
		}

		/// <summary>
		/// Deletes the event.
		/// </summary>
		/// <param name="eventData">The raw event data.</param>
		public void DeleteEvent(Hashtable eventData)
		{
			Event ev = new Event(eventData);
			if (this.FindEventByID(ev.Id) == null)
			{
				throw new Exception(String.Format("Unable to delete event, event with id {0} not found.", ev.Id));
			}
			_pendingDeleteEvents.Add(ev);
			if (!_isUpdating)
			{
				this.SaveData();
			}
		}

		/// <summary>
		/// Finds the event by its ID.
		/// </summary>
		/// <param name="id">The event id.</param>
		public Event FindEventByID(string id)
		{
			Event ev = null;
			for (int i = 0; i < _Events.Count; i++)
			{
				if (String.Compare(_Events[i].Id, id, true) == 0)
				{
					return _Events[i];
				}
			}
			return ev;
		}


		#region ** calendars store

		public Hashtable AddCalendar(Hashtable calData)
		{
			try
			{
				IDataSource ds = GetCalendarsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					ParameterCollection commandParameters = _getCommandParameters("InsertParameters", ds);
					Dictionary<string, object> values = GetItemValues(calData,
						commandParameters, this.CalendarStorage.Mappings, false, false);
					view.Insert(values,
						new DataSourceViewOperationCallback(_genericDBActionCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
					return calData;
				}
				else
				{
					throw new Exception("Unable to update calendar, data source not found.");
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public Hashtable UpdateCalendar(Hashtable calData)
		{
			try
			{
				IDataSource ds = GetCalendarsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					ParameterCollection commandParameters = _getCommandParameters("UpdateParameters", ds);
					Dictionary<string, object> newValues = GetItemValues(calData,
						commandParameters, this.CalendarStorage.Mappings, false, true);
					Dictionary<string, object> keyValues = GetItemValues(calData,
						commandParameters, this.CalendarStorage.Mappings, true, true);
					Dictionary<string, object> oldValues = null;
					view.Update(keyValues, newValues, oldValues,
						new DataSourceViewOperationCallback(_genericDBActionCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
					return calData;
				}
				else
				{
					throw new Exception("Unable to update calendar, data source not found.");
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void DeleteCalendar(Hashtable calData)
		{
			try
			{
				IDataSource ds = GetCalendarsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					ParameterCollection commandParameters = _getCommandParameters("DeleteParameters", ds);
					Dictionary<string, object> values = GetItemValues(calData,
						commandParameters, this.CalendarStorage.Mappings, false, false);
					Dictionary<string, object> keyValues = GetItemValues(calData,
						commandParameters, this.CalendarStorage.Mappings, true, false);
					view.Delete(keyValues, values,
						new DataSourceViewOperationCallback(_genericDBActionCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
				}
				else
				{
					throw new Exception("Unable to delete calendar, data source not found.");
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void SelectCalendarsCallback(IEnumerable data)
		{
			_Calendars = _readSelectData(data, this.CalendarStorage.Mappings).ToArray();
			_readingData = false;
		}

		#endregion

		#region ** private implementation

		private C1.C1Schedule.C1ScheduleExchanger CreateExchanger(FileFormatEnum format, C1.C1Schedule.C1ScheduleStorage c1Storage)
		{
			C1.C1Schedule.C1ScheduleExchanger ex = null;

			switch (format)
			{
				case FileFormatEnum.iCal:
					ex = new C1.C1Schedule.ICalExchanger(c1Storage);
					break;
				case FileFormatEnum.XML:
					ex = new C1.C1Schedule.XmlExchanger(c1Storage);
					break;
#if (!SILVERLIGHT)
				case FileFormatEnum.Binary:
					ex = new C1.C1Schedule.BinaryExchanger(c1Storage);
					break;
#endif
				default:
					throw new System.ArgumentException(C1Localizer.GetString("Format {0} is not supported."), format.ToString());
			}
			return ex;
		}

		#endregion

		private void RemoveEventFromList(List<Event> events, Event ev)
		{
			for (int i = 0; i < events.Count; i++)
			{
				if (events[i].Id == ev.Id)
				{
					events.Remove(events[i]);
					return;
				}
			}
			events.Remove(ev);
		}

		internal void AddEventToDataSource(Hashtable eventData)
		{
			Event evAdd = new Event(eventData);
			try
			{
				IDataSource ds = GetEventsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					ParameterCollection commandParameters = _getCommandParameters("InsertParameters", ds);
					Dictionary<string, object> values = GetItemValues(eventData,
						commandParameters, this.EventStorage.Mappings, false, false);
					view.Insert(values,
						new DataSourceViewOperationCallback(_genericDBActionCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
				}

			}
			catch (Exception ex)
			{
#if DEBUG
				System.Windows.Forms.MessageBox.Show("[debug] Unable add event: " + ex.Message + "\n" + ex.StackTrace);
#endif
				throw ex;
			}
		}

		internal void DeleteEventFromDataSource(Hashtable eventData)
		{
			try
			{
				IDataSource ds = GetEventsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					ParameterCollection commandParameters = _getCommandParameters("DeleteParameters", ds);
					Dictionary<string, object> values = GetItemValues(eventData,
						commandParameters, this.EventStorage.Mappings, false, false);
					Dictionary<string, object> keyValues = GetItemValues(eventData,
						commandParameters, this.EventStorage.Mappings, true, false);

					Dictionary<string, object> oldValues = null;
					if (eventData.ContainsKey("prevData") && eventData["prevData"] is Hashtable)
					{
						oldValues = GetItemValues((Hashtable)eventData["prevData"],
						commandParameters, this.EventStorage.Mappings, false, true);
					}

					view.Delete(keyValues, oldValues == null ? values : oldValues,
						new DataSourceViewOperationCallback(_genericDBActionCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
				}
			}
			catch (Exception ex)
			{
#if DEBUG
				System.Windows.Forms.MessageBox.Show("[debug] Unable delete event: " + ex.Message + "\n" + ex.StackTrace);
#endif
				throw ex;
			}
		}

		#endregion

		#region ** ViewState

		/// <summary>
		/// ViewState
		/// </summary>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		internal StateBag ViewState
		{
			get
			{
				if (this.viewState == null)
				{
					this.viewState = new StateBag(false);
					if (this.isTrackingViewState)
					{
						((IStateManager)this.viewState).TrackViewState();
					}
				}
				return this.viewState;
			}
		}

		bool IsTrackingViewState
		{
			get
			{
				return this.isTrackingViewState;
			}
		}

		bool IStateManager.IsTrackingViewState
		{
			get
			{
				return this.isTrackingViewState;
			}
		}

		void IStateManager.TrackViewState()
		{
			this.isTrackingViewState = true;
			if (this.viewState != null)
				((IStateManager)this.viewState).TrackViewState();
			if (_EventStorage != null)
				((IStateManager)_EventStorage).TrackViewState();
			if (_CalendarStorage != null)
				((IStateManager)_CalendarStorage).TrackViewState();
		}



		object IStateManager.SaveViewState()
		{
			try
			{
				object[] stateObj = new object[3];
				if ((this.viewState != null))
					stateObj[0] = ((IStateManager)this.viewState).SaveViewState();
				if (_EventStorage != null)
					stateObj[1] = ((IStateManager)_EventStorage).SaveViewState();
				if (_CalendarStorage != null)
					stateObj[2] = ((IStateManager)_CalendarStorage).SaveViewState();
				return stateObj;
			}
			catch (Exception)
			{
				return new object[3];
			}
		}

		void IStateManager.LoadViewState(object state)
		{
			if (state != null)
			{
				object[] stateObj = (object[])state;
				int len = stateObj.Length;
				if (len > 0 && stateObj[0] != null)
					((IStateManager)this.ViewState).LoadViewState(stateObj[0]);
				if (len > 1 && stateObj[1] != null)
					((IStateManager)this.EventStorage).LoadViewState(stateObj[1]);
				if (len > 2 && stateObj[2] != null)
					((IStateManager)this.CalendarStorage).LoadViewState(stateObj[2]);
			}
		}

		#endregion

		#region --- public override ( ToString() ) ---

		/// <summary>
		/// ToString
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			/*
			int k = 0;
			if (this.winStorage.AppointmentStorage.BoundMode == true)
				k++;
			if (this.winStorage.CategoryStorage.BoundMode == true)
				k++;
			if (this.winStorage.ContactStorage.BoundMode == true)
				k++;
			if (this.winStorage.LabelStorage.BoundMode == true)
				k++;
			if (this.winStorage.ResourceStorage.BoundMode == true)
				k++;
			if (this.winStorage.StatusStorage.BoundMode == true)
				k++;

			string s = this.winStorage.AppointmentStorage.BoundMode == true ? "Bounded to Database." : "";
			switch(k) {
				case 0:
					s = "Bounded to File.";
					if (this.IsNeedSaveDataToFile())
						s += " Data not saved yet";
					break;
				case 6:
					s = "Fully Bounded to Database(s).";
					break;
				default:
					s = "Partially Bounded to Database(s).";
					break;
			}
			return s;*/
			return "";
		}

		#endregion


		#region ** private properties/methods

		private string ResolvedDataFile
		{
			get
			{
				string resolvedFileName = this.DataFile;
				if (!string.IsNullOrEmpty(resolvedFileName))
				{
					if (!resolvedFileName.StartsWith("~/"))
					{
						resolvedFileName = "~/" + resolvedFileName.Replace(@"\", "/");
					}
					resolvedFileName = HttpContext.Current.Server.MapPath(resolvedFileName);
				}
				return resolvedFileName;
			}
		}

		#endregion

		#region ** private storage implementation

		private void _ReadData()
		{
			string[] visibleCalendars = this.VisibleCalendars;
			if (_readingData)
			{
				return;
			}
			try
			{
				_readingData = true;
				bool eventsLoaded = false;
				bool calendarsLoaded = false;
				// load events:
				IDataSource ds = GetEventsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					view.Select(new DataSourceSelectArguments(),
								new DataSourceViewSelectCallback(SelectEventsCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
					eventsLoaded = true;
				}
				_readingData = true;
				ds = GetCalendarsDataSource();
				if (ds != null)
				{
					DataSourceView view = ds.GetView("");
					_readingData = true;
					view.Select(new DataSourceSelectArguments(),
								new DataSourceViewSelectCallback(SelectCalendarsCallback));
					while (_readingData)
					{
						System.Threading.Thread.Sleep(10);
					}
					calendarsLoaded = true;
				}
				_readingData = true;
				if (!eventsLoaded || !calendarsLoaded)
				{
					if (System.IO.File.Exists(this.ResolvedDataFile))
					{
						_C1EventsCalendarXmlExchanger.Import(this.ResolvedDataFile);
						if (!eventsLoaded)
						{
							_Events.Clear();
							_Events.AddRange(_C1EventsCalendarXmlExchanger.Events);
						}
						if (!calendarsLoaded)
						{
							_Calendars = _C1EventsCalendarXmlExchanger.Calendars;
						}
					}
				}
			}
			finally
			{
				_readingData = false;
			}
		}

		private void SelectEventsCallback(IEnumerable data)
		{
			List<Hashtable> arr = _readSelectData(data, this.EventStorage.Mappings);
			_Events.Clear();
			for (int i = 0; i < arr.Count; i++)
			{
				Event ev = new Event(arr[i]);
				_Events.Add(ev);
			}
			_readingData = false;
		}

		private ParameterCollection _getCommandParameters(string commandName, IDataSource ds)
		{
			ParameterCollection commandParameters = null;
			System.Reflection.PropertyInfo pi = ds.GetType().GetProperty(commandName);
			if (pi != null)
			{
				commandParameters = pi.GetValue(ds, null) as ParameterCollection;
			}
			return commandParameters;
		}

		private bool _genericDBActionCallback(int affectedRecords, Exception ex)
		{
			if (ex != null)
				throw ex;
			if (affectedRecords == 0)//qq:<=0? ObjectDataSource returns -1 for success action....
			{
				throw new Exception("Warning: DataBase operation failed, affectedRecords=" + affectedRecords);
			}
			_readingData = false;
			return true;
		}

		private static Hashtable _dataFieldTypesAutoDetect = new Hashtable();

		private List<Hashtable> _readSelectData(IEnumerable data, C1MappingsBase mappings)
		{
			//To support other types of datasource, (Entityframework's data for example).
			IEnumerator e = data.GetEnumerator();
			List<Hashtable> dataList = new List<Hashtable>();
			while (e.MoveNext())
			{
				object o = e.Current;
				if (o != null)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
					Hashtable ht = new Hashtable();
					MappingInfo[] mappingsArr = mappings.ToArray();
					for (int j = 0; j < mappingsArr.Length; j++)
					{
						MappingInfo mapping = mappingsArr[j];
						string fieldName = mapping.IsMapped ?
											mapping.MappingName : mapping.PropertyName;

						PropertyDescriptor descr = pdc.Find(fieldName, true);
						object val = descr != null ?
							descr.GetValue(o) : mapping.DefaultValue;
						if (val is System.DBNull)
						{
							// fix for
							// [20587] [ASP.Net Wijmo][C1EventsCalendar][ControlExplorer] 
							// after loading DataBinding sample of C1EventsCalendar, 
							// “System.InvalidCastException” occurred when the link for 
							// other C1EventsCalendar sample is clicked:
							val = null;
						}
						if (mapping.PropertyName == "id")
						{
							// convert id to string:
							// fix for [20551] [ASP.Net Wijmo][C1EventsCalendar] Unlike previous build, “System.InvalidCastException” occurred when C1EventsCalendar is bound to AccessDataSource:
							ht[mapping.PropertyName] = val.ToString();
						}
						else
						{
							ht[mapping.PropertyName] = val;
						}
						if (val != null)
						{
							_dataFieldTypesAutoDetect[fieldName] = val;
							if (mapping.PropertyName == "properties")
							{
								// support old Schedule.Net properties format:
								string sourceProps = val.ToString();
								Hashtable deserializedProps = new Hashtable();

								if (!string.IsNullOrEmpty(sourceProps) && !string.IsNullOrEmpty(sourceProps.Trim()))
								{
									using (TextReader tr = new StringReader(sourceProps))
									using (XmlReader reader = XmlReader.Create(tr, this._XmlReaderSettings))
									{
										Event.PropsFromXmlInternal(reader, ht, null);
									}
								}
							}
						}
					}
					dataList.Add(ht);
				}
			}
			return dataList;
		/*
			DataView dataView = data as DataView;
			List<Hashtable> dataList = new List<Hashtable>();
			for (int i = 0; i < dataView.Count; i++)
			{

				DataRow row = dataView[i].Row;
				Hashtable ht = new Hashtable();
				MappingInfo[] mappingsArr = mappings.ToArray();
				for (int j = 0; j < mappingsArr.Length; j++)
				{
					MappingInfo mapping = mappingsArr[j];
					string fieldName = mapping.IsMapped ?
										mapping.MappingName : mapping.PropertyName;

					object val = row.Table.Columns.Contains(fieldName) ?
						row[fieldName] : mapping.DefaultValue;
					if (val is System.DBNull)
					{
						// fix for
						// [20587] [ASP.Net Wijmo][C1EventsCalendar][ControlExplorer] 
						// after loading DataBinding sample of C1EventsCalendar, 
						// “System.InvalidCastException” occurred when the link for 
						// other C1EventsCalendar sample is clicked:
						val = null;
					}
					if (mapping.PropertyName == "id")
					{
						// convert id to string:
						// fix for [20551] [ASP.Net Wijmo][C1EventsCalendar] Unlike previous build, “System.InvalidCastException” occurred when C1EventsCalendar is bound to AccessDataSource:
						ht[mapping.PropertyName] = val.ToString();
					}
					else
					{
						ht[mapping.PropertyName] = val;
					}
					if (val != null)
					{
						_dataFieldTypesAutoDetect[fieldName] = val;
						if (mapping.PropertyName == "properties")
						{
							// support old Schedule.Net properties format:
							string sourceProps = val.ToString();
							Hashtable deserializedProps = new Hashtable();

							if (!string.IsNullOrEmpty(sourceProps) && !string.IsNullOrEmpty(sourceProps.Trim()))
							{
								using (TextReader tr = new StringReader(sourceProps))
								using (XmlReader reader = XmlReader.Create(tr, this._XmlReaderSettings))
								{
									Event.PropsFromXmlInternal(reader, ht, null);
								}
							}
						}
					}
				}
				dataList.Add(ht);
			}
			return dataList;
		 * */
		}

		private Dictionary<string, object> GetItemValues(Hashtable item,
			ParameterCollection commandParameters, C1MappingsBase mappings, bool primaryKeysOnly, bool isUpdate)
		{
			if (item == null)
				return null;
			object val;
			var mappingsArray = mappings.ToArray();
			IEnumerable<string> fieldNames;

			Dictionary<string, object> ret = new Dictionary<string, object>();

			if (commandParameters != null && commandParameters.Count > 0)
			{
				fieldNames = commandParameters.OfType<Parameter>().Select(p => { return _resolveBasicParameterName(p.Name);});
			}
			else if (mappingsArray.Length > 0)
			{
				fieldNames = mappingsArray.Where(mi => { return mi.IsMapped; }).Select(mi => { return mi.MappingName; });
			}
			else
			{
				fieldNames = item.Keys.OfType<string>().Select(k => { return _resolveBasicParameterName(k); });
			}

			foreach (var fieldName in fieldNames)
			{
				if (primaryKeysOnly && mappings.IdMapping.IsMapped && fieldName != mappings.IdMapping.MappingName)
				{
					continue;
				}

				MappingInfo mappingInfo = mappings.FindByMappingName(fieldName);
				Parameter parameter = commandParameters[fieldName];

				if (mappingInfo != null)
				{
					val = item[mappingInfo.PropertyName];
					ProcessDefaultValue(ref val, mappingInfo, parameter);
					item[mappingInfo.PropertyName] = val;//update client side value
				}
				else
				{
					if (fieldName.ToLowerInvariant() == "properties")
					{
						val = new Event(item).PropsToXml();
					}
					else
					{
						val = item[fieldName];
					}

					ProcessDefaultValue(ref val, mappingInfo, parameter);
				}

				if (isUpdate && parameter != null)
				{
					DbType dbType = parameter.DbType;
					string dbTypeStr = dbType.ToString().ToLowerInvariant();
					if (_dataFieldTypesAutoDetect.ContainsKey(parameter.Name))
					{
						Object o = _dataFieldTypesAutoDetect[parameter.Name];
						if (o is Guid)
						{
							val = new Guid(val.ToString());
						}
					}
					else if (dbTypeStr == "guid")
					{
						val = new Guid(val.ToString());
					}
					else if (dbTypeStr == "object" || dbTypeStr == "empty")
					{
						// fix for 23234:
						if (val != null && IsValidGUID(val.ToString()))
						{
							val = new Guid(val.ToString());
						}
					}
				}
				else if (val is Guid)
				{
					// use string for insert/select operations
					val = ((Guid)val).ToString("D").ToLowerInvariant();
				}

				ret[fieldName] = val;
			}

			return ret;
		}

		private void ProcessDefaultValue(ref object val, MappingInfo mi, Parameter p)
		{
			if (val is string && string.IsNullOrEmpty((string)val))
			{
				if (mi != null && !string.IsNullOrEmpty(mi.DefaultValue))
				{
					val = mi.DefaultValue;
				}
				else if (p != null && !string.IsNullOrEmpty(p.DefaultValue))
				{
					val = p.DefaultValue;
				}
			}
		}

		private bool IsValidGUID(string GUIDCheck)
		{
			if (!string.IsNullOrEmpty(GUIDCheck))
			{
				return new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$").IsMatch(GUIDCheck);
			}
			return false;
		}

		private string _resolveBasicParameterName(string parameterName)
		{
			if (parameterName.StartsWith("original_"))
			{
				parameterName = parameterName.Substring("original_".Length);
			}
			if (parameterName.StartsWith("old_"))
			{
				parameterName = parameterName.Substring("old_".Length);
			}
			return parameterName;
		}

		private bool _equalParameterName(string parameterName, string propertyName)
		{
			parameterName = _resolveBasicParameterName(parameterName);
			return parameterName == propertyName;
		}

		[NonSerialized]
		XmlReaderSettings _readerSettings;
		[NonSerialized]
		XmlWriterSettings _writerSettings;

		private XmlReaderSettings _XmlReaderSettings
		{
			get
			{
				if (_readerSettings == null)
				{
					_readerSettings = new XmlReaderSettings();
					_readerSettings.IgnoreWhitespace = true;
					_readerSettings.IgnoreComments = true;
					_readerSettings.IgnoreProcessingInstructions = true;
				}
				return _readerSettings;
			}
		}

		private XmlWriterSettings _XmlWriterSettings
		{
			get
			{
				if (_writerSettings == null)
				{
					_writerSettings = new XmlWriterSettings();
					_writerSettings.OmitXmlDeclaration = true;
				}
				return _writerSettings;
			}
		}

		#endregion

	}

	/// <summary>
	/// Represents C1Event object collection.
	/// </summary>
	[Serializable]
	public class C1EventCollection : List<Event>
	{

		#region ** fields

		C1EventsCalendarStorage _storage;

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1EventCollection"/> class.
		/// </summary>
		/// <param name="storage">The events calendar storage object.</param>
		public C1EventCollection(C1EventsCalendarStorage storage)
			: base()
		{
			_storage = storage;
		}

		#endregion

		#region ** methods


		/// <summary>
		/// This method is hidden. Please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify collection.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new void Add(Event item)
		{
			if (!_storage._readingData)
			{
				throw new HttpException("This collection is read only, please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify collection.");
			}
			base.Add(item);
		}

		/// <summary>
		/// This method is hidden. Please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify collection.
		/// </summary>
		/// <param name="collection">The collection.</param>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]		
		public new void AddRange(IEnumerable<Event> collection)
		{
			if (!_storage._readingData)
			{
				throw new HttpException("This collection is read only, please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify this collection.");
			}
			base.AddRange(collection);
		}


		/// <summary>
		/// This method is hidden. Please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify collection.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]		
		public new bool Remove(Event item)
		{
			if (!_storage._readingData)
			{
				throw new HttpException("This collection is read only, please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify this collection.");
			}
			return base.Remove(item);
		}


		/// <summary>
		/// This method is hidden. Please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify collection.
		/// </summary>
		public new void Clear()
		{
			if (!_storage._readingData)
			{
				throw new HttpException("This collection is read only, please, use DataStorage methods(AddEvent/DeleteEvent/UpdateEvent) in order to modify this collection.");
			}
			base.Clear();
		}


		#endregion


	}

	/// <summary>
	/// Specifies the format of the exported or imported file.
	/// </summary>
	public enum FileFormatEnum
	{
		/// <summary>
		/// XML format (ComponentOne's own format).
		/// </summary>
		XML,
		/// <summary>
		/// iCal format according to RFC 2445.
		/// Format specification could be found here:
		/// http://tools.ietf.org/rfc/rfc2445.txt
		/// </summary>
		iCal,
		/// <summary>
		/// Binary format.
		/// </summary>
		Binary
	}
}
/*
 	System.Collections.ArrayList ToStringArrayList(string s)
	{
		s = s.Trim(new char[] { '[', ']' });
		string[] indicesStrArr = s.Split(',');
		System.Collections.ArrayList arr = new System.Collections.ArrayList();
		for (int i = 0; i < indicesStrArr.Length; i++)
		{
			if (!string.IsNullOrEmpty(indicesStrArr[i]))
			{
				arr.Add(indicesStrArr[i]);
			}

		}
		return arr;
	}

 */