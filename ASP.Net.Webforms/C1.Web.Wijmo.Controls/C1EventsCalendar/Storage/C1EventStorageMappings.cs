﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{
	/// <summary>
	/// C1EventStorage mappings configuration.
	/// </summary>
	[PersistenceMode(PersistenceMode.InnerProperty), TypeConverter(typeof(ExpandableObjectConverter))]
	[Serializable]
	public class C1EventStorageMappings : C1MappingsBase
	{

		#region ** fields

		C1EventStorage _c1EventStorage;
		private MappingInfo _calendar;
		private MappingInfo _subject;
		private MappingInfo _location;
		private MappingInfo _start;
		private MappingInfo _end;		
		private MappingInfo _description;
		private MappingInfo _color;
		private MappingInfo _properties;
		private MappingInfo _tag;

		/*
		/// Event object fields:
		///     id - String, unique event id, this field generated automatically;
		///		calendar - String, calendar id to which the event belongs;
		///		subject - String, event title;
		///		location - String, event location;
		///		start - Date, start date/time;
		///		end - Date, end date/time;
		///		description - String, event description;
		///		color - String, event color;
		///		allday - Boolean, indicates all day event
		///		tag - String, this field can be used to store any user-defined information.
		 
		 Note, allday and recurrence pattern stored into "properties" field.
		 */

		#endregion

		#region ** constructor

		public C1EventStorageMappings(C1EventStorage c1EventStorage)
			: base(c1EventStorage.ViewState)
		{
			this._c1EventStorage = c1EventStorage;
			_calendar = Add("calendar", false, string.Empty);
			_subject = Add("subject", false, string.Empty);
			_location = Add("location", false, string.Empty);
			_start = Add("start", false, string.Empty);
			_end = Add("end", false, string.Empty);			
			_description = Add("description", false, string.Empty);
			_color = Add("color", false, string.Empty);
			_properties = Add("properties", false, string.Empty);
			_tag = Add("tag", false, string.Empty);
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Allows the calendar property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.CalendarMapping", "Allows the calendar property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo CalendarMapping
		{
			get
			{
				return _calendar;
			}
		}

		/// <summary>
		/// Allows the tag property to be bound to appropriate field in the data source (the tag property can be used to store any user-defined information).
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.TagMapping", "Allows the tag property to be bound to appropriate field in the data source. (the tag property can be used to store any user-defined information)")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo TagMapping
		{
			get
			{
				return _tag;
			}
		}

		/// <summary>
		/// Allows other event properties to be bound to appropriate field in the data source. 
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.PropertiesMapping", "Allows other event properties to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo PropertiesMapping
		{
			get
			{
				return _properties;
			}
		}		

		/// <summary>
		/// Allows the location property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.LocationMapping", "Allows the location property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo LocationMapping
		{
			get
			{
				return _location;
			}
		}

		/// <summary>
		/// Allows the start property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.StartMapping", "Allows the start property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo StartMapping
		{
			get
			{

				return _start;

			}
		}

		/// <summary>
		/// Allows the end property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.EndMapping", "Allows the end property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo EndMapping
		{
			get
			{

				return _end;

			}
		}

		/// <summary>
		/// Allows the subject property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.SubjectMapping", "Allows the subject property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo SubjectMapping
		{
			get
			{

				return _subject;

			}
		}

		/// <summary>
		/// Allows the description property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.DescriptionMapping", "Allows the description property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo DescriptionMapping
		{
			get
			{

				return _description;

			}
		}

		/// <summary>
		/// Allows the color property to be bound to appropriate field in the data source.
		/// </summary>
		[C1Category("Mappings")]
		[C1Description("C1EventsCalendar.C1EventStorageMappings.ColorMapping", "Allows the color property to be bound to appropriate field in the data source.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public MappingInfo ColorMapping
		{
			get
			{

				return _color;

			}
		}


		#endregion

	}
}
