﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1EventsCalendar
{

	/// <summary>
	/// Serializes the C1EventsCalendar Control.
	/// </summary>
	public class C1EventsCalendarSerializer : C1BaseSerializer<C1EventsCalendar, object, C1EventsCalendar>
	{

		#region ** fields


		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the C1EventsCalendarSerializer class.
		/// </summary>
		public C1EventsCalendarSerializer(Object obj)
			: base(obj)
		{
		}

		#endregion

		#region ** protected override

		#endregion

		#region ** private implementation

		#endregion

	}
}
