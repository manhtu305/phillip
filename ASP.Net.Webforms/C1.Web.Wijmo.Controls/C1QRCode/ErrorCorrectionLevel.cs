﻿using WinErrorCorrectionLevel = C1.Win.C1BarCode.ErrorCorrectionLevel;

namespace C1.Web.Wijmo.Controls.C1QRCode
{
	/// <summary>
	/// Specifies the error-correction level (higher levels consume more space).
	/// </summary>
	public enum ErrorCorrectionLevel
	{
		/// <summary>
		/// Able to correct up to 7% damage.
		/// </summary>
		L = WinErrorCorrectionLevel.L,

		/// <summary>
		/// Able to correct up to 15% damage.
		/// </summary>
		M = WinErrorCorrectionLevel.M,

		/// <summary>
		/// Able to correct up to 25% damage.
		/// </summary>
		Q = WinErrorCorrectionLevel.Q,

		/// <summary>
		/// Able to correct up to 30% damage.
		/// </summary>
		H = WinErrorCorrectionLevel.H
	};
}