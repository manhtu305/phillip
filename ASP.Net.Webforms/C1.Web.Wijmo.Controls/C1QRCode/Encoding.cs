﻿using WinEncoding = C1.Win.C1BarCode.Encoding;

namespace C1.Web.Wijmo.Controls.C1QRCode
{
	/// <summary>
	/// Specifies the type of encoding to use (more flexible encodings consume more space).
	/// </summary>
	public enum Encoding
	{
		/// <summary>
		/// Select encoding automatically based on the content.
		/// </summary>
		Automatic = WinEncoding.Automatic,

		/// <summary>
		/// Encode up to 395 alpha-numeric values. Alpha-numeric values include
		/// digits from 0 to 9, uppercase letters from A to Z, space, 
		/// and the following additional characters: dollar, percentage, 
		/// asterisk, plus, minus, slash, and colon ([0-9][A-Z][$%*+-./:]).
		/// </summary>
		AlphaNumeric = WinEncoding.AlphaNumeric,

		/// <summary>
		/// Encode up to 652 numeric values.
		/// </summary>
		Numeric = WinEncoding.Numeric,

		/// <summary>
		/// Encode up to 271 bytes.
		/// </summary>
		Byte = WinEncoding.Byte
	}
}