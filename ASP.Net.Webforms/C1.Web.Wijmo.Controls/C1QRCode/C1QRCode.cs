﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
using WinQRCode = C1.Win.C1BarCode.C1QRCode;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1QRCode", "wijmo")]

namespace C1.Web.Wijmo.Controls.C1QRCode
{
	using Localization;

    /// <summary>
    /// Represents a C1QRCode in an ASP.NET Web page.
    /// </summary>
#if ASP_NET45
    [Designer(
        "C1.Web.Wijmo.Controls.Design.C1QRCode.C1QRCodeDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer(
		"C1.Web.Wijmo.Controls.Design.C1QRCode.C1QRCodeDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1QRCode.C1QRCodeDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1QRCode runat=server></{0}:C1QRCode>")]
	[ToolboxBitmap(typeof (C1QRCode), "C1QRCode.png")]
	[LicenseProviderAttribute]
	public class C1QRCode : C1TargetControlBase
	{
		#region Fields
		
		private WinQRCode _innerQRCode;
		private bool _innerQRCodeInvalidated = true;
		private Image _image;
		private string _imageUrl;
		private bool _productLicensed;
		private bool _shouldNag;
		private const int DefaultCodeVersion = 0;
		private const int DefaultSymbolSize = 3;
		private const string DefaultBackColorName = "White";
		private const string DefaultForeColorName = "Black";
		private static readonly Color DefaultBackColor = Color.FromName(DefaultBackColorName);
		private static readonly Color DefaultForeColor = Color.FromName(DefaultForeColorName);

		#endregion

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1QRCode"/> class.
		/// </summary>
		public C1QRCode():base(HtmlTextWriterTag.Img)
		{
			VerifyLicense();
			InitMembers();
		}

		private void InitMembers()
		{
			BackColor = DefaultBackColor;
			ForeColor = DefaultForeColor;
		}

		private void VerifyLicense()
		{
			var licinfo = Util.Licensing.ProviderInfo.Validate(typeof(C1QRCode), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region Properties

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string[] CDNDependencyPaths
		{
			get { return base.CDNDependencyPaths; }
			set { base.CDNDependencyPaths = value; }
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string CDNPath
		{
			get { return base.CDNPath; }
			set { base.CDNPath = value; }
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override bool UseCDN
		{
			get { return base.UseCDN; }
			set { base.UseCDN = value; }
		}

		protected override IDictionary PropertiesContainer
		{
			get { return ViewState; }
		}

		private WinQRCode InnerQRCode
		{
			get 
			{
				EnsureInnerQRCode();
				return _innerQRCode;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string Theme
		{
			get { return string.Empty; }
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string ThemeSwatch
		{
			get { return base.ThemeSwatch; }
			set { base.ThemeSwatch = value; }
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string WijmoCssAdapter
		{
			get { return base.WijmoCssAdapter; }
			set { base.WijmoCssAdapter = value; }
		}

		internal override bool IsWijMobileControl
		{
			get { return false; }
		}

		/// <summary>
		/// Gets or sets the background color for the control.
		/// </summary>
		[Browsable(true)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[DefaultValue(typeof(Color), DefaultBackColorName)]
		[C1Description("C1QRCode.BackColor")]
		public override Color BackColor
		{
			get { return base.BackColor; }
			set
			{
				if (BackColor == value) return;

				base.BackColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// Gets or sets the foreground color for the control.
		/// </summary>
		[Browsable(true)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[DefaultValue(typeof(Color), DefaultForeColorName)]
		[C1Description("C1QRCode.ForeColor")]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set
			{
				if (ForeColor == value) return;

				base.ForeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// Gets or sets the value that is encoded as a QR code image.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1QRCode.Text")]
		[C1Category("Category.Data")]
		public string Text
		{
			get
			{
				return GetPropertyValue("Text", string.Empty);
			}
			set
			{
				if (value == Text)
				{
					return;
				}

				SetPropertyValue("Text", value);
				Invalidate();
			}
		}

		/// <summary>
		/// Gets or sets the "version" of QR code to generate.
		/// </summary>
		/// <remarks>
		/// <para>The QR specification includes 40 "versions" of QR codes. 
		/// Higher versions consume more space and are able to encode more 
		/// information.</para>
		/// <para>The <see cref="C1QRCode"/> control can generate QR codes
		/// versions one through ten only, which allow encoding up to 
		/// 652 digits, or 395 alphanumeric characters, or 271 bytes.</para>
		/// <para>Set the <see cref="CodeVersion"/> property to zero (the default
		/// value) to automatically use the most compact version able to render the
		/// content specified in the <see cref="Text"/> property.</para>
		/// </remarks>
		[DefaultValue(DefaultCodeVersion)]
		[C1Description("C1QRCode.CodeVersion")]
		[C1Category("Category.Data")]
		public int CodeVersion
		{
			get
			{
				return GetPropertyValue("CodeVersion", DefaultCodeVersion);
			}
			set
			{
				if (value < 0 || value > 10)
				{
					throw new ArgumentOutOfRangeException("value", C1Localizer.GetString("C1QRCode.InvalidCodeVersionException",
						"Invalid CodeVersion value (must be between 0 and 10)."));
				}

				if (value == CodeVersion)
				{
					return;
				}

				SetPropertyValue("CodeVersion", value);
				Invalidate();
			}
		}


		/// <summary>
		/// Gets or sets the error correction level used to create the QR code.
		/// </summary>
		/// <remarks>
		/// <para>Higher levels of error correction add more redundant information to 
		/// the QR code, making it more resistant to damage.
		/// Higher levels of error correction also reduce the amount of data
		/// that can be encoded in a given area.</para>
		/// <para>The default value for this property is the lowest level, which
		/// produces the most compact QR codes.</para>
		/// </remarks>
		[DefaultValue(ErrorCorrectionLevel.L)]
		[C1Description("C1QRCode.ErrorCorrectionLevel")]
		[C1Category("Category.Data")]
		public ErrorCorrectionLevel ErrorCorrectionLevel
		{
			get { return GetPropertyValue("ErrorCorrectionLevel", ErrorCorrectionLevel.L); }
			set
			{
				if (value == ErrorCorrectionLevel)
				{
					return;
				}

				SetPropertyValue("ErrorCorrectionLevel", value);
				Invalidate();
			}
		}
		/// <summary>
		/// Gets or sets the <see cref="C1QRCode.Encoding"/> used to translate
		/// the content in the <see cref="Text"/> property into binary values
		/// to be encoded in the QR code.
		/// </summary>
		/// <remarks>
		/// <para>More flexible encodings have lower capacity. The <see cref="C1QRCode"/>
		/// control can encode up to 652 digits, or 395 alphanumeric characters, or
		/// 271 bytes.</para>
		/// <para>The default value for this property is 
		/// <see cref="C1.Web.Wijmo.Controls.C1QRCode.Encoding.Automatic"/>, which causes the 
		/// control to automatically select the most compact encoding that can be 
		/// used based on the content specified in the <see cref="Text"/> 
		/// property.</para>
		/// </remarks>
		[DefaultValue(Encoding.Automatic)]
		[C1Description("C1QRCode.Encoding")]
		[C1Category("Category.Data")]
		public Encoding Encoding
		{
			get { return GetPropertyValue("Encoding", Encoding.Automatic); }
			set
			{
				if (value == Encoding)
				{
					return;
				}

				SetPropertyValue("Encoding", value);
				Invalidate();
			}
		}

		/// <summary>
		/// Gets or sets the size, in pixels, of the symbols used to build the QR image.
		/// </summary>
		/// <remarks>
		/// <para>Larger values will result in larger images which consume more space 
		/// but may be easier to for some scanners to read.</para>
		/// <para>The default symbol size is three pixels, which usually represents a 
		/// good compromise between size and readability.</para>
		/// <para>This property must be set to values between 2 and 10.</para>
		/// </remarks>
		[DefaultValue(DefaultSymbolSize)]
		[C1Description("C1QRCode.SymbolSize")]
		[C1Category("Category.Data")]
		public int SymbolSize
		{
			get { return GetPropertyValue("SymbolSize", DefaultSymbolSize); }
			set
			{
				if (value < 2 || value > 10)
				{
					throw new ArgumentOutOfRangeException("value", C1Localizer.GetString("C1QRCode.InvalidSymbolSizeException",
						"Invalid symbol size (should be between 2 and 10)."));
				}

				if (value == SymbolSize)
				{
					return;
				}

				SetPropertyValue("SymbolSize", value);
				Invalidate();
			}
		}

		/// <summary>
		/// Gets or sets a value to determine whether control should reset its size according to the QR code image.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1QRCode.AutoSize")]
		[C1Category("Category.Appearance")]
		public bool AutoSize
		{
			get { return GetPropertyValue("AutoSize", false); }
			set
			{
				SetPropertyValue("AutoSize", value);
			}
		}

		/// <summary>
		/// Gets or sets the width of control.
		/// </summary>
		[C1Description("C1QRCode.Width")]
		public override Unit Width
		{
			get
			{
				return CanAutoSize ? Image.Width : ControlWidth;
			}
			set
			{
				ControlWidth = value;
			}
		}

		/// <summary>
		/// Gets or sets the height of control.
		/// </summary>
		[C1Description("C1QRCode.Height")]
		public override Unit Height
		{
			get
			{
				return CanAutoSize ? Image.Height : ControlHeight;
			}
			set
			{
				ControlHeight = value;
			}
		}

		private bool CanAutoSize
		{
			get { return AutoSize && HasImage; }
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		internal bool HasImage
		{
			[SmartAssembly.Attributes.DoNotObfuscate]
			get { return Image != null; }
		}

		private Unit ControlWidth
		{
			get { return GetPropertyValue("ControlWidth", Unit.Empty); }
			set { SetPropertyValue("ControlWidth", value); }
		}

		private Unit ControlHeight
		{
			get { return GetPropertyValue("ControlHeight", Unit.Empty); }
			set { SetPropertyValue("ControlHeight", value); }
		}

		/// <summary>
		/// Gets the image of the QR code.
		/// </summary>
		[Browsable(false)]
		public Image Image
		{
			get
			{
				EnsureInnerQRCode();
				return _image;
			}
		}

		/// <summary>
		/// Get a Base64 string which is a conversion of the QR code image.
		/// Users can set this result into an ASP.net Image control's "ImageUrl" property
		/// or a HTML Image element's "Src" attribute to show the QR code image manually.
		/// </summary>
		[Browsable(false)]
		public string ImageUrl
		{
			get
			{
				EnsureInnerQRCode();
				return _imageUrl;
			}
		}

		/// <summary>
		/// Gets the <see cref="Exception"/> that prevented the <see cref="Text"/> from being encoded.
		/// </summary>
		/// <remarks>
		/// <para>The <see cref="C1QRCode"/> control can be used to encode up to
		/// 652 digits, or 395 alphanumeric characters (uppercase only), or 271 bytes.</para>
		/// <para>If these limits are exceeded, or if any of the control properties is set
		/// to values that prevent the content of the <see cref="Text"/> property from
		/// being encoded, the control remains blank, and the <see cref="EncodingException"/>
		/// property contains details that explain why the code could not be generated.</para>
		/// </remarks>
		/// <example>
		/// The code below sets the <see cref="Text"/> property and shows a message in case 
		/// any errors are detected:
		/// <code>
		/// c1QRCode1.Text = textBox1.Text;
		/// label1.Text = c1QRCode1.EncodingException == null 
		///   ? string.Empty 
		///   : c1QRCode1.EncodingException.Message;
		/// </code>
		/// </example>
		[Browsable(false)]
		public Exception EncodingException
		{
			get
			{
				EnsureInnerQRCode();
				return InnerQRCode.EncodingException;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return new List<ScriptDescriptor>();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			ControlStyle.Width = CanAutoSize ? Unit.Empty : ControlWidth;
			ControlStyle.Height = CanAutoSize ? Unit.Empty : ControlHeight;

			base.AddAttributesToRender(writer);

			if (!string.IsNullOrEmpty(ImageUrl))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Src, ImageUrl);
			}

			var tip = GetTip();
			writer.AddAttribute(HtmlTextWriterAttribute.Alt, tip);
			writer.AddAttribute(HtmlTextWriterAttribute.Title, tip);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		internal string GetTip()
		{
			if (string.IsNullOrEmpty(Text))
			{
				return C1Localizer.GetString("C1QRCode.Empty", "An empty C1QRCode control.");
			}

			return EncodingException == null ? Text
				: C1Localizer.GetString("C1QRCode.EncodingFailed",
					"Failed to encode Text property as the QR code image. Please check EncodingException property for more details.");
		}

		private void EnsureInnerQRCode()
		{
			if (!_innerQRCodeInvalidated && _innerQRCode != null)
			{
				return;
			}

			if (_innerQRCode == null)
			{
				_innerQRCode = new WinQRCode();
			}

			_innerQRCode.BackColor = BackColor.IsEmpty ? DefaultBackColor : BackColor;
			_innerQRCode.ForeColor = ForeColor.IsEmpty ? DefaultForeColor : ForeColor;
			_innerQRCode.Text = Text;
			_innerQRCode.CodeVersion = CodeVersion;
			_innerQRCode.ErrorCorrectionLevel = (Win.C1BarCode.ErrorCorrectionLevel)(int)ErrorCorrectionLevel;
			_innerQRCode.Encoding = (Win.C1BarCode.Encoding)(int)Encoding;
			_innerQRCode.SymbolSize = SymbolSize;

			if (_image != null)
			{
				_image.Dispose();
			}

			if (!string.IsNullOrEmpty(_imageUrl) && IsDesignMode)
			{
				try
				{
					File.Delete(_imageUrl);
				}
				catch
				{
				}
			}

			_image = null;
			_imageUrl = string.Empty;
			if(!string.IsNullOrEmpty(Text))
			{
				var format = ImageFormat.Png;
				_image = _innerQRCode.GetImage(format);
				if (_image != null)
				{
					if (IsDesignMode)
					{
						_imageUrl = Path.GetTempFileName() + "." + format.ToString().ToLower();
						_image.Save(_imageUrl, format);
					}
					else
					{
						using (var memoryStream = new MemoryStream())
						{
							_image.Save(memoryStream, format);
							_imageUrl = "data:image/" + format + ";base64," + Convert.ToBase64String(memoryStream.ToArray());
						}
					}
				}
			}

			_innerQRCodeInvalidated = false;
		}

		private void Invalidate()
		{
			_innerQRCodeInvalidated = true;
		}

		#endregion
	}
}