﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1TreeMap
{
	/// <summary>
	/// Represents a tree map in an ASP.NET Web page.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeMap.C1TreeMapDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeMap.C1TreeMapDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeMap.C1TreeMapDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1TreeMap.C1TreeMapDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1TreeMap runat=server></{0}:C1TreeMap>")]
	[ToolboxBitmap(typeof(C1TreeMap), ("C1TreeMap.png"))]
	[LicenseProviderAttribute]
	public partial class C1TreeMap : C1TargetHierarchicalDataBoundControlBase, IPostBackDataHandler, IC1Serializable
	{
		#region ** fields

		private const string CONST_DEF_WIDTH = "512px";
		private const string CONST_DEF_HEIGHT = "384px";

		internal bool _productLicensed;
		private bool _shouldNag;
		private LocalizationOption _localizationOption;
		#endregion

		#region ** constructor
		public C1TreeMap() 
		{
			VerifyLicense();
			Width = new Unit(CONST_DEF_WIDTH);
			Height = new Unit(CONST_DEF_HEIGHT);
		}
		#endregion

		#region ** properties
		/// <summary>
		/// Gets a collection of C1TreeMapItemBinding objects that define the relationship 
		/// between a data item and the data items of treemap it is binding to.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1TreeMap.DataBindings")]
		[DefaultValue((string)null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public C1TreeMapItemBindingCollection DataBindings
		{
			get
			{
				if (this._bindings == null)
				{
					this._bindings = new C1TreeMapItemBindingCollection(this);
				}
				return this._bindings;
			}
		}

		private Hashtable _innerStates;
		/// <summary>
		/// Gets the innerStates for tooltip. it send to client side. to deserialize the client function option.
		/// </summary>
		[Json(true)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public Hashtable InnerStates
		{
			get
			{
				if (_innerStates == null)
				{
					_innerStates = new Hashtable();
				}

				if (!string.IsNullOrEmpty(this.TitleFormatter)) 
				{
					_innerStates.Add("TitleFormatter", TitleFormatter);
				}

				if (!string.IsNullOrEmpty(this.LabelFormatter)) 
				{
					_innerStates.Add("LabelFormatter", LabelFormatter);
				}

				return _innerStates;
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public LocalizationOption Localization
		{
			get { return _localizationOption ?? (_localizationOption = new LocalizationOption()); }
		}

		/// <summary>
		/// A value that indicates the width of the control.
		/// </summary>
		[C1Description("C1TreeMap.Width")]
		[C1Category("Category.Layout")]
		[DefaultValue(typeof(Unit), CONST_DEF_WIDTH)]
		[Layout(LayoutType.Sizes)]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// A value that indicates the height of the control.
		/// </summary>
		[C1Description("C1TreeMap.Height")]
		[C1Category("Category.Layout")]
		[DefaultValue(typeof(Unit), CONST_DEF_HEIGHT)]
		[Layout(LayoutType.Sizes)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}
		#endregion

		#region ** override methods
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// OnPreRender override.
		/// Performs registration in ScriptManager and register <see cref="C1TreeView"/> treeview for PostBack.
		/// Relate all <see cref="C1TreeViewNode"/> nodes to its <see cref="C1TreeView"/> treeview
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		#endregion

		#region ** data binding
		private C1TreeMapItemBindingCollection _bindings;

		/// <summary>
		/// Binds a data source to the invoked <see cref="C1TreeView"/> tree view and all its nodes.
		/// </summary>
		public sealed override void DataBind()
		{
			base.DataBind();
		}

		/// <summary>
		/// When overridden in a derived class, binds data from the data source to the
		/// control.
		/// </summary>
		protected override void PerformDataBinding()
		{
			if (IsDesignMode)
			{
				return;
			}

			base.PerformDataBinding();

			if (!IsBoundUsingDataSourceID && (DataSource == null))
			{
				return;
			}
			HierarchicalDataSourceView view = this.GetData(null);
			if (view == null)
			{
				throw new InvalidOperationException
					(C1Localizer.GetString("C1TreeMap.NoViewReturn"));
			}
			IHierarchicalEnumerable enumerable = view.Select();
			DataBind(enumerable, 0, this.Items);
		}

		/// <summary>
		/// Binds a data source to the invoked server control and all its child controls.
		/// </summary>
		/// <param name="enumerable">Enumerable object to bind</param>
		protected virtual void DataBind(IHierarchicalEnumerable enumerable, int level, List<TreeMapItem> items)
		{
			items.Clear();
			foreach (object o in enumerable)
			{
				IHierarchyData data = enumerable.GetHierarchyData(o);
				string text = data.Type;
				TreeMapItem item = new TreeMapItem();
				item.Depth = level;
				C1TreeMapItemBinding binding = this.GetBinding(text, level);

				//if null,binding root node
				if (binding == null)
				{
					binding = this.GetBinding(text, 0);
				}
				this.DataBindInternal(binding, enumerable, o, item);
				items.Add(item);
				if (data.HasChildren)
				{
					IHierarchicalEnumerable childEnum = data.GetChildren();
					if (childEnum != null)
					{
						this.DataBind(childEnum, ++item.Depth, item.Items);
					}
				}
			}
		}

		private object GetFieldValue(object data, PropertyDescriptorCollection propCollection, string propName)
		{
			PropertyDescriptor descr = propCollection.Find(propName, true);
			if (descr != null)
			{
				return descr.GetValue(data);
			}
			else return null;
		}

		private void DataBindInternal(C1TreeMapItemBinding binding, IHierarchicalEnumerable enumerable, object data, TreeMapItem item) 
		{
			IHierarchyData hdata = enumerable.GetHierarchyData(data);
			if (binding != null)
			{
				PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(data);
				if (!string.IsNullOrEmpty(binding.LabelField))
				{
					string label = null;
					if (GetFieldValue(data, pdc, binding.LabelField) != null)
					{
						label = (string)GetFieldValue(data, pdc, binding.LabelField);
					}
					if (label != null)
					{
						item.Label = label;
					}
				}

				if (!string.IsNullOrEmpty(binding.ValueField))
				{
					double value;
					if (GetFieldValue(data, pdc, binding.ValueField) != null)
					{
						if(double.TryParse(GetFieldValue(data, pdc, binding.ValueField).ToString(), out value))
						{
							item.Value = value;
						}
					}
				}

				if (!string.IsNullOrEmpty(binding.ColorField))
				{
					Color? color = null;
					if (GetFieldValue(data, pdc, binding.ColorField) != null)
					{

						color = C1WebColorConverter.ToColor(GetFieldValue(data, pdc, binding.ColorField).ToString());
					}
					if (color != null)
					{
						item.Color = (Color)color;
					}
				}
			}
			
		}

		internal virtual C1TreeMapItemBinding GetBinding(string dataMember, int depth)
		{
			if (this._bindings == null)
				return null;
			return this._bindings.GetBinding(dataMember, depth);
		}

		#endregion

		#region ** private methods

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1TreeMap), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		#region ** IPostBackDataHandler
		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			this.RestoreStateFromJson(data);
			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			
		}
		#endregion

		#region ** IC1Serializable
		void IC1Serializable.SaveLayout(string filename)
		{
			C1TreeMapSerializer serializer = new C1TreeMapSerializer(this);
			serializer.SaveLayout(filename);
		}

		void IC1Serializable.SaveLayout(System.IO.Stream stream)
		{
			C1TreeMapSerializer serializer = new C1TreeMapSerializer(this);
			serializer.SaveLayout(stream);
		}

		void IC1Serializable.LoadLayout(string filename)
		{
			C1TreeMapSerializer serializer = new C1TreeMapSerializer(this);
			serializer.LoadLayout(filename);
		}

		void IC1Serializable.LoadLayout(System.IO.Stream stream)
		{
			C1TreeMapSerializer serializer = new C1TreeMapSerializer(this);
			serializer.LoadLayout(stream);
		}

		void IC1Serializable.LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1TreeMapSerializer serializer = new C1TreeMapSerializer(this);
			serializer.LoadLayout(filename, layoutTypes);
		}

		void IC1Serializable.LoadLayout(System.IO.Stream stream, LayoutType layoutTypes)
		{
			C1TreeMapSerializer serializer = new C1TreeMapSerializer(this);
			serializer.LoadLayout(stream, layoutTypes);
		}
		#endregion

		#region ** deserialize the formatters
		[EditorBrowsable( EditorBrowsableState.Never)]
		public void C1DeserializeTitleFormatter(object value) 
		{
			Hashtable ht = value as Hashtable;
			if (ht["type"].ToString() == "fun" && ht["value"]!=null) 
			{
				this.TitleFormatter = ht["value"].ToString();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeLabelFormatter(object value)
		{
			Hashtable ht = value as Hashtable;
			if (ht["type"].ToString() == "fun" && ht["value"] != null)
			{
				this.LabelFormatter = ht["value"].ToString();
			}
		}
		#endregion

		#region ** LocalizationOption class

		/// <summary>
		/// The localization of the treeMap string
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public class LocalizationOption : Settings, IJsonEmptiable
		{
			#region ** localization string properties

			/// <summary>
			/// BackButtonTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string BackButtonTitle
			{
				get
				{
					return GetPropertyValue("BackButtonTitle", C1Localizer.GetString("C1TreeMap.BackButtonTitle", "back"));
				}
				set { SetPropertyValue("BackButtonTitle", value); }
			}

			/// <summary>
			/// HomeButtonTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string HomeButtonTitle
			{
				get
				{
					return GetPropertyValue("HomeButtonTitle", C1Localizer.GetString("C1TreeMap.HomeButtonTitle", "home"));
				}
				set { SetPropertyValue("HomeButtonTitle", value); }
			}
			#endregion

			#region ** implement ICustomOptionType interface

			internal bool ShouldSerialize()
			{
				return true;
			}

			#endregion end of ** IJsonEmptiable interface implementation


			bool IJsonEmptiable.IsEmpty
			{
				get { return !ShouldSerialize(); }
			}
		}

		#endregion end of ** LocalizationOption class
	}
}
