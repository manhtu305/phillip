﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1TreeMap
{
	public class C1TreeMapSerializer : C1BaseSerializer<C1TreeMap, object, object>
	{
		public C1TreeMapSerializer(C1TreeMap obj)
			: base(obj)
		{
		}
	}
}
