﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1TreeMap
{
	/// <summary>
	/// Defines the relationship between a data item and the node it is binding to in a <see cref="TreeMapItem"/> Object.
	/// </summary>
	[AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	public class C1TreeMapItemBinding : IStateManager, ICloneable, IDataSourceViewSchemaAccessor
	{

		#region ** fields
		private bool _isTrackingViewState;
		private StateBag _viewState;
		#endregion

		#region Constructor
        /// <summary>
		/// Initializes a new instance of the <see cref="C1TreeMapItemBinding"/> class.
        /// </summary>
        public C1TreeMapItemBinding()
        {
        }

        /// <summary>
		/// Initializes a new instance of the <see cref="C1TreeMapItemBinding"/> class.
        /// </summary>
        /// <param name="dataMember"></param>
		public C1TreeMapItemBinding(string dataMember)
        {
            this.DataMember = dataMember;
        }
        #endregion

		#region ** properties
		/// <summary>
		/// Gets or sets the value to match against a <see cref="IHierarchyData"/>. Type property for a data item to determine whether to apply the tree node binding.
		/// </summary>
		[C1Description("C1TreeMapItemBinding.DataMember")]
		[DefaultValue("")]
		[C1Category("Category.Data")]
		public string DataMember
		{
			get
			{
				object obj2 = this.ViewState["DataMember"];
				if (obj2 == null)
				{
					return string.Empty;
				}
				return (string)obj2;
			}
			set
			{
				this.ViewState["DataMember"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the node depth at which the <see cref="C1TreeMapItemBinding"/> object is applied.
		/// </summary>
		[C1Category("Category.Data")]
		[DefaultValue(-1)]
		[C1Description("C1TreeMapItemBinding.Depth")]
		public int Depth
		{
			get
			{
				object obj2 = this.ViewState["Depth"];
				if (obj2 == null)
				{
					return -1;
				}
				return (int)obj2;
			}
			set
			{
				this.ViewState["Depth"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the value field at which the <see cref="C1TreeMapItemBinding"/> object is applied.
		/// </summary>
		[C1Category("Category.Data")]
		[DefaultValue(-1)]
		[C1Description("C1TreeMapItemBinding.ValueField")]
		public string ValueField 
		{
			get 
			{
				return this.ViewState["ValueField"] == null ? string.Empty : (string)this.ViewState["ValueField"];
			}
			set
			{
				this.ViewState["ValueField"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the label field at which the <see cref="C1TreeMapItemBinding"/> object is applied.
		/// </summary>
		[C1Category("Category.Data")]
		[DefaultValue(-1)]
		[C1Description("C1TreeMapItemBinding.LabelField")]
		public string LabelField
		{
			get 
			{
				return this.ViewState["LabelField"] == null ? string.Empty : (string)this.ViewState["LabelField"];
			}
			set 
			{
				this.ViewState["LabelField"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the color field at which the <see cref="C1TreeMapItemBinding"/> object is applied.
		/// </summary>
		[C1Category("Category.Data")]
		[DefaultValue(-1)]
		[C1Description("C1TreeMapItemBinding.ColorField")]
		public string ColorField 
		{
			get
			{
				return this.ViewState["ColorField"] == null ? string.Empty : (string)this.ViewState["ColorField"];
			}
			set
			{
				this.ViewState["ColorField"] = value;
			}
		}
		#endregion

		#region ** IStateManager
		bool IStateManager.IsTrackingViewState
		{
			get
			{
				return this._isTrackingViewState;
			}
		}

		void IStateManager.LoadViewState(object state)
		{
			if (state != null)
			{
				((IStateManager)this.ViewState).LoadViewState(state);
			}
		}

		object IStateManager.SaveViewState()
		{
			if (this._viewState != null)
			{
				return ((IStateManager)this._viewState).SaveViewState();
			}
			return null;
		}

		void IStateManager.TrackViewState()
		{
			this._isTrackingViewState = true;
			if (this._viewState != null)
			{
				((IStateManager)this._viewState).TrackViewState();
			}
		}

		private StateBag ViewState
		{
			get
			{
				if (this._viewState == null)
				{
					this._viewState = new StateBag();
					if (this._isTrackingViewState)
					{
						((IStateManager)this._viewState).TrackViewState();
					}
				}
				return this._viewState;
			}
		}
		#endregion

		#region ** IDataSourceViewSchemaAccessor
		object IDataSourceViewSchemaAccessor.DataSourceViewSchema
		{
			get
			{
				return this.ViewState["IDataSourceViewSchemaAccessor.DataSourceViewSchema"];
			}
			set
			{
				this.ViewState["IDataSourceViewSchemaAccessor.DataSourceViewSchema"] = value;
			}
		}
		#endregion

		#region ** ICloneable
		object ICloneable.Clone()
		{
			C1TreeMapItemBinding binding = new C1TreeMapItemBinding();
			binding.ColorField = this.ColorField;
			binding.DataMember = this.DataMember;
			binding.Depth = this.Depth;
			binding.LabelField = this.LabelField;
			binding.ValueField = this.ValueField;
			return binding;
		}
		#endregion

		#region ** internal methods
		internal bool IsEmpty(string propName)
		{
			return (this.ViewState[propName] == null);
		}

		internal void SetDirty()
		{
			this.ViewState.SetDirty(true);
		}
		#endregion
	}
}
