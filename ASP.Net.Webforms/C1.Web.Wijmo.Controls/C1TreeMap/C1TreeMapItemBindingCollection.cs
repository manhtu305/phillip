﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1TreeMap
{
	/// <summary>
	/// Represents a collection of <see cref="C1TreeMapItemBinding"/> objects in the <see cref="C1TreeMap"/> control. 
	/// </summary>
	[AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	public class C1TreeMapItemBindingCollection : StateManagedCollection
	{
		#region ** Fields
		private static readonly Type[] s_knownTypes;
		private readonly C1TreeMap _owner;
		private C1TreeMapItemBinding _defaultBinding;
		#endregion

		#region ** Constructors
        static C1TreeMapItemBindingCollection()
        {
            C1TreeMapItemBindingCollection.s_knownTypes = new Type[1] { typeof(C1TreeMapItemBinding) };
        }

        internal C1TreeMapItemBindingCollection()
        {
        }

		internal C1TreeMapItemBindingCollection(C1TreeMap owner)
        {
            this._owner = owner;
        }
        #endregion

		#region ** public methods
		/// <summary>
		/// Adds the binding to the collection.
		/// </summary>
		/// <param name="binding"></param>
		/// <returns></returns>
		public int Add(C1TreeMapItemBinding binding)
		{
			return ((System.Collections.IList)this).Add(binding);
		}

		/// <summary>
		/// Determines whether the specified <see cref="C1TreeMapItemBinding"/> object is in the collection.
		/// </summary>
		/// <param name="binding"></param>
		/// <returns></returns>
		public bool Contains(C1TreeMapItemBinding binding)
		{
			return ((System.Collections.IList)this).Contains(binding);
		}

		/// <summary>
		/// Copies the elements collection
		///  to an array, starting at a particular array index.
		/// </summary>
		/// <param name="bindingArray"></param>
		/// <param name="index"></param>
		public void CopyTo(C1TreeMapItemBinding[] bindingArray, int index)
		{
			base.CopyTo(bindingArray, index);
		}

		/// <summary>
		/// Gets the index of binding.
		/// </summary>
		/// <param name="binding"></param>
		/// <returns></returns>
		public int IndexOf(C1TreeMapItemBinding binding)
		{
			return ((System.Collections.IList)this).IndexOf(binding);
		}

		/// <summary>
		/// Inserts binding into the collection.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="binding"></param>
		public void Insert(int index, C1TreeMapItemBinding binding)
		{
			((System.Collections.IList)this).Insert(index, binding);
		}

		/// <summary>
		/// Removes binding from the collection.
		/// </summary>
		/// <param name="binding"></param>
		public void Remove(C1TreeMapItemBinding binding)
		{
			((System.Collections.IList)this).Remove(binding);
		}

		/// <summary>
		/// Removes binding from the collection from specific index.
		/// </summary>
		/// <param name="index"></param>
		public void RemoveAt(int index)
		{
			((System.Collections.IList)this).RemoveAt(index);
		}

		/// <summary>
		/// Gets or sets the <see cref="C1TreeMapItemBinding"/> object at the specified index in the <see cref="C1TreeMapItemBindingCollection"/> object.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public C1TreeMapItemBinding this[int i]
		{
			get
			{
				return (C1TreeMapItemBinding)((System.Collections.IList)this)[i];
			}
			set
			{
				this[i] = value;
			}
		}
		#endregion

		#region Protected override

		/// <summary>
		/// When overridden in a derived class, performs additional work before the System.Web.UI.StateManagedCollection.Clear()
		///	method removes all items from the collection.
		/// </summary>
		protected override void OnClear()
		{
			base.OnClear();
			this._defaultBinding = null;
		}

		/// <summary>
		/// When overridden in a derived class, performs additional work after the System.Web.UI.StateManagedCollection.System.Collections.IList.Remove(System.Object)
		///	or System.Web.UI.StateManagedCollection.System.Collections.IList.RemoveAt(System.Int32)
		///	method removes the specified item from the collection.
		/// </summary>
		/// <param name="index">
		/// The zero-based index of the item to remove, which is used when System.Web.UI.StateManagedCollection.System.Collections.IList.RemoveAt(System.Int32)
		///	is called.
		/// </param>
		/// <param name="value">
		/// The object removed from the System.Web.UI.StateManagedCollection, which is
		///	used when System.Web.UI.StateManagedCollection.System.Collections.IList.Remove(System.Object)
		///	is called.
		/// </param>
		protected override void OnRemoveComplete(int index, object value)
		{
			if (value == this._defaultBinding)
			{
				this.FindDefaultBinding();
			}
		}

		/// <summary>
		/// When overridden in a derived class, validates an element of the System.Web.UI.StateManagedCollection
		///	collection.
		/// </summary>
		/// <param name="value">
		/// The System.Web.UI.IStateManager to validate.
		/// </param>
		protected override void OnValidate(object value)
		{
			base.OnValidate(value);
			C1TreeMapItemBinding binding = value as C1TreeMapItemBinding;
			if (((binding != null) && (binding.DataMember.Length == 0)) && (binding.Depth == -1))
			{
				this._defaultBinding = binding;
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates an instance of a class that implements
		///	System.Web.UI.IStateManager. The type of object created is based on the specified
		///	member of the collection returned by the System.Web.UI.StateManagedCollection.GetKnownTypes()
		///	method.
		/// </summary>
		/// <param name="index">
		/// The index, from the ordered list of types returned by System.Web.UI.StateManagedCollection.GetKnownTypes(),
		///	of the type of System.Web.UI.IStateManager to create.
		/// </param>
		/// <returns>
		/// An instance of a class derived from System.Web.UI.IStateManager, according
		///	to the index provided.
		/// </returns>
		protected override object CreateKnownType(int index)
		{
			return new C1TreeMapItemBinding();
		}

		/// <summary>
		/// When overridden in a derived class, instructs an object contained by the
		///	collection to record its entire state to view state, rather than recording
		///	only change information.
		/// </summary>
		/// <param name="o">
		/// The System.Web.UI.IStateManager that should serialize itself completely.
		/// </param>
		protected override void SetDirtyObject(object o)
		{
			if (o is C1TreeMapItemBinding)
			{
				((C1TreeMapItemBinding)o).SetDirty();
			}
		}

		/// <summary>
		/// GetKnownTypes override.
		/// </summary>
		/// <returns></returns>
		protected override Type[] GetKnownTypes()
		{
			return C1TreeMapItemBindingCollection.s_knownTypes;
		}
		#endregion

		#region Private methods
		private void FindDefaultBinding()
		{
			this._defaultBinding = null;
			foreach (C1TreeMapItemBinding binding in this)
			{
				if ((binding.Depth == -1) && (binding.DataMember.Length == 0))
				{
					this._defaultBinding = binding;
					return;
				}
			}
		}
		#endregion

		#region Internal
		internal C1TreeMapItemBinding GetBinding(string dataMember, int depth)
		{
			C1TreeMapItemBinding binding = null;
			int num = 0;
			if ((dataMember != null) && (dataMember.Length == 0))
			{
				dataMember = null;
			}
			foreach (C1TreeMapItemBinding bind in this)
			{
				if (bind.Depth == depth)
				{
					if (string.Compare(bind.DataMember, dataMember, true, Thread.CurrentThread.CurrentCulture) == 0)
					{
						return bind;
					}
					if ((num < 1) && (bind.DataMember.Length == 0))
					{
						binding = bind;
						num = 1;
					}
				}
				if (string.Compare(bind.DataMember, dataMember, true, Thread.CurrentThread.CurrentCulture) == 0)
				{
					if (bind.Depth == depth)
					{
						return bind;
					}
					if ((num < 2) && (bind.Depth == -1))
					{
						binding = bind;
						num = 2;
					}
				}
			}
			if ((binding != null) || (this._defaultBinding == null))
			{
				return binding;
			}
			if ((this._defaultBinding.Depth != -1) || (this._defaultBinding.DataMember.Length != 0))
			{
				this.FindDefaultBinding();
			}
			return this._defaultBinding;
		}
		#endregion
	}
}
