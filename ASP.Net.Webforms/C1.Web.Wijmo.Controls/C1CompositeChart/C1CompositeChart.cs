﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Drawing;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.IO;
using System.Collections;
using System.Data;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	/// <summary>
	/// The C1CompositeChart class contains properties specific to CompositeChart charts.
	/// </summary>
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1CompositeChart.C1CompositeChartDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1CompositeChart.C1CompositeChartDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1CompositeChart.C1CompositeChartDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1CompositeChart.C1CompositeChartDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1CompositeChart runat=server ></{0}:C1CompositeChart>")]
	[ToolboxBitmap(typeof(C1CompositeChart), "C1CompositeChart.png")]
	[LicenseProviderAttribute()]
	public partial class C1CompositeChart : C1ChartCore<CompositeChartSeries, ChartAnimation, C1CompositeChartBinding>, IC1Serializable
	{

		#region ** fields
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion ** end of fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the C1CompositeChart class.
		/// </summary>
		public C1CompositeChart()
			: base()
		{
			VerifyLicense();
			this.InitChart();
		}

		private void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1CompositeChart), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY 
				_productLicensed = licinfo.IsValid; 
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion ** end of constructors.

		#region ** override methods

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		#endregion ** end of override methods.

		#region ** DataBinding

		private int _dataBindingIndex = -1;

		/// <summary>
		/// When overridden in a derived class, binds data from the data source to the
		///	control.
		/// </summary>
		/// <param name="dataSource"></param>
		protected override void PerformDataBinding(IEnumerable dataSource)
		{
			if (this.IsDesignMode || this.DataBindings.Count == 0 || dataSource == null)
			{
				return;
			}

			ResetSeriesList();
			sharedPieIndexHash = new Dictionary<string, int>();
			sharedPieSeriesHash = new Dictionary<int, CompositeSharedPieSeries>();

			// fixed issue 31018 by dail 2012-12-13
			for (int i = 0; i < DataBindings.Count; i++)
			{
				_dataBindingIndex = i;
				C1CompositeChartBinding binding = DataBindings[i];
				// just handle the dataset datasource, I don't know the datamember can use the other types.
				if (DataSource != null && DataSource.GetType() == typeof(DataSet) && !string.IsNullOrEmpty(binding.DataMember) && !IsBoundUsingDataSourceID)
				{
					this.DataMember = binding.DataMember;
					var ov = this.GetData();
					DataSourceViewSelectCallback cv = new DataSourceViewSelectCallback(OnDataSourceViewSelectCallBack);
					ov.Select(DataSourceSelectArguments.Empty, cv);
				}
				else {
					IEnumerator e = dataSource.GetEnumerator();

					while (e.MoveNext())
					{
						object o = e.Current;

						if (o != null)
						{
							PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
							// for sharedPie
							if (binding.Type == ChartSeriesType.SharedPie)
							{
								BindSharedPieData(o, pdc, binding);
							}
							else
							{
								int newIndex = i;
								
								// If contains shared pie binding, here should calculate the count of the series
								for (int idx = 0; idx < i && idx < SeriesList.Count; idx++)
								{
									if (SeriesList[idx].SharedPieChartSeries.Count > 0)
									{
										newIndex -= (SeriesList[idx].SharedPieChartSeries.Count - 1);
										idx += (SeriesList[idx].SharedPieChartSeries.Count - 1);
									}
								}

								if (newIndex >= SeriesList.Count)
								{
									SeriesList.Add(new CompositeChartSeries());
								}

								this.InternalDataBind(o, pdc, SeriesList[newIndex], binding);
							}
						}
					}
				}

			}
			//base.PerformDataBinding(dataSource);
		}

		private Dictionary<string, int> sharedPieIndexHash;
		private Dictionary<int, CompositeSharedPieSeries> sharedPieSeriesHash;

		private void BindSharedPieData(object data, PropertyDescriptorCollection pdc, C1CompositeChartBinding binding)
		{
			CompositeChartSeries series;
			if (!sharedPieIndexHash.ContainsKey(binding.SharedPieGroup))
			{
				series = new CompositeChartSeries();
				series.Type = binding.Type;
				SeriesList.Add(series);
				sharedPieIndexHash[binding.SharedPieGroup] = SeriesList.Count - 1;
			}
			else {
				series = this.SeriesList[sharedPieIndexHash[binding.SharedPieGroup]];
			}
			CompositeSharedPieSeries pieseriesList;
			if (!sharedPieSeriesHash.ContainsKey(_dataBindingIndex))
			{
				pieseriesList = new CompositeSharedPieSeries();
				series.SharedPieChartSeries.Add(pieseriesList);
				sharedPieSeriesHash[_dataBindingIndex] = pieseriesList;
			}
			else 
			{
				pieseriesList = sharedPieSeriesHash[_dataBindingIndex];
			}
			BindDataForPieSeries(data, pdc, pieseriesList, binding);
		}

		private void OnDataSourceViewSelectCallBack(IEnumerable obj)
		{
			IEnumerator e = obj.GetEnumerator();
			C1CompositeChartBinding binding = this.DataBindings[_dataBindingIndex];
			while (e.MoveNext())
			{
				object o = e.Current;
				if (o != null)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(o);
					if (binding.Type == ChartSeriesType.SharedPie)
					{
						BindSharedPieData(o, pdc, binding);
					}
					else 
					{
						int newIndex = _dataBindingIndex;						
						// If contains shared pie binding, here should calculate the count of the series
						for (int idx = 0; idx < _dataBindingIndex && idx < SeriesList.Count; idx++)
						{
							if (SeriesList[idx].SharedPieChartSeries.Count > 0)
							{
								newIndex -= (SeriesList[idx].SharedPieChartSeries.Count - 1);
								idx += (SeriesList[idx].SharedPieChartSeries.Count - 1);
							}
						}
						if (newIndex >= SeriesList.Count)
						{
							SeriesList.Add(new CompositeChartSeries());
						}
						InternalDataBind(o, pdc, SeriesList[newIndex], binding);
					}
				}
			}
		}

		private void BindDataForPieSeries(object data, PropertyDescriptorCollection pdc, CompositeChartSeries series, C1CompositeChartBinding binding)
		{
			PieChartSeries s = new PieChartSeries();

			if (!binding.Center.IsEmpty)
			{
				series.Center = binding.Center;
			}

			if (binding.Radius != 0)
			{
				series.Radius = binding.Radius;
			}

			if (!String.IsNullOrEmpty(binding.PieSeriesDataField))
			{
				object dataValue = GetFieldValue(data, pdc, binding.PieSeriesDataField);
				if (dataValue != null)
				{
					try
					{
						s.Data = Double.Parse(dataValue.ToString());
					}
					catch
					{
						throw new Exception("The PieSeriesDataField only can be bind to a number type field.");
					}
				}
			}

			if (!String.IsNullOrEmpty(binding.PieSeriesLabelField))
			{
				object labelValue = GetFieldValue(data, pdc, binding.PieSeriesLabelField);
				if (labelValue != null)
				{
					s.Label = labelValue.ToString();
				}
			}

			if (!String.IsNullOrEmpty(binding.PieSeriesOffsetField))
			{
				object offsetValue = GetFieldValue(data, pdc, binding.PieSeriesOffsetField);
				if (offsetValue != null)
				{
					try
					{
						s.Offset = Int32.Parse(offsetValue.ToString());
					}
					catch
					{
						throw new Exception("The PieSeriesOffsetField only can be bind to a number type field.");
					}
				}
			}
			series.PieSeriesList.Add(s);
		}

		private void BindDataForCandlestickSeries(object data, PropertyDescriptorCollection pdc, CompositeChartSeries series, C1CompositeChartBinding binding)
		{
			if (series.CandlestickSeries == null)
				series.CandlestickSeries = new CandlestickChartSeries();
			CandlestickChartSeriesData hlcsData = series.CandlestickSeries.Data;

			object xValue = GetFieldValue(data, pdc, binding.XField);
			AddDataToChart(binding.XFieldType.ToString(), xValue, hlcsData.X);

			object highValue = GetFieldValue(data, pdc, binding.CandlestickSeriesHighField);
			AddDoubleDataToChart(highValue, hlcsData.High);

			object lowValue = GetFieldValue(data, pdc, binding.CandlestickSeriesLowField);
			AddDoubleDataToChart(lowValue, hlcsData.Low);

			object openValue = GetFieldValue(data, pdc, binding.CandlestickSeriesOpenField);
			AddDoubleDataToChart(openValue, hlcsData.Open);

			object closeValue = GetFieldValue(data, pdc, binding.CandlestickSeriesCloseField);
			AddDoubleDataToChart(closeValue, hlcsData.Close);

			if (!String.IsNullOrEmpty(binding.Label))
			{
				series.Label = binding.Label;
			}
			series.LegendEntry = binding.LegendEntry;
			series.Visible = binding.Visible;

			if (!String.IsNullOrEmpty(binding.HintField))
			{
				object hintValue = GetFieldValue(data, pdc, binding.HintField);
				if (hintValue != null)
				{
					List<string> values = series.HintContents.ToList<string>();
					values.Add(hintValue.ToString());
					series.HintContents = values.ToArray();
				}
			}
		}


		protected override void InternalDataBind(object data, PropertyDescriptorCollection pdc, CompositeChartSeries series, C1CompositeChartBinding binding)
		{
			series.Type = binding.Type;
			series.YAxis = binding.YAxis;

			switch (binding.Type) 
			{ 
				case ChartSeriesType.Pie:
					BindDataForPieSeries(data, pdc, series, binding);
					break;
				case ChartSeriesType.Candlestick:
				case ChartSeriesType.Hl:
				case ChartSeriesType.Ohlc:
					BindDataForCandlestickSeries(data, pdc, series, binding);
					break;
				default:
					base.InternalDataBind(data, pdc, series, binding);
					break;
			}
		}
		#endregion ** end of DataBinding.

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1CompositeChartSerializer sz = new C1CompositeChartSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1CompositeChartSerializer sz = new C1CompositeChartSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1CompositeChartSerializer sz = new C1CompositeChartSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1CompositeChartSerializer sz = new C1CompositeChartSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations		
	}
}
