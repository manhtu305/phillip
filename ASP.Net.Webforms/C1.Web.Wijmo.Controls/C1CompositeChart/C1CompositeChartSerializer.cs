﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	/// <summary>
	/// Serializes C1CompositeChart Control.
	/// </summary>
	public class C1CompositeChartSerializer : C1ChartSerializer<C1CompositeChart>
	{

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the C1BarChartSerializer class.
		/// </summary>
		/// <param name="obj">
		/// The <see cref="C1CompositeChart"/> instance.
		/// </param>
		public C1CompositeChartSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor.
	}
}
