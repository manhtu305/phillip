﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;
using System.Drawing;

namespace C1.Web.Wijmo.Controls.C1Chart
{

	/// <summary>
	/// Defines the relationship between data item and the series data it is binding to.
	/// </summary>
	public class C1CompositeChartBinding : C1ChartBinding
	{

		#region ** fields
		private ChartSeriesType _type = ChartSeriesType.Column;
		private string _pieSeriesDataField = "";
		private string _pieSeriesOffsetField = "";
		private string _pieSeriesLabelField = "";
		#endregion ** end of fields.

		#region ** constructors

		/// <summary>
		/// Initialize a new instance of the C1CompositeChartBinding class.
		/// </summary>
		public C1CompositeChartBinding() 
			: base()
		{ }

		/// <summary>
		/// Initialize a new instance of the C1CompositeChartBinding class.
		/// </summary>
		/// <param name="dataMember">Data member to bind chart data.</param>
		public C1CompositeChartBinding(string dataMember)
			: base(dataMember)
		{ }
		#endregion ** end of constructors.

		#region ** properties
		/// <summary>
		/// A value specifies the type of the chart series.
		/// </summary>
		[C1Description("C1Chart.C1CompositeChartBinding.Type")]
		[C1Category("Category.Databinding")]
		[DefaultValue(ChartSeriesType.Column)]
		[Layout(LayoutType.Data)]
		public ChartSeriesType Type
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		/// <summary>
		/// A value specifies the center of the pie chart.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.C1CompositeChartBinding.Center")]
		[NotifyParentProperty(true)]
		[Layout(LayoutType.Appearance)]
		public Point Center { get; set; }

		/// <summary>
		/// A value specifies the radius of the pie chart.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.C1CompositeChartBinding.Radius")]
		[NotifyParentProperty(true)]
		[DefaultValue(0)]
		[Layout(LayoutType.Appearance)]
		public int Radius { get; set; }

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the data of the pie series.
		/// </summary>
        /// <remarks>
        /// The PieSeriesDataField only can be bind to a number field.
        /// </remarks>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.PieSeriesDataField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string PieSeriesDataField
		{
			get
			{
				return this._pieSeriesDataField;
			}
			set
			{
				this._pieSeriesDataField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the offset of the pie series.
        /// </summary>
        /// <remarks>
        /// The PieSeriesOffsetField only can be bind to a number field.
        /// </remarks>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.PieSeriesOffsetField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string PieSeriesOffsetField
		{
			get
			{
				return this._pieSeriesOffsetField;
			}
			set
			{
				this._pieSeriesOffsetField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the label of the pie series.
        /// </summary>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.PieSeriesLabelField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string PieSeriesLabelField
		{
			get
			{
				return this._pieSeriesLabelField;
			}
			set
			{
				this._pieSeriesLabelField = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the high of the candlestick series.
		/// </summary>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.CandlestickSeriesHighField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string CandlestickSeriesHighField { get; set; }

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the low of the candlestick series.
		/// </summary>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.CandlestickSeriesLowField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string CandlestickSeriesLowField { get; set; }

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the open of the candlestick series.
		/// </summary>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.CandlestickSeriesOpenField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string CandlestickSeriesOpenField { get; set; }

		/// <summary>
		/// Gets or sets the name of field from the data source that indicates the close of the candlestick series.
		/// </summary>
		[C1Category("Category.Databindings")]
		[C1Description("C1Chart.C1CompositeChartBinding.CandlestickSeriesCloseField")]
		[DefaultValue("")]
		[TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[Layout(LayoutType.Data)]
		public string CandlestickSeriesCloseField { get; set; }

        /// <summary>
        /// Hide this property for CompositeChart, CopositeChart leverage Type to check whether it's trendline.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool IsTrendline
        {
            get
            {
                return this.Type == ChartSeriesType.Trendline;
            }
        }

		/// <summary>
		/// A value used for bind data to sharedPie series, this value group the pie series.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.C1CompositeChartBinding.SharedPieGroup")]
		[NotifyParentProperty(true)]
		[Layout(LayoutType.Behavior)]
		public string SharedPieGroup
		{
			get;
			set;
		}

		/// <summary>
		/// A value used for binding data to YAxis of the compositechart series.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.C1CompositeChartBinding.YAxis")]
		[NotifyParentProperty(true)]
		[Layout(LayoutType.Behavior)]
		public int YAxis
		{
			get;
			set;
		}

        #endregion ** end of properties.

		#region ** ICloneable interface implementation
		/// <summary>
		/// Clone the chart binding.
		/// </summary>
		/// <returns>
		/// Returns the copy of a chart binding object.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override object Clone()
		{
			C1CompositeChartBinding binding = new C1CompositeChartBinding();
			binding.DataMember = this.DataMember;
			binding.XField = this.XField;
			binding.YField = this.YField;
			binding.HintField = this.HintField;
			binding.XFieldType = this.XFieldType;
			binding.YFieldType = this.YFieldType;
			binding.Type = this.Type;
			binding.Radius = this.Radius;
			binding.Center = this.Center;
			binding.PieSeriesDataField = this.PieSeriesDataField;
			binding.PieSeriesLabelField = this.PieSeriesLabelField;
			binding.PieSeriesOffsetField = this.PieSeriesOffsetField;

			return binding;
		}
		#endregion ** end of ICloneable interface implementation.
	}
}
