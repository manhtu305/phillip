﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Pager
{
	using C1.Web.Wijmo.Controls.Localization;

	/// <summary>
	/// Represents the C1Pager control.
	/// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Pager.C1PagerDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Pager.C1PagerDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Pager.C1PagerDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[DefaultProperty("PageIndex")]
	[DefaultEvent("PageIndexChanged")]
	[LicenseProvider()]
	[SupportsEventValidation]
	[ToolboxData("<{0}:C1Pager PageCount=10 runat=server></{0}:C1Pager>")]
	[ToolboxBitmap(typeof(C1Pager), "C1Pager.png")]
	public partial class C1Pager : C1TargetControlBase, IPostBackDataHandler, IPostBackEventHandler  
	{
		#region static

		private static object EventPageIndexChanged = new object();

		#endregion

		#region const

		private const bool DEF_AUTOPOSTBACK = false;
		private const string POSTBACK_EVENT_ARGS = "Page";

		#endregion

		#region fields

		private HtmlGenericControl _ul;
		
		private bool _isProductLicensed = false;
		private bool _shouldNag;

		#endregion

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1Pager"/> class.
		/// </summary>
		public C1Pager() : base(HtmlTextWriterTag.Div)
		{
			VerifyLicense();
		}

		#region properties

		/// <summary>
		/// Gets or sets a value that determines whether post back to server automatically when one of the pager buttons is clicked.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Pager.AutoPostBack")]
		[DefaultValue(false)]
		[Json(true, true, DEF_AUTOPOSTBACK)]
		public virtual bool AutoPostBack
		{
			get { return GetPropertyValue("AutoPostBack", DEF_AUTOPOSTBACK); }
			set { SetPropertyValue("AutoPostBack", value); }
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Never)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] 
		[Json(true, true, "")]
		public string PostbackReference
		{
			get
			{
				return (!DesignMode && Page != null && AutoPostBack)
					? Page.ClientScript.GetPostBackEventReference(this, POSTBACK_EVENT_ARGS)
					: string.Empty;
			}
		}

		#endregion

		#region events

		/// <summary>
		/// Occurs when one of the pager buttons is clicked, but after the <see cref="C1Pager"/> control handles the paging operation.
		/// </summary>
		[C1Category("Category.Action")]
		[C1Description("C1Pager.PageIndexChanged")]
		public event EventHandler PageIndexChanged
		{
			add { base.Events.AddHandler(C1Pager.EventPageIndexChanged, value); }
			remove { base.Events.RemoveHandler(C1Pager.EventPageIndexChanged, value); }
		}

		/// <summary>
		/// Raises the <see cref="PageIndexChanged"/> event.
		/// </summary>
		/// <param name="args">A <see cref="EventArgs"/> that contains event data.</param>
		protected virtual void OnPageIndexChanged(EventArgs args)
		{
			EventHandler handler = (EventHandler)base.Events[C1Pager.EventPageIndexChanged];
			if (handler != null)
			{
				handler(this, args);
			}
		}

		#endregion

		#region override

		/// <summary>
		/// Creates the control hierarchy used to render the <see cref="C1Pager"/> control.
		/// </summary>
		protected override void CreateChildControls()
		{
			Controls.Clear();

			base.CreateChildControls();

			if (DesignMode)
			{
				CreateDesignTimeControls();
			}
		}

		/// <summary>
		/// Adds the HTML attributes and styles of a <see cref="C1Pager"/> control to be rendered to
		/// the specified output stream.
		/// </summary>
		/// <param name="writer">An <see cref="HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = CssClass;

			if (DesignMode)
			{
				CssClass = "ui-widget wijmo-wijpager ui-helper-clearfix " + cssClass;
			}

			base.AddAttributesToRender(writer);

			if (DesignMode)
			{
				CssClass = cssClass;
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_isProductLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}

			base.OnInit(e);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_isProductLicensed, Page, this);

			if (!DesignMode && Page != null && AutoPostBack)
			{
				Page.RegisterRequiresRaiseEvent(this);
			}

			base.OnPreRender(e);
		}

		/// <summary>
		/// Renders the Web server control content to the client's browser using the specified <see cref="HtmlTextWriter"/> object.
		/// </summary>
		/// <param name="writer">The <see cref="HtmlTextWriter"/> used to render the server control content on the client's browser.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (DesignMode)
			{
				if (Width.IsEmpty)
				{
					writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "100%");
				}

				ChildControlsCreated = false;
				EnsureChildControls();
			}
			else
			{
				if (Page != null && AutoPostBack)
				{
					Page.ClientScript.RegisterForEventValidation(UniqueID, POSTBACK_EVENT_ARGS);
				}
			}

			base.Render(writer);
		}

		#endregion

		#region private

		private void CreateDesignTimeControls()
		{
			_ul = new HtmlGenericControl("ul");
			_ul.Attributes["class"] = "ui-list ui-corner-all ui-widget-content ui-helper-clearfix";

			_ul.Style.Add(HtmlTextWriterStyle.Width, "100%"); // design-time fix

			Controls.Add(_ul);

			switch (Mode)
			{
				case PagerMode.NextPrevious:
					CreateNextPrevPager(false);
					break;

				case PagerMode.NextPreviousFirstLast:
					CreateNextPrevPager(true);
					break;

				case PagerMode.Numeric:
					CreateNumericPager(false);
					break;

				case PagerMode.NumericFirstLast:
					CreateNumericPager(true);
					break;
			}
		}

		private void CreateNextPrevPager(bool addFirstLast)
		{
			// first button
			if (addFirstLast && (PageIndex > 0))
			{
				HtmlControl item = CreatePagerItem(false, FirstPageText);
				item.Controls.Add(CreatePagerControl(1, FirstPageText, FirstPageClass, false));
				_ul.Controls.Add(item);
			}

			// previous button
			if (PageIndex > 0)
			{
				HtmlControl item = CreatePagerItem(false, PreviousPageText);
				item.Controls.Add(CreatePagerControl(PageIndex, PreviousPageText, PreviousPageClass, false));
				_ul.Controls.Add(item);
			}

			// next button
			if (PageIndex + 1 < PageCount)
			{
				HtmlControl item = CreatePagerItem(false, NextPageText);
				item.Controls.Add(CreatePagerControl(PageIndex + 2, NextPageText, NextPageClass, false));
				_ul.Controls.Add(item);
			}

			// last button
			if (addFirstLast && (PageIndex + 1 < PageCount))
			{
				HtmlControl item = CreatePagerItem(false, LastPageText);
				item.Controls.Add(CreatePagerControl(PageCount, LastPageText, LastPageClass, false));
				_ul.Controls.Add(item);
			}
		}

		private void CreateNumericPager(bool addFirstLast)
		{
			int currentPage = PageIndex + 1;
			int startPagerNumber = 1;
			int endPageNumber = Math.Min(PageCount, PageButtonCount);

			if (currentPage > endPageNumber)
			{
				startPagerNumber = (int)(Math.Floor((double)(PageIndex / PageButtonCount))) * PageButtonCount + 1;

				endPageNumber = startPagerNumber + PageButtonCount - 1;
				endPageNumber = Math.Min(endPageNumber, PageCount);

				if (endPageNumber - startPagerNumber + 1 < PageButtonCount)
				{
					startPagerNumber = Math.Max(1, endPageNumber - PageButtonCount + 1);
				}
			}

			// first + "..." buttons
			if (startPagerNumber != 1)
			{
				HtmlControl item;

				if (addFirstLast)
				{
					item = CreatePagerItem(false, FirstPageText);
					item.Controls.Add(CreatePagerControl(1, FirstPageText, FirstPageClass, false));
					_ul.Controls.Add(item);
				}

				item = CreatePagerItem(false, "...");
				item.Controls.Add(CreatePagerControl(startPagerNumber - 1, "...", string.Empty, false));
				_ul.Controls.Add(item);
			}

			// page number buttons
			for (int i = startPagerNumber; i <= endPageNumber; i++)
			{
				HtmlControl item = CreatePagerItem(i == currentPage, i.ToString());
				item.Controls.Add(CreatePagerControl(i, i.ToString(), string.Empty, i == currentPage));
				_ul.Controls.Add(item);
			}

			// "..." + last button
			if (PageCount > endPageNumber)
			{
				HtmlControl item = CreatePagerItem(false, "...");
				item.Controls.Add(CreatePagerControl(endPageNumber + 1, "...", string.Empty, false));
				_ul.Controls.Add(item);

				if (addFirstLast)
				{
					item = CreatePagerItem(false, LastPageText );
					item.Controls.Add(CreatePagerControl(PageCount, LastPageText, LastPageClass, false));
					_ul.Controls.Add(item);
				}
			}
		}

		private HtmlGenericControl CreatePagerItem(bool active, string title)
		{
			HtmlGenericControl li = new HtmlGenericControl("li");

			li.Attributes["class"] = (active)
				? "wijmo-wijpager-button ui-corner-all ui-state-active"
				: "wijmo-wijpager-button ui-corner-all ui-state-default";

			li.Attributes["title"] = title;

			return li;
		}

		private HtmlContainerControl CreatePagerControl(int pageIndex, string btnText, string btnClass, bool disabled)
		{
			HtmlContainerControl ctrl = null;

			if (disabled)
			{
				ctrl = new HtmlGenericControl("span");
				ctrl.InnerText = btnText;
			}
			else
			{
				if (!string.IsNullOrEmpty(btnClass))
				{
					ctrl = new HtmlGenericControl("span");
					ctrl.Attributes["class"] = "ui-icon " + btnClass;
				}
				else
				{
					ctrl = new HtmlGenericControl("a");
					ctrl.Attributes["href"] = "#";
					ctrl.InnerText = btnText;
				}
			}

			ctrl.Attributes["title"] = btnText;

			return ctrl;
		}

		#endregion

		#region licensing support

		private void VerifyLicense()
		{
			var licInfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Pager), this, false);
			_shouldNag = licInfo.ShouldNag;
#if GRAPECITY
			_isProductLicensed = licInfo.IsValid;
#else
			_isProductLicensed = licInfo.IsValid || licInfo.IsLocalHost;
#endif
		}

		#endregion

		#region IPostBackDataHandler Members

		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			int pageIndex = PageIndex;

			System.Collections.Hashtable data = this.JsonSerializableHelper.GetJsonData(Page.Request.Form);
			this.RestoreStateFromJson(data);

			return pageIndex != PageIndex;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			OnPageIndexChanged(EventArgs.Empty);
		}

		#endregion

		#region IPostBackEventHandler Members

		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (Page != null)
			{
				Page.ClientScript.ValidateEvent(UniqueID, eventArgument);
			}
		}

		#endregion
	}
}
