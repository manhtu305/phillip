﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1Menu
{

    #region ** C1MenuItemCollectionChangedEventArgs
    /// <summary>
    /// C1MenuItemCollectionChangedEventArgs is used for the C1MenuItemCollectionChange event.
    /// </summary>
    public class C1MenuItemCollectionChangedEventArgs : EventArgs
    {
        #region fields

        private IC1MenuItemCollectionOwner _item;
        private ChangeStatus _status;

        #endregion

        #region constructor

        /// <summary>
		/// Initializes a new instance of the <see cref="C1MenuItemCollectionChangedEventArgs"/> class.
        /// </summary>
        /// <param name="item">The owner menu item</param>
        public C1MenuItemCollectionChangedEventArgs(IC1MenuItemCollectionOwner item)
        {
            _item = item;
        }

        #endregion

        #region properties

        /// <summary>
        /// indicate the item which to be add or remove from the C1ToolBarItemCollection.
        /// </summary>
        public IC1MenuItemCollectionOwner Item
        {
            get { return _item; }
        }

        /// <summary>
        /// indicate the ChangeStatus of the C1ToolBarItemCollection.
        /// </summary>
        public ChangeStatus Status
        {
            get { return _status; }
            internal set { _status = value; }
        }

        #endregion
    }

    /// <summary>
    /// Specifies <see cref="IC1MenuItemCollectionOwner"/> change status.
    /// </summary>
    public enum ChangeStatus
    {
        /// <summary>
        /// <see cref="IC1MenuItemCollectionOwner"/> add.
        /// </summary>
        ItemAdd = 0,
        /// <summary>
        /// <see cref="IC1MenuItemCollectionOwner"/> remove.
        /// </summary>
        ItemRemove = 1
    }
    #endregion

    #region ** C1MenuEventHander
    /// <summary>
    /// Delegate type for handling events that are related to the items.
    /// </summary>
    /// <param name="sender">The source of the event</param>
    /// <param name="e">C1MenuItemArgs object that contains the event data</param>
    public delegate void C1MenuEventHandler(object sender, C1MenuEventArgs e);

    /// <summary>
    /// C1MenuEventArgs is used by the ItemClick event.
    /// </summary>
    public sealed class C1MenuEventArgs : CommandEventArgs
    {

        #region ** fields
        private object _commandSource;
        private C1MenuItem _item;
        #endregion

        #region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1MenuEventArgs"/> class.
		/// </summary>
		/// <param name="item">Menu item that raised the event.</param>
        public C1MenuEventArgs(C1MenuItem item)
            : this(item, null, new CommandEventArgs(string.Empty, null))
        {

        }

        //public C1MenuEventArgs(C1MenuItemBase item, object commandSource, CommandEventArgs originalArgs)
        /// <summary>
		/// Initializes a new instance of the <see cref="C1MenuEventArgs"/> class..
        /// </summary>
        /// <param name="item">Menu item that raised the event.</param>
        /// <param name="commandSource">Object that raised the event.</param>
        /// <param name="originalArgs">A System.Web.UI.WebControls.CommandEventArgs that contains the event data.</param>
        public C1MenuEventArgs(C1MenuItem item, object commandSource, CommandEventArgs originalArgs)
            : base(originalArgs)
        {
            this._item = item;
            this._commandSource = commandSource;
        }

        #endregion

        #region ** properties
        /// <summary>
        /// Gets the Object that raised the event.
        /// </summary>
        public object CommandSource
        {
            get
            {
                return this._commandSource;
            }
        }

        //public C1MenuItemBase Item
        ///<summary>
        /// Gets the menu item that raised the event.
        ///</summary>
        public C1MenuItem Item
        {
            get
            {
                return this._item;
            }
        }
        #endregion
    }
    #endregion

    //#region ** C1ItemPropertyChangedEventArgs

    //public class C1ItemPropertyChangedEventArgs : EventArgs
    //{

    //    #region ** fields
    //    private readonly C1MenuItem _item;
    //    private readonly string _propertyName;
    //    private object _newValue;
    //    #endregion

    //    #region properties
    //    /// <summary>
    //    /// An item in the control which raised an event.
    //    /// </summary>
    //    public C1MenuItem Item
    //    {
    //        get
    //        {
    //            return this._item;
    //        }
    //    }

    //    /// <summary>
    //    /// Property name.
    //    /// </summary>
    //    public string PropertyName
    //    {
    //        get
    //        {
    //            return _propertyName;
    //        }
    //    }

    //    /// <summary>
    //    /// New value of the property.
    //    /// </summary>
    //    public object NewValue
    //    {
    //        get
    //        {
    //            return _newValue;
    //        }
    //    }
    //    #endregion

    //    internal C1ItemPropertyChangedEventArgs(C1MenuItem item, string propertyName, object newValue)
    //        :base()
    //    {
    //        _item = item;
    //        _propertyName = propertyName;
    //        _newValue = newValue;
    //    }
    //}

    //public delegate void C1ItemPropertyChangedEventHandler(object sender,C1ItemPropertyChangedEventArgs e);
    //#endregion
}
