﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base.Collections;
using System.ComponentModel;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1Menu
{
	/// <summary>
	/// Represents a collection of C1MenuItemBase objects that are used by Items property.
	/// </summary>
	public class C1MenuItemCollection : C1ObservableItemCollection<IC1MenuItemCollectionOwner, C1MenuItem>
	{

		#region ** fields
		private EventHandlerList _events;
		private string CollectionChangedEvent = "CollectionChangedEvent";
		private string BeforeCollectionChangedEvent = "BeforeCollectionChangedEvent";
		#endregion

		#region constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1MenuItemCollection"/> class.
		/// </summary>
		/// <param name="owner">The owner object of the collection.</param>
		public C1MenuItemCollection(IC1MenuItemCollectionOwner owner)
			: base(owner)
		{
			_events = new EventHandlerList();
		}
		#endregion

		#region properties
		/// <summary>
		/// Events of C1MenuItemCollection Change.
		/// </summary>
		public new EventHandlerList Events
		{
			get
			{
				return _events;
			}
		}
		#endregion

		#region ** Event handlers

		/// <summary>
		/// Occurs after <see cref="C1MenuItemCollection"/> collection was changed.
		/// </summary>
		public event EventHandler OnCollectionChanged
		{
			add { _events.AddHandler(CollectionChangedEvent, value); }
			remove { _events.RemoveHandler(CollectionChangedEvent, value); }
		}

		/// <summary>
		/// Occurs before <see cref="C1MenuItemCollection"/> collection was changed.
		/// </summary>
		internal event EventHandler OnBeforeCollectionChanged
		{
			add { _events.AddHandler(BeforeCollectionChangedEvent, value); }
			remove { _events.RemoveHandler(BeforeCollectionChangedEvent, value); }
		}

		private void RaiseCollectionChangedEvent(IC1MenuItemCollectionOwner item, ChangeStatus status)
		{
			EventHandler handler = (EventHandler)Events[CollectionChangedEvent];
			if (handler != null)
			{
				C1MenuItemCollectionChangedEventArgs e = new C1MenuItemCollectionChangedEventArgs(item);
				e.Status = status;
				handler(this, e);
			}
		}

		private void RaiseBeforeCollectionChangedEvent(IC1MenuItemCollectionOwner item, ChangeStatus status)
		{
			EventHandler handler = (EventHandler)Events[BeforeCollectionChangedEvent];
			if (handler != null)
			{
				C1MenuItemCollectionChangedEventArgs e = new C1MenuItemCollectionChangedEventArgs(item);
				e.Status = status;
				handler(this, e);
			}
		}
		#endregion

		#region ** methods
		/// <summary>
		/// Removes all items from collection.
		/// </summary>
		public new void Clear()
		{
			while (this.Count > 0)
			{
				Remove(this[0]);
			}
			//base.ClearItems();
		}

		/// <summary>
		/// Adds new menu item to the end of the list.
		/// </summary>
		/// <param name="child">Child menu item</param>
		public new void Add(C1MenuItem child)
		{
			if (child != null)
			{
				this.Insert(this.Count, child);
			}
		}

		/// <summary>
		/// Insert <see cref="C1MenuItem"/> item to specific position into the collection.
		/// </summary>
        /// <param name="index">Position, value  should be mayor or equal to 0</param>
		/// <param name="child">Child menu item</param>
		public new void Insert(int index, C1MenuItem child)
		{
			child.SetParent(Owner);

			RaiseBeforeCollectionChangedEvent(child, ChangeStatus.ItemAdd);

			if (this.Count == 0)
				this.InsertItem(0, child);
			else
				this.InsertItem(index, child);

			RaiseCollectionChangedEvent(child, ChangeStatus.ItemAdd);
		}

		/// <summary>
		/// Removes item from list.
		/// </summary>
		/// <param name="child">Child menu item.</param>
		public new void Remove(C1MenuItem child)
		{
			RemoveAt(IndexOf(child));
		}

		/// <summary>
		/// Removes item from list by index.
		/// </summary>
		/// <param name="index">An index that indicates which submenu to remove.</param>
		public new void RemoveAt(int index)
		{
			IC1MenuItemCollectionOwner item = this[index];
			RaiseCollectionChangedEvent(item, ChangeStatus.ItemRemove);

			this.RemoveItem(index);

			RaiseCollectionChangedEvent(item, ChangeStatus.ItemRemove);
		}

		#endregion

		#region ** IJsonEmptiable Members

		/// <summary>
		/// Gets value that indicates if the object is empty or not.
		/// If object is empty than it will be skipped during the serialization process.
		/// </summary>
		/// <value></value>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool IsEmpty
		{
			get
			{
				return (this.Count <= 0);
			}
		}

		#endregion
	}
}
