﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Collections;
using System.Reflection;
using System.Web.UI.HtmlControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Menu
{

	/// <summary>
	/// Represents a Menu in an ASP.NET Web page.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Menu.C1MenuDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Menu.C1MenuDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Menu.C1MenuDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Menu.C1MenuDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Menu runat=server></{0}:C1Menu>")]
	[ToolboxBitmap(typeof(C1Menu), "C1Menu.png")]
	[LicenseProvider()]
	public partial class C1Menu : C1TargetHierarchicalDataBoundControlBase,
		INamingContainer, IC1MenuItemCollectionOwner, IPostBackDataHandler, IPostBackEventHandler, IC1Serializable
	{

		#region ** fields
		private C1MenuItemCollection _items;
		private bool _subControlsDataBound;
		private bool _isDirty = false;
		private C1MenuItemBindingCollection _bindings;
		private int _dataBindStartLevel = 0;
		internal string _postBackEventReference;
		internal C1MenuEventArgs _postback_event_ItemClick = null;
		private List<C1MenuItemTemplateContainer> _allTemplateContainers = new List<C1MenuItemTemplateContainer>();
		private ITemplate _itemsTemplate;
		private ITemplate _childItemsTemplate;
		private ITemplate _topItemsTemplate;
		private Panel superpanel;
		HtmlGenericControl menuContainer;
		internal bool ChildItemCreated = false;

		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1Menu"/> class.
		/// </summary>
		public C1Menu() 
		{
			VerifyLicense();
			this.InitMenu();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Menu), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

//                /// <summary>
//        /// Initializes a new instance of the <see cref="C1Menu"/> class.
//        /// </summary>
//        /// <param name="key">The key.</param>
//        [EditorBrowsable(EditorBrowsableState.Never)]
//        public C1Menu(string key)
//        {
//#if GRAPECITY
//            _productLicensed = true;
//#else
//            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Menu), this,
//                Assembly.GetExecutingAssembly(), key);
//            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
//#endif
//            this.InitMenu();
//        }

		private void InitMenu()
		{
			this.HideAnimation = new Animation();
			this.HideAnimation.Animated = new AnimatedOption() { Effect = "fade" };
		}
		#endregion

		#region ** properties

		/// <summary>
		/// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1Menu.AutoPostBack")]
		public bool AutoPostBack
		{
			get
			{
				return this.ViewState["AutoPostBack"] == null ? false : (bool)this.ViewState["AutoPostBack"];
			}
			set
			{
				this.ViewState["AutoPostBack"] = value;
			}
		}

		/// <summary>
		/// Gets the postback event reference. send it to client side. 
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable( EditorBrowsableState.Never)]
		[Json(true)]
		public string PostBackEventReference
		{
			get 
			{
				return _postBackEventReference;
			}            
		}

		/// <summary>
		/// Gets the menu items.
		/// </summary>
		[Layout(LayoutType.Appearance)]
		[C1Description("C1Menu.Items")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[RefreshProperties(RefreshProperties.All)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public C1MenuItemCollection Items
		{
			get
			{
				if (_items == null)
				{
					_items = new C1MenuItemCollection(this);
				}
				return _items;
			}
		}

		/// <summary>
		/// Owner of the menu. 
		/// Always returns null.
		/// </summary>
		[Browsable(false)]
		public IC1MenuItemCollectionOwner Owner
		{
			get
			{
				return null;
			}
		}

		/// <summary>
		/// Gets or sets a value indicates whether this instance is dirty.
		/// </summary>
		[Browsable(false)]
		[DesignOnly(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsDirty
		{
			get
			{
				return this._isDirty;
			}
			set
			{
				this._isDirty = value;
			}
		}

		/// <summary>
		/// Gets or sets the height of the menu.
		/// </summary>
		[WidgetOption]
		[Layout(LayoutType.Sizes)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the width of the menu.
		/// </summary>
		[WidgetOption]
		[Layout(LayoutType.Sizes)]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// If specified, this template will be applied for all menu items that does not have other defined templates.
		/// </summary>
		[C1Description("C1Menu.ItemsTemplate")]
		[C1Category("Category.Behavior")]
		[TemplateContainer(typeof(C1MenuItemTemplateContainer))]
		[Browsable(false)]
		[Bindable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public virtual ITemplate ItemsTemplate
		{
			get
			{
				return this._itemsTemplate;
			}
			set
			{
				this._itemsTemplate = value;
			}
		}

		/// <summary>
		/// If specified, this template will be applied for all menu items instead of top level menu items.
		/// </summary>
		[C1Description("C1Menu.ChildItemsTemplate")]
		[C1Category("Category.Behavior")]
		[TemplateContainer(typeof(C1MenuItemTemplateContainer))]
		[Browsable(false)]
		[Bindable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public virtual ITemplate ChildItemsTemplate
		{
			get
			{
				return this._childItemsTemplate;
			}
			set
			{
				this._childItemsTemplate = value;
			}
		}

		/// <summary>
		/// If specified, this template will be applied for top level menu items.
		/// </summary>
		[C1Description("C1Menu.TopItemsTemplate")]
		[C1Category("Category.Behavior")]
		[TemplateContainer(typeof(C1MenuItemTemplateContainer))]
		[Browsable(false)]
		[Bindable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateInstance(TemplateInstance.Single)]
		public virtual ITemplate TopItemsTemplate
		{
			get
			{
				return this._topItemsTemplate;
			}
			set
			{
				this._topItemsTemplate = value;
			}
		}

		#region ** hidden properties

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override BorderStyle BorderStyle
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit BorderWidth
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override FontInfo Font
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		/// This property is hidden.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		#endregion

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region ** override methods
		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			
			base.OnInit(e);
			if (!IsDesignMode && (!string.IsNullOrEmpty(this.DataSourceID) || (this.DataSource != null)))
			{
				this.RequiresDataBinding = true;
			}
			AddSysStaticKey(this.Items, 0);
		}

		private int AddSysStaticKey(C1MenuItemCollection collection, int index)
		{
			foreach (C1MenuItem item in collection)
			{
				item.StaticKey = "sk" + index.ToString();
				index++;
				IC1MenuItemCollectionOwner own = item as IC1MenuItemCollectionOwner;
				if (own.Items != null && own.Items.Count > 0)
				{
					index = AddSysStaticKey(own.Items, index);
				}
			}
			return index;
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
			if (!this.IsDesignMode)
			{
				if (this.Page != null)
				{
					this.Page.RegisterRequiresRaiseEvent(this);
					this.Page.RegisterRequiresPostBack(this);
					_postBackEventReference = this.Page.ClientScript.GetPostBackEventReference(this, "_Args");
				}
			}
		}

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				if (IsDesignMode && this.Mode == MenuMode.Sliding)
				{
					return HtmlTextWriterTag.Div;
				}
				return HtmlTextWriterTag.Ul;
			}
		}

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This
		/// method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			Unit _oldHeight = Height;
			//string cssClass = string.Format("{0} {1}", this.CssClass, "ui-helper-hidden-accessible");
			if (IsDesignMode)
			{
				if (Mode == MenuMode.Flyout)
				{
					string menucss = "ui-widget ui-widget-header wijmo-wijmenu ui-corner-all ui-helper-clearfix";
					if (this.Orientation == Wijmo.Controls.Orientation.Horizontal)
					{
						menucss += " wijmo-wijmenu-horizontal";
						if (Height == Unit.Empty)
						{
							Height = 40;
						}
					}
					writer.AddAttribute(HtmlTextWriterAttribute.Class, menucss);
					writer.RenderBeginTag(HtmlTextWriterTag.Div);
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "scrollcontainer checkablesupport");
					writer.RenderBeginTag(HtmlTextWriterTag.Div);
					string listCss = "wijmo-wijmenu-list ui-helper-reset";
					if (Direction == Direction.Rtl) 
					{
						listCss += " wijmo-wijmenu-rtl";
					}
					writer.AddAttribute(HtmlTextWriterAttribute.Class, listCss);
					//base.RenderBeginTag(writer);
					//Height = _oldHeight;
				}
				else
				{

					writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-widget ui-widget-header wijmo-wijmenu ui-corner-all ui-helper-clearfix wijmo-wijmenu-ipod wijmo-wijmenu-container");
					writer.AddStyleAttribute(HtmlTextWriterStyle.OverflowY, "auto");
				}
			}
			else
			{
				string cssClass = string.Format("{0} {1}", this.CssClass, Utils.GetHiddenClass());
				writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
			}
			
			base.RenderBeginTag(writer);
			Height = _oldHeight;
		}

		/// <summary>
		/// Renders the HTML closing tag of the control into the specified writer. This
		/// method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render
		/// HTML content on the client.
		/// </param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{
			base.RenderEndTag(writer);
			if (IsDesignMode)
			{
				if (Mode == MenuMode.Flyout)
				{
					writer.RenderEndTag();
					writer.RenderEndTag();
				}                
			}
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">
		/// The System.Web.UI.HtmlTextWriter object that receives the control content.
		/// </param>
		protected override void Render(HtmlTextWriter writer)
		{
			EnsureChildControls();
			base.Render(writer);
		}

		/// <summary>
		/// Determines whether the server control contains child controls. If it does
		/// not, it creates child controls.
		/// </summary>
		protected override void EnsureChildControls()
		{
			base.EnsureChildControls();
			if (this.Controls.Count < 1 && this.Items.Count > 0 || this.IsDirty)
			{
				this.CreateChildControls();
				this.IsDirty = false;
			}
		}

		protected override void EnsureEnabledState()
		{
			return;
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for
		/// posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();
			if (base.RequiresDataBinding && (!string.IsNullOrEmpty(this.DataSourceID) || (this.DataSource != null)))
			{
				this.EnsureDataBound();
			}
			else
			{
				this.CreateChildControlsFromItems(false);
				this.ClearChildViewState();
			}
		}

		/// <summary>
		/// Calls the System.Web.UI.WebControls.BaseDataBoundControl.DataBind() method
		/// if the System.Web.UI.WebControls.BaseDataBoundControl.DataSourceID property
		/// is set and the data-bound control is marked to require binding.
		/// </summary>
		protected override void EnsureDataBound()
		{
			base.EnsureDataBound();
			if (!this._subControlsDataBound)
			{
				foreach (Control c in this.Controls)
				{
					c.DataBind();
				}
				foreach (C1MenuItemTemplateContainer c in this._allTemplateContainers)
				{
					c.DataBind();
				}
				this._subControlsDataBound = true;
			}
		}


		/// <summary>
		/// Outputs the content of a server control's children to a provided System.Web.UI.HtmlTextWriter
		/// object, which writes the content to be rendered on the client.
		/// </summary>
		/// <param name="writer">
		/// The System.Web.UI.HtmlTextWriter object that receives the rendered content.
		/// </param>
		protected override void RenderChildren(HtmlTextWriter writer)
		{
			foreach (Control control in Controls)
			{

				if (control is C1MenuItem || (IsDesignMode&& Mode== MenuMode.Sliding && control is Panel))
				{
					control.RenderControl(writer);
				}
			}
		}
		#endregion

		#region ** Data Binding


		[C1Category("Category.Data")]
		[C1Description("C1Menu.DataBindings")]
		[DefaultValue((string)null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public C1MenuItemBindingCollection DataBindings
		{
			get
			{
				if (this._bindings == null)
				{
					this._bindings = new C1MenuItemBindingCollection(this);
				}
				return this._bindings;
			}
		}

		/// <summary>
		/// Override PerformDataBinding.
		/// </summary>
		protected override void PerformDataBinding()
		{
			base.PerformDataBinding();
			if (!IsBoundUsingDataSourceID && (DataSource == null))
			{
				CreateChildControlsFromItems(false);
				return;
			}
			if (!IsBoundUsingDataSourceID && DataSource.GetType().Name == "HierarchicalSampleData")
			{
				CreateChildControlsFromItems(true);
				return;
			}
			HierarchicalDataSourceView view = this.GetData(null);
			if (view == null)
			{
				throw new InvalidOperationException("NoDataSourceView");
			}
			IHierarchicalEnumerable enumerable = view.Select();
			_dataBindStartLevel = 0;
			if (this.DataBindings.Count > 0 && this.DataBindings[0].Depth == 1)
			{
				object o = null;
				foreach (object o1 in enumerable)
				{
					o = o1;
					break;
				}
				if (o != null)
				{
					IHierarchyData data = enumerable.GetHierarchyData(o);
					enumerable = data.GetChildren();
					_dataBindStartLevel = 1;
				}
			}
			CreateItemsFromDataSource(enumerable);
			CreateChildControlsFromItems(true);
			ChildControlsCreated = true;            
		}

		/// <summary>
		/// Create the items from data source.
		/// </summary>
		/// <param name="enumerable"></param>
		private void CreateItemsFromDataSource(IHierarchicalEnumerable enumerable)
		{
			this.Items.Clear();
			foreach (object o in enumerable)
			{
				IHierarchyData data = enumerable.GetHierarchyData(o);
				string text = data.Type;
				C1MenuItemBinding binding = this.GetBinding(text, GetDataBindStartLevel());
				C1MenuItem item = this.CreateMenuItem();
				item.DataBind(binding, enumerable, o, GetDataBindStartLevel(), this.IsBindingsNullOrEmpty());
				this.Items.Add(item);
				if (data.HasChildren)
				{
					IHierarchicalEnumerable childEnum = data.GetChildren();
					if (childEnum != null)
					{
						item.DataBindInternal(childEnum, 1, binding);
					}
				}
			}
		}

		internal C1MenuItemBinding GetBinding(string dataMember, int depth)
		{
			if (this._bindings == null)
			{
				return null;
			}
			return this._bindings.GetBinding(dataMember, depth);
		}

		internal int GetDataBindStartLevel()
		{
			return _dataBindStartLevel;
		}

		internal bool IsBindingsNullOrEmpty()
		{
			if (this._bindings == null) return true;
			return this._bindings.Count == 0;
		}

		internal C1MenuItem CreateMenuItem()
		{
			return new C1MenuItem();
		}
		#endregion

		#region ** private methods

		private void LoadChildItemsFromJsonData(IC1MenuItemCollectionOwner owner, ArrayList jsonItems)
		{
			if (jsonItems == null) 
			{
				return;
			}
			C1MenuItemCollection items = new C1MenuItemCollection(owner);
			if (jsonItems != null && jsonItems.Count > 0)
			{
				foreach (object o in jsonItems)
				{
					Hashtable jsonItem=(Hashtable)o;
					C1MenuItem curItem = new C1MenuItem();
					if (jsonItem["staticKey"] != null && jsonItem["staticKey"].ToString().Length > 0)
					{
						FindAndReplaceInitItems(ref curItem, jsonItem["staticKey"].ToString(), _items);
					}
					items.Add(curItem);
					JsonRestoreHelper.RestoreStateFromJson(curItem, jsonItem);

					//Added by eric 2012-11-21 for bug 30098
					string navigateUrl = (string)jsonItem["navigateUrl"];
					string navigateUrlHelper = (string)jsonItem["navigateUrlHelper"];
					if (!String.IsNullOrEmpty(navigateUrlHelper) && base.ResolveClientUrl(navigateUrlHelper) == navigateUrl)
					{
						curItem.NavigateUrl = navigateUrlHelper;
					}
					//end
					//fix #36004, if navigateUrl is "#", set it to ""
					if (curItem.NavigateUrl == "#")
					{
						curItem.NavigateUrl = "";
					}
					//end comments

					if (jsonItem["items"] != null)
					{
						LoadChildItemsFromJsonData(curItem, (ArrayList)jsonItem["items"]);
					}

					
				}
			}
			if (owner is C1Menu)
			{
				_items = items;
				items.OnCollectionChanged += new EventHandler(items_OnCollectionChanged);
				items.OnBeforeCollectionChanged += new EventHandler(items_OnBeforeCollectionChanged);
			}
			else
			{
				((C1MenuItem)owner).SetItems(items);
			}
		}

		void items_OnBeforeCollectionChanged(object sender, EventArgs e)
		{
			
		}

		void items_OnCollectionChanged(object sender, EventArgs e)
		{
			C1MenuItemCollectionChangedEventArgs args = (C1MenuItemCollectionChangedEventArgs)e;
			if (args != null)
			{
				if (!ChildControlsCreated)
				{
					return;
				}
				Control con = args.Item as Control;
				if (con == null)
				{
					return;
				}
				if (args.Status == ChangeStatus.ItemAdd)
				{
					this.Controls.Add(con);
				}
				else if (args.Status == ChangeStatus.ItemRemove)
				{
					this.Controls.Remove(con);
				}
			}
		}

		private bool FindAndReplaceInitItems(ref C1MenuItem item, string strStaticKey, C1MenuItemCollection tarItems)
		{
			for (int i = 0; i < tarItems.Count; i++)
			{
				if (tarItems[i].StaticKey == strStaticKey)
				{
					//tarItems.Remove(it);
					item = tarItems[i];
					return true;
				}
				C1MenuItem itemBase = tarItems[i];
				if (itemBase.Items != null && itemBase.Items.Count > 0)
				{
					if (FindAndReplaceInitItems(ref item, strStaticKey, itemBase.Items))
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Create child controls from items.
		/// </summary>
		/// <param name="dataBinding">A value that determines whether is databinding</param>
		private void CreateChildControlsFromItems(bool dataBinding)
		{
			this.Controls.Clear();
			this._allTemplateContainers = new List<C1MenuItemTemplateContainer>();

			bool isFlyoutDesigner = IsDesignMode && Mode == MenuMode.Sliding;
			if (isFlyoutDesigner)
			{
				superpanel = new Panel();
				menuContainer = new HtmlGenericControl("ul");
				if (this.Height == Unit.Empty)
				{
					superpanel.Height = 200;
				}
				else
				{
					superpanel.Height = this.Height;
				}
				superpanel.CssClass = "scrollcontainer checkablesupport wijmo-wijsuperpanel ui-widget ui-widget-content ui-corner-all";
				superpanel.Style.Add(HtmlTextWriterStyle.OverflowY, "scroll");
				menuContainer.Attributes["class"] = "wijmo-wijmenu-list ui-helper-reset wijmo-wijmenu-content wijmo-wijmenu-current ui-widget-content ui-helper-clearfix";
				superpanel.Controls.Add(menuContainer);
				this.Controls.Add(superpanel);
			}

			for (int i = 0; i < Items.Count; i++)
			{
				C1MenuItem item = Items[i];
				if (isFlyoutDesigner)
				{
					menuContainer.Controls.Add(item);
				}
				else
				{
					this.Controls.Add(item);
				}
				CreateChildControlsFromItems(item, i, 0, dataBinding);               
			} 
			ChildControlsCreated = true;
		}

		private void CreateChildControlsFromItems(C1MenuItem item, int position, int depth, bool dataBinding)
		{
			ITemplate template = item.Template;
			if (template == null)
			{
				if (item.Owner is C1Menu)
				{
					template = this.TopItemsTemplate;
				}
				else
				{
					template = this.ChildItemsTemplate;
				}
			}
			if (template == null)
			{
				template = this.ItemsTemplate;
			}

			if (template != null)
			{
				item.IsTemplated = true;
				C1MenuItemTemplateContainer container = new C1MenuItemTemplateContainer(position, item);
				item.TemplateContainer = container;
				template.InstantiateIn(container);
				this.Controls.Add(container);
				if (dataBinding)
				{
					container.DataBind();
				}
				_allTemplateContainers.Add(container);
			}
			else
			{
				item.IsTemplated = false;
			}

			if (IsDesignMode)
			{
				return;
			}
			ChildItemCreated = true;
			for (int i = 0; i < item.Items.Count; i++)
			{
				CreateChildControlsFromItems(item.Items[i], i, depth + 1, dataBinding);
			}
		}
		#endregion

		#region ** Events
		/// <summary>
		/// Fires when an item is clicked
		/// </summary>
        [C1Category("Category.Events")]
		[C1Description("C1Menu.ItemClick")]
		[Browsable(true)]
		public event C1MenuEventHandler ItemClick;
		
		/// <summary>
		/// Raise ItemClick event.
		/// </summary>
		/// <param name="e">A C1MenuEventArgs object that contains the event data.</param>
		public virtual void OnItemClick(C1MenuEventArgs e)
		{
			if (this.ItemClick != null)
			{
				this.ItemClick(this, e);
			}
		}

		///// <summary>
		///// Raise ItemPropertyChanged event.
		///// </summary>
		///// <param name="e">A C1ItemPropertyChangedEventArgs object that contains the event data.</param>
		//internal void OnItemPropertyChanged(C1ItemPropertyChangedEventArgs e)
		//{
		//    if (this.ItemPropertyChanged != null)
		//    {
		//        this.ItemPropertyChanged(this, e);
		//    }
		//}
		
		#endregion

		#region ** IPostBackDataHandler interface implementations

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeItems(object obj)
		{ 
		
		}


		/// <summary>
		/// Processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(Page.Request.Form);
			this.RestoreStateFromJson(data);
			LoadChildItemsFromJsonData(this, (ArrayList)data["items"]);
			return true;
		}

		/// <summary>
		/// Notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			InternalRaisePostbackServerEvents();
		}

		private void InternalRaisePostbackServerEvents()
		{
			if (_postback_event_ItemClick != null)
			{
				OnItemClick(_postback_event_ItemClick);
				_postback_event_ItemClick = null;
			}
		}

		private bool NeedPostback
		{
			get
			{
				return Enabled && AutoPostBack;
			}
		}

		internal string DetermineHrefAttribute(C1MenuItem c1MenuItem)
		{
			string str =  base.ResolveClientUrl(c1MenuItem.NavigateUrl);

			//add UrlPropertyAttribute to C1MenuItem's NavigateUrl property instead of this.
			//add comments to fix tfs issue 27996
			//if (str != c1MenuItem.NavigateUrl)
			//{
			//    c1MenuItem.NavigateUrl = str;
			//}
			//end comments
			if (!c1MenuItem.Enabled||!ParentEnabled((IC1MenuItemCollectionOwner) c1MenuItem))
			{
				return "#";
			}
			//Fix #36004
			if (NeedPostback && (str == String.Empty || str == "#"))
			//if (NeedPostback && str.Length == 0)
			//end comments
			{
				return "javascript:" + _postBackEventReference;
			}
			else
			{
				if (str.Length > 0)
				{
					return str;
				}
				else
				{
					return "#";
				}
			}
		}

		private bool ParentEnabled(IC1MenuItemCollectionOwner item)
		{
			if (item.Owner is C1Menu)
			{
				return ((C1Menu)item.Owner).Enabled;
			}
			else
			{
				C1MenuItem itembase = (C1MenuItem)item.Owner;
				if (!itembase.Enabled)
				{
					return false;
				}
				else
				{
					return ParentEnabled(item.Owner);
				}
			}
		}

		private C1MenuItem FindMenuItem(string path)
		{
			string[] arrPath = path.Split('-');
			C1MenuItemCollection _gitems=Items;
			C1MenuItem item = null;
			int index = 0;
			foreach (string s in arrPath)
			{
				int.TryParse(s, out index);
				item = _gitems[index];
				_gitems = item.Items;
			}
			if (item == null)
			{
				return null;
			}
			return item;
		}
		#endregion
		
		#region ** IPostBackEventHandler interface implementations

		/// <summary>
		/// Process an event
		/// raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument">A System.String that represents an optional event argument to be passed to the event hander.</param>
		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("_ItemClick_"))
			{
				string path = eventArgument.Replace("_ItemClick_", "");
				C1MenuItem item = FindMenuItem(path);
				if (item != null)
				{
					_postback_event_ItemClick = new C1MenuEventArgs(item);
				}
			}

			InternalRaisePostbackServerEvents();
		}
		#endregion

		#region ** IC1Serializable Layout
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1MenuSerializer sz = new C1MenuSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(System.IO.Stream stream)
		{
			C1MenuSerializer sz = new C1MenuSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			C1MenuSerializer sz = new C1MenuSerializer(this);
			sz.LoadLayout(filename);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(System.IO.Stream stream)
		{
			C1MenuSerializer sz = new C1MenuSerializer(this);
			sz.LoadLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1MenuSerializer sz = new C1MenuSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(System.IO.Stream stream, LayoutType layoutTypes)
		{
			C1MenuSerializer sz = new C1MenuSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion




	}
}
