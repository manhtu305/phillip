﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Menu
{

    [DefaultProperty("TextField")]
    public sealed class C1MenuItemBinding : ICloneable, IDataSourceViewSchemaAccessor
    {

        #region ** fields

        private int _depth = -1;
        private string _dataMember = string.Empty;
        private string _headerField = string.Empty;
        private string _separatorField = string.Empty;
        private string _formatString = string.Empty;
        private string _textField = string.Empty;
        private string _valueField = string.Empty;
        private string _ImageUrlField = string.Empty;
        private string _navigateUrlField = string.Empty;
        private object _dataSourceViewSchema;
        #endregion

        #region ** constructor
        /// <summary>
        /// Create new instance of the C1MenuItemBinding class.
        /// </summary>
        public C1MenuItemBinding() 
        {
        }
        /// <summary>
        /// Create new instance of the C1MenuItemBinding class.
        /// </summary>
        /// <param name="dataMember">Data member to bind a menu item.</param>
        public C1MenuItemBinding(string dataMember)
        {
            this.DataMember = dataMember;  
        }
        #endregion

        #region ** properties
        /// <summary>
        /// Gets or sets the data member to bind a menu item.
        /// </summary>
		[C1Description("C1MenuItemBinding.DataMember")]
        [DefaultValue("")]
		[C1Category("Category.Data")]
        public string DataMember
        {
            get
            {
                return this._dataMember;            
            }
            set
            {
                this._dataMember = value;
            }
        }

        /// <summary>
        /// Gets or sets the menu depth to which the C1MenuItemBinding object is applied.
        /// </summary>
		[C1Description("C1MenuItemBinding.Depth")]
		[C1Category("Category.Data")]
        [DefaultValue(-1)]
        // to do TypeConverter
        public int Depth
        {
            get
            {
                return this._depth;            
            }
            set
            {
                this._depth = value;
            }
        }

        /// <summary>
        /// Gets or sets the string that specifies the display format for the text of a C1MenuItem to which the C1MenuItemBinding object is applied
        /// </summary>
		[C1Description("C1MenuItemBinding.FormatString")]
        [DefaultValue("")]
        [Localizable(true)]
		[C1Category("Category.Data")]
        public string FormatString
        {
            get
            {
                return this._formatString;
            }
            set
            {
                this._formatString = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source that indicates C1Menu header item.
        /// </summary>
		[C1Description("C1MenuItemBinding.HeaderField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
        public string HeaderField
        {
            get
            {
                return this._headerField;
            }
            set
            {
                this._headerField = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source that indicates C1Menu separator item.
        /// </summary>
		[C1Description("C1MenuItemBinding.SeparatorField")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
		[C1Category("Category.Databindings")]
        public string SeparatorField
        {
            get
            {
                return this._separatorField;
            }
            set
            {
                this._separatorField = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the Text property of a C1menuItem object to which the C1MenuBinding object is applied.
        /// </summary>
		[C1Description("C1MenuItemBinding.TextField")]
		[C1Category("Category.Databindings")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string TextField
        {
            get
            {
                return this._textField;
            }
            set
            {
                this._textField = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind the value property of a C1MenuItem object to which the C1MenuBinding object is applied.
        /// </summary>
		[C1Description("C1MenuItemBinding.ValueField")]
		[C1Category("Category.Databindings")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string ValueField
        {
            get
            {
                return this._valueField;
            }
            set
            {
                this._valueField = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind the imageUrl property of a C1MenuItem object to which the C1MenuBinding object is applied.
        /// </summary>
		[C1Description("C1MenuItemBinding.ImageUrlField")]
		[C1Category("Category.Databindings")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string ImageUrlField
        {
            get
            {
                return this._ImageUrlField;
            }
            set
            {
                this._ImageUrlField = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the NavigateUrl property of a C1MenuItem object to which the C1MenuBinding object is applied.
        /// </summary>
		[C1Description("C1MenuItemBinding.NavigateUrlField")]
		[C1Category("Category.Databindings")]
        [DefaultValue("")]
        [TypeConverter(typeof(DataSourceViewSchemaConverter))]
        public string NavigateUrlField
        {
            get
            {
                return this._navigateUrlField;
            }
            set
            {
                this._navigateUrlField = value;
            }
        }
        #endregion

        #region ** ICloneable
		object ICloneable.Clone()
        {
            C1MenuItemBinding binding = new C1MenuItemBinding();
            binding.DataMember = this.DataMember;
            binding.Depth = this.Depth;
            binding.HeaderField = this.HeaderField;
            binding.SeparatorField = this.SeparatorField;
            binding.ValueField = this.ValueField;
            binding.ImageUrlField = this.ImageUrlField;
            binding.TextField = this.TextField;
            binding.NavigateUrlField = this.NavigateUrlField;
            return binding;
        }
        #endregion

        #region ** IDataSourceViewSchemaAccessor
        object IDataSourceViewSchemaAccessor.DataSourceViewSchema
        {
            get
            {
                return _dataSourceViewSchema;
            }
            set
            {
                this._dataSourceViewSchema = value;
            }
        }
        #endregion
    }
}
