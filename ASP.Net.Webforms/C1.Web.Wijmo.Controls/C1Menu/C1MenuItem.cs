﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Menu
{
    [ToolboxItem(false)]
    [ParseChildren(true)]
    public class C1MenuItem : UIElement, INamingContainer, IC1MenuItemCollectionOwner, IJsonRestore
    {

        #region ** fields
        private Hashtable _properties = new Hashtable();
        private IC1MenuItemCollectionOwner _owner;
        private C1MenuItemCollection _items;
        private ITemplate _template;
        private C1MenuItemTemplateContainer _container;
        private bool _istemplated;
        private string _staticKey;
        private string _iconClass;
        private ImagePosition _imagePosition;
        //protected bool IsDesignMode = HttpContext.Current == null;
        #endregion

        #region ** properties
        /// <summary>
        /// A value that determines whether the menu item is a header item.
        /// </summary>
        [WidgetOption]
        [C1Description("C1MenuItem.Header")]
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public bool Header
        {
            get
            {
                return _properties["Header"] == null ? false : (bool)_properties["Header"];
            }
            set
            {
                _properties["Header"] = value;
            }
        }

        /// <summary>
        /// A value that determines whether the item is a separator.
        /// </summary>
        [WidgetOption]
        [C1Description("C1MenuItem.Separator")]
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public bool Separator
        {
            get
            {
                return _properties["Separator"] == null ? false : (bool)_properties["Separator"];
            }
            set
            {
                _properties["Separator"] = value;
            }
        }

        /// <summary>
        /// Menu item template.
        /// </summary>
        [C1Description("C1MenuItem.Template")]
        [C1Category("Category.Behavior")]
        [TemplateContainer(typeof(C1MenuItemTemplateContainer))]
        [Browsable(false)]
        [Bindable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate Template
        {
            get
            {
                return this._template;
            }
            set
            {
                this._template = value;
            }
        }

        //Apr 7,2010 by lulianbo for bug #10073
        /// <summary>
        /// Gets whether the item is templated.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(false)]
        public bool IsTemplated
        {
            get
            {
                bool isTemplated = false;

                if (_properties["IsTemplated"] != null)
                {
                    isTemplated = (bool)_properties["IsTemplated"];
                }

                return isTemplated;
            }
            set
            {
                _properties["IsTemplated"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the template container.
        /// </summary>
        /// <value>The template container.</value>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public C1MenuItemTemplateContainer TemplateContainer
        {
            get
            {
                return this._container;
            }
            set
            {
                this._container = value;
                this._istemplated = value != null;
            }
        }

        /// <summary>
        /// The menu item's text.
        /// </summary>
        [WidgetOption]
        [C1Description("C1MenuItem.Text")]
        [DefaultValue("")]
        [C1Category("Category.Appearance")]
        [Layout(LayoutType.Appearance)]
        public string Text
        {
            get
            {
                return _properties["Text"] == null ? "" : _properties["Text"].ToString();
            }
            set
            {
                _properties["Text"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of the menu item.
        /// </summary>
        [WidgetOption]
        [DefaultValue("")]
        [C1Description("C1MenuItem.Value")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public string Value
        {
            get
            {
                return _properties["Value"] == null ? "" : _properties["Value"].ToString();
            }
            set
            {
                _properties["Value"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected of the menu item.
        /// </summary>
        [WidgetOption]
        [DefaultValue(false)]
        [C1Description("C1MenuItem.Selected", "A value that indicates whether the menu item is selected")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        public bool Selected
        {
            get
            {
                return _properties["Selected"] == null ? false : (bool)_properties["Selected"];
            }
            set
            {
                _properties["Selected"] = value;
            }
        }

        /// <summary>
        ///  The menu's navigate url.
        /// </summary>
        [WidgetOption]
        [DefaultValue("")]
        [C1Description("C1MenuItem.NavigateUrl")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        [UrlProperty]
        public string NavigateUrl
        {
            get
            {
                return _properties["NavigateUrl"] == null ? "" : _properties["NavigateUrl"].ToString();
            }
            set
            {
                _properties["NavigateUrl"] = value;
            }
        }

        // Added by eric 2012-11-21 for bug 30098
        /// <summary>
        /// For postback, get the original NavigateUrl value.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [WidgetOption]
        [DefaultValue("")]
        public string NavigateUrlHelper
        {
            get
            {
                return this.NavigateUrl;
            }
        }

        /// <summary>
        /// Gets or sets the target window or frame in which to display the Web page content associated with a menu item when the menu item is clicked or the enter key is pressed.
        /// </summary>
        /// <remarks>The default value is an empty string.</remarks>
        [DefaultValue("")]
        [C1Description("C1MenuItem.Target")]
        [C1Category("Category.Behavior")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public virtual string Target
        {
            get
            {
                return (string)(this.ViewState["Target"] == null ? "" : this.ViewState["Target"]);
            }
            set
            {
                this.ViewState["Target"] = value;
            }
        }

        /// <summary>
        /// Gets the menu item's owner menu.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public C1Menu Menu
        {
            get
            {
                if (this._owner is C1MenuItem)
                {
                    return ((C1MenuItem)this._owner).Menu;
                }
                return this._owner as C1Menu;
            }
        }

        /// <summary>
        /// Gets or Sets the StaticKey for C1MenuItem.
        /// </summary>
        [WidgetOption]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue("")]
        public string StaticKey
        {
            get
            {
                return _staticKey;
            }
            internal set
            {
                _staticKey = value;
            }
        }

        /// <summary>
        /// Gets or sets the icon css class to the menuitem that is displayed next to the text in the menu item.
        /// </summary>
        [C1Description("C1MenuItem.IconClass")]
        [C1Category("Category.Appearance")]
        [Layout(LayoutType.Appearance)]
        public string IconClass
        {
            get
            {
                return _iconClass;
            }
            set
            {
                _iconClass = value;
            }
        }

        /// <summary>
        /// Gets or sets which side, left or right, the image will be rendered from the menu item.
        /// </summary>
        [C1Description("C1MenuItem.ImagePosition")]
        [C1Category("Category.Appearance")]
        [Layout(LayoutType.Appearance)]
        [DefaultValue(ImagePosition.Left)]
        public ImagePosition ImagePosition
        {
            get
            {
                return _imagePosition;
            }
            set
            {
                _imagePosition = value;
            }
        }

        [WidgetOption]
        [C1Category("Category.Appearance")]
        [Layout(LayoutType.Appearance)]
        [DefaultValue(true)]
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
            }
        }

        #endregion

        #region ** override methods
        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Li;
            }
        }

        /// <summary>
        /// Renders the HTML opening tag of the control to the specified writer. This
        /// method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">
        /// A System.Web.UI.HtmlTextWriter that represents the output stream to render
        /// HTML content on the client.
        /// </param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            string itemCssClass = "";
            if (IsDesignMode)
            {
                if (Header)
                {
                    itemCssClass = "ui-widget-header ui-corner-all";
                }
                else if (Separator)
                {
                    itemCssClass = "wijmo-wijmenu-separator ui-state-default ui-corner-all";
                }
                else
                {

                    itemCssClass = "ui-widget wijmo-wijmenu wijmo-wijmenu-item wijmo-wijmenu-item ui-state-default ui-corner-all";
                    if (this.Menu.Mode == MenuMode.Flyout && this.Menu.Orientation == Orientation.Horizontal)
                    {
                        //itemCssClass = "ui-widget ui-state-default ui-corner-all";
                        //writer.AddStyleAttribute("float", "left");
                        writer.AddStyleAttribute("border", "medium none");
                    }
                    if (this.Items.Count > 0)
                    {
                        itemCssClass += " wijmo-wijmenu-parent";
                    }
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Class, itemCssClass);
            }
            base.RenderBeginTag(writer);
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method
        /// is used primarily by control developers.
        /// </summary>
        /// <param name="writer">
        /// A System.Web.UI.HtmlTextWriter that represents the output stream to render
        /// HTML content on the client.
        /// </param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (Header)
            {
                RenderHeader(writer);
            }
            else if (Separator)
            {
                RenderSeparator(writer);
            }
            else
            {
                RenderMenuItem(writer);
                RenderMenuItems(writer);
            }
        }

        private void RenderMenuItems(HtmlTextWriter writer)
        {
            if (this.Items.Count > 0)
            {
                if (!IsDesignMode)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                    foreach (Control c in Controls)
                    {
                        if (c is C1MenuItem)
                        {
                            c.RenderControl(writer);
                        }
                    }
                    writer.RenderEndTag();
                }
            }
        }

        /// <summary>
        /// Raises the System.Web.UI.Control.Init event.
        /// </summary>
        /// <param name="e">
        /// An System.EventArgs object that contains the event data.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            EnsureChildControls();
        }

        /// <summary>
        /// Sends server control content to a provided System.Web.UI.HtmlTextWriter object,
        /// which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">
        /// The System.Web.UI.HtmlTextWriter object that receives the server control content. 
        /// </param>
        protected override void Render(HtmlTextWriter writer)
        {
            EnsureChildControls();
            base.Render(writer);
        }

        /// <summary>
        /// Called by the ASP.NET page framework to notify server controls that use composition-based
        /// implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            this.Controls.Clear();
            foreach (C1MenuItem item in Items)
            {
                this.Controls.Add(item);
            }
            if (this.Template != null)
            {
                this.Template.InstantiateIn(this);
            }
            ChildControlsCreated = true;
        }

        #endregion

        #region ** private methods
        /// <summary>
        /// Render header
        /// </summary>
        /// <param name="writer"></param>
        private void RenderHeader(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.H3);
            if (IsTemplated)
            {
                this.TemplateContainer.RenderControl(writer);
            }
            else
            {
                writer.Write(Text);
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// Render separator
        /// </summary>
        /// <param name="writer"></param>
        private void RenderSeparator(HtmlTextWriter writer)
        {
        }

        /// <summary>
        /// Render menu item.
        /// </summary>
        /// <param name="writer"></param>
        private void RenderMenuItem(HtmlTextWriter writer)
        {

            if (this.IsTemplated)
            {
                this.TemplateContainer.RenderControl(writer);
            }
            else
            {

                writer.AddAttribute(HtmlTextWriterAttribute.Href, Menu.DetermineHrefAttribute(this));
                if (!string.IsNullOrEmpty(Target))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, Target);
                }

                if (IsDesignMode)
                {
                    if (IsEnabled)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-link ui-corner-all");
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-link ui-corner-all ui-state-disabled");
                    }
#if !ASP_NET4
                    writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0");
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "auto");
#endif
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    RenderIcon(writer);

                    string iconCss = "";

                    if (this.Items.Count > 0)
                    {
                        // to do if the orientation is vertical.
                        if (this.Menu.Orientation == Orientation.Horizontal && this.Menu.Mode == MenuMode.Flyout)
                        {
                            iconCss = "ui-icon ui-icon-triangle-1-s";
                        }
                        else
                        {
                            iconCss = "ui-icon ui-icon-triangle-1-e";
                        }
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, iconCss);
                        writer.AddStyleAttribute("float", "none");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                }
                else
                {
                    if (!IsEnabled)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-state-disabled");
                    }
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    RenderIcon(writer);
                    writer.RenderEndTag();
                }
            }
        }

        private void RenderIcon(HtmlTextWriter writer)
        {
            if (IconClass != null)
            {
                if (this.Menu.Direction == Direction.Ltr)
                {
                    if (this.ImagePosition == ImagePosition.Left)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, IconClass + " wijmo-wijmenu-icon-left");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-text");
                        if (IsDesignMode)
                        {
                            writer.AddStyleAttribute("float", "left");
                        }
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.Write(Text);
                        writer.RenderEndTag();
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-text");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.Write(Text);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Class, IconClass + " wijmo-wijmenu-icon-right");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.RenderEndTag();
                    }
                }
                else
                {
                    if (this.ImagePosition == ImagePosition.Right)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, IconClass + " wijmo-wijmenu-icon-right");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-text");
                        if (IsDesignMode)
                        {
                            writer.AddStyleAttribute("float", "left");
                        }
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.Write(Text);
                        writer.RenderEndTag();
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-text");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.Write(Text);
                        writer.RenderEndTag();

                        writer.AddAttribute(HtmlTextWriterAttribute.Class, IconClass + " wijmo-wijmenu-icon-left");
                        writer.RenderBeginTag(HtmlTextWriterTag.Span);
                        writer.RenderEndTag();
                    }
                }
            }
            else
            {
                if (IsDesignMode)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmo-wijmenu-text");
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "block");
                    writer.AddStyleAttribute("float", "none");
                    writer.RenderBeginTag(HtmlTextWriterTag.Span);
                    writer.Write(Text);
                    writer.RenderEndTag();
                }
                else
                {
                    writer.Write(Text);
                }
            }
        }

        #endregion

        #region ** IC1MenuItemCollectionOwner interface implementations

        /// <summary>
        /// Gets a C1MenuItemCollection object that includes the submenu items of the current menu item.
        /// </summary>
        [C1Description("C1MenuItemBase.Items")]
        [C1Category("Category.Default")]
        [Layout(LayoutType.Behavior)]
        [Browsable(false)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [WidgetOption]
        public virtual C1MenuItemCollection Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new C1MenuItemCollection(this);
                    _items.OnBeforeCollectionChanged += new EventHandler(items_OnBeforeCollectionChanged);
                    _items.OnCollectionChanged += new EventHandler(items_OnCollectionChanged);
                    //if (IsTrackingViewState)((IStateManager)_items).TrackViewState();
                }
                return this._items;
            }
        }

        internal void SetItems(C1MenuItemCollection collection)
        {
            _items = collection;
            collection.OnBeforeCollectionChanged += new EventHandler(items_OnBeforeCollectionChanged);
            collection.OnCollectionChanged += new EventHandler(items_OnCollectionChanged);
        }

        /// <summary>
        /// fires after C1MenuItem's collection changed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">C1ToolBarItemCollectionChangedEventArgs</param>
        protected virtual void items_OnCollectionChanged(object sender, EventArgs e)
        {
            C1MenuItemCollectionChangedEventArgs args = (C1MenuItemCollectionChangedEventArgs)e;
            if (args != null)
            {
                if (Menu == null || !Menu.ChildItemCreated)
                {
                    return;
                }
                Control con = args.Item as Control;
                if (con == null)
                {
                    return;
                }
                if (args.Status == ChangeStatus.ItemAdd)
                {
                    ((C1MenuItem)_items.Owner).Controls.Add(con);
                }
                else if (args.Status == ChangeStatus.ItemRemove)
                {
                    ((C1MenuItem)_items.Owner).Controls.Remove(con);
                }
            }
        }

        void items_OnBeforeCollectionChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Gets the owner object which contains the item. 
        /// It might be an instance of the C1Menu class or the C1MenuItem 
        /// class depending on the current hierarchy level. 
        /// </summary>
        [Browsable(false)]
        public IC1MenuItemCollectionOwner Owner
        {
            get { return _owner; }
        }

        #endregion

        #region ** internal and private code

        #region ** internal Data Binding implementation
        internal void DataBind(C1MenuItemBinding binding, IHierarchicalEnumerable enumerable, object data, int depth, bool auto)
        {
            IHierarchyData hdata = enumerable.GetHierarchyData(data);
            string htype = hdata.Type;
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(data);
            string value = TryGetStringFieldValue(data, pdc, binding, "Value");
            if (value != null)
            {
                Value = value;
            }
            else if (auto)
            {
                if (value == "")
                {
                    Value = data.ToString();
                }
            }

            string text = TryGetStringFieldValue(data, pdc, binding, "Text");
            bool separator = TryGetBooleanFieldValue(data, pdc, "Separator");
            bool header = TryGetBooleanFieldValue(data, pdc, "Header");
            this.Header = header;
            this.Separator = separator;
            if (!Separator)
            {
                if (text != null)
                {
                    if (binding != null && binding.FormatString.Length > 0)
                    {
                        Text = string.Format(CultureInfo.CurrentCulture, binding.FormatString, new object[] { text });
                    }
                    else
                    {
                        Text = text;
                    }
                }
                else if (auto)
                {
                    if (!string.IsNullOrEmpty(this.Text))
                    {
                        this.Text = data.ToString();
                    }
                }

                if (!Header)
                {
                    string navigateUrl = TryGetStringFieldValue(data, pdc, binding, "NavigateUrl");
                    if (navigateUrl != null)
                    {
                        this.NavigateUrl = navigateUrl;
                    }

                    string target = TryGetStringFieldValue(data, pdc, binding, "Target");
                    if (target != null)
                    {
                        this.Target = target;
                    }
                    // to do imageUrl
                    //string imageUrl = TryGetStringFieldValue(data, pdc, binding, "ImageUrl");
                    //if (imageUrl != null)
                    //{ 
                    //    this.ima
                    //}
                }
            }
            if (data is INavigateUIData)
            {
                if (!Separator)
                {
                    INavigateUIData n_data = (INavigateUIData)data;
                    this.Value = n_data.Value;
                    this.ToolTip = n_data.Description;
                    this.Text = n_data.Name;
                    this.NavigateUrl = n_data.NavigateUrl;
                }
            }
        }

        private string TryGetStringFieldValue(object data, PropertyDescriptorCollection pdc, C1MenuItemBinding binding, string propName)
        {
            object aText = null;
            if (binding != null)
            {
                PropertyDescriptorCollection binding_pdc = TypeDescriptor.GetProperties(binding);
                PropertyDescriptor descr = binding_pdc.Find(propName + "Field", true);
                if (descr != null)
                {
                    object oFieldName = descr.GetValue(binding);
                    string sFieldName = oFieldName as string;
                    if (!string.IsNullOrEmpty(sFieldName))
                    {
                        PropertyDescriptor descr2 = pdc.Find(sFieldName, true);
                        if (descr2 != null)
                        {
                            aText = descr2.GetValue(data);
                        }
                    }
                }
            }
            if (aText == null)
            {
                PropertyDescriptor descr = pdc.Find(propName, true);
                if (descr != null)
                {
                    aText = descr.GetValue(data);
                }
            }
            return aText as string;
        }

        internal static bool TryGetBooleanFieldValue(object data,
            PropertyDescriptorCollection pdc,
            string propName)
        {
            object obj = C1MenuItem.TryGetFieldValue(data, pdc, propName);
            if (obj == null)
                obj = C1MenuItem.TryGetFieldValue(data, pdc, "Is" + propName);
            if (obj != null
                && C1MenuItem.ObjectToBool(obj))
                return true;
            return false;
        }

        private static object TryGetFieldValue(object data, PropertyDescriptorCollection propCollection, string propName)
        {
            PropertyDescriptor descr = propCollection.Find(propName, true);
            if (descr != null)
                return descr.GetValue(data);
            return null;
        }

        private Unit ObjectToUnit(object o)
        {
            if (o == null)
                return Unit.Empty;
            string s = o.ToString();
            try
            {
                Unit u = Unit.Parse(s);
                return u;
            }
            catch (Exception)
            {

            }
            return Unit.Empty;
        }

        internal static int ObjectToInt(object obj)
        {
            if (obj == null)
                return 0;
            if (obj is int)
                return (int)obj;
            string s = obj.ToString().ToLower();
            return int.Parse(s);
        }

        internal static bool ObjectToBool(object obj)
        {
            if (obj == null)
                return false;
            if (obj is bool)
                return (bool)obj;
            string s = obj.ToString().ToLower();
            if (s == "1" || s == "true" || s == "on")
                return true;
            return false;

        }

        internal virtual void DataBindInternal(IHierarchicalEnumerable enumerable, int depth, C1MenuItemBinding parBinding)
        {
            foreach (object o in enumerable)
            {
                IHierarchyData data = enumerable.GetHierarchyData(o);
                string text = data.Type;
                C1MenuItemBinding binding = this.Menu.GetBinding(text, depth);
                C1MenuItem item = this.Menu.CreateMenuItem();
                item.DataBind(binding, enumerable, o, depth, this.Menu.IsBindingsNullOrEmpty());
                this.Items.Add(item);
                if (data.HasChildren)
                {
                    IHierarchicalEnumerable childEnum = data.GetChildren();
                    if (childEnum != null)
                    {
                        item.DataBindInternal(childEnum, depth + 1, binding);
                    }
                }
                //}
            }
        }
        #endregion

        internal void SetParent(IC1MenuItemCollectionOwner aParent)
        {
            this._owner = aParent;
        }

        internal void SetDirty()
        {
            this.ViewState.SetDirty(true);
        }

        //protected override void TrackViewState()
        //{
        //    base.TrackViewState();
        //}        

        #endregion

        #region ** IJsonRestore interface implementations
        /// <summary>
        /// Avoid to deserialize the items property.
        /// </summary>
        /// <param name="obj">object to deserialize.</param>
        public void C1DeserializeItems(object obj)
        {

        }

        void IJsonRestore.RestoreStateFromJson(object state)
        {
            this.RestoreStateFromJson(state);
        }

        protected virtual void RestoreStateFromJson(object state)
        {
            JsonRestoreHelper.RestoreStateFromJson(this, state);
        }
        #endregion
    }
}
