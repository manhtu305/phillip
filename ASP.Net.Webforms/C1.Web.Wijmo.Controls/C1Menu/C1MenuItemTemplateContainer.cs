﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Security.Permissions;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1Menu
{

    /// <summary>
    /// Represents a container that holds the contents of a 
    /// templated menu item in a C1Menu control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ToolboxItem(false)]
    public sealed class C1MenuItemTemplateContainer : Control, IDataItemContainer, INamingContainer
    {
        #region ** fields
        private object _dataItem;
        private int _itemIndex;
        #endregion

        #region ** constructor
        /// <summary>
        /// Initializes a new instance of the C1MenuItemTemplateContainer class 
        /// using the specified menu item index and menu item.
        /// </summary>
        /// <param name="itemIndex">The index of the item</param>
        /// <param name="dataItem">An menuitem object provide data to the template</param>
        public C1MenuItemTemplateContainer(int itemIndex, C1MenuItem dataItem)
        {
            this._itemIndex = itemIndex;
            this._dataItem = dataItem;
        }
        #endregion

        #region ** properties
        /// <summary>
        /// Gets or sets the menu item associated with the container.
        /// </summary>
        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        /// <summary>
        /// Gets the index of the menu item associated with the container.
        /// </summary>
        public int ItemIndex
        {
            get
            {
                return this._itemIndex;
            }
        }
        #endregion

        #region ** methods

        /// <summary>
        /// Sends server control content to a provided System.Web.UI.HtmlTextWriter object,
        /// which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            // C1Outer
            //writer.RenderBeginTag(HtmlTextWriterTag.Div);          

            base.Render(writer);

           // writer.RenderEndTag();// C1Content
        }

        /// <summary>
        /// OnBubbleEvent override.
        /// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">An System.EventArgs object that contains the event data.</param>
        /// <returns></returns>
        protected override bool OnBubbleEvent(object source, EventArgs e)
        {
            CommandEventArgs args = e as CommandEventArgs;
            if (args == null)
            {
                return false;
            }
            if (args is C1MenuEventArgs)
            {
                base.RaiseBubbleEvent(this, args);
            }
            else
            {
                if (this._dataItem is C1MenuItem)
                {
                    C1MenuEventArgs args2 = new C1MenuEventArgs((C1MenuItem)this._dataItem, source, args);
                    base.RaiseBubbleEvent(this, args2);
                }
            }
            return true;
        }
        #endregion

        #region ** IDataItemContainer interface implementation

        object IDataItemContainer.DataItem
        {
            get
            {
                return this._dataItem;
            }
        }

        int IDataItemContainer.DataItemIndex
        {
            get
            {
                return this.ItemIndex;
            }
        }

        int IDataItemContainer.DisplayIndex
        {
            get
            {
                return this.ItemIndex;
            }
        }
        #endregion
    }
}
