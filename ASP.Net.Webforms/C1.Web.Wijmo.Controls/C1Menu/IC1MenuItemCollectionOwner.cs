﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Menu
{
    /// <summary>
    /// IC1MenuItemCollectionOwner interface.
    /// </summary>
    public interface IC1MenuItemCollectionOwner
    {

        /// <summary>
        /// Menu items collection
        /// </summary>
        C1MenuItemCollection Items
        {
            get;
        }

        /// <summary>
        /// Menu item owner.
        /// </summary>
        IC1MenuItemCollectionOwner Owner
        {
            get;
        }

    }
}
