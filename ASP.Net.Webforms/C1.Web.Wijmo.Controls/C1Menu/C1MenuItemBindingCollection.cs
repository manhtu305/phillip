﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base.Collections;
using System.Threading;

namespace C1.Web.Wijmo.Controls.C1Menu
{
    /// <summary>
    /// Represents a collection of C1MenuItemBinding objects that are used by data-bound C1Menu control.
    /// </summary>
    public class C1MenuItemBindingCollection : C1ObservableItemCollection<C1Menu,C1MenuItemBinding>
    {

        #region ** field
        private readonly C1Menu _owner;
        private C1MenuItemBinding _defaultBinding;
        #endregion

        #region ** constructors
        internal C1MenuItemBindingCollection(C1Menu owner)
            : base(owner)
        {
            this._owner = owner;
            this.Cleared += new CollectionChangedEventHandler(C1MenuItemBindingCollection_Cleared);
            this.ItemAdded += new CollectionChangedEventHandler(C1MenuItemBindingCollection_ItemAdded);
            this.ItemRemoved += new CollectionChangedEventHandler(C1MenuItemBindingCollection_ItemRemoved);
            this.ItemReplaced += new CollectionChangedEventHandler(C1MenuItemBindingCollection_ItemReplaced);
        }

        #endregion

        #region ** collection events handlers
        void C1MenuItemBindingCollection_Cleared(object sender, C1ObservableItemCollection<C1Menu, C1MenuItemBinding>.CollectionChangedEventArgs e)
        {
            this._defaultBinding = null;
        }

        void C1MenuItemBindingCollection_ItemReplaced(object sender, C1ObservableItemCollection<C1Menu, C1MenuItemBinding>.CollectionChangedEventArgs e)
        {
            C1MenuItemBinding binding = e.Item as C1MenuItemBinding;
            if (binding != null && binding.DataMember.Length == 0 && binding.Depth == -1)
            {
                this._defaultBinding = binding;
            }
        }

        void C1MenuItemBindingCollection_ItemRemoved(object sender, C1ObservableItemCollection<C1Menu, C1MenuItemBinding>.CollectionChangedEventArgs e)
        {
            if (e.Item == this._defaultBinding)
            {
                this.FindDefaultBinding();
            }
        }

        void C1MenuItemBindingCollection_ItemAdded(object sender, C1ObservableItemCollection<C1Menu, C1MenuItemBinding>.CollectionChangedEventArgs e)
        {
            C1MenuItemBinding binding = e.Item as C1MenuItemBinding;
            if (binding != null && binding.DataMember.Length == 0 && binding.Depth == -1)
            {
                this._defaultBinding = binding;
            }
        }
        #endregion

        #region ** private and internal implementation
        private void FindDefaultBinding()
        {
            this._defaultBinding = null;
            foreach (C1MenuItemBinding binding in this)
            {
                if (binding.Depth == -1 && binding.DataMember.Length == 0)
                {
                    this._defaultBinding = binding;
                    return;
                }
            }
        }

        internal C1MenuItemBinding GetBinding(string dataMember, int depth)
        {
            C1MenuItemBinding binding = null;
            int num = 0;
            if ((dataMember != null) && (dataMember.Length == 0))
            {
                dataMember = null;
            }
            foreach (C1MenuItemBinding bind in this)
            {
                if (bind.Depth == depth)
                {
                    if (string.Compare(bind.DataMember, dataMember, true, Thread.CurrentThread.CurrentCulture) == 0)
                    {
                        return bind;
                    }
                    if ((num < 1) && (bind.DataMember.Length == 0))
                    {
                        binding = bind;
                        num = 1;
                    }
                }
                if (string.Compare(bind.DataMember, dataMember, true, Thread.CurrentThread.CurrentCulture) == 0)
                {
                    if (bind.Depth == depth)
                    {
                        return bind;
                    }
                    if ((num < 2) && (bind.Depth == -1))
                    {
                        binding = bind;
                        num = 2;
                    }
                }
            }
            if ((binding != null) || (this._defaultBinding == null))
            {
                return binding;
            }
            if ((this._defaultBinding.Depth != -1) || (this._defaultBinding.DataMember.Length != 0))
            {
                this.FindDefaultBinding();
            }
            return this._defaultBinding;
        }
        #endregion
    }
}
