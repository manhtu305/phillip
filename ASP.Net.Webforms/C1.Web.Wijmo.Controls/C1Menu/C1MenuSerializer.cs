﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Menu
{
    public class C1MenuSerializer : C1BaseSerializer<C1Menu, C1MenuItem, IC1MenuItemCollectionOwner>
    {

        #region ** constructors

        /// <summary>
		/// Initializes a new instance of the <see cref="C1MenuSerializer"/> class.
        /// </summary>
        public C1MenuSerializer(Object obj)
            : base(obj)
        {

        }

        #endregion

        #region ** protected override

        /// <summary>
		/// This method preliminary used by control developers..
        /// </summary>
        /// <param name="collection">Menu item collection</param>
        /// <param name="item">Menu item</param>
		/// <returns>Return true if object is added to array or collection.</returns>
        protected override bool OnAddItem(object collection, object item)
        {
            if (collection is C1MenuItemCollection)
            {
                ((C1MenuItemCollection)collection).Add((C1MenuItem)item);
                return true;
            }
            return base.OnAddItem(collection, item);
        }

        /// <summary>
		/// This method preliminary used by control developers..
        /// </summary>
        /// <param name="collection">Menu item collection</param>
		/// <returns>Return true if array or collection is cleared.</returns>
        protected override bool OnClearItems(object collection)
        {
            if (collection is C1MenuItemCollection)
            {
                ((C1MenuItemCollection)collection).Clear();
                return true;
            }
            return base.OnClearItems(collection);
        }

        #endregion
    }
}
