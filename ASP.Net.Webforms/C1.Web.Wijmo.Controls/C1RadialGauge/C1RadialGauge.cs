﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;
using System.Drawing;

namespace C1.Web.Wijmo.Controls.C1Gauge
{
	/// <summary>
	/// The RadialGauge is a webcontrol that shows a radial gauge in web page.
	/// </summary>
	#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1Gauge.C1RadialGaugeDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1Gauge.C1RadialGaugeDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Gauge.C1RadialGaugeDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Gauge.C1RadialGaugeDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1RadialGauge runat=server ></{0}:C1RadialGauge>")]
	[ToolboxBitmap(typeof(C1RadialGauge), "C1RadialGauge.png")]
	[LicenseProviderAttribute()]
    //[ToolboxItem(true)]
    public partial class C1RadialGauge : C1Gauge, IC1Serializable
	{
		#region ** fields
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1RadialGauge"/> class.
		/// </summary>
		public C1RadialGauge()
			: base()
		{
			this.Width = Unit.Pixel(600);
			this.Height = Unit.Pixel(400);
			VerifyLicense();
		}

		private void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1RadialGauge), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY 
				_productLicensed = licinfo.IsValid; 
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		#region ** Options
		/// <summary>
		/// A value that indicates the width of the gauge.
		/// </summary>
		[DefaultValue(typeof(Unit), "600")]
		[C1Description("C1RadialGauge.Width")]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// A value that indicates the height of the gauge.
		/// </summary>
		[DefaultValue(typeof(Unit), "400")]
		[C1Description("C1RadialGauge.Height")]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}
		#endregion

		#region ** override methods
		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnInit(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this); 
			base.OnPreRender(e);
		}
		#endregion


		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1RadialGaugeSerializer sz = new C1RadialGaugeSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(System.IO.Stream stream)
		{
			C1RadialGaugeSerializer sz = new C1RadialGaugeSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(System.IO.Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1RadialGaugeSerializer sz = new C1RadialGaugeSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(System.IO.Stream stream, LayoutType layoutTypes)
		{
			C1RadialGaugeSerializer sz = new C1RadialGaugeSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		} 
		#endregion
	}
}
