﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Gauge
{
	/// <summary>
	/// Serializes the RadialGauge Control.
	/// </summary>
	public class C1RadialGaugeSerializer : C1BaseSerializer<C1RadialGauge, object,object>
	{
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1RadialGaugeSerializer"/> class.
		/// </summary>
		/// <param name="obj">Object to serialize.</param>
		public C1RadialGaugeSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
