﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Data;
using System.Drawing.Design;




namespace C1.Web.Wijmo.Controls.C1Tabs
{
    using C1.Web.Wijmo.Controls.Base;
    using C1.Web.Wijmo.Controls.Localization;

    /// <summary>
    /// Represents a collection of tab pages.
    /// </summary>
    [Editor("System.Windows.Forms.Design.CollectionEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
    public class C1TabPageCollection : ControlCollection
    {
        #region Constructor

        /// <summary>
        /// Creates an instance of C1TabPageCollection class.
        /// </summary>
        /// <param name="owner">The owner of this collection.</param>
        public C1TabPageCollection(C1Tabs owner)
            : base(owner)
        {
        }

        #endregion


        // Summary:
        //     Gets a reference to the tab page control at the specified index location in
        //     the C1TabPageCollection object.
        //
        // Parameters:
        //   index:
        //     The location of the tab page control in the C1TabPageCollection.
        //
        // Returns:
        //     The reference to the control.
        //
        // Exceptions:
        //   System.ArgumentOutOfRangeException:
        //     The index parameter is less than zero or greater than or equal to C1TabPageCollection.Count.
        new public C1TabPage this[int index] 
        {
            get
            {
                return base[index] as C1TabPage;
            }
        }

		/// <summary>
		/// Get pages from Json
		/// </summary>
		/// <param name="arr"></param>
		internal void FromJson(ArrayList arr)
		{
            //check delete item
            int count = this.Count;
            for(int i= 0; i<count; i++){
               bool pageFound = this.FindPageInJson(this[i], arr);
                //remove the deleted page from client and
                //the added page from client
               if (!pageFound)
               {
                   this.Remove(this[i]);
                   count--;
                   i--;
               }
            }

            int index = 0;
			foreach (Hashtable item in arr)
			{
                C1TabPage page;
                //text changed
                if (item["staticKey"] != null)
                {
                    page = GetPageByStaticKey(item["staticKey"].ToString());
                    page.Text = item["text"].ToString();
                }
                else
                {
                    page = new C1TabPage();
                    page.Text = item["text"].ToString();
                    this.AddAt(index,page);
                }
                index++;
			}
            
		}

		private C1TabPage GetPageByStaticKey(string staticKey)
		{
			foreach (C1TabPage page in this)
			{
				if (page.StaticKey == staticKey) 
				{
					return page;
				}
			}
			return null;
		}


        private bool FindPageInJson(C1TabPage page, ArrayList arr)
        {
                foreach (Hashtable item in arr)
                {
                    if (item["staticKey"] == null) {
                        return false;
                    } else if (page.StaticKey == item["staticKey"].ToString())
                    {
                        return true;                        
                    }
                }
                return false;
        }
    }
}
