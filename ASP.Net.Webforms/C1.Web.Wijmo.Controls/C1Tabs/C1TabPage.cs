﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Permissions;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Data;
using System.Drawing.Design;


namespace C1.Web.Wijmo.Controls.C1Tabs
{
    using C1.Web.Wijmo.Controls.Base;
    using C1.Web.Wijmo.Controls.Localization;


    /// <summary>
    /// Represents a single tab page in a C1Tabs. 
    /// </summary>
    [ToolboxItem(false)]
    [PersistChildren(true)]
    [ParseChildren(false)]
    public class C1TabPage : Panel
    {
        #region Fields

        private C1Tabs _owner;
		private string _staticKey;

        #endregion

        #region Consturctor

        /// <summary>
        /// Constructor of the C1TabPage class.
        /// </summary>
        public C1TabPage()
        {
        }

        #endregion

        #region Properties

        internal C1Tabs Owner
        {
            get { return this._owner; }
            set { this._owner = value; }
        }

        /// <summary>
        /// Gets or sets the text displayed on the tab.
        /// </summary>
        [Localizable(true)]
        [DefaultValue("")]
		[WidgetOption]
        [C1Description("C1TabPage.Text")]
        public string Text
        {
            get
            {
                object obj = this.ViewState["Text"];
                if (obj == null) return string.Empty;

                return (string)obj;
            }
            set
            {
                this.ViewState["Text"] = value;
            }
        }

		/// <summary>
		/// Gets or Sets the StaticKey for C1MenuItem.
		/// </summary>
		[WidgetOption]
		[EditorBrowsable( EditorBrowsableState.Never)]
		[Browsable(false)]
		[DefaultValue("")]
		public string StaticKey
		{
			get
			{
				return _staticKey;
			}
			set
			{
				_staticKey = value;
			}
		}

        #endregion

        #region Rendering

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.EnsureID();
            if (DesignMode)
            {
                string align = this._owner.GetAlignmentName(false);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-tabs-panel ui-widget-content ui-corner-" + align + " " + CssClass);
            }

            if (this.ID == null)
                writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);

            base.AddAttributesToRender(writer);
        }
        

        #endregion
    }
}
