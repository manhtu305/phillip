﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;
using System.IO;
using System.Collections.Specialized;
using System.Drawing.Design;


namespace C1.Web.Wijmo.Controls.C1Tabs
{

	using C1.Web.Wijmo;
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;

	[ToolboxData("<{0}:C1Tabs runat=server></{0}:C1Tabs>")]
	[ToolboxBitmap(typeof(C1Tabs), "C1Tabs.png")]
	[ParseChildren(true)]
	[LicenseProviderAttribute()]
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Tabs.C1TabsDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Tabs.C1TabsDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Tabs.C1TabsDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Tabs.C1TabsDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	public partial class C1Tabs : C1TargetControlBase, 
		INamingContainer,
		IPostBackEventHandler,
		IPostBackDataHandler
	{
		#region Fields

		private bool _productLicensed = false;
		private bool _shouldNag;

		#endregion

		#region Constructor

		 /// <summary>
		/// Construct a new instance of C1Tabs.
		/// </summary>
		[C1Description("C1Tabs.Constructor")]
		public C1Tabs()
			: base(HtmlTextWriterTag.Div)
		{
			VerifyLicense();
		}

		#endregion

		#region Licensing

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Tabs), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region Properties

		/// <summary>
		/// PostBack Event Reference. Empty string if AutoPostBack property is false.
		/// </summary>
		[C1Description("C1Tabs.PostBackEventReference")]
		[WidgetOption()]
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string PostBackEventReference
		{
			get
			{
				if (!IsDesignMode && this.AutoPostBack)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this.GetPostBackOptions());
				}
				return "";
			}
		}

		/// <summary>
		/// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1Tabs.AutoPostBack")]
		public bool AutoPostBack
		{
			get
			{
				return GetPropertyValue<bool>("AutoPostBack", false);
			}
			set
			{
				SetPropertyValue<bool>("AutoPostBack", value);
			}
		}

		/// <summary>
		/// Gets or sets the URL of the page to post to from the current page when a tab is clicked.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Tabs.PostBackUrl")]
		[Layout(LayoutType.Behavior)]
		[Editor("System.Web.UI.Design.UrlEditor, System.Design", typeof(UITypeEditor))]
		[UrlProperty("*.aspx")]
		[DefaultValue("")]
		[Themeable(false)]
		public virtual string PostBackUrl
		{
			get
			{
				return GetPropertyValue<string>("PostBackUrl", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("PostBackUrl", value);
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether validation is performed when a tab is clicked.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Tabs.CausesValidation")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue(false)]
		public virtual bool CausesValidation
		{
			get
			{
				return GetPropertyValue<bool>("CausesValidation", false);
			}
			set
			{
				SetPropertyValue<bool>("CausesValidation", value);
			}
		}

		/// <summary>
		/// Gets or sets the group of controls for which the tab causes validation when it posts back to the server.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Tabs.ValidationGroup")]
		[Layout(LayoutType.Behavior)]
		[DefaultValue("")]
		[Bindable(true)]
		public string ValidationGroup
		{
			get
			{
				return GetPropertyValue<string>("ValidationGroup", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("ValidationGroup", value);
			}
		}

		/// <summary>
		/// Gets the current selected page.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public C1TabPage SelectedPage
		{
			get
			{
				if (this.Selected < 0 || this.Selected >= this.Pages.Count) return null;
				return (C1TabPage)this.Pages[this.Selected];
			}
		}

		/// <summary>
		/// Gets the tab page collection.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[CollectionItemType(typeof(C1TabPage))]
		[Browsable(false)]
		public C1TabPageCollection Pages
		{
			get
			{
				return (C1TabPageCollection)this.Controls;
			}
		}

		/// <summary>
		/// Gets value that can be used for tracking original position inside of data source.
		/// This property only used if <see cref="C1TreeView"/> tree view is bounded to data source and callbacks are enabled.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[WidgetOption]
		public ArrayList DisabledIndexes
		{
			get
			{
				ArrayList list = new ArrayList();
				for (int i = 0; i < this.Pages.Count; i++)
				{
					if (!this.Pages[i].Enabled)
						list.Add(i);
				}
				return list.Count == 0 ? null : list;
			}
			set
			{
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Raised during postback when the selected index changed.
		/// </summary>
		[C1Description("C1Tabs.SelectedChanged")]
		[C1Category("Category.Behavior")]
		public event EventHandler SelectedChanged;

		/// <summary>
		/// Called when selected pane is changed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnSelectedChanged(EventArgs e)
		{
			if (this.SelectedChanged != null)
			{
				SelectedChanged(this, e);

			}
		}

		#endregion

		#region IPostBackEventHandler Members

		/// <summary>
		/// Raises an event for the C1Tabs control when it posts back to the server.
		/// </summary>
		/// <param name="eventArgument">A String that represents the event argument passed to the event handler.</param>
		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("selectedIndex="))
			{
				int newInd = int.Parse(eventArgument.Remove(0, "selectedIndex=".Length));
				if (newInd != this.Selected)
				{
					this.Selected = newInd;
					OnSelectedChanged(new EventArgs());
				}
			}	
		}

		#endregion

		#region ** IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			this.RestoreStateFromJson(data);
			if (data["pages"] != null)
			{
				ArrayList arr = data["pages"] as ArrayList;
				this.Pages.FromJson(arr);
			}
			return false;
		}

		/// <summary>
		/// Notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}
		#endregion end of ** IPostBackDataHandler interface implementations

		#region  Render

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
			AddStaticKey(this.Pages, 0);
		}

		private void AddStaticKey(C1TabPageCollection pages, int index)
		{
			foreach (C1TabPage page in pages) 
			{
				page.StaticKey = "SK" + index.ToString();
				index++;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);

			if (!this.IsDesignMode)
			{
				if (this.Page != null)
				{
					this.Page.RegisterRequiresRaiseEvent(this);
				}
			}
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (!this.DesignMode)
			{
				this.RenderTabs(writer);
				this.RenderPages(writer, false);
			}
			else
			{
				switch (this.Alignment)
				{
					case Wijmo.Controls.C1Tabs.Alignment.Top:
						this.RenderTabs(writer);
						this.RenderPages(writer, false);
						break;

					case Wijmo.Controls.C1Tabs.Alignment.Bottom:
						this.RenderPages(writer, false);
						this.RenderTabs(writer);
						break;

					case Wijmo.Controls.C1Tabs.Alignment.Left:
						this.RenderTabs(writer);
						this.RenderPages(writer, true);
						break;

					case Wijmo.Controls.C1Tabs.Alignment.Right:
						this.RenderPages(writer, true);
						this.RenderTabs(writer);
						break;
				}
			}
			
		}

		private void RenderTabs(HtmlTextWriter writer)
		{
			HtmlGenericControl ul = new HtmlGenericControl("ul");
			if (this.DesignMode)
			{
				ul.Attributes["class"] = "ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all";

				if (this.Alignment == Wijmo.Controls.C1Tabs.Alignment.Top || this.Alignment == Wijmo.Controls.C1Tabs.Alignment.Bottom)
					ul.Style.Add(HtmlTextWriterStyle.Height, "40px");
			}

			string align = this.GetAlignmentName(true);

			// Render tabs
			for (int i = 0; i < this.Pages.Count; i++)
			{
				C1TabPage page = ((C1TabPage)this.Pages[i]);
				HtmlGenericControl li = new HtmlGenericControl("li");

				if (this.DesignMode)
				{
					li.Attributes.Add("class", "ui-state-default" + " ui-corner-" + align + (this.Selected == i ? " ui-tabs-selected ui-state-active" : ""));
					li.Attributes.Add(System.Web.UI.Design.DesignerRegion.DesignerRegionAttributeName, string.Format("{0}", i));
				}
				else
				{
					//if (!page.Visible) continue;
					if (!page.Visible)
					{
						li.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
					}
				}


				HyperLink link = new HyperLink();
                link.Attributes["href"] = "#" + page.ClientID;
				link.Text = page.Text;
				li.Controls.Add(link);
				ul.Controls.Add(li);
			}

			ul.RenderControl(writer);
		}

		internal string GetAlignmentName(bool tabs) {

			Alignment align = this.Alignment;

			if (!tabs){
				switch (align) {
				 case Wijmo.Controls.C1Tabs.Alignment.Top:
					 align = Wijmo.Controls.C1Tabs.Alignment.Bottom;
					 break;

				 case Wijmo.Controls.C1Tabs.Alignment.Bottom:
					 align = Wijmo.Controls.C1Tabs.Alignment.Top;
					 break;

				 case Wijmo.Controls.C1Tabs.Alignment.Left:
					 align = Wijmo.Controls.C1Tabs.Alignment.Right;
					 break;

				 case Wijmo.Controls.C1Tabs.Alignment.Right:
					 align = Wijmo.Controls.C1Tabs.Alignment.Left;
					 break;
				}
			}

			return Enum.GetName(typeof(Alignment), align).ToLower();
		 }

		private void RenderPages(HtmlTextWriter writer, bool wrap)
		{
			if (this.DesignMode)
			{
				if (this.Selected >= 0 && this.Selected < this.Pages.Count)
				{
					C1TabPage page = (C1TabPage)this.Pages[this.Selected];
					page.Owner = this;

					if (wrap)
					{
						writer.WriteBeginTag("div");
						writer.WriteAttribute("class", "wijmo-wijtabs-content");
						writer.Write(HtmlTextWriter.TagRightChar);
					}
					page.RenderControl(writer);

					if (wrap)
						writer.WriteEndTag("div");
				}
			}
			else
			{
				for (int i = 0; i < this.Pages.Count; i++)
				{
					C1TabPage page = ((C1TabPage)this.Pages[i]);
					//page.RenderControl(writer);
					if (page.Visible)
					{
						page.RenderControl(writer);
					}
					else
					{
						writer.AddAttribute("id", page.ClientID);
						writer.AddStyleAttribute("display", "none");
						writer.RenderBeginTag("div");
						writer.RenderEndTag();
					}
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="writer"></param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.DesignMode)
			{
				if (!this.Visible)
					return;

				writer.AddAttribute(HtmlTextWriterAttribute.Class, Utils.GetHiddenClass() + " " + CssClass);
			}

			if (this.Page != null)
				this.Page.VerifyRenderingInServerForm(this);

			base.Render(writer);
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.EnsureID();
			if (DesignMode)
			{
				string align = this.GetAlignmentName(true);
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-tabs wijmo-wijtabs" +" ui-tabs-" +  align + " ui-widget ui-widget-content ui-corner-all ui-helper-clearfix " + CssClass);
			}

			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected override ControlCollection CreateControlCollection()
		{
			return new C1TabPageCollection(this);
		}

		#endregion

		#region Internal Methods

		internal virtual PostBackOptions GetPostBackOptions()
		{
			PostBackOptions options = new PostBackOptions(this, this.UniqueID);
			options.ClientSubmit = true;
			options.Argument = "selectedIndex={0}";
			if (this.Page != null)
			{
				if (this.CausesValidation && (this.Page.GetValidators(this.ValidationGroup).Count > 0))
				{
					options.PerformValidation = true;
					options.ValidationGroup = this.ValidationGroup;
				}

				if (!string.IsNullOrEmpty(this.PostBackUrl))
				{
					options.ActionUrl = HttpUtility.UrlPathEncode(this.ResolveClientUrl(this.PostBackUrl));
				}
			}
			return options;
		}

		protected override void EnsureEnabledState()
		{
			return;
		}

		#endregion
	}

}
