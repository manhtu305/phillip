﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using System.Web.UI;
using System.Web;
using System.Globalization;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.C1AutoComplete
{
	internal class C1AutoCompleteSerializer : C1BaseSerializer<C1AutoComplete, object, object>
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="serializableObject">Serializable Object.</param>
		public C1AutoCompleteSerializer(object serializableObject)
			: base(serializableObject)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="componentChangeService"></param>
		/// <param name="serializableObject"></param>
        public C1AutoCompleteSerializer(IComponentChangeService componentChangeService, object serializableObject)
			: base(componentChangeService, serializableObject)
		{
		}
	}
}