﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;

using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1AutoComplete
{
    public class C1AutoCompleteDataItem
    {
        private Hashtable _properties = new Hashtable();

        #region ** properties

        /// <summary>
        /// Gets or sets the label of autocomplete item.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1AutoComplete.C1AutoCompleteDataItem.Label")]
        [WidgetOption]
        [Layout(LayoutType.Appearance)]
        public string Label
        {
            get
            {
                return _properties["Label"] == null ? "" : _properties["Label"].ToString();
            }
            set
            {
                _properties["Label"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of autocomplete item.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1AutoComplete.C1AutoCompleteDataItem.Value")]
        [WidgetOption]
        [Layout(LayoutType.Appearance)]
        public string Value
        {
            get
            {
                return _properties["Value"] == null ? "" : _properties["Value"].ToString();
            }
            set
            {
                _properties["Value"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the category of autocomplete item.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1AutoComplete.C1AutoCompleteDataItem.Category")]
        [WidgetOption]
        [Layout(LayoutType.Appearance)]
        public string Category
        {
            get
            {
                return _properties["Category"] == null ? "" : _properties["Category"].ToString();
            }
            set
            {
                _properties["Category"] = value;
            }
        }

        #endregion end of ** properties.
    }
}
