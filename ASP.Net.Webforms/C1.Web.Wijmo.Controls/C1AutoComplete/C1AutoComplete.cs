﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;

using C1.Web.Wijmo;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Licensing;


[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1AutoComplete", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1AutoComplete
{
   #if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1AutoComplete.C1AutoCompleteDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)] 
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1AutoComplete.C1AutoCompleteDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
    [Designer("C1.Web.Wijmo.Controls.Design.C1AutoComplete.C1AutoCompleteDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
    [Designer("C1.Web.Wijmo.Controls.Design.C1AutoComplete.C1AutoCompleteDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [WidgetDependencies(
        typeof(WijPager), 
        typeof(WijTextBox), 
        "extensions.c1autocomplete.js",
		ResourcesConst.C1WRAPPER_OPEN
    )]
    [ToolboxData("<{0}:C1AutoComplete runat=\"server\"></{0}:C1AutoComplete>")]
    [ToolboxBitmap(typeof(C1AutoComplete), "C1AutoComplete.png")]
    [ParseChildren(true)]
    [LicenseProvider()]
    public partial class C1AutoComplete : C1TargetDataBoundControlBase, IC1Serializable, IPostBackDataHandler, IPostBackEventHandler, ICallbackEventHandler
    {
        #region ** fields

        private PositionSettings _position;
        private C1AutoCompleteDataItemCollection _items = null;
        private IEnumerable _data;
        private AutoCompleteSourceOption _source;

        private const string REGISTER_FUNC = "saveAutoCompleteState";
        private const string CSS_CONTENT = "wijmo-wijtextbox ui-widget ui-state-default ui-corner-all ui-autocomplete-input";

        #region licensing

        internal bool _productLicensed;
        private bool _shouldNag;

        #endregion

        #endregion

        #region ** constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1AutoComplete"/> class.
        /// </summary>
        public C1AutoComplete()
        {
            VerifyLicense();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1AutoComplete"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public C1AutoComplete(string key)
        {
#if GRAPECITY
            _productLicensed = true;
#else
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1AutoComplete), this,
                Assembly.GetExecutingAssembly(), key);
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif

        }

        #endregion

        #region ** override

        internal override bool IsWijMobileControl
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (IsDesignMode && !_productLicensed && _shouldNag)
            {
                _shouldNag = false;
                ShowAbout();
            }

            base.OnInit(e);
            EnsureChildControls();
        }

        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Input;
            }
        }

        /// <summary>
        /// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);

            if (this.LoadOnDemand && Page!=null)
            {
                Page.ClientScript.GetCallbackEventReference(this, "", "", "");
            }
            if (this.AutoPostBack && Page != null)
            {
                Page.ClientScript.GetPostBackEventReference(this, "");
            }

            base.OnPreRender(e);
        }

        /// <summary>
        /// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. 
        /// This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute("aria-label", "autocomplete");

            if (this.MaxLength != 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, this.MaxLength.ToString());
            }

            if (!string.IsNullOrEmpty(this.Text))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, this.Text);
            }

            string uniqueID = this.UniqueID;
            if (uniqueID != null)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Name, uniqueID);
            }  

            string cssClass = this.CssClass;
            if (IsDesignMode)
            {
                this.CssClass = string.Format("{0} {1}", cssClass, CSS_CONTENT); 
            }
            else
            {
                this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass);
            }
            base.AddAttributesToRender(writer);
            this.CssClass = cssClass;
        }


        /// <summary>
        /// Registers an OnSubmit statement in order to save the states of the widget
        /// to json hidden input.
        /// </summary>
        protected override void RegisterOnSubmitStatement()
        {
			this.RegisterOnSubmitStatement(REGISTER_FUNC);
        }
        #endregion

        #region ** properties

        #region **** widget

        #region ****** option
        /// <summary>
        /// Which element the menu should be appended to.
        /// Reference: http://jqueryui.com/demos/autocomplete/#appendTo
        /// </summary>
        [C1Description("C1AutoComplete.AppendTo")]
        [C1Category("Category.Behavior")]
        [DefaultValue("")]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public string AppendTo
        {
            get
            {
                return GetPropertyValue("AppendTo", string.Empty);
            }
            set
            {
                SetPropertyValue("AppendTo", value);
            }
        }

        /// <summary>
        /// If set to true the first item will be automatically focused.
        /// Reference: http://jqueryui.com/demos/autocomplete/#autoFocus
        /// </summary>
        [C1Description("C1AutoComplete.AutoFocus")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public bool AutoFocus
        {
            get
            {
                return GetPropertyValue("AutoFocus", false);
            }
            set
            {
                SetPropertyValue("AutoFocus", value);
            }
        }

        /// <summary>
        /// The delay in milliseconds the Autocomplete waits after a keystroke to activate itself. A zero-delay makes sense for local data (more responsive), but can produce a lot of load for remote data, while being less responsive.
        /// Reference: http://jqueryui.com/demos/autocomplete/#delay
        /// </summary>
        [C1Description("C1AutoComplete.Delay")]
        [C1Category("Category.Behavior")]
        [DefaultValue(300)]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public int Delay
        {
            get
            {
                return GetPropertyValue("Delay", 300);
            }
            set
            {
                SetPropertyValue("Delay", value);
            }
        }

        /// <summary>
        /// The minimum number of characters a user has to type before the Autocomplete activates. Zero is useful for local data with just a few items. Should be increased when there are a lot of items, where a single character would match a few thousand items.
        /// Reference: http://jqueryui.com/demos/autocomplete/#minLength
        /// </summary>
        [C1Description("C1AutoComplete.MinLength")]
        [C1Category("Category.Behavior")]
        [DefaultValue(1)]
        [WidgetOption]
        [Layout(LayoutType.Behavior)]
        public int MinLength
        {
            get
            {
                return GetPropertyValue("MinLength", 1);
            }
            set
            {
                SetPropertyValue("MinLength", value);
            }
        }

        /// <summary>
        /// Identifies the position of the Autocomplete widget in relation to the associated input element. The "of" option defaults to the input element, but you can specify another element to position against. You can refer to the jQuery UI Position utility for more details about the various options.
        /// Reference: http://jqueryui.com/demos/autocomplete/#position
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1AutoComplete.Position")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PositionSettings Position
        {
            get
            {
                if (this._position == null)
                {
                    this._position = new PositionSettings();
                }
                return this._position;
            }
            set
            {
                this._position = value;
            }
        }

        /// <summary>
        /// Defines the data to use, must be specified. See Overview section for more details, and look at the various demos.
        /// Reference: http://jqueryui.com/demos/autocomplete/#source
        /// </summary>
        [C1Description("C1AutoComplete.Source")]
        [WidgetOption]
        [C1Category("Category.Behavior")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public AutoCompleteSourceOption Source
        {
            get
            {
                if (this._source == null)
                {
                    this._source = new AutoCompleteSourceOption();
                }
                return this._source;
            }
        }
        

        #endregion

        #region ****** Client-side events

        /// <summary>
        /// The name of the function which will be called when autocomplete is created.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientCreate")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("create")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientCreate
        {
            get
            {
                return GetPropertyValue("OnClientCreate", "");
            }
            set
            {
                SetPropertyValue("OnClientCreate", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called Before a request (source-option) is started, 
        /// after minLength and delay are met. 
        /// Can be canceled (return false), 
        /// then no request will be started and no items suggested.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientSearch")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("search")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientSearch
        {
            get
            {
                return GetPropertyValue("OnClientSearch", "");
            }
            set
            {
                SetPropertyValue("OnClientSearch", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called when the suggestion menu is opened.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientOpen")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("open")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientOpen
        {
            get
            {
                return GetPropertyValue("OnClientOpen", "");
            }
            set
            {
                SetPropertyValue("OnClientOpen", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called when the list is hidden - doesn't have to occur together with a change.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientClose")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("close")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientClose
        {
            get
            {
                return GetPropertyValue("OnClientClose", "");
            }
            set
            {
                SetPropertyValue("OnClientClose", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called before focus is moved to an item (not selecting), 
        /// ui.item refers to the focused item. 
        /// The default action of focus is to replace the text field's value with the value of the focused item, 
        /// though only if the focus event was triggered by a keyboard interaction. 
        /// Canceling this event prevents the value from being updated, 
        /// but does not prevent the menu item from being focused.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientFocus")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("focus")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientFocus
        {
            get
            {
                return GetPropertyValue("OnClientFocus", "");
            }
            set
            {
                SetPropertyValue("OnClientFocus", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called when an item is selected from the menu; 
        /// ui.item refers to the selected item. The default action of select is to replace the text field's value with the value of the selected item. 
        /// Canceling this event prevents the value from being updated, 
        /// but does not prevent the menu from closing.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientSelect")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("select")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientSelect
        {
            get
            {
                return GetPropertyValue("OnClientSelect", "");
            }
            set
            {
                SetPropertyValue("OnClientSelect", value);
            }
        }
       

        /// <summary>
        /// The name of the function which will be called when the field is blurred, if the value has changed; ui.item refers to the selected item.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientChange")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("change")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientChange
        {
            get
            {
                return GetPropertyValue("OnClientChange", "");
            }
            set
            {
                SetPropertyValue("OnClientChange", value);
            }
        }

        /* add for jquery-ui 1.9*/
        /// <summary>
        /// The name of the function which will be called when a search completes, even if the menu will not be shown because there are no results or the Autocomplete is disabled.
        /// </summary>
        [C1Description("C1AutoComplete.OnClientResponse")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("response")]
        [DefaultValue("")]
        [Layout(LayoutType.Behavior)]
        public string OnClientResponse
        {
            get
            {
                return GetPropertyValue("OnClientResponse", "");
            }
            set
            {
                SetPropertyValue("OnClientResponse", value);
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Gets a <see cref="C1AutoCompleteDataItemCollection"/> 
        /// object that contains the items of the current <see cref="C1AutoComplete"/> control.
        /// </summary>
        [C1Description("C1AutoComplete.Items")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [RefreshProperties(RefreshProperties.All)]
        [CollectionItemType(typeof(C1AutoCompleteDataItem))]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [Layout(LayoutType.Appearance)]
        [WidgetOption]
        public C1AutoCompleteDataItemCollection Items
        {
            get
            {
                if (this._items == null)
                {
                    this._items = new C1AutoCompleteDataItemCollection(this);
                }
                return this._items;
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the label.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1AutoComplete.DataLabelField")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataLabelField
        {
            get
            {
                return this.GetPropertyValue<string>("DataLabelField", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataLabelField", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the value.
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1AutoComplete.DataValueField")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataValueField
        {
            get
            {
                return this.GetPropertyValue<string>("DataValueField", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataValueField", value);
            }
        }

        /// <summary>
        /// Gets or sets the name of the field from the data source to bind to the category.       
        /// </summary>
        [DefaultValue("")]
        [C1Description("C1AutoComplete.DataCategoryField")]
        [C1Category("Category.Data")]
        [Layout(LayoutType.Misc)]
        public string DataCategoryField
        {
            get
            {
                return this.GetPropertyValue<string>("DataCategoryField", "");
            }
            set
            {
                this.SetPropertyValue<string>("DataCategoryField", value);
            }
        }

        /// <summary>
        /// Gets or sets the value that indicates whether or not Loads on demand is enabled.
        /// If it set to true, the auto complete will get suggestions from datasource you specified.
        /// </summary>
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [C1Description("C1AutoComplete.LoadOnDemand")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool LoadOnDemand
        {
            get
            {
                return GetPropertyValue("LoadOnDemand", false);
            }
            set
            {
                SetPropertyValue("LoadOnDemand", value);
            }
        }


        /// <summary>
        /// Gets or sets the value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
        /// </summary>
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [C1Description("C1AutoComplete.AutoPostBack")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public bool AutoPostBack
        {
            get
            {
                return GetPropertyValue("AutoPostBack", false);
            }
            set
            {
                SetPropertyValue("AutoPostBack", value);
            }
        }

        /// <summary>
        /// Gets or sets the value that indicates the max number of callback results 
        /// (you must specified the data source and set property "LoadOnDemand" to true) . 
        /// </summary>
        [DefaultValue(12)]
        [C1Category("Category.Behavior")]
        [C1Description("C1AutoComplete.MaxCount")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public virtual int MaxCount
        {
            get
            {
                return GetPropertyValue("MaxCount", 12);
            }
            set
            {
                SetPropertyValue("MaxCount", value);
            }
        }

        /// <summary>
        /// The attribute "maxlength" of textbox. 
        /// </summary>
        [DefaultValue(0)]
        [C1Category("Category.Behavior")]
        [C1Description("C1AutoComplete.MaxLength")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public virtual int MaxLength
        {
            get
            {
                return GetPropertyValue("MaxLength", 0);
            }
            set
            {
                SetPropertyValue("MaxLength", value);
            }
        }

        /// <summary>
        /// Gets or sets the text of autocomplete control.
        /// </summary>
        [DefaultValue("")]
        [C1Category("Category.Behavior")]
        [C1Description("C1AutoComplete.Text")]
        [Layout(LayoutType.Behavior)]
        [WidgetOption]
        public virtual string Text
        {
            get
            {
                return GetPropertyValue("Text", "");
            }
            set
            {
                SetPropertyValue("Text", value);
            }
        }

        [Browsable(false)]
        [DefaultValue("")]
        [WidgetOption]
        public override string UniqueID
        {
            get
            {
                return base.UniqueID;
            }
        }

        #endregion

        #region ** DataBind method

        /// <summary>
        /// This method overrides the generic DataBind method to bind the related data to the control.
        /// </summary>
        public sealed override void DataBind()
        {
            base.DataBind();
        }

        // <summary>
        /// Binds data from the data source to the control.
        /// </summary>
        /// <param name="retrievedData">The IEnumerable list of data returned from a PerformSelect method call.</param>
        protected override void PerformDataBinding(IEnumerable data)
        {
            base.PerformDataBinding(data);
            if (IsDesignMode || data == null)
            {
                return;
            }

            this._data = data;

            IEnumerator enumerator = data.GetEnumerator();
            this.Items.Clear();
            
            if (this.LoadOnDemand)
            {
                return;    
            }

            while (enumerator.MoveNext())
            {
                object itemData = enumerator.Current;
                C1AutoCompleteDataItem item =  GenerateItemFromData(itemData);

                C1AutoCompleteDataItemEventArgs arg = new C1AutoCompleteDataItemEventArgs(item);
                OnItemDataBinding(arg);
                this.Items.Add(item);
                OnItemDataBound(arg);
            }
        }

        #endregion

        #region ** Events

        ///<summary>
        /// Occurs before <see cref="C1AutoCompleteDataItem"/> is created in data binding.
        ///</summary>
        [C1Category("Category.Misc")]
        [C1Description("C1AutoComplete.ItemDataBinding")]
        public event C1AutoCompleteDataItemEventHandler ItemDataBinding;

        private void OnItemDataBinding(C1AutoCompleteDataItemEventArgs e)
        {
            if (ItemDataBinding != null)
            {
                ItemDataBinding(this, e);
            }
        }

        ///<summary>
        /// Occurs after <see cref="C1AutoCompleteDataItem"/> is created in data binding.
        ///</summary>
        [C1Category("Category.Misc")]
        [C1Description("C1AutoComplete.ItemDataBound")]
        public event C1AutoCompleteDataItemEventHandler ItemDataBound;

        private void OnItemDataBound(C1AutoCompleteDataItemEventArgs e)
        {
            if (ItemDataBound != null)
            {
                ItemDataBound(this, e);
            }
        }

        ///<summary>
        /// Occurs after text of <see cref="C1AutoComplete"/> is changed.
        ///</summary>
        [C1Category("Category.Misc")]
        [C1Description("C1AutoComplete.TextChanged")]
        public event EventHandler TextChanged;

        private void OnTextChanged(EventArgs e)
        {
            if (TextChanged != null)
            {
                TextChanged(this, e);
            }
        }

        ///<summary>
        /// Occurs after item of <see cref="C1AutoComplete"/> is selected.
        ///</summary>
        [C1Category("Category.Misc")]
        [C1Description("C1AutoComplete.ItemSelected")]
        public event C1AutoCompleteDataItemEventHandler ItemSelected;

        private void OnItemSelected(C1AutoCompleteDataItemEventArgs e)
        {
            if (ItemSelected != null)
            {
                ItemSelected(this, e);
            }
        }

        #endregion

        #region ** private methods

        internal virtual void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1AutoComplete), this, false);
            _shouldNag = licinfo.ShouldNag;
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        
        /// <summary>
        /// Generate <see cref="C1AutoCompleteDataItem"/> item from the item data in data source.
        /// </summary>
        internal C1AutoCompleteDataItem GenerateItemFromData(object itemData)
        {
            C1AutoCompleteDataItem item = new C1AutoCompleteDataItem();

            if (!String.IsNullOrEmpty(this.DataLabelField))
            {
                item.Label = DataBinder.GetPropertyValue(itemData, this.DataLabelField).ToString();
            }
            else
            {
                item.Label = GetValueByIndex(0, itemData);
            }

            if (!String.IsNullOrEmpty(this.DataValueField))
            {
                item.Value = DataBinder.GetPropertyValue(itemData, this.DataValueField).ToString();
            }
            else
            {
                item.Value = GetValueByIndex(1, itemData);
            }

            if (!String.IsNullOrEmpty(this.DataCategoryField))
            {
                item.Category = DataBinder.GetPropertyValue(itemData, this.DataCategoryField).ToString();
            }
            else
            {
                item.Category = GetValueByIndex(2, itemData);
            }

            return item;
        }

        /// <summary>
        /// get value from data source.
        /// </summary>
        /// <returns>value if the item</returns>
        private string GetValueByIndex(int index, object dataItem)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(dataItem);
            string contentContent = String.Empty;

            if (props.Count >= 2)
            {
                if (null != props[1].GetValue(dataItem))
                {
                    contentContent = props[1].GetValue(dataItem).ToString();
                }
            }
            return contentContent;
        }

        #endregion

        #region ** IPostBackDataHandler interface implementations
        /// <summary>
        /// Processes postback data for a <see cref="C1AutoComplete"/> control.
        /// </summary>
        /// <param name="postDataKey">
        /// The key identifier for the control.
        /// </param>
        /// <param name="postCollection">
        /// The collection of all incoming name values.
        /// </param>
        /// <returns>
        /// Returns true if the server control's state changes as a result of the postback;
        /// otherwise, false.
        /// </returns>
        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

            this.RestoreStateFromJson(data);

            return CheckTextChanged(postCollection[postDataKey]);
        }

        private bool CheckTextChanged(string newValue) 
        {
            if (this.LoadOnDemand && this.Page.IsCallback)
            {
                return false;
            }
            string oldValue = this.Text;
            if (!newValue.Equals(oldValue))
            {
                Text = newValue;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Signals the <see cref = "C1Splitter"/> control to notify the ASP.NET
        /// application that the state of the control has changed.
        /// </summary>
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            this.RaisePostDataChangedEvent();
        }

        protected virtual void RaisePostDataChangedEvent()
        {
            if (AutoPostBack && !this.Page.IsPostBackEventControlRegistered)
            {
                this.Page.AutoPostBackControl = this;
            }

            OnTextChanged(EventArgs.Empty);
        }

        #endregion end of ** IPostBackDataHandler interface implementations

        #region ** IPostBackEventHandler Members

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            if (!string.IsNullOrEmpty(eventArgument) && 
                eventArgument.StartsWith("itemSelected_"))
            {
                string key = eventArgument.Replace("itemSelected_","");
                C1AutoCompleteDataItem item = this.GetItemByKey(key);
                C1AutoCompleteDataItemEventArgs args = 
                    new C1AutoCompleteDataItemEventArgs(item);
                this.OnItemSelected(args);
            }
        }

        private C1AutoCompleteDataItem GetItemByKey(string key)
        {
            return null;
        }

        #endregion

        #region ** ICallBackEventHandler

        private string _response;

        /// <summary>
        /// Implement of ICallbackEventHandler.GetCallbackResult
        /// </summary>
        string ICallbackEventHandler.GetCallbackResult()
        {
            return _response;
        }

        /// <summary>
        /// Implement of ICallbackEventHandler.RaiseCallbackEvent
        /// </summary>
        void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
        {
            try
            {
                Hashtable data = JsonHelper.StringToObject(eventArgument);
                string commandName = (string)data["CommandName"];
                Hashtable commandData = data["CommandData"] as Hashtable;
                string term = "";
                if (commandData != null)
                {
                    term = commandData["term"] as string;
                }
                _response = ProcessAjaxCommand(commandName, term);
            }
            catch (Exception ex)
            {
                _response = ex.Message;
            }
        }

        /// <summary>
        /// Identify the action to take as response to the client request
        /// </summary>
        private string ProcessAjaxCommand(string commandName, string term)
        {
            if (commandName == "GetItems" && !string.IsNullOrEmpty(term))
            {
                object callbackData = LoadRequiredData(term);
                return JsonHelper.WidgetToString(callbackData, this);
            }
            return "";
        }

        /// <summary>
        /// Load callback data of specified nodes.
        /// </summary>
        private IEnumerable LoadRequiredData(string term)
        {
            base.DataBind();
            IList<object> bindItems = new List<object>() { };

            int count = 0;
            if (this._data != null)
            {
                foreach (object itemData in this._data)
                {
                    if (this.MaxCount == count)
                        break;

                    C1AutoCompleteDataItem item = GenerateItemFromData(itemData);

                    if (item.Label.ToLower().Contains(term.ToLower()))
                    {
                        bindItems.Add(item);
                        count++;
                    }                    
                }
            }
            return bindItems;
        }

        #endregion

        #region ** IC1Serializable

        /// <summary>
        /// Saves the control layout properties to the file.
        /// </summary>
        /// <param name="filename">The file where the values of the layout properties will be saved.</param> 
        public void SaveLayout(string filename)
        {
            C1AutoCompleteSerializer sz = new C1AutoCompleteSerializer(this);
            sz.SaveLayout(filename);
        }

        /// <summary>
        /// Saves control layout properties to the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be saved.</param> 
        public void SaveLayout(Stream stream)
        {
            C1AutoCompleteSerializer sz = new C1AutoCompleteSerializer(this);
            sz.SaveLayout(stream);
        }

        /// <summary>
        /// Loads control layout properties from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        public void LoadLayout(string filename)
        {
            LoadLayout(filename, LayoutType.All);
        }

        /// <summary>
        /// Load control layout properties from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
        public void LoadLayout(Stream stream)
        {
            LoadLayout(stream, LayoutType.All);
        }

        /// <summary>
        /// Loads control layout properties with specified types from the file.
        /// </summary>
        /// <param name="filename">The file where the values of layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        public void LoadLayout(string filename, LayoutType layoutTypes)
        {
            C1AutoCompleteSerializer sz = new C1AutoCompleteSerializer(this);
            sz.LoadLayout(filename, layoutTypes);
        }

        /// <summary>
        /// Loads the control layout properties with specified types from the stream.
        /// </summary>
        /// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
        /// <param name="layoutTypes">The layout types to load.</param>
        public void LoadLayout(Stream stream, LayoutType layoutTypes)
        {
            C1AutoCompleteSerializer sz = new C1AutoCompleteSerializer(this);
            sz.LoadLayout(stream, layoutTypes);
        }

        #endregion
    }
}
