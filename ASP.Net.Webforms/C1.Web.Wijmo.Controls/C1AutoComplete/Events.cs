﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1AutoComplete
{
    ///<summary>
    /// Represents the method that will handle an data binding events of <see cref="C1AutoCompleteDataItem"/>.
    /// Such as <see cref="C1AutoCompleteDataItem.BoundItemCreated"/> event and <see cref="C1AutoCompleteDataItem.ItemDataBinding"/> event.
    ///</summary>
    ///<param name="sender">The source of the event.</param>
    ///<param name="args">An <see cref="C1CarouselItemEventArgs"/> that contains event data.</param>
    public delegate void C1AutoCompleteDataItemEventHandler(object sender, C1AutoCompleteDataItemEventArgs args);

    /// <summary>
    /// Contains event data for data binding events.
    /// Such as <see cref="C1AutoCompleteDataItem.BoundItemCreated"/> event and <see cref="C1AutoCompleteDataItem.ItemDataBinding"/> event.
    /// </summary>
    public class C1AutoCompleteDataItemEventArgs : EventArgs
    {
        private C1AutoCompleteDataItem _item;

        /// <summary>
        /// Gets the <see cref="C1AutoCompleteDataItem"/> related with this event.
        /// </summary>
        public C1AutoCompleteDataItem Item
        {
            get
            {
                return this._item;
            }
        }

        /// <summary>
        /// Initializes a new instance of the C1CarouselItem class.
        /// </summary>
        /// <param name="item"><see cref="C1AutoCompleteDataItem"/> related with this event</param>
        public C1AutoCompleteDataItemEventArgs(C1AutoCompleteDataItem item)
        {
            this._item = item;
        }
    }
}
