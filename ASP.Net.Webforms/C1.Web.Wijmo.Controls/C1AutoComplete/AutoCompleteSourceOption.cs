﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1AutoComplete
{
    public class AutoCompleteSourceOption : ICustomOptionType, IJsonEmptiable
    {
        /// <summary>
        /// Gets or sets an array can be used for local data.
        /// </summary>
        [C1Description("C1AutoComplete.AutoCompleteSourceOption.Labels")]
        public string[] Labels
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a string to point to a URL resource that will return JSON data.
        /// </summary>
        [C1Description("C1AutoComplete.AutoCompleteSourceOption.Url")]
        public string Url
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a callback function that provides the most flexibility and can be used to connect any data source to Autocomplete.
        /// </summary>
        [C1Description("C1AutoComplete.AutoCompleteSourceOption.CallBack")]
        public string CallBack
        {
            get;
            set;
        }

        string ICustomOptionType.SerializeValue()
        {
            if (Labels!=null && Labels.Length > 0)
            {
                return string.Format("[\"{0}\"]", string.Join("\",\"", Labels));
            }
            else if (string.IsNullOrEmpty(Url))
            {
                return string.Format("\"{0}\"", Url);
            }
            else if(string.IsNullOrEmpty(CallBack))
            {
                return CallBack;
            }
            return null;
        }

        bool ICustomOptionType.IncludeQuotes
        {
            get {
                return false;
            }
        }

        void ICustomOptionType.DeSerializeValue(object state)
        {

        }

        bool IJsonEmptiable.IsEmpty
        {
			get
			{
				return (Labels == null || Labels.Length == 0) && string.IsNullOrEmpty(Url) && string.IsNullOrEmpty(CallBack);
			}
        }

    }
}
