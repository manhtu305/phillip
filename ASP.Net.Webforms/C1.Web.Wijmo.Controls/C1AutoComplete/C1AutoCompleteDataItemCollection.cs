﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using C1.Web.Wijmo.Controls.Base.Collections;

namespace C1.Web.Wijmo.Controls.C1AutoComplete
{
    public class C1AutoCompleteDataItemCollection : C1ObservableItemCollection<C1AutoComplete, C1AutoCompleteDataItem>
    {
        #region ** constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="C1AutoCompleteDataItem"/> class..
		/// </summary>
		/// <param name="owner">Parent ComboBox.</param>
        public C1AutoCompleteDataItemCollection(C1AutoComplete owner)
			: base(owner)
		{ }

        #endregion

        #region ** methods
        /// <summary>
        /// Adds <see cref="C1AutoCompleteDataItem"/> node to the collection.
        /// </summary>
        /// <param name="item">item to add.</param>
        public new void Add(C1AutoCompleteDataItem item)
        {
            if (item != null)
            {
                Insert(base.Count, item);
            }
        }

        /// <summary>
        /// Insert a <see cref="C1AutoCompleteDataItem"/> item into a specific position in the collection.
        /// </summary>
        /// <param name="index">Item position. Value should be greater or equal to 0</param>
        /// <param name="child">The new <see cref="C1AutoCompleteDataItem"/> item.</param>
        public new void Insert(int index, C1AutoCompleteDataItem child)
        {
            if (this.Count < index)
                this.InsertItem(this.Count, child);
            else
                this.InsertItem(index, child);
        }
        #endregion
    }
}
