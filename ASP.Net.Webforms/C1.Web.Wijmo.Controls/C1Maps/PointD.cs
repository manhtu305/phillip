﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Represents an ordered pair of double x- and y-coordinates that defines a point in a two-dimensional plane.
	/// </summary>
	[Serializable]
	[TypeConverter(typeof(PointDConverter))]
	public struct PointD: ICustomOptionType
	{
		/// <summary>
		/// Represents a <see cref="PointD"/> that has <see cref="X"/> and <see cref="Y"/> values set to zero.
		/// </summary>
		public static readonly PointD Empty = new PointD();

		private double _x;
		private double _y;

		/// <summary>
		/// Initializes a new instance of the PointD class with the specified coordinates.
		/// </summary>
		/// <param name="x">The horizontal position of the point.</param>
		/// <param name="y">The vertical position of the point.</param>
		public PointD(double x, double y)
		{
			_x = x;
			_y = y;
		}

		/// <summary>
		/// Gets or sets the x-coordinate of this point.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("PointD.X")]
		[DefaultValue(0d)]
		[Json(true)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double X
		{
			get { return _x; }
			set { _x = value; }
		}

		/// <summary>
		/// Gets or sets the y-coordinate of this point.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("PointD.Y")]
		[DefaultValue(0d)]
		[Json(true)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double Y
		{
			get { return _y; }
			set { _y = value; }
		}

		/// <summary>
		/// Compares two PointD objects. 
		/// The result specifies whether the values of the <see cref="X"/> and 
		/// <seealso cref="Y"/> properties of the two PointD objects are equal.
		/// </summary>
		/// <param name="left">A <see cref="PointD"/> to compare.</param>
		/// <param name="right">A <see cref="PointD"/> to compare.</param>
		/// <returns><c>true</c> if the <see cref="X"/> and <see cref="Y"/> values of the 
		/// <paramref name="left"/> and <paramref name="right"/> are equal; 
		/// otherwise, <c>false</c>.</returns>
		public static bool operator ==(PointD left, PointD right)
		{
			return left.Equals(right);
		}

		/// <summary>
		/// Compares two PointD objects.
		/// The result specifies whether the values of the <seealso cref="X"/> and
		/// <see cref="Y"/> properties of the two PointD objects are unequal.
		/// </summary>
		/// <param name="left">A <see cref="PointD"/> to compare.</param>
		/// <param name="right">A <see cref="PointD"/> to compare.</param>
		/// <returns><c>true</c> if the <see cref="X"/> and <see cref="Y"/> values of the 
		/// <paramref name="left"/> and <paramref name="right"/> differ; 
		/// otherwise, <c>false</c>.</returns>
		public static bool operator !=(PointD left, PointD right)
		{
			return !(left.Equals(right));
		}

		/// <summary>
		/// Specifies whether this <see cref="PointD"/> contains the same coordinates as the specified <see cref="PointD"/>.
		/// </summary>
		/// <param name="obj">The <see cref="Object"/> to test.</param>
		/// <returns>
		/// <c>true</c> if <paramref name="obj"/> is a <see cref="PointD"/> 
		/// and has the same coordinates as this <see cref="PointD"/>.
		/// </returns>
		public override bool Equals(object obj)
		{
			if (!(obj is PointD)) return false;

			var that = (PointD)obj;
			return _x == that._x
				   && _y == that._y;
		}

		/// <summary>
		/// Returns a hash code for this <see cref="PointD"/>.
		/// </summary>
		/// <returns>
		/// An integer value that specifies a hash value for this <see cref="PointD"/>.
		/// </returns>
		public override int GetHashCode()
		{
			return _x.GetHashCode() ^ _y.GetHashCode();
		}


		#region ICustomOptionType

		string ICustomOptionType.SerializeValue()
		{
			return "{"+string.Format("\"x\":{0}, \"y\":{1}", X, Y)+"}";
		}

		void ICustomOptionType.DeSerializeValue(object state)
		{
			var ht = state as Hashtable;
			if (ht != null)
			{
				if (ht["x"] != null)
				{
					_x = Convert.ToDouble(ht["x"]);
				} 
				
				if (ht["y"] != null)
				{
					_y = Convert.ToDouble(ht["y"]);
				}
			}
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#endregion
	}
}
