﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.C1Rdl.Rdl2008;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps.GeoJson
{
	/// <summary>
	/// Specifies the type of the GeoJSON object.
	/// </summary>
	public enum GeoJsonObjectType
	{
		/// <summary>
		/// The GeooJSON Point geometry object.
		/// </summary>
		Point,
		/// <summary>
		/// The GeoJSON MultiPoint geometry object.
		/// </summary>
		MultiPoint,
		/// <summary>
		/// The GeoJSON LineString geometry object.
		/// </summary>
		LineString,
		/// <summary>
		/// The GeoJSON MultiLineString geometry object.
		/// </summary>
		MultiLineString,
		/// <summary>
		/// The GeoJSON Polygon geometry object.
		/// </summary>
		Polygon,
		/// <summary>
		/// The GeoJSON MultiPolygon geometry object.
		/// </summary>
		MultiPolygon,
		/// <summary>
		/// The GeoJSON GeometryCollection geometry object.
		/// </summary>
		GeometryCollection,
		/// <summary>
		/// The GeoJSON Feature object.
		/// </summary>
		Feature,
		/// <summary>
		/// The GeoJSON FeatureCollection object.
		/// </summary>
		FeatureCollection
	}

	/// <summary>
	/// Reprensents a general GeoJSON object.
	/// </summary>
	public abstract class GeoJsonObject : Settings
	{
		/// <summary>
		/// Initializes a new instance of <see cref="GeoJsonObject"/> object.
		/// </summary>
		/// <param name="type">The type of the GeoJSON object.</param>
		protected GeoJsonObject(GeoJsonObjectType type)
		{
			Type = type.ToString();
		}

		/// <summary>
		/// Gets the type of the GeoJSON object.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonObject.Type")]
		[WidgetOption]
		[Json(true)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string Type
		{
			get { return GetPropertyValue("Type", ""); }
			internal set { SetPropertyValue("Type", value); }
		}
	}

	/// <summary>
	/// Represents a general GeoJSON geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public abstract class GeometryObject : GeoJsonObject
	{
		/// <summary>
		/// Initializes a new instance of <see cref="GeometryObject"/> object.
		/// </summary>
		/// <param name="type">The type of the GeoJSON object.</param>
		protected GeometryObject(GeoJsonObjectType type) : base(type)
		{
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		protected void DeserializePolygonCoordinatesList(List<PolygonCoordinates> coordinates, object value)
		{
			var list = value as ArrayList;
			if(list == null) return;

			foreach (var item in list)
			{
				var subCoordinates = new PolygonCoordinates();
				DeserializeLineCoordinatesList(subCoordinates.LineStrings, item);
				coordinates.Add(subCoordinates);
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		protected void DeserializeLineCoordinatesList(List<LineStringCoordinates> coordinates, object value)
		{
			var list = value as ArrayList;
			if (list == null) return;

			foreach (var item in list)
			{
				var subCoordinates = new LineStringCoordinates();
				DeserializePositionList(subCoordinates.Positions, item);
				coordinates.Add(subCoordinates);
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		protected void DeserializePositionList(List<Position> coordinates, object value)
		{
			var list = value as ArrayList;
			if (list == null) return;

			foreach (var item in list)
			{
				var position = new Position();
				((ICustomOptionType)position).DeSerializeValue(item);
				coordinates.Add(position);
			}
		}
	}

	/// <summary>
	/// Represents a GeoJSON Point geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class Point : GeometryObject
	{
		private Position _position = new Position();

		/// <summary>
		/// Initializes a new instance of <see cref="Point"/> object.
		/// </summary>
		public Point() : base(GeoJsonObjectType.Point)
		{
		}

		/// <summary>
		/// For internal use only. Gets or sets the coordinates of the object.
		/// </summary>
		/// <remarks>
		/// The coordinates member is a single <see cref="Position"/>.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPoint.Coordinates")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Position Coordinates
		{
			get { return _position; }
			set { _position = value; }
		}

		/// <summary>
		/// Gets or sets the position of the GeoJSON <see cref="Point"/> object.
		/// </summary>
		/// <remarks>
		/// The coordinates member is a single <see cref="Position"/>.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPoint.Position")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Position Position
		{
			get { return _position; }
			set { _position = value; }
		}
	}

	/// <summary>
	/// Represents the GeoJSON MultiPoint geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MultiPoint : GeometryObject
	{
		private List<Position> _positions = new List<Position>();

		/// <summary>
		/// /Initializes a new instance of <see cref="MultiPoint"/> object.
		/// </summary>
		public MultiPoint() : base(GeoJsonObjectType.MultiPoint)
		{
		}

		/// <summary>
		/// For internal use only. Gets the coordinates of the object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of <see cref="Position"/>.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPoint.Coordinates")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[Json(true)]
		[CollectionItemType(typeof(Position))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<Position> Coordinates
		{
			get { return _positions; }
		}

		/// <summary>
		/// Gets the positions of the GeoJSON <see cref="MultiPoint"/> object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of <see cref="Position"/>.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPoint.Positions")]
		[CollectionItemType(typeof(Position))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<Position> Positions
		{
			get { return _positions; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPoint.Positions")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("Positions")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<Position> InnerPositions
		{
			get
			{
				return _positions;
			}
			set
			{
				_positions = value ?? new List<Position>();
			}
		}
	}

	/// <summary>
	/// Represents the GeoJSON LineString geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class LineString : GeometryObject
	{
		private List<Position> _positions = new List<Position>();

		/// <summary>
		/// Intializes a new instance of <see cref="LineString"/> object.
		/// </summary>
		public LineString() : base(GeoJsonObjectType.LineString)
		{
		}

		/// <summary>
		/// For internal use only. Gets the coordinates of the object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of two or more <see cref="Position"/>.<p/>
		/// A LinearRing is closed LineString with 4 or more positions. 
		/// The first and  last positions are equivalent (they represent equivalent points). 
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonLineString.Coordinates")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[Json(true)]
		[CollectionItemType(typeof(Position))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<Position> Coordinates
		{
			get { return _positions; }
		}

		/// <summary>
		/// Gets the positions of the GeoJSON <see cref="LineString"/> object.
		/// </summary>
		/// <remarks>
		/// The positions is a collection of two or more <see cref="Position"/>.<p/>
		/// A LinearRing is closed LineString with 4 or more positions. 
		/// The first and  last positions are equivalent (they represent equivalent points). 
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonLineString.Positions")]
		[CollectionItemType(typeof(Position))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<Position> Positions
		{
			get { return _positions; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("GeoJsonLineString.Positions")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("Positions")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<Position> InnerPositions
		{
			get { return _positions; }
			set { _positions = value ?? new List<Position>(); }
		}
	}

	/// <summary>
	/// Represents the GeoJSON MultiLineString geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MultiLineString : GeometryObject
	{
		private List<LineStringCoordinates> _lines = new List<LineStringCoordinates>();

		/// <summary>
		/// Initializes a new instance of <see cref="MultiLineString"/> object.
		/// </summary>
		public MultiLineString()
			: base(GeoJsonObjectType.MultiLineString)
		{
		}

		/// <summary>
		/// For internal use only. Gets the coordinates of the object.
		/// </summary>
		/// <remarks>
		/// The coordinates a collection of <see cref="LineString"/> coordinates collection.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiLineString.Coordinates")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<List<Position>> Coordinates
		{
			get
			{
				return _lines.Select(line => line.Positions).ToList();
			}
		}

		/// <summary>
		/// Gets the LineStrings of the GeoJSON <see cref="MultiLineString"/> object.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiLineString.LineStrings")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<LineStringCoordinates> LineStrings
		{
			get { return _lines; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiLineString.LineStrings")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("LineStrings")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<LineStringCoordinates> InnerLineStrings
		{
			get
			{
				return _lines;
			}
			set
			{
				_lines = value ?? new List<LineStringCoordinates>();
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeCoordinates(object value)
		{
			_lines.Clear();
			DeserializeLineCoordinatesList(_lines, value);
		}
	}

	/// <summary>
	/// Represents the GeoJSON Polygon geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class Polygon : GeometryObject
	{
		private List<LineStringCoordinates> _lines = new List<LineStringCoordinates>();

		/// <summary>
		/// Initializes a new instance of <see cref="Polygon"/> object.
		/// </summary>
		public Polygon()
			: base(GeoJsonObjectType.Polygon)
		{
		}

		/// <summary>
		/// For internal use only. Gets the coordinates of the object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of <see cref="LineString"/> coordinates.<p/>
		/// For Polygons with multiple rings, the first must be the exterior ring and any others must be interior rings or holes.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPolygon.Coordinates")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[Json(true)]
		[CollectionItemType(typeof(List<Position>))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<List<Position>> Coordinates
		{
			get
			{
				return _lines.Select(line => line.Positions).ToList();
			}
		}

		/// <summary>
		/// Gets the LineStrings of the GeoJSON <see cref="Polygon"/> object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of <see cref="LineStringCoordinates"/>
		/// For Polygons with multiple rings, the first must be the exterior ring and any others must be interior rings or holes.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPolygon.LineStrings")]
		[CollectionItemType(typeof(List<Position>))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<LineStringCoordinates> LineStrings
		{
			get { return _lines; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPolygon.LineStrings")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("LineStrings")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<LineStringCoordinates> InnerLineStrings
		{
			get
			{
				return _lines;
			}
			set
			{
				_lines = value ?? new List<LineStringCoordinates>();
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeCoordinates(object value)
		{
			_lines.Clear();
			DeserializeLineCoordinatesList(_lines, value);
		}
	}

	/// <summary>
	/// Represents the GeoJSON MultiPolygon geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MultiPolygon : GeometryObject
	{
		private List<PolygonCoordinates> _polygons = new List<PolygonCoordinates>();

		/// <summary>
		/// Initializes a new instance of <see cref="MultiPolygon"/> object.
		/// </summary>
		public MultiPolygon()
			: base(GeoJsonObjectType.MultiPolygon)
		{
		}

		/// <summary>
		/// For internal use only. Gets the coordinates of the object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of <see cref="Polygon"/> coordinates collection.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPolygon.Coordinates")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<List<List<Position>>> Coordinates
		{
			get
			{
				return _polygons.Select(
					polygon => polygon.LineStrings.Select(
						line => line.Positions
						).ToList()
					).ToList();
			}
		}

		/// <summary>
		/// Gets the polygons of the GeoJSON <see cref="MultiPolygon"/> object.
		/// </summary>
		/// <remarks>
		/// The coordinates is a collection of <see cref="PolygonCoordinates"/> coordinates collection.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPolygon.Polygons")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<PolygonCoordinates> Polygons
		{
			get { return _polygons; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPolygon.Polygons")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("Polygons")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<PolygonCoordinates> InnerPolygons
		{
			get
			{
				return _polygons;
			}
			set
			{
				_polygons = value ?? new List<PolygonCoordinates>();
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeCoordinates(object value)
		{
			_polygons.Clear();
			DeserializePolygonCoordinatesList(_polygons, value);
		}
	}

	/// <summary>
	/// Represents the GeoJSON GeometryCollection geometry object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class GeometryCollection : GeometryObject
	{
		private List<GeometryObject> _geometries = new List<GeometryObject>();

		/// <summary>
		/// Intializes a new instance of <see cref="GeometryCollection"/> object.
		/// </summary>
		public GeometryCollection()
			: base(GeoJsonObjectType.GeometryCollection)
		{
		}

		/// <summary>
		/// Gets the collection of GeoJSON <see cref="GeometryObject"/>s.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonGeometryCollection.Geometries")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonGeometriesEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonGeometriesEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonGeometriesEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		public List<GeometryObject> Geometries
		{
			get { return _geometries; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("GeoJsonGeometryCollection.Geometries")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("Geometries")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonGeometriesEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonGeometriesEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonGeometriesEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		public List<GeometryObject> InnerGeometries
		{
			get
			{
				return _geometries;
			}
			set
			{
				_geometries = value ?? new List<GeometryObject>();
			}
		}
	}

	/// <summary>
	/// Represents the GeoJSON Feature object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public abstract class Feature : GeoJsonObject
	{
		private readonly IDictionary<string, object> _properties = new Dictionary<string, object>();
		//private GeometryObject _geometry;

		/// <summary>
		/// Initializes a new instance of <see cref="Feature"/> object.
		/// </summary>
		protected Feature()
			: base(GeoJsonObjectType.Feature)
		{
		}

		/// <summary>
		/// Gets or sets the identifier of this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonFeature.FeatureId")]
		[WidgetOption]
		[WidgetOptionName("id")]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string FeatureId
		{
			get { return GetPropertyValue("FeatureId", ""); }
			set { SetPropertyValue("FeatureId", value); }
		}

		/// <summary>
		/// Gets additional properties for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonFeature.Properties")]
		[WidgetOption]
		[Json(true)]
		[Browsable(false)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public IDictionary<string, object> Properties
		{
			get { return _properties; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="Point"/>.
	/// </summary>
	public class PointFeature : Feature
	{
		private Point _geometry = new Point();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="Point"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPointFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Point Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="MultiPoint"/>.
	/// </summary>
	public class MultiPointFeature : Feature
	{
		private MultiPoint _geometry = new MultiPoint();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="MultiPoint"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPointFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public MultiPoint Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="LineString"/>.
	/// </summary>
	public class LineStringFeature : Feature
	{
		private LineString _geometry = new LineString();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="LineString"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonLineStringFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public LineString Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="MultiLineString"/>.
	/// </summary>
	public class MultiLineStringFeature : Feature
	{
		private MultiLineString _geometry = new MultiLineString();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="MultiLineString"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiLineStringFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public MultiLineString Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="Polygon"/>.
	/// </summary>
	public class PolygonFeature : Feature
	{
		private Polygon _geometry = new Polygon();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="Polygon"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPolygonFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Polygon Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="MultiPolygon"/>.
	/// </summary>
	public class MultiPolygonFeature : Feature
	{
		private MultiPolygon _geometry = new MultiPolygon();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="MultiPolygon"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonMultiPolygonFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public MultiPolygon Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents a GeoJSON <see cref="Feature"/> object which geometry is a <see cref="GeometryCollection"/>.
	/// </summary>
	public class GeometryCollectionFeature : Feature
	{
		private GeometryCollection _geometry = new GeometryCollection();

		/// <summary>
		/// Gets or sets the GeoJSON <see cref="GeometryCollection"/> geometry for this feature.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonGeometryCollectionFeature.Geometry")]
		[WidgetOption]
		[Json(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public GeometryCollection Geometry
		{
			get { return _geometry; }
			set { _geometry = value; }
		}
	}

	/// <summary>
	/// Represents the GeoJSON FeatureCollection object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class FeatureCollection : GeoJsonObject
	{
		private readonly List<Feature> _features = new List<Feature>();

		/// <summary>
		/// Initializes a new instance of <see cref="FeatureCollection"/> object.
		/// </summary>
		public FeatureCollection()
			: base(GeoJsonObjectType.FeatureCollection)
		{
		}

		/// <summary>
		/// Gets the collection of <see cref="Feature"/> objects.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonFeatureCollection.Features")]
		[WidgetOption]
		[Json(true)]
		[CollectionItemType(typeof(Feature))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonFeaturesEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonFeaturesEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.GeoJsonFeaturesEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		public List<Feature> Features
		{
			get { return _features; }
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeFeatures(object value)
		{
			_features.Clear();

			var list = value as IList;
			if (list == null) return;

			foreach (object item in list)
			{
				var hsItem = item as Hashtable;
				if (hsItem == null) continue;

				var geometry = hsItem["geometry"] as Hashtable;
				if(geometry == null) continue;

				GeoJsonObjectType type;
				try
				{
					type = (GeoJsonObjectType) Enum.Parse(typeof (GeoJsonObjectType), geometry["type"].ToString(), true);
				}
				catch (Exception)
				{
					return;
				}

				Feature feature = null;
				switch (type)
				{
					case GeoJsonObjectType.Point:
						feature = new PointFeature();
						break;
					case GeoJsonObjectType.MultiPoint:
						feature = new MultiPointFeature();
						break;
					case GeoJsonObjectType.LineString:
						feature = new LineStringFeature();
						break;
					case GeoJsonObjectType.MultiLineString:
						feature = new MultiLineStringFeature();
						break;
					case GeoJsonObjectType.Polygon:
						feature = new PolygonFeature();
						break;
					case GeoJsonObjectType.MultiPolygon:
						feature = new MultiPolygonFeature();
						break;
					case GeoJsonObjectType.GeometryCollection:
						feature = new GeometryCollectionFeature();
						break;
				}

				if (feature != null)
				{
					((IJsonRestore) feature).RestoreStateFromJson(item);
					_features.Add(feature);
				}
			}
		}
	}

	/// <summary>
	/// Represents the coordinates of the geometry position.
	/// </summary>
	/// <remarks>
	/// The altitude is not support now.
	/// </remarks>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class Position : Settings, ICustomOptionType
	{
		/// <summary>
		/// Initializes a new instance of <see cref="Position"/> object.
		/// </summary>
		public Position()
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="Position"/> object.
		/// </summary>
		/// <param name="longitude">The longitude coordinate.</param>
		/// <param name="latitude">The latitude coordinate.</param>
		public Position(double longitude, double latitude)
		{
			Longitude = longitude;
			Latitude = latitude;
		}

		/// <summary>
		/// Initializes a new instance of <see cref="Position"/> object from <see cref="PointD"/>.
		/// </summary>
		/// <param name="point">The <see cref="PointD"/> object which represents the longitude and latitude coordinates.</param>
		public Position(PointD point)
		{
			Longitude = point.X;
			Latitude = point.Y;
		}

		/// <summary>
		/// Gets or sets the longitude coordinate of the position.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPosition.Longitude")]
		[DefaultValue(0d)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double Longitude
		{
			get { return GetPropertyValue("Longitude", 0d); }
			set { SetPropertyValue("Longitude", value); }
		}

		/// <summary>
		/// Gets or sets the latitude coordinate of the position.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("GeoJsonPosition.Latitude")]
		[DefaultValue(0d)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double Latitude
		{
			get { return GetPropertyValue("Latitude", 0d); }
			set { SetPropertyValue("Latitude", value); }
		}

		#region ICustomOptionType members
		string ICustomOptionType.SerializeValue()
		{
			return string.Format(CultureInfo.InvariantCulture, "[{0},{1}]", Longitude, Latitude);
		}

		void ICustomOptionType.DeSerializeValue(object state)
		{
			var list = state as IList;
			if (list == null) return;

			Longitude = double.Parse(list[0].ToString());
			Latitude = double.Parse(list[1].ToString());
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				return false;
			}
		}

		#endregion
	}

	/// <summary>
	/// Represents the coordinates of a line.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class LineStringCoordinates
	{
		private List<Position> _positions = new List<Position>();

		/// <summary>
		/// Gets the coordinates of the line.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("LineStringCoordinates.Positions")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<Position> Positions
		{
			get { return _positions; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("LineStringCoordinates.Positions")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("Positions")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<Position> InnerPositions
		{
			get
			{
				return _positions;
			}
			set
			{
				_positions = value ?? new List<Position>();
			}
		}
	}

	/// <summary>
	/// Represents the coordinates of a polygon.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class PolygonCoordinates
	{
		private List<LineStringCoordinates> _lines = new List<LineStringCoordinates>();

		/// <summary>
		/// Gets the coordinates of the polygon.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("PolygonCoordinates.LineStrings")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		public List<LineStringCoordinates> LineStrings
		{
			get { return _lines; }
		}

		/// <summary>
		/// Internal used only, for design time support.
		/// </summary>
		/// <remarks>
		/// In collection editor, the properties or the selected item will be wrapped with a "Value" property, 
		/// if the selected item only contains one readonly property.
		/// </remarks>
		[DesignOnly(true)]
		[C1Category("Category.Data")]
		[C1Description("PolygonCoordinates.LineStrings")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DisplayName("LineStrings")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<LineStringCoordinates> InnerLineStrings
		{
			get
			{
				return _lines;
			}
			set
			{
				_lines = value ?? new List<LineStringCoordinates>();
			}
		}
	}
}
