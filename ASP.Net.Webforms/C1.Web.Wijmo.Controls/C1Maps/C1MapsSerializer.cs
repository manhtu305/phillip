﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Serializes the C1Maps Control.
	/// </summary>
	public class C1MapsSerializer : C1BaseSerializer<C1Maps, object, object>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="C1MapsSerializer"/> class.
		/// </summary>
		/// <param name="obj">The <see cref="C1Maps"/> instance.</param>
		public C1MapsSerializer(object obj)
			: base(obj)
		{ }
	}
}
