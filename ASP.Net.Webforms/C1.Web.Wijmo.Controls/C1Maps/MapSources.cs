﻿using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Represents the tile source for the <see cref="C1Maps"/>.
	/// </summary>
	[ParseChildren(true)]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MultiScaleTileSource : Settings
	{
		private const int DefaultTileWidth = 256;
		private const int DefaultTileHeight = 256;
		private const int DefaultMinZoom = 1;
		private const int DefaultMaxZoom = 19;
		private const string DefaultGetUrl = "";

		/// <summary>
		/// Gets or sets the tile width of the image.
		/// </summary>
		/// <remarks>
		/// The default value is 256.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("MultiScaleTileSource.TileWidth")]
		[DefaultValue(DefaultTileWidth)]
		[Json(true)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		public virtual int TileWidth
		{
			get { return GetPropertyValue("TileWidth", DefaultTileWidth); }
			set { SetPropertyValue("TileWidth", value); }
		}

		/// <summary>
		/// Gets or sets the tile height of the image.
		/// </summary>
		/// <remarks>
		/// The default value is 256.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("MultiScaleTileSource.TileHeight")]
		[DefaultValue(DefaultTileHeight)]
		[Json(true)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		public virtual int TileHeight
		{
			get { return GetPropertyValue("TileHeight", DefaultTileHeight); }
			set { SetPropertyValue("TileHeight", value); }
		}

		/// <summary>
		/// Gets or sets the minmum zoom level supports by this tile source.
		/// </summary>
		/// <remarks>
		/// The default value is 1.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("MultiScaleTileSource.MinZoom")]
		[DefaultValue(DefaultMinZoom)]
		[Json(true)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public virtual int MinZoom
		{
			get { return GetPropertyValue("MinZoom", DefaultMinZoom); }
			set { SetPropertyValue("MinZoom", value); }
		}

		/// <summary>
		/// Gets or sets the maximum zoom level supports by this tile source. 
		/// </summary>
		/// <remarks>
		/// The default value is 19.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("MultiScaleTileSource.MaxZoom")]
		[DefaultValue(DefaultMaxZoom)]
		[Json(true)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public virtual int MaxZoom
		{
			get { return GetPropertyValue("MaxZoom", DefaultMaxZoom); }
			set { SetPropertyValue("MaxZoom", value); }
		}

		/// <summary>
		/// Gets or sets the callback function to get the url of the specific tile.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("MultiScaleTileSource.GetUrl")]
		[DefaultValue(DefaultGetUrl)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[WidgetFunction]
		[Layout(LayoutType.Behavior)]
		public virtual string GetUrl 
		{
			get { return GetPropertyValue("GetUrl", DefaultGetUrl); }
			set { SetPropertyValue("GetUrl", value); }
		}
	}
}
