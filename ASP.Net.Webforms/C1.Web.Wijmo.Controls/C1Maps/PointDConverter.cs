﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	internal class PointDConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return ((sourceType == typeof(string)) || base.CanConvertFrom(context, sourceType));
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return ((destinationType == typeof(InstanceDescriptor)) || base.CanConvertTo(context, destinationType));
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (!(value is string))
			{
				return base.ConvertFrom(context, culture, value);
			}
			string text = ((string)value).Trim();
			if (text.Length == 0)
			{
				return null;
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentCulture;
			}

			char ch = culture.TextInfo.ListSeparator[0];
			var textArray = text.Split(new [] { ch });
			var numArray = new double[textArray.Length];
			if (numArray.Length != 2)
			{
				throw new ArgumentException(string.Format(C1Localizer.GetString("PointDConverter.TextParseFailedFormat"), text, "x, y"));
			}
			var converter = TypeDescriptor.GetConverter(typeof(double));
			for (var i = 0; i < numArray.Length; i++)
			{
				numArray[i] = (double)converter.ConvertFromString(context, culture, textArray[i]);
			}
			return new PointD(numArray[0], numArray[1]);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}

			if ((destinationType == typeof(string)) && (value is PointD))
			{
				var pointD = (PointD)value;
				if (culture == null)
				{
					culture = CultureInfo.CurrentCulture;
				}
				string separator = culture.TextInfo.ListSeparator[0] + " ";
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(double));
				var textArray = new string[2];
				var num = 0;
				textArray[num++] = converter.ConvertToString(context, culture, pointD.X);
				textArray[num] = converter.ConvertToString(context, culture, pointD.Y);
				return string.Join(separator, textArray);
			}

			if ((destinationType == typeof(InstanceDescriptor)) && (value is PointD))
			{
				var pointD2 = (PointD)value;
				var member = typeof(PointD).GetConstructor(new [] { typeof(double), typeof(double) });
				if (member != null)
				{
					return new InstanceDescriptor(member, new object[] { pointD2.X, pointD2.Y });
				}
			}

			return base.ConvertTo(context, culture, value, destinationType);

		}

		//public override object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
		//{
		//	return new PointD((double)propertyValues["X"], (double)propertyValues["Y"]);
		//}

		//public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
		//{
		//	return true;
		//}

		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(typeof(PointD), attributes).Sort(new [] { "X", "Y" });
		}

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
