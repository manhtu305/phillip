﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Represents the layer which can be appended to the <see cref="C1Maps"/>.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public abstract class C1Layer : Settings
	{
		/// <summary>
		/// Initializes a new instance of <see cref="C1Layer"/> object.
		/// </summary>
		/// <param name="type">The type of the layer.</param>
		protected C1Layer(LayerType type)
		{
			Type = type;
		}

		/// <summary>
		/// Gets the type of the <see cref="C1Layer"/> instance.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Layer.LayerType")]
		[WidgetOption]
		[Json(true)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public LayerType Type
		{
			get { return GetPropertyValue("Type", LayerType.Vector); }
			internal set { SetPropertyValue("Type", value); }
		}
	}

	/// <summary>
	/// Represents a layer for drawing vectors.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1VectorLayer : C1Layer, ICustomOptionType
	{
		private const double DEF_MINSIZE = 3.0d;

		private Placemark _placemark;
		private WijJsonData _wijdata;
		private GeoJsonData _geodata;
		private Hashtable _innerStates;

		/// <summary>
		/// Initializes a new instance of <see cref="C1VectorLayer"/> object.
		/// </summary>
		public C1VectorLayer() : base(LayerType.Vector)
		{
		}

		/// <summary>
		/// Gets or sets a value specifies the type of the json object requested from <see cref="DataObject"/> property.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorLayer.DataType")]
		[DefaultValue(DataType.WijJson)]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public DataType DataType
		{
			get { return GetPropertyValue("DataType", DataType.WijJson); }
			set { SetPropertyValue("DataType", value); }
		}

		/// <summary>
		/// Gets or sets the client function which returns vector data.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorLayer.DataFunction")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string DataFunction
		{
			get { return GetPropertyValue("DataFunction", ""); }
			set { SetPropertyValue("DataFunction", value); }
		}

		/// <summary>
		/// Gets or sets the url string which is used to get the vector data.
		/// This property works when <see cref="DataFunction"/> property is not set.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorLayer.DataUrl")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string DataUrl
		{
			get { return GetPropertyValue("DataUrl", ""); }
			set { SetPropertyValue("DataUrl", value); }
		}

		/// <summary>
		/// Gets or sets the <see cref="DataType.Wijjson"/> type of vector data.
		/// This property only works when <see cref="DataType"/> is set to <see cref="DataType.Wijjson"/>, meanwhile <see cref="DataFunction"/> and <see cref="DataUrl"/> are not set.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorLayer.DataWijJson")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public WijJsonData DataWijJson
		{
			get
			{
				return _wijdata ?? (_wijdata = new WijJsonData());
			}
			set
			{
				_wijdata = value;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="DataType.Geojson"/> type of vector data.
		/// This property only works when <see cref="DataType"/> is set to <see cref="DataType.Geojson"/>, meanwhile <see cref="DataFunction"/> and <see cref="DataUrl"/> are not set.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorLayer.DataGeoJson")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public GeoJsonData DataGeoJson
		{
			get
			{
				return _geodata ?? (_geodata = new GeoJsonData());
			}
			set
			{
				_geodata = value;
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public Hashtable InnerStates
		{
			get
			{
				return _innerStates ?? (_innerStates = new Hashtable());
			}
		}

		/// <summary>
		/// Gets or sets the minimal size at which element becomes visible.
		/// </summary>
		/// <remarks>
		/// The default value is 3.0.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorLayer.MinSize")]
		[DefaultValue(DEF_MINSIZE)]
		[Layout(LayoutType.Appearance)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double MinSize
		{
			get { return GetPropertyValue("MinSize", DEF_MINSIZE); }
			set{SetPropertyValue("MinSize", value);}
		}

		/// <summary>
		/// Specifies how to render the placemark.
		/// </summary>
		[C1Category("Category.Misc")]
		[C1Description("C1VectorLayer.Placemark")]
		[Layout(LayoutType.Misc)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Placemark Placemark
		{
			get { return _placemark ?? (_placemark = new Placemark()); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called after the shape of the vector created.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientShapeCreated")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientShapeCreated
		{
			get { return GetPropertyValue("OnClientShapeCreated", ""); }
			set { SetPropertyValue("OnClientShapeCreated", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called when the mouse enter the shape.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientMouseEnter")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientMouseEnter
		{
			get { return GetPropertyValue("OnClientMouseEnter", ""); }
			set { SetPropertyValue("OnClientMouseEnter", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called when the mouse leave the shape.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientMouseLeave")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientMouseLeave
		{
			get { return GetPropertyValue("OnClientMouseLeave", ""); }
			set { SetPropertyValue("OnClientMouseLeave", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called when click the mouse down on the shape.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientMouseDown")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientMouseDown
		{
			get { return GetPropertyValue("OnClientMouseDown", ""); }
			set { SetPropertyValue("OnClientMouseDown", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called when click the mouse up on the shape.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientMouseUp")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientMouseUp
		{
			get { return GetPropertyValue("OnClientMouseUp", ""); }
			set { SetPropertyValue("OnClientMouseUp", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called when move the mouse on the shape.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientMove")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientMove
		{
			get { return GetPropertyValue("OnClientMove", ""); }
			set { SetPropertyValue("OnClientMove", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called when click the mouse on the shape.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1VectorLayer.OnClientClick")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientClick
		{
			get { return GetPropertyValue("OnClientClick", ""); }
			set { SetPropertyValue("OnClientClick", value); }
		}

		private Object DataObject
		{
			get
			{
				switch (DataType)
				{
					case DataType.WijJson:
						return DataWijJson;
					case DataType.GeoJson:
						return DataGeoJson;
					default:
						return null;
				}
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePlacemark()
		{
			return _placemark != null
				&& !_placemark.IsDefault;
		}

		#region serialize
		string ICustomOptionType.SerializeValue()
		{
			var serializedData = new Dictionary<string, string>();

			serializedData.Add("type", JsonHelper.WidgetToString(Type, null));
			serializedData.Add("dataType", JsonHelper.WidgetToString(DataType, null));

			if (MinSize != DEF_MINSIZE)
			{
				serializedData.Add("minSize", MinSize.ToString());
			}

			if (ShouldSerializePlacemark())
			{
				serializedData.Add("placemark", JsonHelper.WidgetToString(Placemark, null));
			}

			if (!string.IsNullOrEmpty(DataFunction))
			{
				SerializeClientEvent("data", DataFunction, serializedData);
			}
			else if (!string.IsNullOrEmpty(DataUrl))
			{
				serializedData.Add("data", "\"" + DataUrl + "\"");
			}
			else
			{
				serializedData.Add("data",  JsonHelper.WidgetToString(DataObject, null));
			}

			SerializeClientEvent("shapeCreated", OnClientShapeCreated, serializedData);
			SerializeClientEvent("mouseEnter", OnClientMouseEnter, serializedData);
			SerializeClientEvent("mouseLeave", OnClientMouseLeave, serializedData);
			SerializeClientEvent("mouseDown", OnClientMouseDown, serializedData);
			SerializeClientEvent("mouseUp", OnClientMouseUp, serializedData);
			SerializeClientEvent("move", OnClientMove, serializedData);
			SerializeClientEvent("click", OnClientClick, serializedData);

			if (InnerStates.Count > 0)
			{
				serializedData.Add("innerStates", JsonHelper.WidgetToString(InnerStates, null));
			}

			return "{" +
				string.Join(", ", serializedData.Select((keyValuePair, index) => { return "\"" + keyValuePair.Key + "\": " + keyValuePair.Value; }).ToArray()) + "}";
		}

		void ICustomOptionType.DeSerializeValue(object state)
		{
			var states = state as Hashtable;
			if (states == null) return;

			try
			{
				DataType = (DataType)Enum.Parse(typeof(DataType), states["dataType"].ToString(), true);
			}
			catch
			{
				return;
			}

			var innerStates = states["innerStates"] as Hashtable;
			if (innerStates!=null && innerStates["data"] != null && innerStates["data"] is string)
			{
				DataFunction = (string)innerStates["data"];
			}
			else if (states["data"] != null)
			{
				if (states["data"] is string)
				{
					DataUrl = (string)states["data"];
				}
				else if (states["data"] is Hashtable)
				{
					var dataTable = (Hashtable)states["data"];
					var dataType = dataTable["type"];
					var dataValue = dataTable["value"];

					if (dataType != null && dataType is string && (string)dataType == "fun" && dataValue != null && dataValue is string)
					{
						DataFunction = (string)dataValue;
					}
					else
					{
						((IJsonRestore)DataObject).RestoreStateFromJson(dataTable);
					}
				}
			}

			if (states["minSize"] != null)
			{
				MinSize = Convert.ToDouble(states["minSize"]);
			}

			if (states["placemark"] != null)
			{
				((IJsonRestore)Placemark).RestoreStateFromJson(states["placemark"]);
			}

			OnClientShapeCreated = DeSerializeClientEvent("shapeCreated", states, innerStates);
			OnClientMouseEnter = DeSerializeClientEvent("mouseEnter", states, innerStates);
			OnClientMouseLeave = DeSerializeClientEvent("mouseLeave", states, innerStates);
			OnClientMouseDown = DeSerializeClientEvent("mouseDown", states, innerStates);
			OnClientMouseUp = DeSerializeClientEvent("mouseUp", states, innerStates);
			OnClientMove = DeSerializeClientEvent("move", states, innerStates);
			OnClientClick = DeSerializeClientEvent("click", states, innerStates);
		}

		private void SerializeClientEvent(string clientEventName, string clientEventValue, Dictionary<string, string> serializedData)
		{
			if (!string.IsNullOrEmpty(clientEventValue))
			{
				serializedData.Add(clientEventName, clientEventValue);
				InnerStates.Add(clientEventName, clientEventValue);
			}
		}

		private string DeSerializeClientEvent(string clientEventName, Hashtable states, Hashtable innerStates)
		{
			if (innerStates!=null && innerStates[clientEventName] != null && innerStates[clientEventName] is string)
			{
				return (string)innerStates[clientEventName];
			}
			else if (states[clientEventName] != null && states[clientEventName] is string)
			{
				return (string)states[clientEventName];
			}

			return "";
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}
		#endregion
	}

	/// <summary>
	/// Represents the settings for customize the rendering of placemmark.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class Placemark : Settings
	{
		/// <summary>
		/// Gets or sets the image to be drawn for the mark.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("Placemark.Image")]
		[WidgetOption]
		[DefaultValue("")]
		[Layout(LayoutType.Appearance)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string Image
		{
			get { return GetPropertyValue("Image", ""); }
			set { SetPropertyValue("Image", value); }
		}

		/// <summary>
		/// Gets or sets the callback functiton for drawing the mark.
		/// </summary>
		/// <remarks>
		/// The <see cref="Image"/> is ignored if it is set.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("Placemark.Render")]
		[WidgetEvent]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string Render
		{
			get { return GetPropertyValue("Render", ""); }
			set { SetPropertyValue("Render", value); }
		}

		/// <summary>
		/// Gets or sets the size of the mark.
		/// </summary>
		/// <remarks>
		/// The default value is (0,0). The size of the image is used if not specified.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("Placemark.Size")]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Size Size
		{
			get { return GetPropertyValue("Size", Size.Empty); }
			set { SetPropertyValue("Size", value); }
		}

		/// <summary>
		/// Gets or sets the point within the mark to pin the mark relates to the specific placemark location.
		/// </summary>
		/// <remarks>
		/// The default value is (0,0).
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("Placemark.PinPoint")]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Point PinPoint
		{
			get { return GetPropertyValue("PinPoint", Point.Empty); }
			set{SetPropertyValue("PinPoint", value);}
		}

		/// <summary>
		/// Gets or sets a value specifies when the label will be visible. 
		/// </summary>
		/// <remarks>
		/// The default value is <see cref="C1.Web.Wijmo.Controls.C1Maps.LabelVisibility.Hidden"/>.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("Placemark.LabelVisibility")]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		[DefaultValue(LabelVisibility.Hidden)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public LabelVisibility LabelVisibility
		{
			get { return GetPropertyValue("LabelVisibility", LabelVisibility.Hidden); }
			set { SetPropertyValue("LabelVisibility", value); }
		}

		/// <summary>
		/// Gets or sets a value specifies the label position relates to the placemark location.
		/// </summary>
		/// <remarks>
		/// The default value is <see cref="C1.Web.Wijmo.Controls.C1Maps.LabelPosition.Center"/>.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("Placemark.LabelPosition")]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		[DefaultValue(LabelPosition.Center)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public LabelPosition LabelPosition
		{
			get { return GetPropertyValue("LabelPosition", LabelPosition.Center); }
			set { SetPropertyValue("LabelPosition", value); }
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSize()
		{
			return !Size.IsEmpty;
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePinPoint()
		{
			return !PinPoint.IsEmpty;
		}

		internal bool IsDefault 
		{
			get
			{
				return string.IsNullOrEmpty(Image)
					   && string.IsNullOrEmpty(Render)
				       && !ShouldSerializeSize()
				       && !ShouldSerializePinPoint()
				       && LabelVisibility == LabelVisibility.Hidden
				       && LabelPosition == LabelPosition.Center;
			}
		}
	}

	/// <summary>
	/// Represents a layer for drawing its items positioned geographically inside a <see cref="C1Maps"/>.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1ItemsLayer : C1Layer, ICustomOptionType
	{
		private Hashtable _innerStates;

		/// <summary>
		/// Initializes a new instance of <see cref="C1ItemsLayer"/> object.
		/// </summary>
		public C1ItemsLayer() : base(LayerType.Items)
		{
		}

		/// <summary>
		/// Gets or sets the client function to be called when request the data.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1ItemsLayer.Request")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string Request
		{
			get { return GetPropertyValue("Request", ""); }
			set { SetPropertyValue("Request", value); }
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Layout(LayoutType.None)]
		public Hashtable InnerStates
		{
			get
			{
				_innerStates = _innerStates ?? (_innerStates = new Hashtable());
				_innerStates.Clear();

				if (!string.IsNullOrEmpty(Request))
				{
					_innerStates.Add("request", Request);
				}
				return _innerStates;
			}
		}

		#region serialize
		string ICustomOptionType.SerializeValue()
		{
			var serializeStrings = new List<string>();
			serializeStrings.Add(string.Format("\"{0}\": {1}", "type", JsonHelper.WidgetToString(Type, null)));

			if (!string.IsNullOrEmpty(Request))
			{
				serializeStrings.Add(string.Format("\"{0}\": {1}", "request", Request));
				serializeStrings.Add(string.Format("\"{0}\": {1}", "innerStates", JsonHelper.WidgetToString(InnerStates, null)));
			}

			return "{" + string.Join(",", serializeStrings.ToArray()) + "}";
		}

		void ICustomOptionType.DeSerializeValue(object state)
		{
			var states = state as Hashtable;
			if (states == null) return;

			var innerStates = states["innerStates"] as Hashtable;
			if (innerStates == null) return;

			if (innerStates["request"] != null && innerStates["request"] is string)
			{
				Request = (string)innerStates["request"];
			}
			else if (states["request"] != null && states["request"] is string)
			{
				Request = (string)states["request"];
			}
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}
		#endregion
	}

	/// <summary>
	/// Represents a layer for drawing its items positioned geographically and only shows the items actually in view.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1VirtualLayer : C1Layer, ICustomOptionType
	{
		private Hashtable _innerStates;
		private readonly List<MapSlice> _slices = new List<MapSlice>();

		/// <summary>
		/// Initializes a new instance of <see cref="C1VirtualLayer"/> object.
		/// </summary>
		public C1VirtualLayer() : base(LayerType.Virtual)
		{
		}

		/// <summary>
		/// Gets or sets the client function to be called when request the data.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1VirtualLayer.Request")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string Request
		{
			get { return GetPropertyValue("Request", ""); }
			set { SetPropertyValue("Request", value); }
		}

		/// <summary>
		/// Gets a collection of <see cref="MapSlice"/> which specifies how the map is partitioned for virtualization.
		/// </summary>
		/// <remarks>
		/// Each slice defines a set of regions this virtual layer will get items form when needed.
		/// The minimum zoom of a slice is the value value of it's <see cref="MapSlice.Zoom"/> property, 
		/// while the maximum zoom is the value from the <see cref="MapSlice.Zoom"/> property of the next slice.
		/// Each slice divides the map in a uniform grid of regions according to the current projection.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1VirtualLayer.Slices")]
		[CollectionItemType(typeof(MapSlice))]
		[Layout(LayoutType.Behavior)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public List<MapSlice> Slices
		{
			get
			{
				return _slices;
			}
		}

		/// <summary>
		/// Infrastructure.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Layout(LayoutType.None)]
		public Hashtable InnerStates
		{
			get
			{
				_innerStates = _innerStates ?? (_innerStates = new Hashtable());
				_innerStates.Clear();

				if (!string.IsNullOrEmpty(Request))
				{
					_innerStates.Add("request", Request);
				}
				return _innerStates;
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSlices()
		{
			return _slices.Count > 0;
		}

		#region serialize
		string ICustomOptionType.SerializeValue()
		{
			var serializeStrings = new List<string>();
			serializeStrings.Add(string.Format("\"{0}\": {1}", "type", JsonHelper.WidgetToString(Type, null)));

			if (!string.IsNullOrEmpty(Request))
			{
				serializeStrings.Add(string.Format("\"{0}\": {1}", "request", Request));
				serializeStrings.Add(string.Format("\"{0}\": {1}", "innerStates", JsonHelper.WidgetToString(InnerStates, null)));
			}

			if (ShouldSerializeSlices())
			{
				serializeStrings.Add(string.Format("\"{0}\": {1}", "slices", JsonHelper.WidgetToString(Slices, null)));
			}

			return "{" + string.Join(",", serializeStrings.ToArray()) + "}";
		}

		void ICustomOptionType.DeSerializeValue(object state)
		{
			var states = state as Hashtable;
			if (states == null) return;

			var innerStates = states["innerStates"] as Hashtable;
			if (innerStates != null && innerStates["request"] != null && innerStates["request"] is string)
			{
				Request = (string)innerStates["request"];
			}
			else if (states["request"] != null && states["request"] is string)
			{
				Request = (string)states["request"];
			}

			if (states["slices"] != null && states["slices"] is Hashtable)
			{
				((IJsonRestore)Slices).RestoreStateFromJson(states["slices"]);
			}
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}
		#endregion
	}

	/// <summary>
	/// Represents a custom layer. The rendering of the layer is customized by user.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1CustomLayer : C1Layer
	{
		/// <summary>
		/// Initialize an instance of <see cref="C1CustomLayer"/> object.
		/// </summary>
		public C1CustomLayer() : base(LayerType.Custom)
		{
		}

		/// <summary>
		/// Gets or sets the name of the client widget which is used to render the custom C1Maps layer.
		/// </summary>
		/// <remarks>
		/// The client widget need implements the client IWijlayer interface.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1CustomLayer.WidgetName")]
		[Json(true)]
		[WidgetOption]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string WidgetName
		{
			get
			{
				return GetPropertyValue("WidgetName", "");
			}
			set
			{
				SetPropertyValue("WidgetName", value);
			}
		}
	}
}
