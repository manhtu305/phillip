﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	internal class LODConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return ((sourceType == typeof(string)) || base.CanConvertFrom(context, sourceType));
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return ((destinationType == typeof(InstanceDescriptor)) || base.CanConvertTo(context, destinationType));
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (!(value is string))
			{
				return base.ConvertFrom(context, culture, value);
			}
			string text = ((string)value).Trim();
			if (text.Length == 0)
			{
				return null;
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentCulture;
			}

			char ch = culture.TextInfo.ListSeparator[0];
			var textArray = text.Split(new[] { ch });
			var numArray = new double[textArray.Length];
			if (numArray.Length != 4)
			{
				throw new ArgumentException(string.Format(C1Localizer.GetString("LODConverter.TextParseFailedFormat"), text, "minSize, maxSize, minZoom, maxZoom"));
			}
			var converter = TypeDescriptor.GetConverter(typeof(double));
			for (var i = 0; i < numArray.Length; i++)
			{
				numArray[i] = (double)converter.ConvertFromString(context, culture, textArray[i]);
			}
			return new LOD(numArray[0], numArray[1], numArray[2], numArray[3]);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}

			if ((destinationType == typeof(string)) && (value is LOD))
			{
				var lod = (LOD)value;
				if (culture == null)
				{
					culture = CultureInfo.CurrentCulture;
				}
				string separator = culture.TextInfo.ListSeparator[0] + " ";
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(double));
				var textArray = new string[4];
				textArray[0] = converter.ConvertToString(context, culture, lod.MinSize);
				textArray[1] = converter.ConvertToString(context, culture, lod.MaxSize);
				textArray[2] = converter.ConvertToString(context, culture, lod.MinZoom);
				textArray[3] = converter.ConvertToString(context, culture, lod.MaxZoom);
				return string.Join(separator, textArray);
			}

			if ((destinationType == typeof(InstanceDescriptor)) && (value is LOD))
			{
				var lod2 = (LOD)value;
				var member = typeof(LOD).GetConstructor(new[] { typeof(double), typeof(double), typeof(double), typeof(double) });
				if (member != null)
				{
					return new InstanceDescriptor(member, new object[] { lod2.MinSize, lod2.MaxSize, lod2.MinZoom, lod2.MaxZoom });
				}
			}

			return base.ConvertTo(context, culture, value, destinationType);

		}

		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(typeof(LOD), attributes).Sort(new[] { "MinSize", "MaxSize", "MinZoom", "MaxZoom" });
		}

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
