﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Represents a map slice as used in <see cref="C1VirtualLayer"/>.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MapSlice : Settings
	{
		/// <summary>
		/// Gets or sets the number of latitude divisions for this slice.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("MapSlice.LatitudeSlices")]
		[Json(true)]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double LatitudeSlices
		{
			get { return GetPropertyValue("LatitudeSlices", 0d); }
			set { SetPropertyValue("LatitudeSlices", value); }
		}

		/// <summary>
		/// Gets or sets the number of longitude divisions for this slice.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("MapSlice.LongitudeSlices")]
		[Json(true)]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double LongitudeSlices
		{
			get { return GetPropertyValue("LongitudeSlices", 0d); }
			set { SetPropertyValue("LongitudeSlices", value); }
		}

		/// <summary>
		/// Gets or sets the minimum zoom of this slice.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("MapSlice.Zoom")]
		[Json(true)]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double Zoom
		{
			get { return GetPropertyValue("Zoom", 1.0d); }
			set { SetPropertyValue("Zoom", value); }
		}
	}
}
