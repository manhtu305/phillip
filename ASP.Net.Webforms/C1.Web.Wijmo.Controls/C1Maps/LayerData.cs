﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1Maps.GeoJson;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Represents the data object of the data source for <see cref="C1VectorLayer"/>, 
	/// when the <see cref="C1VectorLayer.DataType"/> is set to <see cref="DataType.GeoJson"/>
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class WijJsonData : Settings
	{
		private readonly List<C1VectorItemBase> _vectors = new List<C1VectorItemBase>();

		/// <summary>
		/// Gets a collection of <see cref="C1VectorItemBase"/> that specifies the vectors to be drawn.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("WijJsonData.Vectors")]
		[CollectionItemType(typeof(C1VectorItemBase))]
		[Json(true)]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.C1VectorItemsEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.C1VectorItemsEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.C1VectorItemsEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		public List<C1VectorItemBase> Vectors
		{
			get 
			{
				return _vectors; 
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeVectors()
		{
			return _vectors.Count > 0;
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeVectors(object value)
		{
			_vectors.Clear();

			var list = value as IList;
			if (list != null)
			{
				foreach (object item in list)
				{
					var hsItem = item as Hashtable;
					if (hsItem != null)
					{
						VectorType type;
						try
						{
							type = (VectorType)Enum.Parse(typeof(VectorType), hsItem["type"].ToString(), true);
						}
						catch (Exception)
						{
							continue;
						}

						C1VectorItemBase vector = null;
						switch (type)
						{
							case VectorType.Placemark:
								vector = new C1VectorPlacemark();
								break;
							case VectorType.Polyline:
								vector = new C1VectorPolyline();
								break;
							case VectorType.Polygon:
								vector = new C1VectorPolygon();
								break;
						}

						if (vector != null)
						{
							((IJsonRestore)vector).RestoreStateFromJson(item);
							_vectors.Add(vector);
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// Represents the data object of the data source for <see cref="C1VectorLayer"/>, 
	/// when the <see cref="C1VectorLayer.DataType"/> is set to <see cref="DataType.GeoJson"/>
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class GeoJsonData : FeatureCollection
	{
		
	}
}
