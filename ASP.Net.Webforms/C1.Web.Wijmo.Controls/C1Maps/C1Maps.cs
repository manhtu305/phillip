﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Localization;

[assembly: WebResource("C1.Web.Wijmo.Controls.C1Maps.Images.WorldMap.png", "image/x-png")]
[assembly: WebResource("C1.Web.Wijmo.Controls.C1Maps.Images.ZoomPanel.png", "image/x-png")]
[assembly: WebResource("C1.Web.Wijmo.Controls.C1Maps.Images.ScalePanel.png", "image/x-png")]
namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Represents the C1Maps control.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Maps.C1MapsDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Maps.C1MapsDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Maps.C1MapsDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Maps.C1MapsDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ToolboxData("<{0}:C1Maps runat=server></{0}:C1Maps>")]
	[ToolboxBitmap(typeof(C1Maps), "C1Maps.png")]
	[LicenseProvider]
	[WidgetDependencies(typeof(WijMaps), "extensions.c1maps.js", ResourcesConst.C1WRAPPER_PRO)]
	public class C1Maps : C1TargetControlBase, IPostBackDataHandler, IC1Serializable
	{
		#region ** fields

		private const string CONST_DEF_WIDTH = "512px";
		private const string CONST_DEF_HEIGHT = "384px";
	
		private bool _productLicensed;
		private bool _shouldNag;

		private readonly  MultiScaleTileSource _defSource = new MultiScaleTileSource();
		private readonly List<C1Layer> _layers = new List<C1Layer>();
		#endregion ** end of fields.

		#region properties

		/// <summary>
		/// Gets or sets the center of this map in geographic coordinates.
		/// </summary>
		/// <remarks>
		/// The default value is (0,0).
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Maps.Center")]
		[WidgetOption]
		[Layout(LayoutType.Behavior)]
		public PointD Center
		{
			get { return GetPropertyValue("Center", PointD.Empty); }
			set { SetPropertyValue("Center", value); }
		}

		/// <summary>
		/// Gets or sets the zoom factor of this map.
		/// </summary>
		/// <remarks>
		/// The default value is 1. The minimum zoom factor is 1.
		///  An increment of 1 in the zoom factor doubles the map resolution.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Maps.Zoom")]
		[WidgetOption]
		[DefaultValue(1)]
		[Layout(LayoutType.Behavior)]
		public double Zoom
		{
			get { return GetPropertyValue("Zoom", 1d); }
			set { SetPropertyValue("Zoom", value); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the default tools are shown.
		/// </summary>
		/// <remarks>
		/// The default value is <c>true</c>.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1Maps.ShowTools")]
		[WidgetOption]
		[DefaultValue(true)]
		[Layout(LayoutType.Appearance)]
		public bool ShowTools
		{
			get { return GetPropertyValue("ShowTools", true); }
			set { SetPropertyValue("ShowTools", value); }
		}

		/// <summary>
		/// Gets or sets a value that determines the speed with which <see cref="Zoom" /> approaches target zoom.
		/// </summary>
		/// <remarks>
		/// The default value is 0.3. The value should be between 0 and 1. 
		/// A value of 1 indicates that Zoom instantly changes to target zoom.
		/// When clicking the zoom in/zoom out buttons or moving mouse wheel or double-clicking the map tiles or pressing "+/-" key,
		/// the maps should zoom in/zoom out 1 times which is called target zoom here.
		/// The property is used to specify the zooming speed from current size to the target zoom.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Maps.ZoomingSpeed")]
		[WidgetOption]
		[WidgetOptionName("targetZoomSpeed")]
		[DefaultValue(0.3)]
		[Layout(LayoutType.Behavior)]
		public double ZoomingSpeed
		{
			get { return GetPropertyValue("ZoomingSpeed", 0.3d); }
			set { SetPropertyValue("ZoomingSpeed", value); }
		}

		/// <summary>
		/// Gets or sets a value that determines the speed with which <see cref="Center" /> approaches target center.
		/// </summary>
		/// <remarks>
		/// The default value is 0.3. The value should be between 0 and 1. 
		/// A value of 1 indicates that Center instantly changes to target center.
		/// When clicking the pan direction buttions or pressing direction keys, the maps should offset a certain distance
		/// which is called target center here.  
		/// The property is used to specify the panning speed from current center to the target center.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Maps.PanningSpeed")]
		[WidgetOption]
		[WidgetOptionName("targetCenterSpeed")]
		[DefaultValue(0.3)]
		[Layout(LayoutType.Behavior)]
		public double PanningSpeed
		{
			get { return GetPropertyValue("PanningSpeed", 0.3d); }
			set { SetPropertyValue("PanningSpeed", value); }
		}

		/// <summary>
		/// Gets or sets the <see cref="MultiScaleTileSource" /> used as source for the custom map tiles.
		/// </summary>
		/// <remarks>
		/// This property works only when <see cref="Source"/> is set to <see cref="MapSource.CustomSource"/>.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Maps.CustomSource")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[MergableProperty(false)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Behavior)]
		public MultiScaleTileSource CustomSource
		{
			get { return GetPropertyValue("CustomSource", _defSource); }
			set { SetPropertyValue("CustomSource", value); }
		}

		/// <summary>
		/// Gets or sets the <see cref="MapSource" /> used as source for the base map tiles.
		/// </summary>
		/// <remarks>
		/// The default value is <see cref="MapSource.BingMapsAerialSource"/>. 
		/// If the source is set to <see cref="MapSource.None"/>, no map tiles are shown.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Maps.Source")]
		[DefaultValue(MapSource.BingMapsAerialSource)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Behavior)]
		public MapSource Source
		{
			get { return GetPropertyValue("Source", MapSource.BingMapsAerialSource); }
			set { SetPropertyValue("Source", value); }
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		[WidgetOptionName("source")]
		public object InnerSource 
		{
			get
			{
				switch (Source)
				{
					case MapSource.BingMapsAerialSource:
						return "bingMapsAerialSource";
					case MapSource.BingMapsHybridSource:
						return "bingMapsHybridSource";
					case MapSource.BingMapsRoadSource:
						return "bingMapsRoadSource";
					case MapSource.CustomSource:
						return CustomSource;
					case MapSource.None:
					default:
						return null;
				}
			}
			set
			{
				if (value is MultiScaleTileSource)
				{
					CustomSource = (MultiScaleTileSource)value;
				}
				else if(value is string)
				{
					switch ((string)value)
					{
						case "bingMapsAerialSource":
							Source = MapSource.BingMapsAerialSource;
							break;
						case "bingMapsHybridSource":
							Source = MapSource.BingMapsHybridSource;
							break;
						case "bingMapsRoadSource":
							Source = MapSource.BingMapsRoadSource;
							break;
						default:
							Source = MapSource.None;
							break;
					}
				}
			}
		}

		/// <summary>
		/// Gets the collection of <see cref="C1Layer"/> objects.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Maps.Layers")]
		[CollectionItemType(typeof(C1Layer))]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		#if ASP_NET45
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.C1LayersEditor, C1.Web.Wijmo.Controls.Design.45", typeof(UITypeEditor))]
#elif ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.C1LayersEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.C1Maps.C1LayersEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		public List<C1Layer> Layers
		{
			get 
			{
				return _layers; 
			}
		}

		/// <summary>
		/// Gets or sets the Bing maps key.
		/// </summary>
		[C1Category("Category.Misc")]
		[C1Description("C1Maps.BingMapsKey")]
		[WidgetOption]
		[DefaultValue("")]
		[Layout(LayoutType.Misc)]
		public string BingMapsKey 
		{
			get 
			{
				return GetPropertyValue<string>("BingMapsKey", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("BingMapsKey", value);
			}
		}

		/// <summary>
		/// Gets or sets the callback function which is called after the zoom of the maps changed.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Maps.OnClientZoomChanged")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientZoomChanged
		{
			get { return GetPropertyValue("OnClientZoomChanged", ""); }
			set { SetPropertyValue("OnClientZoomChanged", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called after the target zoom of the maps changed.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Maps.OnClientTargetZoomChanged")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientTargetZoomChanged
		{
			get { return GetPropertyValue("OnClientTargetZoomChanged", ""); }
			set { SetPropertyValue("OnClientTargetZoomChanged", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called after the center of the maps changed.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Maps.OnClientCenterChanged")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientCenterChanged
		{
			get { return GetPropertyValue("OnClientCenterChanged", ""); }
			set { SetPropertyValue("OnClientCenterChanged", value); }
		}

		/// <summary>
		/// Gets or sets the callback function which is called after the target center of the maps changed.
		/// </summary>
		[WidgetEvent("event, data")]
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Maps.OnClientTargetCenterChanged")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string OnClientTargetCenterChanged
		{
			get { return GetPropertyValue("OnClientTargetCenterChanged", ""); }
			set { SetPropertyValue("OnClientTargetCenterChanged", value); }
		}

		/// <summary>
		/// A value that indicates the width of the control.
		/// </summary>
		[C1Description("C1Maps.Width")]
		[C1Category("Category.Layout")]
		[DefaultValue(typeof(Unit), CONST_DEF_WIDTH)]
		[Layout(LayoutType.Sizes)]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// A value that indicates the height of the control.
		/// </summary>
		[C1Description("C1Maps.Height")]
		[C1Category("Category.Layout")]
		[DefaultValue(typeof(Unit), CONST_DEF_HEIGHT)]
		[Layout(LayoutType.Sizes)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value that corresponds to this Web server control. This property is used primarily by control developers.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// One of the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> enumeration values.
		/// </returns>
		protected override HtmlTextWriterTag TagKey
		{
			get { return HtmlTextWriterTag.Div; }
		}

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}
	
		#endregion

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1Maps"/> class.
		/// </summary>
		public C1Maps()
		{
			VerifyLicense();
			Width = new Unit(CONST_DEF_WIDTH);
			Height = new Unit(CONST_DEF_HEIGHT);
		}

		private void VerifyLicense()
		{
			var licInfo = Util.Licensing.ProviderInfo.Validate(typeof(C1Maps), this, false);
			_shouldNag = licInfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licInfo.IsValid;
#else
			_productLicensed = licInfo.IsValid || licInfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified System.Web.UI.HtmlTextWriterTag. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (IsDesignMode)
			{
				var backgroundImage = Page.ClientScript.GetWebResourceUrl(typeof(C1Maps), "C1.Web.Wijmo.Controls.C1Maps.Images.WorldMap.png");
				Attributes.Add("style", string.Format("position: relative; background:50% 50% no-repeat; background-image: url({0})", backgroundImage));
			}
			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// Outputs the content of a server control's children to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered on the client.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the rendered content.</param>
		protected override void RenderChildren(HtmlTextWriter writer)
		{
			if (IsDesignMode)
			{
				writer.Write("<img src='{0}' style='position: absolute; left:10px; top: 10px;' />", Page.ClientScript.GetWebResourceUrl(typeof(C1Maps), "C1.Web.Wijmo.Controls.C1Maps.Images.ZoomPanel.png"));
				writer.Write("<img src='{0}' style='position: absolute; right:10px; bottom:10px;' />", Page.ClientScript.GetWebResourceUrl(typeof(C1Maps), "C1.Web.Wijmo.Controls.C1Maps.Images.ScalePanel.png"));
			}
			base.RenderChildren(writer);
		}

		#region IPostbackDataHandler members

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			var data = JsonSerializableHelper.GetJsonData(postCollection);

			RestoreStateFromJson(data);

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}

		#endregion

		#region serialize

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeLayers()
		{
			return _layers.Count > 0;
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeInnerSource(object value)
		{
			if (value is string)
			{
				InnerSource = value;
			}
			else
			{
				((IJsonRestore)InnerSource).RestoreStateFromJson(value);
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeLayers(object value)
		{
			_layers.Clear();

			var list = value as IList;
			if (list != null)
			{
				foreach (object item in list)
				{
					var hsItem = item as Hashtable;
					if (hsItem != null)
					{
						LayerType type;
						try
						{
							type = (LayerType) Enum.Parse(typeof (LayerType), hsItem["type"].ToString(), true);
						}
						catch (Exception)
						{
							continue;
						}

						C1Layer layer = null;
						switch (type)
						{
							case LayerType.Vector:
								layer = new C1VectorLayer();
								break;
							case LayerType.Items:
								layer = new C1ItemsLayer();
								break;
							case LayerType.Virtual:
								layer = new C1VirtualLayer();
								break;
							case LayerType.Custom:
								layer = new C1CustomLayer();
								break;
						}

						if (layer != null)
						{
							if (layer is ICustomOptionType)
							{
								((ICustomOptionType)layer).DeSerializeValue(item);
							}
							else if (layer is IJsonRestore)
							{
								((IJsonRestore)layer).RestoreStateFromJson(item);
							}
							_layers.Add(layer);
						}
					}
				}
				
			}
		}
		#endregion serialize

		#region ** IC1Serializable
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			var sz = new C1MapsSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			var sz = new C1MapsSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			var sz = new C1MapsSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			var sz = new C1MapsSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}

		#endregion
	}
}
