﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// The base calss of vector item, which is used to construct wijjson data object.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public abstract class C1VectorItemBase : Settings
	{
		private static readonly Color DefaultFill = Color.Transparent;
		private static readonly Color DefaultStroke = Color.Black;

		private List<PointD> _points = new List<PointD>();

		/// <summary>
		/// Initializes a new instance of <see cref="C1VectorItemBase"/> object.
		/// </summary>
		/// <param name="type">The type of the vector item.</param>
		protected C1VectorItemBase(VectorType type)
		{
			Type = type;
		}

		/// <summary>
		/// Gets the type of the vector item.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorItemBase.Type")]
		[WidgetOption]
		[Json(true)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public VectorType Type
		{
			get { return GetPropertyValue("Type", VectorType.Placemark); }
			internal set { SetPropertyValue("Type", value); }
		}

		/// <summary>
		/// Gets or sets the name of the vector item.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorItemBase.Name")]
		[DefaultValue("")]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string Name
		{
			get { return GetPropertyValue("Name", ""); }
			set { SetPropertyValue("Name", value); }
		}

		/// <summary>
		/// Gets or sets the fill color of the vector item.
		/// </summary>
		/// <remarks>
		/// The default value is Transparent.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.Fill")]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Color Fill
		{
			get { return GetPropertyValue("Fill", DefaultFill); }
			set { SetPropertyValue("Fill", value); }
		}

		/// <summary>
		/// Gets or sets the fill opacity of the vector item, from 0 to 1.
		/// </summary>
		/// <remarks>
		/// The default value is 1.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.FillOpacity")]
		[DefaultValue(1d)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double FillOpacity
		{
			get { return GetPropertyValue("FillOpacity", 1d); }
			set { SetPropertyValue("FillOpacity", value); }
		}

		/// <summary>
		///  Gets or sets the level of details of the vector item.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.Lod")]
		[WidgetOption]
		public LOD Lod
		{
			get { return GetPropertyValue("Lod", new LOD()); }
			set { SetPropertyValue("Lod", value); }
		}

		/// <summary>
		/// Gets or sets the stroke color of the vector item.
		/// </summary>
		/// <remarks>
		/// The default value is Balck.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.Stroke")]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public Color Stroke
		{
			get { return GetPropertyValue("Stroke", DefaultStroke); }
			set { SetPropertyValue("Stroke", value); }
		}

		/// <summary>
		/// Gets or sets the stroke opacity of the vector item, from 0 to 1.
		/// </summary>
		/// <remarks>
		/// The default value is 1.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.StrokeOpacity")]
		[DefaultValue(1d)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double StrokeOpacity
		{
			get { return GetPropertyValue("StrokeOpacity", 1d); }
			set { SetPropertyValue("StrokeOpacity", value); }
		}
		/// <summary>
		/// Gets or sets the stroke thicknees of the vector item, in pixel.
		/// </summary>
		/// <remarks>
		/// The default value is 1.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.StrokeWidth")]
		[DefaultValue(1.0d)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double StrokeWidth
		{
			get { return GetPropertyValue("StrokeWidth", 1.0d); }
			set { SetPropertyValue("StrokeWidth", value); }
		}

		/// <summary>
		/// Gets or sets the stroke dash style of the vector item.
		/// </summary>
		/// <example>
		/// [“”, “-”, “.”, “-.”, “-..”, “. ”, “- ”, “--”, “- .”, “--.”, “--..”]
		/// </example>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.StrokeDashArray")]
		[DefaultValue("")]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string StrokeDashArray
		{
			get { return GetPropertyValue("StrokeDashArray", ""); }
			set { SetPropertyValue("StrokeDashArray", value); }
		}

		/// <summary>
		/// Gets or sets the shape at the end of the line.
		/// </summary>
		/// <remarks>
		/// The default value is <see cref="PenLineCap.Butt"/>.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.StrokeLineCap")]
		[DefaultValue(PenLineCap.Butt)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public PenLineCap StrokeLineCap
		{
			get { return GetPropertyValue("StrokeLineCap", PenLineCap.Butt); }
			set { SetPropertyValue("StrokeLineCap", value); }
		}

		/// <summary>
		/// Gets or sets the shape that join two lines.
		/// </summary>
		/// <remarks>
		/// The default value is <see cref="PenLineJoin.Bevel"/>.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.StrokeLineJoin")]
		[DefaultValue(PenLineJoin.Bevel)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public PenLineJoin StrokeLineJoin
		{
			get { return GetPropertyValue("StrokeLineJoin", PenLineJoin.Bevel); }
			set { SetPropertyValue("StrokeLineJoin", value); }
		}

		/// <summary>
		/// Gets or sets the limit on the ratio of the miter length to half the stroke thickness of the shape.
		/// </summary>
		/// <remarks>
		/// Defualt value is 1. This value is always a positive number that is greater than or equal to 1.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.StrokeMiterLimit")]
		[DefaultValue(1.0d)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public double StrokeMiterLimit
		{
			get { return GetPropertyValue("StrokeMiterLimit", 1.0d); }
			set { SetPropertyValue("StrokeMiterLimit", value); }
		}

		/// <summary>
		/// Gets a collection of <see cref="PointD"/> which represents the coordinates of this vector item, in geographic unit (longitude and latitude).
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorItemBase.Points")]
		[CollectionItemType(typeof(PointD))]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual List<PointD> Points
		{
			get
			{
				return _points;
			}
			internal set
			{
				_points = value;
			}
		}

		/// <summary>
		/// Gets a collection of <c>double</c> which represents the coordinates of this vector item, in geographic unit (longitude and latitude).
		/// </summary>
		/// <remarks>
		/// This property is used to serialize. Use <see cref="Points"/> property to add or remove points.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1VectorItemBase.Coordinates")]
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[CollectionItemType(typeof(Double))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual IList<double> Coordinates
		{
			get
			{
				if (_points == null) return null;

				var coordinates = new List<double>(_points.Count * 2);
				foreach (var point in _points)
				{
					coordinates.Add(point.X);
					coordinates.Add(point.Y);
				}

				return coordinates;
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeCoordinates(object value)
		{
			_points.Clear();

			var list = value as ArrayList;
			if (list == null) return;

			var count = list.Count - list.Count % 2;
			for (int i = 0; i < count; i++)
			{
				var x = Convert.ToDouble(list[i]);
				var y = Convert.ToDouble(list[++i]);
				var point = new PointD(x, y);
				_points.Add(point);
			}
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeFill()
		{
			return (Fill != DefaultFill);
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeLod()
		{
			return (!Lod.IsDefault);
		}

		/// <summary>
		/// Internal used for serialization.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStroke()
		{
			return (Stroke != DefaultStroke);
		}
	}

	/// <summary>
	/// Represents a placemark vector item.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1VectorPlacemark : C1VectorItemBase
	{
		/// <summary>
		/// Initializes a new instance of <see cref="C1VectorPlacemark"/> object.
		/// </summary>
		public C1VectorPlacemark()
			: base(VectorType.Placemark)
		{
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override List<PointD> Points
		{
			get { return base.Points; }
			internal set { base.Points = value; }
		}

		/// <summary>
		/// Gets or sets the coordinates of the mark, in geographic unit (longitude and latitude).
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1VectorPlacemark.Point")]
		[Layout(LayoutType.Data)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public PointD Point
		{
			get
			{
				if (Points.Count == 0)
				{
					Points.Add(new PointD());
				}
				return Points[0];
			}
			set
			{
				if (Points.Count == 0)
				{
					Points.Add(value);
					return;
				}
				Points[0] = value;
			}
		}

		/// <summary>
		/// Gets or sets the label of the vector item.
		/// </summary>
		/// <remarks>
		/// When drawing the label of the placemark, the <see cref="Label"/> property is used, and then <see cref="C1VectorItemBase.Name"/> property.
		/// </remarks>
		[C1Category("Category.Data")]
		[C1Description("C1VectorPlacemark.Label")]
		[DefaultValue((string)null)]
		[WidgetOption]
		[Layout(LayoutType.Appearance)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public string Label
		{
			get { return GetPropertyValue("Label", (string)null); }
			set { SetPropertyValue("Label", value); }
		}
	}

	/// <summary>
	/// Represents a polyline vector item.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1VectorPolyline : C1VectorItemBase
	{
		/// <summary>
		/// Intializes a new instance of <see cref="C1VectorPolyline"/> object.
		/// </summary>
		public C1VectorPolyline()
			: base(VectorType.Polyline)
		{
		}
	}

	/// <summary>
	/// Represents a polygon vector item.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1VectorPolygon : C1VectorItemBase
	{
		/// <summary>
		/// Intializes a new instance of <see cref="C1VectorPolygon"/> object.
		/// </summary>
		public C1VectorPolygon()
			: base(VectorType.Polygon)
		{
		}
	}
}
