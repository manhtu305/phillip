﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// The LOD (Level Of Detail) structure associates element visibility with the 
	/// map scale (zoom and size).
	/// </summary>
	[Serializable]
	[TypeConverter(typeof(LODConverter))]
	public struct LOD : ICustomOptionType
	{
		private double _minSize;
		private double _maxSize;
		private double _minZoom;
		private double _maxZoom;

		/// <summary>
		/// When the pixel size of element is less than MinSize the element is not visible.
		/// </summary>
		[DefaultValue(0d)]
		[WidgetOption]
		public double MinSize
		{
			get { return _minSize; }
			set { _minSize = value; }
		}

		/// <summary>
		/// When the pixel size of element is greater than MaxSize the element is not visible.
		/// </summary>
		[DefaultValue(0d)]
		[WidgetOption]
		public double MaxSize
		{
			get { return _maxSize; }
			set { _maxSize = value; }
		}

		/// <summary>
		/// When the map zoom is less than MinZoom the element is not visible.
		/// </summary>
		[DefaultValue(0d)]
		[WidgetOption]
		public double MinZoom
		{
			get { return _minZoom; }
			set { _minZoom = value; }
		}

		/// <summary>
		/// When the map zoom is greater than MaxZoom the element is not visible.
		/// </summary>
		[DefaultValue(0d)]
		[WidgetOption]
		public double MaxZoom
		{
			get { return _maxZoom; }
			set { _maxZoom = value; }
		}

		internal bool IsDefault
		{
			get
			{
				return MinSize == 0 && MaxSize == 0 && MinZoom == 0 && MaxZoom == 0;
			}
		}
	
		/// <summary>
		/// Initializes a new instance of an <see cref="LOD"/> structure.
		/// </summary>
		/// <param name="minSize"></param>
		/// <param name="maxSize"></param>
		/// <param name="minZoom"></param>
		/// <param name="maxZoom"></param>
		public LOD(double minSize, double maxSize, double minZoom, double maxZoom)
		{
			_minSize = minSize; _maxSize = maxSize; _minZoom = minZoom; _maxZoom = maxZoom;
		}

		#region ICustomOptionType

		string ICustomOptionType.SerializeValue()
		{
			return "{" + string.Format("\"minSize\":{0}, \"maxSize\":{1}, \"minZoom\":{2}, \"maxZoom\":{3}", MinSize, MaxSize, MinZoom, MaxZoom) + "}";
		}

		void ICustomOptionType.DeSerializeValue(object state)
		{
			var ht = state as Hashtable;
			if (ht != null)
			{
				if (ht["minSize"] != null)
				{
					MinSize = Convert.ToDouble(ht["minSize"]);
				}
				if (ht["maxSize"] != null)
				{
					MaxSize = Convert.ToDouble(ht["maxSize"]);
				}
				if (ht["minZoom"] != null)
				{
					MinZoom = Convert.ToDouble(ht["minZoom"]);
				}
				if (ht["maxZoom"] != null)
				{
					MaxZoom = Convert.ToDouble(ht["maxZoom"]);
				}
			}
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#endregion
	}
}
