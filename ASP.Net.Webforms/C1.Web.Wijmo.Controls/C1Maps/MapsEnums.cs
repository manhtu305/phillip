﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Maps
{
	/// <summary>
	/// Specifies the map source for the tile layer.
	/// </summary>
	public enum MapSource
	{
		/// <summary>
		/// Use the bing maps with road style.
		/// </summary>
		BingMapsRoadSource,
		/// <summary>
		/// Use the bing maps with aerial style.
		/// </summary>
		BingMapsAerialSource,
		/// <summary>
		/// Use the bing maps with hybrid style.
		/// </summary>
		BingMapsHybridSource,
		/// <summary>
		/// Use the <see cref="C1Maps.CustomSource"/> as the tile server. 
		/// </summary>
		CustomSource,
		/// <summary>
		/// Does not show the tile layer.
		/// </summary>
		None
	}

	/// <summary>
	/// Specifies the type of the <see cref="C1Layer"/>.
	/// </summary>
	[EditorBrowsable(EditorBrowsableState.Never)]
	public enum LayerType
	{
		/// <summary>
		/// It's a <see cref="C1VectorLayer"/>.
		/// </summary>
		Vector,
		/// <summary>
		/// It's a <see cref="C1ItemsLayer"/>.
		/// </summary>
		Items,
		/// <summary>
		/// It's a <see cref="C1VirtualLayer"/>.
		/// </summary>
		Virtual,
		/// <summary>
		/// It's a <see cref="C1CustomLayer"/>.
		/// </summary>
		Custom
	}

	/// <summary>
	/// Specifies the type of data for <see cref="C1VectorLayer"/>.
	/// </summary>
	public enum DataType
	{
		/// <summary>
		/// The data is a collection of <see cref="C1VectorItemBase"/>.
		/// </summary>
		WijJson,
		/// <summary>
		/// The data is an object with the <a href="http://geojson.org/">GeoJSON</a> format.
		/// </summary>
		GeoJson
	}

	/// <summary>
	/// Specifies the type of the vector item.
	/// </summary>
	public enum VectorType{
		/// <summary>
		/// A placemark. Used to draw a mark and label.
		/// </summary>
		Placemark,
		/// <summary>
		/// A polyline.
		/// </summary>
		Polyline,
		/// <summary>
		/// A polygon.
		/// </summary>
		Polygon
	}

	/// <summary>
	/// Specifies the visibility of the label for the placemark.
	/// </summary>
	public enum LabelVisibility
	{
		/// <summary>
		/// Always not draw the label.
		/// </summary>
		Hidden,
		/// <summary>
		/// Does not draw the label if it overlaps with other labels.
		/// </summary>
		AutoHide,
		/// <summary>
		/// Always draw the label.
		/// </summary>
		Visible
	}

	/// <summary>
	/// Specifies the label position relates to the placemark location.
	/// </summary>
	public enum LabelPosition {
		/// <summary>
		/// Label is centered.
		/// </summary>
		Center,
		/// <summary>
		/// Label is at left.
		/// </summary>
		Left,
		/// <summary>
		/// Label is at right.
		/// </summary>
		Right,
		/// <summary>
		/// Label is at top.
		/// </summary>
		Top,
		/// <summary>
		/// Label is at bottom.
		/// </summary>
		Bottom
	}

	/// <summary>
	/// Specifies the shape at the end of the line.
	/// </summary>
	public enum PenLineCap 
	{
		/// <summary>
		/// A cap that does not extend past the last point of the line. Comparable to no line cap.
		/// </summary>
		Butt,
		/// <summary>
		/// A rectangle that has height equals to the line thickness and a length equal to half the line thickness.
		/// </summary>
		Square,
		/// <summary>
		/// A semicircle that has a diameter equal to the line thickness.
		/// </summary>
		Round,
	}

	/// <summary>
	/// Specifies the shape that joins two lines.
	/// </summary>
	public enum PenLineJoin
	{
		/// <summary>
		/// Beveled vertices.
		/// </summary>
		Bevel,
		/// <summary>
		/// Rounded vertices.
		/// </summary>
		Round,
		/// <summary>
		/// Regular angular vertices.
		/// </summary>
		Miter
	}
}
