﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Chart
{
	/// <summary>
	/// Serializes a <see cref="C1LineChart"/> control.
	/// </summary>
	public class C1LineChartSerializer : C1ChartSerializer<C1LineChart>
	{
		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the C1LineChartSerializer class.
		/// </summary>
		/// <param name="obj">
		/// Serializable Object.
		/// </param>
		public C1LineChartSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
