﻿=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20202.332/4.0.20202.332/4.5.20202.332)   Drop Date: 07/11/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20202.160
Winform 4.0.20202.438/4.5.20202.438

All
    Upgrade Winforms dlls from 20202.428 to 20202.438.

C1GridView
    [430737] The CheckBoxField does not get updated in UI on ClientSideEditing.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20202.331/4.0.20202.331/4.5.20202.331)   Drop Date: 07/07/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20202.160
Winform 4.0.20202.428/4.5.20202.428

C1FileExplorer
    [443715] Error occur when right-click on Folder/File.

Licenses
    [443384] Different behavior are observed between Trial license is expired and removed the Trial license when system date is changed to + 30 days.

C1GridView
    [436912] Master GridView Column's width gets changed to full in width on expanding the Detail Grid if doRefresh method is called.

C1GridView
    [431832] Error occurs on Calling RowCommand for Detail Row.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20202.330/4.0.20202.330/4.5.20202.330)   Drop Date: 07/01/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20202.159
Winform 4.0.20202.428/4.5.20202.428

C1FileExplorer
    [442285] Warning message occur when Open, Paste, Delete and Rename Folder.
    [442278] Folder Name is not shown in Rename Box when open Rename dialog of Folder.

Samples
    [437862] Header text is incorrectly displayed at 'ControlExplorer/C1CompositeChart/SharedPie'.

Wijmo Widgets
    [442490] date displays incorrect if the value equal min date and value cannot be empty.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20202.329/4.0.20202.329/4.5.20202.329)   Drop Date: 06/25/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20202.158
Winform 4.0.20202.428/4.5.20202.428

Licenses
    [440608] Found different behavior of license in About box between Webform project and other projects (e.gWPF/Winform etc).
    [442012] License error do not occurs and it works as license although license.licx file or '331cf6cd-b73c-429f-ba79-fa2f85eebd68.gclicx' file or both are deleted in project.
    [442430] Unlike the other products (WFP/WinForm), new trial license overrides valid full old license key.

C1InputText
    [437860] Enter text is appeared again after delete the all data if Japanese string is entered using certain scenarios.

C1GridView
    [440410] Several scrolling issues of GridView.

C1EventsCalendar
    [243179] Console error occurs after clicked "Create Appointment" if "Appointment History" view is opened.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20202.328/4.0.20202.328/4.5.20202.328)   Drop Date: 06/17/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20202.157
Winform 4.0.20202.428/4.5.20202.428

All
    Upgrade Winforms dlls from 20201.424 to 20202.428.

C1GridView
    [436912] Master GridView Column's width gets changed to full in width on expanding the Detail Grid if doRefresh method is called.

C1FileExplorer
    [422907] Add Date column to show the date in FileExplorer.

Samples
    [437862] Header text is incorrectly displayed at 'ControlExplorer/C1CompositeChart/SharedPie'.
    [369660] Postback is not occurred when delete the value in inpudate and lost focus.
    [438049] Mask property is still used in 'C1InputMask2' control instead of MaskFormat property in Password page of C1InputMask.
    [438050] ‘Bold’ style do not applied into Grid data when ‘Bold’ is set to ‘true’ in ‘RowStyle’ property.

C1Editor
    [214591] Block Quote does not work in Edge browser.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20202.327/4.0.20202.327/4.5.20202.327)   Drop Date: 05/21/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20202.156
Winform 4.0.20201.424/4.5.20201.424

All
    Upgrade Winforms dlls from 20201.416 to 20201.424.

C1Editor
    [214591] Block Quote does not work in Edge browser. 

C1GridView
    [398360] [C1GridView] [Print] Error: Script controls must be registered using RegisterScriptControl() before calling RegisterScriptDescriptors() occurred.
    [431829] Detail Row remains expanded on page index change while the Detail DataSource is set by code behind.

C1Upload
    [430277] When you select multiple large files of about 1GB and upload them all at once, the progress bar remains at 0% then suddenly changes to 100%.

Samples
    [379778] Request to update copy right year in some samples.
    [402173] Remove LiveChat icon in all samples.
    [242883] [Control Explorer] export file is downloaded although error is displayed in console when user does not enter server url or invalid server url is enter.
    [422874] Can't export space file name at Custom Handle Response data with IE browser.
    [431002] Replace new MaskFormat.

C1InputNumeric
    [379397] 'InvalidInput' event is not fired on adding invalid character.

C1GridView
    [431942] GridView's data items are not sorted if the GridView virtualization is enabled.

License
    [434131] Different behavior of webform controls are observed in Trail state of licensing when it is run form VS 2019 and VS 2017/VS2015.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20201.326/4.0.20201.326/4.5.20201.326)   Drop Date: 03/05/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20201.155
Winform 4.0.20201.416/4.5.20201.416

All
    Upgrade Winforms dlls from 20193.398 to 20201.416.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20201.325/4.0.20201.325/4.5.20201.325)   Drop Date: 02/27/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20201.154
Winform 4.0.20193.398/4.5.20193.398

Samples
    [419696] Warning messages occur when build ControlExplorer sample.

C1Combox
    [421238] C1ComboBox's dropdown is not shown at correct position when itemTemplate is used in ComboBox.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20201.324/4.0.20201.324/4.5.20201.324)   Drop Date: 02/13/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20201.153
Winform 4.0.20193.398/4.5.20193.398

Samples
    [419364] ControlExplorer Exported file name is different with other sample exporting when blank file name is exported at Custom Handle Response data.
    [419595] Height of some Rows are not retained after scrolling Horizontally when VirtualizationSettings.Mode is set as Columns.

C1Combox
    [403319] The ID attribute is not set properly in the C1ComboBox Control.

C1Dialog
    [419692] [IE][Edge] Error occur when navigate to 'ContentUrl' page of C1Dialog.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20201.323/4.0.20201.323/4.5.20201.323)   Drop Date: 02/03/2020
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20201.152
Winform 4.0.20193.398/4.5.20193.398

All
    Upgrade Winforms dlls from 393 to 398.

C1GridView
    [407417] Data is not displayed at initial state after run sample when VirtualizationSettings.Mode is set as Both or Columns.

ThemeRoller
    [417113] When creating a theme, we confirmed that the theme name displayed by default cannot be changed.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.322/4.0.20193.322/4.5.20193.322)   Drop Date: 11/14/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.151
Winform 4.0.20193.393/4.5.20193.393

C1InputNumber
    [391973] A cursor(caret) will be moving to unexpected position when click and select number in C1InputNumeric on Edge and IE11.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.321/4.0.20193.321/4.5.20193.321)   Drop Date: 11/14/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.150
Winform 4.0.20193.393/4.5.20193.393

All
    Just version upgrade.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.320/4.0.20193.320/4.5.20193.320)   Drop Date: 11/12/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.150
Winform 4.0.20193.393/4.5.20193.393

All
    Upgrade Winforms dlls from 387 to 393.

C1Combox
    [403319] Rollback changeset 372441.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.319/4.0.20193.319/4.5.20193.319)   Drop Date: 11/09/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.150
Winform 4.0.20193.387/4.5.20193.387

All
    Just version upgrade.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.318/4.0.20193.318/4.5.20193.318)   Drop Date: 11/06/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.150
Winform 4.0.20193.387/4.5.20193.387

Samples
    [404127] Request to update online link for images in  some samples.

C1GridView
    [395974] How to prevent pasting in C1GridView, filter textbox in IE10/11?.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.317/4.0.20193.317/4.5.20193.317)   Drop Date: 11/02/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.149
Winform 4.0.20193.387/4.5.20193.387

All
    Upgrade Winforms dlls from 382 to 387.

Samples
    [379778] Request to update copy right year in some samples.
    [402173] Remove LiveChat icon in all samples.
    [397382] [ControlExplorer][Regression] Cursor cannot focus in Index text box of Add function setting in ClientSideFunction page of C1Menu.

C1InputNumber
    [391973] A cursor(caret) will be moving to unexpected position when click and select number in C1InputNumeric on Edge and IE11.

C1Combox
    [403319] The ID attribute is not set properly in the C1ComboBox Control.

C1Tab
    [403747] CssClass not working properly.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20193.316/4.0.20193.316/4.5.20193.316)   Drop Date: 10/18/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20193.148
Winform 4.0.20192.382/4.5.20192.382

All
    [390213] [WebForm] Remove unused project, solution files.

Samples
    [392123] [Localization] Collect new resource strings for adding new sample pages.    
    [371514] Axis label is overlap and some display of page FixedLabelWidth is incorrect.
    [193887] [C1ReportViewer][HowTo] Export format is not correct using 'Custom export・button in 践owTo・sample 舛1ReportViewerChangeExportButtonBehavior・of VB application.
    [395814] [Asp.Net Wijmo][ControlExplorer] Javascript error occurs when 'Center' or 'Bottom' is applied to 'Aligns to TextBox' setting in Combobox -> Position sample.

C1Editor
    [394284] Even if "RemoveFormat" is set to the SimpleModeCommands property, "Remove Format" is not displayed on the toolbar.
    [395844] [C1Editor][Edge] Some inconsistent behavior are observed when editor control is run with edge browser.

C1GridView
    [400551][C1GridView] [Scroll Position] [Edit] ScrollPosition is not maintained for postback call to edit the row.
    [398360] [C1GridView] [Print] Error: Script controls must be registered using RegisterScriptControl() before calling RegisterScriptDescriptors() occurred.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.315/4.0.20192.315/4.5.20192.315)   Drop Date: 09/06/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.147
Winform 4.0.20192.382/4.5.20192.382

All
    Upgrade WinForms dll from 375 to 382.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.314/4.0.20192.314/4.5.20192.314)   Drop Date: 08/28/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.147
Winform 4.0.20192.375/4.5.20192.375

All
    Just version upgrade.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.313/4.0.20192.313/4.5.20192.313)   Drop Date: 08/20/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.146
Winform 4.0.20192.375/4.5.20192.375

ThemeRoller
    [385912] 'Central dir not found' error occurs when new theme is added by Themeroller into controls.

EventsCalendar
    [385416] Adding a custom field to EventsCalendar and then Update the field in DataBase.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.312/4.0.20192.312/4.5.20192.312)   Drop Date: 07/04/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.145
Winform 4.0.20192.375/4.5.20192.375

All
    Upgrade WinForms dll to 375.

C1GridView
    [386447] [Asp.Net Wijmo][ControlExplorer][C1GridView][Regression] Columns are disorted and console error occurs  in Basic columns page.
    [379775][C1GridView] H-Scrollbar overlaps the last row and fixed-columns offset and overlaps H-Scrollbar.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.311/4.0.20192.311/4.5.20192.311)   Drop Date: 06/28/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.145
Winform 4.0.20192.372/4.5.20192.372

All
    [385940] Cannot load some report file in ControlExplorer because 4.5 dll version for 'C1.C1Report.CustomFields' is missing in bin folder of Webform.

C1GridView
    [379775][C1GridView] H-Scrollbar overlaps the last row and fixed-columns offset and overlaps H-Scrollbar.

Samples
    [376153] [ASP.Net Wijmo][Wijmo Widget][WidgetExplorer][MobileExplorer][MVCExplorer][ToolkitExplorer] Data cannot load and error is occurred in some sample pages.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.310/4.0.20192.310/4.5.20192.310)   Drop Date: 06/25/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.144
Winform 4.0.20192.372/4.5.20192.372

All
    Upgrade WinForms dll to 372.
    [384962] Webform 4.5 dll and WInform 4.5 dll are misssing in bin folder of Webform build.
    [385223][385371] [Target452] Change designer reference of webform's controls to 45.
    [385368] WebForm and WinForm dlls cannot restore and license error is displayed.

=========================================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20192.308/4.0.20192.308/4.5.20192.308)   Drop Date: 06/14/2019
=========================================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20192.144
Winform 4.0.20191.362

All
    Upgrade to support target framework 4.5.2.
    Apply target framework 4.5.2 for some samples: C1Finance.4, ControlExplorer, iTunes.4, MattressBank4.

C1Upload
    [256635]  Some files get skipped on uploading using C1Upload if number of files is 2593.

Samples    
    [376251] Replace the quandl data source with our own datasource for WebForm.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.307/4.0.20191.307)		Drop Date: 05/21/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.143
Winform 4.0.20191.362

C1GridView
    [380243] [Asp.NetWijmo][Wijmo Widget][C1GridView][Regression] Columns are disorted when showRowHeader is set as True in gridview containing band columns

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.306/4.0.20191.306)		Drop Date: 05/15/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.143
Winform 4.0.20191.362

C1GridView
    [376353] [Asp.NetWijmo][Wijmo Widget][C1GridView][Regression] Data values are shown incorrectly in gridview containing band columns

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.305/4.0.20191.305)		Drop Date: 05/14/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.143
Winform 4.0.20191.362

Samples
    [377885] CDN link version is still used 142 instead of 143 in 'C1ReportViewerMVC' sample.

C1GridView
    [377815] [Asp.NetWijmo][Wijmo Widget][C1GridView][Regression] Fore color of header text is not shown in C1TemplateField column
    [377973] [Asp.NetWijmo][Wijmo Widget][C1GridView][Regression] Content of HeaderTemplate cannot be displayed in gridivew

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.304/4.0.20191.304)		Drop Date: 05/06/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.143
Winform 4.0.20191.362

C1GridView
    [376359] [C1GridView] Second Band Column is not displaying with Lower level
    [376353] [Asp.NetWijmo][Wijmo Widget][C1GridView][Regression] Data values are shown incorrectly in gridview containing band columns

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.303/4.0.20191.303)		Drop Date: 04/23/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.142
Winform 4.0.20191.362

wijinput
    [371354] A part of number in TextBox are hidden by drop down button when project are applied Bootstrap theme.

C1ThemeRoller
    [374604] Error is occurred when a custom theme is created by using 'ThemeRoller'.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.301/4.0.20191.301)		Drop Date: 04/09/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.142
Winform 4.0.20191.362

All
    Upgrade winform build from 359 to 362.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.300/4.0.20191.300)		Drop Date: 03/06/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.142
Winform 4.0.20191.359

All
    Upgrade winform build from 343 to 359.

C1InputDate
    [344880] A post back happen when control lost focus even though user don't change value.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20191.299/4.0.20191.299)		Drop Date: 01/28/2019
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20191.141
Winform 4.0.20183.343

All
    Upgrade winform build from 338 to 343.
    
C1Editor
    [361413] The Focus method do not work on calling from code -behind.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20183.298/4.0.20183.298)		Drop Date: 11/8/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20183.140
Winform 4.0.20183.338

All
    Upgrade winform build from 336 to 338.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20183.297/4.0.20183.297)		Drop Date: 11/5/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20183.140
Winform 4.0.20183.336

All
    Upgrade winform build from 335 to 336.

C1GridView
    [341626] Incorrect row selection on window resize after virtual scrolling.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20183.296/4.0.20183.296)		Drop Date: 11/1/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20183.140
Winform 4.0.20183.335

All
    Upgrade winform build from 333 to 335.

C1Input
    [346247] The number is not selected when enter number before focus outside of control on IOS touch device.

C1InputDate
    [344880] A post back happen when control lost focus even though user don't change value.

Samples
    [346221][C1Finance] Error is occurred after browse the sample.
    [346118][ControlExplorer] Some of the words are displayed as JP words in BubbleChart control' sample although ENG build is used in ENG environment.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20183.295/4.0.20183.295)		Drop Date: 10/26/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20183.139
Winform 4.0.20183.333

All
    Upgrade winform build from 326 to 333.

C1GridView
    [346944] [Header Template] [Virtual Scrolling] If the Header Template is applied with VirtualScrolling, the incorrect string gets displayed on horizontal scrolling.

C1InputNumber
    [346848] A value is not same with shown value when post back.

C1Input
    [346107] Value is disappeared if focusing behind last two digits of decimal point after value is changed in Chrome and FF browsers.
    [346247] The number is not selected when enter number before focus outside of control on IOS touch device.

Samples
    [346221] Error is occurred after browse the sample C1Finance.4.
    [346118][ControlExplorer] Some of the words are displayed as JP words in BubbleChart control' sample although ENG build is used in ENG environment.
    [346814][ControlExplorer] Value of 'Apply' button is displayed as '<%= Resources...........' in 'Carousel - Animation' sample page.
    [253409][Maps] Tooltip does not disappear on zoom in action.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20183.294/4.0.20183.294)		Drop Date: 10/8/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20183.137
Winform 4.0.20183.326

All
    Just version upgrade.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20183.293/4.0.20183.293)		Drop Date: 10/4/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20183.137
Winform 4.0.20183.326

All
    Upgrade winform build from 4.0.20182.314 to 4.0.20183.326.
    [342441] Prevent XSS attack for wjimo resources.

C1Input
    [336370] A String can be pasted to InputText on Chrome even though control has disable setting.
    [319493] Postback is not occur in C1InputCurrency,C1InputNumeric & C1InputPercent although setting 'HideEnter' property to false as default.
    [329614] Value is not changed to negative when enter (-) sign with with Japanese Romaji keyboard.

C1InputText
    [343860] Text gets set back on focus out after clearing text using (x) button for C1InputText in Edge.

C1InputDate
    [107724] Focus moves to the last field when C1InputDate gains focus through tab key navigation.
    [344880] A post back happen when control lost focus even though user don't change value.

C1Combox
    [331392] Fix bound data issue.

C1GridView
    [341753] A Script error happen when user click grid control during virtual scrolling.
    [341626] Add ResetScrollState() method.
    [344157] On Browser resize, GridView do not fit according to Browser width after paging if GridView width is not sepcified.

Samples
    Update http://services.odata.org to https://services.odata.org in ControlExplorer sample.
    [336482] Some pages of ControlExplorer sample do not work in https.
    [332342] Support https in ThemeExplorer sample with latest wijmo3.
    [341896][Localization] Apply JP localization string to ControlExplorer.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.291/4.0.20182.291)		Drop Date: 7/6/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.136
Winform 4.0.20182.314

Samples
    [332320] Error occurs if accessing online samples via https.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.290/4.0.20182.290)		Drop Date: 7/2/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.136
Winform 4.0.20182.314

All
    Upgrade winform build from 313 to 314.

ControlExplorer
    [330292] Need to localize in some words for 'ExportingPdf.aspx' samples for Charts, EventsCalendar and GridView control.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.289/4.0.20182.289)		Drop Date: 6/27/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.136
Winform 4.0.20182.313

All
    Upgrade winform build from 312 to 313.
    [329318] Apply JP xml documents.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.288/4.0.20182.288)		Drop Date: 6/25/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.136
Winform 4.0.20182.312

C1Inputs
    [318435] Input entered for C1Input gets cleared on focus out from Control.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.287/4.0.20182.287)		Drop Date: 6/21/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.135
Winform 4.0.20182.312

All
    Upgrade winform build from 310 to 312.

ControlExplorer
    [327644] Japanese words are displayed in side menu in ENG environment although ENG build is used.
    [327748] Some of the words need to localize in sample.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.286/4.0.20182.286)		Drop Date: 6/16/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.135
Winform 4.0.20182.310

Samples
    [326789] Apply JP Resources to some samples.

ControlExplorer
    [326887] Apply button tile is displayed as '<%=Resource' in 'CandlestickChart -> Trendline' sample.

C1Calendar
    [322977] The year and Era format is not display correctly in Calendar header.

C1Inputs
    [318435] Input entered for C1Input gets cleared on focus out from Control.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.285/4.0.20182.285)		Drop Date: 6/8/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.134
Winform 4.0.20182.310

All
    Upgrade winform build from 309 to 310.

ControlExplorer
    [324526] The value of MaskFormat is invalid' error occurs when navigate to InputMask('DBCS' and 'Hiragana') sample page.
    [324527] Era format cannot displayed properly in 'InputDate -> CustomEra' sample page.

Samples
    [326078] Chart data and grid data do not load in Finance samples.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20182.284/4.0.20182.284)		Drop Date: 5/25/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20182.133
Winform 4.0.20182.309

All
    Upgrade winform build from 296 to 309.
    [315864] Use JP xml docments for JP build.

Samples
    [313254] Add JP resources in WebForms ControlExplorer sample.

C1GridView
    [312551] When putting TemplateField in C1DetailGridView and UpdateBindings is set, there would be an exception thrown.
    [322777] The caption of button on C1CommandField will be changed to JP String after we postback web forms.
    [311329] Improved the performance of C1GridView.

C1InputDate
    [310036] Inconsistent behavior for short date pattern on wijinputdate.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20181.283/4.0.20181.283)		Drop Date: 3/8/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20181.132
Winform 4.0.20181.296

C1GridView
    [311354]Add zh resources for C1GridView filter options.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20181.282/4.0.20181.282)		Drop Date: 3/7/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20181.132
Winform 4.0.20181.296

C1GridView
    [311354]Add resources for C1GridView filter options.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20181.281/4.0.20181.281)		Drop Date: 3/2/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20181.132
Winform 4.0.20181.296

All
    Upgrade winform build from 293 to 296.

C1GridView
    [299338]Grid scrolls up when changing the column width after scrolling.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20181.280/4.0.20181.280)		Drop Date: 2/9/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20181.131
Winform 4.0.20181.293

All
    Upgrade winform build from 292 to 293.

C1Upload
    [307947]Show all uploaded files after uploading multiple files.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20181.279/4.0.20181.279)		Drop Date: 2/2/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20181.130
Winform 4.0.20181.292

C1Editor
    [306427]Both Japanese words and English words are displayed in tooltip for some icons in JP environment.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20181.278/4.0.20181.278)		Drop Date: 1/26/2018
=============================================================================================
Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20181.129
Winform 4.0.20181.292

All
    Upgrade winform build from 286 to 292.

C1Editor
    [299007]Localization of font size becomes invalid after postback.

C1GridView
    [299338]Grid scrolls up when changing the column width after scrolling.
    [304993]RowCreated event of C1DetailGridView is not fired.

Samples
    [275759]C1BarChart axix gets interchanged on every alternative SelectionChanged in ProductDashBoard sample.
    [305453]Update the documentation links in ControlExplorer sample.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.277/4.0.20173.277)		Drop Date: 11/30/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.128
Winform 4.0.20173.286

All
    Upgrade winform build from 285 to 286.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.276/4.0.20173.276)		Drop Date: 11/27/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.128
Winform 4.0.20173.285

All
    Upgrade winform build from 282 to 285.
    [298124]Request to change Asian market C1 icon and request to remove PostalCode/Address/Email in AboutBox of the C1 Controls.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.275/4.0.20173.275)		Drop Date: 11/21/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.127
Winform 4.0.20173.282

All
    Downgrade winform build from 283 to 282.

Samples
    [296530]Request to update online link in 'C1Finance' (Webform samples) and 'StockPortfolio' (MVC Classic sample).

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.274/4.0.20173.274)		Drop Date: 11/02/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.126
Winform 4.0.20173.283

All
    Upgrade winform build from 282 to 283.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.273/4.0.20173.273)		Drop Date: 10/30/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.126
Winform 4.0.20173.282

All
    Just version upgrade.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.272/4.0.20173.272)		Drop Date: 10/27/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.125
Winform 4.0.20173.282

All
    Upgrade winform build from 277 to 282.

Samples
    [293064]Video file does not load after navigate to videos sample page of Lightbox and Carousel control.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.271/4.0.20173.271)		Drop Date: 10/18/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.124
Winform 4.0.20173.277

All
    Just version upgrade.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.270/4.0.20173.270)		Drop Date: 10/13/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.123
Winform 4.0.20173.277

Samples
    [288979]Images cannot be displayed in 'Thumbnails' page of Carousel in ControlExplorer and ToolkitExplorer samples.
    [290247]Logo of ToolKitExplorer sample is not updated.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.269/4.0.20173.269)		Drop Date: 09/29/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.122
Winform 4.0.20173.277

C1GridView
    [276657]An error is thrown when double clicking on the control placed in the "EmptyDataTemplate".

Samples
    [289072]Error is observed when any theme is selected in ThemeExplorer sample.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20173.268/4.0.20173.268)		Drop Date: 09/26/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20173.121
Winform 4.0.20173.277

All
    Upgrade winform build from 271 to 277.

Samples
    [288727]Update new icons and links in WebForm samples.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.267/4.0.20172.267)		Drop Date: 09/15/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.120
Winform 4.0.20172.271

Samples
    [285289]Add google analytics tags in all web demo samples.

C1GridView
    [285295]Make a private sample or assembly to export all pages when the total row count is very huge.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.266/4.0.20172.266)		Drop Date: 09/13/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.119
Winform 4.0.20172.271

C1GridView
    [285295]Make a private sample or assembly to export all pages when the total row count is very huge.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.265/4.0.20172.265)		Drop Date: 09/12/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.119
Winform 4.0.20172.271

Samples
    [271794]Add C1.Win.Bitmap.4.dll reference in ControlExplorer sample.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.264/4.0.20172.264)		Drop Date: 06/29/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.118
Winform 4.0.20172.271

All
    Upgrade winform build from 267 to 271.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.263/4.0.20172.263)		Drop Date: 06/20/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.116
Winform 4.0.20172.267

All
    Upgrade winform build from 266 to 267.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.262/4.0.20172.262)		Drop Date: 06/15/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.116
Winform 4.0.20172.266

All
    Upgrade winform build from 263 to 266.

Samples
    [265426] (404) Not Found error occurs after browse the C1Finance samples.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.261/4.0.20172.261)		Drop Date: 06/06/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.115
Winform 4.0.20172.263

C1Editor
    [264218] Editor can be edit when modal dialog box from special ribbon(such as image browser dialog) is opened.
    [264358] Cannot set paragraph style in editor using style from paragraph's ribbon.
    [264398] In compact design, error occurs after open the spell checker dialog if full screen mode is on.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20172.260/4.0.20172.260)		Drop Date: 06/03/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20172.114
Winform 4.0.20172.263

All
	Upgrade winform build from 260 to 263.

C1Editor
	[245498] Image is always added on the top of the html page.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20171.259/4.0.20171.259)		Drop Date: 05/10/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20171.113
Winform 4.0.20171.260

All
	Upgrade winform build from 259 to 260.
	[256730]"C1.C1Zip.4.dll" is missing in "bin" folder of JPN build.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20171.258/4.0.20171.258)		Drop Date: 05/05/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20171.113
Winform 4.0.20171.259

All
	Upgrade winform build from 248 to 259.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20171.257/4.0.20171.257)		Drop Date: 04/28/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20171.113
Winform 4.0.20171.248

C1GridView
	[247635] C1GridView disappears when rendering the control in IE11 compatibility mode.

C1InputDate
	[205993] DesignTime error in VisualStudio when C1InputDate is placed inside C1Accordion.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20171.256/4.0.20171.256)		Drop Date: 03/01/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20171.112
Winform 4.0.20171.248

All
	Upgrade winform build from 243 to 248.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20171.255/4.0.20171.255)		Drop Date: 02/24/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20171.112
Winform 4.0.20171.243

C1TooltipExtender
	[231597] Unlike the others extender controls, evaluation message of Tooltipextender control does not show in Trial state.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20171.253/4.0.20171.253)		Drop Date: 02/13/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20171.111
Winform 4.0.20171.241

C1Editor
	[236507] "ApplyChlorfoam" not considered as an incorrect word by the C1Editor spell checker.

C1EventsCalendar
	[234442] Events not getting set at regular time interval after making the event as an All Day event in the demo sample.

C1FlipCard
	[235258] How to access the controls placed inside the FlipCardPane on server side.

C1GridView
	[234422] Backcolor of column header not getting set as expected when using the HeaderStyle property.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.252/4.0.20163.252)		Drop Date: 01/10/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.110
Winform 4.0.20163.227

All
	Upgrade winform build from 226 to 227.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.251/4.0.20163.251)		Drop Date: 01/06/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.110
Winform 4.0.20163.226

All
	Upgrade winform build from 225 to 226.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.250/4.0.20163.250)		Drop Date: 01/04/2017
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.110
Winform 4.0.20163.225

	Just version upgrade.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.249/4.0.20163.249)		Drop Date: 12/28/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.109
Winform 4.0.20163.225

C1EventsCalendar
	[229219] How to serialize C1EventsCalendarStorage and save it in the session.
	[229521] Calendar name does not show in calendar's dropdown of new event dialog box when using custom data storage.

C1GridView
	[229195] Some inconsistence behavior is observed when set the "ShowFilter" property in "page_load" method and if the data is bind with dynamically.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.248/4.0.20163.248)		Drop Date: 12/19/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.109
Winform 4.0.20163.219

C1AutoComplete
	[227959] Background color of pull-down list is transparent when WijmoCssAdapter property is set to 'bootstrap'.

C1CompositeChart
	[219615] When C1CompositeChart has multiple Series and ChartStyles some LineChart are displayed broken off.

C1Dialog
	[218669] JavaScript error is occurred when postback is occurred twice in minimized state.

C1GridView
	[222796] Row selection not getting updated when navigating through cells in Safari.
	[220469] Value not getting selected on receiving focus on an iOS touch device.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.247/4.0.20163.247)		Drop Date: 11/2/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.108
Winform 4.0.20163.212

All
	Update wijmo3 client side js from 107 to 108 and upgrade winform build from 211 to 212.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.246/4.0.20163.246)		Drop Date: 10/31/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.107
Winform 4.0.20163.21

C1GridView
	[215380] Navigation buttons lose the text value set using the properties during the postback.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.245/4.0.20163.245)		Drop Date: 10/27/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20163.105
Winform 4.0.20163.211

C1InputText
	[214556] AutoPostback doesn't work.

C1TreeMapExtender
	[214567] TreeMapExtender show evaluation message even license key is activate.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.244/4.0.20163.244)		Drop Date: 10/19/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.104
Winform 4.0.20163.200

All
	[209796] WebForm release package does not contains the C1.C1Reports and other dependency dlls.

ToolKitExplorer
	[211471] 'C1.Web.Wijmo.Extenders.C1Video.C1VideoExtender' is missing in licenses.licx.
	[211463] Images do not load properly and "403 (Forbidden)" error are displayed.

HowTo Samples
	[202354] Showing non-existed file "readme-guids.txt" in ProductDashboard sample with VS.

C1GridView
	[202195] Content of HeaderTemplate disappeared when we refresh the C1GridView after resize on client side.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20163.243/4.0.20163.243)		Drop Date: 09/29/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.104
Winform 4.0.20163.200

ControlExplorer
	[206943] JavaScript error is occurred when Excel file (.xlsx) file type is exported.

C1ButtonExtender
	[206008] In trial-license state, evaluation message is not displayed in button extender.

C1GridView
	A obvious flash is observed when running a page with C1GridView.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.242/4.0.20162.242)		Drop Date: 08/30/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.103
Winform 4.0.20162.188

ControlExplorer
	[205360] Images do not load properly and "403 (Forbidden)" error are displayed.

C1GridView
	[204890] Error are observed when grid data are filter by date time value.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.241/4.0.20162.241)		Drop Date: 08/29/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.103
Winform 4.0.20162.188

All controls
	[201624] Application crashes on adding additional controls to the WebForm along with the extender control.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.240/4.0.20162.240)		Drop Date: 08/24/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.102
Winform 4.0.20162.181

C1AutoComplete
	[202319] ARIA Errors occur in some conditions.

C1GridView
	[203911] ARIA support errors are observed when input box is displayed in grid view by editing AutoGenerateEdit button.
	[203008] C1GridView continuously showing "Loading...." message if we filter it with "<h1>" tag.

C1InputDate
	[204495] AutoPostBack property does not work in C1InputDate.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.239/4.0.20162.239)		Drop Date: 08/15/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.101
Winform 4.0.20162.181

C1ReportViewer
	[201868] C1ReportViewer does not pre-compile if we change its DataSource expliciltly after deployment.
	[202319] ARIA Errors occur in some conditions.
	[198096] C1ReportViewer application not getting deployed when published disabling "Enable Updateable".

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.238/4.0.20162.238)		Drop Date: 08/04/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.100
Winform 4.0.20162.166

ThemeRoller
	[201535] Can not add custom theme back to the project while created using ThemeRoller.

C1EventsCalendar
	[150473] Can't use EventsCalendar with multiple users and multiple data file.
	[198536] Refactor the implementation of VisibleCalendars and Calendars properties of C1EventCalendar control.

HospitalOne
	[147348] Error message is shown with certain scanerio.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.237/4.0.20162.237)		Drop Date: 07/05/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.99
Winform 4.0.20162.166

C1EventsCalendar
	[197760] Fix the bug about created new calendar is not displayed in Japanese Language setting.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.236/4.0.20162.236)		Drop Date: 07/04/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.98

ControlExplorer
	[197336]Export file is downloaded although error is displayed in console when server url link is expired in 'Custom Handle Response' Sample.

C1EventsCalendar
	[197760] New calendar which is created in run time is always show in Calendar list although it is uncheck in display check box.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.235/4.0.20162.235)		Drop Date: 06/29/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.97

C1Editor
	[195722]Ribbon of Editor control is distorted after browsing in jp environment.

ControlExplorer
	[196253] New event cannot create when eventcalendar is using data binding.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.234/4.0.20162.234)		Drop Date: 06/23/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.96

C1InputDate
	[195027]Unable to enter date manually in C1InputDate after partial postback.

Samples
	[193887] Export format is not correct using 'Custom export' button in 'HowTo' sample 'C1ReportViewerChangeExportButtonBehavior' of VB application.

C1Editor
	[152624]Ribbon title cannot display as a Japanese word when set width in C1Editor.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20162.233/4.0.20162.233)		Drop Date: 06/14/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20162.95

All Extenders
	[191927] Extender does not work if AW key is activated.

ControlExplorer
	[151838] When we export C1XLBook into an excel (.xlsx) file then a "Invalid Format" error is thrown while opening it with MSExcel.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.232/4.0.20161.232)		Drop Date: 06/01/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.94

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.231/4.0.20161.231)		Drop Date: 05/06/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.92

C1GridView
	[152483] OnRowUpdating event is not fired while updating a row in C1GridView.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.230/4.0.20161.230)		Drop Date: 04/29/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.92

Extenders
	Fix the issue that license can't be applied to all extenders.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.229/4.0.20161.229)		Drop Date: 04/25/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.91

ControlExplorer
	[148290] Download links on demos.
	[151838] When we export C1XLBook into an excel (.xlsx) file then a "Invalid Format" error is thrown while opening it with MSExcel.

ControlExplorer & ToolkitExplorer
	[151154] Request to provide a way to display events that are stored in web storage on initial load of C1EventsCalendar.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.228/4.0.20161.228)		Drop Date: 04/12/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.90

C1EventsCalendar
	[144868] In Data-Binding Sample, clicking "Delete" button does not work after adding new events.
	[149639] C1EventsCalendar displays all the calendars even on setting the VisibleCalendars property.

ToolkitExplorer
	[147287] Next arrow key does not work when set the animation in Carousel.

C1Dialog
	[150325] Can't click button on C1Dialog in Safari.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.227/4.0.20161.227)		Drop Date: 03/01/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.89

C1EventsCalendar
	[143262]Can't use C1 controls on EventDialogTemplate.
	[146938]System.Data.EvaluateException is occurred when filter by 'greater than/less than' in Boolean data type.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.226/4.0.20161.226)		Drop Date: 02/22/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.88

All
	[99495] "Error invoking About... " is observed when opening the AboutBox link of inherited C1 control.

ControlExplorer & ToolkitExplorer
	[47884] Incorrect sample name 'Hirigana' is observed when opening C1InputMask samples.
	[142828] Filtering results of 'Total Amount' column are not shown correctly after navigating another page which is filtered by FilterExpression in C1TemplateField.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.225/4.0.20161.225)		Drop Date: 02/16/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.87

ToolkitExplorer
	[106481] Request to change Copyright year from '2013' to '2015' in home page

Chart
	[144706] Error is occurred when 'Collection Editor' of charts are opend.
	[144666] Error is occurred when a custom theme is created by using 'ThemeRoller'.

Accordion
	[144642] Error is occurred when 'C1Accordion Designer Form' is opened.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20161.224/4.0.20161.224)		Drop Date: 01/29/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20161.85

All
	[140070]Do obfuscation on webform project.

C1Calendar & C1CalendarExtender
	[138399] Add an InitialView property to Calendar.
	[138400] Add titleformat support to year when calendar is on month view.

ControlExplorer & ToolkitExplorer
	[138402] Add InitialView sample.
	[138401] Add titleFormat sample.
	[138396] Add filter sample for template field.

C1GridView
	[137280] Implement filtering support in template field.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20153.223/4.0.20153.223)		Drop Date: 01/03/2016
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20153.84

C1Charts
	[135519] Fix the incorrect default values in AnnotationBase Collection Editor

ControlExplorer
	Replace the value of the property Href for ImageAnnotation with C1 icon.
	[41908] Data series are not loaded in the "External DataSource" sample of BarChart and "Ajax Data" sample of LineChart in Control Explorer.

C1TreeView
	[135698] Selection is lost when pressing 'Enter' key after selecting one treeview item using 'Down' arrow key.

C1Editor
	[94251] Unlike previous build 188, uploading cannot be done second time after an image is placed in the textarea of editor.
	[109562] Make the Culture property to work so as to localize C1Editor.

C1PieChart & C1Sparkline
	[136605] Request to remove "Annotation" from its Smart tag and property window.

MattressBank
	[136725] Javascript error occurs when running MattressBank sample.

EventPlannerForWebForms
	[124404] New Event can't be created at all view in EventPlannerForWebForms sample.
	[129772]'HTTP403.14' error found when starting up page in sample EventPlannerForWebForm.

Hospital_One
	[136842] 'starts' and 'ends' input textboxes are inconsistence when opening one appointment at the Week view in 'Appointment History' tab.

C1Combox
	[99709] Unlike previous build,error "Cannot read property of null" is thrown if C1ComboBox column has empty value with ItemTemplate.

Core
	[101124]C1Controls' bootstrap style can't work properly when C1Controls and C1Extenders are placed together on the same page.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20153.222/4.0.20153.222)		Drop Date: 11/03/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20153.83

C1ThemeRoller
	[136576] Unlike previous build, Custom theme cannot be created by ThemeRoller.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20153.221/4.0.20153.221)		Drop Date: 11/02/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20153.83

Hospital_One
	[129779] 'Internal Server Error' occurs after sorting any column of Appointments tab in 'UserDashboard' page.

C1Charts
	[135509] Add jp resources for the annotation of C1Charts.

C1Editor & C1EditorExtender
	[111339] Request to provide an option to make C1Editor readonly

C1Upload
	Update the description of property 'TempFolder' with a more exact expaination.

ControlExplorer
	[129547] Exception is thrown when enter null value into Date cell in 'Client-side Editing' sample.
	[111339] (Add sample to ToolkitExplorer and ControlExplorer)

AdventureWorks
	[129516]Javascript error occurs when clicking 'Buy' button in product.aspx page.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20153.220/4.0.20153.220)		Drop Date: 10/23/2015
=============================================================================================
Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20153.82

ControlExplorer
	[134240] Unlike previous sample, console error occurs when clicking on header of C1Accordion in 'Postback Events' sample
	[134099] Unlike previous 217_dev, error "Server Error in '/' Application" occurs after clicking C1BarChart's Annotation sample

C1Upload
	[113279] Upload button is still able to click in dim state.

C1Charts
	[131910] Filled colors of circle annotation are disappeared after postback is occurred.
	[131881] All removed annotations display again after postback is occurred.

C1ThemeRoller
	[129375] Error occurs in Themeroller when we try to create a new theme in C1 controls.

C1Dialog
	[86585] Click event of C1Dialog does not work in Firefox if the script is registered in server side code.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20153.219/4.0.20153.219)		Drop Date: 10/10/2015
=============================================================================================
Samples
	[124294] Tooltip is displayed in Description tab page (Fixed in both ToolkitExplorer and ControlExplorer).

Core
	[124469] Internal Server Error occurred when WebRequest class is used to send data to server containing C1Gallery.

C1Accordion
	[123290] UniqueID of control placed in pane is changed when removing another pane.
	[87140] Server error is observed when Accordion is binding.

C1BarCode
	[96601]Value set to some properties at runtime are not retained after postback.

C1InputDateExtender & C1InputDate
	[126680] Add a new property called DbValue.

C1Expander
	[88742]C1GridView does not resize when loaded inside C1Expander.

C1ReportViewer
	[94691] 'report parameters' tab is still shown when setting DisplayVisible property to false.

Chart
	Add chart annotation support to webform controls.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.218/4.0.20152.218)		Drop Date: 09/01/2015
=============================================================================================
C1Editor
	[95494] 'Undefined' is shown in textarea and code side of Split View.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.217/4.0.20152.217)		Drop Date: 08/31/2015
=============================================================================================
Samples
	[124161] "Licx" files are not included in some of the samples in "C1WebWijmo" sample.
	[124160][124157] Fix the js error when both Extenders and Controls are used in the sample.
	[124284] Animation setting is displayed in Description page.
	[124286] The last tab bar's title does not display completely.
	[124358] Error : 'Could not load file or assembly' error is observed  when clicking the  'Drop Down' sample page.
	[124285] 404 error occurs when click "Template" in Radial Guage Toolkit
	[124296] No description is mention in some of the sample page
	[124399] "Error : HTTP Error 500.19 Internal Server Error" is observed after browsing "C1ReportViewerPrintWithoutPreview" sample.

ControlExplorer
	[119955] The CSS style of ControlExplorer was not correctly display .

C1Gauge
	[123637] The pointer of C1RadialGauge is not working properly.

C1FileExplorer
	[114280] Warning message is shown when copy and paste files using shortcuts [Shift + M] to open ContextMenu.

C1Dialog
	[94783] Javascript error is observe after postback occur when C1Dialog is in minimize state and setting "ContentUrl".

C1EventsCalendar
	[C1EventsCalendar][Japan Issue] Can't export data after modifying recurring event.

C1ComboBox
	[99737] Unlike previous build , the dropdown cannot be opened after making C1ComboBox item invisible with Item Template.

C1Editor
	[123979][Spartan] Vertical scroll bar always scroll-up when formatting is changed.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.216/4.0.20152.216)		Drop Date: 07/10/2015
=============================================================================================
C1GridView
	[123816][Asp.Net Wijmo][C1GridView] C1GridView is distorted when perform keyboard navigation in scrollable grid.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.215/4.0.20152.215)		Drop Date: 07/07/2015
=============================================================================================
Extenders
	Update the jp resources.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.214/4.0.20152.214)		Drop Date: 06/30/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20152.76

	Drop extenders for JP version.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.213/4.0.20152.213)		Drop Date: 06/28/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20152.75

C1ListView
	[120865]'FilterPlaceholder' property in C1ListView is not working at run time

ControlExplorer
	[119955][Asp.Net Wijmo][ControlExplorer][VS2015]The CSS style of ControlExplorer was not correctly display .

C1AppView C1ListView
	[39848][Mobile Controls][C1AppView][C1ListView][C1AppViewPage]There is no specific icon image for C1AppView, C1ListView and C1AppViewPage in Visual Studio Toolbox.
	[41801][C1AppViewExtender][C1ListViewExtender] Specific icons for C1AppViewExtender and C1ListViewExtender are not display in the toolbox

EventPlannerForWebForms
	Fix the issue that EventPlannerForWebForms sample can work well when it's deployed to IIS hosting.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20152.212/4.0.20152.212)		Drop Date: 06/10/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20152.74

C1GridView
	[118696][Japan Issue][ASP.Net Wijmo][C1GridView] Horizontal scrollbar is not working with IE 11 compatibility view when VirtualizationSettings.Mode is "Both".
	Add google analytics to CE.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.211/4.0.20151.211)		Drop Date: 05/31/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.71

C1Accordion
	[87394][Asp.Net Wijmo][C1Accordion][C1Input][C1Wizard][C1Expander]Unlike Previous build 181,Javascript error is observe after AutoPostback is set to true when setting script manager is place on the same page.

C1TreeView
	[115047] Add AllowDrag/AllowDrop to the treeNode.

C1GridView
	Add KeyActionTab to C1GridView.
	[118696][Japan Issue][ASP.Net Wijmo][C1GridView] Horizontal scrollbar is not working with IE 11 compatibility view when VirtualizationSettings.Mode is "Both".

C1ComboBox
	[114903]Request to provide information of the significance of DataSelectedField in C1ComboBox.

C1FileExplorer
	[113500][Asp.Net Wijmo][C1FileExplorer]Unlike previous build 181,  Image file can't be opened  with  double clicking on it or clicking 'Open' icon from toolbar.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.210/4.0.20151.210)		Drop Date: 05/10/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.71

C1Splitter
	[94232][Wijmo Widget][WijSplitter]WijSplitter collpases, when resizing the browser window.

C1GridView
	[113911] [Asp.Net Wijmo][C1GridView]Unlike previous build 207, "not implemented" javascript error is observe after setting "ScrollMode" to "Auto" without setting "Width" property.

C1EventCalendar
	[112255][Asp.Net Wijmo][C1EventsCalendar][ControlExplorer][Export]Created events can't be displayed in exported image of pdf file of C1EventsCalendar.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.209/4.0.20151.209)		Drop Date: 03/30/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.71

C1Splitter
	[94232][Wijmo Widget][WijSplitter]WijSplitter collpases, when resizing the browser window.

C1GridView
	[113911] [Asp.Net Wijmo][C1GridView]Unlike previous build 207, "not implemented" javascript error is observe after setting "ScrollMode" to "Auto" without setting "Width" property.

C1EventCalendar
	[112255][Asp.Net Wijmo][C1EventsCalendar][ControlExplorer][Export]Created events can't be displayed in exported image of pdf file of C1EventsCalendar.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.208/4.0.20151.208)		Drop Date: 03/27/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.70

C1GridView
	[113453][ASP.Net Wijmo][C1GridView] Expand/Collapse cell and its data row are misaligned when expand child grid in hierarchical grid

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.207/4.0.20151.207)		Drop Date: 03/19/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.70

Control Explorer
	Add theme option when exporting eventscalendar.

C1GridView
	[112510][WebForms][C1GridView] Only one row is displayed in UpdatePanel when calling UpdatePanel.Update().
	Rows are not overlapped now if scrolling mode is used and the Height property is not set (related issue: #112382).
	[112636][Asp.Net Wijmo][C1GridView]"NullReferenceException" is observe after editing in the child grids in Hiererchical Grid when "AllowClientEditing" is set to true.
	[99740][ASP.Net Wijmo][C1GridView] JScript error occurs on grouping and paging when setting "EnableConditionalDependencies - True".

C1GridExtender
	[30381][ToolkitExplorer][C1GridExtender] Javascript error is observed after navigating to Custom paging and DataSources samples in ToolkitExplorer.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.206/4.0.20151.206)		Drop Date: 03/13/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.69

	Update with C1Report 2015v1 release(38).

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.205/4.0.20151.205)		Drop Date: 03/13/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.69

C1GridView
	[111680][ASP.Net Wijmo][C1GridView]Unlike previous build, extra spaces are displayed above and below of child grid when sorting or changing page number in Hierarchical Grid.
	[112113][WebForms][C1GridView] Can't filter datetime value by using "equals" expression.
	[111571][ASPNET Wijmo][C1GridView] Exception "Uncaught invalid id" occurs on collapsing the second child grid in Hierarchical Grid.
	[112397][ASP.Net Wijmo][C1GridView] Unlike previous build, JavaScript errors occur on double-clicking on a last row after columns virtual scrolling.

C1ChartNavigator
	[112284][ASP.Net Wijmo][C1ChartNavigator][JP]Unlike Eng build, "SeriesHoverStyles" link is available in the Smart Tag of C1ChartNavigator with the localized JP build

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.204/4.0.20151.204)		Drop Date: 03/10/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.68

C1ChartNavigator
	[111674][ASP.Net Wijmo][C1ChartNavigator]Unlike previous build, the range selector is not displayed although RangeMin and RangeMax properties are set.

C1EventCalendar
	[109162][Asp.Net Wijmo][C1EventsCalendar]Some incorrect behaviors are found in '3 Months' view after an event is deleted in 'Day' View

C1GridView
	[93586][ASP.Net Wijmo][C1GridView] Second-level detail grid is still disabled on setting "Enabled" and "DisplayVisible" properties in hierarchical grid.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.203/4.0.20151.203)		Drop Date: 03/05/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.64

C1ChartNavigator
	[106880][ASP.Net Wijmo][C1ChartNavigator]"Uncaught TypeError: undefined is..." is thrown on mobile mode.
	[106538][ASP.Net Wijmo][C1ChartNavigator]"OnClientUpdating" and "OnClientUpdated" events get fired although the selection range is not changed if RangeMin and RangeMax properties are not defined.
	[110212][ASP.Net Wijmo][C1ChartNavigator]The removed data series is displayed again in C1ChartNavigator after postback.
	[110235][ASP.Net Wijmo][Wijmo Widget][C1Chart]"BeforePaint()" events get fired 3 or 4 times in C1Chart binding with C1ChartNavigator if this event is cancelled.

C1EventCalendar
	[109113][Asp.Net Wijmo][C1EventsCalendar] Javascript error is occurred after changing Views of C1EventCalendar.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.202/4.0.20151.202)		Drop Date: 03/03/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.64

All controls
	Fix the issue that page flicks when browsing CE samples using chrome browser.
    Fix the issue that compatible mode gets turned on in IE when using C1GridView.

C1GridView
	[109385][ASPNET Wijmo][C1GridView]Unlike previous build, paging bar is invisible if scrolling mode is used.
	[109396][ASPNET Wijmo][C1GridView]Unlike previous build, error "The state information is invalid for this page and might be corrupted" is shown when child grid is expanded for second time in Hierarchical Grid.
	[109151][ASPNET Wijmo][C1GridView] Unlike previous build, Collapse/Expand icons of hierarchical grid are missing after dragdrop column header in parent grid.
	[109694][ASP.Net Wijmo][C1GridView][ASP.Net Wijmo][C1GridView] Unlike previous build, Javascript error occurs when setting "FreezingMode" property in the grid and scrolling mode is turned off.
	[109706][ASPNET Wijmo][C1GridView] Unlike previous build, child grid width is reduced to very small size after parent row is expanded in Hierarchical Grid.
	[109698][ASPNET Wijmo][C1GridView] Unlike previous build, setting Enabled property does not consistently work in Hierarchical Grid.
	[109693][ASPNET Wijmo][C1GridView] Unlike previous build, extra spaces are displayed above and below of child grids in Hierarchical Grid.
	[101418][ASP.Net Wijmo][C1GridView] Filter row and vertical freezing bar is not shown properly when setting "VirtualizationSettings.Mode" property to Rows.

Control Explorer
	[106043][ASP.Net Wijmo][C1GridView][ControlExplorer] Exception occurs on navigating to last page when last row of current page is in edit mode.
	[110121][Asp.Net Wijmo][C1EventsCalendar][ControlExplorer] When 'Midnight/ Rocket/ Metro Dark' theme is applied, label text are invisible in EventDialogTemplate of 'Custom Edit Event' sample.

C1Menu
	[109700][ASP.NET Wijmo][C1Menu] Can't use elements over the disabled menu.

C1ChartNavigator
	[106321][ASP.Net Wijmo][C1ChartNavigator]The data is not displayed and the range value is changed after page is postback
	[106724][ASP.Net Wijmo][C1ChartNavigator]Request to remove "Themes" dropdown and "Create new theme..." link from its Smart tag
	[110073][ASP.Net Wijmo][C1Chart]"Animation" property does not work in C1Chart if it is binding with C1ChartNavigator.
	[106471][ASP.Net Wijmo][Wijmo Widget][C1ChartNavigator]Unlike C1Chars,the Japanese text are not displayed in C1ChartNavigator.
	[106534][ASP.Net Wijmo][Wijmo Widget][C1ChartNavigator]The selection of range can be changed in the disabled C1ChartNavigator.
	[106799][ASP.Net Wijmo][C1ChartNavigator]Unlike C1Charts, C1ChartNavigator cannot data bind with datasource.

Export
	[106149][ASP.Net Wijmo][C1GridView][Export] Only first row is exported to Excel, PDF and CSV files when setting 'VirtualizationSettings' and 'CallbackSettings' properties

C1Slider
	[101004][ASP.Net Wijmo][C1Slider] Increment and Decrement buttons are not displayed.

C1EventCalendar
	[106771][ASP.Net Wijmo][C1EventsCalendar] Error occurs when edited event is deleted after editing one occurance of repeated event and repeated event is changed to none.
	[107145][Asp.Net Wijmo][C1EventsCalendar] C1EventsCalendar's views that are added or deleted at run time are not retained when postback occur.
	[110032][Asp.Net Wijmo] In Custom view ['2 Weeks'], inconsistent behaviour is observed when clicking 'today' button after navigating other date.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20151.201/4.0.20151.201)		Drop Date: 02/11/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20151.64

C1Chart
	Add C1ChartNavigator control.

C1EventCalendar
	Add custom views support to C1EventCalendar.

C1GridView
	[101254][ASPNET Wijmo][C1GridView]Unlike previous build, error "The state information is invalid for this page and might be corrupted." is shown when "Edit" link is clicked for second time in child grid in Hierarchical Grid.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.200/4.0.20143.200)		Drop Date: 02/10/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.63

C1Menu
	Fix an issue that set Target property in xml file, but it is useless when binding the xml file to c1menu.

C1GridView
	[98371][C1GridView] RowCommand event does not fire from C1ButtonField/C1TemplateField.
	[102238][ASPNET Wijmo][C1GridView] Unlike previous build, row data get lost when child grid is expanded after edit link is clicked in Hierarchical Grid.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.199/4.0.20143.199)		Drop Date: 01/19/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.61

ControlExplorer
	New sample added: C1GridView\Columns Virtual Scrolling.

C1GridView
	[101263][ASPNET Wijmo][C1GridView] Unlike previous build, parent grid is not refreshed when child grid is expanded in Hierarchical Grid.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.198/4.0.20143.198)		Drop Date: 01/06/2015
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.61

C1Chart
	[100323][Wijmo Widget][C1Chart]Unlike previous build,the alignment lost in axis.annotation if "valuelabel" method is used once.

C1LineChart
	[100444][ASP.Net Wijmo][Wijmo Widget][C1LineChart]Unlike previous build,the data series and legend items become inconsistent after "FitType" option is changed with trendline.

C1Splitter
	[95504][ASP.Net Wijmo][C1Splitter]Unlike previous build,the whole page is frozen and cannot access to controls when making C1Splitter invisible.

C1GridView
	[78730][ASPNET Wijmo][Wijmo Widget][C1GridView][Bootstrap] Unlike previous build, DateTime data type columns get lost alignment.
	[99815]NullReference Exception is raised on filtering an empty C1GridView.

C1Accordion
	[96934][Wijaccordion] Wijaccordion does not get resized on setting width to 100%.

All Controls
	Fix an issue that an exception is thrown when running page with any C1 control and its Visible property is set to false.

C1Menu
	[99708][ASP.Net Wijmo][C1Menu] 'Disabled' option of 'SlidingAnimation'  do not work

C1ComboBox
	[99741][ASP.Net Wijmo][C1ComboBox]Unlike previous build,the dropdown list can be opened after hiding the whole C1ComboBox with Item Template.

C1TreeView
	[99589][ASPNET Wijmo][C1TreeView][wijtree] Unlike previous build, checkboxes are not displayed in Bootstrap Style

C1EventsCalendar
	[99639][ASPNET Wijmo][C1EventsCalendar] Unlike previous build, Javascript error[ Unable to get value of the property 'value': object is null or undefined] is thrown on clicking the data bound events

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.197/4.0.20143.197)		Drop Date: 12/23/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

C1GridView
	Fix an issue that cannot export the data when exporting the grid to pdf.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.196/4.0.20143.196)		Drop Date: 12/22/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

C1GridView
	[95297] [Asp.Net Wijmo][C1GridView] Javascript error is observe after opening second child grid and clicking on any cell of first child grid when Callback setting is set and "ClientSelectionMode" is set to single or multiple column
	[95214] [Asp.Net Wijmo][C1GridView] Multiple selection and editing can be done in child grid although multiple selection is not set when Callback setting is set and AutoGenerateSelectButton is to true.
	[95153] [Asp.Net Wijmo][C1GridView] Filtering is not applied in child grid when "CallBackSetting" is set in Hierarchical Grid.
	[93963] [Asp.Net Wijmo][C1GridView][Hierarchical Grid] Exception messagebox is display when sorting the column of child grid in Hierarchical Grid.
	[95216] [Asp.Net Wijmo][C1GridView] Javascript error is observe when Callback setting is set and opening a child grid and clicking on select button in Hierarchical Grid.
	[94850] [ASP.Net Wijmo][C1GridView] Error message shows on expanding second child grid when setting "AutoGenerateSelectButton - True" in child grid.
	[94844] [ASP.Net Wijmo][C1GridView] C1GridView hangs and Internal Server Error occurs on collapsing the child grid for second time in Hierarchical Grid.
	[95526] [ASP.Net Wijmo][C1GridView] First level detail grid do not remove its child grids properly using "Remove" or "Clear" methods.
	[94885] [ASP.Net Wijmo][C1GridView][IE11] "Member not found" error occurs for hierarchical grid when browsing with compatibility view mode.
	[94414] C1GridView rendering incorrect decimal values in a system having Italy locale settings, when applying DataFormatString to a column.
	[94337] [ASPNET Wijmo][C1GridView] Unlike previous build, frozen column and row position is always reset after clicking the "Edit" link.
	[95418] [ASP.Net Wijmo][C1GridView] Band column header text displays beyond the border with IE 11 compatibility view.

Wijmaps
	Add culture support for wijmaps.

Controls
	[97505]Missing resources error occurs when Wijmo controls are inherited
	Add conditional dependencies support in c1gridview.

WebConfigWorker
	[91785][ASP.NET Wijmo] Server side performance is so slow than built-in controls
	
ControlExplorer
	[98472]Replace the UpdatePanel with native callback in C1GridView Filtering sample of CE.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.195/4.0.20143.195)		Drop Date: 12/09/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

Controls
	Integrate all JP/Chinese resources to controls.

C1GridView
	[95370][Asp.Net Wijmo][C1GridView] doRefresh() causes loss of formatting for C1TemplateField.	
	[95238][ASPNET Wijmo][C1GridView] Unlike previous build, scroll position is not maintained when clicking the dropdown button of C1Input controls.
	[94310][ASP.Net Wijmo][C1GridView] Request to remove some duplicated properties from C1GridView properties window.
	[94665][ASP.Net Wijmo][C1GridView] "ScrollingSettings" properties are not affected in grid when setting from properties window.
	[94312][ASP.Net Wijmo][C1GridView] Property descriptions are not mentioned properly for "ScrollingSettings" and "VirtualizationSettings" properties.
	The possibility to fix rows and columns in hierarchical grid is now turned off (in the same manner as for the grid which uses column grouping). Related issues: 95454, 95409.
	The possibility to merge columns in hierarchical grid is now turned off (in the same manner as for the grid which uses column grouping). Related issue: 94842.
	[95442][ASP.Net Wijmo][C1GridView] Data rows are misaligned in a scrollable grid and vertical scrollbar is not shown when setting "CallbackSettings.Action" to "All" in hierarchical grid.
	Hide Exporting CallbackSettings-Action.
	[96679][Asp.Net Wijmo][C1GridView] C1GridView does not update when multiple controls are used in EditTemplate with CallBackSetting.
	[97546][ASP.Net Wijmo][C1GridView]In client editing, when editing an empty data is a set numeric item in "NaN" is displayed.
	An exception is thrown when calling the client-side "update" method if datasource is empty.

C1Dialog
	[94810][ASPNET Wijmo][C1Dialog][WijDialog][Knockout] Unlike previous build, Javascript error[Unable to get value of the property 'attr': object is null or undefined] occurs on creating Dialog

C1Charts
	[83637][ASP.Net Wijmo][Wijmo Widget][C1Chart]Uncaught TypeError "Cannot read property data..." is thrown when a data series is added after adding empty data series in WijChart.
	[94690][ASPNET Wijmo][C1PieChart][wijpiechart] Unlike previous build, setting "disableDefaultTextStyle" option does not work

C1BarChart
	[94329][ASPNET Wijmo][C1BarChart][WijBarChart][Charts] Unlike previous build, UI is not correctly displayed in disabled state.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.194/4.0.20143.194)		Drop Date: 11/21/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

Controls
	Integrate JP resources to controls.

C1GridView
	[79165][ASPNET Wijmo][C1GridView] Unlike previous build, numeric value is not correctly displayed in edit mode when AllowC1InputEditors = True and AllowVirtualScrolling = True.
	[79648][ASPNET Wijmo][C1GridView] Unlike previous build, Two InputDate dropdown buttons are shown in edit mode when AllowVirtualScrolling = True.
	[83487][ASPNET Wijmo][C1GridView] Unlike previous build, Edit mode is stopped when frozenbar is dragged.
	[93407][Asp.Net Wijmo][C1GridIView] Column resizing is not working in child grid when Hierarchical Grid is applied.
	[73562][ASP.Net Wijmo][C1GridView] Multiple issues are observed when export scrollable grid with static rows and columns to Excel formats
	Hierarchical grid: client-side editing doesn't work for the child grids.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.193/4.0.20143.193)		Drop Date: 11/13/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

C1EventsCalendar
	[78524][Wijmo Widget][ASP.Net Wijmo][wijevcal][Touch] Unlike previous build, Horizontal scrollbar is appeared when timeslot area is hovered which cause the bottom-most timeslot cannot be clicked

C1GridView
	[94176][ASP.NET Wijmo][C1GridView] Double byte space on the edge of the data does not display.
	[79066][ASPNET Wijmo][C1GridView] Unlike previous build, frozenbar cannot be dragged to bottom.
	
C1LineChart
	[94167][Wijmo Widget][WijLineChart] Unlike previous build, Axis Text cannot be retrieved by class name.

C1Combobox
	[94007][ASPNET Wijmo][C1ComboBox][wijcombobox] Unlike previous build, setting dropdownWidth option does not work

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.192/4.0.20143.192)		Drop Date: 11/12/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

	Update C1Report from .54735 to .54737.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.191/4.0.20143.191)		Drop Date: 11/12/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

	Update shared licensing(make aboutbox shown better).

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.190/4.0.20143.190)		Drop Date: 11/11/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.59

Controls
	Fixed an issue that an exception is thrown when postback a page which contains ScriptManager in chrome.

C1Menu
	[91296][Asp.Net Wijmo][C1Menu]Unlike previous build 181, inconsistent behaviour is observed when showing menu items with different template

C1FileExplorer
	[93991][Wijmo Widget][WidgetExplorer][WijFileExplorer]Unlike previous build 56, Javascript error occur when navigating to wijfileexplorer page then navigating to other samples pages
	[92440][Wijmo Widget][WijFileExplorer] "destroy" method is not working in wijfileexplorer

C1AppView
	[44470][Mobile Controls][C1AppView] "Enabled" property is not working in C1AppView.

ControlExplorer
	[92747][ASP.Net Wijmo][ControlExplorer][C1BubbleChart]Error "Invalid value for <circle>..." is shown on reloading the data in "Series Transition" sample.

C1Dialog
	[93961][Asp.net Wijmo][C1Dialog]JavaScript error is observed when 'DisplayVisible' property is used at runtime.

C1Tab
	[93965][ASPNET Wijmo][C1Tabs] Unlike previous build , Javascript error[Unable to get value of the property 'height': object is null or undefined] occurs after postback.

Adventurework
	[67210][Asp.Net Wijmo][AdventureWorks][Sample] Script Error is occurred when Color is changed and mouse is placed over the image of a bike.

C1GridView
	[93949][ASP.Net Wijmo][C1GridView] Visual Studio stop responding when binding C1GridView to a Data Source at Design-time.
	Type of the C1GridView.Detail property is changed to collection to avoid design-time errors due to recursive initialization.
	[93973][ASP.Net Wijmo][C1GridView] Unlike previous build, keyboard navigation is not working for horizontal scrolling.
	[93955][ASP.Net Wijmo][C1GridView] Unlike previous build, Detail grids are not expanded on setting "StartExpanded" property to True at run-time.
	[93322][ASP.Net Wijmo][C1GridView] Unhandled exception occurs when sorting last column of first child table in a "Hierarchy" sample.
	[93959][Asp.Net Wijmo][C1GridView][Hierarchical Grid] Unlike previous build 188, a blank row is increased by one whenever collapsing and expanding the last row icon in parent grid when callback setting is set and ShowFooter is set to "True".

C1Editor
	[93977][ASPNET Wijmo][C1Editor][wijeditor] Unlike previous build, Javascript error[cannot call methods on wijtabs prior to initialization; attempted to call method 'destroy'] occurs when mode option is set to "bbcode"

C1Gallery
	[91736][49175]Thumbnails for C1gallery shows top left corner of an image instead of complete image.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.189/4.0.20143.189)		Drop Date: 11/7/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.58

C1TreeMap
	[93599][ASP.Net Wijmo][C1TreeMap]Request to provide VS Toolbox icon for C1TreeMap
	[89428][ASP.Net Wiijmo][C1Maps][C1TreeMap]Unlike the other controls,C1Map control UI is not properly shown and cannot select it at design view unless size is defined

All controls
	Update the new licensing for controls.
	[93535][Asp.Net Wijmo][C1Controls][3.5 dll] Unlike previous build 186, JavaScript error " Unable to set property ..." occurs when ScriptManager is placed  with C1Controls on the same page

Control Explorer
	[65157][Asp.Net Wijmo][C1ComboBox][Control Explorer]JavaScript runtime error: Unable to get property 'value'... is occurred when clicking the dropdown button after the selected item is clear in "Select" sample

Drag feature
	[91148][ASP.Net Wijmo][Drag Feature][Touch Environment]  Drag feature is not working in touch environment

C1FlipCard
	[93374][Asp.Net Wijmo][C1FlipCard] C1Flipcard become invisible when 'enabled' property is set as 'false'

Hospital_One
	[92750][Asp.Net Wijmo][MVC][Hospital_One Sample] Unhandled exception occurs when running 'Hospital_One' sample

C1Dialog & C1Tooltip & C1Menu
	[93413][Asp.net Wijmo][C1Dialog][C1Tooltip][C1Menu]'DisplayVisible' property does not work

C1ReportViewerMVC
	[91064][ASP.Net Wijmo][HowTo][C1ReportViewerMVC] JavaScript runtime error occurred when C1ReportViewerMVC sample is run

C1FileExplorer
	[92365][Wijmo Widget][WijFileExplorer]Multiple selection is not working in Thumbnail view when it is set at run time in wijfileexplorer
	[92362][Wijmo Widget][WijFileExplorer][IE]Newly created folders are not display/refresh and deleted folders/files are still display in wijfileexplorer when browsing with IE
	[92769][Asp.Net Wijmo][C1FileExplorer] Unlike previous build, Column resizing does not work correctly and row height is doubled after increasing the width of a column
	[93646][ASP.Net Wijmo][C1FileExplorer][FireFox]Unlike previous build, Server error is shown on clicking the image in detail view

C1BubbleChart
	[92643][Wijmo Widget][WijBubbleChart]Unlike previous build,JavaScript error is observed on changing data in WijBubbleChart.

C1CandlestickChart
	[92818][Asp.Net Wijmo][C1CandlestickChart] Unlike previous build, labels are not displayed correctly in c1andlestickchart if used bounded data

C1TreeView
	[92587][ASPNET Wijmo][Wijmo Widget][WijTreeView] JavaScript runtime error occurs after node dragdrop action is performed at second time between two WijTreeView

C1CompositeChart
	[92582][Wijmo Widget][WijCompositeChart]The data series and annotation are changed on changing some properties in WijCompositeChart with Candlestick Chart type
	[93361][community][wijcompositechart] Tooltip is not shown for the Line Chart when Line is over a Bar Chart.

C1ToolTip
	[91247][Asp.Net Wijmo][C1ToolTip]Property settings at runtime are not retained after postback is occurred

C1Editor
	[85126][Wijmo Widget][WijEditor]Cursor lose focus on 'Insert Hyperlink' dialog box.
	[86982][Asp.Net Wijmo][C1Editor]Server Error 'System.Web.HttpException: Failed to load viewstate.' is occurred when 'ShowFooter' is set as 'false' in disbaled state of editor
	[82961][ASPNET Wijmo][C1Editor][IE11] Unlike previous build, C1Editor does not work when placed inside C1Splitter
	[73709][Asp.Net Wijmo][C1Editor][IE] Unlike previous build 160, C1Editor hang after adding images inside the content and clicking on "FullScreen" mode
	[91704][ASPNET Wijmo][C1Editor] Unlike previous build, added control UI are automatically transformed into HTML Tags after postback

C1GridView
	[86761][ASPNET Wijmo][C1GridView] Unlike previous build, Width property does not work in column.
	[69279][Asp.Net Wijmo][C1GridView][ControlExplorer] "System.Web.HttpRequestValidationException" is thrown on moving column in "GridView -> Groupping -> Totals" sample.
	[92585][ASPNET Wijmo][C1GridView] Unlike previous build , Html tag <input /> is displayed when click Edit link in a callback mode.
	[93208][ASP.Net Wijmo][C1GridView]Unlike previous build,Visual Studio becomes not responding on placing C1GridView onto the page.
	[93378][Asp.Net Wijmo][C1GridView]Javascript error is observe after changing the page and expanding the child grid in hierarchical grid when CallBack setting is applied.
	[93412][ASP.Net Wijmo][C1GridView] Hierarchical grid is able to expand or collapse child tables even when "Enabled" property is set to false.
	[93571][ASP.Net Wijmo][C1GridView] Data records of Detail grids are not selected on clicking "Select" button in hierarchical grid.
	[93516][ASP.Net Wijmo][C1GridView] Pager of child grid is overlapped over parent grid when scrolling is enabled in hierarchical grid.

C1SuperPanel
	[88570][Asp.Net Wijmo][C1SuperPanel]Javascript error is observed when 'DisplayVisible' property is set as false on setting 'AutoRefresh' property to true.

C1Menu
	[79757][Asp.Net Wijmo][Wijmo Widget][WidgetExplorer][ToolkitExplorer][C1Menu] Javascript error occur when clicking "previousPage" button  of "Functions" sample in "C1Menu" control

C1Charts
	[93316][ASP.Net Wijmo][Wijmo Widget][C1BubbleChart] Unlike previous build, alignment is lost when CSS style is applied.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.188/4.0.20143.188)		Drop Date: 10/31/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.56

C1FileExplorer
	[92708][Asp.Net Wijmo][C1FileExplorer]Unlike previous build, Some icons in Header pane is still working and context menu can be opened at header pane at disabled state in C1FileExplorer
	[92600][Asp.Net Wijmo][C1FileExplorer] Unlike previous build, folder path is not correctly displayed after folder is renamed in treeview pane or clicking "Refresh" icon
	[92595][Asp.Net Wijmo][C1FileExplorer][Bootstrap]Unlike previous build, Extra empty area is shown between Header Bar and Body area
	[92766][Asp.Net Wijmo][C1FileExplorer] Unlike previous build and standard contextmenu, horizontal scrolling is observed on right clicking at the rightmost in C1FileExplorer
	[92440][Wijmo Widget][WijFileExplorer] "destroy" method is not working in wijfileexplorer
	[92833][ASP.Net Wijmo][C1FileExplorer]Unlike previous build 178,files are not shown when setting 'Mode=FileTree'.
	[92786][Asp.Net Wijmo][C1FileExplorer] Unlike previous build, Firtst item is always selected in ContextMenu

C1ComboBox
	[91081][Asp.Net Wijmo][C1ComboBox]Unlike previous build, Selection is still shown when deleting by backspace key one by one after postback occur
	
C1CandlestickChart
	[90621][Wijmo Widget][WijCandlestickChart]Unlike WijBarChart , javascript error is observed when applying axis in C1CandlstickChart

C1GridView
	Hierarchical grid feature added.
	Fixed an issue that group row content is displayed as a HTML markup.
    The following properties are obsoleted and moved into the new ScrollingSettings property: AllowVirtualScrolling, FreezingMode, RowHeight, StaticColumnIndex, StaticRowIndex.

C1TreeView
	[46333][Bootstrap Style][C1TreeView] Selection is not shown when selecting an item or multiple items in C1TreeView

C1Dialog
	[92233][Wijmo Widget][WijDialog][IE] Can move focus to element in background when restoring modal dialog size.

C1EventsCalendar
	[77188][C1EventsCalendar] Color field binding does not work when C1EventsCalendar is bound to a database.

C1Editor
	[79065][ASPNET Wijmo][C1Editor] Unlike previous build, added objects are displayed outside of C1Editor after restoring the C1Dialog.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20143.187/4.0.20143.187)		Drop Date: 10/27/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20143.56

C1ReportViewer
	[57177][BootStrap Style][C1ReportViewer] Toolbar is transparent and selected tab is not displayed properly in Bootstrap style

C1Maps
	[89637][Asp.Net Wijmo][C1Maps][3.5 dll] When C1Maps is placed to web form,error message 'Fail to create designer' is observed at design time.
	[90250][ASP.Net Wijmo][C1Maps][BootStrap Style] Pan tool does not render and ‘Width’ of C1Maps is larger when applied bootstrap style.
	[90788][ASP.Net Wijmo][C1Maps] Unlike previous sample, geographical coordinates UI is not correctly aligned in Map samples.
	[90782][ASP.Net Wijmo][C1Maps] Unlike previous sample, empty maps is shown when running ‘VectorLayer’ sample
	[89428][ASP.Net Wijmo][C1Maps][C1TreeMap]Unlike the other controls,C1Map control UI is not properly shown and cannot select it at design view unless size is defined.
	[90188][Asp.net Wijmo][C1Maps] Zooming and panning actions cannot be done using keyboard in C1Mapsv
	[89429][ASP.Net Wijmo][C1Maps]The data can be zoomed or navigated in the disabled C1Map
	[91080][ASP.Net Wijmo][C1Maps] Unlike previous build, setting ‘Center’ property at run-time is not affected in C1Maps.

C1TreeMap
	[89603][ASP.Net Wijmo][C1TreeMap]The back button is not displayed although 'ShowBackButton' property is set in C1TreeMap
	[90718][ASP.Net Wijmo][C1TreeMap]The child items are disappeared in C1TreeMap after postback

C1ReportView
	[91064][ASP.Net Wijmo][HowTo][C1ReportViewerMVC] JavaScript runtime error occurred when C1ReportViewerMVC sample is run

C1TreeView
	[91175][ASPNET Wijmo][C1TreeView][WijTree] Unlike previous build, dim UI is still shown when "disabled" option is set to false

C1Gallery
	[63590][ASP.Net Wijmo][C1Gallery]'ShowControlsOnHover' property of Gallery control is still work although it is set as 'false' in default

C1Sparkline
	[72438][ASP.Net Wijmo][C1Sparkline][C1FlipCard]Request to provide Toolbox icons for new controls in VS Toolbox.

C1FlipCard
	[90170][Asp.Net Wijmo][C1FlipCard]Animation effect is still shown although 'Enabled' property is set as 'false' after postback.

C1FileExplorer
	[78228][ASP.Net Wijmo][C1FileExplorer] Current Address of AddressBar cannot get from "Address" property
	[88589][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, ContextMenu Default ShortCut and PopupWindowClose Custom ShortCut are not working properly
	[88597][ASP.Net Wijmo][C1FileExplorer]Unlike previous build, Server error[The resource cannot be found] occurs when loading jpg files in Thumbnail view
	[89942][Asp.Net Wijmo][C1FileExplorer][Bootstrap Style] Unlike previous build 182, folder name is truncated in Thumbnail view when applying Bootstrap Style
	[88698][Asp.NET Wijmo][C1FileExplorer] Unlike previous build, folder path is removed when filter is cleared
	[90745][Asp.NET Wijmo][C1FileExplorer] Unlike previous build, typed text in the filter cannot not be cancelled by pressing Ctrl + Z at once
	[90755][ASP.NET Wijmo][C1FileExplorer]Unlike previous build, Context Menu cannot be cancelled on pressing Esc key
	[90793][Asp.Net Wijmo][C1FileExplorer]Unlike previous build 181, javascript error is observe after setting "Mode" to "FileTree" when ViewPaths or InitPath is not set
	[90807][Asp.Net Wijmo][C1FileExplorer]Unlike previous build 181, folder cannot be open with one click on "Open" toolbar or context menu when "Mode" is set to "FileTree"
	[91077][ASP.Net Wijmo][C1FileExplorer]Unlike previous build 183,postback occurs by pressing 'Enter' key in 'FilterTextBox'.
	[91200][Asp.Net Wijmo][C1FileExplorer]Unlike previous build, Filter texts cannot be correctly deleted by pressing 'Delete' key.

C1Accordion
	[86490][Asp.Net Wijmo][C1Accordion] Unlike previous build, JavaScript error is shown when C1Linechart is inserted in first pane of C1Accordion

C1Slider
	[89349][ASPNET Wijmo][C1Slider] Unlike previous build, control UI is not shown

C1Charts,C1Gauges
	[90804][ASP.Net Wijmo][C1Chart]Visual Studio becomes crash after removing its width value in C1Chart

C1GridView
	[90982][Asp.Net Wijmo][C1GridView] "&" is shown as "&amp" in C1GridView.
	[90115][Asp.Net Wijmo][C1GridView] Error occurs when C1GridView containing AJAX Toolkit control in a template column is placed inside the UpdatePanel and bound again.
	[88703][ASP.Net Wijmo][C1GridView] Group area and paging row are still visible in grid on setting "DisplayVisible" property to false.
	[78760][ASP.Net Wijmo][C1GridView] Unlike previous build, script error occurs on scrolling the rows using keyboard when "VirtualScrolling" is enabled.

ControlExplorer
	[83677][ASP.Net Wijmo][ControlExplorer][C1CandleStick]Legend icon does not show in C1CandleStickChart.
	[73983][Asp.Net Wijmo][C1GridView][Control Explorer] Exception message appears when clicking "previous page" button in Custom Paging of "Paging" sample.

C1EventsCalendar
	[88701][ASP.Net Wijmo][C1EventsCalendar] Unlike previous build, when one of the repeated events is moved, event is displayed at both source and target.
	[90082][Customer Support][WijEvCalThe event is drag dropped to the new date, even if an error occurs while performing the drag drop operation in the Month View.
	
C1Inputs
	[89105][ASPNET Wijmo][C1Input] Unlike previous build, control UI is not shown
	
C1BarChart
	[90595][Wijmo Widget][WijBarChart] Unlike previous build, first data item of valueLabels binding is not shown.
	
C1Editor
	[20374][Asp.Net Wijmo][Wijmo Widget][C1Editor]Unable to Merge cell on vertical cells of table inside C1Editor.
	[90294][CustomerSupport][WijEditor]Browser becomes unresponsive for a while after pasting HTML content in WijEditor.
	[89199][ASPNET Wijmo][C1Editor][WijEditor] Unlike previous build, "Font Size" combo dropdown items are not shown if mode is set to bbcode
	[90627]'Next page' link does not work properly when 'Preview' dialog box is re-opened.

C1ComboBox
	[88545][ASPNET Wijmo][C1ComboBox]Unlike previous build, selected property 'true' does not work when 'SelectionMode' is set 'Multiple' if c1comboboxItem is blank item

C1Charts
	[89023][Community][WijAccordion]Wijmo charts not getting rendered in WijAccordion, if the chart data is returned through a callback service and the click event of chart is handled.
	[46548][WijmoWidget][WidgetExplorer][WijBubbleChart][WijBarChart] The data series and plot area are not display properly after setting 'Orientation' to horizontal and 'Compass' to 'east' or 'west'.
	[89954][ASP.Net Wijmo][C1LineChart]JavaScript runtime error is occurred when chart is data bound dynamically

C1Tabs
	[46263][Asp.Net Wijmo][Wijmo Widget][Bootstrap Style][C1Tabs] C1Tab contents display below tab strip on setting ‘Alignment’ property to ‘Left’ or ‘Right’

C1Calendar
	[47649][Wijmo Widget][Bootstrap Style][C1Calendar] Preview buttons are not able to see if use 'BootStrap' style

C1TreeView
	[90827][ASPNET Wijmo][Wijmo Widget][WijTreeView] Javascript error occurs after node dragdrop action between two WijTreeView.

C1Dialog
	[61683][Wijmo Widget][Bootstrap Style][C1Dialog] Scrollbars are not displayed when ‘BootStrap’ style is set in C1Dialog

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.186/4.0.20142.186)		Drop Date: 10/15/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.55

C1ReportViewer
	[83813][C1ReportViewer] The text of button in the log dialog is Japanese in Chinese OS.

C1Slider
	[88522][Asp.Net Wijmo][Wijmo Widget][C1Slider]Client side "start" event of C1Slider raised twice when the "Range" property is set to True

C1Editor
	[20374][Asp.Net Wijmo][Wijmo Widget][C1Editor]Unable to Merge cell on vertical cells of table inside C1Editor.

C1ComboBox
	[89426][Asp.NET Wijmo][C1ComboBox][WijComboBox][Knockout] Unlike previous build, changes in "selectedIndex" property is not correctly updated to the underlying data source.

C1CompositeChart
	[90298][Asp.Net Wijmo][C1CompositeChart] Min/Max of X axis changes on postback in C1CompositeChart.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.185/4.0.20142.185)		Drop Date: 10/13/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.55

C1QRCode
	[90222]Make QRCode samples for CE.

C1FileExplorer
	[89941][Asp.Net Wijmo][C1FileExplorer][Bootstrap Style] Unlike previous build 182, the first item is always selected unintentionally although user selected other file or folder.
	[89942][Asp.Net Wijmo][C1FileExplorer][Bootstrap Style] Unlike previous build 182, folder name is truncated in Thumbnail view when applying Bootstrap Style.
	[89150][Asp.NET Wijmo][C1FileExplorer] Unlike previous build, search text in the filter cannot not be cancelled by pressing Esc key.
	[88778][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, left pane of TreeView is disappear after resizing the browser and selecting a file.
	[88682][Asp.Net Wijmo][C1FileExplorer][Bootstrap] Unlike previous build, Extra empty area is displayed between Header Bar and Body area.
	[88774][Asp.Net Wijmo][C1FileExplorer][BootStrap] Unlike previous build, Context Menu UI gets lost after open it for a few times.
	[88641][Asp.Net Wijmo][C1FileExplorer] Unlike previous build, Context Menu is still displayed although column header is clicked.
	[88512][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, when selected folder is changed in TreeView, DetailView or ThumbnailView is not refreshed if multiple files/folders are selected
	[88578][ASP.Net Wijmo][C1FileExplorer] [Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, Javascript error is observe after selecting multiple files and deselecting all the files in Thumbnail view
	[88518][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, ExpandCollapse Triangle does not show/hide properly on child folder creating/deleting
	[88534][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 182, toolbar icons are not visible with default theme "Aristo"

C1CompositeChart
	[85258][ASP.Net Wijmo][Wijmo Widget][WijCompositeChart]Unlike previous build, the whole SharedPie chart is disappeared after adding Candlestick chart in C1CompositeChart

C1CandlestickChart
	[87958][Wijmo Widget][WijCandlestickChart]Unlike previous build 48, the annotation value are changed and trendline is not displayed after applying legend/header/series styles in WijCandlestickChart.

C1BarChart & C1CompositeChart
	[89807][Asp.Net Wijmo][C1CompositeChart]Extra space generated between the YAxis and the bar on the first data point of the chart if 17 or more values are plotted on the chart.

C1TreeView
	[88547][Asp.Net Wijmo][C1TreeView][WijTree]Fixed an issue that WijTree remain enabled even though disabled option is set to true.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.184/4.0.20142.184)		Drop Date: 09/29/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.54

C1Sparkline
	[77289][ASP.Net Wijmo][C1Sparkline]Animation.Enabled property does not take effect to C1Sparkline

C1Carousel, C1Gallery, C1Slider
	[85273][ASP.Net Wijmo][C1Carousel][C1Gallery][C1Slider]DisplayVisible property does not work properly

C1Controls
	[87292][ASP.Net Wijmo]Error is occurred when new theme 'extensions' is selected from smart tag

C1ComboBox
	[78768][Asp.Net Wijmo][C1ComboBox] Unlike previous build 165, Javascript error is observe after navigating with keyboard when "AutoPostBack" and "SelectOnItemFocus" are set to true
	[77798][ASP.NET Wijmo] C1ComboBox's litems be cleared in UpdatePanel

C1Inputs
	[86488][ASP.Net Wijmo][C1Input] C1Input controls are not invisible although DisplayVisible is set as "False"

C1CompositeChart
	Fixed an issue that if there are more than 2 pies in "sharedPie" type serie, JS error will occur when click the legend.
	[77421][ASP.Net Wijmo][C1CompositeChart]Error"Index was out of range. Must be non-negative..." is thrown when data binding in C1CompositeChart with SharedPie type and other type

C1Charts
	[83859][Community] Export Chart not working after adding SpreadJS.

C1FileExplorer
	[77582][ASP.Net Wijmo][BootStrap][C1FileExplorer]Unlike Listview,selection mark is not shown in Thumbnails View and TreeView in C1FileExplorer.
	[87035][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, page selected is not retain when deleting a file or creating a folder.
	[86839][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, toolbar button are still working although setting "Enabled" property to false in C1FileExplorer.
	[88698][ASPNET Wijmo][C1FileExplorer] Unlike previous build, folder path is removed when filter is cleared

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.183/4.0.20142.183)		Drop Date: 09/24/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.53

C1ReportViewer
	[85008][ASP.Net Wijmo][C1Editor]Dropdown button of table in 'Insert' tab is disappeared when 'arctic' theme is used

C1Charts
	[87046][ASP.Net Wijmo][C1Chart]Error "Index was out of range..." is thrown if the design time added data series count is larger than the count of binding data series in C1Chart

C1Editor
	[79030][ASPNET Wijmo][C1Editor] Unlike previous build , typed text is not shown after asynchronous postback occurs

C1Upload,C1BinaryImage
	[78294][ASP.Net Wijmo][C1Upload][C1BinaryImage] About Box is not appeared when C1Upload or C1BinaryImage is added to the form although license is not applied

C1QRCode
	Add the new control: C1QRCode control.

C1ComboBox
	[85170][ASP.Net Wijmo][C1ComboBox]Unlike previous build , the template items are not displayed in C1ComboBox with validation

C1ListView
	[86892][ASP.Net Wijmo][C1ListView] JavaScript error occurs at run-time when C1ListView is simply placed on the page.

C1Controls
	[87259][ASP.Net Wijmo][C1Controls]Unlike previous build 181,C1Controls tags entry are not added into source view when C1Controls are placed inside UpdatePanel at design view
	[87262][ASP.Net Wijmo][C1Controls]Error "The control collection cannot be modified..." is occurred at design view if C1Controls is used inside UpdatePanel

C1ThemeRoller
	[87133][ASP.Net Wijmo][C1ThemeRoller]Controls are distorted when custom theme is created by using ThemeRoller

C1Accordion
	[87140][ASP.Net Wijmo][C1Accordion]Server error is observed when Accordion is binding
	[87394][Asp.Net Wijmo][C1Accordion][C1Input][C1Wizard][C1Expander]Unlike Previous build 181,Javascript error is observe after AutoPostback is set to true when setting script manager is place on the same page

Controls and Extenders
	[87167][ASP.Net Wijmo][C1ComboBoxExtender] Unlike previous build, Javascript error[Unable to get property 'addCultureInfo' of undefined or null reference] occurs if bounded to C1DataSource
	[86716][ASPNET Wijmo][C1GridView][Bootstrap] Unlike previous build, Server side error[Can not find the 'C1.Web.Wijmo.Controls.Resources.themes.Custom.jquery-wijmo.css' resource.'] occurs if Theme = "Custom"

Charts, C1Sparkline, C1EventCalendar and Gauges Designer
	[86708][ASP.Net Wijmo][C1Chart][C1Gauge][C1Sparkline][C1EventCalendar]Unlike previous build,Error "Object reference not set to..." is shown and the control's UI are not displayed at design time
	[86979][ASP.Net Wijmo][C1Chart]Unlike previous build , all data series cannot be removed from the 'Series List Collection' window

C1AutoComplete
	[86592][Asp.Net Wijmo][C1AutoComplete] 'Page Load' event is fired when a character is entered in an empty C1AutoComplete

C1FileExplorer
	Make filtering in a case-insensitive way.
	[86780][Asp.Net Wijmo][ControlExplorer][C1FileExplorer] Unlike previous buid 181, Javascript error is observe after browsing "Client Side Events" sample page
	Rename the Shortcuts.FocusGridPagingSlider to Shortcuts.FocusPager.
	[86734][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, "The directory is not empty" alert box is display when deleting a folder which contain a file
	[86710][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, "System.Index Out of Range Exception" is observe after placing C1FileExplorer on the form and browse it
	[86857][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, only one file is deleted although selecting multiple files to delete by using context menu
	[86838][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, Javascript Error is observe after pressing "Esc" key in C1FileExplorer.
	[87090][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, cursor is lost from FilterTextBox when files/folders are filtered inside subfolder.
	[87159][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, warnning message doesn't show when occuring the same name in creating New Folder.
	[87033][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, two files are always dragged although only one files is selected to drag
	[87302][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, the tree nodes get lost in C1FileExplorer after post back while the sub tree node gets selection.
	[86839][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, toolbar button are still working although setting "Enabled" property to false in C1FileExplorer
	[87035][Asp.Net Wijmo][C1FileExplorer] Unlike previous build 181, page selected is not retain when deleting a file or creating a folder.
	[87104][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, the sub folders in the treeview pane are disappeared after refresh C1FileExplorer
	[87108][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, more than 2 files/folders cannot be selected using Ctrl key + Click in Detail View of FileExplorer
	[87119][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, resizing arrow is lost to resize Name/Size columns in Detail view.
	[87147][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, the selected folder name is not shown in the textbox when renaming
	[87160][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 181, TreeView is disappeared after dragging and dropping file into level 2 subfolder.
	[86840][Asp.Net Wijmo][C1FileExplorer][Bootstrap Style]Unlike previous build 181, Bootstrap style is not applied in toolbar of C1FileExplorer
	[77418][ASP.Net Wijmo][C1FileExplorer]Some visible control does not contain in C1FileExplorer's SmartTag.
	[77384][ASP.Net Wijmo][C1FileExplorer]Cannot remove filter by clicking delete button in filter text box.

C1GridView
	[84227][Asp.Net Wijmo][C1GridView]AllowClientEditing for C1CheckBoxField does not really make CheckBox editable client side.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.182/4.0.20142.182)		Drop Date: 09/11/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.53

ControlExplorer
	[69304][Asp.Net Wijmo][ControlExplorer][C1SuperPanel] JavaScript error occurs on clicking "Table of Contents" links in "Content Navigation" sample
	
C1FileExplorer
	[83842][ASP.Net Wijmo][C1FileExplorer]Folder path is inserted in 'FilterTextBox' when doing visiblecontrols of this textbox on/off .
	[77868][ASP.Net Wijmo][C1FileExplorer] After selecting a file in Thumbnail View, vertical scrollbar is automatically scrolled down and the last file is opened instead of selected file when "Enter" is pressed
	[74126][Asp.Net Wijmo][C1FileExplorer][Control Explorer] Parser error occur when opening "Upload File" sample of C1FileExplorer with VS 2012 solution

C1Superpanel
	[83443][ASP.Net Wijmo][C1Superpanel] C1SuperPanel does not display correctly when scrolling with gesture and resizing the browser
	[83602][ASP.Net Wijmo][C1Superpanel] JavaScript error occurs on setting ‘DisplayVisible’ property to false

C1Charts
	[84173]Added Reverse property to Chart Legend.

C1Menu
	[72284][ASP.Net Wijmo][C1InputDate] Unlike previous build, boundary is not shown if C1InputDate is placed in the Template of C1MenuItem

C1ReportViewer
	[78767][ASP.Net Wijmo][C1ReportViewer][BootStrap Style] "continuous view" and "full screen" buttons from toolbar are not visible in Bootstrap style

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.181/4.0.20142.181)		Drop Date: 09/05/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.46

C1Wizard
	[83240][ASP.Net Wijmo][C1Wizard] Browser scrollbar scroll up to top when click 'next' or 'back' buttons of C1Wizard.

C1Slider
	[83242][Wijmo Widget][wijSlider] Although "disabled" option is set as true at page initialize or run time, increase or decrease button is not dim.

C1Video
	[83237][Wijmo Widget][wijVideo] When "disabled" option is set as true at page initialize , wijVideo's controls  are still working although video screen is dim

C1GridView
	[83009][Asp.Net Wijmo][C1GridView] C1GridView displays incorrect when ViewStateMode is disabled and any row is selected using 'Select' Command.
	
C1Superpanel&& C1SparkLine && C1ProgressBar
	[83333][wijProgressBar][wijPopUp][wijSparkLine][wijSuperpanel] Although "disabled" option is set as true at page initialize, widgets are not dim

C1Accordion	
	[79862][ASP.Net Wijmo][Wijmo Widget][C1Accordion]Header and content area is not consistent  when changing expend directions.

C1CandlestickChart
	[83047][Wijmo Widget][wijcandlestickchart][wijcompositechart]Unlike previous version 48, '0NAN' is display in X-axis when 'Trendline' is added in wijcandlestickchart.

C1CompositeChart
	[83047][Wijmo Widget][wijcandlestickchart][wijcompositechart]Unlike previous version 48, '0NAN' is display in X-axis when 'Trendline' is added in wijcandlestickchart.

ControlExplorer
	Update the web.config file to fix an issue that the samples can't run without .net framework 3.5.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.180/4.0.20142.180)		Drop Date: 08/26/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.46

C1Editor
	[83048][Wijmo Widget][WijDialog][WijEditor]Unlike previous version 48, "disabled" option is not working in wijdialog at run time.

C1Dialog
	[83048][Wijmo Widget][WijDialog][WijEditor]Unlike previous version 48, "disabled" option is not working in wijdialog at run time.

C1Upload
	[83178][Wijmo Widget][WijUpload]'Disabled' option do not work properly.

C1TreeView
	Fixed an issue that an exception is thrown when using C1TreeView.

Controls and Extenders
	Fixed an issue that an exception is thrown when using CDNDependencePaths property.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.179/4.0.20142.179)		Drop Date: 08/25/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.46

Controls and Extenders
	Add CDNDependencePaths property to all controls and extenders for specifing the CDN dependencies in this property.

C1Charts
	[74328][ASP.Net Wijmo][Wijmo Widget][C1Chart]JavaScript error "Unable to get property showAt..." is occurred If "Indicator" is used and Hint is disabled in C1Chart.

C1EventsCalendar
	[76971][45626][C1EventCalendar] Error occurs on postback when custom editEventDialogTemplate is set in C1EventCalendar.
	Fix the issue that when bind C1EventsCalendar to EntityDataSource, the event can not insert, update and delete.
	[77188][C1EventsCalendar] Color field binding does not work when C1EventsCalendar is bound to a database.
	[77151][C1EventCalendar] Event are duplicated when custom editEventDialogTemplate is set in C1EventCalendar.

C1FileExplorer
	Fix the issue that when target folder does not exist and do copy, the error message are not localized.

C1SiteMap
	[74344][ASP.Net Wijmo][C1SiteMap] All sitemap nodes appear on post back even though all nodes are cleared from C1SiteMap.

C1BarCode
	[60318][Asp.Net Wijmo][C1BarCode] BarCode is not retained 'Text' value when setting 'Visible' property on and off.

C1Editor
	[61465][IE9][Wijmo widget][WidgetExplorer][WijEditor] html tags are shown in 'Text to display' text box of 'Insert hyperlink' dialog box when reopen the 'Insert hyperLink' dialog box after link is set

C1GridView
	[77101][ASPNET Wijmo][C1GridView] Unlike previous build, Date value is automatically changed into Numeric value in Group Header Row after getting into edit mode.
	[78504][ASP.Net Wijmo][C1GridView][Win8.1] Grid do not scroll along with current cell using keyboard navigation.

C1CompositeChart
	[75054][ASP.Net Wijmo][Wijmo Widget][WijCompositeChart]The legend item for the hidden series is not dim with 'SharedPie' type in C1CompositeChart.
	[74106][ASP.Net Wijmo][Wijmo Widget][C1CompositeChart] JavaScript Error "Unable to get property..." is thrown in the disabled C1CompositeChart
	[77157][ASP.Net Wijmo][Wijmo Widget][WijCompositeChart]Unlike previous build 40,JavaScript error is observed if the data series is added at the secondary axis in C1CompositeChart
	[78520][Wijmo Widget][WijCandlestickChart][WijCompositeChart]Unlike previous build 43, some of the data series are displayed at the same point after seriesList option is set in C1CompositeChart.

C1Calendar
	[56126][Wijmo Widget][wijCalendar] wijCalendar is not refreshed after updating "maxDate" or "minDate" option.

C1Combobox
	[78741][Wijmo Widget][WijComboBox] Items become non-selectable when dataSource is set in the Search method
	[82241][Wijmo Widget][WijComboBox] AutoFilter does not work when inserted text is modified using 'Del' key.

C1CandlestickChart
	[78520][Wijmo Widget][WijCandlestickChart][WijCompositeChart]Unlike previous build 43, some of the data series are displayed at the same point after seriesList option is set in C1CompositeChart.

C1Dialog
	[78660][ASP.NET Wijmo][Wijmo Widget][WijDialog] Unlike previous build, some columns are not visible after restoring the WijDialog.

C1Wizard
	[79278][Wijmo Widget][wijWizard] NavigationButtons can't work when 'navButtons' property is changed.

C1Accordion
	[79837][Asp.Net Wijmo][Wijmo Widget][WijAccordion] Unlike previous version 47, accordion's pane is unable to open when there is empty content in it.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.178/4.0.20142.178)		Drop Date: 07/31/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.46

C1Eidtor
	[73709][Asp.Net Wijmo][C1Editor][IE] Unlike previous build 160, C1Editor hang after adding images inside the content and clicking on "FullScreen" mode

C1ThemeRoller
	[56809][ASP.Net Wijmo][C1ThemeRoller] New themes "Lucid" and "Stafford" are not applied  properly to any C1WijmoWebForm control at runtime and icons for these themes are not observed in "Smart tag" and "New Theme Form"

C1CompositeChart
	[72552][ASP.Net Wijmo][C1CompostieChart]Error The 'Type' property is read-only ...is thrown with "SharedPie" type in C1CompositeChart
	[74406][Enhancement Request][C1CompositeChart] Request to provide feature to create multiple Y axes when chart is bound to a datasource
	[74319][ASP.Net Wijmo][C1CompositeChart]JavaScript runtime error: Unable to get property 'attr'... is thrown when clicking on the plot area in C1CompositeChart with "Indicator".

C1ReportViewer
	[73304][Asp.Net Wijmo][ControlExplorer][C1ReportViewer] Javascript error is observed after selecting drop down items and resizing the browser

C1BarChart
	Fixed an issue that axis.y.origin doesn't work correctly.

C1Editor
	[68672][ASPNET Wijmo][Wijmo Widget][C1Accordion] Unlike previous build, cursor is shown in accordion header and user can type if C1Editor is placed inside its Content
	[37787][ASP.Net Wijmo][C1Editor][localized] "Set Background Color" dialog which can be opened from Insert Table Dialog is not localized in Japanese Version
	Fix the localization issue of cancel button in Insert Special Character dialog.
	[77627][ASP.NET Wijmo][Wijmo Widget][WijEditor] Unlike previous build, typed text gets lost after drag drop
	[77658][ASP.NET Wijmo][Wijmo Widget][WijEditor] Unlike previous build, cursor focus cannot be set and user cannot type any text after drag drop.
	[73165][Wijmo Widget][WidgetExplorer][WijEditor]Dropdown list and dialog boxes of C1Editor are shown behind the flash file in content area of Editor.

All Controls and Extenders
	[70481][ASP.Net Wijmo][C1SiteMap] JavaScript errors occur when "UseCDN" property is set as True in C1SiteMap
	Add JP localized resources to C1Controls.

C1Sparkline
	[73710][Asp.Net Wijmo][C1Sparkline] The series data added at run time is not retain when postback occur
	[73759][ASP.Net Wijmo][C1Sparkline]Request to provide "About link" in the smart tag of C1Sparkline

C1BubbleChart
	[65400][Wijmo Widget][WijBubbleChart]"ShowChartLabels" property does not work in C1BubbleChart

C1ComboBox
	[67282][Wijmo Widget][WijExplorer][C1PieChart]The position of hint value and the dropdown value are not synchronzied in Tooltip sample
	[73418][Asp.Net Wijmo][C1ComboBox][ControlExplorer]Argument Exception Error occurs when choosing "Center" in Dropdown list VPosition in C1ComboBox.

C1FileExplorer
	[73997][ASP.Net Wijmo][C1FileExplorer]Lost some icon of treeview and listview after postback occurs in C1FileExplorer.
	[77182][ASP.Net Wijmo][C1FileExplorer]Unlike list vew,multiple selection can't remove in Thumbnail view.
	[75748][ASP.Net Wijmo][C1FileExplorer] "Drag and drop" action cannot be performed after postback action occurred.
	[77901][ASP.Net Wijmo][C1FileExplorer][Localized] "Target [filepath] is already exist" message is not localized in Japanese build
	Fix C1FileExplorer runtime localization issue.

C1GridView
	[56491][Asp.Net Wijmo][C1GridView] "NaN" is displayed in date column when date column is moved out of band column in a certain scenario.

C1Sparkline and C1FlipCard
	[72438][ASP.Net Wijmo][C1Sparkline][C1FlipCard]Request to provide Toolbox icons for new controls in VS Toolbox

C1Menu
	[71895][Wijmo Widget][WidgetExplorer][WijMenu] Sub menu items are cut off in NEWS CATEGORY menu of Overview sample page

C1Accordion
	[77112][ASPNET Wijmo][C1Accordion] Unlike previous build, 'AutoPostBack' property does not work.

C1TreeView
	[64888][Mobile][wijtree] Expand button is displayed above the text with Mobile Safari.

C1EventsCalendar
	[65064][Touch][Wijmo Widget][MobileExplorer][WijEvcal]The labels are not shown and text cannot be entered to the textbox of "name,location" in the New Appointment Dialog.
	[77008][ASP.Net Wijmo][C1EventsCalendar][localized] Unlike previous builds, newly added japanese character '日' is dropped to next line for some days in navigation bar
	[77928][ASP.Net Wijmo][C1EventsCalendar][Localized]  Unlike previous build 176_JPN, Day of Month is displayed instead of Day of Week in the left pane of Day View
	[75801][Wjmo Widget][WijEvcal] Javascript error is observe pressing "Delete" key or "Backspace" key in in "start" time input textbox of Appointment dialog

C1ScatterChart
	[75398][ASP.Net Wijmo][Wijmo Widget][WijScatterChart]Unlike previous build 39,the symbol markers are not zoomed when hovering on them in C1ScatterChart
	Fixed an issue that when click the legend item, the labels will not hide.

C1Calendar
	[56060][Wijmo Widget][wijCalendar] wijCalendar is not refreshed after updating "navButtons" option.

C1FlipCard
	[77590][Wijmo Widget][WijFlipCard] Unlike previous build, control is not restored to original state after "destroy" method is called

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.177/4.0.20142.177)		Drop Date: 07/24/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.46

C1Editor
	[73709][Asp.Net Wijmo][C1Editor][IE] Unlike previous build 160, C1Editor hang after adding images inside the content and clicking on "FullScreen" mode

All Controls
	Update JP resources to the c1 controls.

C1ToolTip
	[74021][Asp.Net Wijmo][C1Tooltip] C1ToolTip content is not loaded on demand in a ASP.NET WebForms Site application

C1ReportViewer
	[69817][Asp.Net Wijmo][C1Editor][C1EventCalendar][C1ReportViewer][BootStrap Style] Unlike previous build 168, toolbar buttons icons images in C1Editor are not display properly in C1Editor when apply to Bootstrap style

C1DataSourceExtender
	[73933][ASP.Net Wijmo][C1List] Unlike previous build, JavaScript error ['wijdatasource' is undefined] is thrown binding with local data

C1EventsCalendar
	[42968][ASP.Net Wijmo][C1EventsCalendar] Calendar cannot be added in the "DataModel" sample of EventsCalendar in Control Explorer sample dropped on [13-Sept-2013]

C1Charts
	[65149][ASP.Net Wijmo][C1CandlestickChart][C1BarChart]JavaScript runtime error: Unable to get property 'startX'... is occurred when hovering mouse over the chart after the client-side event is cancelled.

C1Upload
	[46837][C1Upload] C1Upload takes long time to upload files on iOS7 devices.

C1TreeView
	Fixed an issue that when using c1treeviewnode's add method to add an item to an tree node, then postback the page, the added tree node will render incorrect children nodes.

Bootstrap
	[46627][Bootstrap Style][C1Dialog][C1LightBox][C1Tooltip] Modal mode is not working for dialog, lightbox and tooltip if Bootstrap Style is applied on the application.
	[69228][Wijmo Widget][WijComboBox]C1ComboBox gets resized after 'repaint' method is called.

C1ComboBox
	[74414][Asp.Net Wijmo][C1ComboBox] SelectedValue & SelectedIndex are not returned when accessed in postback after partial postback
	[70396][ASPNET Wijmo][C1ComboBox] Unlike previous build, Boundary UI is cut off on the right-hand side when "showTrigger" property is set
	[67696][Wijmo Widget][WijComboBox]"Open" event is always fired on typing the text in C1ComboBox Editor

C1CandlestickChart
	[65134][Wijmo Widget][WijCandlestickChart]Uncaught TypeError: Cannot read property 'stroke-width'... is thrown when applying style after changing chart type.
	[65143][ASP.Net Wijmo][C1CandlestickChart]Unlike C1BubbleChart, JavaScript error : Unable to get property length ...is thrown if one of the data series has no X values.

ControlExplorer
	[69307][ASP.Net Wijmo][ControlExplorer][C1ReportViewer]Unlike previous sample,Javascript error '0x800a1391 - Microsoft JScript runtime error: 'changeParentSize' is undefined' is observed when click on text in Setting of 'Flexible Size' sample page.
	[73848][ASP.Net Wijmo][ControlExplorer][C1Tooltip]'Custom' event does not fire in 'Trigger' sample page of C1Tootip
	[73831][C1TreeView][ControlExplorer] In the "Custom Drag & Drop" sample of C1TreeView, when node is dragged and dropped, it is behaved like moving instead of cloning

C1Dialog
	[69046][ASP.Net Wijmo][C1Dialog][C1ComboBox]Unlike previous build, javascript error['e' is undefinied] is occured when clicking the cancel button after changing selected index from c1combobox.
	[74404][Wijmo Widget][WijEditor] Unlike previous build, right border is removed and WijEditor stop working if WijEditor is placed in WijDialog and dialog is reopened
	[74035][Wijmo Widget][WijDialog] Dialog is disappeared when contentUrl is set to dialog at minimized state.
	[73774][ASP.Net Wijmo][C1ComboBox] Unlike previous build, Selected item cannot be cleared by cross button if C1ComboBox is placed in C1Dialog.
	[69914][Asp.Net Wijmo][C1Dialog][ControlExplorer] Unlike previous sample, multiple issues are observed when running Dialog samples of ControlExplorer
	[75173][44652][C1Dialog] C1Dialog does not work correctly when ScriptManager is placed on the same page
	[65640][Wijmo Widget][WijDialog] Disabled property does not work properly when width property of dialog is changed at run time.

C1FlipCard
	[71687][Wijmo Widget][WijFlipCard]'Animation' option does not  work when it is set at runtime.
	[75759]Client side events of flip card do not fire.
	[70736][Wijmo Widget][WijFlipCard]'width' and 'height' options do not work well at run time
	[73644][Asp.Net Wijmo][C1FlipCard] "SideChanged" and "UpdateContent" server events are not fired properly

C1GridView
	[Asp.Net Wijmo][C1GridView] "System.FormatException: Invalid JSON text." Exception is observe after postback is occur in grouping mode when "AllowC1InputEditor" is set to true.
	[64959][Asp.Net Wijmo][C1GridView] Script error occurs on dragging vertical freezing bar when setting "ShowHeader - False".
	[60864][ASPNET Wijmo][C1GridView] DataFormatString and ApplyFormatInEditMode properties do not work if AllowC1InputEditors = true.
	[73991][ASPNET Wijmo][C1GridView] Unlike previous build, Date value of the read-only column is changed into Number value when row gets into edit mode.
	[63185][Asp.Net Wijmo][C1GridView] Error message shows when entering edit mode into a merged cell at client-side.
	[21414][ASP.Net Wijmo][C1GridView] Checkbox field of C1GridView is not able to edit at client-side on setting "AllowClientEditing" property to True.

C1FileExplorer
	Fixed an issue that when the filter text not changed, it will still send filter data to server.
	Fixed an issue that when EnableFilteringOnEnterPressed property is true, press enter key again, the filter behavior lost effect.
	Fix the issue of when currentfolder contains no files and folders, switch to tilelist view, an pager shows inside of the right view.
	Fix the issue of the droppable donesn't work after paging/filter the grid and tilelist in C1FileExplorer.
	Fix the issue that switch to thumbnail view, click filter text box and press left key, the filtered result is empty.
	Fix the issue that when press "Shift+M", the contextmenu does not show.
	Fix the issue that use contextmenu to open a folder in gridview, the items of folder is not show in gridview.
	[73558][ASP.Net Wijmo][C1FileExplorer]Some Keyboard Support shortcut are not working properly in C1FileExplorer.
	[73867][ASP.Net Wijmo][C1FileExplorer] Unlike previous build 172, FilterTextBox is dropped to nextline when browser's width is reduced.
	[72937][ASP.Net Wijmo][C1FileExplorer] When AddressBox is excluded from the VisibleControls, FilterTextBox is covered by TreeView and Grid.
	[73576][ASP.Net Wijmo][C1FileExplorer]Related Treeview Item is deleted when deleting search keyword from search box in C1FileExplorer.
	[74040][ASP.Net Wijmo][ControlExplorer][C1FileExplorer] If ControlExplorer is hosted on IIS, "HTTP Error 404.0 - Not Found" is occurred when file is opened in C1FileExplorer.
	[73583][Asp.Net Wijmo][C1FileExplorer] "javascript" error is observe when "TreeView" control is invisible in C1FileExplorer

C1Sparkline
	[74042][Wijmo Widget][WijSparkline]Error "Invalid value for <rect> attribute..." is thrown after C1Sparkline is binding with array in column type
	[73854]C1Sparkline control is not shown after control size is changed

C1CompositeChart
	[74414][Customer Support][WijCompositeChart]WijCompositeChart throws "TypeError: axisOptions.tickMajor is undefined", when creating mutliple axis to display multiple WijBarChart.
	[73791][ASP.Net Wijmo][C1CompositeChart]There is no data binding properties for SharedPie in C1CompositeChart

C1ScatterChart
	[68086][Customer Support][WijScatterChart] "Transform" option of "chartLabelStyle" not working for WijScatterChart.

C1Expander
	[72154][Wijmo Widget][wijExpander] WijExpander's 'destroy' method is not working correctly when it has been destroyed.
	[69448][Wijmo Widget][wijExpander]Collapse state can't be retained when changing Expand state to Collapse state

ToolkitExplorer
	[75844][Asp.Net Wijmo][ToolkitExplorer] "Error: Function expected" javascript error is observe after clicking on some controls sample page in ToolkitExplorer sample.

C1Carousel
	[69875]Images in carousel widget can be changed by clicking number or slider button although 'Disabled' property is set to 'true' when 'PagerType' is set to 'Numbers' or 'Slider' at runtime

C1BinaryImage
	[73495][ASP.Net Wijmo][C1BinaryImage] SaveImageName is not correctly displayed at run-time if Japanese Text is set.

C1Calendar
	[64353][Wijmo Widget][WijCalendar] Setting popupMode option from client side does not work.

C1Accordion
	[72154][Wijmo Widget][wijExpander][wijAccordion] WijExpander's 'destroy' method is not working correctly when it has been destroyed

C1Tabs
	[67470][Wijmo Widget][WijTabs]The Tabstrips are shown beyond the C1Tabs and they can't be scrollable with Left/Right alignment

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.176/4.0.20142.176)		Drop Date: 07/09/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.45

C1FileExplorer
	[73624][ASP.Net Wijmo][C1FileExplorer] Evaluation Messages for the content controls of FileExplorer are displayed whether it is licensed or not.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.175/4.0.20142.175)		Drop Date: 07/09/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.45

C1Editor
	[73709][Asp.Net Wijmo][C1Editor][IE] Unlike previous build 160, C1Editor hang after adding images inside the content and clicking on “FullScreen” mode.

C1FileExplorer
	[73624][ASP.Net Wijmo][C1FileExplorer] Evaluation Messages for the content controls of FileExplorer are displayed whether it is licensed or not.
	[73582][Asp.Net Wijmo][C1FileExplorer][Bootstrap Style] Toolbar are not display when C1FileExplorer is apply with Bootstrap style.
	[72902][ASP.Net Wijmo][C1FileExplorer] GridView is displayed the upper level of current files/folders after sorting.
	[73307][ASP.Net Wijmo][C1FileExplorer] Expand/Collapse State of TreeView and Load file/folders of GridView are not retained after postback.

C1GridView
	[72805][ASP.Net Wijmo][C1GridView] Unlike previous build, filter drop-down can be accessed even when the grid is disabled.
	[72636][ASPNET Wijmo][C1GridView] Unlike previous build, filter operator dropdown list is not closed down on clicking the filter dropdown button of another column.
	[74019][ASPNET Wijmo][C1GridView] Unlike previous build, "Filter" command text gets disappeared automatically when showGroupArea option is set.

C1Tree
	[70065][ASPNET Wijmo][C1TreeView] Unlike previous build, Javascript error [Unable to get value of the property '$navigateUrl': object is null or undefined] occurs when 'disabled' option is toggled.

C1CandlestickChart
	[71365][ASPNET Wijmo][C1CandlestickChart] Unlike previous build , 'fill' property of 'highLow' present in 'seriesStyles' collection does not work.

C1CompositeChart
	[73784][Asp.Net Wijmo][C1CompositeChart]Unlike previous build 172, Javascript error is observe after hovering over the series data point of C1CompositeChart when hint are added [Regression Bug].

C1FlipCard
	[72403][Wijmo Widget][WidgetExplorer][Wijflipcard] Multiple issues are observe in "Customize animation" sample in WidgetExplorer.
	[70736][Wijmo Widget][WijFlipCard]'width' and 'height' options do not work well at run time

Export Service
	Implement license supporting. When the license is trial, trial version info is written to the exported file.
	
C1SuperPanel
	[70334][ASPNET Wijmo][C1ComboBox] Unlike previous build, last combo item cannot be seen in dropdown.

C1Combobox
	[68976][ASPNET Wijmo][C1ComboBox] Unlike previous build, more spaces are added to the dropdown list after selecting the last item and searching the last item.

C1ThemeRoller
	[73721][ASP.Net Wijmo][C1ThemeRoller]'Edit current theme' link is not displayed in smart tag after new theme is created [Regressin Bug].
	
ControlExplorer
	[69921][C1ReportViewer]Preview area does not fit in C1ReportViewer when running ReportViewer samples.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.174/4.0.20142.174)		Drop Date: 07/07/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.44

ControlExplorer
	Update the web.config to fixed an issue that the demo can't run when new controls build is droped

C1GridView
	[72921][ASP.Net Wijmo][C1GridView] Unlike previous build, group icons are not displayed correctly when "ShowRowHeader" property is set to true.
	[72544][ASPNET Wijmo][C1GridView] Unlike previous build, Hyperlink of "Edit" and "Delete" command fields are automatically converted into html buttons after Postback action.
	[70174][Asp.Net Wijmo][C1GridView] Command columns are shown with band column header when setting "AutoGenerateEditButton/AutoGenerateDeleteButton/AutoGenerateSelectButton - True".

C1FileExplorer
	Fix the issue that gridview looks smaller when swtiching view from thumbnail to detail.
	Fix the issue that clicking FileExplorer's Upload sample throws an exception.
	Fix the issue that can't open file using toolbar's open button.
	Fixed an issue that the expand icon doen't show when the item contains sub folder.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.173/4.0.20142.173)		Drop Date: 07/06/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.44

ControlExplorer
	[69337][ASP.Net Wijmo][ControlExplorer][C1Tooltip]Unlike previous sample, default selected value 'Auto' in 'Close Behavior' dropdown list is work only one time at initial state in 'Close Behavior' sample page.
	[69877][ASP.Net Wijmo][ControlExplorer][C1Dialog]Unlike previous sample, Dialog is not restored to original place and dialog cannot be hidden again when click restore button in 'Overview' and 'Animation' sample pages.
	[C1GridView][C1Charts]Use grid.exportGrid and chart.exportChart method instead of export js in exporting samples.

C1Upload
	[70240][ASP.Net Wijmo][Wijmo Widget][C1Upload]When 'EnableSWFUploadOnIE' or 'EnableSWFUpload' is set as 'True', upload button cannot be clicked and so it cannot upload any file.
	[69849][Asp.Net Wijmo][C1Upload][BootStrapStyle] Javascript error occurs when hover on C1Upload.

C1EventsCalendar
	[71495][Asp.Net Wijmo][C1EventsCalendar] Incorrect date format gets displayed on the tool tip of the navigation bar of week view.

Bootstrap
	[72907][ASPNET Wijmo][C1GridView][Bootstrap] Unlike previous build , Command text are not displayed for "Edit" / "Delete" buttons

C1FileExplorer
	[72431][ASP.Net Wijmo][C1Sparkline][C1FileExplorer][C1FlipCard]Error "Failed to create designer..." is thrown in design view when placing C1Sparkline onto the form.
	[72439][ASP.Net Wijmo][C1FileExplorer] "Object reference not set to an instance of an object" error is occurred when C1FileExplorer is placed on the form and run the project.
	[72902][ASP.Net Wijmo][C1FileExplorer] GridView is displayed the upper level of current files/folders after sorting.
	Fix the issue that address/filter box shows ugly when only one of them is visible.

C1Charts
	[71822][Wijmo Widget][WijLineChart]Unlike WijBarChart , the axes are not displayed in C1LineChart if Origin value is set.
	[71878][Wijmo Widget][WijChart]"SeriesHoverStyle" does not take effect to the trendlines in C1Chart.
	Remove the class definition of ChartAxis and fix the possible problems by Nazim's modification about axis.
	[72123][Wijmo Widget][WijChart]The legend and the data series become inconsistent and some trendlines are not shown if Y value count is more than X for the trendline in C1 Chart.
	[71943][Wijmo Widget][WijChart]Error: Invalid value for <path> attribute d="H430" is thrown if logarithmic "FitType" is used for Trendline and 'Min' value-0 is set in C1Chart.
	Add trend line support for C1Charts.
	[C1Charts]Add exportChart method for C1Charts in client-side.
	
C1GridView
	[C1GridView]Add exportGrid method for C1GridView in client-side.

C1FlipCard
	[70922][Wijmo Widget][WijFlipCard] FlipCard UI not dim when setting “disabled” option at page initialize although it is disabled.
	[72057][Wijmo Widget][WijFlipCard]Flipping event does not fire when flipcard is flipping from backpanel to frontpanel.
	[71687][Wijmo Widget][WijFlipCard]'Animation' option does not  work when it is set at runtime.

All Controls
	[69840][ASPNET Wijmo] Unlike previous build, Server side error [The string parameter 'url' xxx] occurs when Extender control is used.
	[69233][Asp.Net Wijmo][Localized] JavaScript error occurs after any control from localized build is placed to WebForm and run

C1AppView
	[69863][Asp.Net Wijmo][C1AppView] JavaScript error occurs on running application when C1AppView is contained C1AppViewItem.

C1LineChart
	[71914][Wijmo Widget][WijLineChart]The symbol markers are not displayed in the trendline but, visible in Legend of C1LineChart.

C1Dialog
	[72430][ASP.Net Wijmo][C1Editor][C1Dialog]Script Error "Object doesn't support property or method '_trackFocus'" is occurred.
	[73438][Wijmo Widget][WijDialog]Position option of wijdialog do not work
	[72742][ASP.Net Wijmo][C1ReportViewer][C1EventsCalendar] Unlike previous build, Log window appears at incorrect position when setting 'EnableLogs – True'

C1BinaryImage
	[70252][ASP.Net Wijmo][C1BinaryImage] "About..." link is not displayed in Smart Tag of C1BinaryImage control.
	[70523][ASP.Net Wijmo][C1BinaryImage] "Enabled" property does not work when it is set to "False".
	[70300][ASP.Net Wijmo][C1BinaryImage] The checked state of "Auto Adjust Image Control Size" checkbox cannot be changed from Smart Tag.

C1Charts
	[68903]Support Indicator Line in server controls and extenders.

C1BarChart
	[71204][Wijmo Widget][WijBarChart]Uncaught TypeError:"Cannot read property 'bar' of undefined" is thrown when toggling legend item if the trendline is set in the stacked C1BarChart.
	[71130][ASPNET Wijmo][C1BarChart] Unlike previous build, Javascript error[Unable to get value of the property 'stroke-width': object is null or undefined] is thrown on setting 'stacked' option when bounded to Knockout data source
	[71849][Wijmo Widget][WijBarChart]Unlike WijLineChart, the trendlines are not drawn in the plot area if "AnnoMethod"- ValueLabels and 'Min' value are set in C1BarChart.

C1Editor
	[70731][Wijmo Widget][WijRibbon] Unlike previous build, split button and drop-down are not displayed correctly.
	[70729][ASP.NET Wijmo][Wijmo Widget][C1Editor]Icon for 'Table' in 'Tables' ribbon group of 'Insert' tab is distorted.
	[72415][Wijmo Widget][WidgetExplorer][WijEditor] Unlike previous sample, 'Set Background Color' dialog box is shown at bottom-left corner of the sample page

C1FlipCard
	[71833][Wijmo Widget][WijFlipCard] Content inside backpanel and frontpanel are not display anymore when wijflipcard is destroy and recreated again.

C1LineChart & C1ScatterChart
	[69862][Asp.Net Wijmo][C1LineChart][C1ScatterChart] Unlike previous build 168, Data Series are not display in C1LineChart and C1ScatterChart when DateTime values are set in Yaxis
	[69942][ASP.Net Wijmo][C1LineChart]Unlike previous build ,the data series are displayed at incorrect point with Area Chart type

C1SiteMap & C1SiteMapDataSource
	[71200][ASP.Net Wijmo][C1SiteMap] C1Sitemap UI is distorted on setting 'Width' and 'Height' properties.
	[71154][ASP.Net Wijmo][C1SiteMapDataSource] 'Add Child Item' and 'Insert Item' buttons are not working properly in C1SiteMap designer form.
	[70980][ASP.Net Wijmo][C1SiteMapDataSource] 'SiteMapFile' property is not retained when post back occurs.
	[71839][Asp.Net Wijmo][C1SiteMap] Although C1SiteMap's Enabled property is set as false, C1SiteMap still remain enable.
	[71936][ASP.Net Wijmo][C1SiteMap] VS crash when an item is added in 'C1SiteMapNodeBinding'/'C1SiteMapLevelSetting' editor and cancel the operation.
	[70912][ASP.Net Wijmo][C1SiteMapDataSource] Exception is thrown when 'Edit Current SiteMap' link is clicked in smart tag.
	[71469][ASP.Net Wijmo][C1SiteMap] Unbound items are restored in C1Sitemap when setting 'DataSourceID' at run-time and post back occurs.

C1Combobox
	[69896][ASP.Net Wijmo][C1ComboBox]Unlike previous build, mutiple selection is shown when SelectionMode is set to Single and Selected is set true to comboboxItem.
	[70935][ASPNET Wijmo][C1ComboBox] Unlike previous build, Javascript error[Unable to set value of the property 'selected': object is null or undefined] is thrown on clicking dropdown button after changing selectedIndex through the underlying data source

C1Gallery
	[68135][ASP.Net Wijmo][C1Gallery] If 'Mode' property of gallery is set as 'Swf', 'Autoplay' property does not work correctly.
	[69908][ASP.Net Wijmo][ControlExplorer][C1Gallery] Unlike previous sample, Paging numbers does not work correctly in 'Paging' sample page of C1Gallery
	
C1CandlestickChart
	[72817][ASP.Net Wijmo][C1CandlestickChart]Unlike previous build 170,JavaScript runtime error: Unable to get property 'valueOf'... is thrown if some binding properties are null

C1ThemeRoller
	[72990][ASP.Net Wijmo][C1ThemeRoller]Unlike previous build,some error messages are shown when create new theme with ThemeRoller.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.172/4.0.20142.172)		Drop Date: 07/01/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.42

C1Editor
--------
	[69817][Asp.Net Wijmo][C1Editor][C1EventCalendar][C1ReportViewer][BootStrap Style] Unlike previous build 168, toolbar buttons icons images in C1Editor are not display properly in C1Editor when apply to Bootstrap style

All Controls
------------
	[69233][Asp.Net Wijmo][Localized] JavaScript error occurs after any control from localized build is placed to WebForm and run

ControlExplorer
	[69307][ASP.Net Wijmo][ControlExplorer][C1ReportViewer]Unlike previous sample,Javascript error '0x800a1391 - Microsoft JScript runtime error: 'changeParentSize' is undefined' is observed when click on text in Setting of 'Flexible Size' sample page.

C1Dialog
	[69046][ASP.Net Wijmo][C1Dialog][C1ComboBox]Unlike previous build, javascript error['e' is undefinied] is occured when clicking the cancel button after changing selected index from c1combobox.

BootStrapStyle
	[69849][Asp.Net Wijmo][C1Upload][BootStrapStyle] Javascript error occurs when hover on C1Upload.

C1Charts & C1Gauges
	[69857][Asp.Net Wijmo][Charts][Gauges] Control UI is not displayed at design-time when placed charts and gauges on the design surface.
	
C1Dialog
	[69914][Asp.Net Wijmo][C1Dialog][ControlExplorer] Unlike previous sample, multiple issues are observed when running Dialog samples of ControlExplorer.
		
C1ThemeRoller
	[64629][ASP.Net Wijmo][C1ThemeRoller]The selected theme styles are not displayed in the preview image of the calendar control.

C1FlipCard
	[70736][Wijmo Widget][WijFlipCard]'width' and 'height' options do not work well at run time

C1CompositeChart
	[70355][Wijmo Widget][WidgetExplorer][WijCompositeChart]Unlike previous build 39,Error: Invalid value for <circle> attribute cy="NaN" ... is observed and scatter chart type is not shown correctly.

C1SiteMap
	[70147]Add Themeroller support to new controls.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.171/4.0.20142.171)		Drop Date: 06/29/2014
=============================================================================================

Dependencies
------------
jQuery 1.11.1 
jQuery UI 1.11.0
jQuery Mobile 1.4.0
Wijmo 3.20142.42

ControlExplorer
	[69303][ASP.Net Wijmo][ControlExplorer][C1RadialGauge]Unlike previous sample,image of RadialGauge in 'Template' sample page of 'RadialGuage' is truncated

C1CandlestickChart & C1CompositeChart
	[68832][Wijmo Widget][WijCandlestickChart]Unlike previouse build,the styles are incorrectly applied to the data series of  C1CandlestickChart.
	[68902][Wijmo Widget][WijCompositeChart]Unlike previous build , the data series color and legend color are inconsistent with 'candlestick' chart type in C1CompositeChart.

C1Editor
	[68050][Wijmo Widget][WijEditor][WijTabs] Unlike previous build, Ribbon toolbar UI is removed after dragging the splitter bar.
	[68934][ASPNET Wijmo][C1Editor][WijEditor] Unlike previous build, Text value gets lost when objects is inserted after changing the 'editorMode' of WijEditor

C1Calendar
	[67787][Asp.Net Wijmo][C1InputDate] Unable to navigate between months on Android touch device using Chrome browser: can't click month on month view.

C1TreeView
	[69888][Wijmo Widget][WijTree] Unlike previous build, 'showExpandCollapse' option does not work when WijTree is bounded to Knockout.
	[68756][Wijmo Widget][WijTree] Unlike previous build, collapse/expand icon is still shown even through no child item is present in tree node.
	[68795][Wijmo Widget][WijTree] Unlike previous build, tree node disappear after doing drag drop action for two times.
	[68797][Customer Support][WijTree]Selected nodes collection not getting reset after a node is dragged from one WIjTree to other WijTree

C1EventsCalendar
	[33594][Wijmo Widget][wijEventsCalendar][Sample] Page is navigated to Home page after clicking the "Cancel" button of New Event Dialog in the samples of EventsCalendar except the Overview sample.	

C1Combobox
	[71185]C1ComboBox does not render correctly inside panel wrapped in updatepanel when panel visibility is toggled and panel is initially invisible
	[68993][43799][WijComboBox] Items are not traversed using arrow keys when BootStrap theme is applied
	[69252][CustomerSupport][WijComboBox] WijComboBox appearance affected when setting the ensureBodyOnDropDown option to true or false.
	[68191][43254][43193-139][WijComboBox] The dropdown list does not appear correctly upon typing if WijComboBox is used with BootStrap.

C1GridView
	[71168][Asp.Net Wijmo][C1GridView]Aggregates do not work when there is null value in Template field.

C1Tab
	[67429][Wijmo Widget][WijTabs]Uncaught TypeError: Cannot read property ... is thrown when changing "Collapsible" property.
	[67749][Wijmo Widget][WijTabs]C1Tabs is distorted when changing the Alignment after collapsed.

C1Expander
	[43521][Wijmo Mobile][wijExpander][ASP.Net Wijmo] The height of Header and Content are not consistence if ExpandDirection is 'Left' or 'Right'

C1CompositeChart
	[64615][CompositeChart][FeatureRequest] Need shared Legend in Pie Chart: Added a compositechart type "sharedPie" to implement this feature.

C1ToolTip
	Fixed an issue of wijtooltip that when there are two elements with tooltip and one is destroyed, the other works incorrectly.
	[65490][Customer Support][WijToolTip]Memory Leaks observed when WijToolTip is destroyed using the 'removeNode' method of KnockOut.

C1ScatterChart
	[67756][Wijmo Widget][WidgetExplorer][WijScatterChart][IE8] Javascript error is observe after navigating "Dataview" sample of ScatterChart in WidgetExplorer

C1BubbleChart
	[33265]Implement one part of the request: adjustment for overlapping bubbles.

C1Inputs
	[69838][C1Input][Asp.Net Wijmo][C1Input][C1TreeView][C1GridView][Bootstrap Style] Unlike previous build 166, Styles are not applied when bootstrap style are applied to C1Input, C1TreeView and C1GridView
	
=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20142.170/4.0.20142.170)		Drop Date: 06/13/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.40

C1Tab
	[67989][Wijmo Widget][WijEditor][WijTabs] Unlike previous build, WijEditor's cursor is displayed on the tab header of WijTabs after changing tab item.

C1Calendar
	[67787][Asp.Net Wijmo][C1InputDate] Unable to navigate between months on Android touch device using Chrome browser.

C1ScatterChart
	[68086][Customer Support][WijScatterChart] "Transform" option of "chartLabelStyle" not working for WijScatterChart. (Only fixed in IE9+)

C1Ribbon
	[66490][Wijmo Widget][wijribbon] Button text gets truncated when text size is larger than the button size

C1EventsCalendar
	[68242][Wijmo Widget][WijEvcal] Unlike previous build, Javascript error[Unable to get value of the property 'top': object is null or undefined] occurs when "goToTime" client side method is invoked

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20141.169/4.0.20141.169)		Drop Date: 06/11/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.39

BootStrapStyle
	[69120][Asp.Net Wijmo][BootStrapStyle] C1Splitter, C1Slider, C1ProgressBar, C1Dialog, C1Calendar can't work properly in bootstrap style

C1InputDate
	[69035][Asp.Net Wijmo][C1InputDate] After clicking the cross sign in C1InputDate dropdown, NaN/NaN/0NaN is appeared instead of M/d/yyyy.

C1ThemeRoller
	[69125][ASP.Net Wijmo][C1ThemeRoller] "Value cannot be null. Parameter name: stream" error is occured C1ThemeRoller.

AllControls
	[68953][Asp.Net Wijmo][All C1 Controls] Unlike previous build, Culture property setting does not work.
	[69099][ASP.Net Wijmo][C1 control][Query] JavaScript runtime error: Object doesn't support property... is observed when creating widgets with/without EnableCombinedJavaScripts property.
	[69155][ASP.Net Wijmo][Control Explorer] Script Error is occurred when "Control Explorer" is run.

C1GridView
	[68938][ASP.NET Wijmo][C1GridView] Unlike previous build, 'CultureCalendar' property does not work.
	
C1BarCode
	[68981][Asp.Net Wijmo][C1BarCode] After placing C1BarCode to a web form and running, javascript error occur.

C1Gallery
	[68978][Asp.Net Wijmo][C1Gallery] 'Play/Pause' of C1Gallery does not work in runtime.

C1SuperPanel
	Fixed an issue that in some cases, superpanel can not scroll to the bottom of content.

C1ComboBox
	[68984][ASP.Net Wijmo][C1ComboBox] Unlike the previous build, first item is always shown as selected item.
	[69032][ASP.Net Wijmo][C1ComboBox] Unlike previous build, selected item cannot be cleared by 'Back Space' key when SelectItemOnFocus is set True.
	[68965][Asp.Net Wijmo][C1ComboBox] Unlike previous build ,C1ComboBox UI becomes invisible if C1Chart is used on the same page.
	[68463][Customer Support][WijCombBox]WijCombBox dropdwon items are not displayed if a WijComboBox is initially disabled via knockout model and enabled later.
	
C1Upload
	[69036][ASP.Net Wijmo][C1Upload]Javascript error 'Error: Object doesn't support property or method 'wijprogressbar'' is observed when click "upload all" button.

C1Ribbon
	[66489][Wijmo Widget][wijribbon] Dropdown's string get truncated.

C1TreeView
	Add "OnClientNodeCheckChanging" event.

C1EventsCalendar
	[68232][Wijmo Widget][WijEvcal] Unlike previous build, time slots are shifted to the left while dragging the mouse at all day area.

C1Chart
	[69150][ASP.Net Wijmo][C1Chart]"IsContentHtml" property does not take effect to the Chart's hint without changing fill color.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20141.168/4.0.20141.168)		Drop Date: 06/06/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.39

AllControls
	Fix the issue that gauge and chart can't work well when both two controls are on a same page.

C1Dialog
	[65269][Asp.Net Wijmo][C1Dialog]Close button does not work in minimize state and restore state (i.e. when reopen the dialog after minimize state)  when ContentUrl property is defined for dialog control.

C1Tabs
	[68083][Wijmo Widget][WijEditor][Firefox] Unlike previous build, Typed text gets disappeared after dragging the tab item.

C1Editor
	[68067][Wijmo Widget][WijEditor] Unlike previous build, text is displayed on the web page when added through "Insert Code" dialog.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20141.167/4.0.20141.167)		Drop Date: 06/05/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.39

AllControls
	Improve the JS loading performance(using individual JS files).

C1TreeView
	Improve wijtree performance.

C1CandlestickChart & C1CompositeChart
	[61590]Candlestick type default should have defaults like C1CandleChart.

C1ComboBox
	[66482][ASP.Net Wijmo][C1ComboBox]Unlike previous build 156,it navigates to the last item through all items with one click on the scroll bar down button.
	[66954][ASPNET Wijmo][Wijmo Widget][C1ComboBox] Unlike previous build, selection is always reset to first combo item after invoking "destroy" client side method.
	[65971][Custmer Support][WijCombobox]Wijcombobox does not support multiple selection mode with Knockout.
	[64599][CustomerSupport]Multiple issues observed when using wijcombobox and wijinputdate in AppView.
	[66167][Community][WijComboBox]SelectedValue option of wijcombobox not selecting an item in wijcombobox that has value equal to '0'.
	[67438] An exception thrown when submit form from client side.

C1ToolTip
	Fixed 2 bugs in the wijtooltip "showAt" method.

C1Editor
	[64630][ASPNET Wijmo][C1Editor] Unlike previous build, three line break tags are automatically added if Text property is not set and FullScreenMode = True.
	[65487][ASPNET Wijmo][Wijmo Widget][WijEditor] Unlike previous build, existing objects are removed when items are added after switching to "Full Screen" mode.
	[68010][Wijmo Widget][WijEditor] Unlike previous build, splitter bar is not shown in the middle when setting editorMode = "split".
	[67383][ASPNET Wijmo][Wijmo Widget][C1Editor] Unlike previous build, only one control is added to C1Editor on clicking twice in control icon in "Insert" tab.

C1TreeView
	[64888][Mobile][wijtree] Expand button is displayed above the text with Mobile Safari.

C1PieChart
	[65118][Communitty][WijPieChart]Setting the stroke-width option to 0 in seriesStyle option does not sets the width of the stroke around the legend boxes to 0 in the chart legend.

C1SuperPanel
	[64635][Customer Support]The thumb in the vertical scrollbar of wijcombobox cannot be dragged using the left mouse button in AppView Page.
	[67928]Fixed an issue that user cannot get the scroller newValue form the parameter of the "scrolled" event.

C1Chart
	Add a property in chart_hint option named "isContentHtml" to determine whether to use a html style tooltip.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20141.166/4.0.20141.166)		Drop Date: 05/20/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.37

C1BarChart
	[64582][Mobile][wijbarchart] Drill down is executed twice by single tap.

C1TreeView
	[64888][Mobile][wijtree] Expand button is displayed above the text with Mobile Safari.

C1Editor
	[65187][ASPNET Wijmo][Wijmo Widget][WijEditor] Unlike previous build, items are always added to the top of web page after "Hidden Field" is added and clicking outside WijEditor.
	[65215][ASPNET Wijmo][Wijmo Widget][WijEditor] Unlike previous build, Javascript error[Object doesn't support property or method 'pasteHTML'] occurs when Html control is added for second time
	[65228][ASPNET Wijmo][Wijmo Widget][WijEditor] Unlike previous build, right border is not shown and Height is increased if placed in Splitter.

C1Tabs
	[65295][ASPNET Wijmo][Wijmo Widget][WijTabs] Unlike previous build, tab item selection does not work correctly and content of all tab items are displayed after item re-ordering

C1Charts
	[62872][Asp.Net Wijmo][C1LineChart] Tooltip with white "fill color" becomes transparent when C1LineChart is loaded in compatibility mode in IE9

C1Dialog
	[65693][ASPNET Wijmo][C1Dialog] Unlike previous build, C1Dialog's position is changed on Asynchronous Postback.

C1LineerGauge C1RadialGauge
	[65450][Asp.Net Wijmo][C1LinearGauge][C1RadialGauge] Tick major and tick minor are not displayed correctly when 'Marker' is set to 'Tri'

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.165/4.0.20133.165)		Drop Date: 05/07/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.37

C1Charts
	[62872]Tooltip with white "fill color" becomes transparent when C1LineChart is loaded in compatibility mode in IE9.

C1SuperPanel
	[62949]Scroll thumb is overlapped by the down button of scrollbar if page index is changed.

globalize.cultures
	[64768]'CultureCalendar' property is not working properly in C1Calendar control.

C1Editor
	[65072]Javascript error[Object doesn't support property or method 'getBookmark'] occurs on clicking the "FullScreen" button after selecting the added TextBox.

C1TreeView
	[64509]TreeNodes are expanded and automatically collapsed on clicking "Expand" icon if WijTree is recreated.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.164/4.0.20133.164)		Drop Date: 04/30/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.37

C1Charts & C1BarCode & C1Gauge
	[64518]The css settings of JQuery datepicker are overriden when C1LineChart is rendered on the web page.

C1Charts 
	[60918]C1CandlestickChart&C1BubbleChart&C1Composite:Compilation Error "Value of type Double? cannot be ..." is thrown when running the C1 Charts in VB application.

C1Editor
	[64231]Selected editorType is not shown as selected icon in status bar
	[64484]Clicking 'Source View' and 'Design View' buttons in status bar does not correctly work
	[64321]Clicking ribbon buttons in "Insert" Tab does not work correctly when two instances of C1Editor are present on the form.
	[64725]Items are always added to the top of web page when clicking ribbon buttons in "Insert" Tab after double clicking outside WijEditor

C1GridView
	[64213]Aggregates do not get calculated for TemplateField.
	[64489]Setting font using HeaderStyle property has no effect.
	[64466]C1InputText loses focus when placed inside ItemTemplate of C1GridView, on Android.
	[64612]The method attached to the "OnClientPageIndexChanging" property of C1GridView is not called again once the event is cancelled.
	[64583]Band columns are distorted when setting "ShowRowHeader-True".
	[64687]"CultureCalendar" property is not affected to date columns from filter row.

C1Tabs
	[64425]Setting double byte characters in 'ID' property of C1Tabs produces javascript exception

C1Splitter
	[64378]Clicking the Ribbon toolbar does not work after draggging the Splitter expander bar.

C1SuperPanel
	Fixed an issue that when initialize the superpanel will ignore the "scrollValue" if "scrollBarVisibility" is hidden.

C1Calendar
	[64593]Can't select calendar title.

C1Tree
	[64503]Property settings of underlying UL tags are not maintained when WijTree is destroyed

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.163/4.0.20133.163)		Drop Date: 04/22/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.36

C1ComposisteChart
	[63306]"HintField" property does not take effect with C1Candlestick chart type if the chart is data binding.

Added some Jp resources.

C1ThemeRoller
	[60185]A Target invocation exception occurs when 'Create New Theme' Smart Tag option is clicked in Visaul Web Developer 2010.
	[62876]Headers of Properties in ThemeRoller are not localized in Japanese version of C1WebWijmo.WebForms.

C1CompositeChart
	[56612]C1Composite chart with one Datetime X axis value does not display correctly.

C1Combobox
	[46941][IE11] When filtering by entering Japanese character, duplicated characters are shown in C1ComboBox
	[63146][MVC] WijComboBox resizes abruptly when placed inside WijExpander

C1Editor
	[63012]Typed text get lost completely after moving the tab
	[61660]Entered text get lost on changing the EditorMode after Asynchronous Postback
	[35404]Table border is disappear in C1Editor after postback occur.
	[63461]Formatting applied to the hyperlink text is displayed in the hyperlink dialog as HTML style tags.
	[63572]Javascript error[cannot call methods on wijmenu prior to initialization; attempted to call method 'hideAllMenus'] occurs when pressing "Esc" key in color dialog
	[64114]Height setting of underlying textarea get lost when WijEditor instance is created if CSS class is used
	[63912]Wijpopup is automatically closed down when selecting the date from WijInptuDate present on that

C1Video
	[63214]Video Player Extender throws error when added to a panel.

C1LineChart
	[62872]Tooltip with white "fill color" becomes transparent when C1LineChart is loaded in compatibility mode in IE9

C1Calendar
	[63004]Incorrect calendar week numbering is shown when the culture is set to 'fi-FI'.

C1Carousel
	[63305]Images in carousel control can be moved although 'Enabled' property is set to 'false' when 'PagerType' is set to 'Slider'

C1TreeView
	[63313]Expand / Collapse icons and treenodes are not correctly aligned in data bound WijTreeView

C1BubbleChart
	[63015]Chart UI (Axis, GridLines, Legend, Labels)  is completely removed if seriesList is set from client side script

C1Expander
	[63739][Mobile]Web site isn't displayed in the content pane.

C1GridView
	[63597]Sort glyph do not display in band column headers on sorting band columns when setting "ShowFilter - True".

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.162/4.0.20133.162)		Drop Date: 04/04/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.35

C1Charts
	Add CultureCalendar property.

C1Calendar
	Add CultureCalendar property.

C1CandlestickChart
	[56470]"SeriesHoverStyles" lost in C1CandlestickChart after postback.

C1ThemeRoller
	[60185]A Target invocation exception occurs when 'Create New Theme' Smart Tag option is clicked in Visaul Web Developer 2010.

C1Editor
	[61414]selected Font Name is not shown as Selected Item in "Font Name" dropdown
	[61365][40862-139]"Add to Dictionary" feature not working correctly.

C1EventsCalendar
	[61529]Lower part of "New Event" dialog is displayed outside visible region if placed in C1Wizard.
	Add CultureCalendar property.

C1GridView
	[56601][IE] Memory leak is observed when C1GridView is displayed inside a FrameSet.
	Add CultureCalendar property.

C1ComposisteChart
	[40464]'Center' xy position set for pie chart type is not retain and reset back to default position when postback occur

C1Input
	  [48485]Unlike C1InputText, the placholder value in C1InputMask is still display as text value after postback occur.
	  [50687]'Placeholder' is displayed as “Text” after post back if “AutoPostBack” is set as “true” and “Format” is set at DesignTime.

C1Wizard
	[44204]Javascript Error is observed after setting 'WijmoControlMode' to Mobile in C1Wizard.
	Fixed an issue when wijwizard renders in Mobile mode.

C1ReportViewer
	[63025]Exported PDF is not read-only when setting ‘PdfSecuritySetting.AllowCopyContent - False’ property at run-time

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.161/4.0.20133.161)		Drop Date: 04/01/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.34

Added some Jp resources.

C1LineChart
	[61415]DataSeries's display exceed the ChartArea and Axis-X is covered up.

C1BarChart & C1CompositeChart
	[56612]C1Composite chart with one Datetime X axis value does not display correctly.

C1Editor
	[52052][39539]Hyperlink is always inserted at the beginning in Wijeditor
	[61299]tab item headers and Ribbon toolbar are not correctly aligned.
	[60927]"Font Size" combo dropdown items are not shown if mode = bbcode.

C1CandlestickChart
	Add HintField property to C1CandlestickChartBinding, this property is used to bind hint contents.
	[60823]"Nan/NaN/0Nan" is display for X-axis anno when postback is occur continuously

C1CompositeChart
	[60837]'Label' and 'Visible' properties do not work for the legend of C1Candlestick chart type if the chart is data binding
	[60823]"Nan/NaN/0Nan" is display for X-axis anno when postback is occur continuously

C1GridView
	[Designer] The Column.Width property is removed from the Format tab (duplicates the Selected Columns -> Column.Width property of the Columns tab).

C1ComboBox
	Fixed an issue that "labelText" element will be repeated if set otpion "labelText" mutil times.
	[61480]System.InvalidOperationException: Assembly cannot be... is thrown if ScriptManager is used

C1BubbleChart
	[60877]some axis label are not shown and bubble object overlap the axis

C1Tabs
	[61423]Ribbon toolbar UI is completely removed after sorting the C1TabItem if C1Editor instance is present on C1TabItem.

C1ReportViewer
	[56802]Error occurs when report is printed with DPI set as 192.
	Add "PdfSecuritySetting" property, current the "PdfSecuritySetting" include "AllowCopyContent".

C1GridView
	[61630]Header Template text is not shown when the C1Band consists of a columns collection

C1Menu
	For case [34581]: Add property "SubmenuTriggerEvent" to specify the event used to show the submenu.
	[34581][29004-139]Request to provide a property for sub menu item to display the sub menu on mouse enter when C1Menu is use as context menu set on right click.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.160/4.0.20133.160)		Drop Date: 03/19/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.34

C1Charts
	[52830] The hidden ValueLabel gridline is displayed with the other GridlineStyle after postback.

ControlExplorer
	Update the candlestick chart samples.

ToolkitExplorer
	Update the candlestick chart samples.

ControlExplorer
	[ASP.Net Wijmo] Incorrect browser image is shown in 'Symbols' sample page

ToolkitExplorer
	[ASP.Net Wijmo] Incorrect browser image is shown in 'Symbols' sample page
	
C1CandlestickChart
	Fix an issue that when there is only one data value is set, nothing is shown in chart.

C1CompositeChart
	Add Height property to CompositeChartYAxis to support part axis.
	[57596] Unlike previous build 158,JavaScript error : Unable to get property max ... is thrown if C1CompoiteChart has candlestick data series
	[56614] DataSeries is displayed out of ChartArea partially when mutiple Y axis are plotted
	[57322] Unlike previous build 157,Axis X labels lost when page is postback after changing data series with string value from the client side.
	Change the value "Hloc" to "Ohlc"  in the type ChartSeriesType.
	[59179] Unlike previous build, visibility of bubble series are not correctly toggled on clicking the legend items
	[58430] Javascript error is observe when binding with more than one data series when chart type is set to candlestick , hloc or hl.

C1EventsCalendar
	[57248][IE11] Unlike previous build, "New Event" dialog is automatically closed down on selecting the date in calendar dropdown

C1GridView
	[57267] Unlike previous build, edit action is not cancelled by clicking "Cancel" link button.
	If there is no data bound to the grid, the keydown event will bubble out of the grid.

C1BarCode
	Add property "EncodingException" to provide Error Description.

C1ComboBox
	[58069] Unlike previous build 120 , the dropdown list is not opened after postback in C1ComboBox control having multiple columns with ItemTemplate.
	[60163] Javascript error[Unable to get value of the property 'label': object is null or undefined] is thrown on clicking the dropdown button after remove the data items from underlying data source

C1GridView
	C1 input editors are not able to see in grid when setting 'AllowC1InputEditors-True' and 'CallbackSettings.Action-Editing'

C1SuperPanel
	Fixed a superpanel issue for [57243]: scrollValue of scroller can not update when the scrollMax changed to be smaller enough.

C1Editor
	[60389] Unlike previous build, 'Next Page' link in Preview dialog of C1Editor is not working


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.159/4.0.20133.159)		Drop Date: 03/16/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.33

ControlExplorer
	Add samples of Excel and Pdf.

C1Charts
	[57268]ValueLabels text and styles of Axes lost after post back

C1CandlestickChart
	[52308]The data series added at design time are not displayed after adding new one at run time.
	[52075]Unlike other charts, SeriesStyles set in the first data series is also effect on the second data series.

C1BubbleChart
	Fix an issue same as 52308 that the data series display incorrectly after adding new one at run time.
	 
C1BarChart
	Fix an issue same as 52308 that the data series display incorrectly after adding new one at run time.
	
C1ScatterChart
	Fix an issue same as 52308 that the data series display incorrectly after adding new one at run time.

C1CandlestickChart/C1CandlestickChartExtender
	Remove the maxWidth property for it's not necessary, and the default value of this option is 15, if user want to set bigger width in the style, the style can't applied.
	Update the tooltip behavior to let user easily show the tooltip. 
	Rename the Hloc type to Ohlc type.
	Rename the raisingClose to risingClose in seriesStyles option

C1CompositeChart
	[56128]WijBubbleChart not getting rendered in WijCompositeChart.
	Fix an issue same as 52308 that the data series display incorrectly after adding new one at run time.

C1Charts
	Remove ThemeSwatch property from PPG.

C1Gauges
	Remove Theme and ThemeSwatch properties from PPG.

C1BarCode
	Remove Theme and ThemeSwatch properties from PPG.

C1EventsCalendar
	Add "EnsureEventDialogOnBody" property.
	[56234]callout pointer is not shown at the selected time slot

C1Editor
	[57203]The width of the title bar in the 'Insert Hyperlink' dialog is getting reduced, when it is opened in IE7 document mode.
	[57200]Hyperlink address and text are getting lost in the 'Insert Hyperlink' dialog, when certain style settings are applied to the hyperlink text.
	[57314]Heading1 (<h1> tag) text does not wrap in IE and FF.
	[57360]'Browse…' button in 'Image Browser' dialog box of C1Editor is not visible when bootstrap style is applied

C1Grid
	[50483]record ordering is not maintained after grouping is applied

C1BarCode
	Add a property named "ImageUrl" to provide a Base64 string for C1BarCode to be simply used with ASP.net control.

C1LineChart
	[50427]The hint bubble keeps on displaying even when the focus moves out of the wijlinechart, when the chart margins are set to 0.
	Fix an issue same as 52308 that the data series display incorrectly after adding new one at run time.

C1PieChart
	[50103]Label connectors not rotating with the wijpiechart on mobile devices.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.158/4.0.20133.158)		Drop Date: 03/13/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.32

C1BubbleChart
	Fix an issue(same as 54753) that the old data series are displayed after changing DataSource dynamically.

C1Gallery
	[56290][IE 10][IE 9] Images are not displayed in Gallery when clicking thumbnails

C1CandlestickChart
	Update the default tooltip text and style.
	[52229]JavaScript runtime error: Bad number format specifier is thrown if some AnnoFormatString vlaue is set for Axis
	[52619]"NaN/NaN/0NaN" is displayed as ValueLabels for Aixs after postback
	[52085]Unlike other C1Charts,Javascript runtime error:Unable to get property X ... is thrown if the chart series has no ChartXData for X property
	[52223]Unlike C1CandlestickChart,there is no High/Low/Open/Close properties for DataBinding for Candlestick/Hloc chart types in C1CompositeChart.
	[54753]Unlike C1BarChart,the old data series are displayed after changing DataSource dynamically.
	[56365]Uncaught TypeError: Cannot read property 'stroke-width'... is thrown if only one series style is set in C1CandlestickChart with multiple data series.
	[56555]JavaScript error  Unable to get property 'setTargets... is thrown with disabled hint in C1CandlestickChart

C1Editor
	[56485]"undefined"  is display instead of user entered "hyperlink text" in C1Editor after user input any hyperlink
	[52236]Clicking the toolbar button effect outside of C1Editor after switching the full screen mode
	[56811]Font settings does not change in IE

C1BarCode
	[52115]Request to remove 'Theme' combo and "Create new theme" link from smart tag since theme is not supported in C1BarCode
	[52187]Property descriptions are not shown for some properties of C1BarCode in Properties window of Visual Studio.

C1CompositeChart
	[52205]JavaScript error "Unable to get property length..." is thrown if C1CandlestickChart and C1PieChart are set
	[52220]Javascript Error is observe after "Postback" is occur when Candlestick chart type is included in C1CompositeChart in certain scenarios
	Fix an issue(same as 54753) that the old data series are displayed after changing DataSource dynamically.

C1Accordion
	[56604]JavaScript error is observed when C1LineChart control is inserted in the C1Accordion pane

C1GridView
	[49810]C1GridView display the time increased by one hour in a DateTime column, when using DST time and the displayed date is the first date of a month that falls on Sunday.
	[50090]Size of paging bar is set to very high
	[56497]Paging bar is not shown in Chrome browser
	[56498]Paging bar disappear after setting the "showGroupArea" option to true
	[56472][IE] Input editor flicker when clicking spin buttons when setting 'AllowC1InputEditors' property to true.
	[56729]Exception occurs on deleting a row when 'RowDeleting' and 'RowDeleted' events are set.

C1EventsCalendar
	[56500][IE11]Time slots are not correctly displayed
	[56400]Repeat dropdownlist is not fully displayed in New Event Dialog of C1EventsCalendar

C1RadialGauge
	[49629]Radius property of C1RadialGauge not working.

C1Combobox
	[52251]Duplicate CSS class name are applied to the dropdown list item
	[56801]Javascript error : "Unable to get property items" is thrown when opening the dropdown if C1ComboBox has multiple columns with ItemTemplate

C1Tabs
	[56300]Uncaught TypeError: Cannot read property ... is thrown and the tab item is shown beyond the C1Tab when changing 'Scrollable' property to False.
	[56288]Error "Uncaught jQuery UI Tabs: Mismatching..." is occurred when removing the tab item after sorting.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.157/4.0.20133.157)		Drop Date: 03/11/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.31

C1Carousel
	[56043]JavaScript error is observed after "Enabled" property is set to "False" while "Dots", "Slider" or "Thumbnails" pager types are set in carousel control

C1Upload
	[56121]Javascript error is observe after canceling the process while it is uploading

C1Eidtor
	[52052]Hyperlink is always inserted at the beginning in Wijeditor

C1CandlestickChart
	[54613]NullReferenceException: Object reference... is thrown if "C1ChartBinding" is added without "DataMember" or "Field".
	[52268]Data series cannot be saved and loaded.

C1BarChart
	[54613]NullReferenceException: Object reference... is thrown if "C1ChartBinding" is added without "DataMember" or "Field".

C1BarCode
	[53486]'Text' values set at run-time are not retained after post back.
	[53214]'BarCodeImage' property returns Null value when getting the image of barcode.
	[52185]Control UI cannot be seen on Design surface when drag and drop C1BarCode control.

C1Input
	[50765]When using 'RequiredFieldValidator', validation is performed for C1InputText as soon as it loses focus.
	[52087]PostBack is not occurred when control is lost focus in C1InputText and C1InputMask

C1Tabs
	[27848]Client side property "sortable" does not work.

C1ListView
	[56080]Filter bar is not displayed at run-time when 'Filter' property is set to true.

C1List
	[50273]Setting selected to false does not work.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.156/4.0.20133.156)		Drop Date: 03/07/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
jQuery Mobile 1.4.0
Wijmo 3.20141.31

C1CandlestickChart
	[55659]Unlike C1BarChart,"ShowChartLabels" property does not work
	[55678][IE] The series hover styles does not take effect to C1CandlestickChart

C1PieChart
	[49523]Negative series values getting overlapped with the positive series values in C1PieChart.

C1Upload
	[49091]Viewport jumps to the top of the page when the Cancel button in wijupload is pressed.

C1LineChart
	[49920]Area chart does not fill correctly when negative values are plotted

C1BarCode
	Add the toolbox icons.

C1CandlestickChart
	Update the toolbox icons and add PNG for ControlExplorer.

C1Eidtor
	[52041]'Syntax error' is observed when a hyperlink is entered manually and the insert hyperlink dialog is opened
	[49969]Javacript error is observe after uploading an image two times in "Image Browser" and "Insert Hyperlink" dialog.
	[53355]No validation when entering incorrect email or url in insert hyperlink dialog.

C1EventsCalendar
	[51376]Events from the first C1EventsCalendar getting added to the second C1EventsCalendar, when the events data is loaded from different xml files.
	[52032]Calling "Import" method to import an XML file throws "Value cannot be null" error.

C1CandlestickChartExtender
	Update the toolbox icons and add PNG for ToolkitExplorer.
	Fix the issue that CandlestickFormater doesn't work.

Mobile Control
	[52076]AppView is not display properly when browsing with IE10 or Firefox.

C1AutoComplete
	[51344]When C1AutoComplete is displayed in C1Dialog, then the drop down list of C1AutoComplete items appears below the C1Dialog.

C1Tabs
	[51860]When the alignment of WijTabs is set to left or right, then tabs are not getting switched if the user does not clicks on the text in the tab.

C1Combobox
	[52105]Html code is displayed in the dropdown list after typing text in C1ComboBox which has Item Template

C1GridView
	[51931]Focus is lost in IE8 after asynchronous post back when textbox is placed in C1GridView
	New property added: bool AllowC1InputEditors { get; set; }
	[52282]frozen bar position is not maintained after record selection.

C1Input
	[49354]Data lost on double postback in C1InputMask with Advanced mode.
	[52762]Script Error is occured when text is set from design time in the C1InputMask in which range is set as Mask
	[47884]Incorrect sample name ‘Hirigana’ is observed when opening C1InputMask samples

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.154/4.0.20133.154)		Drop Date: 02/21/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.30

Support jqm1.4 in controls.
Add C1CandlestickChart and C1CandlestickChartExtender.
Add C1BarCode.

Bootstrap
	[50028]Custom BootStrap theme is not applied on the page containing Wijmo server controls.

C1Input
	[48136]Wijmo controls do not appear in the Controls dropdown when adding a Where clause to the C1ComboBox datasource configuration

C1Eidtor
	[50733]The anchor radio button is automatically selected if the insert hyperlink dialog is closed and reopened again
	[49862]Unlike previous build, clicking the toolbar button effect outside of C1Editor after switching the editorMode

C1Menu
	[49677]Text appears before SubMenu is displayed in IE8.
	[48197]ItemClick event fires for disabled MenuItems

C1ComboBox
	[49116]Deleted Text is retained after Postback in C1ComboBox editor.
	[49999]Unlike previous build, background of dropdown are not display after dynamic data is set in Dynamic Data sample page
	[51335]Unlike previous build 120 ,error "A  potentially dangerous ..." is thrown in C1ComboBox with Item Template after Postback occurred
	[49909]Unlike previous build, dropdown is not correctly aligned with the C1ComboBox.
	[48136]Wijmo controls do not appear in the Controls dropdown when adding a Where clause to the C1ComboBox datasource configuration.

C1List
	[50255]Unlike previous build, autoSize property does not work.

C1GridView
	Added new property ShowClientSelectionOnRender.
	[50496]Using Tab keys to navigate between cells does not scroll the column headers when "staticColumnIndex" is set.
	[51530]With ClientEditing enabled, double clicking an empty C1GridView cell displays "&nbsp;" instead of blank value.
	Filtering is now triggered when Enter key is pressed.

C1GridExtender
	Added new property ShowSelectionOnRender.

C1SuperPanel
	Fixed an issue that if "customScrolling" is true, the grid in superpanel may not be able to scroll to the last item.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.153/4.0.20133.153)		Drop Date: 01/23/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.29

C1SuperPanel
	[46340]Javascript Error is observed after browsing WidgetExplorer with IE8

C1AppView
	[47951]'Custom menu' is display on the left side of wijappview which overlap the menu of wijappeview.

C1CompositeChart
	[49875]Javascript Error is observed after adding pie chart type in the C1Composite Chart.

C1ListView
	[46433]Clicking 'C1ListViewNestedItem' is not navigated to its child items when C1ListView is placed in C1AppViewPage.
	
C1EventsCalendar
	[49676]"New Event" dialog is closed down on navigation the months in calendar dropdown in IE11.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.152/4.0.20133.152)		Drop Date: 01/21/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.28

Add some Japanese resources.

C1EventCalendar
	[49575]Calendar stop working if disabled is set to false
	[49394]setting viewType to "month" does not work

C1AppView
	[49149]Javascirpt Error is observed when navigating to any appview page items.

C1TreeView
	[48510]NavigateUrl property of TreeView nodes is not getting set using the nodes option.

C1Editor
	[48857]C1Editor footer bar is not refreshed on switching the editorMode
	[48389]the controls inserted from C1Editor is display outside the boundary of C1Editor when it is placed at the bottom of the web page in IE10
	[48114]font size combo items are not correctly displayed and Javascript error is thrown on clicking if set "mode" option to bbcode with javascript.
	[49236]cursor is shown in disabled C1Editor
	[49361]Javascript error [Unable to get property 'createRange' of undefined or null reference] is thrown on clicking "Block Quote" button in toolbar if mode=bbcode in IE11

C1GridView
	[49376]IE browser crash after entering text in filter textbox of String column data type in C1GridView.
	[49541]Dialog appears even through "Cancel" button is clicked when running "HowTo" sample "C1GridView_DialogEditing".
	[48058]A javascript exception thrown from a C1GridView in a UpdataPanel.
	[49550]Javascript error[Unable to get value of the property "type": object is null or undefined] is thrown on dragging the scollbar after cell editing if AllowVirtualScrolling = true.

C1SuperPanel
	[48961]the data is not shown in C1GridView after the page is loaded in win8.1.
	Fixed an issue in case [49444] that horizontal scrollbar can not display in mobile device.

C1ComboBox
	[47944]Javascript error[Unable to set value of the property 'selected': object is null or undefined] is thrown on selecting combo item if selectedIndex is data bound
	[49233]dim UI is not observed in disabled C1ComboBox.

C1PieChart
	[43383]Hint is always visible and chart rotates on click on a Win8 machine with IE10

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.151/4.0.20133.151)		Drop Date: 01/13/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.26

C1AppView
	[37217]Javascript error is observe after clicking "Cancel" button or "Submit" button in "Widgest" sample of AppView.

Add some Japanese resources.

C1CompositeChart
	[46954]"StrokeDashArray" is automatically applied to a series if another series with different type has "StrokeDashArray" set.

C1ToolTip
	[48097]'aria-Describedby' attribute for the underlying DIV for WijTooltip shows "undefined".

C1ComboBox
	[46972]Javascript Error is observed after C1CombBox is placed with 'RequiredFieldValidator' control on the same page when 'SelectionMode' is set to Multiple
	[48212]Unable to set SelectedItem using C1ComboBox extender

C1EventsCalendar
	[48912]Total 7 days are displayed with Day/List view after changing view from Week view

C1Menu
	[46978]The submenu closes if the TriggerEvent is set to "mouseenter" and parent menu item is clicked.

C1GridView
	[49205]'<a href="javascript..." text appear in select command cell when clicking 'Select' button if 'AllowClientEditing' property is set true


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.150/4.0.20133.150)		Drop Date: 01/07/2014
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.24

C1ListExtender
	Added "Selected" property to ListItem class, added "SelectedIndex" property to C1ListExtender.

C1ComboBox
	[48380]The dropdownlist doesn't appear when the 'disabled' option is set to false twice on client side
	[48088]keyboard navigation does not work if Selected=true in a comboitem.
	[47349]underlying data source is not correctly updated if selectedValue is databound.
	[47823]The whole "LabelText" property is not shown in C1ComboBox with Bootstarp style in Chrome
	[46136]Incorrect selection & hover styles are applied on dropdown items.

C1Charts/C1Gauges
	[48360]Request to remove 'Use Create new theme…' links and 'Use BootStrap' style in smart tag of all Charts and Guages which does not support themes

C1Editor
	[48008]Javascript Error is observed after setting 'ShowFooter' property to false and 'FullScreen' mode
	[47882]Javascript error is observed after clicking on 'Source View' button when C1Editor is in full screen mode with IE11.
	[47841]Javascript error is observed after right clicking on the text area of C1Editor when browsing with IE11

C1GridView
	[44177]"Pager" disappeared and shown blank data in the grid when visible from unvisible state.
	[28078]Unlike MS-GridView and other C1 Controls, C1GridView disappear after toggling the Enabled property of the parent C1Tabs control.
	[48210]Javascript error observed while selecting a row with C1Input controls as ItemTemplate for C1GridView placed inside an update panel.

C1EventsCalendar
	[46180]Appointment dialog opens up with a flicker in IE11

C1InputMask
	[47844]]System.ArgumentException: The value of Text is invalid is thrown after the page is post back if C1InputMask has non-default "PromptChar" and Advanced "FromatMode"

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.149/4.0.20133.149)		Drop Date: 12/26/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.22

C1RadialGauge
	[47347]In some cases changing the WijRadialGauge value from ViewModel forces the pointer to start from 0 again

C1BubbleChart
	Fixed an issue that index property in Symbol class doesn't work.

C1Editor
	[47124]'OK' and 'Cancel' buttons clip in Chrome and do not display in IE9 when open 'Insert Media' dialog

C1Tabs
	[46857]'show' event does not fire when creating WijTabs using AngularJS

C1Dialog
	[46179]javascript error is observed after postbback occur and clicking on the close button in C1Dialog when 'MaintainStatesOnPostback' property is set to true

C1SuperPanel
	[46393]When isTouchEnabled is true grid virtualmode scrolling is broken.

C1ComboBox
	[47371]right border is missing if TriggerPosition="Left".
	[46972]Javascript Error is observed after C1CombBox is placed with "RequiredFieldValidator" control on the same page when "SelectionMode" is set to Multiple
	[47257]setting SelectionMode=Single does not work correctly.
	[48096]item selection through keyboard does not work correctly if Selected=true in a comboitem.
	[46963]'System.FormatException' is observe after C1ComboBox is used with 'RequiredFieldValidator' control when 'AutoPostback' postback property is set to true
	[48024]In some cases the "onchange" event does not fire when a double byte character is selected from WijComboBox dropdown list

C1GridView
	[47777]The Available Columns list in GridView designer properties window does not show the last column completely.
	[47873]all the checkboxes present on C1GridView are changed into unchecked on pressing Esc key.
C1InputDate
	[47680]The none is list in highlightText option of C1InputDate control in design time.
	[46172]Unlike previous build, today date is not displayed as default when Date property is not set.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.148/4.0.20133.148)		Drop Date: 12/16/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.22

C1ComboBox
	[45163]Selected item is not rendered correctly when WijCombobox has multiple column & Aristo  is set

C1Editor
	[46389]boundary of C1Editor exceed the browser window screen on clicking the "Full Screen" button if fullScreenMode is set to true
	[41034][IE11] ForeColor and BackColor cannot be changed in C1Editor

C1Carousel
	[46300]How to alter the sequence of images in C1Carousel.


C1Gallery
	[42908][IE10]Unlike previous build, 'Next' and 'Previous' buttons are not display when C1Gallery is placed in the same page with Charts controls.
	
C1EventsCalendar
	[46138][Bootstrap] Appointment dialog is distorted.

C1BarChart
	[46539]legend item visibility is not toggled when clicking on it.

C1InputDate
	[46172]Unlike previous build, today date is not displayed as default when Date property is not set
	[46108]‘AMDesignator’ and ‘PMDesignator’ properties are not working in C1InputDate
	[45900]‘OnClientDropDownClose’ event fired twice after opening the dropdown calendar selecting any date

C1InputText
	[45606]‘AutoPostback’ property is not working in C1InputText


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.147/4.0.20133.147)		Drop Date: 12/6/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.21

C1ComboBox
	[46239]Javascript error [Unable to get value of the property "length": object is null or undefined] is thrown on clicking the trigger button after pasting the text into C1ComboBox.

C1Dialog
	[42672]'Unspecified error' error is observed in IE8 when the GridView with visible filter area is contained inside a C1Dialog

C1EventsCalendar
	[46932]Time line is displayed only till 9pm in 'Week' view if 'ViewType' is set to 'Month' initially

C1ThemeRoller
	[44550]Update fixing for inputting empty text.
	[46135]No message to confirm the overriding of a saved custom theme when saving with the same name and at the same location

C1TreeView
	Fixed an issue that child nodes are disappear after expanding the tree in IE8.

C1GridView
	[46081]HeaderStyle settings are not applied in C1GridView.
	C1BoundField.ImeMode property added.

C1Pager
	[46134]First & last buttons are not rendered on load.

C1Upload
	[42525]Request to provide a 'response' object in the 'complete' event

C1Editor
	[45984]IE ignores cursor placement when inserting text into editor
	[41031]Inserted elements are not displayed in C1Editor

C1InputText
	[46174]Unlike previous build 139, user entered text are not retain after postback occur in C1InputText when ‘ShowDropDownButton’ is set to true

C1InputDate
	[47009]Unlike build 136, “Error setting property ‘Date’ to value” error message is display at design time when C1InputDate is placed in the content of C1Expander control

C1InputMask
	[47010]The prompt characters between the numbers are disappeared after Post back in C1InputMask with "Advaced" FormatMode

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.146/4.0.20133.146)		Drop Date: 11/25/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.20

C1Calendar
	[44047]Unable to select date in C1Calendar when browsing with IE11.
	
C1Carousel
	[46270]paging buttons and navigation buttons are not correctly displayed when C1Carousel is placed in table

C1ComboBox
	[46239]Javascript error [Unable to get value of the property "length": object is null or undefined] is thrown on clicking the trigger button after pasting the text into C1ComboBox
	[46165]last character of LabelText is overlapped by the dropdown icon

All controls
	Compatible mode gets turned on in IE when using any Wijmo control (related issue: #36235).
	
C1Dialog
	[44245]WijEditor options which opens a dialog cannot be used when the WijEditor is placed inside an Ajax form and a modal WijDialog

C1Charts, C1Gauges
	[42189]Chart does not render correctly on mobile devices when the orientation is changed from Portrait to Landscape

C1ThemeRoller
	[44635][C1ThemeRoller]Update fixing.
	[44550]"ThemeRoller requires internet connection to work" message is display after deleting the color code in color picker textbox and click on anywhere to leave focus

C1Editor
	[44651]Request font list in WijEditor as an interface to change font at client side.

C1GridView
	[44238]Setting "FreezingMode" for column does not work in C1GridView when "WijmoControlMode" property is set to Mobile.
	[44283]Selected Row style is not display for selected row in C1GridView when "WijmoControlMode" property is set to Mobile.
	[44225]JavaScript runtime error "type property can't be changed" is thrown when "ShowFilter" property is True if C1GridView has "Date" column type
	[36235]Compatible mode gets turned on in IE when using C1GridView.
	[39957]Filter operator drop down portion doesn't closed and overlap the C1Calendar in "DateField" column  when clicking on calendar's dropdown button.

C1Slider
	[44253]'Stop' event does not fire when using WijSlider on touch devices.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.145/4.0.20133.145)		Drop Date: 11/13/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.20

C1Editor
	[45098][Bootstrap Style] Multiple issues are observed in C1Editor when applying Bootstrap style

C1Charts
	Fixed an issue that setting 'UnitMajor'/'UnitMinor' value for DateTime, the browser stops responding.

C1LineChart
	[44367] Visible option not working for wijLineChart SeriesList

C1Tabs
	[44956][BootStrap] The last tab item is remained at second line when set 'scrollable = true'.
	 
C1ThemeRoller
	[44635][C1ThemeRoller] Request to localize ThemeRoller in Japanese version of ASP.Net Wijmo Controls


C1InputDate, C1InputMask, C1InputNumberic, C1InputPercent, C1InputCurrency
	Add Japanese related features.

C1InputDate
	Add date & time picker.

C1InputText
	Add the new widget.


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.143/4.0.20133.143)		Drop Date: 11/13/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.19

C1ReportViewer
	Updated to latest C1Report

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.141/4.0.20133.141)		Drop Date: 11/12/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.19

C1GridView
	[45974]focus is always moved to first row after removing the record from the underlying data source.
	[45934]horizontal scrollbar is removed after leaving the client side edit mode in frozen C1GridView.
	[45894]grid lines of column headers are not correctly aligned with data row grid lines.
	[44183]Javascript error is observe after mouse hovering over the rows of C1GridView for several times.

C1ComboBox
	[45979]pressing "Backspace" key does not work.
	
C1BarChart
	[43594]the height of series lists get decreased after postback occur.

C1CompositeChart
	[43594]the height of series lists get decreased after postback occur.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.140/4.0.20133.140)		Drop Date: 11/11/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20133.18

C1ThemeRoller
	[45603]the sample image display for "metro-dark" and "metro" theme is not display in "NewThemeForm" dialog
	[45880]"Object reference not set to an instance of an object" exception is occur after selecting any "jQuery UI Themes" and clicking on "Next" button
	[45246]update fixing for an exception

C1Dialog
	[44928]Extra buttoon icons on the header is increased by one whenever maximizing the C1Dialog.
	[44927]Multiple Issues are observed after C1Dialog is applied with Bootstrap styles when others wijmo controls are placed on the same page with dialog.

C1Combobox
	[44997]'Highlight Matching' style is still effected in the drop down portion of C1ComboBox although items have been selected.
	[43591]unknown gray area is observed after selecting the combo items from multiple C1ComboBox instances.
	[44650]the background style of combo items in the drop down portion are changed and appear as gray instead of white.
	[45885]'Javascript' error is observed after entering second time in wijcombobox in 'Remote data' sample page.
	
C1ReportViewer
	[44913]"Outline" "Search" "Thumbs" tab disappeared and icon images in  toolbar button are not display.

C1EventsCalendar
	[45365]Date number are not display in navigation bar after 'Bootstrap' style is applied in C1EventCalendar when browsing with IE10
	[44744]"New Event" dialog is closed down when clicking a date in dropdown calendar of Start/End date

C1ListView
	[44365]If "ControlGroupType" is "radiolist", the InnerListControls of C1ListViewControlGroupItem are enabled although the "Disable" property of C1ListViewControlGroupItem is set as "True"
	
C1AppView
	[44475]'Visible' property of appview item is not working in C1AppView control.
	[44847]"NavigateUrl" and "SplitUrl" does not work if C1ListView is inside C1AppViewPage.

C1Gellery
	[44904]Cannot able to click play button in C1Gallery with 'Vertical' ThumbnailOrientation and Bootstrap mode

C1GridView
	[45691]cursor is not shown in C1ComboBox on clicking inside it when used as embedded editor in C1GridView

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.139/4.0.20133.139)		Drop Date: 11/8/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.16

C1ComboBox
	Fixed an issue that javascript exception is thrown after selected an item when AutoPostBack is true.

[C1Charts]
	[43195]Setting 'Max' value for DateTime axis does not work

C1CompositeChart
	[44521]AxisStyle stroke not getting set for multiple Y Axis in C1CompositeChart.
	[45567]Second axis is drawn to the max point of first axis if multiple Y-axes exist

C1BarChart
	[45903]'Click' event does not fire on mobile touch devices.

C1EventsCalendar
	[44735]Unlike previous build, width property does not work

C1ThemeRoller
	[44550]"ThemeRoller requires internet connection to work" message is display after deleting the color code in color picker textbox and click on anywhere to leave focus
	[45246]Some style are not effected to the wijmo controls at run time after user has selected "Wijmo Themes" from "New ThemeForm" dialog and edited it

C1GridView
	[45476]Unlike previous build, Javascript error [Unable to get value of the property 'find': object is null or undefined] is thrown on clicking the cell after leaving the edit mode and virtual scrolling.
	[45477]Unlike previous build, data cell gets disappeared on clicking the another cell after leaving the edit mode and virtual scrolling.
	[45460]Unlike previous build, unknown gray object is shown above the right most column header if allowVirtualScrolling = true.
	[45695]Unlike previous build, sort direction indicator is not shown on clicking the column in GroupArea.
	[45657]Unlike previous build, Javascript error[Can be used only if the editingMode option is set to "row".] is thrown on clicking the "Edit" button in data row
	[45639]Unlike previous build, Javascript error[Unable to get value of the property 'text': object is null or undefined] is thrown when C1ButtonField is added to the Columns collection.
	[45668]Unlike previous build, today date is not shown in the filter bar of datetime column.
	[44929][IE]The button styles are not properly shown and cannot collapse or expand the grouped items in C1Grid with Bootstrap mode.
	[44925]Sperators are not displayed between the headers of columns if UseBootStrap is true.
	[44963]If Bootstrap style is set, Records are overlapped with the Header of C1GridView when the records are scrolled vertically.
	[44973]The data row and column header are covered up with frozen line with Bootstrap mode.

C1Editor	
	[45132]Unlike preivous build, Splitter bar is not shown on clicking "Split View" button.
	[44276]C1Editor placed inside an update panel does not fire the 'TextSaved' event in some cases.

C1ToolTip
	[32288]If 'Theme=metro-dark' is set, 'CalloutFilled' style is not applied in 'C1ToolTip' even 'CalloutFilled' is set to 'True'

C1EventsCalendar
	[44903]Today background color, Time-slot lines and the down arrow button of vertical scrollbar are not applied in the Day View of C1EventsCalendar if UseBootStrap is true.

C1AppView
	[44473]Javascript Error is observed when clicking on any AppView item if 'AppViewPageUrl' property of any item is not set


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.138/4.0.20133.138)		Drop Date: 11/6/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.16

C1ComboBox
	[44419]text cannot be deleted from the textbox area of C1ComboBox
	[44324]keyboard navigation does not work if it consists of duplicate items

C1Gallery
	[44914]Button, border and background styles are missing C1Gallery with Bootstrap mode in IE.

C1ThemeRoller
	[44548]Unlike JQuery ThemeRoller, some shortcut keys are not supported in C1ThemeRoller
	[45076]Incorrect Spelling of "ThemeRoller" is shown in the dialog
	[44633]"metro-dark theme" is applied and downloaded instead of "metro" theme in C1ThemeRollerDesigner although "metro" theme is selected.
	[44792]Toolbar icons images are not display in C1Editor at run time when "Wijmo Themes" which is downloaded from C1ThemeRoller are applied
	[45139]Problem while navigating through the properties using keyboard keys

C1Editor
	[42969]Changing the text font size does not retain and revert back to default font size after postback occur when the text are in selection mode.

C1ReportViewer
	[42965]Bookmarks are not displayed in the exported PDF when C1Report is instantiated from C1ReportViewer

C1PieChart
	[32172]Labels are clipped if radius is set to null

C1ToolTip
	[39682]WijTooltip does not hide when trigger is set to any option except hover.

C1GridView
	[43727][IE9] Unlike previous build, filter dropdown trigger button is missing in last column of C1GridView which is placed inside C1SuperPanel.
	[44955]"OutlineMode" property of the GroupInfo does not work in C1Grid with Bootstrap mode.

C1EventsCalendar
	[44911]EventList from left pane is listed horizontally and only first 2 events are visible.

C1Rating
	[44902]"Reset" button is not display in C1Rating when setting to 'Bootstrap style'.

C1ListView
	[44543]The items and settings for binding properties are disappeared after closing "Bindings Collection Editor" by clicking OK button
	[44433]In C1ListView Designer Form, C1ListViewItems are able to be dragged and dropped, and then it is disappeared after dropped.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.137/4.0.20133.137)		Drop Date: 10/29/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.16

All controls
	Added bootstrap style.

C1Editor
	[42924] Text entered in full screen mode are not retain when postback occur
	[42970] Line break are not retain after postback occur when C1Editor is in 'Bbcode' mode
	[43133] Unlike previous build, user can type in the header of accordion pane if C1Editor is placed inside its Content

C1Combobox
	[42941] Unlike previous build, the deselected items is still display in text area of combobox after pressing ‘Tab’ key when  it is removed by close (x) button in IE10
	[43368] Unlike previous build, previously selected item is shown as selected in C1ComboBox dropdown
	[43438] Unlike previous build, selected item style is not correctly applied to the combo item if the underlying wijdatasource containing space
	[44326] Unlike previous build, textChanged event does not occur consistently

C1Dialog
	[42492] Setting 'stack' option to false at run time does not effect the wijdialog

C1Calendar
	[44407] When calendar is in decade view, set displayDate, calendar's title is wrong.

C1EventsCalendar
	[43981] Unlike previous build, status bar is dispalyed outside the C1EventsCalendar.

C1Charts
	[43608] Unlike previous build, charts are not rendered at correct axis origin value


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.136/4.0.20133.136)		Drop Date: 10/23/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.16

C1ReportViewer
	[43918] Unlike previous build, setting the height property does not work

Mobile Controls
	Add C1AppView/C1AppViewPage/C1ListView into Visual Studio's ToolBox.
	Add WijmoControlMode and ThemeSwatch property.

C1GridView
	[43360] Unlike previous build, sorting does not work after grouping is applied.
	[41813] Unlike previous build, calendar dropdown of C1Input is overlapped by frozen bar of C1GridView.

C1Editor
	[42886] "Full Screen" button UI is not shown in checked state even though FullScreenMode=true

C1RadialGauge
	[42503] Unlike previous versions, colorized ranges in WijRadialGauge appear jagged in the latest Wijmo version

C1ThemeRoller
	Correct the wrong merge from Development.

C1LineChart
	[42963] Gradient color is lost after WijLineChart is loaded in IE8


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.135/4.0.20133.135)		Drop Date: 10/14/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.14

C1Charts
	If Min/Max of Axis property is set, AutoMin/AutoMax will be set to false automatically.

C1Combobox
	[38415] 'selectElementWidthFix' option is not effected on width of the original HTML select element
	[42891] Unlike previous build, Javascript error[Unable to get value of the property 'replace': object is null or undefined] is thrown on pressing "Down" key after last comboitem is selected
	[42936] Unlike previous build, OnSelectedIndexChanged event does not work if C1ComboBox is placed inside ContentTemplate of UpdatePanel

C1GridView
	[42954] Unlike previous build, keyboard navigation does not work in Chrome browser.
	[42888] Unlike previous build, UI is not correctly maintained after setting data at runtime in ContentTemplate of UpdatePanel.
	[26846] Multiple issues are observed when C1GridView is placed inside C1Dialog.

C1EventsCalendar
	[41688][IE8] Events are not rendered correctly in IE8
	[42892] Unlike previous build, existing event dialog remained opened after navigating to other date by clicking on 'NavigationBar'

C1RadialGauge
	[42767] Memory leak is observed on rendering C1RadialGauge

C1Calendar
	[42890][IE10]Unlike previous build 133, Selected style is affected on hovering any date on C1Calendar

C1Wizard
	[42962] Unlike previous build, Javascript error [ Unable to get value of the property 'outerHeight': object is null or undefined] occurs

C1Accordion
	[42756][IE8] Unlike previous build 126, Javascript error is observed after clicking on the Accordion pane when browsing with IE 8

C1Editor
	[40702]WijEditor does not work correctly inside WijTab

C1ThemeRoller
	Add ThemeRoller for VisualStudio.
	[42180][metro-dark] Unlike released build [3.5.20131.115] , Design is crashed when set 'metro-dark' theme for all C1Wijmo Controls
	[42191][C1EventsCalendar] With Chrome, FireFox and Safari, Color dropdown from New Event Dialog cannot be opened after applying theme by using ThemeRoller
	[42188][C1EventsCalendar] View Types are displayed as radio buttons
	Fix the issue that in design time the background image cannot display, which is caused by fixing 42188
	[42228] Text from the TextBoxes to customize theme settings cannot be deleted by using “Delete” key
	[42234][C1Editor] The toolbar icons does not display and toolbar button gets distorted after applying jQuery UI themes downloaded from C1ThemeRoller
	[42236][C1AutoComplete] The width of AutoCompleteItem list is very wide and displayed with bullet
	[42239][C1Carousel][C1Gallery][C1Lightbox] The navigation button images and other images icons are not display properly for multimedia wijmo controls
	[42237][C1Dialog] Text and icons from dialog title overlapped and resize symbol is displayed on the bottom-left corner of C1Dialog
	[42230][C1Accordion] Setting custom theme disappeared at design page, if added accordion with content.
	[42182] 'Error invoking ‘Customize theme…’ .error is observe after clicking ‘Customize theme’ link in ‘Smart tag’of any controls
	[42196]Selecting any ‘Wijmo Themes’ and clicking ‘Next’ button in ‘NewThemeForm’ does not get apply on the C1Wijmo control or downloaded from online

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20133.134/4.0.20133.134)		Drop Date: 10/2/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.14

ControlExplorer
	[41934] Description is covered by the images in "Overview" sample of C1Tooltip in Control Explorer

C1Gallery
	[35611] Enable = false property doesn't work in C1Gallery when placed inside C1Expander.

C1Wizard
	[41816] Navigation does not work in Chrome and Firefox browser if C1GridView is placed inside C1WizardStep.

C1EventsCalendar
	[42231] Unlike previous build, "New Event" dialog is closed down on navigation the months in calendar dropdown
	[42178] Unlike previous build 131, existing events are not display in left pane of 'List' view
	[41607] Vertical scroll bar doesn't appear and "New Event" dialog box shown cutoff.
	[41519] Rectangle is appeared on Header when 'statusBarVisible' is set as 'true'.

C1Menu
	[41838][IE] Unlike previous build [3.5.20131.115]  If set Mode is sliding, doesn't correctly back to the page when navigate url.
	[41844] Setting disable MenuItem effect doesn't retain when post back occur.

C1Editor
	[41800] Unlike previous build, "Full Screen" button UI is not changed into checked state when clicking on it.
	[41758] 'Unable to get property "colSpan" of undefined or null reference' message box is display after clicking on "splitCell" button for more than two times
	[41717][IE] FontColor are not effected on Underline and Strikethrough when this style are apply first.
	[42252] Unlike previous build, splitter bar is not shown at correct position on clicking "Split View" button

C1Combobox
	[42229] Unlike previous build, ItemPopulate event is not working correctly.
	[42256][IE10] Unlike previous build 131, "InvalidCastException" is observe after postback occur when deleting the combo items with close(x) button
	[42012] Unlike previous build 130, 'OnClientTextChanged' event is not fired after entering from  keyboard when 'AutoComplete' property is set to false
	[42690] Unlike previous build, empty data gets displayed with smaller height
	[42691] Unlike previous build, Javascript error[Unable to get value of the property 'toString': object is null or undefined] is thrown on clicking the trigger button after selecting empty data

C1Tabs
	[42105] First tabitem is always shown as selected item after postback if C1Tabs instance is placed inside C1Dialog

C1GridView
	[42198] Unlike previous build, server side error [if a data source does not return DataView then it cannot be used to implement automatic sorting.] is thrown when C1GridView is bounded to OledbDataReader.

C1GridExtender, C1GridView
	FreezingMode property added.

C1Splitter
	[42323][IE10] Splitter cannot be moved if it is dragged and released in the middle of paragraph in the Split View of C1Editor.

C1Accordion
	[42246] If set c1EvCal as a content of Second accordion pane, Design is crashed.
	[42315] If set c1ReportViewer as a content of Second accordion pane, Design is crashed.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.133/4.0.20132.133)		Drop Date: 9/19/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.13

C1CompositeChart
	[41985] Request you to please add Area Chart to the available series types in C1CompositeChart

C1ReportViewer
	[41945] Error occurred when the ¡°C1ReportViewerChangeExportButtonBehavior¡± sample is rebuilt.

C1Gallery
	[41915][IE10] Unlike previous build 129, script error is occurred on mouse hover if "Mode" is not "img"

C1Reportviewer
	[41788] Unlike previous build [3.5.20132.130] ReportViewer Status text bar doesn't appear until re size with slider bar.

C1Tabs
	[41780] Unlike previous build 130, Child controls inside C1TabPages are disappeared after postback
	[41786] Unlike previous build, focus is shown in non-selected tabitem on clicking outside of C1Tabs
	[41970] Unlike previous build 130, script error is occurred on postback when a control is placed inside UpdatePanel in C1TabPage
	[42049] Unlike previous build, hidden tab items are getting shown after PostBack

C1Input
	[41616] Unlike previous build, ShowNullText and NullText properties do not work.

C1EventCalendar
	[41778] Unlike previous build 130, javascript error is observed after clicking on delete button which the event has not yet been created
	[41787] Unlike previous build, "Javascript" error is observed after placing C1EventCalendar inside C1Superpanel
	[41777][IE 10] Unlike previous build [3.5.20132.130] unable to select day in 'Day View' from Calendar.

C1Combobox
	[41779] Unlike previous build, oncallbackdatabind event does not work
	[41815] Unlike previous build 130, 'Null Reference Exception' is observed after postback is occur continuously when there are 3 or more C1Wijmo controls in same page.
	[41831] Unlike previous build,  previous selected item is still highlighted in the drop down after postback occur although this item has been deselected
	[41843] Unlike previous build, combobox is not deselecting previous selection after loading new data
	[41847] Unlike previous build 130, Javascript error is observe after opening the drop down button when Dynamic Data is set in C1ComboBox
	[41955] Unlike previous build, setting selectedIndex property at runtime does not work

C1Calendar
	Update the touch supporting source code.
	[41776][Win8] Unlike previous build 130, Today background color is not applied for Today Date.
	[32274] Setting the client side property "popupMode" does not work correctly.

C1List
	[41676] Unlike previous build, Javascript error [Unable to get value of the property 'element': object is null or undefined] is thrown on when C1List selected item is bounded to C1ComboBox

C1Editor
	[41800] Unlike previous build, "Full Screen" button UI is not changed into checked state when clicking on it


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.131/4.0.20132.131)		Drop Date: 9/11/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.10

C1LightBox
	[41591][C1LightBox][ControlExplorer] "QuickTime" sample of LightBox cannot be opened in Control Explorer.

C1ToolTip
	[30420][C1ToolTip] 'Height' and 'Width' properties setting of 'C1ToolTip' do not take effect.

C1Expander
	[35611][C1Expander][C1Gallery] Enable = false property doens't work in C1Gallery when placed inside C1Expander.

C1Wizard
	[41560][C1Wizard] Child Item ID is conflicted and occurs error by adding after close and reopen C1Wizard's Open Designer.
	
C1Carousel
	Fixed an issue that prevBtn/nextBtn will be convered by video in iframe. 

C1Dialog
	[41538][WijDialog] 'Restore' button is disappear and others buttons are appear after setting 'minimizeZoneElementId' option when wijdialog is in minimize state
	[41530][WijDialog] The buttons add in wijdialog are disappear after setting 'maxwidth', 'maxheight', 'minwidth' and 'minheight' at run time
	[39419][C1Dialog] Tooltip is not display for dialog when hovering over it although 'Tooltip' property is set.
	[41526][WijDialog] 'width' and 'height' of wijdialog is not resets to their default values although setting 'resets' method at run time

C1Tree
	[36650][WidgetExplorer][WijTree] Child nodes are disappear after applying 'Pulsate' animation in "Animation" sample page of WidgetExplorer.

C1Gauges
	[41562][All Gauge] Unlike previous build [3.5.20132.129] "UI" of ALL gauge are not display at design time .

C1EventsCalendar
	[41552][C1EventsCalendar] Unlike previous build 129, C1EventsCalendar is not displayed at Design View
	[41442][wijEventsCalendar] The list of events from the Left pane of List view can be clicked in the disabled EventsCalendar
	[41441][wijEventsCalendar] "Delete" button is not displayed in the Event Dialog for the events added from eventsData option

Charts
	[41550][All Charts] Unlike previous build 129, UI of all charts are not display at design time although adding series list

All Controls
	[41589][All Wijmo Controls][IE 8] Javascript error is observed after placing any wijmo controls and browse with IE 8

C1Input
	[41558][C1Input][IE9] Unlike previous build [4.0.20132.129] , JScript Error occurs on loading any C1Input control at run time

C1Slider
	[32245][C1Slider] Unlike Help file image, C1SliderBar's Track image is not retained at original position when 'Orientation' property is set to 'Vertical'

C1Splitter
	[41557][C1Splitter] Unlike previous build [3.5.20132.129] , unable to drag splitter bar to specific postion , it is revert back to original position.

C1Tabs
	[33910][C1Tabs] Adding and Removing the tab item at run time doesn't retain state when post back occurs.
	
C1Combobox
	[41490][C1ComboBox]Unlike previous build,'Text' property does not refresh when an item is selected for the second time.
	[33666]OnClientTextChanged event is not fired when existing combobox item are entered from  the textbox when "AutoComplete" property is set to true
	[41598][WijComboBox] 'TextChanged' event does not fire when text is changed using keyboard keys.
	[41643][C1ComboBox]Unlike build 126, the deselected item is again selected in the combobox at run time when "selected" property is set to true at design time
	[41642][C1ComboBox] Unlike previous build, selected item style is not removed from combo dropdown after selection is cleared.
	[41697][WijComboBox] Setting 'selectedndex' property using AngularJS does not work when the 'selectionMode' is set to multiple

C1Editor
	[41554][C1Editor][C1Tabs] Unlike previous build, Ribbon UI is not correctly maintained when C1Editor is created in C1Tabs
	[41551][C1Editor] Unlike previous build, Javascript error [Unable to get value of the property 'document': object is null or undefined] is thrown when "Insert Code" diloag is closed
	[41618][C1Editor][IE]Unlike previous build 127, the special characters gets selected and overwritten by second one after selecting it from "Insert special character" dialog
	[41646][C1Editor][Chrome][Firefox][Safari] 'String Not Found' message box is display after using 'Find & Replace' dialog when the words that user is searching is selected in C1Editor

C1GridView
	C1GridView can't edit the CheckBoxField when the AllowClientEditing property is turned on.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.130/4.0.20132.130)		Drop Date: 9/4/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.10

C1Menu
	[39815][C1Menu] After doing 'Click' trigger event, 'Mouse Enter' trigger event doesn't work correctly.

C1Pager
	[36081][C1Pager][IE10] script error is occurred when a button is clicked in C1Pager in the Overview sample of Control Explorer.

C1Splitter
	[41403][WijSplitter] Resizing is slow when WijSplitter contains WijGrid in one of the panes

C1Upload
	[41336][34620-139][C1Upload] 'ValidatingFile' event does not fire if JS code is registered server side in the event.

C1ProgressBar
	[38177][C1ProgressBar] 'Label' is partially show when 'FillDirection = North (or) South' and "LabelAlign = North".

C1Slider
	[37966][C1Slider] The slider value doesn't retain after postback occurs if setting  'Range = true'.

C1LightBox
	[41271][wijlightbox] controlsPosition property doesn't work when set at runtime.

C1EventCalendar
	[41225][wijEventsCalendar] Unlike previous version, Duplicate Events are created when an option is set in wijevcal
	[41255][wijEventsCalendar] New timeRulerFormat is not reflected in  wijEventsCalendar when 'timeRulerFormat' option is set at run time
	[41050][C1Tabs][C1EventCalender] Design of EventCalender is crashed when placed in  second tab page of C1Tabs.
	[41217][wijEventsCalendar] 'firstDayOfWeek' option does not work
	[37754][wijevcal] Overlapping appointments are positioned incorrectly
	[41489][wijEventsCalendar] Event is not updated by using "updateEvent" in wijevcal

C1Inputs
	[41124][C1InputMask]Unlike build 115, the existing valid value are disappear when focus has lost after entering invalid value in the C1InputMask.
	[41123][C1Input]Unlike build 115, postback is not occur when selecting a valid item from the drop down of InputMask when 'AutoPostback' is set to true.

C1InputCurrency
	[40873][C1InputCurrency] Value gets rounded off automatically if more than 15 digits.

C1TreeView
	[41139][WijTree] WijTree is not updated when the 'removeAll' KnockOut method is called

C1InputNumeric
	[39968][WijInputNumber] 'showNullText' property does not work when any text is entered and deleted from it.

C1Dialog
	[41156][C1Dialog] Label inside C1Dialog does not get updated when C1Dialog is placed inside UpdatePanel

C1Carousel
	[41032][C1Carousel] Able to click and image is moving although get dim state at Preview Area when set 'Enabled = false'.
	[41011][C1Carousel] The images are still moving when clicking on last thumbnail in disabled Carousel.
	[34802][C1Carousel] When images are navigated through pager in auto play mode, the images are still auto playing although  it is set to paused.

C1Tabs
	[41015][C1Tabs] Unlike previous build, size of the tab item header is not correctly maintained and icons are not shown at correct position

C1Combobox
	[40998][C1ComboBox] Unlike previous build, selected value is not shown as selected item
	[41004][C1ComboBox][IE 10]Unlike previous build 127, Javascript error is observe after deleting the items with delete (x) button and selecting and pressing "Enter"
	[41187][C1CombBox][IE 10]Unlike build 127, the item is not display in text area of wijcombbox after deselecting it with 'delete' (x) button in IE 10 and selected it again.
	[41319][WijComboBox]Unlike previous version 10, Javascript error is observed when opening the drop down button of WijComboBox after dynamic data is added through "data" option
	[33109][C1ComboBox] ToolTip does not appear on hovering over dropdown button or C1ComboBoxItem at run time
	[33132][C1ComboBox] Unlike 'ComboBox' of C1WebUI, 'Selected' property of 'C1ComboBoxItem' doesn’t take effect at run time when it is set in button click event using the item's ID.
	[38349][C1ComboBox] 'SelectedIndices.Count' return incorrect number 1 although there is no selected number in the C1ComboBox.

C1Gallery
	[37625][C1Gallery]Thumbnail is moved along and display as blank space although there is no thumbnail images when 'ScrollWithSelection' is set to true.
	[40604][Touch][C1Gallery] Unlick mouse click, auto play button disappear when touch the play/pause button to play.
	[40543][C1Gallery][IE10] Unlike IE 9, Next and Previous button doesn't appear until click the thumb link button.

C1Editor
	Fixed an issue that spell check error.
	[40391][C1Editor] The template applied in text area of C1Editor is disapper after postback occur
	[40388][C1Editor][Chrome][Firefox] The existing font style are not applied to the new replaced word when using 'Find And Replace' in C1Editor when browsing with Chrome
	
C1PieChart
	Added "EnableTouchBehavior" property.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.129/4.0.20132.129)		Drop Date: 8/15/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.10

C1Carousel
	[37623] 'Next' button and 'Pager' button doesn't work when set 'Display' option at runtime.

C1Wizard
	[37529] Child Item ID is conflicted and occus error if placing two or more  controls at the same page.

C1Tabs
	[37529] Child Item ID is conflicted and occus error if placing two or more  controls at the same page.

C1InputNumber
	[40695][34231] nullText is displayed if entered value is equal to minValue

C1Editor
	Fixed an issue that the "DictionaryPath" takes no effect when spell check.


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.128/4.0.20132.128)		Drop Date: 8/12/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.9

C1AutoComplete
	[40461] C1AutoCompleteItems are covered by C1Calendar.

C1Rating
	[40506] Selected rate is disappeared when increase 'Count' at 'Rating Mode' is 'Single'.

C1EventCalendar
	[20597] Validation for Start-time and End-time is not provided when Start-date or End-date is changed

C1Charts
	[32241] Javascript error [Unable to get value of the property 'data': object is null or undefined] occurs when LoadLayout method is called

C1Carousel
	[37490] wijCarousel doesn't get dim state although set 'disabled = true' at design time
	[37512] Setting "showControlsOnHover" option in wijcarousel does not work, if setting at runtime
	[39178] Able to click and image is moving although get dim state at Preview Area when set 'Disabled = true'.
	[37609] Pager button doesn't appear again, if set preview to false first before 'ShowPager' to true again.
	[34801] Next/Previous buttons are still working although C1Carousel is disabled when ¡°ButtonPosition¡± property is set to "Outside"

ControlExplorer
	[38131][ControlExplorer] Error message shown when open "ContentUrl" page of C1Expander in ControlExplorer.
	[35378][Control Explorer] 404 Error occurs when clicking 'Politics' link under Function page.

C1SuperPanel
	[40479] Superpanel is able to scroll by dragging 'scroll thumb' although set 'enabled' to false.

C1Dialog
	Fixed an issue that  title aligns to right when an External jQuery theme is applied.
	[40772] Calling 'open' after 'destroy' does not work

C1Grid
	[40417] Unlike previous build, filter dropdown is overlapped by the frozen line.
	[40429] Cancelling currentCellChanging event does not work correctly.

C1Editor
	[40514] Unlike previous build, C1Editor overlap the control which is placed right below of it.

C1Menu
	[40217] Flickering is observed when moving the mouse over menu items in Chrome browser
	[C1Menu] When using the sample in 40217 and clicking the first child item of the fifth item, it is expected to show the sub menu but the menu hides after showing

C1ComboBox
	[40450] Unlike previous build 126, deselected item is still selected in C1ComboBox when "AutoPostback" property is set to true in "Multiple Selection" mode
	[40482] Unlike previous build 126, controls added in ItemsTemplate of C1ComboBox are display as html tag after clicking on the drop down button of combobox
	[40478][IE 10] Unlike previous build 126, all the item that hover through are highlighted in the drop down instead of highlighting only the first item that is hover
	[40774] WijComboBox trigger does not appear correctly on Zoom
	[40441][IE 10] Muitple combo items are still highlighted in the drop down of C1ComboBox after postback occur when using ¡®x¡¯ button in IE 10

C1ReportViewer
	[38917] Buttons and Splitter of ToolsPanel can be clicked in the ¡°Enabled=False¡± C1ReportViewer

C1Tabs
	[40109] Unlike previous build, size and alignment of tab header items are not correctly maintained.

C1Inputnumber
	[39968] 'showNullText' property does not work when any text is entered and deleted from it.
	[39143] Unlike build [3.5.20131.102] Error occurs when set value for C1Input that is placed inside C1Tabs

C1GridView
	[39981] Sorting in C1GridView does not work if the underlying column name in datatable has special characters
	
C1Combobox
	[33109] ToolTip does not appear on hovering over dropdown button or C1ComboBoxItem at run time
	[24127] Unlike MS-DropdownList, different text are shown in selected textarea and listitem if closing/opening tags are included in text
	Fixed an issue that two combobox use the same data source, only the last combobox item can selected.
	
C1Dialog
	[30325][ControlExplorer] Dialog is resized and  does not shown properly when ¡°Show Animation¡± is set to Clip,Scale or Size
	
C1TreeView
	[39816][33177-139][IE10] A small dot is appear before the treeview which is hosted in accordion and a script manager is include in the page.

C1Editor
	[40137][IE10] Unlike previous build, IE10 crash after double-clicking inside C1Editor
	[40134] Unlike previous build, right border is missing in C1Editor if placed inside C1Expander
	[40133] Unlike previous build, typed text are not appeared in C1Editor if placed inside C1Expander
	[40123][IE10] Unlike build 115, IE10 crash after creating an event in C1EventCalendar and setting cursor in C1Editor
	[40407] Unlike previous build, focus gets lost from C1Editor after closing color dialog

C1EventCalendar
	Fixed an issue that changes made in existing events are not effected in wijEvcal when browsing with IE 10 and Firefox and the amplify is not referenced.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.126/4.0.20132.126)		Drop Date: 7/18/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1GridView
	[39491][IE] Unlike build 115, the last column  and its filter dropdown button in the grid are partially display and its filter drop down portion is overlap with vertical scrollbar
	[39952][IE9][Chrome][Firefox] Unlike build 115, javascript error is observe after clicking on up/down arrow scrollbar button continuously in Virtual Scrolling mode.

C1EventCalender
	[39955] Unlike previous build, current month name in navigation bar and header text cannot be seen clearly

C1UploadExtender
	[35877][C1Upload] Unlike build[3.5.20123.88], when open 'Upload' page  'Web Page Error' occurs in ToolkitExplorer

C1Tabs
	[39752] The last tab item is remained at second line when set 'scrollable = true'.

ControlExplorer
	[35189][Chrome] Remove function doesn¡¯t work and ¡®Remove¡¯ button is disappeared after clicking in ¡®ClientSide add and remove¡¯ sample

C1Editor
	[20291] Table is able to edit by placing cursor inside the cell instead of selection on the table.

C1Calendar
	[30863] Unlike design time, 'MonthSelectorImage' is disappeared at run time if 'Enabled' is set to 'False'.


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.125/4.0.20132.125)		Drop Date: 7/17/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1Accordion
	[ASP.Net Wijmo] Indicator of 'ExpandDirection' is showing opposite at design time.

C1EventCalender
	Support other types of datasource, (Entityframework's data for example).
	[34642] Unlike build 84 , Japanese localized warning message box is display for English build in English environment although setting Culture to en-US.

C1TreeView
	Fixed an issue that when drag all the children items from parent item, the parent ul element is not hide, and the parent item's CSS class is not correct.
	[36427] Unlike previous build 109, a gap space is left behind between parent nodes after dragging and dropping last child tree items in C1TreeView
	[36964] Unlike previous build, inconsistent spacing is shown in between of sibling treenodes after drag drop

C1InputMask
	[39890] C1InputMask shifts towards right when IE9 is run in IE7 Compatibility Mode
	[39891] C1InputMask is positioned incorrectly when IE9 is run in IE7 Compatibility Mode

ControlExplorer
	[39796] Documentation Links from the Samples of C1Input controls are incorrect and "File or directory not found" error is occurred when each link clicked 
	[38151] Unlike previous ControlExplorer, "Change" button is missing in "Temperature" Page of C1LinearGauge.

C1Editor
	[39937][Chrome][Firefox][Safari] Unlike build 119, focus is still appear and able to edit in C1Editor although ¡°Enabled¡± property is set to false at run time

	
=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.124/4.0.20132.124)		Drop Date: 7/15/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1Editor
	[38320][32732-139][Chrome][Firefox]The cursor loses focus after entering any text and click any option from the toolbar like bold or italic

C1Combobox
	[39687][Forum-Community]HoverStyle is not applied on all WijComboBoxes except last one when they are bound to same datasource

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.123/4.0.20132.123)		Drop Date: 7/14/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

Added mobile mode for ASP.Net mobile controls, use property "WijmoControlMode" to switch from Web mode to mobile mode.
Added ThemeSwatch property to set swatch in mobile mode.
Added C1AppView and C1ListView mobile controls.

C1EventsCalender
	[39826][Win8+IE10] Unlike previous build, the item from ‘repeat’ dropdown and ‘calendar’ dropdown cannot be selected after scrolling down the browser
	[39717][IE10] Unlike previous build, color dropdown button is shifted to right-hand side on opening the New Event Dialog

C1CompositeChart
	[39735] Unlike previous build 121, PieChart in C1CompositeChart increase in size and overlap other chart data series

C1GridView
	[38952] Unlike previous build, Javascript error [Unable to get value of the property 'length': object is null or undefined] is thrown if wijdatasource is used
	
C1Editor
	[39749] Unlike previous build, selected item is not shown in selected item style in FontName/FontSize comboboxes and Ribbon Toolbar in C1Editor


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.122/4.0.20132.122)		Drop Date: 7/10/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

All controls
------------
Fixed an issue of checking if touch is enabled in IE10.

C1Inputnumber
	[39298][Forum-Community][WijInputnumber] Wijinputnumber renders with dual spinners in Chrome
	Wrote DbValueTypeConverter for DbValue proprty and hid the property in design-time.

C1EventCalender
	[37748][ASPNET Wijmo][C1EventsCalendar][IE10] Unlike previous build, new event dialog is overlapped with time slot vertical scrollbar

C1PieChart
	[37755][ASPNET Wijmo][C1PieChart][IE 10] Unlike previous build, hint text is always shown above the C1PieChart and hint text is not shown on hovering the mouse on pie slice.
	
C1SuperPanel
	[38914][C1SuperPanel]Unlike build 115, Javascript error is observed after  pressing arrow key when "KeyboardSupport" property is set to true
	[39614][C1Superpanel][IE 10] Unlike build 115, "MaintainSrollPositionOnPostback" property is not working anymore when browsing with IE 10

C1ComboBox
	[38753][ASPNET Wijmo][C1ComboBox][IE10] Unlike previous build, DropdownHeight property does not work in opening the dropdown for second time
	[39016][ASPNET Wijmo][C1ComboBox] Unlike previous build, Focus method does not work
	[39459][ASPNET Wijmo][C1ComboBox][IE10] Unlike previous build, OnItemPopulate event does not work

ControlExplore
	[38768][Asp.net Wijmo][C1Tooltip][ControlExplorer]Unlike previous sample, other message is shown instead of tooltip in 'ToolTip's Ajax page.

C1DataSource
	[36313][ASPNET Wijmo][C1DataSource] Unlike previous build, Javascript error[Unable to get value of the property 'prototype': object is null or undefined] is thrown if C1DataSource object is instantiated

C1Dialog
	[38911][ASPNET Wijmo][C1Dialog] Unlike previous build, PostBack occurs on clicking the button in C1Dialog present inside UpdatePanel
	[39408][C1Dialog]Unlike previous build 115, "Stack" property is not working any more although setting one of the dialog to false

C1Menu
	[38762][ASPNET Wijmo][C1Menu] Unlike previous build, border is invisible in dropdown menu

C1GridView
	[26387][ASP.Net Wijmo][C1GridView] Expand icon is shown in collapsed group header when "ShowRowHeader" property is set true in scrollable grid.
	[38898][C1GridView][IE 8] Scrollbar updown arrow button is not working although clicking on it when browsing with IE 8
	[39285][32782-139][32639-139][C1GridView][C1InputDate] The DateTime values in grid datetime column changes when "timezones" of local machine changes

C1Accordion
	[39197][C1Accordion]Unlike previous build, postback is not occur when clicking on the pane although setting "AutoPostback" to true when "RequireOpenedPane" is set to false

C1BubbleChart
	[38353][C1BubbleChart] SeriesStyles rotation is not effected in run time of C1Bubble Chart

C1BarChart
	[38351][C1BarChart] The bar series are rotated after hovering over it although only "rotation" option is set in SeriesStyles and not SeriesHoverStyle


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.121/4.0.20132.121)		Drop Date: 7/2/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1Tabs
	[38176] Unlike previous build, item focus is shown in other tab item when a tabitem is doubleclicked

C1GridView
	[34917]  Unlike previous build, incorrect data value is shown in group header row while C1GridView is in edit mode
	[38140][Win8] Unlike previous build, scrollbar is not displayed although ScrollMode is set as "Both" in C1GridView
	[37703] Unlike previous build, focus jump to first row after removing the last row.
	[38758] Unlike previous build, width and height of grouped rows are not correctly maintained
	[38760] Unlike previous build, ScrollMode property does not work.

C1EventsCalendar
	[38766] Unlike previous build, Object has no method live error occurred in the DataModel sample of EventsCalendar in Control Explorer
	[38752] Unlike previous build, The events more than 100 events is not loaded in the ListView

C1Dialog
	[38875][Chrome][Firefox]Unlike build 115, C1Dialog is not dim although it is disabled when browsing with Chrome
	[38874][IE]Unlike previous build, Javascript error is observed after setting Draggable property and Enabled property to false
	[38800]Unlike build 115, Javascript error is observed after setting ¡°MaintainStatesOnPostback¡± property of C1Dialog to true.

C1Editor
	[38905][IE10 Compatibility View] Unlike previous build, Script error occurred if C1Editor is browsed

C1ReportViewer
	[38906]Unlike previous build,  tools panel is not correctly displayed


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.120/4.0.20132.120)		Drop Date: 6/28/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1ComboBox
	[38061][Win8+IE10] Unlike previous build 115, Dropdown is disappeared if the scrollbar of the dropdown of C1ComboBox is clicked or dragged

C1InputDate
	[37520][32081-139] The InputDate control does not loose focus and shift the focus to another control when the Tab is pressed for the first time

C1EventsCalendar
	[38210][IE10] Unlike previous build, Javascript error[Unable to get property 'start' of undefined or null reference] is thrown on clicking the event after deleting an event
	[38214][IE10] Unlike previous build, scrollbar is not shown in listbox containing created events

C1Calendar
	[38315][Localized] Request to localize 'Wk' into 'ßL' in C1Calendar for Japanese build

C1GridView
	[38379] Unlike previous build, C1Input does not work if used inside EditItemTemplate of C1GridView
	[38339] Unlike previous build , data values are not shown in DateTime columns present in C1Band
	[38170] Unlike previous build, customFilterOperators does not work
	[38501] Unlike previous build, "NaN (???)" is displayed in grouped row on clicking the edit link.
	[38471] Unlike previous build, Javascript error [Unable to get value of the property 'innerHTML': object is null or undefined] is thrown when data is set at client side.
	[16982][8080-139][25915-139] Request to provide a calendar in the filter bar for date/time column
	[35400] StaticColumnIndex and StaticRowIndex value can still be set by dragging the line at run time even though gridview is disabled.

C1Editor
	[38045] Unlike previous build, toolbar buttons of disabled C1Editor are not correctly disabled
	[38258] Unlike previous build, focus is not shown in C1Editor at form load

C1Dialog
	[22371][IE9] 'MicrosoftJScript runtime error' is generated when adding 'ContentUrl'
	[38166] In IE9, JS errors are observed on setting 'ContentUrl' to websites which uses jquery

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.119/4.0.20132.119)		Drop Date: 6/24/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1Slider
	[37970][Chrome] Unlike previous build [3.5.20131.115], when set 'Enabled = false' for c1slider, not only slider but also  others controls get unable state.

C1ReportViewer
	[38042][IE][Firefox]Unlike build 115, Unable to select the item in 'Subset' combobox in 'Print' dialog of C1ReportViewer

C1GridView
	[34319] [28128-139] DateFormat are display incorrectly in gridview when browsing with Safari on iPad.
	[36464] [31204-139][IE 7] Loading Text property is not working properly when browsing with IE 7 or IE 8 compatible view mode.
	[37744] [32010-139] Updating a blank field with DateTime data in jQuery does not update the cell in C1Gridview.

C1BubbleChart
	[37835] Unlike build 115, data series maker are left behind after hovering the mouse over the marker when Roation is set in SeriesHoverStyles

C1EventsCalendar
	[38122] Unlike previous ControlExplorer, Parser Error occurred and some C1EventsCalendar samples cannot be loaded in the Control Explorer



=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.118/4.0.20132.118)		Drop Date: 6/19/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20132.8

C1Charts
	[37886] [32398-139] Numbers and date value are display incorrectly in C1BarChart when culture property is set to se-SE.

C1Menu
	Update the wijmenu to prevent the Javascript error with IE7 and IE8.
	[37175][31731-139] Sub menu is shown behind the ocx control grid when C1Menu is place above ocx grid

C1EventsCalendar
	[37706] Unlike previous build, color dropdown is automatically shown on opening the New Event Dialog for second time
	[37697] Unlike previous build, the whole list got hover effect instead of applying hover effect to individual event of the list

C1ReportViewer
	[37707][IE][Firefox] Unlike previous build 115, content in side of ToolPanels are not display when browsing with IE10, IE9 and Firefox

C1Input
	[37761] Unlike previous build, popupPosition property does not work

C1Editor
	[37727] Unlike previous build 115, splitter bar is unable to drag to the top in split view  when C1Editor is in full screen mode
	[37794] Unlike previous build, lower half of the control present above the C1Editor is covered up by gray area if Enabled=false in C1Editor

C1InputMask
	[37728] Unlike previous build, Server error occurred in C1InputMask control after postback

C1GridView
	[37810] Unlike previous build, Javascript error [Unable to get value of the property 'refresh': object is null or undefined] occurs when grouped column is removed.

C1TreeView
	[37804] Unlike previous build, disabled property does not work correctly


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.117/4.0.20132.117)		Drop Date: 6/11/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20131.7



=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20132.116/4.0.20132.116)		Drop Date: 6/10/2013
=============================================================================================

Dependencies
------------
jQuery 1.9.1 
jQuery UI 1.10.1
Wijmo 3.20131.7


C1GridView
	[36821] [ASPNET Wijmo][C1GridView] Javascript error[Unable to get value of the property 'rowType': object is null or undefined] is thrown on dragging the frozen bar for second time.

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.115/4.0.20131.115)		Drop Date: 6/6/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.1

C1Menu
	Update the wijmenu to prevent sub menu display behind the activex control.
	[37175][31731-139] Sub menu is shown behind the ocx control grid when C1Menu is place above ocx grid

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.113/4.0.20131.113)		Drop Date: 5/24/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.1

C1BubbleChart
	[27539] LabelStyle properties of ChartLabel are not affected in C1BubbleChart when ChartLabel.Position is Inside
	[27516] Images set for Markers Symbol are disappeared after post back occurs

C1Accordion
	[34329] Request to provide Description in the Properties Window  for some properties of C1LinearGuage, C1Menu and C1Accordion

C1CompositeChart
	[34615] Unlike other chart type, ChartLabels are not displayed for Scatter Chart Series although "ShowChartLabels" property is set as "true" for C1CompositeChart

C1LineChart
	[30015][25525-139] DataPoints are shown outside ChartArea if AutoMax=false and Max property is less than the highest  data point value
	[35743] Line is shown below x-axis if Axis.Y.Min is greater than minimum Y


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.111/4.0.20131.111)		Drop Date: 5/9/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.1

C1EventsCalender
	[35817][IE10] Unlike previous build, Confirmation Dialog is displayd twice if Repeated event is reopened

C1Editor
	[35931] Unlike previous build, postback occur on pressing Enter key if EditorMode=Code

C1Calendar
	[36096] Tooltips for "Quick" NavButtons are still not localized

C1Pager
	[35839] Unlike previous build, Script Error occurred if C1ScatterChart and C1Pager are placed in same page

C1Ratting
	[36013] Unlike previous build, Script Error occurred and all Chart controls are not displayed if C1Rating control is placed at the top of the page

C1CompositeChart
	[35610] The second data series are  not hidden although clicking on the legend when datetime data value type is set in X-axis
	[34734] Unlike Scatter Chart, the icons from plot area and the legend icons and are inconsistence in Scatter Chart Series of C1CompositeChart

C1RadialGauge
	[36403] C1RadialGauge shrinks on using Gauge ranges and removes range indicator if range is set explicitly

C1Accordion
	[36063] "Uncaught ReferenceError : nextContent is not defined" error occurs in 'Client Object Model" page.

C1BubbleChart
	[35809] SeriesStyles.Fill proepty doesn't effect on legend in BubbleChart.



=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.109/4.0.20131.109)		Drop Date: 4/26/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.0

C1Upload
	Added EnableSWFUpload to force SWF upload in all browsers. This can be used instead of EnableSWFUploadIEOnIE if you want consistent behavior for all browsers.

C1Calendar
	[30863] Unlike design time, MonthSelectorImage is disappeared at run time if Enabled is set to False.


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.107/4.0.20131.107)		Drop Date: 4/24/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.0

C1Upload
	[35799] C1Upload's OnClientChange event is not fired when 'EnableSWFUploadOnIE' is true

C1Editor
	[34622] Toolbar is not correctly disabled when EditorMode=Code

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.106/4.0.20131.106)		Drop Date: 4/19/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.0

All controls
	Display control type instead of control id for Evaluation Version.

C1Expander
	[21103] The height of 'Expander' is not enlarged at runtime even its frame seem to  be  larged

C1Eventscalendar
	[33954] [Chrome][FireFox] Page is navigated to Home page after clicking the Cancel button of New Event Dialog in the samples of EventsCalendar except the Overview sample

C1Carousel
	[35399] Thumbnail Images are not display in C1Carousel when there is ~ symbol set in Thumbnail.Images property

C1Input
	[35500] Focus does not change when Tab key is used to move cursor in Firefox

C1ProgressBar
	[35235] image doesn't show at design time that is set in "IndicatorImage" property.

C1ReportViewer
	[30934][IE10 Compatibility]  Search button appears along with the scrolling when the vertical scrollbar of Search pane is dragged up and down

C1BubbleChart
	[27611] Symbol marker image is shown at run-time even when Visible property of bubble chart series is set to false

Video
	[34665][IE9] JScript error occurs when clicking on 'Full Screen' button on the Track bar of C1VideoExtender in ToolkitExplorer.

C1GridView
	[35644] Javascript error is observed after calling DataBind method in the grid which have C1TemplateField in C1Band columns.


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.105/4.0.20131.105)		Drop Date: 4/11/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.0

C1EventCalendar
	[33884] [Win8][IE10] Unlike previous build, new event dialog windows is not shown on clicking the time slot

C1GridView
	[35397] [Chrome][Firefox][Safari] Unlike build 94, Select/Edit Button are not working when ‘CallbackAction?are set for Editing and Selection in C1GridView.
	[35576]  Script Error occurred after scrolling by using mouse wheel if "AllowVirtualScrolling" is set as "True".
	[35221] [29016-139] Javascript error is observed after filtering a value from the column which is bind to object data source.

C1DropDown
	[34280] Button and comboBox stay highlighted in IE and FireFox

C1ReportViewer
	[34460] Extra blank page is printed when C1ReportViewer contains only 2 pages

C1CompositeChart
	[35610] The second data series are  not hidden although clicking on the legend when datetime data value type is set in X-axis


=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.104/4.0.20131.104)		Drop Date: 4/4/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.4.0


C1Editor
	[35318] Unlike previous build, user can type the text in Header of C1Expander if C1Editor is present in the Content of C1Expander.

C1TreeView	
	[35366] Unlike previous build, selection gets lost after cell value is edited

C1CompositeChart
	[34279] Dataseries does not match the legend entries

=============================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.103/4.0.20131.103)		Drop Date: 3/27/2013
=============================================================================================

Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.9

C1Input
	[33767][28260-139] “InvalidCastException:Specified cast is not valid?error is observed after binding C1InputNumeric to int/decimal values in item template of Form view
	[35045]  Unlike previous build, trigger button is displayed outside the C1Menu if C1InputDate is placed in the Template of C1MenuItem.

C1Input
	[35209] Unlike previous build, no item shown in combo dropdown after selecting an item

C1Combobox
	[33736] Values are not display in C1ComboBox when setting ComboBoxBinding with SelectedValue in Item Template of C1GridView

C1Editor
	[35323] Unlike previous build, font name /size comboBox is overlapped with Header of C1Expander if C1Editor is placed in the Content of C1Editor

C1Calendar
	[33632] Unlike previous build, weekday header is overlapped by the control present right above the C1Calendar if showTitle = false


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.102/4.0.20131.102)
==================================================================================

C1Editor
	[35309] 'Height'& 'Width' attributes of "img" tag does not change when image is resized in design view

C1Calendar
	[34746] Unlike previous build, calendar dropdown is not correctly shown on clicking the trigger button after resizing the browser window
	
==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.101/4.0.20131.101)
==================================================================================

C1GridView
	[34758] Spinner buttons of C1InputNumeric are not correctly shown if used an editor in C1GridView.
	[34749] Border is missing if C1Input is used as an editor in C1GridView.

C1BarChart
	[34732] Data series are not display when setting Y-axis to Date Time value

C1ReportViewer
	[34660] Extra blank page is printed when C1ReportViewer contains only 2 pages
	[34591] C1ReportViewer does not work with IE 6.0 and IE 7.0

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.100/4.0.20131.100)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.9

C1Gallery
	[27433][IE9] If ShowTimer property is set to false, image showing time is different

C1Upload
	[32210] If files upload is cancelled for first time, 'Upload progress bar' still appears although files upload is completed in second time
	[30624] Able to upload more than allowed maximum number of files if EnableSWFUploadOnIE is set to True

C1Menu
	[34506] Unlike previous build, Javascript error [Unable to get value of the property 'getParent': object is null or undefined] is thrown on clicking header link after navigating to sub menu item

C1List
	[33556] Multiple issues are observed with 'autoSize option at run time

C1Charts
	[24364] Although chart label is display as required culture specific settings but Hint Value is not display as required culture setting

C1GridView
	[34607] Setting the Row height does not work when 'StaticColumnIndex' property is set.
	[30647] Style of header row of band row cannot be changed.
	[34635] Unlike previous build, row height is not correctly maintain after clicking the group collapse icon

C1Editor
	[34193] The lay out of settings screen of font color gets crumbled in IE6

C1Combobox
	[34695] Unlike previous build, ForceSelectionText property does not work
	[34689] Unlike previous build, OnClientChanged event does not fire correctly

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.99/4.0.20131.99)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.9

C1ToolTip
	[27371] Callout element is still appeared in 'C1ToolTip' even Client-Side property of 'showCallOut' is set to 'False'

C1Carousel
	[27408] In Design time, CarouselItems Name is automatically changed After pressing F2 and focus lost on Carousel Item in Carousel Designer Form

C1Splitter
	[32424] Right border of Panel 2 is missing at run time if CollapsingPanel of C1Splitter is set to Panel2 and Orientation is set to Vertical.

C1Tabs
	[34061] The value of 'C1Tabs.Selected' property becomes incorrect when there is a hidden tab.

C1PieChart
	[34303] Unlike previous build 97, the slices of pie chart are distorted at design time.
	[34308] Unlike previous build 97, Animation effect is not display in pie chart when hovering over the slices although Animation.Enabled is set to true.
	[34307] Unlike previous build, hint text is always shown above the C1PieChart and hint text is not shown on hovering the mouse on pie slice.

C1Treeview
	[34305] Unlike previous build, drag drop action does not work on second time.

C1Dialog
	[34342] Dialogs button is clickable and dialog can be resize although setting Enabled property to false
	[26788] Dialog does not appear according to the position value after postback is occur

C1Input
	[34340] Unlike previous build,  selected date is not shown as selected text in textbox of C1InputDate after opening the dropdown calendar
	[34362][28816-139] Setting the Date property for C1InputDate in DateChanged event of another C1InputDate is not working

C1FormDecorator
	[34280] dropdown stay highlighted in IE and FireFox

C1Upload
	[32210] If files upload is cancelled for first time, 'Upload progress bar' still appears although files upload is completed in second time
	[30636] Able to upload more than allowed maximum file size if EnableSWFUploadOnIE is set to True.
	[30624] Able to upload more than allowed maximum number of files if EnableSWFUploadOnIE is set to True
	[24678] MaximumFiles property doesnt reset to default value 0
	[29566] Files can be uploaded even  Enabled is set to False

C1LineChart
	[33834] Same display for different 'FillOpacity' value settings  and also same display for 'Opacity' and 'FillOpacity' settings.

C1GridView
	[34226] Unlike previous build, keyboard navigation does not work if rows contain checkbox.
	[33772] [28262-139] Request to provide “ColumnByName("Column0")).ValueList?property for a C1BoundField in C1GridView
	C1BaseFieldCollection.ColumnByName(string) method added.
	C1BoundField.ValueList {get; set } property added.

C1Editor
	[34193] The lay out of settings screen of font color gets crumbled in IE6
	[34466] Upper part of textarea in C1Editor is covered up by gray area after setting the mode property
	[30639] Unlike MS-TextBox, focus is not disaplyed in C1Editor on pressing the key combination of "Alt" + AccessKey value
	[29871]Wijmo Editor's setText method does not synch up code view with designer view in split mode

C1Combobox
	[34462] Unlike previous build, combo dropdown does not work if C1ComboBox is placed in C1Dialog
	[33666] OnClientTextChanged event is not fired when existing combobox item are entered from  the textbox when AutoComplete property is set to true
	[33109] ToolTip does not appear on hovering over dropdown button or C1ComboBoxItem at run time

C1CompositeChart
	[34279] Dataseries does not match the legend entries

C1Charts
	[24364] Although chart label is display as required culture specific settings but Hint Value is not display as required culture setting

ToolkitExplorer_2010
	[30326] Ribbon bottom border and content of C1Editor is not display in ToolkitExplorer


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20131.98/4.0.20131.98)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.7

C1Upload
	[34070][Chrome][FireFox] Page is navigated to Home page after clicking the Upload icon from File input sample of Upload

C1LightBox
	[34037] The image from previous sample is combined in the current sample of wijLightBox

C1Gallery
	[34197] An error is thrown when [Iframe] of ControlExplorer is opened
	[32082] Images are not display when C1Carousel or LightBox are added in WebUserControls and these UserControls Page is added in a subfolder

C1Carousel
	[20915] If PagerPosition.At property is set to "Left", Pager is clilpped with C1Carousel

C1Menu
	[33042] 'Horizontal Scrollbar' is not appeared in 'Preview' pane when multiple child items are not fit in one row
	[23965] When C1Menu " Orientation " is set to 'Horizontal',  its Separator is not shown correctly in top-level menu
	[23507] When setting long text to 'BackLinkText', BackLinkText's text is shown out of back link button.

C1Dialog
	[32134] The resource cannot be found HTTP 404. Error is  observed when there is ~ symbol in ContentUrl property of C1Dialog

C1Rating
	[21457] Dynamically set star option is distorted if reset button is disabled.
	[29596] C1Rating control is disappeared at Design-Time when DisplayVisible property is set as False from Properties Window.

C1Tooltip
	[33971][ControlExplorer] Javascript error is occur after browsing Template?sample page of ToolTip in ControlExplorer

C1ReportViewer
	C1ReportViewer.ClearCachedDocument static method returns true if the specified document has actually been removed from the cache.	
	[29422] [24221-139] C1Preview.C1PrintDocument is not disposing

C1Splitter
	[33604] Panel gets displayed even smaller than the actual size set in 'MinSize' property

C1Combobox
	[33868] Unlike previous build, two items are shown in bold font style when selecting for second time
	[33861] Empty data gets displayed with smaller height.
	[33475] Unlike MS-DropDownList, Combo items cannot be selected if C1ComboBox dropdown list overlaps with disabled C1Editor
	[33191] Javascript error is observed after selecting items from the C1ComboBoxColumn if set AutoPostback=True and 'SelectOnItemFocus=True'
	[32112] Javascript error is occurred after deleting the combo box item in text area using Backspace key where some specific setting is applied	

C1Input
	[33926] Unlike previous build 96, width?and height?properties are not working for all C1Input at run time
	[33230] Unable to get the entered string on setting the password character

C1Editor
	[32998] Unlike C1Editor of WebUI , pressing <Esc> key does not close dialog window (Spell checker ) and dropdown menus (FontName menu, FontSize menu and Table menu)
	[34193] The lay out of settings screen of font color gets crumbled in IE6

C1BubbleChart
	[24667] Unlike C1BarChart, setting client side property "showChartLabels" to 'false' does not work

C1ScatterChart
	[29842] Markers in C1ScatterChart are zoomed on mouse hover effect even  Enabled is set to False.

C1GridView
	[34083] Issue #1: When "AllowColSizing=true" and at runtime columns are resized, the width of Grid remains the same due to which there is a blank space left at the right end of the grid.
	[34006] On setting CallbackSettings.Action to Paging, the value of previous page gets fetched in SelectedIndexChanging event.


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.97/4.0.20123.97)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.6

C1ReportViewer
	[27572] [27779-139] Error [error:An item with the same key has already been added.] is thrown when exporting to "Open XML Excel" or "Microsoft Excel"

C1TreeView
	[33712] Node can't be added or deleted at index 0 in C1TreeView

C1Editor
	[33550] Unlike previous build, toolbar of C1Editor become disabled if full screen mode is reset

C1EventsCalendar
	[33474] Unlike previous build, client side property "disabled" does not work

C1Input
	[33230] Unable to get the entered string on setting the password character
	[33183] Incorrect value displayed with 'nl-BE' culture
	[33570] Unlike initial state, deleting entire value in C1InputNumeric control at run time does not set back to Null value
	[33367] WijInputDate value returns to minDate if time (Hours) starting with 1 or 2 is entered & dateFormat is "HH:mm"
	[33797] The height of C1Input control is not applied if set in the Style
	[33798] If the C1Input control is placed in the UpdatePanel, then the position of the character string gets slightly displaced when the page is updated

C1Calendar
	[32206] Calendar is not rendered when monthCols/monthRows is set when calendar shows year in title

C1Combobox
	[33446] Unlike previous build, bold font style is applied to recently selected item


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.96/4.0.20123.96)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.5

	
C1EventsCalendar
	[32503] [Control Explorer] Unlike 'OverView' Sample, items in calendar drop-down of 'New Events Dialog' are not localized and missing one item in 'CustomDataStorage' Sample
	[33374] [DatePager] 'selectedDateChanged' event fires incorrectly.

C1ReportViewer
	[27572] [27779-139] Error [error:An item with the same key has already been added.] is thrown when exporting to "Open XML Excel" or "Microsoft Excel"
	[29422] [24221-139] C1Preview.C1PrintDocument is not disposing

C1CompositeChart
	[33340] Unlike previous build, value labels are not correctly hidden on toggling the serie display by clicking legend item

C1Pager
	[33440] Unlike previous build, ‘C1Pager?is distorted when placed on page.

C1GridView
	[33414]	Unlike previous build, Javascript error [Unable to set value of the property 'rowSpan': object is null or undefined] is thrown if C1GridView is merged.
	[33399] On setting multiple column headers or fixed columns, the display of header becomes incorrect (IE8 issue).

C1Combobox
	[33446] Unlike previous build, bold font style is applied to recently selected item

C1Editor
	[33423] Unlike previous build, ribbon toolbar is covered up by gray area on clicking "Full Screen" button

C1Calendar
	[33231] Fixed issue with selecting previous day

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.95/4.0.20123.95)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.5

C1PieChart
	[32703]	[Forum-Community][PieChart] Renders incorrectly in Safari 6 on Mac.

C1Dialog
	[33169] [C1Dialog] After post back, or form submit, the position gets changed
	[33186] [C1Dialog] C1Dialog placed on UpdatePanel does not get updated correctly
	[33224] [C1Dialog] If a record of C1GridView is edited in C1Dialog, then a comma gets inserted at the beginning of the field

C1Combobox
	 [33122] Request to add selectedItems property
	 [32309] Unlike build 81, Javascript error is observed after uploading an image in C1Editor and  setting  focus in the C1ComboBox and pressing down arrow key
	 [32324] Unlike previous build, combobox dropdown is not correctly populated with the data items if the underlying data is retrieved using wijhttpproxy
	 [33017] Unlike previous build, bold font style is not applied to the text of selected C1ComboBoxItem.
	 [33033] Unlike previous build, combobox dropdown does not work if C1Editor is placed right below the C1ComboBox		

C1Input
	[25504] Unlike C1InputDate, post back does not occur on changing value by selecting dropdown item in C1InputNumeric and C1InputCurrency
	[32699][Japan Environment] Unlike previous build, double-byte number which is working as string can be entered when the digit mask ??or ??is set
	[33048] PostBack does not occur even after pressing Enter key
	 
C1Editor
	[32702] Unlike previous build, text can be typed beyond the border of text editor if 'ShowFooter' is set to 'False' and 'EditorMode' is set to 'Code'

C1Tooltip
	[30256][IE10 Compatibility] Even 'CloseBehavior' is set to 'Auto' or 'None' close button shows in ‘C1ToolTip?for the first time hovering the 'TargetSelector' control.

C1Upload
	[32170] Unlike build 90, Javscript error is observed after placing  C1Upload on top of and any Wijmo charts or any guage in the same web page

C1GridView
	Added new property AllowVirtualScrolling.
	Added new value CallbackAction.Scrolling.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.94/4.0.20123.94)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.4

C1Carousel
	[32161] [26851-139] NextClass and PrevClass properties and its attributes of C1Carousel are not working
	 
C1Accordion
	[32089] Click event of ImageButton inside C1Expander's Header, doesn't fire.

C1Combobox
	 [32132] Unlike previous build 92, Javascript error is observed after selecting all the comob items display in the textbox with Ctrl+A and clicking the trigger button.
	 [32133] Unlike previous build, incorrect item is selected on pressing "Enter" key after typing a key to filter the comboitems
	 [32112] Javascript error is occurred after deleting the combo box item in text area using Backspace key where some specific setting is applied	
	 [31905] Combobox not deselecting previous selection after loading new data

C1GridView
	[32108] Unlike build 90, Column is not resized after dragging it although setting AllowColSizing property to true

C1Tab
	[32175] Unlike previous build, clicking "Insert" ribbon tab item does not work if it is created in Content of C1Expander

C1Editor
	[32230][IE10 Compatibility] Unlike previous build, javascript error is observed when selecting the item from 'Font Name' or  'Font Size' combo box  of ribbon toolbar
	[30299] Page is automatically scrolled up on clicking the empty area in C1Editor

C1Calendar
	[32206] Calendar is not rendered when monthCols/monthRows is set when calendar shows year in title
	[32258] Unlike previous build, Javascript error [cannot call methods on wijpopup prior to initialization; attempted to call method 'show'] is thrown on calling "popup" method.

C1Slider
	[27708] Unlike server side setting, Client-Side "values" property cannot show range on C1Slider

C1Menu
	[28136][C1Menu] 'OnClientHidden', 'OnClientHidding', 'OnClientShowing' and 'OnClientShown' clients events are not fired when "Mode" property is set to Sliding

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.93/4.0.20123.93)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.3

C1Report
	Updated build version of C1Report dependency

C1Upload
	[31352] Unlike build 88, Error rendering control is observed at design time in C1Upload version 3.5 control when placing on the form
	
C1GridView
	[30795] Unlike previous build, filter dropdown button is overlapped with filter text

C1Combobox
	[30847] Unlike previous build, previously selected data is not disappeared in text area of C1ComboBox after setting data dynamically from client-side.
	[31591] Unlike previous build, incorrect item is selected on typing the text and pressing "Enter" key

C1Input
	[31369] Unlike previous build, Date can be selected for the first time only from the Calendar drop-down and another date cannot be selected again

C1Menu
	[31355] Unlike build 88, Export toolbar menu item?of C1ReportViewer are replaced with context menu of C1Editor if both controls are placed together
	[21150] Request to add a property for selecting menu item in code

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.92/4.0.20123.92)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.3

C1Accordion
	[30434] Postback is repeatedly occurring when expanding ‘C1AccordionPanes’ if  set ‘AutoPostBack=True’  and ‘RequireOpenedPane=False’

C1Upload
	[30535] If EnableSWFUploadOnIE is set to True, upload file dialog is not opened when clicking over the text of C1Upload.
	[30391] Request user friendly message in place of C1Upload.MaxSizeReached

C1TreeView
	[30458] Javascript error["DOM Exception: HIERARCHY_REQUEST_ERR (3)"] is thrown after doing the drag-drop action twice.
	[25173] In C1TreeView Designer Form, Up and Right Arrow are still disabled after inserting a new node

C1Editor
	[21151] Upload Failed message is displayed when loading local file using 'Insert Hyperlink' Dialog
	[24254] Setting table border solid line with thickness value is changed to normal dotted line when switching mode to 'Full Screen'
	[30813] Unlike previous build, C1Editor UI is not maintained after reopening the accordion pane if C1Editor is placed inside it

C1GridView
	[30793] Unlike previous build, clicking the command links or doing any postback actions (filtering / grouping) in C1GridView does not work.

C1Gauges
	[30797] Unlike previous build, C1RadialGauge and C1LinearGauge are displayed as blank at design time and javascript error is occurred at run time

C1Input
	[30798] Unlike previous build, Javascript error[Object doesn't support property or method 'wijpopup'] occur on clicking the trigger button for second time
	[30805] Unlike previous build, ComboHeight property does not work when date is chosen using datepicker, the time is preserved (not reset to 12am)

List(ListExtender)
	[30593] Request to add FindByLabel method
	[30917] autoSize property is broken in latest build 2.3.1

C1Combobox
	[30406] Focus does not move even when Focus method is run in C1InputNumeric control(For C1Combobox)
	[30847] Unlike previous build, previously selected data is not disappeared in text area of C1ComboBox after setting data dynamically from client-side.
	[30855] Unlike previous build, C1ComboBox dropdown is not correctly populated after reopening the dropdown

C1Calendar
	[20697] Unlike design time, 'CssClass' property of 'C1Calendar' is not applied at run time
	[30449] Selected Dates are not displayed in 'Auto Postback' sample of 'C1Calendar' when 'Theme' is set to 'Metro Dark'.

C1Compositechart C1BubbleChart
	[31018] Binding to multiple tables not working for C1CompositeChart and C1BubbleChart

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.91/4.0.20123.91)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.2

C1PieChart
	Added "StartAngle" property.

C1Accordion
	[29193] [25076-139] Accordion panes don't open correctly when SelectIndexChanged event is included

C1EventCalendar
	[29300] [JPN Environment] List view of EventCalendar gets control desorted

C1TreeView
	[30168] Unlike build 58, the TriState parent checkbox changed to checked state after postback is occur

C1Input
	[24236] Setting container’s ‘Height?makes  increase/decrease the ‘Height?of  C1Input control’s ‘Trigger?and ‘Spinner?button
	[30406] [IE] Focus does not move even when Focus method is run in C1InputNumeric control. 
	[30492] Unlike previous build, scroll down button is missing in dropdown
	[30096] Unable to fetch C1InputDate value of the template column when C1GridView is placed inside UpdatePanel

C1Combobox
	[30457] Height gets changed when PostBack occurs

C1List
	[30526] WijList- selectItems() Not finding Values that are Numbers

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.90/4.0.20123.90)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.2

C1Upload
	Added EnableSWFUploadOnIE property to support multiple file upload in oldIE. 

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.89/4.0.20123.89)
==================================================================================
Dependencies
------------
jQuery 1.8.2 
jQuery UI 1.9.1
Wijmo 2.3.2

C1Splitter
	[29981] Empty space is added to the panel2 with nested splitter when outer splitter is fullsplit
C1Gallery
	[30192] Selection is off by 1 if first image is removed using 'Remove' method

C1Dialog
	[30176] "captionButtons property is not working when it is set from client side
	[30252] 'Buttons' option does not get set after WijDialog is initialized

C1EventCalendar
	[30197] Unlike build 79, ‘more?button option is not display  in Month View when browsing with Chrome and Safari

C1TreeView
	[30038] Unlike build 81, Javascript error is observe after drag and drop a node to another node when AutoPostBack property is set to true
	[30250] NavigateUrl does not work.
	[30223] Unlike previous build, duplicate treeviews are shown when multiple C1TreeView instances are created

C1Gallery/C1Carousel
	[30000] Autoplay is still working although Enabled property is set to false.

C1Input
	[30060] Input 'Change' event does not fire
	[30174] InvalidOperationException?is observed after adding script manager with C1InputDate on the same web page
	[30096] Unable to fetch C1InputDate value of the template column when C1GridView is placed inside UpdatePanel
	[30264] Exception is observed on clicking Input when destroy method is called/widget is recreated
	[30095] C1InputDate is displayed with different height regardless of  'Height' property setting
	[30406] Focus does not move even when Focus method is run in C1InputNumeric control

C1GridView
	[30056] Unlike previous build, filter dropdown is overlapped with frozen line.
	[30284] [26046-139]Control in template fields disappear when C1GridView object is accessed via client side.
	[30357] [ToolkitExplorer]Javascript error is observed after navigating to Cell and row formatting?sample of ToolktiExplorer
	[30380] [ToolkitExplorer]Javascript error is observed after navigating to Custom data parsers?sample in ToolkitExplorer
	[30381] [ToolkitExplorer]Javascript error is observed after navigating to Custom paging?and DataSources?samples in ToolkitExplorer

C1Calendar
	[29995][Forum-Community][Calendar] First day is not selected when multiple months are shown & selectionMode is WeekDay

C1Editor
	[27846] Unable to set Backcolor when browsing with Firefox browser
	[30008] The text are replaced repeatedly in the same word although there are the same word in another sentences after using FindAndReplace in the editor
	[30012] Incorrect tooltip("Form") is displayed for "Save" button in ribbon toolbar if C1Editor is created at client side
	[30195] Unlike previous build, Control UI is not maintained when selecting the item from Font Size combobox of ribbon toolbar
C1Menu
	[29816] 'NavigateUrl' is not working properly in sub menu after navigating through keyboard when 'Mode' property is set to 'sliding'
	[30098][23522-139] "HttpException" is observe when C1Menu is use in Master Page and web page using master page is placed in subfolder and postback is occur
	[29900][ControlExplorer] Extra button is included in 'ClientSide add and remove' sample of C1Menu

C1Slider
	[30225] JSError is observed when refresh method is used in Window.resize event

C1Combobox
	[28763] Javascript error [Unable to set value of the property 'selected': object is null or undefined] occur on setting selectedIndex client side property if bounded to wijdatasource
	[30309] Javascript error is occurred after filtering combo items from the dropdown list if 'AutoPostBack' and 'SelectOnItemFocus' are set to 'True'.



==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.88/4.0.20123.88)
==================================================================================
Theme
	[30030] Fixed control rendering using metro and metro-dark theme

C1Accordion
	[30027] ‘C1Accordion’ is distorted if ‘ExpandDirection’ is changed to ‘Left’ or ‘Right’ at run time.

C1ReportViewer
	C1Report assemblies updated to *.6.20123.54602.

C1TreeView
	[30038] Javascript error is observe after drag and drop a node to another node when AutoPostBack?property is set to true

C1Editor
	[30036] Ribbon toolbar does not work if it is created in Content of C1Expander

C1InputDate
	Prevented page scrolling when date is incremented/decremented by a mouse wheel
	Fixed an issue: when no date is specified, the text updated to the current date on blur

C1InputNumber
	Added support for mouse wheel

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.87/4.0.20123.87)
==================================================================================

C1ScatterChart
	[29922] Series marker is displayed outside the chart area if stacked property is toggled

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.86/4.0.20123.86)
==================================================================================

C1TreeView
	[29323] StackOverflowException occurs if ExpandAllNode method is called

C1Calendar
	[29935] C1Calendar's month header and body are misaligned after choosing the month using quick pick mode if AllowPreview is set to True
	[23455] C1ComboBoxColumn.Width property does not work

C1Input
	[29955] Javascript error [cannot call methods on wijinputmask prior to initialization; attempted to call method 'widget'] is thrown if 
			C1GridView is placed inside Content of C1Expander

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.85/4.0.20123.85)
==================================================================================

C1Accordion
	[29875] All 'C1AccordionPanes' are expanded when setting 'RequireOpenedPane' to 'True'
	[29888] C1Accordion header and content are not rendering correctly at run time.

C1Combobox
	[23891] Up/down key was not working when C1ComboBox's dropdown list is filtered for the second time
	[29020] Unlike run time, setting ShowTrigger and TriggerPosition are not applied at design time
	[29937] Postback is always occurring and javascript error is observe after setting "AutoPostBack" set to true and "SelectOnItemFocus" to true

C1PieChart	
	[29859] Labels are not shown when label.offset is set & chart data is reloaded.

C1FormDecorator
	[27803] Decorated MS CheckBox and RadioButtons remain dim after their "Enabled" property is OFF and ON

C1GridView
	[29869] Javascript error occurred when C1GridView is databound

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.84/4.0.20123.84)
==================================================================================

C1ReportViewer
	Synced with latest C1.C1Report assemblies (*.6.20123.54601).

C1ProgressBar
	[27509] C1ProgressBar is applied with 100% fill effect when setting the FillDirection is either North or South and the 
			ProgressBar.Value is zero.

C1Carousel
	[25073] The scrollbar and the text position area are overlapped in the LightBox

C1Splitter
	[29786] Setting Enabled to false does not work

C1Splitter
	[28789] Setting 'Enabled = false' disable the controls not present inside C1Splitter if C1Editor is added in C1Splitter

C1Menu
	[29572] Menu items get removed after double clicking the menu item in 'Windows7 Basic' Theme.

C1Accordion
	[29709] Javascript error occurs when Script Manager is placed on the same page with C1Accordion and SelectedIndexChanged event is 
			added to the control.

C1Autocomplete
	[24458] Unable to set the Source property.

C1EventsCalendar
	[29305,29300] Fixed rendering of List view

C1Expander
	[29616] Client-side events ('BeforeCollapse/Expand') and ('AfterCollapse/Expand') are not fired at run time.

C1Gauges and C1Charts
	[24783] Unlike other wijmo controls, setting "Visible= false" affects at design time

C1Combobox
	[29706] ASP C1ComboBox bug with FireFox 16.0.2.
	[29162] Incorrect 'SelectedIndex' /'SelectedValue' is returned when bound to Remote DB

Dropdown
	[29698] Incorrect value is returned for selected item in Firefox and IE9

C1ToolTip
	[27524] [IE8, IE9] 'Text' of control [e.g.'CheckBox' or 'RadioButton'] is not shown if it is contained in 'Template' of 'C1ToolTip's content area
	[27576] If a C1Control is placed in the Template of C1ToolTip's content area, tooltip is not shown on hovering TargetSelector control 
			for the second time
	[28790] Modal is not apply completely according to the browser after resizing the browser

C1Charts
	[29718] Exception occur in data binding
	[29573] Different behavior is observed between gridlines after changing the Compass setting for the X Axis from South to North.

C1BarChart
	[29739] C1BarChart is not correctly rendered if the underlying data source contains negative value

C1PieChart
	[29788] Filled color disappears at both design time and run time if Enabled property is set to False.
	[27988] PieChart is overlap with grid view if these two controls are placed in different column in the same row of html table after browsing with Chrome.
	
C1Editor
	[29731] Javascript error [An menuitem must be a child of menu] is thrown if C1Editor is created using client side script
	[29738] Javascript error is occurred when changing to Full Screen Mode at run time if Mode property is set to Bbcode.

C1Charts and C1Gauges
	[24799] Unlike IE9, control doesn't get dim state in IE8, when set "Enabled = false"

C1GridView
	[29676] Expanding/Collapsing child grid rows affect parent group rows to expand/collapse 
	[29710] Javascript error [Unable to get value of the property 'resolvedHeaderImageUrl': object is null or undefined] is thrown if AutoGenerateEditButton/AutoGenerateSelectButton/AutoGenerateDeleteButton="true"

C1CompositeChart
	[29722] Added support for Multiple Y- Axis

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.83/4.0.20123.83)
==================================================================================

C1Accordion
	[29695] Panes do not close when selection changes and 'ExpandDirection' is set to top/left

C1ReportViewer
	[29491] Unlike previous build, incorrect total page count value ("0") is displayed in toolbar and buttons (Print,Export,Navigation,Zoom) do not work

C1Carousel
	[26287] C1Carousel's items are not shown properly in C1Tab when C1Tab's direction is set to Right to Left.

C1RadialGauge, C1LinearGauge
	[26376] The designer was throwing an exception when "Pointer.Template" property was empty.

C1ToolTip
	[26904] JavaScript error occurs if ‘MouseTrailing’ is set to ‘True’ and ‘Position’ is applied as {At: ‘Center, Center’; My: ‘Center, Center’}

C1TreeView
	[29350] Nodes becomes undraggable & incorrect selection style is applied when a node is dragged out & added to another tree using custom drag drop.

C1AutoComplete
	[29269] PostBack action does not occur when AutoComplete Item is selected; ItemSelected event is not fired although AutoPostBack=True is set

C1Gallery
	[29279] Large Image from LinkUrl is not displayed in C1Gallery at Run-Time

C1Menu
	[29214] "Error Rendering Control" is displayed at design time after adding menu items in Designer Form.
	[29316] Javascript error [Unable to get value of the property 'destroy': object is null or undefined] is thrown when client side property "mode" is set
	[29424] Second Level Sub-Menu is not opened on hover in DataBindTemplate Sample of C1Menu

C1LineChart
	[29243] Data series of line chart is filled with color after mouse hovering over the data series although series hover style is not added
	[29463] ChartBindings to multiple tables do not work

C1GridView
	[20755] Unlike design time, header images are not displayed at run time when ‘HeaderImageUrl?property is set with an image
	[29473] Milliseconds of DateTime type does not get displayed in  'BoundField' column

C1BubbleChart
	[29255] Stroke width is not reset to original width after hovering over the dataseries and move out of that data series when adding stroke in series style

C1Editor
	[29312] Javascript error is observed after adding two C1Editor in C1Tab pages each
	[29444] Able to type text in text area of C1Editor even  Enabled is set to False

C1Calendar
	[29507] Postback doesn't occur after selecting the date and having AutoPostBack property set to True.

C1Combobox
	[29495] "AutoComplete" property is not working properly when Captial letter is enter in the comboBox
	[29523] Autocomplete feature does not work if ItemsTemplate is used
	[29532] Items are not correctly displayed in textbox of C1ComboBox if selection is done through keyboard
	[28947] Menu gets no style & is transparent when CSS scope is used

C1CompositeChart
	[29122] A gap is occurred between Chart Series and X-Axis if 'StrokeWidth' is set for Series Style

DropDown
	[29400] OnSelectChange event is not raised for the first element in the options when it is selected second time

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.82/4.0.20123.82)
==================================================================================
- Wijmo (Core JavaScript Library)
	jQuery UI version 1.9.1

C1Editor
	[29234] Javascript error is observed after setting Mode property to Bbcode or Simple
	[29230]	Context menu is always appear at the top and far away from selected text	
	[29435]	Javascript error is occurred at runtime if ShowFooter is set to False

C1EventsCalendar
	Added new client side option - readOnly //Set this option to true if you want to prevent users to edit events data.
		Usage example:
			$("#<%=C1EventsCalendar1.ClientID%>").c1eventscalendar("option", "readOnly", true);	
	[29229] New Event Dialog is not displayed after clicking the time-slot of C1EventsCalendar

C1ReportViewer
	[29178] When viewing full screen after scrolling the browser, an irregular margin appears
	[29212] Javascript error [cannot call methods on button prior to initialization; attempted to call method 'option'] is thrown	

C1GridView
	[29208] C1GridView instance gets crashed in Design View.

C1Combobox
	[28359] The selected text gets appended instead of replacing the selected original text when shif key is press for a while
	[29179] Performance issue is observed when dropdown list contains 2000+ items in IE9


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.81/4.0.20123.81)
==================================================================================
- Wijmo (Core JavaScript Library)
	Wijmo version 2.3.0
	jQuery version 1.8.2
	jQuery UI version 1.9.0
	
C1Tooltip
	[29181] Tooltip is shown after moving waya mouse if 'mouseTrailing' & 'showDelay' are set to true & non-default value respectively.


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20123.80/4.0.20123.80)
==================================================================================
- Wijmo (Core JavaScript Library)
	Updated Wijmo to version 2.3.0
	Updated jQuery to version 1.8.2
	Updated jQuery UI to version 1.9.0

All Controls
	[28259] Failed to create designer when a control is dropped onto the design surface.
	[28896] C1 Wijmo controls don't work if Session State of the webpage is disabled Support Enhancement
	[27988] [Chrome] PieChart is overlap with grid view if these two controls are placed in different column in the same row of html table after browsing with Chrome.

C1Charts/C1Gauges
	[29180] Wijmo Charts in IE9.

C1Inputs
	[29051] [IE10 Compatibility] JavaScript error occurred if any time-slot of C1EventsCalendar is clicked.

C1PieChart
	[27662] White color is set for all slices beyond 12 items.

C1Accordion
	fixed problems with child controls view state.
	[28797] Checkbox Resetting After Handling Changed Event in Accordion
	[28908] C1Accordion- Designer Issue with UpdatePanel in Pane
	[28876] Static Accordion Panes Not Resizing
	[28695] Loading Panes in Accordion Dynamically with Postback

C1Combobox
	[27917] Textbox area is able to edit although 'IsEditabled' property is set to false on button click
	[27810] Unlike build 3.5/4.0.20113.47, the text enter in the C1ComboBox for the second time are not retain when 'AutoPostBack' property is set to true
	[28695] Unlike build 47, 'SelectedIndex' property is not working when 'SelectionMode' is set to Multiple
	[28979] Unlike previous build, selected text cannot be cleared with 'Back Space' key if SelectItemOnFocus=True
	[28693]	Combobox not updating when labels are changed dynamically
	[28976]	Binding selectedValue option to a ko observable does not work
	[29108]	'TypeError' is observed when text is entered in WijComboBox bound to remote db in Chrome /FireFox
	[29074] 'SelectedIndex' option does not return all selected items when 'SelectionMode' is set to 'Multiple'
	[29021] [VS2012] C1ComboBox UI is distorted at design time [missing bottom border]
	[28717] Viewmodel is not updated when text is typed in the WijCombobox

C1CompositeChart
	[28861] Inconsistent SeriesTransition behavior of Line series & Column series

C1Editor
	[28758] Unlike previous build, clicking "Cancel" button of "Insert Table" dialog does not work in Japanase language setting
	[27844] [IE] Html Tags are display in design view when changing to Full Screen Mode after 'Mode' property is set to 'Bbcode'
	[27652] Corrected words are not updated in source view immediately after checking the misspell word with the Spell Checking
	[27566] Multiple issue are observed after placing C1Editor in C1Tab control and disabling it
	[28367] [IE10] Unlike MS Word, underline style doesnt apply in misspelled words
	[29176] Multiple issues seen when C1Editor & C1Menu are placed on same page in IE9/Firefox 16

C1EventsCalendar
	[23234] Data Binding Problems
	[28855] "beforeEditEventDialogShow" event of C1EventsCalendar is missing
	[28851] Unlike previous build, existing events from xml file cannot be loaded in C1EventsCalendar
	[28802] Unlike previous build, event cannot be created in C1EventsCalendar
	[26155] [IE9] Unlike previous build, Repeated events cannot be created on the second time if the same type of repeated value is used on different time slot

C1Gallery
	[24169] [IE]'Next' and 'Previous' button are not display in gallery after clicking on the next button or previous button without hovering the mouse outside of Gallery

C1GridView
	[28263] In IE8, unable to move through the cells more than once by using the arrow keys on the client side edit mode

C1InputDate
	[28737] Unlike previous build, client side property "culture" does not work
	[28993] [Localized] Unlike C1Calendar, the Date are not displayed properly in Japanese Date Format in the drop-down calendar of C1InputDate

C1Inputs
	[28891] Unlike previous build, the Trigger and Spinner buttons are not properly displayed at design time

C1Menu
	[27996] Unlike build 58, 'The resource cannot be found HTTP 404.' Error is  observed when there is '~' symbol in 'NavigateURL' property

C1LineChart
	[27533] JScript error is observed on adding Serieslist when 'Animation' is enabled
	[28907] Multiple issues are observed with click event.
	[28854] Line chart legends are not rendered correctly when Stroke-dasharray is set to non-default value

C1LightBox
	[28283] Unable to localize CounterFormat String to a particular culture when setting in "CounterFormat" property

C1ReportViewer
	Localization for outline tool updated.
	[28208] Clicking on Search,Thumb pane in TOC of C1ReportViewer which in table shows JScript runtime error
	[28983] [ControlExplorer] DataModel Sample : After editing a calendar, both old name and new name are displayed as 2 calendars in New Event Dialog and the deleted calendar is still displayed in New Event Dialog

C1Tooltip
	[28873] JS Error is observed with Jquery 1.9.0.min.js.

C1Upload
	[28278] 'ValidatingFile' server side event is fired twice by clicking UploadAll button although only 1 file is uploaded
	[27817] Multiple files cannot be selected at once from the same folder

C1Slider
	[27963] Javascript error [Unable to get value of the property 'left': object is null or undefined] is thrown on clicking thumbs handle if Range="True"

C1DroDown
	[29066] Dropown remains selected even after focus is lost

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.79/4.0.20122.79)
==================================================================================
- Wijmo (Core JavaScript Library)
	Updated Wijmo to version 2.2.2
	Updated jQuery to version 1.8.1
	Updated jQuery UI to version 1.8.23

C1Accordion
	[28695] Loading Panes in Accordion dynamcically now works properly during Postback
C1Splitter
	[28059] Splitter is not rendered correctly if placed in inactive tab & browser is resized
C1Editor
	[20447] Previously uploaded images cannot be deleted in 'Image Browser' dialog
C1Input
	[28210] C1Input not found in Turkish environment
C1BubbleChart
	[27526] "InvisibleMarkLabels" property did not work correctly in bubble series
C1BarChart
	[26358] Visibility issue with clicking series item in legend

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.78/4.0.20122.78)
==================================================================================

- Wijmo (Core JavaScript Library)
	Updated Wijmo to version 2.2.0
	Updated jQuery to version 1.8.1
	Updated jQuery UI to version 1.8.23

C1PieChart
	[27816] Height increased when slice value is changed.
C1Menu
	[26789] "Header" and "Separator" are not displayed in right to left direction although "Direction" property is set to "Rtl".
C1Menu
	[27393] Sub menu is not opened up on double clicking of Top level menu when 'TriggerEvent' is set to 'Dbclick'
C1Dialog
	[27667] "Pin" icon is not changed when client-side pin function is invoked
C1Menu
	Client events 'OnClientShown', 'OnClientShown', 'OnClientHidding' and 'OnClientHidden' have been added.
	[27573] Request to provide 'Closing' event.
C1Dialog
	[27439] "Stack" property is not working in Wijmo Dialog when set to true.
C1Accordion
	[25688] In 'Data Bound' C1Accordion, if 'RequireOpenedPane' is set to 'False' at design time, 'C1AccordionPane' set as 'SelectedIndex' is not expanded at run time
	[25980] When clicking the Header area of C1Accordion, any C1Accordion inside its Content area is collapsed automatically.
	[25480] Properties set on C1AccordionPane were not rendered such as (AccessKey, CssClass, BackColor, BorderColor, BorderStyle, BorderWidth, Font, ForeColor, Enabled, ToolTip)	
	[25299] When "Save as xml" and "Load from xml" some properties (AccessKey, Enabled, ExpandDirection, RequireOpenedPane, SelectedIndex) are not retained.
	[26152] If RequireOpenedPane is set to "False" some of the expanded panes are not retained after postback occurs.
C1EventsCalendar
	[26155] [IE9] Unlike previous build, Repeated events cannot be created on the second time if the same type of repeated value is used on different time slot
	[25120] The event duration is reduced, if the event in which start-date and end-date are different, is moved (dragged)
	[21282] The left pane of C1EventsCalendar is enlarged and overlapped with the "today" button when a date of Wednesday or Thursday is selected in ControlExplorer Sample
	[21036] [IE9] EventsCalendar is corrupted when 'control width' is set to '800 px'
	[27383] Blank Tooltip is appeared in the NavigationBar of the "ListView"
	[21613] Request to provide support for localization on changing the Culture property
	[24373] Request to provide 'GoToTime' method to scroll to particular time
	[20772] When "all-day" checkbox is checked, the same Start-Date and End-Date is still displayed and event disappears
C1Expander
	[20632] [IE8/IE9Compatibility] When 'Enabled=False' is set,  Theme is not getting dimmed at run time
C1ReportViewer
	Search tool localization updated.
	[24728] [IE9 Compatibility View] C1ReportViewer disappears when clicking 'continuous view' button in full screen mode
	[20963] "SaveLayout" method does not save some properties of C1ReportViewer

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.77/4.0.20122.77)
==================================================================================

- C1Carousel
	[27417] Caption of second Item is not shown if setting Preview = True and Display = 2

- C1ComboBox
	[26392] The width of the combo box increases to 100% after browsing with IE 9 in compatibility view mode

- C1LineChart
	[27397] ChartLabels are displayed for DataHole values.

- C1ReportViewer
	[27394] Searching text was not working under Japanese locale.
	[27354] C1EventsCalendar is distroted in MonthView

- C1ToolTip
	[21467] Tooltip position was working incorrectly.

- C1Input
	[26091] NullText property wasn't working.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.76/4.0.20122.76)
==================================================================================

- C1Editor
	[26628] 'Server Error in '/'application' Message is thrown during Postback after adding image into a Text Editor Area
	[26379] The image is not uploaded a second time from 'Image browser' Dialog
	[26902] Ribbon toolbar remain enabled in "Source View" if it is created inside C1Expander

- C1CompositeChart
	[24430] Javascript error is observed after hovering over the plot area quickly and then hovering over the bar chart type
	[24788] AnnoMethod of the Scatter chart does not work properly after setting ValueLabelList in Axis X and Axis Y

- C1BubbleChart
	[27256] Clicking the legend item does not toggle the series visibility
	[27262] Animation effect is not seen on hovering the mouse over serie markers
	[27275] "Y1" values are not correctly applied in series marker
	[26741] Hint is not displayed properly on setting both Hint.Content.Content and Hint.Title.Content properties

- C1ToolTip
	[21416] 'CssClass' property of 'C1ToolTip' is not applied at run time.

- C1InputMask
	[25546] Not able to input correctly in C1InputMask if set Space to PromptChar property

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.75/4.0.20122.75)
==================================================================================

- C1ReportViewer
	[25479] FormatException[Input string was not in a correct format.] is thrown on clicking "Set parameters" button 
			if any of named zoom values is set

- C1EventsCalendar
	[24515] Dates are not described in correct Japanese date format in localized version
	[24534] Unlike C1Calendar, Title and Tooltips are displayed with Incorrect Japanese Date Format in the "dropdown calendar" from C1EventsCalendar’s Event dialog

- C1TreeView
	[26795] All events stop working if false is returned in nodeBeforeDropped event of wijtree

- C1InputDate
	[26153] 'Access is denied' javascript error is observed after selecting the date from C1InputDate when 
			browsing with IE 9 compatibility view mode

- C1LightBox
	[25098] Image with bmp format is not displayed in the Preview Tab of C1LightBox Designer Form.

- C1Menu
	[25540] Menu does not work with jQuery 1.8.0.

- C1ToolTip
	[21386] JavaScript error is observed if 'ShowCallout' property of 'C1ToolTip' is set to 'False'.

- C1Editor
	[26334] 'Server Error in '/'application ' Message is thrown on postback

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.74/4.0.20122.74)
==================================================================================

- C1Dialog
	[24235] C1Dialog does not reappear after being hidden if certain 'ShowAnimation' and 'HideAnimation' are set.

- C1Combobox
	[24546] Dropdown list is empty when opening the second time when using Alt+DownArrow key to open the Dropdown
	[25224] Selection with keyboard does not work a second time if ItemsTemplate is used

- C1ReportViewer
	[24770] "Ajax error 500 (Internal Server Error)" text is shown in the status bar of C1ReportViewer

- C1Gauge
	[24774] Pointer doesn't show correct value and disappears at design time when setting the Value a second time.

- C1BubbleChart
	[24746] AnnoMethod = ValueLabels does not work on the Y Axis.

- C1CompositeChart
	[24752] Hole value does not work for Line type.
	[24431] Having a bar chart and line chart wasn't not correctly rendering at desing time.
	[24788] AnnoMethod does not work properly after setting ValueLabelList in Axis X and Axis Y.

- C1PieChart
	[25009] TextStyle property setting is not applied to ChartLabels.
	[24940] ChartLabelFormatString property was not working.

- C1Menu
	[24830] Javascript error [Unable to set value of the property 'value': object is null or undefined] is thrown on postback 
			after client side property "mode" is set.

- C1GridView
	[25096] 'Microsoft JScript runtime error' is thrown on moving column more than once.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.73/4.0.20122.73)
==================================================================================

- C1AutoComplete
	[24456] Designer Form get closed when clicking anywhere on the designer after renaming the auto complete item

- C1Carousel
	[24726] Inconsistence between Design time and Run time after setting 'PagerType' to 'dots'

- C1EventsCalendar
	[25070] Multiple issues are observed on dragging appointments
	[24623] The event list cannot be scrolled to the last item in the left pane of List view
	[24814] Jscript error occurred after clicking the time slot in any data view

- C1ReportViewer
	[24231] C1ReportViewer is distorted and some texts are not able to see in ‘Metro’ and ‘Metro Dark’ themes

- C1Input
	[24817]/[24815] "Microsoft JScript runtime error: 'console' is undefined" occurs when place any C1Input control on page and run

- C1Carousel
	[24764] Object of type System.Int64 cannot be converted to type System.Int32 Error message occurs after resetting Display
			property in property grid

- C1Editor
	[24436] Misspelled words are not highlighted with a red line for spell checking in the editor when browsing with IE browser

- C1ComboBox	
	[24723] ComboBox Item cannot be selected a second time if ItemsTemplates are used
	[24729] Dropdown is not populated with all comboitems after deleting the text in C1ComboBox if ItemsTemplate is used

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.72/4.0.20122.72)
==================================================================================

- C1Carousel
	[24145] Carousel Backcolor gets highlighted after clicking the next button quickly in Chrome browser

- C1Tabs
	[23968] Javascript Exception is displayed when new page containing a C1Calender is loaded in a Tab Page using Url() method.

- C1Wizard
	[24151] Setting NextBtnText and BackBtnText at design-time is not reflected in design-view.
	[22855] The control UI is distorted after postback on another step when the control is placed inside an 
			UpdatePanel and this panel is place inside a C1Wizard control.

- C1ComboBox
	[23863] 'SelectedIndex' does not update on setting 'SelectedValue'
	[24133] Selection does not work in second time
	
- C1BarChart
	[24213] Javascript error [Unable to get value of the property 'bar': object is null or undefined] is thrown on clicking 
			the legend item item after adding BarChartSeries to SeriesList

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.71/4.0.20122.71)
==================================================================================

- C1TreeView
	[21040] Able to drag parent node and drop over a child node.

- C1Menu
	[24267] Javascript error [An menuitem must be a child of menu] is thrown when C1Menu is created at client side.
	[24277] Javascript error [Unable to get value of the property 'vNeedScrollBar': object is null or undefined] is thrown 
			when client side property "mode" is set
	[24288] Submenu items are not visible on hovering the mouse on menuitem after toggling mode property
	[24238] Submenu item’s text is not shown if Template is used in client side for shortcuts.

- C1GridView
	[24435] EmptyDataText property does not work.
	[24573] "AllowKeyboardNavigation" property does not work after leaving edit mode
	[24587] "NaN (???)" is displayed in edit mode if DataFormatString is set.
	[24543] Grid is not render properly on page postback ,When C1GridView is placed inside an update panel and its 
			Visible property is set to false and true again
	[24537] Gridlines of data rows and header row are misaligned on entering edit mode on the last visible column if ScrollMode="auto".

- C1Editor
	[24323] Footer region is covered up by unknown gray area and not accessible if editorMode = "code"

- C1Inputs
	[24317] Text of C1DateInput are shown outside of the control when DateFormat="U"
	[24577] Content is not completely displayed in IE8 browser with arctic theme.

- C1ComboBox
	[24248] When using multicolumn, first item is lost in dropdown list  after selecting an item
	[24344] ComboboxExtender does not work with ScriptManager that is placed in the ContentPage
	[24588] Selected item is not shown in C1ComboBox if ItemsTemplate is set
	[24548] Typing the text in combobox does not select the combobox item correctly if ItemTemplate is used

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.69/4.0.20122.69)
==================================================================================

- C1Lightbox
	[24243] When C1LightBox is navigated, Next Image is displayed before the Transition is applied

- C1Upload
	[24341] If IsSeparatorProgress is set to False and ShowUploadedFiles is set to True, uploaded file list is not shown after uploading the files

- C1LightBox
	[20988] After navigating C1LightBoxItems, QuickTime movie is not displayed in IE9

- C1GridView
	[20595] Multiple issues are observed on setting ScrollMode property and resizing browser.
	[24245] StaticColumnIndex property does not work.
	[24253] In IE9, C1GridView is distorted on opening filter drop-down in a scrollable grid.
	[24367] Vertical alignment of header area, data record area and pager area render incorrectly when editing a partially visible column.

- C1BarChart
	[24212] Axis labels are not correctly displayed after postback
	[24451] Server side property "Horizontal" does not work

- C1Charts
	[24137] Jscript runtime error occurred when C1EventsCalendar ( culture set except en-US ) is placed in the same page with any 
			Chart control

- C1Combobox
	[24134] Post back occurs when pressing 'Enter Key' when 'Autopostback' is set to 'False'
	[24106] Javascript error [Unable to set value of the property 'selected': object is null or undefined] occurs on pressing 
			"Enter" key in C1ComboBox if AutoComplete="False"
	[24130] Dropdown is not shown if ItemsTemplate is used

- C1Editor
	[21292] Editor is not displaying properly when it is placed in a UserControl and this UserControl is placed in a sub folder
	[22852] Image Size is not automatically set when Image source is an Image from HTTP page
	[20619] "Permission denied" javacript error is observed after canceling the "Tag Inspector" dialog and clicking link 
			toolbar button in "FullScreen" mode in IE8
	[24323] Footer region is covered up by unknown gray area and not accessible if editorMode = "code"

- C1CompositeChart
	[24430] Javascript error is observed after hovering over the plot area quickly and then hovering over the bar chart type

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.68/4.0.20122.68)
==================================================================================

- C1Upload
	[16240] C1Upload limits uploads to allowed maximum number of files.
	[23910] If IsSeparatorProgress is set to False, upload buttons and upload progress bar were still appearing.

- C1GridView
	[21330] The value in 'HorizontalAlign' property was resetting after closing the 'C1GridView' properties dialog.
	
==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.67/4.0.20122.67)
==================================================================================

- Synced with C1Report build 54513 

- C1Gallery
  [24000] The last navigation button of ‘Thumbnail’ displayed at incorrect position when setting ‘ScrollWithSelection’ property to true

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.66/4.0.20122.66)
==================================================================================

- C1Wizard
	Added new properties "NextBtnText" and "BackBtnText".

- C1Calendar
	[24061] Postback doesn't occur on selecting date after navigating to previous or next month when AutoPostBack is set to True

- C1Tabs
	When using nested Tab controls, setting the AutoPostback property to true for the parent tab was causing a postback when selecting
	a tab in the child tab control.

- C1EventsCalendar
	[24047] When the C1EventsCalendar is added in Japanese Environment, Culture is not automatically set as 'ja-JP' [ Query ]

- C1InputDate
	[24023] Calendar dropdown is not localized on setting client side "culture" property

- C1GridView
	[23111] Grouped records can be expanded / collapsed even though OutlineMode is set to OutlineMode.None
	Exception "It is not possible to mix two different editing methods" is raised now when both AllowClientEditing and EditIndex 
	properties are set.
	[23873] Javascript error is thrown when a date column with aggregates is dragged and dropped onto the GroupByArea
	[24053] Alternate row style is applied to all rows

- C1LineChart
	[24057] Fixed incorrect rendering at design time.
	
- C1CompositeChart
	[24050] Style is null or not an object error occurs when PieChartSeries are added.

- C1Combobox
	[24043] JScript error occurs when Enabled property is set to "False" if ShowTrigger="False"
	[24045] JScript error occurs after deleting selected items in the textbox area of C1ComboBox when using multiple selection mode.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.65/4.0.20122.65)
==================================================================================

- Combobox
	[16052-139] Fixed an issue when displaying in a Table, related to ui-helper-clearfix

- C1EventsCalendar
	[23766] Setting on client side property "culture" does not work

- Charts
	Fixed a bug of ValueLabels.

- ScatterChart
	[23893] Throws 'Microsoft JScript RunTime Error' on [Charts-->Scatter Plot Charts] and [Menu --> OverView] Pages

- C1Inputs
	[23822] Theme doesn't apply at design time
	[23821] Spinner button and Trigger button appear at the bottom of the C1Input controls after setting ShowSpinner 
			and ShowTrigger property to true

- C1GridView
	[20637] "Visible" property of "PagerSettings" do not work in C1GridView.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.64/4.0.20122.64)
==================================================================================

- C1Upload
	[23877] Warning message "Upload session is expired" is shown and files are not uploaded if AutoSubmit=True

- C1Gallery
	[23846] Invalid character javascript error is observed after navigating to the next images in thumbnail when 
			the gallery control is bound to a SQL datasource and place in C1Tab control
	[23843] UI is rendered incorrectly when images exceeded the control size.

- C1Carousel
	[22865] JScript runtime error is generated in IE9 when  'LoadOnDemand' is set to 'True'

- C1LightBox
	[22987] Navigation does not work or Exception occurs when Transitions are executing.
	[20925] Incorrect Image is displayed in Large Image Dialog after navigating several times

- C1GridView
	The AllowAutoSort property is now visible.
	[23510] C1TemplateField column does not work in callback edit mode
	[23474] Unlike other rows, clicking "Cancel" link button in last row does not work in callback mode
	[23894] Text of CheckBoxField column is not shown in edit mode

- C1Rating
	[21105] Thumbup/Thumbdown/BarArray ratings are not reversed when setting Driection=Reverse.

- C1ComboBox
	Setting the selectedValue doesn't update selectedIndex.
	[23818] Control is distorted on setting client side property "showTrigger"
	[23823] After selection in C1ComboBox, some comboBox items are missing in the dropdown

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.63/4.0.20122.63)
==================================================================================

- C1Rating
	[20944] Enabled property value can't set to True after setting it to false.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.62/4.0.20122.62)
==================================================================================

- Wijmo (Core JavaScript Library)
	Updated to version 2.1.4


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.61/4.0.20122.61)
==================================================================================

- C1Gallery
	Multiple issues were observed when C1Gallery was bound to SqlDataSource

- C1Combobox
	After setting items at design time, items were displayed twice in drop down list
	Client-side 'selectedValue' property did not work
	Dropdown did not get opened when clicking the trigger button for a second time
	Last item was not displayed properly in dropdown of C1ComboBox
	Selection could not be cleared by setting selectedIndex to -1

- C1EventsCalendar
	Jscript error occured after edited Recurrence Event was opened twice.

- C1InputCurrency
	When 'MinValue' was set to negative value, 'ShowNullText' was not working.

- C1Inputs
	CssClass property didn't apply at both design and runtime.

- C1CompositeChart
	Error occured when C1CompositeChart was added to WebForm in design view.

- C1Editor
	'Microsoft Jscript run time error' was thrown when right-clicking inside C1Editor.TextEditor Area

- C1GridView
	Filtering on date column with default date value did not work.
	Filter dropdown did not open on clicking the filter icon if AutoGenerateColumns="true".
	Jscript error occurred in scrollable C1GridView that had a Band column.
	Input numbers were not correctly entered in filter input cell of numeric columns


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.60/4.0.20122.60)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1AutoComplete (New Control)
	Create auto-complete search boxes and more with this Ajax powered AutoComplete Control

- C1Menu
	[20798] Added support for Right-to-Left rendering in Wijmo Menu for arabic languages


Corrections
-----------
- C1ReportViewer
	[23481] Exporting no longer throws ‘Unknown export format’ exception 

- C1Inputs
	[22004] ID of  "ComboBoxItems"  were being duplicated when the control is added after page is closed and reopened
	[22152] Value now properly Increases/Decreases when using the Spinner buttons

- C1GridView
	[23466] Javascript error [Unable to get value of the property '1': object is null or undefined] is no longer thrown 
			when pressing DownArrow key after selection is set to last row and AllowKeyboardNavigation="True".
	[23476] Columns are now fully displayed in grouped C1GridView when ShowHeader="False"

- C1Combobox
	[23405] SelectedIndex property now works properly just like the Microsoft DropDownList Control


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20122.59/4.0.20122.59)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1ReportViewer
	New property was added:
		bool ShowParameterInputForm {get; set;} 
		
		This property is used to display parameter input forms for reports with parameters. The default value is true.

- C1EventsCalendar
	You can now remove or change event occurrences.
	[20545] Added iCal support
	Added new option: eventsData, the event objects array.
	Added new event: eventsDataChanged, occurs when the eventsData option is changed.
	Option appointments are now marked as obsolete (existent behavior is not changed), use eventsData option instead.

Corrections
-----------
- C1ReportViewer
	[23254] File name/Report name cannot display Japanese reports.
	[22864] Incorrect data & formatting is exported when Report is exported to Excel format.

- C1EventsCalendar
	[20913] Outlines are not displayed in Outline pane when ‘ExpandedTool’ is not set with 'Outline'
	[21234] C1EventsCalendar is displayed as black rectangle at design time.
	[21908] Data binding is not working when control is running in master page.
	Fixed few UI interaction problems when events loaded using eventsData option (or using appointments option).

- C1Tooltip
	[21432] Server Error is occurred if  "Enabled" property is set to "False"
	[21433] If "Content" or "Title" property is set with HTML code, Server Error is thrown after postback occur

- C1TreeView
	[22289] Multiple issues observed with TreeView

- C1Upload
	[22147] File dialog opens on the area outside the Upload button area

- C1RadialGauge
	[22007] Setting Origin.X does not add in xaml page and setting Origin.Y automatically changed back to default value.
	[21990] Format property does not work

- C1Dialog
	[21430][ASP.Net Wijmo][C1Dialog] After minimize and restore the C1Dialog, button inside the C1Dialog does not postback by clicking on it.

- C1Menu
	[20912] When setting value of "CrumbDefaultText" in both Design time and Runtime, C1menu isnot applied.
	[20547] "NavigateUrl" property is not working after using "Enter" key when "Mode" property is set to "Sliding"
	[20975] Drop-down Menu and Submenu items are overlapping and two items are selected at the same time after clicking top-level drop down arrow and mouse move to drop drown menu’s item immediately.
	[21458] Inside C1Tab control, C1Menu’s ScrollBar can be scrolled although setting C1Menu’s Enable to False?

- C1Wizard
	[21037] C1 controls inside C1Wizard Step remains in disabled state after Enabled property of C1Wizard is set from false to true

- C1Tabs
	[21087] When setting the Enable property to true from False, its child tabs are not changed back to enabled


- C1LineChart
	[20821] Unlike 'Numeric' value, 'DateHole' property does not work on 'Date' value
	[22243] Unable to handle NaN values.
	[20416] Unlike runtime, Fontsize property under ChartLabelStyle is partially appeared at design view if font size increased.

- C1CompositeChart
	[21367] Jscript error occurs when "Fasle" is set in the "Enabled" property of C1CompositeChart
	[21442] JScript error occurred when the chart series is hidden by clicking chart legend after "stacked" client-side option is set as "true"
	[21256] Y-Axis is overlapped with X-Axis if the "Compass" property of Y-Axis is not set
	[21232] C1CompositeChart is displayed as black rectangle at design view
	[20669] At run-time, en-US culture is not reflected for AnnoFormatString in Japanese Environment
	[21454] Unlike Column Chart Series and MS-Excel, "1" is taken as "100%" in Line Chart and the value larger 
			than 1 are not visible in line chart series if "is100Percent" client-side option is set as true

- C1ScatterChart
	[20876] 'Fill.Color' does not work under ChartLabelStyle

- C1BarChart
	[23292] 'Culture' property does not work

- C1PieChart
	[21449] Color of data that exceed count 12 , apply only first data's color
	[23297] Setting X property of legend at client side does not work.

- C1BubbleChart
	[21253] Unlike previous build, axis Y labels and text are displayed on Axis X if  Axis.Y.Text property is set
	[21301] Jscript error occurs on setting Enabled = False in C1BubbleChart

C1Charts
	[23002] HintField property does not update on rebinding.
	[22069] Legend forces Chart to Shrink.

- C1SuperPanel
	[21133] Cannot set value to ResizableOptions.MaxWidth property in properties grid.

- C1TreeView
	[21038] The C1TreeViewNode from C1TreeView Designer Form is updated either the (ID) property is set or the (Text) property is set.
	[21109] showCheckBoxes client-side option does not work without PostBack
	[21048] The value of 'Visible' property of C1TreeViewNode is not saved when C1TreeView is Saved as XML through C1TreeView Designer Form.
	[21074] C1TreeViewNode can be dragged and dropped although Enabled=False is set for C1TreeView.
	[21078] Animation Easing is not applied when Easing is set for CollapseAnimation or ExpandAnimation.
	[21161] Trial-License About Box Behavior of C1TreeView, C1Upload is inconsistence with other C1Wijmo controls in localized version.
	[21010] C1TreeView Designer Form is closed when CheckState property is reset
	[21028] When AllowEdit=True is set, Text cannot be edited properly in IE9, IE8, FireFox and Chrome
	[21276] TreeView control always return null on Page load event when it is placed inside the Update Panel.
	[21031] Add button of 'String Collection Editor' for 'Option' Property is dimmed.
	[21422] SelectedNodeChanged event is fired only for first time when C1TreeView is in UpdatePanel.

- C1ProgressBar
	[20937] Control is still working although Enabled property is set to false

- C1Carousel
	[21054] C1Carousel cannot navigate next image after Easing is applied
	[20915] If PagerPosition.At property  is set to "Left",  Pager is clipped with C1Carousel

- C1Upload
	[21161] Trial-License About Box Behavior of C1TreeView, C1Upload is inconsistence with other C1Wijmo controls in localized version.

- C1GridView
	[20637] "Visible" property of "PagerSettings" do not work in C1GridView.
	[20737] "Allow custom paging" checkbox do not work in "Paging" tab of "Property builder" window.
	[20601] Footer text do not display in boolean type column of C1GridView at run-time.
	[20572] "Filter" button from filter row disappears after setting "ShowRowHeader = True"
	[20589] "Edit", "Delete" and "Select" buttons are displayed inside Filter cell of filter bar on scrolling vertical scrollbar of 
			C1GridView in a certain scenario
	[22988] Javascript error [Unable to get value of the property 'nodeType': object is null or undefined] is thrown in pressing 
			the navigation key if AllowKeyboardNavigation="True"
	[23166] Incorrect data is displayed and editing does not work in callback mode.
	[23088] Data is not fully displayed in grouped C1GridView.
	[23211] Columns are not fully displayed in merged C1GridView if column width is not set.
	[23183] Row merging does not work for column of Date data type.

- C1Editor
	[20731] Text is not retained in the TextEditor when switching mode
	[20609] Not able to do editing on the created link
	[21142] When localized build is registered for Trial-License, Evaluation Message is not displayed at run-time
	[21152] Dropdown list for Anchor option remains  when choosing Local File option in Insert HyperLink Dialog
	[20568] Microsoft Jscript Runtime error thrown on clicking Spelling button
	[20738] Microsoft Jscript Runtime error thrown on clicking ImageBrowser button
	[21292] Editor is not displaying properly when it is placed in UserControl and this UserControl is placed in a sub folder
	[23258] Indent and outdent icons are switched in C1Editor
	[22852] Image Size is not automatically set when Image source is an Image from HTTP page
	[23003] Unable to assign a hyperlink to an image

- C1ComboBox
	[21479] Combo Box error when setting the AutoPostback property to true in IE.
	[20558] When "C1Accordion" or "C1Expander" is collapsed, the opening drop down of "C1ComboBox" present inside it do not close
	[21403] Item with "Selected=True" set from design time is still selected although another item is selected at run time and navigated back
	[21421] When C1Combox's selectionMode is set to Multiple in Wizard Control; all selected items are not retained after navigated back

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.58/4.0.20121.58)
==================================================================================

- C1GridExtender, C1GridView
	New property:

	public string LoadingText { get; set; }

	This property determines the text to be displayed when the grid is loading during
	an ajax operation.

Corrections
-----------
- C1Upload
	[21019] Upload progress bar shows NaN after uploading files

- C1TreeView
	[21034] Fixed issue with DisplayVisible at design time.

- C1BarChart
	[20750] Invalid image is seen at Runtime if Y-Axis contains null value in IE8 compatibility view.
	[20600] The value for 'Axis.X.Compass' property  and 'Axis.Y.Compass' property cannot be selected at design time.

- C1ScatterChart
	[20886] SeriesHoverStyles property was not working.
	[20631] Y-axis was not positioned correctly when setting Axis.Y.Compass = South.

- C1LinearGauge
	[21110] With Horizontal 'orientation', 'TickMajor' and 'TickMinor' render incorrectly when the shape was set
			to 'Circle/ Tri/ InvertedTri/ Box/ Diamond'
	[20786] "Fill" property of TickMajor was not working
	[20753] TickMajor and TickMinor disappear on setting their 'Marker' property as "Cross"

- C1RadialGauge
	[21160] The pointer was not rendering when setting the value property to a value greater than zero.
	[20540] Pointer disappears when setting the Cap.BehindPointer=False at design time

- C1BubbleChart
	[20702] Multiple issues are found when adding more than 12 data series

- C1CompositeChart
	[20810] Setting the enable property client side was having no effect.

- C1ComboBox
	[21029] Selected item is not displayed in Single selection mode when setting C1ComboBoxItem.Selected=True
	[20689] Multiple issues are observed with ItemTemplate.

- C1GridView
	[20831] Column data was shifted to the previous column when inserting new fields in C1GridView dynamically.
	[20951] Grouping on DateTime field does not work.
	[20838] Cell text are not rendered as bold and not centered horizontally when RowHeaderColumn property is set with a column name.
	[20801] When added a column in C1GridView, footer text of a column is duplicated to the next adjacent column.
	[20776] Java Script error occurs on clicking Select or Edit command button when C1GridView has a Banded column
	[20775] Filter drop-down is shown in filter row of C1GridView at run time even when the ShowFilter property of an individual column is set 'False'
	[20684] Java Script error occurs on setting AutogenerateColumns = False when C1GridView has a CheckBoxField
	[20677] Java Script error occurs on selecting a row from C1GridView which has invisible column
	[20658] Java Script error occurs on clicking column header of AutoGenerate columns when ‘AllowSorting?property is set True
	[20559] Grouped column can be ungrouped and mouse over style are getting applied in a disabled C1GridView
	[21014] When Band is added inside the grid, column width are not working due to the C1Band when browsing with Chrome and Safari

- C1Wizard
	[21058] Two wizards with different Delay values are auto-played at the same time span
	[21608] Blind property of HideOption/ShowOption seems was not working
	[21079] Unlike design-time, CssClass property did not apply at run time
	[21112] Step headers are clipped or transparent at design-time when Description property is set in wizard steps

- C1Editor
	[20619] Permission denied java Script error is observed after canceling the Tag Inspector dialog and clicking the 
			link toolbar button in FullScreen mode


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.57/4.0.20121.57)
==================================================================================

Corrections
-----------
- C1Wizard
	[21017] ActiveIndexChanged event is not fired on changing ActiveIndex value by clicking back/next button of C1Wizard

- C1LightBox
	[20861] C1LightBox is still working after setting true to disabled option from client-side
	[20870] In C1LightBox Designer, Images are not displayed in 'Preview' when Preview Tab is clicked for the second time
	[20873] In C1LightBox Designer, the text of LightBoxItem from the Edit Tree-view is inconsistent
	[20882] "OnClientShow" event is not fired when the first slide is shown in Large Image Dialog
	[20914] Unlike AutoPlay client-side option, the ClickPause client-side option does not work without PostBack
	[20933] Content MaxWidth value  is applied to Controls Width after PostBack
	[20938] showNavButtons client-side option does not work without PostBack
	[20959] [IE9] showControlsOnHover client-side option does not work without PostBack

- C1Dialog
	[20895] C1Dialog's Size is changed on Asynchronous Postback when C1UpdatePanel's AsyncPostBackTrigger is set.

- C1Menu
	[20650] Sliding Menu disappears on click
	[20651] Menu recreation refuse to close

- C1Input
	[20712] CssClass property doesn't apply at design and runtime.

- C1Calendar
	[20719] 'MaxDate' can be set to a date less than 'MinDate' property
	[20381] DayCols is obsolete.
	[20780] Although SelectionMode.Month=True and ShowWeekNumbers=True is set, MonthSelectorImage disappears after postback
	[20979] Although SelectionMode.WeekDay=true and SelectionMode.WeekNumber=true are set, days under WeekDay and WeekNumber
			cannot be selected after postback

- C1GridView
	"System.Web.HttpException: 'C1GridView1': if a data source does not return DataView then it cannot be used to implement automatic 
	filtering" is observed after C1GridView is bound to an SqlDataReader and the FilterValue\FilterOperator properties are not set.
	[20551] Editing and Selection enums of CallBackSettings.Action property are not reflected immediately on setting those properties at run time

- C1Editor
	[20372] 'Undo' removes complete word instead of the formatting applied on the text in the editor
	[20782] Default C1Editors context menu is not displayed when browsing with IE although CustomContextMenu property is set to true
	[20812] Path Selector is not displayed at Runtime when ShowPathSelector= True
	[18143] Texts written in the editor are not retain after postback
	[20788] Browsing Page/File is not opened up for added URL/FileUpload which is set using Insert Hyperlink dialog

- C1EventsCalendar
	[20735] New Event dialog disappears if [Starts or Ends] date from the Calendar's last two columns (Fri , Sat) is clicked

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.56/4.0.20121.56)
==================================================================================

Synced with C1Reports 2012 v1 release.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.55/4.0.20121.55)
==================================================================================

Corrections
-----------
- C1GridView
	[19442] When same inline css is applied to both C1SuperPanel and C1GridView placed in it, then it gets applied on C1SuperPanel but not on gridview.
	[20505] ‘AllowGrouping?checkbox is not able to check through ‘Property builder > General window
	[20535] Unlike MS GridView, C1GridView size is extremely small and default empty grid is not displayed when grid is using unbound data

- C1PieChart
	[20588]	Microsoft JScript error occurs when data is dynamically set from the client code in IE9.

- C1EventsCalendar
	[20618] C1EventsCalendar remained in loading state when it is browsed with IE9 or IE8.

- C1InputDate
	[20634] Microsoft JScript run time error: error occur when browse in IE9 in Japanese locale.
	
- C1Charts
	[20641] The toggle show/ hide legend color icon was not working.



==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.53/4.0.20121.53)
==================================================================================

Corrections
-----------
- C1TreeView
	[20521] NavigateUrl is not working with the Enter key.
	
- C1Carousel/C1Gallery	
	[20519] Invisible carousel/gallery item become visible after postback occur
	
- C1Calendar
	[20409] Multiple dates cannot be selected using CTRL key although 'SelectionMode. Days=True' is set as default value

- C1InputDate
	[20476] Calendar does not appear when clicking on the Trigger button in IE8.

- C1InputMask
	[20508] JScript error: Object doesn't support this property or method occurs in IE8 when ShowFilter property is set to True
	[20504] Unlike design time, picker for the Day of week picker chooser property appears irrelevant at runtime

- C1LightBox
	[20522] javascript error is observed after adding video and images files in link url in the same lightbox control in certain scenarios
	Added Supports the wijmo HTML5 Video Player.

- C1Editor
	[20303] No media file is displayed  if set valid 'Media URL' and the media rectangle is disppeared when double clicking on it
	[20446] Inserted special characters are appear outside of C1Editor when browsing with IE
	[20450] Inserted images are always appear in one location although cursor is set in different location with IE browser
	[20382] Table is disappear and copied text is appear  after copying text inside table and paste using context menu in the editor
	[20275] 'OK' button is useless to choose background color in 'Insert Table' color box
	[20253] Unlike released build, Text is not displayed although selecting SourceView at ToolBar
	[20291] Table is able to edit by placing cursor inside the cell instead of selection on the table
	[20300] Text cannot be input in Editor area if the table is created first
	[20316] Finding words has been skipped in Editor's box after modification is behaved
	[20444] 'Cancel' button is not working in  both 'Clean up' and 'InsertTable' dialogs after opening 'background' color dialog
	[20488] Unable to cut/copy and paste the table using context menu in C1Editor.
	[20383] Adjusted CSS to fix display issues in chrome

- C1ComboBox
	[20257] Although selecting multiple items , only last selected combo item is displayed in the textbox area when AutoPostBack is set to true

- C1BubbleChart
	[20403] Unlike C1BarChart, Size property (Width, Height) do not work in Legend of C1BubbleChart
	[20453] Jscript error Undefined is null or not an object occurs on setting LegendEntry property in a certain scenario
	[20419]	Stroke property of GridLineStyle is not reflected in ValueLabel of X and Y Axis
	[20365] Labels.TextAlign property do not work correctly in X and Y Axis

- C1BarChart
	[20417]	ClusterRadius does not work properly when Negative values are used in ChartSeries or Column type is set
	
- C1PieChart
	[20442] Unlike previous released build, Shadow is moving to different direction while the PieChartSeries is animated
	[20443] Unlike previous released build, After hovering a pie chart element, the chart element is not moved back to original position automatically when the mouse is moved to another element of PieChartSeries
	[20422] Unlike previous build, TextStyle Rotation property under Header, Footer and Legend do not work when setting Scale property
   
- C1CompositeChart
	[20462] HintContents are not displayed in PieChart, BarChart, ColumnChart and ScatterChart
	[20415] At run-time, the position of the Axis and the position of Origin is not identical when  Bar Chart Type is set
	[20364] Culture is not reflected for the AnnoFormat
	[20405] All ChartSeries except PieChart become invisible when the LegendEntry properties of each series are set as False in a certain scenario
	[20424] At both run-time and design-time, Major GridLines of Y-Axis are still visible although Axis.Y.GridMajor.Visible property is set as False
	[20440] Unlike Major GridLines, Minor GridLines are not visible if TickMinor.Position is not set explicitly

- C1GridView
	[20512] Unlike released build, Jscript error occurs on filtering Boolean column of C1GridView with boolean value 'True'	
	[20496] JScript error occurs on setting ‘ScrollMode’ property in a certain scenario
	[20571] Unlike previous released build, text cannot be entered in filter row on setting ‘ClientSelectionMode’ property
	
- C1RadialGauge
	[20539] Setting 'Cap.PointerCapStyle.Fill.Color' was not rendering.


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.52/4.0.20121.52)
==================================================================================

Enhancements/Behavior changes
-----------------------------

A new control, Event Calendar has been added to the studio.

The Events Calendar Control is a fully functional schedule that allows users to add, edit, 
and manage their appointments. By default, the Events Calendar uses an XML data source, so 
you can easily add this Control to your application without additional configuration.

- C1Inputs
	New property was added to C1InputDate
		public bool AutoPostBack { get; set; }
	When this value is true, the page will postback when the control loses focus.

- C1Upload
	You can now select multiple files at once in the same folder.
	You can now modify the Default File filter when 'ValidFileExtensions' is set to some file extensions

- C1LineChart
	Added more DataBinding options for properties of all Charts. LegendEntry, Visible and Label can now be bound 
	to the DataSource for more control over rendering.

Corrections
-----------
- C1CompositeChart
	[20389] Tick is not visible when the Position is set as Inside in X-Axis
	[20391] Tick Factor does not work properly when the Tick Position is set as Inside
	[20360] Same animation effect is applied in C1CompositeChart on setting any enum values of Animation.Easing property
	[20304] [ASP.Net Wijmo][C1CompositeChart] Unlike released build, the data series are not hidden correctly by clicking the toggle legend when PieChartSeries are used

- C1LineChart
	[20345] Animation properties has no effect on C1LineChart Animation
		[16828] [C1LineChart] Request to provide some properties for the data seres which has bound data
	[16828] [C1LineChart] Request to provide some properties for the data seres which has bound data

- C1PieChart
	[20334] Animation duration is not reflected for PieChartSeries on mouse over

- C1BubbleChart
	[20331] Same animation effect is applied in C1BubbleChart on setting any enum values of Animation.Easing property
	[20313] Length is null or not an object Jscript error is observed when C1BubbleChart has BubbleChartSeries and postback occurs
	[20321] SeriesHoverStyle is still applied in chart element after hovering mouse over chart element and mouse is moved to outside of chart element
	[20338]	Callout triangle of chart tooltip is shown on setting ShowCallout = False when CalloutFilled is set to True
	[20367] Compass enum (North) do not work in the Footer at both design time and run time
	[20386]	Bubble chart series is still displayed on setting the Visible property of bubble chart series is to False

- C1Calendar
	[20337] DisabledDates are not displayed in disabled style after postback

- C1Inputs
	[20169] Empty Date Fields

- C1TreeView
	[20260] Javascript error is observed after clicking disable tree view item in IE8.
	[20254] TreeView item text changed after postback occurs when DisplayVisible property of one of the item is set to false

- C1Editor
	[20290] The deleted table is not actually cleared in the Editor box before refreshing the browser
	[20288] Invalid warning dialog box is opened when clicking on the 'Cleanup' button after a table is created
	[20275] 'OK' button is useless to choose background color in 'Insert Table' color box
	[20276] Multiple warning message box is displayed with incorrect name for required value of the text box
	[20270] 'FontColor' is applied to background if primitive sentence on page is set with "Background Color" &  setting BackGround Color covers text
	[20305] Warning messages were incorrect in the Insert dialogs (Table, Image Browser, Hyper Link, Special   Character,etc)
	[20289] The title name with wrong spelling 'Clearnup' is used instead of 'Cleanup'
	[20316] Words were being skipped when using the Find dialog after the contents of the editor were modified.
	[20268] Unlike MSWord,  the alignment set on sentence is not shown as 'selected' in C1Editor menu
	[20016] SpecialCharacters and images are inserted multiple times.
	[20287] When Apply Template” dialog box is canceled and forecolor or Backcolor is applied,  'contentWindow' is null or not an object” javascript error is observed.
	[20370] Error message box is displayed after clicking Spell Checking button in editor when it is placed in web page that use a MasterPage
	[20382] Table is replaced by text after copying text inside of a table and pasting it using context menu in the editor
	[20372] 'Undo' removes complete word instead of the formatting applied on the text in the editor

C1ComboBox
	[20256] Javascript error is observed after selecting combo items when AutoPostback property is set to true and "SelectOnItemFocus" property is set to true.
	[20253] 'Selected' property of ComboxItem does not work
	[20257] Although selecting multiple items , only last selected combo item is displayed in the textbox area when AutoPostBack is set to true
	[20255] Disable and invisible combo item are still working although Enabled and Visible property of these combo item are set to false
	[20174] SelectedItem.Value property does not work
	[20320] SelectedIndex's value that was set at design time was not being retained after postback.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.51/4.0.20121.51)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1ReportViewer
	Added new client side methods:
		/// 
		/// Force the loading of visible pages textual data.
		/// This method is primarily used by control developers.
		/// 
		/// <param name="skipFastCalls" type="Boolean" />
		///	Optional. If true then the data will be loaded 
		///	after timeout in order to avoid frequently calls
		///	(e.g. during document scrolling).
		///	
		/// <param name="completeCallback" type="Function" />
		///	Optional. Function which will be called when data is loaded.
		///	
		loadVisiblePagesData(skipFastCalls, completeCallback)

		/// 
		/// Update page image by its index.
		/// This method is primarily used by control developers.
		/// 
		/// <param name="pageIndex" type="Number" />
		///	Page index to load.
		///	
		/// <param name="forceUpdate" type="Boolean" />
		///	Optional. Set this parameter to true if you want to 
		///	update an already loaded image.
		///	
		updatePageImage(pageIndex, forceUpdate)

- C1PieChart
	[18590] You can now define the series style through the 'CssStyle' property in C1PieChart

Corrections
-----------
- C1Editor
	[20020] Text displayed is not retained after postback when the upload button is clicked without selecting the file path.
	[20017] Text displayed in the design view are moved to the source view and Split view is displayed after 
			clicking "Tab" key although C1Editor is in design view.
	[20016] SpecialCharacters and image are increased by one when number of inserted times is odd times.
	[20022] When the text of C1Editor is retrieved and displayed in the textbox, the existing images in the listbox 
			of "Image Browser" Dialog are not displayed
	[19836] C1Editor's Toolbar buttons in Status bar are displayed vertically at Design Time in VS11.

- C1ComboBox
	[19934] Text property does not refresh when an item is selected the second time.
	[20174] SelectedItem.Value property does not work.(Add selectedValue property)

- C1Input
	[19886] InvalidOperationException is observed when placing script manager on the same page with C1Input controls
	[19945] Dropdown calendar is not localized in C1InputDate although Culture property is set
	[19953] Decimal is ignored & Incorrect value is returned if Culture is set to Danish (da-DK)

- C1LightBox
	[20119] Error message is displayed when adding LightBox item from the C1LightBox Designer

- C1Calendar
	[19689] The title and dates are misaligned in a certain scenario with default theme

- C1Tabs
	Fixed an issue that the disabled tabs are not really disabled.

- C1ReportViewer
	[19971] Performance issue on loading report for the first time

- CGridView
	[19999] Unable to collapsed or expand the grouped row when grid is in scroll mode after setting ShowRowHeader?to true.
	[20000] Scroll bar are still working and Freezed rows are not dim although grid is disabled.
	[20001] Unable to resize the column although AllowColSizing?property is set to true.
	[20039] When ScrollMode" property is set to Auto" and setting the column width, the width set in the column is not applied at run time.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20121.50/4.0.20121.50)
==================================================================================

Corrections
-----------
- C1CompositeChart
  ----------------
	[19834] Javascript Error is observed after mouse hovering over the data point of scatter chart type

- C1Carousel
  ----------
	[19721] Click event is not raised when adding button in ItemContent of C1Carousel

- C1Menu
  ------
	[18483] OnClientFocus and OnCientBlur events are not fired when Mode is Sliding.

- C1Editor
  --------
	[19406] Text property of C1Editor was not working properly in all browsers except IE
	[19401] "Text" property return nulls when uploading any image and type any text in the editor
	[19693] "DefaultFontName" And "DefaultFontSize" are not applied in IE8 and IE9 although these properties are set in C1Editor

- C1LightBoxExtender
  ------------------
	[19723] Disable light box extender is still working although Enabled is set to false

- C1LightBoxExtender
  ------------------
	[19724] Inconsistence DialogButton icon are displayed after closing the image when it is in Fullscreen mode
	[19748] LightBox item that are added or deleted at run time are not retained after postback

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20113.49/4.0.20113.49)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1Editor
  --------
	[18647] You can now specify the default font and add new fonts in C1Editor.

- C1BubbleChart
  -------------
	Add an "SizingMethod" property to C1BubbleChart.

Corrections
-----------
- Common
  ------
	JSON Writer: fixed stackoverflow problem with Guid values
	Fixed problem with default localization value.

- C1GridView, GridExtender
  ------------------------
	StaticColumnIndex property added.
	The behavior of the StaticRowIndex property is changed, now this property indicates 
	the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.

- C1Tabs
  ------
	[18574] Multiple issue are occured when C1TabPage's 'visible' properties is set to 'false'.
	[18576] Scrollable property does not work if tabs are added at client side.

- C1InputDateExtender
  -------------------
	[19131] JS Error is observed when 'Culture' property of C1InputDateExtender is set

- C1SuperPanel
  ------------
	[18888] NullReference exception occurs when binding the C1GridView control inside the C1SuperPanel control

- C1TreeView
  ----------
	[18921] Nodes cannot be collapsed when 'ShowExpandCollapse' is set to false.
	Add methods "FindNodeByText", "FindNodeByValue", "FindNodeByNavigateUrl".

- C1Editor
  --------
	[18777]'TextSaved' event does not work.

- C1Gallery
  ---------
	[18113] Caption is not displayed although Caption property is set for each gallery items and ShowCaption property is set to true

- C1GridView
  ----------
	[18803] Data is filled in C1BoundField only and does not filled in TemplateField at first Button_Click event.

- C1Menu
  ------
	Fixed a bug that the visible property lost on page postback.


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20113.47/4.0.20113.47)
==================================================================================

The v.3 release of ComponentOne Studio for ASP.NET Wijmo contains a number of new
controls.

Editor
------
The rich text editor has been rewritten as a Wijmo control. It will feature the same 
powerful editor surface with WYSIWYG, Code, and Split views. It also boasts a Microsoft 
Word-style Ribbon.

Linear & Radial Gauges
------
This release features new HTML5 Gauges powered by SVG and Wijmo. These controls are 
highly interactive with built-in animation, transitions, themes, and tooltips.

Multimedia Controls
--------
This release adds a new family of graphical controls. These controls are focused around 
modern website development and offer interactive ways of displaying media.

	Gallery – A gallery for displaying photos/images
	Carousel – An interactive UI for navigating media elements like photos
	Lightbox – A pop-up image viewer used to display full-size images after a thumbnail is clicked

HTML5 Video Player
------------------
The new HTML5 Video Player is an interactive version of the browser’s video player. It 
surfaces stylized UI that matches the current Theme. It also surfaces all of the features 
in a video player like volume, play, pause and timeline.

More Chart Types
----------------
To make sure you can port your WebCharts to Wijmo charts we've added more chart controls to 
the studio. This includes Area, Bubble, Scatter, and Spline chart types.

Rating
------
The new rating control is an input for giving fields a numeric rating. It supports different 
visuals like stars, thumbs, and bars. It can handle any range of values for the rating system.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.31/4.0.20112.31)
==================================================================================

Corrections
-----------
- C1Dialog
	[17930] When dialog is change from browser maximize state to browser restore state or restore state to maximize state the 
	collapsed dialog is change to expand dialog although the collaspse/expand icon button is still shown as collapsed.

- C1Combobox
	[17873] "items is null or not an object" error is observed in when clicking the drop down of the combobox using callback data bind event.
	[17871] Explode animation is always displayed behind the content of the control explorer in Firefox and Chrome.

- C1Calendar
	[17870] Unable to select the month that is between the Min/Max range after selecting with QuickNavigation

- C1Accordion
	[17869] Unable to select the desire panel after all the panels are open in certain scenarios.

- C1ReportViewer
	[17043] The design time selected zoom value is not shown in the current zoom value dropdown at run time
	
- C1GridView
	17783 Row header enlarges if any other column width is explicitly defined.
	17931 Incorrect grouping is observe in grid after grouping Boolean column.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.30/4.0.20112.30)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1GridView
	If the SelectedRowStyle.CssClass property is now defaulted to "ui-state-highlight".
	Clicking on a cell in the grid no longer highlights the cell.  To turn this behavior on set the
	HighlightCurrentCell property to true.

- C1InputDate
	[17099] Added MinDate and MaxDate properties.

Corrections
-----------
- C1GridView
	"Object doesn't support this property or method" Javascript error is thrown when AutogenerateColumns property is turned on.
	[17695] C1GridView is invisible in firefox  after setting ShowFooter?property to true.

- C1Combobox
	C1Combobox was resizing after a postback.
	[17528] The selectedItem was changed after a postback when the AutoComplete property was set.
	[7727-139] Unable to set Width property of C1ComboBox to 100%

- C1ToolTip
	[17696] Invalid JSON text exception is observe after placing another C1ToolTip and another wijmo controls on the form and postback occur.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.29/4.0.20112.29)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1InputDate
	Added MinDate and MaxDate properties.

Corrections
-----------
- C1GridView
	C1Combobox controls in the template columns look broken.
	[16951] InvalidOperationException is thrown when rendering GridView to Pdf file.

- C1Chart
	[17651] "There was an error rendering the control" error is osberved after placing  C1Chart on the form

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.28/4.0.20112.28)
==================================================================================

Corrections
-----------
- C1PieChart
	Incorrect this context was being passed to the hint function.

- C1GridView
	[17115] Unable to add a Wijmo Combobox to a templated item.
	[17392] AjaxToolkitExtenders es not work when used with C1GridView.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.27/4.0.20112.27)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1Combobox
	Added template support.
	[17049] Added SelectedItems and SelectedText properties.

Corrections
-----------

All controls
	[17407] Fixed conflicts when page contains both server controls and extenders in certain scenario.

- C1Dialog
	[17299] The page was not posting back when clicking a Button which is inside WebUserControl that is called by C1Dialog.

- C1Upload
	[17044]/[17311] Callback error is observed when C1Upload is used in master content page.

- C1Chart
	[16823] The pop up message box 'hint' does not extend outside of the chart area.

- C1ReportViewer
	Fixed regression issue with layout in percentages when page is loaded for the first time.

- C1Menu
	[17037] C1MenuEventArgs returns the first hidden menu item instead of the menu item being clicked in ItemClick event.
	[16408] Width and Height property is not working properly in C1Menu.
	[16326] "OnClientBlur" and "OnClientFocus" event fired twice in C1Menu in certain scenarios.

- C1GridView
	[16936] Fixed "'options' is null or not an object" javascript errror is observed when data bind the grid at run time with code
	[17079] GridView throws JS error on binding at runtime
	[17154] Can't set column width
	[17029] The Columns Treeview in the Format tab of the property builder need to use fields names if available
	[17262] Cannot navigate to any page in C1GridView if the web page has scriptmanager when browsing in FireFox 6.0.2
	[17113] Fixed issue with Templated Column Header.
	Fixed exception when AllowCustomPaging property is turned on and the datasource doesn't support IColletion.
	Name of the "args" parameter is changed to "e" for all server-side events.
	Fixed issue: invisible columns can't be grouped.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.26/4.0.20112.26)
==================================================================================

Corrections
-----------
- C1ReportViewer
	[16990] Error 'this.options.documentStatus.pageCount...' occurrs when choosing any value in 'CurrentZoom' dropdown

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.25/4.0.20112.25)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- C1ReportViewer
	Added new client side method exportToFile.
		$("#report1").c1reportviewer("exportToFile",  exportFormat, exportFormatExt);

	This method exports the current active report to a file.  The exportFormat argument is one
	of the documentStatus.exportformats member.  The exportFormatExt is the file extension (without the '.').

	Added new property ReportsFolderPath: 
		public string ReportsFolderPath { get; set; }

	This property gets or sets the relative path to folder that will be used to store generated report data. Default value is "~/tempReports"

Corrections
-----------
- C1Chart
	[16903] JScript error :'Object doesn't support this property or method' is thrown if the same page has C1Chart and other wijmo controls.

- C1Menu
	[16918] Items texts on the menu is not displayed after adding Headeritem in the menu in IE 8 compatibility view mode
	[16929] The template ‘button control disappears after a postback.

- C1Slider
	[16945]	Track of the slider increases its height after a postback.

- C1Input
	[16955] When typing one number in C1Input ,two duplicated number are  displayed in IE 8 compatibility view mode.

- C1ReportViewer
	[16938] javascript exception is thrown when FileName and ReportName properties are empty.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.24/4.0.20112.24)
==================================================================================

Corrections
-----------
- C1TreeView
	[16911] Fixed design time control rendering after adding tree nodes
	[16930] "'JSON' is undefined" Javascript error is observed when postback is occur in IE 8 compatibility view mode

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.23/4.0.20112.23)
==================================================================================

Enhancements/Behavior changes
-----------------------------
- All controls
	Updated to use jQuery UI 1.8.16.

- C1Accordion
	[16380] You can now navigate the the accordion panes using the keyboard when the accordion header has focus.

- C1Input
	You can now change the AM/PM section by using the 'a' or 'p' key.
	Added support for copy and paste.

Corrections
-----------
- C1TreeView
	[16801] Webpage Error is observe after clicking tree node that is added at run time when server side node click event is set

- C1Input
	[16679] Unable to validate the C1Input with validation controls

- C1ComboBox
	[16679] Unable to validate the C1Combobox with validation controls
	[16818] Exception is observed when C1ComboBox inside Repeater is bound with data.

- C1Chart
	[16850] Unable to see all C1Chart UI at design time.
	[16827] Xml web page error is thrown when Axis X is binding with string datatype in C1Chart.
	[16824] JScript error is thrown when hovering mouse over any data point which set 'Compass' to 'NorthEast' for 'Hint'.

- C1GridView
	[16843] 'OnClientColumnUngrouping()' event is not fired when removing column in the grouping area
	[16853] When 'PageIndex' property is set at design time , it does not take effect to C1GridView at run time

- C1Splitter
	[16392] An Error is observe when placing 3 level nested splitters and setting FullSplit property of all the splitter to true.
	Expander wasn't rendered correctly when nested.
	Hover style wasn't being applied correctly to the sizing bar.

- C1Calendar
	UI actions weren't working when using jQuery 1.6.2.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.22/4.0.20112.22)
==================================================================================

Enhancements/Behavior changes
----------------------------
- C1GridView/C1GridExtender
	The grid now supports interactive grouping of columns.

- C1GridView
	New events added: OnColumnGrouped, OnColumnGrouping, OnColumnUngrouped, OnColumnUngrouping.
	CallbackAction.Grouping value added.
	Added ShowGroupArea property to provide user with a convienient way of grouping.
	New properties added: GroupAreaCaption, ShowGroupArea, OnClientColumnGrouping, OnClientColumnUngrouping.


- C1GridExtender
	New properties added: OnClientColumnGrouped, OnClientColumnUngrouped, ReadAttributesFromData.
	Added ShowGroupArea property to provide user with a convienient way of grouping.
	New properties added: GroupAreaCaption, ShowGroupArea, OnClientColumnGrouping, OnClientColumnUngrouping.

Corrections
-----------
- All Controls
	Fixed json date serilization.

- C1Accordion
	[15949] Fixed first time drag issue onto the design surface.

- C1SuperPanel
	Content control is clipped after resizing.

- C1Dialog
	"Pin" button is not working correctly when C1Dialog is in Maximize state

- C1Wizard
	[16388] "C1WizardDesigner" close automatically after selecting the rename menu in and click anywhere in the designer
	[16409] Incorrect control name is mention at the title of "Designer Form"

- C1Slider
	[16695] "Object doesn't support this property or method" javascript error is observe when running C1Slider in compatibility View in IE 8 and IE 9.

- C1ComboBox
	[16387] Second levels child Items can be added like C1Menu in the C1ComboBoxEditor at design time.
	Added "Text" property in server.

- C1Splitter
	[16391] Collapse/Expand button is not displayed in the nested split after collapsing the panel1.

- C1Menu
	Add menu item disabled support.
	[16633] "Header" and "Separator" are save as "Link Item" in xml file, after saving c1menu as xml

- C1GridView
	An edit mode is enabled for a row only after the second click on Edit when callback editing is enabled.
	JavaScript exception is thrown during callback when one of the OnClient<EventName> properties is set.
	The sequence of column is not correct when drag the first column to the left of the third column.
	Grid can not show the sorted data after setting groupInfo.
	[16631] 'OnClientFilterOperatorsListShowing' event does not get fired on filter drop down is opening.
	[16632]  Filter value 'Contains' does not take effect to C1GridView after setting 'NotEqual' filter value in  the other column.
	[16473]  All records are not shown when databinding to an SqlDataReader.


==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.21/4.0.20112.21)
==================================================================================

Corrections
-----------
- All controls
	Version of the wijmo scripts for CDN was upgraded from 1.3.0 to 1.4.0
	Fixed design time problems when Theme property contains url to external stylesheet.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.20/4.0.20112.20)
==================================================================================

Enhancements/Behavior changes
----------------------------
- (C1GridView)
	AllowCustomPaging and VirtalItemCount properties added.
	New property added: ClientEditingUpdateMode.
	New events added: BeginRowUpdate, EndRowUpdated.
	If CallbackSetting.Action includes CallbackAction.ColMove but is not equal to CallbackAction.All column moving is performed via postback instead of callback.

- (C1GridExtender)
	AlwaysParseData and ColumnsAutogenerationMode properties added.

- (C1GridView, C1GridExtender)
	OnClientFilterOperatorsListShowning property is renamed to OnClientFilterOperatorsListShowing.
	OnClientColumnResizing and OnClientColumnResized properties added.

Corrections
-----------
- All controls
	Behavior for Theme property was corrected:
		Runtime:
			Theme stylesheet will be not linked when Theme property is empty.
		Design time:
			Fixed problem when path to theme is specified using relative path, e.g. "~/css/custome-theme.css" or "css/jquery-wijmo.css"
			Default embedded theme(Aristo) will be linked when url given by Theme property is absolute or UseCDN property is set to true.
			Theme stylesheet will be not linked when Theme property is empty.

- (C1GridView)
	Exception is thrown during postback or callback when one of the columns is grouped.

- (C1Menu)
	16324 "Header" and "Separator" display and act  as "LinkItem" when adding from "menu" or "toolbar" in "C1MenuDesignerForm" after closing the designer form
	16325 xml file is not save although it is save from menu in "C1MenuDesignerForm" of "C1Menu"

==================================================================================
ComponentOne Studio for ASP.NET Wijmo (3.5.20112.19/4.0.20112.19)
==================================================================================

Corrections
-----------
- (C1GridView)
		When doing a callback causing a full content refresh an "Script controls must be registered using RegisterScriptControl() 
		before calling RegisterScriptDescriptors()" exception is thrown.

==================================================================================
ComponentOne Studio for ASP.NET Wijmo  	  GrapeCity, Inc - www.componentone.com
==================================================================================

Welcome to the next generation of components for ASP.NET powered by HTML5 and jQuery

Based upon the new Wijmo Framework, these ASP.NET controls have been completely
re-engineered from the ground up.  The new controls leverage the latest technologies
available to create the ultimate development experience including:

* Improved Performance: Fully extensible client-side and server-side 
  object models with faster load times.
* Latest Standards Support: CSS3 and HTML5 compliance.
* Major Browser Support: Internet Explorer, Firefox, Chrome and Safari.
* Themes: Built-in premium CSS3 themes and all Controls are compatible with jQuery UI Themeroller. 
* Tightly integrated with jQuery.

Community support is provided in the following forum:

http://our.componentone.com/groups/asp-net/studio-for-asp-net/forum/


- Installed Files

The ComponentOne Studio for ASP.NET Wijmo installs the following files and components.

 Files: C1.C1Report (CLR 2.0), C1.Web.Wijmo.Controls.3.dll (CLR 3.5), C1.Web.Wijmo.Design.3.dll (CLR 3.5), C1.Web.Wijmo.Extenders.3.dll (CLR 3.5)
		C1.Web.Wijmo.Controls.4.dll (CLR 4.0), C1.Web.Wijmo.Design.4.dll (CLR 4.0), C1.Web.Wijmo.Extenders.4.dll (CLR 4.0)

			 
During installation the CLR 3.5 bits will be installed in the VS2008 toolbox.  If you're using VS2010 the
toolbox will be installed with the 3.5 and 4.0 bits.

