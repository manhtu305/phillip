﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Design.C1ChartCore;
using System.ComponentModel;
using System.Drawing;
using System.Collections;
using System.Web.UI;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.Design.C1Gauge
{
    public class C1GaugeDesigner : C1ControlDesinger
	{
		C1ChartBrowser _browser = null;
		System.Drawing.Image _image = null;

		public C1GaugeDesigner() { }

		/// <summary>
		/// Initializes the control designer and loads the specified component.
		/// </summary>
		/// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (component != null)
			{
				base.Initialize(component);
				SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
				SetViewFlags(ViewFlags.CustomPaint, true);
				_browser = new C1ChartBrowser();
				_browser.ImageComplete += new C1ChartBrowser.ImageCompleteEventHandler(_browser_ImageComplete);
				_browser.ScriptErrorsSuppressed = true;
			}
		}

		void _browser_ImageComplete(object sender, EventArgs e)
		{
			// now that we have an image invalidate the control design surface to we can draw it
			_image = _browser.Image;
			this.Invalidate();
		}

		protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
		{
			if (_image != null)
			{
				e.Graphics.DrawImage(_image, new Point(0, 0));
			}
			base.OnPaint(e);
		}

		/// <summary>
		/// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions.
		/// </summary>
		/// <param name="regions"> A collection of control designer regions for the associated control.</param>
		/// <returns>The design-time HTML markup for the associated control, including all control designer regions.</returns>
		public override string GetDesignTimeHtml()
		{
			C1TargetControlBase control = Component as C1TargetControlBase;
			if (control.Width.IsEmpty)
			{
				_browser.Width = 600;
			}
			else
			{
				_browser.Width = (int)control.Width.Value;
			}
			if (control.Height.IsEmpty)
			{
				_browser.Height = 400;
			}
			else
			{
				_browser.Height = (int)control.Height.Value;
			}

			if (control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}

			// store the control visible state when is designer, the control will show all the time.
			bool visible = control.Visible;
			control.Visible = true;

			string sResultBodyContent = "";
			StringBuilder sb = new StringBuilder();
			System.IO.StringWriter tw = new System.IO.StringWriter(sb);
			System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
			control.RenderControl(htw);
			sResultBodyContent = sb.ToString();

			//remove to fix issue 18186.
			//var scriptItems = this.RootDesigner.GetClientScriptsInDocument();
			//StringBuilder sbScripts = new StringBuilder();
			//foreach (ClientScriptItem item in scriptItems)
			//{
			//	sbScripts.AppendLine(item.Text);
			//}

			StringBuilder sDocumentContent = new StringBuilder();
			sDocumentContent.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			sDocumentContent.AppendLine("<html  xmlns=\"http://www.w3.org/1999/xhtml\">");
			sDocumentContent.AppendLine("<body style=\"margin: 0\">");
			sDocumentContent.AppendLine(GetScriptReferences());
			sDocumentContent.AppendLine("<script type=\"text/javascript\">");
			//sDocumentContent.AppendLine(sbScripts.ToString());
			sDocumentContent.AppendLine("$(document).ready(function () {");
			sDocumentContent.AppendLine(GetScriptDescriptors());
			//Add comments by RyanWu@20110707.
			//I added the following strange code in order to fix the issue#15889.
			//I also don't know why the last raphael element can't be drawn in the webbrowser control.
			//In bar chart, I first draw the chart label then draw bar and shadow (if shadow = true),
			//So if shadow = false, then last bar can't be shown.  In pie chart, I first draw sector,
			//then draw chart labels.  So the last chart label can't be shown.
			//Hence, to solve this issue, we will draw a useless shape after the chart drawn.
			sDocumentContent.AppendLine("new Raphael(-100, -100, 1, 1).circle().hide();");
			//end by RyanWu@20110707.
			sDocumentContent.AppendLine("});");
			sDocumentContent.AppendLine("</script>");
			sDocumentContent.AppendLine(sResultBodyContent);
			sDocumentContent.AppendLine("</body>");
			sDocumentContent.AppendLine("</html>");
			_browser.DocumentWrite(sDocumentContent.ToString(), control, this.RootDesigner.DocumentUrl);

			//restore the control visible state
			control.Visible = visible;

			return base.GetDesignTimeHtml();
		}

		private string GetScriptReference(WijmoResourceInfo resource)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("<script type='text/javascript'>");
			sb.AppendLine(resource.ResourceContent);
			sb.AppendLine("</script>");
			return sb.ToString();
		}

		private string GetScriptReferences()
		{
			StringBuilder strScriptRefs = new StringBuilder();
			if (Component is IWijmoWidgetSupport)
			{
				List<WijmoResourceInfo> resources = WijmoResourceManager.GetScriptReferenceForControl((IWijmoWidgetSupport)Component);
				foreach (WijmoResourceInfo resource in resources)
				{
					strScriptRefs.AppendLine(GetScriptReference(resource));
				}
			}
			return strScriptRefs.ToString();
		}

		private string GetScriptDescriptors()
		{
			string scriptDesc = "";
			List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)((IScriptControl)Component).GetScriptDescriptors();
			foreach (WidgetDescriptor desc in scriptDescr)
			{
				scriptDesc += this.GetScriptInternal(desc);
			}
			return scriptDesc;
		}

		private string GetScriptInternal(WidgetDescriptor desc)
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("$(\"#");
			builder.Append(((C1TargetControlBase)Component).ClientID);
			builder.Append("\").");
			builder.Append(desc.Type);
			builder.Append("(");
			builder.Append(desc.Options);
			builder.Append(");");
			return builder.ToString();
		}

		/// <summary>
		/// override ActionLists
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection lists = new DesignerActionListCollection();

				lists.AddRange(base.ActionLists);
				lists.Add(new C1GaugeActionList(this, this.Component));
				return lists;
			}
		}
	}

	internal class C1GaugeActionList : DesignerActionListBase
	{
		private C1GaugeDesigner _designer;
		public C1GaugeActionList(C1GaugeDesigner designer, IComponent component)
			:base(designer)
		{
			_designer = designer;
		}

		internal override bool DisplayThemeSupport()
		{
			return false;
		}

		protected internal override bool SupportBootstrap()
		{
			return false;
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection col = new DesignerActionItemCollection();
			AddBaseSortedActionItems(col);
			return col;
		}
	}
}
