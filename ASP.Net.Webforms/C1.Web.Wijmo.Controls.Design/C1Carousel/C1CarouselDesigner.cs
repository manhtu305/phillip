﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.Design.C1Carousel
{
	using C1.Web.Wijmo.Controls.C1Carousel;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using System.Web.UI.WebControls;

	/// <summary>
	///  Carousel Designer
	/// </summary>
	[SupportsPreviewControl(true)]
	public class C1CarouselDesigner : C1DataBoundControlDesigner
	{
		#region ** fields

		private C1Carousel _control;
		private DesignerActionListCollection _actionList;
		private TemplateGroupCollection _templateGroups;
		private static readonly string[] _templateNames;

		#endregion
		
		#region ** structor
		static C1CarouselDesigner()
		{
			_templateNames = new string[] { "ItemContent" };
		}
		#endregion

		#region ** override

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			_control = (C1Carousel)component;

			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			base.SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			string a = base.GetDesignTimeHtml();
			return a;
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actionList == null)
				{
					_actionList = new DesignerActionListCollection();
					_actionList.AddRange(base.ActionLists);
					_actionList.Add(new C1CarouselActionList(this));
				}
				return _actionList;
			}
		}

		/// <summary>
		/// TemplateGroups implementation.
		/// </summary>
		public override TemplateGroupCollection TemplateGroups
		{
			get
			{
				TemplateGroupCollection templateGroups = base.TemplateGroups;
				if (this._templateGroups == null)
				{
					this._templateGroups = new TemplateGroupCollection();
					TemplateGroup group = new TemplateGroup("CommonItemTemplates", ((WebControl)base.ViewControl).ControlStyle);
					TemplateDefinition templateDefinition = new TemplateDefinition(this, _templateNames[0], this._control, _templateNames[0], false);
					templateDefinition.SupportsDataBinding = true;
					group.AddTemplateDefinition(templateDefinition);
					this._templateGroups.Add(group);
				}
				templateGroups.AddRange(this._templateGroups);
				return templateGroups;
			}
		}

		public void OpenBuilder()
		{
			List<C1CarouselItem> originalData = new List<C1CarouselItem>();
			foreach (C1CarouselItem tp in _control.Items)
			{
				originalData.Add(tp);
			}

			C1CarouselDesignerForm editorForm = new C1CarouselDesignerForm(this._control);
			bool accept = ShowControlEditorForm(this._control, editorForm);

			if (accept)
				this.UpdateDesignTimeHtml();
			else
			{
				_control.Items.Clear();
				foreach (C1CarouselItem tp in originalData)
				{
					_control.Items.Add(tp);
				}
			}
		}

		public bool ShowControlEditorForm(object control, System.Windows.Forms.Form editorForm)
		{
			System.Windows.Forms.DialogResult dr;
			IServiceProvider serviceProvider = ((IComponent)control).Site;
			if (serviceProvider != null)
			{
				IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
				DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1Carousel.EditControl"));
				using (trans)
				{
					System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
					IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

					if (service != null)
					{
						ccs.OnComponentChanging(control, null);
						dr = service.ShowDialog(editorForm);
					}
					else
						dr = System.Windows.Forms.DialogResult.None;
					if (dr == System.Windows.Forms.DialogResult.OK)
					{
						ccs.OnComponentChanged(control, null, null, null);
						trans.Commit();
					}
					else
					{
						trans.Cancel();
					}
				}
			}
			else
				dr = editorForm.ShowDialog();

			editorForm.Dispose();
			editorForm = null;

			return dr == System.Windows.Forms.DialogResult.OK;
		}
		

		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}

		#endregion
	}

	internal class C1CarouselActionList : DesignerActionListBase
	{

		private DesignerActionItemCollection _items;
		private C1Carousel _carousel;

		public C1CarouselActionList(C1CarouselDesigner parent)
			: base(parent)
		{
			this._carousel = base.Component as C1Carousel;
		}

		public C1.Web.Wijmo.Controls.Orientation Orientation
		{
			get
			{
				return this._carousel.Orientation;
			}
			set
			{
				SetProperty("Orientation", value);
			}
		}

		public Unit Width
		{
			get 
			{ 
				return this._carousel.Width; 
			}
			set
			{
				SetProperty("Width", value);
			}
		}

		public Unit Height
		{
			get
			{
				return this._carousel.Height;
			}
			set
			{
				SetProperty("Height", value);
			}
		}

		public void OpenBuilder()
		{
			C1CarouselDesigner designer = this.Designer as C1CarouselDesigner;
			designer.OpenBuilder();
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (this._items == null)
			{
				this._items = new DesignerActionItemCollection();
				this._items.Add(new DesignerActionPropertyItem("Orientation",
					C1Localizer.GetString("C1Carousel.SmartTag.Orientation"),
					"Behavior",
					C1Localizer.GetString("C1Carousel.SmartTag.OrientationDescription")));
				this._items.Add(new DesignerActionPropertyItem("Width",
					C1Localizer.GetString("C1Carousel.SmartTag.Width"),
					"Behavior",
					C1Localizer.GetString("C1Carousel.SmartTag.WidthDescription")));
				this._items.Add(new DesignerActionPropertyItem("Height",
					C1Localizer.GetString("C1Carousel.SmartTag.Height"),
					"Behavior",
					C1Localizer.GetString("C1Carousel.SmartTag.HeightDescription")));
				this._items.Add(new DesignerActionMethodItem(this, "OpenBuilder",
					C1Localizer.GetString("C1Carousel.SmartTag.OpenBuilder"),//Add CarouselItem
					"",
					C1Localizer.GetString("C1Carousel.SmartTag.OpenBuilderDescription"),true));

				AddBaseSortedActionItems(this._items);
			}
			return this._items;
		}
	}
}
