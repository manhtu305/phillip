﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.IO;
using System.Web.Configuration;
using System.Configuration;

namespace C1.Web.Wijmo.Controls.Design.C1Upload
{
	using C1.Web.Wijmo.Controls.C1Upload;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using C1.Web.Wijmo.Controls.Design.Localization;

    public class C1UploadDesigner : C1ControlDesinger
	{

		/// <summary>
		/// Gets C1Upload control from designer.
		/// </summary>
		public C1Upload C1Upload
		{
			get
			{
				return this.Component as C1Upload;
			}
		}

		public override string GetDesignTimeHtml()
		{
			if (this.C1Upload.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			string a = base.GetDesignTimeHtml();
			return a;
		}

		protected override void RegisterHttpHandler()
		{
			base.RegisterHttpHandler();
			this.DesignerHelper.RegisterWijmoHttpHandler("C1UploadProgress", "C1UploadProgress.axd", typeof(UploadProgressHandler));
		}

		protected override void RegisterHttpModule()
		{
			base.RegisterHttpModule();
			this.DesignerHelper.RegisterWijmoHttpModule("C1UploadModule", typeof(UploadModule));
		}

		/// <summary>
		/// Gets the action list collection for the control designer.
		/// </summary>
		public override System.ComponentModel.Design.DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1UploadDesignerActionList(this));
				return actionLists; 
			}
		}

		/// <summary>
		/// Provides a custom class for types that define a list of items used to create a smart tag panel.
		/// </summary>
		private class C1UploadDesignerActionList : DesignerActionListBase
		{
			#region ** fields
			private C1UploadDesigner _parent;
			private DesignerActionItemCollection items;
			#endregion end of ** fields..

			#region ** constructor
			/// <summary>
			/// CustomControlActionList constructor.
			/// </summary>
			/// <param name="parent">The Specified C1UploadDesigner designer.</param>
			public C1UploadDesignerActionList(C1UploadDesigner parent)
				: base(parent)
			{
				_parent = parent;
			}
			#endregion end of ** constructor.

			/// <summary>
			/// Override GetSortedActionItems method.
			/// </summary>
			/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (items == null)
				{
					items = new DesignerActionItemCollection();
					AddBaseSortedActionItems(items);
				}
				return items;
			}
		}
	}
}
