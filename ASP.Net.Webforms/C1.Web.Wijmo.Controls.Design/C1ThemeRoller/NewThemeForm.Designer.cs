﻿using C1.Web.Wijmo.Controls.Design.Localization;
namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
    partial class NewThemeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.labelDescription = new System.Windows.Forms.Label();
            this.panelContent = new System.Windows.Forms.Panel();
            this.lvThemes = new System.Windows.Forms.ListView();
            this.themeImageList = new System.Windows.Forms.ImageList(this.components);
            this.panelBottom = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBottomNext = new System.Windows.Forms.Button();
            this.panelTitle.SuspendLayout();
            this.panelContent.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitle
            // 
            this.panelTitle.Controls.Add(this.btnImport);
            this.panelTitle.Controls.Add(this.labelDescription);
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(689, 50);
            this.panelTitle.TabIndex = 0;
            // 
            // btnImport
            // 
            this.btnImport.Enabled = false;
            this.btnImport.Location = new System.Drawing.Point(320, 12);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(120, 23);
            this.btnImport.TabIndex = 1;
			this.btnImport.Text = C1Localizer.GetString("ThemeRoller.UI.ImportTheme", "Import Theme");
			
            this.btnImport.UseVisualStyleBackColor = true;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelDescription.Location = new System.Drawing.Point(5, 15);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(315, 17);
            this.labelDescription.TabIndex = 0;
			this.labelDescription.Text = C1Localizer.GetString("ThemeRoller.UI.ChooseTheme", "Please select a theme from the following list OR  ");
            // 
            // panelContent
            // 
            this.panelContent.Controls.Add(this.lvThemes);
            this.panelContent.Location = new System.Drawing.Point(0, 50);
            this.panelContent.Name = "panelContent";
            this.panelContent.Size = new System.Drawing.Size(689, 430);
            this.panelContent.TabIndex = 1;
            // 
            // lvThemes
            // 
            this.lvThemes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvThemes.LargeImageList = this.themeImageList;
            this.lvThemes.Location = new System.Drawing.Point(0, 0);
            this.lvThemes.MultiSelect = false;
            this.lvThemes.Name = "lvThemes";
            this.lvThemes.Size = new System.Drawing.Size(689, 430);
            this.lvThemes.TabIndex = 0;
            this.lvThemes.UseCompatibleStateImageBehavior = false;
            // 
            // themeImageList
            // 
            this.themeImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.themeImageList.ImageSize = new System.Drawing.Size(93, 60);
            this.themeImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.btnCancel);
            this.panelBottom.Controls.Add(this.btnBottomNext);
            this.panelBottom.Location = new System.Drawing.Point(0, 480);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(689, 49);
            this.panelBottom.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(570, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = C1Localizer.GetString("Base.Cancel", "Cancel");
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnBottomNext
            // 
            this.btnBottomNext.Enabled = false;
            this.btnBottomNext.Location = new System.Drawing.Point(460, 13);
            this.btnBottomNext.Name = "btnBottomNext";
            this.btnBottomNext.Size = new System.Drawing.Size(75, 23);
            this.btnBottomNext.TabIndex = 0;
			this.btnBottomNext.Text = C1Localizer.GetString("Base.UI.Next", "Next");
            this.btnBottomNext.UseVisualStyleBackColor = true;
            // 
            // NewThemeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(689, 529);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelContent);
            this.Controls.Add(this.panelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewThemeForm";
			this.Text = C1Localizer.GetString("ThemeRoller.UI.NewThemeForm", "NewThemeForm");
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            this.panelContent.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Panel panelContent;
        private System.Windows.Forms.ListView lvThemes;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBottomNext;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.ImageList themeImageList;
    }
}