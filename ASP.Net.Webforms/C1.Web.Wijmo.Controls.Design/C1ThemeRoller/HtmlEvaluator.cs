﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using C1.Web.Wijmo.Controls.Design.Localization;


namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	internal class HtmlEvaluator
	{

		private const string FILE_JQUERY_JS = "jquery.js";
		private const string FILE_JQUERYUI_JS = "jquery-ui.js";
		private const string FILE_WIJMO_OPEN_JS = "wijmo_open.js";
		private const string FILE_WIJMO_PRO_JS = "wijmo_pro.js";
		private const string FILE_WIJMO_OPEN_CSS = "wijmo_open.css";
		private const string FILE_WIJMO_PRO_CSS = "wijmo_pro.css";

		private static readonly Assembly ThisAssembly = typeof(ThemeEditor).Assembly;
#if DEBUG
		private static readonly string[] RES_NAMES = ThisAssembly.GetManifestResourceNames();
#endif

		private string _jQueryUIVersion;
		private string _themeCss;
		private string _requestThemeParam;
		private IWijmoScriptsAccessor _wijmoScriptsAccessor;
		private DirectoryInfo _workingDir;
		private DirectoryInfo _customDir;
		private readonly string[] _callableMethodNames;

		public HtmlEvaluator(string jQueryUIVersion, string themeCss, string requestThemeParam, IWijmoScriptsAccessor wijmoScriptsAccessor, DirectoryInfo workingDir, DirectoryInfo customDir)
		{
			_jQueryUIVersion = jQueryUIVersion;
			_themeCss = themeCss;
			_requestThemeParam = requestThemeParam;

			_wijmoScriptsAccessor = wijmoScriptsAccessor;
			_workingDir = workingDir;
			_customDir = customDir;
            _callableMethodNames = new string[] {"getJQueryUICssUrl", "createInjectableCssElement"};
		}

		public string Evaluate(string htmlResxName)
		{
			string html = _GetResourceText(ThisAssembly, htmlResxName);

			// replace the {%...%} placeholders with the actual content.
			html = Regex.Replace(html, @"{%(\w{4}):(.+)%}", (Match match) =>
			{
				string scheme = match.Groups[1].Value;
				string value = match.Groups[2].Value;

				switch (scheme)
				{
					case "code":
						Match methodMatch = new Regex(@"^([^\(]+)\((.*)\)$").Match(value);
						if (!methodMatch.Success)
						{
							throw new InvalidOperationException("invalid %code% parameter");
						}

						string methodName = methodMatch.Groups[1].Value;
						List<string> argList = new List<string>();

						foreach (Match argMatch in new Regex("[^']*'([^']+)'").Matches(methodMatch.Groups[2].Value))
						{
							argList.Add(argMatch.Groups[1].Value);
						}

						return _CallMethod(methodName, argList.ToArray());

					case "resx":
						return _GetUriAndCreateFileFromResource(ThisAssembly, value);
					case "wija":
						return _GetUriAndCreateFileFromWijmoAccessor(value);
				}

				return string.Empty;
			});

			string path = Path.Combine(_workingDir.FullName, htmlResxName);
			File.WriteAllText(path, html);

			_CreateImages();

			return path;
		}

		private string _CallMethod(string name, string[] args)
		{
			MethodInfo methodInfo = this.GetType().GetMethod(name); // public only

			if (methodInfo == null)
			{
				throw new InvalidOperationException(string.Format("{0}: not found.", name));
			}

			bool isCallable = this.CheckMethodIsCallable(name);
			if (!isCallable)
			{
				throw new InvalidOperationException(string.Format("{0}: CallableAttribute missing.", name));
			}

			string result = (string)methodInfo.Invoke(this, args);

			return result;
		}


		private Stream _GetResourceStream(Assembly assembly, string name)
		{
			string resolvedName = _ResolveHtmlResxName(name);
#if DEBUG
			if (Array.IndexOf(RES_NAMES, resolvedName) < 0)
			{
				throw new InvalidOperationException(string.Format("Unknown resource name: \"{0}\"", name));
			}
#endif

			return assembly.GetManifestResourceStream(resolvedName);
		}

		private string _GetResourceText(Assembly assembly, string name)
		{
			string result;

			using (Stream stream = _GetResourceStream(assembly, name))
			{
				using (StreamReader streamReader = new StreamReader(stream))
				{
					result = streamReader.ReadToEnd();
				}
			}

			return result;
		}

		private string _GetUriAndCreateFileFromResource(Assembly assembly, string name)
		{
			string content = _GetResourceText(assembly, name);

			string path = Path.Combine(_workingDir.FullName, name);
			File.WriteAllText(path, content);

			Uri uri = new Uri(path);
			return uri.AbsoluteUri;
		}

		private string _GetUriAndCreateFileFromWijmoAccessor(string name)
		{
			string path = null;
			string content = null;

			switch (name)
			{
				case "wijmo_open_css":
					content = _wijmoScriptsAccessor.getWijmoOpenCSS();
					path = Path.Combine(_workingDir.FullName, FILE_WIJMO_OPEN_CSS);
					File.WriteAllText(path, content);
					break;

				case "wijmo_pro_css":
					content = _wijmoScriptsAccessor.getWijmoProCSS();
					path = Path.Combine(_workingDir.FullName, FILE_WIJMO_PRO_CSS);
					File.WriteAllText(path, content);
					break;

				case "wijmo_open_js":
					content = _wijmoScriptsAccessor.getWijmoOpenJS();
					path = Path.Combine(_workingDir.FullName, FILE_WIJMO_OPEN_JS);
					File.WriteAllText(path, content);
					break;

				case "wijmo_pro_js":
					content = _wijmoScriptsAccessor.getWijmoProJS();
					path = Path.Combine(_workingDir.FullName, FILE_WIJMO_PRO_JS);
					File.WriteAllText(path, content);
					break;

				case "jquery":
					content = _wijmoScriptsAccessor.getJQueryJS();
					path = Path.Combine(_workingDir.FullName, FILE_JQUERY_JS);
					File.WriteAllText(path, content);
					break;

				case "jquery_ui":
					content = _wijmoScriptsAccessor.getJQueryUIJS();
					path = Path.Combine(_workingDir.FullName, FILE_JQUERYUI_JS);
					File.WriteAllText(path, content);
					break;

				default:
					throw new ThemeRollerException(string.Format("Unknown name: \"{0}\"", name));

			}

			Uri uri = new Uri(path);
			return uri.AbsoluteUri;
		}

		private void _CreateBinFileFromResource(Assembly assembly, string filePath, string name)
		{
			using (Stream stream = _GetResourceStream(assembly, name))
			{
				using (FileStream fileStream = File.Create(filePath))
				{
					stream.CopyTo(fileStream);
				}
			}
		}

		private void _CreateImages()
		{
			DirectoryInfo imagesDir = _workingDir.CreateSubdirectory("images");

			// themeeditor
			_CreateBinFileFromResource(ThisAssembly, Path.Combine(imagesDir.FullName, "bg_tr_group_headers.png"), "images.bg_tr_group_headers.png");
			_CreateBinFileFromResource(ThisAssembly, Path.Combine(imagesDir.FullName, "bg_tr_group_headers_hover.png"), "images.bg_tr_group_headers_hover.png");
			_CreateBinFileFromResource(ThisAssembly, Path.Combine(imagesDir.FullName, "tr_icons_white.png"), "images.tr_icons_white.png");

			// colorpicker
			_CreateBinFileFromResource(ThisAssembly, Path.Combine(imagesDir.FullName, "marker.png"), "images.marker.png");
			_CreateBinFileFromResource(ThisAssembly, Path.Combine(imagesDir.FullName, "mask.png"), "images.mask.png");
			_CreateBinFileFromResource(ThisAssembly, Path.Combine(imagesDir.FullName, "wheel.png"), "images.wheel.png");
		}

		private string _ResolveHtmlResxName(string name)
		{
			return "C1.Web.Wijmo.Controls.Design.Resources.c1themeroller." + name;
		}

		#region callable

		public string getJQueryUICssUrl(string value) // value == http://code.jquery.com/ui/{0}/themes/smoothness/jquery-ui.css
		{
			return string.Format(value, _jQueryUIVersion);
		}

		public string createInjectableCssElement()
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendFormat("<style type=\"text/css\" id=\"{0}\">\n", Constants.CSS_ELEMENT_ID);
			sb.AppendLine(this._themeCss);
			sb.AppendLine(Utilites.GetCustomCSSContent(_customDir));
			sb.AppendLine("</style>");

			return sb.ToString();
		}

		public bool CheckMethodIsCallable(string fileName)
		{
			int count = _callableMethodNames.Length;
			for (int i = 0; i < count; i++)
			{
				if (string.Compare(_callableMethodNames[i], fileName, true) == 0)
					return true;
			}
			return false;
		}

		#endregion
	}
}
