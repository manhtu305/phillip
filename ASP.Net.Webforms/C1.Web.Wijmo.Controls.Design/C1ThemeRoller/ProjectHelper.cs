﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using EnvDTE;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	internal static class ProjectHelper
	{
		private static readonly Regex RG_SLASHES = new Regex(@"^\\*", RegexOptions.Compiled);

		public static string Path(this ProjectItem projectItem)
		{
			return projectItem.get_FileNames(1);
		}

		public static string Path(this Project project)
		{
			string path = project.FullName;

			if (!string.IsNullOrEmpty(path) && path.StartsWith("http:") && project.ProjectItems.Count > 0) // IIS
			{
				// TODO?
				ProjectItem webConfig = project.FindPath(@"web\.config$");
				if (webConfig == null)
				{
					throw new ThemeRollerException(C1Localizer.GetString("EMsgWebConfigMissing"));
				}
				path = webConfig.Path();
			}

			path = System.IO.Path.GetDirectoryName(path);

			return path;
		}

		public static Project GetActiveProject(EnvDTE80.DTE2 dte)
		{
			if (dte == null)
			{
				return null;
			}

			Array projects = dte.ActiveSolutionProjects as Array;

			if (projects != null && projects.Length > 0)
			{
				return projects.GetValue(0) as Project;
			}

			return null;
		}

		public static ProjectItem FindPath(this Project project, string regexPath, int maxDepth = -1)
		{
			return (project != null)
				? project.ProjectItems.FindPath(regexPath, maxDepth)
				: null;
		}

		public static ProjectItem FindPath(this ProjectItems projectItems, string regexPath, int maxDepth = -1)
		{
			if (projectItems != null)
			{
				// 1-based
				Regex rg = new Regex(regexPath, RegexOptions.IgnoreCase);

				for (int i = 1; i <= projectItems.Count; i++)
				{
					ProjectItem result = _FindPath(projectItems.Item(i), rg, maxDepth, 0);
					if (result != null)
					{
						return result;
					}
				}

			}

			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="project"></param>
		/// <param name="regexPath">Path to search.</param>
		/// <param name="maxDepth">Maximum depth of the search. -1 (default) = no limit, 0 = this item only, 1 = this item and the 1st level descendants etc.</param>
		/// <returns></returns>
		public static ProjectItem FindPath(this ProjectItem projectItem, string regexPath, int maxDepth = -1)
		{
			return _FindPath(projectItem, new Regex(regexPath, RegexOptions.IgnoreCase), maxDepth, 0);
		}

		private static ProjectItem _FindPath(ProjectItem projectItem, Regex regex, int maxDepth, int currentDepth)
		{
			if (projectItem == null || (maxDepth >= 0 && currentDepth > maxDepth))
			{
				return null;
			}


			string path = projectItem.Path();

			if (!string.IsNullOrEmpty(path) && regex.IsMatch(path))
			{
				return projectItem;
			}

			if (projectItem.ProjectItems != null)
			{
				// 1-based
				for (int i = 1; i <= projectItem.ProjectItems.Count; i++)
				{
					ProjectItem result = _FindPath(projectItem.ProjectItems.Item(i), regex, maxDepth, currentDepth + 1);
					if (result != null)
					{
						return result;

					}
				}
			}

			return null;
		}

		public static ProjectItem AddPath(this Project project, string path)
		{
			return (project != null)
				? project.ProjectItems.AddPath(path)
				: null;
		}

		public static ProjectItem AddPath(this ProjectItems projectItems, string path)
		{
			return (projectItems != null)
				? _AddPath(projectItems, path)
				: null;
		}

		public static ProjectItem AddPath(this ProjectItem projectItem, string path)
		{
			return (projectItem != null)
				? _AddPath(projectItem, path)
				: null;
		}

		private static ProjectItem _AddPath(object container, string path)
		{
			const string DELIM = "\\";
			ProjectItem result = null;

			if (path != null)
			{
				path = RG_SLASHES.Replace(path, string.Empty); // remove slashes from the beginning of the path
			}

			if (!string.IsNullOrEmpty(path))
			{
				int delimPos = path.IndexOf(DELIM);
				if (delimPos != 0)
				{
					string subPath = (delimPos < 0)
						? path.Substring(0)
						: path.Substring(0, delimPos);

					ProjectItem projectItem = container as ProjectItem;
					if (projectItem != null)
					{
						result = projectItem.FindPath(@"\\" + subPath + @"\\$", 0);
						if (result == null)
						{
							result = projectItem.ProjectItems.AddFolder(subPath);
						}
					}
					else
					{
						ProjectItems projectItems = container as ProjectItems;
						if (projectItems != null)
						{
							result = projectItems.FindPath(@"\\" + subPath + @"\\$", 0);
							if (result == null)
							{
								result = projectItems.AddFolder(subPath);
							}
						}
						else
						{
							Debug.Fail("DocHelper._AddPath: unsupported container.");
						}
					}
				}

				if (delimPos >= 0)
				{
					result = _AddPath(result.ProjectItems, path.Substring(delimPos + DELIM.Length));
				}
			}

			return result; // last added/ traversed projectItem
		}
	}
}
