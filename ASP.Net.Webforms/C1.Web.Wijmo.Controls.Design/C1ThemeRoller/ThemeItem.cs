﻿using System;
using System.IO;
using System.Reflection;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	public abstract class ThemeItem
	{
		private string _uri;

		public ThemeItem(string uri)
		{
			_uri = uri;
		}

		public string Uri
		{
			get { return _uri; }
		}

		public string HostName
		{
			get { return GetNameFromURI(Uri); }
		}

		public abstract Stream Read();

		public void SaveTo(string themeRootPath)
		{
			if (Uri.EndsWith(".css"))
			{
				SaveContentToFile(Path.Combine(themeRootPath, HostName));
			}
			else
			{
                SaveContentToFile(Utilites.Combine(themeRootPath, Constants.THEME_IMAGES_FOLDER, HostName));
			}

		}

		protected abstract string GetNameFromURI(string uri);

		protected void SaveContentToFile(string filePath)
		{
			Directory.CreateDirectory(Path.GetDirectoryName(filePath));

			using (Stream stream = Read())
			{
				using (FileStream fileStream = File.Create(filePath, 1024))
				{
					stream.Position = 0;
					stream.CopyTo(fileStream);
				}
			}
		}
	}

	internal class ThemeItemResx : ThemeItem
	{
		private Assembly _assembly;

		public ThemeItemResx(Assembly assembly, string uri)
			: base(uri)
		{
			_assembly = assembly;
		}

		public override Stream Read()
		{
			return _assembly.GetManifestResourceStream(Uri);
		}

		protected override string GetNameFromURI(string uri)
		{
			int idx = uri.LastIndexOf('.');
			idx = uri.LastIndexOf('.', idx - 1);

			return uri.Substring(idx + 1);
		}
	}

	internal class ThemeItemFile : ThemeItem
	{
		public ThemeItemFile(string filePath)
			: base(filePath)
		{
		}

		public override Stream Read()
		{
			return File.OpenRead(Uri);
		}

		protected override string GetNameFromURI(string uri)
		{
			return Path.GetFileName(uri);
		}
	}
}
