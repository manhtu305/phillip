﻿using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
    internal enum ThemeType
    {
        NotSet,
        Wijmo,
        JQueryUI,
        Custom,
    }

    internal class C1Theme
    {
        public C1Theme()
        {
            DisplayName = string.Empty;
            Kind = ThemeType.NotSet;
        }

        public string DisplayName
        {
            get;
            set;
        }

        public ThemeType Kind
        {
            get;
            set;
        }
    }

    internal static class ThemeProcesser
    {
        internal const string CUSTOM_THEMES_FOLDER = "~/Content/themes";
        internal const string RESX_THEME_ROOT = "C1.Web.Wijmo.Controls.Resources.themes";
        internal static ThemesAccessor EmbeddedThemes = Utilites.GetResxThemesAccesor(typeof(C1TargetControlBase).Assembly, "C1.Web.Wijmo.Controls.Resources.themes");
        internal static IWijmoScriptsAccessor WijmoScriptsAccessor = new WijmoScriptsAccessor();

        public static C1Theme GetThemeFromName(string themeName)
        {
            C1Theme themeResult = new C1Theme();
            string lowerCaseTheme = themeName.ToLower().Trim();
            if (!string.IsNullOrEmpty(lowerCaseTheme))
            {
                if (lowerCaseTheme.StartsWith("~/") ||
                lowerCaseTheme.StartsWith("http://") ||
                    lowerCaseTheme.StartsWith("https://") ||
                    lowerCaseTheme.EndsWith(".css"))
                {
                    string regText = "^" + CUSTOM_THEMES_FOLDER + @"/(?<themeName>.+?)/jquery-ui(.*)\.css$";
                    Regex reg = new Regex(regText, RegexOptions.IgnoreCase);
                    MatchCollection result = reg.Matches(lowerCaseTheme);
                    if (result.Count == 1)
                    {
                        themeResult.DisplayName = result[0].Groups["themeName"].Value;
                        themeResult.Kind = ThemeType.Custom;
                        return themeResult;
                    }
                }
                if (EmbeddedThemes.GetThemeMapperByDisplayName(lowerCaseTheme) != null)
                {
                    themeResult.DisplayName = lowerCaseTheme;
                    themeResult.Kind = ThemeType.Wijmo;
                    return themeResult;
                }
            }
            return themeResult;
        }

        public static string GetThemePath(string displayName, ThemeType themeType, System.Web.UI.Design.IProjectItem rootProjectItem)
        {
            switch (themeType)
            {
                case ThemeType.Wijmo:
                    return displayName;
                case ThemeType.JQueryUI:
                    return string.Empty;
                case ThemeType.Custom:
					ThemesAccessor customThemeAccessor = Utilites.GetActiveProjectCustomThemesAccessor(Process.GetCurrentProcess().Id, rootProjectItem);
                    ThemeMapper mapper = customThemeAccessor.GetThemeMapperByDisplayName(displayName);
                    return CUSTOM_THEMES_FOLDER + "/" + displayName + "/" + mapper.CSS.HostName;
                case ThemeType.NotSet:
                default:
                    return string.Empty;
            }
        }
    }

    internal class WijmoScriptsAccessor : IWijmoScriptsAccessor
    {
        public string getJQueryJS()
        {
            string jsFileName = WidgetDependenciesResolver.ResolveDependency(ResourcesConst.JQUERY, ".js");
            return GetResource(jsFileName);
        }

        public string getJQueryUIJS()
        {
            string jsFileName = WidgetDependenciesResolver.ResolveDependency(ResourcesConst.JQUERY_UI, ".js");
            return GetResource(jsFileName);
        }

        public string getWijmoOpenJS()
        {
            string jsFileName = WidgetDependenciesResolver.ResolveDependency(ResourcesConst.WIJMO_OPEN_JS, ".js");
            return GetResource(jsFileName);
        }

        public string getWijmoProJS()
        {
            string jsFileName = WidgetDependenciesResolver.ResolveDependency(ResourcesConst.WIJMO_PRO_JS, ".js");
            return GetResource(jsFileName);
        }

        public string getWijmoOpenCSS()
        {
            string cssFileName = WidgetDependenciesResolver.ResolveDependency(ResourcesConst.WIJMO_OPEN_CSS, ".css");
            return GetResource(cssFileName);
        }

        public string getWijmoProCSS()
        {
            string cssFileName = WidgetDependenciesResolver.ResolveDependency(ResourcesConst.WIJMO_PRO_CSS, ".css");
            return GetResource(cssFileName);
        }

        private static string GetResource(string name)
        {
            string result = null;

            using (Stream stream = typeof(C1TargetControlBase).Assembly.GetManifestResourceStream(name))
            {
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    result = streamReader.ReadToEnd();
                }
            }

            return result;
        }
    }
}
