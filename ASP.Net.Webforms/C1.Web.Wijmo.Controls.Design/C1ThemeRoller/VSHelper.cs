﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text.RegularExpressions;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	internal static class VsHelper
	{
		[DllImport("ole32.dll")]
		private static extern int CreateBindCtx(int reserved, out IBindCtx ppbc);

		[DllImport("ole32.dll")]
		private static extern int GetRunningObjectTable(int reserved, out IRunningObjectTable prot);

		public static EnvDTE80.DTE2 GetDTE(int pid)
		{
			Regex testDTEMoniker = new Regex(@"!VisualStudio\.DTE.+:" + pid, RegexOptions.IgnoreCase);

			IRunningObjectTable rot = null;
			IEnumMoniker enumMoniker = null;
			IntPtr fetched = IntPtr.Zero;
			IMoniker[] moniker = new IMoniker[1] { null };

			try
			{
				if (GetRunningObjectTable(0, out rot) == 0)
				{
					rot.EnumRunning(out enumMoniker);
					enumMoniker.Reset();

					while (enumMoniker.Next(1, moniker, fetched) == 0)
					{
						IBindCtx bindCtx = null;
						string displayName = null;
						object dte = null;

						if (CreateBindCtx(0, out bindCtx) == 0)
						{
							moniker[0].GetDisplayName(bindCtx, null, out displayName);

							Marshal.ReleaseComObject(bindCtx);

							if (testDTEMoniker.Match(displayName).Success)
							{
								rot.GetObject(moniker[0], out dte);
							}

							Marshal.ReleaseComObject(moniker[0]);
							moniker[0] = null;

							if (dte != null)
							{
								return (EnvDTE80.DTE2)dte;
							}
						}
					}
				}
			}
			finally
			{
				if (rot != null)
				{
					Marshal.ReleaseComObject(rot);
					rot = null;
				}

				if (enumMoniker != null)
				{
					Marshal.ReleaseComObject(enumMoniker);
					enumMoniker = null;
				}

				if (moniker[0] != null)
				{
					Marshal.ReleaseComObject(moniker[0]);
					moniker[0] = null;
				}
			}

			return null;
		}
	}
}
