﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	public abstract class ThemeMapper
	{
		protected static readonly Regex rgImageTest = new Regex(@"\.((jpg)|(png)|(gif)){1}$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		protected static readonly Regex rgImageCoreTest = new Regex("^((ui-bg_)|(ui-icons_)){1}", RegexOptions.Compiled | RegexOptions.IgnoreCase);


		private object _host;
		protected ThemeItem[] cssArray;
		protected ThemeItem[] imageArray;

		public ThemeMapper(object host)
		{
			_host = host;
		}

		public object Host
		{
			get { return _host; }
		}

		public abstract string HostName
		{
			get;
		}

		public abstract string DisplayName
		{
			get;
		}

		public ThemeItem CSS
		{
			get
			{
				_EnsureLists();

				return (cssArray.Length > 0)
					? cssArray[0]
					: null;
			}
		}

		public ThemeItem[] Images
		{
			get
			{
				_EnsureLists();
				return imageArray;
			}
		}

		public abstract string GetRequestParameter();


		public void SaveTo(string directory, Func<ThemeItem, bool> filter)
		{
			ThemeItem cssItem = CSS;
			if (cssItem != null)
			{
				if (filter == null || filter(cssItem))
				{
					cssItem.SaveTo(directory);
				}
			}

			foreach (ThemeItem item in Images)
			{
				if (filter == null || filter(item))
				{
					item.SaveTo(directory);
				}
			}
		}

		/// <summary>
		/// Saves css (custom part only) and images which are not started with "ui-"
		/// </summary>
		/// <param name="directory"></param>
		/// <param name="needUpdateUrlPath"></param>
        public void SaveCustomContentTo(string directory, bool needUpdateUrlPath)
        {
            SaveTo(directory, item =>
            {
                if (item.Uri.EndsWith(".css", StringComparison.OrdinalIgnoreCase))
                {
                    string customPart = GetCustomPart(item);
					if (customPart == null)
					{
						customPart = string.Empty;
					}
					if (needUpdateUrlPath)
					{
						customPart = Regex.Replace(customPart, "<%\\s*=\\s*WebResource\\(\"(?<resourceName>[^\"]*)\"\\)\\s*%>", new MatchEvaluator(match =>
						{
							string embeddedResource = match.Groups["resourceName"].Value; //C1.Web.Wijmo.Controls.Resources.themes.cobalt.images.ui-bg_glass_55_b3c4d8_500x100.png
							int startIndex = embeddedResource.IndexOf("images.", StringComparison.OrdinalIgnoreCase);							
							if (startIndex != -1)
							{
								int endIndex = embeddedResource.LastIndexOf(".");
								if (endIndex > startIndex + 6)
								{
									embeddedResource = embeddedResource.Substring(startIndex, endIndex-startIndex).Replace('.', '/')
										+ embeddedResource.Substring(endIndex);
								}
							}
							return embeddedResource;
						}));
					}
                    if (!string.IsNullOrEmpty(customPart))
                    {
                        using (StreamWriter streamWriter = File.CreateText(Path.Combine(directory, Constants.CSS_CUSTOM_FILENAME)))
                        {
                            streamWriter.Write(customPart);
                        }
                    }

                    return false;
                }
                else
                {
                    if (ThemeMapper.rgImageCoreTest.Match(item.HostName).Success)
                    {
                        return false; // skip core images (which are generated by the ThemeRoller service itself)
                    }
                }

                return true;
            });
        }

		protected abstract void EnsureLists();

        private string GetCustomPart(ThemeItem cssItem)
        {
            if (cssItem != null)
            {
                using (Stream stream = cssItem.Read())
                {
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        string content = streamReader.ReadToEnd();

                        int start = content.IndexOf(Constants.CSS_CUSTOMPART_MARKER);

                        if (start >= 0)
                        {

                            int end = content.IndexOf(Constants.CSS_CUSTOMPART_MARKER, start + Constants.CSS_CUSTOMPART_MARKER.Length);

                            if (end < 0)
                            {
                                // core
                                // <marker>
                                // custom

                                content = content.Substring(start + Constants.CSS_CUSTOMPART_MARKER.Length);
                            }
                            else
                            {
                                // core
                                // <marker>
                                // custom
                                // <marker>

                                content = content.Substring(start + Constants.CSS_CUSTOMPART_MARKER.Length, end - (start + Constants.CSS_CUSTOMPART_MARKER.Length));
                            }

                            return content.Trim(' ', '\n');
                        }
                    }
                }
            }

            return null;
        }

		private void _EnsureLists()
		{
			if (cssArray == null || imageArray == null)
			{
				EnsureLists();
			}
		}
	}

    /// <summary>
    /// jquery ui theme mapper
    /// </summary>
    internal sealed class JQueryUIThemeFakeMapper : ThemeMapper
    {
        private string _serializedThemeValue;

        public JQueryUIThemeFakeMapper(string themeName, string serializedThemeValue)
            : base(themeName)
        {
            _serializedThemeValue = serializedThemeValue;
        }

        public override string HostName
        {
            get { return (string)Host; }
        }

        public override string DisplayName
        {
            get { return HostName; }
        }

        public override string GetRequestParameter()
        {
            return _serializedThemeValue;
        }

        protected override void EnsureLists()
        {
            cssArray = new ThemeItem[0];
            imageArray = new ThemeItem[0];
        }
    }

	/// <summary>
	/// Both wijmo and jquery ui (not mobile).
	/// </summary>
	internal abstract class ThemeMapperDesktopBase : ThemeMapper
	{
		public ThemeMapperDesktopBase(object host): base(host)
		{
		}

		public override string GetRequestParameter()
		{
			ThemeItem css = CSS;

			if (css != null)
			{
				using (Stream stream = css.Read())
				{
					stream.Position = 0;
					using (StreamReader streamReader = new StreamReader(stream, System.Text.Encoding.UTF8))
					{
						string content = streamReader.ReadToEnd();

						int startIdx = content.IndexOf(Constants.CSS_PARAMETER_MARKER, StringComparison.OrdinalIgnoreCase);
						if (startIdx >= 0) {
							startIdx += Constants.CSS_PARAMETER_MARKER.Length;

							int endIndex = content.IndexOfAny(new char[] { ' ', '\n', '*' }, startIdx);
							if (endIndex > 0) {
								string param = content.Substring(startIdx, endIndex - startIdx);
								return param.Trim();
							}
						}
					}
				}
			}

			return null;
		}
	}
	
	internal sealed class ThemeMapperResx : ThemeMapperDesktopBase 
	{
		private string _resxThemeRoot;

		public ThemeMapperResx(Assembly assembly, string resxThemeRoot): base(assembly)
		{
			_resxThemeRoot = resxThemeRoot;
		}

		public override string DisplayName
		{
			get
			{
				return HostName.ResxNameToDisplayName();
			}
		}

		public override string HostName
		{
			get
			{
				int idx = _resxThemeRoot.LastIndexOf(".");
				return _resxThemeRoot.Substring(idx + 1);
			}
		}

		protected  override void EnsureLists() {
			List<ThemeItemResx> cssList = new List<ThemeItemResx>();
			List<ThemeItemResx> imgList = new List<ThemeItemResx>();

			foreach (string uri in ((Assembly)Host).GetManifestResourceNames())
			{
                if (uri.StartsWith(_resxThemeRoot + ".") && !uri.StartsWith(_resxThemeRoot + "_mobile"))
				{
					if (uri.EndsWith(".css", StringComparison.OrdinalIgnoreCase))
					{
						ThemeItemResx themeItem = new ThemeItemResx((Assembly)Host, uri);
						cssList.Add(themeItem);
					}


					if (ThemeMapper.rgImageTest.Match(uri).Success)
					{
						ThemeItemResx themeItem = new ThemeItemResx((Assembly)Host, uri);
						imgList.Add(themeItem);
					}
				}
			}

			cssArray = cssList.ToArray();
			imageArray = imgList.ToArray();
		}
	}

	internal sealed class ThemeMapperDirectory : ThemeMapperDesktopBase
	{
		public ThemeMapperDirectory(string path): base(path)
		{
		}

		public override string HostName
		{
			get
			{
				return Path.GetFileName((string)Host);
			}
		}

		public override string DisplayName
		{
			get
			{
				return HostName;
			}
		}

		protected override void EnsureLists()
		{
			List<ThemeItemFile> cssList = new List<ThemeItemFile>();
			List<ThemeItemFile> imgList = new List<ThemeItemFile>();

			foreach (string cssFilePath in Directory.GetFiles((string)Host))
			{
				if ((Path.GetExtension(cssFilePath) == ".css") && Utilites.TestCssFile(cssFilePath))
				{
					cssList.Add(new ThemeItemFile(cssFilePath));
				}
			}


			string imagesDir = Path.Combine((string)Host, Constants.THEME_IMAGES_FOLDER);
			if (Directory.Exists(imagesDir))
			{
                foreach (string imgFilePath in Directory.GetFiles(imagesDir))
				{
					if (ThemeMapper.rgImageTest.IsMatch(imgFilePath))
					{
						imgList.Add(new ThemeItemFile(imgFilePath));
					}
				}
			}

			cssArray = cssList.ToArray();
			imageArray= imgList.ToArray();
		}
	}
}
