﻿using System.IO;
using System.Reflection;
using System.Windows.Forms;
using EnvDTE;
using C1.Web.Wijmo.Controls.Design.Localization;
using System.Text;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	internal static class Constants
	{
        internal const string CSS_JQUERY_UI = "jquery-ui.css";

		internal const string CSS_CUSTOMPART_MARKER = "/*@wijmo*/";
		internal const string CSS_CUSTOM_FILENAME = "wijmo_custom_part.css";
		internal const string CSS_PARAMETER_MARKER = "http://jqueryui.com/themeroller/?";

		internal const string CUSTOM_THEMES_FOLDER = "Content\\themes";
		internal const string THEME_IMAGES_FOLDER = "images";

		internal const string THEME_POSTFIX = "-custom";

		/* html */
		internal const string CSS_ELEMENT_ID = "themeRollerCss";

        /*net exception*/
        internal const string PROXY_AUTHENTICATION_REQUIRED = "Proxy Authentication Required";
		internal const string INTERNAL_SERVER_ERROR = "Internal Server Error";
	}

	public static class Utilites
	{
		public static readonly ThemesAccessor JQueryUIThemes = new ThemesAccessorJQueryUIPredefened();

		internal static IProjectItem GetRootProjectItem(object component)
		{
			System.Web.UI.Control control = component as System.Web.UI.Control;
			if(control != null)
			{
				IWebApplication webApp = (IWebApplication)control.Site.GetService(typeof(IWebApplication));
				if(webApp != null)
				{
					return webApp.RootProjectItem;
				}
			}
			return null;
		}

		public static ThemesAccessor GetActiveProjectCustomThemesAccessor(int pid, IProjectItem rootProjectItem)
		{
			EnvDTE80.DTE2 _dte = VsHelper.GetDTE(pid);

			if (_dte == null && rootProjectItem == null)
			{
				throw new ThemeRollerException(string.Format(C1Localizer.GetString("EMsgNoDTE"), pid));
			}

			string projectPath = GetProjectPath(ProjectHelper.GetActiveProject(_dte), rootProjectItem);

			if (!string.IsNullOrEmpty(projectPath))
			{
				string folderToScan = Path.Combine(projectPath, Constants.CUSTOM_THEMES_FOLDER);
				return new ThemesAccessorDirectory(folderToScan);
			}

			return null;	
		}

		public static ThemesAccessor GetResxThemesAccesor(Assembly assembly, string themesRootUri)
		{
			return new ThemesAccessorResx(assembly, themesRootUri);
		}

		public static bool TestThemePath(string path)
		{
			if (Directory.Exists(path))
			{
				string[] css = Directory.GetFiles(path, "*.css", SearchOption.TopDirectoryOnly);

				if (css.Length > 0)
				{
					foreach (string cssFilePath in css)
					{
						return TestCssFile(cssFilePath);
					}
				}
			}

			return false;
		}

		internal static bool TestCssFile(string path)
		{
			string content = File.ReadAllText(path);
			return content.IndexOf(Constants.CSS_PARAMETER_MARKER) >= 0;
		}

		internal static string ToThemeFolderName(this string value)
		{
			return (value != null)
				? value.ToLower().Replace(' ', '-')
				: value;
		}

		internal static string ResxNameToDisplayName(this string value)
		{
			return (value != null)
				? value.Replace('_', '-')
				: value;
		}

		internal static string DisplayNameToResxName(this string value)
		{
			return (value != null)
				? value.Replace('-', ' ')
				: value;
		}

		internal static void CopyTo(this Stream input, Stream output)
		{
			input.CopyTo(output, null);
		}

		internal static void CopyTo(this Stream input, Stream output, int? bufferSize)
		{
			if (!bufferSize.HasValue)
				bufferSize = 0x14000;
			byte[] buffer = new byte[bufferSize.Value]; // Fairly arbitrary size
			int bytesRead;

			while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write(buffer, 0, bytesRead);
			}
		}

		internal static string Combine(string path1, string path2, string path3)
		{
			string rPath = Path.Combine(path1, path2);
			return Path.Combine(rPath, path3);
		}

		internal static void ShowErrorMessage(string message)
		{
			MessageBox.Show(message, "C1ThemeRoller", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		internal static string GetCustomCSSContent(DirectoryInfo customDir)
		{
			if (customDir == null)
				return string.Empty;
			StringBuilder sb = new StringBuilder();
			FileInfo[] customCss = customDir.GetFiles(Constants.CSS_CUSTOM_FILENAME);
			foreach (FileInfo customcssFile in customCss)
			{
				string customContent = File.ReadAllText(customcssFile.FullName);

				if (!string.IsNullOrEmpty(customContent))
				{
					sb.AppendLine(customContent);
				}
			}
			return sb.ToString();
		}

		internal static DirectoryInfo AddFolder(string parentPath, string folderName, bool needDeleted)
		{
			string folderPath = Path.Combine(parentPath, folderName);
			DirectoryInfo result = null;
			if (needDeleted)
			{
				if (Directory.Exists(folderPath))
				{
					Directory.Delete(folderPath, true);
				}
			}
			result = Directory.CreateDirectory(folderPath);

			return result;
		}

		internal static void Copy(string sourceDirPath, string desDirPath)
		{
			if (string.IsNullOrEmpty(sourceDirPath) || string.IsNullOrEmpty(desDirPath)
				|| !Directory.Exists(sourceDirPath))
			{
				return;
			}
			DirectoryInfo desDir = Directory.CreateDirectory(desDirPath);
			foreach (string filePath in Directory.GetFiles(sourceDirPath))
			{
				File.Copy(filePath, Path.Combine(desDirPath, Path.GetFileName(filePath)), true);
			}
			foreach (string dirPath in Directory.GetDirectories(sourceDirPath))
			{
				Copy(dirPath, Path.Combine(desDirPath, Path.GetFileName(dirPath)));
			}
		}

		internal static string GetProjectPath(Project project, IProjectItem rootProjectItem)
		{
			if (project != null)
			{
				return project.Path();
			}
			else if (rootProjectItem != null)
			{
				return Path.GetDirectoryName(rootProjectItem.PhysicalPath);
			}

			return string.Empty;
		}
	}

}
