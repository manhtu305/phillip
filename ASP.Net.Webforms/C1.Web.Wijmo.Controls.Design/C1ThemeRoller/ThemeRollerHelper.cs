﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	[Flags]
	internal enum JQueryUISpecficCSS
	{
		None = 0,

		Widget = 1,
		Resizable = 2,
		Selectable = 4,
		Accordion = 8,
		Autocomplete = 16,
		Button = 32,
		Datepicker = 64,
		Dialog = 128,
		Menu = 256,
		Progressbar = 512,
		Slider = 1024,
		Spinner = 2048,
		Tabs = 4096,
		Tooltip = 8192,

		All = Widget | Resizable | Selectable | Accordion | Autocomplete | Button | Datepicker | Dialog | Menu | Progressbar | Slider | Spinner | Tabs | Tooltip
	}

	internal static class ThemeRollerHelper
	{
		private static readonly Encoding DEF_ENCODING = System.Text.Encoding.UTF8;

		public const string URL_ParseThemeSvc = "http://download.jqueryui.com/themeroller/parsetheme.css";
		public const string URL_RuntimeImagesRoot = "http://download.jqueryui.com";

		public const string URL_GetZippedThemeSvc = "https://download.jqueryui.com/download";
		public const string URL_GetJQueryVersionSvc = "http://download.jqueryui.com/download";
		public const string URL_GetThemesListSvc = "http://download.jqueryui.com/download/theme";


		public static void ProcessJQueryUIVersion(CancellationTokenSource cToken, Action<string> processjQueryVersion, Action<Exception> processException)
		{
			using (WebClient webClient = new WebClient())
			{
				cToken.Register(() => webClient.CancelAsync());
				webClient.Encoding = DEF_ENCODING;
				DownloadStringCompletedEventHandler downloadStringCompleted = null;
				downloadStringCompleted = (sender, e) =>
					{
						try
						{
							webClient.DownloadStringCompleted -= downloadStringCompleted;
							string version = null;
							Match match = (new Regex(@"(<input.+name=""version"".+checked=""checked"".*>|<input.+checked=""checked"".+name=""version"".*>)")).Match(e.Result);
							if (match.Success)
							{
								match = (new Regex(@"value=""([^""]+).+")).Match(match.Groups[1].Value);
								if (match.Success)
								{
									version = match.Groups[1].Value;
								}
							}
							processjQueryVersion(version);
						}
						catch (Exception ex)
						{
							processException(ex);
						}
					};
				webClient.DownloadStringCompleted += downloadStringCompleted;
				webClient.DownloadStringAsync(new Uri(URL_GetJQueryVersionSvc));
			}
		}

		public static void ProcessZippedTheme(string jQueryVersion, string themeFolderName, string scope,
			string serializedTheme, JQueryUISpecficCSS specCSS,
			CancellationTokenSource cToken, Action<FileStream> processZippedTheme, Action<Exception> processException)
		{
			const int BUFFER_SIZE = 4096;

			Debug.Assert(!string.IsNullOrEmpty(jQueryVersion), "jQueryVersion is empty");
			Debug.Assert(!string.IsNullOrEmpty(serializedTheme), "serializedThemeValues is empty");
			Debug.Assert(!string.IsNullOrEmpty(themeFolderName), "themeFolderName is empty");

			using (WebClient webClient = new WebClient())
			{
				cToken.Register(() => webClient.CancelAsync());
				UploadValuesCompletedEventHandler uploadValuesCompleted = null;

				uploadValuesCompleted = (sender, e) =>
				{
					webClient.UploadValuesCompleted -= uploadValuesCompleted;
					FileStream fileStream = null;

					try
					{
						string fileName = Path.GetTempFileName();

						using (MemoryStream memoryStream = new MemoryStream(e.Result))
						{
							fileStream = File.Create(fileName, BUFFER_SIZE, FileOptions.DeleteOnClose);
							memoryStream.CopyTo(fileStream, BUFFER_SIZE);
							fileStream.Position = 0;
						}
					}
					catch (Exception ex)
					{
						if (fileStream != null)
						{
							fileStream.Close();
							fileStream = null;
						}

						processException(ex);
						return;
					}

					processZippedTheme(fileStream);
				};

				webClient.UploadValuesCompleted += uploadValuesCompleted;

				NameValueCollection requestParams = new NameValueCollection {
					{ "version", jQueryVersion },
					{ "theme", serializedTheme },
					{ "theme-folder-name", themeFolderName},
					{ "scope", scope}
				};

				foreach (JQueryUISpecficCSS value in Enum.GetValues(typeof(JQueryUISpecficCSS)))
				{
					if ((value != JQueryUISpecficCSS.None) && (value != JQueryUISpecficCSS.All) && ((value & specCSS) != 0))
					{
						var name = value.ToString().ToLowerInvariant();
						requestParams.Add((name != "widget" ? "widgets/" : string.Empty) + name, "on");
					}
				}

				webClient.UploadValuesAsync(new Uri(URL_GetZippedThemeSvc), requestParams);
			}
		}

		public static void ProcessCSSTheme(string themeValue, CancellationTokenSource cToken, Action<string> processCSSTheme, Action<Exception> processException)
		{
			using (WebClient webClient = new WebClient())
			{
				cToken.Register(() => webClient.CancelAsync());
				OpenReadCompletedEventHandler openReadCompleted = null;

				openReadCompleted = (sender, e) =>
				{
					try
					{
						webClient.OpenReadCompleted -= openReadCompleted;
						string cssContent = (new StreamReader(e.Result)).ReadToEnd();

						// chanage all relative urls to absoulute.
						cssContent = Regex.Replace(cssContent, @"url\((.+)\)", new MatchEvaluator(match =>
						{
							string value = match.Groups[0].Value; // url(/a/b/c.d)
							string relUrl = match.Groups[1].Value;  // /a/b/c.d

                            relUrl = relUrl.Trim(new char[] { '"', '\'' });

							return value.Replace(relUrl, URL_RuntimeImagesRoot + relUrl);
						}));
						processCSSTheme(cssContent);
					}
					catch (Exception ex)
					{
						processException(ex);
					}
				};

				webClient.OpenReadCompleted += openReadCompleted;
				webClient.OpenReadAsync(new Uri(string.Format("{0}?{1}", URL_ParseThemeSvc, themeValue)));
			}
		}

		public static void InjectCSS(this HtmlDocument document, string styleID, string cssText)
		{
			Debug.Assert(!string.IsNullOrEmpty(styleID), "InjectCSS: linkID is undefined.");
			Debug.Assert(document != null, "InjectCSS: document is null.");

			mshtml.IHTMLDocument3 doc = document.DomDocument as mshtml.IHTMLDocument3;
			mshtml.IHTMLDOMNode styleElement = doc.getElementById(styleID) as mshtml.IHTMLDOMNode;

			if (styleElement != null)
			{
				styleElement.parentNode.removeChild(styleElement);
			}

			mshtml.IHTMLDOMNode header = doc.getElementsByTagName("head").item(0) as mshtml.IHTMLDOMNode;

			mshtml.IHTMLElement newElement = ((mshtml.IHTMLDocument2)doc).createElement("style");
			newElement.setAttribute("id", styleID);
			newElement.setAttribute("type", "text/css");

			((mshtml.IHTMLStyleElement)newElement).styleSheet.cssText = cssText;

			header.appendChild((mshtml.IHTMLDOMNode)newElement);
		}

		public static void DocumentTextTaskAsync(WebBrowser webBrowser, string htmlUri, Action continueOperation)
		{
			WebBrowserDocumentCompletedEventHandler docCompletedEventHander = null;

			docCompletedEventHander = (sender, e) =>
			{
				webBrowser.DocumentCompleted -= docCompletedEventHander;
				continueOperation();
			};

			webBrowser.DocumentCompleted += docCompletedEventHander;

			webBrowser.Navigate(htmlUri);
			//webBrowser.DocumentText = content;
		}
	}
}
