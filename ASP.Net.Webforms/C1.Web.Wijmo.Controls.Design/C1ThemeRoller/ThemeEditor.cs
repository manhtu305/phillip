﻿using C1.Web.Wijmo.Controls.Design.Localization;
using EnvDTE;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	public interface IWijmoScriptsAccessor
	{
		string getJQueryJS();
		string getJQueryUIJS();
		string getWijmoOpenJS();
		string getWijmoProJS();
		string getWijmoOpenCSS();
		string getWijmoProCSS();
	}

	public class ThemeEditor : IDisposable
	{
		private EnvDTE80.DTE2 _dte = null;
		private bool _disposed = false;
		private ThemesAccessor _embeddedThemesAccessor;
		private IWijmoScriptsAccessor _wijmoScriptsAccessor;
		private DirectoryInfo _workingDir;
		private readonly DesignerActionListBase _actionList;
		private IProjectItem _rootProjectItem;

		public ThemeEditor(DesignerActionListBase actionList, int pid, ThemesAccessor embbededThemesAccessor, IWijmoScriptsAccessor wijmoScriptsAccessor)
		{
			_actionList = actionList;
			_dte = VsHelper.GetDTE(pid);

			if (_dte == null)
			{
				if (_actionList != null)
				{
					_rootProjectItem = Utilites.GetRootProjectItem(_actionList.Component);
				}
				else
				{
					throw new ThemeRollerException(string.Format(C1Localizer.GetString("EMsgNoDTE"), pid));
				}
			}

			_embeddedThemesAccessor = embbededThemesAccessor;
			_wijmoScriptsAccessor = wijmoScriptsAccessor;

			_workingDir = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()));
		}

		~ThemeEditor()
		{
			Dispose(false);
		}

		internal string[] EditWijmoTheme(Assembly assembly, string resxThemeRoot, C1Theme wijmoTheme)
		{
			Project project = ProjectHelper.GetActiveProject(_dte);


			if (project != null || _rootProjectItem != null)
			{
				DirectoryInfo customDir = null;

				try
				{
					string displayThemeName = wijmoTheme.DisplayName;
					customDir = _workingDir.CreateSubdirectory(Path.GetRandomFileName());

					//ThemeMapper themeMapper = new ThemeMapperResx(assembly, resxThemeRoot + "." + displayThemeName);
					ThemeMapper themeMapper = _embeddedThemesAccessor.GetThemeMapperByDisplayName(displayThemeName);

					if (themeMapper == null)
					{
						throw new ThemeRollerException(string.Format("Invalid theme name: \"{0}\".", displayThemeName));
					}

					themeMapper.SaveCustomContentTo(customDir.FullName, true);

					using (ThemeEditorForm form = new ThemeEditorForm(
						_actionList,
						project,
						themeMapper.GetRequestParameter(),
						displayThemeName.ToThemeFolderName() + Constants.THEME_POSTFIX,
						_embeddedThemesAccessor,
						_wijmoScriptsAccessor,
						_workingDir,
						customDir,
						_rootProjectItem))
					{
						List<string> addedThemes = new List<string>();

						form.ShowEditor(addedThemes);

						return addedThemes.ToArray();
					}
				}
				finally
				{
					if (customDir != null)
					{
						try
						{
							customDir.Delete(true);
						}
						catch
						{
						}
					}
				}
			}

			return new string[0];
		}

		internal string[] EditJQueryUITheme(C1Theme jqueryuiTheme)
		{
			Project project = ProjectHelper.GetActiveProject(_dte);

			if (project != null || _rootProjectItem != null)
			{
				try
				{
					string themeName = jqueryuiTheme.DisplayName;
					ThemeMapper jquiTheme = Utilites.JQueryUIThemes.GetThemeMapperByHostName(themeName);

					if (jquiTheme == null)
					{
						throw new ThemeRollerException(string.Format("Invalid theme name \"{0}\".", themeName));
					}
					using (ThemeEditorForm form = new ThemeEditorForm(
						_actionList,
						project,
						jquiTheme.GetRequestParameter(),
						themeName.ToThemeFolderName() + Constants.THEME_POSTFIX,
						_embeddedThemesAccessor,
						_wijmoScriptsAccessor,
						_workingDir,
						null,
						_rootProjectItem))
					{
						List<string> addedThemes = new List<string>();

						form.ShowEditor(addedThemes);

						return addedThemes.ToArray();
					}
				}
				finally
				{
				}
			}

			return new string[0];
		}

		internal string[] EditCustomTheme(string path, bool overrided)
		{
			Project project = ProjectHelper.GetActiveProject(_dte);

			if (project != null || _rootProjectItem != null)
			{
				DirectoryInfo customDir = null;

				try
				{
					string themeFolderName = Path.GetFileName(path).ToThemeFolderName();

					customDir = _workingDir.CreateSubdirectory(Path.GetRandomFileName());

					ThemeMapper themeMapper = new ThemeMapperDirectory(path);
					themeMapper.SaveCustomContentTo(customDir.FullName, false);

					using (ThemeEditorForm form = new ThemeEditorForm(
						_actionList,
						project,
						themeMapper.GetRequestParameter(),
						overrided ? themeFolderName : themeFolderName + Constants.THEME_POSTFIX,
						_embeddedThemesAccessor,
						_wijmoScriptsAccessor,
						_workingDir,
						customDir,
						_rootProjectItem))
					{
						List<string> addedThemes = new List<string>();

						form.ShowEditor(addedThemes);

						return addedThemes.ToArray();
					}
				}
				finally
				{
					if (customDir != null)
					{
						try
						{
							customDir.Delete(true);
						}
						catch
						{
						}
					}
				}
			}

			return new string[0];
		}

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				try
				{
					if (_dte != null)
					{
						Marshal.ReleaseComObject(_dte);
						_dte = null;
					}

					if (_workingDir != null)
					{
						_workingDir.Delete(true);
						_workingDir = null;
					}
				}
				catch
				{
				}

				_disposed = true;
			}
		}

		#endregion
	}


	public class ThemeRollerException : Exception
	{
		public ThemeRollerException(string message)
			: base(message)
		{
		}
	}
}
