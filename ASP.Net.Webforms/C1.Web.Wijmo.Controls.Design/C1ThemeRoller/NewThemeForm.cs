﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.Windows.Forms;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
    internal partial class NewThemeForm : Form
    {
        private readonly C1Theme _currentTheme;
        private readonly IWijmoScriptsAccessor _accessor;
        private readonly DesignerActionListBase _designActionList;

        public NewThemeForm()
        {
            InitializeComponent();
        }

        public NewThemeForm(DesignerActionListBase designActionList, C1Theme currentTheme, IWijmoScriptsAccessor accessor)
            : this()
        {
            _designActionList = designActionList;
            _currentTheme = currentTheme;
            _accessor = accessor;
            CustomizeComponent();
        }

        private void CustomizeComponent()
        {
            //Init listview
            if (!string.IsNullOrEmpty(_currentTheme.DisplayName))
            {
				AppendItemsToListView(C1Localizer.GetString("ThemeRoller.UI.YourTheme", "Your Theme"), new string[] { _currentTheme.DisplayName }, _currentTheme.Kind, this.lvThemes, this.themeImageList);
            }
            FillListView(this.lvThemes, this.themeImageList, true, Utilites.GetRootProjectItem(_designActionList.Component));
            this.lvThemes.Items[0].Selected = true;
            this.btnBottomNext.Enabled = true;
            this.btnImport.Enabled = true;
            this.btnBottomNext.Click += btnNext_Click;
            this.btnImport.Click += btnImport_Click;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
				dialog.Description = C1Localizer.GetString("ThemeRoller.UI.ImportDialogDescription", "Open a folder which contains custom theme");
                dialog.ShowNewFolderButton = false;

                if (dialog.ShowDialog() == DialogResult.OK && (Utilites.TestThemePath(dialog.SelectedPath)))
                {
                    try
                    {
                        this.Close();
                        using (ThemeEditor te = new ThemeEditor(_designActionList, System.Diagnostics.Process.GetCurrentProcess().Id, ThemeProcesser.EmbeddedThemes, _accessor))
                        {
                            te.EditCustomTheme(dialog.SelectedPath, true);
                        }
                    }
                    catch (Exception)
                    {}
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (this.lvThemes.SelectedItems.Count != 1)
                return;
            ListViewItem selectedItem = this.lvThemes.SelectedItems[0];
            ThemeType themeType = (ThemeType)selectedItem.Group.Tag;
            if (themeType == ThemeType.NotSet)
                return;
            C1Theme selectedTheme = new C1Theme();
            selectedTheme.Kind = themeType;
            selectedTheme.DisplayName = selectedItem.Text;
            string newThemeName = string.Empty;
            this.Close();
            CreateTheme(selectedTheme, _accessor, out newThemeName);
        }

        private static void AppendItemsToListView(string groupHeaderText, string[] themeDisplayNames, ThemeType themeType,
            ListView listView, ImageList imageList)
        {
            int themeCount = themeDisplayNames.Length;
            if (themeCount == 0)
                return;
            ListViewGroup group = new ListViewGroup(groupHeaderText, HorizontalAlignment.Left);
            group.Tag = themeType;
            listView.Groups.Add(group);
            for (int i = 0; i < themeCount; i++)
            {
                ListViewItem item = new ListViewItem(themeDisplayNames[i], group);
                Image image = null;
                if (themeType == ThemeType.Wijmo)
                {
                    image = Properties.Resources.GetThemeVisualImage(themeDisplayNames[i].DisplayNameToResxName());
                }
                else if (themeType == ThemeType.JQueryUI)
                {
                    image = Properties.Resources.GetThemeVisualImage(themeDisplayNames[i]);
                }
                else if (themeType == ThemeType.Custom)
                {
                    image = Properties.Resources.GetThemeVisualImage("Custom");
                }

                if (image != null)
                {
                    imageList.Images.Add(themeDisplayNames[i], image);
                    item.ImageKey = themeDisplayNames[i];
                }

                listView.Items.Add(item);
            }
        }

        internal static void FillListView(ListView listView, ImageList imageList, bool all, IProjectItem rootProjectItem)
        {
            imageList.ColorDepth = ColorDepth.Depth24Bit;
            imageList.ImageSize = new Size(93, 60);
            listView.MultiSelect = false;
            listView.HotTracking = true;
			AppendItemsToListView(C1Localizer.GetString("ThemeRoller.UI.WijmoThemes", "Wijmo Themes"), ThemeProcesser.EmbeddedThemes.GetDisplayNames(), ThemeType.Wijmo, listView, imageList);
            if (all)
            {
				AppendItemsToListView(C1Localizer.GetString("ThemeRoller.UI.jQueryUIThemes", "jQuery UI Themes"), Utilites.JQueryUIThemes.GetDisplayNames(), ThemeType.JQueryUI, listView, imageList);
            }
			AppendItemsToListView(C1Localizer.GetString("ThemeRoller.UI.CustomizedThemes", "Customized Themes"), Utilites.GetActiveProjectCustomThemesAccessor(System.Diagnostics.Process.GetCurrentProcess().Id, rootProjectItem).GetHostNames(), ThemeType.Custom, listView, imageList);
        }

        private bool CreateTheme(C1Theme theme, IWijmoScriptsAccessor scriptsAccessor, out string themeName)
        {
            themeName = string.Empty;
            try
            {
                int pid = Process.GetCurrentProcess().Id;
                using (ThemeEditor te = new ThemeEditor(_designActionList, pid, ThemeProcesser.EmbeddedThemes, scriptsAccessor))
                {
                    string[] addedThemes;

                    switch (theme.Kind)
                    {
                        case ThemeType.JQueryUI:
                            addedThemes = te.EditJQueryUITheme(theme);
                            break;
                        case ThemeType.Wijmo:
                            addedThemes = te.EditWijmoTheme(typeof(C1TargetControlBase).Assembly, ThemeProcesser.RESX_THEME_ROOT, theme);
                            break;
                        case ThemeType.Custom:
							ThemeMapper customTheme = Utilites.GetActiveProjectCustomThemesAccessor(pid, Utilites.GetRootProjectItem(_designActionList.Component)).GetThemeMapperByHostName(theme.DisplayName);
                            if (customTheme == null)
                            {
                                return false;
                            }
                            addedThemes = te.EditCustomTheme((string)customTheme.Host, false);
                            break;
                        default:
                            return false;
                    }

                    if (addedThemes.Length >= 1)
                    {
                        themeName = addedThemes[addedThemes.Length - 1];
                        return true;
                    }
                }
            }
            catch (Exception)
            { }
            return false;
        }
    }
}
