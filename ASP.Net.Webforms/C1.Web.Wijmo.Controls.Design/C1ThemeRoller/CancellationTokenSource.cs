﻿using System;

namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
    internal class CancellationTokenSource : IDisposable
    {
        public delegate void CancellationEventHandler();
        public event CancellationEventHandler CancellationEvent;
        private bool _isDisposed = false;

        public void Register(Action action)
        {
            CancellationEvent += delegate { action(); };
        }

        public void UnRegister(Action action)
        {
            CancellationEvent -= delegate { action(); };
        }

        public void Cancel()
        {
            if (this.CancellationEvent != null)
                this.CancellationEvent();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;
            CancellationEvent = null;
        }
    }
}
