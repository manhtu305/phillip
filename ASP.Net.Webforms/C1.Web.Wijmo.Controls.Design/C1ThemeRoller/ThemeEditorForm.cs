﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Threading;
using EnvDTE;

using Debug = System.Diagnostics.Debug;
using C1.Web.Wijmo.Controls.Design.Localization;
using System.Web.UI.Design;


namespace C1.Web.Wijmo.Controls.Design.C1ThemeRoller
{
	internal sealed partial class ThemeEditorForm : Form
	{
		private string _jQueryUIVersion = "";
		private Project _project = null;
		private IProjectItem _rootProjectItem = null;
		private string _requestThemeValue;
		private string _folderName;
		private ThemesAccessor _embeddedThemesAccessor;
		private IWijmoScriptsAccessor _wijmoScriptsAccessor;
		private DirectoryInfo _workingDir;
		private DirectoryInfo _customDir;
		private bool _closing = false;
		private ScriptingMediator _scriptingMediator;
		private IList<string> _addedThemes = null;
		private CancellationTokenSource _cancellationTokenSource = null;
		private readonly DesignerActionListBase _designActionList;
		private string _projectPath = string.Empty;

		public ThemeEditorForm(DesignerActionListBase designActionList, Project project, string requestThemeValue,
			string folderName, ThemesAccessor embeddedThemesAccessor, IWijmoScriptsAccessor wijmoScriptsAccessor,
			DirectoryInfo workingDir, DirectoryInfo customDir, IProjectItem rootProjectItem)
		{
			if (designActionList == null)
			{
				throw new ThemeRollerException("\"designActionList\" is null.");
			}

			if (project == null && rootProjectItem == null)
			{
				throw new ThemeRollerException("\"project\" or \"rootProjectItem\" is null.");
			}

			if (string.IsNullOrEmpty(requestThemeValue))
			{
				throw new ThemeRollerException("\"requestThemeValue\" is null or empty.");
			}

			if (string.IsNullOrEmpty(folderName))
			{
				throw new ThemeRollerException("\"folderName\" is null or empty.");
			}

			if (wijmoScriptsAccessor == null)
			{
				throw new ThemeRollerException("\"wijmoScriptsAccessor\" is null");
			}

			if (wijmoScriptsAccessor == null)
			{
				throw new ThemeRollerException("\"wijmoScriptsAccessor\" is null");
			}

			if (workingDir == null)
			{
				throw new ThemeRollerException("\"workingDir\" is null");
			}

			_designActionList = designActionList;
			_embeddedThemesAccessor = embeddedThemesAccessor;
			_project = project;
			_rootProjectItem = rootProjectItem;
			_requestThemeValue = requestThemeValue;
			_folderName = folderName;
			_wijmoScriptsAccessor = wijmoScriptsAccessor;
			_workingDir = workingDir;
			_customDir = customDir;
			_projectPath = Utilites.GetProjectPath(_project, _rootProjectItem);

			InitializeComponent();

			_scriptingMediator = new ScriptingMediator(this, requestThemeValue);
			webBrowser1.ObjectForScripting = _scriptingMediator;

			tbFolderName.Text = folderName;

#if DEBUG
			this.Text = "#debug: " + _workingDir.FullName;
#endif

			_DisableControls(true);

			_InititalizeContent();
		}

		public void ShowEditor(IList<string> addedThemes)
		{
			this._addedThemes = addedThemes;
			this.ShowDialog();
		}

		private void _EnableControls()
		{
			if (_closing)
			{
				return;
			}

			panel2.Enabled = true;
			panel1.Enabled = true;
			((Control)webBrowser1).Enabled = true;

			statusLabel.Text = C1Localizer.GetString("Done");
			statusProgressBar.Style = ProgressBarStyle.Blocks;
		}

		private void _DisableControls(bool needDisabledMainArea)
		{
			if (_closing)
			{
				return;
			}
			
			panel1.Enabled = false;
			if (needDisabledMainArea)
			{
				panel2.Enabled = false;
				((Control)webBrowser1).Enabled = false;
			}

			statusLabel.Text = C1Localizer.GetString("Done");
			statusProgressBar.Style = ProgressBarStyle.Marquee;
		}


		#region event handlers

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnDownloadSelectedTheme_Click(object sender, EventArgs e)
		{
			string themeValue = _scriptingMediator.GetCurrentSerializedThemeValue();
			string themeFolderName = tbFolderName.Text;

			if (!string.IsNullOrEmpty(_projectPath) && CheckFolderName(themeFolderName)
				&& CheckFolderName(themeFolderName, Path.Combine(_projectPath, Constants.CUSTOM_THEMES_FOLDER)))
			{
				DownloadAndEmbedTheme(themeFolderName, themeValue);
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			string themeValue = _scriptingMediator.GetCurrentSerializedThemeValue();
			string themeFolderName = tbFolderName.Text;

			if (CheckFolderName(themeFolderName))
			{
				using (FolderBrowserDialog dialog = new FolderBrowserDialog())
				{
					dialog.Description = C1Localizer.GetString("MsgOpenFolder");
					dialog.ShowNewFolderButton = false;

					if (dialog.ShowDialog() == DialogResult.OK && CheckFolderName(themeFolderName, dialog.SelectedPath))
					{
						SaveTheme(dialog.SelectedPath, themeFolderName, themeValue);
					}
				}
			}
		}

		#endregion

		private void _InititalizeContent()
		{
			Dispatcher dispatcher = Dispatcher.CurrentDispatcher;

			_cancellationTokenSource = new CancellationTokenSource();

			try
			{
				dispatcher.Invoke(new Action(() => { statusLabel.Text = C1Localizer.GetString("DeterminingJQueryUIVersion"); }));
				ThemeRollerHelper.ProcessJQueryUIVersion(_cancellationTokenSource,
					version =>
					{
						try
						{
							_jQueryUIVersion = version;
							dispatcher.Invoke(new Action(() => { statusLabel.Text = C1Localizer.GetString("DownloadingThemeCSS"); }));
							ThemeRollerHelper.ProcessCSSTheme(_requestThemeValue, _cancellationTokenSource,
								themeCss =>
								{
									try
									{
										dispatcher.Invoke(new Action(() => { statusLabel.Text = C1Localizer.GetString("CreatingHTML"); }));
										string htmlUri = new HtmlEvaluator(_jQueryUIVersion, themeCss, _requestThemeValue, _wijmoScriptsAccessor, _workingDir, _customDir).Evaluate("page.html");
										ThemeRollerHelper.DocumentTextTaskAsync(webBrowser1, htmlUri, delegate()
										{
											dispatcher.Invoke(new Action(() => { _EnableControls(); }));

											if (_cancellationTokenSource != null)
											{
												_cancellationTokenSource.Dispose();
												_cancellationTokenSource = null;
											}
										});
									}
									catch (Exception ex3)
									{
										OnAsyncException(ex3);
									}
								},
								exception =>
								{
									OnAsyncException(exception);
									dispatcher.Invoke(new Action(() => { _EnableControls(); }));

									if (_cancellationTokenSource != null)
									{
										_cancellationTokenSource.Dispose();
										_cancellationTokenSource = null;
									}
								});
						}
						catch (Exception ex2)
						{
							OnAsyncException(ex2);
							dispatcher.Invoke(new Action(() => { _EnableControls(); }));

							if (_cancellationTokenSource != null)
							{
								_cancellationTokenSource.Dispose();
								_cancellationTokenSource = null;
							}
						}

					},
					exception =>
					{
						OnAsyncException(exception);
						dispatcher.Invoke(new Action(() => { _EnableControls(); }));

						if (_cancellationTokenSource != null)
						{
							_cancellationTokenSource.Dispose();
							_cancellationTokenSource = null;
						}
					});

			}
			catch (Exception ex1)
			{
				OnAsyncException(ex1);
				dispatcher.Invoke(new Action(() => { _EnableControls(); }));

				if (_cancellationTokenSource != null)
				{
					_cancellationTokenSource.Dispose();
					_cancellationTokenSource = null;
				}
			}
		}

		

		private void DownloadAndEmbedTheme(string themeFolderName, string themeValue, string scope = "")
		{
			_DisableControls(true);

			Dispatcher dispatcher = Dispatcher.CurrentDispatcher;

			DirectoryInfo pathToExtract = null;
			_cancellationTokenSource = new CancellationTokenSource();

			try
			{
				pathToExtract = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()));

				// 1. Download .zip and extract it to the temp folder
				dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("DownloadingZippedTheme")));
				ThemeRollerHelper.ProcessZippedTheme(_jQueryUIVersion, themeFolderName, scope, themeValue, JQueryUISpecficCSS.All, _cancellationTokenSource,
					fileStream =>
					{
						try
						{
							dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("Extracting")));
							Extract(fileStream, pathToExtract.FullName, themeFolderName);
							fileStream.Dispose();

							// 2. Merge
							MergeWithCustom(Path.Combine(pathToExtract.FullName, themeFolderName));

							// 3. Append extracted directories to the project 
							dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("AddingToProject")));
							string themeFolderPath = string.Empty;
							if (_project != null)
							{
								ProjectItem themesFolder = ProjectHelper.AddPath(_project, Constants.CUSTOM_THEMES_FOLDER);

								if (themesFolder == null)
								{
									throw new ThemeRollerException(string.Format(C1Localizer.GetString("EMsgCantCreateDirectory"), Path.Combine(_projectPath, Constants.CUSTOM_THEMES_FOLDER)));
								}

								foreach (DirectoryInfo subDir in pathToExtract.GetDirectories())
								{
									// delete directory if already exists
									ProjectItem temp = themesFolder.FindPath(subDir.Name + @"\\$", 1);
									if (temp != null)
									{
										temp.Delete();
									}

									// add to the project
									themesFolder.ProjectItems.AddFromDirectory(subDir.FullName);
								}
								themeFolderPath = themesFolder.Path();
							}
							else
							{
								try
								{
									DirectoryInfo themesFolder = Utilites.AddFolder(_projectPath, Constants.CUSTOM_THEMES_FOLDER, false);
									themeFolderPath = themesFolder.FullName;
									foreach (DirectoryInfo subDir in pathToExtract.GetDirectories())
									{
										Utilites.Copy(subDir.FullName, Utilites.AddFolder(themeFolderPath, subDir.Name, true).FullName);
									}
								}
								catch
								{
									throw new ThemeRollerException(string.Format(C1Localizer.GetString("EMsgCantCreateDirectory"), Path.Combine(_projectPath, Constants.CUSTOM_THEMES_FOLDER)));
								}								
							}

							// 4. Update the web.config
							dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("UpdatingWebConfig")));

							//="~/Content/themes/<themeFolderName>/<file>.css"
							string themeDirectory = Path.Combine(themeFolderPath, themeFolderName);
							string cssFilePath = Directory.GetFiles(themeDirectory, "*.css", SearchOption.TopDirectoryOnly)[0];
							string wijmoThemeValue = string.Format("~/{0}/{1}/{2}", Constants.CUSTOM_THEMES_FOLDER, themeFolderName, Path.GetFileName(cssFilePath)).Replace('\\', '/');

							_designActionList.Theme = wijmoThemeValue;

							OnThemeAddedToProject(themeFolderName);
						}
						catch (Exception ex1)
						{
							OnAsyncException(ex1);
						}
						finally
						{
							dispatcher.Invoke(new Action(() => _EnableControls()));

							if (pathToExtract != null && pathToExtract.Exists)
							{
								try
								{
									pathToExtract.Delete(true);
								}
								catch
								{
								}
							}

							if (_cancellationTokenSource != null)
							{
								_cancellationTokenSource.Dispose();
								_cancellationTokenSource = null;
							}
						}
					},
					exception =>
					{
						OnAsyncException(exception);
						dispatcher.Invoke(new Action(() => _EnableControls()));

						if (pathToExtract != null && pathToExtract.Exists)
						{
							try
							{
								pathToExtract.Delete(true);
							}
							catch
							{
							}
						}

						if (_cancellationTokenSource != null)
						{
							_cancellationTokenSource.Dispose();
							_cancellationTokenSource = null;
						}
					});
			}
			catch (Exception ex2)
			{
				OnAsyncException(ex2);
				dispatcher.Invoke(new Action(() => _EnableControls()));

				if (pathToExtract != null && pathToExtract.Exists)
				{
					try
					{
						pathToExtract.Delete(true);
					}
					catch
					{
					}
				}

				if (_cancellationTokenSource != null)
				{
					_cancellationTokenSource.Dispose();
					_cancellationTokenSource = null;
				}
			}
		}

		private void SaveTheme(string pathToExtract, string themeFolderName, string themeValue, string scope = "")
		{
			_DisableControls(true);

			Dispatcher dispatcher = Dispatcher.CurrentDispatcher;

			try
			{
				_cancellationTokenSource = new CancellationTokenSource();

				dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("DownloadingZippedTheme")));
				ThemeRollerHelper.ProcessZippedTheme(_jQueryUIVersion, themeFolderName, scope, themeValue, JQueryUISpecficCSS.All, _cancellationTokenSource, delegate(FileStream fs)
				{
					try
					{
						dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("Extracting")));
						Extract(fs, pathToExtract, themeFolderName);
						fs.Dispose();
						MergeWithCustom(Path.Combine(pathToExtract, themeFolderName));
					}
					catch (Exception ex)
					{
						OnAsyncException(ex);
					}
					finally
					{

						dispatcher.Invoke(new Action(() => _EnableControls()));

						if (_cancellationTokenSource != null)
						{
							_cancellationTokenSource.Dispose();
							_cancellationTokenSource = null;
						}
					}
				},
				exception =>
				{
					OnAsyncException(exception);
					dispatcher.Invoke(new Action(() => _EnableControls()));

					if (_cancellationTokenSource != null)
					{
						_cancellationTokenSource.Dispose();
						_cancellationTokenSource = null;
					}
				});
			}
			catch (Exception ex1)
			{
				OnAsyncException(ex1);
				dispatcher.Invoke(new Action(() => _EnableControls()));

				if (_cancellationTokenSource != null)
				{
					_cancellationTokenSource.Dispose();
					_cancellationTokenSource = null;
				}
			}
		}

		internal void DonwloadAndInjectCSS(string themeFolderName, string themeValue)
		{
			_DisableControls(false);

			Dispatcher dispatcher = Dispatcher.CurrentDispatcher;

			_cancellationTokenSource = new CancellationTokenSource();

			try
			{
				dispatcher.Invoke(new Action(() => statusLabel.Text = C1Localizer.GetString("DownloadingThemeCSS")));
				ThemeRollerHelper.ProcessCSSTheme(themeValue, _cancellationTokenSource,
					cssContent =>
					{
						try
						{
							dispatcher.Invoke(new Action(() =>
							{
								statusLabel.Text = C1Localizer.GetString("InjectingCSS");
								ThemeRollerHelper.InjectCSS(webBrowser1.Document, Constants.CSS_ELEMENT_ID, cssContent + "\n" + Utilites.GetCustomCSSContent(_customDir));
							}));
						}
						catch (Exception ex)
						{
							OnAsyncException(ex);
						}
						finally
						{
							dispatcher.Invoke(new Action(() => _EnableControls()));

							if (_cancellationTokenSource != null)
							{
								_cancellationTokenSource.Dispose();
								_cancellationTokenSource = null;
							}
						}
					},
					exception =>
					{
						OnAsyncException(exception);
						dispatcher.Invoke(new Action(() => _EnableControls()));

						if (_cancellationTokenSource != null)
						{
							_cancellationTokenSource.Dispose();
							_cancellationTokenSource = null;
						}
					});

			}
			catch (Exception e)
			{
				OnAsyncException(e);
				dispatcher.Invoke(new Action(() => _EnableControls()));

				if (_cancellationTokenSource != null)
				{
					_cancellationTokenSource.Dispose();
					_cancellationTokenSource = null;
				}
			}
			finally
			{

			}
		}

		private void Extract(FileStream fileStream, string pathToExtract, string themeFolderName)
		{
			DirectoryInfo extractDirectoryRoot = Directory.CreateDirectory(pathToExtract);

            //for jquery 1.11.0, only need the jquery-ui.css, and the whole images folder.
			using (C1.C1Zip.C1ZipFile zip = new C1Zip.C1ZipFile(fileStream))
			{
				foreach (C1.C1Zip.C1ZipEntry zipEntry in zip.Entries)
				{
                    string fileName = zipEntry.FileName;//jquery-ui-1.11.0.custom/images/a.img or jquery-ui-1.11.0.custom/jquery-ui.css
                    fileName = fileName.Substring(fileName.IndexOf('/') + 1);

                    if(fileName.StartsWith("images/", StringComparison.OrdinalIgnoreCase)
                        || string.Compare(fileName, Constants.CSS_JQUERY_UI, true) == 0)
					{
                        string filePath = Path.Combine(extractDirectoryRoot.FullName, themeFolderName + "\\" + fileName);
						string dir = Path.GetDirectoryName(filePath);

						Directory.CreateDirectory(dir);
						zip.Extract(zipEntry, filePath);
					}
				}
			}
		}

		private bool CheckFolderName(string folderName)
		{
			if (string.IsNullOrEmpty(folderName))
			{
				MessageBox.Show(C1Localizer.GetString("EMsgEmptyDir"));
				return false;
			}

			foreach (var info in Utilites.JQueryUIThemes)
			{
				if (folderName.ToLower() == info.HostName.ToLower())
				{
					MessageBox.Show(C1Localizer.GetString("EMsgFolderNameConflictJQuery"));
					return false;
				}
			}

			if (_embeddedThemesAccessor != null)
			{
				foreach (var themeName in _embeddedThemesAccessor.GetHostNames())
				{
					if (folderName.ToLower() == themeName.ToLower())
					{
						MessageBox.Show(C1Localizer.GetString("EMsgFolderNameConflictWijmo"));
						return false;
					}
				}
			}

			return true;
		}

		private bool CheckFolderName(string themeName, string parentFolder)
		{
			if (Directory.Exists(Path.Combine(parentFolder, themeName)))
			{
				string msg = C1Localizer.GetString("ThemeRoller.UI.ConfirmWhetherToOverride", "The folder already contains the theme named \"{0}\". Would you like to replace the existing one?");
				msg = string.Format(msg, themeName);
				if (MessageBox.Show(msg, C1Localizer.GetString("ThemeRoller.UI.ConfirmThemeReplace", "Confirm Theme Replace"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
					return false;
			}

			return true;
		}

		private void MergeWithCustom(string themeFolder)
		{
			if (_customDir != null)
			{
				// 1. copy images
				DirectoryInfo imagesDir = new DirectoryInfo(Path.Combine(_customDir.FullName, Constants.THEME_IMAGES_FOLDER));
				if (imagesDir.Exists)
				{
					FileInfo[] images = imagesDir.GetFiles();
					if (images.Length > 0)
					{
						string destination = Path.Combine(themeFolder, Constants.THEME_IMAGES_FOLDER);

						Directory.CreateDirectory(destination);

						foreach (FileInfo image in images)
						{
							image.CopyTo(Path.Combine(destination, image.Name), true);
						}
					}
				}

				// 2. extend a .css with a custom content
				string[] desination = Directory.GetFiles(themeFolder, "*.css", SearchOption.TopDirectoryOnly);
				if (desination.Length > 0)
				{
					string customContent = Utilites.GetCustomCSSContent(_customDir);
					if (!string.IsNullOrEmpty(customContent))
					{
						customContent =
							Constants.CSS_CUSTOMPART_MARKER + "\n" +
							customContent +
							Constants.CSS_CUSTOMPART_MARKER + "\n";
						File.AppendAllText(desination[0], customContent);
					}
				}

			}
		}

		private void OnAsyncException(Exception e)
		{
			if (_closing) // if (_cancellationTokenSource != null && _cancellationTokenSource.IsCancellationRequested)
			{
				return; // do not show exceptions (cancellation) when form is closing.
			}

            while (e != null && e.InnerException != null)
            {
                e = e.InnerException;
            }
			//error settings for theme values.
			//The remote server returned an error: (500) Internal Server Error.

			//network error list:
            //The remote server returned an error: (407) Proxy Authentication Required.
            //The proxy name could not be resolved: 'shproxy'-----> remove the network.
            //The remote name could not be resolved: 'download.jqueryui.com'----->remove the proxy and network
            if (e is System.Net.WebException)
            {
                string msg = string.Empty;
                if (e.Message.IndexOf(Constants.PROXY_AUTHENTICATION_REQUIRED, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    msg = C1Localizer.GetString("ThemeRoller.Exception.ProxyRequired",
                     "ThemeRoller for Visual Studio does not work with proxy internet connections. Please try again without using a proxy.");
                }
				else if (e.Message.IndexOf(Constants.INTERNAL_SERVER_ERROR, StringComparison.OrdinalIgnoreCase) != -1)
				{
					msg = C1Localizer.GetString("ThemeRoller.Exception.InvalidSettings",
					 "Invalid settings for some style settings in the theme. Please check your inputs.");
				}
                else
                {
                    msg = C1Localizer.GetString("ThemeRoller.Exception.CannotConnect",
                     "ThemeRoller for Visual Studio requires internet connection to work. Please make sure you are connected to the internet and try again.");
                }
                Utilites.ShowErrorMessage(msg);
            }
            else
            {
                Utilites.ShowErrorMessage(e.Message);
            }
		}

		private void ThemeEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			_closing = true;

			if (_cancellationTokenSource != null)
			{
				_cancellationTokenSource.Cancel();
			}
		}

		internal string getFolderName()
		{
			return tbFolderName.Text;
		}

		private void OnThemeAddedToProject(string themeName)
		{
			if (_addedThemes != null)
			{
				_addedThemes.Add(themeName);
			}
		}

        private void webBrowser1_PreviewKeyDown(object sender, System.Windows.Forms.PreviewKeyDownEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Control | Keys.A: // Select All
                case Keys.Control | Keys.C: // Copy
                case Keys.Control | Keys.V: // Paste
                case Keys.Delete: // Delete
				case Keys.Control | Keys.X: // Cut
				case Keys.Control | Keys.Z: // Undo
                    ((WebBrowser)sender).WebBrowserShortcutsEnabled = true;
                    break;
                default:
                    ((WebBrowser)sender).WebBrowserShortcutsEnabled = false;
                    break;
            }
        }

		internal void DisableButtons()
		{
			this.btnSave.Enabled = false;
			this.btnAddToProject.Enabled = false;
		}

		internal void EnableButtons()
		{
			this.btnSave.Enabled = true;
			this.btnAddToProject.Enabled = true;
		}
	}

	[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
	[ComVisible(true)]
	public sealed class ScriptingMediator
	{
		private ThemeEditorForm _form;
		private string _serializedThemeValue;

		internal ScriptingMediator(ThemeEditorForm form, string serializedThemeValue)
		{
			_form = form;
			_serializedThemeValue = serializedThemeValue;
		}

		public void ThemeChanged(bool isValid, string propertyName, string newThemeValue)
		{
			if (!isValid)
			{
				_form.DisableButtons();
				string msg = C1Localizer.GetString("ThemeRoller.Exception.InvalidValue",
					 "Invalid value for {0} in the theme. Please check your inputs.");
				
				Utilites.ShowErrorMessage(string.Format(msg, propertyName));
				return;
			}
			_form.EnableButtons();
			_serializedThemeValue = newThemeValue;

			string themeName = _form.getFolderName();
			_form.DonwloadAndInjectCSS(themeName, newThemeValue);
		}

		public string GetCurrentSerializedThemeValue()
		{
			return _serializedThemeValue;
		}

		public string GetResource(string resourceName, string defaultValue)
		{
			return C1Localizer.GetString(resourceName, defaultValue);
		}
	}
}
