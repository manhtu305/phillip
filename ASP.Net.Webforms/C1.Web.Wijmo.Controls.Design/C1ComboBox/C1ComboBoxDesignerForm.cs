﻿#region File Header
//==============================================================================
//  FileName    :   C1comboDesignerForm.cs
//
//  Description :   Provides a UI operation to handle the combo control.
//
//  Copyright (c) 2001 - 2008 GrapeCity, Inc.
//  All rights reserved.
//
//                                                          ... Ryan Wu
//==============================================================================

//------------------------------------------------------------------------------
//  Update Log:
//
//  Status          Date            Name                    BUG-ID
//  ----------------------------------------------------------------------------
//  Created         2008/06/28      Ryan Wu					None
//  Updated         2008/07/23      Ryan Wu                 Update comments      
//
//------------------------------------------------------------------------------
#endregion end of File Header.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Text;
using Control = System.Web.UI.Control;


namespace C1.Web.Wijmo.Controls.Design.C1ComboBox
{
	using C1.Web.Wijmo.Controls.C1ComboBox;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// Represents a UI form to handle the combo control.
	/// </summary>
	public class C1ComboBoxDesignerForm : C1BaseItemEditorForm
	{
		private C1ComboBox _c1ComboBox;
		private Stream _clipboardData;
		private C1ItemInfo _clipboardType;

		public C1ComboBoxDesignerForm(C1ComboBox control)
			: base(control)
		{
			try
			{
				_c1ComboBox = control;
				InitializeComponent();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		protected override List<C1ItemInfo> FillAvailableControlItems()
		{
			List<C1ItemInfo> list = new List<C1ItemInfo>();

			C1ItemInfo itemInfo = new C1ItemInfo();
			itemInfo.EnableChildItems = true;
			itemInfo.ContextMenuStripText = "C1ComboBox";
			itemInfo.NodeImage = Properties.Resources.RootItemIco;
			itemInfo.DefaultNodeText = "C1ComboBox";
			itemInfo.Visible = false;
			list.Add(itemInfo);

			itemInfo = new C1ItemInfo();
			itemInfo.EnableChildItems = false;
			itemInfo.ContextMenuStripText = "C1ComboBoxItem";
			itemInfo.NodeImage = Properties.Resources.LinkItemIco;
			itemInfo.DefaultNodeText = "C1ComboBoxItem";
			itemInfo.Default = true;
			itemInfo.Visible = true;
			list.Add(itemInfo);

			//itemInfo = new C1ItemInfo();
			//itemInfo.EnableChildItems = false;
			//itemInfo.ContextMenuStripText = "C1ComboCell";
			//itemInfo.NodeImage = Properties.Resources.C1GridViewColDBField;
			//itemInfo.DefaultNodeText = "C1ComboCell";
			//itemInfo.Visible = true;

			//list.Add(itemInfo);
			return list;
		}

		protected override void LoadControl(TreeView mainTreeView)
		{
			TreeNode rootNode = new TreeNode();
			C1ItemInfo itemInfo = ItemsInfo[0];
			string nodeText = _c1ComboBox.ID ?? itemInfo.DefaultNodeText;
			C1NodeInfo nodeTag = new C1NodeInfo(_c1ComboBox, itemInfo, nodeText);

			mainTreeView.BeginUpdate();
			mainTreeView.Nodes.Clear();

			SetNodeAttributes(rootNode, nodeTag);
			itemInfo = ItemsInfo[1];
			//C1ItemInfo cellInfo = ItemsInfo[2];
			foreach (C1ComboBoxItem item in _c1ComboBox.Items)
			{
				TreeNode itemNode = new TreeNode();
				nodeText = item.Text;
				nodeTag = new C1NodeInfo(item, itemInfo, nodeText);
				SetNodeAttributes(itemNode, nodeText, nodeTag);
				//foreach (C1ComboCell cell in item.Cells)
				//{
				//    TreeNode cellNode = new TreeNode();
				//    string cellText = cell.Text;
				//    nodeTag = new C1NodeInfo(cell, cellInfo, cellText);
				//    SetNodeAttributes(cellNode, cellText, nodeTag);
				//    itemNode.Nodes.Add(cellNode);
				//}

				rootNode.Nodes.Add(itemNode);
			}

			mainTreeView.Nodes.Add(rootNode);
			mainTreeView.SelectedNode = mainTreeView.Nodes[0];
			mainTreeView.ExpandAll();
			mainTreeView.EndUpdate();
		}

		/// <summary>
		/// Updates status of controls of Maincombo, MainMenu and context menus.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo of current selected item.</param>
		/// <param name="parentItemInfo">C1ItemInfo of parent of current selected item.</param>
		/// <param name="previousItemInfo">C1ItemInfo of previous item of current selected item</param>
		protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
		{
			bool atRoot = (parentItemInfo == null);
			bool atItem = itemInfo.DefaultNodeText == "C1ComboBoxItem";
			//bool atCellItem = itemInfo.DefaultNodeText == "C1ComboCell";

			base.AllowAdd = atRoot;//atRoot || atItem;
			//base.AllowInsert = atCellItem || atItem;
			base.AllowInsert = atItem;
			
			base.AllowCopy = !atRoot;
			base.AllowCut = !atRoot;
			base.AllowDelete = !atRoot;
			base.AllowRename = !atRoot;
			base.AllowPaste = !atRoot;

			base.AllowMoveLeft = false;
			base.AllowMoveRight = false;
			base.AllowChangeType = false;

			AddChildTypeContextMenu.Items[0].Visible = atRoot;
			//AddChildTypeContextMenu.Items[1].Visible = atItem;
			
			InsertItemTypeContextMenu.Items[0].Visible = atItem;
			//InsertItemTypeContextMenu.Items[1].Visible = atCellItem;

			if (atRoot)
			{
				// combo item
				_lastAddedItemInfo = ItemsInfo[1];
				_lastInsertedItemInfo = null;
			}
			if (atItem)
			{
				// cell item
				//_lastAddedItemInfo = ItemsInfo[2];
				_lastInsertedItemInfo = ItemsInfo[1];
			}
			//if (atCellItem)
			//{
			//    // cell item
			//    _lastAddedItemInfo = null;
			//    _lastInsertedItemInfo = ItemsInfo[2];
			//}
			
			
			

			base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
		}

		/// <summary>
		/// Creates the C1NodeInfo object that will contain both the specific C1 control tab depending on the itemInfo
		/// and given itenInfo.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo object.</param>
		/// <param name="nodesList">Nodes that currently exist into the list where the new created item will be inserted.</param>
		/// <returns>Returns a C1 control that depends on the C1ItemInfo. Given nodesList
		/// could be used to obtain a new numbered name.</returns>
		protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
		{
			string nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);
			C1NodeInfo nodeInfo = null;
			if (itemInfo.DefaultNodeText == "C1ComboCell")
			{

				//C1ComboCell cell = new C1ComboCell();
				//cell.Text = nodeText;
				//// create a new node info
				//nodeInfo = new C1NodeInfo(cell, itemInfo, nodeText);

				//// set nodeText property
				//nodeInfo.NodeText = nodeText;
			}
			else
			{

				C1ComboBoxItem item = new C1ComboBoxItem(nodeText);
				// create a new node info
				nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);
				// set nodeText property
				nodeInfo.NodeText = nodeText;
			}


			return nodeInfo;
		}

		/// <summary>
		///  Indicates when an combo item was inserted in a specific position.
		/// </summary>
		/// <param name="item">The inserted combo item.</param>
		/// <param name="destinationItem">The destination tab that will contain inserted tab.</param>
		/// <param name="destinationIndex">The index position of given tab within destinationItem items collection.</param>
		protected override void Insert(object item, object destinationItem, int destinationIndex)
		{
			try
			{
				//if (item is C1ComboCell && destinationItem is C1ComboBoxItem)
				//{
				//    ((C1ComboBoxItem)destinationItem).Cells.Insert(destinationIndex, item as C1ComboCell);
				//}
				//else if (item is C1ComboBoxItem)
				//{
				_c1ComboBox.Items.Insert(destinationIndex, item as C1ComboBoxItem);
				//}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// <summary>
		///  Indicates when an combo item was deleted.
		/// </summary>
		/// <param name="item">The deleted combo item.</param>
		protected override void Delete(object item)
		{
			try
			{
				//C1ComboCell cell = item as C1ComboCell;
				//if (cell !=null)
				//{
				//    cell.ParentItem.Cells.Remove(cell);
				//}
				//else
				//{
				_c1ComboBox.Items.Remove(item as C1ComboBoxItem);
				//}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// Indicates when an combo item was copyed to a specific position.
		/// </summary>
		/// <param name="item">The item to copy.</param>
		/// <param name="itemInfo">The item info of the item to be copied.</param>
		protected override void Copy(object item, C1ItemInfo itemInfo)
		{
			if (_clipboardData == null)
			{
				_clipboardData = new MemoryStream();
			}

			Copy(_clipboardData, item);

			_clipboardType = itemInfo;
			// tell to base there is som data into clipboard
			ClipboardData = true;
		}

		/// <summary>
		/// Copy the current target to the buffer.
		/// </summary>
		/// <param name="buffer">The current buffer.</param>
		/// <param name="target">The specified targert that will be copied to buffer.</param>
		private static void Copy(Stream buffer, object target)
		{
			C1ComboBoxSerializer serializer = new C1ComboBoxSerializer(target);

			buffer.SetLength(0);
			serializer.SaveLayout(buffer);
		}


		/// <summary>
		/// Pastes node from clipboard to given destination node.
		/// </summary>
		/// <param name="destinationNode">Destination node.</param>
		protected override void Paste(TreeNode destinationNode)
		{
			Paste(_clipboardData, destinationNode);
		}

		/// <summary>
		/// Paste the current buffer value to the specified destination node.
		/// </summary>
		/// <param name="buffer">The current buffer value.</param>
		/// <param name="destNode">The specified destination node that will be pasted.</param>
		private void Paste(Stream buffer, TreeNode destNode)
		{
			C1NodeInfo info = (C1NodeInfo)destNode.Tag;
			TreeNode node = null;

			if (info.ItemInfo.DefaultNodeText == "C1ComboBoxItem")
			{
				if (_clipboardType.DefaultNodeText == "C1ComboBoxItem")
				{
					C1ComboBoxItem item = new C1ComboBoxItem();
					C1ComboBoxSerializer serializer = new C1ComboBoxSerializer(item);
					buffer.Seek(0, SeekOrigin.Begin);
					// lets to serializer to retrieve data from clipboard
					serializer.LoadLayout(buffer, LayoutType.All);
					C1NodeInfo nodeInfo = new C1NodeInfo(item, _clipboardType, item.Text);
					_c1ComboBox.Items.Insert(_c1ComboBox.Items.IndexOf(info.Element as C1ComboBoxItem) +1, item);
					
					// create new treenode to be added on destination node
					node = new TreeNode();
					// set node text and node tag
					SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);
					destNode.Parent.Nodes.Insert(destNode.Index + 1, node);
					//foreach (C1ComboCell cell in item.Cells)
					//{
					//    TreeNode cellNode = new TreeNode();
					//    // set node text and node tag
					//    C1NodeInfo cellNodeInfo = new C1NodeInfo(cell, ItemsInfo[2], cell.Text);
					//    SetNodeAttributes(cellNode, cellNodeInfo.NodeText, cellNodeInfo);
					//    node.Nodes.Add(cellNode);
					//}
				}
				if (_clipboardType.DefaultNodeText == "C1ComboCell")
				{
					//C1ComboCell cell = new C1ComboCell();
					//C1ComboBoxSerializer serializer = new C1ComboBoxSerializer(cell);
					//buffer.Seek(0, SeekOrigin.Begin);
					//// lets to serializer to retrieve data from clipboard
					//serializer.LoadLayout(buffer, LayoutType.All);

					//// add deserialized item
					//((C1ComboBoxItem)info.Element).Cells.Add(cell);
					//// create node info
					//C1NodeInfo nodeInfo = new C1NodeInfo(cell, _clipboardType, cell.Text);
					//// create new treenode to be added on destination node
					//node = new TreeNode();
					//// set node text and node tag
					//SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);
					//destNode.Nodes.Add(node);
				}
			}
			else if (info.ItemInfo.DefaultNodeText == "C1ComboCell")
			{
				//if (_clipboardType.DefaultNodeText == "C1ComboCell")
				//{
				//    C1ComboCell cell = new C1ComboCell();
				//    C1ComboBoxSerializer serializer = new C1ComboBoxSerializer(cell);
				//    buffer.Seek(0, SeekOrigin.Begin);
				//    // lets to serializer to retrieve data from clipboard
				//    serializer.LoadLayout(buffer, LayoutType.All);

				//    // add deserialized item
				//    C1ComboCell c = info.Element as C1ComboCell;
				//    if (c == null)
				//    {
				//        return;
				//    }
				//    C1ComboBoxItem item = c.ParentItem;
				//    item.Cells.Insert(item.Cells.IndexOf(c) + 1, cell);
				//    // create node info
				//    C1NodeInfo nodeInfo = new C1NodeInfo(cell, _clipboardType, cell.Text);
				//    // create new treenode to be added on destination node
				//    node = new TreeNode();
				//    // set node text and node tag
				//    SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);
				//    destNode.Parent.Nodes.Insert(destNode.Index + 1, node);
				//}
			}
		}

		/// <summary>
		/// Shows current control into preview tab.
		/// </summary>
		/// <param name="previewer">The web browser previewer</param>
		internal override void ShowPreviewHtml(C1WebBrowser previewer)
		{
			try
			{
				string sResultBodyContent = "";
				StringBuilder sb = new StringBuilder();
				StringWriter tw = new StringWriter(sb);
				HtmlTextWriter htw = new HtmlTextWriter(tw);
				_c1ComboBox.RenderControl(htw);
				sResultBodyContent = sb.ToString();
				string sDocumentContent = "";
				sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + Environment.NewLine;
				sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + Environment.NewLine;
				sDocumentContent += "<head>" + Environment.NewLine;
				sDocumentContent += "</head>" + Environment.NewLine;
				sDocumentContent += "<body>" + Environment.NewLine;
				sDocumentContent += sResultBodyContent + Environment.NewLine;
				sDocumentContent += "</body>" + Environment.NewLine;
				sDocumentContent += "</html>" + Environment.NewLine;
				if (previewer.Document != null)
				{
					previewer.DocumentWrite(sDocumentContent, this._c1ComboBox);
				}
				
			}
			catch (Exception ex)
			{
				MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
			}
		}

		/// <summary>
		/// Saves combo control to a XML file.
		/// </summary>
		/// <param name="fileName">The name for XML file.</param>
		protected override void SaveToXML(string fileName)
		{
			try
			{
				_c1ComboBox.SaveLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		/// <summary>
		/// Fills combo control from a saved XML file. 
		/// </summary>
		/// <param name="fileName">The name of the XML file to be loaded.</param>
		protected override void LoadFromXML(string fileName)
		{
			try
			{
				_c1ComboBox.LoadLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		/// <summary>
		/// Performs an action after treeview label edition.
		/// </summary>
		/// <param name="e">Node label edit event arguments.</param>
		/// <param name="selectedNode">Current selected node.</param>
		protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
		{
			if (e.Label == null)
			{
				return;
			}
			bool cancel = e.CancelEdit;
			FinishLabelEdit(e.Node, e.Label, ref cancel);
			e.CancelEdit = cancel;
		}

		/// <summary>
		/// Finish to set the text of the node.
		/// </summary>
		/// <param name="node">The specified node.</param>
		/// <param name="text">The specified text value.</param>
		/// <param name="cancelEdit">Indicates whether we will cancel the current edit action.</param>
		private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
		{
			string nodeText = text;
			string property = "Text";
			object obj = ((C1NodeInfo)node.Tag).Element;

			// set new text value to selected object and property grid
			TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
			base.RefreshPropertyGrid();
			node.Text = nodeText;
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// C1ComboBoxDesignerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(492, 516);
			this.Name = C1Localizer.GetString("C1ComboBox.DesignerForm.Title");
			this.Text = C1Localizer.GetString("C1ComboBox.DesignerForm.Title");
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		#endregion
	}
}
