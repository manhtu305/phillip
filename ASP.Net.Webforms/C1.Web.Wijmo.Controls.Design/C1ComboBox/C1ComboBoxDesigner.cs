﻿#region using

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Windows.Forms;
 

#endregion

namespace C1.Web.Wijmo.Controls.Design.C1ComboBox
{
	using C1.Web.Wijmo.Controls.C1ComboBox;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// Represents a C1ToolBarDesigner class for the C1ToolBar control.
	/// </summary>
	[SupportsPreviewControl(true)]
	public class C1ComboBoxDesigner : C1DataBoundControlDesigner
	{

		#region ** fields

		internal delegate bool EditMethod();
		private C1ComboBox _control;
		private TemplateGroupCollection _templateGroups;

		#endregion end of ** fields


		#region ** properties

		/// <summary>
		/// DataTextField
		/// </summary>
		public string DataTextField
		{
			get
			{
				return _control.DataTextField;
			}
			set
			{
				_control.DataTextField = value;
			}
		}

		/// <summary>
		/// DataValueField
		/// </summary>
		public string DataValueField
		{
			get
			{
				return _control.DataValueField;
			}
			set
			{
				_control.DataValueField = value;
			}
		}

		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}


		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection lists = new DesignerActionListCollection();

				lists.AddRange(base.ActionLists);
				lists.Add(new C1ComboBoxActionList(this, Component));
				return lists;
			}
		}


		///// <summary>
		///// Gets a collection of template groups, each containing one or more template definitions.
		///// </summary>
		public override TemplateGroupCollection TemplateGroups
		{
			get
			{
				TemplateGroupCollection templateGroupCollection = base.TemplateGroups;
				_templateGroups = new TemplateGroupCollection();
				//AddCommonTemplateGroup(_templateGroups, "HeaderTemplate", "Header");
				AddCommonTemplateGroup(_templateGroups, "ItemsTemplate", "Items");
				//AddCommonTemplateGroup(_templateGroups, "FooterTemplate", "Footer");

				for (int i = 0; i < _control.Columns.Count; i++)
				{
					//C1ComboBoxBindColumn c = _control.Columns[i] as C1ComboBoxBindColumn;
					//if (c != null)
					//{
					//    AddCommonTemplateGroup(_templateGroups, c.HeaderText, "Content");
					//}
				}
				templateGroupCollection.AddRange(_templateGroups);
				return templateGroupCollection;
			}
		}

		protected virtual void AddCommonTemplateGroup(TemplateGroupCollection templateGroups, string templateName, string templatePrefix)
		{
			TemplateGroup templateGroup = new TemplateGroup(templateName);
			TemplateDefinition templateDefinition = new TemplateDefinition(this, templateName, Component, String.Format("{0}Template", templatePrefix));
			templateDefinition.SupportsDataBinding = true;
			templateGroup.AddTemplateDefinition(templateDefinition);
			templateGroups.Add(templateGroup);
		}

		#endregion ** properties

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			_control = (C1ComboBox)component;
			SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		internal void BuildItems()
		{
			InvokeItemsCollectionEditor(EditItems);
		}

		internal bool InvokeItemsCollectionEditor(EditMethod m)
		{
			IDesignerHost idesignerHost = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
			using (DesignerTransaction designerTransaction = idesignerHost.CreateTransaction(C1Localizer.GetString("C1ComboBox.ItemsCollectionEditor")))
			{
				IComponentChangeService icomponentChangeService = (IComponentChangeService)Component.Site.GetService(typeof(IComponentChangeService));
				if (icomponentChangeService != null)
				{
					try
					{
						icomponentChangeService.OnComponentChanging(Component, null);
					}
					catch (CheckoutException e)
					{
						if (e != CheckoutException.Canceled)
						{
							throw e;
						}
						return false;
					}
				}
				if (!m.Invoke())
				{
					designerTransaction.Cancel();
					return false;
				}
				if (icomponentChangeService != null)
				{
					icomponentChangeService.OnComponentChanged(Component, null, null, null);
				}
				designerTransaction.Commit();
			}
			return true;
		}

		private bool EditItems()
		{
			C1ComboBoxDesignerForm controlItemContainerEditor = new C1ComboBoxDesignerForm(_control);
			return controlItemContainerEditor.ShowDialog() == System.Windows.Forms.DialogResult.OK;
			 
		}

		/// <summary>
		/// GetDesignTimeHtml
		/// </summary>
		/// <returns>returns design time html.</returns>
		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			string s;
			try
			{
				s = base.GetDesignTimeHtml();
			}
			catch (Exception e)
			{
				s = GetErrorDesignTimeHtml(e);
			}
			return s;
		}

		/// <summary>
		/// Edits the columns.
		/// </summary>
		private bool EditColumns()
		{
			C1ComboBoxColumnsForm controlItemContainerEditor = new C1ComboBoxColumnsForm(_control);
			return controlItemContainerEditor.ShowDialog() == System.Windows.Forms.DialogResult.OK;
		 
		}

		internal void BuildColumns()
		{
			InvokeItemsCollectionEditor(EditColumns);
		}
	}


	/// <summary>
	/// Represents a C1ToolBarActionList class which derived from DesignerActionList
	/// to define Smart Tag entries and resultant actions.
	/// </summary>
	public class C1ComboBoxActionList : DesignerActionListBase
	{

		#region ** fields
		internal IComponent _component;
		private readonly C1ComboBoxDesigner _designer;
		//private DesignerActionItemCollection items;
		#endregion end of ** fields.


		#region ** constructor

		/// <summary>
		/// C1ComboBoxActionList constructor.
		/// </summary>
		/// <param name="designer">The specified control designer.</param>
		/// <param name="component">The specified control.</param>
		public C1ComboBoxActionList(C1ComboBoxDesigner designer, IComponent component)
			: base(designer)
		{
			_designer = designer;
			_component = component;
		}

		#endregion end of ** constructor.

		#region ** properties



		protected bool IsDataBound
		{
			get
			{
				return !String.IsNullOrEmpty(_designer.DataSourceID);
			}
		}

		protected virtual DesignerActionItemCollection ActionItems
		{
			get
			{
				DesignerActionItemCollection designerActionItemCollection = new DesignerActionItemCollection();
				if (IsDataBound)
				{
					CopyActionItemsTo(DataBindingActionItems, designerActionItemCollection);
				}
				else
				{
					designerActionItemCollection.Add(new DesignerActionMethodItem(this, "EditItems", C1Localizer.GetString("C1ComboBox.SmartTag.EditItems"), String.Empty, C1Localizer.GetString("C1ComboBox.SmartTag.EditItemsDescription"), true));
				}
				designerActionItemCollection.Add(new DesignerActionMethodItem(this, "EditColumns", C1Localizer.GetString("C1ComboBox.SmartTag.EditColumns"), String.Empty, C1Localizer.GetString("C1ComboBox.SmartTag.EditColumnsDescription"), true));
				CopyActionItemsTo(base.GetSortedActionItems(), designerActionItemCollection);
				return designerActionItemCollection;
			}
		}

		protected virtual DesignerActionItemCollection DataBindingActionItems
		{
			get
			{
				DesignerActionItemCollection designerActionItemCollection = new DesignerActionItemCollection();
				designerActionItemCollection.Add(new DesignerActionPropertyItem("DataTextField", C1Localizer.GetString("C1ComboBox.SmartTag.DataTextField"), "Data", C1Localizer.GetString("C1ComboBox.SmartTag.DataTextFieldDescription")));
				designerActionItemCollection.Add(new DesignerActionPropertyItem("DataValueField", C1Localizer.GetString("C1ComboBox.SmartTag.DataValueField"), "Data", C1Localizer.GetString("C1ComboBox.SmartTag.DataValueFieldDescription")));
				return designerActionItemCollection;
			}
		}

		/// <summary>
		/// DataTextField
		/// </summary>
		[Editor(typeof(DataFieldEditor), typeof(UITypeEditor))]
		public string DataTextField
		{
			get
			{
				return _designer.DataTextField;
			}
			set
			{
				SetPropertyValue("DataTextField", value);
			}
		}

		/// <summary>
		/// DataValueField
		/// </summary>
		[Editor(typeof(DataFieldEditor), typeof(UITypeEditor))]
		public string DataValueField
		{
			get
			{
				return _designer.DataValueField;
			}
			set
			{
				SetPropertyValue("DataValueField", value);
			}
		}
		#endregion end of ** properties

		#region ** methods

		/// <summary>
		/// Override GetSortedActionItems method.
		/// </summary>
		/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			return ActionItems;
		}


		/// <summary>
		/// Edit control's items.
		/// </summary>
		internal void EditItems()
		{
			_designer.BuildItems();
		}

		/// <summary>
		/// Edit control's items.
		/// </summary>
		internal void EditColumns()
		{
			_designer.BuildColumns();
		}

		/// <summary>
		/// Copy action items between lists.
		/// </summary>
		/// <param name="sourceCollection"></param>
		/// <param name="targetCollection"></param>
		private static void CopyActionItemsTo(DesignerActionItemCollection sourceCollection, DesignerActionItemCollection targetCollection)
		{
			foreach (DesignerActionItem designerActionItem in sourceCollection)
			{
				targetCollection.Add(designerActionItem);
			}
		}

		protected void SetPropertyValue(string propertyName, string value)
		{
			TypeDescriptor.GetProperties(_designer.Component)[propertyName].SetValue(_designer.Component, value);
		}

		#endregion end of ** methods.
	}
}