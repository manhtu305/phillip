﻿#region using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Web.UI;
using System.Text;
#endregion end of using

namespace C1.Web.Wijmo.Controls.Design.C1ComboBox
{
	using C1.Web.Wijmo.Controls.C1ComboBox;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// Represents a UI form to handle the combo control.
	/// </summary>
	public class C1ComboBoxColumnsForm : C1BaseItemEditorForm
	{
		private C1ComboBox _c1ComboBox;
		private Stream _clipboardData;
		private C1ItemInfo _clipboardType;

		public C1ComboBoxColumnsForm(C1ComboBox control)
			: base(control)
		{
			try
			{
				_c1ComboBox = control;
				InitializeComponent();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		protected override List<C1ItemInfo> FillAvailableControlItems()
		{
			List<C1ItemInfo> list = new List<C1ItemInfo>();

			C1ItemInfo itemInfo = new C1ItemInfo();
			itemInfo.EnableChildItems = true;
			itemInfo.ContextMenuStripText = "C1ComboBox";
			itemInfo.NodeImage = Properties.Resources.RootItemIco;
			itemInfo.DefaultNodeText = "C1ComboBox";
			itemInfo.Visible = false;
			itemInfo.Default = false;

			list.Add(itemInfo);

			itemInfo = new C1ItemInfo();
			itemInfo.EnableChildItems = false;
			itemInfo.ContextMenuStripText = "C1ComboBoxColumn";
			itemInfo.NodeImage = Properties.Resources.LinkItemIco;
			itemInfo.DefaultNodeText = "C1ComboBoxColumn";
			itemInfo.Visible = true;
			itemInfo.Default = true;

			list.Add(itemInfo);

			//itemInfo = new C1ItemInfo();
			//itemInfo.EnableChildItems = false;
			//itemInfo.ContextMenuStripText = "C1ComboBoxBindColumn";
			//itemInfo.NodeImage = Properties.Resources.LinkItemIco;
			//itemInfo.DefaultNodeText = "C1ComboBoxBindColumn";
			//itemInfo.Visible = true;

			//list.Add(itemInfo);
			return list;
		}

		protected override void LoadControl(TreeView mainTreeView)
		{
			TreeNode rootNode = new TreeNode();
			C1ItemInfo itemInfo = ItemsInfo[0];
			string nodeText = _c1ComboBox.ID ?? itemInfo.DefaultNodeText;
			C1NodeInfo nodeTag = new C1NodeInfo(_c1ComboBox, itemInfo, nodeText);

			mainTreeView.BeginUpdate();
			mainTreeView.Nodes.Clear();

			SetNodeAttributes(rootNode, nodeTag);

			foreach (C1ComboBoxColumn item in _c1ComboBox.Columns)
			{
				TreeNode itemNode = new TreeNode();
				nodeText = item.Name;
				//if (item is C1ComboBoxBindColumn)
				//{
				//    itemInfo = ItemsInfo[2];
				//}
				//else
				//{
				//    itemInfo = ItemsInfo[1];
				//}
				nodeTag = new C1NodeInfo(item, itemInfo, nodeText);
				SetNodeAttributes(itemNode, nodeText, nodeTag);
				rootNode.Nodes.Add(itemNode);
			}

			mainTreeView.Nodes.Add(rootNode);
			mainTreeView.SelectedNode = mainTreeView.Nodes[0];
			mainTreeView.ExpandAll();
			mainTreeView.EndUpdate();
		}

		/// <summary>
		/// Updates status of controls of Maincombo, MainMenu and context menus.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo of current selected item.</param>
		/// <param name="parentItemInfo">C1ItemInfo of parent of current selected item.</param>
		/// <param name="previousItemInfo">C1ItemInfo of previous item of current selected item</param>
		protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
		{
			bool atRoot = parentItemInfo == null;
			base.AllowMoveUp = previousItemInfo != null;
			base.AllowMoveDown = true;
			base.AllowMoveLeft = false;
			base.AllowMoveRight = false;
			base.AllowAdd = atRoot;
			base.AllowInsert = !atRoot;
			base.AllowChangeType = false;
			base.AllowCopy = !atRoot;
			base.AllowPaste = !atRoot;
			base.AllowCut = !atRoot;
			base.AllowDelete = !atRoot;
			base.AllowRename = !atRoot;
			base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
		}

		/// <summary>
		/// Creates the C1NodeInfo object that will contain both the specific C1 control tab depending on the itemInfo
		/// and given itenInfo.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo object.</param>
		/// <param name="nodesList">Nodes that currently exist into the list where the new created item will be inserted.</param>
		/// <returns>Returns a C1 control that depends on the C1ItemInfo. Given nodesList
		/// could be used to obtain a new numbered name.</returns>
		protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
		{
			string nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);
			C1NodeInfo nodeInfo = null;
			if (itemInfo.DefaultNodeText == "C1ComboBoxColumn")
			{
				C1ComboBoxColumn item = new C1ComboBoxColumn();
				item.Name = nodeText;
				nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);
			}
			else
			{
				//C1ComboBoxBindColumn item = new C1ComboBoxBindColumn();
				//item.HeaderText = nodeText;
				//nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);
			}
			// set nodeText property
			nodeInfo.NodeText = nodeText;

			return nodeInfo;
		}

		/// <summary>
		///  Indicates when an combo item was inserted in a specific position.
		/// </summary>
		/// <param name="item">The inserted column.</param>
		/// <param name="destinationItem">The destination tab that will contain inserted tab.</param>
		/// <param name="destinationIndex">The index position of given tab within destinationItem items collection.</param>
		protected override void Insert(object item, object destinationItem, int destinationIndex)
		{
			try
			{
				_c1ComboBox.Columns.Insert(destinationIndex, item as C1ComboBoxColumn);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// <summary>
		///  Indicates when an combo item was deleted.
		/// </summary>
		/// <param name="item">The deleted combo item.</param>
		protected override void Delete(object item)
		{
			try
			{
				_c1ComboBox.Columns.Remove(item as C1ComboBoxColumn);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// Indicates when an combo item was copyed to a specific position.
		/// </summary>
		/// <param name="item">The item to copy.</param>
		/// <param name="itemInfo">The item info of the item to be copied.</param>
		protected override void Copy(object item, C1ItemInfo itemInfo)
		{
			if (_clipboardData == null)
			{
				_clipboardData = new MemoryStream();
			}

			Copy(_clipboardData, item);

			_clipboardType = itemInfo;
			// tell to base there is som data into clipboard
			ClipboardData = true;
		}

		/// <summary>
		/// Copy the current target to the buffer.
		/// </summary>
		/// <param name="buffer">The current buffer.</param>
		/// <param name="target">The specified targert that will be copied to buffer.</param>
		private static void Copy(Stream buffer, object target)
		{
			C1ComboBoxSerializer serializer = new C1ComboBoxSerializer(target);

			buffer.SetLength(0);
			serializer.SaveLayout(buffer);
		}


		/// <summary>
		/// Pastes node from clipboard to given destination node.
		/// </summary>
		/// <param name="destinationNode">Destination node.</param>
		protected override void Paste(TreeNode destinationNode)
		{
			Paste(_clipboardData, destinationNode);
		}

		/// <summary>
		/// Paste the current buffer value to the specified destination node.
		/// </summary>
		/// <param name="buffer">The current buffer value.</param>
		/// <param name="destNode">The specified destination node that will be pasted.</param>
		private void Paste(Stream buffer, TreeNode destNode)
		{
			C1ComboBoxColumn item;
			if (_clipboardType.DefaultNodeText=="C1ComboBoxColumn" )
			{
				item = new C1ComboBoxColumn();

			}
			else
			{
				item = new C1ComboBoxColumn();// C1ComboBoxBindColumn();
			}
			C1ComboBoxSerializer serializer = new C1ComboBoxSerializer(item);
			buffer.Seek(0, SeekOrigin.Begin);
			// lets to serializer to retrieve data from clipboard
			serializer.LoadLayout(buffer, LayoutType.All);

			C1ComboBoxColumn column = ((C1NodeInfo) destNode.Tag).Element as C1ComboBoxColumn;
			// add deserialized item
			_c1ComboBox.Columns.Insert(_c1ComboBox.Columns.IndexOf(column), item);

			// create node info
			C1NodeInfo nodeInfo = new C1NodeInfo(item, _clipboardType, item.Name);

			// create new treenode to be added on destination node
			TreeNode node = new TreeNode();
			// set node text and node tag
			SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);

			destNode.Parent.Nodes.Insert(destNode.Index +1, node);
			destNode.ExpandAll();
		}

		/// <summary>
		/// Shows current control into preview tab.
		/// </summary>
		/// <param name="previewer">The web browser previewer</param>
		internal override void ShowPreviewHtml(C1WebBrowser previewer)
		{
			try
			{
				string sResultBodyContent = "";
				StringBuilder sb = new StringBuilder();
				StringWriter tw = new StringWriter(sb);
				HtmlTextWriter htw = new HtmlTextWriter(tw);
				_c1ComboBox.RenderControl(htw);
				sResultBodyContent = sb.ToString();
				string sDocumentContent = "";
				sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + Environment.NewLine;
				sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + Environment.NewLine;
				sDocumentContent += "<head>" + Environment.NewLine;
				sDocumentContent += "</head>" + Environment.NewLine;
				sDocumentContent += "<body>" + Environment.NewLine;
				sDocumentContent += sResultBodyContent + Environment.NewLine;
				sDocumentContent += "</body>" + Environment.NewLine;
				sDocumentContent += "</html>" + Environment.NewLine;
				if (previewer.Document != null)
				{
					previewer.DocumentWrite(sDocumentContent, this._c1ComboBox);
				}
				
			}
			catch (Exception ex)
			{
				MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
			}
		}

		/// <summary>
		/// Saves combo control to a XML file.
		/// </summary>
		/// <param name="fileName">The name for XML file.</param>
		protected override void SaveToXML(string fileName)
		{
			try
			{
				_c1ComboBox.SaveLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		/// <summary>
		/// Fills combo control from a saved XML file. 
		/// </summary>
		/// <param name="fileName">The name of the XML file to be loaded.</param>
		protected override void LoadFromXML(string fileName)
		{
			try
			{
				_c1ComboBox.LoadLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		/// <summary>
		/// Performs an action after treeview label edition.
		/// </summary>
		/// <param name="e">Node label edit event arguments.</param>
		/// <param name="selectedNode">Current selected node.</param>
		protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
		{
			if (e.Label == null)
			{
				return;
			}
			bool cancel = e.CancelEdit;
			FinishLabelEdit(e.Node, e.Label, ref cancel);
			e.CancelEdit = cancel;
		}

		/// <summary>
		/// Finish to set the text of the node.
		/// </summary>
		/// <param name="node">The specified node.</param>
		/// <param name="text">The specified text value.</param>
		/// <param name="cancelEdit">Indicates whether we will cancel the current edit action.</param>
		private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
		{
			string nodeText = text;
			string property = "Name";
			object obj = ((C1NodeInfo)node.Tag).Element;

			// set new text value to selected object and property grid
			TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
			base.RefreshPropertyGrid();
			node.Text = nodeText;
		}

		/// <summary>
		/// Performs some action when a property value changes.
		/// </summary>
		/// <param name="changedGridItem">The grid item changed.</param>
		/// <param name="selectedNode">Current selected node.</param>
		protected override void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
		{
			if (changedGridItem.PropertyDescriptor.Name == "Name")
				if (selectedNode.Text != changedGridItem.Value.ToString())
					selectedNode.Text = changedGridItem.Value.ToString();
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// C1ComboBoxDesignerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(492, 516);
			this.Name = C1Localizer.GetString("C1ComboBox.ColumnsForm.Title");
			this.Text = C1Localizer.GetString("C1ComboBox.ColumnsForm.Title");
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		#endregion
	}
}
