﻿#region using

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Web.UI.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

#endregion

namespace C1.Web.Wijmo.Controls.Design.C1ComboBox
{
    internal class DataFieldEditor : UITypeEditor
    {
        private IServiceProvider _serviceProvider;

        //private IComponent CurrentComponent
        //{
        //    get
        //    {
        //        ISelectionService iselectionService = (ISelectionService) _serviceProvider.GetService(typeof (ISelectionService));
        //        if (iselectionService == null)
        //            return null;
        //        return (IComponent) iselectionService.PrimarySelection;
        //    }
        //}

        /// <summary>
        /// EditValue
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            _serviceProvider = provider;
            DisplayClass displayClass = new DisplayClass();
            displayClass.Value = value;
            DataFieldConverter dataFieldConverter = new DataFieldConverter();
            C1ComboBoxActionList l = context.Instance as C1ComboBoxActionList;
            IComponent c = null;
            if (l != null)
            {
                c = l._component;
            }
            TypeConverter.StandardValuesCollection standardValuesCollection = dataFieldConverter.GetStandardValues(new TypeDescriptorContext(c));
            displayClass.ListBox = new ListBox();
            displayClass.ListBox.BorderStyle = BorderStyle.None;
            foreach (object obj in standardValuesCollection)
            {
                displayClass.ListBox.Items.Add(obj);
            }
            displayClass.ListBox.SelectedValue = displayClass.Value;
            displayClass.EditorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            displayClass.ListBox.SelectedIndexChanged += displayClass.SelectionChanged;
            displayClass.EditorService.DropDownControl(displayClass.ListBox);
            return base.EditValue(context, provider, displayClass.Value);
        }


        /// <summary>
        /// GetEditStyle
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        #region Nested type: DisplayClass

        private class DisplayClass
        {
            public IWindowsFormsEditorService EditorService;
            public ListBox ListBox;
            public object Value;

            public void SelectionChanged(object sender, EventArgs e)
            {
                Value = ListBox.Text;
                EditorService.CloseDropDown();
            }
        }

        #endregion
    }

    internal class TypeDescriptorContext : ITypeDescriptorContext
    {
        private readonly IComponent _component;

        public TypeDescriptorContext(IComponent component)
        {
            _component = component;
        }

        #region ITypeDescriptorContext Members

        public IContainer Container
        {
            get
            {
                ISite isite = _component.Site;
                if (isite != null)
                    return isite.Container;
                return null;
            }
        }

        public object Instance
        {
            get
            {
                return _component;
            }
        }

        public PropertyDescriptor PropertyDescriptor
        {
            get
            {
                return null;
            }
        }

        public object GetService(Type serviceType)
        {
            if (_component.Site == null)
                return null;
            return _component.Site.GetService(serviceType);
        }

        public void OnComponentChanged()
        {
        }

        public bool OnComponentChanging()
        {
            return true;
        }

        #endregion
    }
}