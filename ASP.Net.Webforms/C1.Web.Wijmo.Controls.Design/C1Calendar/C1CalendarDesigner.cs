using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.IO;
using System.Globalization;


namespace C1.Web.Wijmo.Controls.Design.C1Calendar
{
    using C1.Web.Wijmo.Controls.C1Calendar;
    using C1.Web.Wijmo.Controls.Design.Localization;


    [SupportsPreviewControl(true)]
    public class C1CalendarDesigner : C1ControlDesinger
    {
        #region Fields

        private C1Calendar _calendar;
        internal DesignerActionUIService _designerActionUISvc = null;

        #endregion

        #region Constructor

        public C1CalendarDesigner()
        {
        }

        #endregion

        public override string GetDesignTimeHtml()
        {
            if (_calendar.WijmoControlMode == WijmoControlMode.Mobile)
            {
                //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
            }
            return base.GetDesignTimeHtml();
        }

        protected override bool UsePreviewControl
        {
            get
            {
                return true;
            }
        }

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this._calendar = (C1Calendar)component;

            if (_calendar.Page == null && RootDesigner != null)
            {
                var page = RootDesigner.Component as Page;
                if (page != null)
                {
                    _calendar.Page = page;
                }
            }

            // turn on template editing
            SetViewFlags(ViewFlags.TemplateEditing, true);
            this._designerActionUISvc = GetService(typeof(DesignerActionUIService)) as DesignerActionUIService;
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();
                lists.AddRange(base.ActionLists);
                lists.Add(new C1CalendarActionList(this));
                return lists;
            }
        }

        public override bool AllowResize
        {
            get { return false; }
        }

        protected override string GetEmptyDesignTimeHtml()
        {
            return CreatePlaceHolderDesignTimeHtml("ComponentOne Calendar Control");
        }

        protected override string GetErrorDesignTimeHtml(Exception e)
        {
            return CreatePlaceHolderDesignTimeHtml(e.Message);
        }
    }

    internal class C1CalendarActionList : DesignerActionListBase
    {
        #region Fields

        private C1Calendar _calendar;

        #endregion

        #region Constructor

        public C1CalendarActionList(C1CalendarDesigner designer)
            : base(designer)
        {
            this._calendar = designer.Component as C1Calendar;
        }

        #endregion

        #region Action Lists

        [DefaultValue(true)]
        public bool ShowOtherMonthDays
        {
            get { return _calendar.ShowOtherMonthDays; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["ShowOtherMonthDays"];
                prop.SetValue(_calendar, value);
            }
        }

        [DefaultValue(typeof(CultureInfo), "en-US")]
        public CultureInfo Culture
        {
            get { return _calendar.Culture; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["Culture"];
                prop.SetValue(_calendar, value);
            }
        }

        [DefaultValue(WeekDayFormat.FirstLetter)]
        public WeekDayFormat WeekDayFormat
        {
            get { return _calendar.WeekDayFormat; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["WeekDayFormat"];
                prop.SetValue(_calendar, value);
            }
        }

        [DefaultValue(false)]
        public bool ShowDayPadding
        {
            get { return _calendar.ShowDayPadding; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["ShowDayPadding"];
                prop.SetValue(_calendar, value);
            }
        }

        [DefaultValue(false)]
        public bool AllowPreview
        {
            get { return _calendar.AllowPreview; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["AllowPreview"];
                prop.SetValue(_calendar, value);
            }
        }

        [DefaultValue(true)]
        public bool ShowWeekDays
        {
            get { return _calendar.ShowWeekDays; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["ShowWeekDays"];
                prop.SetValue(_calendar, value);
            }
        }

        [DefaultValue(true)]
        public bool ShowWeekNumbers
        {
            get { return _calendar.ShowWeekNumbers; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_calendar)["ShowWeekNumbers"];
                prop.SetValue(_calendar, value);
            }
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection actions = new DesignerActionItemCollection();
            actions.Add(new DesignerActionPropertyItem("ShowOtherMonthDays", C1Localizer.GetString("C1Calendar.ShowOtherMonthDays"), "Display"));
            actions.Add(new DesignerActionPropertyItem("ShowWeekDays", C1Localizer.GetString("C1Calendar.ShowWeekdays"), "Display"));
            actions.Add(new DesignerActionPropertyItem("ShowWeekNumbers", C1Localizer.GetString("C1Calendar.ShowWeekNumbers"), "Display"));
            actions.Add(new DesignerActionPropertyItem("WeekDayFormat", C1Localizer.GetString("C1Calendar.WeekdayFormat"), "Display"));
            actions.Add(new DesignerActionPropertyItem("AllowPreview", C1Localizer.GetString("C1Calendar.AllowPreview"), "Behavior"));

            this.AddBaseSortedActionItems(actions);

            return actions;
        }

        #endregion
    }
}
