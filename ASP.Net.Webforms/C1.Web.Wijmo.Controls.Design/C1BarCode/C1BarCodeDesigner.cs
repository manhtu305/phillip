﻿using System.Web.UI.Design;
using System.Drawing;

namespace C1.Web.Wijmo.Controls.Design.C1BarCode
{
    using C1.Web.Wijmo.Controls.C1BarCode;
    using System.ComponentModel;

    using System.ComponentModel.Design;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using System;

    public class C1BarCodeDesigner : C1ControlDesinger
    {
        #region ** fields

        C1BarCode _barCode;
        private DesignerActionListCollection _actions;

        #endregion

        #region ** contructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1BarCodeDesigner"/> class.
		/// </summary>

        #endregion

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            if (component != null)
            {
                SetViewFlags(ViewFlags.CustomPaint, true);
                this._barCode = component as C1BarCode;
            }
        }

        public override DesignerActionListCollection ActionLists 
        {
            get
            {
                if (_actions == null)
                {
                    _actions = new DesignerActionListCollection();
                    _actions.AddRange(base.ActionLists);
                    _actions.Add(new C1BarCodeActionList(this));
                }
                return _actions;
            }
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            if (this._barCode.BarCodeImage != null) {
                e.Graphics.DrawImage(this._barCode.BarCodeImage, new Point(0, 0));
            }
        }

        public override string GetDesignTimeHtml()
        {
            if (this._barCode.BarCodeImage == null)
            {
                return string.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", C1Localizer.GetString("C1BarCode.Empty"));
            }
            return base.GetDesignTimeHtml();
        }
    }

    internal class C1BarCodeActionList : DesignerActionListBase
    {
        #region Fields

        private C1BarCode _barCode;
        private DesignerActionItemCollection _actions;

        #endregion

        #region Constructor

        public C1BarCodeActionList(C1BarCodeDesigner designer)
            : base(designer)
        {
            _barCode = designer.Component as C1BarCode;
        }

        #endregion

        #region Properties

        public WijmoCodeTypeEnum CodeType
        {
            get
            {
                return (WijmoCodeTypeEnum)GetProperty(_barCode, "CodeType").GetValue(_barCode);
            }
            set
            {
                SetProperty("CodeType", value);
            }
        }

        public string Text
        {
            get
            {
                return (string)GetProperty(_barCode, "Text").GetValue(_barCode);
            }
            set
            {
                SetProperty("Text", value);
            }
        }

        public bool ShowText
        {
            get
            {
                return (bool)GetProperty(_barCode, "ShowText").GetValue(_barCode);
            }
            set
            {
                SetProperty("ShowText", value);
            }
        }

        public WijmoBarDirectionEnum BarDirection
        {
            get
            {
                return (WijmoBarDirectionEnum)GetProperty(_barCode, "BarDirection").GetValue(_barCode);
            }
            set
            {
                SetProperty("BarDirection", value);
            }
        }

        #endregion

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_actions == null)
            {
                _actions = new DesignerActionItemCollection();
                AddBaseSortedActionItems(_actions);
                _actions.Add(new DesignerActionPropertyItem("CodeType", C1Localizer.GetString("C1BarCode.SmartTag.CodeType", "CodeType"), "CodeType", C1Localizer.GetString("C1BarCode.SmartTag.CodeTypeDescription")));
                _actions.Add(new DesignerActionPropertyItem("Text", C1Localizer.GetString("C1BarCode.SmartTag.Text", "Text"), "Text", C1Localizer.GetString("C1BarCode.SmartTag.TextDescription")));
                _actions.Add(new DesignerActionPropertyItem("ShowText", C1Localizer.GetString("C1BarCode.SmartTag.ShowText", "ShowText"), "ShowText", C1Localizer.GetString("C1BarCode.SmartTag.ShowTextDescription")));
                _actions.Add(new DesignerActionPropertyItem("BarDirection", C1Localizer.GetString("C1BarCode.SmartTag.BarDirection", "BarDirection"), "BarDirection", C1Localizer.GetString("C1BarCode.SmartTag.BarDirectionDescription")));
            }
            return _actions;
        }

        internal override bool DisplayThemeSupport()
        {
            return false;
        }
    }
}
