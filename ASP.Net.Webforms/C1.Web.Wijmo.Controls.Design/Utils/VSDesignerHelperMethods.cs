using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.Design;
using System.ComponentModel;
using System.Reflection;
using System.ComponentModel.Design;
using System.IO;
using System.Web.UI;
using System.Collections;
using System.Diagnostics;

namespace C1.Web.Wijmo.Controls.Design.Utils
{
    /// <summary>
    /// Contains static methods to work with VisualStudio IDE.
    /// </summary>
    public class VSDesignerHelperMethods
    {

        /// <summary>
        /// Add ScriptBlock to ActiveDocument.
        /// </summary>
        /// <param name="designer"></param>
        /// <param name="block"></param>
        public static void AddScriptBlockToDocument(ControlDesigner designer, string block)
        {
            WebFormsRootDesigner designer1 = GetWebFormsRootDesigner(designer);
            if (designer1 != null)
            {
                ClientScriptItemCollection clientScripts = designer1.GetClientScriptsInDocument();
                //Add comments by RyanWu@20090307.
                //For fixing the issue#2927.
                //In this case, WebFormsRootDesigner can't remove the client script from the document and throw an Catastrophic failure.
                //So we will not add the script text to the new script block.
                //designer1.RemoveClientScriptFromDocument("ComponentOneClientScript");
                ArrayList c1ScriptsArray = new ArrayList();
                int c1ScriptId = 1;
                if (clientScripts != null)
                {
                    foreach (ClientScriptItem scriptItem in clientScripts)
                    {
                        //Nov 9,2009 by Willow Yang for #7752.
                        //if (scriptItem.Id.IndexOf("ComponentOneClientScript") > -1)
                        if (!String.IsNullOrEmpty(scriptItem.Id) && scriptItem.Id.IndexOf("ComponentOneClientScript") > -1)
                        {
                            c1ScriptsArray.Add(scriptItem.Id.Replace("ComponentOneClientScript", ""));
                        }
                    }
                }
                if (c1ScriptsArray.Count > 0)
                {
                    for (int i = 0; i < c1ScriptsArray.Count; i++)
                    {
                        if (c1ScriptsArray.Contains(c1ScriptId.ToString()))
                        {
                            c1ScriptId++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                designer1.AddClientScriptToDocument(new ClientScriptItem("\n" + block, string.Empty, string.Empty, "text/javascript", "ComponentOneClientScript" + c1ScriptId.ToString()));
                #region 
                //ClientScriptItem prevScriptItem = null;
                //if (clientScripts != null)
                //{
                //    foreach (ClientScriptItem scriptItem in clientScripts)
                //    {
                //        if (scriptItem.Id == "ComponentOneClientScript")
                //        {
                //            prevScriptItem = scriptItem;
                //            break;
                //        }
                //    }
                //}
                //string text1 = "";

                //if (prevScriptItem != null)
                //{
                //    if (prevScriptItem.Text != null)
                //    {
                //        text1 = prevScriptItem.Text.Replace("\n<!--\n", "").Replace("// -->\n", "");
                //    }

                //    try
                //    {
                //        designer1.RemoveClientScriptFromDocument("ComponentOneClientScript");
                //    }
                //    catch
                //    {
                //        designer1.AddClientScriptToDocument(new ClientScriptItem("\n" + block, string.Empty, string.Empty, "text/javascript", string.Empty));
                //        return;
                //    }
                //}
                //string text2 = "\n" + text1 + block + "";
                //designer1.AddClientScriptToDocument(new ClientScriptItem(text2, string.Empty, string.Empty, "text/javascript", "ComponentOneClientScript"));
                #endregion
                //end by RyanWu@20090307.
            }
        }

        /// <summary>
        /// Returns collection of the ClientScriptItem registered within current document.
        /// </summary>
        /// <returns></returns>
        public static ClientScriptItemCollection GetClientScriptsInDocument(ControlDesigner designer)
        {
            WebFormsRootDesigner rootDesigner = GetWebFormsRootDesigner(designer);
            if (rootDesigner == null)
            {
                return null;
            }
            return rootDesigner.GetClientScriptsInDocument();
        }

        /// <summary>
        /// Returns WebForms root designer.
        /// </summary>
        /// <param name="designer">System.Web.UI.Design.ControlDesigner</param>
        /// <returns>instance of WebFormsRootDesigner</returns>
        public static WebFormsRootDesigner GetWebFormsRootDesigner(ControlDesigner designer)
        {
            if (((designer != null) && (designer.Component != null)) && (designer.Component.Site != null))
            {
                IDesignerHost host1 = designer.Component.Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
                if ((host1 != null) && (host1.RootComponent != null))
                {
                    return (host1.GetDesigner(host1.RootComponent) as WebFormsRootDesigner);
                }
            }
            return null;
        }

        /// <summary>
        /// Switch to HtmlView and select javascript function.
        /// </summary>
        /// <param name="provider">IServiceProvider</param>
        /// <param name="function">name of the javascript function to move caret to</param>
        /// <param name="comment">comment inside function to select</param>
        public static bool SwitchToHtmlViewAndSelectFunction(IServiceProvider provider, string function, string comment)
        {
            BindingFlags flagsGetProperty = BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance;
            BindingFlags flagsInvokeMethod = BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance;

            Assembly envdteAssembly = null;
            object oEnvDTEService = null;
            object oActiveDocument = null;
            object oActiveWindow = null;
            object oActiveWindowObject = null;
            Enum enumHTMLTabs = null;
            object oHTMLTabsSource = null;

            if (!GetEnvDteActiveDocumentMembers(provider, out envdteAssembly,
                out oEnvDTEService, out oActiveDocument, out oActiveWindow,
                out oActiveWindowObject, out enumHTMLTabs, out oHTMLTabsSource))
            {
                return false;
            }

            Type tCurType = envdteAssembly.GetType("EnvDTE.HTMLWindow");
            if (tCurType != null)
            {
                tCurType.GetProperty("CurrentTab").SetValue(oActiveWindowObject, oHTMLTabsSource, new object[0]);
            }
            tCurType = envdteAssembly.GetType("EnvDTE.Document");
            object oDocumentSelection = tCurType.GetProperty("Selection").GetValue(oActiveDocument, new object[0]);
            if (oDocumentSelection != null)
            {
                tCurType = envdteAssembly.GetType("EnvDTE.TextSelection");
                if (tCurType != null)
                {
                    tCurType.InvokeMember("MoveToLineAndOffset", BindingFlags.InvokeMethod, null, oDocumentSelection, new object[] { 1, 1, false });
                    tCurType.InvokeMember("FindText", BindingFlags.InvokeMethod, null, oDocumentSelection, new object[] { function, 0 });
                    tCurType.InvokeMember("Collapse", BindingFlags.InvokeMethod, null, oDocumentSelection, new object[0]);
                    tCurType.InvokeMember("FindText", BindingFlags.InvokeMethod, null, oDocumentSelection, new object[] { comment, 0 });
                    object oParentWindow = envdteAssembly.GetType("EnvDTE.HTMLWindow").InvokeMember("Parent", flagsGetProperty, null, oActiveWindowObject, new object[0]);
                    tCurType = envdteAssembly.GetType("EnvDTE.Window");
                    if (oParentWindow != null)
                    {
                        tCurType.InvokeMember("Activate", flagsInvokeMethod, null, oParentWindow, new object[0]);
                    }
                }
            }
            return true;

        }

        /// <summary>
        /// Update Web Control designer.
        /// </summary>
        /// <param name="control">WebControl</param>
        /// <param name="propName">property name</param>
        /// <returns></returns>
        public static void UpdateDesigner(object control, string propName)
        {
            UpdateDesigner(control, new string[] { propName });
        }

        /// <summary>
        /// Update Web Control designer.
        /// </summary>
        /// <param name="control">WebControl</param>
        /// <param name="props">array with properties names</param>
        public static void UpdateDesigner(object control, string[] props)
        {
            if (props == null)
                return;

            ISite site1 = (control is IComponent) ? ((IComponent)control).Site : null;
            if (site1 == null)
            {
                return;
            }
            object obj1 = null;
            object obj2 = site1.GetService(typeof(IDesignerHost));
            if (obj2 is IDesignerHost)
            {
                obj1 = ((IDesignerHost)obj2).GetService(typeof(IComponentChangeService));
                obj2 = ((IDesignerHost)obj2).GetDesigner((IComponent)control);
            }
            if (!(obj2 is ControlDesigner))
            {
                return;
            }
            if (obj1 is IComponentChangeService)
            {
                for (int i = 0; i < props.Length; i++)
                {
                    string propName = props[i];
                    MemberDescriptor descriptor1 = (propName == null) ? null : ((MemberDescriptor)TypeDescriptor.GetProperties(control)[propName]);
                    ((IComponentChangeService)obj1).OnComponentChanged((IComponent)control, descriptor1, null, null);
                }
            }
        }


		internal static string GetTemplateContent(System.Web.UI.Control control, string templatePropertyName)
		{
			if (control == null)
				return "";
			PropertyDescriptor prop = TypeDescriptor.GetProperties(control)[templatePropertyName];
			if (prop != null)
			{
				ITemplate iTemplate = (ITemplate)prop.GetValue(control);
				if (iTemplate == null)
					return "";
				return GetTemplateContent(control, iTemplate);
			}
			return "";
		}

		internal static string GetTemplateContent(System.Web.UI.Control control, ITemplate template)
		{
			DesignerPanel contentPanel = new DesignerPanel();
			contentPanel.ID = "ContainerControl";

			template.InstantiateIn(contentPanel);
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost), control);

			Debug.Assert(host != null, "Failed to get IDesignerHost?!?");

			StringBuilder persistedControl = new StringBuilder(1024);
			foreach (System.Web.UI.Control c in contentPanel.Controls)
			{
				persistedControl.Append(System.Web.UI.Design.ControlPersister.PersistControl(c, host));
			}
			return persistedControl.ToString();
		}

		internal static string GetControlContent(System.Web.UI.Control control)
		{
			if (control == null)
				return "";
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost), control);
			StringBuilder persistedControl = new StringBuilder(1024);
			foreach (System.Web.UI.Control c in control.Controls)
			{
				persistedControl.Append(System.Web.UI.Design.ControlPersister.PersistControl(c, host));
			}
			return persistedControl.ToString();

		}

		internal static void SetTemplateContent(System.Web.UI.Control control, string templatePropertyName, string newContent)
		{
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost), control);
			ITemplate template = System.Web.UI.Design.ControlParser.ParseTemplate(host, newContent);
			PersistTemplate(control, host, template, templatePropertyName);
		}

		internal static ISite GetControlSite(System.Web.UI.Control _control)
		{
			if (_control.Site != null) return _control.Site;

			for (System.Web.UI.Control control = _control.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}

		internal static object GetService(Type serviceType, System.Web.UI.Control control)
		{
			IServiceProvider serviceProvider = GetControlSite(control);
			return serviceProvider.GetService(serviceType);
		}

        #region --- private helper methods ---

        private static bool GetEnvDteActiveDocumentMembers(IServiceProvider provider, out Assembly envdteAssembly, out object oEnvDTEService, out object oActiveDocument, out object oActiveWindow, out object oActiveWindowObject, out Enum enumHTMLTabs, out object oHTMLTabsSource)
        {
            try
            {
                BindingFlags flags1 = BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance;

                envdteAssembly = Assembly.Load("envdte");//LoadWithPartialName

                oEnvDTEService = provider.GetService(envdteAssembly.GetType("EnvDTE.DTE"));

                Type tCurType = envdteAssembly.GetType("EnvDTE.DTE");
                oActiveDocument = tCurType.InvokeMember("ActiveDocument", flags1, null, oEnvDTEService, new object[0]);

                tCurType = envdteAssembly.GetType("EnvDTE.Document");
                oActiveWindow = tCurType.InvokeMember("ActiveWindow", flags1, null, oActiveDocument, new object[0]);

                tCurType = envdteAssembly.GetType("EnvDTE.Window");
                oActiveWindowObject = tCurType.InvokeMember("Object", flags1, null, oActiveWindow, new object[0]);

                tCurType = envdteAssembly.GetType("EnvDTE.vsHTMLTabs");
                enumHTMLTabs = envdteAssembly.CreateInstance("EnvDTE.vsHTMLTabs", false) as Enum;

                oHTMLTabsSource = Enum.Parse(tCurType, "vsHTMLTabsSource", false);
            }
            catch (Exception)
            {
                provider = null;
                envdteAssembly = null;
                oEnvDTEService = null;
                oActiveDocument = null;
                oActiveWindow = null;
                oActiveWindowObject = null;
                enumHTMLTabs = null;
                oHTMLTabsSource = null;
                return false;
            }
            return true;
        }

		/// <summary>
		/// Helper method to save the value of a template.  This sets up all the right Undo state.
		/// </summary>
		/// <param name="panel"></param>
		/// <param name="host"></param>
		/// <param name="template"></param>
		/// <param name="propertyName"></param>
		private static void PersistTemplate(System.Web.UI.Control panel, IDesignerHost host, ITemplate template, string propertyName)
		{
			PropertyDescriptor descriptor = TypeDescriptor.GetProperties(panel)[propertyName];
			using (DesignerTransaction transaction = host.CreateTransaction("SetEditableDesignerRegionContent"))
			{
				descriptor.SetValue(panel, template);
				transaction.Commit();
			}
		}

        #endregion


    }

	/// <summary>
	/// Simple class to use for template instantiation
	/// </summary>
	internal class DesignerPanel : System.Web.UI.WebControls.Panel, INamingContainer
	{
	}
}
