using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.IO;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using C1.Web.Wijmo;


namespace C1.Web.Wijmo.Controls.Design.Utils
{
    /// <summary>
    /// A class of common properties and methods.
    /// </summary>
    public class Common
    {
        private static readonly Guid DesignTimeEnvironmentCLSID = new Guid(0x4a72314, 0x32e9, 0x48e2, 0x9b, 0x87, 0xa6, 0x36, 3, 0x45, 0x4f, 0x3e);
        private static bool isVs2008 = false;
        private static bool isVs2010 = false;

        /// <summary>
        /// Gets whether the current IDE is VS2008.
        /// </summary>
        public static bool IsVS2008
        {
            get
            {
                if (!isVs2008)
                {
                    string fileName = AppDomain.CurrentDomain.BaseDirectory + "devenv.exe";

                    try
                    {
                        System.Diagnostics.FileVersionInfo fversinfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(fileName);

                        if (fversinfo.FileVersion.StartsWith("9"))
                        {
                            isVs2008 = true;
                        }
                    }
                    catch
                    {
                    }
                }

                return isVs2008;
            }
        }

        /// <summary>
        /// Gets whether the current IDE is VS2010.
        /// </summary>
        public static bool IsVS2010
        {
            get
            {
                if (!isVs2010)
                {
                    string fileName = AppDomain.CurrentDomain.BaseDirectory + "devenv.exe";

                    try
                    {
                        System.Diagnostics.FileVersionInfo fversinfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(fileName);

                        if (fversinfo.FileVersion.StartsWith("10"))
                        {
                            isVs2010 = true;
                        }
                    }
                    catch
                    {
                    }
                }

                return isVs2010;
            }
        }

        /// <summary>
        /// Gets active document path
        /// </summary>
        /// <param name="provider"></param>
        /// <returns>A string of active document path</returns>
        [System.ComponentModel.EditorBrowsable(EditorBrowsableState.Never)]
        public static string GetActiveDocumentPath(IServiceProvider provider)
        {
            BindingFlags flags1 = BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance;
            Type type1 = Type.GetTypeFromCLSID(DesignTimeEnvironmentCLSID, false);
            if (type1 != null)
            {
                object obj1 = provider.GetService(type1);
                if (obj1 != null)
                {
                    object obj2 = type1.InvokeMember("ActiveDocument", flags1, null, obj1, new object[0]);
                    if (obj2 != null)
                    {
                        object obj3 = obj2.GetType().InvokeMember("Path", flags1, null, obj2, new object[0]);
                        return (obj3 as string);
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets site of webcontrol
        /// </summary>
        /// <param name="webControl">webcontrol</param>
        /// <returns>site</returns>
        public static ISite GetSite(System.Web.UI.Control webControl)
        {
            if (webControl.Site != null) return webControl.Site;
            for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
            {
                if (control.Site != null) return control.Site;
            }
            return null;
        }

        /// <summary>
        /// Gets resolved physical path.
        /// </summary>
        /// <param name="control">control</param>
        /// <param name="url">url</param>
        /// <returns>resolved physical path</returns>
        public static string ResolvePhysicalPath(System.Web.UI.Control control, string url)
        {

            ISite site = GetSite(control);
            if (site == null) return url;

            IWebApplication service = (IWebApplication)site.GetService(typeof(IWebApplication));
            IProjectItem item = service.GetProjectItemFromUrl(url);
            if (item == null)
                return url.Replace("~/", service.RootProjectItem.PhysicalPath);

            string path = item.PhysicalPath;
            return path;
        }

        /// <summary>
        /// Gets a value that indicates whether the control is in the design time environment.
        /// </summary>
        public static bool IsDesignMode { get { return (System.Web.HttpContext.Current == null); } }

        /// <summary>
        /// Converts a FontUnit to Pixels.
        /// </summary>
        /// <param name="unitSize">The FontUnit to convert.</param>
        /// <returns>The pixel representation of the FontUnit.</returns>
        public static float FontUnitToPixels(FontUnit unitSize)
        {
            float result = 16;

            if (unitSize.Type != FontSize.NotSet && unitSize.Type != FontSize.AsUnit)
            {
                // size in x-small, etc
                switch (unitSize.Type)
                {
                    case FontSize.XXSmall:
                        result = 9;
                        break;
                    case FontSize.XSmall:
                        result = 10;
                        break;
                    case FontSize.Smaller:
                    case FontSize.Small:
                        result = 13;
                        break;
                    case FontSize.Medium:
                        result = 16;
                        break;
                    case FontSize.Large:
                    case FontSize.Larger:
                        result = 18;
                        break;
                    case FontSize.XLarge:
                        result = 24;
                        break;
                    case FontSize.XXLarge:
                        result = 32;
                        break;
                    default:
                        result = 16;
                        break;
                }
            }
            else if (unitSize.Type == FontSize.AsUnit)
            {
                switch (unitSize.Unit.Type)
                {
                    case UnitType.Pixel:
                        result = (int)Math.Round(unitSize.Unit.Value);
                        break;
                    case UnitType.Point:
                        result = (int)Math.Round(unitSize.Unit.Value * 1.33);
                        break;
                    case UnitType.Em:
                        result = (int)Math.Round(unitSize.Unit.Value * 16);
                        break;
                    case UnitType.Percentage:
                        result = (int)Math.Round(unitSize.Unit.Value * 16 / 100);
                        break;
                    default:
                        // other types are not supported. just return the medium
                        result = 16;
                        break;
                }
            }

            return result;
        }

        static double DefaultScreenDPI = 96;

        /// <summary>
        /// Parses unit to pixel
        /// </summary>
        /// <param name="val">Unit value</param>
        /// <returns>Pixel value</returns>
        static public int UnitToPixels(System.Web.UI.WebControls.Unit val)
        {
            double res = val.Value;

            switch (val.Type)
            {
                case System.Web.UI.WebControls.UnitType.Cm:
                    res *= DefaultScreenDPI / 2.54;
                    break;
                case System.Web.UI.WebControls.UnitType.Em:
                    res *= 16; // just "usual" font size
                    break;
                case System.Web.UI.WebControls.UnitType.Ex:
                    //res *= 10; // just "usual" font size
                    res *= 8; // just "usual" font size
                    break;
                case System.Web.UI.WebControls.UnitType.Inch:
                    res *= DefaultScreenDPI;
                    break;
                case System.Web.UI.WebControls.UnitType.Mm:
                    res *= DefaultScreenDPI / 25.4;
                    break;
                case System.Web.UI.WebControls.UnitType.Percentage:
                    res *= 6.4; // to get 640 pixel = 100%
                    break;
                case System.Web.UI.WebControls.UnitType.Pica:
                    res *= DefaultScreenDPI / 6;
                    break;
                case System.Web.UI.WebControls.UnitType.Pixel:
                    break;
                case System.Web.UI.WebControls.UnitType.Point:
                    res *= DefaultScreenDPI / 72;
                    break;
            }

            return (int)Math.Round(res);
        }

        // Added by Yankun@20090305
        // for adding user control rendering logic.
        private const string CONST_SEPARATOR_BLOCKS = "|$#";
        private const string CONST_SEPARATOR_BLOCK = "~!@";
        private const string CONST_SEPARATOR_KEYVALUE = "~:~";
        private static XmlDocument _emptyScriptManager;
        /// <summary>
        /// Loads and renders a user control.
        /// </summary>
        /// <param name="inputString">User Control content string.</param>
        /// <param name="path">Path of this control</param>
        /// <param name="isEmbedded">A boolean value indicate whether this control is embedded in an assembly/</param>
        /// <param name="data">Data that maybe bounded to this control</param>
        /// <param name="page">Current Page.</param>
        /// <param name="visualStyle">Visual Style.</param>
        /// <param name="embeddedVisualStyle">Use embeded Visual Style.</param>
        /// <param name="visualStylePath">visualStylePath.</param>
        /// <returns>HTML result</returns>
        public static string LoadUserControl(string inputString, string path, bool isEmbedded, object data, Page page, string visualStyle, bool embeddedVisualStyle, string visualStylePath)
        {
            Control viewControl = null;
            if (isEmbedded)
            {
                if (inputString.Length > 0)
                {
                    viewControl = page.ParseControl(inputString);
                }
            }
            else
            {
                viewControl = page.LoadControl(path);
            }

            if (_emptyScriptManager == null)
            {
                _emptyScriptManager = InternalRenderView(null, visualStyle, embeddedVisualStyle, visualStylePath);
            }
            XmlDocument view = InternalRenderView(viewControl, visualStyle, embeddedVisualStyle, visualStylePath);
            RemoveDuplicateNode(view.FirstChild, _emptyScriptManager.FirstChild);

            return GetTemplateResult(view);
        }

        /// <summary>
        /// Render user control in a page.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="visualStyle"></param>
        /// <param name="embeddedVisualStyle"></param>
        /// <param name="visualStylePath"></param>
        /// <returns></returns>
        private static XmlDocument InternalRenderView(Control c, string visualStyle, bool embeddedVisualStyle, string visualStylePath)
        {
            string s;
            XmlDocument doc = new XmlDocument();
            Page pageHolder = new Page();
            HtmlHead head = new HtmlHead();
            LiteralControl docType = new LiteralControl("<html>\r\n");
            pageHolder.Controls.Add(docType);
            pageHolder.Controls.Add(head);
            pageHolder.Controls.Add(new LiteralControl("\r\n<body style=\"margin:0px;padding:0px;\">\r\n    "));
            HtmlForm form = new HtmlForm();
            form.ID = "form1";
            ScriptManager m = new ScriptManager();
            m.ID = "scriptManager";
            m.SupportsPartialRendering = false;
            form.Controls.Add(m);
            pageHolder.Controls.Add(form);
            pageHolder.Controls.Add(new LiteralControl("\r\n</body>\r\n</html>    "));
            if (c != null)
            {
                HtmlGenericControl div = new HtmlGenericControl("TemplateControl");
                div.Controls.Add(c);
                form.Controls.Add(div);
                //SetVisualStyleForThemeableControls(pageHolder.Controls, visualStyle, embeddedVisualStyle, visualStylePath);
            }
            using (StringWriter output = new StringWriter())
            {
                HttpContext.Current.Server.Execute(pageHolder, output, false);
                s = output.ToString();
            }
            doc.LoadXml(s);
            return doc;
        }

        /// <summary>
        /// RemoveDuplicateNode
        /// </summary>
        /// <param name="oriNode"></param>
        /// <param name="duplicateNode"></param>
        private static void RemoveDuplicateNode(XmlNode oriNode, XmlNode duplicateNode)
        {
            if (!duplicateNode.HasChildNodes)
            {
                //string beginTagText = duplicateNode.
                if (duplicateNode.NodeType == XmlNodeType.CDATA)
                {
                    oriNode.InnerXml = oriNode.InnerXml.Replace(duplicateNode.ParentNode.OuterXml, string.Empty);

                    string text = duplicateNode.InnerText.Replace("\r\n", string.Empty).Replace("//", string.Empty);

                    if (oriNode.InnerXml.Contains(text))
                    {
                        oriNode.InnerXml = oriNode.InnerXml.Replace(text, string.Empty);
                    }
                }
                else if (duplicateNode.Value != "\r\n//")
                {
                    oriNode.InnerXml = oriNode.InnerXml.Replace(duplicateNode.OuterXml, string.Empty);
                }

                return;
            }

            foreach (XmlNode node in duplicateNode.ChildNodes)
            {
                RemoveDuplicateNode(oriNode, node);
            }
        }

        private static string GetTemplateResult(XmlDocument xDoc)
        {
            string styles = GetStyles(xDoc);
            string scripts = GetScripts(xDoc);
            string html = GetHtmlForTemplate(xDoc);

            return string.Format("{0}{1}{2}{1}{3}", styles, CONST_SEPARATOR_BLOCKS, scripts, html);
        }

        private static string GetStyles(XmlDocument xDoc)
        {
            ArrayList styles = new ArrayList();
            XmlNodeList linkNodes = xDoc.SelectNodes("//link");
            if (linkNodes == null)
            {
                return "";
            }
            foreach (XmlNode link in linkNodes)
            {
                if (link.Attributes["rel"] != null && link.Attributes["rel"].Value.Equals("stylesheet") && link.Attributes["href"] != null)
                {
                    styles.Add(string.Format("href{0}{1}", CONST_SEPARATOR_KEYVALUE, link.Attributes["href"].Value));
                }
            }

            XmlNodeList stylesNodes = xDoc.SelectNodes("//style");
            if (stylesNodes == null)
            {
                return "";
            }
            foreach (XmlNode style in stylesNodes)
            {
                if (!style.InnerText.Equals(string.Empty))
                {
                    styles.Add(string.Format("text{0}{1}", CONST_SEPARATOR_KEYVALUE, style.InnerText));
                }
            }

            return string.Join(CONST_SEPARATOR_BLOCK, (string[])styles.ToArray(typeof(string)));
        }

        private static string GetScripts(XmlDocument xDoc)
        {
            ArrayList scripts = new ArrayList();
            XmlNodeList scriptNodes = xDoc.SelectNodes("//script");
            if (scriptNodes == null)
            {
                return "";
            }
            foreach (XmlNode script in scriptNodes)
            {
                if (script.Attributes["src"] != null)
                {
                    scripts.Add(string.Format("src{0}{1}", CONST_SEPARATOR_KEYVALUE, script.Attributes["src"].Value));
                }
                else if (!script.InnerText.Equals(string.Empty))
                {
                    scripts.Add(string.Format("text{0}{1}", CONST_SEPARATOR_KEYVALUE, script.InnerText));
                }
            }

            return string.Join(CONST_SEPARATOR_BLOCK, (string[])scripts.ToArray(typeof(string)));
        }

        private static string GetHtmlForTemplate(XmlDocument xDoc)
        {
            return xDoc.SelectSingleNode("//TemplateControl").InnerXml;
        }
        // End YanKun@20090305
    }
}
