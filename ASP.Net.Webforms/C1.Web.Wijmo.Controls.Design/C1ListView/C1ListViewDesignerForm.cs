﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1ListView
{
    using C1.Web.Wijmo.Controls.C1ListView;
    using System.IO;
    using C1.Web.Wijmo.Controls.Design.Localization;

    /// <summary>
    /// Element Types enumerations used by Designer
    /// </summary>
    public enum ItemType
    {
        /// <summary>
        /// Item is NestedItem, which has children items in.
        /// </summary>
        C1ListViewNestedItem,
        /// <summary>
        /// Item is ControlGroupItem, 
        /// it can render a list of checkbox, radiobutton or normal optionItem in one C1ListVeiwItem.
        /// </summary>
        C1ListViewControlGroupItem,
        /// <summary>
        /// Item is FlipSwitchItem, it is a switch about ON or OFF.
        /// </summary>
        C1ListViewFlipSwitchItem,
        /// <summary>
        /// Item is InputItem, 
        /// it contains a label and a MessageContorl which can be textbox, searchbar, silder.
        /// </summary>
        C1ListViewInputItem,
        /// <summary>
        /// Item is LinkItem, which has a click event and button apperance.
        /// </summary>
        C1ListViewLinkItem,
        /// <summary>
        /// Item is ButtonItem, which contains a button
        /// </summary>
        C1ListViewButtonItem,
        /// <summary>
        /// Item is DividerItem, and set it as Divider
        /// </summary>
        C1ListViewDividerItem,
        /// <summary>
        /// Item is C1ListViewItem, a normal item.
        /// </summary>
        C1ListViewItem,
        /// <summary>
        /// Item is C1ListView
        /// </summary>
        C1ListView
    }

    public partial class C1ListViewDesignerForm : C1BaseItemEditorForm
    {
        #region Fields

        private C1ListView _listView;
        private Stream _clipboardData;
        private C1ItemInfo _clipboardType;

        #endregion

        public C1ListViewDesignerForm(C1ListView listView)
            :base(listView)
        {
            _listView = listView;
            InitializeComponent();
        }

        protected override List<C1ItemInfo> FillAvailableControlItems()
        {
            List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();
            C1ItemInfo item;

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.ListViewItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewItem";
            item.Default = true;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewDividerItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.ListViewDivider");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewDivider";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewButtonItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.C1ListViewButtonItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewButtonItem";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewInputItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.C1ListViewInputItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewInputItem";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewLinkItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.C1ListViewLinkItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewLinkItem";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewNestedItem;
            item.EnableChildItems = true;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.C1ListViewNestedItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewNestedItem";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewFlipSwitchItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.C1ListViewFlipSwitchItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewFlipSwitchItem";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListViewControlGroupItem;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.C1ListViewControlGroupItem");
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1ListViewControlGroupItem";
            item.Default = false;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1ListView;
            item.EnableChildItems = true;
            item.ContextMenuStripText = C1Localizer.GetString("C1ListView.ListViewText.ListView");
            item.NodeImage = Properties.Resources.RootItemIco;
            item.DefaultNodeText = "C1ListView";
            //item.Default = true;
            item.Visible = false;
            itemsInfo.Add(item);

            return itemsInfo;
        }

        const string NEW_LINE = "\r\n";
        /// <summary>
        /// Shows current control into preview tab
        /// </summary>
        /// <param name="previewer">The web browser previewer</param>
        internal override void ShowPreviewHtml(C1WebBrowser previewer)
        {
            try
            {
                string sResultBodyContent = "";
                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new StringWriter(sb);
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);

                string s = _listView.CssClass;
                _listView.RenderControl(htw);
                _listView.CssClass = s;
                sResultBodyContent = sb.ToString();
                string sDocumentContent = "";
                sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
                sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
                sDocumentContent += "<head>" + NEW_LINE;
                sDocumentContent += "</head>" + NEW_LINE;
                sDocumentContent += "<body>" + NEW_LINE;
                sDocumentContent += sResultBodyContent + NEW_LINE;
                sDocumentContent += "</body>" + NEW_LINE;
                sDocumentContent += "</html>" + NEW_LINE;
                previewer.DocumentWrite(sDocumentContent, this._listView);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
            }
        }

        protected override void SaveToXML(string fileName)
        {
            try
            {
                _listView.SaveLayout(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void LoadFromXML(string fileName)
        {
            try
            {
                _listView.LoadLayout(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Loads <see cref="TreeNode"/> nodes into the main TreeView
        /// </summary>
        /// <param name="mainTreeView"></param>
        protected override void LoadControl(TreeView mainTreeView)
        {
            string nodeText;
            object nodeTag;
            C1ItemInfo itemInfo;

            mainTreeView.BeginUpdate();
            mainTreeView.Nodes.Clear();

            TreeNode rootListView = new TreeNode();
            itemInfo = GetItemInfo(GetItemType(_listView));

            nodeText = _listView.ID == null ? itemInfo.DefaultNodeText : _listView.ID.ToString();
            nodeTag = new C1NodeInfo(_listView, itemInfo, nodeText);
            SetNodeAttributes(rootListView, nodeText, nodeTag);

            foreach (C1ListViewItem item in _listView.Items)
            {
                TreeNode listViewItemNode = new TreeNode();
                if (item is C1ListViewInputItem)
                {
                    string text = ((C1ListViewInputItem)item).LabelText;
                    nodeText = string.IsNullOrEmpty(text) ? GetItemType(item).ToString() : text;
                }
                else
                {
                    nodeText = string.IsNullOrEmpty(item.Text) ? GetItemType(item).ToString() : item.Text;
                }
                itemInfo = GetItemInfo(GetItemType(item));
                nodeTag = new C1NodeInfo(item, itemInfo, nodeText);
                SetNodeAttributes(listViewItemNode, nodeText, nodeTag);

                IterateChildNodes(listViewItemNode, item);


                rootListView.Nodes.Add(listViewItemNode);
            }

            mainTreeView.Nodes.Add(rootListView);
            mainTreeView.SelectedNode = mainTreeView.Nodes[0];
            mainTreeView.ExpandAll();
            mainTreeView.EndUpdate();
        }

        /// <summary>
        /// Loads nested items from current item
        /// </summary>
        /// <param name="menuItemNode">TreeNode parent</param>
        /// <param name="item">Control item</param>
        private void IterateChildNodes(TreeNode treeViewNode, C1ListViewItem parentItem)
        {
            if (parentItem.Items != null && parentItem.Items.Count > 0)
            {
                string nodeText;
                object nodeTag;
                C1ItemInfo itemInfo;

                treeViewNode.Nodes.Clear();
                foreach (C1ListViewItem item in parentItem.Items)
                {
                    TreeNode treeNode = new TreeNode();
                    nodeText = item.Text;
                    itemInfo = GetItemInfo(GetItemType(item));
                    nodeTag = new C1NodeInfo(item, itemInfo, nodeText);
                    SetNodeAttributes(treeNode, nodeText, nodeTag);

                    IterateChildNodes(treeNode, item);

                    treeViewNode.Nodes.Add(treeNode);
                }
            }
        }

        protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
        {
            base.AllowAdd = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListView, ItemType.C1ListViewNestedItem });
            base.AllowInsert = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowCopy = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowCut = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowDelete = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowRename = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListView, ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowMoveUp = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowMoveDown = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem, ItemType.C1ListViewDividerItem, ItemType.C1ListViewButtonItem, ItemType.C1ListViewInputItem, ItemType.C1ListViewLinkItem, ItemType.C1ListViewNestedItem, ItemType.C1ListViewFlipSwitchItem, ItemType.C1ListViewControlGroupItem });
            base.AllowMoveLeft = EnableMoveLeft(itemInfo, parentItemInfo);
            base.AllowMoveRight = EnableMoveRight(itemInfo, previousItemInfo);
            base.AllowDrop = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListViewItem });

            base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
        }

        protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
        {
            C1NodeInfo nodeInfo;
            string nodeText;

            C1ListViewItem item = new C1ListViewItem();

            switch ((ItemType)itemInfo.ItemType)
            {
                case ItemType.C1ListViewButtonItem:
                    item = new C1ListViewButtonItem();
                    break;
                case ItemType.C1ListViewDividerItem:
                    item = new C1ListViewDividerItem();
                    break;
                case ItemType.C1ListViewInputItem:
                    item = new C1ListViewInputItem();
                    break;
                case ItemType.C1ListViewLinkItem:
                    item = new C1ListViewLinkItem();
                    break;
                case ItemType.C1ListViewNestedItem:
                    item = new C1ListViewNestedItem();
                    break;
                case ItemType.C1ListViewFlipSwitchItem:
                    item = new C1ListViewFlipSwitchItem();
                    break;
                case ItemType.C1ListViewControlGroupItem:
                    item = new C1ListViewControlGroupItem();
                    break;
            }

            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);

            if (item is C1ListViewInputItem)
            {
                (item as C1ListViewInputItem).LabelText = nodeText;
            }
            else
            {
                item.Text = nodeText;
            }

            nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);

            nodeInfo.NodeText = nodeText;

            return nodeInfo;
        }

        /// <summary>
        /// Performs some action when a property value changes.
        /// </summary>
        /// <param name="changedGridItem">The grid item changed.</param>
        /// <param name="selectedNode">Current selected node.</param>
        protected override void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
        {
            base.OnPropertyGridPropertyValueChanged(changedGridItem, selectedNode);

            if (changedGridItem.PropertyDescriptor.Name == "LabelText")
                if (selectedNode.Text != changedGridItem.Value.ToString())
                    selectedNode.Text = changedGridItem.Value.ToString();
        }

        /// <summary>
        ///  Indicates when an item was inserted in a specific position.
        /// </summary>
        /// <param name="item">The inserted item.</param>
        /// <param name="destinationItem">The destination item that will contain inserted item.</param>
        /// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
        protected override void Insert(object item, object destinationItem, int destinationIndex)
        {
            ((IC1ListViewItemCollectionOwner)destinationItem).Items.Insert(destinationIndex, (C1ListViewItem)item);
        }

        /// <summary>
        ///  Indicates when an item was deleted.
        /// </summary>
        /// <param name="item">The deleted item.</param>
        protected override void Delete(object item)
        {
            ((IC1ListViewItemCollectionOwner)item).Owner.Items.Remove((C1ListViewItem)item);
        }

        protected override void Copy(object item, C1ItemInfo itemInfo)
        {
            C1ListViewSerializer serializer = new C1ListViewSerializer(item);
            if (_clipboardData == null)
                _clipboardData = new MemoryStream();

            _clipboardData.SetLength(0);
            serializer.SaveLayout(_clipboardData);
            base.ClipboardData = true;

            _clipboardType = itemInfo;
            base.AllowPaste = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1ListView, ItemType.C1ListViewItem });
        }

        protected override void Paste(TreeNode destinationNode)
        {
            C1ListViewItem listViewItem = new C1ListViewItem();
            C1ListViewSerializer serializer = new C1ListViewSerializer(listViewItem);
            _clipboardData.Seek(0, SeekOrigin.Begin);
            serializer.LoadLayout(_clipboardData, LayoutType.All);

            object parentObject = ((C1NodeInfo)destinationNode.Tag).Element;

            ((IC1ListViewItemCollectionOwner)parentObject).Items.Add((C1ListViewItem)listViewItem);

            C1NodeInfo nodeInfo = new C1NodeInfo(listViewItem, _clipboardType, listViewItem.Text);

            TreeNode treeNode = new TreeNode();

            SetNodeAttributes(treeNode, nodeInfo.NodeText, nodeInfo);

            IterateChildNodes(treeNode, listViewItem);

            destinationNode.Nodes.Add(treeNode);
            destinationNode.ExpandAll();
        }

        protected override void MoveDrag(TreeNode sourceNode, TreeNode destinationNode)
        {
            if (!IsChildNode(sourceNode, destinationNode))
            {
                base.MoveDrag(sourceNode, destinationNode);
            }
        }

        protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
        {
            bool cancel = e.CancelEdit;
            FinishLabelEdit(e.Node, e.Label, ref cancel);
            e.CancelEdit = cancel;
        }

        /// <summary>
        /// Gets de item info according to the given item type
        /// </summary>
        /// <param name="itemType">Item type</param>
        /// <returns>C1ItemInfo object that contains the item info</returns>
        private C1ItemInfo GetItemInfo(ItemType itemType)
        {
            foreach (C1ItemInfo itemInfo in base.ItemsInfo)
            {
                if ((ItemType)itemInfo.ItemType == itemType)
                    return itemInfo;
            }
            return null;
        }

        /// <summary>
        /// Gets the type of the given item
        /// </summary>
        /// <param name="item">Given item for checking its type</param>
        /// <returns>The <seealso cref="ItemType"/> of given item</returns>
        private ItemType GetItemType(object item)
        {
            ItemType elementType = ItemType.C1ListViewItem;

            if (item is C1ListView)
            {
                elementType = ItemType.C1ListView;
            }
            else if (item is C1ListViewButtonItem)
            {
                elementType = ItemType.C1ListViewButtonItem;
            }
            else if (item is C1ListViewDividerItem)
            {
                elementType = ItemType.C1ListViewDividerItem;
            }
            else if (item is C1ListViewLinkItem)
            {
                elementType = ItemType.C1ListViewLinkItem;
            }
            else if (item is C1ListViewNestedItem)
            {
                elementType = ItemType.C1ListViewNestedItem;
            }
            else if (item is C1ListViewFlipSwitchItem)
            {
                elementType = ItemType.C1ListViewFlipSwitchItem;
            }
            else if (item is C1ListViewControlGroupItem)
            {
                elementType = ItemType.C1ListViewControlGroupItem;
            }
            else if (item is C1ListViewInputItem)
            {
                elementType = ItemType.C1ListViewInputItem;
            }
            else if (item is C1ListViewItem)
            {
                elementType = ItemType.C1ListViewItem;
            }
            return elementType;
        }

        private bool IsChildNode(TreeNode sourceNode, TreeNode destinationNode)
        {
            TreeNode pNode = destinationNode.Parent;
            if (pNode != null)
            {
                if (pNode == sourceNode)
                {
                    return true;
                }
                else
                {
                    return IsChildNode(sourceNode, pNode);
                }
            }
            return false;
        }

        private bool EnableActionByItemType(C1ItemInfo itemInfo, ItemType[] types)
        {
            bool enable = false;
            if (itemInfo != null)
            {
                foreach (ItemType type in types)
                {
                    enable |= itemInfo.ItemType.Equals(type);
                }
            }
            return enable;
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the left
        /// </summary>
        private bool EnableMoveLeft(C1ItemInfo itemInfo, C1ItemInfo parenItemInfo)
        {
            if (parenItemInfo == null || ((ItemType)parenItemInfo.ItemType).Equals(ItemType.C1ListView)) // Root node
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the right
        /// </summary>
        private bool EnableMoveRight(C1ItemInfo itemInfo, C1ItemInfo previousItemInfo)
        {
            if (previousItemInfo != null && previousItemInfo.EnableChildItems) // Root node
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool EnableMoveUp()
        {
            return true;
        }

        private bool EnableMoveDown()
        {
            return true;
        }

        private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
        {
            string nodeText = "";
            string property = "Text";
            object obj = ((C1NodeInfo)node.Tag).Element;
            C1ItemInfo itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;

            if (text != null)
            {
                nodeText = text;
            }
            else
            {
                if (obj is C1ListView)
                {
                    nodeText = ((C1ListView)obj).ID == null ? "" : ((C1ListView)obj).ID.ToString();
                }
                else
                {
                    if (obj is C1ListViewInputItem)
                    {
                        if (!string.IsNullOrEmpty(((C1ListViewInputItem)obj).LabelText))
                        {
                            nodeText = ((C1ListViewInputItem)obj).LabelText;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(((C1ListViewItem)obj).Text))
                        {
                            nodeText = ((C1ListViewItem)obj).Text;
                        }
                    }
                    if (string.IsNullOrEmpty(nodeText))
                    {
                        if (node.Parent == null)
                        {
                            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Nodes);
                        }
                        else
                        {
                            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Parent.Nodes);
                        }
                    }
                }
            }

            if (obj is C1ListView)
            {
                property = "ID";
            }
            else if (obj is C1ListViewInputItem)
            {
                property = "LabelText";
            }
            else if (obj is C1ListViewItem)
            {
                property = "Text";
            }

            TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
            base.RefreshPropertyGrid();
            node.Text = nodeText;
        }
    }
}
