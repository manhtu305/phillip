﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.Design;
using System.ComponentModel.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;


namespace C1.Web.Wijmo.Controls.Design.C1ListView
{
    using C1.Web.Wijmo.Controls.C1ListView;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using System.Web.UI.Design.WebControls;


    [SupportsPreviewControl(false)]
    public class C1ListViewDesigner : C1HierarchicalDataBoundControlDesigner
    {
        C1ListView _listView;

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this._listView = (C1ListView)component;
        }

        private DesignerActionListCollection _actions;
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (_actions == null)
                {
                    _actions = new DesignerActionListCollection();
                    _actions.AddRange(base.ActionLists);
                    _actions.Add(new C1ListViewActionList(this));
                }
                return _actions;
            }
        }

        public void EditItems()
        {
            List<C1ListViewItem> originalData = new List<C1ListViewItem>();
            foreach (C1ListViewItem item in _listView.Items)
            {
                originalData.Add(item);
            }

            C1ListViewDesignerForm editForm = new C1ListViewDesignerForm(this._listView);
            bool accept = false;
            accept = ShowControlEditorForm(this._listView, editForm);

        }

        public bool EditBindings()
        {
            C1ListViewBindingsEditorForm editorForm = new C1ListViewBindingsEditorForm();
            editorForm.SetComponent(this._listView);
            return ShowControlEditorForm(this._listView, editorForm);
        }

        public bool ShowControlEditorForm(object control, Form _editorForm)
        {
            DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1ListView.EditControl"));
                using (trans)
                {
                    IUIService service = (IUIService)serviceProvider.GetService(typeof(IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(_editorForm);
                    }
                    else
                        dr = DialogResult.None;
                    if (dr == DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = _editorForm.ShowDialog();

            _editorForm.Dispose();
            _editorForm = null;

            return dr == DialogResult.OK;
        }

        public override string GetDesignTimeHtml()
        {
            if (this._listView is C1ListView)
            {
                C1ListView listView = (C1ListView)this._listView;

                if (listView.Items.Count == 0
                    &&
                    string.IsNullOrEmpty(listView.DataSourceID))
                {
                    return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", C1Localizer.GetString("C1ListView.Empty"));
                }
            }
            return base.GetDesignTimeHtml();
        }
    }

    internal class C1ListViewActionList : DesignerActionListBase
    {
        #region Fields

        private C1ListView _listView;
        private DesignerActionItemCollection _actions;

        #endregion

        #region Constructor

        public C1ListViewActionList(C1ListViewDesigner designer)
            : base(designer)
        {
            _listView = designer.Component as C1ListView;
        }

        #endregion

        #region Actions

        public void EditItems()
        {
            C1ListViewDesigner designer = this.Designer as C1ListViewDesigner;
            designer.EditItems();
        }

        public void EditBindings()
        {
            C1ListViewDesigner designer = this.Designer as C1ListViewDesigner;
            designer.EditBindings();
        }
        #endregion

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_actions == null)
            {
                _actions = new DesignerActionItemCollection();
                _actions.Add(new DesignerActionMethodItem(this, "EditItems", C1Localizer.GetString("C1ListView.SmartTag.EditListView"), "", C1Localizer.GetString("C1ListView.SmartTag.EditListViewDescription"), true));
                _actions.Add(new DesignerActionMethodItem(this, "EditBindings", C1Localizer.GetString("C1ListView.SmartTag.EditListViewBindings"), "", C1Localizer.GetString("C1ListView.SmartTag.EditListViewBindingsDescription"), true));
                AddBaseSortedActionItems(_actions);
            }
            return _actions;
        }

		internal override bool DisplayThemeSwatch()
		{
			return true;
		}
    }
}
