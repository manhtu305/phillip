﻿using C1.Web.Wijmo.Controls.Design.Localization;
namespace C1.Web.Wijmo.Controls.Design.C1ListView
{
    partial class C1ListViewBindingsEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._panel1 = new System.Windows.Forms.Panel();
            this._btnOK = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this._btnHelp = new System.Windows.Forms.Button();
            this._splitContainer = new System.Windows.Forms.SplitContainer();
            this._lbl1 = new System.Windows.Forms.Label();
            this._btnDelete = new System.Windows.Forms.Button();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._btnMoveDown = new System.Windows.Forms.Button();
            this._btnMoveUp = new System.Windows.Forms.Button();
            this._btnAdd = new System.Windows.Forms.Button();
            this._cbxAvailableBindings = new System.Windows.Forms.ComboBox();
            this._listBindings = new System.Windows.Forms.ListBox();
            this._lblBindName = new System.Windows.Forms.Label();
            this._propGrid = new System.Windows.Forms.PropertyGrid();
            this._ctxPropGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmiReset = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this._cmiDescription = new System.Windows.Forms.ToolStripMenuItem();
            this._panel1.SuspendLayout();
            this._splitContainer.Panel1.SuspendLayout();
            this._splitContainer.Panel2.SuspendLayout();
            this._splitContainer.SuspendLayout();
            this._ctxPropGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // _panel1
            // 
            this._panel1.Controls.Add(this._btnOK);
            this._panel1.Controls.Add(this._btnCancel);
            this._panel1.Controls.Add(this._btnHelp);
            this._panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panel1.Location = new System.Drawing.Point(0, 327);
            this._panel1.Name = "_panel1";
            this._panel1.Size = new System.Drawing.Size(633, 40);
            this._panel1.TabIndex = 0;
            // 
            // _btnOK
            // 
            this._btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnOK.Location = new System.Drawing.Point(470, 10);
            this._btnOK.Name = "_btnOK";
            this._btnOK.Size = new System.Drawing.Size(75, 23);
            this._btnOK.TabIndex = 2;
            this._btnOK.Text = C1Localizer.GetString("Base.OK");
            this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(551, 10);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(75, 23);
            this._btnCancel.TabIndex = 1;
            this._btnCancel.Text = C1Localizer.GetString("Base.Cancel");
            // 
            // _btnHelp
            // 
            this._btnHelp.Location = new System.Drawing.Point(0, 0);
            this._btnHelp.Name = "_btnHelp";
            this._btnHelp.Size = new System.Drawing.Size(75, 23);
            this._btnHelp.TabIndex = 3;
            this._btnHelp.Visible = false;
            // 
            // _splitContainer
            // 
            this._splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainer.IsSplitterFixed = true;
            this._splitContainer.Location = new System.Drawing.Point(0, 0);
            this._splitContainer.Name = "_splitContainer";
            // 
            // _splitContainer.Panel1
            // 
            this._splitContainer.Panel1.Controls.Add(this._lbl1);
            this._splitContainer.Panel1.Controls.Add(this._btnDelete);
            this._splitContainer.Panel1.Controls.Add(this._btnMoveDown);
            this._splitContainer.Panel1.Controls.Add(this._btnMoveUp);
            this._splitContainer.Panel1.Controls.Add(this._btnAdd);
            this._splitContainer.Panel1.Controls.Add(this._cbxAvailableBindings);
            this._splitContainer.Panel1.Controls.Add(this._listBindings);
            // 
            // _splitContainer.Panel2
            // 
            this._splitContainer.Panel2.Controls.Add(this._lblBindName);
            this._splitContainer.Panel2.Controls.Add(this._propGrid);
            this._splitContainer.Size = new System.Drawing.Size(633, 327);
            this._splitContainer.SplitterDistance = 274;
            this._splitContainer.TabIndex = 1;
            this._splitContainer.Text = "splitContainer1";
            // 
            // _lbl1
            // 
            this._lbl1.AutoSize = true;
            this._lbl1.Location = new System.Drawing.Point(13, 13);
            this._lbl1.Name = "_lbl1";
            this._lbl1.Size = new System.Drawing.Size(162, 13);
            this._lbl1.TabIndex = 6;
            this._lbl1.Text = C1Localizer.GetString("Base.BindingsEditorForm.SelectItem");
            // 
            // _btnDelete
            // 
            this._btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnDelete.ImageList = this._imageList;
            this._btnDelete.Location = new System.Drawing.Point(245, 123);
            this._btnDelete.Name = "_btnDelete";
            this._btnDelete.Size = new System.Drawing.Size(23, 23);
            this._btnDelete.TabIndex = 5;
            this._btnDelete.Click += new System.EventHandler(this._btnDelete_Click);
            // 
            // _imageList
            // 
            this._imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this._imageList.ImageSize = new System.Drawing.Size(16, 16);
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // _btnMoveDown
            // 
            this._btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnMoveDown.ImageList = this._imageList;
            this._btnMoveDown.Location = new System.Drawing.Point(245, 94);
            this._btnMoveDown.Name = "_btnMoveDown";
            this._btnMoveDown.Size = new System.Drawing.Size(23, 23);
            this._btnMoveDown.TabIndex = 4;
            this._btnMoveDown.Click += new System.EventHandler(this._btnMoveDown_Click);
            // 
            // _btnMoveUp
            // 
            this._btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnMoveUp.ImageList = this._imageList;
            this._btnMoveUp.Location = new System.Drawing.Point(245, 69);
            this._btnMoveUp.Name = "_btnMoveUp";
            this._btnMoveUp.Size = new System.Drawing.Size(23, 23);
            this._btnMoveUp.TabIndex = 3;
            this._btnMoveUp.Click += new System.EventHandler(this._btnMoveUp_Click);
            // 
            // _btnAdd
            // 
            this._btnAdd.Location = new System.Drawing.Point(190, 34);
            this._btnAdd.Name = "_btnAdd";
            this._btnAdd.Size = new System.Drawing.Size(47, 23);
            this._btnAdd.TabIndex = 1;
            this._btnAdd.Text = C1Localizer.GetString("Base.BindingsEditorForm.Add");
            this._btnAdd.Click += new System.EventHandler(this._btnAdd_Click);
            // 
            // _cbxAvailableBindings
            // 
            this._cbxAvailableBindings.FormattingEnabled = true;
            this._cbxAvailableBindings.Location = new System.Drawing.Point(4, 34);
            this._cbxAvailableBindings.Name = "_cbxAvailableBindings";
            this._cbxAvailableBindings.Size = new System.Drawing.Size(180, 21);
            this._cbxAvailableBindings.TabIndex = 0;
            // 
            // _listBindings
            // 
            this._listBindings.FormattingEnabled = true;
            this._listBindings.Location = new System.Drawing.Point(4, 67);
            this._listBindings.Name = "_listBindings";
            this._listBindings.Size = new System.Drawing.Size(236, 251);
            this._listBindings.TabIndex = 2;
            this._listBindings.SelectedIndexChanged += new System.EventHandler(this._listBindings_SelectedIndexChanged);
            this._listBindings.KeyDown += new System.Windows.Forms.KeyEventHandler(this._listBindings_KeyDown);
            // 
            // _lblBindName
            // 
            this._lblBindName.AutoSize = true;
            this._lblBindName.Location = new System.Drawing.Point(15, 13);
            this._lblBindName.Name = "_lblBindName";
            this._lblBindName.Size = new System.Drawing.Size(94, 13);
            this._lblBindName.TabIndex = 1;
            this._lblBindName.Text = C1Localizer.GetString("Base.BindingsEditorForm.BindProperties");
            // 
            // _propGrid
            // 
            this._propGrid.ContextMenuStrip = this._ctxPropGrid;
            this._propGrid.Location = new System.Drawing.Point(3, 37);
            this._propGrid.Name = "_propGrid";
            this._propGrid.Size = new System.Drawing.Size(347, 288);
            this._propGrid.TabIndex = 0;
            this._propGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._propGrid_PropertyValueChanged);
            // 
            // _ctxPropGrid
            // 
            this._ctxPropGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmiReset,
            this._toolStripSeparator,
            this._cmiDescription});
            this._ctxPropGrid.Name = "_ctxPropGrid";
            this._ctxPropGrid.Size = new System.Drawing.Size(139, 54);
            this._ctxPropGrid.Opening += new System.ComponentModel.CancelEventHandler(this._ctxPropGrid_Opening);
            // 
            // _cmiReset
            // 
            this._cmiReset.Enabled = false;
            this._cmiReset.Name = "_cmiReset";
            this._cmiReset.Size = new System.Drawing.Size(138, 22);
            this._cmiReset.Text = C1Localizer.GetString("Base.BindingsEditorForm.Reset");
            this._cmiReset.Click += new System.EventHandler(this._cmiReset_Click);
            // 
            // _toolStripSeparator
            // 
            this._toolStripSeparator.Name = "_toolStripSeparator";
            this._toolStripSeparator.Size = new System.Drawing.Size(135, 6);
            // 
            // _cmiDescription
            // 
            this._cmiDescription.Checked = true;
            this._cmiDescription.CheckState = System.Windows.Forms.CheckState.Checked;
            this._cmiDescription.Name = "_cmiDescription";
            this._cmiDescription.Size = new System.Drawing.Size(138, 22);
            this._cmiDescription.Text = C1Localizer.GetString("Base.BindingsEditorForm.Description");
            this._cmiDescription.Click += new System.EventHandler(this._cmiDescription_Click);
            // 
            // C1TreeViewBindingsEditorForm
            // 
            this.AcceptButton = this._btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(633, 367);
            this.Controls.Add(this._splitContainer);
            this.Controls.Add(this._panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "C1TreeViewBindingsEditorForm";
            this.Text = C1Localizer.GetString("Base.BindingsEditorForm.Title");
            this.Load += new System.EventHandler(this.C1ListViewBindingsEditorForm_Load);
            this._panel1.ResumeLayout(false);
            this._splitContainer.Panel1.ResumeLayout(false);
            this._splitContainer.Panel1.PerformLayout();
            this._splitContainer.Panel2.ResumeLayout(false);
            this._splitContainer.Panel2.PerformLayout();
            this._splitContainer.ResumeLayout(false);
            this._ctxPropGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel _panel1;
        private System.Windows.Forms.SplitContainer _splitContainer;
        private System.Windows.Forms.Button _btnAdd;
        private System.Windows.Forms.ComboBox _cbxAvailableBindings;
        private System.Windows.Forms.ListBox _listBindings;
        private System.Windows.Forms.PropertyGrid _propGrid;
        private System.Windows.Forms.Button _btnDelete;
        private System.Windows.Forms.Button _btnMoveDown;
        private System.Windows.Forms.Button _btnMoveUp;
        private System.Windows.Forms.Button _btnOK;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.Button _btnHelp;
        private System.Windows.Forms.Label _lbl1;
        private System.Windows.Forms.Label _lblBindName;
        private System.Windows.Forms.ImageList _imageList;
        private System.Windows.Forms.ContextMenuStrip _ctxPropGrid;
        private System.Windows.Forms.ToolStripMenuItem _cmiReset;
        private System.Windows.Forms.ToolStripSeparator _toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem _cmiDescription;
    }
}