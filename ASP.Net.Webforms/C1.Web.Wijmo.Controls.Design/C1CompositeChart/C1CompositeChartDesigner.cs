﻿using System;
using System.Collections.Generic;
using System.Web.UI.Design;
using System.Drawing;
using System.Web.UI;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1CompositeChart
{
	using C1.Web.Wijmo.Controls.C1Chart;
	using C1.Web.Wijmo.Controls.Design.C1ChartCore;
	using System.ComponentModel.Design;

	/// <summary>
	/// Provides a C1CompositeChartDesigner class for extending the design-mode behavior of a C1CompositeChart control.
	/// </summary>
	public class C1CompositeChartDesigner : C1ChartCoreDesigner
	{
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
		}

		protected override void AddSampleData()
		{
			C1CompositeChart compositeChart = Component as C1CompositeChart;
			CompositeChartSeries series = new CompositeChartSeries();
			series.Label = "SampleData1";
			series.Type = ChartSeriesType.Column;
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 5, 3, 4, 7, 2 });
			compositeChart.SeriesList.Add(series);

			series = new CompositeChartSeries();
			series.Label = "SampleData2";
			series.Type = ChartSeriesType.Column;
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 2, 2, 3, 2, 1 });
			compositeChart.SeriesList.Add(series);

			series = new CompositeChartSeries();
			series.Label = "SampleData3";
			series.Type = ChartSeriesType.Column;
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 3, 4, 4, 2, 5 });
			compositeChart.SeriesList.Add(series);

			series = new CompositeChartSeries();
			series.Label = "SampleData4";
			series.Type = ChartSeriesType.Pie;
			series.Center = new Point(150, 150);
			series.Radius = 60;

			PieChartSeries pseries = new PieChartSeries();
			pseries.Label = "SampleData4";
			pseries.Data = 1;
			series.PieSeriesList.Add(pseries);

			pseries = new PieChartSeries();
			pseries.Label = "SampleData5";
			pseries.Data = 2;
			series.PieSeriesList.Add(pseries);

			pseries = new PieChartSeries();
			pseries.Label = "SampleData6";
			pseries.Data = 3;
			series.PieSeriesList.Add(pseries);

			compositeChart.SeriesList.Add(series);

			series = new CompositeChartSeries();
			series.Label = "SampleData7";
			series.Type = ChartSeriesType.Line;
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 3, 6, 2, 9, 5 });
			compositeChart.SeriesList.Add(series);

			series = new CompositeChartSeries();
			series.Label = "SampleData8";
			series.Type = ChartSeriesType.Line;
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 1, 3, 4, 7, 2 });
			compositeChart.SeriesList.Add(series);
		}
	}
}