﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.C1AppView;
using C1.Web.Wijmo.Controls.Design.Localization;
using C1.Web.Wijmo.Controls.Design.Utils;

namespace C1.Web.Wijmo.Controls.Design.C1AppView
{
    class AppViewPageDesigner : C1ControlDesinger
	{

		/// <summary>
        /// Initializes a new instance of the AccordionDesigner class
        /// </summary>
		public AppViewPageDesigner()
        {
            
        }

		public override string GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			if (AppViewPage.Header.Template == null || AppViewPage.Content.Template == null)
			{
				// empty html.
				// add a designer region for the "#addtemplates" UI.
				DesignerRegion dr = new DesignerRegion(this, "#addtemplates");
				regions.Add(dr);
				string sStyleString = "";
				if (!AppViewPage.Height.IsEmpty || !AppViewPage.Width.IsEmpty)
				{
					sStyleString = " style=\"";
					if (!AppViewPage.Width.IsEmpty)
					{
						sStyleString += "width:" + AppViewPage.Width.ToString() + ";";
					}
					if (!AppViewPage.Height.IsEmpty)
					{
						sStyleString += "height:" + AppViewPage.Height.ToString() + ";";
					}
					sStyleString += "\"";
				}
                
                var emptyAppViewHint = string.Format(C1Localizer.GetString("C1AppViewPageDesigner.Empty"), AppViewPage.GetType().Name);

                return string.Format("<div id=\"{0}\" {1}><span style=\"color:white;background-color:gray;cursor:pointer;\" {2}=\"0\">{3}</span></div>",
                        AppViewPage.ID, sStyleString, DesignerRegion.DesignerRegionAttributeName, emptyAppViewHint);
			}
			else if(AppViewPage.Header.TemplateContainer!=null && AppViewPage.Content.TemplateContainer!=null)
			{
				regions.Add(new EditableDesignerRegion(this, "Header", false));
				regions.Add(new EditableDesignerRegion(this, "Content", false));

				AppViewPage.Header.TemplateContainer.Style.Add(HtmlTextWriterStyle.Height, string.Format("{0}px", 45));
				AppViewPage.Content.TemplateContainer.Style.Add(HtmlTextWriterStyle.Height, string.Format("{0}px", AppViewPage.Height.Value - 45));
				AppViewPage.Header.TemplateContainer.Attributes[DesignerRegion.DesignerRegionAttributeName] = "0";
				AppViewPage.Content.TemplateContainer.Attributes[DesignerRegion.DesignerRegionAttributeName] = "1";
			}
			return base.GetDesignTimeHtml();
		}

		protected override void OnClick(DesignerRegionMouseEventArgs e)
		{
			if (e.Region != null && e.Region.Name == "#addtemplates") 
			{
				OnAddTemplates();
			}
			base.OnClick(e);
		}

		internal void OnAddTemplates()
		{
			// First, dirty Page, workaround for VS2008 SP1 issue 
			// fix for bug 5487 [Accordion/C1NavPanel] Pane is disappeared when place control inside C1Window and click on ShowOnLoad checkbox
			bool isControlEnabled = this.AppViewPage.Enabled;
			TypeDescriptor.GetProperties(this.AppViewPage)["Enabled"].SetValue(this.AppViewPage, !isControlEnabled);
			TypeDescriptor.GetProperties(this.AppViewPage)["Enabled"].SetValue(this.AppViewPage, isControlEnabled);
			//
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
			if (host != null)
			{

				
				//Accordion viewControl = this.ViewControl as Accordion;
				using (DesignerTransaction dt = host.CreateTransaction("Add new pane"))
				{

					AppViewPage.Header.Template = CreateTemplate();
					AppViewPage.Content.Template = CreateTemplate();
					UpdateDesignTimeHtml();
					dt.Commit();
					VSDesignerHelperMethods.UpdateDesigner(this.Component, "Header");
					VSDesignerHelperMethods.UpdateDesigner(this.Component, "Content");
				}
			}

		}

		private ITemplate CreateTemplate()
		{
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
			return ControlParser.ParseTemplate(host, " ");
		}
		

		public override void Initialize(System.ComponentModel.IComponent component)
		{
			base.Initialize(component);
			// Turn on template editing
			SetViewFlags(ViewFlags.TemplateEditing, true);
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			
		}


		public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
		{
			IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
			if (host != null)
			{
				ITemplate template = null; ;
				if (region.Name == "Header") {
					template = AppViewPage.Header.Template;
				}
				else if (region.Name == "Content") {
					template = AppViewPage.Content.Template;
				}
					

				if (template != null)
					return ControlPersister.PersistTemplate(template, host);
			}

			return String.Empty;
		}

		public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
		{
			if (content == null)
				return;
			IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
			if (host != null)
			{
				ITemplate template = ControlParser.ParseTemplate(host, content);

				if (template != null)
				{
					if (region.Name == "Header")
						AppViewPage.Header.Template = template;
					else if (region.Name == "Content")
						AppViewPage.Content.Template = template;
				}
			}
			//base.SetEditableDesignerRegionContent(region, content);
		}

		
		private C1AppViewPage AppViewPage
		{
			get
			{
				return Component as C1AppViewPage;
			}
		}

	}
}
