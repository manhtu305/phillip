﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1AppView
{
	using C1.Web.Wijmo.Controls.C1AppView;
	using C1.Web.Wijmo.Controls.Design.Localization;

	public partial class C1AppViewDesignerForm : C1BaseItemEditorForm
	{
		#region Custom Element Types enumeration
		/// <summary>
		/// Element Types enumerations used by Designer
		/// </summary>
		private enum ItemType
		{
			/// <summary>
			/// Indicates what default item type must be used.
			/// </summary>
			None,
			/// <summary>
			/// AppView Item
			/// </summary>
			AppViewItem,
			/// <summary>
			/// AppView.
			/// </summary>
			AppViewRoot
		}


		/// <summary>
		/// Gets the type of the given item
		/// </summary>
		/// <param name="item">Given item for checking its type</param>
		/// <returns>The <seealso cref="ItemType"/> of given item</returns>
		private ItemType GetItemType(object item)
		{
			// Sets the default item type
			ItemType elementType = ItemType.AppViewItem;

			if (item is C1AppView)
			{
				elementType = ItemType.AppViewRoot;
			}
			else if (item is C1AppViewItem)
			{
				elementType = ItemType.AppViewItem;
			}
			return elementType;
		}

		/// <summary>
		/// Fills the allowable types that can compose the control. 
		/// You should load default item in first place. 
		/// </summary>
		/// <param name="itemInfo">List of available control items to be filled</param>
		protected override List<C1ItemInfo> FillAvailableControlItems()
		{
			List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();
			C1ItemInfo item;

			item = new C1ItemInfo();
			item.ItemType = ItemType.AppViewItem;
			item.EnableChildItems = false;
			item.ContextMenuStripText = C1Localizer.GetString("AppView.DesignerForm.ContextMenuAppViewItem");
			item.NodeImage = Properties.Resources.LinkItemIco; //GetImageFromResource("LinkItemIco"); 
			item.DefaultNodeText = "AppViewItem";//AppView Item
			item.Visible = true;
			item.Default = true;
			itemsInfo.Add(item);

			item = new C1ItemInfo();
			item.ItemType = ItemType.AppViewRoot;
			item.EnableChildItems = true;
			item.ContextMenuStripText = "AppView";
			item.NodeImage = Properties.Resources.RootItemIco; //GetImageFromResource("RootItemIco");
			item.DefaultNodeText = "AppView";
			item.Visible = false;
			item.Default = false;
			itemsInfo.Add(item);

			return itemsInfo;
		}

		#endregion  // Custom Element Types enumeration
		private C1AppView _appView;
		private C1AppViewDesigner _designer;
		// clipboard for current control
		private Stream _clipboardData;
		private C1ItemInfo _clipboardType;

		public C1AppViewDesignerForm(C1AppView appView, C1AppViewDesigner designer)
			: base(appView)
		{
			try
			{
				this._appView = appView;
				this._designer = designer;
				InitializeComponent();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		#region ** C1BaseItemEditorForm implementations
		const string NEW_LINE = "\r\n";
		/// <summary>
		/// Shows current control into preview tab
		/// </summary>
		/// <param name="previewer">The web browser previewer</param>
		internal override void ShowPreviewHtml(C1WebBrowser previewer)
		{
			try
			{
				string sResultBodyContent = _designer.GetDesignTimeHtml();

				string sDocumentContent = "";
				sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
				sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
				sDocumentContent += "<head>" + NEW_LINE;
				sDocumentContent += "</head>" + NEW_LINE;
				sDocumentContent += "<body>" + NEW_LINE;
				sDocumentContent += sResultBodyContent + NEW_LINE;
				sDocumentContent += "</body>" + NEW_LINE;
				sDocumentContent += "</html>" + NEW_LINE;

				previewer.DocumentWrite(sDocumentContent, this._appView);               
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
			}
		}

		/// <summary>
		/// Called to initialize the treeview with the object hierarchy.
		/// </summary>
		/// <param name="mainTreeView"></param>
		protected override void LoadControl(TreeView mainTreeView)
		{
			string nodeText;
			object nodeTag;
			C1ItemInfo itemInfo;

			// populate mainTreeView with C1 control items
			mainTreeView.BeginUpdate();
			mainTreeView.Nodes.Clear();

			TreeNode rootMenuItem = new TreeNode();
			itemInfo = GetItemInfo(GetItemType(_appView));
			nodeText = _appView.ID == null ? itemInfo.DefaultNodeText : _appView.ID.ToString();
			nodeTag = new C1NodeInfo(_appView, itemInfo, nodeText);
			SetNodeAttributes(rootMenuItem, nodeText, nodeTag);

			foreach (C1AppViewItem menuItem in _appView.Items)
			{
				TreeNode menuItemNode = new TreeNode();
				nodeText = menuItem.Text;
				itemInfo = GetItemInfo(GetItemType(menuItem));
				nodeTag = new C1NodeInfo(menuItem, itemInfo, nodeText);
				SetNodeAttributes(menuItemNode, nodeText, nodeTag);

				IterateChildNodes(menuItemNode, menuItem);

				rootMenuItem.Nodes.Add(menuItemNode);
			}

			mainTreeView.Nodes.Add(rootMenuItem);
			mainTreeView.SelectedNode = mainTreeView.Nodes[0];
			mainTreeView.ExpandAll();
			mainTreeView.EndUpdate();
		}

		/// <summary>
		/// Loads nested items from current item
		/// </summary>
		/// <param name="menuItemNode">TreeNode parent</param>
		/// <param name="item">Control item</param>
		private void IterateChildNodes(TreeNode menuItemNode, C1AppViewItem item)
		{
			menuItemNode.Nodes.Clear();
		}

		/// <summary>
		/// Gets de item info according to the given item type
		/// </summary>
		/// <param name="itemType">Item type</param>
		/// <returns>C1ItemInfo object that contains the item info</returns>
		private C1ItemInfo GetItemInfo(ItemType itemType)
		{
			foreach (C1ItemInfo itemInfo in base.ItemsInfo)
			{
				if ((ItemType)itemInfo.ItemType == itemType)
					return itemInfo;
			}
			return null;
		}

		protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
		{
			base.AllowAdd = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewRoot });
			base.AllowInsert = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewItem });
			base.AllowChangeType = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewItem });
			base.AllowCopy = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewItem, });
			base.AllowPaste = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewItem, ItemType.AppViewRoot });
			base.AllowCut = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewItem });
			base.AllowDelete = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewItem });
			base.AllowRename = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AppViewRoot, ItemType.AppViewItem });
			base.AllowMoveUp = EnableMoveUp();
			base.AllowMoveDown = EnableMoveDown();
			base.AllowMoveLeft = EnableMoveLeft(itemInfo, parentItemInfo);
			base.AllowMoveRight = EnableMoveRight(itemInfo, previousItemInfo);

			base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
		}

		/// <summary>
		/// Enables given action if it is in given types.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo info.</param>
		/// <param name="types">Allowed types for given action.</param>
		private bool EnableActionByItemType(C1ItemInfo itemInfo, ItemType[] types)
		{
			bool enable = false;
			if (itemInfo != null)
			{
				foreach (ItemType type in types)
				{
					enable |= itemInfo.ItemType.Equals(type);
				}
			}
			return enable;
		}

		/// <summary>
		/// Enables current node to be added as a child of its available node to the left
		/// </summary>
		private bool EnableMoveLeft(C1ItemInfo itemInfo, C1ItemInfo parenItemInfo)
		{
			return false;
		}

		/// <summary>
		/// Enables current node to be added as a child of its available node to the right
		/// </summary>
		private bool EnableMoveRight(C1ItemInfo itemInfo, C1ItemInfo previousItemInfo)
		{
			return false;
		}

		private bool EnableMoveUp()
		{
			return true;
		}

		private bool EnableMoveDown()
		{
			return true;
		}

		/// <summary>
		/// Creates the C1NodeInfo object that will contain both the specific C1 control item depending on the itemInfo
		/// and given itenInfo.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo object.</param>
		/// <param name="nodesList">Nodes that currently exist into the list where the new created item 
		/// will be inserted.</param>
		/// <returns>Returns a C1 control that depends on the C1ItemInfo. Given nodesList
		/// could be used to obtain a new numbered name.</returns>
		protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
		{
			C1NodeInfo nodeInfo;
			string nodeText;

			C1AppViewItem item = CreateNewItem(itemInfo);

			nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);

			// set the text for created item
			item.ID = _appView.ClientID + "_" + nodeText;

			// create a new node info
			nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);

			// set nodeText property
			nodeInfo.NodeText = nodeText;

			return nodeInfo;
		}

		/// <summary>
		///  Indicates when an item was inserted in a specific position.
		/// </summary>
		/// <param name="item">The inserted item.</param>
		/// <param name="destinationItem">The destination item that will contain inserted item.</param>
		/// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
		protected override void Insert(object item, object destinationItem, int destinationIndex)
		{
			try
			{
				_appView.Items.Insert(destinationIndex, (C1AppViewItem)item);
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// <summary>
		///  Indicates when an item was deleted.
		/// </summary>
		/// <param name="item">The deleted item.</param>
		/// <param name="parent">The parent that contains the deleted item.</param>
		protected override void Delete(object item)
		{
			_appView.Items.Remove((C1AppViewItem)item);
		}

		protected override void Copy(object item, C1ItemInfo itemInfo)
		{
			if (_clipboardData == null)
				_clipboardData = new MemoryStream();
			Copy(_clipboardData, item);

			this._clipboardType = itemInfo;
			base.ClipboardData = true;
		}

		private void Copy(Stream buffer, object target)
		{
			C1AppViewSerializer serializer = new C1AppViewSerializer(target);
			buffer.SetLength(0);
			serializer.SaveLayout(buffer);
		}

		protected override void Paste(TreeNode destinationNode)
		{
			Paste(_clipboardData, destinationNode);
		}

		private void Paste(Stream buffer, TreeNode destNode)
		{

			C1AppViewItem item = new C1AppViewItem(_appView);
			C1AppViewSerializer serializer = new C1AppViewSerializer(item);
			buffer.Seek(0, SeekOrigin.Begin);
			// lets to serializer to retrieve data from clipboard
			serializer.LoadLayout(buffer, LayoutType.All);

			// retrieve the AccordionItem form destination node
			object destinationObject = ((C1NodeInfo)destNode.Tag).Element;

			_appView.Items.Add((C1AppViewItem)item);

			// create node info
			C1NodeInfo nodeInfo = new C1NodeInfo(item, _clipboardType, item.Text);

			// create new treenode to be added on destination node
			TreeNode node = new TreeNode();
			// set node text and node tag
			SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);

			IterateChildNodes(node, item);
			if (destinationObject is C1AppViewItem)
			{
				destNode.Parent.Nodes.Add(node);
			}
			else
			{
				destNode.Nodes.Add(node);
				destNode.ExpandAll();
			}

		}

		/// <summary>
		/// Creates a new item which its type depends on itemsInfo.
		/// </summary>
		/// <param name="itemInfo">The info of new item to be created.</param>
		/// <returns>New C1AppViewItem</returns>
		private C1AppViewItem CreateNewItem(C1ItemInfo itemInfo)
		{
			return new C1AppViewItem(_appView);
		}

		protected override void SaveToXML(string fileName)
		{
			try
			{
				_appView.SaveLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		protected override void LoadFromXML(string fileName)
		{
			try
			{
				_appView.LoadLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		#endregion
	}
}
