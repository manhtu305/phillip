﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI.Design;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.Design.C1AppView
{
	using C1.Web.Wijmo.Controls.C1AppView;
	using C1.Web.Wijmo.Controls.Design.Localization;

	[SupportsPreviewControl(false)]
    public class C1AppViewDesigner : C1ControlDesinger
	{
		C1AppView _c1AppView;

		/// <summary>
		/// Create and return action list.
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new AppViewDesignerActionList(this));

				return actionLists;
			}
		}

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);

			this._c1AppView = (C1AppView)component;
		}

		public override String GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			if (this._c1AppView.Items.Count == 0)
			{
				string s = "<div style=\"width:{0};height:{1};border:solid 1px #bbbbbb;\"></div>";
				s = string.Format(s,
					_c1AppView.Width.IsEmpty ? "300px" : _c1AppView.Width.ToString(),
					_c1AppView.Height.IsEmpty ? "250px" : _c1AppView.Height.ToString()
					);
				return s;
			}

			for (int i = 0; i < this._c1AppView.Items.Count; i++)
			{
				DesignerRegion region = new DesignerRegion(this, i.ToString(), true);
				region.EnsureSize = true;
				regions.Add(region);
			}

			regions.Add(new EditableDesignerRegion(this, "header", false));
			regions.Add(new EditableDesignerRegion(this, "content", false));
			string result = base.GetDesignTimeHtml(regions);
			return result;
		}

		internal bool EditAppViewItems()
		{
			bool accept = false;
			// Save original control data, to restore data in case of DialogResult is 'Cancel'
			List<TreeData> originalData = SaveOriginalControlData(((C1AppView)Component));

			C1AppViewDesignerForm _editorForm = new C1AppViewDesignerForm(((C1AppView)Component), this);
			accept = ShowControlEditorForm(((C1AppView)Component), _editorForm);

			if (!accept)
				RestoreOriginalData(((C1AppView)Component), originalData);
			ClearOriginalData(originalData);

			return accept;
		}

		public bool ShowControlEditorForm(object control, System.Windows.Forms.Form _editorForm)
		{
			System.Windows.Forms.DialogResult dr;
			IServiceProvider serviceProvider = ((IComponent)control).Site;
			if (serviceProvider != null)
			{
				IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
				DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("Accordion.EditControl"));
				using (trans)
				{
					System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
					IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

					if (service != null)
					{
						ccs.OnComponentChanging(control, null);
						dr = System.Windows.Forms.DialogResult.None;
						try
						{
							dr = service.ShowDialog(_editorForm);
						}
						catch (Exception ex)
						{
							System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
						}
					}
					else
						dr = System.Windows.Forms.DialogResult.None;
					if (dr == System.Windows.Forms.DialogResult.OK)
					{
						ccs.OnComponentChanged(control, null, null, null);
						trans.Commit();
					}
					else
					{
						trans.Cancel();
					}
				}
			}
			else
				dr = _editorForm.ShowDialog();

			_editorForm.Dispose();
			_editorForm = null;

			return dr == System.Windows.Forms.DialogResult.OK;
		}

		#region Save and Restore Original data

		/// <summary>
		/// TreeData is used to save any object with hierarchical structure
		/// </summary>
		private class TreeData
		{
			public TreeData(object dataObj)
			{
				_dataObj = dataObj;
				_items = new List<TreeData>();
			}

			public List<TreeData> PanesData
			{
				get
				{
					return _items;
				}
			}

			public object DataObj
			{
				get { return _dataObj; }
			}

			private readonly object _dataObj;
			private List<TreeData> _items;
		}

		private void SaveDataItem(List<TreeData> parent, object item)
		{
			TreeData itemData = new TreeData(item);
			parent.Add(itemData);
		}

		/// <summary>
		/// Save original control data 
		/// </summary>
		/// <param name="control">Control to be saved</param>
		/// <returns>Returns a list of <see cref="TreeData"/></returns>
		private List<TreeData> SaveOriginalControlData(object control)
		{
			List<TreeData> savedData = new List<TreeData>();
			if (control is C1AppView)
			{
				C1AppView menu = (C1AppView)control;

				foreach (C1AppViewItem item in ((C1AppView)control).Items)
				{
					SaveDataItem(savedData, item);
				}
			}
			return savedData;
		}

		/// <summary>
		/// Restores item data
		/// </summary>
		/// <param name="parent">Object to be restored</param>
		/// <param name="data">Data to be restored to parent object</param>
		private void RestoreDataItem(object parent, TreeData data)
		{
			if (parent is C1AppViewItem)
			{
				foreach (TreeData d in data.PanesData)
				{
					C1AppViewItem item = (C1AppViewItem)d.DataObj;

					RestoreDataItem(item, d);
				}
			}
		}

		/// <summary>
		/// Restores saved data to original state when edition was canceled
		/// </summary>
		/// <param name="control">Control to be restored</param>
		/// <param name="originalData">Original Data</param>
		private void RestoreOriginalData(object control, List<TreeData> originalData)
		{
			if (control is C1AppView)
			{
				((C1AppView)control).Items.Clear();
				foreach (TreeData d in originalData)
				{
					C1AppViewItem item = (C1AppViewItem)d.DataObj;
					((C1AppView)control).Items.Add(item);
					RestoreDataItem(item, d);
				}
			}
		}

		private void ClearOriginalData(List<TreeData> originalData)
		{
			originalData.Clear();
		}
		#endregion
	}

	/// <summary>
	/// Manages our designer verbs
	/// </summary>
	internal class AppViewDesignerActionList : DesignerActionListBase
	{
		private C1AppViewDesigner _designer;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Justification = "Security handled by base class")]
		public AppViewDesignerActionList(C1AppViewDesigner designer)
			: base(designer)
		{
			_designer = designer;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Justification = "Security handled by base class")]
		public override DesignerActionItemCollection GetSortedActionItems()
		{

			DesignerActionItemCollection items = new DesignerActionItemCollection();

			DesignerActionMethodItem editAppViewItems = new DesignerActionMethodItem(this, "EditAppViewItems", C1Localizer.GetString("AppView.SmartTag.EditAppViewItems"), "", C1Localizer.GetString("AppView.SmartTag.EditAppViewItemsDescription"), true);
			items.Add(editAppViewItems);

			AddBaseSortedActionItems(items);
			return items;
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called via reflection")]
		public void EditAppViewItems()
		{
			this._designer.EditAppViewItems();
		}

		internal override bool DisplayThemeSwatch()
		{
			return true;
		}
	}
}
