﻿

using C1.Web.Wijmo.Controls.Design.C1ThemeRoller;
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    internal class WijmoControlThemeEditor : UITypeEditor
    {
        private IWindowsFormsEditorService _editorService;
        private IComponent _component;

        public WijmoControlThemeEditor()
        {
        }

        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            ISelectionService selectionService = (ISelectionService)provider.GetService(typeof(ISelectionService));
            if (selectionService != null)
            {
                _component = (IComponent)selectionService.PrimarySelection;
                if(_component == null)
                    _component = ((DesignerActionList)context.Instance).Component;
            }
            
            _editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (_editorService == null)
                return null;

            ImageList imageList = new ImageList();            
            ListView listView = new ListView();
            listView.BorderStyle = BorderStyle.None;
            listView.LargeImageList = imageList;
            listView.Size = new Size(161, 190);
            listView.Click += new EventHandler(this.ListView1_Click);
			NewThemeForm.FillListView(listView, imageList, false, Utilites.GetRootProjectItem(_component));
            if (imageList.Images.Count > 0)
            {
                ListViewItem item = listView.Items[(string)(TypeDescriptor.GetProperties(_component)["Theme"].GetValue(_component))];
                if (item != null)
                    item.Selected = true;
                _editorService.DropDownControl(listView);
                if (listView.SelectedItems.Count != 0)
                {
                    return GetSelectedThemePath(listView);
                }
                
            }
            return base.EditValue(context, provider, value);
        }

        private string GetSelectedThemePath(ListView listView)
        {
            string themePath = string.Empty;
            if (listView.SelectedItems.Count > 0)
            {
                ListViewItem selectItem = listView.SelectedItems[0];
                ThemeType selectedThemeType = (ThemeType)selectItem.Group.Tag;
                switch (selectedThemeType)
                {
                    case ThemeType.Wijmo:
                    case ThemeType.Custom:
                        themePath = ThemeProcesser.GetThemePath(selectItem.Text, selectedThemeType, Utilites.GetRootProjectItem(_component));
                        break;
                    case ThemeType.JQueryUI:
                    case ThemeType.NotSet:
                    default:
                        break;
                }
            }
            return themePath;
        }

        private void ListView1_Click(object sender, EventArgs e)
        {
            if (_editorService != null)
                _editorService.CloseDropDown();
        }
    }
}
