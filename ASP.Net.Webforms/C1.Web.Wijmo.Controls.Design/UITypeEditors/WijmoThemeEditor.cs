using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Design;
using System.ComponentModel;
using System.Windows.Forms.Design;
using System.IO;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using System.Web.UI;
using C1.Web.Wijmo;
using System.Collections;

namespace C1.Web.Wijmo.Design.UITypeEditors
{
    internal class HelperListBoxTextItem : object
    {
        public string text;
        public string value;
        public int type;

        public HelperListBoxTextItem(string sText, string sValue, int iType)
        {
            text = sText;
            value = sValue;
            type = iType;
        }

        public override string ToString()
        {
            return text;
        }
    }
    
    /// <summary>
    /// VisualStyle UITypeEditor.
    /// </summary>
    public class WijmoThemeEditor : UITypeEditor
    {
		// Fields
		#region ** fields

		internal WidgetExtenderControlBase _inspectedControl;
        private ITypeDescriptorContext context;
        internal IWindowsFormsEditorService editorService;
        private System.Windows.Forms.ListBox _ListBox;

		#endregion


		/// <summary>
		/// Initializes a new instance of the <see cref="WijmoThemeEditor"/> class.
		/// </summary>
		public WijmoThemeEditor()
		{

            _ListBox = new System.Windows.Forms.ListBox();
            _ListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }




        /// <summary>
        /// Edits the specified object's value using the editor style indicated by the <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle"/> method.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that can be used to gain additional context information.</param>
        /// <param name="provider">An <see cref="T:System.IServiceProvider"/> that this editor can use to obtain services.</param>
        /// <param name="value">The object to edit.</param>
        /// <returns>
        /// The new value of the object. If the value of the object has not changed, this should return the same object it was passed.
        /// </returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {

			this._inspectedControl = GetComponent(provider) as WidgetExtenderControlBase;
            if (_inspectedControl == null)
            {
//System.Windows.Forms.MessageBox.Show("Edit value * GetComponent, control is null ! (1)");
				_inspectedControl = context.Instance as WidgetExtenderControlBase;
                if (_inspectedControl == null)
                {
//System.Windows.Forms.MessageBox.Show("Edit value * context.Instance, control is null ! (2)");    
                    return value;
                }
                
            }

            this.context = context;
            IDesignerHost service = (IDesignerHost)provider.GetService(typeof(IDesignerHost));
            
            
            this.editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            this._ListBox.Items.Clear();
            this._ListBox.Sorted = true;

            _ListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _ListBox.Click += new EventHandler(this.ListBox_Click);


            // Load embedded VisualStyles:
			WijmoThemeNameConverter wijmoThemeNameConverter = new WijmoThemeNameConverter();
			System.ComponentModel.TypeConverter.StandardValuesCollection col = wijmoThemeNameConverter.GetStandardValues(null);
			for (int i = 0; i < col.Count; i++)
			{
				HelperListBoxTextItem item11 = new HelperListBoxTextItem((string)col[i], (string)col[i], 0);
				_ListBox.Items.Add(item11);
			}
			if (_ListBox.Items.Count > 0)
			{
				for (int i = 0; i < _ListBox.Items.Count; i++)
				{
					HelperListBoxTextItem ii = (HelperListBoxTextItem)_ListBox.Items[i];
					if ((string)ii.value == ((WidgetExtenderControlBase)this._inspectedControl).Theme)
					{
						_ListBox.SelectedIndex = i;
						break;
					}

				}
				this.editorService.DropDownControl(_ListBox);
			}

            HelperListBoxTextItem selectedItem = (HelperListBoxTextItem)_ListBox.SelectedItem;
            if (selectedItem != null)
            {
                return selectedItem.value;
            }
            return value;// base.EditValue(context, provider, value);
        }

        /// <summary>
        /// Gets information about the container that hosts the current control when rendered on a design surface.
        /// </summary>
        /// <returns>An ISite that contains information about the container that the control is hosted in.</returns>
        internal ISite GetSite()
        {
            Control _control = this._inspectedControl;
            if (_control.Site != null) return _control.Site;

            for (Control control = _control.Parent; control != null; control = control.Parent)
            {
                if (control.Site != null) return control.Site;
            }

            return null;
        }

        internal static IComponent GetComponent(IServiceProvider serviceProvider)
        {
            try
            {
                ISelectionService service = (ISelectionService)serviceProvider.GetService(typeof(ISelectionService));
                return (IComponent)service.PrimarySelection;
            } catch (Exception){
                return null;
            }
        }

        /// <summary>
        /// Gets the editing style for this UITypeEditor.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        private void ListBox_Click(object sender, EventArgs e)
        {
            if (((System.Windows.Forms.ListBox)sender).Items.Count > 0)
            {
                HelperListBoxTextItem aItem = ((HelperListBoxTextItem)((System.Windows.Forms.ListBox)sender).SelectedItem);
                TypeDescriptor.GetProperties(typeof(WidgetExtenderControlBase))["Theme"].SetValue(this._inspectedControl, aItem.value);
            }
            this.editorService.CloseDropDown();
        }
    }

}
