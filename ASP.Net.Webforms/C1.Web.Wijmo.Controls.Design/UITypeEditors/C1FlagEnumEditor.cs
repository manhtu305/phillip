﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{

	/// <summary>
	/// Represents a C1FlagEnumEditor - UI type editor for flag enumeration.
	/// </summary>
	[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Unrestricted = true)]
	internal class C1FlagEnumEditor : UITypeEditor
	{
		
		/// <summary>
		/// Edits the value of the specified object using the specified 
		/// service provider and context.
		/// </summary>
		/// <param name="context">
		/// An <see cref="ITypeDescriptorContext"/> that can be used to 
		/// gain additional context information. 
		/// </param>
		/// <param name="provider">
		/// An <see cref="IServiceProvider"/> through which editing services 
		/// may be obtained.
		/// </param>
		/// <param name="value">
		/// An instance of the value being edited.
		/// </param>
		/// <returns>
		/// The new value of the object. If the value of the object hasn't 
		/// changed, this should return the same object it was passed.
		/// </returns>
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			IWindowsFormsEditorService editorService;
			
			// Do nothing if we don't have a valid service provider.
			if (provider != null)
			{
				editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

				if (editorService != null)
				{
					C1FlatEnumCheckedListBoxUI ctrlUI = new C1FlatEnumCheckedListBoxUI(this, value.GetType());

					if (ctrlUI != null)
					{
						ctrlUI.Start(editorService, value);
						editorService.DropDownControl(ctrlUI);
						value = ctrlUI.Value;
						ctrlUI.End();
					}
				}
			}

			return value;
		}

		/// <summary>
		/// Gets the editor style used by the <see cref="EditValue"/> method.
		/// </summary>
		/// <param name="context">
		/// An <see cref="ITypeDescriptorContext"/> that can be used to gain 
		/// additional context information. 
		/// </param>
		/// <returns>
		/// One of the <see cref="UITypeEditorEditStyle"/> values indicating 
		/// the provided editing style. If the method is not supported, this 
		/// method will return <b>None</b>.
		/// </returns>
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}
	}

	#region C1FlatEnumCheckedListBoxUI class
	/// <summary>
	/// A base class that will become the base for all UI editing
	/// managed through UI type editors.
	/// </summary>
	[ToolboxItem(false)]
	internal class C1FlatEnumCheckedListBoxUI : CheckedListBox
	{

		#region ** members
		private IWindowsFormsEditorService _editorService = null;
		private string[] _items = null;
		#endregion end of ** members.

		#region ** properties
		/// <summary>
		/// Gets the value chosen through the UI type editors.
		/// </summary>
		/// <value>
		/// An object value of any type.
		/// </value>
		public object Value
		{
			get
			{
				int val = 0;

				for (int idx = 0; idx < this.Items.Count; idx++)
				{
					if (this.GetItemChecked(idx))
					{
						val |= (int)Enum.Parse(this.Type, this._items[idx]);
					}
				}

				return TypeDescriptor.GetConverter(this.Type).ConvertFrom(val.ToString());
			}
		}

		private Type Type
		{
			get;
			set;
		}
		#endregion end of ** properties.

		#region ** constructors
		/// <summary>
		/// Initialize the instance of the C1FlatEnumCheckedListBoxUI class.
		/// </summary>
		/// <param name="editor">
		/// </param>
		public C1FlatEnumCheckedListBoxUI(C1FlagEnumEditor editor, Type type)
		{
			this.BorderStyle = BorderStyle.None;
			this.CheckOnClick = true;
			this.Type = type;
			string[] items = Enum.GetNames(type);

			if (items[items.Length - 1].ToLower() == "all")
			{
				this._items = new string[items.Length - 1];
				Array.Copy(items, 0, this._items, 0, this._items.Length);
			}
			else
			{
				this._items = items;
			}

			this.Items.AddRange(this._items);
		} 
		#endregion end of ** constructors.

		#region ** methods
		/// <summary>
		/// Starts up the component that handles the UI portion
		/// for the provided "Type".
		/// </summary>
		/// <param name="editorService">
		/// An <see cref="IWindowsFormsEditorService"/> instance that
		/// invoked this UI type editor.
		/// </param>
		/// <param name="value">
		/// A value that the property currently holds.
		/// </param>
		public void Start(IWindowsFormsEditorService editorService, object value)
		{
			try
			{
				this._editorService = editorService;
				this.Height = (this.ItemHeight + 1) * (this.Items.Count + 1);

				int flag = (int)value;

				for (int idx = 0; idx < this._items.Length; idx++)
				{
					this.SetItemChecked(idx, ((flag & (int)Enum.Parse(this.Type, this._items[idx])) != 0));
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.StackTrace);
			}
		}

		/// <summary>
		/// Terminates the component that handles the UI portion
		/// for the provided "Type".
		/// </summary>
		public void End()
		{
			this._editorService = null;
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);

			if ((int)(e.KeyChar) == 13)
			{
				this._editorService.CloseDropDown();
			}
		} 
		#endregion end of ** methods.
	}
	#endregion end of C1FlatEnumCheckedListBoxUI class
}

