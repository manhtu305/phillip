﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;


namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    /// <summary>
    /// A class of string array editor.
    /// </summary>
    public class StringArrayEditor : BaseArrayEditor<string>
    {
        /// <summary>
        /// Creates a new instance of the specified collection item type.
        /// </summary>
        /// <param name="itemType">The type of item to create.</param>
        /// <returns>A new instance of the specified object.</returns>
        protected override object CreateInstance(Type itemType)
        {
            if (itemType == typeof(string))
            {
                return string.Empty;
            }

            return base.CreateInstance(itemType);
        }
    }

    /// <summary>
    /// A class of double array editor.
    /// </summary>
    public class DoubleArrayEditor : BaseArrayEditor<Double>
    {
    }

    /// <summary>
    /// A class of datetime array editor.
    /// </summary>
    public class DateTimeArrayEditor : BaseArrayEditor<DateTime>
    {
        /// <summary>
        /// Creates a new instance of the specified collection item type.
        /// </summary>
        /// <param name="itemType">The type of item to create.</param>
        /// <returns>A new instance of the specified object.</returns>
        protected override object CreateInstance(Type itemType)
        {
            if (itemType == typeof(DateTime))
            {
                return DateTime.Now;
            }

            return base.CreateInstance(itemType);
        }
    }

    /// <summary>
    /// A class of array editor.
    /// </summary>
    public class BaseArrayEditor<T> : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the BaseArrayEditor class using the specified collection type.
        /// </summary>
        public BaseArrayEditor()
            : this(typeof(T[]))
        {
        }

        /// <summary>
        /// Initializes a new instance of the BaseArrayEditor class using the specified collection type.
        /// </summary>
        /// <param name="type">The type of the collection for this editor to edit.</param>
        public BaseArrayEditor(Type type)
            : base(type)
        {
        }

        /// <summary>
        /// Gets the data type that this collection contains.
        /// </summary>
        /// <returns>The data type of the items in the collection, or an System.Object if no Item property can be located on the collection.</returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(T);
        }

        /// <summary>
        /// Edits the value of the specified object using the specified service provider and context.
        /// </summary>
        /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that can be used to gain additional context information.</param>
        /// <param name="provider">A service provider object through which editing services can be obtained.</param>
        /// <param name="value">The object to edit the value of.</param>
        /// <returns>The new value of the object. If the value of the object has not changed, this should return the same object it was passed.</returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (value is T[])
            {
                List<T> items = new List<T>(value as T[]);
                items = base.EditValue(context, provider, items) as List<T>;

                return items.ToArray();
            }

            return base.EditValue(context, provider, value);
        }
    } 
}
