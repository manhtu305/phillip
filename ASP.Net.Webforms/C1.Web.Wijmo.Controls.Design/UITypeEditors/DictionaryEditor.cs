﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Diagnostics;
using System.ComponentModel.Design.Serialization;


namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    
    /// <summary>
    /// Provides a user interface that can edit string type of dictionary at design time.
    /// </summary>
    public class StringDictionaryEditor : DictionaryEditor<string, string>
    {
        /// <summary>
        /// Initializes a new instance of the StringDictionaryEditor class using the specified collection type.
        /// </summary>
        /// <param name="type">The type of the dictionary for this editor to edit.</param>
        public StringDictionaryEditor(Type type)
            : base(type)
        {  
        
        }
    }  

    /// <summary>
    /// Provides a user interface that can edit most types of dictionary at design time.
    /// </summary>
    /// <typeparam name="TKey">type of key in dictionary</typeparam>
    /// <typeparam name="TValue">type of value in dictionary</typeparam>
    public class DictionaryEditor<TKey, TValue> : CollectionEditor
    {
        /// <summary>
        /// Initializes a new instance of the DictionaryEditor class using the specified collection type.
        /// </summary>
        /// <param name="type">The type of the dictionary for this editor to edit.</param>
        public DictionaryEditor(Type type)
            : base(type)
        {
            
        }

        /// <summary>
        /// Gets the value of form text.
        /// </summary>
        protected virtual string FormText 
        { 
            get 
            {
                return "Dictionary Editor";
            } 
        }

        /// <summary>
        /// Creates a new form to display and edit the current collection.
        /// </summary>
        /// <returns>A System.ComponentModel.Design.CollectionEditor.CollectionForm to provide as the user interface for editing the collection.</returns>
        protected override CollectionEditor.CollectionForm CreateCollectionForm()
        {
            CollectionEditor.CollectionForm form = base.CreateCollectionForm();

            form.Text = this.FormText;

            Type formType = form.GetType();
            PropertyInfo pi = formType.GetProperty("CollectionEditable", BindingFlags.NonPublic | BindingFlags.Instance);
            pi.SetValue(form, true, null);
            
            return form;
        }

        /// <summary>
        /// Edits the value of the specified object using the specified service provider and context.
        /// </summary>
        /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that can be used to gain additional context information.</param>
        /// <param name="provider">A service provider object through which editing services can be obtained.</param>
        /// <param name="value">The object to edit the value of.</param>
        /// <returns>The new value of the object. If the value of the object has not changed, this should return the same object it was passed.</returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            IList<EditablePair<TKey, TValue>> list = new List<EditablePair<TKey, TValue>>() { };
            if (value is IDictionary)
            {
                IDictionary<TKey, TValue> dict = value as IDictionary<TKey, TValue>;

                foreach (KeyValuePair<TKey, TValue> entry in dict)
                {
                    EditablePair<TKey, TValue> entry2 = new EditablePair<TKey, TValue>(entry.Key, entry.Value);
                    list.Add(entry2);
                }
            }

            var editValue = base.EditValue(context, provider, list);

            IList<EditablePair<TKey, TValue>> list2 = editValue as IList<EditablePair<TKey, TValue>>;
            IDictionary<TKey, TValue> newDict = new Dictionary<TKey, TValue>();
            foreach (EditablePair<TKey, TValue> pair in list2)
            {
                newDict.Add(pair.Key, pair.Value);
            }

            return newDict;
        }

        /// <summary>
        /// Gets the data type that this collection contains.
        /// </summary>
        /// <returns>The data type of the items in the collection, or an System.Object if no Item property can be located on the collection.</returns>
        protected override Type CreateCollectionItemType()
        {
            return typeof(EditablePair<TKey, TValue>);
        }

        /// <summary>
        /// Creates a new instance of the specified collection item type.
        /// </summary>
        /// <param name="itemType">The type of item to create.</param>
        /// <returns>A new instance of the specified object.</returns>
        protected override object CreateInstance(Type itemType)
        {
            TKey key;
            TValue value;
            key = this.KeyInstance;
            value = this.ValueInstance;

            if (key == null)
            {
                key = default(TKey);
            }
            if (value == null)
            {
                value = default(TValue);
            }

            EditablePair<TKey, TValue> pair = new EditablePair<TKey, TValue>(key, value);

            return pair;
        }

        private TKey KeyInstance
        {
            get 
            {
                Type t = typeof(TKey);
                if (t.IsPrimitive || t.IsEnum)
                    return default(TKey);
                else if (t == typeof(string))
                    return (TKey)(object)String.Empty;
                else
                    return Activator.CreateInstance<TKey>();
            }
        }

        private TValue ValueInstance
        {
            get
            {
                Type t = typeof(TValue);
                if (t.IsPrimitive || t.IsEnum)
                    return default(TValue);
                else if (t == typeof(string))
                    return (TValue)(object)String.Empty;
                else
                    return Activator.CreateInstance<TValue>();
            }
        }

        /// <summary>
        /// Gets an array of objects containing the specified collection.
        /// </summary>
        /// <param name="editValue">The collection to edit.</param>
        /// <returns>An array containing the collection objects, or an empty object array if the specified collection does not inherit from System.Collections.ICollection.</returns>
        protected override object[] GetItems(object editValue)
        {
            if (editValue == null)
            {
                return new object[0];
            }

            IList<EditablePair<TKey, TValue>> dict = editValue as IList<EditablePair<TKey, TValue>>;

            return dict.ToArray();
        }

        /// <summary>
        /// Sets the specified array as the items of the collection.
        /// </summary>
        /// <param name="editValue">The collection to edit.</param>
        /// <param name="value">An array of objects to set as the collection items.</param>
        /// <returns>The newly created collection object or, otherwise, the collection indicated by the editValue parameter.</returns>
        protected override object SetItems(object editValue, object[] value)
        {
            if (value == null)
                throw new ArgumentNullException("value can't be null!");

            IList<EditablePair<TKey, TValue>> dict = editValue as IList<EditablePair<TKey, TValue>>;

            dict.Clear();
            foreach (EditablePair<TKey, TValue> entry in value)
            {
                dict.Add(new EditablePair<TKey, TValue>(entry.Key, entry.Value));
            }
            return dict;
        }

        /// <summary>
        /// Retrieves the display text for the given list item.
        /// </summary>
        /// <param name="value">The list item for which to retrieve display text.</param>
        /// <returns>The display text for value.</returns>
        protected override string GetDisplayText(object value)
        {
            EditablePair<TKey, TValue> pair = value as EditablePair<TKey, TValue>;
            if (pair != null)
                return string.Format(CultureInfo.CurrentCulture, "[{0}:{1}]", pair.Key, pair.Value);
            else
                return base.GetDisplayText(value);
        }
    }

    internal class EditablePair<TKey, TValue>
    {
        public EditablePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
        public TKey Key { get; set; }
        public TValue Value { get; set; }
    }

}
