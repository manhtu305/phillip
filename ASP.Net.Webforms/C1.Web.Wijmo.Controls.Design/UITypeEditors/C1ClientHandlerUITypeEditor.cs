using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Design;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using System.Windows.Forms.Design;
using System.Reflection;
using System.Drawing;
using System.Threading;
using System.Web.UI.Design.WebControls;
using System.Collections;
using C1.Web.Wijmo.Controls.Design.Utils;
using C1.Web.Wijmo.Controls.Design.Localization;
using C1.Web.Wijmo.Controls.Design;
using C1.Web.Wijmo.Controls;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    /// <summary>
    /// Property editor for client side handlers.
    /// </summary>
    public class C1ClientHandlerUITypeEditor : UITypeEditor
    {
        // Fields
        private IWindowsFormsEditorService _editorService;
        private ToolTip _toolTip;
        private string _selectedJsFunctionName;
        private ListBox _listBoxJsFunctionsNames;
        private int _prevFunctionNameIndex = -1;

        /// <summary>
        /// Construcror.
        /// </summary>
        public C1ClientHandlerUITypeEditor()
        {
            this._listBoxJsFunctionsNames = new ListBox();
            this._listBoxJsFunctionsNames.BorderStyle = BorderStyle.None;
            this._toolTip = new ToolTip();
        }
		#region ** helper static methods

		//private static readonly Guid DesignTimeEnvironmentCLSID = new Guid(0x4a72314, 0x32e9, 0x48e2, 0x9b, 0x87, 0xa6, 0x36, 3, 0x45, 0x4f, 0x3e);
		private static bool isVs2008 = false;
		private static bool isVs2010 = false;

		/// <summary>
		/// Gets whether the current IDE is VS2008.
		/// </summary>
		public static bool IsVS2008
		{
			get
			{
				if (!isVs2008)
				{
					string fileName = AppDomain.CurrentDomain.BaseDirectory + "devenv.exe";

					try
					{
						System.Diagnostics.FileVersionInfo fversinfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(fileName);

						if (fversinfo.FileVersion.StartsWith("9"))
						{
							isVs2008 = true;
						}
					}
					catch
					{
					}
				}

				return isVs2008;
			}
		}

		/// <summary>
		/// Gets whether the current IDE is VS2010.
		/// </summary>
		public static bool IsVS2010
		{
			get
			{
				if (!isVs2010)
				{
					string fileName = AppDomain.CurrentDomain.BaseDirectory + "devenv.exe";

					try
					{
						System.Diagnostics.FileVersionInfo fversinfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(fileName);

						if (fversinfo.FileVersion.StartsWith("10"))
						{
							isVs2010 = true;
						}
					}
					catch
					{
					}
				}

				return isVs2010;
			}
		}

		#endregion
		/// <summary>
        /// Inspected WebControl.
        /// </summary>
        protected System.Web.UI.Control _inspectedControl;

        /// <summary>
        /// Type Descriptor Context.
        /// </summary>
        protected ITypeDescriptorContext _context;

        /// <summary>
        /// Service Provider.
        /// </summary>
        protected IServiceProvider _provider;        

        /// <summary>
        /// Override this method if you want to change comment 
        /// what will be added to new client side handlers.
        /// </summary>
        public virtual string ClientSideHandlerDefaultInnerComment
        {
            get
            {
                string s = "";
                s += "  //\r\n";
                s += "  // Put your code here.\r\n";
                s += "  //";
                return s;
            }
        }

        /// <summary>
        /// EditValue
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            // null test for _inspectedControl is needed here so that classes derived from C1ClientHandlerUITypeEditor can set _inspectedControl be for EditValue method is called.
            // Please look at C1SuperPanelClientHandlerUITypeEditor
            if (_inspectedControl == null)
            {
                _inspectedControl = context.Instance as System.Web.UI.Control;
                if (_inspectedControl == null)
                {
                    return value;
                }
            }

            string id = _inspectedControl.ID;

            this._provider = provider;
            this._context = context;
            IDesignerHost host1 = provider.GetService(typeof(IDesignerHost)) as IDesignerHost;
            System.Web.UI.Design.ControlDesigner aControlDesigner = (System.Web.UI.Design.ControlDesigner)host1.GetDesigner(_inspectedControl);
            
            this._listBoxJsFunctionsNames.Items.Clear();
            this._listBoxJsFunctionsNames.Sorted = false;

            ClientScriptItemCollection scriptNodes = VSDesignerHelperMethods.GetClientScriptsInDocument(aControlDesigner);

            if (scriptNodes == null)
            {
                return value;
            }
            IEnumerator scriptNodesEnum = scriptNodes.GetEnumerator();
            while (scriptNodesEnum.MoveNext())
            {
                ClientScriptItem curScriptNode = (ClientScriptItem)scriptNodesEnum.Current;
                if ((curScriptNode.Source != null && curScriptNode.Source.Length > 0)
                    || ((string.Compare(curScriptNode.Type, "text/javascript", true) != 0) && (string.Compare(curScriptNode.Language, "javascript", true) != 0)))
                {
                    continue;
                }
                string curScriptText = curScriptNode.Text;
                string text2 = "";

                int indFunc = 0;
                int ind1 = 0;

                while (indFunc >= 0 && ind1 >= 0)
                {
                    indFunc = curScriptText.IndexOf("function", indFunc);
                    if (indFunc >= 0)
                    {
                        if ((this.FindInCComment(curScriptText, indFunc) 
                            || this.FindInSlashComment(curScriptText, indFunc)) 
                            || (this.FindInQuotes(curScriptText, indFunc) 
                            || this.FindInNestedFunction(curScriptText, indFunc)))
                        {
                            indFunc++;
                            continue;
                        }
                        ind1 = curScriptText.IndexOf("(", indFunc);
                        if (ind1 >= 0)
                        {
                            text2 = curScriptText.Substring(indFunc + 8, (ind1 - indFunc) - 8).Trim();
                            if ((text2 != null) && (text2.Length > 0))
                            {
                                this._listBoxJsFunctionsNames.Items.Add(text2);
                            }
                            indFunc = ind1;
                            continue;
                        }
                    }
                }
            }
            
            this._listBoxJsFunctionsNames.Sorted = true;
            // added by YanKun@20100329 for fixing #9890
            // because if sorted property of listbox is true, the newly added item will be placed at the sorted position.
            this._listBoxJsFunctionsNames.Sorted = false;
            // end YanKun@20100329

//#if GRAPECITY
//            this.lbFunctionNames.Items.Insert(0, "新規クライアント側ハンドラの追加");
//#else
            this._listBoxJsFunctionsNames.Items.Insert(0, C1Localizer.GetString("Base.ClientHandlerUITypeEditor.AddNewHandler"));
//#endif
            if (!_dropDownEventAdded)
            {
                _dropDownEventAdded = true;
                this._listBoxJsFunctionsNames.Click += new EventHandler(this.ListBox_Click);
                this._listBoxJsFunctionsNames.MouseMove += new MouseEventHandler(this.ListBox_MouseMove);
            }
            
            this._editorService = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
            if (this._editorService == null)
            {
                return value;
            }
            this._editorService.DropDownControl(this._listBoxJsFunctionsNames);
            if (this._listBoxJsFunctionsNames.SelectedIndex == -1)
            {
                return value;
            }
            if (this._listBoxJsFunctionsNames.SelectedIndex > 0)
            {
                return this._listBoxJsFunctionsNames.SelectedItem;
            }

            // Determine function name:
            int uniqCount = 1;
            this._selectedJsFunctionName = id + "_" + context.PropertyDescriptor.Name;

            while (this._selectedJsFunctionName.Length > 0
                && this._listBoxJsFunctionsNames.Items.Contains(this._selectedJsFunctionName))
            {
                this._selectedJsFunctionName = id + "_" + context.PropertyDescriptor.Name + uniqCount;
                uniqCount++;
            }

            if (this._selectedJsFunctionName.Length == 0)
                return "";

            // Determine ClientSide Handler Arguments:
            string sClientEventArgs = "";
			WidgetEventAttribute attr = (WidgetEventAttribute)context.PropertyDescriptor.Attributes[typeof(WidgetEventAttribute)];
            if (attr != null)
                sClientEventArgs = attr.Arguments;

            // Add new client function to document:
            string newClientHandlerText = "function " + this._selectedJsFunctionName + "(" + sClientEventArgs + "){\n" + ClientSideHandlerDefaultInnerComment + "\n};\n";
            VSDesignerHelperMethods.AddScriptBlockToDocument(aControlDesigner, newClientHandlerText);

            // Listen when designer change component and switch to source code.
            _componentChangeService = host1.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            _componentChangedEventHandler = new ComponentChangedEventHandler(_componentChangeService_ComponentChanged);
            _componentChangeService.ComponentChanged += _componentChangedEventHandler;
            
            
            _inspectedControl = null;
            return this._selectedJsFunctionName;
        }

        private IComponentChangeService _componentChangeService;
        private ComponentChangedEventHandler _componentChangedEventHandler;
        private bool _dropDownEventAdded;

        private void _componentChangeService_ComponentChanged(object sender, ComponentChangedEventArgs e)
        {
            if (_componentChangedEventHandler != null)
            {
                _componentChangeService.ComponentChanged -= _componentChangedEventHandler;

                //Add comments by RyanWu@20091111.
                //For fixing the issue#7647.
                //Here fixing an issue for VS2005.  If the client event property is a sub property 
                //of a complex property, then the clent event handler can't be persisted after
                //switching to html view.  But if the clent event property is a control's property,
                //then the issue will not be occurred.
                if (!IsVS2008)
                {
                    _componentChangeService.OnComponentChanged(this._inspectedControl, null, null, null);   
                }
                //end by RyanWu@20091111.
                
                if (!C1BaseItemEditorForm.isInModalMode)
                {
                    VSDesignerHelperMethods.SwitchToHtmlViewAndSelectFunction(this._provider, this._selectedJsFunctionName, ClientSideHandlerDefaultInnerComment);
                }                
                _componentChangeService = null;
                _componentChangedEventHandler = null;
            }
        }


        /// <summary>
        /// GetEditStyle
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null && context.PropertyDescriptor != null)
            {
                return UITypeEditorEditStyle.DropDown;
            }
            return UITypeEditorEditStyle.None;
        }

        #region --- private helper methods ---

        /// <summary>
        /// Find text in C style comment starting index
        /// </summary>
        /// <param name="text"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool FindInCComment(string text, int index)
        {
            return (text.LastIndexOf("*/", index) < text.LastIndexOf("/*", index));
        }

        /// <summary>
        /// Find text in nested function
        /// </summary>
        /// <param name="text"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool FindInNestedFunction(string text, int index)
        {
            int num1 = text.LastIndexOf("\n", index);
            int num2 = text.LastIndexOf(";", index);
            return (Math.Max(num1, num2) < text.LastIndexOf("new", index));
        }


        /// <summary>
        /// Find text in quotes
        /// </summary>
        /// <param name="text"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool FindInQuotes(string text, int index)
        {
            int num1 = text.LastIndexOf('"', index);
            int num2 = text.LastIndexOf("'", index);
            int num3 = text.LastIndexOf("\n", index);
            int num4 = text.LastIndexOf(";", index);
            return (Math.Max(num3, num4) < Math.Max(num1, num2));
        }


        /// <summary>
        /// Find text in slash comment
        /// </summary>
        /// <param name="text"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool FindInSlashComment(string text, int index)
        {
            return (text.LastIndexOf("\n", index) < text.LastIndexOf("//", index));
        }

        #endregion

        /// <summary>
        /// ListBox_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBox_Click(object sender, EventArgs e)
        {
            this._editorService.CloseDropDown();
        }

        /// <summary>
        /// ListBox_MouseMove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBox_MouseMove(object sender, MouseEventArgs e)
        {
            int ind = this._listBoxJsFunctionsNames.IndexFromPoint(new Point(e.X, e.Y));
            if (ind >= 0)
            {
                if (ind == _prevFunctionNameIndex)
                    return;
                _prevFunctionNameIndex = ind;
                this._toolTip.SetToolTip(this._listBoxJsFunctionsNames, this._listBoxJsFunctionsNames.Items[ind].ToString());
            }
        }
    }


}
