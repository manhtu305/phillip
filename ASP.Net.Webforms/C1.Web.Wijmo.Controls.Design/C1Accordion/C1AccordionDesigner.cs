﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Text.RegularExpressions;
using C1.Web.Wijmo.Controls.Design.Localization;


namespace C1.Web.Wijmo.Controls.Design.C1Accordion
{

    using C1.Web.Wijmo.Controls.C1Accordion;
    using C1.Web.Wijmo.Controls.Design.Utils;

    /// <summary>
    /// Control Designer for the Accordion
    /// </summary>
    public class C1AccordionDesigner : C1DataBoundControlDesigner
    {
        #region ** fields

        //DesignerAutoFormatCollection _autoFormats;
        protected string must_have_id_message = C1Localizer.GetString("Accordion.Exception.MustHaveID");

        #endregion

        #region ** constructor

        /// <summary>
        /// Initializes a new instance of the AccordionDesigner class
        /// </summary>
        public C1AccordionDesigner()
        {
            
        }

        #endregion

        #region ** properties

        /// <summary>
        /// Helper property to get the Accordion we're designing.
        /// </summary>
        private C1Accordion Accordion
        {
            get
            {
                return (C1Accordion)Component;
            }
        }

        /// <summary>
        /// Create and return action list.
        /// </summary>
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection actionLists = new DesignerActionListCollection();
                actionLists.AddRange(base.ActionLists);
                actionLists.Add(new AccordionDesignerActionList(this));

                return actionLists;
            }
        }
        /*
        /// <summary>
        /// Create and return autoformats list.
        /// </summary>
        public override DesignerAutoFormatCollection AutoFormats
        {
            get
            {
                if (this._autoFormats == null)
                {
                    this._autoFormats = new DesignerAutoFormatCollection();
                    C1DesignerAutoFormat.FillVisualStyleAutoFormatCollection(this._autoFormats, (Accordion)this.Component);
                    if (this._autoFormats.Count <= 0)
                    {
                        return base.AutoFormats;
                    }
                }
                return this._autoFormats;
            }
        }*/

        /// <summary>
        /// Sets or gets the current panel ID.  If this is set,
        /// it will locate the panel and notify the designer of the change.
        /// </summary>
        private string CurrentPanelID
        {
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    C1AccordionPane panel = FindAccordionPane(value);

                    if (panel == null)
                    {
                        throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, C1Localizer.GetString("Accordion.Exception.Can'tFindChild"), value));
                    }

                    int index = -1;

                    // find the index.  we set by index so that the
                    // SelectedIndex property will get changed, which makes Undo/Redo happy.
                    //
                    C1Accordion tc = Accordion;
                    for (int i = 0; i < tc.Panes.Count; i++)
                    {
                        if (tc.Panes[i] == panel)
                        {
                            index = i;
                            break;
                        }
                    }

                    Debug.Assert(index != -1, "Couldn't find panel in list!");

                    if (index != -1)
                    {
                        TypeDescriptor.GetProperties(tc)["SelectedIndex"].SetValue(tc, index);
                    }
                }
                UpdateDesignTimeHtml();
            }

        }

        /// <summary>
        /// Tell the designer we're creating our own UI.
        /// </summary>
        protected override bool UsePreviewControl
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// Allow resize.
        /// </summary>
        public override bool AllowResize
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region ** edit panes (Accordion Designer form)

        internal bool EditPanes()
        {
            bool accept = false;

            // Save original control data, to restore data in case of DialogResult is 'Cancel'
            List<TreeData> originalData = SaveOriginalControlData(((C1Accordion)Component));

            C1AccordionDesignerForm _editorForm = new C1AccordionDesignerForm(((C1Accordion)Component), this);
            accept = ShowControlEditorForm(((C1Accordion)Component), _editorForm);

            if (!accept)
                RestoreOriginalData(((C1Accordion)Component), originalData);
            ClearOriginalData(originalData);

            return accept;
        }

        public bool ShowControlEditorForm(object control, System.Windows.Forms.Form _editorForm)
        {
            System.Windows.Forms.DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("Accordion.EditControl"));
                using (trans)
                {
                    System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = System.Windows.Forms.DialogResult.None;
                        try
                        {
                            dr = service.ShowDialog(_editorForm);
                        }
                        catch (Exception ex)
                        {
                            System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
                        }
                    }
                    else
                        dr = System.Windows.Forms.DialogResult.None;
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = _editorForm.ShowDialog();

            _editorForm.Dispose();
            _editorForm = null;

            return dr == System.Windows.Forms.DialogResult.OK;
        }

        #region Save and Restore Original data

        /// <summary>
        /// TreeData is used to save any object with hierarchical structure
        /// </summary>
        private class TreeData
        {
            public TreeData(object dataObj)
            {
                _dataObj = dataObj;
                _items = new List<TreeData>();
            }

            public List<TreeData> PanesData
            {
                get
                {
                    return _items;
                }
            }

            public object DataObj
            {
                get { return _dataObj; }
            }

            private readonly object _dataObj;
            private List<TreeData> _items;
        }

        private void SaveDataItem(List<TreeData> parent, object item)
        {
            TreeData itemData = new TreeData(item);
            parent.Add(itemData);
            if (item is C1AccordionPane)
            {
                //foreach (AccordionPane it in ((AccordionPane)item).Panes) SaveDataItem(itemData.PanesData, it);
            }
        }

        /// <summary>
        /// Save original control data 
        /// </summary>
        /// <param name="control">Control to be saved</param>
        /// <returns>Returns a list of <see cref="TreeData"/></returns>
        private List<TreeData> SaveOriginalControlData(object control)
        {
            List<TreeData> savedData = new List<TreeData>();
            if (control is C1Accordion)
            {
                C1Accordion menu = (C1Accordion)control;

                foreach (C1AccordionPane item in ((C1Accordion)control).Panes)
                {
                    SaveDataItem(savedData, item);
                }
            }
            return savedData;
        }

        /// <summary>
        /// Restores item data
        /// </summary>
        /// <param name="parent">Object to be restored</param>
        /// <param name="data">Data to be restored to parent object</param>
        private void RestoreDataItem(object parent, TreeData data)
        {
            if (parent is C1AccordionPane)
            {
                foreach (TreeData d in data.PanesData)
                {
                    C1AccordionPane item = (C1AccordionPane)d.DataObj;
                    //item.Panes.Clear();
                    //((AccordionPane)parent).Panes.Add(item);
                    RestoreDataItem(item, d);
                }
            }
        }

        /// <summary>
        /// Restores saved data to original state when edition was canceled
        /// </summary>
        /// <param name="control">Control to be restored</param>
        /// <param name="originalData">Original Data</param>
        private void RestoreOriginalData(object control, List<TreeData> originalData)
        {
            if (control is C1Accordion)
            {
                ((C1Accordion)control).Panes.Clear();
                foreach (TreeData d in originalData)
                {
                    C1AccordionPane item = (C1AccordionPane)d.DataObj;
                    ((C1Accordion)control).Panes.Add(item);
                    RestoreDataItem(item, d);
                }
            }
        }

        private void ClearOriginalData(List<TreeData> originalData)
        {
            originalData.Clear();
        }


        #endregion

        #endregion

        /// <summary>
        /// Helper method to instantiate the given template into a control
        /// an slurp out the markup.
        /// </summary>
        /// <param name="template"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private string GetTemplateContent(ITemplate template, string id)
        {
            DesignerPanel contentPanel = new DesignerPanel();
            contentPanel.ID = id;

            template.InstantiateIn(contentPanel);
            IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));

            Debug.Assert(host != null, "Failed to get IDesignerHost?!?");


            StringBuilder persistedControl = new StringBuilder(1024);
            foreach (System.Web.UI.Control c in contentPanel.Controls)
            {
                persistedControl.Append(ControlPersister.PersistControl(c, host));
            }
            return persistedControl.ToString();
        }

        /// <summary>
        /// Get the content for a given panel or header
        /// </summary>
        /// <param name="panel">The panel to search</param>
        /// <param name="content">True for ContentTemplate, otherwise it'll do HeaderTemplate</param>
        /// <returns></returns>
        private string GetPanelContent(C1AccordionPane panel, bool content)
        {
            if (panel != null)
            {
                if (content && panel.Content != null)
                {
                    return GetTemplateContent(panel.Content, "_content");
                }
                else if (!content)
                {
                    if (panel.Header != null)
                    {
                        return GetTemplateContent(panel.Header, "_header");
                    }
                    else
                    {
                        return "";// return empty string if template is not defined.
                    }
                }
            }
            return "";
        }

        /// <summary>
        /// Initialize the deisigner
        /// </summary>
        /// <param name="component"></param>
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
            SetViewFlags(ViewFlags.TemplateEditing, true);            
            // make sure all the panels have IDs and they're sited.
            foreach (C1AccordionPane tp in Accordion.Panes)
            {
                if (String.IsNullOrEmpty(tp.ID))
                {
//                    tp.ID = this.GetUniqueAccordionPaneName(Accordion);
                    throw new InvalidOperationException(must_have_id_message);
                }
            }
        }
        
        /// <summary>
        /// GetDesignTimeHtml.
        /// </summary>
        /// <param name="regions"></param>
        /// <returns></returns>
        public override string GetDesignTimeHtml(DesignerRegionCollection regions)
        {
            try
            {
                if (this.Accordion.WijmoControlMode == WijmoControlMode.Mobile) {
                    //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                    return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
                }
                if (!string.IsNullOrEmpty(this.Accordion.DataSourceID))
                {
                    //System.Windows.Forms.MessageBox.Show("this.Accordion.DataSourceID=" + this.Accordion.DataSourceID);
                    return base.GetDesignTimeHtml(regions); // Return databound data.
                }
                if (regions == null)
                {
                    throw new ArgumentNullException("regions");
                }

                // Return HTML with editable designer regions:

                // Get View control:
                C1Accordion viewControl = this.ViewControl as C1Accordion;// this.ViewControl as Accordion;
                if (viewControl == null)
                    viewControl = this.Accordion;
                C1AccordionPane selectedPane = viewControl.SelectedPane;
                if (viewControl.Panes.Count > 0)
                {
                    // start with two if selectedPane not null. 
                    // these numbers need to correspond to the order in the regions collection
                    int count = 2;

                    if (selectedPane != null)
                    {
                        // create the main editable region if selectedPane not null
                        EditableDesignerRegion region = new EditableDesignerRegion(this, String.Format(CultureInfo.InvariantCulture, "c{0}", selectedPane.ID));
                        regions.Add(region);
                    }
                    else
                    {
                        count = 0;
                    }

                    // looping through the panels and either building a link for clicking, 
                    // and a plain DesignerRegion, or,
                    // we build a plain span an attach an EditableDesigner region to it.

                    
                    string selectedPaneId = "";
                    if (selectedPane != null)
                        selectedPaneId = selectedPane.ID;
                    foreach (C1AccordionPane panel in viewControl.Panes)
                    {
                        panel.HeaderPanel.ContentElementAttributes.Remove(DesignerRegion.DesignerRegionAttributeName);
                        panel.ContentPanel.ContentElementAttributes.Remove(DesignerRegion.DesignerRegionAttributeName);

                        //panel.HeaderPanel.Attributes.Add("style", "overflow:hidden;margin:0;padding:0;");
                        //panel.ContentPanel.Attributes.Add("style", "overflow:auto;margin:0;padding:0;");

                        bool isActive = (selectedPane != null && panel.ID == selectedPaneId);

                        //string headerText = GetPanelContent(tp, false);

                        

                            // if it's the editable one, it has to be index 1
                            if (isActive)
                            {
                                panel.HeaderPanel.ContentElementAttributes.Add(DesignerRegion.DesignerRegionAttributeName, "1");
                            }
                            else
                            {
                                panel.HeaderPanel.Attributes.Add(DesignerRegion.DesignerRegionAttributeName, "" + (isActive ? 1 : count));
                            }
                            
                        

                        // the region names are arbitrary.  for this purpose, we encode them by a letter - h or t for header or panel, respectively,
                        // and then pop on the panel ID.
                        //
                        if (isActive)
                        {
                            // the editable header region is always to be 1, so we insert it there.
                            //
                            regions.Insert(1, new EditableDesignerRegion(this, String.Format(CultureInfo.InvariantCulture, "h{0}", panel.ID)));
                        }
                        else
                        {
                            // otherwise, just create a plain region.
                            //
                            DesignerRegion clickRegion = new DesignerRegion(this, String.Format(CultureInfo.InvariantCulture, "t{0}", panel.ID));
                            clickRegion.Selectable = true;
                            count++;
                            regions.Add(clickRegion);
                        }
                    }


                    // Active content panel:
                    if (selectedPane != null)
                    {
                        selectedPane.ContentPanel.ContentElementAttributes.Add(DesignerRegion.DesignerRegionAttributeName, "0");
                    }

                    // Build final design time HTML:
                    //                               
                    string sResultBodyContent = "";
                    StringBuilder sb2 = new StringBuilder();
                    System.IO.StringWriter tw = new System.IO.StringWriter(sb2);
                    System.Web.UI.HtmlTextWriter writer = new System.Web.UI.HtmlTextWriter(tw);


                    //Use Fill AutoSize when user editing control at design time:
                    //AccordionAutoSize prevAutoSize = viewControl.AutoSize;
                    //viewControl.AutoSize = AccordionAutoSize.Fill;
                    // Always render control at design time:
                    bool originalVisible = viewControl.Visible;
                    viewControl.Visible = true;

                    //render design time html:
                    viewControl.RenderControl(writer);
                    

                    //Restore Visible state:                    
                    viewControl.Visible = originalVisible;
                    //Restore AutoSize:
                    //viewControl.AutoSize = prevAutoSize;

                    sResultBodyContent = sb2.ToString();
                    // Remove temporary attributes from original control after design time rendering done
                    // Please note, this is important for VS2008, otherwise we will have some 
                    // unexpected behavior:
                    foreach (C1AccordionPane panel in viewControl.Panes)
                    {
                        panel.HeaderPanel.ContentElementAttributes.Remove(DesignerRegion.DesignerRegionAttributeName);
                        panel.HeaderPanel.Attributes.Remove(DesignerRegion.DesignerRegionAttributeName);
                        panel.ContentPanel.ContentElementAttributes.Remove(DesignerRegion.DesignerRegionAttributeName);
                        panel.ContentPanel.Attributes.Remove(DesignerRegion.DesignerRegionAttributeName);                        
                    }
                    return sResultBodyContent;
                }
                else
                {
                    // empty html.
                    // add a designer region for the "#addpanel" UI.
                    DesignerRegion dr = new DesignerRegion(this, "#addpanel");
                    regions.Add(dr);
                    string sStyleString = "";
                    if (!viewControl.Height.IsEmpty || !viewControl.Width.IsEmpty)
                    {
                        sStyleString = " style=\"";
                        if (!viewControl.Width.IsEmpty)
                        {
                            sStyleString += "width:" + viewControl.Width.ToString() + ";";
                        }
                        if (!viewControl.Height.IsEmpty)
                        {
                            sStyleString += "height:" + viewControl.Height.ToString() + ";";
                        }
                        sStyleString += "\"";
                    }
                    return "<div id=\"" + viewControl.ID + "\"" + sStyleString + "><span style=\"color:white;background-color:gray;cursor:pointer;\" " + DesignerRegion.DesignerRegionAttributeName + "=\"0\">" + viewControl.GetType().Name + " " + C1Localizer.GetString("Accordion.Designer.Empty") + "</span></div>";
                    //return sb.ToString();
                }
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show("(e1291) " + ex.Message);
                return GetErrorDesignTimeHtml(ex);
            }
        }

        /// <summary>
        /// The Designer will call us back on this for each EditableDesignerRegion that we created.
        /// In this we return the markup that we want displayed in the editable region.
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
        {
            if (region == null)
            {
                throw new ArgumentNullException("region");
            }

            string regionName = region.Name;

            Debug.Assert(regionName[0] == 'c' || regionName[0] == 'h', "Expected regionName to start with c or h, not " + regionName);

            // is it a content template or a header?
            //
            bool content = regionName[0] == 'c';
            regionName = regionName.Substring(1);

            C1AccordionPane activePanel = FindAccordionPane(regionName);            
            Debug.Assert(activePanel != null, "Couldn't find pane " + regionName);

            return GetPanelContent(activePanel, content);
        }

        private C1AccordionPane FindAccordionPane(string id)
        {
            C1AccordionPane activePanel = null;
            for (int i = 0; i < Accordion.Panes.Count; i++)
            {
                C1AccordionPane pane = Accordion.Panes[i];
                if (pane.ID == id)
                    activePanel = pane;
            }            
            if (activePanel == null)
                activePanel = (C1AccordionPane)Accordion.FindControl(id);
              return activePanel;
        }

        /// <summary>
        /// After a editable region is edited, the designer calls back with the updated markup so we can
        /// stuff it into the right panel.
        /// </summary>
        /// <param name="region"></param>
        /// <param name="content"></param>
        public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
        {
            if (region == null)
            {
                throw new ArgumentNullException("region");
            }

            string regionName = region.Name;

            Debug.Assert(regionName[0] == 'c' || regionName[0] == 'h', "Expected regionName to start with c or h, not " + regionName);

            bool setPanelContent = regionName[0] == 'c';
            regionName = regionName.Substring(1);

            // figure out which panel we have.
            C1AccordionPane activePane = FindAccordionPane(regionName);
            Debug.Assert(activePane != null, "Couldn't find pane " + regionName);

            IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
            Debug.Assert(host != null, "Failed to get IDesignerHost?!?");

/*
            // push the content into the right template
            PersistTemplateContent(activePane, host, content, (setPanelContent ? "Content" : "Header"));
*/
            if (!setPanelContent)
            {
                activePane.Header = ControlParser.ParseTemplate(host, content);
            }
            else
            {
                activePane.Content = ControlParser.ParseTemplate(host, content);
            }
            //pane.Header = ControlParser.ParseTemplate(host, "hello 1");
            //pane.Content = ControlParser.ParseTemplate(host, "hello 2");

        }
        /*
        private static void PersistTemplateContent(AccordionPane panel, IDesignerHost host, string content, string propertyName)
        {
            ITemplate template = ControlParser.ParseTemplate(host, content);
            PersistTemplate(panel, host, template, propertyName);
        }

        /// <summary>
        /// Helper method to save the value of a template.  This sets up all the right Undo state.
        /// </summary>
        /// <param name="panel"></param>
        /// <param name="host"></param>
        /// <param name="template"></param>
        /// <param name="propertyName"></param>
        private static void PersistTemplate(AccordionPane panel, IDesignerHost host, ITemplate template, string propertyName)
        {
            PropertyDescriptor descriptor = TypeDescriptor.GetProperties(panel)[propertyName];
            using (DesignerTransaction transaction = host.CreateTransaction("SetEditableDesignerRegionContent"))
            {
                descriptor.SetValue(panel, template);
                transaction.Commit();
            }
        }
        */

        /// <summary>
        /// Called when we get a click on a designer region.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClick(DesignerRegionMouseEventArgs e)
        {
            // is it a panel?
            if (e.Region != null && e.Region.Name.StartsWith("t", StringComparison.Ordinal))
            {

                CurrentPanelID = e.Region.Name.Substring(1);

            }
            else if (e.Region != null && e.Region.Name == "#addpanel")
            {
                OnAddAccordionPane();
            }

            base.OnClick(e);
        }       

        /// <summary>
        /// Add a new panel panel.
        /// </summary>
        internal void OnAddAccordionPane()
        {
            // First, dirty Page, workaround for VS2008 SP1 issue 
            // fix for bug 5487 [Accordion/C1NavPanel] Pane is disappeared when place control inside C1Window and click on ShowOnLoad checkbox
            bool isControlEnabled = this.Accordion.Enabled;
            TypeDescriptor.GetProperties(this.Accordion)["Enabled"].SetValue(this.Accordion, !isControlEnabled);
            TypeDescriptor.GetProperties(this.Accordion)["Enabled"].SetValue(this.Accordion, isControlEnabled);
            //
            IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
            if (host != null)
            {

                int insertIndex = this.Accordion.SelectedIndex + 1;
                if (insertIndex < 0)
                    insertIndex = 0;
                if (insertIndex > this.Accordion.Panes.Count)
                    insertIndex = this.Accordion.Panes.Count;
                //Accordion viewControl = this.ViewControl as Accordion;
                using (DesignerTransaction dt = host.CreateTransaction("Add new pane"))
                {

                    C1AccordionPane panel = CreateAccordionPane(host);
                    if (panel != null)
                    {
                        // set up the inital state
                        //
                        panel.ID = C1AccordionSerializer.GetNextUniqueAccordionPaneName(this.Accordion);
                        IComponentChangeService changeService = (IComponentChangeService)GetService(typeof(IComponentChangeService));

                        try
                        {
                            changeService.OnComponentChanging(this.Accordion, TypeDescriptor.GetProperties(this.Accordion)["Panes"]);
                            this.Accordion.Panes.Insert(insertIndex, panel);
                        }
                        finally
                        {
                            changeService.OnComponentChanged(this.Accordion, TypeDescriptor.GetProperties(this.Accordion)["Panes"], this.Accordion.Panes, this.Accordion.Panes);
                        }
                        TypeDescriptor.GetProperties(this.Accordion)["SelectedPane"].SetValue(this.Accordion, panel);
                        CurrentPanelID = panel.ID;
                    }
                    UpdateDesignTimeHtml();
                    dt.Commit();
                    VSDesignerHelperMethods.UpdateDesigner(this.Component, "Panes");
                    
                }
            }

        }

        /// <summary>
        /// Remove the active panel panel and set the active panel to be the previous one.
        /// </summary>
        internal void OnRemoveAccordionPane()
        {
            // First, dirty Page, workaround for VS2008 SP1 issue 
            // fix for bug 5487 [Accordion/C1NavPanel] Pane is disappeared when place control inside C1Window and click on ShowOnLoad checkbox
            bool isControlEnabled = this.Accordion.Enabled;
            TypeDescriptor.GetProperties(this.Accordion)["Enabled"].SetValue(this.Accordion, !isControlEnabled);
            TypeDescriptor.GetProperties(this.Accordion)["Enabled"].SetValue(this.Accordion, isControlEnabled);
            //
            C1Accordion accordion = Accordion;
            if (accordion.SelectedPane != null)
            {
                int oldIndex = accordion.SelectedIndex;

                IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));

                if (host != null)
                {
                    using (DesignerTransaction dt = host.CreateTransaction(C1Localizer.GetString("Accordion.RemoveSelectedPane")))
                    {
                        C1AccordionPane activePanel = accordion.SelectedPane;

                        IComponentChangeService changeService = (IComponentChangeService)GetService(typeof(IComponentChangeService));

                        try
                        {
                            changeService.OnComponentChanging(accordion, TypeDescriptor.GetProperties(accordion)["Panes"]);
                            accordion.Panes.Remove(activePanel);
                        }
                        finally
                        {
                            changeService.OnComponentChanged(accordion, TypeDescriptor.GetProperties(accordion)["Panes"], accordion.Panes, accordion.Panes);
                        }

                        activePanel.Dispose();

                        if (accordion.Panes.Count > 0)
                        {
                            TypeDescriptor.GetProperties(accordion)["SelectedIndex"].SetValue(accordion, Math.Min(oldIndex, accordion.Panes.Count - 1));
                        }
                        else
                        {
                            // set index to -1 in order to update design surface
                            // fix for issue 5463
                            // ([Accordion] One AccordionPane is left although all panes are deleted  with ‘Remove Selected Pane’ option):
                            TypeDescriptor.GetProperties(accordion)["SelectedIndex"].SetValue(accordion, -1);
                        }
                        UpdateDesignTimeHtml();
                        dt.Commit();
                    }

                }
            }

        }

        #region ** virtual protected methods

        /// <summary>
        /// Creates the accordion pane.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <returns></returns>
        protected virtual C1AccordionPane CreateAccordionPane(IDesignerHost host)
        {
            C1AccordionPane pane = (C1AccordionPane)host.CreateComponent(typeof(C1AccordionPane));
//pane.Header = ControlParser.ParseTemplate(host, "hello 1");
//pane.Content = ControlParser.ParseTemplate(host, "hello 2");
            return pane;
        }

        internal virtual void FillControlDesignerActionItems(
            AccordionDesignerActionList actionList, 
            DesignerActionItemCollection items)
        {
            DesignerActionPropertyItem expandDirection = new DesignerActionPropertyItem("ExpandDirection", C1Localizer.GetString("Accordion.SmartTag.ExpandDirection"), "Appearance");

            DesignerActionMethodItem editPanes = new DesignerActionMethodItem(actionList, "EditPanes", C1Localizer.GetString("Accordion.SmartTag.EditPanes"), "", C1Localizer.GetString("Accordion.SmartTag.EditPanesDescription"), true);

            //DesignerActionPropertyItem autoSize = new DesignerActionPropertyItem("AutoSize", C1Localizer.GetString("Accordion.SmartTag.AutoSize"), "Layout");
            //DesignerActionPropertyItem defaultHeaderSize = new DesignerActionPropertyItem("DefaultHeaderSize", C1Localizer.GetString("Accordion.SmartTag.DefaultHeaderSize"), "Layout");
            //DesignerActionPropertyItem width = new DesignerActionPropertyItem("Width", C1Localizer.GetString("Accordion.SmartTag.Width"), "Layout");
            //DesignerActionPropertyItem height = new DesignerActionPropertyItem("Height", C1Localizer.GetString("Accordion.SmartTag.Height"), "Layout");

            DesignerActionMethodItem addItem = new DesignerActionMethodItem(actionList, "OnAddAccordionPane", C1Localizer.GetString("Accordion.SmartTag.AddNewPane"), "SelectedPaneProperties", true);
            DesignerActionMethodItem removeItem = new DesignerActionMethodItem(actionList, "OnRemoveAccordionPane", C1Localizer.GetString("Accordion.SmartTag.RemoveSelectedPane"), "SelectedPaneProperties", true);
            DesignerActionPropertyItem selectedIndex = new DesignerActionPropertyItem("SelectedIndex", C1Localizer.GetString("Accordion.SmartTag.SelectedIndex"), "SelectedPaneProperties");
            //DesignerActionHeaderItem selectedPanePropertiesHeader = new DesignerActionHeaderItem(C1Localizer.GetString("Accordion.SmartTag.SelectedPaneProperties"), "SelectedPaneProperties");
            //DesignerActionPropertyItem buttonName = new DesignerActionPropertyItem("ButtonName", C1Localizer.GetString("Accordion.SmartTag.ButtonName"), "SelectedPaneProperties");
            //DesignerActionPropertyItem contentUrl = new DesignerActionPropertyItem("ContentUrl", C1Localizer.GetString("Accordion.SmartTag.ContentUrl"), "SelectedPaneProperties");

            //DesignerActionPropertyItem displayVisible = new DesignerActionPropertyItem("DisplayVisible", C1Localizer.GetString("Accordion.SmartTag.DisplayVisible"), "SelectedPaneProperties");
            //DesignerActionPropertyItem removed = new DesignerActionPropertyItem("Removed", C1Localizer.GetString("Accordion.SmartTag.Removed"), "SelectedPaneProperties");

            items.Add(expandDirection);
            items.Add(editPanes);
            //items.Add(autoSize);
            //items.Add(defaultHeaderSize);
            //items.Add(width);
            //items.Add(height);

            items.Add(addItem);
            items.Add(removeItem);
            items.Add(selectedIndex);
            //items.Add(selectedPanePropertiesHeader);
            //items.Add(buttonName);
            //items.Add(contentUrl);
            //items.Add(displayVisible);
            //items.Add(removed);
        }

        #endregion



    }

    /// <summary>
    /// Manages our designer verbs
    /// </summary>
    internal class AccordionDesignerActionList : DesignerActionListBase
    {
        private C1AccordionDesigner _designer;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Justification = "Security handled by base class")]
        public AccordionDesignerActionList(C1AccordionDesigner designer)
            : base(designer)
        {
            _designer = designer;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2116:AptcaMethodsShouldOnlyCallAptcaMethods", Justification = "Security handled by base class")]
        public override DesignerActionItemCollection GetSortedActionItems()
        {

            DesignerActionItemCollection items = new DesignerActionItemCollection();

            _designer.FillControlDesignerActionItems(this, items);

            AddBaseSortedActionItems(items);
            return items;
        }


        public ExpandDirection ExpandDirection
        {
            get { return ((C1Accordion)_designer.Component).ExpandDirection; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(((C1Accordion)_designer.Component))["ExpandDirection"];
                prop.SetValue(((C1Accordion)_designer.Component), value);
            }
        }

        public Unit Width
        {
            get { return ((C1Accordion)_designer.Component).Width; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(((C1Accordion)_designer.Component))["Width"];
                prop.SetValue(_designer.Component, value);
            }
        }

        public Unit Height
        {
            get { return ((C1Accordion)_designer.Component).Height; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_designer.Component)["Height"];
                prop.SetValue(_designer.Component, value);
            }
        }
        
        public int SelectedIndex
        {
            get { return ((C1Accordion)_designer.Component).SelectedIndex; }
            set
            {
                PropertyDescriptor prop = TypeDescriptor.GetProperties(_designer.Component)["SelectedIndex"];
                prop.SetValue(_designer.Component, value);
            }
        }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called via reflection")]
        public void EditPanes()
        {
            this._designer.EditPanes();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called via reflection")]
        private void OnAddAccordionPane()
        {
            _designer.OnAddAccordionPane();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called via reflection")]
        private void OnRemoveAccordionPane()
        {
            _designer.OnRemoveAccordionPane();
        }

    }

    /// <summary>
    /// Simple class to use for template instantiation
    /// </summary>
    internal class DesignerPanel : System.Web.UI.WebControls.Panel, INamingContainer
    {
    }

}

/*
                System.Windows.Forms.Form aa = new System.Windows.Forms.Form();
                System.Windows.Forms.TextBox bb = new System.Windows.Forms.TextBox();
                bb.Text = sResultBodyContent;
                bb.Width = 500;
                bb.Height = 500;
                bb.Multiline = true;
                aa.Width = 520;
                aa.Height = 520;
                aa.Controls.Add(bb);
                aa.Show();
*/
