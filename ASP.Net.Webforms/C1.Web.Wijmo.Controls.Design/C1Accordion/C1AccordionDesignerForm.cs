﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Globalization;



namespace C1.Web.Wijmo.Controls.Design.C1Accordion
{
	using C1.Web.Wijmo.Controls.C1Accordion;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls;

    public partial class C1AccordionDesignerForm : C1BaseItemEditorForm
    {
        #region Custom Element Types enumeration
        /// <summary>
        /// Element Types enumerations used by Designer
        /// </summary>
        private enum ItemType
        {
            /// <summary>
            /// Indicates what default item type must be used.
            /// </summary>
            None,
            /// <summary>
            /// Accordion/NavPanel pane.
            /// </summary>
            Pane,
            /// <summary>
            /// Accordion.
            /// </summary>
            AccordionRoot
        }

        /// <summary>
        /// Gets an automatic name for an <typeparamref name="itemType"/>
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="nodeCollection">Gets access to the count of its parent's nodes</param>
        /// <returns>
        /// New automatic name for given <typeparamref name="itemType"/>
        /// </returns>
        protected override string GetNextItemTextId(string prefix, TreeNodeCollection nodeCollection)
        {
            if (prefix == "PANE_PREFIX")
            {
                return C1AccordionSerializer.GetNextUniqueAccordionPaneName(this._accordion);
            }
            return base.GetNextItemTextId(prefix, nodeCollection);
        }

        /// <summary>
        /// Gets the type of the given item
        /// </summary>
        /// <param name="item">Given item for checking its type</param>
        /// <returns>The <seealso cref="ItemType"/> of given item</returns>
        private ItemType GetItemType(object item)
        {
            // Sets the default item type
            ItemType elementType = ItemType.Pane;

            if (item is C1Accordion)
            {
                elementType = ItemType.AccordionRoot;
            }
            else if (item is C1AccordionPane)
            {
                elementType = ItemType.Pane;
            }
            return elementType;
        }

        /// <summary>
        /// Fills the allowable types that can compose the control. 
        /// You should load default item in first place. 
        /// </summary>
        /// <param name="itemInfo">List of available control items to be filled</param>
        protected override List<C1ItemInfo> FillAvailableControlItems()
        {
            List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();
            C1ItemInfo item;

            item = new C1ItemInfo();
            item.ItemType = ItemType.Pane;
            item.EnableChildItems = true;
            item.ContextMenuStripText = C1Localizer.GetString("Accordion.DesignerForm.ContextMenuPaneItem");
            item.NodeImage = Properties.Resources.LinkItemIco; //GetImageFromResource("LinkItemIco"); 
            item.DefaultNodeText = "PANE_PREFIX";//AccordionPane
            item.Visible = true;
            item.Default = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.AccordionRoot;
            item.EnableChildItems = true;
            item.ContextMenuStripText = "Accordion";
            item.NodeImage = Properties.Resources.RootItemIco; //GetImageFromResource("RootItemIco");
            item.DefaultNodeText = "Accordion";
            item.Visible = false;
            item.Default = false;
            itemsInfo.Add(item);

            return itemsInfo;
        }

        #endregion  // Custom Element Types enumeration

        #region ** fields
        // Accordion local control
        private C1Accordion _accordion;
        private C1AccordionDesigner _designer;
        // clipboard for current control
        private Stream _clipboardData;
        private C1ItemInfo _clipboardType;
        #endregion

        #region ** constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="menu">C1 Control</param>
        public C1AccordionDesignerForm(C1Accordion accordion, C1AccordionDesigner designer)
            : base(accordion)
        {
            try
            {
                this._accordion = accordion;
                this._designer = designer;
                InitializeComponent();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #endregion

        #region ** C1BaseItemEditorForm implementations
        /// <summary>
        /// Form loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AccordionDesignerForm_Load(object sender, EventArgs e)
        {
        }

        const string NEW_LINE = "\r\n";
        /// <summary>
        /// Shows current control into preview tab
        /// </summary>
        /// <param name="previewer">The web browser previewer</param>
        internal override void ShowPreviewHtml(C1WebBrowser previewer)
        {
            try
            {
                /*
                string sResultBodyContent = "";
                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new System.IO.StringWriter(sb);
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
                _accordion.RenderControl(htw);
                sResultBodyContent = sb.ToString();
                */
                string sResultBodyContent = _designer.GetDesignTimeHtml();

                string sDocumentContent = "";
                sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
                sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
                sDocumentContent += "<head>" + NEW_LINE;
                //sDocumentContent += sScriptContent + NEW_LINE;
                sDocumentContent += "</head>" + NEW_LINE;
                sDocumentContent += "<body>" + NEW_LINE;
                sDocumentContent += sResultBodyContent + NEW_LINE;
                sDocumentContent += "</body>" + NEW_LINE;
                sDocumentContent += "</html>" + NEW_LINE;


                //previewer.Document.Write(sDocumentContent);
                previewer.DocumentWrite(sDocumentContent, this._accordion);

//previewer.DocumentText = sDocumentContent;
//System.Windows.Forms.MessageBox.Show("**doc.Write=" + sDocumentContent);
//previewer.Navigate("about:blank");                
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
            }
        }

        /// <summary>
        /// Called to initialize the treeview with the object hierarchy.
        /// </summary>
        /// <param name="mainTreeView"></param>
        protected override void LoadControl(TreeView mainTreeView)
        {
            string nodeText;
            object nodeTag;
            C1ItemInfo itemInfo;

            // populate mainTreeView with C1 control items
            mainTreeView.BeginUpdate();
            mainTreeView.Nodes.Clear();

            TreeNode rootMenuItem = new TreeNode();
            itemInfo = GetItemInfo(GetItemType(_accordion));
            nodeText = _accordion.ID == null ? itemInfo.DefaultNodeText : _accordion.ID.ToString();
            nodeTag = new C1NodeInfo(_accordion, itemInfo, nodeText);
            SetNodeAttributes(rootMenuItem, nodeText, nodeTag);

            foreach (C1AccordionPane menuItem in _accordion.Panes)
            {
                TreeNode menuItemNode = new TreeNode();
                nodeText = menuItem.ID;
                itemInfo = GetItemInfo(GetItemType(menuItem));
                nodeTag = new C1NodeInfo(menuItem, itemInfo, nodeText);
                SetNodeAttributes(menuItemNode, nodeText, nodeTag);

                IterateChildNodes(menuItemNode, menuItem);

                rootMenuItem.Nodes.Add(menuItemNode);
            }

            mainTreeView.Nodes.Add(rootMenuItem);
            mainTreeView.SelectedNode = mainTreeView.Nodes[0];
            mainTreeView.ExpandAll();
            mainTreeView.EndUpdate();
        }

        /// <summary>
        /// Loads nested items from current item
        /// </summary>
        /// <param name="menuItemNode">TreeNode parent</param>
        /// <param name="item">Control item</param>
        private void IterateChildNodes(TreeNode menuItemNode, C1AccordionPane item)
        {
            menuItemNode.Nodes.Clear();
        }

        /// <summary>
        /// Gets de item info according to the given item type
        /// </summary>
        /// <param name="itemType">Item type</param>
        /// <returns>C1ItemInfo object that contains the item info</returns>
        private C1ItemInfo GetItemInfo(ItemType itemType)
        {
            foreach (C1ItemInfo itemInfo in base.ItemsInfo)
            {
                if ((ItemType)itemInfo.ItemType == itemType)
                    return itemInfo;
            }
            return null;
        }

        protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
        {
            base.AllowAdd = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AccordionRoot });
            base.AllowInsert = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Pane});
            base.AllowChangeType = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Pane });
            base.AllowCopy = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Pane, });
            base.AllowPaste = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Pane, ItemType.AccordionRoot });
            base.AllowCut = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Pane });
            base.AllowDelete = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Pane });
            base.AllowRename = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.AccordionRoot, ItemType.Pane });
            base.AllowMoveUp = EnableMoveUp();
            base.AllowMoveDown = EnableMoveDown();
            base.AllowMoveLeft = EnableMoveLeft(itemInfo, parentItemInfo);
            base.AllowMoveRight = EnableMoveRight(itemInfo, previousItemInfo);

            base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
        }

        /// <summary>
        /// Enables given action if it is in given types.
        /// </summary>
        /// <param name="itemInfo">C1ItemInfo info.</param>
        /// <param name="types">Allowed types for given action.</param>
        private bool EnableActionByItemType(C1ItemInfo itemInfo, ItemType[] types)
        {
            bool enable = false;
            if (itemInfo != null)
            {
                foreach (ItemType type in types)
                {
                    enable |= itemInfo.ItemType.Equals(type);
                }
            }
            return enable;
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the left
        /// </summary>
        private bool EnableMoveLeft(C1ItemInfo itemInfo, C1ItemInfo parenItemInfo)
        {
            return false;
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the right
        /// </summary>
        private bool EnableMoveRight(C1ItemInfo itemInfo, C1ItemInfo previousItemInfo)
        {
            return false;
        }

        private bool EnableMoveUp()
        {
            return true;
        }

        private bool EnableMoveDown()
        {
            return true;
        }

        /// <summary>
        /// Creates the C1NodeInfo object that will contain both the specific C1 control item depending on the itemInfo
        /// and given itenInfo.
        /// </summary>
        /// <param name="itemInfo">C1ItemInfo object.</param>
        /// <param name="nodesList">Nodes that currently exist into the list where the new created item 
        /// will be inserted.</param>
        /// <returns>Returns a C1 control that depends on the C1ItemInfo. Given nodesList
        /// could be used to obtain a new numbered name.</returns>
        protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
        {
            C1NodeInfo nodeInfo;
            string nodeText;

            C1AccordionPane item = CreateNewItem(itemInfo);

            // itemInfo.DefaultNodeText could be used as default item text. if not, you can get a new numbered name.
            // item.Text = itemInfo.DefaultNodeText;

            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);            

            // set the text for created item
            item.ID = nodeText;

            // create a new node info
            nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);

            // set nodeText property
            nodeInfo.NodeText = nodeText;

            return nodeInfo;
        }

        /// <summary>
        ///  Indicates when an item was inserted in a specific position.
        /// </summary>
        /// <param name="item">The inserted item.</param>
        /// <param name="destinationItem">The destination item that will contain inserted item.</param>
        /// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
        protected override void Insert(object item, object destinationItem, int destinationIndex)
        {
            try
            {
              _accordion.Panes.Insert(destinationIndex, (C1AccordionPane)item);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
            }
        }

        /// <summary>
        ///  Indicates when an item was deleted.
        /// </summary>
        /// <param name="item">The deleted item.</param>
        /// <param name="parent">The parent that contains the deleted item.</param>
        protected override void Delete(object item)
        {
            _accordion.Panes.Remove((C1AccordionPane)item);
        }

        protected override void Copy(object item, C1ItemInfo itemInfo)
        {
            if (_clipboardData == null)
                _clipboardData = new MemoryStream();
            Copy(_clipboardData, item);

            this._clipboardType = itemInfo;
            base.ClipboardData = true;
        }

        private void Copy(Stream buffer, object target)
        {
            C1AccordionSerializer serializer = new C1AccordionSerializer(target);
            buffer.SetLength(0);
            serializer.SaveLayout(buffer);
        }

        protected override void Paste(TreeNode destinationNode)
        {
            Paste(_clipboardData, destinationNode);
        }

        private void Paste(Stream buffer, TreeNode destNode)
        {

            C1AccordionPane item = new C1AccordionPane(this._accordion);
            C1AccordionSerializer serializer = new C1AccordionSerializer(item);
            buffer.Seek(0, SeekOrigin.Begin);
            // lets to serializer to retrieve data from clipboard
            serializer.LoadLayout(buffer, LayoutType.All);

            // retrieve the AccordionItem form destination node
            object destinationObject = ((C1NodeInfo)destNode.Tag).Element;
            
                        
            item.ID = EnsureUniqueAccordionId(item.ID);

            _accordion.Panes.Add((C1AccordionPane)item);

            // create node info
            C1NodeInfo nodeInfo = new C1NodeInfo(item, _clipboardType, item.ID);

            // create new treenode to be added on destination node
            TreeNode node = new TreeNode();
            // set node text and node tag
            SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);

            IterateChildNodes(node, item);
            if (destinationObject is C1AccordionPane)
            {
                destNode.Parent.Nodes.Add(node);
            }
            else
            {
                destNode.Nodes.Add(node);
                destNode.ExpandAll();
            }

        }

        private string EnsureUniqueAccordionId(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return C1AccordionSerializer.GetNextUniqueAccordionPaneName(this._accordion);
            }
            C1AccordionPaneCollection panes = _accordion.Panes;
            for (int i = 0; i < panes.Count; i++)
            {
                if (panes[i].ID == id)
                {
                    return C1AccordionSerializer.GetNextUniqueAccordionPaneName(this._accordion);
                }
            }
            return id;
        }

        /// <summary>
        /// Creates a new item which its type depends on itemsInfo.
        /// </summary>
        /// <param name="itemInfo">The info of new item to be created.</param>
        /// <returns>New AccordionItem</returns>
        private C1AccordionPane CreateNewItem(C1ItemInfo itemInfo)
        {
            C1AccordionPane item = new C1AccordionPane(this._accordion);
            if ((ItemType)itemInfo.ItemType == ItemType.Pane)
            {
                //
            }
            return item;
        }

        protected override void SaveToXML(string fileName)
        {
            try
            {
                _accordion.SaveLayout(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void LoadFromXML(string fileName)
        {
            try
            {
                _accordion.LoadLayout(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #region Utils
   
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <param name="selectedNode"></param>
        protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
        {
            bool cancel = e.CancelEdit;
            FinishLabelEdit(e.Node, e.Label, ref cancel);
            e.CancelEdit = cancel;
        }

        private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
        {
            string nodeText = "";
            string property = "Text";
            object obj = ((C1NodeInfo)node.Tag).Element;
            C1ItemInfo itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;

            if (text != null) //&& text.CompareTo(_emptyItemText) != 0
            {              
                nodeText = text;
            }

            if (obj is C1Accordion || obj is C1AccordionPane)
            {
                property = "ID";
                if (string.IsNullOrEmpty(nodeText == null ? "" : nodeText.Trim()))
                {
                    cancelEdit = true;
                    //System.Windows.Forms.MessageBox.Show("Control ID can not be empty.");
                    return;
                }
            }

            /*
            if (obj is Accordion)
            {
                nodeText = ((Accordion)obj).ID == null ? "" : ((Accordion)obj).ID.ToString();
            }
            else if (obj is AccordionPane)
            {
                if (!string.IsNullOrEmpty(((AccordionPane)obj).ID))
                    nodeText = ((AccordionPane)obj).ID;
                if (string.IsNullOrEmpty(nodeText))
                {
                    if (node.Parent == null)
                    {
                        nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Nodes);
                    }
                    else
                    {
                        nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Parent.Nodes);
                    }
                }
            }
            */



            // set new text value to selected object and property grid
            TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
            base.RefreshPropertyGrid();
            node.Text = nodeText;
        }

        // Gets image from resource. This method is used just only for testing purposes.
        // This form must be moved to C1.Web.UI.Design.2 assembly when designer is finished.
        private System.Drawing.Image GetImageFromResource(string imageName)
        {
            string resId;
            Stream stream;
            resId = string.Format("C1.Web.UI.Controls.Accordion.Resources.{0}.png", imageName);
            stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resId);
            if (stream != null)
                return System.Drawing.Image.FromStream(stream);
            return null;
        }

        #endregion


        #endregion

    }
}
