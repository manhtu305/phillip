﻿using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1FlipCard
{
    using C1.Web.Wijmo.Controls.Design.Localization;
    using Controls.C1FlipCard;

	[SupportsPreviewControl(true)]
	internal class C1FlipCardDesigner : C1ControlDesinger
	{
		private const string FrontDesignerRegionName = "Front";
		private const string BackDesignerRegionName = "Back";

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				var actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1FlipCardDesignerActionList(this));
				return actionLists;
			}
		}

		public override bool AllowResize
		{
			get { return true; }
		}

		private C1FlipCard C1FlipCard
		{
			get { return Component as C1FlipCard; }
		}

		public override string GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			regions.Add(new EditableDesignerRegion(this, FrontDesignerRegionName));
			regions.Add(new EditableDesignerRegion(this, BackDesignerRegionName));
			var flipCard = ViewControl as C1FlipCard;
			if (flipCard != null)
			{
				flipCard.FrontPanel.Attributes[DesignerRegion.DesignerRegionAttributeName] = "0";
				flipCard.BackPanel.Attributes[DesignerRegion.DesignerRegionAttributeName] = "1";
			}

			return base.GetDesignTimeHtml(regions);
		}

		public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
		{
			var host = (IDesignerHost) GetService(typeof (IDesignerHost));
			Debug.Assert(host != null, "host != null");
			var result = string.Empty;
			switch (region.Name)
			{
				case FrontDesignerRegionName:
					result = ControlPersister.PersistTemplate(C1FlipCard.FrontSide, host);
					break;
				case BackDesignerRegionName:
					result = ControlPersister.PersistTemplate(C1FlipCard.BackSide, host);
					break;
			}
			return result;
		}

		public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
		{
			var host = (IDesignerHost) GetService(typeof (IDesignerHost));
			Debug.Assert(host != null, "host != null");
			switch (region.Name)
			{
				case FrontDesignerRegionName:
					C1FlipCard.FrontSide = ControlParser.ParseTemplate(host, content);
					break;
				case BackDesignerRegionName:
					C1FlipCard.BackSide = ControlParser.ParseTemplate(host, content);
					break;
			}
		}
	}

	internal class C1FlipCardDesignerActionList : DesignerActionListBase
	{
		private readonly C1FlipCard _c1FlipCard;

		public C1FlipCardDesignerActionList(ControlDesigner designer)
			: base(designer)
		{
			_c1FlipCard = designer.Component as C1FlipCard;
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			var items = new DesignerActionItemCollection
			{
                //new DesignerActionHeaderItem("Appearance"),
				new DesignerActionPropertyItem("CurrentSide", C1Localizer.GetString("C1FlipCard.SmartTag.CurrentSide"), "Appearance", C1Localizer.GetString("C1FlipCard.SmartTag.CurrentSideDescription"))
			};
			AddBaseSortedActionItems(items);
			return items;
		}

		public FlipCardSide CurrentSide
		{
			get { return _c1FlipCard.CurrentSide; }
			set { TypeDescriptor.GetProperties(_c1FlipCard)["CurrentSide"].SetValue(_c1FlipCard, value); }
		}
	}
}