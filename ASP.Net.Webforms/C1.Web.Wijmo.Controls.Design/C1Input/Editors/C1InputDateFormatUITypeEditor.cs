﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using C1.Web.Wijmo.Controls.C1Input;
using C1.Web.Wijmo.Controls.Design.C1Input;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{

    internal class C1InputDateFormatUITypeEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            // use modal dialog
            return (context != null && context.Instance != null)
                ? UITypeEditorEditStyle.Modal
                : base.GetEditStyle(context);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null || context.Instance == null || provider == null)
                return value;

            // get service
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc == null)
                return value;
            // initialize editor

            C1InputDate c1InputDate = context.Instance as C1InputDate;
            if (c1InputDate == null)
            {
                C1Input.C1InputDesignerActionList actionList = context.Instance as C1Input.C1InputDesignerActionList;
                c1InputDate = actionList.Component as C1InputDate;
            }
            SetFormatDialog<C1DateFormatEditor> frm = new SetFormatDialog<C1DateFormatEditor>();
            (frm.FormatEditor as IC1DesignEditor).setInspectedComponent(c1InputDate, null);
            frm.FormatEditor.DateFormat = (string)value;
            
            DialogResult dr = edSvc.ShowDialog(frm);
            if (dr == DialogResult.OK)
            {
                value = frm.FormatEditor.DateFormat;
                context.OnComponentChanged();
            }


            // done			
            return value;
        }
    }

    internal class C1InputDisplayFormatUITypeEditor : C1InputDateFormatUITypeEditor
    {
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null || context.Instance == null || provider == null)
                return value;

            // get service
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc == null)
                return value;
            // initialize editor

            C1InputDate c1InputDate = context.Instance as C1InputDate;
            if (c1InputDate == null)
            {
                C1Input.C1InputDesignerActionList actionList = context.Instance as C1Input.C1InputDesignerActionList;
                c1InputDate = actionList.Component as C1InputDate;
            }
            SetFormatDialog<C1DisplayFormatEditor> frm = new SetFormatDialog<C1DisplayFormatEditor>();
            frm.SetTitle(C1Localizer.GetString("C1InputSetDisplayFormatDialog.DialogTitle"));
            (frm.FormatEditor as IC1DesignEditor).setInspectedComponent(c1InputDate, null);
            frm.FormatEditor.DateFormat = (string)value;
            frm.FormatEditor.DefaultFormat = c1InputDate.DateFormat;

            DialogResult dr = edSvc.ShowDialog(frm);
            if (dr == DialogResult.OK)
            {
                value = frm.FormatEditor.DateFormat;
                context.OnComponentChanged();
            }

            // done			
            return value;
        }

    }
}
