﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using C1.Web.Wijmo.Controls.C1Input;
using C1.Web.Wijmo.Controls.Design.C1Input;
using C1.Web.Wijmo.Controls.Design.Localization;
using C1.Web.Wijmo.Controls.Design.Properties;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    /// <summary>
    /// Provides a UI to edit the MaskFormat class.
    /// </summary>
    internal sealed class C1AdvancedMaskFormatEditor : UserControl, IFormatEditor
    {

        #region Component Designer generated code

        #region Instance Data
        private MaskedTextBox previewMask;
        private PictureBox picHelpProperties;
        private TextBox patternEditor;
        private Button patternButton;
        private DataGridViewList sampleList;
        private ContextMenuStrip patternContextMenuStrip;
        private Label lblPattern;
        private Label lblPreview;

        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;

        private ErrorProvider _errorProvider = null;
        #endregion

        #region Initialize & Dispose

        /// <summary> 
        /// Required method for Designer support - do not mod ify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.previewMask = new MaskedTextBox();
            this.picHelpProperties = new System.Windows.Forms.PictureBox();
            this.lblPattern = new System.Windows.Forms.Label();
            this.lblPreview = new System.Windows.Forms.Label();
            this.patternEditor = new System.Windows.Forms.TextBox();
            this.patternButton = new System.Windows.Forms.Button();
            this.sampleList = new C1.Web.Wijmo.Controls.Design.UITypeEditors.DataGridViewList();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patternContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.picHelpProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sampleList)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // previewMask
            // 
            this.previewMask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewMask.Location = new System.Drawing.Point(63, 16);
            this.previewMask.Name = "previewMask";
            this.previewMask.Size = new System.Drawing.Size(200, 50);
            this.previewMask.TabIndex = 2;
            this.previewMask.ReadOnly = true;
            // 
            // picHelpProperties
            // 
            this.picHelpProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picHelpProperties.Location = new System.Drawing.Point(373, 0);
            this.picHelpProperties.Name = "picHelpProperties";
            this.picHelpProperties.Size = new System.Drawing.Size(16, 16);
            this.picHelpProperties.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picHelpProperties.TabIndex = 2;
            this.picHelpProperties.TabStop = false;
            // 
            // lblPattern
            // 
            this.lblPattern.AutoSize = true;
            this.lblPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPattern.Location = new System.Drawing.Point(3, 0);
            this.lblPattern.Name = "lblPattern";
            this.lblPattern.Size = new System.Drawing.Size(54, 13);
            this.lblPattern.TabIndex = 0;
            this.lblPattern.Text = C1Localizer.GetString("C1AdvancedMaskFormatEditor.lblPattern");
            // 
            // lblPreview
            // 
            this.lblPreview.AutoSize = true;
            this.lblPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPreview.Location = new System.Drawing.Point(63, 0);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(304, 13);
            this.lblPreview.TabIndex = 0;
            this.lblPreview.Text = C1Localizer.GetString("C1AdvancedMaskFormatEditor.lblPreview");
            // 
            // patternEditor
            // 
            this.patternEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patternEditor.Location = new System.Drawing.Point(3, 16);
            this.patternEditor.Margin = new System.Windows.Forms.Padding(3, 3, 1, 2);
            this.patternEditor.Name = "patternEditor";
            this.patternEditor.Size = new System.Drawing.Size(56, 20);
            this.patternEditor.TabIndex = 1;
            this.patternEditor.TextChanged += new System.EventHandler(this.PatternEditor_TextChanged);
            this.patternEditor.Validating += new System.ComponentModel.CancelEventHandler(this.PatternEditor_Validating);
            // 
            // patternButton
            // 
            this.patternButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patternButton.Location = new System.Drawing.Point(1, 271);
            this.patternButton.Margin = new System.Windows.Forms.Padding(1, 2, 25, 1);
            this.patternButton.Name = "patternButton";
            this.patternButton.Size = new System.Drawing.Size(56, 17);
            this.patternButton.TabIndex = 2;
            this.patternButton.TabStop = false;
            this.patternButton.Text = ">";
            this.patternButton.Click += new System.EventHandler(this.PatternButton_Click);
            this.patternButton.Validating += new System.ComponentModel.CancelEventHandler(this.patternButton_Validating);
            // 
            // sampleList
            // 
            this.sampleList.AllowUserToAddRows = false;
            this.sampleList.AllowUserToDeleteRows = false;
            this.sampleList.AllowUserToResizeRows = false;
            this.sampleList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sampleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sampleList.Location = new System.Drawing.Point(3, 16);
            this.sampleList.MultiSelect = false;
            this.sampleList.Name = "sampleList";
            this.sampleList.ReadOnly = true;
            this.sampleList.RowHeadersVisible = false;
            this.sampleList.SelectedIndex = -1;
            this.sampleList.SelectedItem = null;
            this.sampleList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sampleList.Size = new System.Drawing.Size(384, 110);
            this.sampleList.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Content";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Description";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Content";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Description";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // patternContextMenuStrip
            // 
            this.patternContextMenuStrip.AllowDrop = true;
            this.patternContextMenuStrip.Name = "patternContextMenuStrip";
            this.patternContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            this.patternContextMenuStrip.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.PatternContextMenuStrip_Closed);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.sampleList, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(396, 430);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.Controls.Add(this.lblPattern, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.patternEditor, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.patternButton, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblPreview, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.previewMask, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 138);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.SetColumnSpan(this.previewMask, 2);
            this.tableLayoutPanel2.Size = new System.Drawing.Size(390, 289);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // C1AdvancedMaskFormatEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "C1AdvancedMaskFormatEditor";
            this.Size = new System.Drawing.Size(396, 430);
            ((System.ComponentModel.ISupportInitialize)(this.picHelpProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sampleList)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }


        /// <summary> 
        /// Cleans up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion


        #endregion

        #region Instance Data

        /// <summary>
        /// A event indicates the format is been modified by user.
        /// </summary>
        public event EventHandler FormatModified;

        /// <summary>
        /// Defines the format setting information.
        /// </summary>
        private FormatSetting _maskFormatSetting = null;

        /// <summary>
        /// Defines the custom item in smaple list.
        /// </summary>
        private DataGridViewRow _customListItem = null;

        /// <summary>
        /// Defines a event lock used to synchronize the SelectedIndexChanged event of sample list 
        /// and the TextChanged event text box.
        /// </summary>
        private bool _eventLock = false;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private bool _patternEditorChanged = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="C1AdvancedMaskFormatEditor"/> class.
        /// </summary>
        /// <param name="isPropertyPageStyle">
        /// A <b>bool</b> value indicate the UI style.
        /// <b>true</b> is property page style;<b>false</b> is standard style
        /// </param>
        public C1AdvancedMaskFormatEditor()
        {
            InitializeComponent();
            this.sampleList.CausesValidation = false;

            LoadResources();
            this.TempMask.FormatMode = FormatMode.Advanced;
        }

        protected override void OnLayout(LayoutEventArgs e)
        {
            base.OnLayout(e);
            this.sampleList.AutoSizeColumns();
        }

        #endregion

        #region Properties

        internal ErrorProvider ErrorProvider
        {
            get
            {
                if (this._errorProvider == null)
                {
                    this._errorProvider = new ErrorProvider(this.components);
                }
                return this._errorProvider;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="MaskFormat"/>.
        /// </summary>
        /// <value><see cref="MaskFormat"/> that is the editing result</value>
        public string Format
        {
            get
            {
                return this.patternEditor.Text;
            }
            set
            {
                if (this._eventLock)
                {
                    return;
                }

                this._eventLock = true;

                this.sampleList.SelectedIndex = -1;

                if (value == null)
                {
                    this.patternEditor.Text = string.Empty;
                }
                else
                {
                    this.patternEditor.Text = value;
                    this.TempMask.MaskFormat = value;
                    this.previewMask.Text = this.TempMask.EmptyString;
                }

                this._eventLock = false;
            }
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.sampleList.AutoResizeColumns();
           
        }

        public void PostFormLoad()
        {
            this.sampleList.SelectedIndex = -1;
            this.sampleList.SelectedIndexChanged += this.SampleList_SelectedIndexChanged;
        }
        // End by jiang

        /// <summary>
        /// Determines whether the format should be persisted.
        /// </summary>
        /// <returns>
        /// Boolean value to stop the Format property automatically creating code at design time;
        /// this implementation always returns false.
        /// </returns>
        private bool ShouldSerializeFormat()
        {
            return false;
        }

        /// <summary>
        /// Loads the resources from an XML file.
        /// </summary>
        private void LoadResources()
        {
            //
            // Load the resources.
            //
            this._maskFormatSetting = FormatSetting.Parse(C1Localizer.GetString("C1Input.MaskInput.MaskFormatSetting"))[0];

            //
            // Load the context menu strip resources.
            //
            for (int i = 0; i < this._maskFormatSetting.Defination.PatternDefinations.Count; i++)
            {
                if (this._maskFormatSetting.Defination.PatternDefinations[i].Name == "Pattern")
                {
                    FormatEditorHelper.Initialize(
                        this.patternContextMenuStrip,
                        this._maskFormatSetting.Defination.PatternDefinations[i],
                        this.patternEditor);
                }
            }

            const string PREFIX = "C1AdvancedMaskFormatEditor.";
            const string DEFAULT_PREFIX = "FormatEditor.";

            //
            // Load the sample list resources.
            //
            FormatEditorHelper.Initialize(this.sampleList, this._maskFormatSetting.Samples);
            this.sampleList.Columns[0].HeaderText = FormatEditorHelper.GetStringFromResource("SampleList.Header.Pattern", PREFIX, DEFAULT_PREFIX);
            this.sampleList.Columns[1].HeaderText = FormatEditorHelper.GetStringFromResource("SampleList.Header.Description", PREFIX, DEFAULT_PREFIX);
            this.sampleList.Rows.Add(FormatEditorHelper.GetStringFromResource("Custom", PREFIX, DEFAULT_PREFIX));
            this._customListItem = this.sampleList.Rows[this.sampleList.Rows.Count - 1];
         
            this.patternButton.Image = new Bitmap(Resources.XmlFormatDropDownButton);
            this.patternButton.Text = string.Empty;

            //
            // localization
            //
            this.lblPattern.Text = C1Localizer.GetString("C1AdvancedMaskFormatEditor.lblPattern");
        }

        /// <summary>
        /// Raises the FormatModified event.
        /// </summary>
        /// <param name="e"><see cref="EventArgs"/> that contains the event arguments</param>
        private void OnFormatModified(EventArgs e)
        {
            if (this.FormatModified != null)
            {
                this.FormatModified(this, e);
            }
        }


        private C1InputMask _preViewMask = new C1InputMask();

        public C1InputMask TempMask
        {
            get { return this._preViewMask; }
        }


        /// <summary>
        /// Validates the Pattern property.
        /// </summary>
        /// <returns>
        /// A <b>bool</b> value indicate whether the format is right.
        /// </returns>
        private bool ValidatePattern()
        {
            try
            {
                this.ErrorProvider.SetError(this.patternButton, null);
                this.TempMask.MaskFormat = this.patternEditor.Text;
            }
            catch (Exception ex)
            {
                this.ErrorProvider.SetError(this.patternButton, ex.Message);
                return false;
            }

            return true;
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the PatternButton click.
        /// </summary>
        /// <param name="sender"><see cref="object"/> that is the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> containing the event data</param>
        private void PatternButton_Click(object sender, EventArgs e)
        {
            this.patternContextMenuStrip.Show(this.patternButton, new Point(this.patternButton.Width, 0));
        }

        /// <summary>
        /// Occurs when the SampleList selected index changed.
        /// </summary>
        /// <param name="sender"><see cref="object"/> that is the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> containing the event data</param>
        private void SampleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._eventLock)
            {
                return;
            }

            try
            {
                this._eventLock = true;
                FormatSample sample = null;
                if (this.sampleList.SelectedItem != this._customListItem && this.sampleList.SelectedIndex != -1)
                {
                    sample = this._maskFormatSetting.Samples[this.sampleList.SelectedIndex];
                }

                if (sample != null)
                {
                    this.patternEditor.Text = string.Empty;

                    for (int i = 0; i < sample.PatternNames.Count && i < sample.PatternValues.Count; i++)
                    {
                        switch (sample.PatternNames[i])
                        {
                            case "Pattern":
                                this.patternEditor.Text = sample.PatternValues[i];
                                break;
                        }
                    }
                }

                this.TempMask.MaskFormat = this.patternEditor.Text;

                this.previewMask.Text = this.TempMask.EmptyString;

                this.OnFormatModified(EventArgs.Empty);
            }
            finally
            {
                this._eventLock = false;
            }
        }

        private void ValidatingAll()
        {
            if (this._eventLock)
            {
                return;
            }
            try
            {
                this._eventLock = true;
                this.OnFormatModified(EventArgs.Empty);
            }
            finally
            {
                this._eventLock = false;
            }
        }

        /// <summary>
        /// Occurs when the PatternEditor validating.
        /// </summary>
        /// <param name="sender"><see cref="object"/> that is the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> containing the event data</param>
        private void PatternEditor_Validating(object sender, CancelEventArgs e)
        {
            // if (this.ActiveControl != patternButton && this._patternEditorChanged)
            if (this._patternEditorChanged)
            {
                e.Cancel = !this.ValidatePattern();
                if (!e.Cancel)
                {
                    ValidatingAll();
                    this.sampleList.SelectedItem = this._customListItem;
                    this._patternEditorChanged = false;
                }
            }
        }


        /// <summary>
        /// Occurs when the patternButton validating.
        /// </summary>
        /// <param name="sender"><see cref="object"/> that is the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> containing the event data</param>
        private void patternButton_Validating(object sender, CancelEventArgs e)
        {
            if (this.ActiveControl != patternEditor)
            {
                e.Cancel = !this.ValidatePattern();
            }
        }

        /// <summary>
        /// Occurs when the PatternEditor text changed.
        /// </summary>
        /// <param name="sender"><see cref="object"/> that is the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> containing the event data</param>
        private void PatternEditor_TextChanged(object sender, EventArgs e)
        {
            this._patternEditorChanged = true;
        }


        /// <summary>
        /// Occurs when the PatternContextMenuStrip closed.
        /// </summary>
        /// <param name="sender"><see cref="object"/> that is the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> containing the event data</param>
        private void PatternContextMenuStrip_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            this.patternEditor.Focus();
        }

        #endregion
    }

}
