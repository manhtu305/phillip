using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Design;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel.Design;


namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    using C1.Web.UI.Interfaces;

    public class C1CalendarIdUITypeEditor : UITypeEditor
    {
        private ITypeDescriptorContext a;
        private object b;
        private IWindowsFormsEditorService c;
        private System.Windows.Forms.ListBox d;
        private object e;

        public C1CalendarIdUITypeEditor()
        {
            this.d = new System.Windows.Forms.ListBox();
            //this.d.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d.SelectedValueChanged += new EventHandler(this.ValueChanged);
        }

        public override object EditValue(ITypeDescriptorContext _context, IServiceProvider provider, object value)
        {
            this.e = null;
            if (((_context != null) && (_context.Instance != null)) && (provider != null))
            {
                if (_context.Instance is ControlDesigner)
                {
                    this.b = ((ControlDesigner)_context.Instance).Component;
                }
                else if (_context.Instance is DesignerActionList)
                {
                    this.b = ((DesignerActionList)_context.Instance).Component;
                }
                else
                {
                    this.b = _context.Instance;
                }
                this.a = _context;
                this.c = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                if (this.c != null)
                {
                    this.d.Items.Clear();
                    WebControl control1 = (WebControl)this.b;
                    this.FillDropDownList(this.d, control1.Page.Controls);
                    this.c.DropDownControl(this.d);
                    if (control1 is IC1CalendarConsumer)
                    {
                        ((IC1CalendarConsumer)control1).SetCalendarAtDesignTime((string)this.e);
                    }
                }
            }
            if (this.e != null)
            {
                return this.e;
            }
            return value;
        }


        private void FillDropDownList(System.Windows.Forms.ListBox _list, ControlCollection controls)
        {
            for (int num1 = 0; num1 < controls.Count; num1++)
            {
                System.Web.UI.Control control1 = controls[num1];
                string text1 = control1.GetType().ToString().ToLower();
                if (text1.IndexOf("c1calendar") >= 0)
                {
                    if(!_list.Items.Contains(controls[num1].ID))
                        _list.Items.Add(controls[num1].ID);
                }
                if (control1.Controls != null)
                {
                    if (control1.Controls.Count > 0)
                        this.FillDropDownList(_list, control1.Controls);
                }
            }
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext _context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            if (this.c != null)
            {
                this.e = this.d.SelectedItem;
                this.c.CloseDropDown();
                this.a.OnComponentChanged();
            }
        }
    }

}
