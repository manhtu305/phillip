﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using C1.Web.Wijmo.Controls.C1Input;
using C1.Web.Wijmo.Controls.Design.C1Input;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    internal class C1InputTextFormatEditor : UserControl, IFormatEditor
    {
        #region Instance Data
        private Label lblPattern;
        private Button formatButton;
        private TextBox formatEditor;
        private DataGridViewListPlus sampleList;
        private DataGridViewList headerSampleList;
        private TableLayoutPanel tableLayoutPanel2;
        private ContextMenuStrip formatContextMenuStrip;
        private C1InputText preview;

        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel3;
        private FormatSetting _editFormatSetting = null;
        private ErrorProvider _errorProvider = null;
        private DataGridViewRow _customListItem = null;
        private bool _eventLock = false;
        private bool checkedFromKey = false;
        private IContainer components = null;
        public event EventHandler FormatModified;

        #endregion

        #region Constructors
        public C1InputTextFormatEditor()
            : this(false)
        {
        }

        public C1InputTextFormatEditor(bool isPropertyPageStyle)
        {
            InitializeComponent();
            AttachEvent();
            LoadResources();
        }

        protected override void OnLayout(LayoutEventArgs e)
        {
            base.OnLayout(e);
            this.sampleList.AutoSizeColumns();
            headerSampleList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            this.headerSampleList.Columns[1].Width = this.sampleList.ClientSize.Width - this.sampleList.Columns[0].Width -
                                                     this.sampleList.Columns[1].Width;
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
            int oldformatButtonWidth = this.formatButton.Size.Width;
            this.formatButton.Dock = DockStyle.None;
            this.formatButton.Size = new Size(oldformatButtonWidth, this.formatEditor.Height);
        }

        #endregion

        #region Initialize & Dispose

        protected void InitializeComponent()
        {

            this.components = new System.ComponentModel.Container();
            this.lblPattern = new System.Windows.Forms.Label();
            this.formatEditor = new System.Windows.Forms.TextBox();
            this.formatButton = new System.Windows.Forms.Button();
            this.sampleList = new DataGridViewListPlus();
            this.headerSampleList = new DataGridViewList();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.formatContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.preview = new C1InputText();

            ((System.ComponentModel.ISupportInitialize)(this.sampleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerSampleList)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();

            InitializeStandardStyleUI();
            this.sampleList.CausesValidation = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        #region Properties

        private ErrorProvider ErrorProvider
        {
            get
            {
                return this._errorProvider ?? (this._errorProvider = new ErrorProvider(this.components));
            }
        }

        public string Format
        {
            get
            {
                return this.formatEditor.Text;
            }
            set
            {
                if (this._eventLock)
                {
                    return;
                }

                this._eventLock = true;
                this.formatEditor.Text = value;

                this._eventLock = false;
            }
        }

        #endregion

        #region Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            SyncListBoxWidth();
        }

        /// <summary>
        /// Initializes the UI with standard style.
        /// </summary>
        private void InitializeStandardStyleUI()
        {
            // 
            // lblPattern
            // 
            this.lblPattern.AutoSize = true;
            this.lblPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPattern.Location = new System.Drawing.Point(12, 17);
            this.lblPattern.Margin = new System.Windows.Forms.Padding(0, 1, 3, 1);
            this.lblPattern.Name = "lblPattern";
            this.lblPattern.Size = new System.Drawing.Size(41, 13);
            this.lblPattern.TabIndex = 0;
            this.lblPattern.Text = "Pattern";
            // 
            // formatEditor
            // 
            this.formatEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formatEditor.Location = new System.Drawing.Point(14, 37);
            this.formatEditor.Margin = new System.Windows.Forms.Padding(3, 1, 3, 5);
            this.formatEditor.Name = "formatEditor";
            this.formatEditor.Size = new System.Drawing.Size(343, 22);
            this.formatEditor.TabIndex = 1;
            // 
            // formatButton
            //
            this.formatButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formatButton.Margin = new System.Windows.Forms.Padding(0, 1, 5, 5);
            this.formatButton.Name = "formatButton";
            this.formatButton.Size = new System.Drawing.Size(23, 22);
            this.formatButton.TabIndex = 2;
            this.formatButton.Text = ">";
            this.formatButton.TabStop = false;

            //
            //tableLayoutPanel3
            //
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.lblPattern, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.formatEditor, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.formatButton, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());

            // 
            // sampleList
            // 
            this.sampleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sampleList.Location = new System.Drawing.Point(14, 20);
            this.sampleList.Name = "sampleList";
            this.sampleList.Size = new System.Drawing.Size(368, 185);
            this.sampleList.TabIndex = 0;
            this.sampleList.TabStop = false;
            this.sampleList.Margin = new Padding(3, 0, 3, 3);
            this.sampleList.AllowUserToResizeColumns = false;
            //
            //headerSampleList
            //
            this.headerSampleList.Dock = DockStyle.Fill;
            this.headerSampleList.Location = new System.Drawing.Point(14, 20);
            this.headerSampleList.Name = "headerSampleList";
            this.headerSampleList.TabIndex = 0;
            this.headerSampleList.TabStop = false;
            this.headerSampleList.Margin = new Padding(3, 3, 3, 0);
            this.headerSampleList.Size = new System.Drawing.Size(368, 185);
            this.headerSampleList.AllowUserToResizeRows = false;
            this.headerSampleList.AllowUserToResizeColumns = false;
            this.headerSampleList.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.headerSampleList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            //
            //tableLayoutPanel2
            //
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.headerSampleList, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.sampleList, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel2.TabIndex = 3;

            // 
            // formatContextMenuStrip
            // 
            this.formatContextMenuStrip.AllowDrop = true;
            this.formatContextMenuStrip.Name = "patternContextMenuStrip";
            this.formatContextMenuStrip.Size = new System.Drawing.Size(61, 4);

            //
            //tableLayoutPanel1
            //
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());

            // 
            // EditFormatEditor
            // 
            this.Controls.Add(this.tableLayoutPanel1);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Name = "EditFormatEditor";
            ((System.ComponentModel.ISupportInitialize)(this.sampleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerSampleList)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
        }

        public void PostFormLoad()
        {
            this.sampleList.SelectedIndex = -1;
            this.sampleList.SelectedIndexChanged += this.SampleList_SelectedIndexChanged;
        }

        /// <summary>
        /// Determines whether the format should be persisted.
        /// </summary>
        /// <returns>
        /// Boolean value to stop the Format property automatically creating code at design time;
        /// this implementation always returns false.
        /// </returns>
        protected bool ShouldSerializeFormat()
        {
            return false;
        }

        /// <summary>
        /// Loads the resources from an XML file.
        /// </summary>
        private void LoadResources()
        {
            const string prefix = "C1InputTextFormatEditor.";

            this._editFormatSetting = FormatSetting.Parse(C1Localizer.GetString("C1InputTextFormatEditor.EditFormatSetting"))[0];

            FormatEditorHelper.Initialize(this.headerSampleList, this._editFormatSetting.Samples);
            FormatEditorHelper.Initialize2(this.sampleList, this._editFormatSetting.Samples);

            this.sampleList.Columns[0].HeaderText = FormatEditorHelper.GetStringFromResource("SampleList.Header.Pattern", prefix) + "  ";
            this.sampleList.Columns[1].HeaderText = FormatEditorHelper.GetStringFromResource("SampleList.Header.Description", prefix);
            this.headerSampleList.Columns[0].HeaderText = FormatEditorHelper.GetStringFromResource("SampleList.Header.Pattern", prefix) + "  ";
            this.headerSampleList.Columns[1].HeaderText = FormatEditorHelper.GetStringFromResource("SampleList.Header.Description", prefix);

            for (int i = 0; i < this._editFormatSetting.Defination.PatternDefinations.Count; i++)
            {
                if (this._editFormatSetting.Defination.PatternDefinations[i].Name == "Format")
                {
                    FormatEditorHelper.Initialize(
                        this.formatContextMenuStrip,
                        this._editFormatSetting.Defination.PatternDefinations[i],
                        this.formatEditor);
                }
            }

            if (FormatEditorHelper.FormatDropDownButtonImage != null)
            {
                this.formatButton.Image = new Bitmap(FormatEditorHelper.FormatDropDownButtonImage);
                this.formatButton.Text = string.Empty;
            }
            else
            {
                this.formatButton.Text = ">";
            }

            this.lblPattern.Text = C1Localizer.GetString("C1AdvancedMaskFormatEditor.lblPattern");
        }

        /// <summary>
        /// Raises the FormatModified event.
        /// </summary>
        /// <param name="e">
        /// A <see cref="EventArgs"/> indicates the event arguments
        /// </param>
        protected virtual void OnFormatModified(EventArgs e)
        {
            if (this.FormatModified != null)
            {
                this.FormatModified(this, e);
            }
        }

        /// <summary>
        /// Validates the format property.
        /// </summary>
        /// <returns>
        /// A <b>bool</b> value indicate whether the format is right.
        /// </returns>
        private bool ValidateFormat()
        {
            try
            {
                this.ErrorProvider.SetError(this.formatButton, null);
                string format = this.preview.Format;
                if (format != null && format != this.formatEditor.Text)
                {
                    this.preview.Format = this.formatEditor.Text;
                }
            }
            catch (Exception ex)
            {
                this.ErrorProvider.SetError(this.formatButton, ex.Message);
                return false;
            }
            return true;
        }

        private void AttachEvent()
        {
            this.formatButton.Click += this.FormatButton_Click;
            this.formatEditor.Validating += this.FormatEditor_Validating;
            this.formatEditor.TextChanged += this.FormatEditor_TextChanged;
            this.formatContextMenuStrip.Closed += this.FormatContextMenuStrip_Closed;
            this.formatButton.Validating += formatButton_Validating;
            this.sampleList.Scroll += sampleList_Scroll;
            this.Paint += EditFormatEditor_Paint;
        }

        private void SyncListBoxWidth()
        {
            headerSampleList.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            headerSampleList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            sampleList.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            sampleList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            sampleList.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.sampleList.Columns[1].Width = this.headerSampleList.Columns[0].Width - this.sampleList.Columns[0].Width;
            this.headerSampleList.Height = this.headerSampleList.ColumnHeadersHeight;
            this.sampleList.AutoSizeColumns();
        }

        #endregion

        #region Events


        private void EditFormatEditor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void sampleList_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                headerSampleList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
                this.headerSampleList.Columns[1].Width = this.sampleList.Columns[2].Width + this.sampleList.Columns[0].Width;

                this.headerSampleList.HorizontalScrollingOffset = e.NewValue;
            }
        }

        private void SampleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._eventLock)
            {
                return;
            }

            try
            {
                this._eventLock = true;

                int selectIndex = this.sampleList.SelectedIndex;

                if (selectIndex == -1)
                    return;

                if ((bool)this.sampleList.Rows[selectIndex].Cells[0].Value)  //s DataGridViewCheckBoxColumn)
                {
                    this.sampleList.Rows[selectIndex].Cells[0].Value = false;
                }
                else
                {
                    this.sampleList.Rows[selectIndex].Cells[0].Value = true;
                }

                FormatSample sample = this._editFormatSetting.Samples[selectIndex];
                string selectedPatternValue = string.Empty;
                if (sample != null)
                {
                    for (int i = 0; i < sample.PatternNames.Count && i < sample.PatternValues.Count; i++)
                    {
                        switch (sample.PatternNames[i])
                        {
                            case "Format":
                                selectedPatternValue = sample.PatternValues[i];
                                break;
                        }
                    }
                }

                int caretOffset = this.formatEditor.SelectionStart;
                if ((bool)this.sampleList.Rows[selectIndex].Cells[0].Value)
                {
                    if (this.Format != null)
                    {
                        string prefixString = this.Format.Substring(0, caretOffset);
                        string suffixString = this.Format.Substring(caretOffset, this.Format.Length - caretOffset);
                        if (suffixString.Length > 0 && this.formatEditor.SelectionLength > 0)
                        {
                            suffixString = suffixString.Substring(this.formatEditor.SelectionLength, suffixString.Length - this.formatEditor.SelectionLength);
                        }
                        this.formatEditor.Text = prefixString + selectedPatternValue + suffixString;

                    }
                    else
                    {
                        this.formatEditor.Text = selectedPatternValue;
                    }
                    this.formatEditor.SelectionStart = caretOffset + 1;
                }
                else
                {
                    int newCaretOffset = caretOffset;
                    string newFormat = String.Empty;
                    for (int i = 0; i < this.Format.Length; i++)
                    {
                        if (selectedPatternValue.Equals(this.Format.Substring(i, 1)))
                        {
                            if (i < caretOffset)
                            {
                                newCaretOffset = newCaretOffset - 1;
                            }
                        }
                        else
                        {
                            newFormat = newFormat + this.Format.Substring(i, 1);
                        }
                    }

                    this.formatEditor.Text = newFormat;
                    this.formatEditor.SelectionStart = newCaretOffset;
                }

                if (!checkedFromKey)
                {
                    this.formatEditor.Focus();
                }

                SelectCustomFormat(this.Format);
                this.sampleList.SelectedIndex = -1;

                try
                {
                    this.preview.Format = this.formatEditor.Text;
                    this.OnFormatModified(EventArgs.Empty);
                }
                catch (Exception)
                {

                }

            }
            finally
            {
                this._eventLock = false;
            }
        }

        internal void SelectCustomFormat(string format)
        {
            for (int i = 0; i < sampleList.Rows.Count; i++)
            {
                this.sampleList.Rows[i].Cells[0].Value = format.Contains((string)this.sampleList.Rows[i].Cells[1].Value);
            }
        }

        private void FormatEditor_TextChanged(object sender, EventArgs e)
        {
            SelectCustomFormat(this.Format);
            try
            {
                this.preview.Format = this.formatEditor.Text;
                this.OnFormatModified(EventArgs.Empty);
            }
            catch (Exception)
            {

            }
        }

        private void FormatButton_Click(object sender, EventArgs e)
        {
            this.formatContextMenuStrip.Show(this.formatButton, new Point(this.formatButton.Width, 0));
        }

        private void FormatContextMenuStrip_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            this.formatEditor.Focus();
        }

        private void FormatEditor_Validating(object sender, CancelEventArgs e)
        {
            if (this.ActiveControl != formatButton)
            {
                e.Cancel = !this.ValidateFormat();
                if (!e.Cancel)
                {
                    if (this._eventLock)
                    {
                        return;
                    }
                    try
                    {
                        this._eventLock = true;
                        this.OnFormatModified(EventArgs.Empty);
                    }
                    finally
                    {
                        this._eventLock = false;
                    }
                }
            }
        }

        void formatButton_Validating(object sender, CancelEventArgs e)
        {
            if (this.ActiveControl != formatEditor)
            {
                e.Cancel = !this.ValidateFormat();
            }
        }

        #endregion
    }
}
