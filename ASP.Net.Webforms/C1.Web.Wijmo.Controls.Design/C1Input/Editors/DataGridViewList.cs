﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    internal class DataGridViewList : DataGridView
    {
        private bool _ignoreSelectionChanged;

        #region Constructor

        public DataGridViewList()
        {
            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.AllowUserToOrderColumns = false;
            this.AllowUserToResizeRows = false;
            this.RowHeadersVisible = false;
            this.ReadOnly = true;
            this.MultiSelect = false;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            this.Columns.Add("Content", "Content");
            this.Columns.Add("Description", "Description");
            this.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            this.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

            this.SelectionChanged += new EventHandler(DataGridViewList_SelectionChanged);
        }

        #endregion Constructor

        #region Properties

        public int SelectedIndex
        {
            get
            {
                if (this.SelectedRows.Count > 0)
                {
                    return this.SelectedRows[0].Index;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                this.ClearSelection();
                if (value != -1)
                {
                    this.Rows[value].Selected = true;
                }
            }
        }

        public DataGridViewRow SelectedItem
        {
            get
            {
                int index = this.SelectedIndex;
                if (index == -1)
                {
                    return null;
                }
                else
                {
                    return this.Rows[index];
                }
            }
            set
            {
                this.ClearSelection();
                if (value != null)
                {
                    value.Selected = true;
                }
            }
        }

        #endregion Properties

        #region Methods

        public void FillColumns()
        {
            this.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        public void AllowSorting()
        {
            this.Columns[0].SortMode = DataGridViewColumnSortMode.Automatic;
            this.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
        }

        public void AutoSizeColumns()
        {
            this.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            int autoWidth = this.Columns[1].Width;
            int width = this.ClientSize.Width - this.Columns[0].Width - 3;

            int detalValue = (IsVerticalScrollShow() ? SystemInformation.VerticalScrollBarWidth : 0);
            if (width - autoWidth > detalValue)
            {
                this.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                this.Columns[1].Width = width - detalValue;
            }
        }

        public bool IsVerticalScrollShow()
        {
            if (this.RowCount > 0)
            {
                int preferredSize = this.Rows[0].Height * this.RowCount + this.ColumnHeadersHeight;
                if (preferredSize >= this.ClientSize.Height - 2)
                {
                    return true;
                }
            }
            return false;
        }

        public void SelectByContent(string content)
        {
            this.ClearSelection();
            foreach (DataGridViewRow row in this.Rows)
            {
                if (row.Cells[0].Value.ToString() == content)
                {
                    row.Selected = true;
                    return;
                }
            }
        }

        #endregion Methods

        #region Events

        public event EventHandler SelectedIndexChanged;

        private void DataGridViewList_SelectionChanged(object sender, EventArgs e)
        {
            if (_ignoreSelectionChanged)
                return;

            OnSelectedIndexChanged(e);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            _ignoreSelectionChanged = true;
            base.OnHandleCreated(e);
            _ignoreSelectionChanged = false;
        }

        private void OnSelectedIndexChanged(EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
            {
                SelectedIndexChanged(this, e);
            }
        }

        #endregion Events
    }

    internal class DataGridViewListPlus : DataGridViewList
    {
        public DataGridViewListPlus()
        {
            this.ColumnHeadersVisible = false;
            DataGridViewCheckBoxColumn newColumn = new DataGridViewCheckBoxColumn();
            this.Columns.Insert(0, newColumn);
        }

        public new void AutoSizeColumns()
        {
            this.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            int autoWidth = this.Columns[2].Width;
            int width = this.ClientSize.Width - this.Columns[0].Width - this.Columns[1].Width - 3;

            int detalValue = (IsVerticalScrollShow() ? SystemInformation.VerticalScrollBarWidth : 0);
            if (width - autoWidth > detalValue)
            {
                this.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                this.Columns[2].Width = width - detalValue;
            }
        }
    }
}
