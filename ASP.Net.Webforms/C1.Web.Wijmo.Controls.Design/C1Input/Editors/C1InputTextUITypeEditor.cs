using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.ComponentModel;
using System.Globalization;



namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    using C1.Web.Wijmo.Controls.C1Input;
    using C1.Web.Wijmo.Controls.Design.C1Input;

    /// <summary>
    /// Text Property editor
    /// </summary>
    internal class C1InputTextUITypeEditor : UITypeEditor
    {

        internal class HelperMaskedTextBox : MaskedTextBox
        {
            public bool isAbortedFlag = false;

            public HelperMaskedTextBox()
                : base()
            {
            }
            public HelperMaskedTextBox(MaskedTextProvider maskedTextProvider)
                : base(maskedTextProvider)
            {
            }

            protected override bool IsInputKey(Keys keyData)
            {
                if (keyData == Keys.Escape)
                {
                    isAbortedFlag = true;
                }
                return base.IsInputKey(keyData);
            }
        }

        internal IWindowsFormsEditorService edSvc;
        HelperMaskedTextBox _MaskedTextBox;

        public C1InputTextUITypeEditor()
        {
        }
        //public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        //{
        //    return true;
        //}

        //public override void PaintValue(PaintValueEventArgs e)
        //{
        //    base.PaintValue(e);
        //}
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if ((context != null) && (context.Instance != null))
            {
                return UITypeEditorEditStyle.DropDown;
            }
            return base.GetEditStyle(context);

        }


        /// <summary>
        /// Edits the value of the specified object using the editor.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null || context.Instance == null || provider == null)
                return value;
            try
            {
                // get service
                edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                if (edSvc == null)
                    return value;
                string prevValue = (string)value;
                _MaskedTextBox = new HelperMaskedTextBox();


                string mask = (string)TypeDescriptor.GetProperties(context.Instance)["Mask"].GetValue(context.Instance);
                
                CultureInfo culture = (CultureInfo)TypeDescriptor.GetProperties(context.Instance)["UsedCulture"].GetValue(context.Instance);

                _MaskedTextBox.Mask = C1MaskedTextProvider.GetResultingMaskForStandardMaskedTextBox(mask, culture);
                _MaskedTextBox.Culture = culture;
                char promptChar = (char)TypeDescriptor.GetProperties(context.Instance)["PromptChar"].GetValue(context.Instance);
                _MaskedTextBox.PromptChar = promptChar;

                string passwordChar = (string)TypeDescriptor.GetProperties(context.Instance)["PasswordChar"].GetValue(context.Instance);
                if (passwordChar.Length > 0)
                    _MaskedTextBox.PasswordChar = passwordChar[0];

                _MaskedTextBox.KeyDown += new KeyEventHandler(MaskedTextBox_KeyDown);
                _MaskedTextBox.Text = (string)value;
                this.edSvc.DropDownControl(_MaskedTextBox);
                string result;
                if (!_MaskedTextBox.isAbortedFlag)
                {
                    result = _MaskedTextBox.Text;
                }
                else
                {
                    result = prevValue;
                }
                _MaskedTextBox.Dispose();
                _MaskedTextBox = null;
                this.edSvc = null;
                return result;

            }
            catch (Exception)
            {
                //MessageBox.Show("Error:" + e.ToString() + ":" + e.StackTrace);
                return value;
            }
        }


        void MaskedTextBox_KeyDown(object sender, KeyEventArgs e)
        {            
            if (e.KeyData == Keys.Escape)
            {
                this.edSvc.CloseDropDown();
            }
 			if(e.KeyData == Keys.Enter)
 			{
 				this.edSvc.CloseDropDown();
 			}

        }
    }



}