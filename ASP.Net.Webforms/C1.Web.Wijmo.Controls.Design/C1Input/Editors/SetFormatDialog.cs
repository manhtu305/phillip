﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    // Added by jiang for fixed bug3018 at Sep. 16 2010
    // Copy from MR
    interface IFormatEditor
    {
        void PostFormLoad();
    }
    // End by jiang

    #region The SetFormatDialog class.
    /// <summary>
    /// Represents a common host dialog for all format editors.
    /// </summary>
    /// <typeparam name="TFormatEditor">
    /// A custom control that is hosted in this dialog. The control must have a default constructor.
    /// </typeparam>
    internal class SetFormatDialog<TFormatEditor> : Form
        // Modified by jiang for fixed bug3018 at Sep. 16 2010
        // Copy from MR
        //where TFormatEditor : Control, new()
        where TFormatEditor : Control, IFormatEditor, new()
    // End by jiang
    {

        #region Members
        /// <summary>
        /// A <see cref="System.Windows.Forms.Button"/> indicates the cannecl button.
        /// </summary>
        private System.Windows.Forms.Button _cancelButton;
        /// <summary>
        /// A <see cref="System.Windows.Forms.Button"/> indicates the ok button.
        /// </summary>
        private System.Windows.Forms.Button _okButton;
        /// <summary>
        /// A <typeparamref name="TFormatEditor"/> indicates the format editor control.
        /// </summary>
        /// 
        private TFormatEditor _formatEditor;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        #endregion end of Members.

        /// <summary>
        /// Creates a new format dialog editor.
        /// </summary>
        public SetFormatDialog()
        {
            this._cancelButton = new System.Windows.Forms.Button();
            this._okButton = new System.Windows.Forms.Button();
            this._formatEditor = new TFormatEditor();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Cancel Button
            // 
            this._cancelButton.AutoSize = true;
            this._cancelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelButton.CausesValidation = false;
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Margin = new System.Windows.Forms.Padding(0, 5, 12, 0);
            this._cancelButton.TabIndex = 2;
            this._cancelButton.Text = C1Localizer.GetString("Base.Cancel");
            this._cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Ok Button
            // 
            this._okButton.AutoSize = true;
            this._okButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._okButton.Margin = new System.Windows.Forms.Padding(0, 5, 12, 0);
            this._okButton.TabIndex = 1;
            this._okButton.Text = C1Localizer.GetString("Base.OK");
            this._okButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // Format Editor
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._formatEditor, 3);
            this._formatEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._formatEditor.Margin = new System.Windows.Forms.Padding(12, 12, 12, 3);
            this._formatEditor.TabIndex = 0;
            //
            //tableLayoutPanel
            //  
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100));
            this.tableLayoutPanel1.Controls.Add(this._formatEditor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._okButton, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this._cancelButton, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(532, 423);
            this.tableLayoutPanel1.TabIndex = 0;

            // 
            // SetFormatDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 430);
            this.Controls.Add((this.tableLayoutPanel1));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            // Bug CMN_NEW_003_003 fixed by KevinShan on 2005/09/26
            // ------------------------------------------------------
            this.AcceptButton = this._okButton;
            this.CancelButton = this._cancelButton;
            this.ShowInTaskbar = false;
            //this.HelpButton = true;
            // ------------------------------------------------------

            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = C1Localizer.GetString("C1InputSetFormatDialog.DialogTitle");
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

            // Leo added at 2005.11.01 fix bug ID = TIM_FEA_001_020.
            // When popup the set format dialog in design time, when the control set Site property, it will call OnCursorChanged 
            // if its Cursor is default state. In our EditBase, the value of control's Cursor will set to our view system. Then
            // the cached value in view is not null, we can not show IBeam in our editor. The reason is: we have such logic in
            // our OnCursorChanged method: this.Wrapper.Cursor = base.Cursor. Then our views will hold a cursor value of
            // Cursors.Default.So the cursor will always a arrow but not IBeam.
            // Eric does not want to change current logic of cursor this version. So I have to reset each InputMan control's
            // Cursor before form is shown.
            this.Load += new EventHandler(SetFormatDialog_Load);
        }

        /// <summary>
        /// Processes the load event of the SetFormatDialog.
        /// </summary>
        /// <param name="sender"><b>Object</b> that contains the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> object that contains no event data</param>
        private void SetFormatDialog_Load(object sender, EventArgs e)
        {
            this._formatEditor.PostFormLoad();
        }

        /// <summary>
        /// Receives the Click event coming form the OK button.
        /// </summary>
        /// <param name="sender"><b>Object</b> that contains the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> that contains the event arguments</param>
        private void OKButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        internal void SetTitle(string title)
        {
            this.Text = title;
        }

        /// <summary>
        /// Receives the click event coming form the Cancel button.
        /// </summary>
        /// <param name="sender"><b>Object</b> that contains the source of the event</param>
        /// <param name="e"><see cref="EventArgs"/> that contains the event arguments</param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Gets the format editor.
        /// </summary>
        /// <value><typeparamref name="TFormatEditor"/> that contains the format editor control</value>
        public TFormatEditor FormatEditor
        {
            get
            {
                return this._formatEditor;
            }
        }
    }

    #endregion
}