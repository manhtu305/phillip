﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    internal class SmartTagDateTimeEditor : UITypeEditor
    {
        private class DateTimeUI : Control
        {
            private IWindowsFormsEditorService _edSvc;
            private readonly InnerCalendar _monthCalendar = new InnerCalendar();
            private object _value;

            public DateTimeUI()
            {
                this.InitializeComponent();
                base.Size = this._monthCalendar.SingleMonthSize;
                this._monthCalendar.Resize += this.MonthCalResize;
            }

            public void End()
            {
                this._edSvc = null;
                this._value = null;
            }

            private void InitializeComponent()
            {
                this._monthCalendar.DateSelected += this.OnDateSelected;
                this._monthCalendar.KeyDown += this.MonthCalKeyDown;
                base.Controls.Add(this._monthCalendar);
            }

            private void MonthCalKeyDown(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    this.OnDateSelected(sender, null);
                }
            }

            private void MonthCalResize(object sender, EventArgs e)
            {
                base.Size = this._monthCalendar.Size;
                if (this.Dock == DockStyle.Fill && this.Size != this._monthCalendar.Size)
                {
                    if (this.Parent != null)
                    {
                        this.Parent.ClientSize = this._monthCalendar.Size;
                    }
                }
            }

            private void OnDateSelected(object sender, DateRangeEventArgs e)
            {
                this._value = this._monthCalendar.SelectionStart;
                this._edSvc.CloseDropDown();
            }

            public void Start(IWindowsFormsEditorService edSvc, object value)
            {
                this._monthCalendar.MaxSelectionCount = 1;
                this._edSvc = edSvc;
                this._value = value;
                if (value is DateTime)
                {
                    this._monthCalendar.Value = (DateTime)value;
                }
            }

            public object Value
            {
                get
                {
                    return this._value;
                }
            }


        }

        private IWindowsFormsEditorService _edSvc;

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context != null && context.Instance != null && provider != null)
            {
                _edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

                if (_edSvc != null)
                {
                    InnerCalendar calendar = new InnerCalendar();
                    DateTime dateTime = DateTime.Now;

                    if (value is DateTime)
                    {
                        dateTime = (DateTime)value;
                    }


                    using (DateTimeUI eui = new DateTimeUI())
                    {
                        eui.Start(_edSvc, dateTime);
                        _edSvc.DropDownControl(eui);
                        value = eui.Value;
                        eui.End();
                    }

                }
            }
            return value;
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null)
            {
                return UITypeEditorEditStyle.DropDown;
            }

            return base.GetEditStyle(context);
        }
    }

    internal class InnerCalendar : MonthCalendar
    {
        private bool _isValueChanged;
        private DateTime _value;
        private DateTime _maxDate = DateTime.MaxValue;
        private DateTime _minDate = DateTime.MinValue;
        private static readonly DateTime MIN_MINDATE = new DateTime(1753, 1, 1, 0, 0, 0, 0);
        private static readonly DateTime MAX_MAXDATE = new DateTime(9998, 12, 31, 23, 59, 59, 999);

        public DateTime Value
        {
            get
            {
                return this.ValidateValue(this._value);
            }

            set
            {
                if (this._value != value)
                {
                    this._value = value;
                    this.UpdateValueToCalendar();
                }
            }
        }

        public DateTime NewMaxDate
        {
            get
            {
                return this._maxDate;
            }

            set
            {
                if (value < this.NewMinDate)
                {
                    throw new ArgumentException("Invalid value.");
                }

                this._maxDate = value;

                this.UpdateMaxDateToCalendar();
            }
        }

        public DateTime NewMinDate
        {
            get
            {
                return this._minDate;
            }

            set
            {
                if (value > this.NewMaxDate)
                {
                    throw new ArgumentException("Invalid value.");
                }

                this._minDate = value;

                this.UpdateMinDateToCalendar();
            }
        }

        public bool IsValueChanged
        {
            get
            {
                return this._isValueChanged;
            }
        }

        private void UpdateValueToCalendar()
        {
            DateTime realValue = this.ValidateValue(this._value);
            this.SelectionStart = this.CalendarValidateValue(realValue);
        }

        private void UpdateMaxDateToCalendar()
        {
            this.MaxDate = this.CalendarValidateValue(this._maxDate);
        }

        private void UpdateMinDateToCalendar()
        {
            this.MinDate = this.CalendarValidateValue(this._minDate);
        }

        private DateTime ValidateValue(DateTime value)
        {
            if (value > this.NewMaxDate)
            {
                return this.NewMaxDate;
            }
            if (value < this.NewMinDate)
            {
                return this.NewMinDate;
            }

            return value;
        }

        private DateTime CalendarValidateValue(DateTime value)
        {
            if (value > MAX_MAXDATE)
            {
                return MAX_MAXDATE;
            }
            if (value < MIN_MINDATE)
            {
                return MIN_MINDATE;
            }
            return value;
        }

        protected override void OnDateSelected(DateRangeEventArgs drevent)
        {
            this._isValueChanged = false;
            DateTime realValue = this.ValidateValue(drevent.Start);
            if (this.Value != realValue)
            {
                this.Value = realValue;
                this._isValueChanged = true;
            }
            base.OnDateSelected(drevent);
        }
    }
}
