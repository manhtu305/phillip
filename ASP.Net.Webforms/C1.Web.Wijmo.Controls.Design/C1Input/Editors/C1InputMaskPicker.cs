using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;



namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    using C1.Web.Wijmo.Controls.C1Input;
    using C1.Web.Wijmo.Controls.Design.C1Input;
    using C1.Web.Wijmo.Controls.Design.Localization;

    internal partial class C1InputMaskPicker : UserControl, IC1DesignEditor
    {
        Hashtable _mask = new Hashtable();
        ListViewItem _custom = null;
        private int _nonUserUpdating = 0;

        public C1InputMaskPicker()
        {
            InitializeComponent();
            LoadListView();
            //C1.Util.Localization.C1Localizer.LocalizeForm(this, components);
            //post localize for mask description & data format captions
            this.listView1.Columns[0].Text = this.listView1.Columns[0].Text;
            this.listView1.Columns[1].Text = this.listView1.Columns[1].Text;
        }

        #region <<<<<< private methods and implementation
        // adds a row to the list view
        private ListViewItem AddListViewRow(string key)
        {
            const string prefix = "C1Input.C1InputMaskPicker.";
            string desc = C1Localizer.GetString(prefix + key, Culture);
            string sample = C1Localizer.GetString(prefix + key + "Sample", Culture);
            string format = C1Localizer.GetString(prefix + key + "Format", Culture);

            var item = this.listView1.Items.Add(format, desc, 0);
            item.SubItems.Add(sample);
            item.Tag = format;
            return item;
        }
        private void LoadListView()
        {
            listView1.Items.Clear();

            AddListViewRow("Time1");
            AddListViewRow("Time2");
            AddListViewRow("ShortDate");
            AddListViewRow("ShortDateTime");

            AddListViewRow("Numeric");

            AddListViewRow("PhoneNumberNoAreaCode");
            AddListViewRow("PhoneNumber");

            AddListViewRow("ZipCode");

            AddListViewRow("State");
            AddListViewRow("Notes");

            AddListViewRow("SSN");
            AddListViewRow("Cost");

            _custom = AddListViewRow("Custom");

        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected && e.Item != _custom)
            {
                this.textBox1.Text = (string)e.Item.Tag;
                SetMaskedTextBoxMask(C1MaskedTextProvider.GetResultingMaskForStandardMaskedTextBox((string)e.Item.Tag, CultureInfo.CurrentCulture));
                this.textBox1.Modified = false;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (this.textBox1.Modified)
            {
                _custom.Selected = true;

                string m = C1MaskedTextProvider.GetResultingMaskForStandardMaskedTextBox(this.textBox1.Text, CultureInfo.CurrentCulture);//dma
                if (!string.IsNullOrEmpty(m))
                {
                    this.SetMaskedTextBoxMask(m);
                }
                _custom.Tag = this.textBox1.Text;//dma
            }
            if (!InNonUserUpdate)
            {
                Binding bind = DataBindings["Mask"];
                if (bind != null)
                    bind.WriteValue();
            }
            OnMaskChanged();

            if (!InNonUserUpdate)
                OnUserPropertyChanged();
        }

        private void SetMaskedTextBoxMask(string maskFormat)
        {
            this.maskedTextBox1.Mask = ConvertNewFormatKeyWord(maskFormat);
            var maskedTextProvider = this.maskedTextBox1.MaskedTextProvider;

            string displayTtext = "";
            if (maskedTextProvider != null)
            {
                displayTtext = maskedTextProvider.ToString(false, true, true, 0, maskedTextProvider.Length);
            }
            this.maskedTextBox1.Mask = "";
            this.maskedTextBox1.Text = displayTtext;
           
        }

        private static Char[]  NewKeyWord = new Char[]{'H', 'K', 'N', MaskFormat.DBCS_9, MaskFormat.DBCS_K, MaskFormat.DBCS_J, MaskFormat.DBCS_Z, MaskFormat.DBCS_N, MaskFormat.DBCS_G};
        private static string ConvertNewFormatKeyWord(string maskFormt)
        {
            StringBuilder sb = new StringBuilder(maskFormt);
            for (int i = 0; i < sb.Length; i++)
            {
                if (NewKeyWord.Contains(sb[i]))
                {
                    if (i - 1 >= 0 && sb[i - 1] == '\\')
                    {

                    }
                    else
                    {
                        sb[i] = '0';
                    }
                }
            }

            return sb.ToString();
        }

        private void SyncListView(string mask)
        {
            string s = mask;
            ListViewItem item = _custom;
            ListViewItem[] items = this.listView1.Items.Find(mask, false);
            if (items.Length > 0)
                item = items[0];
            this.textBox1.Text = mask;
            SetMaskedTextBoxMask(C1MaskedTextProvider.GetResultingMaskForStandardMaskedTextBox(mask, null));
            this.textBox1.Modified = false;
            if (Mask.Length == 0)
            {
                this.listView1.SelectedItems.Clear();
            }
            else if (item != null)
                item.Selected = true;
        }

        internal delegate void MaskedChangedEventHandler(object sender, EventArgs e);
        internal event MaskedChangedEventHandler MaskedChanged;
        internal void OnMaskChanged()
        {
            if (MaskedChanged != null)
                MaskedChanged(this, EventArgs.Empty);
        }
        internal event MaskedChangedEventHandler MaskedUpdateChanged;
        internal void OnMaskUpdateChanged()
        {
            if (MaskedUpdateChanged != null)
                MaskedUpdateChanged(this, EventArgs.Empty);
        }

        private void BeginNonUserUpdate()
        {
            _nonUserUpdating++;
        }
        private void EndNonUserUpdate()
        {
            if (_nonUserUpdating > 0)
                _nonUserUpdating--;
        }
        private bool InNonUserUpdate
        {
            get { return _nonUserUpdating > 0; }
        }


        private void OnUserPropertyChanged()
        {
            if (UserPropertyChanged != null)
                UserPropertyChanged(this, EventArgs.Empty);
        }
        #endregion

        #region <<<<<<<< Properties
        [DefaultValue("")]
        [Bindable(BindableSupport.Yes)]
        public string Mask
        {
            get { return this.textBox1.Text; }
            set
            {
                BeginNonUserUpdate();
                try
                {
                    SyncListView(value);
                }
                finally
                {
                    EndNonUserUpdate();
                }
            } // this.textBox1.Text = value; }
        }

        CultureInfo _culture = CultureInfo.CurrentCulture;
        [DefaultValue("")]
        public CultureInfo Culture
        {
            get { return _culture; }
            set
            {
                value = value ?? CultureInfo.CurrentCulture;
                if (value == _culture)
                    return;
                _culture = value;
                LoadListView();
            } // this.textBox1.Text = value; }
        }
        /*
        public bool UpdateWithLiterals
        {
            get { return this.checkBox1.Checked; }
            set { this.checkBox1.Checked = value; }
        }

        public bool UpdateWithLiteralsPromptVisible
        {
            get { return this.checkBox1.Visible; }
            set { this.checkBox1.Visible = value; }
        }*/

        public event EventHandler UserPropertyChanged;

        #endregion

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            OnMaskUpdateChanged();
        }

        private void C1InputMaskPicker_Load(object sender, EventArgs e)
        {

        }

        #region IC1DesignEditor Members

        private C1InputMask _maskEdit;

        void IC1DesignEditor.setInspectedComponent(object aComponent, System.Web.UI.Design.ControlDesigner controlDesigner)
        {
            this._maskEdit = (C1InputMask)aComponent;
            Culture = _maskEdit.Culture;
            LoadListView();
            this.Mask = this._maskEdit.MaskFormat;
        }

        void IC1DesignEditor.doActionApply()
        {
            TypeDescriptor.GetProperties(this._maskEdit)["MaskFormat"].SetValue(this._maskEdit, this.Mask);
        }

        void IC1DesignEditor.doActionCancel()
        {

        }

        void IC1DesignEditor.doActionExit()
        {

        }

        void IC1DesignEditor.doCurrentTabSelected()
        {
            this.Mask = this._maskEdit.MaskFormat;
        }

        void IC1DesignEditor.doOtherTabSelected()
        {

        }

        #endregion
    }
}
