﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;
using C1.Web.Wijmo.Controls.Design.Localization;
using C1.Web.Wijmo.Controls.Design.UITypeEditors;

namespace C1.Web.Wijmo.Controls.Design.C1Input
{

    #region The DOM of format settings.
    /// <summary>
    /// Defines a class describe the format editing information.
    /// </summary>
    internal class FormatSetting
    {
        /// <summary>
        /// A <b>string</b> indicates the "<formatSettings></formatSettings>" element name.
        /// </summary>
        private const string FORMATSETTINGS = "formatSettings";
        /// <summary>
        /// A <b>string</b> indicates the "<definitions></definitions>" element name.
        /// </summary>
        private const string DEFINITIONS = "definitions";
        /// <summary>
        /// A <b>string</b> indicates the "<samples></samples>" element name.
        /// </summary>
        private const string SAMPLES = "samples";
        /// <summary>
        /// A <b>string</b> indicates the "<description></description>" element name.
        /// </summary>
        private const string DESCRIPTION = "description";
        /// <summary>
        /// A <b>string</b> indicates the "<item/>" element name.
        /// </summary>
        private const string ITEM = "item";
        /// <summary>
        /// A <b>string</b> indicates the "<group></group>" element name.
        /// </summary>
        private const string GROUP = "group";
        /// <summary>
        /// A <b>string</b> indicates the "keywords" attribute name.
        /// </summary>
        private const string KEYWORDS = "keywords";
        /// <summary>
        /// A <b>string</b> indicates the "holder" attribute name.
        /// </summary>
        private const string HOLDER = "holder";
        /// <summary>
        /// A <b>string</b> indicates the "replaceAll" attribute name.
        /// </summary>
        private const string REPLACEALL = "replaceAll";

        /// <summary>
        /// A <b>string</b> indicates the default holder attribute value.
        /// </summary>
        public const string DEFAULT_HOLDER = "*";



        /// <summary>
        /// Creates an array of <see cref="FormatSetting"/>s of the specified XML.
        /// </summary>
        /// <param name="xmlFormatSetting"><see cref="Stream"/> that contains the XML</param>
        /// <returns><see cref="FormatSetting"/> array that contains the format result</returns>
        public static FormatSetting[] Parse(string xmlFormatSetting)
        {
            System.IO.StringReader stringReader = new System.IO.StringReader(xmlFormatSetting);
            System.Xml.XmlReader reader = new System.Xml.XmlTextReader(stringReader);
            try
            {
                return Parse(reader);
            }
            finally
            {
                reader.Close();
            }
        }

        /// <summary>
        /// Creates an array of <see cref="FormatSetting"/>s of the specified XML.
        /// </summary>
        /// <param name="xmlFormatSetting"><see cref="Stream"/> that contains the XML</param>
        /// <returns><see cref="FormatSetting"/> array that contains the format result.</returns>
        public static FormatSetting[] Parse(Stream xmlFormatSetting)
        {
            System.Xml.XmlReader reader = System.Xml.XmlReader.Create(xmlFormatSetting);
            try
            {
                return Parse(reader);
            }
            finally
            {
                reader.Close();
            }
        }

        /// <summary>
        /// Creates an array of <see cref="FormatSetting"/>s of the specified XML.
        /// </summary>
        /// <param name="reader"><see cref="XmlReader"/> that contains the XML</param>
        /// <returns><see cref="FormatSetting"/> array that contains the format result</returns>
        public static FormatSetting[] Parse(XmlReader reader)
        {
            System.Diagnostics.Debug.Assert(reader != null);

            Dictionary<string, FormatSetting> formatSettingsCache = new Dictionary<string, FormatSetting>();

            //
            // Reader xml and create the FormatSetting classes.
            //
            while (reader.Read())
            {
                if (reader.NodeType != System.Xml.XmlNodeType.Element)
                {
                    continue;
                }
                if (reader.Name == FORMATSETTINGS)
                {
                    //
                    // Read formatSettings element.
                    //
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.EndElement)
                        {
                            break;
                        }
                        if (reader.NodeType != System.Xml.XmlNodeType.Element)
                        {
                            continue;
                        }

                        if (reader.Name == DEFINITIONS)
                        {
                            //
                            // Read definations element.
                            //
                            while (reader.Read())
                            {
                                if (reader.NodeType == XmlNodeType.EndElement)
                                {
                                    break;
                                }
                                if (reader.NodeType != System.Xml.XmlNodeType.Element)
                                {
                                    continue;
                                }

                                if (!formatSettingsCache.ContainsKey(reader.Name))
                                {
                                    formatSettingsCache.Add(reader.Name, new FormatSetting());
                                }

                                FormatSetting formatSetting = formatSettingsCache[reader.Name];
                                if (formatSetting.Defination == null)
                                {
                                    formatSetting.Defination = new FormatDefination();
                                }
                                ReadFormatDefination(reader, formatSetting.Defination);
                            }
                        }
                        else if (reader.Name == SAMPLES)
                        {
                            //
                            // Read samples element.
                            //
                            while (reader.Read())
                            {
                                if (reader.NodeType == XmlNodeType.EndElement)
                                {
                                    break;
                                }
                                if (reader.NodeType != System.Xml.XmlNodeType.Element)
                                {
                                    continue;
                                }

                                if (!formatSettingsCache.ContainsKey(reader.Name))
                                {
                                    formatSettingsCache.Add(reader.Name, new FormatSetting());
                                }

                                FormatSetting formatSetting = formatSettingsCache[reader.Name];
                                if (formatSetting.Samples == null)
                                {
                                    formatSetting.Samples = new List<FormatSample>();
                                }
                                FormatSample formatSample = new FormatSample(formatSetting);
                                ReadFormatSample(reader, formatSample);
                                formatSetting.Samples.Add(formatSample);
                            }
                        }
                        else
                        {
                            throw new XmlSchemaException();
                        }
                    }
                }
                else
                {
                    throw new XmlSchemaException();
                }
            }


            FormatSetting[] result = new FormatSetting[formatSettingsCache.Count];
            formatSettingsCache.Values.CopyTo(result, 0);
            return result;
        }

        /// <summary>
        /// Reads the informations from the reader, and used it to intialzie the formatSettting.
        /// </summary>
        /// <param name="reader"><b>XML</b> reader that contains the format setting information</param>
        /// <param name="formatDefination"><see cref="FormatDefination"/> that contains the format definition</param>
        private static void ReadFormatDefination(XmlReader reader, FormatDefination formatDefination)
        {
            System.Diagnostics.Debug.Assert(reader != null);
            System.Diagnostics.Debug.Assert(formatDefination != null);
            System.Diagnostics.Debug.Assert(reader.NodeType == XmlNodeType.Element);

            formatDefination.Name = reader.Name;

            //
            // Reads the attributes of format defination element.
            //
            if (reader.MoveToFirstAttribute())
            {
                do
                {
                    if (reader.Name == DESCRIPTION)
                    {
                        formatDefination.Description = reader.Value;
                    }
                    else
                    {
                        throw new XmlSchemaException();
                    }
                }
                while (reader.MoveToNextAttribute());

                reader.MoveToElement();
            }

            //
            // Reads the pattern definations.
            //
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    break;
                }

                if (reader.NodeType != System.Xml.XmlNodeType.Element)
                {
                    continue;
                }

                if (formatDefination.PatternDefinations == null)
                {
                    formatDefination.PatternDefinations = new List<PatternDefination>();
                }

                ReadPatternDefinations(reader, formatDefination.PatternDefinations);
            }
        }

        /// <summary>
        /// Reads the informations from the reader, and uses it to intialize the formatSettting.
        /// </summary>
        /// <param name="reader"><b>XML</b> reader that contains the format setting information</param>
        /// <param name="patternDefinations">Pattern definitions</param>
        private static void ReadPatternDefinations(XmlReader reader, List<PatternDefination> patternDefinations)
        {
            System.Diagnostics.Debug.Assert(reader != null);
            System.Diagnostics.Debug.Assert(patternDefinations != null);
            System.Diagnostics.Debug.Assert(reader.NodeType == XmlNodeType.Element);

            //
            // Find the index of this pattern in the patternDefinations.
            //
            int index = -1;
            if (patternDefinations != null)
            {
                for (int i = 0; i < patternDefinations.Count; i++)
                {
                    if (patternDefinations[i].Name == reader.Name)
                    {
                        index = i;
                        break;
                    }
                }
            }

            //
            // This pattern has not been defined, adds append new space in the pattern arrays in formatSettings 
            // and initializes them.
            //
            if (index == -1)
            {
                index = patternDefinations.Count;
                patternDefinations.Add(new PatternDefination());
                patternDefinations[index].Name = reader.Name;
            }

            //
            // Reads the attributes of pattern defination element.
            //
            if (reader.MoveToFirstAttribute())
            {
                do
                {
                    if (reader.Name == DESCRIPTION)
                    {
                        patternDefinations[index].Description = reader.Value;
                    }
                    else
                    {
                        throw new XmlSchemaException();
                    }
                }
                while (reader.MoveToNextAttribute());

                reader.MoveToElement();
            }

            //
            // Reads the item groups.
            //
            if (patternDefinations[index].Groups == null)
            {
                patternDefinations[index].Groups = new List<PatternItemGroup>();
            }
            if (patternDefinations[index].Items == null)
            {
                patternDefinations[index].Items = new List<PatternItem>();
            }
            ReadPatternGroupDefination(reader, patternDefinations[index].Items, patternDefinations[index].Groups);
        }

        /// <summary>
        /// Reads the item group.
        /// </summary>
        /// <param name="reader"><see cref="XmlReader"/> provides the <I>group</I> XML element</param>
        /// <param name="items">Items</param>
        /// <param name="groups">Item groups</param>
        private static void ReadPatternGroupDefination(XmlReader reader, List<PatternItem> items, List<PatternItemGroup> groups)
        {
            System.Diagnostics.Debug.Assert(reader != null);
            System.Diagnostics.Debug.Assert(items != null);
            System.Diagnostics.Debug.Assert(groups != null);

            //
            // Reads the items.
            //
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    break;
                }
                if (reader.NodeType != System.Xml.XmlNodeType.Element)
                {
                    continue;
                }

                if (reader.Name == ITEM)
                {
                    items.Add(ReadItemDefination(reader));
                }
                else if (reader.Name == GROUP)
                {
                    PatternItemGroup group = new PatternItemGroup();

                    groups.Add(group);

                    //
                    // Reades the attributes of the group element.
                    //
                    if (reader.MoveToFirstAttribute())
                    {
                        do
                        {
                            if (reader.Name == DESCRIPTION)
                            {
                                group.Description = reader.Value;
                            }
                            else
                            {
                                throw new XmlSchemaException();
                            }
                        }
                        while (reader.MoveToNextAttribute());

                        reader.MoveToElement();
                    }

                    group.StartItemIndex = items.Count;
                    group.SubGroups = new List<PatternItemGroup>();
                    ReadPatternGroupDefination(reader, items, group.SubGroups);
                    group.EndItemIndex = items.Count - 1;
                }
                else
                {
                    throw new XmlSchemaException();
                }
            }
        }

        /// <summary>
        /// Uses a item element to create a <see cref="PatternItem"/> instance.
        /// </summary>
        /// <param name="reader">XML reader that is stop in a item element</param>
        /// <returns><see cref="PatternItem"/> result</returns>
        private static PatternItem ReadItemDefination(XmlReader reader)
        {
            System.Diagnostics.Debug.Assert(reader != null);

            PatternItem item = new PatternItem();
            item.Holder = DEFAULT_HOLDER;
            item.ReplaceAll = false;

            //
            // Reads the attributes of item defination element.
            //
            if (reader.MoveToFirstAttribute())
            {
                do
                {
                    if (reader.Name == DESCRIPTION)
                    {
                        item.Description = reader.Value;
                    }
                    else if (reader.Name == KEYWORDS)
                    {
                        item.Keywords = reader.Value;
                    }
                    else if (reader.Name == HOLDER)
                    {
                        item.Holder = reader.Value;
                    }
                    else if (reader.Name == REPLACEALL)
                    {
                        item.ReplaceAll = reader.ReadContentAsBoolean();
                    }
                    else
                    {
                        throw new XmlSchemaException();
                    }
                }
                while (reader.MoveToNextAttribute());

                reader.MoveToElement();
            }

            if (item.Keywords == null || item.Description == null)
            {
                throw new XmlSchemaException();
            }

            if (!reader.IsEmptyElement)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                    if (reader.NodeType != System.Xml.XmlNodeType.Element)
                    {
                        continue;
                    }
                    throw new XmlSchemaException();
                }
            }

            return item;
        }

        /// <summary>
        /// Reads the informations from the reader, and uses it to intialize the formatSettting.
        /// </summary>
        /// <param name="reader"><b>XML</b> reader, contains the format setting information</param>
        /// <param name="formatSample">Format sample</param>
        private static void ReadFormatSample(XmlReader reader, FormatSample formatSample)
        {
            System.Diagnostics.Debug.Assert(reader != null);
            System.Diagnostics.Debug.Assert(formatSample != null);
            System.Diagnostics.Debug.Assert(reader.NodeType == XmlNodeType.Element);

            formatSample.PatternNames = new List<string>();
            formatSample.PatternValues = new List<string>();

            //
            // Reads the attributes of format sample element.
            //
            if (reader.MoveToFirstAttribute())
            {
                do
                {
                    if (reader.Name == DESCRIPTION)
                    {
                        formatSample.Description = reader.Value;
                    }
                    else
                    {
                        throw new XmlSchemaException();
                    }
                }
                while (reader.MoveToNextAttribute());

                reader.MoveToElement();
            }

            // 
            // Reads the pattern sample elements.
            //
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    break;
                }
                if (reader.NodeType != System.Xml.XmlNodeType.Element)
                {
                    continue;
                }

                string patternName = reader.Name;

                if (!reader.IsEmptyElement)
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.EndElement)
                        {
                            break;
                        }
                        if (reader.NodeType == XmlNodeType.Text)
                        {
                            int index = formatSample.PatternNames.IndexOf(patternName);
                            if (index == -1)
                            {
                                formatSample.PatternNames.Add(patternName);
                                formatSample.PatternValues.Add(reader.Value);
                            }
                            else
                            {
                                formatSample.PatternValues[index] = reader.Value;
                            }
                        }
                        if (reader.NodeType != System.Xml.XmlNodeType.Element)
                        {
                            continue;
                        }
                        throw new XmlSchemaException();
                    }
                }
            }
        }

        /// <summary>
        /// A <typeparamref name="List"/> indicates the format samples.
        /// </summary>
        public List<FormatSample> Samples;
        /// <summary>
        /// A <typeparamref name="FormatDefination"/> indicates the format definition.
        /// </summary>
        public FormatDefination Defination;
    }

    /// <summary>
    /// Represents the format samples.
    /// </summary>
    internal class FormatSample
    {
        /// <summary>
        /// A <see cref="FormatSetting"/> indicates the owner of the sample.
        /// </summary>
        private FormatSetting _owner = null;
        /// <summary>
        /// Gets the A <see cref="FormatSetting"/> indicates the owner of the sample.
        /// </summary>
        /// <value>
        /// A <see cref="FormatSetting"/> indicates the owner of the sample.
        /// </value>
        public FormatSetting Owner
        {
            get { return this._owner; }
        }

        /// <summary>
        /// Indicates a new instance of the <see cref="FormatSetting"/>.
        /// </summary>
        /// <param name="owner">
        /// A <see cref="FormatSetting"/> indicates the owner of the sample
        /// </param>
        public FormatSample(FormatSetting owner)
        {
            this._owner = owner;
        }

        /// <summary>
        /// A <typeparamref name="List"/> indicates the pattern names.
        /// </summary>
        public List<string> PatternNames;
        /// <summary>
        /// A <typeparamref name="List"/> indicates the pattern values.
        /// </summary>
        public List<string> PatternValues;
        /// <summary>
        /// A <b>string</b> indicates the description.
        /// </summary>
        public string Description;
        /// <summary>
        /// Gets the text of the <see cref="FormatSetting"/>.
        /// </summary>
        /// <returns>
        /// A <b>string</b> indicate the text.
        /// </returns>
        public string GetText()
        {
            string listSpeparator = Application.CurrentCulture.TextInfo.ListSeparator;
            string result = string.Empty;

            if (this.PatternNames == null || this.PatternValues == null)
            {
                return result;
            }

            if (this.Owner == null || this.Owner.Defination == null || this.Owner.Defination.PatternDefinations == null)
            {
                for (int i = 0; i < this.PatternNames.Count && i < this.PatternValues.Count; i++)
                {
                    if (result != string.Empty)
                    {
                        result += listSpeparator;
                    }

                    result += this.PatternNames[i] + "=" + this.PatternValues[i];
                }
            }
            else
            {
                int speparatorCount = 0;

                for (int i = 0; i < this.Owner.Defination.PatternDefinations.Count; i++)
                {
                    int index = this.PatternNames.IndexOf(this.Owner.Defination.PatternDefinations[i].Name);

                    if (index != -1)
                    {
                        while (speparatorCount > 0)
                        {
                            result += listSpeparator;
                            speparatorCount--;
                        }
                        result += this.PatternValues[index];
                    }

                    speparatorCount++;
                }
            }

            return result;
        }

    }
    /// <summary>
    /// Represents the definition of a format.
    /// </summary>
    public class FormatDefination
    {
        /// <summary>
        /// A <typeparamref name="List"/> indicates the pattern definitions.
        /// </summary>
        public List<PatternDefination> PatternDefinations;
        /// <summary>
        /// A <b>string</b> indicates the format name.
        /// </summary>
        public string Name;
        /// <summary>
        /// A <b>string</b> indicates the descrption of the format.
        /// </summary>
        public string Description;
    }
    /// <summary>
    /// Represents the definition of a pattern.
    /// </summary>
    public class PatternDefination
    {
        /// <summary>
        /// A <see cref="string"/> that indicates the name of the pattern.
        /// </summary>
        public string Name;
        /// <summary>
        /// A <typeparamref name="List"/> that contains the pattern items.
        /// </summary>
        public List<PatternItem> Items;
        /// <summary>
        /// A <typeparamref name="List"/> that cotains the pattern groups.
        /// </summary>
        public List<PatternItemGroup> Groups;

        /// <summary>
        /// A <b>string</b> indicates the description.
        /// </summary>
        public string Description;
    }
    /// <summary>
    /// Represents a pattern item.
    /// </summary>
    public class PatternItem
    {
        /// <summary>
        /// A <b>string</b> indicates the keywords.
        /// </summary>
        public string Keywords;
        /// <summary>
        /// A <b>string</b> indicates the location that is replaced by the selection text or the text of the control.
        /// </summary>
        public string Holder;
        /// <summary>
        /// A <b>bool</b> indicates whether to use the text of the control to do the replacing.
        /// </summary>
        public bool ReplaceAll;

        /// <summary>
        /// A <b>string</b> indicates the description.
        /// </summary>
        public string Description;
    }
    /// <summary>
    /// Represents a group used to organize the context menu.
    /// </summary>
    public class PatternItemGroup
    {
        /// <summary>
        /// A <b>int</b> indicates the start index of the pattern items in the <see cref="PatternDefination"/> class.
        /// </summary>
        public int StartItemIndex;
        /// <summary>
        /// A <b>int</b> indicates the end index of the pattern items in the <see cref="PatternDefination"/> class.
        /// </summary>
        public int EndItemIndex;

        /// <summary>
        /// A <typeparamref name="List"/> indicates the sub groups.
        /// </summary>
        public List<PatternItemGroup> SubGroups;

        /// <summary>
        /// A <b>string</b> indicates the descrption of the group.
        /// </summary>
        public string Description;
    }
    #endregion

    #region The format editor helpers
    /// <summary>
    /// Defines an action handler used to handle the context menu item Click event.
    /// </summary>
    public class PatternMenuActionHandler
    {
        /// <summary>
        /// Intializes an instance of the <see cref="PatternMenuActionHandler"/> class.
        /// </summary>
        /// <param name="editor">
        /// A <see cref="Control"/> indicates the pattern editor
        /// </param>
        /// <param name="patternItem">
        /// A <see cref="PatternItem"/> indicates a pattern item
        /// </param>
        public PatternMenuActionHandler(Control editor, PatternItem patternItem)
        {
            System.Diagnostics.Debug.Assert(editor != null);
            System.Diagnostics.Debug.Assert(patternItem != null);

            this.Editor = editor;
            this.PatternItem = patternItem;
        }

        /// <summary>
        /// A <see cref="Control"/> used to indicate the editor.
        /// </summary>
        protected Control Editor;

        /// <summary>
        /// A <see cref="PatternItem"/> used to indicates sthe pattern item.
        /// </summary>
        protected PatternItem PatternItem;

        /// <summary>
        /// Handles the Click event.
        /// </summary>
        /// <param name="sender">
        /// A <b>object</b> indicates the event sender
        /// </param>
        /// <param name="e">
        /// A <see cref="EventArgs"/> indicates the event arguements
        /// </param>
        public void Handle(object sender, EventArgs e)
        {
            this.Do();
        }
        /// <summary>
        /// Does the action.
        /// </summary>
        protected virtual void Do()
        {
            if (this.Editor is TextBox)
            {
                TextBox textBox = this.Editor as TextBox;

                if (this.PatternItem.ReplaceAll)
                {
                    int caretOffset =
                        (this.PatternItem.Keywords.LastIndexOf(this.PatternItem.Holder) != -1) ?
                        (this.PatternItem.Keywords.Length -
                        this.PatternItem.Keywords.LastIndexOf(this.PatternItem.Holder) -
                        this.PatternItem.Holder.Length) : 0;

                    textBox.Text = this.PatternItem.Keywords.Replace(this.PatternItem.Holder, textBox.Text);
                    textBox.SelectionStart = textBox.Text.Length - caretOffset;
                    textBox.SelectionLength = 0;
                }
                else
                {
                    int caretOffset =
                        ((this.PatternItem.Keywords.LastIndexOf(this.PatternItem.Holder) != -1) ?
                        (this.PatternItem.Keywords.Length -
                        this.PatternItem.Keywords.LastIndexOf(this.PatternItem.Holder) -
                        this.PatternItem.Holder.Length) : 0) +
                        textBox.Text.Length -
                        textBox.SelectionStart -
                        textBox.SelectionLength;

                    textBox.SelectedText = this.PatternItem.Keywords.Replace(this.PatternItem.Holder, textBox.SelectedText);
                    textBox.SelectionStart = textBox.Text.Length - caretOffset;
                    textBox.SelectionLength = 0;
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }
    /// <summary>
    /// Represents a helper class to initialize the format editors by the specified format setting.
    /// </summary>
    internal class FormatEditorHelper
    {
        /// <summary>
        /// Initializes the menu with the specified pattern definition.
        /// </summary>
        /// <param name="contextMenuStrip">
        /// A <see cref="ContextMenuStrip"/> indicates the context menu need to be initialized
        /// </param>
        /// <param name="patternDefination">
        /// A <see cref="PatternDefination"/> contains the items and groups. This method will use those informations 
        /// to initialize the menu
        /// </param>
        /// <param name="edit">
        /// A <see cref="TextBox"/>, it is the editor of the specified pattern. User can input the new pattern in it
        /// </param>
        public static void Initialize(ContextMenuStrip contextMenuStrip, PatternDefination patternDefination, TextBox edit)
        {
            List<int> addedGroupIndices = new List<int>();

            //
            // Add the groups into the context menu.
            //
            for (int i = 0; i < patternDefination.Items.Count; i++)
            {
                if (patternDefination.Groups != null)
                {
                    for (int j = 0; j < patternDefination.Groups.Count; j++)
                    {
                        if (i >= patternDefination.Groups[j].StartItemIndex && i <= patternDefination.Groups[j].EndItemIndex ||
                            i >= patternDefination.Groups[j].EndItemIndex && i <= patternDefination.Groups[j].StartItemIndex)
                        {
                            if (!addedGroupIndices.Contains(j))
                            {
                                addedGroupIndices.Add(j);

                                if (contextMenuStrip.Items.Count > 0)
                                {
                                    contextMenuStrip.Items.Add(new ToolStripSeparator());
                                }

                                InternalInitialize(contextMenuStrip.Items, patternDefination.Items, patternDefination.Groups[j], edit, false);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Helps the <b>Initialize</b> methods to initialize the menu.
        /// </summary>
        /// <param name="menuItems"><see cref="ToolStripItemCollection"/> collection save the new menu items</param>
        /// <param name="patternItems">List PatternItem object that contains the pattern items</param>
        /// <param name="patternGroup"><see cref="PatternItemGroup"/> indicates the current pattern group</param>
        /// <param name="edit"><see cref="TextBox"/>, it is the editor of the specified pattern. User can input the new pattern in it</param>
        /// <param name="halfLayer">Whether to use some separator lines between different subgoups, rather than the submenu</param>
        private static void InternalInitialize(
            ToolStripItemCollection menuItems,
            List<PatternItem> patternItems,
            PatternItemGroup patternGroup,
            TextBox edit,
            bool halfLayer)
        {
            System.Diagnostics.Debug.Assert(menuItems != null);
            System.Diagnostics.Debug.Assert(patternItems != null);
            System.Diagnostics.Debug.Assert(patternGroup != null);

            List<int> addedGroupIndices = new List<int>();

            //
            // Gets the group range in the pattern item collection.
            //
            int startItemIndex = patternGroup.StartItemIndex < patternGroup.EndItemIndex ?
                patternGroup.StartItemIndex : patternGroup.EndItemIndex;
            int endItemIndex = patternGroup.StartItemIndex > patternGroup.EndItemIndex ?
                patternGroup.StartItemIndex : patternGroup.EndItemIndex;
            if (startItemIndex < 0)
            {
                startItemIndex = 0;
            }
            if (endItemIndex > patternItems.Count)
            {
                endItemIndex = patternItems.Count - 1;
            }

            //
            // Add the items and sub groups into the menu collection.
            //
            for (int i = startItemIndex; i <= endItemIndex; i++)
            {
                bool isInSubGroup = false;
                //
                // Searchs the sub groups, if a sub group contains the item, then adds the sub group to the collection.
                //
                for (int j = 0; j < patternGroup.SubGroups.Count; j++)
                {
                    if (i >= patternGroup.SubGroups[j].StartItemIndex && i <= patternGroup.SubGroups[j].EndItemIndex ||
                        i >= patternGroup.SubGroups[j].EndItemIndex && i <= patternGroup.SubGroups[j].StartItemIndex)
                    {
                        isInSubGroup = true;

                        if (!addedGroupIndices.Contains(j))
                        {
                            addedGroupIndices.Add(j);

                            if (halfLayer)
                            {
                                if (menuItems.Count > 0)
                                {
                                    menuItems.Add(new ToolStripSeparator());
                                }
                                InternalInitialize(menuItems, patternItems, patternGroup.SubGroups[j], edit, !halfLayer);
                            }
                            else
                            {
                                ToolStripMenuItem item = new ToolStripMenuItem();
                                item.Text = patternGroup.SubGroups[j].Description;
                                menuItems.Add(item);
                                InternalInitialize(item.DropDownItems, patternItems, patternGroup.SubGroups[j], edit, !halfLayer);
                            }
                        }
                    }
                }

                //
                // If the item is not in any sub group, adds the item in colletion.
                //
                if (!isInSubGroup)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem();
                    item.Text = patternItems[i].Description;
                    PatternMenuActionHandler handler = new PatternMenuActionHandler(edit, patternItems[i]);
                    item.Click += new EventHandler(handler.Handle);
                    menuItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Fills the samples in a list control.
        /// </summary>
        /// <param name="list"><see cref="List"/> that displays the samples</param>
        /// <param name="formatSamples">List FormatSample that contains the samples</param>
        public static void Initialize(DataGridViewList list, List<FormatSample> formatSamples)
        {
            if (formatSamples == null || list == null)
            {
                return;
            }

            //for (int i = 0; i < formatSamples.Count; i++)
            //{
            //    list.Rows.Add(new ListItem(null, formatSamples[i].GetText(), formatSamples[i].Description));
            //}
            foreach (FormatSample sample in formatSamples)
            {
                list.Rows.Add(sample.GetText(), sample.Description);
            }

            list.AutoSizeColumns();
        }

        /// <summary>
        /// Fills the samples in a list control.
        /// </summary>
        /// <param name="list"><see cref="List"/> that displays the samples</param>
        /// <param name="formatSamples">List FormatSample that contains the samples</param>
        public static void Initialize2(DataGridViewList list, List<FormatSample> formatSamples)
        {
            if (formatSamples == null || list == null)
            {
                return;
            }

            foreach (FormatSample sample in formatSamples)
            {
                list.Rows.Add(false, sample.GetText(), sample.Description);
            }

            if (list is DataGridViewListPlus)
            {
                (list as DataGridViewListPlus).AutoSizeColumns();
            }
            else
            {
                list.AutoSizeColumns();
            }
        }


        /// <summary>
        /// Gets a string form resouce by the name and specified perfixs.
        /// </summary>
        /// <param name="name"><see cref="string"/> that contains the resouce name</param>
        /// <param name="perfixs">List of perfixs, methods use combination of the perfixs and name to get the string </param>
        public static string GetStringFromResource(string name, params string[] perfixs)
        {
            if (perfixs == null || perfixs.Length == 0)
            {
                return C1Localizer.GetString(name);
            }

            string result = null;

            for (int i = 0; i < perfixs.Length; i++)
            {
                result = C1Localizer.GetString(perfixs[i] + name);
                if (result != null)
                {
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Indicates an icon that indicates a helper.
        /// </summary>
        public static readonly System.Drawing.Image HelperImage;

        /// <summary>
        /// Indicates the image displayed in the format drop-down button.
        /// </summary>
        public static readonly System.Drawing.Image FormatDropDownButtonImage;

        /// <summary>
        /// Initalizes the <see cref="FormatEditorHelper"/> class.
        /// </summary>
        static FormatEditorHelper()
        {
            System.IO.Stream stream = typeof(FormatEditorHelper).Assembly.GetManifestResourceStream(
                @"GrapeCity.Web.Input.Design.Helper3.bmp");
            if (stream != null)
            {
                HelperImage = new System.Drawing.Bitmap(stream);
                (HelperImage as System.Drawing.Bitmap).MakeTransparent(System.Drawing.Color.FromArgb(255, 0, 255));
                stream.Close();
            }
            else
            {
                HelperImage = null;
            }

            stream = typeof(FormatEditorHelper).Assembly.GetManifestResourceStream(
                @"GrapeCity.Web.Input.Design.FormatDropDownButton.ico");
            if (stream != null)
            {
                FormatDropDownButtonImage = new System.Drawing.Bitmap(stream);
                stream.Close();
            }
            else
            {
                FormatDropDownButtonImage = null;
            }
        }
    }
    #endregion
}
