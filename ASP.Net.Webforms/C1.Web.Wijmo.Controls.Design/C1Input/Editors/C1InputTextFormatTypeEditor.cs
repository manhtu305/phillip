﻿using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{         
    class C1InputTextFormatTypeEditor : UITypeEditor
    {
        public override object EditValue(ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            if (context == null ||
                context.PropertyDescriptor == null ||
                context.PropertyDescriptor.PropertyType != typeof(string))
            {
                return value;
            }

            IWindowsFormsEditorService service =
                provider.GetService(typeof(IWindowsFormsEditorService)) as
                IWindowsFormsEditorService;

            SetFormatDialog<C1InputTextFormatEditor> formatDialog =
                new SetFormatDialog<C1InputTextFormatEditor>();
            if (Screen.PrimaryScreen.Bounds.Height <= 768)
            {
                formatDialog.Height -= 50;
            }
            formatDialog.FormatEditor.Format = value as string;

            if (service != null)
            {
                if (service.ShowDialog(formatDialog) != DialogResult.OK)
                {
                    return value;
                }
            }
            else
            {
                if (formatDialog.ShowDialog() != DialogResult.OK)
                {
                    return value;
                }
            }

            return formatDialog.FormatEditor.Format;
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null &&
                context.PropertyDescriptor != null &&
                context.PropertyDescriptor.PropertyType == typeof(string))
            {
                return UITypeEditorEditStyle.Modal;
            }
            return UITypeEditorEditStyle.None;
        }
    }
}
