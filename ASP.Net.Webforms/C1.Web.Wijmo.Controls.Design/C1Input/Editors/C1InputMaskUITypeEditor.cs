using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using C1.Web.Wijmo.Controls.C1Input;


namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    /// <summary>
    /// 
    /// </summary>
    internal class C1InputMaskUITypeEditor : System.Drawing.Design.UITypeEditor
    {
        /// <summary>
        /// 
        /// </summary>
        public C1InputMaskUITypeEditor()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            // use modal dialog
            return (context != null && context.Instance != null)
                ? UITypeEditorEditStyle.Modal
                : base.GetEditStyle(context);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null || context.Instance == null || provider == null)
                return value;

            // get service
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc == null)
                return value;
            // initialize editor

            FormatMode mode = FormatMode.Simple;

            C1InputMask mask = context.Instance as C1InputMask;
            var actionList = context.Instance as C1Input.C1InputDesignerActionList;
            if (actionList != null)
            {
                mode = actionList.FormatMode;
            }
            else
            {
                if (mask != null)
                {
                    mode = mask.FormatMode;
                }
            }

            if (mode == FormatMode.Advanced)
            {
                SetFormatDialog<C1AdvancedMaskFormatEditor> frm = new SetFormatDialog<C1AdvancedMaskFormatEditor>();
                frm.FormatEditor.Format = (string) value;
                DialogResult dr = edSvc.ShowDialog(frm);
                if (dr == DialogResult.OK)
                {
                    value = frm.FormatEditor.Format;
                    context.OnComponentChanged();
                }
            }
            else
            {


                C1InputMaskPickerForm frm = new C1InputMaskPickerForm();

                // **** remove the comment from the next line if you want to hide the "Update with Literals" prompt
                //frm.UpdateWithLiteralsPromptVisible = false;

                // set the mask
                frm.EditMask = (string)value;
                if (actionList != null && actionList.Culture != null)
                    frm.Culture = actionList.Culture;

                // show the editor
                DialogResult dr = edSvc.ShowDialog(frm);

                // dirty it and get the new value if they said picked a selection
                if (dr == DialogResult.OK)
                {
                    value = frm.EditMask;
                    context.OnComponentChanged();
                }
            }

            // done			
            return value;
        }
    }
}