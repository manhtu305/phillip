using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;



namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{
    internal partial class C1InputMaskPickerForm : Form
    {
        public C1InputMaskPickerForm()
        {
            InitializeComponent(); 
        }

        /// <summary>
        /// Gets or sets the mask
        /// </summary>
        public string EditMask
        {
            get { return this.c1InputMaskPicker1.Mask; }
            set { this.c1InputMaskPicker1.Mask = value; }
        }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        public CultureInfo Culture
        {
            get { return c1InputMaskPicker1.Culture; }
            set { c1InputMaskPicker1.Culture = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the underlying datasource should be updated with the literals in the mask.
        /// </summary>
        /*
        public bool UpdateWithLiterals
        {
            get { return this.c1InputMaskPicker1.UpdateWithLiterals; }
            set { this.c1InputMaskPicker1.UpdateWithLiterals = value; }
        }
        public bool UpdateWithLiteralsPromptVisible
        {
            get { return this.c1InputMaskPicker1.UpdateWithLiteralsPromptVisible; }
            set { this.c1InputMaskPicker1.UpdateWithLiteralsPromptVisible = value; }
        }*/

        private void c1InputMaskPicker1_Load(object sender, EventArgs e)
        {

        }

        private void C1InputMaskPickerForm_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}