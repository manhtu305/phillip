using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;
using System.Reflection;


namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    using C1.Web.Wijmo.Controls.C1Input;
    using C1.Web.Wijmo.Controls.Design.UITypeEditors;
    using C1.Web.Wijmo.Controls.Design.Localization;


    internal partial class C1InputDesignerForm : Form, IC1DesignEditor
    {
        private ControlDesigner _controlDesigner;

        #region --- constructor ---

        public C1InputDesignerForm(object InspectedComponent, ControlDesigner designer)
        {
            //this._controlDesigner = designer;
            this.setInspectedComponent(InspectedComponent, designer);
            
            InitializeComponent();
            InitializeEditors();
        }

        private void InitializeEditors()
        {
            string sDesignerCaption = this._InspectedComponent.GetType().Name.ToString();
            this.Text = sDesignerCaption + " " + C1Localizer.GetString("C1Input.DesignerForm.Title");
            this.tabControlEditors.Controls.Clear();
            // C1InputMaskPicker tab
            if(this._InspectedComponent.GetType().Equals(typeof(C1InputMask))) {
                C1InputMaskPicker aC1InputMaskPicker = new C1InputMaskPicker();
                appendEditorTabPage(C1Localizer.GetString("C1Input.DesignerForm.InputMaskEditor"), aC1InputMaskPicker);
            }
            if(this._InspectedComponent is C1InputDate) {
                C1DateFormatEditor aC1DateFormatPresetsEditor = new C1DateFormatEditor();
                appendEditorTabPage(C1Localizer.GetString("C1Input.DesignerForm.DateFormatPresets"), aC1DateFormatPresetsEditor);
            }
            
            // C1AutoFormatEditor tab
            
            //C1AutoFormatEditor aC1AutoFormatEditor = new C1AutoFormatEditor();
            //appendEditorTabPage("Auto Format", aC1AutoFormatEditor);

            // C1PropertiesEditor tab
            //C1PropertiesEditor aC1PropertiesEditor = new C1PropertiesEditor();
            //appendEditorTabPage("Properties", aC1PropertiesEditor);

            this.tabControlEditors.SelectedIndexChanged += new EventHandler(tabControlEditors_SelectedIndexChanged);
            this.tabControlEditors.Selecting += new TabControlCancelEventHandler(tabControlEditors_Selecting);

            this.doCurrentTabSelected();
        }


        #endregion

        #region --- private fields ---
        private object _InspectedComponent;
        #endregion

        #region --- private methods ---
        private void appendEditorTabPage(string aTabName, System.Windows.Forms.Control aEditor)
        {
            ((IC1DesignEditor)aEditor).setInspectedComponent(this._InspectedComponent, this._controlDesigner);
            TabPage aNewTabPage = new System.Windows.Forms.TabPage(aTabName);
            aEditor.Dock = DockStyle.Fill;
            aNewTabPage.Controls.Add(aEditor);
            this.tabControlEditors.Controls.Add(aNewTabPage);
        }
        #endregion

        #region --- Events Hanslers ---
        private TabPage _prevSelectedTab = null;
        void tabControlEditors_Selecting(object sender, TabControlCancelEventArgs e)
        {
            this._prevSelectedTab = this.tabControlEditors.SelectedTab;
        }
        void tabControlEditors_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._prevSelectedTab != null && this._prevSelectedTab.Equals(this.tabControlEditors.SelectedTab))
            {
                IEnumerator aIEnumerator = this._prevSelectedTab.Controls.GetEnumerator();
                while (aIEnumerator.MoveNext())
                {
                    object aObj = aIEnumerator.Current;
                    ((IC1DesignEditor)aObj).doOtherTabSelected();
                }
            }
            this.doCurrentTabSelected();
        }
        #endregion

        #region --- Main Menu actions ---

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.doActionOk();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.doActionCancel();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            this.doActionApply();
        }

        #endregion


        #region IC1DesignEditor Members

        public void setInspectedComponent(object aComponent, System.Web.UI.Design.ControlDesigner designer)
        {
            this._InspectedComponent = aComponent;
            this._controlDesigner = designer;
        }

        public void doActionApply()
        {
            IEnumerator aIEnumerator = this.tabControlEditors.SelectedTab.Controls.GetEnumerator();
            while(aIEnumerator.MoveNext()) {
                object aObj = aIEnumerator.Current;
                ((IC1DesignEditor)aObj).doActionApply();
            }

            //C1WebInputRuntimeAutoFormat.UpdateDesigner(this._InspectedComponent);
        }

        public void doActionOk()
        {
            this.doActionApply();
            this.doActionExit();
        }

        public void doActionCancel()
        {
            IEnumerator aIEnumerator = this.tabControlEditors.SelectedTab.Controls.GetEnumerator();
            while (aIEnumerator.MoveNext())
            {
                object aObj = aIEnumerator.Current;
                ((IC1DesignEditor)aObj).doActionCancel();
            }
            this.doActionExit();
        }

        public void doActionExit()
        {
            IEnumerator aIEnumerator = this.tabControlEditors.SelectedTab.Controls.GetEnumerator();
            while (aIEnumerator.MoveNext())
            {
                object aObj = aIEnumerator.Current;
                ((IC1DesignEditor)aObj).doActionExit();
            }
            this.Close();
            ISite site1 = (this._InspectedComponent is IComponent)
                ? ((IComponent)this._InspectedComponent).Site : null;
            if (site1 == null)
                return;
            object aIDesignerHost = site1.GetService(typeof(IDesignerHost));
            if (aIDesignerHost is IDesignerHost)
                aIDesignerHost = ((IDesignerHost)aIDesignerHost).GetDesigner((IComponent)this._InspectedComponent);
            if (!(aIDesignerHost is ControlDesigner))
                return;
            ((ControlDesigner)aIDesignerHost).UpdateDesignTimeHtml();
        }
        
        private TabPage prevTab = null;

        public void doCurrentTabSelected()
        {
            this.doOtherTabSelected();
            this.prevTab = this.tabControlEditors.SelectedTab;
            IEnumerator aIEnumerator2 = this.tabControlEditors.SelectedTab.Controls.GetEnumerator();
            while (aIEnumerator2.MoveNext())
            {
                object aObj = aIEnumerator2.Current;
                ((IC1DesignEditor)aObj).doCurrentTabSelected();
            }
        }

        public void doOtherTabSelected()
        {
            if (this.prevTab != null && !this.prevTab.Equals(this.tabControlEditors.SelectedTab))
            {
                IEnumerator aIEnumerator2 = this.prevTab.Controls.GetEnumerator();
                while (aIEnumerator2.MoveNext())
                {
                    object aObj = aIEnumerator2.Current;
                    ((IC1DesignEditor)aObj).doCurrentTabSelected();
                }
            }
        }

        #endregion


    }
}