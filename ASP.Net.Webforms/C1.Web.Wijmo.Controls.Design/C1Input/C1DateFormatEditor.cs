using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Drawing.Design;
using C1.Web.Wijmo.Controls.Design.UITypeEditors;

namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    using System.Collections;
    using C1.Web.Wijmo.Controls.C1Input;
    using C1.Web.Wijmo.Controls.Design.Localization;

    internal partial class C1DateFormatEditor : UserControl, IC1DesignEditor, IFormatEditor
    {
        private Color textBoxDateFormat_PrevBackColor;
        private string defaultFormat = String.Empty;
        public string dateFormatString = String.Empty;

        public C1DateFormatEditor()
        {
            InitializeComponent();
            dateFormatString = C1Localizer.GetString("C1Input.DateInput.DateFormatSetting");

            this.InitformatContextMenu();
            this.formatButton.Click += formatButton_Click;
            this.listView1.ItemSelectionChanged += new ListViewItemSelectionChangedEventHandler(listView1_ItemSelectionChanged);
            this.textBoxDateFormat.TextChanged += new EventHandler(textBoxDateFormat_TextChanged);
            this.formatContextMenuStrip.Closed += formatContextMenuStrip_Closed;
            this.textBoxDateFormat_PrevBackColor = this.textBoxDateFormat.BackColor;
        }

        #region --- Events handlers --- 
       
        void formatContextMenuStrip_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            this.textBoxDateFormat.Focus();
        }

        protected virtual void InitformatContextMenu()
        {
            FormatSetting dateFormatSetting = FormatSetting.Parse(dateFormatString)[0];
            for (int i = 0; i < dateFormatSetting.Defination.PatternDefinations[0].Items.Count; i++)
            {
                string description = dateFormatSetting.Defination.PatternDefinations[0].Items[i].Description;
                string name = dateFormatSetting.Defination.PatternDefinations[0].Items[i].Keywords;
                ToolStripMenuItem item = new ToolStripMenuItem(description, null, MenuItem_Click, name);
                item.AutoSize = true;
                this.formatContextMenuStrip.Items.Add(item);
            }
        }

        void MenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem toolStripMenuItem = (ToolStripMenuItem)sender;
            string format = toolStripMenuItem.Name;
            this.textBoxDateFormat.Text += format;
            this.DateFormat = textBoxDateFormat.Text;
        }

        void formatButton_Click(object sender, EventArgs e)
        {
            this.formatContextMenuStrip.Show(this.formatButton, new Point(this.formatButton.Width, 0));
        }

        void textBoxDateFormat_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxDateFormat.Modified)
            {
                try
                {
                    this.DateFormat = textBoxDateFormat.Text;
                }
                catch (Exception) { }
            }
        }

        internal string DefaultFormat
        {
            get { return this.defaultFormat; }
            set
            {
                this.defaultFormat = value;
                this.SyncListView(this.DateFormat);
            }
        }

        void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected && e.Item != _custom)
            {
                try
                {
                    textBoxDateFormat.BackColor = textBoxDateFormat_PrevBackColor;
                    this.textBoxResultDatePattern.Text = this.dateInput.ParseDateFormatIntoDatePattern((string)e.Item.Tag);
                    this.textBoxDateFormat.Text = (string)e.Item.Tag;
                    this.maskedTextBox1.Text = this.dateInput.GetDate().ToString(this.textBoxDateFormat.Text, this.dateInput.Culture);
                    this.textBoxDateFormat.Modified = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        #endregion

        #region --- properties ---
        
        [DefaultValue("")]
        [Bindable(BindableSupport.Yes)]
        public string DateFormat
        {
            get { return this.textBoxDateFormat.Text; }
            set
            {
                try
                {
                    SyncListView(value);
                }
                catch (Exception)
                {
                    textBoxDateFormat.BackColor = Color.Pink;
                    return;
                }
                textBoxDateFormat.BackColor = textBoxDateFormat_PrevBackColor;
            }
        }

        #endregion

        #region --- private fields ---

        private ListViewItem _custom = null;
        private C1InputDate dateInput;

        #endregion

        #region IC1DesignEditor Members

        void IC1DesignEditor.setInspectedComponent(object aComponent, System.Web.UI.Design.ControlDesigner controlDesigner)
        {
            this.dateInput = aComponent as C1InputDate;

            AddListViewRow("ShortDatePattern", "d");
            AddListViewRow("LongDatePattern", "D");
            AddListViewRow("ShortTimePattern", "t");
            AddListViewRow("LongTimePattern", "T");
            AddListViewRow("FullDateShortTimePattern", "f");
            AddListViewRow("FullDateLongTimePattern", "F");
            AddListViewRow("GeneralDateShortTimePattern", "g");
            AddListViewRow("GeneralDateLongTimePattern", "G");
            AddListViewRow("UniversalSortableDateTimePattern", "U");
            _custom = AddListViewRow("CustomDateTimePattern", "");

            DateFormat = this.dateInput.DateFormat;
        }

        void IC1DesignEditor.doActionApply()
        {
            TypeDescriptor.GetProperties(this.dateInput)["DateFormat"].SetValue(this.dateInput, this.textBoxDateFormat.Text);
        }

        void IC1DesignEditor.doActionCancel()
        {
            
        }

        void IC1DesignEditor.doActionExit()
        {
            
        }

        void IC1DesignEditor.doCurrentTabSelected()
        {
            
        }

        void IC1DesignEditor.doOtherTabSelected()
        {
            
        }

        #endregion

        #region --- private methods ---
                    
        private ListViewItem AddListViewRow(string descKeySuffix, string format)
        {
            var culture = this.dateInput.Culture;
            string desc = C1Localizer.GetString("C1Input.C1DateFormatEditor." + descKeySuffix, culture);
            var date = this.dateInput.GetDate();
            var sample = string.IsNullOrEmpty(format) ? string.Empty : date.ToString(format, culture);
            var item = this.listView1.Items.Add(format, desc, 0);
            item.SubItems.Add(sample);
            item.Tag = format;
            return item;
        }

        private void SyncListView(string format)
        {
            var item = listView1.Items.Cast<ListViewItem>().FirstOrDefault(lvi => format == lvi.Tag as string) ?? _custom;

            // folowing can throw exception if date format is invalid

            maskedTextBox1.Text = this.getMaskedText(dateInput.GetDate(), format, dateInput.Culture);
            textBoxDateFormat.Text = format;
            textBoxResultDatePattern.Text = dateInput.ParseDateFormatIntoDatePattern(format);
            if (format == "" && this.DefaultFormat != "")
            {
                textBoxResultDatePattern.Text = this.DefaultFormat;
                maskedTextBox1.Text = this.getMaskedText(dateInput.GetDate(), this.DefaultFormat, dateInput.Culture);
            }
            textBoxDateFormat.Modified = false;
            if (item != null)
                item.Selected = true;
        }

        private string getMaskedText(DateTime date, string format, CultureInfo culture)
        {
            if (format == "g")
            {
                return date.ToString(format, culture);
            }

            ArrayList arrayList = this.GetForamtPatterns(format);
            string text = String.Empty;
            for (int i = 0; i < arrayList.Count; i++)
            {
                string value = arrayList[i].ToString();
                if (value.IndexOf('g') != -1)
                {
                    if (value == "g")
                    {
                        text += DateTimeInfo.GetSymbolEraNames(date);
                    }
                    else if (value == "gg")
                    {
                        text += DateTimeInfo.GetAbbreviatedEraNames(date);
                    }
                    else
                    {
                        text += DateTimeInfo.GetEraName(date);
                    }
                }
                else if (value.IndexOf('e') != -1)
                {
                    int year = DateTimeInfo.GetEraYear(date);
                    if (value == "e")
                    {
                        text += year.ToString();
                    }
                    else 
                    {
                        if (year < 10)
                        {
                            text += "0" + year.ToString();
                        }
                        else
                        {
                            text += year.ToString();
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(value.Trim()))
                    {
                        text += date.ToString(value, culture);
                    }
                    else
                    {
                        text += value;
                    }
                }
            }

            return text;
        }

        private ArrayList GetForamtPatterns(string format)
        {
            ArrayList arrayList = new ArrayList();

            string subString = String.Empty;
            string gString = String.Empty;
            string eString = String.Empty;

            for (int i = 0; i < format.Length; i++)
            {
                string c = format.Substring(i, 1);

                if (c == "g")
                {
                    if (subString != "")
                    {
                        arrayList.Add(subString);
                        subString = String.Empty;
                    }

                    if (eString != "")
                    {
                        arrayList.Add(eString);
                        eString = String.Empty;
                    }

                    gString = gString + c;
                }
                else if (c == "e")
                {
                    if (subString != "")
                    {
                        arrayList.Add(subString);
                        subString = String.Empty;
                    }

                    if (gString != "")
                    {
                        arrayList.Add(eString);
                        gString = String.Empty;
                    }

                    eString = eString + c;
                }
                else
                {
                    if (gString != "")
                    {
                        arrayList.Add(gString);
                        gString = String.Empty;
                    }

                    if (eString != "")
                    {
                        arrayList.Add(eString);
                        eString = String.Empty;
                    }

                    subString = subString + c;
                }
            }

            if (subString != "")
            {
                arrayList.Add(subString);
            }
            if (gString != "")
            {
                arrayList.Add(gString);
            }
            if (eString != "")
            {
                arrayList.Add(eString);
            }

            return arrayList;
        }

        #endregion

        public void PostFormLoad()
        {
        }
    }

    internal class C1DisplayFormatEditor : C1DateFormatEditor
    {
        public C1DisplayFormatEditor() : base()
        {
            this.label1.Text = C1Localizer.GetString("C1Input.C1DateFormatEditor.DisplayFormat");
            this.label3.Text = C1Localizer.GetString("C1Input.C1DateFormatEditor.ResultDisplayFormatPattern");
            this.columnHeader1.Text = C1Localizer.GetString("C1Input.C1DateFormatEditor.DisplayFormatDescription");
            this.columnHeader2.Text = C1Localizer.GetString("C1Input.C1DateFormatEditor.DisplayFormat");
        }

        protected override void InitformatContextMenu()
        {
            dateFormatString = C1Localizer.GetString("C1Input.DateInput.DateDisplayFormatSetting");
            base.InitformatContextMenu();
        }
    }

}
