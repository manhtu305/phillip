using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.Design;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using System.Globalization;
using C1.Web.Wijmo.Controls.Design.UITypeEditors;
using ControlDesigner = System.Web.UI.Design.ControlDesigner;


namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    using C1.Web.Wijmo.Controls.C1Input;
    using C1.Web.Wijmo.Controls.Design.Localization;


    [SupportsPreviewControl(true)]
    public class C1InputDesigner : C1ControlDesinger
    {
        internal string needSetDefaultDateID = String.Empty;

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            C1InputBase input = (C1InputBase)component;
            if (input.Page == null && RootDesigner != null)
            {
                var page = RootDesigner.Component as Page;
                if (page != null)
                {
                    input.Page = page;
                }
            }

            IComponentChangeService changeService = this.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            if (changeService != null)
            {
                changeService.ComponentAdded += changeService_ComponentAdded;
            }
        }

        void changeService_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is C1InputDate)
            {
                needSetDefaultDateID = (e.Component as C1InputDate).ID;
            }
        }

        public override string GetDesignTimeHtml()
        {
            if (((C1InputBase)Component).WijmoControlMode == WijmoControlMode.Mobile)
            {
                //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
            }

            return base.GetDesignTimeHtml();
        }

        protected override bool UsePreviewControl
        {
            get { return true; }
        }

        public override bool AllowResize
        {
            get
            {
                return true;
            }
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();
                lists.AddRange(base.ActionLists);
                lists.Add(new C1InputDesignerActionList(this));
                return lists;
            }
        }
    }

    /// <summary>
    /// Designer smart tags for Masked Edit control
    /// </summary>
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal class C1InputDesignerActionList : DesignerActionListBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="Component"></param>
        /// <param name="designer"></param>
        public C1InputDesignerActionList(System.Web.UI.Design.ControlDesigner designer)
            : base(designer)
        {
        }

        private void ShowDesignForm()
        {
            C1InputDesignerForm df = new C1InputDesignerForm(Component, this.Designer);
            df.ShowDialog();
        }

        #region Proxy Properties

        public C1InputMask Mask
        {
            get
            {
                return this.Component as C1InputMask;
            }
        }

        public CultureInfo Culture
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["Culture"].SetValue(Component, value);
            }
            get
            {
                return (CultureInfo)TypeDescriptor.GetProperties(Component)["Culture"].GetValue(Component);
            }
        }

        public FormatMode FormatMode
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["FormatMode"].SetValue(Component, value);
            }
            get
            {
                object value = TypeDescriptor.GetProperties(Component)["FormatMode"].GetValue(Component);
                if (value != null)
                    return (FormatMode)value;
                else
                    return FormatMode.Simple;
            }
        }

        //[Editor(typeof(C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextUITypeEditor), typeof(UITypeEditor))]
        public string Text
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["Text"].SetValue(Component, value);
            }
            get
            {
                return (string)TypeDescriptor.GetProperties(Component)["Text"].GetValue(Component);
            }
        }

        [Editor(typeof(C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputMaskUITypeEditor), typeof(UITypeEditor))]
        public string MaskFormat
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["MaskFormat"].SetValue(Component, value);
            }
            get
            {
                return (string)TypeDescriptor.GetProperties(Component)["MaskFormat"].GetValue(Component);
            }
        }


        [Editor(typeof(C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextFormatTypeEditor), typeof(UITypeEditor))]
        public string Format
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["Format"].SetValue(Component, value);
            }
            get
            {
                return (string)TypeDescriptor.GetProperties(Component)["Format"].GetValue(Component);
            }
        }

        public string PasswordChar
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["PasswordChar"].SetValue(Component, value);
            }
            get
            {
                return (string)TypeDescriptor.GetProperties(Component)["PasswordChar"].GetValue(Component);
            }
        }

        public char PromptChar
        {
            set
            {
                TypeDescriptor.GetProperties(Component)["PromptChar"].SetValue(Component, value);
            }
            get
            {
                return (char)TypeDescriptor.GetProperties(Component)["PromptChar"].GetValue(Component);
            }
        }

        [Editor(typeof (SmartTagDateTimeEditor), typeof(UITypeEditor))]
        public DateTime? Date
        {
            get
            {
                DateTime? dt = (DateTime?)TypeDescriptor.GetProperties(Component)["Date"].GetValue(Component);
                if (dt == null)
                    dt = DateTime.Now;

                return dt;
            }
            set
            {
                try
                {
                    TypeDescriptor.GetProperties(Component)["Date"].SetValue(Component, value);
                }
                catch (Exception)
                {

                }

            }
        }

        [Editor(typeof(C1InputDateFormatUITypeEditor), typeof(UITypeEditor))]
        public string DateFormat
        {
            get
            {
                return (string)TypeDescriptor.GetProperties(Component)["DateFormat"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["DateFormat"].SetValue(Component, value);
            }
        }

        [Editor(typeof(C1InputDisplayFormatUITypeEditor), typeof(UITypeEditor))]
        public string DisplayFormat
        {
            get
            {
                return (string)TypeDescriptor.GetProperties(Component)["DisplayFormat"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["DisplayFormat"].SetValue(Component, value);
            }
        }

        public double Value
        {
            get
            {
                return (double)TypeDescriptor.GetProperties(Component)["Value"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["Value"].SetValue(Component, value);
            }
        }

        public double MinValue
        {
            get
            {
                return (double)TypeDescriptor.GetProperties(Component)["MinValue"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["MinValue"].SetValue(Component, value);
            }
        }

        public double MaxValue
        {
            get
            {
                return (double)TypeDescriptor.GetProperties(Component)["MaxValue"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["MaxValue"].SetValue(Component, value);
            }
        }

        public int DecimalPlaces
        {
            get
            {
                return (int)TypeDescriptor.GetProperties(Component)["DecimalPlaces"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["DecimalPlaces"].SetValue(Component, value);
            }
        }

        public bool ShowNullText
        {
            get
            {
                return (bool)TypeDescriptor.GetProperties(Component)["ShowNullText"].GetValue(Component);
            }
            set
            {
                TypeDescriptor.GetProperties(Component)["ShowNullText"].SetValue(Component, value);
            }
        }

        #endregion


        private bool isControlInFormView(Control input)
        {
            while (input.Parent != null)
            {
                if (input.Parent.GetType().Name == "NestedContainerParent")
                {
                    return true;
                }

                input = input.Parent;
            }

            return false;
        }

        /// <summary>
        /// Returns collection of smart tags actions
        /// </summary>
        /// <returns></returns>
        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection actions = new DesignerActionItemCollection();

            //if (this.Component.GetType().Equals(typeof(C1InputMask)) || this.Component.GetType().Equals(typeof(C1InputDate)))
            //{
            //    // Actions:
            //    DesignerActionMethodItem actionMethod = new DesignerActionMethodItem(this,
            //        "ShowDesignForm", C1Localizer.GetString("C1Input.SmartTag.Designer"), "Actions", "");

            //    actions.Add(actionMethod);
            //}

            // Properties:
            DesignerActionHeaderItem actionHeader2 = new DesignerActionHeaderItem(C1Localizer.GetString("C1Input.SmartTag.Properties"));
            actions.Add(actionHeader2);

            DesignerActionPropertyItem actionProperty;

            if (typeof(C1InputText).Equals(Component.GetType()))
            {
                actionProperty = new DesignerActionPropertyItem("Format",
                                C1Localizer.GetString("C1Input.SmartTag.Format"), "Properties", C1Localizer.GetString("C1Input.SmartTag.MaskDescription"));
                actions.Add(actionProperty);

                actionProperty = new DesignerActionPropertyItem("Text", C1Localizer.GetString("C1Input.SmartTag.Text"), "Properties", C1Localizer.GetString("C1Input.SmartTag.TextDescription"));
                actions.Add(actionProperty);
            }
            
            // C1MaskdEdit smart tags
            if (typeof(C1InputMask).Equals(Component.GetType()))
            {
                actionProperty = new DesignerActionPropertyItem("Text", C1Localizer.GetString("C1Input.SmartTag.Text"), "Properties", C1Localizer.GetString("C1Input.SmartTag.TextDescription"));
                actions.Add(actionProperty);

                actionProperty = new DesignerActionPropertyItem("MaskFormat",
                                                C1Localizer.GetString("C1Input.SmartTag.Mask"), "Properties", C1Localizer.GetString("C1Input.SmartTag.MaskDescription"));
                actions.Add(actionProperty);
                actionProperty = new DesignerActionPropertyItem("PromptChar",
                                                C1Localizer.GetString("C1Input.SmartTag.PromptChar"), "Properties", C1Localizer.GetString("C1Input.SmartTag.PromptCharDescription"));
                actions.Add(actionProperty);
                actionProperty = new DesignerActionPropertyItem("PasswordChar",
                                                C1Localizer.GetString("C1Input.SmartTag.PasswordChar"), "Properties", C1Localizer.GetString("C1Input.SmartTag.PasswordCharDescription"));
                actions.Add(actionProperty);
            }

            // C1InputDate smart tags
            else if (typeof(C1InputDate).Equals(Component.GetType()))
            {
                C1InputDate input = Component as C1InputDate;
                C1InputDesigner inputDesigner = this.Designer as C1InputDesigner;
                if (input.Date == null && inputDesigner.needSetDefaultDateID == input.ID)
                {
                    if (!this.isControlInFormView(input)) 
                    {
                        this.Date = DateTime.Now;
                    }
                    inputDesigner.needSetDefaultDateID = String.Empty;
                }
                
                actionProperty = new DesignerActionPropertyItem("Date",
                                                C1Localizer.GetString("C1Input.SmartTag.Date"), "Properties", C1Localizer.GetString("C1Input.SmartTag.DateDescription"));
                actions.Add(actionProperty);
                actionProperty = new DesignerActionPropertyItem("DateFormat",
                                                C1Localizer.GetString("C1Input.SmartTag.DateFormat"), "Properties", C1Localizer.GetString("C1Input.SmartTag.DateFormatDescription"));
                actions.Add(actionProperty);

                actionProperty = new DesignerActionPropertyItem("DisplayFormat",
                                            C1Localizer.GetString("C1Input.SmartTag.DisplayFormat"), "Properties", C1Localizer.GetString("C1Input.SmartTag.DisplayFormatDescription"));
                actions.Add(actionProperty);

                //actionProperty = new DesignerActionPropertyItem("ShowNullText",
                //                                C1Localizer.GetString("C1Input.SmartTag.ShowNullText"), "Properties", C1Localizer.GetString("C1Input.SmartTag.ShowNullTextDescription"));
                //actions.Add(actionProperty);
            }

            // C1InputNumber and all descendants smart tags
            else if (Component is C1InputNumber)
            {
                actionProperty = new DesignerActionPropertyItem("Value",
                                                C1Localizer.GetString("C1Input.SmartTag.Value"), "Properties", C1Localizer.GetString("C1Input.SmartTag.ValueDescription"));
                actions.Add(actionProperty);

                actionProperty = new DesignerActionPropertyItem("MinValue",
                                                C1Localizer.GetString("C1Input.SmartTag.MinValue"), "Properties", C1Localizer.GetString("C1Input.SmartTag.MinValueDescription"));
                actions.Add(actionProperty);
                actionProperty = new DesignerActionPropertyItem("MaxValue",
                                                C1Localizer.GetString("C1Input.SmartTag.MaxValue"), "Properties", C1Localizer.GetString("C1Input.SmartTag.MaxValueDescription"));
                actions.Add(actionProperty);
                actionProperty = new DesignerActionPropertyItem("DecimalPlaces",
                                                C1Localizer.GetString("C1Input.SmartTag.DecimalPlaces"), "Properties", C1Localizer.GetString("C1Input.SmartTag.DecimalPlacesDescription"));
                actions.Add(actionProperty);
                //actionProperty = new DesignerActionPropertyItem("ShowNullText",
                //                                C1Localizer.GetString("C1Input.SmartTag.ShowNullText"), "Properties", C1Localizer.GetString("C1Input.SmartTag.ShowNullTextDescription"));
                //actions.Add(actionProperty);

            }

            if (!typeof(C1InputText).Equals(Component.GetType()))
            {
                actionProperty = new DesignerActionPropertyItem("Culture",
                                                C1Localizer.GetString("C1Input.SmartTag.Culture"), "Properties", C1Localizer.GetString("C1Input.SmartTag.CultureDescription"));
                actions.Add(actionProperty);
            }

#if DEBUG
            DesignerActionHeaderItem actionHeader = new DesignerActionHeaderItem("Debug information");
            actions.Add(actionHeader);

            string build = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            DesignerActionTextItem pDesignerActionTextItem = new DesignerActionTextItem("Build version: " + build, "Version");
            actions.Add(pDesignerActionTextItem);
#endif

            this.AddBaseSortedActionItems(actions);

            return actions;
        }

    }



}
