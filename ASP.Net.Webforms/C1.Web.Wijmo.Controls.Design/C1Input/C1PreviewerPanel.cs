using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.Reflection;
using System.IO;

namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    using C1.Web.Wijmo.Controls.C1Input;


    internal partial class C1PreviewerPanel : UserControl
    {
        #region --- constructor ---
        public C1PreviewerPanel()
        {
            InitializeComponent();
            this.webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
        }
        #endregion

        #region --- Preview initialization ---

        public void setInspectedComponent(object aComponent, System.Web.UI.Design.ControlDesigner controlDesigner)
        {
            this._inspectedComponent = (C1InputBase)aComponent;
            this._controlDesigner = controlDesigner;
        }

        #endregion

        #region --- Events handlers ---
        void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.doRefreshPreview();
        }
        #endregion

        #region --- private fields ---
        private C1InputBase _inspectedComponent;
        private System.Web.UI.Design.ControlDesigner _controlDesigner;
        #endregion

        #region --- public methods ---
        public void RefreshPreview()
        {
            this.webBrowser1.Navigate("about:blank");
        }
        #endregion

        #region --- private methods ---

        private void doRefreshPreview()
        {
            UpdateControlPreview(this.webBrowser1, (C1InputMask)this._inspectedComponent, this._controlDesigner);
        }

        private static string[] _C1InputJSIncludes = new string[] { 
            findResourceName("C1InputClient.js")};

        /// <summary>
        /// Find full path to resource by short name
        /// </summary>
        /// <param name="aResourceShortName"></param>
        /// <returns></returns>
        internal static string findResourceName(string aResourceShortName)
        {
            Assembly _CurrentAssembly = Assembly.GetExecutingAssembly();
            foreach (string ss in _CurrentAssembly.GetManifestResourceNames())
            {
                if (ss.IndexOf(aResourceShortName) != -1)
                {
                    return ss;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Get javascript includes text by control type
        /// </summary>
        /// <param name="Component"></param>
        /// <returns></returns>
        internal static string getJSIncludesText()
        {
            string sResult = "";
            for (int i = 0; i < _C1InputJSIncludes.Length; i++)
                sResult += "<script language=\"javascript\" type=\"text/javascript\">" + getResourceTextData(_C1InputJSIncludes[i]) + "</script>";

            return sResult;
        }

        /// <summary>
        /// Get Resource Text value
        /// </summary>
        /// <param name="sResourceName">Full path to resource in assembly</param>
        /// <returns></returns>
        internal static string getResourceTextData(string sResourceName)
        {
            Assembly _CurrentAssembly = Assembly.GetExecutingAssembly();
            try
            {
                string sC1MaskedEditInclude = "";
                Stream rs = _CurrentAssembly.GetManifestResourceStream(sResourceName);
                using (StreamReader sr = new StreamReader(rs))
                {
                    sC1MaskedEditInclude = sr.ReadToEnd();
                }
                return sC1MaskedEditInclude;
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.ToString());
                return "";
            }
        }

        public static void UpdateControlPreview(WebBrowser webBrowser, C1InputMask inspectedComponent, System.Web.UI.Design.ControlDesigner controlDesigner)
        {
            try
            {
                webBrowser.Document.Body.InnerHtml = "";
                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new System.IO.StringWriter(sb);
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);

                C1InputMask aControl = (C1InputMask)inspectedComponent;
                aControl.RenderControl(htw);

                string sScriptContent = "";
                sScriptContent += getJSIncludesText();

                string sResultBodyContent = "";
                sResultBodyContent += "<table width=100% height=100% align=center>";
                sResultBodyContent += "<tr><td align=center valign=middle>";
                
                sResultBodyContent += sb.ToString();
                if (aControl.Visible == false)
                {
                    sResultBodyContent += "Control is not visible";
                }
                sResultBodyContent += "</td></tr>";
                sResultBodyContent += "<tr><td height=1>";
                sResultBodyContent += "&nbsp;";

                sResultBodyContent += "</td></tr>";
                sResultBodyContent += "</table>";

                string sDocumentContent = "";
                sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
                sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
                sDocumentContent += "<head>" + NEW_LINE;
                sDocumentContent += sScriptContent + NEW_LINE;
                sDocumentContent += "</head>" + NEW_LINE;
                sDocumentContent += "<body>" + NEW_LINE;
                sDocumentContent += sResultBodyContent + NEW_LINE;
                sDocumentContent += "</body>" + NEW_LINE;
                sDocumentContent += "</html>" + NEW_LINE;

                webBrowser.Document.Write(sDocumentContent);
            }
            catch (Exception)
            {
#if DEBUG
                //MessageBox.Show(ex.ToString());
#endif
            }
        }

        const string NEW_LINE = "\r\n";
        #endregion

    }
}
