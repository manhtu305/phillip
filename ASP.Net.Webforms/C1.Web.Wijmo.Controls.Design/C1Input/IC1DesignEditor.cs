using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Web.UI.Design;


namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    interface IC1DesignEditor
    {
        void setInspectedComponent(object aComponent, ControlDesigner controlDesigner);

        void doActionApply();

        void doActionCancel();

        void doActionExit();

        void doCurrentTabSelected();

        void doOtherTabSelected();

    }
}