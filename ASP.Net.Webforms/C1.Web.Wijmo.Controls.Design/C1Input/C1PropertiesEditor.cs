using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;


namespace C1.Web.Wijmo.Controls.Design.C1Input
{
    using C1.Web.Wijmo.Controls.C1Input;


    internal partial class C1PropertiesEditor : UserControl, IC1DesignEditor
    {
        public C1PropertiesEditor()
        {
            InitializeComponent();
        }

        #region IC1DesignEditor Members
        C1InputBase ownerC1WebInputBase;
        public void setInspectedComponent(object aComponent, System.Web.UI.Design.ControlDesigner controlDesigner)
        {
            this.ownerC1WebInputBase = (C1InputBase)aComponent;
            this.propertyGrid1.SelectedObject = (IComponent)aComponent;
            this.propertyGrid1.Site = ((IComponent)aComponent).Site;
            this.propertyGrid1.PropertyTabs.AddTabType(
                typeof(System.Windows.Forms.Design.EventsTab),
                PropertyTabScope.Global);
            this.propertyGrid1.PropertyValueChanged += new PropertyValueChangedEventHandler(propertyGrid1_PropertyValueChanged);
            this.c1PreviewerPanel.setInspectedComponent(aComponent, controlDesigner);
            this.c1PreviewerPanel.RefreshPreview();
        }

        void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            this.c1PreviewerPanel.RefreshPreview();
        }

        public void doActionApply()
        {
        }

        public void doActionCancel()
        {
        }

        public void doActionExit()
        {
        }

        public void doCurrentTabSelected()
        {
            this.c1PreviewerPanel.RefreshPreview();
        }

        public void doOtherTabSelected()
        {
        }

        #endregion
    }
}
