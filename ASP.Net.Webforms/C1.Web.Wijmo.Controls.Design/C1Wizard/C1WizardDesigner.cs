using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.IO;



namespace C1.Web.Wijmo.Controls.Design.C1Wizard
{
    using C1.Web.Wijmo.Controls.C1Wizard;
    using C1.Web.Wijmo.Controls.Design.Localization;


    [SupportsPreviewControl(false)]
    public class C1WizardDesigner : C1ControlDesinger
    {
        C1Wizard _wizard;

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this._wizard = (C1Wizard)component;
        }

        private DesignerActionListCollection _actions;
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (_actions == null)
                {
                    _actions = new DesignerActionListCollection();
                    _actions.AddRange(base.ActionLists);
                    _actions.Add(new C1WizardActionList(this));
                }
                return _actions;
            }
        }

        public void OpenBuilder()
        {
            List<C1WizardStep> originalData = new List<C1WizardStep>();
            foreach (C1WizardStep tp in _wizard.Steps)
            {
                originalData.Add(tp);
            }

            C1WizardDesignerForm editorForm = new C1WizardDesignerForm(this._wizard);
            bool accept = ShowControlEditorForm(this._wizard, editorForm);

            if (accept)
                this.UpdateDesignTimeHtml();
            else
            {
                _wizard.Steps.Clear();
                foreach (C1WizardStep tp in originalData)
                {
                    _wizard.Steps.Add(tp);
                }
            }
        }

        public bool ShowControlEditorForm(object control, System.Windows.Forms.Form editorForm)
        {
            System.Windows.Forms.DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1Wizard.EditControl"));
                using (trans)
                {
                    System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(editorForm);
                    }
                    else
                        dr = System.Windows.Forms.DialogResult.None;
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = editorForm.ShowDialog();

            editorForm.Dispose();
            editorForm = null;

            return dr == System.Windows.Forms.DialogResult.OK;
        }

        public override String GetDesignTimeHtml(DesignerRegionCollection regions)
        {
            if (_wizard.WijmoControlMode == WijmoControlMode.Mobile)
            {
                //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
            }
            if (this._wizard.Steps.Count == 0)
            {
                string s = "<div style=\"width:{0};height:{1};border:solid 1px #bbbbbb;\"></div>";
                s = string.Format(s, 
                    _wizard.Width.IsEmpty ? "300px" : _wizard.Width.ToString(), 
                    _wizard.Height.IsEmpty ? "250px" : _wizard.Height.ToString()
                    );
                return s;
            }

            for (int i = 0; i < this._wizard.Steps.Count; i++)
            {
                DesignerRegion region = new DesignerRegion(this, i.ToString(), true);
                region.EnsureSize = true;
                regions.Add(region);
            }

            if (this._wizard.ActivedStep != null)
                this._wizard.ActivedStep.Attributes.Add(DesignerRegion.DesignerRegionAttributeName, regions.Count.ToString());

            regions.Add(new EditableDesignerRegion(this, "PageContent", false));
            string result = base.GetDesignTimeHtml(regions);
            return result;
        }

        /// <summary>
        /// Called when we get a click on a designer region.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClick(DesignerRegionMouseEventArgs e)
        {
            if (e.Region != null)
            {
                if (e.Region.Name == "PageContent") return;

                int index = 0;
                try
                {
                    index = int.Parse(e.Region.Name);
                }
                catch
                {
                }

                if (this._wizard.ActiveIndex != index)
                {
                    this._wizard.ActiveIndex = index;
                    base.UpdateDesignTimeHtml();
                }
            }
        }

        private C1WizardStep _editing = null;
        public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
        {
            _editing = null;
            // Get a reference to the designer host
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host == null || _wizard == null) return string.Empty;

            _editing = _wizard.ActivedStep;
            if (_editing == null) return string.Empty;

            TextWriter strWriter = new StringWriter();
            foreach (Control c in _editing.Controls)
            {
                ControlPersister.PersistControl(strWriter, c, host);
            }

            string s = strWriter.ToString();
            return s;
        }

        public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
        {
            if (content == null) return;

            // Get a reference to the designer host
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host == null) return;

            if (_wizard == null) return;
            if (_editing == null) return;

            using (DesignerTransaction transaction = host.CreateTransaction(C1Localizer.GetString("C1Wizard.SetEditableDesignerRegionContent")))
            {
                _editing.Controls.Clear();
                if (!string.IsNullOrEmpty(content))
                {
                    Control[] ctrls = ControlParser.ParseControls(host, content);
                    foreach (Control c in ctrls)
                    {
                        _editing.Controls.Add(c);
                    }
                }

                transaction.Commit();
            }
        }

        internal void NotifyDirty()
        {
            if (_wizard == null) return;

            IComponentChangeService changeSvc =(IComponentChangeService)this.GetService(typeof(IComponentChangeService));
            if (changeSvc != null)
            {
                changeSvc.OnComponentChanging(_wizard, null);
                changeSvc.OnComponentChanged(_wizard, null, null, null);
            }
        }
    }


    internal class C1WizardActionList : DesignerActionListBase
    {
        #region Fields

        private C1Wizard _wizard;
        private DesignerActionItemCollection _actions;

        #endregion

        #region Constructor

        public C1WizardActionList(C1WizardDesigner designer)
            : base(designer)
        {
            _wizard = designer.Component as C1Wizard;
        }

        #endregion

        #region Actions

        public void OpenBuilder()
        {
            C1WizardDesigner designer = this.Designer as C1WizardDesigner;
            designer.OpenBuilder();
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_actions == null)
            {
                _actions = new DesignerActionItemCollection();
                _actions.Add(new DesignerActionMethodItem(this, "OpenBuilder", C1Localizer.GetString("C1Wizard.SmartTag.Designer"), "", C1Localizer.GetString("C1Wizard.SmartTag.DesignerDescription"), true));
                AddBaseSortedActionItems(_actions);
            }

            return _actions;
        }

        #endregion
    }
}
