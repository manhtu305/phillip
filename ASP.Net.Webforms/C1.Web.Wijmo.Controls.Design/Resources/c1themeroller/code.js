jQuery(document).ready(function () {
    $("#themeEditor").themeeditor({
        themeValue: window.external.GetCurrentSerializedThemeValue(),
        serialize: $.proxy(function (e, args) {
            window.setTimeout(function () {
                window.external.ThemeChanged(args.isValid, args.propertyName, args.newValue);
            }, 50);
        }, this),
        applyButtonClicked: function () {
            window.setTimeout(function () {
                window.external.ThemeChanged(true, "", window.external.GetCurrentSerializedThemeValue());
            }, 50);
        }
    });
    $("#cbWijSelector").change(function (e) {
        createWijSample((e.target).value);
    }).change();
    function createWijSample(controlName) {
        var wijContainer = $("#wijSampleContainer"), ctrlTarget;
        wijContainer.empty();
        switch((controlName || "").toLowerCase()) {
            case "wijcalendar":
                wijContainer.append(ctrlTarget = $("<div />"));
                ctrlTarget.wijcalendar({
                });
                break;
            case "wijgrid":
                wijContainer.append(ctrlTarget = $("<table />"));
                ctrlTarget.wijgrid({
                    allowPaging: true,
                    data: (function (count) {
                        var result = [];
                        for(var i = 0; i < count; i++) {
                            result.push({
                                Column0: "abc",
                                Column1: "abc",
                                Column2: "abc"
                            });
                        }
                        return result;
                    })(10),
                    pageSize: 5
                });
                break;
            default:
                alert("Unknown controlName value: " + controlName);
        }
    }
});
