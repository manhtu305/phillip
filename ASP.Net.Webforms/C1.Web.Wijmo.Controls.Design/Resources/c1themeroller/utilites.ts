/// <reference path="declarations/jquery.d.ts"/>
/// <reference path="declarations/jquery.ui.d.ts"/>

interface BrowserPublic {
    ThemeChanged(isValid: bool, propertyName: string, newThemeValue: string): void;
    GetCurrentSerializedThemeValue(): string;
    GetResource(resourceName: string, defaultValue: string): string;
}

module c1themeroller {
	export class JQueryUIWidget {
		_super: Function;
		_superApply: Function;
		widgetEventPrefix: string;
		widgetFullName: string;
		widgetName: string;
		namespace: string;
		options: any;
		element: JQuery;
		destroy() { };
		disable: Function;
		enable: Function;
		_trigger: Function;
		_setOption(name: string, value) { };
		option: Function;
		_create() { };
		_destroy() { };
		_init() { };
		widget() { return this.element; }
	}
}

module c1themeroller.utilites {

	export function stringFormat(pattern: string, ...params: any[]): string {
		var i: number,
			len: number;

		if (!pattern) {
			return "";
		}

		for (i = 0, len = params.length; i < len; i++) {
			pattern = pattern.replace(new RegExp("\\{" + i + "\\}", "gm"), params[i]);
		}

		return pattern;
	}

	export function ensureHEXColor(value: string): string {
		if (value && (value.charAt(0) !== '#')) {
			var hex = parseInt(value, 16);
			if (!isNaN(hex)) {
				return "#" + value;
			}
		}

		return value;
	}

	export function compareStyle(styleName: any, value: any, defaultValue: any, needMore: bool): bool {
	    
	    var currentAppliedValue, defaultAppliedValue, checkedDiv;
	    if (value.toString().toLowerCase() == defaultValue.toString().toLowerCase())
	    {
	        return true;
	    }
	        
	    checkedDiv = $("<div></div>").appendTo(document.body);	    
	    checkedDiv.css(styleName, defaultValue);
	    defaultAppliedValue = checkedDiv.css(styleName);
	    if (needMore)
	    {
	        checkedDiv.removeAttr("style");
	        checkedDiv.css(styleName, value);
	        currentAppliedValue = checkedDiv.css(styleName);
	        if (defaultAppliedValue == currentAppliedValue)
	        {
	            checkedDiv.remove();
	            return true;
	        }	            
	        checkedDiv.removeAttr("style");
	        checkedDiv.css(styleName, defaultValue);
	    }
	    checkedDiv.css(styleName, value);
	    currentAppliedValue = checkedDiv.css(styleName);
	    checkedDiv.remove();
	    if (currentAppliedValue != defaultAppliedValue)
	    {
	        return true;
	    }
	    return false;
	}

	export function checkValid(propertyName: any, styleName: any, value: any): bool {
	    var defaultValue,
            needMore = false;
	    if (propertyName == null || styleName == null)
	        return true;
	    switch (styleName)
	    {
	        case "font-family":
	            defaultValue = "Arial";
	            break;
	        case "font-size":
	            defaultValue = "9px";
	            break;
	        case "font-weight":
	            defaultValue = "bold";
	            break;
	        case "background-color":
	            defaultValue = "red";
	            value = value.replace(/(^\s*)|(\s*$)/g, "");
	            if (!value)
	                return false;
	            if (value && (value.toString().charAt(0) == "#"))
	            {
	                if (value.toString().length > 7)
	                    return false;
	            }
	            needMore = true;
	            break;
	        case "border-radius":
	            defaultValue = "9px";
	            break;
	        case "opacity":
	            defaultValue = 0.5;
	            break;
	        case "padding-top":
	            defaultValue = "9px";
	            break;
	        default:
	            return true;
	    }
	    return compareStyle(styleName, value, defaultValue, needMore);
	}

	export function getResource(resourceName: string, defaultValue: string): string{
	    return  window.external.GetResource(resourceName, defaultValue);
	}
}