﻿/// <reference path="declarations/jquery.d.ts"/>
/// <reference path="declarations/jquery.ui.d.ts"/>

/// <reference path="utilites.ts"/>

module c1themeroller.colorpicker {


	var $ = jQuery;

	export class colorpicker extends JQueryUIWidget {
		options: IColorPickerOptions;

		private _picker: JQuery;
		private _outerDiv: JQuery;
		private _value: any;

		_create() {
			$.Widget.prototype._create.apply(this, arguments);

			this._picker = null;
			this._outerDiv = this.element.closest("div.color-container");
			this._value = undefined;

			// temporary attach the farbtastic to the editor to colorize it
			(<any>$("<div></div>")).farbtastic(this.element).remove();

			var self = this;

			this.element
				.click(function () {
					if (self._picker) {
						self._removePicker();
					} else {
						self._addPicker();
					}
				})
				.focusout(function () {
					self._removePicker();
				})
		}

		_destroy() {
			this._removePicker();
			this.element.unwrap();

			this._outerDiv = null;
			this._picker = null;
		}

		_init() {
			$.Widget.prototype._init.apply(this, arguments)
		}

		_addPicker() {
			this._picker = $("<div class=\"picker\" style=\"position:absolute\"></div>")
				.appendTo(this._outerDiv)
				.position({
					of: this.element,
					my: "left top",
					at: "left bottom"
				})
				.farbtastic(this.element);

			this._value = this.element.val();
		}

		_removePicker() {
			if (this._picker) {
				this._picker.remove();
			}
			this._picker = null;

			if (this._value !== this.element.val()) { // perhaps this comparison is redundant
				this.element.change(); // notify model about changes (IE10 blur issue)
			}
		}
	}

	export interface IColorPickerOptions {
	}

	colorpicker.prototype.widgetEventPrefix = "colorpicker";

	class colorpicker_options implements IColorPickerOptions {
	};

	colorpicker.prototype.options = new colorpicker_options();

	$.widget("c1themeroller.colorpicker", colorpicker.prototype);

}

interface JQuery {
	colorpicker: any;
}