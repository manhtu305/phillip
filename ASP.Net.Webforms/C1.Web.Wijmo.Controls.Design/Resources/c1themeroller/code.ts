/// <reference path="declarations/jquery.d.ts"/>
/// <reference path="declarations/jquery.ui.d.ts"/>
/// <reference path="themeeditor.ts"/>
/// <reference path="utilites.ts"/>

interface JQuery {
	wijcalendar: any;
	wijgrid: any;
}

jQuery(document).ready(function () {

	$("#themeEditor").themeeditor({
		themeValue: window.external.GetCurrentSerializedThemeValue(), // initial value

		serialize: $.proxy(function (e, args) {
			window.setTimeout(function () {
			    window.external.ThemeChanged(args.isValid, args.propertyName, args.newValue);
			}, 50);
		}, this),

		applyButtonClicked: function () {
			window.setTimeout(function () {
				window.external.ThemeChanged(true, "", window.external.GetCurrentSerializedThemeValue());
			}, 50);
		},
	});

	$("#cbWijSelector")
		.change(function (e) {
			createWijSample((<any>e.target).value);
		})
		.change();

	function createWijSample(controlName: string): void {
		var wijContainer: JQuery = $("#wijSampleContainer"),
			ctrlTarget: JQuery;

		wijContainer.empty();

		switch ((controlName || "").toLowerCase()) {
			case "wijcalendar":
				wijContainer.append(ctrlTarget = $("<div />"));

				ctrlTarget.wijcalendar({});

				break;

			case "wijgrid":
				wijContainer.append(ctrlTarget = $("<table />"));

				ctrlTarget.wijgrid({
					allowPaging: true,
					data: (function (count) {
						var result = [];

						for (var i = 0; i < count; i++) {
							result.push({ Column0: "abc", Column1: "abc", Column2: "abc" });
						}

						return result;
					})(10),
					pageSize: 5
				});

				break;

			default:
				alert("Unknown controlName value: " + controlName);
		}
	}
});