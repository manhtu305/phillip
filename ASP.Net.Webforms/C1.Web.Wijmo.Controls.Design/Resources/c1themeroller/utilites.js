var c1themeroller;
(function (c1themeroller) {
    var JQueryUIWidget = (function () {
        function JQueryUIWidget() { }
        JQueryUIWidget.prototype.destroy = function () {
        };
        JQueryUIWidget.prototype._setOption = function (name, value) {
        };
        JQueryUIWidget.prototype._create = function () {
        };
        JQueryUIWidget.prototype._destroy = function () {
        };
        JQueryUIWidget.prototype._init = function () {
        };
        JQueryUIWidget.prototype.widget = function () {
            return this.element;
        };
        return JQueryUIWidget;
    })();
    c1themeroller.JQueryUIWidget = JQueryUIWidget;    
})(c1themeroller || (c1themeroller = {}));
var c1themeroller;
(function (c1themeroller) {
    (function (utilites) {
        function stringFormat(pattern) {
            var params = [];
            for (var _i = 0; _i < (arguments.length - 1); _i++) {
                params[_i] = arguments[_i + 1];
            }
            var i, len;
            if(!pattern) {
                return "";
            }
            for(i = 0 , len = params.length; i < len; i++) {
                pattern = pattern.replace(new RegExp("\\{" + i + "\\}", "gm"), params[i]);
            }
            return pattern;
        }
        utilites.stringFormat = stringFormat;
        function ensureHEXColor(value) {
            if(value && (value.charAt(0) !== '#')) {
                var hex = parseInt(value, 16);
                if(!isNaN(hex)) {
                    return "#" + value;
                }
            }
            return value;
        }
        utilites.ensureHEXColor = ensureHEXColor;
        function compareStyle(styleName, value, defaultValue, needMore) {
            var currentAppliedValue, defaultAppliedValue, checkedDiv;
            if(value.toString().toLowerCase() == defaultValue.toString().toLowerCase()) {
                return true;
            }
            checkedDiv = $("<div></div>").appendTo(document.body);
            checkedDiv.css(styleName, defaultValue);
            defaultAppliedValue = checkedDiv.css(styleName);
            if(needMore) {
                checkedDiv.removeAttr("style");
                checkedDiv.css(styleName, value);
                currentAppliedValue = checkedDiv.css(styleName);
                if(defaultAppliedValue == currentAppliedValue) {
                    checkedDiv.remove();
                    return true;
                }
                checkedDiv.removeAttr("style");
                checkedDiv.css(styleName, defaultValue);
            }
            checkedDiv.css(styleName, value);
            currentAppliedValue = checkedDiv.css(styleName);
            checkedDiv.remove();
            if(currentAppliedValue != defaultAppliedValue) {
                return true;
            }
            return false;
        }
        utilites.compareStyle = compareStyle;
        function checkValid(propertyName, styleName, value) {
            var defaultValue, needMore = false;
            if(propertyName == null || styleName == null) {
                return true;
            }
            switch(styleName) {
                case "font-family":
                    defaultValue = "Arial";
                    break;
                case "font-size":
                    defaultValue = "9px";
                    break;
                case "font-weight":
                    defaultValue = "bold";
                    break;
                case "background-color":
                    defaultValue = "red";
                    value = value.replace(/(^\s*)|(\s*$)/g, "");
                    if(!value) {
                        return false;
                    }
                    if(value && (value.toString().charAt(0) == "#")) {
                        if(value.toString().length > 7) {
                            return false;
                        }
                    }
                    needMore = true;
                    break;
                case "border-radius":
                    defaultValue = "9px";
                    break;
                case "opacity":
                    defaultValue = 0.5;
                    break;
                case "padding-top":
                    defaultValue = "9px";
                    break;
                default:
                    return true;
            }
            return compareStyle(styleName, value, defaultValue, needMore);
        }
        utilites.checkValid = checkValid;
        function getResource(resourceName, defaultValue) {
            return window.external.GetResource(resourceName, defaultValue);
        }
        utilites.getResource = getResource;
    })(c1themeroller.utilites || (c1themeroller.utilites = {}));
    var utilites = c1themeroller.utilites;
})(c1themeroller || (c1themeroller = {}));
