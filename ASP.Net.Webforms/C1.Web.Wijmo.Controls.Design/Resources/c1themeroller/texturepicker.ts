/// <reference path="declarations/jquery.d.ts"/>
/// <reference path="declarations/jquery.ui.d.ts"/>

/// <reference path="utilites.ts"/>

module c1themeroller.texturepicker {

	var $ = jQuery;

	// taken from the http://jqueryui.com/themeroller/ page
	export var textureData = {
		flat: { height: 100, width: 40 },
		glass: { height: 400, width: 1 },
		highlight_soft: { height: 100, width: 1 },
		highlight_hard: { height: 100, width: 1 },
		inset_soft: { height: 100, width: 1 },
		inset_hard: { height: 100, width: 1 },
		diagonals_small: { height: 40, width: 40 },
		diagonals_medium: { height: 40, width: 40 },
		diagonals_thick: { height: 40, width: 40 },
		dots_small: { height: 2, width: 2 },
		dots_medium: { height: 4, width: 4 },
		white_lines: { height: 100, width: 40 },
		gloss_wave: { height: 100, width: 500 },
		diamond: { height: 8, width: 10 },
		loop: { height: 21, width: 21 },
		carbon_fiber: { height: 9, width: 8 },
		diagonal_maze: { height: 10, width: 10 },
		diamond_ripple: { height: 22, width: 22 },
		hexagon: { height: 10, width: 12 },
		layered_circles: { height: 13, width: 13 },
		"3D_boxes": { height: 10, width: 12 },
		glow_ball: { height: 16, width: 16 },
		spotlight: { height: 16, width: 16 },
		fine_grain: { height: 60, width: 60 }
	}

	export var TextureUrl = "http://download.jqueryui.com/themeroller/images/ui-bg_{0}_100_555_{1}x{2}.png";

	export class texturepicker extends JQueryUIWidget {
		options: ITexturePickerOptions;

		private _$pickerDiv: JQuery;

		_create() {
			var texture: string = this.element.val() || "flat", // provided by data model
				self = this;

			this.element
				.hide()
				.after(this._$pickerDiv = $("<div></div>"));

			this._$pickerDiv
				.addClass("texture-picker")
				.append(this._createTexturesList())
				.css({
					"background-image": "url('" + this._getTextureUrl(texture,
						c1themeroller.texturepicker.textureData[texture].height,
						c1themeroller.texturepicker.textureData[texture].width) + "')"
				})
				.click((e) => {
					self._$pickerDiv.find("ul")
						.position({
							of: self._$pickerDiv,
							my: "left top",
							at: "left bottom"
						})
						.attr({
							tabIndex: 0
						})
						.show()
						.focus()
						.focusout((e) => {
							window.setTimeout(() => {
								$(e.target).hide();
							}, 100);
						});
				});

			$.Widget.prototype._create.apply(this, arguments);
		}

		_destroy() {
		}

		_init() {
			$.Widget.prototype._init.apply(this, arguments)
		}

		_createTexturesList(): JQuery {
			var $ul = $("<ul />"),
				self = this;
		
			$.each(c1themeroller.texturepicker.textureData, (key: string, value: { height: number; width: number; }) => {
				var url = self._getTextureUrl(key, value.height, value.width),
					$li: JQuery,
					$a: JQuery;

				$a = $("<a />")
					.attr({ title: key, href: "#" })
					.text(key);

				$li = $("<li/>")
					.addClass(key)
					.css({
						"background-image": "url('" + url + "')"
					})
					.bind("click", $li, $.proxy(self._onItemClicked, self));

				$li.append($a)
				$ul.append($li);
			});

			$ul.css("display", "none");

			return $ul;
		}

		_onItemClicked(e) {
		}

		_getTextureUrl(name: string, height: number, width: number): string {
			return c1themeroller.utilites.stringFormat(c1themeroller.texturepicker.TextureUrl, name.replace("_", "-"), width, height);
		}
	}

	export interface ITexturePickerOptions {
	}

	texturepicker.prototype.widgetEventPrefix = "texturepicker";

	class texturepicker_options implements ITexturePickerOptions {
	};

	texturepicker.prototype.options = new texturepicker_options();

	$.widget("c1themeroller.texturepicker", texturepicker.prototype);
}

interface JQuery {
	texturepicker: any;
}