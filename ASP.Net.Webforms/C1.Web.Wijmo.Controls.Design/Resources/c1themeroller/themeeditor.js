var __extends = this.__extends || function (d, b) {
	function __() { this.constructor = d; }
	__.prototype = b.prototype;
	d.prototype = new __();
};
var c1themeroller;
(function (c1themeroller) {
	(function (themeeditor) {
		var $ = jQuery;
		var themeeditor = (function (_super) {
			__extends(themeeditor, _super);
			function themeeditor() {
				_super.apply(this, arguments);

			}
			themeeditor.prototype._create = function () {
				$.Widget.prototype._create.apply(this, arguments);
			};
			themeeditor.prototype._init = function () {
				this._initialized = false;
				var innerDiv = this.element.find("div:first");
				if (innerDiv.length) {
					ko.removeNode(innerDiv[0]);
				}
				var html = "<div class='themeEditor clearfix'>" + this._getApplyArea() + this._getFontTab() + this._getCornerRadiusTab() + this._createHeaderToolbarTab() + this._createContentTab() + this._createClickableDefaultTab() + this._createClickableHoverTab() + this._createClickableActiveTab() + this._createHighlightTab() + this._createErrorTab() + this._createModalOverlaysTab() + this._createDropShadowsTab() + "</div";
				this.element.append($(html));
				this._themeModel = new c1themeroller.model.ThemeModel(this.options.themeValue);
				ko.applyBindings(this._themeModel, this.element[0]);
				this.element.find(":text.color").colorpicker();
				this.element.find(".tab-header").click(function (e) {
					var $header = $(e.target).closest(".tab-header"), $content = $header.next();
					if ($content.is(":visible")) {
						$content.hide();
						$header.removeClass("state-active").find(".arrow-icon").removeClass("icon-triangle-1-s").addClass("icon-triangle-1-e");
					} else {
						$content.show();
						$header.addClass("state-active").find(".arrow-icon").removeClass("icon-triangle-1-e").addClass("icon-triangle-1-s");
					}
				}).hover(function (e) {
					var $header = $(e.target).closest(".tab-header");
					$header.addClass("state-hover");
				}, function (e) {
					var $header = $(e.target).closest(".tab-header");
					$header.removeClass("state-hover");
				});
				var self = this;
				this._btnApply = this.element.find(".apply-area :button").click(function (e) {
					$(e.target).attr("disabled", "disabled");
					self._trigger("applyButtonClicked", null);
				});
				this._initialized = true;
				ko.bindingHandlers.styleTracked = {
					update: function (element, valueAccessor, allBindings) {
						var $Element, propertyName, styleName, valid, value;
						if (!self._initialized) {
							return;
						}
						$Element = $(element);
						propertyName = $Element.attr("propertyname");
						styleName = $Element.attr("stylename");
						value = ko.utils.unwrapObservable(valueAccessor());
						valid = c1themeroller.utilites.checkValid(propertyName, styleName, value);
						self._themePropChanged(valid, propertyName);
					}
				};
			};
			themeeditor.prototype._themePropChanged = function (valid, propertyName) {
				var value;
				if (this._initialized) {
					if (!valid) {
						this._btnApply.attr("disabled", "disabled");
						value = "";
					} else {
						value = this._themeModel.Serialize();
					}
					this._trigger("serialize", null, {
						isValid: valid,
						propertyName: propertyName,
						newValue: value
					});
					if (valid) {
						this._btnApply.removeAttr("disabled");
					}
				}
			};
			themeeditor.prototype._getApplyArea = function () {
				var result = "<div class='apply-area'>" + "<input type='button' value='" + c1themeroller.utilites.getResource("ThemeRoller.UI.ApplyChanges", "Apply changes") + "' disabled='disabled' />" + "</div>";
				return result;
			};
			themeeditor.prototype._getFontTab = function () {
				var result = this._getOuterTab("font") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings", "Font Settings")) + this._getFontTabContent() + "</div>";
				return result;
			};
			themeeditor.prototype._getCornerRadiusTab = function () {
				var result = this._getOuterTab("corner-radius") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.CornerRadius", "Corner Radius")) + this._getCornerRadiusTabContent() + "</div>";
				return result;
			};
			themeeditor.prototype._createHeaderToolbarTab = function () {
				var result = this._getOuterTab("header-toolbar") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.HeaderAndToolbar", "Header/Toolbar"), "ui-widget-header") + this._getTabContentWrapper() + this._getBackgroundBorderContent("Header") + "</div>" + "</div>";
				return result;
			};
			themeeditor.prototype._createContentTab = function () {
				var result = this._getOuterTab("contentTab") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.Content", "Content"), "ui-widget-content") + this._getTabContentWrapper() + this._getBackgroundBorderContent("Content") + "</div>" + "</div>";
				return result;
			};
			themeeditor.prototype._createClickableDefaultTab = function () {
				return this._getOuterTab("clickable-default") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ClickableDefaultState", "Clickable: default state"), "ui-state-default") + this._getTabContentWrapper() + this._getBackgroundBorderContent("ClickableDefault") + "</div>" + "</div>";
			};
			themeeditor.prototype._createClickableHoverTab = function () {
				return this._getOuterTab("clickable-hover") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ClickableHoverState", "Clickable: hover state"), "ui-state-hover") + this._getTabContentWrapper() + this._getBackgroundBorderContent("ClickableHover") + "</div>" + "</div>";
			};
			themeeditor.prototype._createClickableActiveTab = function () {
				return this._getOuterTab("clickable-active") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ClickableActiveState", "Clickable: active state"), "ui-state-active") + this._getTabContentWrapper() + this._getBackgroundBorderContent("ClickableActive") + "</div>" + "</div>";
			};
			themeeditor.prototype._createHighlightTab = function () {
				return this._getOuterTab("highlight") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.Highlight", "Highlight"), "ui-state-highlight") + this._getTabContentWrapper() + this._getBackgroundBorderContent("Highlight") + "</div>" + "</div>";
			};
			themeeditor.prototype._createErrorTab = function () {
				return this._getOuterTab("error") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.Error", "Error"), "ui-state-error") + this._getTabContentWrapper() + this._getBackgroundBorderContent("Error") + "</div>";
			};
			themeeditor.prototype._createModalOverlaysTab = function () {
				return this._getOuterTab("overlay") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ModalScreenForOverlays", "Modal Screen for Overlays")) + this._getOverlayTabContent("Overlay") + "</div>";
			};
			themeeditor.prototype._createDropShadowsTab = function () {
				return this._getOuterTab("dropshadows") + this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows", "Drop Shadows")) + this._getDropShadowsTabContent("DropShadows") + "</div>";
			};
			themeeditor.prototype._getOuterTab = function (name) {
				return "<div class='tab tab-" + name + " clearfix'>";
			};
			themeeditor.prototype._getHeader = function (caption, previewClass) {
				return "<div class='tab-header state-default corner-all'>" + "<span class=\"arrow-icon icon-triangle-1-e\">Collapse</span>" + (previewClass ? "<div class='state-preview corner-all " + previewClass + "'>abc</div>" : "") + "<a href='#'>" + caption + "</a>" + "</div>";
			};
			themeeditor.prototype._getTabContentWrapper = function () {
				return this.options.collapseAllOnInit ? "<div class='tab-content clearfix' style='display:none'>" : "<div class='tab-content clearfix'>";
			};
			themeeditor.prototype._getFontTabContent = function () {
				return this._getTabContentWrapper() + "<div class='field-group clearfix'>" + "<label for='tbFontFamily'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings.Family", "Family") + "</label>" + "<input id='tbFontFamily' class='font-family' data-bind='value: Font.Family, styleTracked: Font.Family' stylename='font-family' propertyname='Family' type='text' />" + "</div>" + "<div class='field-group clearfix'>" + "<label for='tbFontWeight'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings.Weight", "Weight") + "</label>" + "<select id='tbFontWeight' class='font-weight' data-bind='value: Font.Weight, styleTracked: Font.Weight, options: AvailableFontWeights' stylename='font-weight' propertyname='Weight'>" + "</select>" + "</div>" + "<div class='field-group clearfix'>" + "<label for='tbFontSize'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings.Size", "Size") + "</label>" + "<input id='tbFontSize' class='font-size' data-bind='value: Font.Size, styleTracked: Font.Size' type='text' stylename='font-size' propertyname='Size' />" + "</div>" + "</div>";
			};
			themeeditor.prototype._getCornerRadiusTabContent = function () {
				return this._getTabContentWrapper() + "<div class='field-group clearfix'>" + "<label for='tbCornerRadius'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Corners", "Corners") + ": </label>" + "<input id='tbCornerRadius' class='cornerRadius' type='text' data-bind='value: Corner.Radius, styleTracked: Corner.Radius' stylename='border-radius' propertyname='Radius' />" + "</div>" + "</div>";
			};
			themeeditor.prototype._getOverlayTabContent = function (topDataName) {
				return this._getTabContentWrapper() + this._getBackgroundFieldGroupContent(topDataName) + "<div class='field-group clearfix'>" + "<label class='float-left' for='tb" + topDataName + "Opacity'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.ModalScreenForOverlays.OverlayOpacity", "Overlay Opacity") + ": </label>" + "<input id='tb" + topDataName + "Opacity' class='opacity' type='text' data-bind='value: " + topDataName + ".Opacity, styleTracked: " + topDataName + ".Opacity' stylename='opacity' propertyname='Overlay Opacity' />" + "<span class='opacity-per'>%</span>" + "</div>" + "</div>";
			};
			themeeditor.prototype._getDropShadowsTabContent = function (topDataName) {
				return this._getTabContentWrapper() + this._getBackgroundFieldGroupContent(topDataName) + "<div class='dropshadows-additional-props'>" + "<div class='field-group clearfix'>" + "<label class='float-left' for='tb" + topDataName + "Shadow'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.ShadowOpacity", "Shadow Opacity") + ": </label>" + "<input id='tb" + topDataName + "Shadow' class='opacity' type='text' data-bind='value: " + topDataName + ".Opacity, styleTracked: " + topDataName + ".Opacity' stylename='opacity' propertyname='Shadow Opacity' />" + "<span class='opacity-per'>%</span>" + "</div>" + "<div class='field-group clearfix'>" + "<label class='float-left' for='tb" + topDataName + "Thickness'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.ShadowThickness", "Shadow Thickness") + ": </label>" + "<input id='tb" + topDataName + "Thickness' class='offset' type='text' data-bind='value: " + topDataName + ".Thickness, styleTracked: " + topDataName + ".Thickness' stylename='padding-top' propertyname='Shadow Thickness' />" + "</div>" + "<div class='field-group clearfix'>" + "<label class='float-left' for='tb" + topDataName + "OffsetTop'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.TopOffset", "Top Offset") + ": </label>" + "<input id='tb" + topDataName + "OffsetTop' class='offset' type='text' data-bind='value: " + topDataName + ".OffsetTop, styleTracked: " + topDataName + ".OffsetTop' stylename='padding-top' propertyname='Top Offset' />" + "</div>" + "<div class='field-group clearfix'>" + "<label class='float-left' for='tb" + topDataName + "OffsetLeft'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.LeftOffset", "Left Offset") + ": </label>" + "<input id='tb" + topDataName + "OffsetLeft' class='offset' type='text' data-bind='value: " + topDataName + ".OffsetLeft, styleTracked: " + topDataName + ".OffsetLeft' stylename='padding-top' propertyname='Left Offset' />" + "</div>" + "<div class='field-group clearfix'>" + "<label class='float-left' for='tb" + topDataName + "CornerRadius'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Corners", "Corners") + ": </label>" + "<input id='tb" + topDataName + "CornerRadius' class='cornerRadius' type='text' data-bind='value: " + topDataName + ".Corner.Radius, styleTracked: " + topDataName + ".Corner.Radius' stylename='border-radius' propertyname='Radius' />" + "</div>" + "</div>" + "</div>";
			};
			themeeditor.prototype._getBackgroundBorderContent = function (topDataName) {
				var v = {
					"borderColor": {
						id: "tb" + topDataName + "BorderColor",
						data: topDataName + ".BorderColor.Value"
					},
					"foreColor": {
						id: "tb" + topDataName + "ForeColor",
						data: topDataName + ".ForeColor.Value"
					},
					"iconColor": {
						id: "tb" + topDataName + "IconColor",
						data: topDataName + ".IconColor.Value"
					}
				};
				return this._getBackgroundFieldGroupContent(topDataName) + "<div class='field-group field-group-border float-left clearfix'>" + "<label for='" + v.borderColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Border", "Border") + "</label>" + "<div class='color-container'>" + "<input id='" + v.borderColor.id + "' class='color' type='text' data-bind='value: " + v.borderColor.data + ", styleTracked: " + v.borderColor.data + "' stylename='background-color' propertyname='Color' />" + "</div>" + "</div>" + "<div class='field-group float-left clearfix'>" + "<label for='" + v.foreColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Text", "Text") + "</label>" + "<div class='color-container'>" + "<input id='" + v.foreColor.id + "' class='color' type='text' data-bind='value: " + v.foreColor.data + ", styleTracked: " + v.foreColor.data + "' stylename='background-color' propertyname='Color' />" + "</div>" + "</div>" + "<div class='field-group float-left clearfix'>" + "<label for='" + v.iconColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Icon", "Icon") + "</label>" + "<div class='color-container'>" + "<input id='" + v.iconColor.id + "' class='color' type='text' data-bind='value: " + v.iconColor.data + ", styleTracked: " + v.iconColor.data + "' stylename='background-color' propertyname='Color' />" + "</div>" + "</div>";
			};
			themeeditor.prototype._getBackgroundFieldGroupContent = function (topDataName) {
				var v = {
					"bgColor": {
						id: "tb" + topDataName + "BgColor",
						data: topDataName + ".Background.Color.Value"
					},
					"bgTexture": {
						id: "tb" + topDataName + "BgTexture",
						data: topDataName + ".Background.Texture"
					},
					"bgOpacity": {
						id: "tb" + topDataName + "BgOpacity",
						data: topDataName + ".Background.Opacity"
					}
				};
				return "<div class='field-group field-group-background clearfix'>" + "<label for='" + v.bgColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.BackgroundColorAndTexture", "Background color &amp; texture") + "</label>" + "<div class='color-container'>" + "<input id='" + v.bgColor.id + "' class='color' type='text' data-bind='value: " + v.bgColor.data + ", styleTracked: " + v.bgColor.data + "' stylename='background-color' propertyname='Color' />" + "</div>" + "<div class='texture-container'>" + "<select id='" + v.bgTexture.id + "' class='texture' data-bind='value: " + v.bgTexture.data + ", options: AvailableTextures, styleTracked: " + v.bgTexture.data + "'>" + "</select>" + "</div>" + "<input id='" + v.bgOpacity.id + "' type='text' class='opacity' data-bind='value: " + v.bgOpacity.data + ", styleTracked: " + v.bgOpacity.data + "' stylename='opacity' propertyname='Opacity' />" + "<span class ='opacity-per'>%</span>" + "</div>";
			};
			return themeeditor;
		})(c1themeroller.JQueryUIWidget);
		themeeditor.themeeditor = themeeditor;
		themeeditor.prototype.widgetEventPrefix = "themeeditor";
		var themeeditor_options = (function () {
			function themeeditor_options() {
				this.collapseAllOnInit = true;
				this.themeValue = undefined;
				this.serialize = null;
				this.applyButtonClicked = null;
			}
			return themeeditor_options;
		})();
		;
		themeeditor.prototype.options = new themeeditor_options();
		$.widget("c1themeroller.themeeditor", themeeditor.prototype);
	})(c1themeroller.themeeditor || (c1themeroller.themeeditor = {}));
	var themeeditor = c1themeroller.themeeditor;
})(c1themeroller || (c1themeroller = {}));
