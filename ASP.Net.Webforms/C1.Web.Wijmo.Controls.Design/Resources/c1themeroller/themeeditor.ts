﻿/// <reference path="declarations/jquery.d.ts"/>
/// <reference path="declarations/jquery.ui.d.ts"/>

/// <reference path="thememodel.ts"/>
/// <reference path="colorpicker.ts"/>
/// <reference path="texturepicker.ts"/>
/// <reference path="utilites.ts"/>

declare var ko;

module c1themeroller.themeeditor {


	var $ = jQuery;

	export class themeeditor extends JQueryUIWidget {
		options: IThemeEditorOptions;

		_themeModel: c1themeroller.model.ThemeModel;
		_btnApply: JQuery;
		_initialized: bool;
		
		_create() {
			$.Widget.prototype._create.apply(this, arguments);
		}

		_init() {
			this._initialized = false;

			// unbind
			var innerDiv = this.element.find("div:first");
			if (innerDiv.length) {
				ko.removeNode(innerDiv[0]);
			}

			// create content
			var html: string = "<div class='themeEditor clearfix'>" +
					this._getApplyArea() +
					this._getFontTab() +
					this._getCornerRadiusTab() +
					this._createHeaderToolbarTab() +
					this._createContentTab() +
					this._createClickableDefaultTab() +
					this._createClickableHoverTab() +
					this._createClickableActiveTab() +
					this._createHighlightTab() +
					this._createErrorTab() +
					this._createModalOverlaysTab() +
					this._createDropShadowsTab() +
				"</div";

			this.element.append($(html));	

			// bind
			this._themeModel = new c1themeroller.model.ThemeModel(this.options.themeValue);
			ko.applyBindings(this._themeModel, this.element[0]);

			// attach editors
			this.element.find(":text.color").colorpicker();
			//this.element.find(":text.texture").texturepicker();

			// expand/ collapse
			this.element.find(".tab-header")
				.click((e) => {
					var $header = $(e.target).closest(".tab-header"), // header
						$content = $header.next(); // content tab

					if ($content.is(":visible")) {
						$content.hide();

						$header
							.removeClass("state-active")
							.find(".arrow-icon")
								.removeClass("icon-triangle-1-s")
								.addClass("icon-triangle-1-e")
					} else {
						$content.show();

						$header
							.addClass("state-active")
							.find(".arrow-icon")
								.removeClass("icon-triangle-1-e")
								.addClass("icon-triangle-1-s")
					}
				})
				.hover((e) => {
					var $header = $(e.target).closest(".tab-header");
					$header.addClass("state-hover");
				}, (e) => {
					var $header = $(e.target).closest(".tab-header");
					$header.removeClass("state-hover");
				});

			var self = this;
			this._btnApply = this.element.find(".apply-area :button").click(e => {
				$(e.target).attr("disabled", "disabled");
				self._trigger("applyButtonClicked", null);
			});

			this._initialized = true;

			ko.bindingHandlers.styleTracked = {
				update: function (element, valueAccessor, allBindings) {
					var $Element, propertyName, styleName,
						valid, value;
					if (!self._initialized) {
						return;
					}
					$Element = $(element);
					propertyName = $Element.attr("propertyname");
					styleName = $Element.attr("stylename");
					value = ko.utils.unwrapObservable(valueAccessor());
					valid = c1themeroller.utilites.checkValid(propertyName, styleName, value);
					self._themePropChanged(valid, propertyName);
				}
			}
		}

		_themePropChanged(valid: bool, propertyName: string) {
			var value;
			if (this._initialized) { // view model values may change during ko binding (because undefined properties are filling with default values)
				if (!valid)
				{
					this._btnApply.attr("disabled", "disabled");
					value = "";
				}
				else
					value = this._themeModel.Serialize();
				this._trigger("serialize", null, { isValid: valid, propertyName: propertyName, newValue: value });
				if(valid)
					this._btnApply.removeAttr("disabled");
			}
		}

		_getApplyArea() {
			var result =

				"<div class='apply-area'>" +
					"<input type='button' value='" + c1themeroller.utilites.getResource("ThemeRoller.UI.ApplyChanges", "Apply changes") + "' disabled='disabled' />" +
				"</div>"

			return result;
		}

		_getFontTab() {
			var result =

			this._getOuterTab("font") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings", "Font Settings")) +
				this._getFontTabContent() +
			"</div>";

			return result;
		}

		_getCornerRadiusTab() {
			var result =

			this._getOuterTab("corner-radius") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.CornerRadius", "Corner Radius")) +
				this._getCornerRadiusTabContent() +
			"</div>";

			return result;
		}

		_createHeaderToolbarTab() {
			var result =

			this._getOuterTab("header-toolbar") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.HeaderAndToolbar", "Header/Toolbar"), "ui-widget-header") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("Header") +
				"</div>" +
			"</div>";

			return result;
		}

		_createContentTab() {
			var result =

			this._getOuterTab("contentTab") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.Content", "Content"), "ui-widget-content") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("Content") +
				"</div>" +
			"</div>";

			return result;
		}

		_createClickableDefaultTab() {
			return this._getOuterTab("clickable-default") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ClickableDefaultState", "Clickable: default state"), "ui-state-default") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("ClickableDefault") +
				"</div>" +
			"</div>";
		}

		_createClickableHoverTab() {
			return this._getOuterTab("clickable-hover") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ClickableHoverState", "Clickable: hover state"), "ui-state-hover") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("ClickableHover") +
				"</div>" +
			"</div>";
		}
		_createClickableActiveTab() {
			return this._getOuterTab("clickable-active") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ClickableActiveState", "Clickable: active state"), "ui-state-active") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("ClickableActive") +
				"</div>" +
			"</div>";
		}
		_createHighlightTab() {
			return this._getOuterTab("highlight") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.Highlight", "Highlight"), "ui-state-highlight") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("Highlight") +
				"</div>" +
			"</div>";
		}
		_createErrorTab() {
			return this._getOuterTab("error") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.Error", "Error"), "ui-state-error") +
				this._getTabContentWrapper() +
					this._getBackgroundBorderContent("Error") +
			"</div>";
		}
		_createModalOverlaysTab() {
			return this._getOuterTab("overlay") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.ModalScreenForOverlays", "Modal Screen for Overlays")) +
				this._getOverlayTabContent("Overlay") +
			"</div>";
		}
		_createDropShadowsTab() {
			return this._getOuterTab("dropshadows") +
				this._getHeader(c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows", "Drop Shadows")) +
				this._getDropShadowsTabContent("DropShadows") +
			"</div>";
		}

		_getOuterTab(name: string): string {
			return "<div class='tab tab-" + name + " clearfix'>";
		}

		_getHeader(caption: string, previewClass?: string): string {
			return "<div class='tab-header state-default corner-all'>" +
					"<span class=\"arrow-icon icon-triangle-1-e\">Collapse</span>" +

						(previewClass
							? "<div class='state-preview corner-all " + previewClass + "'>abc</div>"
							: "")
						 +

					"<a href='#'>" + caption + "</a>" +
				"</div>";
		}

		_getTabContentWrapper(): string {
			return this.options.collapseAllOnInit
				? "<div class='tab-content clearfix' style='display:none'>"
				: "<div class='tab-content clearfix'>";
		}

		_getFontTabContent(): string {
			return this._getTabContentWrapper() +
					"<div class='field-group clearfix'>" +
						"<label for='tbFontFamily'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings.Family","Family") + "</label>" +
						"<input id='tbFontFamily' class='font-family' data-bind='value: Font.Family, styleTracked: Font.Family' stylename='font-family' propertyname='Family' type='text' />" +
					"</div>" +

					"<div class='field-group clearfix'>" +
						"<label for='tbFontWeight'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings.Weight", "Weight") + "</label>" +
						"<select id='tbFontWeight' class='font-weight' data-bind='value: Font.Weight, styleTracked: Font.Weight, options: AvailableFontWeights' stylename='font-weight' propertyname='Weight'>" +
						"</select>" +
					"</div>" +

					"<div class='field-group clearfix'>" +
						"<label for='tbFontSize'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.FontSettings.Size", "Size") + "</label>" +
						"<input id='tbFontSize' class='font-size' data-bind='value: Font.Size, styleTracked: Font.Size' type='text' stylename='font-size' propertyname='Size' />" +
					"</div>" +
				"</div>";
		}

		_getCornerRadiusTabContent(): string {
			return this._getTabContentWrapper() +
					"<div class='field-group clearfix'>" +
						"<label for='tbCornerRadius'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Corners", "Corners") + ": </label>" +
						"<input id='tbCornerRadius' class='cornerRadius' type='text' data-bind='value: Corner.Radius, styleTracked: Corner.Radius' stylename='border-radius' propertyname='Radius' />" +
					"</div>" +
				"</div>";
		}

		_getOverlayTabContent(topDataName: string): string {
			return this._getTabContentWrapper() +
				this._getBackgroundFieldGroupContent(topDataName) +
				"<div class='field-group clearfix'>" +
					"<label class='float-left' for='tb" + topDataName + "Opacity'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.ModalScreenForOverlays.OverlayOpacity", "Overlay Opacity") + ": </label>" +
					"<input id='tb" + topDataName + "Opacity' class='opacity' type='text' data-bind='value: " + topDataName + ".Opacity, styleTracked: " + topDataName + ".Opacity' stylename='opacity' propertyname='Overlay Opacity' />" +
					"<span class='opacity-per'>%</span>" +
				"</div>" +
			"</div>";
		}

		_getDropShadowsTabContent(topDataName: string): string {
			return this._getTabContentWrapper() +
				this._getBackgroundFieldGroupContent(topDataName) +

				"<div class='dropshadows-additional-props'>" +
				"<div class='field-group clearfix'>" +
					"<label class='float-left' for='tb" + topDataName + "Shadow'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.ShadowOpacity", "Shadow Opacity") + ": </label>" +
					"<input id='tb" + topDataName + "Shadow' class='opacity' type='text' data-bind='value: " + topDataName + ".Opacity, styleTracked: " + topDataName + ".Opacity' stylename='opacity' propertyname='Shadow Opacity' />" +
					"<span class='opacity-per'>%</span>" +
				"</div>" +

				"<div class='field-group clearfix'>" +
					"<label class='float-left' for='tb" + topDataName + "Thickness'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.ShadowThickness", "Shadow Thickness") + ": </label>" +
					"<input id='tb" + topDataName + "Thickness' class='offset' type='text' data-bind='value: " + topDataName + ".Thickness, styleTracked: " + topDataName + ".Thickness' stylename='padding-top' propertyname='Shadow Thickness' />" +
				"</div>" +

				"<div class='field-group clearfix'>" +
					"<label class='float-left' for='tb" + topDataName + "OffsetTop'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.TopOffset", "Top Offset") + ": </label>" +
					"<input id='tb" + topDataName + "OffsetTop' class='offset' type='text' data-bind='value: " + topDataName + ".OffsetTop, styleTracked: " + topDataName + ".OffsetTop' stylename='padding-top' propertyname='Top Offset' />" +
				"</div>" +

				"<div class='field-group clearfix'>" +
					"<label class='float-left' for='tb" + topDataName + "OffsetLeft'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.DropShadows.LeftOffset", "Left Offset") + ": </label>" +
					"<input id='tb" + topDataName + "OffsetLeft' class='offset' type='text' data-bind='value: " + topDataName + ".OffsetLeft, styleTracked: " + topDataName + ".OffsetLeft' stylename='padding-top' propertyname='Left Offset' />" +
				"</div>" +

				"<div class='field-group clearfix'>" +
					"<label class='float-left' for='tb" + topDataName + "CornerRadius'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Corners", "Corners") + ": </label>" +
					"<input id='tb" + topDataName + "CornerRadius' class='cornerRadius' type='text' data-bind='value: " + topDataName + ".Corner.Radius, styleTracked: " + topDataName + ".Corner.Radius' stylename='border-radius' propertyname='Radius' />" +
				"</div>" +
				"</div>" +

			"</div>";
		}

		_getBackgroundBorderContent(topDataName: string): string {
			var v = {
				"borderColor": { id: "tb" + topDataName + "BorderColor", data: topDataName + ".BorderColor.Value" },
				"foreColor": { id: "tb" + topDataName + "ForeColor", data: topDataName + ".ForeColor.Value" },
				"iconColor": { id: "tb" + topDataName + "IconColor", data: topDataName + ".IconColor.Value" }
			};

			return this._getBackgroundFieldGroupContent(topDataName) +

					"<div class='field-group field-group-border float-left clearfix'>" +
						"<label for='" + v.borderColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Border", "Border") + "</label>" +
						"<div class='color-container'>" +
							"<input id='" + v.borderColor.id + "' class='color' type='text' data-bind='value: " + v.borderColor.data + ", styleTracked: " + v.borderColor.data + "' stylename='background-color' propertyname='Color' />" +
						"</div>" +
					"</div>" +

					"<div class='field-group float-left clearfix'>" +
						"<label for='" + v.foreColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Text", "Text") + "</label>" +
						"<div class='color-container'>" +
							"<input id='" + v.foreColor.id + "' class='color' type='text' data-bind='value: " + v.foreColor.data + ", styleTracked: " + v.foreColor.data + "' stylename='background-color' propertyname='Color' />" +
						"</div>" +
					"</div>" +

					"<div class='field-group float-left clearfix'>" +
						"<label for='" + v.iconColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.Icon", "Icon") + "</label>" +
						"<div class='color-container'>" +
							"<input id='" + v.iconColor.id + "' class='color' type='text' data-bind='value: " + v.iconColor.data + ", styleTracked: " + v.iconColor.data + "' stylename='background-color' propertyname='Color' />" +
						"</div>" +
					"</div>";
		}

		_getBackgroundFieldGroupContent(topDataName): string {
			var v = {
				"bgColor": { id: "tb" + topDataName + "BgColor", data: topDataName + ".Background.Color.Value" },
				"bgTexture": { id: "tb" + topDataName + "BgTexture", data: topDataName + ".Background.Texture" },
				"bgOpacity": { id: "tb" + topDataName + "BgOpacity", data: topDataName + ".Background.Opacity" },
			};

			return "<div class='field-group field-group-background clearfix'>" +
				"<label for='" + v.bgColor.id + "'>" + c1themeroller.utilites.getResource("ThemeRoller.UI.BackgroundColorAndTexture", "Background color &amp; texture") + "</label>" +

				"<div class='color-container'>" +
					"<input id='" + v.bgColor.id + "' class='color' type='text' data-bind='value: " + v.bgColor.data + ", styleTracked: " + v.bgColor.data + "' stylename='background-color' propertyname='Color' />" +
				"</div>" +
				"<div class='texture-container'>" +
					// "<input id='" + v.bgTexture.id + "' class='texture' type='text' data-bind='value: " + v.bgTexture.data + "' />" +
					"<select id='" + v.bgTexture.id + "' class='texture' data-bind='value: " + v.bgTexture.data + ", options: AvailableTextures, styleTracked: " + v.bgTexture.data + "'>" +
					"</select>" +
				"</div>" +
				"<input id='" + v.bgOpacity.id + "' type='text' class='opacity' data-bind='value: " + v.bgOpacity.data + ", styleTracked: " + v.bgOpacity.data + "' stylename='opacity' propertyname='Opacity' />" +
				"<span class ='opacity-per'>%</span>" +
			"</div>";
		}
	}

	export interface IThemeEditorOptions {
		collapseAllOnInit: bool;
		themeValue: string; // serialized theme values
		serialize: any; // event, args: { newValue: string }
	}

	themeeditor.prototype.widgetEventPrefix = "themeeditor";

	class themeeditor_options implements IThemeEditorOptions {
		collapseAllOnInit: bool = true;
		themeValue: string = undefined;
		serialize: any = null;
		applyButtonClicked: any = null;
	};

	themeeditor.prototype.options = new themeeditor_options();

	$.widget("c1themeroller.themeeditor", themeeditor.prototype);
}

interface JQuery {
	themeeditor: any;
}