var __extends = this.__extends || function (d, b) {
	function __() { this.constructor = d; }
	__.prototype = b.prototype;
	d.prototype = new __();
};
var c1themeroller;
(function (c1themeroller) {
	(function (colorpicker) {

		var $ = jQuery;
		var colorpicker = (function (_super) {
			__extends(colorpicker, _super);
			function colorpicker() {
				_super.apply(this, arguments);

			}
			colorpicker.prototype._create = function () {
				$.Widget.prototype._create.apply(this, arguments);
				this._picker = null;
				this._outerDiv = this.element.closest("div.color-container");
				this._value = undefined;
				($("<div></div>")).farbtastic(this.element).remove();
				var self = this;
				this.element.click(function () {
					if (self._picker) {
						self._removePicker();
					} else {
						self._addPicker();
					}
				}).focusout(function () {
					self._removePicker();
				});
			};
			colorpicker.prototype._destroy = function () {
				this._removePicker();
				this.element.unwrap();
				this._outerDiv = null;
				this._picker = null;
			};
			colorpicker.prototype._init = function () {
				$.Widget.prototype._init.apply(this, arguments);
			};
			colorpicker.prototype._addPicker = function () {
				this._picker = $("<div class=\"picker\" style=\"position:absolute\"></div>").appendTo(this._outerDiv).position({
					of: this.element,
					my: "left top",
					at: "left bottom"
				}).farbtastic(this.element);
				this._value = this.element.val();
			};
			colorpicker.prototype._removePicker = function () {
				if (this._picker) {
					this._picker.remove();
				}
				this._picker = null;
				if (this._value !== this.element.val()) {
					this.element.change();
				}
			};
			return colorpicker;
		})(c1themeroller.JQueryUIWidget);
		colorpicker.colorpicker = colorpicker;
		colorpicker.prototype.widgetEventPrefix = "colorpicker";
		var colorpicker_options = (function () {
			function colorpicker_options() { }
			return colorpicker_options;
		})();
		;
		colorpicker.prototype.options = new colorpicker_options();
		$.widget("c1themeroller.colorpicker", colorpicker.prototype);
	})(c1themeroller.colorpicker || (c1themeroller.colorpicker = {}));
	var colorpicker = c1themeroller.colorpicker;
})(c1themeroller || (c1themeroller = {}));
