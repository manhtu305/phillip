var __extends = this.__extends || function (d, b) {
	function __() { this.constructor = d; }
	__.prototype = b.prototype;
	d.prototype = new __();
};
var c1themeroller;
(function (c1themeroller) {
	(function (model) {
		var ThemeModel = (function () {
			function ThemeModel(value) {
				this.AvailableFontWeights = [
					"normal",
					"bold"
				];
				this.AvailableTextures = [
					"flat",
					"glass",
					"highlight_soft",
					"highlight_hard",
					"inset_soft",
					"inset_hard",
					"diagonals_small",
					"diagonals_medium",
					"diagonals_thick",
					"dots_small",
					"dots_medium",
					"white_lines",
					"gloss_wave",
					"diamond",
					"loop",
					"carbon_fiber",
					"diagonal_maze",
					"diamond_ripple",
					"hexagon",
					"layered_circles",
					"3D_boxes",
					"glow_ball",
					"spotlight",
					"fine_grain"
				];
				this.Font = new BaseFontSettings("Default");
				this.Corner = new BaseCornerRadiusSettings("");
				this.Header = new BaseColorSettings("Header");
				this.Content = new BaseColorSettings("Content");
				this.ClickableDefault = new BaseColorSettings("Default");
				this.ClickableHover = new BaseColorSettings("Hover");
				this.ClickableActive = new BaseColorSettings("Active");
				this.Highlight = new BaseColorSettings("Highlight");
				this.Error = new BaseColorSettings("Error");
				this.Overlay = new BaseOverlaySettings("Overlay");
				this.DropShadows = new BaseDropShadowsSettings("Shadow");
				if (value) {
					this.Deserialize(value);
				}
			}
			ThemeModel.prototype.Serialize = function () {
				var value = c1themeroller.utilites.stringFormat("{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&{8}&{9}&{10}", this.Font.Serialize(), this.Corner.Serialize(), this.Header.Serialize(), this.Content.Serialize(), this.ClickableDefault.Serialize(), this.ClickableHover.Serialize(), this.ClickableActive.Serialize(), this.Highlight.Serialize(), this.Error.Serialize(), this.Overlay.Serialize(), this.DropShadows.Serialize());
				return value;
			};
			ThemeModel.prototype.Deserialize = function (value) {
				this.Font.Deserialize(value);
				this.Corner.Deserialize(value);
				this.Header.Deserialize(value);
				this.Content.Deserialize(value);
				this.ClickableDefault.Deserialize(value);
				this.ClickableHover.Deserialize(value);
				this.ClickableActive.Deserialize(value);
				this.Highlight.Deserialize(value);
				this.Error.Deserialize(value);
				this.Overlay.Deserialize(value);
				this.DropShadows.Deserialize(value);
			};
			return ThemeModel;
		})();
		model.ThemeModel = ThemeModel;
		var Serializer = (function () {
			function Serializer(postfix) {
				this._postfix = postfix;
			}
			Serializer.prototype.Serialize = function () {
				var result = this._SerializeInternal();
				result = result.replace(/\*=/gm, this._postfix + "=");
				return result;
			};
			Serializer.prototype.Deserialize = function (value) {
				this._DeserializeInternal(value);
			};
			Serializer.prototype._SerializeInternal = function () {
				return "";
			};
			Serializer.prototype._DeserializeInternal = function (value) {
			};
			Serializer.prototype._EncodeComponent = function (value) {
				return encodeURIComponent(value + "");
			};
			Serializer.prototype._DecodeComponent = function (value) {
				return decodeURIComponent(value);
			};
			Serializer.prototype._Extract = function (value, propShortName, callback) {
				if (value && propShortName && callback) {
					var match = new RegExp(propShortName + this._postfix + "=([^&]*)").exec(value);
					if (match) {
						callback(match[1]);
					}
				}
			};
			Serializer.prototype._Reset = function () {
			};
			return Serializer;
		})();
		model.Serializer = Serializer;
		var ColorHex = (function (_super) {
			__extends(ColorHex, _super);
			function ColorHex(postfix, name, value) {
				_super.call(this, postfix);
				this._name = name;
				this.Value = ko.observable(c1themeroller.utilites.ensureHEXColor(value));
			}
			ColorHex.prototype._SerializeInternal = function () {
				return c1themeroller.utilites.stringFormat("{0}*={1}", this._name, this._EncodeComponent(this.Value()));
			};
			ColorHex.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this._Extract(value, this._name, function (newValue) {
					self.Value(c1themeroller.utilites.ensureHEXColor(self._DecodeComponent(newValue)));
				});
			};
			ColorHex.prototype._Reset = function () {
				this.Value(undefined);
			};
			return ColorHex;
		})(Serializer);
		model.ColorHex = ColorHex;
		var BaseFontSettings = (function (_super) {
			__extends(BaseFontSettings, _super);
			function BaseFontSettings(postfix, family, weight, size) {
				_super.call(this, postfix);
				this.Family = ko.observable(family);
				this.Weight = ko.observable(weight);
				this.Size = ko.observable(size);
			}
			BaseFontSettings.prototype._SerializeInternal = function () {
				return c1themeroller.utilites.stringFormat("ff*={0}&fw*={1}&fs*={2}", this._EncodeComponent(this.Family()), this._EncodeComponent(this.Weight()), this._EncodeComponent(this.Size()));
			};
			BaseFontSettings.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this._Extract(value, "ff", function (newValue) {
					self.Family(self._DecodeComponent(newValue));
				});
				this._Extract(value, "fw", function (newValue) {
					self.Weight(self._DecodeComponent(newValue));
				});
				this._Extract(value, "fs", function (newValue) {
					self.Size(self._DecodeComponent(newValue));
				});
			};
			BaseFontSettings.prototype._Reset = function () {
				this.Family(undefined);
				this.Weight(undefined);
				this.Size(undefined);
			};
			return BaseFontSettings;
		})(Serializer);
		model.BaseFontSettings = BaseFontSettings;
		var BaseCornerRadiusSettings = (function (_super) {
			__extends(BaseCornerRadiusSettings, _super);
			function BaseCornerRadiusSettings(postfix, radius) {
				_super.call(this, postfix);
				this.Radius = ko.observable(radius);
			}
			BaseCornerRadiusSettings.prototype._SerializeInternal = function () {
				return "cornerRadius*=" + this._EncodeComponent(this.Radius());
			};
			BaseCornerRadiusSettings.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this._Extract(value, "cornerRadius", function (newValue) {
					self.Radius(self._DecodeComponent(newValue));
				});
			};
			BaseCornerRadiusSettings.prototype._Reset = function () {
				this.Radius(undefined);
			};
			return BaseCornerRadiusSettings;
		})(Serializer);
		model.BaseCornerRadiusSettings = BaseCornerRadiusSettings;
		var BaseBackgroundSettings = (function (_super) {
			__extends(BaseBackgroundSettings, _super);
			function BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity) {
				_super.call(this, postfix);
				this.Color = new ColorHex(postfix, "bgColor", bgColor);
				this.Texture = ko.observable(bgTexture);
				this.Opacity = ko.observable(bgOpacity);
			}
			BaseBackgroundSettings.prototype._SerializeInternal = function () {
				return c1themeroller.utilites.stringFormat("{0}&bgTexture*={1}&bgImgOpacity*={2}", this.Color.Serialize(), this._EncodeComponent(this.Texture()), this._EncodeComponent(this.Opacity()));
			};
			BaseBackgroundSettings.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this.Color._DeserializeInternal(value);
				this._Extract(value, "bgTexture", function (newValue) {
					self.Texture(self._NormalizeTextureValue(self._DecodeComponent(newValue)));
				});
				this._Extract(value, "bgImgOpacity", function (newValue) {
					self.Opacity(parseInt(self._DecodeComponent(newValue)));
				});
			};
			BaseBackgroundSettings.prototype._Reset = function () {
				this.Color._Reset();
				this.Texture(undefined);
				this.Opacity(undefined);
			};
			BaseBackgroundSettings.prototype._NormalizeTextureValue = function (value) {
				if (value) {
					value = value.replace(/^\d+_/, "");
					value = value.replace(/\..{3}$/, "");
				}
				return value;
			};
			return BaseBackgroundSettings;
		})(Serializer);
		model.BaseBackgroundSettings = BaseBackgroundSettings;
		var BaseColorSettings = (function (_super) {
			__extends(BaseColorSettings, _super);
			function BaseColorSettings(postfix, bgColor, bgTexture, bgOpacity, brdColor, foreColor, iconColor) {
				_super.call(this, postfix);
				this.Background = new BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity);
				this.BorderColor = new ColorHex(postfix, "borderColor", brdColor);
				this.ForeColor = new ColorHex(postfix, "fc", foreColor);
				this.IconColor = new ColorHex(postfix, "iconColor", iconColor);
			}
			BaseColorSettings.prototype._SerializeInternal = function () {
				return c1themeroller.utilites.stringFormat("{0}&{1}&{2}&{3}", this.Background.Serialize(), this.BorderColor.Serialize(), this.ForeColor.Serialize(), this.IconColor.Serialize());
			};
			BaseColorSettings.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this.Background._DeserializeInternal(value);
				this.BorderColor._DeserializeInternal(value);
				this.ForeColor._DeserializeInternal(value);
				this.IconColor._DeserializeInternal(value);
			};
			BaseColorSettings.prototype._Reset = function () {
				this.Background._Reset();
				this.BorderColor._Reset();
				this.ForeColor._Reset();
				this.IconColor._Reset();
			};
			return BaseColorSettings;
		})(Serializer);
		model.BaseColorSettings = BaseColorSettings;
		var BaseOverlaySettings = (function (_super) {
			__extends(BaseOverlaySettings, _super);
			function BaseOverlaySettings(postfix, bgColor, bgTexture, bgOpacity, opacity) {
				_super.call(this, postfix);
				this.Background = new BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity);
				this.Opacity = ko.observable(opacity);
			}
			BaseOverlaySettings.prototype._SerializeInternal = function () {
				return c1themeroller.utilites.stringFormat("{0}&opacity*={1}", this.Background.Serialize(), this._EncodeComponent(this.Opacity()));
			};
			BaseOverlaySettings.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this.Background._DeserializeInternal(value);
				this._Extract(value, "opacity", function (newValue) {
					self.Opacity(parseInt(self._DecodeComponent(newValue), 10));
				});
			};
			BaseOverlaySettings.prototype._Reset = function () {
				this.Background._Reset();
				this.Opacity(undefined);
			};
			return BaseOverlaySettings;
		})(Serializer);
		model.BaseOverlaySettings = BaseOverlaySettings;
		var BaseDropShadowsSettings = (function (_super) {
			__extends(BaseDropShadowsSettings, _super);
			function BaseDropShadowsSettings(postfix, bgColor, bgTexture, bgOpacity, opacity, thickness, offsetTop, offsetLeft, cornerRadius) {
				_super.call(this, postfix);
				this.Background = new BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity);
				this.Opacity = ko.observable(opacity);
				this.Thickness = ko.observable(thickness);
				this.OffsetTop = ko.observable(offsetTop);
				this.OffsetLeft = ko.observable(offsetLeft);
				this.Corner = new BaseCornerRadiusSettings(postfix, cornerRadius);
			}
			BaseDropShadowsSettings.prototype._SerializeInternal = function () {
				return c1themeroller.utilites.stringFormat("{0}&opacity*={1}&thickness*={2}&offsetTop*={3}&offsetLeft*={4}&{5}", this.Background.Serialize(), this._EncodeComponent(this.Opacity()), this._EncodeComponent(this.Thickness()), this._EncodeComponent(this.OffsetTop()), this._EncodeComponent(this.OffsetLeft()), this.Corner.Serialize());
			};
			BaseDropShadowsSettings.prototype._DeserializeInternal = function (value) {
				var self = this;
				this._Reset();
				this.Background._DeserializeInternal(value);
				this._Extract(value, "opacity", function (newValue) {
					self.Opacity(parseInt(self._DecodeComponent(newValue), 10));
				});
				this._Extract(value, "thickness", function (newValue) {
					self.Thickness(self._DecodeComponent(newValue));
				});
				this._Extract(value, "offsetTop", function (newValue) {
					self.OffsetTop(self._DecodeComponent(newValue));
				});
				this._Extract(value, "offsetLeft", function (newValue) {
					self.OffsetLeft(self._DecodeComponent(newValue));
				});
				this.Corner._DeserializeInternal(value);
			};
			BaseDropShadowsSettings.prototype._Reset = function () {
				this.Background._Reset();
				this.Opacity(undefined);
				this.Thickness(undefined);
				this.OffsetTop(undefined);
				this.OffsetLeft(undefined);
				this.Corner._Reset();
			};
			return BaseDropShadowsSettings;
		})(Serializer);
		model.BaseDropShadowsSettings = BaseDropShadowsSettings;
	})(c1themeroller.model || (c1themeroller.model = {}));
	var model = c1themeroller.model;
})(c1themeroller || (c1themeroller = {}));
