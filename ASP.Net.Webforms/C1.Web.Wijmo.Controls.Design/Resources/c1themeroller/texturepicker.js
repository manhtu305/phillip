var __extends = this.__extends || function (d, b) {
	function __() { this.constructor = d; }
	__.prototype = b.prototype;
	d.prototype = new __();
};
var c1themeroller;
(function (c1themeroller) {
	(function (texturepicker) {
		var $ = jQuery;
		texturepicker.textureData = {
			flat: {
				height: 100,
				width: 40
			},
			glass: {
				height: 400,
				width: 1
			},
			highlight_soft: {
				height: 100,
				width: 1
			},
			highlight_hard: {
				height: 100,
				width: 1
			},
			inset_soft: {
				height: 100,
				width: 1
			},
			inset_hard: {
				height: 100,
				width: 1
			},
			diagonals_small: {
				height: 40,
				width: 40
			},
			diagonals_medium: {
				height: 40,
				width: 40
			},
			diagonals_thick: {
				height: 40,
				width: 40
			},
			dots_small: {
				height: 2,
				width: 2
			},
			dots_medium: {
				height: 4,
				width: 4
			},
			white_lines: {
				height: 100,
				width: 40
			},
			gloss_wave: {
				height: 100,
				width: 500
			},
			diamond: {
				height: 8,
				width: 10
			},
			loop: {
				height: 21,
				width: 21
			},
			carbon_fiber: {
				height: 9,
				width: 8
			},
			diagonal_maze: {
				height: 10,
				width: 10
			},
			diamond_ripple: {
				height: 22,
				width: 22
			},
			hexagon: {
				height: 10,
				width: 12
			},
			layered_circles: {
				height: 13,
				width: 13
			},
			"3D_boxes": {
				height: 10,
				width: 12
			},
			glow_ball: {
				height: 16,
				width: 16
			},
			spotlight: {
				height: 16,
				width: 16
			},
			fine_grain: {
				height: 60,
				width: 60
			}
		};
		texturepicker.TextureUrl = "http://download.jqueryui.com/themeroller/images/ui-bg_{0}_100_555_{1}x{2}.png";
		var texturepicker = (function (_super) {
			__extends(texturepicker, _super);
			function texturepicker() {
				_super.apply(this, arguments);

			}
			texturepicker.prototype._create = function () {
				var texture = this.element.val() || "flat", self = this;
				this.element.hide().after(this._$pickerDiv = $("<div></div>"));
				this._$pickerDiv.addClass("texture-picker").append(this._createTexturesList()).css({
					"background-image": "url('" + this._getTextureUrl(texture, c1themeroller.texturepicker.textureData[texture].height, c1themeroller.texturepicker.textureData[texture].width) + "')"
				}).click(function (e) {
					self._$pickerDiv.find("ul").position({
						of: self._$pickerDiv,
						my: "left top",
						at: "left bottom"
					}).attr({
						tabIndex: 0
					}).show().focus().focusout(function (e) {
						window.setTimeout(function () {
							$(e.target).hide();
						}, 100);
					});
				});
				$.Widget.prototype._create.apply(this, arguments);
			};
			texturepicker.prototype._destroy = function () {
			};
			texturepicker.prototype._init = function () {
				$.Widget.prototype._init.apply(this, arguments);
			};
			texturepicker.prototype._createTexturesList = function () {
				var $ul = $("<ul />"), self = this;
				$.each(c1themeroller.texturepicker.textureData, function (key, value) {
					var url = self._getTextureUrl(key, value.height, value.width), $li, $a;
					$a = $("<a />").attr({
						title: key,
						href: "#"
					}).text(key);
					$li = $("<li/>").addClass(key).css({
						"background-image": "url('" + url + "')"
					}).bind("click", $li, $.proxy(self._onItemClicked, self));
					$li.append($a);
					$ul.append($li);
				});
				$ul.css("display", "none");
				return $ul;
			};
			texturepicker.prototype._onItemClicked = function (e) {
			};
			texturepicker.prototype._getTextureUrl = function (name, height, width) {
				return c1themeroller.utilites.stringFormat(c1themeroller.texturepicker.TextureUrl, name.replace("_", "-"), width, height);
			};
			return texturepicker;
		})(c1themeroller.JQueryUIWidget);
		texturepicker.texturepicker = texturepicker;
		texturepicker.prototype.widgetEventPrefix = "texturepicker";
		var texturepicker_options = (function () {
			function texturepicker_options() { }
			return texturepicker_options;
		})();
		;
		texturepicker.prototype.options = new texturepicker_options();
		$.widget("c1themeroller.texturepicker", texturepicker.prototype);
	})(c1themeroller.texturepicker || (c1themeroller.texturepicker = {}));
	var texturepicker = c1themeroller.texturepicker;
})(c1themeroller || (c1themeroller = {}));
