/// <reference path="utilites.ts"/>
declare var ko;

module c1themeroller.model {


	export class ThemeModel {

		constructor(value?: string) {
			if (value) {
				this.Deserialize(value);
			}
		}

		public AvailableFontWeights = ["normal", "bold"];
		public AvailableTextures = [
			"flat",
			"glass",
			"highlight_soft",
			"highlight_hard",
			"inset_soft",
			"inset_hard",
			"diagonals_small",
			"diagonals_medium",
			"diagonals_thick",
			"dots_small",
			"dots_medium",
			"white_lines",
			"gloss_wave",
			"diamond",
			"loop",
			"carbon_fiber",
			"diagonal_maze",
			"diamond_ripple",
			"hexagon",
			"layered_circles",
			"3D_boxes",
			"glow_ball",
			"spotlight",
			"fine_grain"
		];

		public Font = new BaseFontSettings("Default");
		public Corner = new BaseCornerRadiusSettings("");
		public Header = new BaseColorSettings("Header");
		public Content = new BaseColorSettings("Content");
		public ClickableDefault = new BaseColorSettings("Default");
		public ClickableHover = new BaseColorSettings("Hover");
		public ClickableActive = new BaseColorSettings("Active");
		public Highlight = new BaseColorSettings("Highlight");
		public Error = new BaseColorSettings("Error");
		public Overlay = new BaseOverlaySettings("Overlay");
		public DropShadows = new BaseDropShadowsSettings("Shadow");

		public Serialize(): string {
			var value = c1themeroller.utilites.stringFormat("{0}&{1}&{2}&{3}&{4}&{5}&{6}&{7}&{8}&{9}&{10}",
				this.Font.Serialize(),
				this.Corner.Serialize(),
				this.Header.Serialize(),
				this.Content.Serialize(),
				this.ClickableDefault.Serialize(),
				this.ClickableHover.Serialize(),
				this.ClickableActive.Serialize(),
				this.Highlight.Serialize(),
				this.Error.Serialize(),
				this.Overlay.Serialize(),
				this.DropShadows.Serialize());

			return value;
		}

		public Deserialize(value: string) {
			this.Font.Deserialize(value);
			this.Corner.Deserialize(value);
			this.Header.Deserialize(value);
			this.Content.Deserialize(value);
			this.ClickableDefault.Deserialize(value);
			this.ClickableHover.Deserialize(value);
			this.ClickableActive.Deserialize(value);
			this.Highlight.Deserialize(value);
			this.Error.Deserialize(value);
			this.Overlay.Deserialize(value);
			this.DropShadows.Deserialize(value);
		}
	}

	export class Serializer {
		_postfix: string;

		constructor(postfix: string) {
			this._postfix = postfix;
		}

		public Serialize(): string {
			var result: string = this._SerializeInternal();

			result = result.replace(/\*=/gm, this._postfix + "=");

			return result;
		}

		public Deserialize(value: string) {
			this._DeserializeInternal(value);
		}

		_SerializeInternal(): string {
			return "";
		}

		_DeserializeInternal(value: string) {
		}

		_EncodeComponent(value: any): string {
			return encodeURIComponent(value + "");
		}

		_DecodeComponent(value: string) {
			return decodeURIComponent(value);
		}

		_Extract(value: string, propShortName: string, callback: (newValue: string) => void) {
			if (value && propShortName && callback) {
				var match = new RegExp(propShortName + this._postfix + "=([^&]*)").exec(value);
				if (match) {
					callback(match[1]);
				}
			}
		}

		_Reset() {
		}
	}

	export class ColorHex extends Serializer {
		public Value: ObservableString;
		private _name: string;

		constructor(postfix: string, name: string, value?: string) {
			super(postfix);

			this._name = name;

			this.Value = ko.observable(c1themeroller.utilites.ensureHEXColor(value));
		}

		_SerializeInternal(): string {
			return c1themeroller.utilites.stringFormat("{0}*={1}",
				this._name,
				this._EncodeComponent(this.Value())
				);
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this._Extract(value, this._name, function (newValue: string) {
				self.Value(c1themeroller.utilites.ensureHEXColor(self._DecodeComponent(newValue)));
			});
		}

		_Reset() {
			this.Value(undefined);
		}
	}

	export class BaseFontSettings extends Serializer {
		public Family: ObservableString;
		public Weight: ObservableString;
		public Size: ObservableString;

		constructor(postfix: string, family?: string, weight?: string, size?: string) {
			super(postfix);

			this.Family = ko.observable(family);
			this.Weight = ko.observable(weight);
			this.Size = ko.observable(size);
		}

		_SerializeInternal(): string {
			return c1themeroller.utilites.stringFormat("ff*={0}&fw*={1}&fs*={2}",
				this._EncodeComponent(this.Family()),
				this._EncodeComponent(this.Weight()),
				this._EncodeComponent(this.Size())
				);
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this._Extract(value, "ff", function (newValue: string) {
				self.Family(self._DecodeComponent(newValue));
			});

			this._Extract(value, "fw", function (newValue: string) {
				self.Weight(self._DecodeComponent(newValue));
			});

			this._Extract(value, "fs", function (newValue: string) {
				self.Size(self._DecodeComponent(newValue));
			});
		}

		_Reset() {
			this.Family(undefined);
			this.Weight(undefined);
			this.Size(undefined);
		}
	}

	export class BaseCornerRadiusSettings extends Serializer {
		public Radius: ObservableString;

		constructor(postfix: string, radius?: string) {
			super(postfix);

			this.Radius = ko.observable(radius);
		}

		_SerializeInternal(): string {
			return "cornerRadius*=" + this._EncodeComponent(this.Radius());
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this._Extract(value, "cornerRadius", function (newValue: string) {
				self.Radius(self._DecodeComponent(newValue));
			});
		}

		_Reset() {
			this.Radius(undefined);
		}
	}

	export class BaseBackgroundSettings extends Serializer {
		public Color: ColorHex;
		public Texture: ObservableString;
		public Opacity: ObservableNumber;

		constructor(postfix: string, bgColor?: string, bgTexture?: string, bgOpacity?: number) {
			super(postfix);

			this.Color = new ColorHex(postfix, "bgColor", bgColor);
			this.Texture = ko.observable(bgTexture);
			this.Opacity = ko.observable(bgOpacity);
		}

		_SerializeInternal(): string {
			return c1themeroller.utilites.stringFormat("{0}&bgTexture*={1}&bgImgOpacity*={2}",
				this.Color.Serialize(),
				this._EncodeComponent(this.Texture()),
				this._EncodeComponent(this.Opacity())
				);
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this.Color._DeserializeInternal(value);

			this._Extract(value, "bgTexture", function (newValue: string) {
				self.Texture(self._NormalizeTextureValue(self._DecodeComponent(newValue)));
			});

			this._Extract(value, "bgImgOpacity", function (newValue: string) {
				self.Opacity(parseInt(self._DecodeComponent(newValue)));
			});
		}

		_Reset() {
			this.Color._Reset();
			this.Texture(undefined);
			this.Opacity(undefined);
		}

		// 03_highlight_soft.png -> highlight_soft
		_NormalizeTextureValue(value) {
			if (value) {
				value = value.replace(/^\d+_/, "");
				value = value.replace(/\..{3}$/, "");
			}

			return value;
		}
	}

	export class BaseColorSettings extends Serializer {
		public Background: BaseBackgroundSettings;
		public BorderColor: ColorHex;
		public ForeColor: ColorHex;
		public IconColor: ColorHex;

		constructor(postfix: string, bgColor?: string, bgTexture?: string, bgOpacity?: number, brdColor?: string, foreColor?: string, iconColor?: string) {
			super(postfix);

			this.Background = new BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity);
			this.BorderColor = new ColorHex(postfix, "borderColor", brdColor);
			this.ForeColor = new ColorHex(postfix, "fc", foreColor);
			this.IconColor = new ColorHex(postfix, "iconColor", iconColor);
		}

		_SerializeInternal(): string {
			return c1themeroller.utilites.stringFormat("{0}&{1}&{2}&{3}",
				this.Background.Serialize(),
				this.BorderColor.Serialize(),
				this.ForeColor.Serialize(),
				this.IconColor.Serialize()
				);
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this.Background._DeserializeInternal(value);
			this.BorderColor._DeserializeInternal(value);
			this.ForeColor._DeserializeInternal(value);
			this.IconColor._DeserializeInternal(value);
		}

		_Reset() {
			this.Background._Reset();
			this.BorderColor._Reset();
			this.ForeColor._Reset();
			this.IconColor._Reset();
		}
	}

	export class BaseOverlaySettings extends Serializer {
		public Background: BaseBackgroundSettings;
		public Opacity: ObservableNumber;

		constructor(postfix: string, bgColor?: string, bgTexture?: string, bgOpacity?: number, opacity?: number) {
			super(postfix);

			this.Background = new BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity);
			this.Opacity = ko.observable(opacity);
		}

		_SerializeInternal(): string {
			return c1themeroller.utilites.stringFormat("{0}&opacity*={1}",
				this.Background.Serialize(),
				this._EncodeComponent(this.Opacity())
				);
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this.Background._DeserializeInternal(value);

			this._Extract(value, "opacity", function (newValue: string) {
				self.Opacity(parseInt(self._DecodeComponent(newValue), 10));
			});
		}

		_Reset() {
			this.Background._Reset();
			this.Opacity(undefined);
		}
	}

	export class BaseDropShadowsSettings extends Serializer {
		public Background: BaseBackgroundSettings;
		public Opacity: ObservableNumber;
		public Thickness: ObservableString;
		public OffsetTop: ObservableString;
		public OffsetLeft: ObservableString;
		public Corner: BaseCornerRadiusSettings;

		constructor(postfix: string, bgColor?: string, bgTexture?: string, bgOpacity?: number, opacity?: number, thickness?: string, offsetTop?: string, offsetLeft?: string, cornerRadius?: string) {
			super(postfix);

			this.Background = new BaseBackgroundSettings(postfix, bgColor, bgTexture, bgOpacity);
			this.Opacity = ko.observable(opacity);
			this.Thickness = ko.observable(thickness);
			this.OffsetTop = ko.observable(offsetTop);
			this.OffsetLeft = ko.observable(offsetLeft);
			this.Corner = new BaseCornerRadiusSettings(postfix, cornerRadius);
		}

		_SerializeInternal(): string {
			return c1themeroller.utilites.stringFormat("{0}&opacity*={1}&thickness*={2}&offsetTop*={3}&offsetLeft*={4}&{5}",
				this.Background.Serialize(),
				this._EncodeComponent(this.Opacity()),
				this._EncodeComponent(this.Thickness()),
				this._EncodeComponent(this.OffsetTop()),
				this._EncodeComponent(this.OffsetLeft()),
				this.Corner.Serialize()
				);
		}

		_DeserializeInternal(value: string) {
			var self = this;

			this._Reset();

			this.Background._DeserializeInternal(value);

			this._Extract(value, "opacity", function (newValue: string) {
				self.Opacity(parseInt(self._DecodeComponent(newValue), 10));
			});

			this._Extract(value, "thickness", function (newValue: string) {
				self.Thickness(self._DecodeComponent(newValue));
			});

			this._Extract(value, "offsetTop", function (newValue: string) {
				self.OffsetTop(self._DecodeComponent(newValue));
			});

			this._Extract(value, "offsetLeft", function (newValue: string) {
				self.OffsetLeft(self._DecodeComponent(newValue));
			});

			this.Corner._DeserializeInternal(value);
		}

		_Reset() {
			this.Background._Reset();
			this.Opacity(undefined);
			this.Thickness(undefined);
			this.OffsetTop(undefined);
			this.OffsetLeft(undefined);
			this.Corner._Reset()
		}

	}
}

interface ObservableString {
	(newValue: string): void;
	(): string;
	subscribe: (callback: (newValue: string) => void) => void;
}

interface ObservableNumber {
	(newValue: number): void;
	(): number;
	subscribe: (callback: (newValue: number) => void) => void;
}