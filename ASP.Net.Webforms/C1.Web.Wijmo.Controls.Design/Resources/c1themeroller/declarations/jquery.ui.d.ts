/// <reference path="jquery.d.ts"/>

// Partial typing for the jQueryUI library, version 1.8.x

interface DraggableEventUIParam {
    helper: JQuery;
    position: { top: number; left: number; };
    offset: { top: number; left: number; };
}

interface DraggableEvent {
    (event: Event, ui: DraggableEventUIParam): void;
}

interface Draggable {
    // Options
    disabled?: bool;
    addClasses?: bool;
    appendTo?: any;
    axis?: string;
    cancel?: string;
    connectToSortable?: string;
    containment?: any;
    cursor?: string;
    cursorAt?: any;
    delay?: number;
    distance?: number;
    grid?: number[];
    handle?: any;
    helper?: any;
    iframeFix?: any;
    opacity?: number;
    refreshPositions?: bool;
    revert?: any;
    revertDuration?: number;
    scope?: string;
    scroll?: bool;
    scrollSensitivity?: number;
    scrollSpeed?: number;
    snap?: any;
    snapMode?: string;
    snapTolerance?: number;
    stack?: string;
    zIndex?: number;
    // Events
    create?: DraggableEvent;
    start?: DraggableEvent;
    drag?: DraggableEvent;
    stop?: DraggableEvent;
}

interface DroppableEventUIParam {
    draggable: JQuery;
    helper: JQuery;
    position: { top: number; left: number; };
    offset: { top: number; left: number; };
}

interface DroppableEvent {
    (event: Event, ui: DroppableEventUIParam): void;
}

interface Droppable {
    // Options
    disabled?: bool;
    accept?: any;
    activeClass?: string;
    greedy?: bool;
    hoverClass?: string;
    scope?: string;
    tolerance?: string;
    // Events
    create?: DroppableEvent;
    activate?: DroppableEvent;
    deactivate?: DroppableEvent;
    over?: DroppableEvent;
    out?: DroppableEvent;
    drop?: DroppableEvent;
}

class JQueryWidget {
    widgetName: string;
    namespace: string;
    options: any;
    element: JQuery;
    destroy();
    disable: () => void;
    _trigger(name: string, eventObj?: JQueryEventObject, data?: any);
    _setOption(name: string, value);
    option(name: string, value);
    _create();
    _init();
}

interface JQueryWidgetStatic {
    prototype: JQueryWidget;
}

interface JQueryUI {
    keyCode: any;
}

interface JQuery {
    draggable(options: Draggable): JQuery;
    draggable(optionLiteral: string, options: Draggable): JQuery;
    draggable(optionLiteral: string, optionName: string, optionValue: any): JQuery;
    draggable(optionLiteral: string, optionName: string): any;
    // draggable(methodName: string): any;
    droppable(options: Droppable): JQuery;
    droppable(optionLiteral: string, options: Draggable): JQuery;
    droppable(optionLiteral: string, optionName: string, optionValue: any): JQuery;
    droppable(optionLiteral: string, optionName: string): any;
    droppable(methodName: string): any;
    button(arg0?: any, arg1?: any, arg2?: any);
    buttonset(arg0?: any, arg1?: any, arg2?: any);
    mousewheel(handler: (e: JQueryEventObject, delta: number) => any);

    resizable(options: any): JQuery;
    effect(effect: any, properties?: any, duration?: any, complete?: Function): JQuery;
    position(position: any);
    zIndex(zindex?: number);
    show(effect?: any, options?: any, speed?: any, callback?: Function);
    hide(effect?: any, options?: any, speed?: any, callback?: Function);
}

interface JQueryStatic {
    effects: any;
    ui: JQueryUI;
    Widget: JQueryWidgetStatic;
    widget(name: string, prototype: Object);
    widget(name: string, baseWidget, prototype: Object);
}