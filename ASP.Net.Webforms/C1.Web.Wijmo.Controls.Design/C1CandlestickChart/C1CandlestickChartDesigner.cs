﻿using System;
using System.Linq;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Design.C1ChartCore;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.C1CandlestickChart
{
	using C1.Web.Wijmo.Controls.C1Chart;

	/// <summary>
	/// Provides a C1CandlestickChartDesigner class for extending the design-mode behavior of a C1CandlestickChart control.
	/// </summary>
	public class C1CandlestickChartDesigner : C1ChartCoreDesigner
	{
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
		}

		protected override void AddSampleData()
		{
			C1CandlestickChart c1candlestickChart = Component as C1CandlestickChart;
			CandlestickChartSeries series = new CandlestickChartSeries();
			series.Label = "SampleData";
			series.Data.X.AddRange(new DateTime[] { new DateTime(2014, 1, 1), new DateTime(2014, 1, 2), new DateTime(2014, 1, 3), new DateTime(2014, 1, 4), new DateTime(2014, 1, 5) });
			series.Data.High.AddRange(new double[] { 10, 12, 11, 14, 16 });
			series.Data.Low.AddRange(new double[] { 7.5, 8.6, 4.4, 4.2, 8 });
			series.Data.Open.AddRange(new double[] { 8, 8.6, 11, 6.2, 13.8 });
			series.Data.Close.AddRange(new double[] { 8.6, 11, 6.2, 13.8, 15 });
			c1candlestickChart.SeriesList.Add(series);
		}

		internal override void BuildSeriesStyles()
		{
			PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["CandlestickChartSeriesStyles"];
			DataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditCollectionCallback), context, C1Localizer.GetString("C1ChartCore.SmartTag.SeriesStyles"), context);
		}

		internal override void BuildSeriesHoverStyles()
		{
			PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["CandlestickChartSeriesHoverStyles"];
			DataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditCollectionCallback), context, C1Localizer.GetString("C1ChartCore.SmartTag.SeriesHoverStyles"), context);
		}
	}
}
