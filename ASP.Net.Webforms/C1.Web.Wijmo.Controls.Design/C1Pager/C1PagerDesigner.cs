﻿using System;

namespace C1.Web.Wijmo.Controls.Design.C1Pager
{
	using C1.Web.Wijmo.Controls.C1Pager;
	using System.Web.UI.Design;
	using System.ComponentModel.Design;

    public class C1PagerDesigner : C1ControlDesinger
	{
		C1Pager _control;

		public override void Initialize(System.ComponentModel.IComponent component)
		{
			base.Initialize(component);
			_control = component as C1Pager;
		}

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection lists = new DesignerActionListCollection();
				lists.AddRange(base.ActionLists);
				lists.Add(new C1PagerActionList(this));
				return lists;
			}
		}
	}
}
