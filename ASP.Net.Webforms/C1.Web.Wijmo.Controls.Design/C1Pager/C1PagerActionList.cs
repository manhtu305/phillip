﻿using System;

namespace C1.Web.Wijmo.Controls.Design.C1Pager
{
	using System.Reflection;

	[Obfuscation(Exclude = true, ApplyToMembers = true)]
	internal class C1PagerActionList : DesignerActionListBase
	{
		public C1PagerActionList(C1PagerDesigner designer)
			: base(designer)
		{
		}
	}
}
