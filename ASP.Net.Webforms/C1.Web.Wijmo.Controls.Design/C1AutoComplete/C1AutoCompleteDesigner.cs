﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.Design.C1AutoComplete
{
	using C1.Web.Wijmo.Controls.C1AutoComplete;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using System.Web.UI.WebControls;

	/// <summary>
	///  AutoComplete Designer
	/// </summary>
	[SupportsPreviewControl(true)]
	public class C1AutoCompleteDesigner : C1DataBoundControlDesigner
	{
		#region ** fields

		private C1AutoComplete _control;
		private DesignerActionListCollection _actionList;
		private TemplateGroupCollection _templateGroups;
		private static readonly string[] _templateNames;

		#endregion

		
        #region ** structor
		static C1AutoCompleteDesigner()
        {
			_templateNames = new string[] { "ItemContent" };
        }
        #endregion

		#region override

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			_control = (C1AutoComplete)component;

			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			base.SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			string a = base.GetDesignTimeHtml();
			return a;
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actionList == null)
				{
					_actionList = new DesignerActionListCollection();
					_actionList.AddRange(base.ActionLists);
					_actionList.Add(new C1AutoCompleteActionList(this));
				}
				return _actionList;
			}
		}

		/// <summary>
		/// TemplateGroups implementation.
		/// </summary>
		public override TemplateGroupCollection TemplateGroups
		{
			get
			{
				TemplateGroupCollection templateGroups = base.TemplateGroups;
				if (this._templateGroups == null)
				{
					this._templateGroups = new TemplateGroupCollection();
					TemplateGroup group = new TemplateGroup("CommonItemTemplates", ((WebControl)base.ViewControl).ControlStyle);
					TemplateDefinition templateDefinition = new TemplateDefinition(this, _templateNames[0], this._control, _templateNames[0], false);
					templateDefinition.SupportsDataBinding = true;
					group.AddTemplateDefinition(templateDefinition);
					this._templateGroups.Add(group);
				}
				templateGroups.AddRange(this._templateGroups);
				return templateGroups;
			}
		}

		public void OpenBuilder()
		{
            List<C1AutoCompleteDataItem> originalData = new List<C1AutoCompleteDataItem>();
            foreach (C1AutoCompleteDataItem tp in _control.Items)
			{
				originalData.Add(tp);
			}

			C1AutoCompleteDesignerForm editorForm = new C1AutoCompleteDesignerForm(this._control);
			bool accept = ShowControlEditorForm(this._control, editorForm);

			if (accept)
				this.UpdateDesignTimeHtml();
			else
			{
				_control.Items.Clear();
                foreach (C1AutoCompleteDataItem tp in originalData)
				{
					_control.Items.Add(tp);
				}
			}
		}

		public bool ShowControlEditorForm(object control, System.Windows.Forms.Form editorForm)
		{
			System.Windows.Forms.DialogResult dr;
			IServiceProvider serviceProvider = ((IComponent)control).Site;
			if (serviceProvider != null)
			{
				IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
				DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1AutoComplete.EditControl"));
				using (trans)
				{
					System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
					IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

					if (service != null)
					{
						ccs.OnComponentChanging(control, null);
						dr = service.ShowDialog(editorForm);
					}
					else
						dr = System.Windows.Forms.DialogResult.None;
					if (dr == System.Windows.Forms.DialogResult.OK)
					{
						ccs.OnComponentChanged(control, null, null, null);
						trans.Commit();
					}
					else
					{
						trans.Cancel();
					}
				}
			}
			else
				dr = editorForm.ShowDialog();

			editorForm.Dispose();
			editorForm = null;

			return dr == System.Windows.Forms.DialogResult.OK;
		}
		

		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}

		#endregion
	}

	internal class C1AutoCompleteActionList : DesignerActionListBase
	{

		private DesignerActionItemCollection _items;
		private C1AutoComplete _AutoComplete;

		public C1AutoCompleteActionList(C1AutoCompleteDesigner parent)
			: base(parent)
		{
			this._AutoComplete = base.Component as C1AutoComplete;
		}

		public Unit Width
		{
			get 
			{ 
				return this._AutoComplete.Width; 
			}
            set
			{
				SetProperty("Width", value);
			}
		}

		public Unit Height
		{
			get
			{
				return this._AutoComplete.Height;
			}
			set
			{
				SetProperty("Height", value);
			}
		}

		public void OpenBuilder()
		{
			C1AutoCompleteDesigner designer = this.Designer as C1AutoCompleteDesigner;
			designer.OpenBuilder();
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (this._items == null)
			{
				this._items = new DesignerActionItemCollection();
				this._items.Add(new DesignerActionPropertyItem("Width",
                    C1Localizer.GetString("C1AutoComplete.SmartTag.Width"),
					"Behavior",
					C1Localizer.GetString("C1AutoComplete.SmartTag.WidthDescription")));
                this._items.Add(new DesignerActionPropertyItem("Height",
                    C1Localizer.GetString("C1AutoComplete.SmartTag.Height"),
					"Behavior",
					C1Localizer.GetString("C1AutoComplete.SmartTag.HeightDescription")));
				this._items.Add(new DesignerActionMethodItem(this, "OpenBuilder",
                    C1Localizer.GetString("C1AutoComplete.SmartTag.OpenBuilder"),
					"",
					C1Localizer.GetString("C1AutoComplete.SmartTag.OpenBuilderDescription"),true));

				AddBaseSortedActionItems(this._items);
			}
			return this._items;
		}
	}
}
