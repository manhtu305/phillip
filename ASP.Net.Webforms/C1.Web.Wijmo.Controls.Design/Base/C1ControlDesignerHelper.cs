﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design
{
	internal class C1ControlDesignerHelper
	{
		IWebApplication _webService;
		private WebConfigUtil _webconfigUtil;

		public C1ControlDesignerHelper(IWebApplication webService)
		{
			_webService = webService;
		}

		internal void RegisterHandlesForResources()
		{
			string path = C1TargetControlBase.WIJMOCONTROLSRESOURCEHANDLER.Replace("~/", "");
			RegisterWijmoHttpHandler("WijmoControlsHttpHandler", path, typeof(WijmoHttpHandler));
		}

		private WebConfigUtil WebConfigUtil
		{
			get 
			{
				if (_webconfigUtil == null)
				{
					_webconfigUtil = new WebConfigUtil(_webService);
				}
				return _webconfigUtil;
			}
		}


		internal void RegisterWijmoHttpHandler(string name, string path, Type type)
		{
			WebConfigUtil.RegisterHandler(name, path, type);
		}

		internal void RegisterWijmoHttpModule(string name,Type type)
		{
			WebConfigUtil.RegisterModule(name, type);
		}
	}
}
