﻿
using System;
using System.Drawing;


namespace C1.Web.Wijmo.Controls.Design
{
    /// <summary>
    /// A class that represents C1Item info.
    /// </summary>
    public class C1ItemInfo
    {
        private object _itemType = null;
        private bool _enableChildItems;
        private Image _nodeImage;
        private string _contextMenuStripText;
        private string _nodeText;
        private bool _visible = false;
        private bool _default = false;

        /// <summary>
        /// C1ItemInfo constructor
        /// </summary>
        public C1ItemInfo() { }
        
        /// <summary>
        /// C1ItemInfo constructor
        /// </summary>
        /// <param name="itemType">Custom item type.</param>
        /// <param name="childItems">Indicates if element can contain child items.</param>
        /// <param name="nodeImage">The Image displayed in menus and treeview that represent this item.</param>
        /// <param name="defaultNodeText">The default text displayed as the node text in the TreeView, when node is created.</param>
        /// <param name="contextMenuStripText">The text used for the ContextMenuStrip and ToolStrip for this item.</param>
        /// <param name="visible">Indicates if element could be loaded into a contol such as ToolStrip, MenuStrip or ContextMenuStrip.</param>
        /// <param name="defaultItem">Indicates if current item is used as the default item type.</param>
        public C1ItemInfo(object itemType, bool childItems, Image nodeImage, string defaultNodeText,
                          string contextMenuStripText, bool visible, bool defaultItem)
        {
            _itemType = itemType;
            _enableChildItems = childItems;
            _nodeImage = nodeImage;
            _nodeText = defaultNodeText;
            _contextMenuStripText = contextMenuStripText;
            _visible = visible;
            _default = defaultItem;
        }

        /// <summary>
        /// Identifies the type of object.
        /// </summary>
        public object ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        /// <summary>
        /// Gets a value indicating of this type can have children.
        /// </summary>
        public bool EnableChildItems
        {
            get { return _enableChildItems; }
            set { _enableChildItems = value; }
        }

        /// <summary>
        /// The Image displayed in menus and treeview that represent this item.
        /// </summary>
        public Image NodeImage
        {
            get { return _nodeImage; }
            set { _nodeImage = value; }
        }

        /// <summary>
        /// The text displayed as the node text in the TreeView.
        /// </summary>
        public string DefaultNodeText
        {
            get { return _nodeText; }
            set { _nodeText = value; }
        }

        /// <summary>
        /// The text used for the ContextMenuStrip and ToolStrip for this item.
        /// </summary>
        public string ContextMenuStripText
        {
            get { return _contextMenuStripText; }
            set { _contextMenuStripText = value; }
        }

        /// <summary>
        /// Indicates if item info could be loaded into a contol such as ToolStrip, MenuStrip or ContextMenuStrip.
        /// </summary>
        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        /// <summary>
        /// Indicates if current item is used as the default item type.
        /// </summary>
        public bool Default
        {
            get { return _default; }
            set { _default = value; }
        }
    }
}
