﻿
namespace C1.Web.Wijmo.Controls.Design
{
    /// <summary>
    /// Gets additional info about attached object in designer TreeView
    /// </summary>
    public class C1NodeInfo
    {
        private C1ItemInfo _itemInfo = null;
        private object _element;
        private string _nodeText;

        /// <summary>
        /// Default constructor
        /// </summary>
        public C1NodeInfo()
        { 
        }

        /// <summary>
        /// C1NodeInfo constructor.
        /// </summary>
        /// <param name="element">Gets and sets the C1 control object.</param>
        /// <param name="itemInfo">Gets the <seealso cref="Element"/> (C1Control) item info.</param>
        /// <param name="nodeText">Gets and sets the text for the TreeNode that will contain this C1NodeInfo.</param>
        public C1NodeInfo(object element, C1ItemInfo itemInfo, string nodeText)
        {
            _element = element;
            _itemInfo  = itemInfo;
            _nodeText = nodeText;
        }

        /// <summary>
        /// Gets the <seealso cref="Element"/> (C1Control) item info
        /// </summary>
        public C1ItemInfo ItemInfo
        {
            get { return _itemInfo; }
            set { _itemInfo = value; }
        }

        /// <summary>
        /// Gets and sets the C1 control
        /// </summary>
        public object Element
        {
            get { return _element; }
            set { _element = value; }
        }

        /// <summary>
        /// Gets and sets the text for the TreeNode that will contain this C1NodeInfo.
        /// </summary>
        public string NodeText
        {
            get { return _nodeText; }
            set { _nodeText = value; }
        }
    }
}
