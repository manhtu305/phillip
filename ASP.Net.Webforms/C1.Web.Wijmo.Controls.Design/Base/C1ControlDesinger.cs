﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Web.UI.Design;
using System.Xml;

namespace C1.Web.Wijmo.Controls.Design
{
    public class C1ControlDesinger : ControlDesigner
    {

		private C1ControlDesignerHelper _designerHelper;

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
			this.RegisterHttpHandlersAndModules();
        }

		internal C1ControlDesignerHelper DesignerHelper 
		{
			get
			{
				if (_designerHelper == null)
				{
					_designerHelper = new C1ControlDesignerHelper((System.Web.UI.Design.IWebApplication)this.GetService(typeof(System.Web.UI.Design.IWebApplication)));
				}
				return _designerHelper;
			}
		}


		protected void RegisterHttpHandlersAndModules() {
			this.RegisterHttpHandler();
			this.RegisterHttpModule();
		}

		protected virtual void RegisterHttpHandler()
		{
			this.DesignerHelper.RegisterHandlesForResources();
		}

		protected virtual void RegisterHttpModule()
		{ 
			
		}
   }
}
