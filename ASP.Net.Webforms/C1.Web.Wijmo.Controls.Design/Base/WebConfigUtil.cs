﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Configuration;
using System.Web.UI.Design;
using System.Xml;

namespace C1.Web.Wijmo.Controls.Design
{
	internal class WebConfigUtil
	{
		#region ** fields
		Configuration _conf;
		XmlDocument _webConfigXmlDoc;
		IWebApplication _service;
		#endregion

		public WebConfigUtil(IWebApplication service)
		{
			_service = service;
		}

		private void InitConfig() 
		{
			if (_conf == null) 
			{
				if (_service != null) 
				{
					_conf = _service.OpenWebConfiguration(false);
					_webConfigXmlDoc = new XmlDocument();
					_webConfigXmlDoc.Load(this._conf.FilePath);
				}
			}
		}

		private void ResetConfig() 
		{
			_conf = null;
			_webConfigXmlDoc = null;
		}

		#region ** properties
		private Configuration WebConfiguration
		{
			get
			{
				return _conf;
			}
		}

		private XmlDocument WebConfigXmlDoc
		{
			get
			{
				return _webConfigXmlDoc;
			}
		}
		#endregion

		#region ** handlers
		private bool checkIIS6HttpHandlerExist(string path, Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			ConfigurationSectionGroup group = WebConfiguration.GetSectionGroup("system.web");
			if (group == null)
			{
				return false;
			}
			else
			{
				HttpHandlersSection httpHandlersSection = (HttpHandlersSection)WebConfiguration.GetSection("system.web/httpHandlers");
				if (httpHandlersSection == null)
				{
					return false;
				}

				foreach (HttpHandlerAction handler in httpHandlersSection.Handlers)
				{
					if (handler.Path == path)
					{
						if (handler.Type == sType)
						{
							return true;
						}
						else if (handler.Type.Contains(type.FullName))
						{
							httpHandlersSection.Handlers.Remove(handler);
						}

					}
				}
				return false;
			}
		}

		private bool checkIIS7HttpHandlerExist(string path, Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			this.LoadWebConfigXml();
			XmlNode node = WebConfigXmlDoc.SelectSingleNode("//handlers");
			if (node == null)
			{
				return false;
			}
			else
			{
				XmlNodeList list = node.SelectNodes("add[@path='" + path + "']");
				if (list.Count == 0)
				{
					return false;
				}

				foreach (XmlNode node2 in list)
				{
					if (node2.Attributes["path"].Value == path)
					{
						if (node2.Attributes["type"].Value.Equals(sType))
						{
							return true;
						}
						else if (node2.Attributes["type"].Value.Contains(type.FullName))
						{
							node.RemoveChild(node2);
						}
					}

				}

				return false;
			}
		}

		public void RegisterHandler(string name, string path, Type type)
		{
			InitConfig();
			if (WebConfiguration == null)
			{
				return;
			}
			this.RegisterHandlerForIIS6(WebConfiguration, path, type);
			this.RegisterHandlerForIIS7(WebConfiguration, name, path, type);
			ResetConfig();
		}


		public void RegisterHandlerForIIS6(Configuration conf, string path, Type type)
		{
			try
			{
				if (checkIIS6HttpHandlerExist(path, type))
				{
					return;
				}

				string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
				ConfigurationSectionGroup group = conf.GetSectionGroup("system.web");
				if (group == null)
				{
					group = new ConfigurationSectionGroup();
					conf.SectionGroups.Add("system.web", group);
				}

				// Add httpHandlers
				HttpHandlersSection httpHandlersSection = (HttpHandlersSection)conf.GetSection("system.web/httpHandlers");
				if (httpHandlersSection == null)
				{
					httpHandlersSection = new HttpHandlersSection();
					group.Sections.Add("httpHandlers", httpHandlersSection);
				}

				HttpHandlerAction httpHandler = new HttpHandlerAction(path, sType, "*", false);
				httpHandlersSection.Handlers.Add(httpHandler);
				conf.Save(ConfigurationSaveMode.Minimal);
			}
			catch (Exception) { }
		}

		private void RegisterHandlerForIIS7(Configuration conf, string name, string path, Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			try
			{
				this.LoadWebConfigXml();
				if (this.WebConfiguration == null || this.WebConfigXmlDoc == null)
				{
					return;
				}
				if (checkIIS7HttpHandlerExist(path, type))
				{
					return;
				}
				//XmlDocument webConfigXmlDoc = new XmlDocument();
				//webConfigXmlDoc.Load(conf.FilePath);
				this.AddHttpHandlerNode(this.WebConfigXmlDoc, name, path, type);
				this.WebConfigXmlDoc.Save(conf.FilePath);
			}
			catch (Exception)
			{
			}
		}

		private void AddHttpHandlerNode(XmlDocument webConfigXmlDoc, string name, string path, Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			XmlNode handlersNode;
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["name"] = name;
			dictionary["verb"] = "*";
			dictionary["preCondition"] = "integratedMode";
			dictionary["path"] = path;
			dictionary["type"] = sType;

			handlersNode = this.EnsureXPathExists(webConfigXmlDoc, "configuration/system.webServer/handlers");
			this.EnsureValidationElement(webConfigXmlDoc);
			this.AppendNode(webConfigXmlDoc, handlersNode, dictionary, "add");
		}
		#endregion

		#region ** modules
		public void RegisterModule(string name, Type type)
		{
			InitConfig();
			if (WebConfiguration == null)
			{
				return;
			}
			this.RegisterMoudleForIIS6(WebConfiguration, name, type);
			this.RegisterMoudleForIIS7(WebConfiguration, name, type);
			ResetConfig();
		}

		private bool checkIIS6HttpModuleExist(Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);

			ConfigurationSectionGroup group = WebConfiguration.GetSectionGroup("system.web");
			if (group == null)
			{
				return false;
			}
			else
			{
				HttpModulesSection httpModulesSection = (HttpModulesSection)WebConfiguration.GetSection("system.web/httpModules");
				if (httpModulesSection == null)
				{
					return false;
				}

				foreach (HttpModuleAction module in httpModulesSection.Modules)
				{
					if (module.Type.Equals(sType))
					{
						return true;
					}
					else if (module.Type.Contains(type.FullName))
					{
						httpModulesSection.Modules.Remove(module);
						break;
					}
				}
			}
			return false;
		}

		private bool checkIIS7HttpModuleExist(Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			this.LoadWebConfigXml();
			XmlNode node = WebConfigXmlDoc.SelectSingleNode("//modules");
			if (node == null)
			{
				return false;
			}
			else
			{
				XmlNodeList list = node.SelectNodes("add");
				if (list.Count == 0)
				{
					return false;
				}

				foreach (XmlNode node2 in list)
				{
					if (node2.Attributes["type"].Value.Equals(sType))
					{
						return true;
					}
					else if (node2.Attributes["type"].Value.Contains(type.FullName))
					{
						node.RemoveChild(node2);
					}
				}
			}
			return false;
		}

		private void RegisterMoudleForIIS6(Configuration conf, string name, Type type)
		{
			try
			{
				if (checkIIS6HttpModuleExist(type))
				{
					return;
				}

				string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
				ConfigurationSectionGroup group = conf.GetSectionGroup("system.web");
				if (group == null)
				{
					group = new ConfigurationSectionGroup();
					conf.SectionGroups.Add("system.web", group);
				}

				// Add httpHandlers
				HttpModulesSection httpModulesSection = (HttpModulesSection)conf.GetSection("system.web/httpModules");
				if (httpModulesSection == null)
				{
					httpModulesSection = new HttpModulesSection();
					group.Sections.Add("httpModules", httpModulesSection);
				}

				HttpModuleAction httpModule = new HttpModuleAction(name, sType);
				httpModulesSection.Modules.Add(httpModule);
				conf.Save(ConfigurationSaveMode.Minimal);
			}
			catch (Exception){
			}
		}

		private void RegisterMoudleForIIS7(Configuration conf, string name, Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			try
			{
				this.LoadWebConfigXml();
				if (this.WebConfiguration == null || this.WebConfigXmlDoc == null)
				{
					return;
				}
				if (checkIIS7HttpModuleExist(type))
				{
					return;
				}
				//XmlDocument webConfigXmlDoc = new XmlDocument();
				//webConfigXmlDoc.Load(conf.FilePath);
				this.AddHttpModuleNode(this.WebConfigXmlDoc, name, type);
				this.WebConfigXmlDoc.Save(conf.FilePath);
			}
			catch (Exception)
			{
			}
		}

		private void AddHttpModuleNode(XmlDocument webConfigXmlDoc, string name, Type type)
		{
			string sType = string.Format("{0}, {1}", type.FullName, type.Assembly.FullName);
			XmlNode handlersNode;
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["name"] = name;
			dictionary["type"] = sType;
			handlersNode = this.EnsureXPathExists(webConfigXmlDoc, "configuration/system.webServer/modules");
			this.EnsureValidationElement(webConfigXmlDoc);
			this.AppendNode(webConfigXmlDoc, handlersNode, dictionary, "add");
		}

		#endregion

		#region ** methods
		private XmlNode EnsureXPathExists(XmlDocument document, string path)
		{
			string[] nodeArray = path.Split(new char[] { '/' });
			XmlNode node = document;
			foreach (string str in nodeArray)
			{
				XmlElement newChild = (XmlElement)node.SelectSingleNode(str);
				if (newChild == null)
				{
					newChild = document.CreateElement(str);
					node.AppendChild(newChild);
				}
				node = newChild;
			}
			return node;
		}

		private void UpdateAttribute(XmlDocument document, XmlNode addElement, string attributeName, string attributeValue)
		{
			if (addElement.Attributes[attributeName] == null)
			{
				XmlAttribute node = document.CreateAttribute(attributeName);
				node.Value = attributeValue;
				addElement.Attributes.Append(node);
			}
			else
			{
				addElement.Attributes[attributeName].Value = attributeValue;
			}
		}

		private void AppendNode(XmlDocument webConfigXmlDoc, XmlNode parentNode, Dictionary<string, string> attributes, string nodeName)
		{
			XmlElement childNode = webConfigXmlDoc.CreateElement(nodeName);
			parentNode.AppendChild(childNode);
			foreach (string attr in attributes.Keys)
			{
				this.UpdateAttribute(webConfigXmlDoc, childNode, attr, attributes[attr]);
			}
		}

		protected void EnsureValidationElement(XmlDocument webConfigXml)
		{
			XmlNode addElement = this.EnsureXPathExists(webConfigXml, "configuration/system.webServer/validation");
			this.UpdateAttribute(webConfigXml, addElement, "validateIntegratedModeConfiguration", "false");
		}

		private void LoadWebConfigXml()
		{
			WebConfigXmlDoc.Load(WebConfiguration.FilePath);
		}
		#endregion
	}
}
