﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Reflection;
using C1.Web.Wijmo.Controls.Design.Localization;
using C1.Web.Wijmo;
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Design.C1ThemeRoller;
using System.Configuration;
using System.Web.Configuration;
using System.Xml;

namespace C1.Web.Wijmo.Controls.Design
{
    /// <summary>
    /// DesignerActionList base class.
    /// </summary>
	public abstract class DesignerActionListBase : DesignerActionList
	{
		#region ** fields

		private ControlDesigner _designer;
		private MethodInfo _about = null;
        private C1Theme _currentTheme;
        private WebConfigUtil _webconfigUtil;

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="DesignerActionListBase"/> class.
		/// </summary>
		/// <param name="designer">The designer associated with this action list.</param>
		public DesignerActionListBase(ControlDesigner designer)
			: base(designer.Component)
		{
			_designer = designer;
            if (this.DisplayThemeSupport())
            {
                //process the project path
            }
		}

		#endregion

		#region ** properties

        /// <summary>
        /// Get control designer.
        /// </summary>
		protected ControlDesigner Designer
		{
			get { return _designer; }
		}

        private C1Theme CurrentTheme
        {
            get
            {
                if (_currentTheme == null)
                    _currentTheme = ThemeProcesser.GetThemeFromName(Theme);
                return _currentTheme;
            }
        }

#if ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.WijmoControlThemeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(System.Drawing.Design.UITypeEditor))]
#elif ASP_NET35
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.WijmoControlThemeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(System.Drawing.Design.UITypeEditor))]
#else
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.WijmoControlThemeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(System.Drawing.Design.UITypeEditor))]
#endif
        /// <summary>
        /// Gets or sets Theme property of control.
        /// </summary>
        public string Theme
        {
            get
            {
				return (string)GetProperty(_designer.Component, "Theme").GetValue(_designer.Component);
            }
			set
			{
				SetProperty("Theme", value);
			}
        }

        /// <summary>
        /// Gets or sets UseCDN property of control.
        /// </summary>
		public bool UseCDN
		{
			get
			{
				return (bool)GetProperty(_designer.Component, "UseCDN").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("UseCDN", value);
			}
		}

        /// <summary>
        /// Gets or sets BootStrap property of control.
        /// </summary>
        public bool UseBootStrap
        {
            get
            {
                string cssAdapter = (string)GetProperty(_designer.Component, "WijmoCssAdapter")
                                        .GetValue(_designer.Component);
                return string.Equals(cssAdapter, "bootstrap");
            }
            set
            {
                if (value)
                {
                    SetProperty("WijmoCssAdapter", "bootstrap");
                }
                else
                {
                    SetProperty("WijmoCssAdapter", "jquery-ui");
                }
            }
        }

        /// <summary>
        /// Gets or sets Mobile mode of control.
        /// </summary>
        public bool MobileMode
        {
            get
            {
                WijmoControlMode controlMode = (WijmoControlMode)GetProperty(_designer.Component,
                    "WijmoControlMode").GetValue(_designer.Component);

                return controlMode == WijmoControlMode.Mobile;
          }
            set
            {               
                if (value)
                {
                    SetProperty("WijmoControlMode", WijmoControlMode.Mobile);
                }
                else
                {
                    SetProperty("WijmoControlMode", WijmoControlMode.Web);
                }
            }
        }

        /// <summary>
        /// Gets or sets CDNPath property of control.
        /// </summary>
		public string CDNPath
		{
			get
			{
				return (string)GetProperty(_designer.Component, "CDNPath").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("CDNPath", value);
			}
		}

		/// <summary>
		/// Gets or sets EnableCombinedJavaScripts property of control.
		/// </summary>
		public bool EnableCombinedJavaScripts
		{
			get
			{
				return (bool)GetProperty(_designer.Component, "EnableCombinedJavaScripts").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("EnableCombinedJavaScripts", value);
			}
		}

		/// <summary>
		/// Gets or sets EnableConditionalDependencies property of control.
		/// </summary>
		public bool EnableConditionalDependencies
		{
			get
			{
				return (bool)GetProperty(_designer.Component, "EnableConditionalDependencies").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("EnableConditionalDependencies", value);
			}
		}

		protected virtual bool SupportConditionalDependencies
		{
			get { return false; }
		}

        internal virtual bool DisplayThemeSupport()
        {
            return true;
        }

		internal protected virtual bool SupportBootstrap() 
		{
			return true;
		}

		internal virtual bool DisplayThemeSwatch()
		{
			return false;
		}

	    internal virtual bool SupportCDN
	    {
		    get { return true; }
	    }

		internal virtual bool SupportWijmoControlMode
		{
			get { return true; }
		}

	    public string ThemeSwatch
		{
			get
			{
				return (string)GetProperty(_designer.Component, "ThemeSwatch").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("ThemeSwatch", value);
			}
		}
        #endregion

		#region ** property setters

		/// <summary>
		/// Gets the PropertyDescriptor associated with the given property name.
		/// </summary>
		/// <param name="obj">The object that contains the property.</param>
		/// <param name="propname">The property name.</param>
		/// <returns></returns>
		protected PropertyDescriptor GetProperty(object obj, string propname)
		{
			return TypeDescriptor.GetProperties(obj)[propname];
		}

		/// <summary>
		/// Gets the value of the given property name.
		/// </summary>
		/// <param name="propname">The property name.</param>
		/// <returns></returns>
		protected object GetProperty(string propname)
		{
			return GetProperty(_designer.Component, propname).GetValue(_designer.Component);
		}

		/// <summary>
		/// Sets the value of a property.
		/// </summary>
		/// <param name="propname">The property name.</param>
		/// <param name="val">The new value.</param>
		protected virtual void SetProperty(string propname, object val)
		{
			SetProperty(_designer.Component, propname, val);
		}
		/// <summary>
		/// Sets the value of a property.
		/// </summary>
		/// <param name="obj">The object that contains the property.</param>
		/// <param name="propname">The property name.</param>
		/// <param name="val">The new value.</param>
		protected void SetProperty(object obj, string propname, object val)
		{
			GetProperty(obj, propname).SetValue(obj, val);
		}
		#endregion

        #region ** methods

		/// <summary>
		/// Displays the about box.
		/// </summary>
		protected virtual void About()
		{
			if (_about == null)
			{
				Type t = typeof(C1TargetControlBase).Assembly.GetType("C1.Util.Licensing.ProviderInfo");
				_about = t.GetMethod("ShowAboutBox", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic, null,
					new Type[] { typeof(object) }, null);
				//_about =  t.GetMethod("ShowAboutBox", BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic);
			}

			_about.Invoke(base.Component, new object[] { this.Component });
		}

        /// <summary>
        /// Display the dialog for customizing theme.
        /// </summary>
        protected void CustomizeTheme()
        {
            if (CurrentTheme.Kind != ThemeType.Custom)
                return;
            int pid = Process.GetCurrentProcess().Id;
			try
			{
            using (ThemeEditor te = new ThemeEditor(this, pid, ThemeProcesser.EmbeddedThemes, ThemeProcesser.WijmoScriptsAccessor))
            {
                string[] addedThemes;
					ThemeMapper customTheme = Utilites.GetActiveProjectCustomThemesAccessor(pid, Utilites.GetRootProjectItem(this.Component)).GetThemeMapperByHostName(CurrentTheme.DisplayName);
                if (customTheme == null)
                {
					string msg = C1Localizer.GetString("ThemeRoller.UI.ThemeNotExist", "The theme named \"{0}\" doesn't exist.");
					Utilites.ShowErrorMessage(string.Format(msg, CurrentTheme.DisplayName));
					return;
                }
                addedThemes = te.EditCustomTheme((string)customTheme.Host, true);
                if (addedThemes.Length < 1)
                    return;
                CurrentTheme.Kind = ThemeType.Custom;
                CurrentTheme.DisplayName = addedThemes[addedThemes.Length - 1];
            }
        }
			catch (Exception e)
			{
				Utilites.ShowErrorMessage(e.Message);
				return;
			}
		}

        /// <summary>
        /// Display the dialog to create new customized theme.
        /// </summary>
        protected void CreateNewTheme()
        {
			try
			{
            using (NewThemeForm fm = new NewThemeForm(this, CurrentTheme, ThemeProcesser.WijmoScriptsAccessor))
            {
                fm.ShowDialog();
            }
        }
			catch (Exception e)
			{
				Utilites.ShowErrorMessage(e.Message);
				return;
			}
		}
        #endregion

        #region ** overrides
        /// <summary>
        /// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/> objects contained in the list.
        /// </summary>
        /// <remarks>This method can not be overridden.</remarks>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/> array that contains the items in this list.
        /// </returns>        
        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection items = new DesignerActionItemCollection();
            AddBaseSortedActionItems(items);            
            return items;
        }

		/// <summary>
		/// Adds the base sorted action items.
		/// </summary>
		/// <param name="items">The items.</param>
        protected void AddBaseSortedActionItems(DesignerActionItemCollection items)
        {
            if (_designer.Component is C1TargetCompositeControlBase
				|| _designer.Component is C1TargetCompositeDataBoundControlBase
				|| _designer.Component is C1TargetControlBase
				|| _designer.Component is C1TargetDataBoundControlBase
				|| _designer.Component is C1TargetHierarchicalDataBoundControlBase)
            {
                
                if (this.DisplayThemeSupport())
                {
                    if (!this.UseBootStrap)
                    {
                        items.Add(new DesignerActionPropertyItem("Theme", C1Localizer.GetString("Base.SmartTag.Theme", "Theme"), "Theme", C1Localizer.GetString("Base.SmartTag.ThemeDescription")));
                        if (CurrentTheme.Kind == ThemeType.Custom)
                            items.Add(new DesignerActionMethodItem(this, "CustomizeTheme", C1Localizer.GetString("Base.SmartTag.CustomizeTheme", "Customize theme..."), "Theme"));
                        items.Add(new DesignerActionMethodItem(this, "CreateNewTheme", C1Localizer.GetString("Base.SmartTag.CreateNewTheme", "Create new theme..."), "Theme"));
                    }
                }

				if (SupportCDN){
					items.Add(new DesignerActionPropertyItem("UseCDN", C1Localizer.GetString("Base.SmartTag.UseCDN", "UseCDN"), "CDN", C1Localizer.GetString("Base.SmartTag.UseCDNDescription")));
					items.Add(new DesignerActionPropertyItem("CDNPath", C1Localizer.GetString("Base.SmartTag.CDNPath", "CDNPath"), "CDN", C1Localizer.GetString("Base.SmartTag.CDNPathDescription")));
				}

				if (this.SupportBootstrap())
				{
                items.Add(new DesignerActionPropertyItem("UseBootStrap", C1Localizer.GetString("Base.SmartTag.UseBootStrap", "UseBootStrap"), "BootStrap", C1Localizer.GetString("Base.SmartTag.UseBootStrapDescription")));
				}

				if (SupportWijmoControlMode){
					items.Add(new DesignerActionPropertyItem("MobileMode", C1Localizer.GetString("Base.SmartTag.MobileMode", "MobileMode"), "MobileMode", C1Localizer.GetString("Base.SmartTag.MobileModeDescription")));
				}

				items.Add(new DesignerActionPropertyItem("EnableCombinedJavaScripts", C1Localizer.GetString("Base.SmartTag.EnableCombinedJavaScripts", "EnableCombinedJavaScripts"), "EnableCombinedJavaScripts", C1Localizer.GetString("Base.SmartTag.EnableCombinedJavaScriptsDescription")));

				if (SupportConditionalDependencies)
				{
					items.Add(new DesignerActionPropertyItem("EnableConditionalDependencies", C1Localizer.GetString("Base.SmartTag.EnableConditionalDependencies", "EnableConditionalDependencies"), "EnableConditionalDependencies", C1Localizer.GetString("Base.SmartTag.EnableConditionalDependenciesDescription")));
				}

				if (this.MobileMode && this.DisplayThemeSwatch())
				{
					items.Add(new DesignerActionPropertyItem("ThemeSwatch", C1Localizer.GetString("Base.SmartTag.ThemeSwatch", "ThemeSwatch"), "ThemeSwatch", C1Localizer.GetString("Base.SmartTag.ThemeSwatchDescription")));
				}
            }
            items.Add(new DesignerActionMethodItem(this, "About", C1Localizer.GetString("Base.SmartTag.AboutBox", "About"), "About"));

        }

        #endregion

        #region ** implementation
        

        #endregion
    }
}