﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design
{
    /// <summary>
    /// Base Items editor form. 
    /// <remarks>Notes: Form designer doesn't allow to modify properties of all new 
    /// for .Net Framework 2.0 controls (ToolStrip, MenuStrip, ContextMenuStrip, StatusStrip, 
    /// TableLayoutPanel, FlowLayoutPanel, DataGridView, BindingNavigator) belonging to 
    /// inherited form even if the value of access modifier on parental form has been established 
    /// to public or protected. 
    /// Nevertheless all these properties can be modified in the program code of inherited form.
    /// We don’t have the same problem with controls, which already existed 
    /// in .Net 1.1 (TabControl, Panel, TextBox, Button… and others).
    /// See: http://connect.microsoft.com/VisualStudio/feedback/ViewFeedback.aspx?FeedbackID=115264
    /// See: http://cs.rthand.com/blogs/blog_with_righthand/archive/2005/11/10/186.aspx
    /// </remarks> 
    /// </summary>
    public class C1BaseItemEditorForm : Form
    {
        /// <summary>
        /// Action mode enumeration
        /// </summary>
        protected enum ActionMode
        {
            Insert,
            Add
        }

        #region ** Fields

        private System.Web.UI.Control _control;

        private List<C1ItemInfo> _itemsInfo;
        protected C1ItemInfo _lastAddedItemInfo = null;
        protected C1ItemInfo _lastInsertedItemInfo = null;
        protected C1ItemInfo _lastChangedItemInfo = null;
        private Dictionary<Image, int> _imageMapper;

        private bool _allowMoveUp = false;
        private bool _allowMoveDown = false;
        private bool _allowMoveLeft = false;
        private bool _allowMoveRight = false;
        private bool _allowAdd = false;
        private bool _allowInsert = false;
        private bool _allowChangeType = false;
        private bool _allowCut = false;
        private bool _allowCopy = false;
        private bool _allowPaste = false;
        private bool _allowDelete = false;
        private bool _allowRename = false;
        private bool _allowSaveXml = true;

        // Relation width among TreeView and Property Grid
        private double _relTreeWidth = 0.35;

        // Component arrays of form controls with same functionality
        private Component[] _commandsRename;
        private Component[] _commandsAddChild;
        private Component[] _commandsInsertItem;
        private Component[] _commandsChangeItemType;

        private Component[] _commandsCut;
        private Component[] _commandsCopy;
        private Component[] _commandsPaste;
        private Component[] _commandsDelete;

        private Component[] _commandsMoveUp;
        private Component[] _commandsMoveDown;
        private Component[] _commandsMoveLeft;
        private Component[] _commandsMoveRight;

        private Component[] _commandsSaveXml;

        // clipboard data for base designer class
        private bool _clipboardData;

        // drag&drop fields
        private TreeNode _draggedNode = null;
        private Rectangle _highLightedRect;


        #endregion  // Fields
        
        private PropertyGrid _mainPropertyGrid;
        private ContextMenuStrip contextMenuStrip_ForPropertyGrid;
        private ToolStripMenuItem toolStripMenuItem1;



        #region ** C1BaseItemEditorForm definition

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private C1WebBrowser _previewer;
        protected TabPage _tabEdit;
        private System.Windows.Forms.Splitter _splitter1;
        private TreeView _mainTreeView;
        protected TabControl _mainTabControl;
        private ImageList _mainTreeViewImageList;
        protected System.Windows.Forms.TabPage _tabPreview;
        private System.Windows.Forms.Panel _panelPreviewHost;
        private System.Windows.Forms.Panel _panelBott;
        private System.Windows.Forms.Button _btnHelp;
        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.SaveFileDialog _saveFileDialog;
        private System.Windows.Forms.OpenFileDialog _openFileDialog;
        protected MenuStrip _mainMenu;
        private System.Windows.Forms.ToolStripMenuItem _miFile;
        private System.Windows.Forms.ToolStripMenuItem _miLoadXML;
        private System.Windows.Forms.ToolStripMenuItem _miSaveXML;
        private System.Windows.Forms.ToolStripSeparator _miFileSep1;
        private System.Windows.Forms.ToolStripMenuItem _miExit;
        private System.Windows.Forms.ToolStripMenuItem _miEdit;
        private System.Windows.Forms.ToolStripMenuItem _miHelp;

        // _ctxAddChildType: this context menu can be filled with all types
        // of items what could be added to given component .
        private System.Windows.Forms.ContextMenuStrip _ctxAddChildType;

        // _ctxInsertItemType: this context menu can be filled with all types
        // of items what could be added to given component .
        private System.Windows.Forms.ContextMenuStrip _ctxInsertItemType;

        // _ctxChangeItemType: this context menu can be filled with all types
        // of items what a component can change to.
        private System.Windows.Forms.ContextMenuStrip _ctxChangeItemType;

        //
        protected System.Windows.Forms.ContextMenuStrip _ctxEditContext;
        private System.Windows.Forms.ToolStripMenuItem _cmiInsertItem;
        private System.Windows.Forms.ToolStripMenuItem _cmiAddChild;
        private System.Windows.Forms.ToolStripSeparator _cmiSep1;
        private System.Windows.Forms.ToolStripMenuItem _cmiCut;
        private System.Windows.Forms.ToolStripMenuItem _cmiCopy;
        private System.Windows.Forms.ToolStripMenuItem _cmiPaste;
        private System.Windows.Forms.ToolStripSeparator _cmiSep2;
        private System.Windows.Forms.ToolStripMenuItem _cmiDelete;
        private System.Windows.Forms.ToolStripSeparator _cmiSep3;
        private System.Windows.Forms.ToolStripMenuItem _cmiRename;
        private ToolStrip _mainToolBar;
        private System.Windows.Forms.ToolStripButton _tbMoveItemUp;
        private System.Windows.Forms.ToolStripButton _tbMoveItemDown;
        private System.Windows.Forms.ToolStripButton _tbMoveItemLeft;
        private System.Windows.Forms.ToolStripButton _tbMoveItemRight;
        private System.Windows.Forms.ToolStripSeparator _tbSep1;
        private System.Windows.Forms.ToolStripSeparator _tbSep2;
        private System.Windows.Forms.ToolStripSeparator _tbSep3;
        private System.Windows.Forms.ToolStripButton _tbCut;
        private System.Windows.Forms.ToolStripButton _tbCopy;
        private System.Windows.Forms.ToolStripButton _tbPaste;
        private System.Windows.Forms.ToolStripSeparator _tbSep4;
        private System.Windows.Forms.ToolStripButton _tbDelete;
        private System.Windows.Forms.ToolStripSeparator _tbSep5;
        private System.Windows.Forms.ToolStripSplitButton _tbAddChild;
        private System.Windows.Forms.ToolStripSplitButton _tbInsertItem;
        private System.Windows.Forms.ToolStripSplitButton _tbChangeItemType;
        private System.Windows.Forms.ToolStripMenuItem _miDynHelp;
        private System.Windows.Forms.ToolStripSeparator _miSepHelp1;
        private ToolStripSeparator _cmiSep4;
        private ToolStripMenuItem _cmiChangeToType;
        private System.Windows.Forms.ToolStripMenuItem _miAbout;


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._previewer = new C1WebBrowser();
            this._tabEdit = new System.Windows.Forms.TabPage();
            this._mainPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this._splitter1 = new System.Windows.Forms.Splitter();
            this._mainTreeView = new System.Windows.Forms.TreeView();
            this._ctxEditContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmiInsertItem = new System.Windows.Forms.ToolStripMenuItem();
            this._ctxInsertItemType = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tbInsertItem = new System.Windows.Forms.ToolStripSplitButton();
            this._cmiAddChild = new System.Windows.Forms.ToolStripMenuItem();
            this._ctxAddChildType = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tbAddChild = new System.Windows.Forms.ToolStripSplitButton();
            this._cmiSep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmiCut = new System.Windows.Forms.ToolStripMenuItem();
            this._cmiCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._cmiPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._cmiSep2 = new System.Windows.Forms.ToolStripSeparator();
            this._cmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._cmiSep3 = new System.Windows.Forms.ToolStripSeparator();
            this._cmiRename = new System.Windows.Forms.ToolStripMenuItem();
            this._cmiSep4 = new System.Windows.Forms.ToolStripSeparator();
            this._cmiChangeToType = new System.Windows.Forms.ToolStripMenuItem();
            this._ctxChangeItemType = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tbChangeItemType = new System.Windows.Forms.ToolStripSplitButton();
            this._miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._mainTreeViewImageList = new System.Windows.Forms.ImageList(this.components);
            this._mainTabControl = new System.Windows.Forms.TabControl();
            this._tabPreview = new System.Windows.Forms.TabPage();
            this._panelPreviewHost = new System.Windows.Forms.Panel();
            this._panelBott = new System.Windows.Forms.Panel();
            this._btnHelp = new System.Windows.Forms.Button();
            this._btnOk = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this._saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this._mainMenu = new System.Windows.Forms.MenuStrip();
            this._miFile = new System.Windows.Forms.ToolStripMenuItem();
            this._miLoadXML = new System.Windows.Forms.ToolStripMenuItem();
            this._miSaveXML = new System.Windows.Forms.ToolStripMenuItem();
            this._miFileSep1 = new System.Windows.Forms.ToolStripSeparator();
            this._miExit = new System.Windows.Forms.ToolStripMenuItem();
            this._miHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._miDynHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._miSepHelp1 = new System.Windows.Forms.ToolStripSeparator();
            this._miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._mainToolBar = new System.Windows.Forms.ToolStrip();
            this._tbMoveItemUp = new System.Windows.Forms.ToolStripButton();
            this._tbMoveItemDown = new System.Windows.Forms.ToolStripButton();
            this._tbMoveItemLeft = new System.Windows.Forms.ToolStripButton();
            this._tbMoveItemRight = new System.Windows.Forms.ToolStripButton();
            this._tbSep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tbSep2 = new System.Windows.Forms.ToolStripSeparator();
            this._tbSep3 = new System.Windows.Forms.ToolStripSeparator();
            this._tbCut = new System.Windows.Forms.ToolStripButton();
            this._tbCopy = new System.Windows.Forms.ToolStripButton();
            this._tbPaste = new System.Windows.Forms.ToolStripButton();
            this._tbSep4 = new System.Windows.Forms.ToolStripSeparator();
            this._tbDelete = new System.Windows.Forms.ToolStripButton();
            this._tbSep5 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuStrip_ForPropertyGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this._tabEdit.SuspendLayout();
            this._ctxEditContext.SuspendLayout();
            this._mainTabControl.SuspendLayout();
            this._tabPreview.SuspendLayout();
            this._panelPreviewHost.SuspendLayout();
            this._panelBott.SuspendLayout();
            this._mainMenu.SuspendLayout();
            this._mainToolBar.SuspendLayout();
            this.contextMenuStrip_ForPropertyGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // _previewer
            // 
            this._previewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._previewer.IsWebBrowserContextMenuEnabled = false;
            this._previewer.Location = new System.Drawing.Point(0, 0);
            this._previewer.Name = "_previewer";
            this._previewer.Size = new System.Drawing.Size(484, 402);
            this._previewer.TabIndex = 3;
            this._previewer.TabStop = false;
            this._previewer.WebBrowserShortcutsEnabled = false;
            this._previewer.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this._previewer_DocumentCompleted);
            // 
            // _tabEdit
            // 
            this._tabEdit.Controls.Add(this._mainPropertyGrid);
            this._tabEdit.Controls.Add(this._splitter1);
            this._tabEdit.Controls.Add(this._mainTreeView);
            this._tabEdit.Location = new System.Drawing.Point(4, 22);
            this._tabEdit.Name = "_tabEdit";
            this._tabEdit.Size = new System.Drawing.Size(484, 402);
            this._tabEdit.TabIndex = 0;
            this._tabEdit.Text = C1Localizer.GetString("Base.ItemsEditorForm.EditTab");
            this._tabEdit.UseVisualStyleBackColor = true;
            // 
            // _mainPropertyGrid
            // 
            this._mainPropertyGrid.ContextMenuStrip = this.contextMenuStrip_ForPropertyGrid;
            this._mainPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainPropertyGrid.Location = new System.Drawing.Point(219, 0);
            this._mainPropertyGrid.Name = "_mainPropertyGrid";
            this._mainPropertyGrid.Size = new System.Drawing.Size(265, 402);
            this._mainPropertyGrid.TabIndex = 1;
            this._mainPropertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._mainPropertyGrid_PropertyValueChanged);
            // 
            // _splitter1
            // 
            this._splitter1.Location = new System.Drawing.Point(216, 0);
            this._splitter1.Name = "_splitter1";
            this._splitter1.Size = new System.Drawing.Size(3, 402);
            this._splitter1.TabIndex = 7;
            this._splitter1.TabStop = false;
            this._splitter1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this._splitter1_SplitterMoved);
            // 
            // _mainTreeView
            // 
            this._mainTreeView.AllowDrop = true;
            this._mainTreeView.ContextMenuStrip = this._ctxEditContext;
            this._mainTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this._mainTreeView.HideSelection = false;
            this._mainTreeView.ImageIndex = 0;
            this._mainTreeView.ImageList = this._mainTreeViewImageList;
            this._mainTreeView.Indent = 19;
            this._mainTreeView.LabelEdit = true;
            this._mainTreeView.Location = new System.Drawing.Point(0, 0);
            this._mainTreeView.Name = "_mainTreeView";
            this._mainTreeView.SelectedImageIndex = 0;
            this._mainTreeView.Size = new System.Drawing.Size(216, 402);
            this._mainTreeView.TabIndex = 0;
            this._mainTreeView.DragLeave += new System.EventHandler(this._mainTreeView_DragLeave);
            this._mainTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this._mainTreeView_AfterLabelEdit);
            this._mainTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this._mainTreeView_DragDrop);
            this._mainTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._mainTreeView_AfterSelect);
            this._mainTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this._mainTreeView_MouseDown);
            this._mainTreeView.DragEnter += new System.Windows.Forms.DragEventHandler(this._mainTreeView_DragEnter);
            this._mainTreeView.KeyUp += new System.Windows.Forms.KeyEventHandler(this._mainTreeView_KeyUp);
            this._mainTreeView.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this._mainTreeView_BeforeLabelEdit);
            this._mainTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this._mainTreeView_ItemDrag);
            this._mainTreeView.DragOver += new System.Windows.Forms.DragEventHandler(this._mainTreeView_DragOver);
            // 
            // _ctxEditContext
            // 
            this._ctxEditContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmiInsertItem,
            this._cmiAddChild,
            this._cmiSep1,
            this._cmiCut,
            this._cmiCopy,
            this._cmiPaste,
            this._cmiSep2,
            this._cmiDelete,
            this._cmiSep3,
            this._cmiRename,
            this._cmiSep4,
            this._cmiChangeToType});
            this._ctxEditContext.Name = "_ctxEditContext";
            this._ctxEditContext.Size = new System.Drawing.Size(165, 204);
            // 
            // _cmiInsertItem
            // 
            this._cmiInsertItem.DropDown = this._ctxInsertItemType;
            this._cmiInsertItem.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.InsertItemIco;
            this._cmiInsertItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiInsertItem.Name = "_cmiInsertItem";
            this._cmiInsertItem.Size = new System.Drawing.Size(164, 22);
            this._cmiInsertItem.Text = C1Localizer.GetString("Base.ItemsEditorForm.InsertItem");
            // 
            // _ctxInsertItemType
            // 
            this._ctxInsertItemType.Name = "_ctxInsertItemType";
            this._ctxInsertItemType.OwnerItem = this._cmiInsertItem;
            this._ctxInsertItemType.Size = new System.Drawing.Size(61, 4);
            this._ctxInsertItemType.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this._ctxInsertItemType_ItemClicked);
            // 
            // _tbInsertItem
            // 
            this._tbInsertItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbInsertItem.DropDown = this._ctxInsertItemType;
            this._tbInsertItem.DropDownButtonWidth = 13;
            this._tbInsertItem.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.InsertItemIco;
            this._tbInsertItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbInsertItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbInsertItem.Name = "_tbInsertItem";
            this._tbInsertItem.Size = new System.Drawing.Size(34, 22);
            this._tbInsertItem.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.InsertItemToolTip");
            this._tbInsertItem.Click += new System.EventHandler(this._tbInsertItem_Click);
            // 
            // _cmiAddChild
            // 
            this._cmiAddChild.DropDown = this._ctxAddChildType;
            this._cmiAddChild.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.AddItemIco;
            this._cmiAddChild.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiAddChild.Name = "_cmiAddChild";
            this._cmiAddChild.Size = new System.Drawing.Size(164, 22);
            this._cmiAddChild.Text = C1Localizer.GetString("Base.ItemsEditorForm.AddChild");
            // 
            // _ctxAddChildType
            // 
            this._ctxAddChildType.Name = "_ctxAddChildType";
            this._ctxAddChildType.OwnerItem = this._cmiAddChild;
            this._ctxAddChildType.Size = new System.Drawing.Size(61, 4);
            this._ctxAddChildType.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this._ctxAddChildType_ItemClicked);
            // 
            // _tbAddChild
            // 
            this._tbAddChild.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbAddChild.DropDown = this._ctxAddChildType;
            this._tbAddChild.DropDownButtonWidth = 13;
            this._tbAddChild.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.AddItemIco;
            this._tbAddChild.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbAddChild.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbAddChild.Name = "_tbAddChild";
            this._tbAddChild.Size = new System.Drawing.Size(34, 22);
            this._tbAddChild.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.AddChildToolTip");
            this._tbAddChild.ButtonClick += new System.EventHandler(this._tbAddChild_ButtonClick);
            // 
            // _cmiSep1
            // 
            this._cmiSep1.Name = "_cmiSep1";
            this._cmiSep1.Size = new System.Drawing.Size(161, 6);
            // 
            // _cmiCut
            // 
            this._cmiCut.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.CutIco;
            this._cmiCut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiCut.Name = "_cmiCut";
            this._cmiCut.Size = new System.Drawing.Size(164, 22);
            this._cmiCut.Text = C1Localizer.GetString("Base.ItemsEditorForm.Cut");
            this._cmiCut.Click += new System.EventHandler(this._cmiCut_Click);
            // 
            // _cmiCopy
            // 
            this._cmiCopy.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.CopyIco;
            this._cmiCopy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiCopy.Name = "_cmiCopy";
            this._cmiCopy.Size = new System.Drawing.Size(164, 22);
            this._cmiCopy.Text = C1Localizer.GetString("Base.ItemsEditorForm.Copy");
            this._cmiCopy.Click += new System.EventHandler(this._cmiCopy_Click);
            // 
            // _cmiPaste
            // 
            this._cmiPaste.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.PasteIco;
            this._cmiPaste.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiPaste.Name = "_cmiPaste";
            this._cmiPaste.Size = new System.Drawing.Size(164, 22);
            this._cmiPaste.Text = C1Localizer.GetString("Base.ItemsEditorForm.Paste");
            this._cmiPaste.Click += new System.EventHandler(this._cmiPaste_Click);
            // 
            // _cmiSep2
            // 
            this._cmiSep2.Name = "_cmiSep2";
            this._cmiSep2.Size = new System.Drawing.Size(161, 6);
            // 
            // _cmiDelete
            // 
            this._cmiDelete.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.DeleteIco;
            this._cmiDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiDelete.Name = "_cmiDelete";
            this._cmiDelete.Size = new System.Drawing.Size(164, 22);
            this._cmiDelete.Text = C1Localizer.GetString("Base.ItemsEditorForm.Delete");
            this._cmiDelete.Click += new System.EventHandler(this._cmiDelete_Click);
            // 
            // _cmiSep3
            // 
            this._cmiSep3.Name = "_cmiSep3";
            this._cmiSep3.Size = new System.Drawing.Size(161, 6);
            // 
            // _cmiRename
            // 
            this._cmiRename.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.RenameIco;
            this._cmiRename.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiRename.Name = "_cmiRename";
            this._cmiRename.Size = new System.Drawing.Size(164, 22);
            this._cmiRename.Text = C1Localizer.GetString("Base.ItemsEditorForm.Rename");
            this._cmiRename.Click += new System.EventHandler(this._cmiRename_Click);
            // 
            // _cmiSep4
            // 
            this._cmiSep4.Name = "_cmiSep4";
            this._cmiSep4.Size = new System.Drawing.Size(161, 6);
            // 
            // _cmiChangeToType
            // 
            this._cmiChangeToType.DropDown = this._ctxChangeItemType;
            this._cmiChangeToType.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ToLinkItemIco;
            this._cmiChangeToType.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._cmiChangeToType.Name = "_cmiChangeToType";
            this._cmiChangeToType.Size = new System.Drawing.Size(164, 22);
            this._cmiChangeToType.Text = C1Localizer.GetString("Base.ItemsEditorForm.ChangeType");
            // 
            // _ctxChangeItemType
            // 
            this._ctxChangeItemType.Name = "_ctxChangeItemType";
            this._ctxChangeItemType.Size = new System.Drawing.Size(61, 4);
            this._ctxChangeItemType.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this._ctxChangeItemType_ItemClicked);
            // 
            // _tbChangeItemType
            // 
            this._tbChangeItemType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbChangeItemType.DropDown = this._ctxChangeItemType;
            this._tbChangeItemType.DropDownButtonWidth = 13;
            this._tbChangeItemType.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ToLinkItemIco;
            this._tbChangeItemType.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbChangeItemType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbChangeItemType.Name = "_tbChangeItemType";
            this._tbChangeItemType.Size = new System.Drawing.Size(34, 22);
            this._tbChangeItemType.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.ChangeTypeToolTip");
            this._tbChangeItemType.ButtonClick += new System.EventHandler(this._tbChangeItemType_ButtonClick);
            // 
            // _miEdit
            // 
            this._miEdit.DropDown = this._ctxEditContext;
            this._miEdit.Name = "_miEdit";
            this._miEdit.Size = new System.Drawing.Size(37, 20);
            this._miEdit.Text = C1Localizer.GetString("Base.ItemsEditorForm.Edit");
            // 
            // _mainTreeViewImageList
            // 
            this._mainTreeViewImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this._mainTreeViewImageList.ImageSize = new System.Drawing.Size(16, 16);
            this._mainTreeViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // _mainTabControl
            // 
            this._mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._mainTabControl.Controls.Add(this._tabEdit);
            this._mainTabControl.Controls.Add(this._tabPreview);
            this._mainTabControl.Location = new System.Drawing.Point(0, 54);
            this._mainTabControl.Name = "_mainTabControl";
            this._mainTabControl.SelectedIndex = 0;
            this._mainTabControl.Size = new System.Drawing.Size(492, 428);
            this._mainTabControl.TabIndex = 3;
            this._mainTabControl.TabStop = false;
            this._mainTabControl.SelectedIndexChanged += new System.EventHandler(this._mainTabControl_SelectedIndexChanged);
            this._mainTabControl.SizeChanged += new System.EventHandler(this._mainTabControl_SizeChanged);
            // 
            // _tabPreview
            // 
            this._tabPreview.Controls.Add(this._panelPreviewHost);
            this._tabPreview.Location = new System.Drawing.Point(4, 22);
            this._tabPreview.Name = "_tabPreview";
            this._tabPreview.Size = new System.Drawing.Size(484, 402);
            this._tabPreview.TabIndex = 1;
            this._tabPreview.Text = C1Localizer.GetString("Base.ItemsEditorForm.Preview");
            this._tabPreview.UseVisualStyleBackColor = true;
            this._tabPreview.Visible = false;
            // 
            // _panelPreviewHost
            // 
            this._panelPreviewHost.Controls.Add(this._previewer);
            this._panelPreviewHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelPreviewHost.Location = new System.Drawing.Point(0, 0);
            this._panelPreviewHost.Name = "_panelPreviewHost";
            this._panelPreviewHost.Size = new System.Drawing.Size(484, 402);
            this._panelPreviewHost.TabIndex = 5;
            // 
            // _panelBott
            // 
            this._panelBott.Controls.Add(this._btnHelp);
            this._panelBott.Controls.Add(this._btnOk);
            this._panelBott.Controls.Add(this._btnCancel);
            this._panelBott.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panelBott.Location = new System.Drawing.Point(0, 491);
            this._panelBott.Name = "_panelBott";
            this._panelBott.Size = new System.Drawing.Size(492, 32);
            this._panelBott.TabIndex = 4;
            // 
            // _btnHelp
            // 
            this._btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._btnHelp.Location = new System.Drawing.Point(412, 4);
            this._btnHelp.Name = "_btnHelp";
            this._btnHelp.Size = new System.Drawing.Size(75, 23);
            this._btnHelp.TabIndex = 2;
            this._btnHelp.Text = C1Localizer.GetString("Base.ItemsEditorForm.Help");
            this._btnHelp.Visible = false;
            // 
            // _btnOk
            // 
            this._btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._btnOk.Location = new System.Drawing.Point(332, 4);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(75, 23);
            this._btnOk.TabIndex = 0;
            this._btnOk.Text = C1Localizer.GetString("Base.ItemsEditorForm.OK");
            // 
            // _btnCancel
            // 
            this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._btnCancel.Location = new System.Drawing.Point(411, 4);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(75, 23);
            this._btnCancel.TabIndex = 1;
            this._btnCancel.Text = C1Localizer.GetString("Base.ItemsEditorForm.Cancel");
            // 
            // _saveFileDialog
            // 
            this._saveFileDialog.DefaultExt = "xml";
            this._saveFileDialog.FileName = "WijmoControl";
            this._saveFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            // 
            // _openFileDialog
            // 
            this._openFileDialog.DefaultExt = "xml";
            this._openFileDialog.Filter = "XML Files (.xml)|*.xml|All Files (*.*)|*.*";
            // 
            // _mainMenu
            // 
            this._mainMenu.BackColor = System.Drawing.SystemColors.ScrollBar;
            this._mainMenu.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._miFile,
            this._miEdit,
            this._miHelp});
            this._mainMenu.Location = new System.Drawing.Point(0, 0);
            this._mainMenu.Name = "_mainMenu";
            this._mainMenu.Size = new System.Drawing.Size(492, 24);
            this._mainMenu.TabIndex = 9;
            // 
            // _miFile
            // 
            this._miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._miLoadXML,
            this._miSaveXML,
            this._miFileSep1,
            this._miExit});
            this._miFile.Name = "_miFile";
            this._miFile.Size = new System.Drawing.Size(35, 20);
            this._miFile.Text = C1Localizer.GetString("Base.ItemsEditorForm.File");
            // 
            // _miLoadXML
            // 
            this._miLoadXML.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.XmlLoadIco;
            this._miLoadXML.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._miLoadXML.Name = "_miLoadXML";
            this._miLoadXML.Size = new System.Drawing.Size(155, 22);
            this._miLoadXML.Text = C1Localizer.GetString("Base.ItemsEditorForm.LoadFromXML");
            this._miLoadXML.Click += new System.EventHandler(this._miLoadXML_Click);
            // 
            // _miSaveXML
            // 
            this._miSaveXML.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.XmlSaveIco;
            this._miSaveXML.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._miSaveXML.Name = "_miSaveXML";
            this._miSaveXML.Size = new System.Drawing.Size(155, 22);
            this._miSaveXML.Text = C1Localizer.GetString("Base.ItemsEditorForm.SaveAsXML");
            this._miSaveXML.Click += new System.EventHandler(this._miSaveXML_Click);
            // 
            // _miFileSep1
            // 
            this._miFileSep1.Name = "_miFileSep1";
            this._miFileSep1.Size = new System.Drawing.Size(152, 6);
            // 
            // _miExit
            // 
            this._miExit.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ExitIco;
            this._miExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._miExit.Name = "_miExit";
            this._miExit.Size = new System.Drawing.Size(155, 22);
            this._miExit.Text = C1Localizer.GetString("Base.ItemsEditorForm.Exit");
            this._miExit.Click += new System.EventHandler(this._miExit_Click);
            // 
            // _miHelp
            // 
            this._miHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._miDynHelp,
            this._miSepHelp1,
            this._miAbout});
            this._miHelp.Name = "_miHelp";
            this._miHelp.Size = new System.Drawing.Size(40, 20);
            this._miHelp.Text = C1Localizer.GetString("Base.ItemsEditorForm.Help");
            this._miHelp.Visible = false;
            // 
            // _miDynHelp
            // 
            this._miDynHelp.Name = "_miDynHelp";
            this._miDynHelp.Size = new System.Drawing.Size(126, 22);
            this._miDynHelp.Text = C1Localizer.GetString("Base.ItemsEditorForm.Help");
            this._miDynHelp.Click += new System.EventHandler(this._miDynHelp_Click);
            // 
            // _miSepHelp1
            // 
            this._miSepHelp1.Name = "_miSepHelp1";
            this._miSepHelp1.Size = new System.Drawing.Size(123, 6);
            // 
            // _miAbout
            // 
            this._miAbout.Name = "_miAbout";
            this._miAbout.Size = new System.Drawing.Size(126, 22);
            this._miAbout.Text = C1Localizer.GetString("Base.ItemsEditorForm.About");
            this._miAbout.Click += new System.EventHandler(this._miAbout_Click);
            // 
            // _mainToolBar
            // 
            this._mainToolBar.BackColor = this.BackColor;
            this._mainToolBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this._mainToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tbMoveItemUp,
            this._tbMoveItemDown,
            this._tbMoveItemLeft,
            this._tbMoveItemRight,
            this._tbSep1,
            this._tbAddChild,
            this._tbSep2,
            this._tbInsertItem,
            this._tbSep3,
            this._tbCut,
            this._tbCopy,
            this._tbPaste,
            this._tbSep4,
            this._tbDelete,
            this._tbSep5,
            this._tbChangeItemType});
            this._mainToolBar.Location = new System.Drawing.Point(0, 24);
            this._mainToolBar.Name = "_mainToolBar";
            this._mainToolBar.Size = new System.Drawing.Size(492, 25);
            this._mainToolBar.TabIndex = 11;
            // 
            // _tbMoveItemUp
            // 
            this._tbMoveItemUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbMoveItemUp.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowUpIco;
            this._tbMoveItemUp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbMoveItemUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbMoveItemUp.Name = "_tbMoveItemUp";
            this._tbMoveItemUp.Size = new System.Drawing.Size(23, 22);
            this._tbMoveItemUp.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.MoveItemUpToolTip");
            this._tbMoveItemUp.Click += new System.EventHandler(this._tbMoveItemUp_Click);
            // 
            // _tbMoveItemDown
            // 
            this._tbMoveItemDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbMoveItemDown.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowDownIco;
            this._tbMoveItemDown.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbMoveItemDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbMoveItemDown.Name = "_tbMoveItemDown";
            this._tbMoveItemDown.Size = new System.Drawing.Size(23, 22);
            this._tbMoveItemDown.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.MoveItemDownToolTip");
            this._tbMoveItemDown.Click += new System.EventHandler(this._tbMoveItemDown_Click);
            // 
            // _tbMoveItemLeft
            // 
            this._tbMoveItemLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbMoveItemLeft.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowLeftIco;
            this._tbMoveItemLeft.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbMoveItemLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbMoveItemLeft.Name = "_tbMoveItemLeft";
            this._tbMoveItemLeft.Size = new System.Drawing.Size(23, 22);
            this._tbMoveItemLeft.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.MoveItemLeftToolTip");
            this._tbMoveItemLeft.Click += new System.EventHandler(this._tbMoveItemLeft_Click);
            // 
            // _tbMoveItemRight
            // 
            this._tbMoveItemRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbMoveItemRight.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowRightIco;
            this._tbMoveItemRight.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbMoveItemRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbMoveItemRight.Name = "_tbMoveItemRight";
            this._tbMoveItemRight.Size = new System.Drawing.Size(23, 22);
            this._tbMoveItemRight.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.MoveItemRightToolTip");
            this._tbMoveItemRight.Click += new System.EventHandler(this._tbMoveItemRight_Click);
            // 
            // _tbSep1
            // 
            this._tbSep1.Name = "_tbSep1";
            this._tbSep1.Size = new System.Drawing.Size(6, 25);
            // 
            // _tbSep2
            // 
            this._tbSep2.Name = "_tbSep2";
            this._tbSep2.Size = new System.Drawing.Size(6, 25);
            // 
            // _tbSep3
            // 
            this._tbSep3.Name = "_tbSep3";
            this._tbSep3.Size = new System.Drawing.Size(6, 25);
            // 
            // _tbCut
            // 
            this._tbCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbCut.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.CutIco;
            this._tbCut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbCut.Name = "_tbCut";
            this._tbCut.Size = new System.Drawing.Size(23, 22);
            this._tbCut.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.CutToolTip");
            this._tbCut.Click += new System.EventHandler(this._tbCut_Click);
            // 
            // _tbCopy
            // 
            this._tbCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbCopy.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.CopyIco;
            this._tbCopy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbCopy.Name = "_tbCopy";
            this._tbCopy.Size = new System.Drawing.Size(23, 22);
            this._tbCopy.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.CopyToolTip");
            this._tbCopy.Click += new System.EventHandler(this._tbCopy_Click);
            // 
            // _tbPaste
            // 
            this._tbPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbPaste.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.PasteIco;
            this._tbPaste.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbPaste.Name = "_tbPaste";
            this._tbPaste.Size = new System.Drawing.Size(23, 22);
            this._tbPaste.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.PasteToolTip");
            this._tbPaste.Click += new System.EventHandler(this._tbPaste_Click);
            // 
            // _tbSep4
            // 
            this._tbSep4.Name = "_tbSep4";
            this._tbSep4.Size = new System.Drawing.Size(6, 25);
            // 
            // _tbDelete
            // 
            this._tbDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tbDelete.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.DeleteIco;
            this._tbDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._tbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tbDelete.Name = "_tbDelete";
            this._tbDelete.Size = new System.Drawing.Size(23, 22);
            this._tbDelete.ToolTipText = C1Localizer.GetString("Base.ItemsEditorForm.DeleteToolTip");
            this._tbDelete.Click += new System.EventHandler(this._tbDelete_Click);
            // 
            // _tbSep5
            // 
            this._tbSep5.Name = "_tbSep5";
            this._tbSep5.Size = new System.Drawing.Size(6, 25);
            // 
            // contextMenuStrip_ForPropertyGrid
            // 
            this.contextMenuStrip_ForPropertyGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStrip_ForPropertyGrid.Name = "contextMenuStrip_ForPropertyGrid";
            this.contextMenuStrip_ForPropertyGrid.Size = new System.Drawing.Size(153, 48);
            this.contextMenuStrip_ForPropertyGrid.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_ForPropertyGrid_Opening);
            this.contextMenuStrip_ForPropertyGrid.Click += new System.EventHandler(this.contextMenuStrip_ForPropertyGrid_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem1.Text = C1Localizer.GetString("Base.ItemsEditorForm.Reset");
            // 
            // C1BaseItemEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 523);
            this.Controls.Add(this._mainToolBar);
            this.Controls.Add(this._mainMenu);
            this.Controls.Add(this._mainTabControl);
            this.Controls.Add(this._panelBott);
            this.MinimumSize = new System.Drawing.Size(500, 550);
            this.Name = "C1BaseItemEditorForm";
            this.Text = "C1BaseItemEditorForm";
            this.Load += new System.EventHandler(this.C1BaseItemEditorForm_Load);
            this._tabEdit.ResumeLayout(false);
            this._ctxEditContext.ResumeLayout(false);
            this._mainTabControl.ResumeLayout(false);
            this._tabPreview.ResumeLayout(false);
            this._panelPreviewHost.ResumeLayout(false);
            this._panelBott.ResumeLayout(false);
            this._mainMenu.ResumeLayout(false);
            this._mainMenu.PerformLayout();
            this._mainToolBar.ResumeLayout(false);
            this._mainToolBar.PerformLayout();
            this.contextMenuStrip_ForPropertyGrid.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void _previewer_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            ShowPreviewHtml(_previewer);
        }

        #endregion  // C1BaseItemEditorForm definition


        #region ** Constructor

        public C1BaseItemEditorForm(System.Web.UI.Control control)
        {
            InitializeComponent();
            _control = control;
        }        

        #endregion  //Constructor


        #region ** Properties

        /// <summary>
        /// Allows move items up.
        /// </summary>
        protected bool AllowMoveUp
        {
            get { return _allowMoveUp; }
            set { _allowMoveUp = value; }
        }

        /// <summary>
        /// Allows move items down.
        /// </summary>
        protected bool AllowMoveDown
        {
            get { return _allowMoveDown; }
            set { _allowMoveDown = value; }
        }

        /// <summary>
        /// Allows move items left.
        /// </summary>
        protected bool AllowMoveLeft
        {
            get { return _allowMoveLeft; }
            set { _allowMoveLeft = value; }
        }

        /// <summary>
        /// Allows move items right.
        /// </summary>
        protected bool AllowMoveRight
        {
            get { return _allowMoveRight; }
            set { _allowMoveRight = value; }
        }

        /// <summary>
        /// Allows adding items.
        /// </summary>
        protected bool AllowAdd
        {
            get { return _allowAdd; }
            set { _allowAdd = value; }
        }

        /// <summary>
        /// Allows inserting items.
        /// </summary>
        protected bool AllowInsert
        {
            get { return _allowInsert; }
            set { _allowInsert = value; }
        }

        /// <summary>
        /// Allows changing items type.
        /// </summary>
        protected bool AllowChangeType
        {
            get { return _allowChangeType; }
            set { _allowChangeType = value; }
        }

        /// <summary>
        /// Allows cut action.
        /// </summary>
        protected bool AllowCut
        {
            get { return _allowCut; }
            set { _allowCut = value; }
        }
        /// <summary>
        /// Allows copy action.
        /// </summary>
        protected bool AllowCopy
        {
            get { return _allowCopy; }
            set { _allowCopy = value; }
        }

        /// <summary>
        /// Allows paste action.
        /// </summary>
        protected bool AllowPaste
        {
            get { return _allowPaste; }
            set { _allowPaste = value; }
        }

        /// <summary>
        /// Allows delete action.
        /// </summary>
        protected bool AllowDelete
        {
            get { return _allowDelete; }
            set { _allowDelete = value; }
        }

        /// <summary>
        /// Allows rename action.
        /// </summary>
        protected bool AllowRename
        {
            get { return _allowRename; }
            set { _allowRename = value; }
        }

        protected bool AllowSaveXML
        {
            get { return _allowSaveXml; }
            set { _allowSaveXml = value; }
        }
        /// <summary>
        /// Indicates if clipboard has some data. This property is used for 
        /// derived classes to indicate to base class that has data for paste. 
        /// </summary>
        protected bool ClipboardData
        {
            get { return _clipboardData; }
            set { _clipboardData = value; }
        }

        /// <summary>
        /// Retrieves selected node from MainMenuTree 
        /// </summary>
        private TreeNode SelectedNode
        {
            get 
            {
                if (this.MainTreeView.SelectedNode != null)
                    return this.MainTreeView.SelectedNode;

                return this.MainTreeView.TopNode;
            }
            set { this.MainTreeView.SelectedNode = value; }
        }

        /// <summary>
        /// Get access to base Main Menu
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        private System.Windows.Forms.MenuStrip MainMenu
        {
            get { return this._mainMenu; }
        }

        /// <summary>
        /// Get access to Main ToolBar
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        private ToolStrip MainToolBar
        {
            get { return _mainToolBar; }
        }

        /// <summary>
        /// Get access to PropertyGrid 
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        protected PropertyGrid MainPropertyGrid //Modifier changed by Dmitri Marfin
        {
            get { return this._mainPropertyGrid; }
        }

        /// <summary>
        /// Get access to base Menu Tree
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        private System.Windows.Forms.TreeView MainTreeView
        {
            get { return this._mainTreeView; }
        }

        /// <summary>
        /// Get access to base Tab Control
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        //Add the following code by RyanWu@20090817.
        //For fixing the issue#5150.
        //private System.Windows.Forms.TabControl MainTabControl
        protected System.Windows.Forms.TabControl MainTabControl
        //end by RyanWu@20090817.
        {
            get { return this._mainTabControl; }
        }

        /// <summary>
        /// Get access to base Previwer
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        internal C1WebBrowser MainPreviewer //Modifier changed by Dmitri Marfin
        {
            get { return this._previewer; }
        }

        /// <summary>
        /// Get access to AddChildType Context Menu 
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        protected ContextMenuStrip AddChildTypeContextMenu
        {
            get { return this._ctxAddChildType; }
        }

        /// <summary>
        /// Get access to InsertItemType Context Menu
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        protected ContextMenuStrip InsertItemTypeContextMenu
        {
            get { return this._ctxInsertItemType; }
        }

        /// <summary>
        /// Get access to ChangeItemType Context Menu 
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        protected ContextMenuStrip ChangeItemTypeContextMenu
        {
            get { return this._ctxChangeItemType; }
        }

        /// <summary>
        /// Get access to Main TreeView ImageList
        /// </summary>
        [Browsable(false)]
        [Category("Inherited Form Controls")]
        protected ImageList MainTreeViewImageList
        {
            get { return this._mainTreeViewImageList; }
        }

        /// <summary>
        /// Gets the allowable types that can compose the control.
        /// </summary>
        protected List<C1ItemInfo> ItemsInfo
        {
            get { return _itemsInfo; }
            set { _itemsInfo = value; }
        }

        /// <summary>
        /// Indicates if selected tab from control tab is Edition tab
        /// </summary>
        private bool IsEditMode
        {
            get { return this.MainTabControl.SelectedTab == _tabEdit; }
        }


        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom an Add Child action.
        /// </summary>
        private Component[] CommandsAddChild
        {
            get
            {
                if (_commandsAddChild == null)
                    _commandsAddChild = new Component[] { this._tbAddChild, this._cmiAddChild, this._ctxAddChildType };
                return _commandsAddChild;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom an Insert Item action.
        /// </summary>
        private Component[] CommandsInsertItem
        {
            get
            {
                if (_commandsInsertItem == null)
                    _commandsInsertItem = new Component[] { this._tbInsertItem, this._cmiInsertItem, this._ctxInsertItemType };
                return _commandsInsertItem;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Change Item Type action.
        /// </summary>
        private Component[] CommandsChangeItemType
        {
            get
            {
                if (_commandsChangeItemType == null)
                    _commandsChangeItemType = new Component[] { this._tbChangeItemType, this._cmiChangeToType, this._ctxChangeItemType };
                return _commandsChangeItemType;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Cut action.
        /// </summary>
        private Component[] CommandsCut
        {
            get
            {
                if (_commandsCut == null)
                    _commandsCut = new Component[] { this._tbCut, this._cmiCut };
                return _commandsCut;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Copy action.
        /// </summary>
        private Component[] CommandsCopy
        {
            get
            {
                if (_commandsCopy == null)
                    _commandsCopy = new Component[] { this._tbCopy, this._cmiCopy };
                return _commandsCopy;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Paste action.
        /// </summary>
        private Component[] CommandsPaste
        {
            get
            {
                if (_commandsPaste == null)
                    _commandsPaste = new Component[] { this._tbPaste, this._cmiPaste };
                return _commandsPaste;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Delete action.
        /// </summary>
        private Component[] CommandsDelete
        {
            get
            {
                if (_commandsDelete == null)
                    _commandsDelete = new Component[] { this._tbDelete, this._cmiDelete };
                return _commandsDelete;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Move Up action.
        /// </summary>
        private Component[] CommandsMoveUp
        {
            get
            {
                if (_commandsMoveUp == null)
                    _commandsMoveUp = new Component[] { this._tbMoveItemUp };
                return _commandsMoveUp;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Move Down action.
        /// </summary>
        private Component[] CommandsMoveDown
        {
            get
            {
                if (_commandsMoveDown == null)
                    _commandsMoveDown = new Component[] { this._tbMoveItemDown };
                return _commandsMoveDown;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Move Left action.
        /// </summary>
        private Component[] CommandsMoveLeft
        {
            get
            {
                if (_commandsMoveLeft == null)
                    _commandsMoveLeft = new Component[] { this._tbMoveItemLeft };
                return _commandsMoveLeft;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Move Left action.
        /// </summary>
        private Component[] CommandsMoveRight
        {
            get
            {
                if (_commandsMoveRight == null)
                    _commandsMoveRight = new Component[] { this._tbMoveItemRight };
                return _commandsMoveRight;
            }
        }

        /// <summary>
        /// Gets access to all (ToolStripButtons, etc) that can perfom a Rename action.
        /// </summary>
        private Component[] CommandsRename
        {
            get
            {
                if (_commandsRename == null)
                    _commandsRename = new Component[] { this._cmiRename };
                return _commandsRename;
            }
        }

        private Component[] CommandsSaveXml
        {
            get
            {
                if (_commandsSaveXml == null)
                    _commandsSaveXml = new Component[] { this._miLoadXML, this._miSaveXML };

                return _commandsSaveXml;
            }
        }
        /// <summary>
        /// Gets access to _saveFileDialog
        /// </summary>
        private SaveFileDialog SaveFileDialog
        {
            get { return this._saveFileDialog; }
        }

        /// <summary>
        /// Gets access to _openFileDialog
        /// </summary>
        private OpenFileDialog OpenFileDialog
        {
            get { return this._openFileDialog; }
        }

        #endregion  // Properties


        #region ** Virtuals

        /// <summary>
        /// Called to initialize the treeview with the object hierarchy.
        /// </summary>
        /// <param name="mainTreeView">The main treeview to be filled.</param>
        protected virtual void LoadControl(TreeView mainTreeView)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Sets the Site property of main property grid with Site property value of C1 control. 
        /// </summary>
        /// <param name="propertyGridSite">The System.ComponentModel.ISite associated with the System.Windows.Forms.Control, if any.</param>
        private void SetPropertyGridSite()
        {
            this.MainPropertyGrid.Site = this._control.Site;
        }

        /// <summary>
        /// Fills the allowable types that can compose the control.
        /// </summary>
        /// <returns>List of C1ItemInfo objects.</returns>
        protected virtual List<C1ItemInfo> FillAvailableControlItems()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates status of controls of MainToolbar, MainMenu and context menus.
        /// </summary>
        /// <param name="itemInfo">C1ItemInfo of current selected item.</param>
        /// <param name="parentItemInfo">C1ItemInfo of parent of current selected item.</param>
        /// <param name="previousItemInfo">C1ItemInfo of previous item of current selected item</param>
        protected virtual void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
        {
            // base class takes control of move up action
            this.SetEnableCommands(CommandsMoveUp, !this.IsFirstNodeInLevel);
            // base class takes control of move down action
            this.SetEnableCommands(CommandsMoveDown, !this.IsLastNodeInLevel);
            this.SetEnableCommands(CommandsMoveLeft, this.AllowMoveLeft);
            this.SetEnableCommands(CommandsMoveRight, this.AllowMoveRight);

            this.SetEnableCommands(CommandsAddChild, this.AllowAdd);
            this.SetEnableCommands(CommandsInsertItem, this.AllowInsert);
            this.SetEnableCommands(CommandsChangeItemType, this.AllowChangeType);

            this.SetEnableCommands(CommandsCopy, this.AllowCopy);
            this.SetEnableCommands(CommandsCut, this.AllowCut);
            this.SetEnableCommands(CommandsPaste, this.AllowPaste && this.ClipboardData);
            this.SetEnableCommands(CommandsDelete, this.AllowDelete);

            this.SetEnableCommands(CommandsRename, this.AllowRename);

            this.SetEnableCommands(CommandsSaveXml, this.AllowSaveXML);
        }

        /// <summary>
        /// Creates the C1NodeInfo object that will contain both the specific C1 control item depending on the itemInfo
        /// and given itenInfo.
        /// </summary>
        /// <param name="itemInfo">C1ItemInfo object.</param>
        /// <param name="nodesList">Nodes that currently exist into the list where the new created item 
        /// will be inserted.</param>
        /// <returns>Returns an C1 control object that depends on the C1ItemInfo. Given nodesList
        /// could be used to obtain a new numbered name.</returns>
        protected virtual C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
        {
            return null;
        }

        /// <summary>
        ///  Indicates when an item was inserted in a specific position.
        /// </summary>
        /// <param name="item">The inserted item.</param>
        /// <param name="destinationItem">The destination item that will contain inserted item.</param>
        /// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
        protected virtual void Insert(object item, object destinationItem, int destinationIndex)
        {
        }

        /// <summary>
        ///  Indicates when an item was deleted.
        /// </summary>
        /// <param name="item">The deleted item.</param>
        protected virtual void Delete(object item)
        {
        }

        /// <summary>
        /// Performs Copy action.
        /// </summary>
        /// <param name="item">The item to copy.</param>
        /// <param name="itemInfo">The item info of the item to be copied.</param>
        protected virtual void Copy(object item, C1ItemInfo itemInfo)
        {
        }

        /// <summary>
        /// Pastes node from clipboard to given destination node.
        /// </summary>
        /// <param name="destinationNode">Destination node.</param>
        protected virtual void Paste(TreeNode destinationNode)
        {
        }

        /// <summary>
        /// Fills C1 control from saved XML file. 
        /// </summary>
        /// <param name="fileName">The name of the XML file to be loaded.</param>
        protected virtual void LoadFromXML(string fileName)
        {
        }

        /// <summary>
        /// Saves C1 control to XML file.
        /// </summary>
        /// <param name="fileName">The name for XML file.</param>
        protected virtual void SaveToXML(string fileName)
        {
        }

        /// <summary>
        /// Executes code when form is closing.
        /// </summary>
        protected virtual void OnExit()
        {
            this.Close();
        }

        /// <summary>
        /// Shows dynamic help
        /// </summary>
        protected virtual void ShowHelp()
        {
        }
        /// <summary>
        /// Shows about form
        /// </summary>
        protected virtual void ShowAbout()
        {
        }

        /// <summary>
        /// Performs an action after treeview label edition.
        /// </summary>
        /// <param name="e">Node label edit event arguments.</param>
        /// <param name="selectedNode">Current selected node.</param>
        protected virtual void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
        {
        }

        /// <summary>
        /// Shows current control into preview tab
        /// </summary>
        /// <param name="previewer">The web browser previewer</param>
        internal virtual void ShowPreviewHtml(C1WebBrowser previewer)
        {
        }

        /// <summary>
        /// Refresh main property grid.
        /// </summary>
        protected void RefreshPropertyGrid()
        {
            this.MainPropertyGrid.Refresh();
        }

        #endregion // Virtuals


        #region ** Methods

        //--> 09 nov. 2008 DMA, fix for TFS issue 1450, 
        // Issue is because C1ClientHandlerUITypeEditor activates page code view 
        // when modal dialog shown.
        
        static internal bool isInModalMode;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Shown"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            C1BaseItemEditorForm.isInModalMode = true;

            //Add comments by RyanWu@20091106.
            //For fixing the issue#7724.
            //#if false
            //// Notify designer that control is changed.
            //// Fix for issue with page dirtying in VS2008SP1
            //try
            //{
            //    PropertyDescriptor pd = TypeDescriptor.GetProperties(_control)["ID"];
            //    object Val = pd.GetValue(_control);
            //    pd.SetValue(_control, "__dirty");
            //    pd.SetValue(_control, Val);
            //}
            //catch (Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show("[e012019] Design time error: " + ex.Message);
            //}
            //#endif
            if (Utils.Common.IsVS2008)
            {
                //Notify designer that control is changed.
                //Fix for issue with page dirtying in VS2008SP1
                try
                {
                    PropertyDescriptor pd = TypeDescriptor.GetProperties(_control)["BorderStyle"];
                    object Val = pd.GetValue(_control);
                    pd.SetValue(_control, System.Web .UI.WebControls.BorderStyle.None);
                    pd.SetValue(_control, Val);
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("[e012019] Design time error: " + ex.Message);
                }
            }
            //end by RyanWu@20091106.
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closed"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            C1BaseItemEditorForm.isInModalMode = false;
        }
        //<-- end of DMA

        /// <summary>
        /// Synchronizes the status of UI controls against selected node
        /// </summary>
        protected void SyncUI()
        {
            if (this.IsEditMode)
            {
                // get C1ItemInfo of current selected node
                C1ItemInfo itemInfo = ((C1NodeInfo)this.SelectedNode.Tag).ItemInfo;
                C1ItemInfo parentItemInfo = null;
                C1ItemInfo previousItemInfo = null;

                // get C1ItemInfo of parent of current selected node
                if (this.SelectedNode.Parent != null)
                {
                    parentItemInfo = ((C1NodeInfo)this.SelectedNode.Parent.Tag).ItemInfo;
                }

                // get C1ItemInfo of previous node of current selected node
                if (this.SelectedNode.PrevNode != null)
                {
                    previousItemInfo = ((C1NodeInfo)this.SelectedNode.PrevNode.Tag).ItemInfo;
                }

                // let to the derived class to set the properties for the action options 
                SyncUI(itemInfo, parentItemInfo, previousItemInfo);
            }
            else
            {
                this.DisableAllCommands();
            }
        }

        /// <summary>
        /// Actions when form is loading.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C1BaseItemEditorForm_Load(object sender, EventArgs e)
        {
            SetUpUIControls();

            // get the available items that can populate the control
            this.ItemsInfo = FillAvailableControlItems();

            // create the image list that is used to display the images for the treeview
            LoadImagesInTreeViewImageList();

            // update the add/insert/change menu items and split buttons
            LoadToolbarAndContextMenuItems();

            LoadControl(this.MainTreeView);
           
        }

        /// <summary>
        /// Sets specialized properties for IU controls.
        /// </summary>
        private void SetUpUIControls()
        {
            SetPropertyGridSite();

//            this.MainPropertyGrid.PropertyTabs.AddTabType(
//                typeof(System.Windows.Forms.Design.EventsTab),
//                PropertyTabScope.Global);
        }

        /// <summary>
        /// Loads available images from ItemsInfo list into TreeView imagelist
        /// </summary>
        private void LoadImagesInTreeViewImageList()
        {
            int index = 0;
            _imageMapper = new Dictionary<Image, int>();
            this.MainTreeViewImageList.Images.Clear();

            // add default image
            this.MainTreeViewImageList.Images.Add(Properties.Resources.DefaultItemIco);
            this._imageMapper.Add(Properties.Resources.DefaultItemIco, index);

            foreach (C1ItemInfo itemInfo in this._itemsInfo)
            {
                if (itemInfo.NodeImage != null)
                {
                    index++;
                    this.MainTreeViewImageList.Images.Add(itemInfo.NodeImage);
                    this._imageMapper.Add(itemInfo.NodeImage, index);
                }
            }
        }

        /// <summary>
        /// Loads allowable item types 
        /// </summary>
        private void LoadToolbarAndContextMenuItems()
        {
            // Counter for items that could be showed into a control container (ToolStrip, MenuStrip or ContextMenuStrip).
            // If counter = 1 then change to type button will be disabled.
            int showedItems = 0;
            foreach (C1ItemInfo itemInfo in this._itemsInfo)
            {
                ToolStripMenuItem menuItem;
                if (itemInfo.Visible)
                {
                    showedItems++;
                    menuItem = new ToolStripMenuItem();
                    menuItem.Text = itemInfo.ContextMenuStripText;
                    menuItem.Image = itemInfo.NodeImage;
                    menuItem.Tag = itemInfo;
                    this.AddChildTypeContextMenu.Items.Add(menuItem);

                    menuItem = new ToolStripMenuItem();
                    menuItem.Text = itemInfo.ContextMenuStripText;
                    menuItem.Image = itemInfo.NodeImage;
                    menuItem.Tag = itemInfo;
                    this.InsertItemTypeContextMenu.Items.Add(menuItem);

                    menuItem = new ToolStripMenuItem();
                    menuItem.Text = itemInfo.ContextMenuStripText;
                    menuItem.Image = itemInfo.NodeImage;
                    menuItem.Tag = itemInfo;
                    this.ChangeItemTypeContextMenu.Items.Add(menuItem);
                }
            }
            if (showedItems <= 1)
            {
                HideChangeTypeButtons();
            }
        }

        /// <summary>
        /// Hides all controls that performs an ChangeType action.
        /// </summary>
        private void HideChangeTypeButtons()
        {
            this._tbChangeItemType.Visible = false;
            this._cmiChangeToType.Visible = false;
            this._ctxChangeItemType.Visible = false;
        }

        /// <summary>
        /// Copy source node collection  to destination node collection.
        /// </summary>
        /// <param name="sourceNodeCollection">Source node collection.</param>
        /// <param name="destinationNodeCollection">Destination node collection.</param>
        private void CloneTreeNodeCollection(TreeNodeCollection sourceNodeCollection, TreeNodeCollection destinationNodeCollection)
        {
            TreeNode newNode;
            foreach (TreeNode sourceNode in sourceNodeCollection)
            {
                newNode = new TreeNode();
                SetNodeAttributes(newNode, sourceNode.Text, sourceNode.Tag);
                CloneTreeNodeCollection(sourceNode.Nodes, newNode.Nodes);
                destinationNodeCollection.Add(newNode);
            }
        }

        protected void SetNodeAttributes(TreeNode node, C1NodeInfo nodeTag)
        {
            node.Text = nodeTag.NodeText;
            node.Tag = nodeTag;
            SetNodeImage(node);
        }

        /// <summary>
        /// Sets given node attributes, including node image.
        /// </summary>
        /// <param name="node">The node that will be updated.</param>
        /// <param name="nodeText">The node text.</param>
        /// <param name="nodeTag">The node tag.</param>
        protected void SetNodeAttributes(TreeNode node, string nodeText, object nodeTag)
        {
            node.Text = nodeText;
            node.Tag = nodeTag;
            SetNodeImage(node);
        }

        /// <summary>
        /// Sets node image for given node.
        /// </summary>
        /// <param name="node">The node that will be updated.</param>
        private void SetNodeImage(TreeNode node)
        {
            C1ItemInfo itemInfo;
            int imageIndex = 0;

            itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;
            // get the index image from image mapper
            if (itemInfo != null) imageIndex = GetImageIndex(itemInfo.NodeImage);
            node.ImageIndex = imageIndex;
            node.SelectedImageIndex = imageIndex;
        }

        /// <summary>
        /// Gets the image index from Image Mapper
        /// </summary>
        /// <param name="nodeImage">Image to look for into Image Mapper</param>
        /// <returns>Image index into Image Mapper</returns>
        private int GetImageIndex(Image nodeImage)
        {
            if (nodeImage != null)
            {
                int imageIndex;
                this._imageMapper.TryGetValue(nodeImage, out imageIndex);
                return imageIndex;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Check if any control in given commands is enabled
        /// </summary>
        /// <param name="commands"></param>
        /// <returns></returns>
        private bool GetCommandsEnabled(Component[] commands)
        {
            bool commandsEnabled = false;
            foreach (Component control in commands)
            {
                commandsEnabled |= GetComponentEnabled(control);
            }
            return commandsEnabled;
        }

        /// <summary>
        /// Sets commands enable status. 
        /// </summary>
        /// <param name="commands">Commands group</param>
        /// <param name="enable">Enable state</param>
        private void SetEnableCommands(Component[] commands, bool enable)
        {
            foreach (Component control in commands)
            {
                if (GetComponentEnabled(control) != enable)
                    SetComponentEnabled(control, enable);
            }
        }

        /// <summary>
        /// Gets component enable status 
        /// </summary>
        /// <param name="component">Given component to get enable status</param>
        /// <returns>Component enable status</returns>
        private bool GetComponentEnabled(Component component)
        {
            if (component is System.Windows.Forms.Control)
                return ((System.Windows.Forms.Control)component).Enabled;
            else if (component is System.Windows.Forms.ToolStripButton)
                return ((System.Windows.Forms.ToolStripButton)component).Enabled;
            else if (component is System.Windows.Forms.ToolStripMenuItem)
                return ((System.Windows.Forms.ToolStripMenuItem)component).Enabled;
            else if (component is System.Windows.Forms.ToolStripDropDownButton)
                return ((System.Windows.Forms.ToolStripDropDownButton)component).Enabled;
            else if (component is System.Windows.Forms.ToolStripSplitButton)
                return ((System.Windows.Forms.ToolStripSplitButton)component).Enabled;
            else
                throw new Exception(C1Localizer.GetString("Base.ItemsEditorForm.InvalidTypeException") + component.GetType().Name);
        }

        /// <summary>
        /// Sets component enable status 
        /// </summary>
        /// <param name="component">Given component to set enable status</param>
        /// <param name="value">Value for Enable property</param>
        private void SetComponentEnabled(Component component, bool value)
        {
            if (component is System.Windows.Forms.Control)
                ((System.Windows.Forms.Control)component).Enabled = value;
            else if (component is System.Windows.Forms.ToolStripButton)
                ((System.Windows.Forms.ToolStripButton)component).Enabled = value;
            else if (component is System.Windows.Forms.ToolStripMenuItem)
                ((System.Windows.Forms.ToolStripMenuItem)component).Enabled = value;
            else if (component is System.Windows.Forms.ToolStripDropDownButton)
                ((System.Windows.Forms.ToolStripDropDownButton)component).Enabled = value;
            else if (component is System.Windows.Forms.ToolStripSplitButton)
                ((System.Windows.Forms.ToolStripSplitButton)component).Enabled = value;
            else
                throw new Exception(C1Localizer.GetString("Base.ItemsEditorForm.InvalidTypeException") + component.GetType().Name);
        }

        /// <summary>
        /// Disables all commands
        /// </summary>
        private void DisableAllCommands()
        {
            SetEnableCommands(CommandsRename, false);
            SetEnableCommands(CommandsCopy, false);
            SetEnableCommands(CommandsCut, false);
            SetEnableCommands(CommandsDelete, false);
            SetEnableCommands(CommandsMoveDown, false);
            SetEnableCommands(CommandsMoveLeft, false);
            SetEnableCommands(CommandsMoveRight, false);
            SetEnableCommands(CommandsMoveUp, false);
            SetEnableCommands(CommandsPaste, false);

            if (_commandsAddChild != null)
                SetEnableCommands(CommandsAddChild, false);
            if (_commandsInsertItem != null)
                SetEnableCommands(CommandsInsertItem, false);
            if (_commandsChangeItemType != null)
                SetEnableCommands(CommandsChangeItemType, false);
        }

        /// <summary>
        /// InsertItemType context menu click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ctxInsertItemType_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            C1ItemInfo itemInfo = (C1ItemInfo)e.ClickedItem.Tag;
            Insert(itemInfo, ActionMode.Insert);
        }

        /// <summary>
        /// Inserts an item.
        /// </summary>
        /// <param name="itemInfo">C1ItemInfo object.</param>
        /// <param name="actionMode">Indicates if inserting mode is insert or add.</param>
        private void Insert(C1ItemInfo itemInfo, ActionMode actionMode)
        {
            if (this.SelectedNode == null) return;

            TreeNodeCollection nodeCollection;
            TreeNode itemNode = null;
            C1NodeInfo nodeInfo = null;
            int index = 0;

            if (itemInfo == null)
                // create a default C1ItemInfo
                itemInfo = this.GetDefaultItemInfo();
            // can't find a default iteminfo
            if (itemInfo == null)
            {
                return;
            }

            //Willow Yang add comments at March 9,2009 to fix bug 2076
            if (this.SelectedNode.IsEditing)
            {
                this.SelectedNode.EndEdit(true);
            }
            //end comments

            switch (actionMode)
            {
                case ActionMode.Add:
                    {
                        nodeCollection = this.SelectedNode.Nodes;
                        index = nodeCollection.Count;
                        // get new item from overriden
                        nodeInfo = CreateNodeInfo(itemInfo, nodeCollection);
                        // create new tree node
                        itemNode = new TreeNode();
                        SetNodeAttributes(itemNode, nodeInfo.NodeText, nodeInfo);
                        // add new item to collection
                        nodeCollection.Add(itemNode);
                        // saves last added item info
                        _lastAddedItemInfo = itemInfo;
                        // pass to overridden the added item info
                        Insert(((C1NodeInfo)itemNode.Tag).Element, ((C1NodeInfo)this.SelectedNode.Tag).Element, index);
                        break;
                    }
                case ActionMode.Insert:
                    {
                        if (this.SelectedNode.Parent != null)
                        {
                            nodeCollection = this.SelectedNode.Parent.Nodes;
                            index = this.SelectedNode.Index;
                            // get new item from overriden
                            nodeInfo = CreateNodeInfo(itemInfo, nodeCollection);
                            // create new tree node
                            itemNode = new TreeNode();
                            SetNodeAttributes(itemNode, nodeInfo.NodeText, nodeInfo);
                            // insert new item to collection
                            nodeCollection.Insert(this.SelectedNode.Index, itemNode);
                            // saves last inserted item info
                            _lastInsertedItemInfo = itemInfo;
                            // pass to overridden the inserted item info
                            Insert(((C1NodeInfo)itemNode.Tag).Element, ((C1NodeInfo)this.SelectedNode.Parent.Tag).Element, index);
                        }
                        break;
                    }
            }
            SetNodeImage(itemNode);
//            this.SelectedNode = itemNode;
            this.SelectedNode.Expand();
        }

        /// <summary>
        /// Changes item type.
        /// </summary>
        /// <param name="itemInfo">C1ItemInfo object</param>
        private void ChangeToType(C1ItemInfo itemInfo)
        {
            // save old node
            TreeNode oldNode = this.SelectedNode;

            if (ConfirmeChangeType(oldNode,itemInfo))
            {
                // insert new item type
                Insert(itemInfo, ActionMode.Insert);
                // delete old node
                Delete(oldNode);
                // saves last changed item info
                _lastChangedItemInfo = itemInfo;
            }
        }

        /// <summary>
        /// Loads a C1 control from saved XML file.
        /// </summary>
        private void LoadFromXML()
        {
            if (this.OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                string xmlFileName = this.OpenFileDialog.FileName;
                LoadFromXML(xmlFileName);
                LoadControl(this.MainTreeView);
                if (!this.IsEditMode)
                {
                    this._previewer.Navigate("about:blank");
                    //ShowPreviewHtml(this.MainPreviewer);
                }
            }
        }

        /// <summary>
        /// Saves a C1 control to XML file.
        /// </summary>
        private void SaveToXML()
        {
			if (this._control != null && !string.IsNullOrEmpty(this._control.ID))
			{
				this.SaveFileDialog.FileName = this._control.ID;
			}
            if (this.SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string xmlFileName = this.SaveFileDialog.FileName;
                SaveToXML(xmlFileName);
            }
        }

        /// <summary>
        /// Gets the default item info.
        /// </summary>
        /// <returns>Default C1ItemInfo object.</returns>
        private C1ItemInfo GetDefaultItemInfo()
        {
            foreach (C1ItemInfo itemInfo in this.ItemsInfo)
            {
                if (itemInfo.Default)
                    return itemInfo;
            }
            return null;
        }

        /// <summary>
        /// Confirms changing type.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool ConfirmeChangeType(TreeNode node,C1ItemInfo newItemInfo)
        {
            //Willow Yang add comments on July 8,2009 to fix bug 4913.
            if (newItemInfo == null)
                // create a default C1ItemInfo
                newItemInfo = this.GetDefaultItemInfo();

            if (newItemInfo.ItemType == ((C1NodeInfo)node.Tag).ItemInfo.ItemType)
            {
                return false;
            }
            //end comments.
            if (node.Nodes.Count > 0)
                if (MessageBox.Show(C1Localizer.GetString("Base.ItemsEditorForm.ConfirmChangeType"), C1Localizer.GetString("Base.ItemsEditorForm.Confirmation"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return false;
            return true;
        }

        /// <summary>
        /// Moves selected node to one position up
        /// </summary>
        private void MoveItemUp()
        {
            if (this.SelectedNode.Index > 0) // First Node
            {
                TreeNode currentNode = this.SelectedNode;
                TreeNode parent = currentNode.Parent;
                int index = currentNode.Index;

                // pass to derived class made changes, so it can update it's object hierarchy.
                Delete(((C1NodeInfo)currentNode.Tag).Element);
                Insert(((C1NodeInfo)currentNode.Tag).Element, ((C1NodeInfo)parent.Tag).Element, index - 1);
                

                currentNode.Remove();
                parent.Nodes.Insert(index - 1, currentNode);
                this.SelectedNode = currentNode;
            }
        }

        /// <summary>
        /// Moves selected node to one position down
        /// </summary>
        private void MoveItemDown()
        {
            if (SelectedNode.Parent != null)
            {
                if (SelectedNode != SelectedNode.Parent.LastNode) // Last Node
                {
                    TreeNode currentNode = this.SelectedNode;
                    TreeNode parent = currentNode.Parent;
                    int index = currentNode.Index;

                    // pass to derived class made changes, so it can update it's object hierarchy.
                    Delete(((C1NodeInfo)currentNode.Tag).Element);
                    Insert(((C1NodeInfo)currentNode.Tag).Element, ((C1NodeInfo)parent.Tag).Element, index + 1);

                    currentNode.Remove();
                    parent.Nodes.Insert(index + 1, currentNode);
                    this.SelectedNode = currentNode;
                }
            }
        }

        /// <summary>
        /// Moves selected node to one position left
        /// </summary>
        private void MoveItemLeft()
        {
            if (SelectedNode.Parent != null)
            {
                if (SelectedNode.Parent.Parent != null) //Root Node
                {
                    TreeNode currentNode = this.SelectedNode;
                    TreeNode parent = currentNode.Parent.Parent;
                    int destinationIndex = currentNode.Parent.Index;

                    // pass to derived class made changes, so it can update it's object hierarchy.
                    Delete(((C1NodeInfo)currentNode.Tag).Element);
                    Insert(((C1NodeInfo)currentNode.Tag).Element, ((C1NodeInfo)parent.Tag).Element, destinationIndex + 1);

                    currentNode.Remove();
                    parent.Nodes.Insert(destinationIndex + 1, currentNode);
                    this.SelectedNode = currentNode;
                }
            }
        }

        /// <summary>
        /// Moves selected node to one position right
        /// </summary>
        private void MoveItemRight()
        {
            TreeNode currentNode = this.SelectedNode;
            TreeNode prevNode = currentNode.PrevNode;
            int index = prevNode.Nodes.Count;

            if (prevNode != null)
            {
                // pass to derived class made changes, so it can update it's object hierarchy.
                Delete(((C1NodeInfo)currentNode.Tag).Element); 
                Insert(((C1NodeInfo)currentNode.Tag).Element, ((C1NodeInfo)prevNode.Tag).Element, index);

                currentNode.Remove();
                prevNode.Nodes.Add(currentNode);
                this.SelectedNode = currentNode;
            }
        }

        /// <summary>
        /// Cuts selected node.
        /// </summary>
        private void Cut()
        {
            Cut(this.SelectedNode);
            this.SyncUI();
        }

        /// <summary>
        /// Cuts given node.
        /// </summary>
        /// <param name="node">Node to be cutted</param>
        private void Cut(TreeNode node)
        {
            if (AllowCut)
            {
                /// pass to overridden the item to copy
                Copy(node);
                Delete(node);
            }
            this.SyncUI();
        }

        /// <summary>
        /// Copies selected node.
        /// </summary>
        private void Copy()
        {
            Copy(this.SelectedNode);
            this.SyncUI();
        }

        /// <summary>
        /// Copies given node.
        /// </summary>
        /// <param name="node">Node to be copied</param>
        private void Copy(TreeNode node)
        {
            if (AllowCopy)
            {
                // pass to overridden the item to copy
                Copy(((C1NodeInfo)node.Tag).Element, ((C1NodeInfo)node.Tag).ItemInfo);
            }
        }

        /// <summary>
        /// Pastes node from clipboard.
        /// </summary>
        private void Paste()
        {
            Paste(this.SelectedNode);
            this.SelectedNode.ExpandAll();
            this.SyncUI();
        }

        /// <summary>
        /// Deletes selected node.
        /// </summary>
        private void Delete()
        {
            Delete(this.SelectedNode);
        }

        private void Delete(TreeNode node)
        {
            if (AllowDelete)
            {
                // We can't delete root node
                if (node.Parent != null)
                {
                    // pass to derived class made changes, so it can update it's object hierarchy.
                    Delete(((C1NodeInfo)node.Tag).Element);
                    node.Remove();
                }
            }
        }

        /// <summary>
        /// Renames selected node.
        /// </summary>
        private void Rename()
        {
            if (AllowRename)
            {
                this.SelectedNode.BeginEdit();
            }
        }

        /// <summary>
        /// AddChildType context menu click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ctxAddChildType_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            C1ItemInfo itemInfo = (C1ItemInfo)e.ClickedItem.Tag;
            Insert(itemInfo, ActionMode.Add);
        }

        /// <summary>
        /// Inserts an item
        /// </summary>
        private void _tbInsertItem_Click(object sender, EventArgs e)
        {
            if (!_tbInsertItem.DropDownButtonPressed)
                Insert(_lastInsertedItemInfo, ActionMode.Insert);
        }

        private void _tbAddChild_ButtonClick(object sender, EventArgs e)
        {
            if (!_tbAddChild.DropDownButtonPressed)
                Insert(_lastAddedItemInfo, ActionMode.Add);
        }

        private void _tbChangeItemType_ButtonClick(object sender, EventArgs e)
        {
            if (!_tbChangeItemType.DropDownButtonPressed)
                ChangeToType(_lastChangedItemInfo);
        }

        private void _ctxChangeItemType_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            C1ItemInfo newItemInfo = (C1ItemInfo)e.ClickedItem.Tag;
            ChangeToType(newItemInfo);
        }

        private void _miLoadXML_Click(object sender, EventArgs e)
        {
            LoadFromXML();
        }

        private void _miSaveXML_Click(object sender, EventArgs e)
        {
            SaveToXML();
        }

        private void _miExit_Click(object sender, EventArgs e)
        {
            OnExit();
        }

        private void _cmiCut_Click(object sender, EventArgs e)
        {
            Cut();
        }

        private void _tbCut_Click(object sender, EventArgs e)
        {
            Cut();
        }

        private void _cmiCopy_Click(object sender, EventArgs e)
        {
            Copy();
        }
        private void _tbCopy_Click(object sender, EventArgs e)
        {
            Copy();
        }

        private void _cmiPaste_Click(object sender, EventArgs e)
        {
            Paste();
        }
        private void _tbPaste_Click(object sender, EventArgs e)
        {
            Paste();
        }

        private void _cmiDelete_Click(object sender, EventArgs e)
        {
            Delete();
        }
        private void _tbDelete_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void _cmiRename_Click(object sender, EventArgs e)
        {
            Rename();
        }

        private void _miDynHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        private void _miAbout_Click(object sender, EventArgs e)
        {
            ShowAbout();
        }

        private void _tbMoveItemUp_Click(object sender, EventArgs e)
        {
            MoveItemUp();
        }

        private void _tbMoveItemDown_Click(object sender, EventArgs e)
        {
            MoveItemDown();
        }

        private void _tbMoveItemLeft_Click(object sender, EventArgs e)
        {
            MoveItemLeft();
        }


        private void _tbMoveItemRight_Click(object sender, EventArgs e)
        {
            MoveItemRight();
        }

        /// <summary>
        /// Wires up the property grid when a node gets selected and updates action state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _mainTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // updates toolbar and context menus status
            SyncUI();
            // wire up the property grid when a node gets selected
            WireUpPropertyGrid();
        }

        /// <summary>
        /// Wires up the property grid when a node gets selected
        /// </summary>
        private void WireUpPropertyGrid()
        {
            C1NodeInfo nodeInfo = (C1NodeInfo)this.MainTreeView.SelectedNode.Tag;
            this.MainPropertyGrid.SelectedObject = nodeInfo.Element;
        }

        /// <summary>
        /// Indicates if selected node id first node in level.
        /// </summary>
        private bool IsFirstNodeInLevel
        {
            get { return this.SelectedNode.Index == 0; }
        }

        /// <summary>
        /// Indicates if selected node is last node in level.
        /// </summary>
        private bool IsLastNodeInLevel
        {
            get { return this.SelectedNode.Parent == null || this.SelectedNode == this.SelectedNode.Parent.LastNode; }
        }

        private void _mainPropertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            OnPropertyGridPropertyValueChanged(e.ChangedItem, this.SelectedNode);
        }

        //Willow Yang add comments on July 16,2009 to fix bug 5348,5353
        //private void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
        //end comments.
        /// <summary>
        /// Performs some action when a property value changes.
        /// </summary>
        /// <param name="changedGridItem">The grid item changed.</param>
        /// <param name="selectedNode">Current selected node.</param>
        protected virtual void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
        {
            if (changedGridItem.PropertyDescriptor.Name == "Text")
                if (selectedNode.Text != changedGridItem.Value.ToString())
                    selectedNode.Text = changedGridItem.Value.ToString();

            if (changedGridItem.PropertyDescriptor.Name == "ID")
                if (selectedNode.Text != changedGridItem.Value.ToString())
                    selectedNode.Text = changedGridItem.Value.ToString();
        }

        private void _mainTreeView_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (!AllowRename)
            {
                e.CancelEdit = true;
            }
        }

        private void _mainTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            OnTreeViewAfterLabelEdit(e, this.SelectedNode);
        }

        /// <summary>
        /// When right-click, then explicitly set focus to the node under the click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _mainTreeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.MainTreeView.SelectedNode = this.MainTreeView.GetNodeAt(e.X, e.Y);
                //Nov 23,2009 by Willow Yang for bug 7957
                SyncUI();
                //end
            }
        }

        private void _mainTreeView_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.F2:
                    {
                        Rename();
                        return;
                    }
                case Keys.Insert:
                    {
                        if (AllowInsert)
                            Insert(GetDefaultItemInfo(), ActionMode.Insert);
                        return;
                    }
                case Keys.Delete:
                    {
                        if (AllowDelete)
                            Delete();
                        return;

                    }
            }
        }

        private void _mainTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            SyncUI();
            if (!this.IsEditMode)
            {
                this._previewer.Navigate("about:blank");
                //ShowPreviewHtml(this.MainPreviewer);
            }
        }

        private void _mainTabControl_SizeChanged(object sender, EventArgs e)
        {
            this.MainTreeView.Width = (int)Math.Round(Width * _relTreeWidth);
        }

        private void _splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            _relTreeWidth = Math.Min(Math.Max((double)e.X / (double)Width, 0.1), 0.9);
        }

        /// <summary>
        /// Gets an automatic name for an <typeparamref name="itemType"/>
        /// </summary>
        /// <param name="itemType">Node tab type</param>
        /// <param name="nodeCollection">Gets access to the count of its parent's nodes</param>
        /// <returns>New automatic name for given <typeparamref name="itemType"/></returns>
        protected virtual string GetNextItemTextId(string prefix, TreeNodeCollection nodeCollection)
        {
            int maxId = 0;
            int nodeValue = 0;
            string nodeTextValue = "0";
            string nodeText = "";

            foreach (TreeNode node in nodeCollection)
            {
                nodeText = node.Text;
                if (nodeText.StartsWith(prefix))
                {
                    nodeTextValue = nodeText.Substring(prefix.Length);
                    if (int.TryParse(nodeTextValue, NumberStyles.Integer, CultureInfo.CurrentCulture, out nodeValue))
                    {
                        if (nodeValue.CompareTo(maxId) > 0)
                        {
                            maxId = nodeValue;
                        }
                    }
                }
            }
            //return prefix + (maxId + 1).ToString().PadLeft(2, '0');
            return prefix + (maxId + 1).ToString();//DMA, PadLeft not needed here. (29 oct 2008)
        }

        #endregion  // Methods


        #region ** Drag&Drop

        private void _mainTreeView_DragDrop(object sender, DragEventArgs e)
        {
            this.MainTreeView.Invalidate(_highLightedRect);
            TreeNode sourceNode = FindSourceDragNode(e);
            TreeNode destinationNode = FindDestinationDragNode(e);
            if (e.Effect == DragDropEffects.Copy)
                CopyDrag(sourceNode, destinationNode);
            else
                if (e.Effect == DragDropEffects.Move)
                    MoveDrag(sourceNode, destinationNode);
        }

        private void _mainTreeView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
            {
                if ((e.KeyState & 8) > 0)
                    e.Effect = DragDropEffects.Copy;
                else
                    e.Effect = DragDropEffects.Move;
            }
            else
                e.Effect = DragDropEffects.None;
        }

        private void _mainTreeView_DragLeave(object sender, EventArgs e)
        {
            this.MainTreeView.Invalidate(_highLightedRect);
        }

        private void _mainTreeView_DragOver(object sender, DragEventArgs e)
        {
            TreeNode sourceNode = FindSourceDragNode(e);
            TreeNode destinationNode = FindDestinationDragNode(e);

            if (destinationNode == null || sourceNode == null)
            {
                e.Effect = DragDropEffects.None;
            }
            else
            {
                if ((e.KeyState & 8) != 0 && CanDragCopy(sourceNode, destinationNode))
                    e.Effect = DragDropEffects.Copy;
                else
                    if (CanDragMove(sourceNode, destinationNode))
                        e.Effect = DragDropEffects.Move;
                    else
                        e.Effect = DragDropEffects.None;
            }
            // highlight
            if (destinationNode != null && _highLightedRect != destinationNode.Bounds)
            {
                if (_highLightedRect != Rectangle.Empty)
                    this.MainTreeView.Invalidate(_highLightedRect);
                _highLightedRect = destinationNode.Bounds;

                Graphics g = Graphics.FromHwnd(this.MainTreeView.Handle);
                g.FillRectangle(new SolidBrush(Color.FromArgb(64, Color.FromKnownColor(KnownColor.Highlight))), _highLightedRect);
            }
        }

        private void _mainTreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            this.SelectedNode = (e.Item as TreeNode);
            if (e.Item != this.MainTreeView.Nodes[0])
            {
                _draggedNode = this.SelectedNode;
                string strItem = ((TreeNode)e.Item).Text;
                DoDragDrop(strItem, DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        private TreeNode FindSourceDragNode(System.Windows.Forms.DragEventArgs e)
        {
            return _draggedNode;
        }

        private TreeNode FindDestinationDragNode(System.Windows.Forms.DragEventArgs e)
        {
            Point pt;
            pt = this.MainTreeView.PointToClient(new Point(e.X, e.Y));
            return this.MainTreeView.GetNodeAt(pt);
        }

        private void CopyDrag(TreeNode sourceNode, TreeNode destinationNode)
        {
            Copy(sourceNode);
            Paste(destinationNode);
        }

        protected virtual void MoveDrag(TreeNode sourceNode, TreeNode destinationNode)
        {
            Cut(sourceNode);
            Paste(destinationNode);
        }       

        private bool CanDragCopy(TreeNode sourceNode, TreeNode destinationNode)
        {
            bool result = true;
            result &= !CheckNodeIsInChilds(destinationNode.Nodes, sourceNode);
            result &= ((C1NodeInfo)destinationNode.Tag).ItemInfo.EnableChildItems;
            return result;
        }

        private bool CanDragMove(TreeNode sourceNode, TreeNode destinationNode)
        {
            if (sourceNode == destinationNode)
                return false;
            return CanDragCopy(sourceNode, destinationNode);
        }

        private bool CheckNodeIsInChilds(TreeNodeCollection nodes, TreeNode node)
        {
            bool result = false;
            foreach (TreeNode n in nodes)
            {
                result |= (n == node);
                if (result)
                    break;
            }
            return result;
        }

        #endregion  // ** Drag&Drop

        private void _miPropReset_Click(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip_ForPropertyGrid_Opening(object sender, CancelEventArgs e)
        {
            GridItem item = _mainPropertyGrid.SelectedGridItem;
            try
            {
                contextMenuStrip_ForPropertyGrid.Enabled = item.PropertyDescriptor.CanResetValue(_mainPropertyGrid.SelectedObject);
            }
            catch
            {
                contextMenuStrip_ForPropertyGrid.Enabled = false;
            }
        }

        private void contextMenuStrip_ForPropertyGrid_Click(object sender, EventArgs e)
        {
            GridItem item = _mainPropertyGrid.SelectedGridItem;
            if (item.PropertyDescriptor.CanResetValue(_mainPropertyGrid.SelectedObject))
            {
                _mainPropertyGrid.ResetSelectedProperty();
                //Dec 3,2009 by lulianbo for bug 7949
                OnPropertyGridPropertyValueChanged(item, SelectedNode);
                //end
            }
        }

    }
}