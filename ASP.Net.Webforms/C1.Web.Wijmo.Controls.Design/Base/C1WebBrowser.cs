using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using C1.Web.Wijmo.Controls.Design.Utils;

namespace C1.Web.Wijmo.Controls.Design
{

	internal class C1WebBrowser : System.Windows.Forms.WebBrowser
	{

		List<string> tempFileList = new List<string>();

		Regex rxscript = null;
        Regex rxinternalimage = null;
        private System.Web.UI.Control control = null;

		public C1WebBrowser()
		{
		}

        private Assembly Assembly
        {
            get
            {
                if (this.control != null)
                {
                    return this.control.GetType().Assembly;
                }

                return Assembly.GetExecutingAssembly();
            }
        }

        public void DocumentWrite(string doctext, System.Web.UI.Control control)
        {
            this.control = control;
            string asmName = this.Assembly.FullName;
            asmName = asmName.Replace(" ", "(?: |%20)");
            string stag = "(<script)[^>]*(src=\"mvwres://[^\"]*" + asmName + "[^\"]*\")[^>]*>\\s*(</script>)";
            string internalimgtag = "((?:\"|\\(|')mvwres://[^\"\\)']*" + asmName + "[^\"\\)']*(?:\\.jpg|\\.gif|\\.png)(?:\"|\\)|'))";
            
            
            rxscript = new Regex(stag, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            rxinternalimage = new Regex(internalimgtag, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            
            this.SetFilteredDocumentText(doctext);
        }

		public void SetFilteredDocumentText(string doctext)
        {
            //string externalimgurl = @"[Ss][rr][Cc]\s*=\s*([\""\'][^\""\']*(\.jpg|\.gif|\.png)[\""\'])";
            // fixed bug 25098 add .bmp image support
            string externalimgurl = @"[Ss][rr][Cc]\s*=\s*([\""\'][^\""\']*(\.jpg|\.gif|\.png|\.bmp)[\""\'])";
            Regex rxexternalimgurl = new Regex(externalimgurl, RegexOptions.IgnoreCase);

            string importcssurl = @"@import\surl\(\'(.+\.css)\'\)";
            Regex rximportcssurl = new Regex(importcssurl, RegexOptions.IgnoreCase);
                        
            this.replaceImageResourceReferenceWithTempFile(ref doctext);
            this.replaceExternalImage(ref doctext, rxexternalimgurl);
            this.replaceExternalCss(ref doctext, rximportcssurl);
			this.replaceScriptResourceReferenceWithScript(ref doctext);
			this.Document.Write(doctext);
		}

		private void clearTempFileList()
		{
			if (tempFileList.Count > 0)
			{
				foreach (string tempFile in tempFileList)
					File.Delete(tempFile);
				tempFileList.Clear();
			}
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			clearTempFileList();
		}

		private void addTempFile(string tfile)
		{
			if (!tempFileList.Contains(tfile))
				tempFileList.Add(tfile);
		}

		private string getResourceTextData(string sResourceName)
		{
			string text = null;
			Stream rs = null;
			try
			{
				rs = this.Assembly.GetManifestResourceStream(sResourceName);
				if (rs != null)
				{
					using (StreamReader sr = new StreamReader(rs))
					{
						text = sr.ReadToEnd();
					}
				}
			}
			catch { text = null; }
			finally { if (rs != null) rs.Close(); }
			return text;
		}

		private string getResourceImageDataToTempFile(string sResourceName)
		{
			string tempFile = null;
			FileStream fs = null;
			Stream rs = null;

			try
			{
                rs = this.Assembly.GetManifestResourceStream(sResourceName);
				if (rs != null)
				{
					string tfile = System.IO.Path.GetTempFileName().ToLower();
					fs = File.Open(tfile, FileMode.Create, FileAccess.ReadWrite);
					if (fs != null)
					{
						int blen = (int)rs.Length;
						byte[] bytes = new byte[blen];

						rs.Read(bytes, 0, blen);
						fs.Write(bytes, 0, blen);
					}
					tempFile = tfile;
				}
			}
			catch { tempFile = null; }
			finally
			{
				if (fs != null) { fs.Close(); fs = null; }
				if (rs != null) { rs.Close(); rs = null; }
			}
			return tempFile;
		}

		private void replaceScriptResourceReferenceWithScript(ref string script)
		{
			// it must be expected that the script files are large and therefore
			// must not be searched after being added to the script.
			MatchCollection mc = rxscript.Matches(script);
			if (mc != null && mc.Count > 0)
			{
				StringBuilder sb = new StringBuilder();
				int start = 0;

				foreach (Match m in mc)
				{
					string resname = m.Groups[2].Value;
					if (resname.Contains("/") && resname.EndsWith("\""))
					{
						resname = resname.Substring(resname.LastIndexOf('/') + 1);
						resname = resname.Substring(0, resname.Length - 1);	// trim quote
						resname = getResourceTextData(resname);
						if (resname != null)
						{
							int g2i = m.Groups[2].Index;
							int g2e = g2i + m.Groups[2].Length;
							int g3i = m.Groups[3].Index;

							sb.Append(script.Substring(start, g2i - start));
							sb.Append(script.Substring(g2e, g3i - g2e));
							sb.Append(resname);
							sb.Append(m.Groups[3]);

							start = g3i + m.Groups[3].Length;
						}
					}
				}

				if (sb.Length > 0)
				{
					sb.Append(script.Substring(start));
					script = sb.ToString();
				}
			}
		}

        private void replaceImageResourceReferenceWithTempFile(ref string doctext)
		{
            MatchCollection mc = rxinternalimage.Matches(doctext);
			if (mc != null && mc.Count > 0)
			{
				StringBuilder sb = new StringBuilder();
				int start = 0;

				foreach (Match m in mc)
				{
					string resname = m.Groups[1].Value;
					char lc = resname[resname.Length - 1];
					if (resname.Contains("/") && (lc == '\'' || lc == '"' || lc == ')'))
					{
						resname = resname.Substring(resname.LastIndexOf('/') + 1);
						resname = resname.Substring(0, resname.Length - 1);	// trim quote
						resname = resname.Replace("%20", " ");
						resname = getResourceImageDataToTempFile(resname);
						if (resname != null)
						{
							addTempFile(resname);
							resname = resname.Replace(" ", "%20");

                            sb.Append(doctext.Substring(start, m.Groups[1].Index - start));
							sb.Append(lc == ')' ? '(' : lc);
							sb.Append("file:///" + resname.Replace("\\","/"));
							sb.Append(lc);
							start = m.Groups[1].Index + m.Groups[1].Length;
						}
					}
				}
				if (sb.Length > 0)
				{
                    sb.Append(doctext.Substring(start));
                    doctext = sb.ToString();
				}
			}
        }

        private void replaceExternalImage(ref string doctext, Regex rxexternalimage)
        {            
            MatchCollection mc = rxexternalimage.Matches(doctext);
            if (mc != null && mc.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                int start = 0;

                foreach (Match m in mc)
                {
                    string resname = m.Groups[1].Value;
                    char lc = resname[resname.Length - 1];

                    if (lc == '\'' || lc == '"' || lc == ')')
                    {
                        resname = resname.Substring(1, resname.Length - 2);	// trim quote
                        resname = Common.ResolvePhysicalPath(this.control, resname);
                        //resname = resname.Substring(0, resname.Length - 1);	// trim /

                        if (resname != null)
                        {
                            resname = resname.Replace(" ", "%20");

                            sb.Append(doctext.Substring(start, m.Groups[1].Index - start));
                            sb.Append(lc == ')' ? '(' : lc);
                            sb.Append("file:///" + resname.Replace("\\", "/"));
                            sb.Append(lc);
                            start = m.Groups[1].Index + m.Groups[1].Length;
                        }
                    }
                }
                if (sb.Length > 0)
                {
                    sb.Append(doctext.Substring(start));
                    doctext = sb.ToString();
                }
            }
        }

        private void replaceExternalCss(ref string doctext, Regex rximportcssurl)
        {
            MatchCollection mc = rximportcssurl.Matches(doctext);
            
            if (mc != null && mc.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                int start = 0;

                foreach (Match m in mc)
                {
                    string resname = m.Groups[1].Value;

                    if (resname.StartsWith("file:///"))
                    {
                        continue;
                    }

                    sb.Append(doctext.Substring(start, m.Groups[1].Index - start));
                    sb.Append("file:///" + resname.Replace("\\", "/"));
                    start = m.Groups[1].Index + m.Groups[1].Length;
                }

                if (sb.Length > 0)
                {
                    sb.Append(doctext.Substring(start));
                    doctext = sb.ToString();
                }
            }
        }
	}
}
