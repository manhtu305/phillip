﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1ChartCore
{
	using C1.Web.Wijmo.Controls.C1Chart;
	using System.Runtime.InteropServices;

	internal class C1ChartBrowser : WebBrowser
	{
		Regex rxscript = null;

		public delegate void ImageCompleteEventHandler(object sender, EventArgs e);
		public event ImageCompleteEventHandler ImageComplete;
		Image _image = null;
		System.Web.UI.Control _control = null;
		string _documentUrl = "";

		public C1ChartBrowser()
		{
			this.ScrollBarsEnabled = false;
			this.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(C1ChartBrowser_DocumentCompleted);
		}

		public C1ChartBrowser(bool trueCapture):this()
		{
			this._trueCapture = trueCapture;
		}

		#region ** alternative image capture
		
		CaptureForm _captureForm = null;
		bool _trueCapture;

		internal void RaiseImageComplete(Image im)
		{
			try
			{
				_image = im;
				ImageComplete(this, EventArgs.Empty);
			}
			catch { }
		}
		
		public void CaptureImage()
		{
			this._captureImage();
		}

		private void _captureImage()
		{
			this._createCaptureForm();
			_captureForm.Width = this.Width + 1000;
			_captureForm.Height = this.Height + 1000;
			_captureForm.StartPosition = FormStartPosition.Manual;
/*
			_captureForm.Location = new Point(10, 10);
			_captureForm.Text = "" + this.Width + 
						" x " + this.Height;
*/
			_captureForm.Location = new Point(-(this.Width + 1000), -(this.Height + 1000));

			_captureForm.Hide();
			_captureForm.Show();			
		}


		private void _createCaptureForm()
		{
			if (_captureForm == null)
			{
				_captureForm = new CaptureForm(this);
				_captureForm.ShowInTaskbar = false;
				this.Dock = DockStyle.Fill;
				_captureForm.Controls.Add(this);
				_captureForm.FormClosed += new FormClosedEventHandler(_captureForm_FormClosed);
			}
		}





		


		/*
		protected override void WndProc(ref Message m)
		{
			//const int WM_NCACTIVATE = 0x6;
			const int WM_ACTIVATE = 0x0006;
			const int WA_ACTIVE = 1;
			
			if (m.Msg == WM_ACTIVATE && ((int)m.WParam) == WA_ACTIVE)
			{
				//	this.Owner.Focus();
				return;
			}
			else
			{
				base.WndProc(ref m);
			}
		} */

		void _captureForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			this._disposeCaptureForm();
		}

		private void _disposeCaptureForm()
		{
			if (_captureForm != null)
			{
				try
				{
					_captureForm.Dispose();
				}
				finally
				{
					_captureForm = null;
				}
			}
		}
		#endregion

		private Assembly ControlAssembly
		{
			get
			{
				if (this._control != null)
				{
					return this._control.GetType().Assembly;
				}

				return Assembly.GetExecutingAssembly();
			}
		}

		Timer _timer;

		private void CaptureInnerImage()
		{
			//this.CaptureImage();
			if (_timer == null)
			{
				_timer = new Timer();
				_timer.Interval = 200;
				_timer.Tick += new EventHandler(_timer_Tick);
			}
			_timer.Start();
		}

		void _timer_Tick(object sender, EventArgs e)
		{
			object iscomplete = this.Document.InvokeScript("isComplete");
			if (iscomplete != null && iscomplete.ToString().ToLower() == "true")
			{
				Bitmap docImage = new Bitmap(this.Width + 100, this.Height + 100);
				var ele = this.Document.GetElementById(_control.ClientID);
				this.DrawToBitmap(docImage, new Rectangle(this.Location.X, this.Location.Y, this.Width, this.Height));
				Rectangle src = new Rectangle(0, 0, this.Width, this.Height);
				_image = docImage.Clone(src, System.Drawing.Imaging.PixelFormat.DontCare);
				try
				{
					ImageComplete(this, EventArgs.Empty);
				}
				catch
				{

				}
				finally
				{
					_timer.Stop();
				}
			}
		}

		void C1ChartBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{
			if (this._trueCapture)
			{
				var ele = this.Document.GetElementById(_control.ClientID);
				this._captureImage();
			}
			else
			{
				try
				{
					CaptureInnerImage();
				}
				catch
				//catch (Exception ex)
				{
					//MessageBox.Show(ex.Message);
				}
			}
		}


		public void DocumentWrite(string docText, System.Web.UI.Control control, string documentUrl)
		{
			this._control = control;
			this._documentUrl = documentUrl;

			string asmName = this.ControlAssembly.FullName;
			asmName = asmName.Replace(" ", "(?: |%20)");
			string stag = "(<script)[^>]*(src=\"mvwres://[^\"]*" + asmName + "[^\"]*\")[^>]*>\\s*(</script>)";

			rxscript = new Regex(stag, RegexOptions.IgnoreCase | RegexOptions.Singleline);
			SetFilteredDocumentText(docText);
			//this.Document.Body.InnerHtml += "<script>function isComplete(){ return true; } \r\n  $(document).ready(function(){ window.isComplete = function(){ return true; }});</script>";
		}

		/// <summary>
		/// Releases unmanaged and - optionally - managed resources
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			try
			{
				this._disposeCaptureForm();
				base.Dispose(disposing);
				if (_timer != null)
				{
					_timer.Dispose();
				}
			}
			catch (Exception) { }
		}

		public Image Image
		{
			get { return _image; }
		}

		private void SetFilteredDocumentText(string doctext)
		{
			if (_control is C1LineChart)
			{
				C1LineChart lineChart = _control as C1LineChart;
				foreach (LineChartSeries series in lineChart.SeriesList)
				{
					foreach (LineChartMarkerSymbol symbol in series.Markers.Symbol)
					{
						replaceExternalImage(ref doctext, symbol);
					}
				}
			}

			this.replaceScriptResourceReferenceWithScript(ref doctext);
			Regex reg = new Regex("</body>", RegexOptions.RightToLeft);
			int count = reg.Matches(doctext).Count;
			doctext = reg.Replace(doctext, "<script>$(document).ready(function(){ window.isComplete = function(){return true}});</script></body>", 1);
			base.DocumentText = doctext;
			//base.Document.Body.InnerText += "<script>$(document).ready(function(){ window.isComplete = function(){ return true; }});</script>";
		}

		private void replaceExternalImage(ref string doctext, LineChartMarkerSymbol symbol)
		{
			string resname = symbol.Url;

			if (string.IsNullOrEmpty(resname))
			{
				return;
			}

			resname = ResolvePhysicalPath(_control, _documentUrl, resname);
			resname = "file:///" + resname.Replace("\\", "/");
			if (resname.EndsWith("/"))
				resname = resname.Substring(0, resname.Length - 1);
			doctext = doctext.Replace(_control.ResolveClientUrl(symbol.Url), resname);
		}

		private string getResourceTextData(string sResourceName)
		{
			string text = null;
			Stream rs = null;
			try
			{
				rs = this.ControlAssembly.GetManifestResourceStream(sResourceName);
				if (rs != null)
				{
					using (StreamReader sr = new StreamReader(rs))
					{
						text = sr.ReadToEnd();
					}
				}
			}
			catch { text = null; }
			finally { if (rs != null) rs.Close(); }
			return text;
		}

		private void replaceScriptResourceReferenceWithScript(ref string script)
		{
			// it must be expected that the script files are large and therefore
			// must not be searched after being added to the script.
			MatchCollection mc = rxscript.Matches(script);
			if (mc != null && mc.Count > 0)
			{
				StringBuilder sb = new StringBuilder();
				int start = 0;

				foreach (Match m in mc)
				{
					string resname = m.Groups[2].Value;
					if (resname.Contains("/") && resname.EndsWith("\""))
					{
						resname = resname.Substring(resname.LastIndexOf('/') + 1);
						resname = resname.Substring(0, resname.Length - 1);	// trim quote
						resname = getResourceTextData(resname);
						if (resname != null)
						{
							int g2i = m.Groups[2].Index;
							int g2e = g2i + m.Groups[2].Length;
							int g3i = m.Groups[3].Index;

							sb.Append(script.Substring(start, g2i - start));
							sb.Append(script.Substring(g2e, g3i - g2e));
							sb.Append(resname);
							sb.Append(m.Groups[3]);

							start = g3i + m.Groups[3].Length;
						}
					}
				}

				if (sb.Length > 0)
				{
					sb.Append(script.Substring(start));
					script = sb.ToString();
				}
			}
		}

		private static string ResolvePhysicalPath(System.Web.UI.Control control, string documentUrl, string url)
		{
			ISite site = GetSite(control);
			if (site == null) return url;

			IWebApplication service = (IWebApplication)site.GetService(typeof(IWebApplication));
			if (url.IndexOf("~/") > -1)
			{
				return url.Replace("~/", service.RootProjectItem.PhysicalPath);
			}

			Uri baseAbsoluteUri = new Uri(documentUrl.Replace("~/", service.RootProjectItem.PhysicalPath));
			string absolutePath = new Uri(baseAbsoluteUri, url).AbsolutePath;
			string docUrl = absolutePath.Replace(service.RootProjectItem.PhysicalPath.Replace("\\", "/"), "~/");

			IProjectItem item = service.GetProjectItemFromUrl(docUrl);

			if (item == null)
				return url;

			string path = item.PhysicalPath;
			if (!path.EndsWith(Path.DirectorySeparatorChar.ToString()))
				path += Path.DirectorySeparatorChar;

			return path;
		}

		//private static string ResolvePhysicalPath(System.Web.UI.Control control, string url)
		//{
		//    ISite site = GetSite(control);
		//    if (site == null) return url;

		////    // This is a deprecated interface, but it does the job.
		////    WebFormsRootDesigner docservice = (WebFormsRootDesigner)site.GetService(typeof(WebFormsRootDesigner));
		////    if (docservice != null)
		////    {
		////        // Get the full filename of the current page (.aspx) in design mode.
		////        String documentUrl = docservice.DocumentUrl;
		////    }
		////    //WebFormsRootDesigner
			
		//    IWebApplication service = (IWebApplication)site.GetService(typeof(IWebApplication));
		//    IProjectItem item = service.GetProjectItemFromUrl(url);
		//    if (item == null)
		//        return url.Replace("~/", service.RootProjectItem.PhysicalPath);

		//    string path = item.PhysicalPath;
		//    if (!path.EndsWith(Path.DirectorySeparatorChar.ToString()))
		//        path += Path.DirectorySeparatorChar;

		//    return path;
		//}

		private static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}
	}

	#region ** web browser screenshot using GDI

	public class Gdi32
	{
		[DllImport("gdi32.dll")]
		public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop);
		[DllImport("gdi32.dll")]
		public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);
		[DllImport("gdi32.dll")]
		public static extern IntPtr CreateCompatibleDC(IntPtr hDC);
		[DllImport("gdi32.dll")]
		public static extern bool DeleteDC(IntPtr hDC);
		[DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);
		[DllImport("gdi32.dll")]
		public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
	}

	public static class User32
	{
		[DllImport("user32.dll")]
		public static extern IntPtr GetDesktopWindow();
		[DllImport("user32.dll")]
		public static extern IntPtr GetWindowDC(IntPtr hWnd);
		[DllImport("user32.dll")]
		public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);
		[DllImport("user32.dll")]
		public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct RECT
	{
		public int left;
		public int top;
		public int right;
		public int bottom;
	}

	public static class ScreenUtilities
	{
		public const int SRCCOPY = 13369376;

		public static System.Drawing.Image CaptureScreen()
		{
			return CaptureWindow(User32.GetDesktopWindow());
		}

		public static System.Drawing.Image CaptureWindow(IntPtr handle)
		{

			IntPtr hdcSrc = User32.GetWindowDC(handle);

			RECT windowRect = new RECT();
			User32.GetWindowRect(handle, ref windowRect);

			int width = windowRect.right - windowRect.left;
			int height = windowRect.bottom - windowRect.top;

			IntPtr hdcDest = Gdi32.CreateCompatibleDC(hdcSrc);
			IntPtr hBitmap = Gdi32.CreateCompatibleBitmap(hdcSrc, width, height);

			IntPtr hOld = Gdi32.SelectObject(hdcDest, hBitmap);
			Gdi32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, SRCCOPY);
			Gdi32.SelectObject(hdcDest, hOld);
			Gdi32.DeleteDC(hdcDest);
			User32.ReleaseDC(handle, hdcSrc);

			System.Drawing.Image image = System.Drawing.Image.FromHbitmap(hBitmap);
			Gdi32.DeleteObject(hBitmap);

			return image;
		}

		public static System.Drawing.Image DrawToImage(this System.Windows.Forms.Control control)
		{
			return ScreenUtilities.CaptureWindow(control.Handle);;
		}



	}

	[ComVisible(true)]
	internal class CaptureForm : Form
	{
		C1ChartBrowser _browser;
		Timer _timer;
		public CaptureForm(C1ChartBrowser browser)
			: base()
		{
			_browser = browser;
			_timer = new Timer();
			_timer.Interval = 20;
			_timer.Tick += new EventHandler(_timer_Tick);
		}

		void _timer_Tick(object sender, EventArgs e)
		{
			if (this.Visible)
			{
				var iscomplete = _browser.Document.InvokeScript("isComplete");
				if (iscomplete!=null && iscomplete.ToString().ToLower() == "true")
				{
					_browser.RaiseImageComplete(ScreenUtilities.DrawToImage(this._browser));
					this.Hide();
				}
			}
		}
		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (this.Visible)
			{
				_timer.Start();
			}
			else
			{
				_timer.Stop();
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
		}
	}

	#endregion
}
