﻿using System;
using System.Collections.Generic;
using System.Web.UI.Design;
using System.Drawing;
using System.Web.UI;
using System.Text;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;

namespace C1.Web.Wijmo.Controls.Design.C1ChartCore
{
    using C1.Web.Wijmo.Controls.C1Chart;
    using C1.Web.Wijmo.Controls.Base;
    using System.ComponentModel.Design;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using System.Web.UI.WebControls;
    using System.Windows.Forms;
    using System.Windows.Forms.Design;
    using System.Collections;
    using System.Reflection;

    /// <summary>
    /// Provides a C1ChartCoreDesigner class for extending the design-mode behavior of a C1ChartCore control.
    /// </summary>
    public class C1ChartCoreDesigner : C1DataBoundControlDesigner
    {
        C1ChartBrowser _browser = null;
        System.Drawing.Image _image = null;
        protected bool _sampleDataAdded = false;

        public C1ChartCoreDesigner() { }

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
        public override void Initialize(IComponent component)
        {
            if (component != null)
            {
                base.Initialize(component);
                SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
                SetViewFlags(ViewFlags.CustomPaint, true);
                _browser = new C1ChartBrowser();
                _browser.ImageComplete += new C1ChartBrowser.ImageCompleteEventHandler(_browser_ImageComplete);
                _browser.ScriptErrorsSuppressed = true;
            }
        }

        void _browser_ImageComplete(object sender, EventArgs e)
        {
            // now that we have an image invalidate the control design surface to we can draw it
            _image = _browser.Image;

            this.Invalidate();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            if (_image != null)
            {
                e.Graphics.DrawImage(_image, new Point(0, 0));
            }
            base.OnPaint(e);
        }


        // avoid to re render the page and call the GetDesignTimeHtml twice.
        protected override void OnDataSourceChanged(bool forceUpdateView)
        {

        }

        /// <summary>
        /// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions.
        /// </summary>
        /// <param name="regions"> A collection of control designer regions for the associated control.</param>
        /// <returns>The design-time HTML markup for the associated control, including all control designer regions.</returns>
        public override string GetDesignTimeHtml()
        {
            if ((Component as C1TargetDataBoundControlBase).WijmoControlMode == WijmoControlMode.Mobile)
            {
                //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
            }
            C1TargetDataBoundControlBase control = Component as C1TargetDataBoundControlBase;
			if (control.Width.IsEmpty)
			{
				_browser.Width = 600;
			}
			else
			{
				_browser.Width = (int)control.Width.Value;
			}
			if (control.Height.IsEmpty)
			{
				_browser.Height = 400;
			}
			else
			{
				_browser.Height = (int)control.Height.Value;
			}

            //store the control's visible state, in designer the control will show all the time.
            bool visible = control.Visible;
            control.Visible = true;

            string sResultBodyContent = "";
            StringBuilder sb = new StringBuilder();
            System.IO.StringWriter tw = new System.IO.StringWriter(sb);
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
            control.RenderControl(htw);
            sResultBodyContent = sb.ToString();

            PropertyDescriptor context = TypeDescriptor.GetProperties(this.Component)["SeriesList"];
            object seriesList = context.GetValue(this.Component);
            IList list = seriesList as IList;
            if (list != null && list.Count == 0)
            {
                AddSampleData();
                _sampleDataAdded = true;
            }


            //var scriptItems = this.RootDesigner.GetClientScriptsInDocument();
            //StringBuilder sbScripts = new StringBuilder();
            //foreach (ClientScriptItem item in scriptItems)
            //{
            //	sbScripts.AppendLine(item.Text);
            //}
            StringBuilder sbScripts = AddVitrualFunctionScript();

            StringBuilder sDocumentContent = new StringBuilder();
            sDocumentContent.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            sDocumentContent.AppendLine("<html  xmlns=\"http://www.w3.org/1999/xhtml\">");
            sDocumentContent.AppendLine("<body style=\"margin: 0\">");
            sDocumentContent.AppendLine(GetScriptReferences());
            sDocumentContent.AppendLine("<script type=\"text/javascript\">");
            sDocumentContent.AppendLine(sbScripts.ToString());
            sDocumentContent.AppendLine("$(document).ready(function () {");
            sDocumentContent.AppendLine(GetScriptDescriptors());
            //Add comments by RyanWu@20110707.
            //I added the following strange code in order to fix the issue#15889.
            //I also don't know why the last raphael element can't be drawn in the webbrowser control.
            //In bar chart, I first draw the chart label then draw bar and shadow (if shadow = true),
            //So if shadow = false, then last bar can't be shown.  In pie chart, I first draw sector,
            //then draw chart labels.  So the last chart label can't be shown.
            //Hence, to solve this issue, we will draw a useless shape after the chart drawn.
            sDocumentContent.AppendLine("new Raphael(-100, -100, 1, 1).circle().hide();");
            //end by RyanWu@20110707.
            sDocumentContent.AppendLine("});");
            sDocumentContent.AppendLine("</script>");
            sDocumentContent.AppendLine(sResultBodyContent);
            sDocumentContent.AppendLine("</body>");
            sDocumentContent.AppendLine("</html>");

            var result = sDocumentContent.ToString();
            result = result.Replace("\"animation\": {\r\r}", "\"animation\": { \"duration\": 0 }"); // mod by wy@20140703

			
            _browser.DocumentWrite(result, control, this.RootDesigner.DocumentUrl);

            if (_sampleDataAdded)
            {
                list.Clear();
                _sampleDataAdded = false;
            }

            // restore the control's visible state.
            control.Visible = visible;

            return base.GetDesignTimeHtml();
        }

        protected virtual StringBuilder AddVitrualFunctionScript() 
        {
            StringBuilder sbScripts = new StringBuilder();
            var prop = Component.GetType().GetProperty("Hint");
            if (prop != null)
            {
                object h = Component.GetType().GetProperty("Hint").GetValue(Component, null);
                if (h != null)
                {
                    ChartHint hint = h as ChartHint;
                    if (hint != null)
                    {
                        if (!String.IsNullOrEmpty(hint.Content.Function))
                        {
                            sbScripts.AppendLine(String.Format(" function {0} () {{}};", hint.Content.Function));
                        }
                        if (!String.IsNullOrEmpty(hint.Title.Function))
                        {
                            sbScripts.AppendLine(String.Format(" function {0} () {{}};", hint.Title.Function));
                        }
                    }
                }
            }

            return sbScripts;
        }

        protected virtual void AddSampleData()
        {
        }

		private string GetScriptReference(WijmoResourceInfo resource)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("<script type='text/javascript'>");
			sb.AppendLine(resource.ResourceContent);
			sb.AppendLine("</script>");
			return sb.ToString();
		}

        private string GetScriptReferences()
        {
            StringBuilder strScriptRefs = new StringBuilder();
			if (Component is IWijmoWidgetSupport)
            {
				List<WijmoResourceInfo> resources = WijmoResourceManager.GetScriptReferenceForControl((IWijmoWidgetSupport)Component);
				foreach (WijmoResourceInfo resource in resources) 
				{
					strScriptRefs.AppendLine(GetScriptReference(resource));
				}
            }
            return strScriptRefs.ToString();
        }

        private string GetScriptDescriptors()
        {
            string scriptDesc = "";
            List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)((IScriptControl)Component).GetScriptDescriptors();
            foreach (WidgetDescriptor desc in scriptDescr)
            {
                scriptDesc += this.GetScriptInternal(desc);
            }
            return scriptDesc;
        }

        private string GetScriptInternal(WidgetDescriptor desc)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("$(\"#");
            builder.Append(((C1TargetDataBoundControlBase)Component).ClientID);
            builder.Append("\").");
            builder.Append(desc.Type);
            builder.Append("(");
            builder.Append(desc.Options);
            builder.Append(");");
            return builder.ToString();
        }

        internal virtual void BuildSeriesStyles()
        {
            PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["SeriesStyles"];
            DataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditCollectionCallback), context, C1Localizer.GetString("C1ChartCore.SmartTag.SeriesStyles"), context);
        }

        internal virtual void BuildSeriesHoverStyles()
        {
            PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["SeriesHoverStyles"];
            DataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditCollectionCallback), context, C1Localizer.GetString("C1ChartCore.SmartTag.SeriesHoverStyles"), context);
        }

        internal void BuildSeriesList()
        {
            PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["SeriesList"];
            DataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditCollectionCallback), context, C1Localizer.GetString("C1ChartCore.SmartTag.SeriesList"), context);
        }

		internal void BuildAnnotations()
		{
			PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["Annotations"];
			DataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditAnnotationCollectionCallback), context, C1Localizer.GetString("C1ChartCore.SmartTag.Annotations"), context);
		}

		internal bool EditAnnotationCollectionCallback(object context)
		{
			IDesignerHost service = (IDesignerHost)this.GetService(typeof(IDesignerHost));
			PropertyDescriptor propDesc = (PropertyDescriptor)context;
			new AnnotationCollectionEditor(propDesc.PropertyType).EditValue(new TypeDescriptorContext(service, propDesc, base.Component), new WindowsFormsEditorServiceHelper(this), propDesc.GetValue(base.Component));
			return true;
		}


        internal bool EditCollectionCallback(object context)
        {
            IDesignerHost service = (IDesignerHost)this.GetService(typeof(IDesignerHost));
            PropertyDescriptor propDesc = (PropertyDescriptor)context;
            new ListItemsCollectionEditor(propDesc.PropertyType).EditValue(new TypeDescriptorContext(service, propDesc, base.Component), new WindowsFormsEditorServiceHelper(this), propDesc.GetValue(base.Component));
            return true;
        }

        /// <summary>
        /// override ActionLists
        /// </summary>
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();

                lists.AddRange(base.ActionLists);
                lists.Add(CreateChartCoreDesignerActionList());
                return lists;
            }
        }

		internal virtual C1ChartCoreDesignerActionList CreateChartCoreDesignerActionList()
		{
			return new C1ChartCoreDesignerActionList(this);
		}
    }

    internal sealed class TypeDescriptorContext : ITypeDescriptorContext, IServiceProvider
    {
        // Fields
        private IDesignerHost _designerHost;
        private object _instance;
        private PropertyDescriptor _propDesc;

        // Methods
        public TypeDescriptorContext(IDesignerHost designerHost, PropertyDescriptor propDesc, object instance)
        {
            this._designerHost = designerHost;
            this._propDesc = propDesc;
            this._instance = instance;
        }

        public object GetService(Type serviceType)
        {
            return this._designerHost.GetService(serviceType);
        }

        public void OnComponentChanged()
        {
            if (this.ComponentChangeService != null)
            {
                this.ComponentChangeService.OnComponentChanged(this._instance, this._propDesc, null, null);
            }
        }

        public bool OnComponentChanging()
        {
            if (this.ComponentChangeService != null)
            {
                try
                {
                    this.ComponentChangeService.OnComponentChanging(this._instance, this._propDesc);
                }
                catch (CheckoutException exception)
                {
                    if (exception != CheckoutException.Canceled)
                    {
                        throw exception;
                    }
                    return false;
                }
            }
            return true;
        }

        // Properties
        private IComponentChangeService ComponentChangeService
        {
            get
            {
                return (IComponentChangeService)this._designerHost.GetService(typeof(IComponentChangeService));
            }
        }

        public IContainer Container
        {
            get
            {
                return (IContainer)this._designerHost.GetService(typeof(IContainer));
            }
        }

        public object Instance
        {
            get
            {
                return this._instance;
            }
        }

        public PropertyDescriptor PropertyDescriptor
        {
            get
            {
                return this._propDesc;
            }
        }
    }

    internal sealed class WindowsFormsEditorServiceHelper : IWindowsFormsEditorService, IServiceProvider
    {
        // Fields
        private ComponentDesigner _componentDesigner;

        // Methods
        public WindowsFormsEditorServiceHelper(ComponentDesigner componentDesigner)
        {
            this._componentDesigner = componentDesigner;
        }

        object IServiceProvider.GetService(Type serviceType)
        {
            if (serviceType == typeof(IWindowsFormsEditorService))
            {
                return this;
            }
            if (this._componentDesigner.Component != null)
            {
                ISite site = this._componentDesigner.Component.Site;
                if (site != null)
                {
                    return site.GetService(serviceType);
                }
            }
            return null;
        }

        void IWindowsFormsEditorService.CloseDropDown()
        {
        }

        void IWindowsFormsEditorService.DropDownControl(Control control)
        {
        }

        DialogResult IWindowsFormsEditorService.ShowDialog(Form dialog)
        {
            IServiceProvider serviceProvider = this as IServiceProvider;
            if (serviceProvider != null)
            {
                IUIService service = (IUIService)serviceProvider.GetService(typeof(IUIService));
                if (service != null)
                {
                    return service.ShowDialog(dialog);
                }
            }
            return dialog.ShowDialog();
        }
    }

    /// <summary>
    /// Provides a custom class for types that define a list of items used to create a smart tag panel.
    /// </summary>
    internal class C1ChartCoreDesignerActionList : DesignerActionListBase
    {
        #region ** fields
        private DesignerActionItemCollection items;
        C1ChartCoreDesigner _designer;
        #endregion end of ** fields..

        #region ** constructor
        /// <summary>
        /// CustomControlActionList constructor.
        /// </summary>
        /// <param name="parent">The Specified C1SplitterDesigner designer.</param>
        public C1ChartCoreDesignerActionList(C1ChartCoreDesigner designer)
            : base(designer)
        {
            _designer = designer;
        }
        #endregion end of ** constructor.

        /// <summary>
        /// Override GetSortedActionItems method.
        /// </summary>
        /// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (items == null)
            {
                items = new DesignerActionItemCollection();

                items.Add(new DesignerActionMethodItem(this, "EditSeriesList", C1Localizer.GetString("C1ChartCore.SmartTag.SeriesList"), "", C1Localizer.GetString("C1ChartCore.SmartTag.SeriesListDesc")));
				if (SupportSeriesStyles)
				{
					items.Add(new DesignerActionMethodItem(this, "EditSeriesStyles", C1Localizer.GetString("C1ChartCore.SmartTag.SeriesStyles"), "", C1Localizer.GetString("C1ChartCore.SmartTag.SeriesStylesDesc")));
					items.Add(new DesignerActionMethodItem(this, "EditSeriesHoverStyles", C1Localizer.GetString("C1ChartCore.SmartTag.SeriesHoverStyles"), "", C1Localizer.GetString("C1ChartCore.SmartTag.SeriesHoverStylesDesc")));
				}

				if (SupportAnnotations)
				{
					items.Add(new DesignerActionMethodItem(this, "BuildAnnotations", C1Localizer.GetString("C1ChartCore.SmartTag.Annotations"), "", C1Localizer.GetString("C1ChartCore.SmartTag.AnnotationsDesc")));
				}

                AddBaseSortedActionItems(items);
            }
            return items;
        }

		internal virtual bool SupportAnnotations
		{
			get
			{
				return true;
			}
		}

		internal virtual bool SupportSeriesStyles
		{
			get
			{
				return true;
			}
		}

        internal override bool DisplayThemeSupport()
        {
            return false;
        }

        protected internal override bool SupportBootstrap()
        {
            return false;
        }

        internal void EditSeriesList()
        {
            _designer.BuildSeriesList();
        }

        internal void EditSeriesStyles()
        {
            _designer.BuildSeriesStyles();
        }

        internal void EditSeriesHoverStyles()
        {
            _designer.BuildSeriesHoverStyles();
        }

		internal void BuildAnnotations()
		{
			_designer.BuildAnnotations();
		}
    }
}
