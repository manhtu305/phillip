using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Design;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using System.Windows.Forms.Design;
using System.Reflection;
using System.Drawing;
using System.Threading;
using System.Web.UI.Design.WebControls;
using System.Collections;
using C1.Web.Wijmo.Controls.Design.Utils;
using C1.Web.Wijmo.Controls.Design.Localization;
using C1.Web.Wijmo.Controls.Design;
using C1.Web.Wijmo.Controls;

namespace C1.Web.Wijmo.Controls.Design.UITypeEditors
{

	/// <summary>
	/// Property editor for AnnoFormatString property of the ChartAxis.
	/// </summary>
	public class C1AnnoFormatStringEditor : UITypeEditor
	{

		#region ** fields
		private IWindowsFormsEditorService _editorService;
		private ListBox _lstAnnoFormat;
		private ToolTip _tipAnnoFormat;
		private int _prevAnnoFormatIndex = -1;
		private bool _dropDownEventAdded;
		private Hashtable _desciptions = new Hashtable();
		#endregion end of ** fields.

		/// <summary>
		/// Construcror.
		/// </summary>
		public C1AnnoFormatStringEditor()
		{
			this._tipAnnoFormat = new ToolTip();
			this._desciptions.Add("n", "n for number");
			this._desciptions.Add("p", "p for percentage");
			this._desciptions.Add("c", "c for currency");
			
			this._desciptions.Add("f", "Long Date, Short Time");
			this._desciptions.Add("F", "Long Date, Long Time");
			this._desciptions.Add("t", "Short Time");
			this._desciptions.Add("T", "Long Time");
			this._desciptions.Add("d", "Short Date or decimal digits");
			this._desciptions.Add("D", "Long Date");
			this._desciptions.Add("Y", "Month/Year");
			this._desciptions.Add("M", "Month/Day");
			
			this._lstAnnoFormat = new ListBox();
			this._lstAnnoFormat.BorderStyle = BorderStyle.None;
			this._lstAnnoFormat.Items.AddRange(new string[]{"n", "p", "c", "f", "F", "t", "T", "d", "D", "Y", "M"});
		}

		/// <summary>
		/// EditValue
		/// </summary>
		/// <param name="context"></param>
		/// <param name="provider"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (!_dropDownEventAdded)
			{
				_dropDownEventAdded = true;
				this._lstAnnoFormat.Click += new EventHandler(this.ListBox_Click);
				this._lstAnnoFormat.MouseMove += new MouseEventHandler(this.ListBox_MouseMove);
			}

			this._lstAnnoFormat.SelectedItem = value;

			this._editorService = provider.GetService(typeof(IWindowsFormsEditorService)) as IWindowsFormsEditorService;
			
			if (this._editorService == null)
			{
				return value;
			}
			
			this._editorService.DropDownControl(this._lstAnnoFormat);
			
			if (this._lstAnnoFormat.SelectedIndex == -1)
			{
				return value;
			}
			
			return this._lstAnnoFormat.SelectedItem;
		}

		/// <summary>
		/// GetEditStyle
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context != null && context.PropertyDescriptor != null)
			{
				return UITypeEditorEditStyle.DropDown;
			}

			return UITypeEditorEditStyle.None;
		}

		/// <summary>
		/// ListBox_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ListBox_Click(object sender, EventArgs e)
		{
			this._editorService.CloseDropDown();
		}

		/// <summary>
		/// ListBox_MouseMove
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ListBox_MouseMove(object sender, MouseEventArgs e)
		{
			int idx = this._lstAnnoFormat.IndexFromPoint(new Point(e.X, e.Y));
			
			if (idx >= 0)
			{
				if (idx == _prevAnnoFormatIndex)
				{
					return;
				}

				_prevAnnoFormatIndex = idx;
				this._tipAnnoFormat.SetToolTip(this._lstAnnoFormat, this._desciptions[this._lstAnnoFormat.Items[idx]].ToString());
			}
		}
	}
}
