﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.Design.C1ReportViewer
{

	using C1.Web.Wijmo.Controls.C1ReportViewer;
	using System.Web.UI.WebControls;
	using System.ComponentModel;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using System.Globalization;
	using System.Diagnostics;
	using C1.Web.Wijmo.Controls.Design.Utils;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.ComponentModel.Design;

    public class C1ReportViewerDesigner : C1ControlDesinger
	{
		#region ** fields
		//private DesignerAutoFormatCollection _autoFormats;
		private DesignerActionListCollection _actions;
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ReportViewerDesigner"/> class.
		/// </summary>
		public C1ReportViewerDesigner()
		{
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Helper property to get the C1HeaderContentControl we're designing.
		/// </summary>
		private C1ReportViewer ReportViewerControl
		{
			get
			{
				return (C1ReportViewer)Component;
			}
		}

		/// <summary>
		/// Tell the designer we're creating our own UI.
		/// </summary>
		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}
		/// <summary>
		/// Allow resize.
		/// </summary>
		public override bool AllowResize
		{
			get
			{
				return true;
			}
		}

		// initialize ActionLists
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actions == null)
				{
					_actions = new DesignerActionListCollection();
					_actions.AddRange(base.ActionLists);
					_actions.Add(new C1ReportViewerActionList(this));
				}
				return _actions;
			}
		}

		/*
		/// <summary>
		///  Gets the collection of predefined automatic formatting schemes to display
		///  in the Auto Format dialog box for the associated control at design time.
		/// </summary>
		public override DesignerAutoFormatCollection AutoFormats
		{
			get
			{
				if (this._autoFormats == null)
				{
					this._autoFormats = new DesignerAutoFormatCollection();
					C1DesignerAutoFormat.FillVisualStyleAutoFormatCollection(this._autoFormats, (Control)this.Component);
				}
				if (this._autoFormats.Count < 1)
					return base.AutoFormats;
				return this._autoFormats;
			}
		}*/

		#endregion

		C1ReportViewer _control;

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}

		// initialize design time verbs
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			//SetViewFlags(ViewFlags.TemplateEditing, true);
			_control = component as C1ReportViewer;
		}



		protected override string GetErrorDesignTimeHtml(Exception e)
		{
#if DEBUG
			return CreatePlaceHolderDesignTimeHtml("Debug error: " + e.Message + "," + e.StackTrace);
#else
            return CreatePlaceHolderDesignTimeHtml(C1Localizer.GetString("Base.Exception.ErrorDesignTimeHtml"));
#endif
		}

	}


	internal class C1ReportViewerActionList : DesignerActionListBase
	{
		// ** fields
		internal C1ReportViewerDesigner _owner;

		internal C1ReportViewer _reportViewer;

		private DesignerActionItemCollection _actions;
		
		public C1ReportViewerActionList(C1ReportViewerDesigner owner)
			: base(owner)
		{
			_owner = owner;

			_reportViewer = base.Component as C1ReportViewer;
		}

		public Unit Width
		{
			get { return _reportViewer.Width; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_reportViewer)["Width"];
				prop.SetValue(_reportViewer, value);
			}
		}

		public Unit Height
		{
			get { return _reportViewer.Height; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_reportViewer)["Height"];
				prop.SetValue(_reportViewer, value);
			}
		}

		public bool ToolBarVisible
		{
			get { return _reportViewer.ToolBarVisible; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_reportViewer)["ToolBarVisible"];
				prop.SetValue(_reportViewer, value);
			}
		}

		public bool StatusBarVisible
		{
			get { return _reportViewer.StatusBarVisible; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_reportViewer)["StatusBarVisible"];
				prop.SetValue(_reportViewer, value);
			}
		}

		public bool CollapseToolsPanel
		{
			get { return _reportViewer.CollapseToolsPanel; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_reportViewer)["CollapseToolsPanel"];
				prop.SetValue(_reportViewer, value);
			}
		}
		
		public bool PagedView
		{
			get { return _reportViewer.PagedView; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_reportViewer)["PagedView"];
				prop.SetValue(_reportViewer, value);
			}
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (_actions == null)
			{
				_actions = new DesignerActionItemCollection();
				_actions.Add(new DesignerActionPropertyItem("Width", C1Localizer.GetString("C1ReportViewer.SmartTag.Width", "Control width"), "Size"));
				_actions.Add(new DesignerActionPropertyItem("Height", C1Localizer.GetString("C1ReportViewer.SmartTag.Height", "Control height"), "Size"));

				_actions.Add(new DesignerActionPropertyItem("ToolBarVisible", C1Localizer.GetString("C1ReportViewer.SmartTag.ToolBarVisible", "ToolBar visible"), "UI"));
				_actions.Add(new DesignerActionPropertyItem("StatusBarVisible", C1Localizer.GetString("C1ReportViewer.SmartTag.StatusBarVisible", "StatusBar visible"), "UI"));
				_actions.Add(new DesignerActionPropertyItem("CollapseToolsPanel", C1Localizer.GetString("C1ReportViewer.SmartTag.CollapseToolsPanel", "Collapse tools panel"), "UI"));

				_actions.Add(new DesignerActionPropertyItem("PagedView", C1Localizer.GetString("C1ReportViewer.SmartTag.PagedView", "Paged view"), "Options"));
				AddBaseSortedActionItems(_actions);
			}
			return _actions;
		}

	}


	/// <summary>
	/// <see cref="TypeConverter"/> used to edit properties that represent reports.
	/// </summary>
	/// <remarks>
	/// <para><see cref="SubreportFieldConverter"/> provides a list of available reports.</para>
	/// <para>You can use this <see cref="TypeConverter"/> in classes that derive from <see cref="Field"/> to 
	/// edit properties that represent reports.</para>
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class SubreportFieldConverter : StringConverter
	{
		private static string _strNull = C1Localizer.GetString("(none)");

		/// <summary>
		/// Returns whether this converter can convert an object of one type to the type of this converter.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that can be used to gain additional context information.</param>
		/// <param name="type">A <see cref="Type"/> that represents the type you want to convert from.</param>
		/// <returns>True if <paramref name="type"/> is string, false otherwise.</returns>
		override public bool CanConvertFrom(ITypeDescriptorContext ctx, Type type)
		{
			return (type == typeof(string))
				? true
				: base.CanConvertFrom(ctx, type);
		}

		/// <summary>
		/// Converts the given value to the type of this converter.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that can be used to gain additional context information.</param>
		/// <param name="ci">The <see cref="CultureInfo"/> to use as the current culture.</param>
		/// <param name="value">The <see cref="Object"/> to convert.</param>
		/// <returns>A <see cref="C1Report"/> object with the specified name, or null if no report by the specified name 
		/// could be found in the report definition file.</returns>
		override public object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object value)
		{
			// get subreport name
			string name = value as string;
			if (name == null)
				return base.ConvertFrom(ctx, ci, value);

			// none?
			if (name == _strNull)
				return null;

			// get control
			C1ReportViewer report = ReportHelper.GetReportViewer(ctx);
			if (report == null)
				return base.ConvertFrom(ctx, ci, value);

			// get report list from control
			/*
			foreach (C1Report rpt in report.ReportList)
			{
				if (string.Compare(rpt.ReportName, name, true, CultureInfo.CurrentCulture) == 0)
				{
					return rpt;
				}
			}*/

			// not found
			return base.ConvertFrom(ctx, ci, value);
		}

		/// <summary>
		/// Returns whether this converter can convert the object to the specified type.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that can be used to gain additional context information.</param>
		/// <param name="type">A <see cref="Type"/> that represents the type you want to convert to.</param>
		/// <returns>True if <paramref name="type"/> is string, false otherwise.</returns>
		override public bool CanConvertTo(ITypeDescriptorContext ctx, Type type)
		{
			return (type == typeof(string))
				? true
				: base.CanConvertTo(ctx, type);
		}

		/// <summary>
		/// Converts the given value object to the specified type.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that can be used to gain additional context information.</param>
		/// <param name="ci">The <see cref="CultureInfo"/> to use as the current culture.</param>
		/// <param name="value">The <see cref="Object"/> to convert.</param>
		/// <param name="type">A <see cref="Type"/> that represents the type you want to convert to.</param>
		/// <returns>The name of the report if <paramref name="type"/> is string, null otherwise.</returns>
		override public object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (type == typeof(string))
			{
				if (value == null) return _strNull;
				if (value is C1.C1Report.C1Report) return ((C1.C1Report.C1Report)value).ReportName;
				if (value is C1ReportViewer) return ((C1ReportViewer)value).ReportName;
			}
			return base.ConvertTo(ctx, ci, value, type);
		}
		/// <summary>
		/// Returns whether this object supports properties, using the specified context.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that provides a format context.</param>
		/// <returns>True because <see cref="GetStandardValues"/> should be called to find the properties of this object.</returns>
		override public bool GetStandardValuesSupported(ITypeDescriptorContext ctx)
		{
			return true;
		}
		/// <summary>
		/// Returns whether the collection of standard values returned from 
		/// <see cref="GetStandardValues"/> is an exclusive list of possible values, 
		/// using the specified context.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that provides a format context.</param>
		/// <returns>True because the <see cref="TypeConverter.StandardValuesCollection"/> returned 
		/// from <see cref="GetStandardValues"/> is an exhaustive list of possible values.</returns>
		override public bool GetStandardValuesExclusive(ITypeDescriptorContext ctx)
		{
			return true;
		}
		/// <summary>
		/// Returns a collection of report names.
		/// </summary>
		/// <param name="ctx">An <see cref="ITypeDescriptorContext"/> that provides a format context that can be 
		/// used to extract additional information about the environment from which this converter is invoked. 
		/// This parameter or properties of this parameter can be a null reference (Nothing in Visual Basic).</param>
		/// <returns>A <see cref="TypeConverter.StandardValuesCollection"/> that holds a standard set of valid values.</returns>
		override public StandardValuesCollection GetStandardValues(ITypeDescriptorContext ctx)
		{
			C1ReportViewer report = ReportHelper.GetReportViewer(ctx);
			if (report == null)
				return base.GetStandardValues(ctx);
			return new StandardValuesCollection(report.GetReportList(report.FileName));
		}
	}

	/// <summary>
	/// ReportFile Location Editor.
	/// </summary>
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class ReportFileLocationEditor : System.Drawing.Design.UITypeEditor
	{
		public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return System.Drawing.Design.UITypeEditorEditStyle.Modal;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="provider"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			using (System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog())
			{
				ofd.AddExtension = true;
				C1ReportViewer aC1ReportViewer = ReportHelper.GetReportViewer(context);
				ofd.InitialDirectory = Common.ResolvePhysicalPath(aC1ReportViewer, "~/");
				string prevFileName = aC1ReportViewer.FileName;
				if (!string.IsNullOrEmpty(prevFileName))
				{
					
					System.IO.FileInfo file = new System.IO.FileInfo(Common.ResolvePhysicalPath(aC1ReportViewer, prevFileName));
					if (file.Exists)
					{
						ofd.InitialDirectory = file.Directory.FullName;
						ofd.FileName = file.FullName;
					}
				}
				ofd.Filter = "C1Report report definition files (*.xml)|*.xml|RDL report definition files (*.rdl)|*.rdl|C1PrintDocument files (*.c1dx;*.c1d)|*.c1dx;*.c1d|All supported formats (*.xml;*.rdl;*.c1dx;*.c1d)|*.xml;*.rdl;*.c1dx;*.c1d|All files (*.*)|*.*";
				ofd.FilterIndex = 4;
				if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				{
					string fileName = ofd.FileName;
					string rootPath = Common.ResolvePhysicalPath(aC1ReportViewer, "~/");
					if (fileName.StartsWith(rootPath))
					{
						fileName = "~/" + fileName.Substring(rootPath.Length).Replace("\\", "/");
					}
					if (prevFileName != fileName)
					{
						aC1ReportViewer.ReportName = "";
					}
					return fileName;
				}
			}
			return value;
		}
	}

	/// <summary>
	/// static utilities
	/// </summary>
	internal class ReportHelper
	{


		// navigate an instance object to find the parent report <<B158>>
		internal static C1ReportViewer GetReportViewer(ITypeDescriptorContext ctx)
		{
			if (ctx == null)
				return null;
			// get instance (can be a report object or an ICustomTypeDescriptor) <<B177>>
			object instance = ctx.Instance;
			if (instance is ICustomTypeDescriptor)
			{
				instance = ((ICustomTypeDescriptor)instance).GetPropertyOwner(ctx.PropertyDescriptor);
			}
			C1ReportViewer report = instance as C1ReportViewer;
			if (instance is C1ReportViewer) report = ((C1ReportViewer)instance);
			if (instance is C1ReportViewerActionList) report = ((C1ReportViewerActionList)instance)._reportViewer;

			return report;
		}



	}
}
