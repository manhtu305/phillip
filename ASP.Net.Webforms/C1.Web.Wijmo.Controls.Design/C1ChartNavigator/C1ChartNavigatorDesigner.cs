﻿using System;
using System.Collections.Generic;
using System.Web.UI.Design;
using System.Drawing;
using System.Web.UI;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1ChartNavigator
{
    using C1.Web.Wijmo.Controls.C1Chart;
    using C1.Web.Wijmo.Controls.Design.C1LineChart;
    using System.ComponentModel.Design;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using C1.Web.Wijmo.Controls.Design.C1ChartCore;

    /// <summary>
    /// Provide a C1ChartNavigatorDesigner class for extending the design-mode of C1ChartNavigator control;
    /// </summary>
    public class C1ChartNavigatorDesigner : C1ChartCoreDesigner
    {
        #region Fields

        private DesignerActionListCollection _actions;

        #endregion

        /// <summary>
        /// Initial the control designer and loads the specified component.
        /// </summary>
        /// <param name="component"></param>
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
        }

        protected override void AddSampleData()
        {
            C1ChartNavigator chartNavigator = Component as C1ChartNavigator;
            LineChartSeries series = new LineChartSeries();
            series.Label = "SampleData";
            series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
            series.Data.Y.AddRange(new double[] { 20, 22, 19, 24, 25 });
            chartNavigator.SeriesList.Add(series);
        }

        /// <summary>
        /// Override ActionLists
        /// </summary>
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (_actions == null)
                {
                    _actions = new DesignerActionListCollection();
                    _actions.Add(new C1ChartNavigatorActionList(this));
                }
                return _actions;
            }
        }

        protected override StringBuilder AddVitrualFunctionScript()
        {
            StringBuilder funcStringBuidler = base.AddVitrualFunctionScript();

            C1ChartNavigator chartNavigator = Component as C1ChartNavigator;
            var i = chartNavigator.Indicator;
            if (i != null)
            {
                NavigatorIndicator indicator = i as NavigatorIndicator;
                if (indicator != null)
                {
                    if (!String.IsNullOrEmpty(indicator.Content.Function))
                    {
                        funcStringBuidler.AppendLine(String.Format(" function {0} () {{}};", indicator.Content.Function));
                    }
                }
            }

            return funcStringBuidler;
        }
    }

    /// <summary>
    /// Provides a custom class for types that define a list of items used to create a smart tag.
    /// </summary>
    internal class C1ChartNavigatorActionList : C1ChartCoreDesignerActionList
    {
        #region Fields

        private C1ChartNavigator _chartNavigator;
        private DesignerActionItemCollection _actions;

        #endregion

        #region Constructor

        /// <summary>
        /// CustomControlActionList constructor.
        /// </summary>
        /// <param name="designer">The Specified C1ChartNavigatorDesigner designer.</param>
        public C1ChartNavigatorActionList(C1ChartNavigatorDesigner designer)
            :base(designer)
        {
            _chartNavigator = designer.Component as C1ChartNavigator;
        }

        #endregion

        #region Properties

        internal override bool DisplayThemeSupport()
        {
            return false;
        }

        internal override bool SupportWijmoControlMode
        {
            get
            {
                return false;
            }
        }

        public string TargetSelector
        {
            get
            {
                return (string)GetProperty(_chartNavigator, "TargetSelector").GetValue(_chartNavigator);
            }
            set
            {
                SetProperty("TargetSelector", value);
            }
        }

        #endregion

        /// <summary>
        /// Override GetSortedActionItems method.
        /// </summary>
        /// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_actions == null)
            {
                _actions = base.GetSortedActionItems();

                for (int i = 0; i < _actions.Count; i++)
                {
                    DesignerActionMethodItem action = _actions[i] as DesignerActionMethodItem;
                    if (action != null && action.MemberName == "EditSeriesHoverStyles")
                    {
                        _actions.RemoveAt(i);
                        break;
                    }
                }

                _actions.Add(new DesignerActionPropertyItem("TargetSelector", C1Localizer.GetString("C1ChartCore.ChartNavigator.SmartTag.TargetSelector", "TargetSelector"), "TargetSelector", C1Localizer.GetString("C1ChartCore.ChartNavigator.SmartTag.TargetSelectorDescription")));
            }

            return _actions;
        }
    }
}
