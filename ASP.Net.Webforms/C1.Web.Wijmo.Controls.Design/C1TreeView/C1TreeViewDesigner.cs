﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.IO;
using System.Windows.Forms.Design;
using System.Globalization;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Drawing.Design;
using System.Diagnostics;


namespace C1.Web.Wijmo.Controls.Design.C1TreeView
{
    using C1.Web.Wijmo.Controls.C1TreeView;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using C1.Web.Wijmo.Controls.Design.Utils;

    [SupportsPreviewControl(true)]
    public class C1TreeViewDesigner : C1HierarchicalDataBoundControlDesigner
    {
        #region Fields

        //private DesignerAutoFormatCollection _autoFormats;
        
        private TemplateGroupCollection _templateGroups;
        private static readonly string[] _templateNames;

        private object _control;

        #endregion

        static C1TreeViewDesigner()
        {
            _templateNames = new string[] { "NodesTemplate" };
        }

        /// <summary>
        /// Allow resize.
        /// </summary>
        public override bool AllowResize
        {
            get
            {
                return false;
                //return true;
            }
        }


        public override string GetDesignTimeHtml()
        {
            if (this._control is C1TreeView)
            {
                C1TreeView treeView = (C1TreeView)this._control;
                if (treeView.WijmoControlMode == WijmoControlMode.Mobile)
                {
                    //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                    return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
                }
                string phantomNodes = "<div style=\"color:white;background-color:gray;font-size:10pt;width:150px\"><div style=\"color:white;background-color:gray;font-size:10pt;\">Node 1</div><div style=\"color:white;background-color:gray;font-size:10pt;padding-left:20px\">Node 11</div><div style=\"color:white;background-color:gray;font-size:10pt;padding-left:20px\">Node 12</div><div style=\"color:white;background-color:gray;font-size:10pt;\">Node 2</div><div>";


                if (treeView.Nodes.Count == 0
                    &&
                    string.IsNullOrEmpty(treeView.DataSourceID))
                {
                    return phantomNodes;
                }
            }
            return base.GetDesignTimeHtml();
        }

        /// <summary>
        /// TemplateGroups implementation.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                TemplateGroupCollection templateGroups = base.TemplateGroups;
                if (this._templateGroups == null)
                {
                    this._templateGroups = new TemplateGroupCollection();
                    TemplateGroup group = new TemplateGroup("CommonItemTemplates", ((WebControl)base.ViewControl).ControlStyle);
                    TemplateDefinition templateDefinition = new TemplateDefinition(this, _templateNames[0], this._control, _templateNames[0], false);
                    templateDefinition.SupportsDataBinding = true;
                    group.AddTemplateDefinition(templateDefinition);
                    this._templateGroups.Add(group);
                }
                templateGroups.AddRange(this._templateGroups);
                return templateGroups;
            }
        }

        protected override bool UsePreviewControl
        {
            get
            {
                return true;
            }
        }

        public bool EditItems()
        {
            bool accept = false;
            if (this._control is C1TreeView)
            {
                C1TreeViewDesignerForm _editorForm = new C1TreeViewDesignerForm((C1TreeView)this._control);
                accept = ShowControlEditorForm(this._control, _editorForm);
            }
            //if (!accept)
            //    RestoreOriginalData(this._control, originalData);
            //ClearOriginalData(originalData);

            return accept;
        }

        public bool EditBindings()
        {
            C1TreeViewBindingsEditorForm _editorForm = new C1TreeViewBindingsEditorForm();
            _editorForm.SetComponent(this._control);
            return ShowControlEditorForm(this._control, _editorForm);
        }

        public bool ShowControlEditorForm(object control, Form _editorForm)
        {
            DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1TreeView.EditControl"));
                using (trans)
                {
                    IUIService service = (IUIService)serviceProvider.GetService(typeof(IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(_editorForm);
                    }
                    else
                        dr = DialogResult.None;
                    if (dr == DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = _editorForm.ShowDialog();

            _editorForm.Dispose();
            _editorForm = null;

            return dr == DialogResult.OK;
        }

        public override void Initialize(IComponent control)
        {
            base.Initialize(control);
            this._control = control;
            base.SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();
                lists.AddRange(base.ActionLists);
                lists.Add(new C1TreeViewActionList(this));
                return lists;
            }
        }
#if false
        //public override DesignerAutoFormatCollection AutoFormats
        //{
        //    get
        //    {
        //        if (this._autoFormats == null)
        //        {
        //            this._autoFormats = new DesignerAutoFormatCollection();
        //            C1DesignerAutoFormat.FillVisualStyleAutoFormatCollection(this._autoFormats, (C1TreeView)this._control);
        //            if (this._autoFormats.Count <= 0)
        //                return base.AutoFormats;
        //        }
        //        return this._autoFormats;
        //    }
        //}
#endif
    }


    public class C1TreeViewActionList : DesignerActionListBase
    {
        // Fields
        private C1TreeViewDesigner _designer;

        // Methods
        public C1TreeViewActionList(C1TreeViewDesigner designer)
            : base(designer)
        {
            this._designer = designer;
        }

        public void EditItems()
        {
            this._designer.EditItems();
        }

        public void EditBindings()
        {
            this._designer.EditBindings();
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection col = new DesignerActionItemCollection();
            col.Add(new DesignerActionMethodItem(this, 
                "EditItems", 
                C1Localizer.GetString("C1TreeView.SmartTag.EditTreeView"), "", 
                C1Localizer.GetString("C1TreeView.SmartTag.EditTreeViewDescription"), 
                true));
            col.Add(new DesignerActionMethodItem(this, 
                "EditBindings", 
                C1Localizer.GetString("C1TreeView.SmartTag.EditTreeViewDatabindings"), "", 
                C1Localizer.GetString("C1TreeView.SmartTag.EditTreeViewDatabindingsDescription"), 
                true));
            AddBaseSortedActionItems(col);
            return col;
        }
    }
}




