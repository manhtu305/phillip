﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace C1.Web.Wijmo.Controls.Design.C1TreeView
{
	using C1.Web.Wijmo.Controls.C1TreeView;

    public class C1TreeViewBindingsEditor : UITypeEditor
    {
        private static C1TreeViewBindingsEditorForm s_form = null;

        public static bool Edit(object component)
        {
            if (s_form == null)
                s_form = new C1TreeViewBindingsEditorForm();
            s_form.SetComponent(component);
            DialogResult dr;
            IServiceProvider sp = ((IComponent)component).Site;
            if (sp != null)
            {
                IUIService service = (IUIService)sp.GetService(typeof(IUIService));
                if (service != null)
                {
                    dr = service.ShowDialog(s_form);
                }
                else
                    dr = DialogResult.None;
            }
            else
                dr = s_form.ShowDialog();
            s_form.Dispose();
            s_form = null;
            return dr == DialogResult.OK;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            C1TreeView control = (C1TreeView)context.Instance;
            Edit(control);
            return control.DataBindings;
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }
}