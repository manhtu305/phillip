﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;


namespace C1.Web.Wijmo.Controls.Design.C1Dialog
{
	using C1.Web.Wijmo.Controls.C1Dialog;
	using C1.Web.Wijmo.Controls.Design.Utils;
	using C1.Web.Wijmo.Controls.Design.Localization;

	[SupportsPreviewControl(true)]
    public class C1DialogDesigner : C1ControlDesinger
	{
		private DesignerActionListCollection _actionList;
		private C1Dialog _control;

		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);

			_control = (C1Dialog)component;
			if (_control.Page == null && RootDesigner != null)
			{
				var page = RootDesigner.Component as Page;
				if (page != null)
				{
					_control.Page = page;
				}
			}
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			base.SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actionList == null)
				{
					_actionList = new DesignerActionListCollection();
					_actionList.AddRange(base.ActionLists);
					_actionList.Add(new C1DialogActionList(this));
				}
				return _actionList;
			}
		}

		#region Editible regions & DesignTime HTML

		public override string GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			regions.Add(new EditableDesignerRegion(this, "Content", false));
			string html = base.GetDesignTimeHtml(regions);
			html = html.Replace("DesignerRegionAttributeName", DesignerRegion.DesignerRegionAttributeName);

			return html;
		}

		public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
		{
			// Get a reference to the designer host
			var host = Component.Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
			if (host != null)
			{
				ITemplate template = _control.Content;
				if (template != null)
				{
					return ControlPersister.PersistTemplate(template, host);
				}
			}
			return String.Empty;
		}

		public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
		{
			var host = Component.Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
			if (host != null)
			{
				if (region.Name == "Content")
				{
					if (content == null || content.Length == 0)
					{
						this._control.Content = null;
					}
					else
					{
						ITemplate template = ControlParser.ParseTemplate(host, content);
						_control.Content = template;
					}
				}				
				OnComponentChanging(_control, new ComponentChangingEventArgs(_control, null));
				OnComponentChanged(_control, new ComponentChangedEventArgs(_control, null, null, null));
			}
		}

		#endregion
		
	}

	internal class C1DialogActionList : DesignerActionListBase
	{

		private DesignerActionItemCollection _items;
		private C1Dialog _window;

		public C1DialogActionList(C1DialogDesigner parent)
			: base(parent)
		{
			this._window = base.Component as C1Dialog;
		}

		public bool ShowOnLoad
		{
			get
			{
				return this._window.ShowOnLoad;
			}
			set
			{
				SetProperty("ShowOnLoad", value);
			}
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (this._items == null)
			{
				this._items = new DesignerActionItemCollection();
				this._items.Add(new DesignerActionPropertyItem("ShowOnLoad",
					C1Localizer.GetString("C1Window.SmartTag.ShowOnLoad"),
					"Behavior",
					C1Localizer.GetString("C1Window.SmartTag.ShowOnLoadDescription")));
				AddBaseSortedActionItems(this._items);
			}
			return this._items;
		}
	}
}