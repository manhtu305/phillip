﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Drawing; 

namespace C1.Web.Wijmo.Controls.Design.C1Slider
{
	using C1.Web.Wijmo.Controls.C1Slider;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// Provides a C1SliderDesigner class for extending the design-mode behavior of a C1Slider control.
	/// </summary>
    public class C1SliderDesigner : C1ControlDesinger
	{
		/// <summary>
		/// Gets C1Slider control from designer.
		/// </summary>
		public C1Slider C1Slider
		{
			get
			{
				return this.Component as C1Slider;
			}
		}

		public override string GetDesignTimeHtml()
		{
			if (this.C1Slider.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}

		/// <summary>
		/// Gets the action list collection for the control designer.
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1SliderDesignerActionList(this));
				return actionLists; 
			}
		}

		/// <summary>
		/// Provides a custom class for types that define a list of items used to create a smart tag panel.
		/// </summary>
		private class C1SliderDesignerActionList : DesignerActionListBase
		{
			#region ** fields
			private C1SliderDesigner _parent;
			private DesignerActionItemCollection items;
			#endregion end of ** fields..

			#region ** constructor
			/// <summary>
			/// CustomControlActionList constructor.
			/// </summary>
			/// <param name="parent">The Specified C1SliderDesigner designer.</param>
			public C1SliderDesignerActionList(C1SliderDesigner parent)
				: base(parent)
			{
				_parent = parent;
			}
			#endregion end of ** constructor.

			/// <summary>
			/// Override GetSortedActionItems method.
			/// </summary>
			/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (items == null)
				{
					items = new DesignerActionItemCollection();
					AddBaseSortedActionItems(items);
				}
				return items;
			}
		}
	}
}
