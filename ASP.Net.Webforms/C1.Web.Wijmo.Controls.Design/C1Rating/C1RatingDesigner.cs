﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.Design.C1Rating
{
	using C1.Web.Wijmo.Controls.C1Rating;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using System.Web.UI.Design;
	using System.ComponentModel;
	using System.ComponentModel.Design;

	/// <summary>
	/// Provides a C1RatingDesigner class for extending the design-mode behavior of a C1Rating control.
	/// </summary>
    public class C1RatingDesigner : C1ControlDesinger
	{
		/// <summary>
		/// Gets C1Rating control from designer.
		/// </summary>
		public C1Rating C1Rating
		{
			get
			{
				return this.Component as C1Rating;
			}
		}

		public override string GetDesignTimeHtml()
		{
			if (this.C1Rating.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}

		/// <summary>
		/// Initializes the control designer and loads the specified component.
		/// </summary>
		/// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);

			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			base.SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1RatingDesignerActionList(this));
				return actionLists; 
			}
		}

		private class C1RatingDesignerActionList: DesignerActionListBase
		{
			#region fields

			private C1RatingDesigner _parent;
			private DesignerActionItemCollection items;
			#endregion

			#region constructors
			
			/// <summary>
			/// C1RatingDesignerActionList constructor.
			/// </summary>
			/// <param name="parent">The Specified C1RatingDesigner designer.</param>
			public C1RatingDesignerActionList(C1RatingDesigner parent)
				: base(parent)
			{
				_parent = parent;
			}
			#endregion

			/// <summary>
			/// Override GetSortedActionItems method.
			/// </summary>
			/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (items == null)
				{
					items = new DesignerActionItemCollection();

					//items.Add(new DesignerActionPropertyItem("Count", "Count"));
					//items.Add(new DesignerActionPropertyItem("TotalValue", "TotalValue"));
					//items.Add(new DesignerActionPropertyItem("Split", "Split"));
					//items.Add(new DesignerActionPropertyItem("Value", "Value"));
					items.Add(new DesignerActionPropertyItem("Count", C1Localizer.GetString("C1Rating.SmartTag.Count")));
					items.Add(new DesignerActionPropertyItem("TotalValue", C1Localizer.GetString("C1Rating.SmartTag.TotalValue")));
					items.Add(new DesignerActionPropertyItem("Split", C1Localizer.GetString("C1Rating.SmartTag.Split")));
					items.Add(new DesignerActionPropertyItem("Value", C1Localizer.GetString("C1Rating.SmartTag.Value")));

					AddBaseSortedActionItems(items);
				}
				return items;
			}

			public double Value
			{
				get
				{
					return (double)this.GetProperty("Value");
				}
				set
				{
					this.SetProperty("Value", value);
				}
			}

			public int TotalValue
			{
				get
				{
					return (int)this.GetProperty("TotalValue");
				}
				set
				{
					this.SetProperty("TotalValue", value);
				}
			}

			public int Count
			{
				get
				{
					return (int)this.GetProperty("Count");
				}
				set
				{
					this.SetProperty("Count", value);
				}
			}

			public int Split
			{
				get
				{
					return (int)this.GetProperty("Split");
				}
				set
				{
					this.SetProperty("Split", value);
				}
			}
		}
	}
}
