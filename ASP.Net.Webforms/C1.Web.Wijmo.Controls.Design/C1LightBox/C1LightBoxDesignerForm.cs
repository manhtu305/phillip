﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Text;
using System.Reflection;

using System.Diagnostics;

namespace C1.Web.Wijmo.Controls.Design.C1LightBox
{
	using C1.Web.Wijmo.Controls.C1LightBox;

	public partial class C1LightBoxDesignerForm : C1BaseItemEditorForm
	{
		#region Custom Element Types enumeration
		/// <summary>
		/// Element Types enumerations used by Designer
		/// </summary>
		private enum ItemType
		{
			/// <summary>
			/// Indicates what default tp type must be used
			/// </summary>
			None,
			/// <summary>
			/// A lightbox item
			/// </summary>
			LightBoxItem,

			/// <summary>
			/// lightbox itself
			/// </summary>
			LightBox
		}

		#endregion  // Custom Element Types enumeration

		// C1LightBox local control
		private C1LightBox _lightBox;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="lightbox">C1LightBox Control</param>
		public C1LightBoxDesignerForm(C1LightBox lightbox)
			: base(lightbox)
		{
			try
			{
				this._lightBox = lightbox;
				InitializeComponent();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		/// <summary>
		/// Called to initialize the treeview with the object hierarchy.
		/// </summary>
		/// <param name="mainTreeView"></param>
		protected override void LoadControl(TreeView mainTreeView)
		{
			// populate mainTreeView with C1 control items
			mainTreeView.BeginUpdate();
			mainTreeView.Nodes.Clear();

			TreeNode rootNode = new TreeNode();
			C1ItemInfo itemInfo = GetItemInfo(GetItemType(_lightBox));
			string nodeText = _lightBox.ID == null ? itemInfo.DefaultNodeText : _lightBox.ID.ToString();
			C1NodeInfo nodeTag = new C1NodeInfo(_lightBox, itemInfo, nodeText);
			SetNodeAttributes(rootNode, nodeTag);

            foreach (C1LightBoxItem tp in _lightBox.Items)
			{
				TreeNode node = new TreeNode();
				nodeText = tp.ID;
				itemInfo = GetItemInfo(GetItemType(tp));
				nodeTag = new C1NodeInfo(tp, itemInfo, nodeText);
				SetNodeAttributes(node, nodeTag);

				rootNode.Nodes.Add(node);
			}

			mainTreeView.Nodes.Add(rootNode);
			mainTreeView.SelectedNode = mainTreeView.Nodes[0];
			mainTreeView.ExpandAll();
			mainTreeView.EndUpdate();
		}

		/// <summary>
		/// Gets the tp info according to the given tp type
		/// </summary>
		/// <param name="itemType">Item type</param>
		/// <returns>C1ItemInfo object that contains the tp info</returns>
		private C1ItemInfo GetItemInfo(ItemType itemType)
		{
			foreach (C1ItemInfo itemInfo in base.ItemsInfo)
			{
				if ((ItemType)itemInfo.ItemType == itemType)
					return itemInfo;
			}
			return null;
		}

		/// <summary>
		/// Gets the type of the given tp
		/// </summary>
		/// <param name="tp">Given tp for checking its type</param>
		/// <returns>The <seealso cref="ItemType"/> of given tp</returns>
		private ItemType GetItemType(object obj)
		{
			// Sets the default obj type
			ItemType it = ItemType.LightBoxItem;

			if (obj is C1LightBoxItem)
			{
				it = ItemType.LightBoxItem;
			}
			else if (obj is C1LightBox)
			{
				it = ItemType.LightBox;
			}

			return it;
		}

		/// <summary>
		/// Fills the allowable types that can compose the control. 
		/// You should load default tp in first place. 
		/// </summary>
		/// <param name="itemInfo">List of available control items to be filled</param>
		protected override List<C1ItemInfo> FillAvailableControlItems()
		{
			List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();
			C1ItemInfo itemInfo;

			itemInfo = new C1ItemInfo();
			itemInfo.ItemType = ItemType.LightBoxItem;
			itemInfo.EnableChildItems = false;
            itemInfo.ContextMenuStripText = "LightBoxItem";
			itemInfo.NodeImage = Properties.Resources.RootItemIco;
			itemInfo.DefaultNodeText = "LightBoxItem";
			itemInfo.Visible = true;
			itemInfo.Default = true;
			itemsInfo.Add(itemInfo);

			itemInfo = new C1ItemInfo();
			itemInfo.ItemType = ItemType.LightBox;
			itemInfo.EnableChildItems = false;
			itemInfo.ContextMenuStripText = "LightBox";
			itemInfo.NodeImage = Properties.Resources.GroupIco;
            itemInfo.DefaultNodeText = "LightBox";
			itemInfo.Visible = false;
			itemInfo.Default = false;
			itemsInfo.Add(itemInfo);

			return itemsInfo;
		}

		protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
		{
			base.AllowAdd = ((ItemType)itemInfo.ItemType == ItemType.LightBox);
			base.AllowInsert = false;
			base.AllowChangeType = false;
			base.AllowCopy = false;
			base.AllowPaste = false;
			base.AllowCut = false;
			base.AllowDelete = true;
			base.AllowRename = true;
			base.AllowMoveUp = false;
			base.AllowMoveDown = false;
			base.AllowMoveLeft = false;
			base.AllowMoveRight = false;
			base.AllowSaveXML = false;

			base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
		}

		/// <summary>
		/// Creates the C1NodeInfo object that will contain both the specific C1 control tp depending on the itemInfo
		/// and given itenInfo.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo object.</param>
		/// <param name="nodesList">Nodes that currently exist into the list where the new created tp 
		/// will be inserted.</param>
		/// <returns>Returns a C1 control that depends on the C1ItemInfo. Given nodesList
		/// could be used to obtain a new numbered name.</returns>
		protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
		{
			C1NodeInfo nodeInfo;
			string nodeText;

			C1LightBoxItem tp = new C1LightBoxItem();
			nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);

			// set the text for created tp
			tp.ID = nodeText;

			// create a new node info
			nodeInfo = new C1NodeInfo(tp, itemInfo, nodeText);

			// set nodeText property
			nodeInfo.NodeText = nodeText;

			return nodeInfo;
		}

		/// <summary>
		///  Indicates when an tp was inserted in a specific position.
		/// </summary>
		/// <param name="tp">The inserted tp.</param>
		/// <param name="destinationItem">The destination tp that will contain inserted tp.</param>
		/// <param name="destinationIndex">The index position of given tp within destinationItem items collection.</param>
		protected override void Insert(object tp, object destinationItem, int destinationIndex)
		{
			try
			{
				((C1LightBox)destinationItem).Items.Insert(destinationIndex, (C1LightBoxItem)tp);
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// <summary>
		///  Indicates when an tp was deleted.
		/// </summary>
		/// <param name="item">The deleted obj.</param>
		/// <param name="parent">The parent that contains the deleted tp.</param>
		protected override void Delete(object item)
		{
			try
			{
				_lightBox.Items.Remove((C1LightBoxItem)item);

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		const string NEW_LINE = "\r\n";

		internal override void ShowPreviewHtml(C1WebBrowser previewer)
		{
			try
			{
				string sResultBodyContent = "";
				StringBuilder sb = new StringBuilder();
				System.IO.StringWriter tw = new System.IO.StringWriter(sb);
				System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
				_lightBox.RenderControl(htw);
				sResultBodyContent = sb.ToString();
				string sDocumentContent = "";
				sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
				sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
				sDocumentContent += "<head>" + NEW_LINE;
				sDocumentContent += "</head>" + NEW_LINE;
				sDocumentContent += "<body>" + NEW_LINE;
				sDocumentContent += sResultBodyContent + NEW_LINE;
				sDocumentContent += "</body>" + NEW_LINE;
				sDocumentContent += "</html>" + NEW_LINE;
				//previewer.Document.Write(sDocumentContent);
				previewer.DocumentWrite(sDocumentContent, this._lightBox);
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
			}
		}

		#region Utils

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		/// <param name="selectedNode"></param>
		protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
		{
			bool cancel = e.CancelEdit;
			FinishLabelEdit(e.Node, e.Label, ref cancel);
			e.CancelEdit = cancel;
		}

        /// <summary>
        /// Performs some action when a property value changes.
        /// </summary>
        /// <param name="changedGridItem">The grid item changed.</param>
        /// <param name="selectedNode">Current selected node.</param>
        protected override void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
        {
            if (changedGridItem.PropertyDescriptor.Name == "ID")
                if (selectedNode.Text != changedGridItem.Value.ToString())
                    selectedNode.Text = changedGridItem.Value.ToString();
        }


		private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
		{
			string nodeText = "";
			string property = "ID";
			object obj = ((C1NodeInfo)node.Tag).Element;
			C1ItemInfo itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;

			if (text != null) //&& text.CompareTo(_emptyItemText) != 0
			{
				nodeText = text;
			}
			else
			{
				if (obj is C1LightBox)
				{
					nodeText = ((C1LightBox)obj).ID == null ? "" : ((C1LightBox)obj).ID.ToString();
				}
				else if (obj is C1LightBoxItem)
				{
					if (!string.IsNullOrEmpty(((C1LightBoxItem)obj).ID))
						nodeText = ((C1LightBoxItem)obj).ID;

					if (string.IsNullOrEmpty(nodeText))
					{
						if (node.Parent == null)
						{
							nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Nodes);
						}
						else
						{
							nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Parent.Nodes);
						}
					}
				}
			}

			if (obj is C1LightBox)
				property = "ID";
			else if (obj is C1LightBoxItem)
				property = "ID";

			// set new text value to selected object and property grid
			TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
			base.RefreshPropertyGrid();
			node.Text = nodeText;
		}

		#endregion

	}
}
