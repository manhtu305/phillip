﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.IO;
using System.Globalization;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;


namespace C1.Web.Wijmo.Controls.Design.C1LightBox
{
    using C1.Web.Wijmo.Controls.C1LightBox;
    using C1.Web.Wijmo.Controls.Design.Localization;


    [SupportsPreviewControl(true)]
    public class C1LightBoxDesigner : C1ControlDesinger
    {
        #region Fields

        private C1LightBox _lightbox;
        internal DesignerActionUIService _designerActionUISvc = null;

        #endregion

        #region Constructor

        public C1LightBoxDesigner()
        {
        }

        #endregion

        public override string GetDesignTimeHtml()
        {
            if (_lightbox.WijmoControlMode == WijmoControlMode.Mobile)
            {
                //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
            }
            return base.GetDesignTimeHtml();
        }

        public override bool AllowResize
        {
            get
            {
                return true;
            }
        }


        protected override bool UsePreviewControl
        {
            get
            {
                return true;
            }
        }

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this._lightbox = (C1LightBox)component;

            if (_lightbox.Page == null && RootDesigner != null)
            {
                var page = RootDesigner.Component as Page;
                if (page != null)
                {
                    _lightbox.Page = page;
                }
            }

            this._designerActionUISvc = GetService(typeof(DesignerActionUIService)) as DesignerActionUIService;
        }

        public bool ShowControlEditorForm(object control, System.Windows.Forms.Form editorForm)
        {
            System.Windows.Forms.DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1LightBox.EditControl"));
                using (trans)
                {
                    System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(editorForm);
                    }
                    else
                        dr = System.Windows.Forms.DialogResult.None;
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = editorForm.ShowDialog();

            editorForm.Dispose();
            editorForm = null;

            return dr == System.Windows.Forms.DialogResult.OK;
        }

        public void OpenBuilder()
        {
            List<C1LightBoxItem> originalData = new List<C1LightBoxItem>();
            foreach (C1LightBoxItem tp in _lightbox.Items)
            {
                originalData.Add(tp);
            }

            C1LightBoxDesignerForm editorForm = new C1LightBoxDesignerForm(this._lightbox);
            bool accept = ShowControlEditorForm(this._lightbox, editorForm);

            if (accept)
                this.UpdateDesignTimeHtml();
            else
            {
                _lightbox.Items.Clear();
                foreach (C1LightBoxItem tp in originalData)
                {
                    _lightbox.Items.Add(tp);
                }
            }
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();
                lists.AddRange(base.ActionLists);
                lists.Add(new C1LightBoxActionList(this));
                return lists;
            }
        }

        protected override string GetEmptyDesignTimeHtml()
        {
            return CreatePlaceHolderDesignTimeHtml("ComponentOne LightBox Control");
        }

        protected override string GetErrorDesignTimeHtml(Exception e)
        {
            return CreatePlaceHolderDesignTimeHtml(e.Message);
        }
    }

    internal class C1LightBoxActionList : DesignerActionListBase
    {
        #region Fields

        private C1LightBox _lightbox;

        #endregion

        #region Constructor

        public C1LightBoxActionList(C1LightBoxDesigner designer)
            : base(designer)
        {
            this._lightbox = designer.Component as C1LightBox;
        }

        #endregion

        #region Action Lists

        public int MaxWidth
        {
            get
            {
                return this._lightbox.MaxWidth;
            }
            set
            {
                SetProperty("MaxWidth", value);
            }
        }

        public int MaxHeight
        {
            get
            {
                return this._lightbox.MaxHeight;
            }
            set
            {
                SetProperty("MaxHeight", value);
            }
        }

        public bool AutoSize
        {
            get
            {
                return this._lightbox.AutoSize;
            }
            set
            {
                SetProperty("AutoSize", value);
            }
        }

        public TextPosition TextPosition
        {
            get
            {
                return this._lightbox.TextPosition;
            }
            set
            {
                SetProperty("TextPosition", value);
            }
        }

        public CtrlButtons CtrlButtons
        {
            get
            {
                return this._lightbox.CtrlButtons;
            }
            set
            {
                SetProperty("CtrlButtons", value);
            }
        }

        public DialogButtons DialogButtons
        {
            get
            {
                return this._lightbox.DialogButtons;
            }
            set
            {
                SetProperty("DialogButtons", value);
            }
        }

        public void OpenBuilder()
        {
            C1LightBoxDesigner designer = this.Designer as C1LightBoxDesigner;
            designer.OpenBuilder();
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection actions = new DesignerActionItemCollection();
            actions.Add(new DesignerActionPropertyItem("MaxWidth", C1Localizer.GetString("C1LightBox.SmartTag.MaxWidth"), "Display"));
            actions.Add(new DesignerActionPropertyItem("MaxHeight", C1Localizer.GetString("C1LightBox.SmartTag.MaxHeight"), "Display"));
            actions.Add(new DesignerActionPropertyItem("AutoSize", C1Localizer.GetString("C1LightBox.SmartTag.AutoSize"), "Display"));
            actions.Add(new DesignerActionPropertyItem("TextPosition", C1Localizer.GetString("C1LightBox.SmartTag.TextPosition"), "Display"));
            actions.Add(new DesignerActionPropertyItem("CtrlButtons", C1Localizer.GetString("C1LightBox.SmartTag.CtrlButtons"), "Display"));
            actions.Add(new DesignerActionPropertyItem("DialogButtons", C1Localizer.GetString("C1LightBox.SmartTag.DialogButtons"), "Display"));
            actions.Add(new DesignerActionMethodItem(this, "OpenBuilder", C1Localizer.GetString("C1LightBox.SmartTag.OpenBuilder"), "", C1Localizer.GetString("C1LightBox.SmartTag.OpenBuilder"), true));

            this.AddBaseSortedActionItems(actions);

            return actions;
        }

        #endregion
    }
}
