﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMap
{
    using C1.Web.Wijmo.Controls.C1SiteMap;
    using C1.Web.Wijmo.Controls.Design.C1TreeView;

    public class C1SiteMapBindingsEditor : UITypeEditor
    {
        private static C1TreeViewBindingsEditorForm _editorForm = null;

        public static bool Edit(object component)
        {
            if (_editorForm == null)
                _editorForm = new C1TreeViewBindingsEditorForm();

            _editorForm.SetComponent(component);

            DialogResult dr;
            IServiceProvider sp = ((IComponent)component).Site;
            if (sp != null)
            {
                IUIService service = (IUIService)sp.GetService(typeof(IUIService));
                if (service != null)
                {
                    dr = service.ShowDialog(_editorForm);
                }
                else
                    dr = DialogResult.None;
            }
            else
                dr = _editorForm.ShowDialog();

            _editorForm.Dispose();
            _editorForm = null;

            return dr == DialogResult.OK;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            C1SiteMap control = (C1SiteMap)context.Instance;
            Edit(control);
            return control.DataBindings;
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }
}