﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMap
{
	using C1.Web.Wijmo.Controls.C1SiteMap;
	using System.IO;
	using C1.Web.Wijmo.Controls.Design.Localization;

    internal enum ItemType
    {
        /// <summary>
        /// Item is C1SiteMapNode
        /// </summary>
        C1SiteMapNode,
        /// <summary>
        /// Item is C1SiteMap
        /// </summary>
        C1SiteMap
    }

    public partial class C1SiteMapDesignerForm : C1BaseItemEditorForm
    {
        #region Fields

        private C1SiteMap _siteMap;
        
        // clipboard for current control
        private Stream _clipboardData;
        private C1ItemInfo _clipboardType;

        #endregion

		public C1SiteMapDesignerForm(C1SiteMap treeView)
			: base(treeView)
        {
            _siteMap = treeView;
            InitializeComponent();
        }  

        protected override List<C1ItemInfo> FillAvailableControlItems()
        {
            List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();
            C1ItemInfo item;

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1SiteMapNode;
            item.EnableChildItems = true;
            item.NodeImage = Properties.Resources.LinkItemIco;
            item.DefaultNodeText = "C1SiteMapNode";
            item.ContextMenuStripText = "C1SiteMapNode";
            item.Default = true;
            item.Visible = true;
            itemsInfo.Add(item);

            item = new C1ItemInfo();
            item.ItemType = ItemType.C1SiteMap;
            item.EnableChildItems = true;
            item.ContextMenuStripText = "C1SiteMap";
            item.NodeImage = Properties.Resources.RootItemIco;
            item.DefaultNodeText = "C1SiteMap";
            item.Visible = false;
            itemsInfo.Add(item);

            return itemsInfo;
        }

        protected override void MoveDrag(TreeNode sourceNode, TreeNode destinationNode)
        {
            if (!IsChildNode(sourceNode, destinationNode))
            {
                base.MoveDrag(sourceNode, destinationNode);
            }
        }

        private bool IsChildNode(TreeNode sourceNode, TreeNode destinationNode)
        {
            TreeNode pnode = destinationNode.Parent;
            if (pnode != null) 
            {
                if (pnode == sourceNode)
                {
                    return true;
                }
                else
                {
                    return IsChildNode(sourceNode, pnode);
                }
            }
            return false;
        }

        // Gets image from resource. This method is used just only for testing purposes.
        // This form must be moved to C1.Web.UI.Design.2 assembly when designer is finished.
        private System.Drawing.Image GetImageFromResource(string imageName)
        {
            string resId;
            Stream stream;
            resId = string.Format("C1.Web.UI.Controls.C1SiteMap.Resources.{0}.png", imageName);
            stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resId);
            if (stream != null)
                return System.Drawing.Image.FromStream(stream);
            return null;
        }
        /// <summary>
        /// Loads <see cref="TreeNode"/> nodes into the main TreeView
        /// </summary>
        /// <param name="mainTreeView"></param>
        protected override void LoadControl(TreeView mainTreeView)
        {
            string nodeText;
            object nodeTag;
            C1ItemInfo itemInfo;

            mainTreeView.BeginUpdate();
            mainTreeView.Nodes.Clear();

            TreeNode treeView = new TreeNode();
            itemInfo = GetItemInfo(GetItemType(_siteMap));

            nodeText = _siteMap.ID == null ? itemInfo.DefaultNodeText : _siteMap.ID.ToString();
            nodeTag = new C1NodeInfo(_siteMap, itemInfo, nodeText);
            SetNodeAttributes(treeView, nodeText, nodeTag);

            foreach (C1SiteMapNode node in _siteMap.Nodes)
            {
                TreeNode treeViewNode = new TreeNode();
                nodeText = node.Text;
                itemInfo = GetItemInfo(GetItemType(node));
                nodeTag = new C1NodeInfo(node, itemInfo, nodeText);
                SetNodeAttributes(treeViewNode, nodeText, nodeTag);

                IterateChildNodes(treeViewNode, node);

                treeView.Nodes.Add(treeViewNode);
            }

            mainTreeView.Nodes.Add(treeView);
            mainTreeView.SelectedNode = mainTreeView.Nodes[0];
            mainTreeView.ExpandAll();
            mainTreeView.EndUpdate();
        }

        /// <summary>
        /// Loads nested items from current item
        /// </summary>
        /// <param name="menuItemNode">TreeNode parent</param>
        /// <param name="item">Control item</param>
        private void IterateChildNodes(TreeNode treeViewNode, C1SiteMapNode parentNode)
        {
            string nodeText;
            object nodeTag;
            C1ItemInfo itemInfo;

            treeViewNode.Nodes.Clear();
            foreach (C1SiteMapNode node in parentNode.Nodes)
            {
                TreeNode treeNode = new TreeNode();
                nodeText = node.Text;
                itemInfo = GetItemInfo(GetItemType(node));
                nodeTag = new C1NodeInfo(node, itemInfo, nodeText);
                SetNodeAttributes(treeNode, nodeText, nodeTag);

                IterateChildNodes(treeNode, node);

                treeViewNode.Nodes.Add(treeNode);
            }
        }

        /// <summary>
        /// Performs some action when a property value changes.
        /// </summary>
        /// <param name="changedGridItem">The grid item changed.</param>
        /// <param name="selectedNode">Current selected node.</param>
        protected override void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
        {
            object obj = ((C1NodeInfo)selectedNode.Tag).Element;
            if (obj is C1SiteMap)
            {
                if (changedGridItem.PropertyDescriptor.Name == "ID")
                    if (selectedNode.Text != changedGridItem.Value.ToString())
                        selectedNode.Text = changedGridItem.Value.ToString();
            }
            else if (obj is C1SiteMapNode)
            {
                C1SiteMapNode node = obj as C1SiteMapNode;
                if (changedGridItem.PropertyDescriptor.Name == "Text")
                    if (selectedNode.Text != changedGridItem.Value.ToString()) 
                    {
                        selectedNode.Text = changedGridItem.Value.ToString();
                        if(string.IsNullOrEmpty(changedGridItem.Value.ToString()))
                        {
                            selectedNode.Text = node.ID;
                        }
                    }                        

                if (changedGridItem.PropertyDescriptor.Name == "ID")
                    if (string.IsNullOrEmpty(node.Text) && 
                        selectedNode.Text != changedGridItem.Value.ToString())
                        selectedNode.Text = changedGridItem.Value.ToString();
            }

        }

        /// <summary>
        /// Gets de item info according to the given item type
        /// </summary>
        /// <param name="itemType">Item type</param>
        /// <returns>C1ItemInfo object that contains the item info</returns>
        private C1ItemInfo GetItemInfo(ItemType itemType)
        {
            foreach (C1ItemInfo itemInfo in base.ItemsInfo)
            {
                if ((ItemType)itemInfo.ItemType == itemType)
                    return itemInfo;
            }
            return null;
        }

        /// <summary>
        /// Gets the type of the given item
        /// </summary>
        /// <param name="item">Given item for checking its type</param>
        /// <returns>The <seealso cref="ItemType"/> of given item</returns>
        private ItemType GetItemType(object item)
        {
            // Sets the default item type
            ItemType elementType = ItemType.C1SiteMapNode;

            if (item is C1SiteMap)
            {
                elementType = ItemType.C1SiteMap;
            }
            else if (item is C1SiteMapNode)
            {
                elementType = ItemType.C1SiteMapNode;
            }
            return elementType;
        }



        const string NEW_LINE = "\r\n";
        /// <summary>
        /// Shows current control into preview tab
        /// </summary>
        /// <param name="previewer">The web browser previewer</param>
        internal override void ShowPreviewHtml(C1WebBrowser previewer)
        {
            try
            {

                string sResultBodyContent = "";
                StringBuilder sb = new StringBuilder();
                System.IO.StringWriter tw = new System.IO.StringWriter(sb);
                System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
                // save the cssclass
                // rendering it sets the property

                //Nov 26,2009 by Willow Yang to fix bug 7996
                //set ChildControlsCreated to false to recreate the controls.
                _siteMap.GetType().InvokeMember("ChildControlsCreated", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.SetProperty, null, _siteMap, new object[] { false });
                //end

                string s = _siteMap.CssClass;
                _siteMap.RenderControl(htw);
                _siteMap.CssClass = s;
                sResultBodyContent = sb.ToString();
                string sDocumentContent = "";
                sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
                sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
                sDocumentContent += "<head>" + NEW_LINE;
                //sDocumentContent += sScriptContent + NEW_LINE;
                sDocumentContent += "</head>" + NEW_LINE;
                sDocumentContent += "<body>" + NEW_LINE;
                sDocumentContent += sResultBodyContent + NEW_LINE;
                sDocumentContent += "</body>" + NEW_LINE;
                sDocumentContent += "</html>" + NEW_LINE;
                //previewer.Document.Write(sDocumentContent);
                previewer.DocumentWrite(sDocumentContent, this._siteMap);
                //previewer.Navigate("about:blank");


            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
            }
        }


        protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
        {
            base.AllowAdd = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMap, ItemType.C1SiteMapNode });
            base.AllowInsert = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMapNode });
            base.AllowCopy = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMapNode });
            base.AllowCut = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMapNode });
            base.AllowDelete = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMapNode });
            base.AllowRename = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMap, ItemType.C1SiteMapNode });
            base.AllowMoveUp = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMapNode });
            base.AllowMoveDown = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMapNode });
            base.AllowMoveLeft = EnableMoveLeft(itemInfo, parentItemInfo);
            base.AllowMoveRight = EnableMoveRight(itemInfo, previousItemInfo);

            base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
        }

        protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
        {
            C1NodeInfo nodeInfo;
            string nodeText;

            C1SiteMapNode item = new C1SiteMapNode();

            // itemInfo.DefaultNodeText could be used as default item text. if not, you can get a new numbered name.
            // item.Text = itemInfo.DefaultNodeText;
            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);

            // set the text for created item
            item.Text = nodeText;

            // create a new node info
            nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);

            // set nodeText property
            nodeInfo.NodeText = nodeText;

            return nodeInfo;

        }

        private C1SiteMapNode CreateNewItem()
        {
            return new C1SiteMapNode();
        }

        private bool EnableActionByItemType(C1ItemInfo itemInfo, ItemType[] types)
        {
            bool enable = false;
            if (itemInfo != null)
            {
                foreach (ItemType type in types)
                {
                    enable |= itemInfo.ItemType.Equals(type);
                }
            }
            return enable;
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the left
        /// </summary>
        private bool EnableMoveLeft(C1ItemInfo itemInfo, C1ItemInfo parenItemInfo)
        {
            if (parenItemInfo == null || ((ItemType)parenItemInfo.ItemType).Equals(ItemType.C1SiteMap)) // Root node
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the right
        /// </summary>
        private bool EnableMoveRight(C1ItemInfo itemInfo, C1ItemInfo previousItemInfo)
        {
            if (previousItemInfo != null && previousItemInfo.EnableChildItems) // Root node
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  Indicates when an item was inserted in a specific position.
        /// </summary>
        /// <param name="item">The inserted item.</param>
        /// <param name="destinationItem">The destination item that will contain inserted item.</param>
        /// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
        protected override void Insert(object item, object destinationItem, int destinationIndex)
        {
            ((IC1SiteMapNodeCollectionOwner)destinationItem).Nodes.Insert(destinationIndex, (C1SiteMapNode)item);
            base.SyncUI();
        }

        /// <summary>
        ///  Indicates when an item was deleted.
        /// </summary>
        /// <param name="item">The deleted item.</param>
        protected override void Delete(object item)
        {
            ((IC1SiteMapNodeCollectionOwner)item).Owner.Nodes.Remove((C1SiteMapNode)item);
        }

        protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
        {
            bool cancel = e.CancelEdit;
            FinishLabelEdit(e.Node, e.Label, ref cancel);
            e.CancelEdit = cancel;
        }

        private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
        {
            string nodeText = "";
            string property = "Text";
            object obj = ((C1NodeInfo)node.Tag).Element;
            C1ItemInfo itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;

            if (text != null) //&& text.CompareTo(_emptyItemText) != 0
            {
                nodeText = text;
            }
            else
            {
                if (obj is C1SiteMap)
                {
                    nodeText = ((C1SiteMap)obj).ID == null ? "" : ((C1SiteMap)obj).ID.ToString();
                }
                else if (obj is C1SiteMapNode)
                {
                    if (!string.IsNullOrEmpty(((C1SiteMapNode)obj).Text))
                        nodeText = ((C1SiteMapNode)obj).Text;

                    if (string.IsNullOrEmpty(nodeText))
                    {
                        if (node.Parent == null)
                        {
                            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Nodes);
                        }
                        else
                        {
                            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Parent.Nodes);
                        }
                    }
                }
            }

            if (obj is C1SiteMap)
                property = "ID";
            else if (obj is C1SiteMapNode)
                property = "Text";

            // set new text value to selected object and property grid
            TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
            base.RefreshPropertyGrid();
            node.Text = nodeText;
        }


        /// <summary>
        /// Form loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C1SiteMapDesignerForm_Load(object sender, EventArgs e)
        {
        }


        protected override void Copy(object item, C1ItemInfo itemInfo)
        {
            C1SiteMapSerializer serializer = new C1SiteMapSerializer(item);
            if (_clipboardData == null)
                _clipboardData = new MemoryStream(); 
            
            _clipboardData.SetLength(0);
            serializer.SaveLayout(_clipboardData);
            base.ClipboardData = true;

            _clipboardType = itemInfo;
            base.AllowPaste = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.C1SiteMap, ItemType.C1SiteMapNode });
        }

        protected override void Paste(TreeNode destinationNode)
        {
            C1SiteMapNode treeViewNode = new C1SiteMapNode();
            C1SiteMapSerializer serializer = new C1SiteMapSerializer(treeViewNode);
            _clipboardData.Seek(0, SeekOrigin.Begin);
            // lets to serializer to retrieve data from clipboard
            serializer.LoadLayout(_clipboardData, LayoutType.All);

            // retrieve the C1MenuItem from destination node
            object parentObject = ((C1NodeInfo)destinationNode.Tag).Element;
            // add deserialized item
            ((IC1SiteMapNodeCollectionOwner)parentObject).Nodes.Add((C1SiteMapNode)treeViewNode);

            // create node info
            C1NodeInfo nodeInfo = new C1NodeInfo(treeViewNode, _clipboardType, treeViewNode.Text);

            // create new treenode to be added on destination node
            TreeNode treeNode = new TreeNode();
            // set node text and node tag
            SetNodeAttributes(treeNode, nodeInfo.NodeText, nodeInfo);
            // load child nodes
            IterateChildNodes(treeNode, treeViewNode);

            destinationNode.Nodes.Add(treeNode);
            destinationNode.ExpandAll();
        }

        protected override void SaveToXML(string fileName)
        {
            try
            {
                _siteMap.SaveLayout(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void LoadFromXML(string fileName)
        {
            try
            {
                _siteMap.LoadLayout(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }   
    }
}
