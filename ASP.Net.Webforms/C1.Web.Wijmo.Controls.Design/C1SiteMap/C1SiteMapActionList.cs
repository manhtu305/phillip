﻿using C1.Web.Wijmo.Controls.C1SiteMap;
using C1.Web.Wijmo.Controls.Design.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMap
{
    public class C1SiteMapActionList : DesignerActionListBase
    {
        private C1SiteMapDesigner _designer;

        public C1SiteMapActionList(C1SiteMapDesigner designer)
            : base(designer)
        {
            this._designer = designer;
        }

        public void EditNodes()
        {
            this._designer.EditNodes();
        }

        public void EditBindings()
        {
            this._designer.EditBindings();
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection col = new DesignerActionItemCollection();

            col.Add(new DesignerActionMethodItem(this,
                "EditNodes",
                C1Localizer.GetString("C1SiteMap.SmartTag.EditNodes"), "",
                C1Localizer.GetString("C1SiteMap.SmartTag.EditSiteMapDescription"),
                true));

            col.Add(new DesignerActionMethodItem(this,
                "EditBindings",
                C1Localizer.GetString("C1SiteMap.SmartTag.EditSiteMapDatabindings"), "",
                C1Localizer.GetString("C1Sitemap.SmartTag.EditSiteMapDatabindingsDescription"),
                true));

            //col.Add(new DesignerActionPropertyItem(
            //    "LevelSettings",
            //    C1Localizer.GetString("C1SiteMap.SmartTag.LevelSettings"), "",
            //    C1Localizer.GetString("C1Sitemap.SmartTag.LevelSettingsDescription")
            //    ));

            AddBaseSortedActionItems(col);

            return col;
        }
    }
}
