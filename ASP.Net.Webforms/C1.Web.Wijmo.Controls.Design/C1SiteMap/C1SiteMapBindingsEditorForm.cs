﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Collections;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMap
{
	using C1.Web.Wijmo.Controls.C1SiteMap;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using C1.Web.Wijmo.Controls.Design.Utils;

    public partial class C1SiteMapBindingsEditorForm : Form
    {
        private class BindingView
        {
            public BindingView(C1SiteMapNodeBinding binding)
            {
                _binding = binding;
            }

            public string Text
            {
                get
                {
                    if (_binding.DataMember != "")
                        return _binding.DataMember;
                    else
                        return C1Localizer.GetString("Base.BindingsEditorForm.Empty");
                }
            }

            public C1SiteMapNodeBinding Binding
            {
                get
                {
                    return _binding;
                }
            }

            public override string ToString()
            {
                return this.Text;
            }

            private readonly C1SiteMapNodeBinding _binding;

        }

        public C1SiteMapBindingsEditorForm()
        {
            InitializeComponent();
            _schemas = new Hashtable();
            //C1.Util.Localization.C1Localizer.LocalizeForm(this, components);
        }

        #region Internal
        internal void SetComponent(object component)
        {
            _control = (C1SiteMap)component;
            LoadExistingBindings();
            LoadDataSchema();
            this._propGrid.Site = this._control.Site;
        }
        #endregion

        #region Fields
        private C1SiteMap _control;
        private Hashtable _schemas;
        #endregion

        private void LoadDataSchema()
        {
            IDesignerHost dhost = (IDesignerHost)this._control.Site.GetService(typeof(IDesignerHost));
            if (dhost != null)
            {
                HierarchicalDataBoundControlDesigner designer = dhost.GetDesigner(this._control) as HierarchicalDataBoundControlDesigner;
                if (designer != null)
                {
                    DesignerHierarchicalDataSourceView view = designer.DesignerView;
                    if (view != null)
                    {
                        IDataSourceSchema schema = null;
                        try
                        {
                            schema = view.Schema;
                        }
                        catch
                        {
                            IComponentDesignerDebugService service = (IComponentDesignerDebugService)_control.Site.GetService(typeof(IComponentDesignerDebugService));
                            if (service != null)
                            {
                                service.Fail(C1Localizer.GetString("Base.BindingsEditorForm.FailToGetSchema"));
                            }
                        }
                        this.PopulateSchema(schema);
                    }
                }
            }
        }


        private void PopulateSchema(IDataSourceSchema schema)
        {
            if (schema != null)
            {
                IDataSourceViewSchema[] schemas = schema.GetViews();
                if (schemas != null)
                {
                    for (int i = 0; i < schemas.Length; i++)
                    {
                        this.PopulateSchemaRecursive(schemas[i]);
                    }
                }
            }
        }

        private void PopulateSchemaRecursive(IDataSourceViewSchema viewSchema)
        {
            if (viewSchema != null)
            {
                if (_cbxAvailableBindings.Items.IndexOf(viewSchema.Name) == -1)
                {
                    _cbxAvailableBindings.Items.Add(viewSchema.Name);
                    _schemas.Add(viewSchema.Name, viewSchema);
                }
                IDataSourceViewSchema[] schemas = viewSchema.GetChildren();
                if (schemas != null)
                {
                    for (int i = 0; i < schemas.Length; i++)
                    {
                        this.PopulateSchemaRecursive(schemas[i]);
                    }
                }
            }
        }

        private void LoadForm()
        {
            LoadImages();
            LoadAvailableBindings();
            UpdateStatus();
        }

        private void LoadAvailableBindings()
        {
        }

        private void LoadExistingBindings()
        {
            _listBindings.Items.Clear();
            foreach (C1SiteMapNodeBinding binding in _control.DataBindings)
            {
                object d = new BindingView((C1SiteMapNodeBinding)((ICloneable)binding).Clone());
                _listBindings.Items.Add(d);
            }
        }

        private void AddAvailableBinding(string name, int level)
        {
            _cbxAvailableBindings.Items.Add(FormAvailableBindingName(name, level));
        }

        private string FormAvailableBindingName(string name, int level)
        {
            string prefix = "";
            for (int i = 0; i < level; i++)
            {
                prefix += "  ";
            }
            return prefix + name;
        }

        private void LoadImages()
        {
            //Willow Yang add comments at March 12,2009 to fix bug 2070
			_imageList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowUpIco);
			_imageList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowDownIco);
			_imageList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.DeleteIco);
            //LoadImageInList(_imageList, "C1.Web.UI.Controls.C1Menu.Resources.C1MenuEditor.Images.ArrowUp.png");
            //LoadImageInList(_imageList, "C1.Web.UI.Controls.C1Menu.Resources.C1MenuEditor.Images.ArrowDown.png");
            //LoadImageInList(_imageList, "C1.Web.UI.Controls.C1Menu.Resources.C1MenuEditor.Images.Delete.png");
            //end comments
            _btnMoveUp.ImageIndex = 0;
            _btnMoveDown.ImageIndex = 1;
            _btnDelete.ImageIndex = 2;
        }

        private void LoadImageInList(ImageList imageList, string resourceName)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            if (stream != null)
                imageList.Images.Add(System.Drawing.Image.FromStream(stream));
        }

        private void Save()
        {
            _control.DataBindings.Clear();
            foreach (object o in _listBindings.Items)
                ((System.Collections.IList)_control.DataBindings).Add(((BindingView)o).Binding);
        }

        private void AddNewBinding()
        {
            C1SiteMapNodeBinding binding = new C1SiteMapNodeBinding(_cbxAvailableBindings.Text);
            _listBindings.Items.Add(new BindingView(binding));
            _listBindings.SelectedIndex = _listBindings.Items.Count-1;
            UpdateStatus();
        }

        private void SelectBinding()
        {
            SetSchemaForSelectedBinding(this.SelectedBinding);
            this._propGrid.SelectedObject = this.SelectedBinding;
            UpdateStatus();
        }

        private void SetSchemaForSelectedBinding(C1SiteMapNodeBinding binding)
        {
            if (binding == null)
                return;
             object schema = _schemas[binding.DataMember];
             ((IDataSourceViewSchemaAccessor)binding).DataSourceViewSchema = schema;
        }

        private void MoveUpSelectedBinding()
        {
            object o = _listBindings.SelectedItem;
            int idx = _listBindings.Items.IndexOf(o);
            _listBindings.Items.RemoveAt(idx);
            _listBindings.Items.Insert(idx - 1, o);
            _listBindings.SelectedIndex = idx-1;
            UpdateStatus();
        }

        private void MoveDownSelectedBinding()
        {
            object o = _listBindings.SelectedItem;
            int idx = _listBindings.Items.IndexOf(o);
            _listBindings.Items.RemoveAt(idx);
            _listBindings.Items.Insert(idx + 1, o);
            _listBindings.SelectedIndex = idx + 1;
            UpdateStatus();
        }

        private void DeleteSelectedBinding()
        {
            object o = _listBindings.SelectedItem;
            int idx = _listBindings.Items.IndexOf(o);
            _listBindings.Items.RemoveAt(idx);
            if (idx < _listBindings.Items.Count)
                _listBindings.SelectedIndex = idx;
            else if (_listBindings.Items.Count > 0)
                _listBindings.SelectedIndex = _listBindings.Items.Count - 1;
            UpdateStatus();
        }

        private void ShowHelp()
        {
            //C1HelpCaller.ShowHelpContents();
        }

        private void UpdateStatus()
        {
            bool selected = _listBindings.SelectedItem != null;
            bool sel_first = selected && _listBindings.SelectedIndex == 0;
            bool sel_last = selected && _listBindings.SelectedIndex == _listBindings.Items.Count - 1;

            this._btnMoveUp.Enabled = selected && !sel_first;
            this._btnMoveDown.Enabled = selected && !sel_last;
            this._btnDelete.Enabled = selected;
        }

        private C1SiteMapNodeBinding SelectedBinding
        {
            get
            {
                if ((BindingView)_listBindings.SelectedItem == null)
                    return null;
                return ((BindingView)_listBindings.SelectedItem).Binding;
            }
        }

        private void PropGridResetProperty()
        {
            _propGrid.ResetSelectedProperty();
            _propGrid.Refresh();
            RefreshCurrentBindingInList();
        }

        private void PropGridSwitchDescription()
        {
            bool v = !_propGrid.HelpVisible;
            _propGrid.HelpVisible = v;
            _cmiDescription.Checked = v;
        }

        private void UpdateResetItem()
        {
            _cmiReset.Enabled = (_propGrid.SelectedObject != null &&
                _propGrid.SelectedGridItem != null &&
                _propGrid.SelectedGridItem.PropertyDescriptor != null &&
                _propGrid.SelectedGridItem.PropertyDescriptor.CanResetValue(_propGrid.SelectedObject));
        }

        private void RefreshCurrentBindingInList()
        {
            object o = _listBindings.SelectedItem;
            int idx = _listBindings.Items.IndexOf(o);
            _listBindings.Items[idx] = o;
        }

        private void C1MenuBindingsEditorForm_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void _btnAdd_Click(object sender, EventArgs e)
        {
            AddNewBinding();
        }

        private void _listBindings_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectBinding();
        }

        private void _btnMoveUp_Click(object sender, EventArgs e)
        {
            MoveUpSelectedBinding();

        }

        private void _btnMoveDown_Click(object sender, EventArgs e)
        {
            MoveDownSelectedBinding();
        }

        private void _btnDelete_Click(object sender, EventArgs e)
        {
            DeleteSelectedBinding();
        }

        private void _btnHelp_Click(object sender, EventArgs e)
        {
            ShowHelp();
        }

        private void _btnOK_Click(object sender, EventArgs e)
        {
            Save();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void _propGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            RefreshCurrentBindingInList();
        }

        private void _cmiReset_Click(object sender, EventArgs e)
        {
            PropGridResetProperty();
        }

        private void _cmiDescription_Click(object sender, EventArgs e)
        {
            PropGridSwitchDescription();
        }

        private void _listBindings_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                DeleteSelectedBinding();
        }

        private void _ctxPropGrid_Opening(object sender, CancelEventArgs e)
        {
            UpdateResetItem();
        }
    }
}
