﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.IO;
using System.Windows.Forms.Design;
using System.Globalization;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Drawing.Design;
using System.Diagnostics;


namespace C1.Web.Wijmo.Controls.Design.C1SiteMap
{
    using C1.Web.Wijmo.Controls.C1SiteMap;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using C1.Web.Wijmo.Controls.Design.Utils;

    [SupportsPreviewControl(true)]
    public class C1SiteMapDesigner : HierarchicalDataBoundControlDesigner
    {
        #region Fields

        C1.Web.Wijmo.Controls.C1SiteMap.C1SiteMap _siteMap;
        
        private TemplateGroupCollection _templateGroups;
        private static readonly string[] _templateNames;

        #endregion

        static C1SiteMapDesigner()
        {
            _templateNames = new string[] { "NodeTemplate" };
        }

        /// <summary>
        /// Allow resize.
        /// </summary>
        public override bool AllowResize
        {
            get
            {
                return false;
            }
        }

        public override void Initialize(IComponent control)
        {
            base.Initialize(control);

            _siteMap = control as C1SiteMap;
            Debug.Assert(_siteMap != null);

            base.SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        public override string GetDesignTimeHtml()
        {
            string html =  base.GetDesignTimeHtml();

            return html;
        }

        protected override string GetEmptyDesignTimeHtml()
        {
            return CreatePlaceHolderDesignTimeHtml(C1Localizer.GetString("C1SiteMap.EmptyDesignTimeHtml"));
        }

         //<summary>
         //TemplateGroups implementation.
         //</summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                TemplateGroupCollection templateGroups = base.TemplateGroups;

                if (this._templateGroups == null)
                {
                    this._templateGroups = new TemplateGroupCollection();
                    
                    TemplateGroup group = new TemplateGroup("NodeTemplate", ((WebControl)base.ViewControl).ControlStyle);
                    TemplateDefinition templateDefinition = new TemplateDefinition(this, _templateNames[0], this._siteMap.DefaultLevelSetting, _templateNames[0], false);
                   
                    templateDefinition.SupportsDataBinding = true;
                    group.AddTemplateDefinition(templateDefinition);
                    
                    this._templateGroups.Add(group);
                }

                templateGroups.AddRange(this._templateGroups);

                return templateGroups;
            }
        }

        protected override bool UsePreviewControl
        {
            get
            {
                return true;
            }
        }

        public bool EditNodes()
        {
            bool accept = false;

            C1SiteMapDesignerForm _editorForm = new C1SiteMapDesignerForm(_siteMap);
            accept = ShowControlEditorForm(this._siteMap, _editorForm);

            return accept;
        }

        public bool EditBindings()
        {
            C1SiteMapBindingsEditorForm _editorForm = new C1SiteMapBindingsEditorForm();
            _editorForm.SetComponent(_siteMap);
            return ShowControlEditorForm(_siteMap, _editorForm);
        }

        public bool ShowControlEditorForm(object control, Form _editorForm)
        {
            DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;

            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1SiteMap.EditControl"));
                
                using (trans)
                {
                    IUIService service = (IUIService)serviceProvider.GetService(typeof(IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(_editorForm);
                    }
                    else
                        dr = DialogResult.None;
                    if (dr == DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = _editorForm.ShowDialog();

            _editorForm.Dispose();
            _editorForm = null;

            return dr == DialogResult.OK;
        }


        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();

                lists.AddRange(base.ActionLists);
                lists.Add(new C1SiteMapActionList(this));

                return lists;
            }
        }
    }

}
