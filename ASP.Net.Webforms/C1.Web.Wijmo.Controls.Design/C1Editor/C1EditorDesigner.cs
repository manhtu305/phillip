using System;
using System.Web.UI.Design;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.Design.C1Editor
{

	using C1.Web.Wijmo.Controls.C1Editor;

	/// <summary>
	/// Provides a C1EditorDesigner class for extending the design-mode behavior of a C1Editor control.
	/// </summary>
    public class C1EditorDesigner : C1ControlDesinger
	{

		private C1Editor _control;
		
		/// <summary>
		/// Gets the action list collection for the control designer.
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1EditorDesignerActionList(this));

				return actionLists;
			}
		}

		public override void Initialize(System.ComponentModel.IComponent component)
		{
			base.Initialize(component);
			_control = (C1Editor)component;
		}

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			string html = base.GetDesignTimeHtml();

			return base.GetDesignTimeHtml();
		}
		/// <summary>
		/// Provides a custom class for types that define a list of items used to create a smart tag panel.
		/// </summary>
		private class C1EditorDesignerActionList : DesignerActionListBase
		{

			#region ** fields
			private DesignerActionItemCollection items;
			#endregion end of ** fields.

			#region ** constructor
			/// <summary>
			/// CustomControlActionList constructor.
			/// </summary>
			/// <param name="parent">The Specified C1SplitterDesigner designer.</param>
			public C1EditorDesignerActionList(C1EditorDesigner parent)
				: base(parent)
			{
			}
			#endregion end of ** constructor.

			/// <summary>
			/// Override GetSortedActionItems method.
			/// </summary>
			/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (items == null)
				{
					items = new DesignerActionItemCollection();

					AddBaseSortedActionItems(items);
				}
				
				return items;
			}
		}
	}
}
