﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1FileExplorer
{
	using Controls.C1FileExplorer;
	using Localization;

	[SupportsPreviewControl(true)]
	internal class C1FileExplorerDesigner : C1ControlDesinger
	{
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				var actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1FileExplorerDesignerActionList(this));
				return actionLists;
			}
		}

		public override string GetDesignTimeHtml()
		{
			var c1FileExplorer = Component as C1FileExplorer;
			Debug.Assert(c1FileExplorer != null, "c1FileExplorer != null");
			if (c1FileExplorer.WijmoControlMode == WijmoControlMode.Mobile)
			{
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>",
					"Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}
	}

	internal class C1FileExplorerDesignerActionList : DesignerActionListBase
	{
		private readonly C1FileExplorer _component;

		public C1FileExplorerDesignerActionList(ControlDesigner designer)
			: base(designer)
		{
			_component = designer.Component as C1FileExplorer;
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			var items = new DesignerActionItemCollection
			{
				new DesignerActionPropertyItem("Mode", C1Localizer.GetString("C1FileExplorer.SmartTag.Mode"), "Appearance",
					C1Localizer.GetString("C1FileExplorer.SmartTag.ModeDescription")),
				new DesignerActionPropertyItem("ViewMode", C1Localizer.GetString("C1FileExplorer.SmartTag.ViewMode"),
					"Appearance", C1Localizer.GetString("C1FileExplorer.SmartTag.ViewModeDescription")),
				new DesignerActionPropertyItem("Toolbar", C1Localizer.GetString("C1FileExplorer.SmartTag.Toolbar"),
					"Controls", C1Localizer.GetString("C1FileExplorer.SmartTag.ToolbarDescription")),
				new DesignerActionPropertyItem("AddressBox", C1Localizer.GetString("C1FileExplorer.SmartTag.AddressBox"),
					"Controls", C1Localizer.GetString("C1FileExplorer.SmartTag.AddressBoxDescription")),
				new DesignerActionPropertyItem("FilterTextBox",
					C1Localizer.GetString("C1FileExplorer.SmartTag.FilterTextBox"), "Controls",
					C1Localizer.GetString("C1FileExplorer.SmartTag.FilterTextBoxDescription")),
				new DesignerActionPropertyItem("TreeView", C1Localizer.GetString("C1FileExplorer.SmartTag.TreeView"),
					"Controls", C1Localizer.GetString("C1FileExplorer.SmartTag.TreeViewDescription")),
				new DesignerActionPropertyItem("Grid", C1Localizer.GetString("C1FileExplorer.SmartTag.Grid"), "Controls",
					C1Localizer.GetString("C1FileExplorer.SmartTag.GridDescription")),
				new DesignerActionPropertyItem("ListView", C1Localizer.GetString("C1FileExplorer.SmartTag.ListView"),
					"Controls", C1Localizer.GetString("C1FileExplorer.SmartTag.ListViewDescription")),
				new DesignerActionPropertyItem("ContextMenu", C1Localizer.GetString("C1FileExplorer.SmartTag.ContextMenu"),
					"Controls", C1Localizer.GetString("C1FileExplorer.SmartTag.ContextMenuDescription"))
			};

			AddBaseSortedActionItems(items);

			return items;
		}

		#region Properties

		public bool ContextMenu
		{
			get { return HasControl(FileExplorerControls.ContextMenu); }
			set { EnsureControl(FileExplorerControls.ContextMenu, value); }
		}

		public bool Toolbar
		{
			get { return HasControl(FileExplorerControls.Toolbar); }
			set { EnsureControl(FileExplorerControls.Toolbar, value); }
		}

		public bool AddressBox
		{
			get { return HasControl(FileExplorerControls.AddressBox); }
			set { EnsureControl(FileExplorerControls.AddressBox, value); }
		}

		public bool FilterTextBox
		{
			get { return HasControl(FileExplorerControls.FilterTextBox); }
			set { EnsureControl(FileExplorerControls.FilterTextBox, value); }
		}

		public bool TreeView
		{
			get { return HasControl(FileExplorerControls.TreeView); }
			set { EnsureControl(FileExplorerControls.TreeView, value); }
		}

		public bool Grid
		{
			get { return HasControl(FileExplorerControls.Grid); }
			set { EnsureControl(FileExplorerControls.Grid, value); }
		}

		public bool ListView
		{
			get { return HasControl(FileExplorerControls.ListView); }
			set { EnsureControl(FileExplorerControls.ListView, value); }
		}

		public ViewMode ViewMode
		{
			get { return _component.ViewMode; }
			set { TypeDescriptor.GetProperties(_component)["ViewMode"].SetValue(_component, value); }
		}

		public ExplorerMode Mode
		{
			get { return _component.Mode; }
			set { TypeDescriptor.GetProperties(_component)["Mode"].SetValue(_component, value); }
		}

		public string Address
		{
			get { return _component.CurrentFolder; }
			set { TypeDescriptor.GetProperties(_component)["CurrentFolder"].SetValue(_component, value); }
		}

		#endregion

		#region Utils

		private bool HasControl(FileExplorerControls control)
		{
			return (_component.VisibleControls & control) == control;
		}

		private void EnsureControl(FileExplorerControls control, bool on)
		{
			var result = _component.VisibleControls;
			if (on)
			{
				result = result | control;
			}
			else
			{
				result = (result | control) ^ control;
			}
			TypeDescriptor.GetProperties(_component)["VisibleControls"].SetValue(_component, result);
		}

		#endregion
	}
}
