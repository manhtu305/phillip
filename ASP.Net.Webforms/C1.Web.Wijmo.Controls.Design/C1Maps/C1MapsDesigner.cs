﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.Design.C1Maps
{
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Drawing;
	using C1.Web.Wijmo.Controls.C1Maps;
	using C1.Web.Wijmo.Controls.Design.Localization;

	public class C1MapsDesigner : C1ControlDesinger
	{
		#region ** fields
		C1Maps _control;
		#endregion

		#region ** override methods
		public override void Initialize(System.ComponentModel.IComponent component)
		{
			base.Initialize(component);
			_control = component as C1Maps;
		}

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1MapsDesignerActionList(this));
				return actionLists;
			}
		}
		#endregion
	}

	internal class C1MapsDesignerActionList : DesignerActionListBase
	{
		#region ** fields
		private DesignerActionItemCollection items;
		#endregion end of ** fields.

		#region ** constructor
		/// <summary>
		/// CustomControlActionList constructor.
		/// </summary>
		/// <param name="parent">The Specified C1MapsDesigner designer.</param>
		public C1MapsDesignerActionList(C1MapsDesigner parent)
			: base(parent)
		{
		}
		#endregion end of ** constructor.

		#region ** properties
		/// <summary>
		/// Gets or sets the Center property of C1Maps
		/// </summary>
		public PointD Center
		{
			get 
			{
				return (PointD)GetProperty(this.Component, "Center").GetValue(this.Component);
			}
			set 
			{
				SetProperty("Center", value);
			}
		}

		/// <summary>
		/// Gets or sets the Zoom property of C1Maps
		/// </summary>
		public double Zoom
		{
			get
			{
				return (double)GetProperty(this.Component, "Zoom").GetValue(this.Component);
			}
			set
			{
				SetProperty("Zoom", value);
			}
		}

		/// <summary>
		/// Gets or sets the ShowTools property of C1Maps
		/// </summary>
		public bool ShowTools 
		{
			get 
			{
				return (bool)GetProperty(this.Component, "ShowTools").GetValue(this.Component);
			}
			set
			{
				SetProperty("ShowTools", value);
			}
		}

		/// <summary>
		/// Gets or sets the Source property of C1Maps.
		/// </summary>
		public MapSource Source 
		{
			get 
			{
				return (MapSource)GetProperty(this.Component, "Source").GetValue(this.Component);
			}
			set
			{
				SetProperty("Source", value);
			}
		}

		internal override bool SupportWijmoControlMode
		{
			get
			{
				return false;
			}
		}
		#endregion

		/// <summary>
		/// Override GetSortedActionItems method.
		/// </summary>
		/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (items == null)
			{
				items = new DesignerActionItemCollection();
				items.Add(new DesignerActionPropertyItem("Center", C1Localizer.GetString("C1Maps.SmartTag.Center"),"Center", C1Localizer.GetString("C1Maps.SmartTag.CenterDesc")));
				items.Add(new DesignerActionPropertyItem("Zoom", C1Localizer.GetString("C1Maps.SmartTag.Zoom"), "Zoom", C1Localizer.GetString("C1Maps.SmartTag.ZoomDesc")));
				items.Add(new DesignerActionPropertyItem("ShowTools", C1Localizer.GetString("C1Maps.SmartTag.ShowTools"), "ShowTools", C1Localizer.GetString("C1Maps.SmartTag.ShowToolsDesc")));
				items.Add(new DesignerActionPropertyItem("Source", C1Localizer.GetString("C1Maps.SmartTag.Source"), "Source", C1Localizer.GetString("C1Maps.SmartTag.SourceDesc")));
				AddBaseSortedActionItems(items);
			}
			return items;
		}
	}
}
