﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using C1.C1Preview;
using C1.Web.Wijmo.Controls.C1Maps;
using C1.Web.Wijmo.Controls.C1Maps.GeoJson;
using GeoPosition = C1.Web.Wijmo.Controls.C1Maps.GeoJson.Position;

namespace C1.Web.Wijmo.Controls.Design.C1Maps
{
	public class C1LayersEditor : CollectionEditor
	{
		private readonly Type[] _types;

		public C1LayersEditor(Type type) : base(type)
		{
			_types = new[]
			{
				typeof (C1VectorLayer),
				typeof (C1ItemsLayer),
				typeof (C1VirtualLayer),
				typeof (C1CustomLayer)
			};
		}

		protected override Type[] CreateNewItemTypes()
		{
			return _types;
		}

		protected override string GetDisplayText(object value)
		{
			var layer = value as C1Layer;
			if(layer == null)
				return base.GetDisplayText(value);

			return layer.Type.ToString() + " Layer";
		}
	}

	public class C1VectorItemsEditor : CollectionEditor
	{
		private readonly Type[] _types;

		public C1VectorItemsEditor(Type type)
			: base(type)
		{
			_types = new[]
			{
				typeof (C1VectorPlacemark),
				typeof (C1VectorPolyline),
				typeof (C1VectorPolygon)
			};
		}
		
		protected override Type[] CreateNewItemTypes()
		{
			return _types;
		}

		protected override string GetDisplayText(object value)
		{
			var item = value as C1VectorItemBase;
			if(item == null)
				return base.GetDisplayText(value);

			return item.Type.ToString();
		}
	}

	public class GeoJsonFeaturesEditor : CollectionEditor
	{
		private readonly Type[] _types;

		public GeoJsonFeaturesEditor(Type type)
			: base(type)
		{
			_types = new[]
			{
				typeof (PointFeature),
				typeof (MultiPointFeature),
				typeof (LineStringFeature),
				typeof (MultiLineStringFeature),
				typeof (PolygonFeature),
				typeof(MultiPolygonFeature),
				typeof(GeometryCollectionFeature)
			};
		}

		protected override Type[] CreateNewItemTypes()
		{
			return _types;
		}

		protected override string GetDisplayText(object value)
		{
			var feature = value as Feature;
			if(feature == null)
				return base.GetDisplayText(value);

			var text = feature.GetType().Name;
			if (!string.IsNullOrEmpty(feature.FeatureId))
				text += "(" + feature.FeatureId + ")";
			return text;
		}
	}

	public class GeoJsonGeometriesEditor : CollectionEditor
	{
		private readonly Type[] _types;

		public GeoJsonGeometriesEditor(Type type)
			: base(type)
		{
			_types = new[]
			{
				typeof (Point),
				typeof (MultiPoint),
				typeof (LineString),
				typeof (MultiLineString),
				typeof (Polygon),
				typeof (MultiPolygon),
				typeof (GeometryCollection)
			};
		}

		protected override Type[] CreateNewItemTypes()
		{
			return _types;
		}

		protected override string GetDisplayText(object value)
		{
			var geometry = value as GeometryObject;
			if(geometry==null)
				return base.GetDisplayText(value);

			return geometry.Type;
		}
	}
}
