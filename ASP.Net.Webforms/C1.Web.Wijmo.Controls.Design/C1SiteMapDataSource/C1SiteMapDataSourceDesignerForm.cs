﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using C1.Web.Wijmo.Controls.C1SiteMap;
using C1.Web.Wijmo.Controls.Design.Localization;
using System.Web;
using System.Xml;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource
{
    public partial class C1SiteMapDataSourceDesignerForm : C1BaseItemEditorForm
    {
        #region Fields

        private MicSiteMapNode _rootNode;
        
        // clipboard for current control
        private Stream _clipBoardStream;
        private C1ItemInfo _clipboardType;

        C1ItemInfo _itemInfo = new C1ItemInfo();

        #endregion

        internal C1SiteMapDataSourceDesignerForm(MicSiteMapNode rootNode)
            : base(rootNode)
        {
            _rootNode = rootNode;
            InitializeComponent();

            //remove preview
            this._mainTabControl.TabPages.RemoveAt(1);
        }

        protected override List<C1ItemInfo> FillAvailableControlItems()
        {
            List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();

            _itemInfo.ItemType = typeof(MicSiteMapNode);
            _itemInfo.EnableChildItems = true;
            _itemInfo.ContextMenuStripText = "C1SiteMapNode";
            _itemInfo.NodeImage = Properties.Resources.RootItemIco;
            _itemInfo.DefaultNodeText = "C1SiteMapNode";
            _itemInfo.Default = true;
            _itemInfo.Visible = true;
            itemsInfo.Add(_itemInfo);

            return itemsInfo;
        }

        protected override void MoveDrag(TreeNode sourceNode, TreeNode destinationNode)
        {
            if (!IsChildNode(sourceNode, destinationNode))
            {
                base.MoveDrag(sourceNode, destinationNode);
            }
        }

        private bool IsChildNode(TreeNode sourceNode, TreeNode destinationNode)
        {
            TreeNode pnode = destinationNode.Parent;
            if (pnode != null) 
            {
                if (pnode == sourceNode)
                {
                    return true;
                }
                else
                {
                    return IsChildNode(sourceNode, pnode);
                }
            }
            return false;
        }

        /// <summary>
        /// Loads <see cref="TreeNode"/> nodes into the main TreeView
        /// </summary>
        /// <param name="mainTreeView"></param>
        protected override void LoadControl(TreeView mainTreeView)
        {
            string nodeText;
            object nodeTag;

            mainTreeView.BeginUpdate();
            mainTreeView.Nodes.Clear();

            TreeNode treeView = new TreeNode();

            nodeText = _itemInfo.DefaultNodeText;
            nodeTag = new C1NodeInfo(_rootNode, _itemInfo, nodeText);

            SetNodeAttributes(treeView, nodeText, nodeTag);

            foreach (MicSiteMapNode node in _rootNode.Nodes)
            {
                TreeNode treeViewNode = new TreeNode();
                nodeText = node.Title;
                nodeTag = new C1NodeInfo(node, _itemInfo, nodeText);
                SetNodeAttributes(treeViewNode, nodeText, nodeTag);

                IterateChildNodes(treeViewNode, node);

                treeView.Nodes.Add(treeViewNode);
            }

            mainTreeView.Nodes.Add(treeView);
            mainTreeView.SelectedNode = mainTreeView.Nodes[0];
            mainTreeView.ExpandAll();
            mainTreeView.EndUpdate();
        }

        /// <summary>
        /// Loads nested items from current item
        /// </summary>
        /// <param name="menuItemNode">TreeNode parent</param>
        /// <param name="item">Control item</param>
        private void IterateChildNodes(TreeNode treeViewNode, MicSiteMapNode parentNode)
        {
            string nodeText;
            object nodeTag;

            treeViewNode.Nodes.Clear();
            foreach (MicSiteMapNode node in parentNode.Nodes)
            {
                TreeNode treeNode = new TreeNode();
                nodeText = node.Title;
                nodeTag = new C1NodeInfo(node, _itemInfo, nodeText);
                SetNodeAttributes(treeNode, nodeText, nodeTag);

                IterateChildNodes(treeNode, node);

                treeViewNode.Nodes.Add(treeNode);
            }
        }

        /// <summary>
        /// Performs some action when a property value changes.
        /// </summary>
        /// <param name="changedGridItem">The grid item changed.</param>
        /// <param name="selectedNode">Current selected node.</param>
        protected override void OnPropertyGridPropertyValueChanged(GridItem changedGridItem, TreeNode selectedNode)
        {
            object obj = ((C1NodeInfo)selectedNode.Tag).Element; 
                if (obj is MicSiteMapNode)
            {
                MicSiteMapNode node = obj as MicSiteMapNode;

                if (changedGridItem.PropertyDescriptor.Name == "Title")
                {
                    string text = changedGridItem.Value.ToString();
                    selectedNode.Text = string.IsNullOrEmpty(text) ? _itemInfo.DefaultNodeText : text;
                }
            }
        }

        protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
        {
            //do not allowed to do insert for the root node.
            AllowInsert = parentItemInfo != null;

            AllowAdd = AllowCopy = AllowCut = AllowDelete = AllowRename = AllowMoveUp = AllowMoveDown = true;

            base.AllowMoveLeft = EnableMoveLeft(itemInfo, parentItemInfo);
            base.AllowMoveRight = EnableMoveRight(itemInfo, previousItemInfo);

            base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
        }

        protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
        {
            C1NodeInfo nodeInfo;
            string nodeText;

            MicSiteMapNode item = new MicSiteMapNode();

            // itemInfo.DefaultNodeText could be used as default item text. if not, you can get a new numbered name.
            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);
            item.Title = nodeText;
            nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);
            nodeInfo.NodeText = nodeText;

            return nodeInfo;
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the left
        /// </summary>
        private bool EnableMoveLeft(C1ItemInfo itemInfo, C1ItemInfo parenItemInfo)
        {
            if (parenItemInfo == null) // Root node
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Enables current node to be added as a child of its available node to the right
        /// </summary>
        private bool EnableMoveRight(C1ItemInfo itemInfo, C1ItemInfo previousItemInfo)
        {
            if (previousItemInfo != null && previousItemInfo.EnableChildItems) // Root node
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  Indicates when an item was inserted in a specific position.
        /// </summary>
        /// <param name="item">The inserted item.</param>
        /// <param name="destinationItem">The destination item that will contain inserted item.</param>
        /// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
        protected override void Insert(object item, object destinationItem, int destinationIndex)
        {
            ((MicSiteMapNode)destinationItem).Nodes.Insert(destinationIndex, (MicSiteMapNode)item);

            base.SyncUI();
        }

        /// <summary>
        ///  Indicates when an item was deleted.
        /// </summary>
        /// <param name="item">The deleted item.</param>
        protected override void Delete(object item)
        {
            if (item is MicSiteMapNode)
            {
                DeleteNode(item as MicSiteMapNode, _rootNode);
            }
        }

        private void DeleteNode(MicSiteMapNode nodeDel, MicSiteMapNode parentNode)
        {
            if (parentNode == null)
                return;

            foreach (var node in parentNode.Nodes)
            {
                if (nodeDel == node)
                {
                    parentNode.Nodes.Remove(node);
                    return;
                }
            }
        }

        protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
        {
            bool cancel = e.CancelEdit;
            FinishLabelEdit(e.Node, e.Label, ref cancel);
            e.CancelEdit = cancel;
        }

        private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
        {
            string nodeText = "";
            string property = "Title";
            object obj = ((C1NodeInfo)node.Tag).Element;
            C1ItemInfo itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;

            if (text != null) //&& text.CompareTo(_emptyItemText) != 0
            {
                nodeText = text;
            }
            else
            {
                if (obj is MicSiteMapNode)
                {
                    if (!string.IsNullOrEmpty(((MicSiteMapNode)obj).Title))
                        nodeText = ((MicSiteMapNode)obj).Title;

                    if (string.IsNullOrEmpty(nodeText))
                    {
                        if (node.Parent == null)
                        {
                            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Nodes);
                        }
                        else
                        {
                            nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Parent.Nodes);
                        }
                    }
                }
            }

             if (obj is MicSiteMapNode)
                property = "Title";

            // set new text value to selected object and property grid
            TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
            base.RefreshPropertyGrid();
            node.Text = nodeText;
        }

        protected override void Copy(object item, C1ItemInfo itemInfo)
        {
            MicSiteMapNode node = item as MicSiteMapNode;

            if (_clipBoardStream == null)
                _clipBoardStream = new MemoryStream();

            _clipBoardStream.SetLength(0);
            StreamWriter sw = new StreamWriter(_clipBoardStream);
            sw.Write(node.SerializeToXml());
            sw.Flush();

            base.ClipboardData = true;

            _clipboardType = itemInfo;

            base.AllowPaste = true;
        }

        protected override void Paste(TreeNode destinationNode)
        {
            MicSiteMapNode siteMapNode = new MicSiteMapNode();

            _clipBoardStream.Seek(0, SeekOrigin.Begin);

            StreamReader reader = new StreamReader(_clipBoardStream);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(reader.ReadToEnd());

            siteMapNode.DeserializeFromXml(xmldoc);

            // retrieve the C1MenuItem from destination node
            object parentObject = ((C1NodeInfo)destinationNode.Tag).Element;

            // add deserialized item
            ((MicSiteMapNode)parentObject).Nodes.Add((MicSiteMapNode)siteMapNode);

            // create node info
            C1NodeInfo nodeInfo = new C1NodeInfo(siteMapNode, _clipboardType, siteMapNode.Title);

            // create new treenode to be added on destination node
            TreeNode treeNode = new TreeNode();
            // set node text and node tag
            SetNodeAttributes(treeNode, nodeInfo.NodeText, nodeInfo);
            // load child nodes
            IterateChildNodes(treeNode, siteMapNode);

            destinationNode.Nodes.Add(treeNode);
            destinationNode.ExpandAll();
        }

        protected override void SaveToXML(string fileName)
        {
            try
            {
                _rootNode.SaveToSiteMapFile(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void LoadFromXML(string fileName)
        {
            try
            {
                _rootNode.LoadFromSiteMapFile(fileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }   
    }
}
