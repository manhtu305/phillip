﻿using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls.Design.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource
{

    public class MicSiteMapNode : System.Web.UI.Control
    {
        List<MicSiteMapNode> _nodes = new List<MicSiteMapNode>();

        #region Properties

        [Browsable(false)]
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        [Browsable(false)]
        public new string ClientIDMode
        {
            get;
            set;
        }
        
        [Browsable(false)]
        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
            }
        }

        [Browsable(false)]
        public override bool EnableViewState
        {
            get
            {
                return base.EnableViewState;
            }
            set
            {
                base.EnableViewState = value;
            }
        }

        [Browsable(false)]
        public new string ViewStateMode
        {
            get;
            set;
        }

        [Browsable(false)]
        public new string ValidateRequestMode
        {
            get;
            set;
        }

        [C1Description("C1SiteMapDataSource.Title")]
        public string Title { get; set; }

        [C1Description("C1SiteMapDataSource.Url")]
        public string Url { get; set; }

        [C1Description("C1SiteMapDataSource.Description")]
        public string Description { get; set; }

        internal List<MicSiteMapNode> Nodes
        {
            get { return _nodes; }
        }

        #endregion

        #region Serialize/Deserialize

        /// <summary>
        /// Save nodes to given sitemap file.
        /// </summary>
        /// <param name="suggestedSiteMapFile"></param>
        internal void SaveToSiteMapFile(string siteMapFile)
        {
            FileStream fs = new FileStream(siteMapFile, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);

            sw.Write(this.SerializeToXml());
            sw.Flush();
            sw.Close();

            fs.Close();
        }

        /// <summary>
        /// Load nodes from given sitemap file.
        /// </summary>
        /// <param name="siteMapFile"></param>
        internal void LoadFromSiteMapFile(string siteMapFile)
        {
            StreamReader sr = new StreamReader(siteMapFile);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sr.ReadToEnd());

            this.DeserializeFromXml(doc);
        }

        internal string SerializeToXml()
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "utf-8", null));

            XmlNode rootNode = doc.CreateElement("siteMap");
            XmlAttribute attr = doc.CreateAttribute("xmlns");
            attr.Value = "http://schemas.microsoft.com/AspNet/SiteMap-File-1.0";

            rootNode.Attributes.Append(attr);
            doc.AppendChild(rootNode);

            SerializeNode(this, rootNode, doc);

            return doc.InnerXml;
        }

        private void SerializeNode(MicSiteMapNode siteMapNode, XmlNode parentNode, XmlDocument doc)
        {
            XmlNode node = doc.CreateElement("siteMapNode");
            
            XmlAttribute url = doc.CreateAttribute("url");
            url.Value = siteMapNode.Url;
            node.Attributes.Append(url);

            XmlAttribute title = doc.CreateAttribute("title");
            title.Value = siteMapNode.Title;
            node.Attributes.Append(title);

            XmlAttribute description = doc.CreateAttribute("description");
            description.Value = siteMapNode.Description;
            node.Attributes.Append(description);

            parentNode.AppendChild(node);

            foreach (var childSiteMapNode in siteMapNode.Nodes)
            {
                SerializeNode(childSiteMapNode, node, doc);
            }
        }

        internal void DeserializeFromXml(XmlDocument doc)
        {
            this.Nodes.Clear();

            XmlNode rootNode = doc.DocumentElement.FirstChild;

            this.Url = rootNode.Attributes["url"] == null ? string.Empty : rootNode.Attributes["url"].Value;
            this.Title = rootNode.Attributes["title"] == null ? string.Empty : rootNode.Attributes["title"].Value;
            this.Description = rootNode.Attributes["description"] == null ? string.Empty : rootNode.Attributes["description"].Value;

            foreach (XmlNode childNode in rootNode.ChildNodes)
            {
                DeserializeNode(this, childNode);
            }
        }

        private void DeserializeNode(MicSiteMapNode parentSiteMapNode, XmlNode node)
        {
            MicSiteMapNode siteMapNode = new MicSiteMapNode();
            siteMapNode.Url = node.Attributes["url"] == null ? string.Empty : node.Attributes["url"].Value;
            siteMapNode.Title = node.Attributes["title"] == null ? string.Empty : node.Attributes["title"].Value;
            siteMapNode.Description = node.Attributes["description"] == null ? string.Empty : node.Attributes["description"].Value;

            parentSiteMapNode.Nodes.Add(siteMapNode);

            foreach (XmlNode childNode in node.ChildNodes)
            {
                DeserializeNode(siteMapNode, childNode);
            }
        }

        #endregion
    }
}
