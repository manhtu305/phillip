﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using TheSiteMapDataSource = C1.Web.Wijmo.Controls.C1SiteMapDataSource.C1SiteMapDataSource;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource
{
    /// <summary>
    /// Design-time code
    /// </summary>
    /// <exclude />
    /// <excludetoc />
    public class C1SiteMapDataSourceDesigner : SiteMapDataSourceDesigner
    {
        private C1DesignTimeSiteMapProvider _designTimeSiteMapProvider;

        public override string GetDesignTimeHtml()
        {
            return base.CreatePlaceHolderDesignTimeHtml();
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection actionLists = new DesignerActionListCollection();

                actionLists.Add(new C1SiteMapDataSourceActionList(this));

                return actionLists;
            }
        }

        public TheSiteMapDataSource SiteMapDataSource
        {
            get
            {
                return (TheSiteMapDataSource)Component;
            }
        }

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// Instantiates a <seealso cref="C1DesignTimeSiteMapProvider"/> providing it with the SiteMap file,
        /// specified in the compnent's (<seealso cref="C1SiteMapDataSource"/>) SiteMapFile property,
        /// and assigns it to it.
        /// </summary>
        /// <param name="component">The control being designed.</param>
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            IDesignerHost service = (IDesignerHost)this.GetService(typeof(IDesignerHost));
            _designTimeSiteMapProvider = new C1DesignTimeSiteMapProvider(service) { SiteMapFile = SiteMapDataSource.SiteMapFile };

            SiteMapDataSource.Provider = _designTimeSiteMapProvider;
        }

        /// <summary>
        /// Called when the associated control changes.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">A <see cref="ComponentChangedEventArgs"/> that contains the event data. </param>
        public override void OnComponentChanged(object sender, ComponentChangedEventArgs e)
        {
            if (string.Compare(e.Member.Name, "SiteMapFile") == 0)
            {
                _designTimeSiteMapProvider.Clear();
                _designTimeSiteMapProvider.SiteMapFile = (string)e.NewValue;
            }

            base.OnComponentChanged(sender, e);
        }

        /// <summary>
        /// Will be called after user changed current site map file from DataSource designer.
        /// </summary>
        public void RaiseDataSourceChangedEvent()
        {
            _designTimeSiteMapProvider.Clear();

            OnDataSourceChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Get design time data source view.
        /// </summary>
        /// <param name="viewPath"></param>
        /// <returns></returns>
        public override DesignerHierarchicalDataSourceView GetView(string viewPath)
        {
            return new C1SiteMapDesignerDataSourceView(this, viewPath);
        }
    }
}
