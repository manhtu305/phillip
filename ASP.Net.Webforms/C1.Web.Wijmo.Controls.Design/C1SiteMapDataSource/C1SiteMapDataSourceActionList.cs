﻿using C1.Web.Wijmo.Controls.Design.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource
{
    class C1SiteMapDataSourceActionList : DesignerActionListBase
    {
        private C1SiteMapDataSourceDesigner _designer;

        public C1SiteMapDataSourceActionList(C1SiteMapDataSourceDesigner designer)
            : base(designer)
        {
            this._designer = designer;
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection col = new DesignerActionItemCollection();

            col.Add(new DesignerActionMethodItem(this,
                "CreateNewSiteMap", 
                //"Create new sitemap file",
                C1Localizer.GetString("C1SiteMapDataSource.SmartTag.CreateNewSiteMap"), "",
                C1Localizer.GetString("C1SiteMapDataSource.SmartTag.CreateNewSiteMapDescription"),
                true));

            if (!string.IsNullOrEmpty(_designer.SiteMapDataSource.SiteMapFile))
            {
            //    col.Add(new DesignerActionTextItem("EditSiteMap", ""));
            //}
            //else
            //{
                col.Add(new DesignerActionMethodItem(this,
                    "EditSiteMap", 
                    //"Edit Current SiteMap",
                    C1Localizer.GetString("C1SiteMapDataSource.SmartTag.EditSiteMap"), "",
                    C1Localizer.GetString("C1SiteMapDataSource.SmartTag.EditSiteMapDescription"),
                    true));
            }

            AddBaseSortedActionItems(col);

            return col;
        }

        public void CreateNewSiteMap()
        {
            var webApplication = (IWebApplication)Component.Site.GetService(typeof(IWebApplication));
            MicSiteMapNode rootNode = new MicSiteMapNode();
            rootNode.Title = "SiteMapNode";

            C1SiteMapDataSourceDesignerForm siteMapEditorForm = new C1SiteMapDataSourceDesignerForm(rootNode);

            if (siteMapEditorForm.ShowDialog() == DialogResult.OK)
            {
                string webAppRoot = webApplication.RootProjectItem.PhysicalPath;
                string siteMapFileTemplate = string.Format("{0}web{{0}}.sitemap", webAppRoot);
                string suggestedSiteMapFile = string.Format(siteMapFileTemplate, string.Empty);
                int suggestedSiteMapFileIndex = 1;

                while (File.Exists(suggestedSiteMapFile))
                    suggestedSiteMapFile = string.Format(siteMapFileTemplate, suggestedSiteMapFileIndex++);

                rootNode.SaveToSiteMapFile(suggestedSiteMapFile);
            }
        }

        public void EditSiteMap()
        {
            var webApplication = (IWebApplication)Component.Site.GetService(typeof(IWebApplication));
            var siteMapFile = webApplication.GetProjectItemFromUrl(_designer.SiteMapDataSource.SiteMapFile).PhysicalPath;

            MicSiteMapNode rootNode = new MicSiteMapNode();
            rootNode.LoadFromSiteMapFile(siteMapFile);

            C1SiteMapDataSourceDesignerForm siteMapEditorForm = new C1SiteMapDataSourceDesignerForm(rootNode);

            if (siteMapEditorForm.ShowDialog() == DialogResult.OK)
            {
                rootNode.SaveToSiteMapFile(siteMapFile);

                _designer.RaiseDataSourceChangedEvent();
            }
        }
    }
}
