﻿using C1.Web.Wijmo.Controls.Design.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource
{
    /// <summary>
    /// The Url choose editor.
    /// </summary>
    public class C1SiteMapFileUrlEditor : UrlEditor
    {
        protected override string Caption
        {
            get
            {
                return C1Localizer.GetString("C1SiteMapFileUrlEditor.Caption");
            }
        }

        protected override string Filter
        {
            get
            {
                return "ASP.NET Sitemap files|*.sitemap";
            }
        }
    }
}
