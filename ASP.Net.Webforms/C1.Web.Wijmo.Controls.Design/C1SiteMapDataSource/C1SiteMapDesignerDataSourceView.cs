using System.Web.UI;
using System.Web.UI.Design.WebControls;

namespace C1.Web.Wijmo.Controls.Design.C1SiteMapDataSource
{
    public class C1SiteMapDesignerDataSourceView : SiteMapDesignerHierarchicalDataSourceView
    {
        private C1SiteMapDataSourceDesigner _designer;

        public C1SiteMapDesignerDataSourceView(C1SiteMapDataSourceDesigner owner, string viewPath)
            : base(owner, viewPath)
        {
            _designer = owner;
        }

        /// <summary>
        /// Get the design time data view.
        /// </summary>
        /// <param name="isSampleData"></param>
        /// <returns></returns>
        public override IHierarchicalEnumerable GetDesignTimeData(out bool isSampleData)
        {
            IHierarchicalEnumerable enumerable = null;

            //back up
            string siteMapProvider = _designer.SiteMapDataSource.SiteMapProvider;
            string startingNodeUrl = _designer.SiteMapDataSource.StartingNodeUrl;

            isSampleData = true;

            try
            {
                _designer.SiteMapDataSource.StartingNodeUrl = null;

                //try get view from current selected file.
                enumerable = ((IHierarchicalDataSource)_designer.SiteMapDataSource).GetHierarchicalView(base.Path).Select();
                
                isSampleData = false;
            }
            finally
            {
                _designer.SiteMapDataSource.StartingNodeUrl = startingNodeUrl;
                _designer.SiteMapDataSource.SiteMapProvider = siteMapProvider;
            }

            return enumerable;
        }
    }
}