﻿using System;
using System.Collections.Generic;
using System.Web.UI.Design;
using System.Drawing;
using System.Web.UI;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1LineChart
{
	using C1.Web.Wijmo.Controls.C1Chart;
	using C1.Web.Wijmo.Controls.Design.C1ChartCore;
	using System.ComponentModel.Design;

	/// <summary>
	/// Provides a C1LineChartDesigner class for extending the design-mode behavior of a C1LineChart control.
	/// </summary>
	public class C1LineChartDesigner : C1ChartCoreDesigner
	{
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
		}

		protected override void AddSampleData()
		{
			C1LineChart lineChart = Component as C1LineChart;
			LineChartSeries series = new LineChartSeries();
			series.Label = "SampleData";
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 20, 22, 19, 24, 25 });
			lineChart.SeriesList.Add(series);
		}
	}
}