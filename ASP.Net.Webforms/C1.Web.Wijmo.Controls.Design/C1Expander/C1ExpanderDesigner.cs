﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.ComponentModel.Design;

namespace C1.Web.Wijmo.Controls.Design.C1Expander
{
	using C1.Web.Wijmo.Controls.C1Expander;
	using System.Web.UI.WebControls;
	using System.ComponentModel;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using System.Globalization;
	using System.Diagnostics;
	using C1.Web.Wijmo.Controls.Design.Utils;
	using System.Web.UI;

	// fix for [16025] 
	[SupportsPreviewControl(false)]
    public class C1ExpanderDesigner : C1ControlDesinger
	{
		#region ** fields
		//private DesignerAutoFormatCollection _autoFormats;
		private DesignerActionListCollection _actions;
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ExpanderDesigner"/> class.
		/// </summary>
		public C1ExpanderDesigner()
		{
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Helper property to get the C1HeaderContentControl we're designing.
		/// </summary>
		private C1Expander HeaderedContentControl
		{
			get
			{
				return (C1Expander)Component;
			}
		}

		/// <summary>
		/// Tell the designer we're creating our own UI.
		/// </summary>
		protected override bool UsePreviewControl
		{
			get
			{
				// fix for [16025] 
				//	(designer) Cursor move outside of C1Expender after one or 
				//	two character is added into the C1Expander header area:
				return false;
			}
		}
		/// <summary>
		/// Allow resize.
		/// </summary>
		public override bool AllowResize
		{
			get
			{
				return true;
			}
		}

		// initialize ActionLists
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actions == null)
				{
					_actions = new DesignerActionListCollection();
					_actions.AddRange(base.ActionLists);
					_actions.Add(new ExpanderActionList(this));
				}
				return _actions;
			}
		}

		/*
		/// <summary>
		///  Gets the collection of predefined automatic formatting schemes to display
		///  in the Auto Format dialog box for the associated control at design time.
		/// </summary>
		public override DesignerAutoFormatCollection AutoFormats
		{
			get
			{
				if (this._autoFormats == null)
				{
					this._autoFormats = new DesignerAutoFormatCollection();
					C1DesignerAutoFormat.FillVisualStyleAutoFormatCollection(this._autoFormats, (Control)this.Component);
				}
				if (this._autoFormats.Count < 1)
					return base.AutoFormats;
				return this._autoFormats;
			}
		}*/

		#endregion

		// initialize design time verbs
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			//SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		/// <summary>
		/// GetDesignTimeHtml.
		/// </summary>
		/// <param name="regions"></param>
		/// <returns></returns>
		public override string GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			if (this.HeaderedContentControl.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			if (regions == null)
			{
				throw new ArgumentNullException("regions");
			}
			C1Expander newViewControl = this.ViewControl as C1Expander;
			if (newViewControl == null)
				newViewControl = this.HeaderedContentControl;
			if (newViewControl.ContentPanel == null || newViewControl.HeaderPanel == null)
			{
				return "<span>Child controls not created...</span>";
			}
			// Content panel:            
			newViewControl.ContentPanel.ContentElementAttributes.Add(DesignerRegion.DesignerRegionAttributeName, "0");
			// Header panel:
			newViewControl.HeaderPanel.ContentElementAttributes.Add(DesignerRegion.DesignerRegionAttributeName, "1");

			// Create the editable region for Content:
			EditableDesignerRegion region = new EditableDesignerRegion(this, String.Format(CultureInfo.InvariantCulture, "c{0}", newViewControl.ID));
			regions.Add(region);
			// Create the editable region for Header:
			region = new EditableDesignerRegion(this, String.Format(CultureInfo.InvariantCulture, "h{0}", newViewControl.ID));
			regions.Add(region);

			// Build final design time HTML:
			//                               
			string sResultBodyContent = "";
			StringBuilder sb2 = new StringBuilder();
			System.IO.StringWriter tw = new System.IO.StringWriter(sb2);
			System.Web.UI.HtmlTextWriter writer = new System.Web.UI.HtmlTextWriter(tw);

			// so the control show's up when visible is set to false at design time
			bool vis = newViewControl.Visible;
			//bool dispVis = newViewControl.DisplayVisible;
			newViewControl.Visible = true;
			//newViewControl.DisplayVisible = newViewControl.Visible = true;
			
			newViewControl.RenderControl(writer);
			newViewControl.Visible = vis;
			//newViewControl.DisplayVisible = dispVis;
			sResultBodyContent = sb2.ToString();

			// Remove temporary attributes from original control after design time rendering done
			// Please note, this is important for VS2008, otherwise we will have some 
			// unexpected behavior:
			newViewControl.HeaderPanel.ContentElementAttributes.Remove(DesignerRegion.DesignerRegionAttributeName);
			newViewControl.ContentPanel.ContentElementAttributes.Remove(DesignerRegion.DesignerRegionAttributeName);

			return sResultBodyContent;


		}

		/// <summary>
		/// The Designer will call us back on this for each EditableDesignerRegion that we created.
		/// In this we return the markup that we want displayed in the editable region.
		/// </summary>
		/// <param name="region"></param>
		/// <returns></returns>
		public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
		{
			if (region == null)
				throw new ArgumentNullException("region");
			string regionName = region.Name;
			Debug.Assert(regionName[0] == 'c' || regionName[0] == 'h', "Expected regionName to start with c or h, not " + regionName);
			bool isContent = regionName[0] == 'c';
			return GetPanelContent(isContent);
		}

		/// <summary>
		/// After a editable region is edited, the designer calls back with the updated markup so we can
		/// stuff it into the right panel.
		/// </summary>
		/// <param name="region"></param>
		/// <param name="content"></param>
		public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
		{
			if (region == null)
				throw new ArgumentNullException("region");
			string regionName = region.Name;
			Debug.Assert(regionName[0] == 'c' || regionName[0] == 'h', "Expected regionName to start with c or h, not " + regionName);
			bool setPanelContent = regionName[0] == 'c';
			C1Expander activePanel = HeaderedContentControl;
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));
			Debug.Assert(host != null, "Failed to get IDesignerHost?!?");
			PersistTemplateContent(HeaderedContentControl, host, content, (setPanelContent ? "Content" : "Header"));
		}

		private static void PersistTemplateContent(C1Expander panel, IDesignerHost host, string content, string propertyName)
		{
			ITemplate template = ControlParser.ParseTemplate(host, content);
			PersistTemplate(panel, host, template, propertyName);
		}

		/// <summary>
		/// Helper method to save the value of a template.  This sets up all the right Undo state.
		/// </summary>
		/// <param name="panel"></param>
		/// <param name="host"></param>
		/// <param name="template"></param>
		/// <param name="propertyName"></param>
		private static void PersistTemplate(C1Expander panel, IDesignerHost host, ITemplate template, string propertyName)
		{
			PropertyDescriptor descriptor = TypeDescriptor.GetProperties(panel)[propertyName];
			using (DesignerTransaction transaction = host.CreateTransaction(C1Localizer.GetString("C1HeadContentControl.SetEditableDesignerRegionContent")))
			{
				descriptor.SetValue(panel, template);
				transaction.Commit();
			}
		}

		/// <summary>
		/// Called when we get a click on a designer region.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnClick(DesignerRegionMouseEventArgs e)
		{
			base.OnClick(e);
		}

		/// <summary>
		/// Get the content for a given panel or header
		/// </summary>
		/// <param name="isContent">True for ContentTemplate, otherwise it'll do HeaderTemplate</param>
		/// <returns></returns>
		private string GetPanelContent(bool isContent)
		{
			C1Expander panel = HeaderedContentControl;
			if (panel != null)
			{
				if (isContent && panel.Content != null)
				{
					return GetTemplateContent(panel.Content, "_content");
				}
				else if (panel.Header != null)
				{
					return GetTemplateContent(panel.Header, "_header");
				}
			}
			return "";
		}

		/// <summary>
		/// Helper method to instantiate the given template into a control
		/// an slurp out the markup.
		/// </summary>
		/// <param name="template"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		private string GetTemplateContent(ITemplate template, string id)
		{
			DesignerPanel contentPanel = new DesignerPanel();
			contentPanel.ID = id;

			template.InstantiateIn(contentPanel);
			IDesignerHost host = (IDesignerHost)GetService(typeof(IDesignerHost));

			Debug.Assert(host != null, "Failed to get IDesignerHost?!?");


			StringBuilder persistedControl = new StringBuilder(1024);
			foreach (System.Web.UI.Control c in contentPanel.Controls)
			{
				persistedControl.Append(ControlPersister.PersistControl(c, host));
			}
			return persistedControl.ToString();
		}


		protected override string GetErrorDesignTimeHtml(Exception e)
		{
#if DEBUG
			return CreatePlaceHolderDesignTimeHtml("Debug error: " + e.Message + "," + e.StackTrace);
#else
			return CreatePlaceHolderDesignTimeHtml(C1Localizer.GetString("Base.Exception.ErrorDesignTimeHtml"));
#endif
		}

		/// <summary>
		/// Simple class to use for template instantiation
		/// </summary>
		internal class DesignerPanel : System.Web.UI.WebControls.Panel, INamingContainer
		{
		}
	}


	internal class ExpanderActionList : DesignerActionListBase
	{
		// ** fields
		internal C1ExpanderDesigner _owner;

		private C1Expander _panel;

		private DesignerActionItemCollection _actions;
		
		public ExpanderActionList(C1ExpanderDesigner owner)
			: base(owner)
		{
			_owner = owner;

			_panel = base.Component as C1Expander;
		}

		public Unit Width
		{
			get { return _panel.Width; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_panel)["Width"];
				prop.SetValue(_panel, value);
			}
		}

		public Unit Height
		{
			get { return _panel.Height; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_panel)["Height"];
				prop.SetValue(_panel, value);
			}
		}

		public bool Expanded
		{
			get { return _panel.Expanded; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_panel)["Expanded"];
				prop.SetValue(_panel, value);
			}
		}

		public ExpandDirection ExpandDirection
		{
			get
			{
				return _panel.ExpandDirection;
			}
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_panel)["ExpandDirection"];
				prop.SetValue(_panel, value);
			}
		}
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (_actions == null)
			{
				_actions = new DesignerActionItemCollection();
				//_actions.Add(new DesignerActionPropertyItem("Width", C1Localizer.GetString("Expander.SmartTag.Width"), "Layout"));
				//_actions.Add(new DesignerActionPropertyItem("Height", C1Localizer.GetString("Expander.SmartTag.Height"), "Layout"));
				_actions.Add(new DesignerActionPropertyItem("Expanded", C1Localizer.GetString("C1Expander.SmartTag.Expanded", "Expanded"), "Options"));
				_actions.Add(new DesignerActionPropertyItem("ExpandDirection", C1Localizer.GetString("C1Expander.SmartTag.ExpandDirection", "Expand direction"), "Options"));
				
				AddBaseSortedActionItems(_actions);
			}
			return _actions;
		}

	}
}
