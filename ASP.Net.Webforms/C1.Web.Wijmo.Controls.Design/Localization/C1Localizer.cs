﻿using System.Collections;
using System.Globalization;
using System.Reflection;
using C1.Web.Wijmo.Controls.Localization;


namespace C1.Web.Wijmo.Controls.Design.Localization
{
	internal class C1Localizer
	{
		private static C1ResourceLoader _loader;
		private static readonly Hashtable _cache = new Hashtable();

		internal static string GetString(string key)
		{
			return GetString(key, null, null);
		}

		internal static string GetString(string key, string defaultValue)
		{
			return GetString(key, defaultValue, null);
		}

		internal static string GetString(string key, CultureInfo c)
		{
			return GetString(key, null, c);
		}

		internal static string GetString(string key, string defaultValue, CultureInfo c)
		{
			string s = c == null ? Loader.GetString(key) : Loader.GetString(key, c);
			return s == null ? (defaultValue == null ? key : defaultValue) : s;
		}

		private static C1ResourceLoader Loader
		{
			get
			{
				if (_loader == null)
				{
					_loader = new C1ResourceLoader("C1.Web.Wijmo.Controls.Design.Localization.Resources.WijmoExtendersDesign", Assembly.GetExecutingAssembly());
				}
				return _loader;
			}
		}
	}
}
