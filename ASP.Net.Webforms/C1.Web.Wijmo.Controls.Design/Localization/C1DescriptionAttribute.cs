﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.Localization
{

    /// <summary>
    /// C1DescriptionAttribute replaces the DescriptionAttribute
    /// and uses the C1Localizer class to return the localized Attribute string
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class C1DescriptionAttribute : DescriptionAttribute
    {
        private readonly string _key;
        private readonly string _default;
        /// <summary>
        /// Initialize C1DescriptionAttribute.
        /// </summary>
        /// <param name="key">Key</param>
        public C1DescriptionAttribute(string key)
        {
            _key = key;
            DescriptionValue = key;
        }

        /// <summary>
        /// Initialize C1DescriptionAttribute.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="defaultvalue">Default value</param>
        public C1DescriptionAttribute(string key, string defaultvalue)
        {
            _key = key;
            DescriptionValue = key;
            _default = defaultvalue;
        }

        public string Key
        {
            get
            {
                return _key;
            }
        }

        /// <summary>
        /// Description of C1DescriptionAttribute.
        /// </summary>
        override public string Description
        {
            get
            {
                // fixed problem with default value
                string s = C1Localizer.GetString(_key, _default);
                return s ?? (_default ?? base.Description);
            }
        }
    }
}
