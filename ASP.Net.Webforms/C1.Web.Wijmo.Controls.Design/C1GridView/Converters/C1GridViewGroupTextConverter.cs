﻿namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	/// <summary>
	/// Converter for <see cref="C1.Web.UI.Controls.C1GridView.GroupInfo.HeaderText"/> and
	/// <see cref="C1.Web.UI.Controls.C1GridView.GroupInfo.FooterText"/> properties
	/// of the <see cref="C1.Web.UI.Controls.C1GridView.GroupInfo"/> object.
	/// </summary>
	internal class C1GridViewGroupTextConverter : C1.Web.Wijmo.Controls.Converters.StringListConverter
	{
		public C1GridViewGroupTextConverter()
		{
			SetList(false, new string[] { "{1}: {0}", "Summary for <b>{0}</b>", "Custom" });
		}
	}
}