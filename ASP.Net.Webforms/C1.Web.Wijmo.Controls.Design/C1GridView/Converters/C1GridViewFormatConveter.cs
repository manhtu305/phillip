namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	/// <summary>
	/// Converter for format property of columns.
	/// </summary>
	internal class C1GridViewFormatConverter : C1.Web.Wijmo.Controls.Converters.StringListConverter
	{
		public C1GridViewFormatConverter()
		{
			SetList(false, new string[]
			{
				"{0:c}", "{0:#}", "{0:0}", "{0:0.00}", "{0:#,##0.00}", "{0:0%}", "{0:0.00%}"
			});
		}
	}

	internal class C1GridViewJqueryGlobFormatConverter : C1.Web.Wijmo.Controls.Converters.StringListConverter
	{
		public C1GridViewJqueryGlobFormatConverter()
		{
			SetList(false, new string[]
			{
				"n", "d", "p", "c"
			});
		}
	}
}