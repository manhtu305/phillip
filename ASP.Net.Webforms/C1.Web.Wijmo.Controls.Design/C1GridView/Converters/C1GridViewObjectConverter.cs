﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	internal class C1GridViewObjectConverter : ExpandableObjectConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			return (destinationType == typeof(string))
				? string.Empty
				: base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
