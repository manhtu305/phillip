namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Data;
	using System.Globalization;
	using System.Reflection;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.Web.UI.Design.WebControls;
	using System.Web.UI.WebControls;
	using System.Windows.Forms;
	using WFD = System.Windows.Forms.Design;
	using System.Xml;

	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// 
	/// </summary>
	[SupportsPreviewControl(true)]
	public class C1GridViewDesigner : C1DataBoundControlDesigner, WFD.IWindowsFormsEditorService, IServiceProvider
	{
		private struct TemplateInfo
		{
			public string DisplayName;
			public string PropertyName;
			public bool Bindable;
			public int StyleIndex;

			public TemplateInfo(string displayName, string propertyName, bool bindable, int styleIndex)
			{
				DisplayName = displayName;
				PropertyName = propertyName;
				Bindable = bindable;
				StyleIndex = styleIndex;
			}
		}

		private static TemplateInfo[] _templatesColumnInfo = new TemplateInfo[]
			{
				new TemplateInfo("AlternatingItem Template", "AlternatingItemTemplate", true, 0),
				new TemplateInfo("Header Template", "HeaderTemplate", false, 1),
				new TemplateInfo("Item Template", "ItemTemplate", true, 2),
				new TemplateInfo("EditItem Template", "EditItemTemplate", true, 3),
				new TemplateInfo("Footer Template", "FooterTemplate", false, 4)
			};

		private static TemplateInfo[] _templatesGridInfo = new TemplateInfo[]
			{
				new TemplateInfo("EmptyData Template", "EmptyDataTemplate", true, 20),
				new TemplateInfo("Pager Template", "PagerTemplate", true, 21)
			};

		private C1GridViewActionList _actions = null;
		private bool _ignoreSchemaRefreshedEvent = false;
		PropertyBuilderDialog _propBuilderDialog = null;
		private int _startPanel = 0;

		/// <summary>
		/// 
		/// </summary>
		public C1GridViewDesigner()
			: base()
		{
		}

		#region properties

		/// <summary>
		/// 
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection res = new DesignerActionListCollection();

				res.AddRange(base.ActionLists);

				if (_actions == null)
					_actions = new C1GridViewActionList(this);

				res.Add(_actions);

				return res;
			}
		}

		private C1GridView C1GridView
		{
			get
			{
				return (Component as C1GridView);
			}
		}

		internal IDataSourceViewSchema DataSourceSchema
		{
			get
			{
				if (DesignerView != null)
				{
					return DesignerView.Schema;
				}

				return null;
			}
		}

		internal IDataSourceFieldSchema[] DataSourceSchemaFields
		{
			get
			{
				IDataSourceViewSchema schema = DataSourceSchema;

				return (schema != null)
					? schema.GetFields()
					: new IDataSourceFieldSchema[] { };
			}
		}

		/// <summary>
		/// 
		/// </summary>
		protected override int SampleRowCount
		{
			get
			{
				// 20 = x / 3
				C1GridView grid = C1GridView;
				return (grid.AllowPaging) ? grid.PageSize : 5;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public override TemplateGroupCollection TemplateGroups
		{
			get
			{
				C1BaseFieldCollection columns = C1GridView.Columns;
				C1GridView grid = C1GridView;

				TemplateGroupCollection groups = base.TemplateGroups;

				// columns templates
				if (columns.Count > 0)
				{
					for (int i = 0; i < columns.Count; i++)
					{
						C1BaseField currentCol = columns[i];
						if (currentCol is C1TemplateField)
						{
							string caption = string.Format("Column[{0}]", i);
							if (!string.IsNullOrEmpty(currentCol.HeaderText))
							{
								caption += " - " + currentCol.HeaderText;
							}

							TemplateGroup group = new TemplateGroup(caption);

							for (int j = 0; j < _templatesColumnInfo.Length; j++)
							{
								TemplateInfo ti = _templatesColumnInfo[j];

								TemplateDefinition def = new TemplateDefinition(this,
									ti.DisplayName, currentCol, ti.PropertyName,
									GetTemplateStyle(ti.StyleIndex, grid, (C1TemplateField)currentCol));

								def.SupportsDataBinding = ti.Bindable;
								group.AddTemplateDefinition(def);
							}

							groups.Add(group);
						}
					}

					// grid.EmptyDataTemplate + grid.PagerTemplate
					for (int i = 0; i < _templatesGridInfo.Length; i++)
					{
						TemplateInfo ti = _templatesGridInfo[i];
						TemplateGroup group = new TemplateGroup(ti.DisplayName);
						TemplateDefinition def = new TemplateDefinition(this, ti.DisplayName, Component,
							ti.PropertyName, GetTemplateStyle(ti.StyleIndex, grid, null));

						def.SupportsDataBinding = ti.Bindable;

						group.AddTemplateDefinition(def);

						groups.Add(group);
					}
				}

				return groups;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		protected override bool UsePreviewControl
		{
			get { return true; }
		}
		#endregion


		#region methods
		internal bool BuilderDialogSetDirty()
		{
			if (_propBuilderDialog != null)
			{
				_propBuilderDialog.SetDirty(null);
				return true;
			}
			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string GetDesignTimeHtml()
		{
			if (this.C1GridView.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			System.ComponentModel.TypeDescriptor.Refresh(Component);
			C1GridView grid = (C1GridView)ViewControl;
			string designHtml = string.Empty;

			try
			{
				designHtml = base.GetDesignTimeHtml();
				string style = string.Empty;
				//if (grid.ScrollSettings.ScrollMode != DOMScrollMode.None)  
				//if (grid.ScrollSettings.ScrollMode != C1GridViewScrollMode.None)
				if (grid._InternalScrollMode() != ScrollMode.None)
				{
					int endTag = designHtml.IndexOf('>');

					//remove style.left property
					designHtml = RemoveOrChangeStyleProp(designHtml, "left:", 0, ref endTag, string.Empty);

					//remove style.top property
					designHtml = RemoveOrChangeStyleProp(designHtml, "top:", 0, ref endTag, string.Empty);

					//remove style.position property
					designHtml = RemoveOrChangeStyleProp(designHtml, "position:", 0, ref endTag, string.Empty);

					if (IsVS2008())
					{
						string scroll = "overflow:scroll";

						//change style.height property to 101%
						designHtml = RemoveOrChangeStyleProp(designHtml, "height:", 0, ref endTag, "101%");

						//change style.height property to 101%
						designHtml = RemoveOrChangeStyleProp(designHtml, "width:", 0, ref endTag, "101%");

						string width = grid.Width.ToString();
						if (string.IsNullOrEmpty(width) && (grid.Style["width"] != null))
							width = (string)grid.Style["width"];

						string height = grid.Height.ToString();
						if (string.IsNullOrEmpty(height) && (grid.Style["height"] != null))
							height = (string)grid.Style["height"];

						style = string.Format("height:{0};width:{1};{2};left:{3};top:{4};",
							height, width, scroll, grid.Style["left"], grid.Style["top"]);
					}

					if (grid.Style["position"] != null)
						style += "position:" + grid.Style["position"];

					designHtml = string.Format("<div style=\"{0}\">{1}</div>", style, designHtml);
				}
			}
			catch (Exception e)
			{
				designHtml = GetErrorDesignTimeHtml(e);
			}

			return designHtml;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="component"></param>
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void OnSchemaRefreshed()
		{
			if (InTemplateMode || _ignoreSchemaRefreshedEvent)
				return;

			ControlDesigner.InvokeTransactedChange(Component, new TransactedChangeCallback(SchemaRefreshedCallback), null, "C1WG_SchemaRefreshedTransaction");
			UpdateDesignTimeHtml();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="properties"></param>
		protected override void PreFilterProperties(IDictionary properties)
		{
			base.PreFilterProperties(properties);
			if (InTemplateMode)
			{
				PropertyDescriptor pd = properties["Columns"] as PropertyDescriptor;
				Attribute[] atrs = new Attribute[1] { BrowsableAttribute.No };
				properties["Columns"] = TypeDescriptor.CreateProperty(pd.ComponentType, pd, atrs);
			}
		}

		public void PropertyBuilder(int startPanel)
		{
			_startPanel = startPanel;
			_ignoreSchemaRefreshedEvent = true;
			PropertyBuilderCallback(null);
			//ControlDesigner.InvokeTransactedChange(Component, new TransactedChangeCallback(PropertyBuilderCallback), null, "C1WG_EditColumnsTransaction");
			_ignoreSchemaRefreshedEvent = false;
		}

		internal void RaiseComponentChanges()
		{
			MemberDescriptor mdColumns = TypeDescriptor.GetProperties(Component)["Columns"];

			RaiseComponentChanging(mdColumns);
			RaiseComponentChanging(null);

			RaiseComponentChanged(mdColumns, null, ((C1GridView)Component).Columns);
			RaiseComponentChanged(null, null, null);
		}

		internal void LoadLayout()
		{
			C1GridView grid = C1GridView;
			if (grid != null)
			{
				OpenFileDialog fd = new OpenFileDialog();
				fd.Filter = "Layout XML files (*.xml)|*.xml|All files (*.*)|*.*";
				fd.RestoreDirectory = true;
				fd.ValidateNames = true;
				fd.CheckFileExists = true;

				if (fd.ShowDialog() == DialogResult.OK)
				{
					using (C1GridViewLayoutDialog layoutDialog = new C1GridViewLayoutDialog())
					{
						if (layoutDialog.ShowDialog() == DialogResult.OK)
						{
							grid.LoadLayout(fd.FileName, (LayoutType)layoutDialog.Tag);
							RaiseComponentChanges();
						}
					}
				}
			}
		}

		internal void SaveLayout()
		{
			C1GridView grid = C1GridView;
			if (grid != null)
			{
				SaveFileDialog fd = new SaveFileDialog();
				fd.Filter = "Layout XML files (*.xml)|*.xml|All files (*.*)|*.*";
				fd.RestoreDirectory = true;

				if (fd.ShowDialog() == DialogResult.OK)
				{
					grid.SaveLayout(fd.FileName);
				}
			}
		}

		private Style GetTemplateStyle(int index, C1GridView grid, C1TemplateField container)
		{
			Style style = new Style();

			switch (index)
			{
				case 0: // alternating template
					style.CopyFrom(container.ItemStyle);
					style.MergeWith(grid.AlternatingRowStyle);
					style.MergeWith(grid.RowStyle);
					break;

				case 1: // header template
					style.CopyFrom(container.HeaderStyle);
					style.MergeWith(grid.HeaderStyle);
					break;

				case 2: // item template
					style.CopyFrom(container.ItemStyle);
					style.MergeWith(grid.RowStyle);
					break;

				case 3: // edititem template
					style.CopyFrom(container.ItemStyle);
					style.MergeWith(grid.EditRowStyle);
					style.MergeWith(grid.RowStyle);
					break;

				case 4: // footer template
					style.CopyFrom(container.FooterStyle);
					style.MergeWith(grid.FooterStyle);
					break;

				case 20: // emptydatarow template
					style.CopyFrom(grid.EmptyDataRowStyle);
					break;

				case 21: // pager template
					//style.CopyFrom(grid.PagerStyle);  
					break;

				default:
					throw new Exception(string.Format("Unknown style index: {0}", index));
			}

			style.MergeWith(grid.ControlStyle);

			return style;
		}

		private bool IsVS2008()
		{
			bool extenderControlFeaturesPresent = false;

			if (null == Component)
				return false;

			ISite site = Component.Site;
			if (site == null)
				return false;

			IDesignerHost host = (IDesignerHost)site.GetService(typeof(IDesignerHost));
			if (host == null)
				return false;

			IComponent rootComponent = host.RootComponent;
			if (rootComponent == null)
				return false;

			ISite rootSite = rootComponent.Site;
			if (rootSite == null)
				return false;

			IDictionaryService dictionarySvc = (IDictionaryService)rootSite.GetService(typeof(IDictionaryService));
			if (dictionarySvc == null)
				return false;

			object val = dictionarySvc.GetValue("ExtenderControlFeaturesPresent");
			if (null != val && val is bool)
				extenderControlFeaturesPresent = (bool)val;

			return extenderControlFeaturesPresent;
		}

		private void RefreshSchema(IDataSourceViewSchema schema)
		{
			if (schema != null)
			{
				IDataSourceFieldSchema[] columns = schema.GetFields();
				if (columns != null || columns.Length > 0)
				{
					C1GridView.AutogenerateColumns = false;

					System.Collections.Generic.List<string> dataKeys = new System.Collections.Generic.List<string>();

					for (int i = 0; i < columns.Length; i++)
					{
						IDataSourceFieldSchema colSchema = columns[i];

						C1BoundField column = (colSchema.DataType == typeof(bool))
							? new C1CheckBoxField()
							: new C1BoundField();

						column.DataField = colSchema.Name;
						column.HeaderText = colSchema.Name;
						column.ReadOnly = colSchema.IsReadOnly || colSchema.PrimaryKey;
						column.SortExpression = colSchema.Name;
						C1GridView.Columns.Add(column);

						if (colSchema.PrimaryKey)
						{
							dataKeys.Add(colSchema.Name);
						}
					}

					if (dataKeys.Count > 0)
					{
						C1GridView.DataKeyNames = dataKeys.ToArray();
					}
				}
			}
		}

		private string RemoveOrChangeStyleProp(string text, string lookfor, int startIdx, ref int count, string replacement)
		{
			int idx = text.IndexOf(lookfor, startIdx, count, StringComparison.InvariantCultureIgnoreCase);
			if (idx >= 0)
			{
				int idx2 = text.IndexOfAny(new char[] { ';', '"' }, idx + lookfor.Length, count);
				if (idx2 >= 0)
				{
					text = text.Remove(idx, idx2 - idx);
					count -= idx2 - idx;

					if (!string.IsNullOrEmpty(replacement))
					{
						string rplc = lookfor + replacement;
						text = text.Insert(idx, rplc);
						count += rplc.Length;
					}
				}
			}

			return text;
		}

		#endregion


		#region actions callbacks
		private bool PropertyBuilderCallback(object context)
		{
			if (DataSourceDesigner != null)
				DataSourceDesigner.SuppressDataSourceEvents();

			_propBuilderDialog = new PropertyBuilderDialog();

			try
			{
				_propBuilderDialog.Initialize(this);

				_propBuilderDialog.Load += new EventHandler(ChangeMenuBarIndex);
				((WFD.IWindowsFormsEditorService)this).ShowDialog(_propBuilderDialog);
			}
			finally
			{
				_propBuilderDialog.Dispose();
				_propBuilderDialog = null;
			}

			if (DataSourceDesigner != null)
				DataSourceDesigner.ResumeDataSourceEvents();

			UpdateDesignTimeHtml();

			return true;
		}

		private void ChangeMenuBarIndex(object sender, EventArgs e)
		{
			(sender as PropertyBuilderDialog)._menuBar.SelectedIndex = _startPanel;
		}

		private bool SchemaRefreshedCallback(object context)
		{
			IDataSourceViewSchema schema = DataSourceSchema;
			C1GridView grid = C1GridView;

			if (DataSourceID.Length > 0 && schema != null)
			{
				if (grid.Columns.Count > 0)
				{
					if (MessageBox.Show(string.Format("{0}: {1}", grid.ID,
						C1Localizer.GetString("C1GridView.Warning.RecreateColumns")), C1Localizer.GetString("C1GridView.Warning.DialogTitle"),
						MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						grid.SelectedIndex = -1;
						grid.DataKeyNames = null;
						grid.Columns.Clear();

						RefreshSchema(schema);
					}
				}
				else
					RefreshSchema(schema);
			}
			else
				if (grid.Columns.Count > 0)
					if (MessageBox.Show(string.Format("{0}: {1}", grid.ID,
						C1Localizer.GetString("C1GridView.Warning.RemoveColumns")), C1Localizer.GetString("C1GridView.Warning.DialogTitle"),
						MessageBoxButtons.YesNo) == DialogResult.Yes)
					{
						grid.SelectedIndex = -1;
						grid.DataKeyNames = null;
						grid.Columns.Clear();
					}

			return true;
		}
		#endregion


		#region IServiceProvider, IWindowsFormsEditorService
		object IServiceProvider.GetService(Type serviceType)
		{
			return (serviceType == typeof(WFD.IWindowsFormsEditorService))
				? this
				: GetService(serviceType);
		}

		void WFD.IWindowsFormsEditorService.CloseDropDown()
		{
		}

		void WFD.IWindowsFormsEditorService.DropDownControl(System.Windows.Forms.Control control)
		{
		}

		DialogResult WFD.IWindowsFormsEditorService.ShowDialog(Form dialog)
		{
			WFD.IUIService service = (WFD.IUIService)GetService(typeof(WFD.IUIService));
			return (service != null) ? service.ShowDialog(dialog) : dialog.ShowDialog();
		}
		#endregion
	}
}