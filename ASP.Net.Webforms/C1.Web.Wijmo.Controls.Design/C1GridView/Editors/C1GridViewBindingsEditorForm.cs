using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	using C1.Web.Wijmo.Controls.C1GridView;

	internal class BindingsEditorForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonOK;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox comboFieldName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonAdd;
		private System.Windows.Forms.Button buttonRemove;
		private System.Windows.Forms.ComboBox comboPropertyName;
		private System.Windows.Forms.ComboBox comboControlName;
		private System.Windows.Forms.ListView listBindings;
		private System.Windows.Forms.Button buttonUp;
		private System.Windows.Forms.Button buttonDown;
		private System.Windows.Forms.Button buttonRetrieve;
		private System.ComponentModel.IContainer components = null;

		public BindingsEditorForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
			"Wasdf"}, -1, System.Drawing.SystemColors.WindowText, System.Drawing.SystemColors.Window, new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BindingsEditorForm));
			this.label1 = new System.Windows.Forms.Label();
			this.listBindings = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.buttonOK = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.comboFieldName = new System.Windows.Forms.ComboBox();
			this.comboPropertyName = new System.Windows.Forms.ComboBox();
			this.comboControlName = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.buttonRemove = new System.Windows.Forms.Button();
			this.buttonUp = new System.Windows.Forms.Button();
			this.buttonDown = new System.Windows.Forms.Button();
			this.buttonRetrieve = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(10, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "Bindings:";
			// 
			// listBindings
			// 
			this.listBindings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this.listBindings.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
			this.columnHeader1});
			this.listBindings.FullRowSelect = true;
			this.listBindings.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.listBindings.HideSelection = false;
			this.listBindings.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
			listViewItem1});
			this.listBindings.LabelWrap = false;
			this.listBindings.Location = new System.Drawing.Point(10, 28);
			this.listBindings.MultiSelect = false;
			this.listBindings.Name = "listBindings";
			this.listBindings.Size = new System.Drawing.Size(268, 235);
			this.listBindings.TabIndex = 1;
			this.listBindings.UseCompatibleStateImageBehavior = false;
			this.listBindings.View = System.Windows.Forms.View.Details;
			this.listBindings.SelectedIndexChanged += new System.EventHandler(this.listBindings_SelectedIndexChanged);
			this.listBindings.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBindings_KeyDown);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Width = 220;
			// 
			// buttonOK
			// 
			this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.buttonOK.Location = new System.Drawing.Point(368, 282);
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.Size = new System.Drawing.Size(90, 26);
			this.buttonOK.TabIndex = 10;
			this.buttonOK.Text = "OK";
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(464, 282);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(90, 26);
			this.buttonCancel.TabIndex = 11;
			this.buttonCancel.Text = "Cancel";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(336, 83);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(120, 27);
			this.label3.TabIndex = 12;
			this.label3.Text = "Property:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(336, 138);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(278, 27);
			this.label4.TabIndex = 14;
			this.label4.Text = "Data source field:";
			// 
			// comboFieldName
			// 
			this.comboFieldName.Location = new System.Drawing.Point(336, 157);
			this.comboFieldName.Name = "comboFieldName";
			this.comboFieldName.Size = new System.Drawing.Size(288, 24);
			this.comboFieldName.TabIndex = 6;
			this.comboFieldName.SelectedIndexChanged += new System.EventHandler(this.comboFieldName_SelectedIndexChanged);
			this.comboFieldName.TextChanged += new System.EventHandler(this.comboFieldName_TextChanged);
			// 
			// comboPropertyName
			// 
			this.comboPropertyName.ItemHeight = 16;
			this.comboPropertyName.Location = new System.Drawing.Point(336, 102);
			this.comboPropertyName.Name = "comboPropertyName";
			this.comboPropertyName.Size = new System.Drawing.Size(288, 24);
			this.comboPropertyName.Sorted = true;
			this.comboPropertyName.TabIndex = 5;
			this.comboPropertyName.TextChanged += new System.EventHandler(this.comboPropertyName_TextChanged);
			// 
			// comboControlName
			// 
			this.comboControlName.ItemHeight = 16;
			this.comboControlName.Location = new System.Drawing.Point(336, 46);
			this.comboControlName.Name = "comboControlName";
			this.comboControlName.Size = new System.Drawing.Size(288, 24);
			this.comboControlName.Sorted = true;
			this.comboControlName.TabIndex = 4;
			this.comboControlName.TextChanged += new System.EventHandler(this.comboControlName_TextChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(336, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(120, 26);
			this.label2.TabIndex = 16;
			this.label2.Text = "Control:";
			// 
			// buttonAdd
			// 
			this.buttonAdd.Location = new System.Drawing.Point(19, 305);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.Size = new System.Drawing.Size(90, 26);
			this.buttonAdd.TabIndex = 8;
			this.buttonAdd.Text = "&Add";
			this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// buttonRemove
			// 
			this.buttonRemove.Location = new System.Drawing.Point(144, 305);
			this.buttonRemove.Name = "buttonRemove";
			this.buttonRemove.Size = new System.Drawing.Size(90, 26);
			this.buttonRemove.TabIndex = 9;
			this.buttonRemove.Text = "&Remove";
			this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
			// 
			// buttonUp
			// 
			this.buttonUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonUp.Image = ((System.Drawing.Image)(resources.GetObject("buttonUp.Image")));
			this.buttonUp.Location = new System.Drawing.Point(224, 28);
			this.buttonUp.Name = "buttonUp";
			this.buttonUp.Size = new System.Drawing.Size(29, 27);
			this.buttonUp.TabIndex = 2;
			this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
			// 
			// buttonDown
			// 
			this.buttonDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonDown.Image = ((System.Drawing.Image)(resources.GetObject("buttonDown.Image")));
			this.buttonDown.Location = new System.Drawing.Point(224, 61);
			this.buttonDown.Name = "buttonDown";
			this.buttonDown.Size = new System.Drawing.Size(29, 28);
			this.buttonDown.TabIndex = 3;
			this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
			// 
			// buttonRetrieve
			// 
			this.buttonRetrieve.Location = new System.Drawing.Point(403, 222);
			this.buttonRetrieve.Name = "buttonRetrieve";
			this.buttonRetrieve.Size = new System.Drawing.Size(135, 36);
			this.buttonRetrieve.TabIndex = 7;
			this.buttonRetrieve.Text = "Retrieve simple bindings";
			this.buttonRetrieve.Click += new System.EventHandler(this.buttonRetrieve_Click);
			// 
			// BindingsEditorForm
			// 
			this.AcceptButton = this.buttonOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(570, 315);
			this.Controls.Add(this.buttonRetrieve);
			this.Controls.Add(this.buttonDown);
			this.Controls.Add(this.buttonUp);
			this.Controls.Add(this.buttonRemove);
			this.Controls.Add(this.buttonAdd);
			this.Controls.Add(this.comboControlName);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.comboFieldName);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.comboPropertyName);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOK);
			this.Controls.Add(this.listBindings);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(576, 346);
			this.Name = "BindingsEditorForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Update bindings";
			this.Load += new System.EventHandler(this.BindingsEditorForm_Load);
			this.ResumeLayout(false);

		}
		#endregion

		// member fields
		private Type _bindingsType;
		private C1GridViewUpdateBindingCollection _bindings;
		private StringCollection _fieldNames = new StringCollection();
		private StringCollection _controlNames = new StringCollection();
		private C1GridViewUpdateBindingCollection _bindingsFromTemplate;
		private System.Web.UI.Control _templateContainer = new System.Web.UI.WebControls.TableCell();
		private int _internalChange;
		private bool _dirty;

		//static methods
		static internal bool ShowEditor(Type bindingsType, C1GridViewUpdateBindingCollection bindings,
			StringCollection fieldNames, System.Web.UI.ITemplate template)
		{
			BindingsEditorForm form = new BindingsEditorForm();
			form._bindingsType = bindingsType;
			form._bindings = (C1GridViewUpdateBindingCollection)Activator.CreateInstance(bindingsType);
			//((ICreateable)form._bindings).CopyFrom(bindings);
			((IC1Cloneable)form._bindings).CopyFrom(bindings);
			form._fieldNames = fieldNames;
			form._bindingsFromTemplate = (C1GridViewUpdateBindingCollection)Activator.CreateInstance(bindingsType);
			template.InstantiateIn(form._templateContainer);
			form.InitFromTemplate(form._templateContainer, true);
			if (form.ShowDialog() == DialogResult.OK && form._dirty)
			{
				bindings.Clear();
				//((ICreateable)bindings).CopyFrom(form._bindings);
				((IC1Cloneable)bindings).CopyFrom(form._bindings);
				return true;
			}
			return false;
		}

		// private methods
		private void InitFromTemplate(System.Web.UI.Control control, bool skipThis)
		{
			if (!skipThis)
			{
				if (control.ID != null && control.ID.Length != 0)
					_controlNames.Add(control.ID);
				System.Web.UI.IDataBindingsAccessor accessor = control as System.Web.UI.IDataBindingsAccessor;
				if (accessor != null)
				{
					System.Web.UI.DataBindingCollection bindings = accessor.DataBindings;
					foreach (System.Web.UI.DataBinding binding in bindings)
					{
						string fieldName = SimpleBindingFiledName(binding);
						if (fieldName != "")
						{
							//C1GridViewUpdateBinding newBinding = (C1GridViewUpdateBinding)((ICreateable)_bindingsFromTemplate).CreateInstance();
							C1GridViewUpdateBinding newBinding = new C1GridViewUpdateBinding();
							newBinding.ControlProperty = control.ID + "." + binding.PropertyName;
							newBinding.UpdateField = fieldName;
							_bindingsFromTemplate.Add(newBinding);
						}
					}
				}
			}
			foreach (System.Web.UI.Control child in control.Controls)
				InitFromTemplate(child, false);
		}
		private string SimpleBindingFiledName(System.Web.UI.DataBinding binding)
		{
			string exp = binding.Expression.Trim();
			if (exp.Length == 0 || exp[exp.Length - 1] != ')')
				return "";
			exp = exp.Remove(exp.Length - 1, 1).TrimEnd();
			if (exp.Length == 0 || exp[exp.Length - 1] != '"')
				return "";
			exp = exp.Remove(exp.Length - 1, 1).TrimEnd();
			int pos = exp.LastIndexOf('"');
			if (pos == -1 || pos == exp.Length - 1)
				return "";
			string fieldName = exp.Substring(pos + 1);
			exp = exp.Substring(0, pos).TrimEnd();
			if (exp.Length == 0 || exp[exp.Length - 1] != ',')
				return "";
			exp = exp.Remove(exp.Length - 1, 1).TrimEnd().ToUpper();
			if (!exp.EndsWith("DATAITEM"))
				return "";
			exp = exp.Substring(0, exp.Length - "DataItem".Length).TrimEnd();
			if (!exp.EndsWith("."))
				return "";
			exp = exp.Substring(0, exp.Length - 1).TrimEnd();
			if (!exp.EndsWith("CONTAINER"))
				return "";
			exp = exp.Substring(0, exp.Length - "Container".Length).TrimEnd();
			if (!exp.EndsWith("("))
				return "";
			exp = exp.Substring(0, exp.Length - 1).TrimEnd();
			if (!exp.EndsWith("EVAL"))
				return "";
			exp = exp.Substring(0, exp.Length - "Eval".Length).TrimEnd();
			if (!exp.EndsWith("."))
				return "";
			exp = exp.Substring(0, exp.Length - 1).TrimEnd();
			if (!exp.EndsWith("DATABINDER"))
				return "";
			exp = exp.Substring(0, exp.Length - "DataBinder".Length).TrimEnd();
			if (exp.Length != 0)
				return "";
			return fieldName;
		}
		private void UpdateControls()
		{
			_internalChange++;
			try
			{
				bool noBindings = _bindings.Count == 0;
				label2.Enabled = !noBindings;
				label3.Enabled = !noBindings;
				label4.Enabled = !noBindings;
				comboControlName.Enabled = !noBindings;
				comboPropertyName.Enabled = !noBindings;
				comboFieldName.Enabled = !noBindings;
				comboControlName.Text = "";
				comboPropertyName.Text = "";
				comboFieldName.Text = "";
				listBindings.Items.Clear();
				foreach (C1GridViewUpdateBinding binding in _bindings)
				{
					ListViewItem item = new ListViewItem(binding.ToString());
					item.Tag = binding;
					listBindings.Items.Add(item);
				}
			}
			finally
			{
				_internalChange--;
			}
		}
		private void UpdateAll()
		{
			_internalChange++;
			try
			{
				comboControlName.Items.Clear();
				foreach (string controlName in _controlNames)
					comboControlName.Items.Add(controlName);
				comboControlName.Text = "";
				comboPropertyName.Items.Clear();
				comboPropertyName.Text = "";
				comboFieldName.Items.Clear();
				foreach (string fieldName in _fieldNames)
					comboFieldName.Items.Add(fieldName);
				comboFieldName.Text = "";
				UpdateControls();
				if (_bindings.Count != 0)
				{
					listBindings.Items[0].Selected = true;
					OnSelectItem();
				}
			}
			finally
			{
				_internalChange--;
			}
		}
		private void OnSelectItem()
		{
			_internalChange++;
			try
			{
				if (listBindings.SelectedItems.Count == 0)
				{
					comboControlName.Text = "";
					comboPropertyName.Text = "";
					comboFieldName.Text = "";
					buttonUp.Enabled = false;
					buttonDown.Enabled = false;
				}
				else
				{
					ListViewItem item = listBindings.SelectedItems[0];
					C1GridViewUpdateBinding binding = (C1GridViewUpdateBinding)item.Tag;
					string ctrlProp = binding.ControlProperty;
					int pos = ctrlProp.IndexOf('.');
					if (pos <= 0 || pos >= ctrlProp.Length - 1)
					{
						comboControlName.Text = ctrlProp;
						comboPropertyName.Text = "";
					}
					else
					{
						comboControlName.Text = ctrlProp.Substring(0, pos); ;
						comboPropertyName.Text = ctrlProp.Substring(pos + 1);
					}
					comboFieldName.Text = binding.UpdateField;
					OnControlNameChange();
					int idx = item.Index;
					buttonUp.Enabled = idx > 0;
					buttonDown.Enabled = idx < _bindings.Count - 1;
				}
			}
			finally
			{
				_internalChange--;
			}
		}
		private void OnControlNameChange()
		{
			_internalChange++;
			try
			{
				string controlName = comboControlName.Text;
				comboPropertyName.Items.Clear();
				System.Web.UI.Control control = FindTemplateControl(_templateContainer, controlName);
				if (control == null)
					return;
				PropertyDescriptorCollection props = TypeDescriptor.GetProperties(control);
				foreach (PropertyDescriptor prop in props)
					comboPropertyName.Items.Add(prop.Name);
			}
			finally
			{
				_internalChange--;
			}
		}
		private System.Web.UI.Control FindTemplateControl(System.Web.UI.Control control, string id)
		{
			if (String.Compare(control.ID, id, true) == 0)
				return control;
			foreach (System.Web.UI.Control child in control.Controls)
			{
				System.Web.UI.Control foundCtrl = FindTemplateControl(child, id);
				if (foundCtrl != null)
					return foundCtrl;
			}
			return null;
		}
		private void OnBindingChange()
		{
			if (listBindings.SelectedItems.Count == 0)
				return;
			ListViewItem item = listBindings.SelectedItems[0];
			C1GridViewUpdateBinding binding = (C1GridViewUpdateBinding)item.Tag;
			binding.ControlProperty = comboControlName.Text + "." + comboPropertyName.Text;
			binding.UpdateField = comboFieldName.Text;
			item.Text = binding.ToString();
			_dirty = true;
		}
		private void OnAddBinding()
		{
			//C1GridViewUpdateBinding binding = (C1GridViewUpdateBinding)((ICreateable)_bindings).CreateInstance();
			C1GridViewUpdateBinding binding = new C1GridViewUpdateBinding();
			_bindings.Add(binding);
			_dirty = true;
			UpdateControls();
			if (_bindings.Count != 0)
			{
				listBindings.Items[_bindings.Count - 1].Selected = true;
				OnSelectItem();
			}
		}
		private void OnRemoveBinding()
		{
			if (listBindings.SelectedItems.Count == 0)
				return;
			C1GridViewUpdateBinding binding = (C1GridViewUpdateBinding)listBindings.SelectedItems[0].Tag;
			int idx = _bindings.IndexOf(binding);
			if (idx == -1)
				return;
			_bindings.RemoveAt(idx);
			_dirty = true;
			UpdateControls();
			if (idx >= listBindings.Items.Count)
				idx = listBindings.Items.Count - 1;
			if (idx >= 0)
			{
				listBindings.Items[idx].Selected = true;
				OnSelectItem();
			}
		}
		private void SwapBindings(int i1, int i2)
		{
			Debug.Assert(i1 < i2);
			Debug.Assert(i1 >= 0 && i1 < _bindings.Count && i2 >= 0 && i2 < _bindings.Count);
			C1GridViewUpdateBinding binding1 = (C1GridViewUpdateBinding)((IList)_bindings)[i1];
			C1GridViewUpdateBinding binding2 = (C1GridViewUpdateBinding)((IList)_bindings)[i2];
			_bindings.RemoveAt(i2);
			_bindings.RemoveAt(i1);
			_bindings.Insert(i1, binding2);
			_bindings.Insert(i2, binding1);
		}
		private void MoveBinding(int index, bool up)
		{
			if (up)
			{
				if (index < 1)
					return;
				SwapBindings(index - 1, index);
				index--;
			}
			else
			{
				if (index > _bindings.Count - 2)
					return;
				SwapBindings(index, index + 1);
				index++;
			}
			_dirty = true;
			UpdateControls();
			listBindings.Items[index].Selected = true;
			OnSelectItem();
		}

		//event handlers
		private void listBindings_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (_internalChange != 0)
				return;
			OnSelectItem();
		}

		private void buttonAdd_Click(object sender, System.EventArgs e)
		{
			OnAddBinding();
		}

		private void buttonRemove_Click(object sender, System.EventArgs e)
		{
			OnRemoveBinding();
		}

		private void buttonUp_Click(object sender, System.EventArgs e)
		{
			if (listBindings.SelectedItems.Count == 0)
				return;
			MoveBinding(listBindings.SelectedItems[0].Index, true);
		}

		private void buttonDown_Click(object sender, System.EventArgs e)
		{
			if (listBindings.SelectedItems.Count == 0)
				return;
			MoveBinding(listBindings.SelectedItems[0].Index, false);
		}

		private void listBindings_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
				OnRemoveBinding();
		}

		private void comboFieldName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		}

		private void BindingsEditorForm_Load(object sender, System.EventArgs e)
		{
			UpdateAll();
		}

		private void buttonRetrieve_Click(object sender, System.EventArgs e)
		{
			_bindings = (C1GridViewUpdateBindingCollection)Activator.CreateInstance(_bindingsType);
			//((ICreateable)_bindings).CopyFrom(_bindingsFromTemplate);
			((IC1Cloneable)_bindings).CopyFrom(_bindingsFromTemplate);
			_dirty = true;
			UpdateAll();
		}

		private void comboControlName_TextChanged(object sender, System.EventArgs e)
		{
			if (_internalChange != 0)
				return;
			OnControlNameChange();
			OnBindingChange();
		}

		private void comboPropertyName_TextChanged(object sender, System.EventArgs e)
		{
			if (_internalChange != 0)
				return;
			OnBindingChange();
		}

		private void comboFieldName_TextChanged(object sender, System.EventArgs e)
		{
			if (_internalChange != 0)
				return;
			OnBindingChange();
		}

	}
}
