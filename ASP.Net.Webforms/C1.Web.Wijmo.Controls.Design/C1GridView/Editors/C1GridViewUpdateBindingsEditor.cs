using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	using C1.Web.Wijmo.Controls.C1GridView;

	/// <summary>
	/// Property editor for <see cref="C1.Web.UI.Controls.C1GridView.C1TemplateField.UpdateBindings"/> property.
	/// </summary>
	internal class C1GridViewUpdateBindingsEditor : CollectionEditor
	{
		public C1GridViewUpdateBindingsEditor()
			: base(typeof(C1GridViewUpdateBindingCollection))
		{
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			// get column and grid
			C1TemplateField templateField = context.Instance as C1TemplateField;

			if (templateField == null || templateField.EditItemTemplate == null)
			{
				throw new Exception("You must define an EditItemTemplate first");
			}

			C1GridView grid = (C1GridView)((IOwnerable)templateField).Owner;

			// get schema from grid (via designer)
			IDesignerHost host = (IDesignerHost)provider.GetService(typeof(IDesignerHost));
			C1GridViewDesigner designer = (C1GridViewDesigner)host.GetDesigner(grid);

			IDataSourceFieldSchema[] schema = designer.DataSourceSchemaFields;

			// populate field name list
			StringCollection fieldNames = new StringCollection();
			foreach (IDataSourceFieldSchema fieldSchema in schema)
			{
				fieldNames.Add(fieldSchema.Name);
			}

			// show the editor
			if (BindingsEditorForm.ShowEditor(typeof(C1GridViewUpdateBindingCollection),
				templateField.UpdateBindings, fieldNames, templateField.EditItemTemplate))
			{
				if (!designer.BuilderDialogSetDirty())
				{
					context.OnComponentChanged();
				}
			}

			return templateField.UpdateBindings;
		}
	}
}
