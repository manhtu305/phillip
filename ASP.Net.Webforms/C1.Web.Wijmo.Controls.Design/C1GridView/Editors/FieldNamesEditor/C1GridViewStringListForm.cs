using System;
using System.Text;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	internal partial class C1GridViewStringListForm : C1GridViewFieldNamesBaseForm
	{
		public C1GridViewStringListForm()
		{
			InitializeComponent();
		}

		public C1GridViewStringListForm(string[] editValues)
			: base(editValues)
		{
			InitializeComponent();
		}

		private void StringListForm_Load(object sender, EventArgs e)
		{
			tbLines.Text = string.Join("\r\n", EditValues);
		}

		private void _btnOK_Click(object sender, EventArgs e)
		{
			string[] values = tbLines.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

			for (int i = 0; i < values.Length; i++)
			{
				values[i] = values[i].Trim();
			}

			SetEditValues(values);
		}
	}
}