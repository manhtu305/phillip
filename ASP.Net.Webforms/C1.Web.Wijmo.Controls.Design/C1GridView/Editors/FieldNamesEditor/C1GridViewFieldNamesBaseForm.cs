using System;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	internal class C1GridViewFieldNamesBaseForm : Form
	{
		private string[] _editValues;

		public C1GridViewFieldNamesBaseForm()
		{
		}

		public C1GridViewFieldNamesBaseForm(string[] editValues)
			: base()
		{
			if (editValues != null)
			{
				_editValues = new string[editValues.Length];
				Array.Copy(editValues, _editValues, editValues.Length);
			}
			else
			{
				_editValues = new string[0];
			}
		}

		public string[] EditValues
		{
			get { return _editValues; }
		}

		protected void SetEditValues(string[] value)
		{
			_editValues = value;
		}
	}
}
