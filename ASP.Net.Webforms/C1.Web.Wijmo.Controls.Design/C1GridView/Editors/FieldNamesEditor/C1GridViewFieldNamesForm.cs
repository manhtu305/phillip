using System;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	internal partial class C1GridViewFieldNamesForm : C1GridViewFieldNamesBaseForm
	{
		private string[] _availableFields;

		public C1GridViewFieldNamesForm()
		{
			InitializeComponent();
		}

		public C1GridViewFieldNamesForm(string[] editValues, string[] availableValues)
			: base(editValues)
		{
			if (availableValues != null)
			{
				_availableFields = new string[availableValues.Length];
				Array.Copy(availableValues, _availableFields, availableValues.Length);
			}
			else
			{
				_availableFields = new string[0];
			}

			InitializeComponent();
		}

		private void FieldNamesForm_Load(object sender, EventArgs e)
		{
			foreach (string val in _availableFields)
			{
				lbAvailableFields.Items.Add(val);
			}

			foreach (string val in EditValues)
			{
				lbSelectedFields.Items.Add(val);
			}
		}

		private void lbAvailableFields_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lbAvailableFields.SelectedIndex >= 0)
			{
				lbSelectedFields.SelectedIndex = -1;
			}

			UpdateUI();
		}

		private void lbSelectedFields_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lbSelectedFields.SelectedIndex >= 0)
			{
				lbAvailableFields.SelectedIndex = -1;
			}

			UpdateUI();
		}

		private void UpdateUI()
		{
			btnRight.Enabled = (lbAvailableFields.SelectedIndex >= 0);
			btnLeft.Enabled = (lbSelectedFields.SelectedIndex >= 0);

			btnUp.Enabled = (lbSelectedFields.SelectedIndex > 0);
			btnDown.Enabled = ((lbSelectedFields.SelectedIndex >= 0) &&
				(lbSelectedFields.SelectedIndex < lbSelectedFields.Items.Count - 1));
		}

		private void btnRight_Click(object sender, EventArgs e)
		{
			int selectedIndex = lbAvailableFields.SelectedIndex;

			string value = (string)lbAvailableFields.SelectedItem;
			lbAvailableFields.Items.RemoveAt(selectedIndex);
			lbSelectedFields.Items.Add(value);

			int cnt = lbAvailableFields.Items.Count;
			if (cnt > 0)
			{
				lbAvailableFields.SelectedIndex = (selectedIndex < cnt)
					? selectedIndex
					: cnt - 1;
			}
		}

		private void btnLeft_Click(object sender, EventArgs e)
		{
			int selectedIndex = lbSelectedFields.SelectedIndex;

			string value = (string)lbSelectedFields.SelectedItem;
			lbSelectedFields.Items.RemoveAt(selectedIndex);
			lbAvailableFields.Items.Add(value);

			int cnt = lbSelectedFields.Items.Count;
			if (cnt > 0)
			{
				lbSelectedFields.SelectedIndex = (selectedIndex < cnt)
					? selectedIndex
					: cnt - 1;
			}
		}

		private void btnUp_Click(object sender, EventArgs e)
		{
			int selectedIndex = lbSelectedFields.SelectedIndex;
			object value = lbSelectedFields.SelectedItem;

			lbSelectedFields.Items.RemoveAt(selectedIndex);
			lbSelectedFields.Items.Insert(selectedIndex - 1, value);

			lbSelectedFields.SelectedIndex = selectedIndex - 1;
		}

		private void btnDown_Click(object sender, EventArgs e)
		{
			int selectedIndex = lbSelectedFields.SelectedIndex;
			object value = lbSelectedFields.SelectedItem;

			lbSelectedFields.Items.RemoveAt(selectedIndex);
			lbSelectedFields.Items.Insert(selectedIndex + 1, value);

			lbSelectedFields.SelectedIndex = selectedIndex + 1;
		}

		private void _btnOK_Click(object sender, EventArgs e)
		{
			string[] values = new string[lbSelectedFields.Items.Count];

			for (int i = lbSelectedFields.Items.Count - 1; i >= 0; i--)
			{
				values[i] = (string)lbSelectedFields.Items[i];
			}

			SetEditValues(values);
		}
	}
}