using C1.Web.Wijmo.Controls.Design.Localization;
namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
    partial class C1GridViewFieldNamesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">True if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAvailableFields = new System.Windows.Forms.ListBox();
            this.labAvailableFields = new System.Windows.Forms.Label();
            this.labSelectedFields = new System.Windows.Forms.Label();
            this.lbSelectedFields = new System.Windows.Forms.ListBox();
            this._btnOK = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbAvailableFields
            // 
            this.lbAvailableFields.FormattingEnabled = true;
            this.lbAvailableFields.ItemHeight = 16;
            this.lbAvailableFields.Location = new System.Drawing.Point(12, 29);
            this.lbAvailableFields.Name = "lbAvailableFields";
            this.lbAvailableFields.Size = new System.Drawing.Size(173, 196);
            this.lbAvailableFields.TabIndex = 0;
            this.lbAvailableFields.SelectedIndexChanged += new System.EventHandler(this.lbAvailableFields_SelectedIndexChanged);
            // 
            // labAvailableFields
            // 
            this.labAvailableFields.AutoSize = true;
            this.labAvailableFields.Location = new System.Drawing.Point(13, 6);
            this.labAvailableFields.Name = "labAvailableFields";
            this.labAvailableFields.Size = new System.Drawing.Size(106, 17);
            this.labAvailableFields.TabIndex = 1;
            this.labAvailableFields.Text = C1Localizer.GetString("C1GridView.FieldNamesForm.AvailableFields");
            // 
            // labSelectedFields
            // 
            this.labSelectedFields.AutoSize = true;
            this.labSelectedFields.Location = new System.Drawing.Point(243, 6);
            this.labSelectedFields.Name = "labSelectedFields";
            this.labSelectedFields.Size = new System.Drawing.Size(104, 17);
            this.labSelectedFields.TabIndex = 3;
            this.labSelectedFields.Text = C1Localizer.GetString("C1GridView.FieldNamesForm.SelectedFields");
            // 
            // lbSelectedFields
            // 
            this.lbSelectedFields.FormattingEnabled = true;
            this.lbSelectedFields.ItemHeight = 16;
            this.lbSelectedFields.Location = new System.Drawing.Point(242, 29);
            this.lbSelectedFields.Name = "lbSelectedFields";
            this.lbSelectedFields.Size = new System.Drawing.Size(173, 196);
            this.lbSelectedFields.TabIndex = 3;
            this.lbSelectedFields.SelectedIndexChanged += new System.EventHandler(this.lbSelectedFields_SelectedIndexChanged);
            // 
            // _btnOK
            // 
            this._btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btnOK.Location = new System.Drawing.Point(255, 243);
            this._btnOK.Name = "_btnOK";
            this._btnOK.Size = new System.Drawing.Size(100, 29);
            this._btnOK.TabIndex = 6;
            this._btnOK.Text = C1Localizer.GetString("Base.OK");
            this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(361, 243);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(100, 29);
            this._btnCancel.TabIndex = 7;
            this._btnCancel.Text = C1Localizer.GetString("Base.Cancel");
            // 
            // btnRight
            // 
            this.btnRight.Enabled = false;
            this.btnRight.Location = new System.Drawing.Point(195, 94);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(37, 29);
            this.btnRight.TabIndex = 1;
            this.btnRight.Text = ">";
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Enabled = false;
            this.btnLeft.Location = new System.Drawing.Point(195, 129);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(37, 29);
            this.btnLeft.TabIndex = 2;
            this.btnLeft.Text = "<";
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnDown
            // 
            this.btnDown.Enabled = false;
            this.btnDown.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowDownIco;
            this.btnDown.Location = new System.Drawing.Point(424, 129);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(37, 29);
            this.btnDown.TabIndex = 5;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUp
            // 
            this.btnUp.Enabled = false;
            this.btnUp.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowUpIco;
            this.btnUp.Location = new System.Drawing.Point(424, 94);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(37, 29);
            this.btnUp.TabIndex = 4;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // FieldNamesForm
            // 
            this.AcceptButton = this._btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(473, 284);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this._btnOK);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this.labSelectedFields);
            this.Controls.Add(this.lbSelectedFields);
            this.Controls.Add(this.labAvailableFields);
            this.Controls.Add(this.lbAvailableFields);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FieldNamesForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = C1Localizer.GetString("C1GridView.FieldNamesForm.Title");
            this.Load += new System.EventHandler(this.FieldNamesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbAvailableFields;
        private System.Windows.Forms.Label labAvailableFields;
        private System.Windows.Forms.Label labSelectedFields;
        private System.Windows.Forms.ListBox lbSelectedFields;
        private System.Windows.Forms.Button _btnOK;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
    }
}