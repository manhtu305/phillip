using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using WF = System.Windows.Forms;
using System.Windows.Forms.Design;


namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	internal class C1GridViewFieldNamesEditor : UITypeEditor
	{
		private ITypeDescriptorContext _context;

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context != null && context.Instance != null)
			{
				return UITypeEditorEditStyle.Modal;
			}

			return base.GetEditStyle(context);
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (provider != null)
			{
				_context = context;

				if (_context != null && _context.Instance != null)
				{
					IWindowsFormsEditorService wfes = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

					string[] valueSorted = new string[((string[])value).Length];
					Array.Copy((string[])value, valueSorted, valueSorted.Length);
					Array.Sort<string>(valueSorted, StringComparer.CurrentCultureIgnoreCase);

					IDataSourceViewSchema schema = GetDataSchema();
					IDataSourceFieldSchema[] fields;

					C1GridViewFieldNamesBaseForm form;

					if (schema != null && (fields = schema.GetFields()).Length > 0)
					{
						List<string> names = new List<string>(fields.Length);

						foreach (IDataSourceFieldSchema field in fields)
						{
							if (Array.BinarySearch<string>(valueSorted, field.Name, StringComparer.CurrentCultureIgnoreCase) < 0)
							{
								names.Add(field.Name);
							}
						}

						form = new C1GridViewFieldNamesForm((string[])value, names.ToArray());
					}
					else
					{
						form = new C1GridViewStringListForm((string[])value);
					}

					IDesignerHost host = context.GetService(typeof(IDesignerHost)) as IDesignerHost;
					DesignerTransaction transaction = null;

					WF.DialogResult dialogResult = WF.DialogResult.Cancel;

					try
					{
						transaction = host.CreateTransaction();
						dialogResult = wfes.ShowDialog(form);
						return form.EditValues;
					}
					catch (CheckoutException)
					{
					}
					finally
					{
						if (transaction != null)
						{
							if (dialogResult == WF.DialogResult.OK)
							{
								transaction.Commit();
							}
							else
							{
								transaction.Cancel();
							}
						}

						form.Dispose();
					}
				}
			}

			return base.EditValue(context, provider, value);
		}

		protected ITypeDescriptorContext Context
		{
			get { return _context; }
		}

		protected IDataSourceViewSchema GetDataSchema()
		{
			IDataSourceViewSchema schema = null;

			if (Context != null)
			{
				Control ctrl = Context.Instance as Control;
				if (ctrl != null)
				{
					if (ctrl.Site != null)
					{

						IDesignerHost host = ctrl.Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
						if (host != null)
						{
							DataBoundControlDesigner designer = host.GetDesigner(ctrl) as DataBoundControlDesigner;
							if (designer != null && designer.DesignerView != null)
							{
								try
								{
									schema = designer.DesignerView.Schema;
								}
								catch
								{
								}
							}
						}
					}
				}
				else
				{
					IDataSourceViewSchemaAccessor accessor = Context.Instance as IDataSourceViewSchemaAccessor;
					if (accessor != null)
					{
						schema = accessor.DataSourceViewSchema as IDataSourceViewSchema;
					}
				}
			}

			return schema;
		}
	}
}
