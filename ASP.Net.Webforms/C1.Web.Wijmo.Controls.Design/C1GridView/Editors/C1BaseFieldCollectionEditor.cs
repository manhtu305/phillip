﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;

namespace C1.Web.Wijmo.Controls.Design.C1GridView.UITypeEditors
{
	using C1.Web.Wijmo.Controls.C1GridView;

	internal class C1BaseFieldCollectionEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context != null && context.Instance != null)
			{
				return UITypeEditorEditStyle.Modal;
			}

			return base.GetEditStyle(context);
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context != null && context.Instance != null && provider != null)
			{
				C1GridView gridView = context.Instance as C1GridView;
				if (gridView != null)
				{
					IDesignerHost host = (IDesignerHost)provider.GetService(typeof(IDesignerHost));

					C1GridViewDesigner designer = (C1GridViewDesigner)host.GetDesigner(gridView);
					designer.PropertyBuilder(1);
				}
			}

			return value;
		}

	}
}