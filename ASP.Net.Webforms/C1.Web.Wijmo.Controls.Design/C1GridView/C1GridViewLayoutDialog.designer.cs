﻿namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	partial class C1GridViewLayoutDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.cbAccessibility = new System.Windows.Forms.CheckBox();
			this.cbSizes = new System.Windows.Forms.CheckBox();
			this.cbStyles = new System.Windows.Forms.CheckBox();
			this.cbMisc = new System.Windows.Forms.CheckBox();
			this.cbBehavior = new System.Windows.Forms.CheckBox();
			this.cbAppearance = new System.Windows.Forms.CheckBox();
			this.rbtnCustom = new System.Windows.Forms.RadioButton();
			this.rbtnAll = new System.Windows.Forms.RadioButton();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(110, 269);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(82, 25);
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel";
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(12, 269);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(82, 25);
			this.btnOK.TabIndex = 10;
			this.btnOK.Text = "OK";
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.cbAccessibility);
			this.panel1.Controls.Add(this.cbSizes);
			this.panel1.Controls.Add(this.cbStyles);
			this.panel1.Controls.Add(this.cbMisc);
			this.panel1.Controls.Add(this.cbBehavior);
			this.panel1.Controls.Add(this.cbAppearance);
			this.panel1.Enabled = false;
			this.panel1.Location = new System.Drawing.Point(21, 67);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(171, 185);
			this.panel1.TabIndex = 2;
			// 
			// cbAccessibility
			// 
			this.cbAccessibility.AutoSize = true;
			this.cbAccessibility.Checked = true;
			this.cbAccessibility.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbAccessibility.Location = new System.Drawing.Point(7, 151);
			this.cbAccessibility.Name = "cbAccessibility";
			this.cbAccessibility.Size = new System.Drawing.Size(106, 21);
			this.cbAccessibility.TabIndex = 9;
			this.cbAccessibility.Text = "Accessibility";
			this.cbAccessibility.UseVisualStyleBackColor = true;
			this.cbAccessibility.Click += new System.EventHandler(this.checkBox_Click);
			// 
			// cbSizes
			// 
			this.cbSizes.AutoSize = true;
			this.cbSizes.Checked = true;
			this.cbSizes.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbSizes.Location = new System.Drawing.Point(7, 122);
			this.cbSizes.Name = "cbSizes";
			this.cbSizes.Size = new System.Drawing.Size(64, 21);
			this.cbSizes.TabIndex = 8;
			this.cbSizes.Text = "Sizes";
			this.cbSizes.Click += new System.EventHandler(this.checkBox_Click);
			// 
			// cbStyles
			// 
			this.cbStyles.AutoSize = true;
			this.cbStyles.Checked = true;
			this.cbStyles.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbStyles.Location = new System.Drawing.Point(7, 93);
			this.cbStyles.Name = "cbStyles";
			this.cbStyles.Size = new System.Drawing.Size(68, 21);
			this.cbStyles.TabIndex = 7;
			this.cbStyles.Text = "Styles";
			this.cbStyles.Click += new System.EventHandler(this.checkBox_Click);
			// 
			// cbMisc
			// 
			this.cbMisc.AutoSize = true;
			this.cbMisc.Checked = true;
			this.cbMisc.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbMisc.Location = new System.Drawing.Point(7, 65);
			this.cbMisc.Name = "cbMisc";
			this.cbMisc.Size = new System.Drawing.Size(58, 21);
			this.cbMisc.TabIndex = 6;
			this.cbMisc.Text = "Misc";
			this.cbMisc.Click += new System.EventHandler(this.checkBox_Click);
			// 
			// cbBehavior
			// 
			this.cbBehavior.AutoSize = true;
			this.cbBehavior.Checked = true;
			this.cbBehavior.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbBehavior.Location = new System.Drawing.Point(7, 36);
			this.cbBehavior.Name = "cbBehavior";
			this.cbBehavior.Size = new System.Drawing.Size(86, 21);
			this.cbBehavior.TabIndex = 5;
			this.cbBehavior.Text = "Behavior";
			this.cbBehavior.Click += new System.EventHandler(this.checkBox_Click);
			// 
			// cbAppearance
			// 
			this.cbAppearance.AutoSize = true;
			this.cbAppearance.Checked = true;
			this.cbAppearance.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbAppearance.Location = new System.Drawing.Point(7, 7);
			this.cbAppearance.Name = "cbAppearance";
			this.cbAppearance.Size = new System.Drawing.Size(107, 21);
			this.cbAppearance.TabIndex = 4;
			this.cbAppearance.Text = "Appearance";
			this.cbAppearance.Click += new System.EventHandler(this.checkBox_Click);
			// 
			// rbtnCustom
			// 
			this.rbtnCustom.AutoSize = true;
			this.rbtnCustom.Location = new System.Drawing.Point(12, 39);
			this.rbtnCustom.Name = "rbtnCustom";
			this.rbtnCustom.Size = new System.Drawing.Size(76, 21);
			this.rbtnCustom.TabIndex = 1;
			this.rbtnCustom.Text = "Custom";
			this.rbtnCustom.Click += new System.EventHandler(this.rbtnCustom_Click);
			// 
			// rbtnAll
			// 
			this.rbtnAll.AutoSize = true;
			this.rbtnAll.Checked = true;
			this.rbtnAll.Location = new System.Drawing.Point(12, 11);
			this.rbtnAll.Name = "rbtnAll";
			this.rbtnAll.Size = new System.Drawing.Size(44, 21);
			this.rbtnAll.TabIndex = 0;
			this.rbtnAll.TabStop = true;
			this.rbtnAll.Text = "All";
			this.rbtnAll.Click += new System.EventHandler(this.rbtnAll_Click);
			// 
			// C1GridViewLayoutDialog
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(204, 307);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.rbtnCustom);
			this.Controls.Add(this.rbtnAll);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "C1GridViewLayoutDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "C1GridViewLayoutDialog";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox cbSizes;
		private System.Windows.Forms.CheckBox cbStyles;
		private System.Windows.Forms.CheckBox cbMisc;
		private System.Windows.Forms.CheckBox cbBehavior;
		private System.Windows.Forms.CheckBox cbAppearance;
		private System.Windows.Forms.RadioButton rbtnCustom;
		private System.Windows.Forms.RadioButton rbtnAll;
		private System.Windows.Forms.CheckBox cbAccessibility;
	}
}