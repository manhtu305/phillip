//----------------------------------------------------------------------------
// C1\Web\C1WebGrid\Design\PropertyBuilderDialog.cs
//
// Implementation of the PropertyBuilderDialog for the C1WebGrid. This is 
// largely a clone of the DataGrid design time dialog. It contains a 
// collection of pages which are UserControls that implement the
// internal IPropertyPanel interface.
//
// Copyright (C) 2002 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status			Date			By						Comments
// Created			Aug 14, 2002	Bernardo				-
//----------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.ComponentModel.Design;


namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;
	/// <summary>
	/// IPropertyPanel
	/// The property builder form contains panels that are UserControl
	/// objects. They all must implement IPropertyPanel.
	/// </summary>
	internal interface IPropertyPanel
	{
		void Initialize(PropertyBuilderDialog owner);
		void Apply();
	}

	/// <summary>
	/// Summary C1Description for PropertyBuilderDialog.
	/// </summary>
	internal class PropertyBuilderDialog : System.Windows.Forms.Form
	{
		internal MenuBar _menuBar;
		internal System.Windows.Forms.Button _btnOK;
		private System.Windows.Forms.Button _btnCancel;
		private System.Windows.Forms.Button _btnApply;
		private System.Windows.Forms.Button _btnHelp;
		private System.Windows.Forms.Panel _panelSize;
		private IPropertyPanel[] _panels = null;
		private Panel _darkBar;
		private ToolTip toolTip1;
		private IContainer components;

		public PropertyBuilderDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			Localize();

			// initialize menu bar
			_panels = new IPropertyPanel[]
				{
					new GeneralPanel(),
					new ColumnsPanel(),
					new PagingPanel(),
					new FormatPanel(),
					new GroupingPanel()
				};

			for (int i = 0; i < _panels.Length; i++)
			{
				(_panels[i] as Control).Parent = this;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._menuBar = new C1.Web.Wijmo.Controls.Design.C1GridView.MenuBar();
			this._btnOK = new System.Windows.Forms.Button();
			this._btnCancel = new System.Windows.Forms.Button();
			this._btnApply = new System.Windows.Forms.Button();
			this._btnHelp = new System.Windows.Forms.Button();
			this._panelSize = new System.Windows.Forms.Panel();
			this._darkBar = new System.Windows.Forms.Panel();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// _menuBar
			// 
			this._menuBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)));
			this._menuBar.BackColor = System.Drawing.SystemColors.Control;
			this._menuBar.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._menuBar.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
			this._menuBar.IntegralHeight = false;
			this._menuBar.Location = new System.Drawing.Point(11, 10);
			this._menuBar.Name = "_menuBar";
			this._menuBar.Size = new System.Drawing.Size(146, 491);
			this._menuBar.TabIndex = 0;
			this._menuBar.SelectedIndexChanged += new System.EventHandler(this._menuBar_SelectedIndexChanged);
			// 
			// _btnOK
			// 
			this._btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnOK.Location = new System.Drawing.Point(293, 475);
			this._btnOK.Name = "_btnOK";
			this._btnOK.Size = new System.Drawing.Size(80, 28);
			this._btnOK.TabIndex = 2;
			this._btnOK.Text = "OK";
			this.toolTip1.SetToolTip(this._btnOK, "OK");
			this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
			// 
			// _btnCancel
			// 
			this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._btnCancel.Location = new System.Drawing.Point(388, 475);
			this._btnCancel.Name = "_btnCancel";
			this._btnCancel.Size = new System.Drawing.Size(80, 28);
			this._btnCancel.TabIndex = 2;
			this._btnCancel.Text = "Cancel";
			this.toolTip1.SetToolTip(this._btnCancel, "Cancel");
			this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
			// 
			// _btnApply
			// 
			this._btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnApply.Enabled = false;
			this._btnApply.Location = new System.Drawing.Point(507, 475);
			this._btnApply.Name = "_btnApply";
			this._btnApply.Size = new System.Drawing.Size(80, 28);
			this._btnApply.TabIndex = 2;
			this._btnApply.Text = "Apply";
			this.toolTip1.SetToolTip(this._btnApply, "ApplyToolTip");
			this._btnApply.Click += new System.EventHandler(this._btnApply_Click);
			// 
			// _btnHelp
			// 
			this._btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btnHelp.Location = new System.Drawing.Point(602, 475);
			this._btnHelp.Name = "_btnHelp";
			this._btnHelp.Size = new System.Drawing.Size(80, 28);
			this._btnHelp.TabIndex = 2;
			this._btnHelp.Text = "Help";
			this.toolTip1.SetToolTip(this._btnHelp, "HelpToolTip");
			this._btnHelp.Click += new System.EventHandler(this._btnHelp_Click);
			// 
			// _panelSize
			// 
			this._panelSize.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._panelSize.Location = new System.Drawing.Point(168, 19);
			this._panelSize.Name = "_panelSize";
			this._panelSize.Size = new System.Drawing.Size(520, 445);
			this._panelSize.TabIndex = 3;
			this._panelSize.Visible = false;
			// 
			// _darkBar
			// 
			this._darkBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._darkBar.BackColor = System.Drawing.SystemColors.ControlDark;
			this._darkBar.Location = new System.Drawing.Point(168, 10);
			this._darkBar.Name = "_darkBar";
			this._darkBar.Size = new System.Drawing.Size(514, 5);
			this._darkBar.TabIndex = 1;
			// 
			// PropertyBuilderDialog
			// 
			this.AcceptButton = this._btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(7, 17);
			this.CancelButton = this._btnCancel;
			this.ClientSize = new System.Drawing.Size(694, 512);
			this.Controls.Add(this._darkBar);
			this.Controls.Add(this._panelSize);
			this.Controls.Add(this._btnOK);
			this.Controls.Add(this._menuBar);
			this.Controls.Add(this._btnCancel);
			this.Controls.Add(this._btnApply);
			this.Controls.Add(this._btnHelp);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PropertyBuilderDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Title";
			this.Load += new System.EventHandler(this.PropertyBuilderDialog_Load);
			this.ResumeLayout(false);

		}
		#endregion

		// fields
		private C1GridView _grid;
		private C1GridViewDesigner _designer;
		internal ArrayList _dirty;
		internal IPropertyPanel _currPanel;

		// store references we need
		internal void Initialize(C1GridViewDesigner designer)
		{
			_dirty = new ArrayList();
			_designer = designer;
			_grid = designer.Component as C1GridView;
		}

		internal C1GridViewDesigner Designer
		{
			get { return _designer; }
		}

		internal C1GridView Grid
		{
			get { return _grid; }
		}

		// called by panels
		internal void SetDirty(IPropertyPanel panel)
		{
			if (panel == null)
				// panel==null if called from a UITypeEditor in a property grid inside a panel.
				// The UITypeEditor code does not know its panel.
				panel = _currPanel;
			if (!_dirty.Contains(panel))
				_dirty.Add(panel);
			_btnApply.Enabled = true;
		}

		// apply all changes
		private void Apply()
		{
			if (_dirty.Count > 0)
			{
				// apply settings for each page
				foreach (MenuItem mi in _menuBar.Items)
					if (_dirty.Contains(mi.Panel))
						mi.Panel.Apply();

				_designer.RaiseComponentChanges();
			}

			// done for now
			_dirty.Clear();
			_btnApply.Enabled = false;
		}

		// initialize menu bar
		private void PropertyBuilderDialog_Load(object sender, System.EventArgs e)
		{
			// initialize menu bar
			AddPanel(C1Localizer.GetString("C1GridView.PropertyBuilderDialog.GeneralTab"), C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewGeneralTab, _panels[0]);
			AddPanel(C1Localizer.GetString("C1GridView.PropertyBuilderDialog.ColumnsTab"), C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColumnsTab, _panels[1]);
			AddPanel(C1Localizer.GetString("C1GridView.PropertyBuilderDialog.PagingTab"), C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewPagingTab, _panels[2]);
			AddPanel(C1Localizer.GetString("C1GridView.PropertyBuilderDialog.FormatTab"), C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewFormatTab, _panels[3]);
			AddPanel(C1Localizer.GetString("C1GridView.PropertyBuilderDialog.GroupingTab"), C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewGroupingTab, _panels[4]);

			// select first panel
			_menuBar.SelectedIndex = 0;
		}

		private void AddPanel(string text, Image image, IPropertyPanel panel)
		{
			// add menu option
			_menuBar.Items.Add(new MenuItem(text, image, panel));

			// add panel
			Control ctl = panel as Control;
			ctl.Visible = false;
			ctl.Bounds = _panelSize.Bounds;
			Controls.Add(ctl);

			// initialize panel
			if (panel == null) return;
			panel.Initialize(this);
		}

		private void _menuBar_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// apply pending changes before switching pages
			// (otherwise the info cached in each page may become outdated)
			Apply();

			// show selected page
			_currPanel = null;
			for (int i = 0; i < _menuBar.Items.Count; i++)
			{
				MenuItem mi = (MenuItem)_menuBar.Items[i];
				bool show = mi.Equals(_menuBar.SelectedItem);
				mi.ShowPanel(show);
				if (show)
					_currPanel = mi.Panel;
			}
		}

		private void _btnApply_Click(object sender, System.EventArgs e)
		{
			Apply();
		}

		private void _btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void _btnOK_Click(object sender, System.EventArgs e)
		{
			_btnApply_Click(sender, e);
			Close();
		}

		// utilities
		internal static string ColorToString(Color color)
		{
			if (color.IsEmpty)
				return "Not Set";

			if (color.IsNamedColor)
				return color.Name;

			return string.Format("#{0:x2}{1:x2}{2:x2}", color.R, color.G, color.B);
		}
		internal static Color ColorFromString(string text)
		{
			//TypeConverter tc = TypeDescriptor.GetConverter(typeof(Color));
			TypeConverter tc = new System.Web.UI.WebControls.WebColorConverter();
			try
			{
				return (Color)tc.ConvertFrom(text);
			}
			catch { }

			return Color.Empty;
		}

		private void _btnHelp_Click(object sender, System.EventArgs e)
		{
			Help.ShowHelpIndex(this._btnHelp, C1Localizer.GetString("C1GridView.PropertyBuilderDialog.HelpUrl"));
			// Help.ShowHelpIndex(this._btnHelp, "ms-help://C1.Web.UI.C1GridView/C1.Web.UI.C1GridView/usingthepropertybuilder.htm");
		}

		private void Localize()
		{
			this._btnOK.Text = C1Localizer.GetString("Base.OK");
			this.toolTip1.SetToolTip(this._btnOK, C1Localizer.GetString("Base.OK"));
			this._btnCancel.Text = C1Localizer.GetString("Base.Cancel");
			this.toolTip1.SetToolTip(this._btnCancel, C1Localizer.GetString("Base.Cancel"));
			this._btnApply.Text = C1Localizer.GetString("C1GridView.PropertyBuilderDialog.Apply");
			this.toolTip1.SetToolTip(this._btnApply, C1Localizer.GetString("C1GridView.PropertyBuilderDialog.ApplyToolTip"));
			this._btnHelp.Text = C1Localizer.GetString("C1GridView.PropertyBuilderDialog.Help");
			this.toolTip1.SetToolTip(this._btnHelp, C1Localizer.GetString("C1GridView.PropertyBuilderDialog.HelpToolTip"));
			this.Text = C1Localizer.GetString("C1GridView.PropertyBuilderDialog.Title");
		}
		/*
		private static bool _isVs2008 = false;
		private static bool IsVS2008()
		{
			if (!_isVs2008)
			{
				string fileName = AppDomain.CurrentDomain.BaseDirectory + "devenv.exe";

				try
				{
					System.Diagnostics.FileVersionInfo fversinfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(fileName);

					if (fversinfo.FileVersion.StartsWith("9"))
					{
						_isVs2008 = true;
					}
				}
				catch
				{
				}
			}

			return _isVs2008;
		}*/
	}

	//-----------------------------------------------------------------------------
	// helper classes
	//-----------------------------------------------------------------------------

	/// <summary>
	/// MenuBar is an owner-draw listbox
	/// </summary>
	internal class MenuBar : ListBox
	{
		public MenuBar()
		{
			BorderStyle = BorderStyle.None;
			BackColor = SystemColors.Control;
			DrawMode = DrawMode.OwnerDrawVariable;
		}

		// draw items with image, text, border
		override protected void OnDrawItem(DrawItemEventArgs e)
		{
			if (e.Index < 0 || e.Index >= Items.Count) return;
			MenuItem item = Items[e.Index] as MenuItem;
			if (item != null)
				item.DrawItem(this, e);
		}
		override protected void OnMeasureItem(MeasureItemEventArgs e)
		{
			e.ItemHeight = (int)e.Graphics.MeasureString("X", Font).Height + 8;
			e.ItemWidth = ClientRectangle.Width;
		}

		// give it a static edge
		private const int WS_EX_STATICEDGE = 0x00020000;
		override protected CreateParams CreateParams
		{
			get
			{
				CreateParams cp = base.CreateParams;
				cp.ExStyle |= WS_EX_STATICEDGE;
				return cp;
			}
		}
	}

	/// <summary>
	/// MenuItem contains text, an image, and a property panel
	/// </summary>
	internal class MenuItem
	{
		private string _text;
		private Image _img;
		private IPropertyPanel _panel;

		private static SolidBrush _selBrush;
		private static StringFormat _sf;

		internal MenuItem(string text, Image image, IPropertyPanel panel)
		{
			_text = text;
			_panel = panel;
			_img = image;

			if (_sf == null)
			{
				_sf = new StringFormat();
				_sf.LineAlignment = StringAlignment.Center;
			}
			if (_selBrush == null)
			{
				Color clr = Color.FromArgb(40, Color.White);
				_selBrush = new SolidBrush(clr);
			}
		}
		internal IPropertyPanel Panel
		{
			get { return _panel; }
		}
		internal void ShowPanel(bool show)
		{
			Control ctl = _panel as Control;
			if (ctl != null)
				ctl.Visible = show;
		}
		internal void DrawItem(MenuBar owner, DrawItemEventArgs e)
		{
			Graphics g = e.Graphics;
			bool selected = (e.State & DrawItemState.Selected) != 0;
			bool hotlight = (e.State & DrawItemState.HotLight) != 0;
			bool focus = (e.State & DrawItemState.Focus) != 0;

			// draw backgound
			Brush brush = (selected) ? _selBrush : SystemBrushes.Control;
			g.FillRectangle(brush, e.Bounds);

			// draw image
			Rectangle rc = e.Bounds;
			rc.Width = _img.Width;
			rc.Height = _img.Height;
			rc.Offset(2, (e.Bounds.Height - rc.Height) / 2);
			e.Graphics.DrawImage(_img, rc);

			// draw text
			rc.X = rc.Right + 2;
			rc.Width = e.Bounds.Width - rc.X;
			e.Graphics.DrawString(_text, owner.Font, SystemBrushes.ControlText, rc, _sf);

			// draw border
			if ((selected && focus) || hotlight)
				ControlPaint.DrawBorder3D(g, e.Bounds, Border3DStyle.RaisedInner);
		}
	}
}