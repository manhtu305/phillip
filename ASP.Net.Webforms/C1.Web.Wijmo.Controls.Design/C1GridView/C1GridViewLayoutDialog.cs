﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	public partial class C1GridViewLayoutDialog : Form
	{
		public C1GridViewLayoutDialog()
		{
			InitializeComponent();

			this.Tag = LayoutType.All;

			this.cbAccessibility.Tag = LayoutType.Accessibility;
			this.cbAppearance.Tag = LayoutType.Appearance;
			this.cbBehavior.Tag = LayoutType.Behavior;
			this.cbMisc.Tag = LayoutType.Misc;
			this.cbSizes.Tag = LayoutType.Sizes;
			this.cbStyles.Tag = LayoutType.Styles;

			Localize();
		}

		private void Localize()
		{
			this.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Title");

			this.btnOK.Text = C1Localizer.GetString("Base.OK");
			this.btnCancel.Text = C1Localizer.GetString("Base.Cancel");

			this.rbtnAll.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.All"); ;
			this.rbtnCustom.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Custom");

			this.cbAccessibility.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Accessibility");
			this.cbAppearance.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Appearance");
			this.cbBehavior.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Behavior");
			this.cbMisc.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Misc");
			this.cbSizes.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Sizes");
			this.cbStyles.Text = C1Localizer.GetString("C1GridView.LoadLayoutDialog.Styles");
		}

		private void SwitchFlag(bool flag, LayoutType layout)
		{
			LayoutType foo = ((LayoutType)this.Tag);

			if (flag)
			{
				this.Tag = foo |= layout;
			}
			else
			{
				this.Tag = foo &= ~layout;
			}
		}

		private void SetValues(bool flag)
		{
			panel1.Enabled = flag;

			SwitchFlag(true, LayoutType.All | LayoutType.Accessibility);

			cbAccessibility.Checked = true;
			cbAppearance.Checked = true;
			cbBehavior.Checked = true;
			cbMisc.Checked = true;
			cbSizes.Checked = true;
			cbStyles.Checked = true;
		}

		private void rbtnAll_Click(object sender, EventArgs e)
		{
			SetValues(!rbtnAll.Checked);
		}

		private void rbtnCustom_Click(object sender, EventArgs e)
		{
			SetValues(rbtnCustom.Checked);
		}

		private void checkBox_Click(object sender, EventArgs e)
		{
			CheckBox cb = (CheckBox)sender;
			SwitchFlag(cb.Checked, (LayoutType)cb.Tag);
		}
	}
}