#define NOTABLESTYLE

//----------------------------------------------------------------------------
// C1\Web\C1WebGrid\Design\BordersPanel.cs
//
// Implements the 'Format' page on the PropertyBuilderDialog.
//
// Copyright (C) 2002 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status			Date			By						Comments
// Created			Aug 14, 2002	Bernardo				-
//----------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Text;
using System.Data;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using WF = System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;
	/// <summary>
	/// Summary description for FormatPanel.
	/// </summary>
	internal class FormatPanel : System.Windows.Forms.UserControl, IPropertyPanel
	{
		private PropertyGrid propertyGrid1;
		private System.Windows.Forms.TreeView treeColumns;
		private System.Windows.Forms.TreeView treeObjects;
		private ImageList imageList1;
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.Button btnResetAll;
		private GroupBox groupBox1;
		private GroupBox groupBox2;
		private GroupBox groupBox3;
		private ToolTip toolTip1;
		private IContainer components;

		public FormatPanel()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			Localize();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.treeColumns = new System.Windows.Forms.TreeView();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.treeObjects = new System.Windows.Forms.TreeView();
			this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnResetAll = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeColumns
			// 
			this.treeColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.treeColumns.HideSelection = false;
			this.treeColumns.ImageIndex = 0;
			this.treeColumns.ImageList = this.imageList1;
			this.treeColumns.Location = new System.Drawing.Point(10, 23);
			this.treeColumns.Name = "treeColumns";
			this.treeColumns.SelectedImageIndex = 0;
			this.treeColumns.Size = new System.Drawing.Size(263, 154);
			this.treeColumns.TabIndex = 10;
			this.treeColumns.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeObjects_AfterSelect);
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// treeObjects
			// 
			this.treeObjects.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.treeObjects.HideSelection = false;
			this.treeObjects.Location = new System.Drawing.Point(10, 23);
			this.treeObjects.Name = "treeObjects";
			this.treeObjects.Size = new System.Drawing.Size(191, 154);
			this.treeObjects.TabIndex = 9;
			this.treeObjects.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeObjects_AfterSelect);
			// 
			// propertyGrid1
			// 
			this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.propertyGrid1.HelpVisible = false;
			this.propertyGrid1.Location = new System.Drawing.Point(10, 23);
			this.propertyGrid1.Name = "propertyGrid1";
			this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this.propertyGrid1.Size = new System.Drawing.Size(490, 164);
			this.propertyGrid1.TabIndex = 11;
			this.propertyGrid1.ToolbarVisible = false;
			this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
			// 
			// btnReset
			// 
			this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnReset.Location = new System.Drawing.Point(256, 408);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(120, 28);
			this.btnReset.TabIndex = 12;
			this.btnReset.Text = "Reset";
			this.toolTip1.SetToolTip(this.btnReset, "ResetToolTip");
			this.btnReset.UseVisualStyleBackColor = true;
			this.btnReset.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnResetAll
			// 
			this.btnResetAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnResetAll.Location = new System.Drawing.Point(391, 408);
			this.btnResetAll.Name = "btnResetAll";
			this.btnResetAll.Size = new System.Drawing.Size(120, 28);
			this.btnResetAll.TabIndex = 22;
			this.btnResetAll.Text = "ResetAll";
			this.toolTip1.SetToolTip(this.btnResetAll, "ResetAllToolTip");
			this.btnResetAll.UseVisualStyleBackColor = true;
			this.btnResetAll.Click += new System.EventHandler(this.btnResetAll_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.treeObjects);
			this.groupBox1.Location = new System.Drawing.Point(4, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(211, 187);
			this.groupBox1.TabIndex = 23;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Objects";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.treeColumns);
			this.groupBox2.Location = new System.Drawing.Point(231, 4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(283, 187);
			this.groupBox2.TabIndex = 24;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Columns";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.propertyGrid1);
			this.groupBox3.Location = new System.Drawing.Point(4, 200);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(510, 198);
			this.groupBox3.TabIndex = 25;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "ColumnsProperties";
			// 
			// FormatPanel
			// 
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnResetAll);
			this.Controls.Add(this.btnReset);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "FormatPanel";
			this.Size = new System.Drawing.Size(520, 440);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		//----------------------------------------------------------------------
		// fields
		//----------------------------------------------------------------------
		private C1GridView _grid;
		private bool _initialized;
		private PropertyBuilderDialog _owner;

		//----------------------------------------------------------------------
		// IPropertyPanel implementation
		//----------------------------------------------------------------------
		void IPropertyPanel.Initialize(PropertyBuilderDialog owner)
		{
			imageList1.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColDBField);
			imageList1.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColBand);

			// store references
			_owner = owner;
			_grid = owner.Grid;

			propertyGrid1.Site = _grid.Site;

			btnReset.LostFocus += new EventHandler(btnLostFocus);
			btnReset.GotFocus += new EventHandler(btnGotFocus);
			btnReset.KeyDown += new KeyEventHandler(btnKeyDown);

			btnResetAll.LostFocus += new EventHandler(btnLostFocus);
			btnResetAll.GotFocus += new EventHandler(btnGotFocus);
			btnResetAll.KeyDown += new KeyEventHandler(btnKeyDown);

			//jga - BUG 2569
			propertyGrid1.SelectedObjectsChanged += new EventHandler(propertyGrid1_SelectedObjectsChanged);
			// done initializing
			_initialized = true;
		}

		void propertyGrid1_SelectedObjectsChanged(object sender, EventArgs e)
		{
			// hack for issue: 3569
			// Seems if the selected object is not sited the propertygrid will set the selectedobject to null
			// this detects that situation and resets it back to what it should be.
			if (this.propertyGrid1.SelectedObject == null && _activeSelectedObject != null)
			{
				propertyGrid1.SelectedObject = _activeSelectedObject;
				propertyGrid1.Invalidate();
				propertyGrid1.Update();
			}
		}

		void IPropertyPanel.Apply()
		{
			ApplyNodes(treeColumns.Nodes);
			ApplyNodes(treeObjects.Nodes);

		}
		private void ApplyNodes(WF.TreeNodeCollection nodes)
		{
			foreach (WF.TreeNode node in nodes)
			{
				// apply children first (important for columns, because the width
				// is set by the parent node and we don't want to override it)
				if (node.Nodes.Count > 0)
					ApplyNodes(node.Nodes);

				StyleCache cache = node.Tag as StyleCache;
				if (cache != null)
					cache.Apply();
			}
		}

		private void ResetNodes(WF.TreeNodeCollection nodes)
		{
			foreach (WF.TreeNode node in nodes)
			{
				if (node.Nodes.Count > 0)
					ResetNodes(node.Nodes);

				StyleCache cache = node.Tag as StyleCache;
				if (cache != null)
					cache.Reset();
			}
		}


		//----------------------------------------------------------------------
		// custom implementation
		//----------------------------------------------------------------------
		// update lists when showing panel
		// (depends on settings made on a different panel)
		override protected void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (!Visible || _owner == null)
				return;

			// re-initialize the page
			_initialized = false;
			treeObjects.Nodes.Clear();
			treeColumns.Nodes.Clear();

			// treeObjects - main items
			AddNode("C1GridView", _grid, false, null, treeObjects);

			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Header"), _grid.HeaderStyle, false, null, treeObjects);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Footer"), _grid.FooterStyle, false, null, treeObjects);
			//AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Pager"), _grid.PagerStyle, null, treeObjects);

			// treeObjects - special items
			WF.TreeNode node = AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Rows"), null, false, null, treeObjects);

			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.NormalRows"), _grid.RowStyle, false, node, treeObjects);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.AlternatingRows"), _grid.AlternatingRowStyle, false, node, treeObjects);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.SelectedRows"), _grid.SelectedRowStyle, false, node, treeObjects);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.EditModeRows"), _grid.EditRowStyle, false, node, treeObjects);

			node.Expand();

			//treeColumns
			if (_grid.Columns.Count > 0)
			{
				AddColumns(_grid.Columns, null);
				for (int i = 0; i < treeColumns.Nodes.Count; i++)
					if (treeColumns.Nodes[i].Nodes.Count > 0)
						treeColumns.Nodes[i].Expand();

				treeColumns.SelectedNode = treeColumns.Nodes[0];
				treeColumns.SelectedNode = null;
			}

			_initialized = true;

			treeObjects.SelectedNode = treeObjects.Nodes[0];
		}

		private void AddColumns(C1BaseFieldCollection columns, WF.TreeNode addTo)
		{
			for (int i = 0; i < columns.Count; i++)
			{
				WF.TreeNode node = AddColumn(columns[i], addTo);
				C1Band band = columns[i] as C1Band;
				if (band != null && band.Columns.Count > 0)
					AddColumns(band.Columns, AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Columns"), null, false, node, treeColumns));
			}
		}

		private WF.TreeNode AddColumn(C1BaseField column, WF.TreeNode addTo)
		{
			//WF.TreeNode node = AddNode(column.ToString(), column, addTo, treeColumns);
			WF.TreeNode node = AddNode(column.Moniker(), column, false, addTo, treeColumns);
			if (column is C1Band)
			{
				node.ImageIndex = 1;
				node.SelectedImageIndex = 1;
			}

			// add usual styles
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Header"), column.HeaderStyle, false, node, treeColumns);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Footer"), column.FooterStyle, false, node, treeColumns);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Item"), column.ItemStyle, false, node, treeColumns);
			AddNode(C1Localizer.GetString("C1GridView.FormatPanel.Controls"), column.ControlStyle, true, node, treeColumns);

			// add group header/footer styles
			C1Field c1c = column as C1Field;
			if (c1c != null && c1c.GroupInfo.Position != GroupPosition.None)
			{
				//AddNode(C1Localizer.GetString("C1GridView.FormatPanel.GroupHeader"), c1c.GroupInfo.HeaderStyle, node, treeColumns);
				//AddNode(C1Localizer.GetString("C1GridView.FormatPanel.GroupFooter"), c1c.GroupInfo.FooterStyle, node, treeColumns);
			}

			C1Band band = column as C1Band;
			if (band != null)
			{
				//AddNode(C1Localizer.GetString("C1GridView.FormatPanel.ChildHeader"), band.ChildHeaderStyle, node, treeColumns);
				//AddNode(C1Localizer.GetString("C1GridView.FormatPanel.ChildFooter"), band.ChildFooterStyle, node, treeColumns);
				//AddNode(C1Localizer.GetString("C1GridView.FormatPanel.ChildItem"), band.ChildItemStyle, node, treeColumns);
			}

			return node;
		}

		private WF.TreeNode AddNode(string text, object style, bool allowSizeProperties, WF.TreeNode parent, WF.TreeView tree)
		{
			StyleCache styleCache = null;
			if (style != null)
			{
				styleCache = new StyleCache(style, allowSizeProperties);
			}

			WF.TreeNode node = new WF.TreeNode(text);
			node.Tag = styleCache;

			WF.TreeNodeCollection nodes = (parent != null) ? parent.Nodes : tree.Nodes;
			nodes.Add(node);

			return node;
		}


		private void _setDirty(object sender, System.EventArgs e)
		{
			if (!_initialized) return;
			_owner.SetDirty(this);

			propertyGrid1.Refresh();
			propertyGrid1.Invalidate();
			propertyGrid1.Update();

		}

		object _activeSelectedObject = null;

		private void treeObjects_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (!_initialized)
				return;

			WF.TreeView tree = (WF.TreeView)sender;

			if (tree == treeColumns)
				treeObjects.SelectedNode = null;
			else
				treeColumns.SelectedNode = null;

			if (tree.SelectedNode != null && tree.SelectedNode.Tag != null)
			{
				StyleCache cache = (StyleCache)tree.SelectedNode.Tag;
				propertyGrid1.SelectedObject = cache._cache;
				btnReset.Enabled = true;
				_activeSelectedObject = propertyGrid1.SelectedObject;
			}
			else
			{
				propertyGrid1.SelectedObject = null;
				btnReset.Enabled = false;
			}
		}

		private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			if (_initialized)
				_owner.SetDirty(this);

			propertyGrid1.Refresh();
			propertyGrid1.Invalidate();
			propertyGrid1.Update();

		}

		private void button1_Click(object sender, EventArgs e)
		{
			WF.TreeNode node = treeObjects.SelectedNode;
			if (node == null)
				node = treeColumns.SelectedNode;

			if (node != null && node.Tag != null)
			{
				((StyleCache)node.Tag).Reset();

				propertyGrid1.Refresh();
				_setDirty(sender, e);
			}
		}

		private void btnKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				((System.Windows.Forms.Button)sender).PerformClick();
		}

		private void btnGotFocus(object sender, System.EventArgs e)
		{
			_owner.AcceptButton = null;
		}

		private void btnLostFocus(object sender, System.EventArgs e)
		{
			_owner.AcceptButton = _owner._btnOK;
		}

		private void btnResetAll_Click(object sender, EventArgs e)
		{
			ResetNodes(treeColumns.Nodes);
			ResetNodes(treeObjects.Nodes);

			propertyGrid1.Refresh();
			_setDirty(sender, e);
		}

		private void Localize()
		{
			this.btnReset.Text = C1Localizer.GetString("C1GridView.FormatPanel.Reset");
			this.toolTip1.SetToolTip(this.btnReset, C1Localizer.GetString("C1GridView.FormatPanel.ResetToolTip"));
			this.btnResetAll.Text = C1Localizer.GetString("C1GridView.FormatPanel.ResetAll");
			this.toolTip1.SetToolTip(this.btnResetAll, C1Localizer.GetString("C1GridView.FormatPanel.ResetAllToolTip"));
			this.groupBox1.Text = C1Localizer.GetString("C1GridView.FormatPanel.Objects");
			this.groupBox2.Text = C1Localizer.GetString("C1GridView.FormatPanel.Columns");
			this.groupBox3.Text = C1Localizer.GetString("C1GridView.FormatPanel.ColumnsProperties");
		}
	}


	/// <summary>
	/// StyleCache
	/// contains a style to be edited and a reference to the original
	/// TableItemStyle (or C1GridView object).
	/// </summary>
	internal class StyleCache
	{
#if NOTABLESTYLE
		private class GridStyle : Style
#else
		private class GridStyle : TableStyle
#endif
		{
			private class _Editor : ImageUrlEditor
			{
				public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
				{
					C1GridView grid = ((GridStyle)context.Instance)._owner;
					IDesignerHost host = (IDesignerHost)grid.Site.GetService(typeof(IDesignerHost));
					return base.EditValue(context, (IServiceProvider)host.GetDesigner(grid), value);
				}
			}

			private C1GridView _owner = null;

			public GridStyle(C1GridView owner)
				: base()
			{
				_owner = owner;
			}

#if NOTABLESTYLE
#else
			[Editor(typeof(GridStyle._Editor), typeof(System.Drawing.Design.UITypeEditor))]
			public override string BackImageUrl
			{
				get
				{
					return base.BackImageUrl;
				}
				set
				{
					base.BackImageUrl = value;
				}
			}

			[DefaultValue(0)]
			public override int CellSpacing
			{
				get
				{
					return base.CellSpacing;
				}
				set
				{
					base.CellSpacing = value;
				}
			}

			[DefaultValue(GridLines.None)]
			public override GridLines GridLines
			{
				get
				{
					return base.GridLines;
				}
				set
				{
					base.GridLines = value;
				}
			}
#endif
		}

		[TypeConverter(typeof(WithoutSizesStyle._Converter))]
		private class WithoutSizesStyle : TableItemStyle
		{
			private class _Converter : TypeConverter
			{
				public override bool GetPropertiesSupported(ITypeDescriptorContext context)
				{
					return true;
				}

				public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
				{
					PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(value, attributes, true);
					System.Collections.Generic.List<PropertyDescriptor> filteredProps = new System.Collections.Generic.List<PropertyDescriptor>();

					int cnt = pdc.Count;
					for (int i = 0; i < cnt; i++)
					{
						PropertyDescriptor pd = pdc[i];
						if (pd.Name != "Width" && pd.Name != "Height")
							filteredProps.Add(pd);
					}

					return new PropertyDescriptorCollection(filteredProps.ToArray(), true);
				}
			}
		}

		[TypeConverter(typeof(EmptyStyle._Converter))]
		private class EmptyStyle : Style
		{
			private class _Converter : TypeConverter
			{
				public override bool GetPropertiesSupported(ITypeDescriptorContext context)
				{
					return true;
				}

				public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
				{
					return new PropertyDescriptorCollection(new PropertyDescriptor[0], true);
				}
			}

			public EmptyStyle(C1BaseField owner)
				: base()
			{
				//Width = ((IOwnerable)owner).Owner.DefaultColumnWidth;
			}
		}

		internal object _main; // grid style or grid itself
		internal Style _cache = null; // style we're editing

		internal StyleCache(object main, bool allowSizeProperties)
		{
			_main = main;

			// copy regular style
			TableItemStyle style = _main as TableItemStyle;
			if (style != null)
			{
				_cache = allowSizeProperties
					? new TableItemStyle()
					: new WithoutSizesStyle();

				_cache.CopyFrom(style);
			}
			else
			{
				//create style for column
				C1BaseField column = main as C1BaseField;
				if (column != null)
				{
					_cache = new EmptyStyle(column);
					//if (!column.ItemStyle.Width.IsEmpty)
					//	_cache.Width = column.ItemStyle.Width;
				}
				else
				{
					// create style from grid
					C1GridView grid = _main as C1GridView;
					if (grid != null)
					{
						_cache = new GridStyle(grid);
						_cache.CopyFrom(grid.ControlStyle);
					}
				}
			}

			// shouldn't get here
			if (_cache == null)
				throw new Exception(C1Localizer.GetString("C1GridView.FormatPanel.Exception.StyleCache"));
		}

		internal void Apply()
		{
			// apply regular style
			TableItemStyle style = _main as TableItemStyle;
			if (style != null)
			{
				style.Reset();
				style.CopyFrom(_cache);
			}
			else
			{
				// apply column style
				C1BaseField col = _main as C1BaseField;
				if (col != null)
				{
					/*
					if (((IOwnerable)col).Owner.DefaultColumnWidth.Equals(_cache.Width))
						col.ItemStyle.Width = Unit.Empty;

					if (!((IOwnerable)col).Owner.DefaultColumnWidth.Equals(_cache.Width))
						col.ItemStyle.Width = _cache.Width;
					*/
				}
				else
				{    // apply grid style
					C1GridView grid = _main as C1GridView;
					if (grid != null)
					{
						grid.ControlStyle.Reset();
						grid.ControlStyle.CopyFrom(_cache);
					}
				}
			}
		}

		internal void Reset()
		{
			_cache.Reset();

			C1GridView grid = _main as C1GridView;
			if (grid != null)
			{
#if NOTABLESTYLE
#else
				((TableStyle)_cache).CellSpacing = 0;
				((TableStyle)_cache).GridLines = GridLines.None;
#endif
			}
			else
			{
				C1BaseField column = _main as C1BaseField;
				if (column != null)
				{
					//_cache.Width = ((IOwnerable)column).Owner.DefaultColumnWidth;
				}
			}
		}
	}
}