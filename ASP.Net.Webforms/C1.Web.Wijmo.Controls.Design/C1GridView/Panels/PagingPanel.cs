//----------------------------------------------------------------------------
// C1\Web\C1WebGrid\Design\BordersPanel.cs
//
// Implements the 'Paging' page on the PropertyBuilderDialog.
//
// Copyright (C) 2002 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status			Date			By						Comments
// Created			Aug 14, 2002	Bernardo				-
//----------------------------------------------------------------------------
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using WebControls = System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;
	/// <summary>
	/// Summary C1Description for PagingPanel.
	/// </summary>
	internal class PagingPanel : System.Windows.Forms.UserControl, IPropertyPanel
	{
		private static class ImageUrlBuilder
		{
			public static string BuildUrl(IServiceProvider provider, string initialValue)
			{
				System.Web.UI.Design.ImageUrlEditor editor = new System.Web.UI.Design.ImageUrlEditor();
				return (string)editor.EditValue(provider, initialValue);
			}
		}

		private System.Windows.Forms.CheckBox _cmbPaging;
		private System.Windows.Forms.CheckBox _cmbCustomPaging;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox _cmbPosition;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox _cmbMode;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox _txtPageSize;
		private System.Windows.Forms.TextBox _txtNextPage;
		private System.Windows.Forms.TextBox _txtPrevPage;
		private System.Windows.Forms.TextBox _txtButtonCount;
		private string _lastValidPageSize = string.Empty;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _txtFirstPage;
		private System.Windows.Forms.TextBox _txtLastPage;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox _txtFirstPageUrl;
		private System.Windows.Forms.Label label10;
		//private System.Windows.Forms.Button _btnFirstPageUrl;
		//private System.Windows.Forms.Button _btnLastPageUrl;
		private System.Windows.Forms.TextBox _txtLastPageUrl;
		private System.Windows.Forms.Label label11;
		//private System.Windows.Forms.Button _btnPrevImageUrl;
		private System.Windows.Forms.TextBox _txtPrevImageUrl;
		private System.Windows.Forms.Label label12;
		//private System.Windows.Forms.Button _btnNextImageUrl;
		private System.Windows.Forms.TextBox _txtNextPageUrl;
		private System.Windows.Forms.Label label13;
		private GroupBox _gbPaging;
		private System.Windows.Forms.Label label3;
		private GroupBox _gbNavigation;
		private System.Windows.Forms.CheckBox _cmbShowNavigation;
		private System.Windows.Forms.Panel _pnlNavigtion;
		private ToolTip toolTip1;
		private IContainer components;

		public PagingPanel()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			Localize();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._cmbPaging = new System.Windows.Forms.CheckBox();
			this._cmbCustomPaging = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this._txtPageSize = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this._cmbPosition = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this._cmbMode = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this._txtNextPage = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this._txtPrevPage = new System.Windows.Forms.TextBox();
			this._txtButtonCount = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this._txtFirstPage = new System.Windows.Forms.TextBox();
			this._txtLastPage = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this._txtFirstPageUrl = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			//this._btnFirstPageUrl = new System.Windows.Forms.Button();
			//this._btnLastPageUrl = new System.Windows.Forms.Button();
			this._txtLastPageUrl = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			//this._btnPrevImageUrl = new System.Windows.Forms.Button();
			this._txtPrevImageUrl = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			//this._btnNextImageUrl = new System.Windows.Forms.Button();
			this._txtNextPageUrl = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this._gbPaging = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this._gbNavigation = new System.Windows.Forms.GroupBox();
			this._pnlNavigtion = new System.Windows.Forms.Panel();
			this._cmbShowNavigation = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this._gbPaging.SuspendLayout();
			this._gbNavigation.SuspendLayout();
			this._pnlNavigtion.SuspendLayout();
			this.SuspendLayout();
			// 
			// _cmbPaging
			// 
			this._cmbPaging.AutoSize = true;
			this._cmbPaging.Location = new System.Drawing.Point(6, 23);
			this._cmbPaging.Name = "_cmbPaging";
			this._cmbPaging.Size = new System.Drawing.Size(98, 21);
			this._cmbPaging.TabIndex = 11;
			this._cmbPaging.Text = "AllowPaging";
			this._cmbPaging.CheckedChanged += new System.EventHandler(this._setDirty);
			// 
			// _cmbCustomPaging
			// 
			this._cmbCustomPaging.AutoSize = true;
			this._cmbCustomPaging.Location = new System.Drawing.Point(199, 23);
			this._cmbCustomPaging.Name = "_cmbCustomPaging";
			this._cmbCustomPaging.Size = new System.Drawing.Size(146, 21);
			this._cmbCustomPaging.TabIndex = 12;
			this._cmbCustomPaging.Text = "AllowCustomPaging";
			this._cmbCustomPaging.CheckedChanged += new System.EventHandler(this._setDirty);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 17);
			this.label2.TabIndex = 4;
			this.label2.Text = "PageSize";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// _txtPageSize
			// 
			this._txtPageSize.Location = new System.Drawing.Point(199, 53);
			this._txtPageSize.Name = "_txtPageSize";
			this._txtPageSize.Size = new System.Drawing.Size(40, 24);
			this._txtPageSize.TabIndex = 13;
			this._txtPageSize.Text = "10";
			this._txtPageSize.TextChanged += new System.EventHandler(this._txtPageSize_TextChanged);
			this._txtPageSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._txtNum_KeyPress);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 10);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(55, 17);
			this.label5.TabIndex = 10;
			this.label5.Text = "Position";
			// 
			// _cmbPosition
			// 
			this._cmbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this._cmbPosition.Location = new System.Drawing.Point(6, 30);
			this._cmbPosition.Name = "_cmbPosition";
			this._cmbPosition.Size = new System.Drawing.Size(168, 25);
			this._cmbPosition.TabIndex = 17;
			this._cmbPosition.SelectedIndexChanged += new System.EventHandler(this._setDirty);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(195, 10);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(41, 17);
			this.label6.TabIndex = 12;
			this.label6.Text = "Mode";
			// 
			// _cmbMode
			// 
			this._cmbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this._cmbMode.Location = new System.Drawing.Point(196, 30);
			this._cmbMode.Name = "_cmbMode";
			this._cmbMode.Size = new System.Drawing.Size(168, 25);
			this._cmbMode.TabIndex = 18;
			this._cmbMode.SelectedIndexChanged += new System.EventHandler(this._setDirty);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 178);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(95, 17);
			this.label7.TabIndex = 14;
			this.label7.Text = "NextPageText";
			// 
			// _txtNextPage
			// 
			this._txtNextPage.Location = new System.Drawing.Point(6, 198);
			this._txtNextPage.Name = "_txtNextPage";
			this._txtNextPage.Size = new System.Drawing.Size(168, 24);
			this._txtNextPage.TabIndex = 26;
			this._txtNextPage.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 234);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(118, 17);
			this.label8.TabIndex = 16;
			this.label8.Text = "PreviousPageText";
			// 
			// _txtPrevPage
			// 
			this._txtPrevPage.Location = new System.Drawing.Point(6, 254);
			this._txtPrevPage.Name = "_txtPrevPage";
			this._txtPrevPage.Size = new System.Drawing.Size(168, 24);
			this._txtPrevPage.TabIndex = 29;
			this._txtPrevPage.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// _txtButtonCount
			// 
			this._txtButtonCount.Location = new System.Drawing.Point(393, 31);
			this._txtButtonCount.Name = "_txtButtonCount";
			this._txtButtonCount.Size = new System.Drawing.Size(104, 24);
			this._txtButtonCount.TabIndex = 19;
			this._txtButtonCount.TextChanged += new System.EventHandler(this._setDirty);
			this._txtButtonCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._txtNum_KeyPress);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(390, 10);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(94, 17);
			this.label9.TabIndex = 18;
			this.label9.Text = "ButtonsCount";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 66);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(91, 17);
			this.label1.TabIndex = 22;
			this.label1.Text = "FirstPageText";
			// 
			// _txtFirstPage
			// 
			this._txtFirstPage.Location = new System.Drawing.Point(6, 86);
			this._txtFirstPage.Name = "_txtFirstPage";
			this._txtFirstPage.Size = new System.Drawing.Size(169, 24);
			this._txtFirstPage.TabIndex = 20;
			this._txtFirstPage.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// _txtLastPage
			// 
			this._txtLastPage.Location = new System.Drawing.Point(6, 142);
			this._txtLastPage.Name = "_txtLastPage";
			this._txtLastPage.Size = new System.Drawing.Size(169, 24);
			this._txtLastPage.TabIndex = 23;
			this._txtLastPage.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 122);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(91, 17);
			this.label4.TabIndex = 24;
			this.label4.Text = "LastPageText";
			// 
			// _txtFirstPageUrl
			// 
			this._txtFirstPageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._txtFirstPageUrl.Location = new System.Drawing.Point(196, 86);
			this._txtFirstPageUrl.Name = "_txtFirstPageUrl";
			this._txtFirstPageUrl.Size = new System.Drawing.Size(300, 24);
			this._txtFirstPageUrl.TabIndex = 21;
			this._txtFirstPageUrl.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(195, 66);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(126, 17);
			this.label10.TabIndex = 26;
			this.label10.Text = "FirstPageClass";
			// 
			// _btnFirstPageUrl
			// 
			/*
			this._btnFirstPageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnFirstPageUrl.Location = new System.Drawing.Point(463, 86);
			this._btnFirstPageUrl.Name = "_btnFirstPageUrl";
			this._btnFirstPageUrl.Size = new System.Drawing.Size(34, 24);
			this._btnFirstPageUrl.TabIndex = 22;
			this._btnFirstPageUrl.Text = "...";
			this.toolTip1.SetToolTip(this._btnFirstPageUrl, "BrowseURL");
			this._btnFirstPageUrl.UseVisualStyleBackColor = true;
			this._btnFirstPageUrl.Click += new System.EventHandler(this.btnImageUrl_Click);
			*/
			// 
			// _btnLastPageUrl
			// 
			/*
			this._btnLastPageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnLastPageUrl.Location = new System.Drawing.Point(463, 142);
			this._btnLastPageUrl.Name = "_btnLastPageUrl";
			this._btnLastPageUrl.Size = new System.Drawing.Size(34, 24);
			this._btnLastPageUrl.TabIndex = 25;
			this._btnLastPageUrl.Text = "...";
			this.toolTip1.SetToolTip(this._btnLastPageUrl, "BrowseURL");
			this._btnLastPageUrl.UseVisualStyleBackColor = true;
			this._btnLastPageUrl.Click += new System.EventHandler(this.btnImageUrl_Click);
			*/
			// 
			// _txtLastPageUrl
			// 
			this._txtLastPageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._txtLastPageUrl.Location = new System.Drawing.Point(196, 142);
			this._txtLastPageUrl.Name = "_txtLastPageUrl";
			this._txtLastPageUrl.Size = new System.Drawing.Size(300, 24);
			this._txtLastPageUrl.TabIndex = 24;
			this._txtLastPageUrl.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(195, 122);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(126, 17);
			this.label11.TabIndex = 29;
			this.label11.Text = "LastPageClass";
			// 
			// _btnPrevImageUrl
			// 
			/*
			this._btnPrevImageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnPrevImageUrl.Location = new System.Drawing.Point(463, 254);
			this._btnPrevImageUrl.Name = "_btnPrevImageUrl";
			this._btnPrevImageUrl.Size = new System.Drawing.Size(34, 24);
			this._btnPrevImageUrl.TabIndex = 31;
			this._btnPrevImageUrl.Text = "...";
			this.toolTip1.SetToolTip(this._btnPrevImageUrl, "BrowseURL");
			this._btnPrevImageUrl.UseVisualStyleBackColor = true;
			this._btnPrevImageUrl.Click += new System.EventHandler(this.btnImageUrl_Click);
			*/
			// 
			// _txtPrevImageUrl
			// 
			this._txtPrevImageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._txtPrevImageUrl.Location = new System.Drawing.Point(196, 254);
			this._txtPrevImageUrl.Name = "_txtPrevImageUrl";
			this._txtPrevImageUrl.Size = new System.Drawing.Size(300, 24);
			this._txtPrevImageUrl.TabIndex = 30;
			this._txtPrevImageUrl.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(195, 234);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(153, 17);
			this.label12.TabIndex = 35;
			this.label12.Text = "PreviousPageClass";
			// 
			// _btnNextImageUrl
			// 
			/*
			this._btnNextImageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnNextImageUrl.Location = new System.Drawing.Point(463, 198);
			this._btnNextImageUrl.Name = "_btnNextImageUrl";
			this._btnNextImageUrl.Size = new System.Drawing.Size(34, 24);
			this._btnNextImageUrl.TabIndex = 28;
			this._btnNextImageUrl.Text = "...";
			this.toolTip1.SetToolTip(this._btnNextImageUrl, "BrowseURL");
			this._btnNextImageUrl.UseVisualStyleBackColor = true;
			this._btnNextImageUrl.Click += new System.EventHandler(this.btnImageUrl_Click);
			*/
			// 
			// _txtNextPageUrl
			// 
			this._txtNextPageUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._txtNextPageUrl.Location = new System.Drawing.Point(196, 198);
			this._txtNextPageUrl.Name = "_txtNextPageUrl";
			this._txtNextPageUrl.Size = new System.Drawing.Size(300, 24);
			this._txtNextPageUrl.TabIndex = 27;
			this._txtNextPageUrl.TextChanged += new System.EventHandler(this._setDirty);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(195, 178);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(130, 17);
			this.label13.TabIndex = 32;
			this.label13.Text = "NextPageClass";
			// 
			// _gbPaging
			// 
			this._gbPaging.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._gbPaging.Controls.Add(this.label3);
			this._gbPaging.Controls.Add(this._cmbPaging);
			this._gbPaging.Controls.Add(this._cmbCustomPaging);
			this._gbPaging.Controls.Add(this.label2);
			this._gbPaging.Controls.Add(this._txtPageSize);
			this._gbPaging.Location = new System.Drawing.Point(4, 3);
			this._gbPaging.Name = "_gbPaging";
			this._gbPaging.Size = new System.Drawing.Size(510, 90);
			this._gbPaging.TabIndex = 10;
			this._gbPaging.TabStop = false;
			this._gbPaging.Text = "Paging";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(251, 56);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 17);
			this.label3.TabIndex = 6;
			this.label3.Text = "Rows";
			// 
			// _gbNavigation
			// 
			this._gbNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._gbNavigation.Controls.Add(this._pnlNavigtion);
			this._gbNavigation.Controls.Add(this._cmbShowNavigation);
			this._gbNavigation.Location = new System.Drawing.Point(4, 104);
			this._gbNavigation.Name = "_gbNavigation";
			this._gbNavigation.Size = new System.Drawing.Size(510, 332);
			this._gbNavigation.TabIndex = 14;
			this._gbNavigation.TabStop = false;
			this._gbNavigation.Text = "PageNavigation";
			// 
			// _pnlNavigtion
			// 
			this._pnlNavigtion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._pnlNavigtion.Controls.Add(this.label5);
			this._pnlNavigtion.Controls.Add(this._txtLastPage);
			this._pnlNavigtion.Controls.Add(this.label9);
			this._pnlNavigtion.Controls.Add(this.label4);
			//this._pnlNavigtion.Controls.Add(this._btnPrevImageUrl);
			this._pnlNavigtion.Controls.Add(this.label10);
			this._pnlNavigtion.Controls.Add(this._txtButtonCount);
			this._pnlNavigtion.Controls.Add(this._txtFirstPage);
			this._pnlNavigtion.Controls.Add(this._txtPrevImageUrl);
			this._pnlNavigtion.Controls.Add(this._txtFirstPageUrl);
			this._pnlNavigtion.Controls.Add(this._txtPrevPage);
			this._pnlNavigtion.Controls.Add(this.label1);
			this._pnlNavigtion.Controls.Add(this.label12);
			//this._pnlNavigtion.Controls.Add(this._btnFirstPageUrl);
			this._pnlNavigtion.Controls.Add(this.label8);
			this._pnlNavigtion.Controls.Add(this._txtNextPage);
			//this._pnlNavigtion.Controls.Add(this._btnNextImageUrl);
			this._pnlNavigtion.Controls.Add(this.label11);
			this._pnlNavigtion.Controls.Add(this.label7);
			this._pnlNavigtion.Controls.Add(this.label6);
			this._pnlNavigtion.Controls.Add(this._txtNextPageUrl);
			this._pnlNavigtion.Controls.Add(this._txtLastPageUrl);
			this._pnlNavigtion.Controls.Add(this._cmbPosition);
			this._pnlNavigtion.Controls.Add(this._cmbMode);
			this._pnlNavigtion.Controls.Add(this.label13);
			//this._pnlNavigtion.Controls.Add(this._btnLastPageUrl);
			this._pnlNavigtion.Location = new System.Drawing.Point(3, 43);
			this._pnlNavigtion.Name = "_pnlNavigtion";
			this._pnlNavigtion.Size = new System.Drawing.Size(504, 286);
			this._pnlNavigtion.TabIndex = 16;
			// 
			// _cmbShowNavigation
			// 
			this._cmbShowNavigation.AutoSize = true;
			this._cmbShowNavigation.Location = new System.Drawing.Point(6, 23);
			this._cmbShowNavigation.Name = "_cmbShowNavigation";
			this._cmbShowNavigation.Size = new System.Drawing.Size(125, 21);
			this._cmbShowNavigation.TabIndex = 15;
			this._cmbShowNavigation.Text = "ShowNavigation";
			this._cmbShowNavigation.UseVisualStyleBackColor = true;
			this._cmbShowNavigation.CheckedChanged += new System.EventHandler(this._cmbShowNavigation_CheckedChanged);
			// 
			// PagingPanel
			// 
			this.Controls.Add(this._gbNavigation);
			this.Controls.Add(this._gbPaging);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "PagingPanel";
			this.Size = new System.Drawing.Size(520, 440);
			this._gbPaging.ResumeLayout(false);
			this._gbPaging.PerformLayout();
			this._gbNavigation.ResumeLayout(false);
			this._gbNavigation.PerformLayout();
			this._pnlNavigtion.ResumeLayout(false);
			this._pnlNavigtion.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion

		//----------------------------------------------------------------------
		// fields
		//----------------------------------------------------------------------
		private C1GridView _grid;
		private bool _initialized;
		private PropertyBuilderDialog _owner;

		//----------------------------------------------------------------------
		// IPropertyPanel implementation
		//----------------------------------------------------------------------
		void IPropertyPanel.Initialize(PropertyBuilderDialog owner)
		{
			// store references
			_owner = owner;
			_grid = owner.Grid;

			// initialize controls
			foreach (object value in Enum.GetValues(typeof(PagerPosition)))
			{
				_cmbPosition.Items.Add(value);
			}

			foreach (object value in Enum.GetValues(typeof(C1.Web.Wijmo.Controls.C1Pager.PagerMode)))
			{
				_cmbMode.Items.Add(value);
			}

			_cmbPaging.Checked = _grid.AllowPaging;
			_cmbCustomPaging.Checked = _grid.AllowCustomPaging;
			_txtPageSize.Text = _grid.PageSize.ToString();
			_lastValidPageSize = this._txtPageSize.Text;

			PagerSettings settings = _grid.PagerSettings;
			_cmbShowNavigation.Checked = settings.Visible;

			_cmbMode.SelectedItem = settings.Mode;
			_cmbPosition.SelectedItem = settings.Position;

			_txtButtonCount.Text = settings.PageButtonCount.ToString();

			_txtFirstPage.Text = settings.FirstPageText;
			_txtFirstPageUrl.Text = settings.FirstPageClass;
			//_btnFirstPageUrl.Tag = _txtFirstPageUrl;

			_txtLastPage.Text = settings.LastPageText;
			_txtLastPageUrl.Text = settings.LastPageClass;
			//_btnLastPageUrl.Tag = _txtLastPageUrl;

			_txtNextPage.Text = settings.NextPageText;
			_txtNextPageUrl.Text = settings.NextPageClass;
			//_btnNextImageUrl.Tag = _txtNextPageUrl;

			_txtPrevPage.Text = settings.PreviousPageText;
			_txtPrevImageUrl.Text = settings.PreviousPageClass;
			//_btnPrevImageUrl.Tag = _txtPrevImageUrl;

			// done initializing
			_initialized = true;
			Enable();
		}

		void IPropertyPanel.Apply()
		{
			_grid.AllowPaging = _cmbPaging.Checked;
			_grid.AllowCustomPaging = _cmbCustomPaging.Checked;

			int tmp;
			if (int.TryParse(_txtPageSize.Text, out tmp))
			{
				_grid.PageSize = tmp;
			}
			else
			{
				_txtPageSize.Text = _grid.PageSize.ToString();
			}

			PagerSettings settings = _grid.PagerSettings;
			settings.Visible = _cmbShowNavigation.Checked;

			settings.Mode = (C1.Web.Wijmo.Controls.C1Pager.PagerMode)_cmbMode.SelectedItem;
			settings.Position = (PagerPosition)_cmbPosition.SelectedItem;

			if (int.TryParse(_txtButtonCount.Text, out tmp))
			{
				settings.PageButtonCount = tmp;
			}
			else
			{
				_txtButtonCount.Text = settings.PageButtonCount.ToString();
			}

			settings.FirstPageText = _txtFirstPage.Text;
			settings.FirstPageClass = _txtFirstPageUrl.Text;

			settings.LastPageText = _txtLastPage.Text;
			settings.LastPageClass = _txtLastPageUrl.Text;

			settings.NextPageText = _txtNextPage.Text;
			settings.NextPageClass = _txtNextPageUrl.Text;

			settings.PreviousPageText = _txtPrevPage.Text;
			settings.PreviousPageClass = _txtPrevImageUrl.Text;
		}

		//----------------------------------------------------------------------
		// custom implementation
		//----------------------------------------------------------------------
		private void _txtNum_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (!char.IsControl(e.KeyChar) && (e.KeyChar < '0' || e.KeyChar > '9'))
				e.Handled = true;
		}
		private void _setDirty(object sender, System.EventArgs e)
		{
			if (!_initialized) return;
			Enable();
			_owner.SetDirty(this);
		}
		private void _txtPageSize_TextChanged(object sender, System.EventArgs e)
		{
			if (!_initialized) return;
			if (_txtPageSize.Text.Length > 0)
			{
				try
				{
					int.Parse(_txtPageSize.Text);
				}
				catch
				{
					_txtPageSize.Text = this._lastValidPageSize;
					_txtPageSize.SelectAll();
					return;
				}
			}
			this._lastValidPageSize = this._txtPageSize.Text;
			_owner.SetDirty(this);
		}
		private void Enable()
		{
			_cmbCustomPaging.Enabled =
				_txtPageSize.Enabled =
				_gbNavigation.Enabled = _cmbPaging.Checked;

			if (_gbNavigation.Enabled)
			{
				_pnlNavigtion.Enabled = _cmbShowNavigation.Checked;

				if (_pnlNavigtion.Enabled)
				{
					bool enableNextPrev = false;
					bool enableFirstLast = false;
					bool enableButtonCount = false;

					C1.Web.Wijmo.Controls.C1Pager.PagerMode mode = (C1.Web.Wijmo.Controls.C1Pager.PagerMode)_cmbMode.SelectedItem;
					switch (mode)
					{
						case C1.Web.Wijmo.Controls.C1Pager.PagerMode.NextPrevious:
							enableNextPrev = true;
							enableFirstLast = false;
							enableButtonCount = false;
							break;

						case C1.Web.Wijmo.Controls.C1Pager.PagerMode.NextPreviousFirstLast:
							enableNextPrev = true;
							enableFirstLast = true;
							enableButtonCount = false;
							break;

						case C1.Web.Wijmo.Controls.C1Pager.PagerMode.Numeric:
							enableNextPrev = false;
							enableFirstLast = false;
							enableButtonCount = true;
							break;

						case C1.Web.Wijmo.Controls.C1Pager.PagerMode.NumericFirstLast:
							enableNextPrev = false;
							enableFirstLast = true;
							enableButtonCount = true;
							break;
					}

					_txtNextPage.Enabled = _txtNextPageUrl.Enabled = /*_btnNextImageUrl.Enabled =*/
						_txtPrevPage.Enabled = _txtPrevImageUrl.Enabled =
						/*_btnPrevImageUrl.Enabled = */enableNextPrev;

					_txtFirstPage.Enabled = _txtFirstPageUrl.Enabled = /*_btnFirstPageUrl.Enabled =*/
						_txtLastPage.Enabled = _txtLastPageUrl.Enabled =
						/*_btnLastPageUrl.Enabled = */enableFirstLast;

					_txtButtonCount.Enabled = enableButtonCount;
				}
			}
		}

		private void _cmbShowNavigation_CheckedChanged(object sender, EventArgs e)
		{
			_pnlNavigtion.Enabled = _cmbShowNavigation.Checked;
			_setDirty(sender, e);
		}

		/*
		private void btnImageUrl_Click(object sender, EventArgs e)
		{
			TextBox txtAssociated = (TextBox)((Button)sender).Tag;
			string tmp = txtAssociated.Text;

			txtAssociated.Text = ImageUrlBuilder.BuildUrl(_owner.Designer, tmp);

			if (txtAssociated.Text != tmp)
			{
				_setDirty(sender, e);
			}
		}
		*/

		private void Localize()
		{
			this._cmbPaging.Text = C1Localizer.GetString("C1GridView.PagingPanel.AllowPaging");
			this._cmbCustomPaging.Text = C1Localizer.GetString("C1GridView.PagingPanel.AllowCustomPaging");
			this.label2.Text = C1Localizer.GetString("C1GridView.PagingPanel.PageSize");
			this.label5.Text = C1Localizer.GetString("C1GridView.PagingPanel.Position");
			this.label6.Text = C1Localizer.GetString("C1GridView.PagingPanel.Mode");
			this.label7.Text = C1Localizer.GetString("C1GridView.PagingPanel.NextPageText");
			this.label8.Text = C1Localizer.GetString("C1GridView.PagingPanel.PreviousPageText");
			this.label9.Text = C1Localizer.GetString("C1GridView.PagingPanel.ButtonsCount");
			this.label1.Text = C1Localizer.GetString("C1GridView.PagingPanel.FirstPageText");
			this.label4.Text = C1Localizer.GetString("C1GridView.PagingPanel.LastPageText");
			this.label10.Text = C1Localizer.GetString("C1GridView.PagingPanel.FirstPageImageClass");
			//this.toolTip1.SetToolTip(this._btnFirstPageUrl, C1Localizer.GetString("C1GridView.PagingPanel.BrowseURL"));
			//this.toolTip1.SetToolTip(this._btnLastPageUrl, C1Localizer.GetString("C1GridView.PagingPanel.BrowseURL"));
			this.label11.Text = C1Localizer.GetString("C1GridView.PagingPanel.LastPageImageClass");
			//this.toolTip1.SetToolTip(this._btnPrevImageUrl, C1Localizer.GetString("C1GridView.PagingPanel.BrowseURL"));
			this.label12.Text = C1Localizer.GetString("C1GridView.PagingPanel.PreviousPageImageClass");
			//this.toolTip1.SetToolTip(this._btnNextImageUrl, C1Localizer.GetString("C1GridView.PagingPanel.BrowseURL"));
			this.label13.Text = C1Localizer.GetString("C1GridView.PagingPanel.NextPageImageClass");
			this._gbPaging.Text = C1Localizer.GetString("C1GridView.PagingPanel.Paging");
			this.label3.Text = C1Localizer.GetString("C1GridView.PagingPanel.Rows");
			this._gbNavigation.Text = C1Localizer.GetString("C1GridView.PagingPanel.PageNavigation");
			this._cmbShowNavigation.Text = C1Localizer.GetString("C1GridView.PagingPanel.ShowNavigation");
		}
	}
}