//----------------------------------------------------------------------------
// C1\Web\C1WebGrid\Design\GroupingPanel.cs
//
// Implements the 'GroupInfo' page on the PropertyBuilderDialog.
//
// Copyright (C) 2002 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status			Date			By						Comments
// Created			Aug 14, 2002	Bernardo				-
//----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;
	/// <summary>
	/// Summary description for GroupingPanel.
	/// </summary>
	internal class GroupingPanel : System.Windows.Forms.UserControl, IPropertyPanel
	{
		private System.Windows.Forms.TreeView _tree;
		private System.Windows.Forms.PropertyGrid _ppg;
		private System.Windows.Forms.ImageList _imgList;
		private GroupBox groupBox1;
		private GroupBox groupBox2;
		private System.ComponentModel.IContainer components;

		public GroupingPanel()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			Localize();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._ppg = new System.Windows.Forms.PropertyGrid();
			this._tree = new System.Windows.Forms.TreeView();
			this._imgList = new System.Windows.Forms.ImageList(this.components);
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// _ppg
			// 
			this._ppg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._ppg.HelpVisible = false;
			this._ppg.LineColor = System.Drawing.SystemColors.ScrollBar;
			this._ppg.Location = new System.Drawing.Point(10, 23);
			this._ppg.Name = "_ppg";
			this._ppg.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this._ppg.Size = new System.Drawing.Size(490, 216);
			this._ppg.TabIndex = 41;
			this._ppg.ToolbarVisible = false;
			this._ppg.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._ppg_PropertyValueChanged);
			// 
			// _tree
			// 
			this._tree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._tree.HideSelection = false;
			this._tree.ImageIndex = 0;
			this._tree.ImageList = this._imgList;
			this._tree.Location = new System.Drawing.Point(10, 23);
			this._tree.Name = "_tree";
			this._tree.SelectedImageIndex = 0;
			this._tree.Size = new System.Drawing.Size(490, 137);
			this._tree.TabIndex = 40;
			this._tree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._tree_AfterSelect);
			// 
			// _imgList
			// 
			this._imgList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this._imgList.ImageSize = new System.Drawing.Size(16, 16);
			this._imgList.TransparentColor = System.Drawing.Color.White;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this._tree);
			this.groupBox1.Location = new System.Drawing.Point(4, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(510, 170);
			this.groupBox1.TabIndex = 44;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "ColumnList";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this._ppg);
			this.groupBox2.Location = new System.Drawing.Point(4, 184);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(510, 251);
			this.groupBox2.TabIndex = 45;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "GroupingProperties";
			// 
			// GroupingPanel
			// 
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "GroupingPanel";
			this.Size = new System.Drawing.Size(520, 440);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		//----------------------------------------------------------------------
		// fields
		//----------------------------------------------------------------------
		private C1GridView _grid;
		private bool _initialized;
		private PropertyBuilderDialog _owner;

		//----------------------------------------------------------------------
		// IPropertyPanel implementation
		//----------------------------------------------------------------------
		void IPropertyPanel.Initialize(PropertyBuilderDialog owner)
		{
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColDBField);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColBand);

			// store references
			_owner = owner;
			_grid = owner.Grid;
			_ppg.Site = _grid.Site;
		}
		void IPropertyPanel.Apply()
		{
			ApplyGroupInfo(_tree.Nodes);
		}

		private void ApplyGroupInfo(TreeNodeCollection nodes)
		{
			for (int i = 0; i < nodes.Count; i++)
			{
				TreeNode node = nodes[i];

				if (node.Tag != null)
					((GroupInfoCache)node.Tag).Apply();
				else
					ApplyGroupInfo(node.Nodes);
			}
		}


		//----------------------------------------------------------------------
		// custom implementation
		//----------------------------------------------------------------------
		// update lists when showing panel
		// (depends on settings made on a different panel)
		override protected void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (!Visible || _owner == null)
				return;

			// re-initialize the page
			_initialized = false;
			_tree.Nodes.Clear();

			AddColumns(_grid.Columns, null);

			for (int i = 0; i < _tree.Nodes.Count; i++)
				if (_tree.Nodes[i].Nodes.Count > 0)
					_tree.Nodes[i].Expand();

			// select first node
			if (_tree.Nodes.Count > 0)
				_tree.SelectedNode = _tree.Nodes[0];

			// done initializing
			_initialized = true;
		}

		private void AddColumns(C1BaseFieldCollection columns, TreeNode addTo)
		{
			for (int i = 0; i < columns.Count; i++)
			{
				TreeNode node = AddColumn(columns[i], addTo);
				C1Band band = columns[i] as C1Band;
				if (band != null)
					AddColumns(band.Columns, node);
			}
		}

		private TreeNode AddColumn(C1BaseField column, TreeNode addTo)
		{
			//TreeNode node = (column is C1Band) ? new TreeNode(column.ToString(), 1, 1) : new TreeNode(column.ToString());
			TreeNode node = (column is C1Band) ? new TreeNode(column.Moniker(), 1, 1) : new TreeNode(column.Moniker());

			C1Field c1c = column as C1Field;
			if (c1c != null)
				node.Tag = new GroupInfoCache(c1c);

			if (addTo == null)
				_tree.Nodes.Add(node);
			else
				addTo.Nodes.Add(node);

			return node;
		}

		private void _ppg_PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
		{
			if (!_initialized) return;
			_owner.SetDirty(this);
		}
		private void _tree_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (_tree.SelectedNode != null && _tree.SelectedNode.Tag != null)
			{
				GroupInfoCache cache = _tree.SelectedNode.Tag as GroupInfoCache;
				_ppg.SelectedObject = cache.GetGroup();
			}
			else
				_ppg.SelectedObject = null;
		}

		private void Localize()
		{
			this.groupBox1.Text = C1Localizer.GetString("C1GridView.GroupingPanel.ColumnList");
			this.groupBox2.Text = C1Localizer.GetString("C1GridView.GroupingPanel.GroupingProperties");
		}
	}

	/// <summary>
	/// GroupInfoCache
	/// internal class used to cache grouping info until we need to save it
	/// </summary>
	internal class GroupInfoCache
	{
		private C1Field _col;
		private GroupInfo _group;

		internal GroupInfoCache(C1Field col)
		{
			_col = col;
			_group = new GroupInfo();
			((IOwnerable)_group).Owner = _col;
			//((ICreateable)_group).CopyFrom(col.GroupInfo); 
			_group.CopyFrom(col.GroupInfo);
		}
		internal GroupInfo GetGroup()
		{
			return _group;
		}

		internal void Apply()
		{
			//((ICreateable)_col.GroupInfo).CopyFrom(_group);
			_col.GroupInfo.CopyFrom(_group);
		}
	}
}