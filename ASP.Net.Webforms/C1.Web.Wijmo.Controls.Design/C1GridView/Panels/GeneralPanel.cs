//----------------------------------------------------------------------------
// C1\Web\C1WebGrid\Design\BordersPanel.cs
//
// Implements the 'General' page on the PropertyBuilderDialog.
//
// Copyright (C) 2002 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status			Date			By						Comments
// Created			Aug 14, 2002	Bernardo				-
//----------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.ComponentModel.Design;
using System.CodeDom;
using System.Runtime.InteropServices;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;
	/// <summary>
	/// Summary C1Description for GeneralPanel.
	/// </summary>
	internal class GeneralPanel : System.Windows.Forms.UserControl, IPropertyPanel
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox _chkHeader;
		private System.Windows.Forms.CheckBox _chkFooter;
		private System.Windows.Forms.CheckBox _chkSorting;
		private System.Windows.Forms.TextBox _txtDataSourceID;
		private System.Windows.Forms.TextBox _txtDataMember;
		private System.Windows.Forms.Label _lblHelp;
		private GroupBox groupBox1;
		private GroupBox groupBox2;
		private GroupBox groupBox3;
		private CheckBox _chkFilter;
		private CheckBox _chkColSizing;
		private CheckBox _chkColMoving;
		private CheckBox _chkGrouping;
		private GroupBox groupBox4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		// ctor
		public GeneralPanel()
		{
			InitializeComponent();
			Localize();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this._lblHelp = new System.Windows.Forms.Label();
			this._chkHeader = new System.Windows.Forms.CheckBox();
			this._chkFooter = new System.Windows.Forms.CheckBox();
			this._chkSorting = new System.Windows.Forms.CheckBox();
			this._txtDataSourceID = new System.Windows.Forms.TextBox();
			this._txtDataMember = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this._chkFilter = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this._chkColMoving = new System.Windows.Forms.CheckBox();
			this._chkColSizing = new System.Windows.Forms.CheckBox();
			this._chkGrouping = new System.Windows.Forms.CheckBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(10, 20);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "DataSourceID";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(186, 20);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "DataMember";
			// 
			// _lblHelp
			// 
			this._lblHelp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this._lblHelp.Location = new System.Drawing.Point(10, 73);
			this._lblHelp.Name = "_lblHelp";
			this._lblHelp.Size = new System.Drawing.Size(494, 35);
			this._lblHelp.TabIndex = 8;
			// 
			// _chkHeader
			// 
			this._chkHeader.AutoSize = true;
			this._chkHeader.Location = new System.Drawing.Point(10, 25);
			this._chkHeader.Name = "_chkHeader";
			this._chkHeader.Size = new System.Drawing.Size(110, 21);
			this._chkHeader.TabIndex = 11;
			this._chkHeader.Text = "ShowHeader";
			this._chkHeader.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// _chkFooter
			// 
			this._chkFooter.AutoSize = true;
			this._chkFooter.Cursor = System.Windows.Forms.Cursors.Arrow;
			this._chkFooter.Location = new System.Drawing.Point(10, 49);
			this._chkFooter.Name = "_chkFooter";
			this._chkFooter.Size = new System.Drawing.Size(105, 21);
			this._chkFooter.TabIndex = 12;
			this._chkFooter.Text = "ShowFooter";
			this._chkFooter.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// _chkSorting
			// 
			this._chkSorting.AutoSize = true;
			this._chkSorting.Cursor = System.Windows.Forms.Cursors.Arrow;
			this._chkSorting.Location = new System.Drawing.Point(10, 23);
			this._chkSorting.Name = "_chkSorting";
			this._chkSorting.Size = new System.Drawing.Size(106, 21);
			this._chkSorting.TabIndex = 15;
			this._chkSorting.Text = "AllowSorting";
			this._chkSorting.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// _txtDataSourceID
			// 
			this._txtDataSourceID.Location = new System.Drawing.Point(10, 39);
			this._txtDataSourceID.Name = "_txtDataSourceID";
			this._txtDataSourceID.ReadOnly = true;
			this._txtDataSourceID.Size = new System.Drawing.Size(160, 24);
			this._txtDataSourceID.TabIndex = 16;
			// 
			// _txtDataMember
			// 
			this._txtDataMember.Location = new System.Drawing.Point(189, 39);
			this._txtDataMember.Name = "_txtDataMember";
			this._txtDataMember.ReadOnly = true;
			this._txtDataMember.Size = new System.Drawing.Size(160, 24);
			this._txtDataMember.TabIndex = 16;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this._txtDataSourceID);
			this.groupBox1.Controls.Add(this._txtDataMember);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this._lblHelp);
			this.groupBox1.Location = new System.Drawing.Point(4, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(513, 121);
			this.groupBox1.TabIndex = 20;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Data";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this._chkFilter);
			this.groupBox2.Controls.Add(this._chkHeader);
			this.groupBox2.Controls.Add(this._chkFooter);
			this.groupBox2.Location = new System.Drawing.Point(4, 133);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(513, 80);
			this.groupBox2.TabIndex = 21;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "HeaderAndFooter";
			// 
			// _chkFilter
			// 
			this._chkFilter.AutoSize = true;
			this._chkFilter.Location = new System.Drawing.Point(145, 25);
			this._chkFilter.Name = "_chkFilter";
			this._chkFilter.Size = new System.Drawing.Size(93, 21);
			this._chkFilter.TabIndex = 13;
			this._chkFilter.Text = "ShowFilter";
			this._chkFilter.UseVisualStyleBackColor = true;
			this._chkFilter.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this._chkSorting);
			this.groupBox3.Location = new System.Drawing.Point(4, 221);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(513, 57);
			this.groupBox3.TabIndex = 22;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Behavior";
			// 
			// _chkColMoving
			// 
			this._chkColMoving.AutoSize = true;
			this._chkColMoving.Location = new System.Drawing.Point(10, 25);
			this._chkColMoving.Name = "_chkColMoving";
			this._chkColMoving.Size = new System.Drawing.Size(159, 21);
			this._chkColMoving.TabIndex = 16;
			this._chkColMoving.Text = "AllowColumnMoving";
			this._chkColMoving.UseVisualStyleBackColor = true;
			this._chkColMoving.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// _chkColSizing
			// 
			this._chkColSizing.AutoSize = true;
			this._chkColSizing.Location = new System.Drawing.Point(10, 49);
			this._chkColSizing.Name = "_chkColSizing";
			this._chkColSizing.Size = new System.Drawing.Size(157, 21);
			this._chkColSizing.TabIndex = 17;
			this._chkColSizing.Text = "AllowColumnResizing";
			this._chkColSizing.UseVisualStyleBackColor = true;
			this._chkColSizing.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// _chkGrouping
			// 
			this._chkGrouping.AutoSize = true;
			this._chkGrouping.Location = new System.Drawing.Point(189, 23);
			this._chkGrouping.Name = "_chkGrouping";
			this._chkGrouping.Size = new System.Drawing.Size(119, 21);
			this._chkGrouping.TabIndex = 18;
			this._chkGrouping.Text = "AllowGrouping";
			this._chkGrouping.UseVisualStyleBackColor = true;
			this._chkGrouping.CheckedChanged += new System.EventHandler(this._chk_chkedChanged);
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this._chkGrouping);
			this.groupBox4.Controls.Add(this._chkColMoving);
			this.groupBox4.Controls.Add(this._chkColSizing);
			this.groupBox4.Location = new System.Drawing.Point(4, 284);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(513, 82);
			this.groupBox4.TabIndex = 23;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "ClientSide";
			// 
			// GeneralPanel
			// 
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "GeneralPanel";
			this.Size = new System.Drawing.Size(520, 440);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion


		//----------------------------------------------------------------------
		// fields
		//----------------------------------------------------------------------
		private C1GridView _grid;
		private bool _initialized;
		private PropertyBuilderDialog _owner;

		//----------------------------------------------------------------------
		// IPropertyPanel implementation
		//----------------------------------------------------------------------
		void IPropertyPanel.Initialize(PropertyBuilderDialog owner)
		{
			// store references
			_owner = owner;
			_grid = owner.Grid;

			// initialize text boxes
			_txtDataSourceID.Text = _owner.Designer.DataSourceID;
			_txtDataMember.Text = _grid.DataMember;

			// initialize checkboxes
			_chkColMoving.Checked = _grid.AllowColMoving;
			_chkColSizing.Checked = _grid.AllowColSizing;
			_chkGrouping.Checked = _grid.ShowGroupArea;
			_chkSorting.Checked = _grid.AllowSorting;

			_chkHeader.Checked = _grid.ShowHeader;
			_chkFilter.Checked = _grid.ShowFilter;
			_chkFooter.Checked = _grid.ShowFooter;

			// done initializing
			_initialized = true;
		}
		void IPropertyPanel.Apply()
		{
			_grid.AllowColMoving = _chkColMoving.Checked;
			_grid.AllowColSizing = _chkColSizing.Checked;
			_grid.ShowGroupArea = _chkGrouping.Checked;
			_grid.AllowSorting = _chkSorting.Checked;

			_grid.ShowHeader = _chkHeader.Checked;
			_grid.ShowFilter = _chkFilter.Checked;
			_grid.ShowFooter = _chkFooter.Checked;
		}

		//----------------------------------------------------------------------
		// custom implementation
		//----------------------------------------------------------------------
		// update help text when showing panel 
		// (depends on settings made on a different panel)
		override protected void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (!Visible || _owner == null)
				return;

			// update help text
			_lblHelp.Text = (_grid.AutogenerateColumns)
				? C1Localizer.GetString("C1GridView.GeneralPanel.AutoGenerateColumns")
				: C1Localizer.GetString("C1GridView.GeneralPanel.DefinedColumns");
		}
		private void _chk_chkedChanged(object sender, System.EventArgs e)
		{
			if (_initialized)
				_owner.SetDirty(this);
		}

		private void Localize()
		{
			this.label2.Text = C1Localizer.GetString("C1GridView.GeneralPanel.DataSourceID");
			this.label3.Text = C1Localizer.GetString("C1GridView.GeneralPanel.DataMember");
			this._chkHeader.Text = C1Localizer.GetString("C1GridView.GeneralPanel.ShowHeader");
			this._chkFooter.Text = C1Localizer.GetString("C1GridView.GeneralPanel.ShowFooter");
			this._chkSorting.Text = C1Localizer.GetString("C1GridView.GeneralPanel.AllowSorting");
			this.groupBox1.Text = C1Localizer.GetString("C1GridView.GeneralPanel.Data");
			this.groupBox2.Text = C1Localizer.GetString("C1GridView.GeneralPanel.HeaderAndFooter");
			this._chkFilter.Text = C1Localizer.GetString("C1GridView.GeneralPanel.ShowFilter");
			this.groupBox3.Text = C1Localizer.GetString("C1GridView.GeneralPanel.Behavior");
			this._chkColMoving.Text = C1Localizer.GetString("C1GridView.GeneralPanel.AllowColumnMoving");
			this._chkColSizing.Text = C1Localizer.GetString("C1GridView.GeneralPanel.AllowColumnResizing");
			this._chkGrouping.Text = C1Localizer.GetString("C1GridView.GeneralPanel.AllowGrouping");
			this.groupBox4.Text = C1Localizer.GetString("C1GridView.GeneralPanel.ClientSide");
		}
	}
}