//----------------------------------------------------------------------------
// C1\Web\C1WebGrid\Design\BordersPanel.cs
//
// Implements the 'Columns' page on the PropertyBuilderDialog.
//
// Copyright (C) 2002 GrapeCity, Inc
//----------------------------------------------------------------------------
// Status			Date			By						Comments
// Created			Aug 14, 2002	Bernardo				-
//----------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.WebControls;
using WF = System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.C1GridView;
	using C1.Web.Wijmo.Controls.Design.Localization;
	/// <summary>
	/// Summary description for ColumnsPanel.
	/// </summary>
	internal class ColumnsPanel : System.Windows.Forms.UserControl, IPropertyPanel
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button _btnDelete;
		private System.Windows.Forms.CheckBox _chkAutoColumns;
		private System.Windows.Forms.TreeView _pickTree;
		private System.Windows.Forms.ImageList _imgList;
		private System.Windows.Forms.Button _btnAdd;
		private System.Windows.Forms.Button _btnMoveUp;
		private System.Windows.Forms.Button _btnMoveDown;
		private System.Windows.Forms.TreeView _lstColumns;
		private System.Windows.Forms.PropertyGrid _ppg;
		private System.Windows.Forms.Button _btnAddInto;
		private GroupBox groupBox1;
		private GroupBox groupBox2;
		private ToolTip toolTip1;
		private System.ComponentModel.IContainer components;

		public ColumnsPanel()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			// TODO: Add any initialization after the InitializeComponent call
			Localize();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._chkAutoColumns = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this._btnAdd = new System.Windows.Forms.Button();
			this._pickTree = new System.Windows.Forms.TreeView();
			this._imgList = new System.Windows.Forms.ImageList(this.components);
			this._lstColumns = new System.Windows.Forms.TreeView();
			this._ppg = new System.Windows.Forms.PropertyGrid();
			this._btnAddInto = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this._btnDelete = new System.Windows.Forms.Button();
			this._btnMoveDown = new System.Windows.Forms.Button();
			this._btnMoveUp = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// _chkAutoColumns
			// 
			this._chkAutoColumns.AutoSize = true;
			this._chkAutoColumns.Location = new System.Drawing.Point(4, 4);
			this._chkAutoColumns.Name = "_chkAutoColumns";
			this._chkAutoColumns.Size = new System.Drawing.Size(277, 21);
			this._chkAutoColumns.TabIndex = 26;
			this._chkAutoColumns.Text = "AutoGenerateColumns";
			this._chkAutoColumns.CheckedChanged += new System.EventHandler(this._setDirty);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(119, 17);
			this.label1.TabIndex = 28;
			this.label1.Text = "AvailableColumns";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(263, 22);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(119, 17);
			this.label3.TabIndex = 31;
			this.label3.Text = "SelectedColumns";
			// 
			// _btnAdd
			// 
			this._btnAdd.AutoSize = true;
			this._btnAdd.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowRightIco;
			this._btnAdd.Location = new System.Drawing.Point(234, 63);
			this._btnAdd.Name = "_btnAdd";
			this._btnAdd.Size = new System.Drawing.Size(25, 40);
			this._btnAdd.TabIndex = 40;
			this.toolTip1.SetToolTip(this._btnAdd, "AddColumn");
			this._btnAdd.Click += new System.EventHandler(this._btnAdd_Click);
			// 
			// _pickTree
			// 
			this._pickTree.HideSelection = false;
			this._pickTree.ImageIndex = 0;
			this._pickTree.ImageList = this._imgList;
			this._pickTree.Location = new System.Drawing.Point(9, 43);
			this._pickTree.Name = "_pickTree";
			this._pickTree.SelectedImageIndex = 0;
			this._pickTree.Size = new System.Drawing.Size(217, 136);
			this._pickTree.TabIndex = 29;
			this._pickTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._pickTree_AfterSelect);
			// 
			// _imgList
			// 
			this._imgList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this._imgList.ImageSize = new System.Drawing.Size(16, 16);
			this._imgList.TransparentColor = System.Drawing.Color.White;
			// 
			// _lstColumns
			// 
			this._lstColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this._lstColumns.HideSelection = false;
			this._lstColumns.ImageIndex = 0;
			this._lstColumns.ImageList = this._imgList;
			this._lstColumns.Location = new System.Drawing.Point(266, 43);
			this._lstColumns.Name = "_lstColumns";
			this._lstColumns.SelectedImageIndex = 0;
			this._lstColumns.Size = new System.Drawing.Size(207, 136);
			this._lstColumns.TabIndex = 32;
			this._lstColumns.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._lstColumns_AfterSelect);
			// 
			// _ppg
			// 
			this._ppg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this._ppg.HelpVisible = false;
			this._ppg.LineColor = System.Drawing.SystemColors.ScrollBar;
			this._ppg.Location = new System.Drawing.Point(10, 23);
			this._ppg.Name = "_ppg";
			this._ppg.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this._ppg.Size = new System.Drawing.Size(490, 167);
			this._ppg.TabIndex = 44;
			this._ppg.ToolbarVisible = false;
			this._ppg.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._ppg_PropertyValueChanged);
			// 
			// _btnAddInto
			// 
			this._btnAddInto.AutoSize = true;
			this._btnAddInto.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.AddItemIco;
			this._btnAddInto.Location = new System.Drawing.Point(234, 118);
			this._btnAddInto.Name = "_btnAddInto";
			this._btnAddInto.Size = new System.Drawing.Size(25, 40);
			this._btnAddInto.TabIndex = 39;
			this.toolTip1.SetToolTip(this._btnAddInto, "AddColumn2BandColumn");
			this._btnAddInto.Click += new System.EventHandler(this._btnAddInto_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this._lstColumns);
			this.groupBox1.Controls.Add(this._btnDelete);
			this.groupBox1.Controls.Add(this._btnAddInto);
			this.groupBox1.Controls.Add(this._btnMoveDown);
			this.groupBox1.Controls.Add(this._btnMoveUp);
			this.groupBox1.Controls.Add(this._pickTree);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this._btnAdd);
			this.groupBox1.Location = new System.Drawing.Point(4, 32);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(510, 191);
			this.groupBox1.TabIndex = 48;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "ColumnList";
			// 
			// _btnDelete
			// 
			this._btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnDelete.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.DeleteIco;
			this._btnDelete.Location = new System.Drawing.Point(479, 155);
			this._btnDelete.Name = "_btnDelete";
			this._btnDelete.Size = new System.Drawing.Size(24, 24);
			this._btnDelete.TabIndex = 43;
			this.toolTip1.SetToolTip(this._btnDelete, "RemoveColumn");
			this._btnDelete.Click += new System.EventHandler(this._btnDelete_Click);
			// 
			// _btnMoveDown
			// 
			this._btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnMoveDown.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowDownIco;
			this._btnMoveDown.Location = new System.Drawing.Point(479, 71);
			this._btnMoveDown.Name = "_btnMoveDown";
			this._btnMoveDown.Size = new System.Drawing.Size(24, 24);
			this._btnMoveDown.TabIndex = 42;
			this.toolTip1.SetToolTip(this._btnMoveDown, "MoveColumnDown");
			this._btnMoveDown.Click += new System.EventHandler(this._btnMoveDown_Click);
			// 
			// _btnMoveUp
			// 
			this._btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnMoveUp.Image = global::C1.Web.Wijmo.Controls.Design.Properties.Resources.ArrowUpIco;
			this._btnMoveUp.Location = new System.Drawing.Point(479, 41);
			this._btnMoveUp.Name = "_btnMoveUp";
			this._btnMoveUp.Size = new System.Drawing.Size(24, 24);
			this._btnMoveUp.TabIndex = 41;
			this.toolTip1.SetToolTip(this._btnMoveUp, "MoveColumnUp");
			this._btnMoveUp.Click += new System.EventHandler(this._btnMoveUp_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this._ppg);
			this.groupBox2.Location = new System.Drawing.Point(4, 233);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(510, 202);
			this.groupBox2.TabIndex = 49;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "ColumnProperties";
			// 
			// ColumnsPanel
			// 
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this._chkAutoColumns);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "ColumnsPanel";
			this.Size = new System.Drawing.Size(520, 440);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		//----------------------------------------------------------------------
		// to identify tree items
		//----------------------------------------------------------------------
		private enum ColumnType
		{
			Band,
			Bound,
			Button,
			Command,
			EditUpdateCancel,
			Select,
			Delete,
			Hyperlink,
			Template,
			CheckBox,
			Image
		}

		//----------------------------------------------------------------------
		// fields
		//----------------------------------------------------------------------
		private C1GridView _grid;
		private bool _initialized;
		private PropertyBuilderDialog _owner;
		private C1BaseFieldCollection _clonedColCollection = null;

		//----------------------------------------------------------------------
		// IPropertyPanel implementation
		//----------------------------------------------------------------------
		void IPropertyPanel.Initialize(PropertyBuilderDialog owner)
		{
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColCurrent);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColDB);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColDBField);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColButton);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColLink);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColTemplate);
			_imgList.Images.Add(C1.Web.Wijmo.Controls.Design.Properties.Resources.C1GridViewColBand);

			// store references
			_owner = owner;
			_grid = owner.Grid;
			_ppg.Site = _grid.Site;
		}

		void IPropertyPanel.Apply()
		{
			// set flag
			_grid.AutogenerateColumns = _chkAutoColumns.Checked;

			// rebuild column collection
			_grid.Columns.Clear();
			ApplyColumnsToGrid(_grid.Columns, _lstColumns.Nodes);
		}


		//----------------------------------------------------------------------
		// custom implementation
		//----------------------------------------------------------------------
		// update lists when showing panel
		// (depends on settings made on a different panel)
		override protected void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (!Visible || _owner == null)
				return;

			// re-initialize the page
			_initialized = false;

			// initialize controls
			_chkAutoColumns.Checked = _grid.AutogenerateColumns;

			// populate lists
			PopulateAvailable();
			PopulateCurrent();

			for (int i = 0; i < _lstColumns.Nodes.Count; i++)
				if (_lstColumns.Nodes[i].Nodes.Count > 0)
					_lstColumns.Nodes[i].Expand();

			if (_lstColumns.Nodes.Count > 0)
			{
				_lstColumns.SelectedNode = _lstColumns.Nodes[0];
				_lstColumns.SelectedNode = null;
			}

			// update controls
			UpdateControls();

			// done initializing
			_initialized = true;
		}

		private void PopulateAvailable()
		{
			WF.TreeNode root, nd;

			// start from scratch

			_pickTree.BeginUpdate();

			_pickTree.Nodes.Clear();

			IDataSourceFieldSchema[] fields = _owner.Designer.DataSourceSchemaFields;

			if (fields.Length > 0)
			{
				C1BaseFieldCollection schema = CreateColumnsFromSchema(fields);

				root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.DataFields"), 1, 1);
				_pickTree.Nodes.Add(root);

				nd = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.AllFields"), 2, 2);
				nd.Tag = schema;
				root.Nodes.Add(nd);

				for (int i = 0; i < schema.Count; i++)
				{
					C1BoundField col = schema[i] as C1BoundField;
					nd = new WF.TreeNode(col.DataField, 2, 2);
					nd.Tag = col;
					root.Nodes.Add(nd);
				}
			}
			// no (or empty) schema, add a single unnamed bound field
			else
			{
				root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.BoundField"), 2, 2);
				root.Tag = ColumnType.Bound;
				_pickTree.Nodes.Add(root);
			}

			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.BandField"), 6, 6);
			root.Tag = ColumnType.Band;
			_pickTree.Nodes.Add(root);

			// add button column
			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.ButtonField"), 3, 3);
			root.Tag = ColumnType.Button;
			_pickTree.Nodes.Add(root);

			// add checkbox column
			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.CheckBoxField"), 2, 2);
			root.Tag = ColumnType.CheckBox;
			_pickTree.Nodes.Add(root);

			// add command columns
			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.CommandField"), 3, 3);
			root.Tag = ColumnType.Command;
			_pickTree.Nodes.Add(root);

			nd = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.EditUpdateCancel"), 3, 3);
			nd.Tag = ColumnType.EditUpdateCancel;
			root.Nodes.Add(nd);

			nd = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.Select"), 3, 3);
			nd.Tag = ColumnType.Select;
			root.Nodes.Add(nd);

			nd = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.Delete"), 3, 3);
			nd.Tag = ColumnType.Delete;
			root.Nodes.Add(nd);

			// add image column
			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.ImageField"), 4, 4);
			root.Tag = ColumnType.Image;
			_pickTree.Nodes.Add(root);

			// hyperlinks
			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.HyperLinkField"), 4, 4);
			root.Tag = ColumnType.Hyperlink;
			_pickTree.Nodes.Add(root);

			// templates
			root = new WF.TreeNode(C1Localizer.GetString("C1GridView.ColumnsPanel.TemplateField"), 5, 5);
			root.Tag = ColumnType.Template;
			_pickTree.Nodes.Add(root);

			_pickTree.EndUpdate();
		}

		private void PopulateCurrent()
		{
			_lstColumns.BeginUpdate();

			// start from scratch
			_lstColumns.Nodes.Clear();

			// add grid columns
			_clonedColCollection = (C1BaseFieldCollection)((ICloneable)_grid.Columns).Clone();
			((IOwnerable)_clonedColCollection).Owner = _grid;
			AddColumns(_clonedColCollection, _lstColumns.SelectedNode, false);

			_lstColumns.EndUpdate();
		}

		private void AddColumns(C1BaseFieldCollection cols, WF.TreeNode addTo, bool addInto)
		{
			for (int i = 0; i < cols.Count; i++)
			{
				WF.TreeNode node = AddColumn(cols[i], addTo, addInto);
				C1Band band = cols[i] as C1Band;
				if (band != null)
					AddColumns(band.Columns, node, true);
			}
		}

		private WF.TreeNode AddColumn(C1BaseField column, WF.TreeNode addTo, bool addInto)
		{
			((IDataSourceViewSchemaAccessor)column).DataSourceViewSchema =
				_owner.Designer.DataSourceSchema;

			string name = column.Moniker();

			//string name = (column is C1BoundField && !string.IsNullOrEmpty(((C1BoundField)column).HeaderText))
			//	? column.HeaderText
			//	: column.ToString();

			int img = 1;
			if (column is C1BoundField) img = 2;
			if (column is C1ButtonField) img = 3;
			if (column is C1ImageField) img = 4;
			if (column is C1CheckBoxField) img = 2;
			if (column is C1CommandField) img = 3;
			if (column is C1HyperLinkField) img = 4;
			if (column is C1TemplateField) img = 5;
			if (column is C1Band) img = 6;

			WF.TreeNode node = new WF.TreeNode(name, img, img);
			node.Tag = column;

			if (addTo == null)
			{
				_lstColumns.Nodes.Add(node);

				if (((IOwnerable)column).Owner == null)
				{
					((IOwnerable)column).Owner = _clonedColCollection;
				}
			}
			else
				if (addInto) //insert into band
				{
					addTo.Nodes.Add(node);

					if (((IOwnerable)column).Owner == null)
					{
						((IOwnerable)column).Owner = ((C1Band)addTo.Tag).Columns;
					}
					//addTo.Expand();
				}
				else
				{
					if (addTo.Parent == null)
					{
						_lstColumns.Nodes.Add(node);

						if (((IOwnerable)column).Owner == null)
						{
							((IOwnerable)column).Owner = _clonedColCollection;
						}
					}
					else  //insert into band
					{
						addTo.Parent.Nodes.Add(node);

						if (((IOwnerable)column).Owner == null)
						{
							((IOwnerable)column).Owner = ((C1Band)addTo.Parent.Tag).Columns;
						}
					}
				}

			UpdateUI();
			return node;
		}

		private void ApplyColumnsToGrid(C1BaseFieldCollection columns, WF.TreeNodeCollection nodes)
		{
			for (int i = 0; i < nodes.Count; i++)
			{
				WF.TreeNode node = nodes[i];
				C1BaseField column = (C1BaseField)node.Tag;

				columns.Add(column);

				//string newName = column.ToString();
				string newName = column.Moniker();

				if (!string.IsNullOrEmpty(newName))
					node.Text = newName;

				C1Band band = node.Tag as C1Band;
				if (band != null)
				{
					band.Columns.Clear();
					ApplyColumnsToGrid(band.Columns, node.Nodes);
				}
			}
		}

		private void _setDirty(object sender, System.EventArgs e)
		{
			if (!_initialized) return;
			_owner.SetDirty(this);
		}

		private void UpdateUI()
		{
			// update UI to reflect new selection
			_btnAdd.Enabled = (_pickTree.SelectedNode != null)
				? _pickTree.SelectedNode.Tag != null
				: false;

			_btnAddInto.Enabled = (_btnAdd.Enabled && _lstColumns.SelectedNode != null && _lstColumns.SelectedNode.Tag != null && _lstColumns.SelectedNode.Tag is C1Band);

			// update UI to reflect new selection
			WF.TreeNode nd = _lstColumns.SelectedNode;
			int index = (nd != null) ? nd.Index : -1;

			// enable/disable buttons
			_btnDelete.Enabled = index > -1;
			_btnMoveUp.Enabled = (index > -1) && (index > 0 || nd.Parent != null);
			_btnMoveDown.Enabled = (index > -1) && (nd.Parent != null || index < _lstColumns.Nodes.Count - 1);
		}


		private void _btnAdd_Click(object sender, System.EventArgs e)
		{
			_lstColumns.BeginUpdate();
			MoveFromPeekTreeToListColumns(_lstColumns.SelectedNode, false);
			_lstColumns.EndUpdate();
		}

		private void _btnAddInto_Click(object sender, EventArgs e)
		{
			_lstColumns.BeginUpdate();
			MoveFromPeekTreeToListColumns(_lstColumns.SelectedNode, true);
			_lstColumns.EndUpdate();
		}

		private void MoveFromPeekTreeToListColumns(WF.TreeNode addTo, bool addInto)
		{
			if (_pickTree.SelectedNode == null || _pickTree.SelectedNode.Tag == null)
				return;

			object source = _pickTree.SelectedNode.Tag;

			// we'll be dirty soon
			_setDirty(null, EventArgs.Empty);

			// add all db fields
			C1BaseFieldCollection schema = source as C1BaseFieldCollection;
			if (schema != null)
			{
				AddColumns(schema, addTo, addInto);
				return;
			}

			// add a single bound column
			C1BoundField col = source as C1BoundField;
			if (col != null)
			{
				AddColumn(col, addTo, addInto);
				return;
			}

			// add predefined empty column
			if (source is ColumnType)
			{
				switch ((ColumnType)source)
				{
					case ColumnType.Band:
						C1Band band = new C1Band();
						AddColumn(band, addTo, addInto);
						break;

					case ColumnType.Bound:
						C1BoundField bnd = new C1BoundField();
						AddColumn(bnd, addTo, addInto);
						break;

					case ColumnType.Button:
						C1ButtonField btn = new C1ButtonField();
						btn.Text = "Button";
						AddColumn(btn, addTo, addInto);
						break;

					case ColumnType.CheckBox:
						C1CheckBoxField checkBox = new C1CheckBoxField();
						AddColumn(checkBox, addTo, addInto);
						break;

					case ColumnType.Image:
						C1ImageField image = new C1ImageField();
						AddColumn(image, addTo, addInto);
						break;

					case ColumnType.Select:
						C1CommandField sel = new C1CommandField();
						sel.ShowSelectButton = true;
						AddColumn(sel, addTo, addInto);
						break;

					case  ColumnType.Command:
						C1CommandField cmd = new C1CommandField();
						AddColumn(cmd, addTo, addInto);
						break;

					case ColumnType.EditUpdateCancel:
						C1CommandField edt = new C1CommandField();
						edt.ShowEditButton = true;
						AddColumn(edt, addTo, addInto);
						break;

					case ColumnType.Delete:
						C1CommandField del = new C1CommandField();
						del.ShowDeleteButton = true;
						AddColumn(del, addTo, addInto);
						break;

					case ColumnType.Hyperlink:
						C1HyperLinkField lnk = new C1HyperLinkField();
						AddColumn(lnk, addTo, addInto);
						break;

					case ColumnType.Template:
						C1TemplateField tpl = new C1TemplateField();
						AddColumn(tpl, addTo, addInto);
						break;

					default:
						// should never get here
						Debug.Assert(false);
						break;
				}
			}
		}

		private C1BaseFieldCollection CreateColumnsFromSchema(IDataSourceFieldSchema[] fieldsSchema)
		{
			if (fieldsSchema.Length > 0)
			{
				C1BaseFieldCollection columns = new C1BaseFieldCollection();

				foreach (IDataSourceFieldSchema fieldSchema in fieldsSchema)
				{
					C1BoundField boundField = (fieldSchema.DataType == typeof(bool))
						? new C1CheckBoxField()
						: new C1BoundField();

					boundField.DataField = boundField.HeaderText = boundField.SortExpression = fieldSchema.Name;

					columns.Add(boundField);
				}

				return columns;
			}

			return null;
		}

		private void _btnMoveUp_Click(object sender, System.EventArgs e)
		{
			WF.TreeNode selected = _lstColumns.SelectedNode;
			WF.TreeNode parent = selected.Parent;
			WF.TreeNode prev = selected.PrevNode;
			int index = selected.Index;

			if (parent == null)
			{
				_lstColumns.Nodes.RemoveAt(index);

				if (prev.Tag is C1Band)
					prev.Nodes.Add(selected);
				else
					_lstColumns.Nodes.Insert(index - 1, selected);
			}
			else
			{
				parent.Nodes.RemoveAt(index);

				if (index == 0)
				{
					if (parent.Parent == null)
						_lstColumns.Nodes.Insert(parent.Index, selected);
					else
						parent.Parent.Nodes.Insert(parent.Index, selected);
				}
				else
				{
					if (prev.Tag is C1Band)
						prev.Nodes.Add(selected);
					else
						parent.Nodes.Insert(index - 1, selected);
				}
			}

			_lstColumns.SelectedNode = selected;
			_lstColumns.Focus();
			_setDirty(sender, e);
		}

		private void _btnMoveDown_Click(object sender, System.EventArgs e)
		{
			WF.TreeNode selected = _lstColumns.SelectedNode;
			WF.TreeNode parent = selected.Parent;
			WF.TreeNode next = selected.NextNode;
			int index = selected.Index;

			if (parent == null)
			{
				_lstColumns.Nodes.RemoveAt(index);

				if (next.Tag is C1Band)
					next.Nodes.Insert(0, selected);
				else
					_lstColumns.Nodes.Insert(index + 1, selected);
			}
			else
			{
				parent.Nodes.RemoveAt(index);

				if (index == parent.Nodes.Count)
				{
					if (parent.Parent == null)
						_lstColumns.Nodes.Insert(parent.Index + 1, selected);
					else
						parent.Parent.Nodes.Insert(parent.Index + 1, selected);
				}
				else
				{
					if (next.Tag is C1Band)
						next.Nodes.Insert(0, selected);
					else
						parent.Nodes.Insert(index + 1, selected);
				}
			}

			_lstColumns.SelectedNode = selected;
			_lstColumns.Focus();
			_setDirty(sender, e);
		}

		private void _btnDelete_Click(object sender, System.EventArgs e)
		{
			WF.TreeNode nd = _lstColumns.SelectedNode;
			if (nd == null) return;
			_lstColumns.Nodes.Remove(nd);
			_lstColumns.Focus();
			_setDirty(sender, e);
			if (_lstColumns.Nodes.Count == 0)
				UpdateUI();
			UpdateControls();
		}

		private void _lstColumns_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (_initialized)
			{
				UpdateUI();
				UpdateControls();
			}
		}

		private void _pickTree_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			UpdateUI();
		}

		private void UpdateControls()
		{
			// initialize label, panels
			_ppg.SelectedObject = null;

			// get column with the info we need to show
			if (_lstColumns.SelectedNode == null) return;
			_ppg.SelectedObject = _lstColumns.SelectedNode.Tag;

			// turn notifications back on
			_initialized = true;
		}

		private void _ppg_PropertyValueChanged(object s, System.Windows.Forms.PropertyValueChangedEventArgs e)
		{
			_setDirty(s, EventArgs.Empty);
		}

		private void Localize()
		{
			this._chkAutoColumns.Text = C1Localizer.GetString("C1GridView.ColumnsPanel.CreateColumns");
			this.label1.Text = C1Localizer.GetString("C1GridView.ColumnsPanel.AvailableColumns");
			this.label3.Text = C1Localizer.GetString("C1GridView.ColumnsPanel.SelectedColumns");
			this.toolTip1.SetToolTip(this._btnAdd, C1Localizer.GetString("C1GridView.ColumnsPanel.AddColumn"));
			this.toolTip1.SetToolTip(this._btnAddInto, C1Localizer.GetString("C1GridView.ColumnsPanel.AddColumn2BandColumn"));
			this.groupBox1.Text = C1Localizer.GetString("C1GridView.ColumnsPanel.ColumnList");
			this.toolTip1.SetToolTip(this._btnDelete, C1Localizer.GetString("C1GridView.ColumnsPanel.RemoveColumn"));
			this.toolTip1.SetToolTip(this._btnMoveDown, C1Localizer.GetString("C1GridView.ColumnsPanel.MoveColumnDown"));
			this.toolTip1.SetToolTip(this._btnMoveUp, C1Localizer.GetString("C1GridView.ColumnsPanel.MoveColumnUp"));
			this.groupBox2.Text = C1Localizer.GetString("C1GridView.ColumnsPanel.ColumnProperties");
		}
	}
}