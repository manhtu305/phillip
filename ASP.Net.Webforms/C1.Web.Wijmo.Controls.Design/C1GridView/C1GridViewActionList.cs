namespace C1.Web.Wijmo.Controls.Design.C1GridView
{
	using System;
	using System.ComponentModel.Design;
	using System.Reflection;

	//using C1.Util.Localization;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// 
	/// </summary>

	/// <summary>
	/// Designer smart tags for the <see cref="C1.Web.UI.Controls.C1GridView.C1GridView"/> control.
	/// </summary>
	public class C1GridViewActionList : DesignerActionListBase
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="designer"></param>
		public C1GridViewActionList(C1GridViewDesigner designer)
			: base(designer)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection dac = new DesignerActionItemCollection();

			dac.Add(new DesignerActionMethodItem(this, "PropertyBuilder", C1Localizer.GetString("C1GridView.SmartTag.PropertyBuilder"), "Action", C1Localizer.GetString("C1GridView.SmartTag.PropertyBuilderDescription")));
			dac.Add(new DesignerActionMethodItem(this, "LoadLayout", C1Localizer.GetString("C1GridView.SmartTag.LoadLayout"), "Action", C1Localizer.GetString("C1GridView.SmartTag.LoadLayoutDescription")));
			dac.Add(new DesignerActionMethodItem(this, "SaveLayout", C1Localizer.GetString("C1GridView.SmartTag.SaveLayout"), "Action", C1Localizer.GetString("C1GridView.SmartTag.SaveLayoutDescription")));

			AddBaseSortedActionItems(dac);

			return dac;
		}

		/// <summary>
		/// 
		/// </summary>
		public void PropertyBuilder()
		{
			((C1GridViewDesigner)Designer).PropertyBuilder(0);
		}

		public void LoadLayout()
		{
			((C1GridViewDesigner)Designer).LoadLayout();

		}

		public void SaveLayout()
		{
			((C1GridViewDesigner)Designer).SaveLayout();
		}

		protected override bool SupportConditionalDependencies
		{
			get
			{
				return true;
			}
		}
	}
}