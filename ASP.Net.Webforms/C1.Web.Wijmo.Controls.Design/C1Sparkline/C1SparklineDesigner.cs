﻿using System.Web.UI.Design;
using System.Linq;

namespace C1.Web.Wijmo.Controls.Design.C1Sparkline
{
    using C1.Web.Wijmo.Controls.C1Sparkline;
    using C1.Web.Wijmo.Controls.Design.C1ChartCore;

    [SupportsPreviewControl(true)]
    internal class C1SparklineDesigner : C1ChartCoreDesigner
    {
		internal override C1ChartCoreDesignerActionList CreateChartCoreDesignerActionList()
		{
			return new C1SparklineDesignerActionList(this);
		}

        protected override void AddSampleData()
        {
            var sparkline = Component as C1Sparkline;
            var series = new SparklineSeries();
            double[] data = { 33, 11, 15, 26, 16, 27, 37, -13, 8, -8, -3, 17, 0, 22, -13, -29, 19, 8 };
            series.Data = data.ToList();
            sparkline.SeriesList.Add(series);
        }
    }

	internal class C1SparklineDesignerActionList : C1ChartCoreDesignerActionList
	{
		public C1SparklineDesignerActionList(C1ChartCoreDesigner designer) : base(designer)
		{
		}

		internal override bool SupportAnnotations
		{
			get
			{
				return false;
			}
		}

		internal override bool SupportSeriesStyles
		{
			get
			{
				return false;
			}
		}
	}
}
