﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace C1.Web.Wijmo.Controls.Design.C1Menu
{
	using C1.Web.Wijmo.Controls.C1Menu;
	using System.IO;
	using C1.Web.Wijmo.Controls.Design.Localization;
	public partial class C1MenuDesignerForm : C1BaseItemEditorForm
	{

		#region ** custom element types 

		/// <summary>
		/// Element Types enumerations used by Designer
		/// </summary>
		private enum ItemType
		{
			/// <summary>
			/// Indicates what default item type must be used
			/// </summary>
			None,
			/// <summary>
			/// Root element of hierarchical component
			/// </summary>
			Menu,
			/// <summary>
			/// Default C1MenuItem type
			/// </summary>
			MenuLinkItem,
			/// <summary>
			/// Indicates that Separator property value from C1MenuItem is true
			/// </summary>
			MenuSeparator,
			/// <summary>
			/// Indicates that Header property value from C1MenuItem is true
			/// </summary>
			MenuHeader
		}

		private static string _menu_DefaultNodeText = "C1Menu";
		private static string _linkItem_DefaultNodeText = "LinkItem";
		private static string _header_DefaultNodeText = "Header";
		private static string _group_DefaultNodeText = "Group";
		private static string _separator_DefaultNodeText = "Separator";



		/// <summary>
		/// Creates a new item which its type depends on itemsInfo.
		/// </summary>
		/// <param name="itemInfo">The info of new item to be created.</param>
		/// <returns>New C1MenuItem</returns>
		private C1MenuItem CreateNewItem(C1ItemInfo itemInfo)
		{
			C1MenuItem item = null;
			item = new C1MenuItem();
			ItemType type = (ItemType)itemInfo.ItemType;
			if (type == ItemType.MenuHeader)
			{
				item.Header = true;
			}
			else if(type== ItemType.MenuSeparator)
			{
				item.Separator = true;
			}
			return item;
		}

		private string GetNodeText(C1MenuItem menuItem)
		{

			if (menuItem.Separator)
			{
				return "Separator";
			}
			else if(menuItem.Text!=null)
			{
				return menuItem.Text;
			}
			return "Unknown";
		}

		/// <summary>
		/// Gets an automatic name for an <typeparamref name="itemType"/>
		/// </summary>
		/// <param name="prefix"></param>
		/// <param name="nodeCollection">Gets access to the count of its parent's nodes</param>
		/// <returns>
		/// New automatic name for given <typeparamref name="itemType"/>
		/// </returns>
		protected override string GetNextItemTextId(string prefix, TreeNodeCollection nodeCollection)
		{
			if (prefix == _group_DefaultNodeText)
				return _group_DefaultNodeText;
			else if (prefix == _separator_DefaultNodeText)
				return _separator_DefaultNodeText;
			else
				return base.GetNextItemTextId(prefix, nodeCollection);
		}

		/// <summary>
		/// Gets the type of the given item
		/// </summary>
		/// <param name="item">Given item for checking its type</param>
		/// <returns>The <seealso cref="ItemType"/> of given item</returns>
		private ItemType GetItemType(object item)
		{
			// Sets the default item type
			ItemType elementType = ItemType.MenuLinkItem;

			if (item is C1Menu)
			{
				elementType = ItemType.Menu;
			}
			else if (item is C1MenuItem)
			{
				C1MenuItem menuItem = (C1MenuItem)item;
				if (menuItem.Separator)
				{
					elementType = ItemType.MenuSeparator;
				}
				else if (menuItem.Header)
				{
					elementType = ItemType.MenuHeader;
				}
				else
				{
					elementType = ItemType.MenuLinkItem;
				}
			}
			return elementType;
		}
		#endregion  // Custom Element Types enumeration

		#region ** fields
		// C1Menu local control
		private C1Menu _menu;
		private C1MenuDesigner _designer;
		// clipboard for current control
		private Stream _clipboardData;
		private C1ItemInfo _clipboardType;
		#endregion

		#region ** constructor        
		public C1MenuDesignerForm(C1Menu menu, C1MenuDesigner designer)
			: base(menu)
		{
			try
			{
				this._menu = menu;
				this._designer = designer;
				InitializeComponent();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}
		#endregion

		#region ** methods
		/// <summary>
		/// Form loading
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void C1MenuDesignerForm_Load(object sender, EventArgs e)
		{
		}

		/// <summary>
		/// Called to initialize the treeview with the object hierarchy.
		/// </summary>
		/// <param name="mainTreeView"></param>
		protected override void LoadControl(TreeView mainTreeView)
		{
			string nodeText;
			object nodeTag;
			C1ItemInfo itemInfo;

			// populate mainTreeView with C1 control items
			mainTreeView.BeginUpdate();
			mainTreeView.Nodes.Clear();

			TreeNode rootMenuItem = new TreeNode();
			itemInfo = GetItemInfo(GetItemType(_menu));
			nodeText = _menu.ID == null ? itemInfo.DefaultNodeText : _menu.ID.ToString();
			nodeTag = new C1NodeInfo(_menu, itemInfo, nodeText);
			SetNodeAttributes(rootMenuItem, nodeText, nodeTag);

			foreach (C1MenuItem menuItem in _menu.Items)
			{
				TreeNode menuItemNode = new TreeNode();
				nodeText = GetNodeText(menuItem);
				itemInfo = GetItemInfo(GetItemType(menuItem));
				nodeTag = new C1NodeInfo(menuItem, itemInfo, nodeText);
				SetNodeAttributes(menuItemNode, nodeText, nodeTag);

				IterateChildNodes(menuItemNode, menuItem);

				rootMenuItem.Nodes.Add(menuItemNode);
			}

			mainTreeView.Nodes.Add(rootMenuItem);
			mainTreeView.SelectedNode = mainTreeView.Nodes[0];
			mainTreeView.ExpandAll();
			mainTreeView.EndUpdate();
		}

		/// <summary>
		/// Loads nested items from current item
		/// </summary>
		/// <param name="menuItemNode">TreeNode parent</param>
		/// <param name="item">Control item</param>
		private void IterateChildNodes(TreeNode menuItemNode, C1MenuItem item)
		{
			string nodeText;
			object nodeTag;
			C1ItemInfo itemInfo;

			menuItemNode.Nodes.Clear();
			foreach (C1MenuItem menuItem in item.Items)
			{
				TreeNode itemNode = new TreeNode();
				nodeText = GetNodeText(menuItem);
				itemInfo = GetItemInfo(GetItemType(menuItem));
				nodeTag = new C1NodeInfo(menuItem, itemInfo, nodeText);
				SetNodeAttributes(itemNode, nodeText, nodeTag);

				IterateChildNodes(itemNode, menuItem);

				menuItemNode.Nodes.Add(itemNode);
			}
		}

		/// <summary>
		/// Gets de item info according to the given item type
		/// </summary>
		/// <param name="itemType">Item type</param>
		/// <returns>C1ItemInfo object that contains the item info</returns>
		private C1ItemInfo GetItemInfo(ItemType itemType)
		{
			foreach (C1ItemInfo itemInfo in base.ItemsInfo)
			{
				if ((ItemType)itemInfo.ItemType == itemType)
					return itemInfo;
			}
			return null;
		}

		/// <summary>
		/// Fills the allowable types that can compose the control. 
		/// You should load default item in first place. 
		/// </summary>
		/// <param name="itemInfo">List of available control items to be filled</param>
		protected override List<C1ItemInfo> FillAvailableControlItems()
		{
			List<C1ItemInfo> itemsInfo = new List<C1ItemInfo>();
			C1ItemInfo item;

			item = new C1ItemInfo();
			item.ItemType = ItemType.MenuLinkItem;
			item.EnableChildItems = true;
			item.ContextMenuStripText = C1Localizer.GetString("C1Menu.MenuText.LinkItem");
			//item.ContextMenuStripText = "Link Item";
			item.NodeImage = Properties.Resources.LinkItemIco; //GetImageFromResource("LinkItemIco"); 
			item.DefaultNodeText = _linkItem_DefaultNodeText;
			item.Visible = true;
			item.Default = true;
			itemsInfo.Add(item);

			item = new C1ItemInfo();
			item.ItemType = ItemType.Menu;
			item.EnableChildItems = true;
			item.ContextMenuStripText = "C1Menu";
			//item.ContextMenuStripText = "C1Menu";
			item.NodeImage = Properties.Resources.RootItemIco; //GetImageFromResource("RootItemIco");
			item.DefaultNodeText = _menu_DefaultNodeText;
			item.Visible = false;
			item.Default = false;
			itemsInfo.Add(item);

			item = new C1ItemInfo();
			item.ItemType = ItemType.MenuHeader;
			item.EnableChildItems = false;
			item.ContextMenuStripText = C1Localizer.GetString("C1Menu.MenuText.Header");
			//item.ContextMenuStripText = "Header";
			item.NodeImage = Properties.Resources.HeaderIco; //GetImageFromResource("HeaderIco");
			item.DefaultNodeText = _header_DefaultNodeText;
			item.Visible = true;
			item.Default = false;
			itemsInfo.Add(item);


			item = new C1ItemInfo();
			item.ItemType = ItemType.MenuSeparator;
			item.EnableChildItems = false;
			item.ContextMenuStripText = C1Localizer.GetString("C1Menu.MenuText.Separator");
			//item.ContextMenuStripText = "Separator";
			item.NodeImage = Properties.Resources.SeparatorIco; //GetImageFromResource("SeparatorIco"); 
			item.DefaultNodeText = _separator_DefaultNodeText;
			item.Visible = true;
			item.Default = false;
			itemsInfo.Add(item);

			return itemsInfo;
		}

#if false
		/// <summary>
		/// Wires up the property grid when a node gets selected
		/// </summary>
		/// <param name="mainTreeView">TreeView that contains control nodes</param>
		/// <param name="mainPropertyGrid">Main Property grid to be wired up to given mainTreeView</param>
		protected override void OnWiresUpPropertyGrid(TreeView mainTreeView, PropertyGrid mainPropertyGrid)
		{
			C1NodeInfo nodeInfo = (C1NodeInfo)mainTreeView.SelectedNode.Tag;

			if ((ItemType)nodeInfo.ItemInfo.ItemType == ItemType.Menu)
			{
				C1.Web.C1Menu.C1Menu menu = (C1.Web.C1Menu.C1Menu)nodeInfo.Element;
				mainPropertyGrid.SelectedObject = menu;
			}
			else
			{
				C1.Web.C1Menu.C1MenuItem menuItem = (C1.Web.C1Menu.C1MenuItem)nodeInfo.Element;
				mainPropertyGrid.SelectedObject = menuItem;
			}
		}
#endif

		protected override void SyncUI(C1ItemInfo itemInfo, C1ItemInfo parentItemInfo, C1ItemInfo previousItemInfo)
		{
			base.AllowAdd = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Menu, ItemType.MenuLinkItem });
			base.AllowInsert = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.MenuLinkItem, ItemType.MenuHeader, ItemType.MenuSeparator });
			base.AllowChangeType = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.MenuLinkItem, ItemType.MenuHeader, ItemType.MenuSeparator });
			base.AllowCopy = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.MenuLinkItem, ItemType.MenuHeader, ItemType.MenuSeparator});
			base.AllowPaste = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Menu, ItemType.MenuLinkItem });
			base.AllowCut = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.MenuLinkItem, ItemType.MenuHeader, ItemType.MenuSeparator });
			base.AllowDelete = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.MenuLinkItem, ItemType.MenuHeader, ItemType.MenuSeparator });
			base.AllowRename = EnableActionByItemType(itemInfo, new ItemType[] { ItemType.Menu, ItemType.MenuLinkItem, ItemType.MenuHeader });
			base.AllowMoveUp = EnableMoveUp();
			base.AllowMoveDown = EnableMoveDown();
			base.AllowMoveLeft = EnableMoveLeft(itemInfo, parentItemInfo);
			base.AllowMoveRight = EnableMoveRight(itemInfo, previousItemInfo);

			base.SyncUI(itemInfo, parentItemInfo, previousItemInfo);
		}

		/// <summary>
		/// Enables given action if it is in given types.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo info.</param>
		/// <param name="types">Allowed types for given action.</param>
		private bool EnableActionByItemType(C1ItemInfo itemInfo, ItemType[] types)
		{
			bool enable = false;
			if (itemInfo != null)
			{
				foreach (ItemType type in types)
				{
					enable |= itemInfo.ItemType.Equals(type);
				}
			}
			return enable;
		}

		/// <summary>
		/// Enables current node to be added as a child of its available node to the left
		/// </summary>
		private bool EnableMoveLeft(C1ItemInfo itemInfo, C1ItemInfo parenItemInfo)
		{
			if (parenItemInfo == null || ((ItemType)parenItemInfo.ItemType).Equals(ItemType.Menu)) // Root node
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		/// <summary>
		/// Enables current node to be added as a child of its available node to the right
		/// </summary>
		private bool EnableMoveRight(C1ItemInfo itemInfo, C1ItemInfo previousItemInfo)
		{
			if (previousItemInfo != null && previousItemInfo.EnableChildItems) // Root node
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private bool EnableMoveUp()
		{
			return true;
		}

		private bool EnableMoveDown()
		{
			return true;
		}

		/// <summary>
		/// Creates the C1NodeInfo object that will contain both the specific C1 control item depending on the itemInfo
		/// and given itenInfo.
		/// </summary>
		/// <param name="itemInfo">C1ItemInfo object.</param>
		/// <param name="nodesList">Nodes that currently exist into the list where the new created item 
		/// will be inserted.</param>
		/// <returns>Returns a C1 control that depends on the C1ItemInfo. Given nodesList
		/// could be used to obtain a new numbered name.</returns>
		protected override C1NodeInfo CreateNodeInfo(C1ItemInfo itemInfo, TreeNodeCollection nodesList)
		{
			C1NodeInfo nodeInfo;
			string nodeText;

			C1MenuItem item = CreateNewItem(itemInfo);

			// itemInfo.DefaultNodeText could be used as default item text. if not, you can get a new numbered name.
			// item.Text = itemInfo.DefaultNodeText;
			nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, nodesList);

			// set the text for created item
			if (!item.Separator)
			{
				((C1MenuItem)item).Text = nodeText;
			}

			// create a new node info
			nodeInfo = new C1NodeInfo(item, itemInfo, nodeText);

			// set nodeText property
			nodeInfo.NodeText = nodeText;

			return nodeInfo;
		}

		/// <summary>
		///  Indicates when an item was inserted in a specific position.
		/// </summary>
		/// <param name="item">The inserted item.</param>
		/// <param name="destinationItem">The destination item that will contain inserted item.</param>
		/// <param name="destinationIndex">The index position of given item within destinationItem items collection.</param>
		protected override void Insert(object item, object destinationItem, int destinationIndex)
		{
			try
			{
				((IC1MenuItemCollectionOwner)destinationItem).Items.Insert(destinationIndex, (C1MenuItem)item);
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		/// <summary>
		///  Indicates when an item was deleted.
		/// </summary>
		/// <param name="item">The deleted item.</param>
		/// <param name="parent">The parent that contains the deleted item.</param>
		protected override void Delete(object item)
		{
			try
			{
				((IC1MenuItemCollectionOwner)item).Owner.Items.Remove((C1MenuItem)item);
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show(ex.Message + "," + ex.StackTrace);
			}
		}

		protected override void Copy(object item, C1ItemInfo itemInfo)
		{
			if (_clipboardData == null)
				_clipboardData = new MemoryStream();
			Copy(_clipboardData, item);

			this._clipboardType = itemInfo;
			// tell to base there is some data into clipboard
			base.ClipboardData = true;
		}

		private void Copy(Stream buffer, object target)
		{
			C1MenuSerializer serializer = new C1MenuSerializer(target);
			buffer.SetLength(0);
			serializer.SaveLayout(buffer);
		}

		protected override void Paste(TreeNode destinationNode)
		{
			Paste(_clipboardData, destinationNode);
		}

		private void Paste(Stream buffer, TreeNode destNode)
		{
			C1MenuItem item = CreateNewItem(((C1ItemInfo)this._clipboardType));
			C1MenuSerializer serializer = new C1MenuSerializer(item);
			buffer.Seek(0, SeekOrigin.Begin);
			// lets to serializer to retrieve data from clipboard
			serializer.LoadLayout(buffer, LayoutType.All);

			// retrieve the C1MenuItem from destination node
			object parentObject = ((C1NodeInfo)destNode.Tag).Element;
			// add deserialized item
			((IC1MenuItemCollectionOwner)parentObject).Items.Add((C1MenuItem)item);

			// create node info
			C1NodeInfo nodeInfo = new C1NodeInfo(item, _clipboardType, GetNodeText(item));

			// create new treenode to be added on destination node
			TreeNode node = new TreeNode();
			// set node text and node tag
			SetNodeAttributes(node, nodeInfo.NodeText, nodeInfo);
			// load child nodes
			IterateChildNodes(node, item);

			destNode.Nodes.Add(node);
			destNode.ExpandAll();
		}

		const string NEW_LINE = "\r\n";

		internal override void ShowPreviewHtml(C1WebBrowser previewer)
		{
			try
			{

				string sResultBodyContent = "";
				StringBuilder sb = new StringBuilder();
				System.IO.StringWriter tw = new System.IO.StringWriter(sb);
				System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
				//System.Windows.Forms.MessageBox.Show("_menu.Items.Count:" + _menu.Items.Count + ", _menu.Controls.Count=" + _menu.Controls.Count);
				_menu.IsDirty = true;
				_menu.RenderControl(htw);
				sResultBodyContent = sb.ToString();


				/*
				string sResultBodyContent = _designer.GetDesignTimeHtml();
				*/

				string sDocumentContent = "";
				sDocumentContent += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + NEW_LINE;
				sDocumentContent += "<html  xmlns=\"http://www.w3.org/1999/xhtml\">" + NEW_LINE;
				sDocumentContent += "<head>" + NEW_LINE;
				sDocumentContent += "</head>" + NEW_LINE;
				sDocumentContent += "<body>" + NEW_LINE;
				sDocumentContent += sResultBodyContent + NEW_LINE;
				sDocumentContent += "</body>" + NEW_LINE;
				sDocumentContent += "</html>" + NEW_LINE;
				//previewer.Document.Write(sDocumentContent);
				previewer.DocumentWrite(sDocumentContent, this._menu);
				//string s = sResultBodyContent.Substring(sResultBodyContent.IndexOf("<style"), sResultBodyContent.LastIndexOf("</style>") - sResultBodyContent.IndexOf("<style") + 8);
				//MessageBox.Show(sResultBodyContent.Replace(s,""));
				//MessageBox.Show(sResultBodyContent);
				//HtmlDocument doc = previewer.Document.OpenNew(true);
				//doc.Write(sDocumentContent);

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("previewer.Document=" + (previewer.Document == null) + "?" + ex.Message + ",,,," + ex.StackTrace);
			}
		}

		protected override void SaveToXML(string fileName)
		{
			try
			{
				_menu.SaveLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		protected override void LoadFromXML(string fileName)
		{
			try
			{
				_menu.LoadLayout(fileName);
			}
			catch (Exception e)
			{
				MessageBox.Show(this, e.Message, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		#endregion

		#region Utils

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		/// <param name="selectedNode"></param>
		protected override void OnTreeViewAfterLabelEdit(NodeLabelEditEventArgs e, TreeNode selectedNode)
		{
			bool cancel = e.CancelEdit;
			FinishLabelEdit(e.Node, e.Label, ref cancel);
			e.CancelEdit = cancel;
		}

		private void FinishLabelEdit(TreeNode node, string text, ref bool cancelEdit)
		{
			string nodeText = "";
			string property = "Text";
			object obj = ((C1NodeInfo)node.Tag).Element;
			C1ItemInfo itemInfo = ((C1NodeInfo)node.Tag).ItemInfo;

			if (text != null) //&& text.CompareTo(_emptyItemText) != 0
			{
				nodeText = text;
			}
			else
			{
				if (obj is C1Menu)
				{
					nodeText = ((C1Menu)obj).ID == null ? "" : ((C1Menu)obj).ID.ToString();
				}
				else if (obj is C1MenuItem)
				{
					//if (!string.IsNullOrEmpty(((C1MenuItemBase)obj).Text))
					nodeText = GetNodeText(((C1MenuItem)obj));

					if (string.IsNullOrEmpty(nodeText))
					{
						if (node.Parent == null)
						{
							nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Nodes);
						}
						else
						{
							nodeText = GetNextItemTextId(itemInfo.DefaultNodeText, node.Parent.Nodes);
						}
					}
				}
			}

			if (obj is C1Menu)
				property = "ID";
			else if (obj is C1MenuItem)
				property = "Text";

			// set new text value to selected object and property grid
			TypeDescriptor.GetProperties(obj)[property].SetValue(obj, nodeText);
			base.RefreshPropertyGrid();
			node.Text = nodeText;
		}

		// Gets image from resource. This method is used just only for testing purposes.
		// This form must be moved to C1.Web.UI.Design.2 assembly when designer is finished.
		/*internal static System.Drawing.Image GetImageFromResource(string imageName)
		{
			string resId;
			Stream stream;
			resId = string.Format("C1.Web.UI.Controls.C1Menu.Resources.{0}.png", imageName);
			stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resId);
			if (stream != null)
				return System.Drawing.Image.FromStream(stream);
			return null;
		}*/

		#endregion
	}
}
