﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1Menu
{
    using C1.Web.Wijmo.Controls.C1Menu;
    using System.Web.UI.WebControls;
    using System.Windows.Forms;
    using System.ComponentModel.Design;
    using C1.Web.Wijmo.Controls.Design.Localization;
    using System.Windows.Forms.Design;
    using C1.Web.Wijmo.Controls.Design.Utils;

    /// <summary>
    ///  Menu Designer
    /// </summary>
    [SupportsPreviewControl(true)]
    public class C1MenuDesigner : C1HierarchicalDataBoundControlDesigner
    {

        #region ** fields
        private C1Menu _control;
        private TemplateGroupCollection _templateGroups;
        private static readonly string[] _templateNames;
        #endregion

        #region ** structor
        static C1MenuDesigner()
        {
            _templateNames = new string[] { "ItemsTemplate", "TopItemsTemplate", "ChildItemsTemplate" };
        }
        #endregion

        #region ** override methods

        #region ** properties
        /// <summary>
        /// Allow resize.
        /// </summary>
        public override bool AllowResize
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Save and Restore Original data

        /// <summary>
        /// TreeData is used to save any object with hierarchical structure
        /// </summary>
        private class TreeData
        {
            public TreeData(object dataObj)
            {
                _dataObj = dataObj;
                _items = new List<TreeData>();
            }

            public List<TreeData> ItemsData
            {
                get
                {
                    return _items;
                }
            }

            public object DataObj
            {
                get { return _dataObj; }
            }

            private readonly object _dataObj;
            private List<TreeData> _items;
        }

        private void SaveDataItem(List<TreeData> parent, object item)
        {
            TreeData itemData = new TreeData(item);
            parent.Add(itemData);
            if (item is C1MenuItem)
            {
                foreach (C1MenuItem it in ((C1MenuItem)item).Items)
                    SaveDataItem(itemData.ItemsData, it);
            }
        }

        /// <summary>
        /// Save original control data 
        /// </summary>
        /// <param name="control">Control to be saved</param>
        /// <returns>Returns a list of <see cref="TreeData"/></returns>
        private List<TreeData> SaveOriginalControlData(object control)
        {
            List<TreeData> savedData = new List<TreeData>();
            if (control is C1Menu)
            {
                C1Menu menu = (C1Menu)control;

                foreach (C1MenuItem item in ((C1Menu)control).Items)
                {
                    SaveDataItem(savedData, item);
                }
            }
            return savedData;
        }

        /// <summary>
        /// Restores item data
        /// </summary>
        /// <param name="parent">Object to be restored</param>
        /// <param name="data">Data to be restored to parent object</param>
        private void RestoreDataItem(object parent, TreeData data)
        {
            if (parent is C1MenuItem)
            {
                foreach (TreeData d in data.ItemsData)
                {
                    C1MenuItem item = (C1MenuItem)d.DataObj;
                    item.Items.Clear();
                    ((C1MenuItem)parent).Items.Add(item);
                    RestoreDataItem(item, d);
                }
            }
        }

        /// <summary>
        /// Restores saved data to original state when edition was canceled
        /// </summary>
        /// <param name="control">Control to be restored</param>
        /// <param name="originalData">Original Data</param>
        private void RestoreOriginalData(object control, List<TreeData> originalData)
        {
            if (control is C1Menu)
            {
                ((C1Menu)control).Items.Clear();
                foreach (TreeData d in originalData)
                {
                    C1MenuItem item = (C1MenuItem)d.DataObj;
                    item.Items.Clear();
                    ((C1Menu)control).Items.Add(item);
                    RestoreDataItem(item, d);
                }
            }
        }

        private void ClearOriginalData(List<TreeData> originalData)
        {
            originalData.Clear();
        }


        #endregion


        public override void Initialize(IComponent control)
        {
            base.Initialize(control);
            //this._control = (C1Menu)component;
            this._control = (C1Menu)control;
            SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
            base.SetViewFlags(ViewFlags.TemplateEditing, true);
            UpdateDesignTimeHtml();
        }

        public override string GetDesignTimeHtml()
        {
            if (this._control is C1Menu)
            {
                C1Menu menu = (C1Menu)this._control;

                if (menu.WijmoControlMode == WijmoControlMode.Mobile)
                {
                    //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                    return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
                }
                if (menu.Items.Count == 0
                    &&
                    string.IsNullOrEmpty(menu.DataSourceID))
                {
                    return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", C1Localizer.GetString("C1Menu.Empty"));
                }
            }
            return base.GetDesignTimeHtml();
            
        }

        /// <summary>
        /// TemplateGroups implementation.
        /// </summary>
        public override TemplateGroupCollection TemplateGroups
        {
            get
            {
                TemplateGroupCollection templateGroups = base.TemplateGroups;
                if (this._templateGroups == null)
                {
                    this._templateGroups = new TemplateGroupCollection();
                    TemplateGroup group = new TemplateGroup("CommonItemTemplates", ((WebControl)base.ViewControl).ControlStyle);
                    TemplateDefinition templateDefinition = new TemplateDefinition(this, _templateNames[0], this._control, _templateNames[0], false);
                    templateDefinition.SupportsDataBinding = true;
                    group.AddTemplateDefinition(templateDefinition);
                    TemplateDefinition definition2 = new TemplateDefinition(this, _templateNames[1], this._control, _templateNames[1], false);
                    definition2.SupportsDataBinding = true;
                    group.AddTemplateDefinition(definition2);
                    TemplateDefinition definition3 = new TemplateDefinition(this, _templateNames[2], this._control, _templateNames[2], false);
                    definition3.SupportsDataBinding = true;
                    group.AddTemplateDefinition(definition3);
                    this._templateGroups.Add(group);
                }
                templateGroups.AddRange(this._templateGroups);
                return templateGroups;
            }
        }

        protected override bool UsePreviewControl
        {
            get
            {
                return true;
            }
        }

        #endregion

        #region ** public methods
        public bool EditItems()
        {
            bool accept = false;

            // Save original control data, to restore data in case of DialogResult is 'Cancel'
            List<TreeData> originalData = SaveOriginalControlData(this._control);

            C1MenuDesignerForm _editorForm = new C1MenuDesignerForm(this._control, this);
            accept = ShowControlEditorForm(this._control, _editorForm);

            if (!accept)
                RestoreOriginalData(this._control, originalData);
            ClearOriginalData(originalData);

            return accept;
        }

        public bool EditBindings()
        {
            C1MenuBindingsEditorForm _editorForm = new C1MenuBindingsEditorForm();
            _editorForm.SetComponent(this._control);
            return ShowControlEditorForm(this._control, _editorForm);
        }

        public bool ShowControlEditorForm(object control, Form _editorForm)
        {
            DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1Menu.EditControl"));
                //DesignerTransaction trans = host.CreateTransaction("EditControl");
                using (trans)
                {
                    IUIService service = (IUIService)serviceProvider.GetService(typeof(IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(_editorForm);
                    }
                    else
                        dr = DialogResult.None;
                    if (dr == DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = _editorForm.ShowDialog();

            _editorForm.Dispose();
            _editorForm = null;

            return dr == DialogResult.OK;
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection lists = new DesignerActionListCollection();
                lists.AddRange(base.ActionLists);
                lists.Add(new C1MenuActionList(this, this.Component));
                return lists;
            }
        }
        #endregion
    }

    internal class C1MenuActionList : DesignerActionListBase
    {
        // Fields
        private C1MenuDesigner _designer;

        // Methods
        public C1MenuActionList(C1MenuDesigner designer, IComponent component)
            : base(designer)
        {
            this._designer = designer;
        }

        public void EditItems()
        {
            this._designer.EditItems();
        }

        public void EditBindings()
        {
            this._designer.EditBindings();
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection col = new DesignerActionItemCollection();
            col.Add(new DesignerActionMethodItem(this, "EditItems", C1Localizer.GetString("C1Menu.SmartTag.EditMenu"), "", C1Localizer.GetString("C1Menu.SmartTag.EditMenuDescription"), true));
            col.Add(new DesignerActionMethodItem(this, "EditBindings", C1Localizer.GetString("C1Menu.SmartTag.EditMenuBindings"), "", C1Localizer.GetString("C1Menu.SmartTag.EditMenuBindingsDescription"), true));
            AddBaseSortedActionItems(col);
            return col;
        }


    }
}
