﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Design.C1ChartCore;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1BubbleChart
{
	using C1.Web.Wijmo.Controls.C1Chart;
	public class C1BubbleChartDesigner : C1ChartCoreDesigner
	{
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
		}

		protected override void AddSampleData()
		{
			C1BubbleChart bubbleChart = Component as C1BubbleChart;
			BubbleChartSeries series = new BubbleChartSeries();
			series.Label = "SampleData";
			series.Data.X.AddRange(new double[] { 1, 2, 3, 4, 5 });
			series.Data.Y.AddRange(new double[] { 20, 22, 19, 24, 25 });
			series.Data.Y1.AddRange(new double[] { 15, 5, 20, 17, 25 });
			bubbleChart.SeriesList.Add(series);
		}
	}
}
