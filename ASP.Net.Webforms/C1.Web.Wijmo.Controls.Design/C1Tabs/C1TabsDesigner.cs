using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.IO;



namespace C1.Web.Wijmo.Controls.Design.C1Tabs
{
    using C1.Web.Wijmo.Controls.C1Tabs;
    using C1.Web.Wijmo.Controls.Design.Localization;


    [SupportsPreviewControl(false)]
    public class C1TabsDesigner : C1ControlDesinger
    {
        C1Tabs _tabs;

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this._tabs = (C1Tabs)component;
        }

        private DesignerActionListCollection _actions;
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (_actions == null)
                {
                    _actions = new DesignerActionListCollection();
                    _actions.AddRange(base.ActionLists);
                    _actions.Add(new C1TabsActionList(this));
                }
                return _actions;
            }
        }

        public void OpenBuilder()
        {
            List<C1TabPage> originalData = new List<C1TabPage>();
            foreach (C1TabPage tp in _tabs.Pages)
            {
                originalData.Add(tp);
            }

            C1TabsDesignerForm editorForm = new C1TabsDesignerForm(this._tabs);
            bool accept = ShowControlEditorForm(this._tabs, editorForm);

            if (accept)
                this.UpdateDesignTimeHtml();
            else
            {
                _tabs.Pages.Clear();
                foreach (C1TabPage tp in originalData)
                {
                    _tabs.Pages.Add(tp);
                }
            }
        }

        public bool ShowControlEditorForm(object control, System.Windows.Forms.Form editorForm)
        {
            System.Windows.Forms.DialogResult dr;
            IServiceProvider serviceProvider = ((IComponent)control).Site;
            if (serviceProvider != null)
            {
                IDesignerHost host = (IDesignerHost)serviceProvider.GetService(typeof(IDesignerHost));
                DesignerTransaction trans = host.CreateTransaction(C1Localizer.GetString("C1Tabs.EditControl"));
                using (trans)
                {
                    System.Windows.Forms.Design.IUIService service = (System.Windows.Forms.Design.IUIService)serviceProvider.GetService(typeof(System.Windows.Forms.Design.IUIService));
                    IComponentChangeService ccs = (IComponentChangeService)serviceProvider.GetService(typeof(IComponentChangeService));

                    if (service != null)
                    {
                        ccs.OnComponentChanging(control, null);
                        dr = service.ShowDialog(editorForm);
                    }
                    else
                        dr = System.Windows.Forms.DialogResult.None;
                    if (dr == System.Windows.Forms.DialogResult.OK)
                    {
                        ccs.OnComponentChanged(control, null, null, null);
                        trans.Commit();
                    }
                    else
                    {
                        trans.Cancel();
                    }
                }
            }
            else
                dr = editorForm.ShowDialog();

            editorForm.Dispose();
            editorForm = null;

            return dr == System.Windows.Forms.DialogResult.OK;
        }

        public override String GetDesignTimeHtml(DesignerRegionCollection regions)
        {
            if (this._tabs.WijmoControlMode == WijmoControlMode.Mobile)
            {
                //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
            }
            if (this._tabs.Pages.Count == 0)
            {
                string s = "<div style=\"width:{0};height:{1};border:solid 1px #bbbbbb;\"></div>";
                s = string.Format(s, 
                    _tabs.Width.IsEmpty ? "300px" : _tabs.Width.ToString(), 
                    _tabs.Height.IsEmpty ? "250px" : _tabs.Height.ToString()
                    );
                return s;
            }

            for (int i = 0; i < this._tabs.Pages.Count; i++)
            {
                DesignerRegion region = new DesignerRegion(this, i.ToString(), true);
                region.EnsureSize = true;
                regions.Add(region);
            }

            if (this._tabs.SelectedPage != null)
                this._tabs.SelectedPage.Attributes.Add(DesignerRegion.DesignerRegionAttributeName, regions.Count.ToString());

            regions.Add(new EditableDesignerRegion(this, "PageContent", false));
            string result = base.GetDesignTimeHtml(regions);
            return result;
        }

        /// <summary>
        /// Called when we get a click on a designer region.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClick(DesignerRegionMouseEventArgs e)
        {
            if (e.Region != null)
            {
                if (e.Region.Name == "PageContent") return;

                int index = 0;
                try
                {
                    index = int.Parse(e.Region.Name);
                }
                catch
                {
                }

                if (this._tabs.Selected != index)
                {
                    this._tabs.Selected = index;
                    base.UpdateDesignTimeHtml();
                }
            }
        }

        private C1TabPage _editingPage = null;
        public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
        {
            _editingPage = null;
            // Get a reference to the designer host
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host == null || _tabs == null) return string.Empty;

            _editingPage = _tabs.SelectedPage;
            if (_editingPage == null) return string.Empty;

            TextWriter strWriter = new StringWriter();
            foreach (Control c in _editingPage.Controls)
            {
                ControlPersister.PersistControl(strWriter, c, host);
            }

            string s = strWriter.ToString();
            return s;
        }

        public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
        {
            if (content == null) return;

            // Get a reference to the designer host
            IDesignerHost host = (IDesignerHost)Component.Site.GetService(typeof(IDesignerHost));
            if (host == null) return;

            if (_tabs == null) return;
            if (_editingPage == null) return;

            using (DesignerTransaction transaction = host.CreateTransaction(C1Localizer.GetString("C1Tabs.SetEditableDesignerRegionContent")))
            {
                _editingPage.Controls.Clear();
                if (!string.IsNullOrEmpty(content))
                {
                    Control[] ctrls = ControlParser.ParseControls(host, content);
                    foreach (Control c in ctrls)
                    {
                        _editingPage.Controls.Add(c);
                    }
                }

                transaction.Commit();
            }
        }

        internal void NotifyDirty()
        {
            if (_tabs == null) return;

            IComponentChangeService changeSvc =(IComponentChangeService)this.GetService(typeof(IComponentChangeService));
            if (changeSvc != null)
            {
                changeSvc.OnComponentChanging(_tabs, null);
                changeSvc.OnComponentChanged(_tabs, null, null, null);
            }
        }
    }


    internal class C1TabsActionList : DesignerActionListBase
    {
        #region Fields

        private C1Tabs _tabs;
        private DesignerActionItemCollection _actions;

        #endregion

        #region Constructor

        public C1TabsActionList(C1TabsDesigner designer)
            : base(designer)
        {
            _tabs = designer.Component as C1Tabs;
        }

        #endregion

        #region Actions

        public void OpenBuilder()
        {
            C1TabsDesigner designer = this.Designer as C1TabsDesigner;
            designer.OpenBuilder();
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            if (_actions == null)
            {
                _actions = new DesignerActionItemCollection();
                _actions.Add(new DesignerActionMethodItem(this, "OpenBuilder", C1Localizer.GetString("C1Tabs.SmartTag.Designer"), "", C1Localizer.GetString("C1Tabs.SmartTag.DesignerDescription"), true));
                AddBaseSortedActionItems(_actions);
            }

            return _actions;
        }

        #endregion
    }
}
