﻿using C1.Web.Wijmo.Controls.Design.Localization;


namespace C1.Web.Wijmo.Controls.Design.C1Tabs
{
    partial class C1TabsDesignerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // C1TabControlDesignerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 516);
            this.Name = "C1TabControl Designer Form";
            this.Text = C1Localizer.GetString("C1TabControl.DesignerForm.Title");
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}