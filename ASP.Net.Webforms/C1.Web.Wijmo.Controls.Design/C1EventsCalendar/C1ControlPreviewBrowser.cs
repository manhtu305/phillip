﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design
{
	using C1.Web.Wijmo.Controls.C1Chart;
	using System.Runtime.InteropServices;
	//	using System.Web.UI;

	internal class C1ControlPreviewBrowser : WebBrowser
	{
		#region ** fields
		
		Regex rxscript = null;
		public delegate void ImageCompleteEventHandler(object sender, EventArgs e);
		public event ImageCompleteEventHandler ImageComplete;
		Image _image = null;		
		string _documentUrl = "";
		System.Web.UI.WebControls.WebControl _control = null;
		internal C1EventsCalendar.C1EventsCalendarDesigner _designer;
		internal WebFormsRootDesigner _rootDesigner;
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1ControlPreviewBrowser"/> class.
		/// </summary>
		/// <param name="designer">The designer.</param>
		/// <param name="rootDesigner">The root designer.</param>
		public C1ControlPreviewBrowser(C1EventsCalendar.C1EventsCalendarDesigner designer, 
			WebFormsRootDesigner rootDesigner)
		{			
			this._designer = designer;
			this._rootDesigner = rootDesigner;
			this._control = (System.Web.UI.WebControls.WebControl)designer.Component;

			this.ScrollBarsEnabled = false;
			this.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(C1ControlPreviewBrowser_DocumentCompleted);
			this.ScriptErrorsSuppressed = true;//fix for 19642
		}

		#endregion

		#region ** methods

		/// <summary>
		/// Updates the state of the preview.
		/// </summary>
		public void UpdatePreviewState()
		{
			if (!_control.Width.IsEmpty)
			{
				this.Width = (int)_control.Width.Value;
			}
			if (!_control.Height.IsEmpty)
			{
				this.Height = (int)_control.Height.Value;
			}
		}

		#endregion

		#region ** alternative image capture

	

		internal void RaiseImageComplete(Image im)
		{
			try
			{
				_image = im;
				ImageComplete(this, EventArgs.Empty);
			}
			catch { }
		}
		
		public void CaptureImage()
		{
			this._captureImage();
		}

		private void _captureImage()
		{
			UpdatePreviewState(); 
			
			if (_form == null)
			{
				_form = new CaptureForm(this);
				
				_form.Controls.Add(this);
				_form.ShowIcon = false;
				_form.ShowInTaskbar = false;				
				_form.Width = this.Width + 1000;
				_form.Height = this.Height + 1000;
				_form.StartPosition = FormStartPosition.Manual;
				_form.Left = -_form.Width;
				_form.Top = -_form.Height;			
				this.Dock = DockStyle.Fill;
				_form.FormClosed += new FormClosedEventHandler(_form_FormClosed);
			}
			_form.Show();
			//this.RaiseImageComplete(C1ControlPreviewBrowser.captureHiddenWindow(this.Handle));

		}


		void _form_FormClosed(object sender, FormClosedEventArgs e)
		{
			_form = null;
		}
		CaptureForm _form;
		
		#endregion

		private Assembly ControlAssembly
		{
			get
			{
				if (this._control != null)
				{
					return this._control.GetType().Assembly;
				}

				return Assembly.GetExecutingAssembly();
			}
		}


		void C1ControlPreviewBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{
			try
			{
				this._captureImage();
			}
			
#if DEBUG
			catch (Exception ex)
#else
			catch
#endif

			{
#if DEBUG
				MessageBox.Show(ex.Message + ";" + ex.StackTrace);
#else
			
#endif
			}
		}


		public void DocumentWrite(string docText, System.Web.UI.WebControls.WebControl control, string documentUrl)
		{
			this._control = control;
			this._documentUrl = documentUrl;
			string asmName = this.ControlAssembly.FullName;
			asmName = asmName.Replace(" ", "(?: |%20)");
			string stag = "(<script)[^>]*(src=\"mvwres://[^\"]*" + asmName + "[^\"]*\")[^>]*>\\s*(</script>)";
			rxscript = new Regex(stag, RegexOptions.IgnoreCase | RegexOptions.Singleline);
			SetFilteredDocumentText(docText);
		}


		public Image Image
		{
			get { return _image; }
		}

		private void SetFilteredDocumentText(string doctext)
		{

			this.replaceScriptResourceReferenceWithScript(ref doctext);
			Regex reg = new Regex("</body>", RegexOptions.RightToLeft);
			int count = reg.Matches(doctext).Count;
			doctext = reg.Replace(doctext, "<script>$(document).ready(function(){ window.isComplete = function(){return true}});</script></body>", 1);
			base.DocumentText = doctext;
			//base.Document.Body.InnerText += "<script>$(document).ready(function(){ window.isComplete = function(){ return true; }});</script>";
		}

		private void replaceExternalImage(ref string doctext, LineChartMarkerSymbol symbol)
		{
			string resname = symbol.Url;

			if (string.IsNullOrEmpty(resname))
			{
				return;
			}

			resname = ResolvePhysicalPath(_control, _documentUrl, resname);
			resname = "file:///" + resname.Replace("\\", "/");
			if (resname.EndsWith("/"))
				resname = resname.Substring(0, resname.Length - 1);
			doctext = doctext.Replace(_control.ResolveClientUrl(symbol.Url), resname);
		}

		private string getResourceTextData(string sResourceName)
		{
			string text = null;
			Stream rs = null;
			try
			{
				rs = this.ControlAssembly.GetManifestResourceStream(sResourceName);
				if (rs != null)
				{
					using (StreamReader sr = new StreamReader(rs))
					{
						text = sr.ReadToEnd();
					}
				}
			}
			catch { text = null; }
			finally { if (rs != null) rs.Close(); }
			return text;
		}

		private void replaceScriptResourceReferenceWithScript(ref string script)
		{
			// it must be expected that the script files are large and therefore
			// must not be searched after being added to the script.
			MatchCollection mc = rxscript.Matches(script);
			if (mc != null && mc.Count > 0)
			{
				StringBuilder sb = new StringBuilder();
				int start = 0;

				foreach (Match m in mc)
				{
					string resname = m.Groups[2].Value;
					if (resname.Contains("/") && resname.EndsWith("\""))
					{
						resname = resname.Substring(resname.LastIndexOf('/') + 1);
						resname = resname.Substring(0, resname.Length - 1);	// trim quote
						resname = getResourceTextData(resname);
						if (resname != null)
						{
							int g2i = m.Groups[2].Index;
							int g2e = g2i + m.Groups[2].Length;
							int g3i = m.Groups[3].Index;

							sb.Append(script.Substring(start, g2i - start));
							sb.Append(script.Substring(g2e, g3i - g2e));
							sb.Append(resname);
							sb.Append(m.Groups[3]);

							start = g3i + m.Groups[3].Length;
						}
					}
				}

				if (sb.Length > 0)
				{
					sb.Append(script.Substring(start));
					script = sb.ToString();
				}
			}
		}

		private static string ResolvePhysicalPath(System.Web.UI.Control control, string documentUrl, string url)
		{
			ISite site = GetSite(control);
			if (site == null) return url;

			IWebApplication service = (IWebApplication)site.GetService(typeof(IWebApplication));
			if (url.IndexOf("~/") > -1)
			{
				return url.Replace("~/", service.RootProjectItem.PhysicalPath);
			}

			Uri baseAbsoluteUri = new Uri(documentUrl.Replace("~/", service.RootProjectItem.PhysicalPath));
			string absolutePath = new Uri(baseAbsoluteUri, url).AbsolutePath;
			string docUrl = absolutePath.Replace(service.RootProjectItem.PhysicalPath.Replace("\\", "/"), "~/");

			IProjectItem item = service.GetProjectItemFromUrl(docUrl);

			if (item == null)
				return url;

			string path = item.PhysicalPath;
			if (!path.EndsWith(Path.DirectorySeparatorChar.ToString()))
				path += Path.DirectorySeparatorChar;

			return path;
		}
		
		private static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}

		#region ** private implementations

		private string GetScriptReference(WijmoResourceInfo resource)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("<script type='text/javascript'>");
			sb.AppendLine(resource.ResourceContent);
			sb.AppendLine("</script>");
			return sb.ToString();
		}

		private string GetScriptReferences()
		{
			StringBuilder strScriptRefs = new StringBuilder();
			if (_control is IWijmoWidgetSupport)
			{
				List<WijmoResourceInfo> resources = WijmoResourceManager.GetScriptReferenceForControl((IWijmoWidgetSupport)_control);
				foreach (WijmoResourceInfo resource in resources)
				{
					strScriptRefs.AppendLine(GetScriptReference(resource));
				}
			}
			return strScriptRefs.ToString();
		}

		private string GetScriptDescriptors()
		{
			string scriptDesc = "";
			List<System.Web.UI.ScriptDescriptor> scriptDescr = (List<System.Web.UI.ScriptDescriptor>)((System.Web.UI.IScriptControl)_control).GetScriptDescriptors();
			foreach (WidgetDescriptor desc in scriptDescr)
			{
				scriptDesc += this.GetScriptInternal(desc);
			}
			return scriptDesc;
		}

		private string GetScriptInternal(WidgetDescriptor desc)
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("$(\"#");
			builder.Append(((C1TargetControlBase)_control).ClientID);
			builder.Append("\").");
			builder.Append(desc.Type);
			builder.Append("(wijmoASPNetParseOptions(");
			builder.Append(desc.Options);
			builder.Append("));");
			return builder.ToString();
		}

		#endregion


		public void RenderControlPreview()
		{
			string sResultBodyContent = "";
			StringBuilder sb = new StringBuilder();
			System.IO.StringWriter tw = new System.IO.StringWriter(sb);
			System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
			_control.RenderControl(htw);
			sResultBodyContent = sb.ToString();

			//remove to fix issue 18186.
			//var scriptItems = this.RootDesigner.GetClientScriptsInDocument();
			//StringBuilder sbScripts = new StringBuilder();
			//foreach (ClientScriptItem item in scriptItems)
			//{
			//	sbScripts.AppendLine(item.Text);
			//}

			StringBuilder sDocumentContent = new StringBuilder();
			sDocumentContent.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			sDocumentContent.AppendLine("<html  xmlns=\"http://www.w3.org/1999/xhtml\">");
			sDocumentContent.AppendLine("<body style=\"margin: 0\">");
			sDocumentContent.AppendLine(GetScriptReferences());
			sDocumentContent.AppendLine("<script type=\"text/javascript\">");
			//sDocumentContent.AppendLine(sbScripts.ToString());
			sDocumentContent.AppendLine("$(document).ready(function () {");
			sDocumentContent.AppendLine(GetScriptDescriptors());
			//Add comments by RyanWu@20110707.
			//I added the following strange code in order to fix the issue#15889.
			//I also don't know why the last raphael element can't be drawn in the webbrowser control.
			//In bar chart, I first draw the chart label then draw bar and shadow (if shadow = true),
			//So if shadow = false, then last bar can't be shown.  In pie chart, I first draw sector,
			//then draw chart labels.  So the last chart label can't be shown.
			//Hence, to solve this issue, we will draw a useless shape after the chart drawn.
			//sDocumentContent.AppendLine("new Raphael(-100, -100, 1, 1).circle().hide();");
			//end by RyanWu@20110707.

			sDocumentContent.AppendLine("});");
			sDocumentContent.AppendLine("</script>");
			sDocumentContent.AppendLine(sResultBodyContent);
			sDocumentContent.AppendLine("</body>");
			sDocumentContent.AppendLine("</html>");
			this.DocumentWrite(sDocumentContent.ToString(), _control, this._rootDesigner.DocumentUrl);
		}



	}

	[ComVisible(true)]
	internal class CaptureForm : Form
	{
		C1ControlPreviewBrowser _browser;
		Timer _timer;
		public CaptureForm(C1ControlPreviewBrowser browser)
			: base()
		{
			_browser = browser;
			_timer = new Timer();
			_timer.Interval = 250;
			_timer.Tick += new EventHandler(_timer_Tick);
		}

		void _timer_Tick(object sender, EventArgs e)
		{
			if (this.Visible)
			{
				_browser.RaiseImageComplete(captureHiddenWindow(this._browser.Handle));
				this.Hide();
				/*
				var iscomplete = _browser.Document.InvokeScript("isComplete");
				if (iscomplete != null && iscomplete.ToString().ToLower() == "true")
				{
					_browser.RaiseImageComplete(captureHiddenWindow(this._browser.Handle));
					this.Hide();
				}*/
			}
		}
		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (this.Visible)
			{
				_timer.Start();
			}
			else
			{
				_timer.Stop();
			}
		}

		public static Bitmap captureHiddenWindow(IntPtr hwnd)
		{
			Bitmap bitmap = null;
			// Takes a snapshot of the window hwnd, stored in the memory device context hdcMem
			IntPtr hdc = User32.GetWindowDC(hwnd);
			if (hdc != IntPtr.Zero)
			{
				IntPtr hdcMem = GDI32.CreateCompatibleDC(hdc);
				if (hdcMem != IntPtr.Zero)
				{
					RECT rc;
					User32.GetWindowRect(hwnd, out rc);
					IntPtr hbitmap = GDI32.CreateCompatibleBitmap(hdc, rc.right - rc.left, rc.bottom - rc.top);
					if (hbitmap != IntPtr.Zero)
					{
						GDI32.SelectObject(hdcMem, hbitmap);
						User32.PrintWindow(hwnd, hdcMem, 0);
						bitmap = System.Drawing.Image.FromHbitmap(hbitmap);
						GDI32.DeleteObject(hbitmap);
					}
					GDI32.DeleteObject(hdcMem);
				}
				User32.ReleaseDC(hwnd, hdc);
			}
			return bitmap;
		}
	}

	#region user32.dll

	public static class User32
	{
		#region ** constants

		public const int SM_CXSCREEN = 0;
		public const int SM_CYSCREEN = 1;

		#endregion

		#region ** methods

		[DllImport("user32.dll", EntryPoint = "GetDC")]
		public static extern IntPtr GetDC(IntPtr ptr);

		[DllImport("user32.dll", EntryPoint = "GetDesktopWindow")]
		public static extern IntPtr GetDesktopWindow();

		[DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
		public static extern int GetSystemMetrics(int abc);

		[DllImport("user32.dll", EntryPoint = "GetWindowDC")]
		public static extern IntPtr GetWindowDC(IntPtr ptr);

		[DllImport("user32.dll")]
		public static extern IntPtr GetWindowRect(IntPtr hWnd, out RECT rect);

		[DllImport("user32.dll")]
		public static extern bool PrintWindow(IntPtr hwnd, IntPtr hdcBlt, uint nFlags);

		[DllImport("user32.dll", EntryPoint = "ReleaseDC")]
		public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);

		[DllImport("User32")]
		public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);

		#endregion

	}

	#endregion

	#region ** gdi32.dll

	public class GDI32
	{

		#region ** constants

		public const int SRCCOPY = 13369376;

		#endregion

		#region ** methods

		[DllImport("gdi32.dll", EntryPoint = "DeleteDC")]
		public static extern IntPtr DeleteDC(IntPtr hDc);

		[DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
		public static extern IntPtr DeleteObject(IntPtr hDc);

		[DllImport("gdi32.dll", EntryPoint = "BitBlt")]
		public static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int
		wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, int RasterOp);

		[DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
		public static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

		[DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC")]
		public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

		[DllImport("gdi32.dll", EntryPoint = "SelectObject")]
		public static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);
		#endregion

	}

	[StructLayout(LayoutKind.Sequential)]
	public struct RECT
	{
		public int left;
		public int top;
		public int right;
		public int bottom;
	}

	#endregion
	




}
