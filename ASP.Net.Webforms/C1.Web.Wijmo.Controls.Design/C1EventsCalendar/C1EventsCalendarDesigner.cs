﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.Design.C1EventsCalendar
{
	using C1.Web.Wijmo.Controls.C1EventsCalendar;
	using System.Web.UI.WebControls;
	using System.ComponentModel;
	using C1.Web.Wijmo.Controls.Design.Localization;
	using System.Globalization;
	using System.Diagnostics;
	using C1.Web.Wijmo.Controls.Design.Utils;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.ComponentModel.Design;
	using C1.Web.Wijmo.Controls.Design.C1ChartCore;
	using System.Drawing;
	using System.Runtime.InteropServices;

    public class C1EventsCalendarDesigner : C1ControlDesinger
	{
		#region ** fields
		//private DesignerAutoFormatCollection _autoFormats;
		private DesignerActionListCollection _actions;

		C1ControlPreviewBrowser _browser = null;
		System.Drawing.Image _image = null;


		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1EventsCalendarDesigner"/> class.
		/// </summary>
		public C1EventsCalendarDesigner()
		{
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Helper property to get the C1HeaderContentControl we're designing.
		/// </summary>
		private C1EventsCalendar EventsCalendarControl
		{
			get
			{
				return (C1EventsCalendar)Component;
			}
		}

		/// <summary>
		/// Tell the designer we're creating our own UI.
		/// </summary>
		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}
		/// <summary>
		/// Allow resize.
		/// </summary>
		public override bool AllowResize
		{
			get
			{
				return true;
			}
		}

		// initialize ActionLists
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actions == null)
				{
					_actions = new DesignerActionListCollection();
					_actions.AddRange(base.ActionLists);
					_actions.Add(new C1EventsCalendarActionList(this));
				}
				return _actions;
			}
		}

		/*
		/// <summary>
		///  Gets the collection of predefined automatic formatting schemes to display
		///  in the Auto Format dialog box for the associated control at design time.
		/// </summary>
		public override DesignerAutoFormatCollection AutoFormats
		{
			get
			{
				if (this._autoFormats == null)
				{
					this._autoFormats = new DesignerAutoFormatCollection();
					C1DesignerAutoFormat.FillVisualStyleAutoFormatCollection(this._autoFormats, (Control)this.Component);
				}
				if (this._autoFormats.Count < 1)
					return base.AutoFormats;
				return this._autoFormats;
			}
		}*/

		#endregion

		#region ** methods

		/// <summary>
		/// Initializes the control designer and loads the specified component.
		/// </summary>
		/// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			SetViewFlags(ViewFlags.CustomPaint, true);
		}

		/// <summary>
		/// Releases the unmanaged resources that are used by the <see cref="T:System.Web.UI.Design.HtmlControlDesigner"/> object and optionally releases the managed resources.
		/// </summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			_browser = null;
			base.Dispose(disposing);
		}

		/// <summary>
		/// Called when the control designer draws the associated control on the design surface, if the <see cref="F:System.Web.UI.Design.ViewFlags.CustomPaint"/> value is true.
		/// </summary>
		/// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs"/> object that specifies the graphics and rectangle boundaries used to draw the control.</param>
		protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
		{
			if (_image != null)
			{
				e.Graphics.DrawImage(_image, new Point(0, 0));
			}
			base.OnPaint(e);

		}


		/// <summary>
		/// Retrieves the HTML markup that is used to represent the control at design time.
		/// </summary>
		/// <returns>
		/// The HTML markup used to represent the control at design time.
		/// </returns>
		public override string GetDesignTimeHtml()
		{
			if (this.EventsCalendarControl.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			if (_browser == null)
			{
				_browser = new C1ControlPreviewBrowser(this, this.RootDesigner);
				_browser.ImageComplete += new C1ControlPreviewBrowser.ImageCompleteEventHandler(_browser_ImageComplete);
			}
			_browser.UpdatePreviewState();
			_browser.RenderControlPreview();			
			return base.GetDesignTimeHtml();

		}

		#endregion


		void _browser_ImageComplete(object sender, EventArgs e)
		{
			// now that we have an image invalidate the control design surface to we can draw it
			if (_browser != null)
			{
				try
				{
					_image = _browser.Image;
					this.Invalidate();
				}
				catch (Exception)
				{
					_browser = null;
				}
			}
		}


		protected override string GetErrorDesignTimeHtml(Exception e)
		{
#if DEBUG
			return CreatePlaceHolderDesignTimeHtml("Debug error: " + e.Message + "," + e.StackTrace);
#else
            return CreatePlaceHolderDesignTimeHtml(C1Localizer.GetString("Base.Exception.ErrorDesignTimeHtml"));
#endif
		}

	}


	internal class C1EventsCalendarActionList : DesignerActionListBase
	{
		// ** fields
		internal C1EventsCalendarDesigner _owner;

		internal C1EventsCalendar _EventsCalendar;

		private DesignerActionItemCollection _actions;

		public C1EventsCalendarActionList(C1EventsCalendarDesigner owner)
			: base(owner)
		{
			_owner = owner;

			_EventsCalendar = base.Component as C1EventsCalendar;
		}

		public Unit Width
		{
			get { return _EventsCalendar.Width; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_EventsCalendar)["Width"];
				prop.SetValue(_EventsCalendar, value);
			}
		}

		public Unit Height
		{
			get { return _EventsCalendar.Height; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_EventsCalendar)["Height"];
				prop.SetValue(_EventsCalendar, value);
			}
		}

		public bool HeaderBarVisible
		{
			get { return _EventsCalendar.HeaderBarVisible; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_EventsCalendar)["HeaderBarVisible"];
				prop.SetValue(_EventsCalendar, value);
			}
		}

		public bool NavigationBarVisible
		{
			get { return _EventsCalendar.NavigationBarVisible; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_EventsCalendar)["NavigationBarVisible"];
				prop.SetValue(_EventsCalendar, value);
			}
		}

		public CultureInfo Culture
		{
			get { return _EventsCalendar.Culture; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_EventsCalendar)["Culture"];
				prop.SetValue(_EventsCalendar, value);
			}
		}

		public DateTime SelectedDate
		{
			get { return _EventsCalendar.SelectedDate; }
			set
			{
				PropertyDescriptor prop = TypeDescriptor.GetProperties(_EventsCalendar)["SelectedDate"];
				prop.SetValue(_EventsCalendar, value);
			}
		}

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (_actions == null)
			{
				_actions = new DesignerActionItemCollection();
				_actions.Add(new DesignerActionPropertyItem("Width", C1Localizer.GetString("C1EventsCalendar.SmartTag.Width", "Control width"), "Size"));
				_actions.Add(new DesignerActionPropertyItem("Height", C1Localizer.GetString("C1EventsCalendar.SmartTag.Height", "Control height"), "Size"));

				_actions.Add(new DesignerActionPropertyItem("HeaderBarVisible", C1Localizer.GetString("C1EventsCalendar.SmartTag.HeaderBarVisible", "HeaderBar visible"), "UI"));
				_actions.Add(new DesignerActionPropertyItem("NavigationBarVisible", C1Localizer.GetString("C1EventsCalendar.SmartTag.NavigationBarVisible", "NavigationBar visible"), "UI"));
				_actions.Add(new DesignerActionPropertyItem("Culture", C1Localizer.GetString("C1EventsCalendar.SmartTag.Culture", "Culture"), "UI"));

				_actions.Add(new DesignerActionPropertyItem("SelectedDate", C1Localizer.GetString("C1EventsCalendar.SmartTag.SelectedDate", "Selected Date"), "Options"));
				AddBaseSortedActionItems(_actions);
			}
			return _actions;
		}

	}





}
