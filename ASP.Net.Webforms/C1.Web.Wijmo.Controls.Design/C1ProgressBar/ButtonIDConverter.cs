using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;
using System.Globalization;
using System.Web.UI.WebControls;

namespace C1.Web.UI.Design
{

	/// <summary>
	///  ButtonIDConverter
	/// </summary>
	public class ButtonIDConverter : ControlIDConverter
	{

		protected override bool FilterControl(Control control)
		{
			return control is Button;
		}
	}
}
