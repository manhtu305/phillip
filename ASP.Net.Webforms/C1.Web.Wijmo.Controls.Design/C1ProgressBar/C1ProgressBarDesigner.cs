#region File Header
//==============================================================================
//  FileName    :   C1ProgressBarDesigner.cs
//
//  Description :   Provides a C1ProgressBarDesigner class for
//	extending the design-mode behavior of a C1ProgressBar control.
//
//  Copyright (c) 2001 - 2011 GrapeCity, Inc.
//  All rights reserved.
//
//                                                          ... Ryan Wu
//==============================================================================

//------------------------------------------------------------------------------
//  Update Log:
//
//  Status          Date            Name                    BUG-ID
//  ----------------------------------------------------------------------------
//  Created         2011/03/23      Ryan Wu                 None
//
//------------------------------------------------------------------------------
#endregion end of File Header.

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.Design.C1ProgressBar
{

	using C1.Web.Wijmo.Controls.C1ProgressBar;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// Provides a C1ProgressBarDesigner class for extending the design-mode behavior of a C1Splitter control.
	/// </summary>
    public class C1ProgressBarDesigner : C1ControlDesinger
	{
		C1ProgressBar _control;

		public override void Initialize(System.ComponentModel.IComponent component)
		{
			base.Initialize(component);
			_control = component as C1ProgressBar;
		}

		public override string GetDesignTimeHtml()
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			return base.GetDesignTimeHtml();
		}

		/// <summary>
		/// Gets the action list collection for the control designer.
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1ProgressBarActionList(this));

				return actionLists;
			}
		}

		/// <summary>
		/// Provides a custom class for types that define a list of items used to create a smart tag panel.
		/// </summary>
		private class C1ProgressBarActionList : DesignerActionListBase
		{

			#region ** fields
			private DesignerActionItemCollection items;
			#endregion end of ** fields..

			#region ** constructor
			/// <summary>
			/// CustomControlActionList constructor.
			/// </summary>
			/// <param name="parent">The Specified C1SplitterDesigner designer.</param>
			public C1ProgressBarActionList(C1ProgressBarDesigner parent)
				: base(parent)
			{
			}
			#endregion end of ** constructor.

			/// <summary>
			/// Override GetSortedActionItems method.
			/// </summary>
			/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (items == null)
				{
					items = new DesignerActionItemCollection();
					items.Add(new DesignerActionPropertyItem("FillDirection", C1Localizer.GetString("C1ProgressBar.SmartTag.FillDirection"), "Appearance", C1Localizer.GetString("C1ProgressBar.SmartTag.FillDirectionDescription")));
					items.Add(new DesignerActionPropertyItem("LabelAlign", C1Localizer.GetString("C1ProgressBar.SmartTag.LabelAlign"), "Appearance", C1Localizer.GetString("C1ProgressBar.SmartTag.LabelAlignDescription")));
					items.Add(new DesignerActionPropertyItem("Value", C1Localizer.GetString("C1ProgressBar.SmartTag.Value"), "Appearance", C1Localizer.GetString("C1ProgressBar.SmartTag.ValueDescription")));

					AddBaseSortedActionItems(items);
				}
				
				return items;
			}

			/// <summary>
			/// Gets or sets a value indicating the direction of the fill for the C1ProgressBar.
			/// </summary>
			public FillDirection FillDirection
			{
				get
				{
					return (FillDirection)this.GetProperty("FillDirection");
				}
				set
				{
					this.SetProperty("FillDirection", value);
				}
			}

			/// <summary>
			/// Gets or sets a value indicating the label's alignment for the C1ProgressBar.
			/// </summary>
			public LabelAlign LabelAlign
			{
				get
				{
					return (LabelAlign)this.GetProperty("LabelAlign");
				}
				set
				{
					this.SetProperty("LabelAlign", value);
				}
			}

			/// <summary>
			/// Gets or sets the value of the C1ProgressBar.
			/// </summary>
			public int Value
			{
				get
				{
					return (int)this.GetProperty("Value");
				}
				set
				{
					this.SetProperty("Value", value);
				}
			}
		}
	}
}
