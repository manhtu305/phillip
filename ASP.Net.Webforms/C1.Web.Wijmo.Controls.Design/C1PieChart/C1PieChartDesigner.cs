﻿using System;
using System.Collections.Generic;
using System.Web.UI.Design;
using System.Drawing;
using System.Web.UI;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.Design.C1PieChart
{
	using C1.Web.Wijmo.Controls.C1Chart;
	using C1.Web.Wijmo.Controls.Design.C1ChartCore;
	using System.ComponentModel.Design;

	/// <summary>
	/// Provides a C1PieChartDesigner class for extending the design-mode behavior of a C1PieChart control.
	/// </summary>
	public class C1PieChartDesigner : C1ChartCoreDesigner
	{
		internal override C1ChartCoreDesignerActionList CreateChartCoreDesignerActionList()
		{
			return new C1PieChartDesignerActionList(this);
		}

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
		}

		protected override void AddSampleData()
		{
			C1PieChart lineChart = Component as C1PieChart;
			PieChartSeries series = new PieChartSeries();
			series.Label = "SampleData1";
			series.Data = 1;
			lineChart.SeriesList.Add(series);
			series = new PieChartSeries();
			series.Label = "SampleData2";
			series.Data = 2;
			lineChart.SeriesList.Add(series);
			series = new PieChartSeries();
			series.Label = "SampleData3";
			series.Data = 3;
			lineChart.SeriesList.Add(series);
			series = new PieChartSeries();
			series.Label = "SampleData4";
			series.Data = 4;
			lineChart.SeriesList.Add(series);
		}
	}

	internal class C1PieChartDesignerActionList : C1ChartCoreDesignerActionList
	{
		public C1PieChartDesignerActionList(C1ChartCoreDesigner designer)
			: base(designer)
		{
		}

		internal override bool SupportAnnotations
		{
			get
			{
				return false;
			}
		}
	}
}