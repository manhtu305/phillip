﻿using System;
using System.ComponentModel;
using System.Web.UI.Design;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Web.UI.WebControls;
using System.Xml;


namespace C1.Web.Wijmo.Controls.Design.C1BinaryImage
{
	using C1.Web.Wijmo.Controls.C1BinaryImage;
	using C1.Web.Wijmo.Controls.Design.Localization;

	[SupportsPreviewControl(true)]
	internal class C1BinaryImageDesigner : C1ControlDesinger
	{
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				var actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1BinaryImageDesignerActionList(this));
				return actionLists;
			}
		}


		protected override void RegisterHttpHandler()
		{
			string path = C1BinaryImage.DefaultHttpHandlerUrl.Replace("~/", "");
			base.RegisterHttpHandler();
			this.DesignerHelper.RegisterWijmoHttpHandler(path.Replace(".axd", ""), path, typeof(C1BinaryImageHandler));
		}

		
		internal class C1BinaryImageDesignerActionList : DesignerActionListBase
		{
			public C1BinaryImageDesignerActionList(C1BinaryImageDesigner designer)
				: base(designer)
			{
			}

			public override DesignerActionItemCollection GetSortedActionItems()
			{
				var actionList = new DesignerActionItemCollection();

                actionList.Add(new DesignerActionPropertyItem("AutoSize", C1Localizer.GetString("C1BinaryImage.SmartTag.AutoSize"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.AutoSizeDescription")));
                actionList.Add(new DesignerActionPropertyItem("ImageUrl", C1Localizer.GetString("C1BinaryImage.SmartTag.ImageUrl"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.ImageUrlDescription")));
                actionList.Add(new DesignerActionPropertyItem("ToolTip", C1Localizer.GetString("C1BinaryImage.SmartTag.ToolTip"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.ToolTipDescription")));
                actionList.Add(new DesignerActionPropertyItem("AlternateText", C1Localizer.GetString("C1BinaryImage.SmartTag.AlternateText"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.AlternateTextDescription")));
                actionList.Add(new DesignerActionPropertyItem("ResizeMode", C1Localizer.GetString("C1BinaryImage.SmartTag.ResizeMode"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.ResizeModeDescription")));
                actionList.Add(new DesignerActionPropertyItem("Width", C1Localizer.GetString("C1BinaryImage.SmartTag.Width"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.WidthDescription")));
                actionList.Add(new DesignerActionPropertyItem("Height", C1Localizer.GetString("C1BinaryImage.SmartTag.Height"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.HeightDescription")));
                actionList.Add(new DesignerActionPropertyItem("CropPosition", C1Localizer.GetString("C1BinaryImage.SmartTag.CropPosition"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.CropPositionDescription")));
                actionList.Add(new DesignerActionPropertyItem("ImageAlign", C1Localizer.GetString("C1BinaryImage.SmartTag.ImageAlign"), "General", C1Localizer.GetString("C1BinaryImage.SmartTag.ImageAlignDescription")));

				actionList.Add(new DesignerActionMethodItem(this, "About", C1Localizer.GetString("Base.SmartTag.AboutBox", "About"), "About"));
				

				return actionList;
			}


			#region ** public properties
			public bool AutoSize
			{
				get
				{
					return BinaryImage.AutoSize;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["AutoSize"].SetValue(base.Component, value);
				}
			}


			[Editor("System.Web.UI.Design.ImageUrlEditor, System.Design", typeof(System.Drawing.Design.UITypeEditor))]
			public string ImageUrl
			{
				get
				{
					return BinaryImage.ImageUrl;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["ImageUrl"].SetValue(base.Component, value);
				}
			}

			public string ToolTip
			{
				get
				{
					return BinaryImage.ToolTip;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["ToolTip"].SetValue(base.Component, value);
				}
			}

			public string AlternateText
			{
				get
				{
					return BinaryImage.AlternateText;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["AlternateText"].SetValue(base.Component, value);
				}
			}

			public ImageResizeMode ResizeMode
			{
				get
				{
					return BinaryImage.ResizeMode;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["ResizeMode"].SetValue(base.Component, value);
				}
			}

			public Unit Width
			{
				get
				{
					return BinaryImage.Width;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["Width"].SetValue(base.Component, value);
				}
			}

			public Unit Height
			{
				get
				{
					return BinaryImage.Height;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["Height"].SetValue(base.Component, value);
				}
			}

			public ImageCropPosition CropPosition
			{
				get
				{
					return BinaryImage.CropPosition;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["CropPosition"].SetValue(base.Component, value);
				}
			}

			public ImageAlign ImageAlign
			{
				get
				{
					return BinaryImage.ImageAlign;
				}
				set
				{
					TypeDescriptor.GetProperties(base.Component)["ImageAlign"].SetValue(base.Component, value);
				}
			}
			#endregion

			#region ** private properties
			private C1BinaryImage BinaryImage
			{
				get
				{
					return (C1BinaryImage)base.Component;
				}
			}

			private IWebApplication webApplication;
			private IWebApplication WebApplication
			{
				get
				{
					if (this.webApplication == null)
					{
						this.webApplication = (IWebApplication)this.Component.Site.GetService(typeof(IWebApplication));
					}
					return this.webApplication;
				}
			}

			private System.Configuration.Configuration configuration;
			private System.Configuration.Configuration Configuration
			{
				get
				{
					if (this.configuration == null)
					{
						this.configuration = this.WebApplication.OpenWebConfiguration(false);
					}
					return this.configuration;
				}
			}

			private System.Xml.XmlDocument webConfigXml;
			private System.Xml.XmlDocument WebConfigXml
			{
				get
				{
					if (this.webConfigXml == null)
					{
						this.webConfigXml = new System.Xml.XmlDocument();
						this.webConfigXml.Load(this.Configuration.FilePath);
					}
					return this.webConfigXml;
				}
			}

			private string HttpHandlerType
			{
				get
				{
					return typeof(C1BinaryImageHandler).FullName;
				}
			}
			#endregion
		}
	}
}
