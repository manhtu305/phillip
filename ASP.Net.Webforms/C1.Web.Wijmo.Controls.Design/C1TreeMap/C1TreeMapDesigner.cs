﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.Design.C1TreeMap
{
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Drawing;
	using System.Web.UI;
	using System.Web.UI.Design;
	using System.Web.UI.Design.WebControls;
	using System.Web.UI.WebControls;
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.C1TreeMap;
	using C1.Web.Wijmo.Controls.Design.C1ChartCore;
	using C1.Web.Wijmo.Controls.Design.Localization;
	public class C1TreeMapDesigner : C1HierarchicalDataBoundControlDesigner
	{
		#region ** fields
		C1ChartBrowser _browser = null;
		System.Drawing.Image _image = null;
		protected bool _sampleDataAdded = false;
		#endregion

		#region ** override methods
		/// <summary>
		/// Initializes the control designer and loads the specified component.
		/// </summary>
		/// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (component != null)
			{
				base.Initialize(component);
				SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
				SetViewFlags(ViewFlags.CustomPaint, true);
				_browser = new C1ChartBrowser();
				_browser.ImageComplete += new C1ChartBrowser.ImageCompleteEventHandler(_browser_ImageComplete);
				_browser.ScriptErrorsSuppressed = true;
			}
		}

		/// <summary>
		/// Called when the control designer draws the associated control on the design surface, if the System.Web.UI.Design.ViewFlags.CustomPaint value is true.
		/// </summary>
		/// <param name="e">A System.Windows.Forms.PaintEventArgs object that specifies the graphics and rectangle boundaries used to draw the control.</param>
		protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
		{
			if (_image != null)
			{
				e.Graphics.DrawImage(_image, new Point(0, 0));
			}
			base.OnPaint(e);
		}

		// avoid to re render the page and call the GetDesignTimeHtml twice.
		protected override void OnDataSourceChanged(bool forceUpdateView)
		{

		}

		/// <summary>
		/// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions.
		/// </summary>
		/// <param name="regions"> A collection of control designer regions for the associated control.</param>
		/// <returns>The design-time HTML markup for the associated control, including all control designer regions.</returns>
		public override string GetDesignTimeHtml()
		{
			C1TreeMap control = Component as C1TreeMap;
			if (control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			Unit width = control.Width;
			Unit height = control.Height;
			if (control.Width.IsEmpty)
			{
				control.Width = 800;
			}
			if (control.Height.IsEmpty)
			{
				control.Height = 600;
			}
			_browser.Width = (int)control.Width.Value;
			_browser.Height = (int)control.Height.Value;

			//store the control's visible state, in designer the control will show all the time.
			bool visible = control.Visible;
			control.Visible = true;

			string labelFormatter = control.LabelFormatter;
			if (!string.IsNullOrEmpty(labelFormatter)) 
			{
				control.LabelFormatter = "";
			}

			string sResultBodyContent = "";
			StringBuilder sb = new StringBuilder();
			System.IO.StringWriter tw = new System.IO.StringWriter(sb);
			System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(tw);
			control.RenderControl(htw);
			sResultBodyContent = sb.ToString();

			PropertyDescriptor context = TypeDescriptor.GetProperties(this.Component)["Items"];
			object seriesList = context.GetValue(this.Component);
			IList list = seriesList as IList;
			if (list != null && list.Count == 0)
			{
				AddSampleData(control);
				_sampleDataAdded = true;
			}

			StringBuilder sDocumentContent = new StringBuilder();
			sDocumentContent.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			sDocumentContent.AppendLine("<html  xmlns=\"http://www.w3.org/1999/xhtml\">");
			sDocumentContent.AppendLine("<head>");
			sDocumentContent.AppendLine("<meta http-equiv=Content-Type content=\"text/html;charset=utf-8\">");
			sDocumentContent.AppendLine("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />");
			sDocumentContent.AppendLine("</head>");
			sDocumentContent.AppendLine("<body style=\"margin: 0\">");
			sDocumentContent.AppendLine(GetScriptReferences());
			sDocumentContent.AppendLine("<script type=\"text/javascript\">");
			sDocumentContent.AppendLine("$(document).ready(function () {");
			sDocumentContent.AppendLine(GetScriptDescriptors());
			sDocumentContent.AppendLine("});");
			sDocumentContent.AppendLine("</script>");
			sDocumentContent.AppendLine(sResultBodyContent);
			sDocumentContent.AppendLine("</body>");
			sDocumentContent.AppendLine("</html>");
			var result = sDocumentContent.ToString();
			_browser.DocumentWrite(result, control, this.RootDesigner.DocumentUrl);

			if (_sampleDataAdded)
			{
				list.Clear();
				_sampleDataAdded = false;
			}

			// restore the control's visible state.
			control.Visible = visible;
			if (!string.IsNullOrEmpty(labelFormatter)) {
				control.LabelFormatter = labelFormatter;
			}
			control.Width = width;
			control.Height = height;
			return base.GetDesignTimeHtml();
		}
		#endregion

		#region ** private methods
		void _browser_ImageComplete(object sender, EventArgs e)
		{
			// now that we have an image invalidate the control design surface to we can draw it
			_image = _browser.Image;
			this.Invalidate();
		}

		private void AddSampleData(C1TreeMap control)
		{
			TreeMapItem item1 = new TreeMapItem("item1", 50);
			TreeMapItem item11 = new TreeMapItem("item11", 30);
			TreeMapItem item12 = new TreeMapItem("item12", 20);
			item1.Items.Add(item11);
			item1.Items.Add(item12);

			TreeMapItem item2 = new TreeMapItem("item2", 100);
			TreeMapItem item21 = new TreeMapItem("item21", 20);
			TreeMapItem item22 = new TreeMapItem("item22", 50);
			TreeMapItem item23 = new TreeMapItem("item23", 30);
			item2.Items.Add(item21);
			item2.Items.Add(item22);
			item2.Items.Add(item23);
			control.Items.Add(item1);
			control.Items.Add(item2);
		}

		private string GetScriptReference(WijmoResourceInfo resource)
		{
			StringBuilder sb = new StringBuilder();
			string content = resource.ResourceContent;
			sb.AppendLine("<script type='text/javascript' charset='"+resource.ResourceEncoding.WebName+"'>");
			sb.AppendLine(content);
			sb.AppendLine("</script>");
			return sb.ToString();
		}

		private string GetScriptReferences()
		{
			StringBuilder strScriptRefs = new StringBuilder();
			if (Component is IWijmoWidgetSupport)
			{
				List<WijmoResourceInfo> resources = WijmoResourceManager.GetScriptReferenceForControl((IWijmoWidgetSupport)Component);
				foreach (WijmoResourceInfo resource in resources)
				{
					strScriptRefs.AppendLine(GetScriptReference(resource));
				}
			}
			return strScriptRefs.ToString();
		}

		

		private string GetScriptDescriptors()
		{
			string scriptDesc = "";
			List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)((IScriptControl)Component).GetScriptDescriptors();
			foreach (WidgetDescriptor desc in scriptDescr)
			{
				scriptDesc += this.GetScriptInternal(desc);
			}
			return scriptDesc;
		}

		private string GetScriptInternal(WidgetDescriptor desc)
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("$(\"#");
			builder.Append(((C1TargetHierarchicalDataBoundControlBase)Component).ClientID);
			builder.Append("\").");
			builder.Append(desc.Type);
			builder.Append("(");
			builder.Append(desc.Options);
			builder.Append(");");
			return builder.ToString();
		}
		#endregion

		#region ** action list
		internal void BuildItems()
		{
			PropertyDescriptor context = TypeDescriptor.GetProperties(base.Component)["Items"];
			C1HierarchicalDataBoundControlDesigner.InvokeTransactedChange(base.Component, new TransactedChangeCallback(this.EditCollectionCallback), context, C1Localizer.GetString("C1TreeMap.SmartTag.Items"), context);
		}

		internal bool EditCollectionCallback(object context)
		{
			IDesignerHost service = (IDesignerHost)this.GetService(typeof(IDesignerHost));
			PropertyDescriptor propDesc = (PropertyDescriptor)context;
			new ListItemsCollectionEditor(propDesc.PropertyType).EditValue(new TypeDescriptorContext(service, propDesc, base.Component), new WindowsFormsEditorServiceHelper(this), propDesc.GetValue(base.Component));
			return true;
		}

		/// <summary>
		/// override ActionLists
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection lists = new DesignerActionListCollection();

				lists.AddRange(base.ActionLists);
				lists.Add(CreateChartCoreDesignerActionList());
				return lists;
			}
		}

		internal virtual C1TreeMapDesignerActionList CreateChartCoreDesignerActionList()
		{
			return new C1TreeMapDesignerActionList(this);
		}
		#endregion
	}

	internal class C1TreeMapDesignerActionList : DesignerActionListBase 
	{
		#region ** fields
		private DesignerActionItemCollection items;
		C1TreeMapDesigner _designer;
		#endregion end of ** fields

		#region ** constructor
        /// <summary>
        /// CustomControlActionList constructor.
        /// </summary>
        /// <param name="parent">The Specified C1SplitterDesigner designer.</param>
		public C1TreeMapDesignerActionList(C1TreeMapDesigner designer)
            : base(designer)
        {
            _designer = designer;
        }
		#endregion end of ** constructor.

		#region ** properties
		/// <summary>
		/// Gets or sets ShowBackButtons property of control.
		/// </summary>
		public bool ShowBackButtons 
		{
			get 
			{
				return (bool)GetProperty(_designer.Component, "ShowBackButtons").GetValue(_designer.Component);
			}
			set 
			{
				SetProperty("ShowBackButtons", value);
			}
		}

		/// <summary>
		/// Gets or sets ShowLabel property of control.
		/// </summary>
		public bool ShowLabel
		{
			get 
			{
				return (bool)GetProperty(_designer.Component, "ShowLabel").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("ShowLabel", value);
			}
		}

		/// <summary>
		/// Gets or sets ShowTitle property of control.
		/// </summary>
		public bool ShowTitle
		{
			get
			{
				return (bool)GetProperty(_designer.Component, "ShowTitle").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("ShowTitle", value);
			}
		}

		/// <summary>
		/// Gets or sets ShowTooltip property of control.
		/// </summary>
		public bool ShowTooltip
		{
			get
			{
				return (bool)GetProperty(_designer.Component, "ShowTooltip").GetValue(_designer.Component);
			}
			set
			{
				SetProperty("ShowTooltip", value);
			}
		}
		#endregion

		#region ** override methods
		/// <summary>
		/// Override GetSortedActionItems method.
		/// </summary>
		/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>

		public override DesignerActionItemCollection GetSortedActionItems()
		{
			if (items == null)
			{
				items = new DesignerActionItemCollection();

				items.Add(new DesignerActionMethodItem(this, "EditItems", C1Localizer.GetString("C1TreeMap.SmartTag.Items"), "", C1Localizer.GetString("C1TreeMap.SmartTag.ItemsDesc")));
				items.Add(new DesignerActionPropertyItem("ShowBackButtons", C1Localizer.GetString("C1TreeMap.SmartTag.ShowBackButtons", "ShowBackButtons"), "ShowBackButtons", C1Localizer.GetString("C1TreeMap.SmartTag.ShowBackButtonsDesc")));
				items.Add(new DesignerActionPropertyItem("ShowLabel", C1Localizer.GetString("C1TreeMap.SmartTag.ShowLabel", "ShowLabel"), "ShowLabel", C1Localizer.GetString("C1TreeMap.SmartTag.ShowLabelDesc")));
				items.Add(new DesignerActionPropertyItem("ShowTitle", C1Localizer.GetString("C1TreeMap.SmartTag.ShowTitle", "ShowTitle"), "ShowTitle", C1Localizer.GetString("C1TreeMap.SmartTag.ShowTitleDesc")));
				items.Add(new DesignerActionPropertyItem("ShowTooltip", C1Localizer.GetString("C1TreeMap.SmartTag.ShowTooltip", "ShowTooltip"), "ShowTooltip", C1Localizer.GetString("C1TreeMap.SmartTag.ShowTooltipDesc")));
				AddBaseSortedActionItems(items);
			}
			return items;
		}
		#endregion

		#region ** other methods
		internal void EditItems()
		{
			_designer.BuildItems();
		}
		#endregion
	}
}
