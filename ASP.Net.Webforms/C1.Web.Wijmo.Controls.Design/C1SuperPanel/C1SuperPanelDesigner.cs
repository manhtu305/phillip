using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1SuperPanel
{

	using C1.Web.Wijmo.Controls.C1SuperPanel;

	/// <summary>
	/// Contains C1SuperPanel design time function.
	/// </summary>
	[SupportsPreviewControl(true)]
    public class C1SuperPanelDesigner : C1ControlDesinger
	{

		private DesignerActionListCollection _actionList;
		private C1SuperPanel _control;

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actionList == null)
				{
					_actionList = new DesignerActionListCollection();
					_actionList.AddRange(base.ActionLists);
					_actionList.Add(new C1SuperPanelActionList(this));
				}
				return _actionList;
			}
		}

		/// <summary>
		///  Gets a value indicating whether the control designer uses a temporary preview control to generate the design-time HTML markup.
		/// </summary>
		protected override bool UsePreviewControl
		{
			get
			{
				return true;
			}
		}

		#region Editible regions & DesignTime HTML
		public override string GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			if (_control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			string html = base.GetDesignTimeHtml(regions);
			html = html.Replace("C1SuperPanelDesignerRegionAttributeName", EditableDesignerRegion.DesignerRegionAttributeName);
			regions.Add(new EditableDesignerRegion(this, "Content", false));
			
			return html;
		}

		public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
		{
			// Get a reference to the designer host
			var host = Component.Site.GetService(typeof (IDesignerHost)) as IDesignerHost;
			if (host != null)
			{
				ITemplate template = _control.ContentTemplate;

				if (template != null)
				{
					return ControlPersister.PersistTemplate(template, host);
				}
			}

			return String.Empty;
		}

		public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
		{
			var host = Component.Site.GetService(typeof (IDesignerHost)) as IDesignerHost;
			if (host != null)
			{
				ITemplate template = ControlParser.ParseTemplate(host, content);
				OnComponentChanging(_control, new ComponentChangingEventArgs(_control, null));
				_control.ContentTemplate = template;
				OnComponentChanged(_control, new ComponentChangedEventArgs(_control, null, null, null));
			}
		}
		#endregion

		public override void Initialize(IComponent component)
		{
			base.Initialize(component);

			_control = (C1SuperPanel)component;
			if (_control.Page == null && RootDesigner != null)
			{
				var page = RootDesigner.Component as Page;
				if (page != null)
				{
					_control.Page = page;
				}
			}
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
		}
	}

	internal class C1SuperPanelActionList : DesignerActionListBase
	{
		public C1SuperPanelActionList(C1SuperPanelDesigner parent)
			: base(parent)
		{
		}
	}
}