using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
#if GRAPECITY
#if ASP_NET4
[assembly: AssemblyTitle("ComponentOne Designer for ASP.NET Wijmo (CLR 4.0) JPN")]
[assembly: AssemblyDescription("ComponentOne Designer for ASP.NET Wijmo (CLR 4.0) JPN")]
[assembly: AssemblyProduct("ComponentOne Designer for ASP.NET Wijmo (CLR 4.0) JPN")]
#elif ASP_NET35
[assembly: AssemblyTitle("ComponentOne Designer for ASP.NET Wijmo (CLR 3.5) JPN")]
[assembly: AssemblyDescription("ComponentOne Designer for ASP.NET Wijmo (CLR 3.5) JPN")]
[assembly: AssemblyProduct("ComponentOne Designer for ASP.NET Wijmo (CLR 3.5) JPN")]
#else
[assembly: AssemblyTitle("ComponentOne Designer for ASP.NET Wijmo (CLR 2.0) JPN")]
[assembly: AssemblyDescription("ComponentOne Designer for ASP.NET Wijmo (CLR 2.0) JPN")]
[assembly: AssemblyProduct("ComponentOne Designer for ASP.NET Wijmo (CLR 2.0) JPN")]
#endif
#else
#if ASP_NET4
[assembly: AssemblyTitle("ComponentOne Designer for ASP.NET Wijmo (CLR 4.0)")]
[assembly: AssemblyDescription("ComponentOne Designer for ASP.NET Wijmo (CLR 4.0)")]
[assembly: AssemblyProduct("ComponentOne Designer for ASP.NET Wijmo (CLR 4.0)")]
#elif ASP_NET35
[assembly: AssemblyTitle("ComponentOne Designer for ASP.NET Wijmo (CLR 3.5)")]
[assembly: AssemblyDescription("ComponentOne Designer for ASP.NET Wijmo (CLR 3.5)")]
[assembly: AssemblyProduct("ComponentOne Designer for ASP.NET Wijmo (CLR 3.5)")]
#else
[assembly: AssemblyTitle("ComponentOne Designer for ASP.NET Wijmo (CLR 2.0)")]
[assembly: AssemblyDescription("ComponentOne Designer for ASP.NET Wijmo (CLR 2.0)")]
[assembly: AssemblyProduct("ComponentOne Designer for ASP.NET Wijmo (CLR 2.0)")]
#endif
#endif

[assembly: AssemblyVersion(C1.Wijmo.Licensing.VersionConst.VerString)]

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyName("")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dab2d3e2-21e4-4e63-b39f-eb4a1c3b4f2c")]

