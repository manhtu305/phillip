#region File Header
//==============================================================================
//  FileName    :   C1SplitterDesigner.cs
//
//  Description :   Provides a C1SplitterDesigner class for extending the design-mode behavior of a C1Splitter control.
//
//  Copyright (c) 2001 - 2008 GrapeCity, Inc.
//  All rights reserved.
//
//                                                          ... Jacky Qiu
//==============================================================================

//------------------------------------------------------------------------------
//  Update Log:
//
//  Status          Date            Name                    BUG-ID
//  ----------------------------------------------------------------------------
//  Created         2008/09/16      Jacky Qiu				None
//
//------------------------------------------------------------------------------
#endregion end of File Header.

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.Design;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.Design.C1Splitter
{

	using C1.Web.Wijmo.Controls.C1Splitter;
	using C1.Web.Wijmo.Controls.Design.Localization;

	/// <summary>
	/// Provides a C1SplitterDesigner class for extending the design-mode behavior of a C1Splitter control.
	/// </summary>
    public class C1SplitterDesigner : C1ControlDesinger
	{
		
		/// <summary>
		/// Gets C1Splitter control from designer.
		/// </summary>
		public C1Splitter C1Splitter
		{
			get
			{
				return this.Component as C1Splitter;
			}
		}

		/// <summary>
		/// Initializes the control designer and loads the specified component.
		/// </summary>
		/// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			base.Initialize(component);
			
			SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
			base.SetViewFlags(ViewFlags.TemplateEditing, true);
		}

		/// <summary>
		/// Retrieves the HTML markup to display the control and populates the collection with the current control designer regions.
		/// </summary>
		/// <param name="regions"> A collection of control designer regions for the associated control.</param>
		/// <returns>The design-time HTML markup for the associated control, including all control designer regions.</returns>
		public override string GetDesignTimeHtml(DesignerRegionCollection regions)
		{
			if (this.C1Splitter.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//C1Localizer.GetString("C1Control.DesignTimeNotSupported")
				return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
			}
			regions.Add(new EditableDesignerRegion(this, "Panel1EditContent", false));
			regions.Add(new EditableDesignerRegion(this, "Panel2EditContent", false));

			
			string str = base.GetDesignTimeHtml(regions);
			str = str.Replace("{0}", EditableDesignerRegion.DesignerRegionAttributeName);
			str = str.Replace("{1}", EditableDesignerRegion.DesignerRegionAttributeName);

			return str;
		}

		/// <summary>
		///  Returns the content for an editable region of the design-time view of the associated control.
		/// </summary>
		/// <param name="region">The System.Web.UI.Design.EditableDesignerRegion object to get content for.</param>
		/// <returns>The persisted content for the region, if the control designer supports editable regions; otherwise, an empty string ("").</returns>
		public override string GetEditableDesignerRegionContent(EditableDesignerRegion region)
		{
			IDesignerHost service = (IDesignerHost)base.Component.Site.GetService(typeof(IDesignerHost));
			if (service != null)
			{
				if (region.Name == "Panel1EditContent")
				{
					ITemplate contentTemplate = this.C1Splitter.Panel1.ContentTemplate;
					if (contentTemplate != null)
					{
						return ControlPersister.PersistTemplate(contentTemplate, service);
					}
				}

				if (region.Name == "Panel2EditContent")
				{
					ITemplate contentTemplate = this.C1Splitter.Panel2.ContentTemplate;
					if (contentTemplate != null)
					{
						return ControlPersister.PersistTemplate(contentTemplate, service);
					}
				}

			}
			return string.Empty;
		}

		/// <summary>
		/// Specifies the content for an editable region of the control at design time.
		/// </summary>
		/// <param name="region">An editable design region contained within the control.</param>
		/// <param name="content">The content to assign for the editable design region.</param>
		public override void SetEditableDesignerRegionContent(EditableDesignerRegion region, string content)
		{
			IDesignerHost service = (IDesignerHost)base.Component.Site.GetService(typeof(IDesignerHost));
			if (service != null)
			{
				if (region.Name == "Panel1EditContent")
				{
					if (content == null || content.Length == 0)
					{
						this.C1Splitter.Panel1.ContentTemplate = null;
					}
					else
					{
						ITemplate template = ControlParser.ParseTemplate(service, content);
						this.C1Splitter.Panel1.ContentTemplate = template;
					}

				}
				else if (region.Name == "Panel2EditContent")
				{
					if (content == null || content.Length == 0)
					{
						this.C1Splitter.Panel2.ContentTemplate = null;
					}
					else
					{
						ITemplate template = ControlParser.ParseTemplate(service, content);
						this.C1Splitter.Panel2.ContentTemplate = template;
					}
				}
			}
		}

		/// <summary>
		/// Gets the action list collection for the control designer.
		/// </summary>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actionLists = new DesignerActionListCollection();
				actionLists.AddRange(base.ActionLists);
				actionLists.Add(new C1SplitterDesignerActionList(this));
				return actionLists;
			}
		}

		/// <summary>
		/// Provides a custom class for types that define a list of items used to create a smart tag panel.
		/// </summary>
		private class C1SplitterDesignerActionList : DesignerActionListBase
		{
			#region ** fields
			private DesignerActionItemCollection items;
			#endregion end of ** fields..

			#region ** constructor
			/// <summary>
			/// CustomControlActionList constructor.
			/// </summary>
			/// <param name="parent">The Specified C1SplitterDesigner designer.</param>
			public C1SplitterDesignerActionList(C1SplitterDesigner designer)
				: base(designer)
			{
			}
			#endregion end of ** constructor.

			/// <summary>
			/// Override GetSortedActionItems method.
			/// </summary>
			/// <returns>Return the collection of System.ComponentModel.Design.DesignerActionItem object contained in the list.</returns>
			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (items == null)
				{
					items = new DesignerActionItemCollection();

					items.Add(new DesignerActionPropertyItem("Orientation", C1Localizer.GetString("C1Splitter.SmartTag.Orientation"), "Appearance", C1Localizer.GetString("C1Splitter.SmartTag.OrientationDescription")));
					items.Add(new DesignerActionPropertyItem("ShowExpander", C1Localizer.GetString("C1Splitter.SmartTag.ShowExpander"), "Behavior", C1Localizer.GetString("C1Splitter.SmartTag.ShowExpanderDescription")));

					AddBaseSortedActionItems(items);
				}
				
				return items;
			}
			
			/// <summary>
			/// Gets or sets a value indicating the horizontal or vertical orientation of the C1WebSplitter panels. 
			/// </summary>
			public Orientation Orientation
			{
				get
				{
					return (Orientation)this.GetProperty("Orientation");
				}
				set
				{
					this.SetProperty("Orientation", value);
				}
			}

			/// <summary>
			/// The splitter style for the Web control.
			/// </summary>
			public bool ShowExpander
			{
				get
				{
					return (bool)this.GetProperty("ShowExpander");
				}
				set
				{
					this.SetProperty("ShowExpander", value);
				}
			}
		}
	}
}
