﻿using System.Web.UI.Design;
using System.ComponentModel.Design;
using C1.Web.Wijmo.Controls.Design.Localization;

namespace C1.Web.Wijmo.Controls.Design.C1QRCode
{
	using Controls.C1QRCode;

	public class C1QRCodeDesigner : C1ControlDesinger
	{

		private DesignerActionListCollection _actions;

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if (_actions == null)
				{
					_actions = new DesignerActionListCollection();
					_actions.AddRange(base.ActionLists);
					_actions.Add(new C1QRCodeActionList(this));
				}

				return _actions;
			}
		}

		public override string GetDesignTimeHtml()
		{
			var qrCode = Component as C1QRCode;
			if (qrCode == null || qrCode.HasImage || qrCode.WijmoControlMode == WijmoControlMode.Mobile)
			{
				return base.GetDesignTimeHtml();
			}

			var sizeStyle = string.Empty;
			if (!qrCode.Width.IsEmpty)
			{
				sizeStyle = "width:" + qrCode.Width + ";";
			}

			if (!qrCode.Height.IsEmpty)
			{
				sizeStyle += "height:" + qrCode.Height + ";";
			}

			return string.Format("<div style='display:inline-block;color:white;background-color:gray;{1}'>{0}</div>", qrCode.GetTip(), sizeStyle);
		}

		private class C1QRCodeActionList : DesignerActionListBase
		{
			private DesignerActionItemCollection _actions;

			public C1QRCodeActionList(ControlDesigner designer)
				: base(designer)
			{
			}

			public int CodeVersion
			{
				get { return GetPropertyValue<int>("CodeVersion"); }
				set { SetProperty("CodeVersion", value); }
			}

			public Encoding Encoding
			{
				get { return GetPropertyValue<Encoding>("Encoding"); }
				set { SetProperty("Encoding", value); }
			}

			public ErrorCorrectionLevel ErrorCorrectionLevel
			{
				get { return GetPropertyValue<ErrorCorrectionLevel>("ErrorCorrectionLevel"); }
				set { SetProperty("ErrorCorrectionLevel", value); }
			}

			public int SymbolSize
			{
				get { return GetPropertyValue<int>("SymbolSize"); }
				set { SetProperty("SymbolSize", value); }
			}

			public string Text
			{
				get { return GetPropertyValue<string>("Text"); }
				set { SetProperty("Text", value); }
			}

			private T GetPropertyValue<T>(string propertyName)
			{
				return (T)GetProperty(Component, propertyName).GetValue(Component);
			}

			public override DesignerActionItemCollection GetSortedActionItems()
			{
				if (_actions == null)
				{
					_actions = new DesignerActionItemCollection
					{
						new DesignerActionPropertyItem("Text",
							C1Localizer.GetString("C1QRCode.SmartTag.Text", "Text"), "Text",
							C1Localizer.GetString("C1QRCode.SmartTag.TextDescription")),
						new DesignerActionPropertyItem("Encoding",
							C1Localizer.GetString("C1QRCode.SmartTag.Encoding", "Encoding"), "Encoding",
							C1Localizer.GetString("C1QRCode.SmartTag.EncodingDescription")),
						new DesignerActionPropertyItem("SymbolSize",
							C1Localizer.GetString("C1QRCode.SmartTag.SymbolSize", "SymbolSize"), "SymbolSize",
							C1Localizer.GetString("C1QRCode.SmartTag.SymbolSizeDescription")),
						new DesignerActionPropertyItem("CodeVersion",
							C1Localizer.GetString("C1QRCode.SmartTag.CodeVersion", "CodeVersion"), "CodeVersion",
							C1Localizer.GetString("C1QRCode.SmartTag.CodeVersionDescription")),
						new DesignerActionPropertyItem("ErrorCorrectionLevel",
							C1Localizer.GetString("C1QRCode.SmartTag.ErrorCorrectionLevel", "ErrorCorrectionLevel"), "ErrorCorrectionLevel",
							C1Localizer.GetString("C1QRCode.SmartTag.ErrorCorrectionLevelDescription"))
					};

					AddBaseSortedActionItems(_actions);
				}

				return _actions;
			}

			internal override bool DisplayThemeSupport()
			{
				return false;
			}

			protected internal override bool SupportBootstrap()
			{
				return false;
			}

			internal override bool SupportCDN
			{
				get { return false; }
			}

			internal override bool SupportWijmoControlMode
			{
				get { return false; }
			}
		}
	}
}