﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Controls.Design.C1ToolTip
{
    using C1.Web.Wijmo.Controls.C1ToolTip;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    public class C1ToolTipDesigner : C1ControlDesinger
    {
        #region ** fields
        private C1ToolTip _control;
        #endregion

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed</param>
        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this._control = (C1ToolTip)component;

            SetViewFlags(ViewFlags.DesignTimeHtmlRequiresLoadComplete, true);
            base.SetViewFlags(ViewFlags.TemplateEditing, true);
        }

        public override string GetDesignTimeHtml()
        {
            if (this._control is C1ToolTip)
            {
                if (_control.WijmoControlMode == WijmoControlMode.Mobile)
                {
                    //C1Localizer.GetString("C1Control.DesignTimeNotSupported")
                    return String.Format("<span style=\"color:white;background-color:gray;\">{0}</span>", "Design time is not supported in mobile mode.");
                }
                C1ToolTip toolTip = (C1ToolTip)this._control;
                string phantomTip = String.Format("<div style=\"color:black;background-color:silver;font-size:10pt;width:200px;height:25px;text-align:center;padding-top:5px;\"><b>C1ToolTip</b> - {0}</div>", toolTip.ID);
                return phantomTip;
            }
            return base.GetDesignTimeHtml();
        }

        public override bool AllowResize
        {
            get
            {
                return false;
            }
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                DesignerActionListCollection actionLists =new DesignerActionListCollection();
                actionLists.AddRange(base.ActionLists);
                actionLists.Add(new C1ToolTipActionList(this));
                return actionLists;
            }
        }
    }

    internal class C1ToolTipActionList : DesignerActionListBase 
    {
        private C1ToolTipDesigner _parent;

        public C1ToolTipActionList(C1ToolTipDesigner parent):base(parent)
        {
            _parent = parent;
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection col = new DesignerActionItemCollection();
            AddBaseSortedActionItems(col);
            return col;
        }
    }
}
