<%@ WebHandler Language="VB" Class="C1ReportHttpHandler" %>
Imports System.Web
Imports System.Collections
Imports System.IO
Imports C1.Web.Wijmo.Controls.C1ReportViewer
Imports C1.Web.Wijmo.Controls.C1ReportViewer.ReportService

Public Class C1ReportHttpHandler
	Implements IHttpHandler

	Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
		Dim reportService As IC1WebReportService = C1WebReportServiceHelper.MakeHelper(DirectCast(context.Request.Params("documentKey"), String))
		' レポートをファイルにエクスポートします。
        Dim url As String = reportService.ExportToFile(DirectCast(context.Request.Params("documentKey"), String), context.Request.Params("exportFormat"), "")

		' url をファイルのパスに変換します。
		Dim pathSource As String = url.Substring(url.IndexOf("tempReports/"))
		pathSource = context.Server.MapPath(pathSource)

		Using fsSource As New FileStream(pathSource, FileMode.Open, FileAccess.Read)
			' ソースファイルをバイトアレイに読み取ります。
			Dim bytes As Byte() = New Byte(fsSource.Length - 1) {}
			Dim numBytesToRead As Integer = CInt(fsSource.Length)
			Dim numBytesRead As Integer = 0
			While numBytesToRead > 0
				' 読み取り処理は、0 〜 numBytesToRead 間の任意値を返します。
				Dim n As Integer = fsSource.Read(bytes, numBytesRead, numBytesToRead)
				' ファイルの最後まで達した際に、ブレークします。
				If n = 0 Then
					Exit While
				End If
				numBytesRead += n
				numBytesToRead -= n
			End While
			' バッフアストリームからすべての出力コンテンツを削除します。 
			context.Response.Clear()
			' ブラウザのダウンロードダイアログのデフォルトのファイル名を指定した 
			' 出力ストリームにHTTPヘッダーを追加します。
			context.Response.AddHeader("Content-Disposition", "attachment; filename=" & DirectCast(context.Request.Params("documentKey"), String) & "." & DirectCast(context.Request.Params("exportFormatExt"), String))
			' コンテンツの長さ（ファイルサイズ）が含まれている出力ストリームに 
			' HTTPヘッダーを追加します。これによって、ブラウザは転送されるデータの量を判定することができます。 
			context.Response.AddHeader("Content-Length", bytes.Length.ToString())
			' 出力ストリームの HTTP MIME タイプを設定します。
			context.Response.ContentType = "application/octet-stream"
			' データをクライアントまで書き込みます。 
			context.Response.BinaryWrite(bytes)
		End Using
	End Sub

	Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
		Get
			Return False
		End Get
	End Property


End Class
                