'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ChangeExportButtonBehavior

    Partial Public Class [Default]

        '''<summary>
        '''Head1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

        '''<summary>
        '''form1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

        '''<summary>
        '''ScriptManager1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''C1ReportViewer1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ReportViewer1 As Global.C1.Web.Wijmo.Controls.C1ReportViewer.C1ReportViewer
    End Class
End Namespace
