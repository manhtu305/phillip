Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Drawing
Imports System.Drawing.Text
Imports System.Data
Imports C1.C1Preview
Imports C1.Web.Wijmo.Controls.C1ReportViewer
Imports System.Web.UI.HtmlControls

Namespace PrintWithoutPreview
	Public Partial Class [Default]
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			Dim toolBar As C1ReportViewerToolbar = C1ReportViewer1.ToolBar
			DirectCast(toolBar.Controls(0), HtmlGenericControl).Style("display") = "none"
			' 元の印刷ボタンを隠します。		
			' カスタム印刷ボタンを追加します。
			Dim printButton As New HtmlGenericControl("button")
			printButton.Attributes("title") = "カスタム印刷"
			printButton.Attributes("class") = "custom-print"
			printButton.InnerHtml = "カスタム印刷"
			toolBar.Controls.AddAt(0, printButton)
		End Sub
	End Class
End Namespace
