'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Tabs


	Public Partial Class TabStrip

		''' <summary>
		''' C1Tab1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Tab1 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1Tabs

		''' <summary>
		''' Page1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Page1 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1TabPage

		''' <summary>
		''' Page2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Page2 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1TabPage

		''' <summary>
		''' Page3 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Page3 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1TabPage

		''' <summary>
		''' C1Tabs2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Tabs2 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1Tabs

		''' <summary>
		''' Page4 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Page4 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1TabPage

		''' <summary>
		''' Page5 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Page5 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1TabPage

		''' <summary>
		''' Page6 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Page6 As Global.C1.Web.Wijmo.Controls.C1Tabs.C1TabPage

		''' <summary>
		''' msg コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected msg As Global.System.Web.UI.WebControls.Label
	End Class
End Namespace
