﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="TabStrip.aspx.vb" Inherits="ControlExplorer.C1Tabs.TabStrip" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .ui-tabs .ui-tabs-panel
        {
            padding-top: 0;
            padding-bottom: 0;
        }
        
        .ui-tabs-left, .ui-tabs-left .ui-tabs-nav
        {
            width: 140px;
        }
        
        .ui-tabs-left .wijmo-wijtabs-content
        {
            width: 0;
        }
        
        .message
        {
            font-size: 3em;
        }
    </style>
    <script type="text/javascript">

        function TabSelected(e, ui) {
            $("#<%=msg.ClientID%>").text("<" + $(ui.tab).text() + "> タブが選択されています。");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Tabs ID="C1Tab1" runat="server" OnClientSelect="TabSelected">
        <Pages>
            <wijmo:C1TabPage ID="Page1" Text="全般">
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page2" Text="コントロール">
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page3" Text="製品情報">
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
    <wijmo:C1Tabs ID="C1Tabs2" runat="server" OnClientSelect="TabSelected" Alignment="Left" Width="100%">
        <Pages>
            <wijmo:C1TabPage ID="Page4" Text="文学">
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page5" Text="科学">
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page6" Text="履歴">
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
    <br />
    <asp:Label ID="msg" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<strong>C1Tabs</strong> を使用して簡単なタブストリップ UI を表示する方法を紹介します。
    </p>
    <p>
        水平方向のタブストリップを表示するには、空白の C1Tabs コントロールを追加して、垂直方向のパディングを 0 に設定します。
    </p>
    <p>
        垂直方向のタブストリップを表示するには、空白の垂直方向 C1Tabs コントロールを追加して、タブとコンテンツ領域の幅を適切な値に設定します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
