Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI

Namespace ControlExplorer
	Public Partial Class Wijmo
		Inherits System.Web.UI.MasterPage
		Implements ICallbackEventHandler
		Private _callbackArgument As String = ""
		Private _callbackResult As String = "unknown command"

		Protected Sub RegisterCallbacks()

			Dim webFormDoCallbackScript As String = Me.Page.ClientScript.GetCallbackEventReference(Me, "arg", "onCallbackSuccess", Nothing, True)
			Dim serverCallScript As String = "function executeCallback(arg){" & webFormDoCallbackScript & ";" & vbLf & "}" & vbLf

			If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("executeCallbackScript") Then
				Me.Page.ClientScript.RegisterClientScriptBlock(Me.[GetType](), "executeCallbackScript", serverCallScript, True)
			End If
		End Sub

		Public Function GetCallbackResult() As String Implements ICallbackEventHandler.GetCallbackResult
			Return _callbackResult
		End Function

		Public Sub RaiseCallbackEvent(eventArgument As String) Implements ICallbackEventHandler.RaiseCallbackEvent
			_callbackArgument = eventArgument
			Dim arr As String() = eventArgument.Split("="C)
			If arr.Length = 2 Then
				Select Case arr(0)
					Case "theme"
						WidgetTabs.Theme = arr(1)
						_callbackResult = "ok"
						Exit Select
					Case Else
						Exit Select
				End Select
			End If
		End Sub
	End Class
End Namespace
