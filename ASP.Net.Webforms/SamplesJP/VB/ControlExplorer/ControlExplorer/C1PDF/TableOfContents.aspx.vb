﻿Imports C1.C1Pdf
Imports System.Collections
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1PDF
    Partial Public Class TableOfContents
        Inherits System.Web.UI.Page
        Private _c1pdf As C1.C1Pdf.C1PdfDocument
        Private _rnd As New Random()
        Private tempFiles As Hashtable = Nothing
        Protected Shared TEMP_DIR As String
        Private filename As String = String.Empty
        Protected Sub Page_Load(sender As Object, e As EventArgs)

        End Sub

        Private Sub CreatePDF()
            _c1pdf = New C1PdfDocument()
            ' pdf generatorの初期化
            _c1pdf.Clear()
            _c1pdf.DocumentInfo.Title = "Pdf Document With Table of Contents"
            TEMP_DIR = Server.MapPath("../Temp")

            If Directory.Exists(TEMP_DIR) Then
            Else

                Directory.CreateDirectory(TEMP_DIR)
            End If
            ' タイトルを追加
            Dim titleFont As New Font("Tahoma", 24, FontStyle.Bold)
            Dim rcPage As RectangleF = GetPageRect()
            Dim rc As RectangleF = RenderParagraph(_c1pdf.DocumentInfo.Title, titleFont, rcPage, rcPage, False)
            rc.Y += 12

            ' ドキュメントを作成
            Dim bkmk As New ArrayList()
            Dim headerFont As New Font("Tahoma", 16, FontStyle.Bold)
            Dim bodyFont As New Font("Tahoma", 10)
            For i As Integer = 0 To 29
                ' ヘッダを追加(リンクターゲットおよびアウトラインエントリとして)
                Dim header As String = String.Format("{0}. {1}", i + 1, BuildRandomTitle())
                rc = RenderParagraph(header, headerFont, rcPage, rc, True, True)

                ' 後で目次をビルドするためのブックマークを追加
                Dim pageNumber As Integer = _c1pdf.CurrentPage + 1
                bkmk.Add(New String() {pageNumber.ToString(), header})

                ' テキストを作成
                rc.X += 36
                rc.Width -= 36
                For j As Integer = 0 To 3 + (_rnd.[Next](10) - 1)
                    Dim text As String = BuildRandomParagraph()
                    rc = RenderParagraph(text, bodyFont, rcPage, rc)
                    rc.Y += 6
                Next
                rc.X -= 36
                rc.Width += 36
                rc.Y += 20
            Next

            ' ページ番号を付加(目次を追加する前に)
            AddFooters()

            ' 目次を開始
            _c1pdf.NewPage()
            ' 新規ページ上で目次を開始
            Dim tocPage As Integer = _c1pdf.CurrentPage
            ' ページインデックスを保存(後で目次を移動するため)
            rc = RenderParagraph("Table of Contents", titleFont, rcPage, rcPage, True)
            rc.Y += 12
            rc.X += 30
            rc.Width -= 40

            ' 目次のレンダリング
            Dim dottedPen As New Pen(Brushes.Gray, 1.5F)
            dottedPen.DashStyle = DashStyle.Dot
            Dim sfRight As New StringFormat()
            sfRight.Alignment = StringAlignment.Far
            rc.Height = bodyFont.Height
            For Each entry As String() In bkmk
                ' ブックマーク情報を取得
                Dim page As String = entry(0)
                Dim header As String = entry(1)

                ' ヘッダ名とページ番号をレンダリング
                _c1pdf.DrawString(header, bodyFont, Brushes.Black, rc)
                _c1pdf.DrawString(page, bodyFont, Brushes.Black, rc, sfRight)

                ' ドットの結合(ドット線の改善)
                Dim dots As String = ". "
                Dim wid As Single = _c1pdf.MeasureString(dots, bodyFont).Width
                Dim x1 As Single = rc.X + _c1pdf.MeasureString(header, bodyFont).Width + 8
                Dim x2 As Single = rc.Right - _c1pdf.MeasureString(page, bodyFont).Width - 8
                Dim x As Single = rc.X
                rc.X = x1
                While rc.X < x2
                    _c1pdf.DrawString(dots, bodyFont, Brushes.Gray, rc)
                    rc.X += wid
                End While
                rc.X = x

                ' エントリするローカルハイパーリンクを追加
                _c1pdf.AddLink("#" & header, rc)

                ' 次のエントリに移動
                rc.Offset(0, rc.Height)
                If rc.Bottom > rcPage.Bottom Then
                    _c1pdf.NewPage()
                    rc.Y = rcPage.Y
                End If
            Next

            ' 目次をドキュメントの初めに移動
            Dim arr As PdfPage() = New PdfPage(_c1pdf.Pages.Count - tocPage - 1) {}
            _c1pdf.Pages.CopyTo(tocPage, arr, 0, arr.Length)
            _c1pdf.Pages.RemoveRange(tocPage, arr.Length)
            _c1pdf.Pages.InsertRange(0, CType(arr, ICollection))

            ' pdfファイルを保存
            Dim uid As String = System.Guid.NewGuid().ToString()
            filename = Server.MapPath("~") & "\Temp\testpdf" & uid & ".pdf"
            _c1pdf.Save(filename)

            ' 表示
            'webBrowser1.Navigate(filename);
        End Sub

        Private Function BuildRandomTitle() As String
            Dim a1 As String() = "Learning|Explaining|Mastering|Forgetting|Examining|Understanding|Applying|Using|Destroying".Split("|"c)
            Dim a2 As String() = "Music|Tennis|Golf|Zen|Diving|Modern Art|Gardening|Architecture|Mathematics|Investments|.NET|Java".Split("|"c)
            Dim a3 As String() = "Quickly|Painlessly|The Hard Way|Slowly|Painfully|With Panache".Split("|"c)
            Return String.Format("{0} {1} {2}", a1(_rnd.[Next](a1.Length - 1)), a2(_rnd.[Next](a2.Length - 1)), a3(_rnd.[Next](a3.Length - 1)))
        End Function
        Private Function BuildRandomParagraph() As String
            Dim sb As New StringBuilder()
            For i As Integer = 0 To 5 + (_rnd.[Next](10) - 1)
                sb.AppendFormat(BuildRandomSentence())
            Next
            Return sb.ToString()

        End Function
        Private Function BuildRandomSentence() As String
            Dim a1 As String() = "Artists|Movie stars|Musicians|Politicians|Computer programmers|Modern thinkers|Gardeners|Experts|Some people|Hockey players".Split("|"c)
            Dim a2 As String() = "know|seem to think about|care about|often discuss|dream about|hate|love|despise|respect|long for|pay attention to|embrace".Split("|"c)
            Dim a3 As String() = "the movies|chicken soup|tea|many things|sushi|my car|deep thoughts|tasteless jokes|vaporware|cell phones|hot dogs|ballgames".Split("|"c)
            Dim a4 As String() = "incessantly|too much|easily|without reason|rapidly|sadly|randomly|vigorously|more than usual|with enthusiasm|shamelessly|on Tuesdays".Split("|"c)
            Return String.Format("{0} {1} {2} {3}. ", a1(_rnd.[Next](a1.Length - 1)), a2(_rnd.[Next](a2.Length - 1)), a3(_rnd.[Next](a3.Length - 1)), a4(_rnd.[Next](a4.Length - 1)))
        End Function
        Private Function GetPageRect() As RectangleF
            Dim rcPage As RectangleF = _c1pdf.PageRectangle
            rcPage.Inflate(-72, -72)
            Return rcPage
        End Function
        Private Sub AddFooters()
            Dim fontHorz As New Font("Tahoma", 7, FontStyle.Bold)
            Dim fontVert As New Font("Viner Hand ITC", 14, FontStyle.Bold)

            Dim sfRight As New StringFormat()
            sfRight.Alignment = StringAlignment.Far

            Dim sfVert As New StringFormat()
            sfVert.FormatFlags = sfVert.FormatFlags Or StringFormatFlags.DirectionVertical
            sfVert.Alignment = StringAlignment.Center

            For page As Integer = 0 To _c1pdf.Pages.Count - 1
                ' ページを選択 (ページサイズを変更可能)
                _c1pdf.CurrentPage = page

                ' テキストをレンダリングするための矩形をビルド
                Dim rcPage As RectangleF = GetPageRect()
                Dim rcFooter As RectangleF = rcPage
                rcFooter.Y = rcFooter.Bottom + 6
                rcFooter.Height = 12
                Dim rcVert As RectangleF = rcPage
                rcVert.X = rcPage.Right + 6

                ' 左寄せのフッタを追加
                Dim text As String = _c1pdf.DocumentInfo.Title
                _c1pdf.DrawString(text, fontHorz, Brushes.Gray, rcFooter)

                ' 右寄せのフッタを追加
                text = String.Format("Page {0} of {1}", page + 1, _c1pdf.Pages.Count)
                _c1pdf.DrawString(text, fontHorz, Brushes.Gray, rcFooter, sfRight)

                ' 垂直テキストを追加
                text = Convert.ToString(_c1pdf.DocumentInfo.Title) & " (document created using the C1Pdf .NET component)"
                _c1pdf.DrawString(text, fontVert, Brushes.LightGray, rcVert, sfVert)

                ' ページの下側と右側に線を描画
                _c1pdf.DrawLine(Pens.Gray, rcPage.Left, rcPage.Bottom, rcPage.Right, rcPage.Bottom)
                _c1pdf.DrawLine(Pens.Gray, rcPage.Right, rcPage.Top, rcPage.Right, rcPage.Bottom)
            Next
        End Sub
        Friend Function RenderParagraph(text As String, font As Font, rcPage As RectangleF, rc As RectangleF, outline As Boolean, linkTarget As Boolean) As RectangleF
            ' このページに合わない場合は改ページを実行
            rc.Height = _c1pdf.MeasureString(text, font, rc.Width).Height
            If rc.Bottom > rcPage.Bottom Then
                _c1pdf.NewPage()
                rc.Y = rcPage.Top
            End If

            ' 文字列の描画
            _c1pdf.DrawString(text, font, Brushes.Black, rc)

            ' アウトラインに見出しを追加
            If outline Then
                _c1pdf.DrawLine(Pens.Black, rc.X, rc.Y, rc.Right, rc.Y)
                _c1pdf.AddBookmark(text, 0, rc.Y)
            End If

            ' リンクターゲットを追加
            If linkTarget Then
                _c1pdf.AddTarget(text, rc)
            End If

            ' 次回用に矩形を更新
            rc.Offset(0, rc.Height)
            Return rc
        End Function
        Friend Function RenderParagraph(text As String, font As Font, rcPage As RectangleF, rc As RectangleF, outline As Boolean) As RectangleF
            Return RenderParagraph(text, font, rcPage, rc, outline, False)
        End Function
        Friend Function RenderParagraph(text As String, font As Font, rcPage As RectangleF, rc As RectangleF) As RectangleF
            Return RenderParagraph(text, font, rcPage, rc, False, False)
        End Function

        Public ReadOnly Property DemoConnectionString() As String
            Get
                Return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|NWind_ja.mdb;Persist Security Info=False"
            End Get
        End Property

        Protected Sub exportbtn_Click(sender As Object, e As EventArgs)
            Try
                CreatePDF()
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.TransmitFile(filename)
                Response.Flush()
                File.Delete(filename)
                Response.[End]()
            Catch ex As Exception

                Response.Write(ex.Message)
            End Try

        End Sub
    End Class
End Namespace
