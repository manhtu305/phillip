﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.C1Pdf
Imports System.Collections
Imports System.IO

Namespace ControlExplorer.C1PDF
    Partial Public Class Overview
        Inherits System.Web.UI.Page
        Private _fontBody As New Font("Tahoma", 10)
        Private _fontTitle As New Font("Tahoma", 15, FontStyle.Bold)
        Private _fontHeader As New Font("Tahoma", 11, FontStyle.Bold)
        Private _sfRight As StringFormat
        Private _sfRightCenter As StringFormat
        Private _c1pdf As C1.C1Pdf.C1PdfDocument
        Private tempFiles As Hashtable = Nothing
        Protected Shared TEMP_DIR As String
        Protected Sub Page_Load(sender As Object, e As EventArgs)
            TEMP_DIR = Server.MapPath("~") + "\Temp"

            If Directory.Exists(TEMP_DIR) Then
            Else

                Directory.CreateDirectory(TEMP_DIR)
            End If
        End Sub

        Private Sub Create()
            _c1pdf = New C1PdfDocument()
            '右寄せフィールドのためのStringFormatを作成
            _sfRight = New StringFormat()
            _sfRight.Alignment = StringAlignment.Far
            _sfRightCenter = New StringFormat()
            _sfRightCenter.Alignment = StringAlignment.Far
            _sfRightCenter.LineAlignment = StringAlignment.Center

            'pdf generatorの初期化
            _c1pdf.Clear()

            'ページ矩形を取得してマージンを削除
            Dim rcPage As RectangleF = _c1pdf.PageRectangle
            rcPage.Inflate(-72, -92)

            '選択したカテゴリでループ
            Dim page As Integer = 0
            Dim dt As DataTable = GetCategories()
            For Each dr As DataRow In dt.Rows
                '改ページを追加してページカウンダを更新
                If page > 0 Then
                    _c1pdf.NewPage()
                End If
                page += 1

                '現在のカテゴリ名を取得
                Dim catName As String = DirectCast(dr("CategoryName"), String)

                'ページにタイトルを追加
                _c1pdf.DrawString(catName, _fontTitle, Brushes.Blue, rcPage)

                'アウトラインエントリを追加
                _c1pdf.AddBookmark(catName, 0, 0)

                '行テンプレートをビルド
                Dim rcRows As RectangleF() = New RectangleF(5) {}
                For i As Integer = 0 To rcRows.Length - 1
                    rcRows(i) = RectangleF.Empty
                    rcRows(i).Location = New PointF(rcPage.X, rcPage.Y + _fontHeader.SizeInPoints + 10)
                    rcRows(i).Size = New SizeF(0, _fontBody.SizeInPoints + 3)
                Next
                rcRows(0).Width = 110
                ' Product Name
                rcRows(1).Width = 60
                ' Unit Price
                rcRows(2).Width = 80
                ' Qty/Unit
                rcRows(3).Width = 60
                ' Stock Units
                rcRows(4).Width = 60
                ' Stock Value
                rcRows(5).Width = 60
                ' Reorder
                For i As Integer = 1 To rcRows.Length - 1
                    rcRows(i).X = rcRows(i - 1).X + rcRows(i - 1).Width + 8
                Next

                '列ヘッダを追加
                _c1pdf.FillRectangle(Brushes.DarkGray, RectangleF.Union(rcRows(0), rcRows(5)))
                _c1pdf.DrawString("Product Name", _fontHeader, Brushes.White, rcRows(0))
                _c1pdf.DrawString("Unit Price", _fontHeader, Brushes.White, rcRows(1), _sfRight)
                _c1pdf.DrawString("Qty/Unit", _fontHeader, Brushes.White, rcRows(2))
                _c1pdf.DrawString("Stock Units", _fontHeader, Brushes.White, rcRows(3), _sfRight)
                _c1pdf.DrawString("Stock Value", _fontHeader, Brushes.White, rcRows(4), _sfRight)
                _c1pdf.DrawString("Reorder", _fontHeader, Brushes.White, rcRows(5))

                'このカテゴリ内の製品でループ
                Dim products As DataRow() = dr.GetChildRows("Categories_Products")

                For Each product As DataRow In products
                    '次の行に移動
                    For i As Integer = 0 To rcRows.Length - 1
                        rcRows(i).Y += rcRows(i).Height
                    Next

                    '行を追加
                    Try
                        _c1pdf.DrawString(product("ProductName").ToString(), _fontBody, Brushes.Black, rcRows(0))
                        _c1pdf.DrawString(String.Format("{0:c}", product("UnitPrice")), _fontBody, Brushes.Black, rcRows(1), _sfRight)
                        _c1pdf.DrawString(String.Format("{0}", product("QuantityPerUnit")), _fontBody, Brushes.Black, rcRows(2))
                        _c1pdf.DrawString(String.Format("{0}", product("UnitsInStock")), _fontBody, Brushes.Black, rcRows(3), _sfRight)
                        _c1pdf.DrawString(String.Format("{0:c}", product("ValueInStock")), _fontBody, Brushes.Black, rcRows(4), _sfRight)
                        If CBool(product("OrderNow")) Then
                            _c1pdf.DrawString("<<<", _fontBody, Brushes.Red, rcRows(5))
                        End If
                        ' Debug.Assert(false);
                    Catch
                    End Try
                Next
                If products.Length = 0 Then
                    rcRows(0).Y += rcRows(0).Height
                    _c1pdf.DrawString("No products in this category.", _fontBody, Brushes.Black, RectangleF.Union(rcRows(0), rcRows(5)))
                End If
            Next

            'ページヘッダを追加
            AddPageHeaders(rcPage)
        End Sub

        Private Sub AddPageHeaders(rcPage As RectangleF)
            Dim rcHdr As RectangleF = rcPage
            rcHdr.Y = 10
            rcHdr.Height = rcPage.Top - 10
            For page As Integer = 0 To _c1pdf.Pages.Count - 1
                '各ページを再オープン
                _c1pdf.CurrentPage = page

                'レターヘッドを描画
                Dim s As String = String.Format("Page {0} of {1}", page + 1, _c1pdf.Pages.Count)
                _c1pdf.DrawString(s, _fontBody, Brushes.LightGray, rcHdr, _sfRightCenter)
                rcHdr.Inflate(0, -30)
                _c1pdf.DrawRectangle(Pens.LightGray, rcHdr)
                rcHdr.Inflate(0, +30)
            Next
        End Sub

        Private Function GetCategories() As DataTable
            Dim conn As String = Me.DemoConnectionString

            'テーブルにデータをロード
            Dim dtCategories As New DataTable("Categories")
            Dim dtProducts As New DataTable("Products")
            Dim daCat As New OleDbDataAdapter("select * from Categories", conn)
            Dim daPrd As New OleDbDataAdapter("select * from Products", conn)
            daCat.Fill(dtCategories)
            daPrd.Fill(dtProducts)

            '計算列を追加
            Dim col As DataColumn = dtProducts.Columns.Add("ValueInStock", GetType(Decimal), "UnitPrice * UnitsInStock")
            col.Caption = "Value In Stock"
            col = dtProducts.Columns.Add("OrderNow", GetType(Boolean), "UnitsInStock <= ReorderLevel")
            col.Caption = "Order Now"

            'カテゴリと製品名でリレーションしたデータセットをビルド
            Dim ds As New DataSet()
            ds.Tables.Add(dtCategories)
            ds.Tables.Add(dtProducts)
            ds.Relations.Add("Categories_Products", dtCategories.Columns("CategoryID"), dtProducts.Columns("CategoryID"))

            'カテゴリテーブルをリターン
            Return dtCategories
        End Function

        Public Function GetTempFileName(ext As String) As String
            If tempFiles Is Nothing Then
                tempFiles = New Hashtable()
            End If

            If Not tempFiles.Contains(ext) Then
                Dim tempFile As String = GetUniqueTempFileName(ext)
                tempFiles.Add(ext, tempFile)
            End If

            Return tempFiles(ext).ToString()
        End Function

        Public Function GetUniqueTempFileName(ext As String) As String
            Dim appName As String = AppDomain.CurrentDomain.FriendlyName
            Dim guid As String = System.Guid.NewGuid().ToString()
            Return [String].Format("{0}\{1}_{2}{3}", TEMP_DIR, appName, guid, ext)
        End Function

        Public ReadOnly Property DemoConnectionString() As String
            Get
                Return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|NWind_ja.mdb;Persist Security Info=False"
            End Get
        End Property

        Protected Sub btnexport_Click(sender As Object, e As EventArgs)
            Try
                Create()

                Dim uid As String = System.Guid.NewGuid().ToString()
                Dim filename As String = Server.MapPath("~") & "\Temp\testpdf" & uid & ".pdf"


                _c1pdf.Save(filename)

                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.TransmitFile(filename)
                Response.Flush()
                File.Delete(filename)
                Response.[End]()
            Catch ex As Exception

                Response.Write(ex.Message)
            End Try



        End Sub
    End Class
End Namespace
