﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="RichGraphics.aspx.vb" Inherits="ControlExplorer.C1PDF.RichGraphics" %>

<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       
   <asp:LinkButton ID="btnexport" runat="server" Text="PDFを作成" OnClick="btnexport_Click" Font-Underline="true" />
    </asp:Content>

    <asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
        PDF for .NET は、.NET の Graphics クラスが提供するものと類似のコマンド、および同様のオブジェクト（フォント、色、矩形、ポイント等）を使用してドキュメントを作成します。
        主な違いとして、.NET の Graphics クラスの座標システムがデフォルトでピクセル単位で、また他の単位に変換できるのに対し、C1PDF は常にポイント単位を使用します。
        このサンプルでは、PDF の閲覧に Adobe Reader が必要です。
    </asp:Content>
