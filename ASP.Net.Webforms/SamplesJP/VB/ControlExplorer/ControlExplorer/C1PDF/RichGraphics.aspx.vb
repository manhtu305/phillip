﻿Imports System.Collections
Imports System.Collections.Generic
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1PDF
    Partial Public Class RichGraphics
        Inherits System.Web.UI.Page
        Private _c1pdf As C1.C1Pdf.C1PdfDocument
        Private tempFiles As Hashtable = Nothing
        Protected Shared TEMP_DIR As String
        Protected Sub Page_Load(sender As Object, e As EventArgs)

        End Sub

        Private Sub CreatePDF()
            _c1pdf = New C1.C1Pdf.C1PdfDocument()
            'ドキュメントの開始
            _c1pdf.Clear()
            TEMP_DIR = Server.MapPath("../Temp")

            If Directory.Exists(TEMP_DIR) Then
            Else

                Directory.CreateDirectory(TEMP_DIR)
            End If
            'Gdiライクなコマンドを使用して描画準備
            Dim penWidth As Integer = 0
            Dim penRGB As Integer = 0
            Dim rc As New Rectangle(50, 50, 300, 200)
            Dim text As String = "Hello world of .NET Graphics and PDF." & vbCr & vbLf & "Nice to meet you."
            Dim font As New Font("Times New Roman", 16, FontStyle.Italic Or FontStyle.Underline)

            'start, c1, c2, end1, c3, c4, end
            Dim bezierPoints As PointF() = New PointF() {New PointF(110.0F, 200.0F), New PointF(120.0F, 110.0F), New PointF(135.0F, 150.0F), New PointF(150.0F, 200.0F), New PointF(160.0F, 250.0F), New PointF(165.0F, 200.0F), _
                New PointF(150.0F, 100.0F)}

            'pdfドキュメントに描画
            Dim g As C1.C1Pdf.C1PdfDocument = _c1pdf
            g.FillPie(Brushes.Red, rc, 0, 20.0F)
            g.FillPie(Brushes.Green, rc, 20.0F, 30.0F)
            g.FillPie(Brushes.Blue, rc, 60.0F, 12.0F)
            g.FillPie(Brushes.Gold, rc, -80.0F, -20.0F)
            For sa As Single = 0 To 359 Step 40
                Dim penColor As Color = Color.FromArgb(penRGB, penRGB, penRGB)
                Dim pen As New Pen(penColor, System.Math.Max(System.Threading.Interlocked.Increment(penWidth), penWidth - 1))
                penRGB = penRGB + 20
                g.DrawArc(pen, rc, sa, 40.0F)
            Next
            g.DrawRectangle(Pens.Red, rc)
            g.DrawBeziers(Pens.Blue, bezierPoints)
            g.DrawString(text, font, Brushes.Black, rc)
        End Sub

        Protected Sub btnexport_Click(sender As Object, e As EventArgs)
            Try
                CreatePDF()
                Dim uid As String = System.Guid.NewGuid().ToString()
                Dim filename As String = Server.MapPath("~") & "\Temp\testpdf" & uid & ".pdf"
                _c1pdf.Save(filename)

                Response.Clear()
                Response.ContentType = "application/pdf"

                Response.TransmitFile(filename)
                Response.Flush()
                File.Delete(filename)
                Response.[End]()
            Catch ex As Exception

                Response.Write(ex.Message)
            End Try

        End Sub
    End Class
End Namespace
