﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="TableOfContents.aspx.vb" Inherits="ControlExplorer.C1PDF.TableOfContents" %>

<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <asp:LinkButton ID="exportbtn" runat="server" Text="PDFを作成" OnClick="exportbtn_Click" Font-Underline="true" />
   
    </asp:Content>

    <asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
        このサンプルでは、目次を持つドキュメントを作成します。
        ドキュメントの作成時にブックマークのリストが追加され、ドキュメントの完成時、このリストを使用して各トピックへのローカルリンクを持つ目次ページを作成します。
        最終的に、目次はページコレクションを使用してドキュメントの最初に移動されます。
        このサンプルでは、PDF の閲覧に Adobe Reader が必要です。
    </asp:Content>
