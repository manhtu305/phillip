﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1PDF.Overview" %>

<asp:content id="content1" contentplaceholderid="Head" runat="Server">

</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContent" runat="server">
       
   <asp:LinkButton ID="btnexport" runat="server" OnClick="btnexport_Click" Text="PDFを作成" Font-Underline="true" />
    </asp:content>

<asp:content id="content3" contentplaceholderid="Description" runat="server">
        PDF for .NET を使用して、アプリケーションから Adobe PDF ドキュメントを作成してください。
        .NET アプリケーションから PDF を作成する利点は、データ接続した PDF ドキュメントが作成できる点です。
        このサンプルでは、NorthWind 製品情報の PDF ファイルを作成します。
        各製品カテゴリは別々のページに出力され、アウトラインにブックマークが追加されます。
    </asp:content>
