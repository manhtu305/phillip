﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Acroform.aspx.vb" Inherits="ControlExplorer.C1PDF.Acroform" %>

<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:LinkButton ID="btnexport" Text="PDFを作成" OnClick="btnexport_Click" runat="server" Font-Underline="true" />

</asp:Content>

<asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
    PDF for .NET を使用すると、 ユーザーが入力したフィールドを含む Acrobat のフォームやドキュメントを作成できます。
    フィールドは AddField メソッドを使用して作成されます。
    テキストボックス、チェックボックス、コンボボックス、ラジオボタン、プッシュボタン、リストボックス、署名を含む様々なフィールドタイプをサポートします。
    このサンプルでは、PDF の閲覧に Adobe Reader が必要です。       
</asp:Content>
