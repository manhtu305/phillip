﻿Imports C1.C1Pdf
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1PDF
    Partial Public Class Acroform
        Inherits System.Web.UI.Page
        Private _textBoxCount As Integer
        Private _checkBoxCount As Integer
        Private _radioButtonCount As Integer
        Private _pushButtonCount As Integer
        Private _comboBoxCount As Integer
        Private _listBoxCount As Integer
        Private _rowCount As Integer
        Private _c1pdf As C1.C1Pdf.C1PdfDocument
        Private tempFiles As Hashtable = Nothing
        Protected Shared TEMP_DIR As String
        Private filename As String = String.Empty

        Protected Sub Page_Load(sender As Object, e As EventArgs)


            ' 表示
            'webBrowser1.Navigate(filename);
        End Sub

        Private Sub CreatePDF()
            _c1pdf = New C1PdfDocument()
            ' pdf ドキュメントを作成
            _c1pdf.Clear()
            _c1pdf.DocumentInfo.Title = "PDF Acroform"
            TEMP_DIR = Server.MapPath("../Temp")

            If Directory.Exists(TEMP_DIR) Then
            Else

                Directory.CreateDirectory(TEMP_DIR)
            End If
            ' ページの領域を計算 (余白の差引)
            Dim rcPage As RectangleF = GetPageRect()
            Dim rc As RectangleF = rcPage


            ' タイトルの追加
            Dim titleFont As New Font("Tahoma", 24, FontStyle.Bold)
            rc = RenderParagraph(_c1pdf.DocumentInfo.Title, titleFont, rcPage, rc, False)

            ' Employees テーブルのレンダリング
            rc = RenderTable(rc, rcPage)

            ' ボタンのレンダリング
            Dim btnFont As New Font("Tahoma", 14, FontStyle.Bold)
            Dim button1 As PdfPushButton = RenderPushButton("Submit", btnFont, New RectangleF(New PointF(rc.X, rc.Y + 10), New SizeF(90, 25)), ButtonLayout.TextLeftImageRight)
            button1.Actions.LostFocus.Add(New PdfPushButton.Action(ButtonAction.GotoPage, "Bmark"))
            button1.BorderStyle = FieldBorderStyle.Inset
            button1.BorderWidth = FieldBorderWidth.Medium
            button1.BorderColor = Color.Black

            Dim button2 As PdfPushButton = RenderPushButton("Clear Fields", btnFont, New RectangleF(New PointF(rc.X + 110, rc.Y + 10), New SizeF(110, 25)), ButtonLayout.TextLeftImageRight)
            button2.Actions.Pressed.Add(New PdfPushButton.Action(ButtonAction.ClearFields))
            button2.BorderStyle = FieldBorderStyle.Dashed
            button2.BorderWidth = FieldBorderWidth.Medium
            button2.BorderColor = Color.Black

            ' ページ番号への2度目のパス
            AddFooters()

            ' ファイルに保存して表示
            ' pdf ファイルに保存

            _c1pdf.Compression = CompressionEnum.None
            Dim uid As String = System.Guid.NewGuid().ToString()
            filename = Server.MapPath("~") & "\Temp\testpdf" & uid & ".pdf"
            _c1pdf.Save(filename)
        End Sub

        Private Function RenderTable(rc As RectangleF, rcPage As RectangleF) As RectangleF
            ' データの取得
            Dim dt As DataTable = DemoDataSource("Employees")
            '  select fonts
            Dim hdrFont As New Font("Tahoma", 12, FontStyle.Bold)
            Dim txtFont As New Font("Tahoma", 10)

            ' テーブルのビルド
            rc = RenderParagraph("NorthWind Employees", hdrFont, rcPage, rc, False)

            ' テーブルのビルド
            rc = RenderTableHeader(hdrFont, rc, New String() {"TextBoxes", "RadioButtons", "CheckBoxes", "ComboBoxes", "ListBoxes"})
            For Each dr As DataRow In dt.Rows
                rc = RenderTableRow(txtFont, hdrFont, rcPage, rc, dr)
            Next

            ' 実行
            Return rc
        End Function

        Private Function RenderTableHeader(font As Font, rc As RectangleF, fields As String()) As RectangleF
            ' セル幅の計算 (全ての列で同様)
            Dim rcCell As RectangleF = rc
            rcCell.Width = rc.Width / fields.Length
            rcCell.Height = 0

            '  セル高さの計算 (全ての列の最大値)
            For Each field As String In fields
                Dim height As Single = _c1pdf.MeasureString(field, font, rcCell.Width).Height
                rcCell.Height = Math.Max(rcCell.Height, height)
            Next

            ' ヘッダーセルのレンダリング
            For Each field As String In fields
                _c1pdf.FillRectangle(Brushes.Black, rcCell)
                _c1pdf.DrawString(field, font, Brushes.White, rcCell)
                rcCell.Offset(rcCell.Width, 0)
            Next

            ' 矩形の更新
            rc.Offset(0, rcCell.Height)
            Return rc
        End Function

        Private Function RenderTableRow(font As Font, hdrFont As Font, rcPage As RectangleF, rc As RectangleF, dr As DataRow) As RectangleF
            _rowCount += 1
            Dim rcCell As RectangleF = rc
            rcCell.Height = 100

            ' 必要に応じて改ページ
            If rcCell.Bottom > rcPage.Bottom Then
                _c1pdf.NewPage()
                rc = RenderTableHeader(hdrFont, rcPage, New String() {"TextBoxes", "RadioButtons", "CheckBoxes", "ComboBoxes", "ListBoxes"})
                rcCell.Y = rc.Y
            End If


            ' データセルのレンダリング
            Dim pen As New Pen(Brushes.Gray, 0.1F)

            _c1pdf.DrawRectangle(pen, rcCell)
            rcCell.Inflate(-4, 0)
            ' 名前のレンダリング
            Dim nameWidth As Single = CSng(rc.Width * 0.5)
            Dim colWidth As Single = rc.Width / 2
            Dim x1 As Single = rc.Location.X + 2
            Dim x2 As Single = rc.Location.X + rc.Width / 2
            Dim y1 As Single = rc.Location.Y + 2
            Dim y2 As Single = rc.Location.Y + 25

            ' 行位置の宣言
            Dim r1c1 As New PointF(rc.Location.X + 2, rc.Location.Y + 2)
            Dim r2c1 As New PointF(rc.Location.X + 2, rc.Location.Y + 30)
            Dim r1c2 As New PointF(rc.Location.X + rc.Width / 2, rc.Location.Y + 2)
            Dim r2c2 As New PointF(rc.Location.X + rc.Width / 2, rc.Location.Y + 32)
            Dim r3c2 As New PointF(rc.Location.X + rc.Width / 2, rc.Location.Y + 62)

            ' 文字列のレンダリング
            Dim employeeName As String = dr("FirstName").ToString() & " " & dr("LastName").ToString()
            _c1pdf.DrawString(employeeName, New Font(font.FontFamily, 20, FontStyle.Bold), Brushes.Black, New RectangleF(r1c1, New SizeF(nameWidth, 25)))

            ' ComboBoxのレンダリング
            _c1pdf.DrawString("Reports to:", font, Brushes.Black, New PointF(x1, y1 + 30))
            Dim reportsTo As Integer = If(String.IsNullOrEmpty(dr("ReportsTo").ToString()), 0, CInt(dr("ReportsTo")))
            RenderComboBox(New String() {"Nancy Davolio", "Andrew Fuller", "Janet Leverling", "Margaret Peacock", "Steven Buchanan", "Michael Suyama", _
                "Robert King"}, reportsTo, font, New RectangleF(New PointF(x1 + 55, y1 + 29), New SizeF(colWidth - 70, 15)))

            ' Radiobuttonsのレンダリング
            _c1pdf.DrawString("Product Division:", font, Brushes.Black, New PointF(x1, y1 + 50))
            RenderRadioButton(True, "GroupDivision" & _rowCount.ToString(), "One", font, New RectangleF(New PointF(x1 + 80, y1 + 50), New SizeF(colWidth - 85, 15)))
            RenderRadioButton(True, "GroupDivision" & _rowCount.ToString(), "Two", font, New RectangleF(New PointF(x1 + 120, y1 + 50), New SizeF(colWidth - 125, 15)))
            RenderRadioButton(True, "GroupDivision" & _rowCount.ToString(), "Three", font, New RectangleF(New PointF(x1 + 160, y1 + 50), New SizeF(colWidth - 165, 15)))

            ' Checkboxのレンダリング
            RenderCheckBox(True, " Receives Email Notifications", font, New RectangleF(New PointF(x2 + 10, y1 + 5), New SizeF(colWidth - 5, 15)))
            _c1pdf.DrawString("Email Address:", font, Brushes.Black, New PointF(x2 + 10, y1 + 30))

            ' Textboxのレンダリング
            RenderTextBox(dr("Lastname").ToString().ToLower() & "@nwind.com", font, New RectangleF(New PointF(x2 + 80, y1 + 29), New SizeF(colWidth - 95, 15)), Color.FromKnownColor(KnownColor.Info), "Enter Email Address", False)

            ' Listboxのレンダリング
            _c1pdf.DrawString("Title:", font, Brushes.Black, New PointF(x2 + 10, y1 + 60))
            RenderListBox(New String() {"Sales Representative", "Sales Manager", "Vice President, Sales", "Inside Sales Coordinator"}, dr("Title").ToString(), font, New RectangleF(New PointF(x2 + 50, y1 + 55), New SizeF(colWidth - 65, 30)))

            rcCell.Inflate(4, 0)


            pen.Dispose()

            ' 矩形の更新
            rc.Offset(0, rcCell.Height)
            Return rc
        End Function

        ' 現在のページ矩形(用紙サイズに依存)を取得して1余白を適用
        Friend Function GetPageRect() As RectangleF
            Dim rcPage As RectangleF = _c1pdf.PageRectangle
            rcPage.Inflate(-72, -72)
            Return rcPage
        End Function

        Friend Function RenderParagraph(text As String, font As Font, rcPage As RectangleF, rc As RectangleF, outline As Boolean, linkTarget As Boolean) As RectangleF
            ' このページに合わない場合は改ページを実行
            rc.Height = _c1pdf.MeasureString(text, font, rc.Width).Height
            If rc.Bottom > rcPage.Bottom Then
                _c1pdf.NewPage()
                rc.Y = rcPage.Top
            End If

            ' 文字列の描画
            _c1pdf.DrawString(text, font, Brushes.Black, rc)

            ' 境界線の表示 (主にワードラップを確認するため)
            ' _c1pdf.DrawRectangle(Pens.Sienna, rc);

            ' アウトラインに見出しを追加
            If outline Then
                _c1pdf.DrawLine(Pens.Black, rc.X, rc.Y, rc.Right, rc.Y)
                _c1pdf.AddBookmark(text, 0, rc.Y)
            End If

            ' リンクターゲットを追加
            If linkTarget Then
                _c1pdf.AddTarget(text, rc)
            End If

            ' 次回用に矩形を更新
            rc.Offset(0, rc.Height)
            Return rc
        End Function
        Friend Function RenderParagraph(text As String, font As Font, rcPage As RectangleF, rc As RectangleF, outline As Boolean) As RectangleF
            Return RenderParagraph(text, font, rcPage, rc, outline, False)
        End Function
        Friend Function RenderParagraph(text As String, font As Font, rcPage As RectangleF, rc As RectangleF) As RectangleF
            Return RenderParagraph(text, font, rcPage, rc, False, False)
        End Function


        ' 共通パラメータとデフォルト名を使用して
        ' PDFドキュメントのフィールドとしてテキストボックスフィールドを追加します
        '  
        Friend Function RenderTextBox(text As String, font As Font, rc As RectangleF, back As Color, toolTip As String, multiline As Boolean) As PdfTextBox
            '  作成
            Dim name As String = String.Format("ACFTB{0}", _textBoxCount + 1)
            Dim textBox As New PdfTextBox()

            '  デフォルトの境界線
            ' textBox.BorderWidth = 3f / 4;
            textBox.BorderStyle = FieldBorderStyle.Solid
            textBox.BorderColor = SystemColors.ControlDarkDark

            ' パラメータ
            textBox.Font = font
            textBox.Name = name
            textBox.DefaultText = text
            textBox.Text = text
            textBox.ToolTip = If(String.IsNullOrEmpty(toolTip), String.Format("{0} ({1})", text, name), toolTip)
            If back <> Color.Transparent AndAlso Not back.IsEmpty Then
                textBox.BackColor = back
            End If
            textBox.IsMultiline = multiline
            ' 追加
            _c1pdf.AddField(textBox, rc)
            _textBoxCount += 1

            ' 実行
            Return textBox
        End Function
        Friend Function RenderTextBox(text As String, font As Font, rc As RectangleF, back As Color) As PdfTextBox
            Return RenderTextBox(text, font, rc, back, Nothing, False)
        End Function
        Friend Function RenderTextBox(text As String, font As Font, rc As RectangleF) As PdfTextBox
            Return RenderTextBox(text, font, rc, Color.Transparent, Nothing, False)
        End Function
        Friend Function RenderTextBox(text As String, font As Font, rc As RectangleF, multiline As Boolean) As PdfTextBox
            Return RenderTextBox(text, font, rc, Color.Transparent, Nothing, multiline)
        End Function

        ' 共通パラメータとデフォルト名を使用して
        ' PDFドキュメントのフィールドとしてコンボボックスフィールドを追加します
        '  
        Friend Function RenderComboBox(list As String(), activeIndex As Integer, font As Font, rc As RectangleF, back As Color, toolTip As String) As PdfComboBox
            ' create
            Dim name As String = String.Format("ACFCLB{0}", _comboBoxCount + 1)
            Dim comboBox As New PdfComboBox()

            ' デフォルトの境界線
            comboBox.BorderWidth = FieldBorderWidth.Thin
            comboBox.BorderStyle = FieldBorderStyle.Solid
            comboBox.BorderColor = SystemColors.ControlDarkDark

            ' 配列
            For Each text As String In list
                comboBox.Items.Add(text)
            Next

            ' パラメータ
            comboBox.Font = font
            comboBox.Name = name
            comboBox.DefaultValue = activeIndex
            comboBox.Value = activeIndex
            comboBox.ToolTip = If(String.IsNullOrEmpty(toolTip), String.Format("{0} ({1})", String.Format("Count = {0}", comboBox.Items.Count), name), toolTip)
            If back <> Color.Transparent AndAlso Not back.IsEmpty Then
                comboBox.BackColor = back
            End If

            ' 追加
            _c1pdf.AddField(comboBox, rc)
            _comboBoxCount += 1

            ' 実行
            Return comboBox
        End Function
        Friend Function RenderComboBox(list As String(), activeIndex As Integer, font As Font, rc As RectangleF, back As Color) As PdfComboBox
            Return RenderComboBox(list, activeIndex, font, rc, back, Nothing)
        End Function
        Friend Function RenderComboBox(list As String(), activeIndex As Integer, font As Font, rc As RectangleF) As PdfComboBox
            Return RenderComboBox(list, activeIndex, font, rc, Color.Transparent, Nothing)
        End Function

        ' 共通パラメータとデフォルト名を使用して
        ' PDFドキュメントのフィールドとしてリストボックスフィールドを追加します
        '  
        Friend Function RenderListBox(list As String(), text As String, font As Font, rc As RectangleF, back As Color, toolTip As String) As PdfListBox
            ' 作成
            Dim name As String = String.Format("ACFLB{0}", _listBoxCount + 1)
            Dim listBox As New PdfListBox()

            ' デフォルトの境界線
            listBox.BorderWidth = FieldBorderWidth.Thin
            listBox.BorderStyle = FieldBorderStyle.Solid
            listBox.BorderColor = SystemColors.ControlDarkDark

            ' 配列
            For Each item As String In list
                listBox.Items.Add(item)
            Next

            ' パラメータ
            listBox.Font = font
            listBox.Name = name
            listBox.Text = text
            listBox.ToolTip = If(String.IsNullOrEmpty(toolTip), String.Format("{0} ({1})", String.Format("Count = {0}", listBox.Items.Count), name), toolTip)
            If back <> Color.Transparent AndAlso Not back.IsEmpty Then
                listBox.BackColor = back
            End If

            ' 追加
            _c1pdf.AddField(listBox, rc)
            _listBoxCount += 1

            ' 実行
            Return listBox
        End Function
        Friend Function RenderListBox(list As String(), text As String, font As Font, rc As RectangleF, back As Color) As PdfListBox
            Return RenderListBox(list, text, font, rc, back, Nothing)
        End Function
        Friend Function RenderListBox(list As String(), text As String, font As Font, rc As RectangleF) As PdfListBox
            Return RenderListBox(list, text, font, rc, Color.Transparent, Nothing)
        End Function

        ' 共通パラメータとデフォルト名を使用して
        ' PDFドキュメントのフィールドとしてラジオボタンフィールドを追加します
        '  
        Friend Function RenderRadioButton(value As Boolean, group As String, text As String, font As Font, rc As RectangleF, back As Color, _
            toolTip As String) As PdfRadioButton
            ' 作成
            Dim name As String = If(String.IsNullOrEmpty(group), "ACFRGR", group)
            Dim radioButton As New PdfRadioButton()

            ' パラメータ
            radioButton.Name = name
            radioButton.DefaultValue = value
            radioButton.Value = value
            radioButton.ToolTip = If(String.IsNullOrEmpty(toolTip), String.Format("{0} ({1})", text, name), toolTip)
            If back <> Color.Transparent AndAlso Not back.IsEmpty Then
                radioButton.BackColor = back
            End If

            ' 追加
            Dim radioSize As Single = font.Size
            Dim radioTop As Single = rc.Top + (rc.Height - radioSize) / 2
            _c1pdf.AddField(radioButton, New RectangleF(rc.Left, radioTop, radioSize, radioSize))
            _radioButtonCount += 1

            ' ラジオボタンフィールドのためのテキスト
            Using brush As New SolidBrush(Color.Black)
                Dim x As Single = rc.Left + radioSize + 1.0F
                Dim y As Single = rc.Top + (rc.Height - radioSize - 1.0F) / 2
                _c1pdf.DrawString(text, New Font(font.Name, radioSize, font.Style), brush, New PointF(x, y))
            End Using

            ' 実行
            Return radioButton
        End Function
        Friend Function RenderRadioButton(value As Boolean, group As String, text As String, font As Font, rc As RectangleF, back As Color) As PdfRadioButton
            Return RenderRadioButton(value, group, text, font, rc, back, _
                Nothing)
        End Function
        Friend Function RenderRadioButton(value As Boolean, group As String, text As String, font As Font, rc As RectangleF) As PdfRadioButton
            Return RenderRadioButton(value, group, text, font, rc, Color.Transparent, _
                Nothing)
        End Function

        ' 共通パラメータとデフォルト名を使用して
        ' PDFドキュメントのフィールドとしてチェックボックスフィールドを追加します
        '  
        Friend Function RenderCheckBox(value As Boolean, text As String, font As Font, rc As RectangleF, back As Color, toolTip As String) As PdfCheckBox
            ' 作成
            Dim name As String = String.Format("ACFCB{0}", _checkBoxCount + 1)
            Dim checkBox As New PdfCheckBox()

            ' デフォルトの境界線
            checkBox.BorderWidth = FieldBorderWidth.Thin
            checkBox.BorderStyle = FieldBorderStyle.Solid
            checkBox.BorderColor = SystemColors.ControlDarkDark

            ' パラメータ
            checkBox.Name = name
            checkBox.DefaultValue = value
            checkBox.Value = value
            checkBox.ToolTip = If(String.IsNullOrEmpty(toolTip), String.Format("{0} ({1})", text, name), toolTip)
            If back <> Color.Transparent AndAlso Not back.IsEmpty Then
                checkBox.BackColor = back
            End If

            ' 追加
            Dim checkBoxSize As Single = font.Size
            Dim checkBoxTop As Single = rc.Top + (rc.Height - checkBoxSize) / 2
            _c1pdf.AddField(checkBox, New RectangleF(rc.Left, checkBoxTop, checkBoxSize, checkBoxSize))
            _checkBoxCount += 1

            ' チェックボックスフィールドのためのテキスト
            Using brush As New SolidBrush(Color.Black)
                Dim x As Single = rc.Left + checkBoxSize + 1.0F
                Dim y As Single = rc.Top + (rc.Height - checkBoxSize - 1.0F) / 2
                _c1pdf.DrawString(text, New Font(font.Name, checkBoxSize, font.Style), brush, New PointF(x, y))
            End Using

            ' 実行
            Return checkBox
        End Function
        Friend Function RenderCheckBox(value As Boolean, text As String, font As Font, rc As RectangleF, back As Color) As PdfCheckBox
            Return RenderCheckBox(value, text, font, rc, back, Nothing)
        End Function
        Friend Function RenderCheckBox(value As Boolean, text As String, font As Font, rc As RectangleF) As PdfCheckBox
            Return RenderCheckBox(value, text, font, rc, Color.Transparent, Nothing)
        End Function

        ' 共通パラメータとデフォルト名を使用して
        ' PDFドキュメントのフィールドとしてプッシュボタンフィールドを追加します
        '  
        Friend Function RenderPushButton(text As String, font As Font, rc As RectangleF, back As Color, fore As Color, toolTip As String, _
            image As System.Drawing.Image, layout As ButtonLayout) As PdfPushButton
            ' 作成
            Dim name As String = String.Format("ACFPB{0}", _pushButtonCount + 1)
            Dim pushButton As New PdfPushButton()

            ' パラメータ
            pushButton.Name = name
            pushButton.DefaultValue = text
            pushButton.Value = text
            pushButton.Font = font
            pushButton.ToolTip = If(String.IsNullOrEmpty(toolTip), String.Format("{0} ({1})", text, name), toolTip)
            If back <> Color.Transparent AndAlso Not back.IsEmpty Then
                pushButton.BackColor = back
            End If
            If fore <> Color.Transparent AndAlso Not fore.IsEmpty Then
                pushButton.ForeColor = fore
            End If

            ' アイコン
            If image IsNot Nothing Then
                pushButton.Image = image
                pushButton.Layout = layout
            End If

            ' 追加
            _c1pdf.AddField(pushButton, rc)
            _pushButtonCount += 1

            ' 実行
            Return pushButton
        End Function
        Friend Function RenderPushButton(text As String, font As Font, rc As RectangleF, back As Color) As PdfPushButton
            Return RenderPushButton(text, font, rc, back, Color.Transparent, Nothing, _
                Nothing, ButtonLayout.TextOnly)
        End Function
        Friend Function RenderPushButton(text As String, font As Font, rc As RectangleF) As PdfPushButton
            Return RenderPushButton(text, font, rc, Color.Transparent, Color.Transparent, Nothing, _
                Nothing, ButtonLayout.TextOnly)
        End Function
        Friend Function RenderPushButton(text As String, font As Font, rc As RectangleF, icon As System.Drawing.Image, layout As ButtonLayout) As PdfPushButton
            Return RenderPushButton(text, font, rc, Color.Transparent, Color.Transparent, Nothing, _
                icon, layout)
        End Function
        Friend Function RenderPushButton(text As String, font As Font, rc As RectangleF, layout As ButtonLayout) As PdfPushButton
            Return RenderPushButton(text, font, rc, Color.Transparent, Color.Transparent, Nothing, _
                Nothing, layout)
        End Function
        Friend Function RenderPushButton(font As Font, rc As RectangleF, back As Color, image As System.Drawing.Image) As PdfPushButton
            Return RenderPushButton(String.Empty, font, rc, back, Color.Transparent, Nothing, _
                image, ButtonLayout.ImageOnly)
        End Function

        '================================================================================
        ' ドキュメントにページフッタを追加
        ' 
        ' このメソッドは、このプロジェクト内の全サンプルから呼び出されます。
        ' これはドキュメントを調査して、各ページフッタにページ番号'page n of m'を追加します。
        ' このフッタはドキュメントの右端に垂直テキストとしてレンダリングされます。
        ' 
        ' 既存ページにコンテンツを追加するのは簡単です。
        ' CurrentPage プロパティに既存ページを設定して通常と同様に記載するだけです。
        ' 
        Private Sub AddFooters()
            Dim fontHorz As New Font("Tahoma", 7, FontStyle.Bold)
            Dim fontVert As New Font("Viner Hand ITC", 14, FontStyle.Bold)

            Dim sfRight As New StringFormat()
            sfRight.Alignment = StringAlignment.Far

            Dim sfVert As New StringFormat()
            sfVert.FormatFlags = sfVert.FormatFlags Or StringFormatFlags.DirectionVertical
            sfVert.Alignment = StringAlignment.Center

            For page As Integer = 0 To _c1pdf.Pages.Count - 1
                ' ページを選択 (ページサイズを変更可能)
                _c1pdf.CurrentPage = page

                ' テキストをレンダリングするための矩形をビルド
                Dim rcPage As RectangleF = GetPageRect()
                Dim rcFooter As RectangleF = rcPage
                rcFooter.Y = rcFooter.Bottom + 6
                rcFooter.Height = 12
                Dim rcVert As RectangleF = rcPage
                rcVert.X = rcPage.Right + 6

                ' 左寄せのフッタを追加
                Dim text As String = _c1pdf.DocumentInfo.Title
                _c1pdf.DrawString(text, fontHorz, Brushes.Gray, rcFooter)

                ' 右寄せのフッタを追加
                text = String.Format("Page {0} of {1}", page + 1, _c1pdf.Pages.Count)
                _c1pdf.DrawString(text, fontHorz, Brushes.Gray, rcFooter, sfRight)

                ' 垂直テキストを追加
                text = Convert.ToString(_c1pdf.DocumentInfo.Title) & " (document created using the C1Pdf .NET component)"
                _c1pdf.DrawString(text, fontVert, Brushes.LightGray, rcVert, sfVert)

                ' ページの下側と右側に線を描画
                _c1pdf.DrawLine(Pens.Gray, rcPage.Left, rcPage.Bottom, rcPage.Right, rcPage.Bottom)
                _c1pdf.DrawLine(Pens.Gray, rcPage.Right, rcPage.Top, rcPage.Right, rcPage.Bottom)
            Next
        End Sub

        Private Function MapJobTitle(title As String) As Integer
            Dim index As Integer = 0
            If title.Equals("Sales Representative") Then
                Return 1
            ElseIf title.Equals("Sales Manager") Then
                Return 2
            ElseIf title.Equals("Vice President, Sales") Then
                Return 3
            ElseIf title.Equals("Inside Sales Coordinator") Then
                Return 4
            End If

            Return index
        End Function


        Public Function GetUniqueTempFileName(ext As String) As String
            Dim appName As String = AppDomain.CurrentDomain.FriendlyName
            Dim guid As String = System.Guid.NewGuid().ToString()
            Return [String].Format("{0}\{1}_{2}{3}", TEMP_DIR, appName, guid, ext)
        End Function

        Public Function DemoDataSource(member As String) As DataTable
            Return DemoDataSource(member, False)
        End Function

        Public ReadOnly Property DemoConnectionString() As String
            Get
                Return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|NWind_ja.mdb;Persist Security Info=False"
            End Get
        End Property

        Public Function DemoDataSource(member As String, [custom] As Boolean) As DataTable
            Dim sql As String = "SELECT * FROM Products"
            Dim dt As New DataTable()
            If [custom] Then
                sql = member
            ElseIf member.Equals("EmployeeOrders") Then
                sql = "SELECT DISTINCTROW Orders.OrderID, Orders.OrderDate, Shippers.CompanyName, Customers.Country, [FirstName] & "" "" & [LastName] AS Salesperson, Products.ProductName AS Product, [Order Details].UnitPrice, [Order Details].Quantity, [Order Details].Discount, CCur([Order Details].[UnitPrice]*[Quantity]*(1-[Discount])/100)*100 AS ExtendedPrice FROM Shippers INNER JOIN (Products INNER JOIN ((Employees INNER JOIN (Customers INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID) ON Employees.EmployeeID = Orders.EmployeeID) INNER JOIN [Order Details] ON Orders.OrderID = [Order Details].OrderID) ON Products.ProductID = [Order Details].ProductID) ON Shippers.ShipperID = Orders.ShipVia;"
            ElseIf member.Equals("Invoices") Then
                sql = "SELECT DISTINCTROW Orders.ShipName, Orders.ShipAddress, Orders.ShipCity, Orders.ShipRegion, Orders.ShipPostalCode, Orders.ShipCountry, Orders.CustomerID, Customers.CompanyName, Customers.Address, Customers.City, Customers.Region, Customers.PostalCode, Customers.Country, [FirstName] & "" "" & [LastName] AS Salesperson, Orders.OrderID, Orders.OrderDate, Orders.RequiredDate, Orders.ShippedDate, Shippers.CompanyName, [Order Details].ProductID, Products.ProductName, [Order Details].UnitPrice, [Order Details].Quantity, [Order Details].Discount, CCur([Order Details].[UnitPrice]*[Quantity]*(1-[Discount])/100)*100 AS ExtendedPrice, Orders.Freight FROM Shippers INNER JOIN (Products INNER JOIN ((Employees INNER JOIN (Customers INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID) ON Employees.EmployeeID = Orders.EmployeeID) INNER JOIN [Order Details] ON Orders.OrderID = [Order Details].OrderID) ON Products.ProductID = [Order Details].ProductID) ON Shippers.ShipperID = Orders.ShipVia;"
            ElseIf member.Equals("Customers") Then
                sql = "SELECT * FROM Customers"
            ElseIf member.Equals("Customers_OrderByCountry") Then
                sql = "SELECT * FROM Customers ORDER BY Country"
            ElseIf member.Equals("Employees") Then
                sql = "SELECT * FROM Employees"
            End If

            Dim da As New OleDbDataAdapter(sql, DemoConnectionString)
            da.Fill(dt)
            Return dt
        End Function

        Protected Sub btnexport_Click(sender As Object, e As EventArgs)
            Try
                CreatePDF()
                Response.Clear()
                Response.Charset = "UTF-8"
                Response.ContentEncoding = System.Text.Encoding.UTF8
                Response.AddHeader("Content-Disposition", "attachment; filename=" & filename)
                Response.ContentType = "application/pdf"
                Response.WriteFile(filename)
                Response.Flush()
                File.Delete(filename)
                Response.[End]()
            Catch ex As Exception

                Response.Write(ex.Message)
            End Try


        End Sub
    End Class
End Namespace
