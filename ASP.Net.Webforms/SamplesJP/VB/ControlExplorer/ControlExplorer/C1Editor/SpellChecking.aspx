﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="SpellChecking.aspx.vb" Inherits="ControlExplorer.C1Editor.SpellChecking" %>


<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="480" >
	</wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1Editor</strong> は組み込みのスペルチェッカを備えています。Microsoft Word と同様に、辞書をダウンロードする必要なく、スペルチェックを実行して赤い波線の下線を表示します。
    </p>
	<p>
    ※スペルチェック機能は日本語の文章には対応していません。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
