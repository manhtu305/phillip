﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="CustomizeSimple.aspx.vb" Inherits="ControlExplorer.C1Editor.CustomizeSimple" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="480" Mode="Simple"></wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
    <strong>C1Editor</strong> は、簡易ツールバーモードを使用する際に、ツールバーのカスタマイズをサポートします。
	ツールバーではカスタムイベントハンドラを使用できます。
	</p>
	<p>以下のプロパティを設定すると、簡易ツールバーのカスタマイズが有効になります。</p>
	<ul>
	<li><strong>Mode </strong>- 値が「Simple」の場合のみ有効になります。</li>
	<li><strong>SimpleModeCommands </strong>- ツールバー内のカスタムボタン。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
