﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="EditingModes.aspx.vb" Inherits="ControlExplorer.C1Editor.EditingModes" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="360" ></wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1Editor</strong> は、デザイン、ソース、並べて表示の３種類の編集ビューをサポートします。
	エディタの下部にあるツールバーを使用して、ユーザーはビューを切り替えることができます。
	<strong>C1Editor</strong> は全画面表示モードもサポートし、ブラウザウィンドウ（または親フレーム）全体に合わせてコントロールがサイズ変更されます。
    全画面表示モードはすべてのビューで使用できます。
	</p>
	<p><strong>EditorMode</strong> プロパティは、以下の値に設定できます。</p>
	<ul>
	<li><strong>WYSIWYG </strong>- デザイン</li>
	<li><strong>Code </strong>- ソース</li>
	<li><strong>Split </strong>- 並べて表示</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<fieldset>
		<legend>編集モード</legend>
		<div class="controloptionswrapper radio">
			<div class="controloptions">
				<input type="radio" id="view_Design" name="view" checked="checked" onclick="switchview(1)" />
				<label for="view_Design">
					デザイン</label>
			</div>
			<div class="controloptions">
				<input type="radio" id="view_Source" name="view" onclick="switchview(2)" />
				<label for="view_Source">
					ソース</label>
			</div>
			<div class="controloptions">
				<input type="radio" id="view_Split" name="view" onclick="switchview(3)" />
				<label for="view_Split">
					並べて表示</label>
			</div>
		</div>
	</fieldset>
	<script type="text/javascript">
		function switchview(index) {
			switch (index) {
				case 1:
					$("#<%= Editor1.ClientID %>").c1editor("executeEditorAction", "wysiwyg");
					break;
				case 2:
					$("#<%= Editor1.ClientID %>").c1editor("executeEditorAction", "code");
					break;
				case 3:
					$("#<%= Editor1.ClientID %>").c1editor("executeEditorAction", "split");
					break;
			}
		}        
	</script>
</asp:Content>
