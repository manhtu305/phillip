Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO

Namespace ControlExplorer.C1Editor
	Public Partial Class CustomizeSimple
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				Editor1.Text = GetFileContent()
				Editor1.SimpleModeCommands = New String() {"Bold", "Italic", "Link", "BlockQuote", "StrikeThrough", "InsertImage"}
			End If
		End Sub

		Private Function GetFileContent() As String
			Dim text As String = String.Empty
			Dim sr As StreamReader = File.OpenText(Server.MapPath("~/App_Data/SimpleEditorText.txt"))
			Try
				text = sr.ReadToEnd()
			Catch
			Finally
				sr.Close()
			End Try
			Return text
		End Function
	End Class
End Namespace
