Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO

Namespace ControlExplorer.C1Editor
	Public Partial Class BBCode
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				Editor1.Text = "[B]サンプルテキスト[/B]。[URL=http://www.grapecity.com/tools/]Grapecity Web サイト[/URL]をご参照ください。"
			End If
		End Sub

	End Class
End Namespace
