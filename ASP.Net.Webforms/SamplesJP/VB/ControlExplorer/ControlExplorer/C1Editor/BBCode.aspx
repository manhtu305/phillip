﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="BBCode.aspx.vb" Inherits="ControlExplorer.C1Editor.BBCode" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="480" Mode="Bbcode" ></wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		<strong>C1Editor </strong>は、Bulletin Board Code（BBCode）に対応します。BBCode は軽量マークアップ言語であり、多くの有名なフォーラムで使用されています。
	</p>
	<p>BBCode モードを有効にするには、下記のプロパティを設定する必要があります。</p>
	<ul>
	<li><strong>Mode </strong>- 値が「Bbcode」の場合、BBCode モードが有効になります。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
