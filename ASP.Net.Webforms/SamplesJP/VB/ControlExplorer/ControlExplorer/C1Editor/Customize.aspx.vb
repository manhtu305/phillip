Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO

Namespace ControlExplorer.C1Editor
	Public Partial Class Customize
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				Editor1.Text = GetFileContent()
				Editor1.RibbonUI.TabPages.RemoveAt(1)
				Dim page As New C1.Web.Wijmo.Controls.C1Editor.C1RibbonTabPage()
				page.Name = "Test"
				page.Text = "テスト"
				Editor1.RibbonUI.TabPages.Add(page)

				Dim group As New C1.Web.Wijmo.Controls.C1Editor.C1RibbonGroup()
				group.Name = "グループ"
				page.Groups.Add(group)

					'Editor1.SimpleModeCommands
				group.Buttons.Add(New C1.Web.Wijmo.Controls.C1Editor.C1RibbonButton("imagebrowser", "画像の参照", "wijmo-wijribbon-imagebrowser"))
			End If
		End Sub

		Private Function GetFileContent() As String
			Dim text As String = String.Empty
			Dim sr As StreamReader = File.OpenText(Server.MapPath("~/App_Data/ComponentOne.txt"))
			Try
				text = sr.ReadToEnd()
			Catch
			Finally
				sr.Close()
			End Try
			Return text
		End Function
	End Class
End Namespace
