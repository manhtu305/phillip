﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Customize.aspx.vb" Inherits="ControlExplorer.C1Editor.Customize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="480">
	</wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
    <strong>C1Editor</strong> はツールバーのカスタマイズをサポートします。
    <strong>TabPages</strong>、<strong>RibbonGroups</strong>、<strong>Buttons</strong> を追加／削除することで、リボンスタイルの UI を変更できます。
    このサンプルでは、画像の参照ボタンを含む「テスト」タブを追加しています。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
