﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" 
CodeBehind="CustomizeFont.aspx.vb" Inherits="ControlExplorer.C1Editor.CustomizeFont" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="480" FontNames="メイリオ,ＭＳ Ｐゴシック,ＭＳ Ｐ明朝,MS UI Gothic"
	 FontSizes="10px,11px,12px,13px,14px,15px"  defaultFontName="ＭＳ Ｐゴシック" defaultFontSize="15px"></wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、フォント名とフォントサイズを変更する方法を紹介します。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
