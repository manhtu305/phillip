﻿Imports C1.C1Excel
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1Chart
Imports ControlExplorer.C1Excel.Data

Namespace ControlExplorer.C1Excel
    Partial Public Class LoadingWorkSheets
        Inherits System.Web.UI.Page
        Private _xlBook As New C1XLBook()
        Private cdsTemp As CompositeChartSeries
        Private cdsPress As CompositeChartSeries
        Private cdsCond As CompositeChartSeries
        Private cdsPh As CompositeChartSeries

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            cdsTemp = New CompositeChartSeries()
            cdsPress = New CompositeChartSeries()
            cdsCond = New CompositeChartSeries()
            cdsPh = New CompositeChartSeries()
            LoadChart(GetFilename)
        End Sub

        Private Function GetChartData(book As C1XLBook) As DrillDataPoints
            ' 最初のシートを取得
            Dim sheet = book.Sheets(0)

            ' location、date、cell数を取得
            Dim location = TryCast(sheet(1, 1).Value, String)
            Dim [date] = CType(sheet(2, 1).Value, DateTime)
            Dim count = sheet.Rows.Count - 5
            ' label.Text = string.Format("{0}, {1} points", location, count);

            ' チャート用の配列に値を格納
            Dim drillData = New DrillDataPoints(count)
            For r As Integer = 0 To count - 1
                drillData.Temperature(r) = CDbl(sheet(r + 5, 1).Value)
                drillData.Pressure(r) = CDbl(sheet(r + 5, 2).Value)
                drillData.Conductivity(r) = CDbl(sheet(r + 5, 3).Value)
                drillData.Ph(r) = CDbl(sheet(r + 5, 4).Value)
                drillData.Depth(r) = r
            Next
            drillData.ScaleValues()

            ' データチャートに送信
            Return drillData
        End Function

        Private Sub LoadChart(filename As String)
            If Not String.IsNullOrEmpty(filename) Then
                Try
                    _xlBook.Load(AppDomain.CurrentDomain.BaseDirectory & "C1Excel\Data\" & filename)
                Catch generatedExceptionName As Exception
                    Response.Write("Unable to load Excel file: " & filename)
                End Try
            Else
                Return
            End If

            ' ロードしたExcel Bookからデータを取得
            Dim data = GetChartData(_xlBook)
            Me.weatherchart.SeriesList.Clear()
            cdsTemp.Data.Y.AddRange(data.Temperature)
            cdsTemp.Data.X.AddRange(data.Depth)
            cdsTemp.Label = "Temperatue"
            cdsPress.Data.Y.AddRange(data.Pressure)
            cdsPress.Data.X.AddRange(data.Depth)
            cdsPress.Label = "Pressure"
            cdsCond.Data.Y.AddRange(data.Conductivity)
            cdsCond.Data.X.AddRange(data.Depth)
            cdsCond.Label = "Conductivity"
            cdsPh.Data.Y.AddRange(data.Ph)
            cdsPh.Data.X.AddRange(data.Depth)
            cdsPh.Label = "Ph"
            cdsTemp.Type = ChartSeriesType.Bezier
            cdsPress.Type = ChartSeriesType.Area
            cdsPh.Type = ChartSeriesType.Spline
            cdsCond.Type = ChartSeriesType.Line
            Me.weatherchart.SeriesList.Add(cdsTemp)
            Me.weatherchart.SeriesList.Add(cdsPress)
            Me.weatherchart.SeriesList.Add(cdsCond)
            Me.weatherchart.SeriesList.Add(cdsPh)

            weatherchart.ShowChartLabels = False

            weatherchart.Axis.X.UnitMajor = 10
            weatherchart.Axis.X.Alignment = ChartAxisAlignment.Near
        End Sub

        Private ReadOnly Property GetFilename() As String
            Get
                Return "Houston.xlsx"
            End Get
        End Property



    End Class
End Namespace
