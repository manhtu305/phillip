﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Drawing

Namespace ControlExplorer.C1Excel.Data
    Public Class DrillDataPoints
        '-----------------------------------------------------------------
#Region "** ctor"

        Public Sub New(count As Integer)
            Temperature = New Double(count - 1) {}
            Pressure = New Double(count - 1) {}
            Conductivity = New Double(count - 1) {}
            Ph = New Double(count - 1) {}
            Depth = New Double(count - 1) {}
        End Sub

#End Region

        '-----------------------------------------------------------------
#Region "** object model"

        Public Property Temperature() As Double()
            Get
                Return m_Temperature
            End Get
            Set(value As Double())
                m_Temperature = Value
            End Set
        End Property
        Private m_Temperature As Double()
        Public Property Pressure() As Double()
            Get
                Return m_Pressure
            End Get
            Set(value As Double())
                m_Pressure = Value
            End Set
        End Property
        Private m_Pressure As Double()
        Public Property Conductivity() As Double()
            Get
                Return m_Conductivity
            End Get
            Set(value As Double())
                m_Conductivity = Value
            End Set
        End Property
        Private m_Conductivity As Double()
        Public Property Ph() As Double()
            Get
                Return m_Ph
            End Get
            Set(value As Double())
                m_Ph = Value
            End Set
        End Property
        Private m_Ph As Double()
        Public Property Depth() As Double()
            Get
                Return m_Depth
            End Get
            Set(value As Double())
                m_Depth = Value
            End Set
        End Property
        Private m_Depth As Double()

        Public Function GetSeriesNames() As String()
            Return "Temperature|Pressure|Conductivity|Ph".Split("|"c)
        End Function


#End Region

        '-----------------------------------------------------------------
#Region "** scale values so they can be seen when plotted together"

        Public Sub ScaleValues()
            Dim max As Double, min As Double
            GetRange(Temperature, max, min)
            ScaleValues(Pressure, max, min)
            ScaleValues(Conductivity, max, min)
            ScaleValues(Ph, max, min)
        End Sub
        Private Sub GetRange(values As Double(), ByRef max As Double, ByRef min As Double)
            max = values(0)
            min = values(0)
            For i As Integer = 1 To values.Length - 1
                max = Math.Max(max, values(i))
                min = Math.Min(min, values(i))
            Next
        End Sub
        Private Sub ScaleValues(values As Double(), max As Double, min As Double)
            Dim rmax As Double, rmin As Double
            GetRange(values, rmax, rmin)
            For i As Integer = 0 To values.Length - 1
                values(i) = (values(i) - rmin) / (rmax - rmin) * (max - min) + min
            Next
        End Sub

#End Region
    End Class
End Namespace
