﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.C1Excel
Imports System.Drawing
Imports System.Data
Imports System.Diagnostics
Imports System.Data.OleDb
Imports System.Collections
Imports System.IO
Namespace ControlExplorer.C1Excel
    Partial Public Class CreatingWorkSheets
        Inherits System.Web.UI.Page

        'Excel スタイル
        Private _styTitle As XLStyle
        Private _styHeader As XLStyle
        Private _styMoney As XLStyle
        Private _styOrder As XLStyle
        Private _c1xl As C1XLBook = New C1.C1Excel.C1XLBook()

        Private tempFiles As Hashtable = Nothing
        Protected Shared TEMP_DIR As String


        Protected Sub Page_Load(sender As Object, e As EventArgs)
            TEMP_DIR = Server.MapPath("../Temp")

            If Directory.Exists(TEMP_DIR) Then
            Else

                Directory.CreateDirectory(TEMP_DIR)
            End If
        End Sub

        Private Function CreateExcelFile() As String
            'Excel bookと空のシートを削除
            _c1xl.Clear()
            _c1xl.Sheets.Clear()
            _c1xl.DefaultFont = New Font("Tahoma", 8)

            'Excel スタイルを作成
            _styTitle = New XLStyle(_c1xl)
            _styHeader = New XLStyle(_c1xl)
            _styMoney = New XLStyle(_c1xl)
            _styOrder = New XLStyle(_c1xl)

            'スタイル設定
            _styTitle.Font = New Font(_c1xl.DefaultFont.Name, 15, FontStyle.Bold)
            _styTitle.ForeColor = Color.Blue
            _styHeader.Font = New Font(_c1xl.DefaultFont, FontStyle.Bold)
            _styHeader.ForeColor = Color.White
            _styHeader.BackColor = Color.DarkGray
            _styMoney.Format = XLStyle.FormatDotNetToXL("c")
            _styOrder.Font = _styHeader.Font
            _styOrder.ForeColor = Color.Red

            'カテゴリ毎に1シートのレポートを作成
            Dim dt As DataTable = GetCategories()
            For Each dr As DataRow In dt.Rows
                CreateSheet(dr)
            Next

            'xls ファイルを保存

            Dim filename As String = GetTempFileName(".xls")
            _c1xl.Save(filename)

            Return filename

        End Function

        Private Function GetCategories() As DataTable
            Dim conn As String = Me.DemoConnectionString

            'テーブルにデータをロード
            Dim dtCategories As New DataTable("Categories")
            Dim dtProducts As New DataTable("Products")
            Dim daCat As New OleDbDataAdapter("select * from Categories", conn)
            Dim daPrd As New OleDbDataAdapter("select * from Products", conn)
            daCat.Fill(dtCategories)
            daPrd.Fill(dtProducts)

            '計算列を追加
            Dim col As DataColumn = dtProducts.Columns.Add("ValueInStock", GetType(Decimal), "UnitPrice * UnitsInStock")
            col.Caption = "Value In Stock"
            col = dtProducts.Columns.Add("OrderNow", GetType(Boolean), "UnitsInStock <= ReorderLevel")
            col.Caption = "Order Now"

            'カテゴリと製品名でリレーションしたデータセットをビルド
            Dim ds As New DataSet()
            ds.Tables.Add(dtCategories)
            ds.Tables.Add(dtProducts)
            ds.Relations.Add("Categories_Products", dtCategories.Columns("CategoryID"), dtProducts.Columns("CategoryID"))

            'カテゴリのテーブル
            Return dtCategories
        End Function

        Private Sub CreateSheet(dr As DataRow)
            '現在のカテゴリ名を取得
            Dim catName As String = DirectCast(dr("CategoryName"), String)

            '新規ワークシートをワークブックに追加 
            '('/' is invalid in sheet names, so replace it with '+')
            Dim sheetName As String = catName.Replace("/", " + ")
            Dim sheet As XLSheet = _c1xl.Sheets.Add(sheetName)

            'ワークシートにタイトルを追加
            sheet(0, 0).Value = catName
            sheet.Rows(0).Style = _styTitle

            '列幅を設定 (twips単位)
            sheet.Columns(0).Width = 300
            sheet.Columns(1).Width = 2200
            sheet.Columns(2).Width = 1000
            sheet.Columns(3).Width = 1600
            sheet.Columns(4).Width = 1000
            sheet.Columns(5).Width = 1000
            sheet.Columns(6).Width = 1000

            '列ヘッダを追加
            Dim row As Integer = 2
            sheet.Rows(row).Style = _styHeader
            sheet(row, 1).Value = "Product Name"
            sheet(row, 2).Value = "Unit Price"
            sheet(row, 3).Value = "Qty/Unit"
            sheet(row, 4).Value = "Stock Units"
            sheet(row, 5).Value = "Stock Value"
            sheet(row, 6).Value = "Reorder"

            'このカテゴリ内で製品をループ
            Dim products As DataRow() = dr.GetChildRows("Categories_Products")
            For Each product As DataRow In products
                '次の行へ移動
                row += 1

                '行の追加
                sheet(row, 1).Value = product("ProductName")
                sheet(row, 2).Value = product("UnitPrice")
                sheet(row, 3).Value = product("QuantityPerUnit")
                sheet(row, 4).Value = product("UnitsInStock")

                'stockの値を計算
                Dim valueInStock As Double = Convert.ToDouble(product("UnitPrice")) * Convert.ToInt32(product("UnitsInStock"))
                sheet(row, 5).Value = valueInStock

                'reorder levelを確認
                If Convert.ToInt32(product("UnitsInStock")) <= Convert.ToInt32(product("ReorderLevel")) Then
                    sheet(row, 6).Value = "<<<"
                    sheet(row, 6).Style = _styOrder
                End If

                'money cellsのフォーマット
                sheet(row, 2).Style = _styMoney
                sheet(row, 5).Style = _styMoney
            Next
            If products.Length = 0 Then
                row += 1
                sheet(row, 1).Value = "No products in this category"
            End If
        End Sub

        Protected Sub btnexcel_Click(sender As Object, e As EventArgs)
            Dim filename As String = CreateExcelFile()
            Try

                Response.Clear()
                Response.Charset = "UTF-8"
                Response.ContentEncoding = System.Text.Encoding.UTF8
                Response.AddHeader("Content-Disposition", "attachment; filename=" & filename)
                Response.ContentType = "application/ms-excel"
                Response.TransmitFile(filename)
                Response.Flush()
                File.Delete(filename)
                Response.[End]()
            Catch
                Response.Write("Unable to load file from temp directory: " & filename)
            End Try
        End Sub

        Public Function GetTempFileName(ext As String) As String
            If tempFiles Is Nothing Then
                tempFiles = New Hashtable()
            End If

            If Not tempFiles.Contains(ext) Then
                Dim tempFile As String = GetUniqueTempFileName(ext)
                tempFiles.Add(ext, tempFile)
            End If

            Return tempFiles(ext).ToString()
        End Function

        Public Function GetUniqueTempFileName(ext As String) As String
            Dim appName As String = AppDomain.CurrentDomain.FriendlyName
            Dim guid As String = System.Guid.NewGuid().ToString()
            Return [String].Format("{0}\{1}_{2}{3}", TEMP_DIR, appName, guid, ext)
        End Function

        Public ReadOnly Property DemoConnectionString() As String
            Get
                Return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|Nwind_ja.mdb;Persist Security Info=False"
            End Get
        End Property

    End Class
End Namespace
