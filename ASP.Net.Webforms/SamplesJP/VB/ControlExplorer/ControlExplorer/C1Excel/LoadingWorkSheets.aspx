﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="LoadingWorkSheets.aspx.vb" Inherits="ControlExplorer.C1Excel.LoadingWorkSheets" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>



<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
      <wijmo:C1CompositeChart ID="weatherchart" runat="server" Width="800" Height="500">

      </wijmo:C1CompositeChart>
   
    </asp:Content>

    <asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
        Excel for .NET により Excel ファイルをアプリケーションに読み込むことができ、Microsoft Excel のインストールさえも不要となります。
        Excel ファイルから取得したデータは様々な形で利用できます。
        このサンプルでは、C1XLBook コンポーネントを使用して、C1CompositeChart コントロールのデータソースとして使用される配列にデータを読み込みます。   
    </asp:Content>