﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Excel.CreatingWorkSheets" %>

<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Excelを作成" />
        <br/>
     </asp:Content>

    <asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server"    >
        Excel for .NET により、アプリケーションのコードから直接 Microsoft Excel ファイルを作成することができます。
        コード上で公開されている操作性の容易な Excel オブジェクトモデルにより、可能性は無限です。
        グリッドやスケジュール、チャートのように直接 Excel 出力をサポートしないコンポーネントのデータから Excel ファイルを作成できます。
        Excel for .NET は数式、セルスタイル、書式設定をサポートします。
        このサンプルでは、C1XLBook コントロールを使用して NorthWind 製品情報のワークブックを作成します。
        各製品カテゴリは別々のワークシートに出力されます。
    </asp:Content>
