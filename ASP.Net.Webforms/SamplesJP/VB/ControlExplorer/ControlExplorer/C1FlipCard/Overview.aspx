﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    Inherits="FlipCard_Overview" CodeBehind="Overview.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FlipCard"
    TagPrefix="C1FlipCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <C1FlipCard:C1FlipCard ID="FlipCard1" runat="server">
        <FrontSide>
            FlipCardの表
        </FrontSide>
        <BackSide>
            FlipCardの裏
        </BackSide>
    </C1FlipCard:C1FlipCard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルは<strong>C1FlipCard</strong>コントロールの既定の動作を説明します。
    </p>
    <p>
        <strong>C1FlipCard</strong>は、表と裏を反転させることができるパネルを表示します。
        <strong>CurrentSide</strong>プロパティを設定すると、表と裏のどちらを表示するかを変更できます。
        <strong>CurrentSide</strong>プロパティの既定値はFrontです。
    </p>
</asp:Content>
