﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    Inherits="FlipCard_SideState" CodeBehind="SideState.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FlipCard"
    TagPrefix="C1FlipCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <C1FlipCard:C1FlipCard ID="FlipCard1" runat="server" Width="50" Height="50" OnSideChanged="FlipCard1_SideChanged">
        <FrontSide>表</FrontSide>
        <BackSide>裏</BackSide>
    </C1FlipCard:C1FlipCard>
    <br />
    <asp:Button ID="Button1" runat="server" Text="Post" OnClick="Button1_Click" />
    <br />
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FlipCard</strong>コントロールをチェックボックスやスイッチのように使用します。
    </p>
    <p>
        <strong>C1FlipCard</strong>では、2つの状態を持ちます。ポストバック時に<strong>CurrentSide</strong>プロパティをチェックして状態を取得できます。また、状態が変更されたときに<strong>SideChanged</strong>サーバー側イベントが発生します。
    </p>
</asp:Content>
