﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    Inherits="FlipCard_Children" CodeBehind="Children.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FlipCard"
    TagPrefix="C1FlipCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <C1FlipCard:C1FlipCard ID="FlipCard1" runat="server">
        <FrontSide>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/C1BubbleChart/images/chrome.png" />
        </FrontSide>
        <BackSide>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/C1BubbleChart/images/firefox.png" />
        </BackSide>
    </C1FlipCard:C1FlipCard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FlipCard</strong>コントロールでASP.NET Webコントロールをホストします。
    </p>
    <p>
        <strong>C1FlipCard</strong>では、<strong>FrontPanel</strong>と<strong>BackPanel</strong>に任意のコンテンツを配置できます。
    </p>
</asp:Content>
