﻿Public Class FlipCard_SideState
    Inherits System.Web.UI.Page

    Private _changed As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub FlipCard1_SideChanged(sender As Object, e As EventArgs)
        _changed = True
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs)
        Dim gender As String = If(FlipCard1.CurrentSide = C1.Web.Wijmo.Controls.C1FlipCard.FlipCardSide.Front, "表", "裏")
        Label1.Text = "現在の値は" & gender & "です。"
        If _changed Then
            Label1.Text += "値が変更されました。"
        End If
    End Sub

End Class