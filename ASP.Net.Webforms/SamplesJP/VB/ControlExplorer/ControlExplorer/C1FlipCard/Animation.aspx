﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    Inherits="FlipCard_Animation" CodeBehind="Animation.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FlipCard"
    TagPrefix="C1FlipCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <C1FlipCard:C1FlipCard ID="FlipCard1" runat="server">
        <Animation Direction="Vertical" Duration="800" />
        <FrontSide>
            FlipCardの表<br />
            垂直方向に反転します
        </FrontSide>
        <BackSide>
            FlipCardの裏<br />
            垂直方向に反転します
        </BackSide>
    </C1FlipCard:C1FlipCard>
    <br />
    <C1FlipCard:C1FlipCard ID="C1FlipCard2" runat="server" TriggerEvent="MouseEnter">
        <Animation Duration="800" />
        <FrontSide>
            FlipCardの表<br />
            マウス移動で反転します
        </FrontSide>
        <BackSide>
            FlipCardの裏<br />
            マウス移動で反転します
        </BackSide>
    </C1FlipCard:C1FlipCard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FlipCard</strong>コントロールのアニメーションオプションを設定します。
    </p>
    <p>
        <strong>C1FlipCard</strong>では、<strong>TriggerEvent</strong>、<strong>Direction</strong>、<strong>Duration</strong>のプロパティを設定することができます。
    </p>
</asp:Content>
