﻿Partial Public Class FlipCard_Callback
    Inherits System.Web.UI.Page
    Private Shared _rand As New Random()

    Protected Sub Page_Load(sender As Object, e As EventArgs)

    End Sub

    Protected Sub FlipCard1_UpdateContent(sender As Object, e As C1.Web.Wijmo.Controls.C1FlipCard.FlipCardEventArgs)
        ' Get current html content through e.Data
        Dim data = e.Data

        ' Seems we don't use that for now.
        ' Set new html content to e.Result.

        Dim symbols As String() = {"Diamond", "Club", "Heart", "Spade"}
        Dim numbers As String() = {"A", "2", "3", "4", "5", "6", _
            "7", "8", "9", "10", "J", "Q", _
            "K"}
        Dim symbol = symbols(_rand.[Next](0, 3))
        Dim number = numbers(_rand.[Next](0, 12))
        e.Result = symbol & "<br/>" & number
    End Sub
End Class
