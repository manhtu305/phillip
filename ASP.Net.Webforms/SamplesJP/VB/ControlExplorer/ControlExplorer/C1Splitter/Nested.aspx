﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Nested.aspx.vb" Inherits="ControlExplorer.C1Splitter.Nested" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Splitter" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function refresh() {
            $("#<%=hsplitter.ClientID%>").c1splitter("refresh");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Splitter ID="vsplitter" runat="server" OnClientExpanded="refresh" OnClientCollapsed="refresh" OnClientSized="refresh">
        <Panel2>
            <ContentTemplate>
                <wijmo:C1Splitter ID="hsplitter" runat="server" FullSplit="True" Orientation="Horizontal">
                </wijmo:C1Splitter>
            </ContentTemplate>
        </Panel2>
    </wijmo:C1Splitter>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Splitter</strong> はテンプレートをサポートし、ネストされたスプリッターを作成できます。</p>
    <p>
        ネストされたスプリッターを作成するには、下記のプロパティとイベントを使用します。</p>
    <ul>
        <li>ContentTemplate</li>
        <li>FullSplit</li>
        <li>Orientation</li>
        <li>OnClientExpanded</li>
        <li>OnClientCollapsed</li>
        <li>OnClientSized</li>
    </ul>
    <p>
        このサンプルでは、ContentTemplate を使用して垂直方向のスプリッタの中に水平方向のスプリッタを配置しています。
        また、<strong>FullSplit</strong> プロパティを使用して水平方向のスプリッタを垂直方向のスプリッタのパネル全体に表示し、<strong>OnClientExpanded</strong>／<strong>OnClientCollapsed</strong>／<strong>OnClientSized</strong> イベントを使用して垂直方向のスプリッタのリサイズ時にネストされた水平方向のスプリッタを更新しています。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
