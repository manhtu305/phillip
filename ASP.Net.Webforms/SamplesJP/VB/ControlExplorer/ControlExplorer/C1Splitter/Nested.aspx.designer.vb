'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Splitter


	Public Partial Class Nested

		''' <summary>
		''' vsplitter コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected vsplitter As Global.C1.Web.Wijmo.Controls.C1Splitter.C1Splitter

		''' <summary>
		''' hsplitter コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected hsplitter As Global.C1.Web.Wijmo.Controls.C1Splitter.C1Splitter
	End Class
End Namespace
