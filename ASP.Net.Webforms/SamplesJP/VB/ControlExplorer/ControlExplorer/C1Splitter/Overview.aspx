﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Splitter.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Splitter" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .splitterContainer
        {
            height: 210px;
        }
        
        .layout
        {
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="splitterContainer">
        <div class="layout">
            <p>
                垂直方向: パネル1</p>
            <wijmo:C1Splitter runat="server" ID="C1Splitter1" Orientation="Vertical" Width="200px" Height="200px" />
            <p>
                垂直方向: パネル2</p>
            <wijmo:C1Splitter runat="server" ID="C1Splitter3" Orientation="Vertical" Width="200px" CollapsingPanel="Panel2" Height="200px" />

        </div>
        <div class="layout" style="width: 100px;">
            &nbsp;</div>
        <div class="layout">
            <p>
                水平方向: パネル1</p>
            <wijmo:C1Splitter runat="server" ID="C1Splitter2" Orientation="Horizontal" Width="200px" Height="200px" />
            <p>
                水平方向: パネル2</p>
            <wijmo:C1Splitter runat="server" ID="C1Splitter4" Orientation="Horizontal" Width="200px" CollapsingPanel="Panel2" Height="200px" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Splitter</strong> はコンテナコントロールであり、コンテナの表示領域を2つのリサイズ可能なペインに分割するバーを提供します。</p>
    <p>
        このサンプルでは、<strong>Orientation</strong> プロパティを使用して水平方向や垂直方向のスプリッターを作成する方法を紹介します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
