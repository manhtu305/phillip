﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputDate_DropDown" Codebehind="DropDown.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputDate ID="C1InputDate1" runat="server" ShowTrigger="true">
<ComboItems>
<wijmo:C1ComboBoxItem Text="1980/4/8" Value="1980/4/8" /> 
<wijmo:C1ComboBoxItem Text="2007/12/25" Value="2007/12/25" /> 
<wijmo:C1ComboBoxItem Text="2011/5/16" Value="2011/5/16" /> 
</ComboItems> 
</wijmo:C1InputDate>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
	    <strong>C1InputDate</strong> は、ドロップダウンリストからの日付の選択をサポートしています。
    </p>
    <p>
        この場合、<b>ShowTriggers</b> プロパティを true に設定して、<b>ComboItems</b> プロパティに項目リストを定義する必要があります。
    </p>
    <p>
        項目リストが指定されていない場合、トリガボタンをクリックすると、<strong>DatePicker</strong> サンプルのようにカレンダーがドロップダウン表示されます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

