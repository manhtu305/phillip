﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="NullText.aspx.vb" Inherits=".NullText" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1InputDate ID="C1InputDate1" runat="server" Width="350px" DateFormat="d" Placeholder="誕生日">
    </wijmo:C1InputDate>
    <br/>
    <wijmo:C1InputDate ID="C1InputDate2" runat="server" Width="350px" DateFormat="g" Placeholder="フライト時刻">
    </wijmo:C1InputDate>
    <br/>
    <wijmo:C1InputDate ID="C1InputDate3" runat="server" Width="350px" DateFormat="U" Placeholder="日付を入力してください...">
    </wijmo:C1InputDate>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1InputDate</strong> では、空の日付値を適用したときに、入力ヒントが表示されます。
         ヒントテキストは、<b>PlaceHolder</b> プロパティによって決定されます。
    </p>
</asp:Content>
