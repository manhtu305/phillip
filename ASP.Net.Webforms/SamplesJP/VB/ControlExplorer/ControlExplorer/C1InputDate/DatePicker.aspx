﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputDate_DatePicker" Codebehind="DatePicker.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputDate ID="C1InputDate1" runat="server" >
    <PopupPosition>
        <Offset Top="4"></Offset>
    </PopupPosition>
    <Pickers>
        <DatePicker Visible="True" />
    </Pickers>
</wijmo:C1InputDate>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
        <strong>C1InputDate</strong> は、ドロップダウンカレンダーによる日付の選択をサポートしています。
    </p>
    <p>
        <b>ShowTrigger</b> プロパティを <b>true</b> に設定すると、ドロップダウンカレンダーを使用可能になります。
    </p>
    <p>
        <b>ButtonAlign</b> プロパティを設定すると、トリガボタンの配置を変更できます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

