Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer
	Public Partial Class [Default]
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				LoadData()
			End If

		End Sub

		Private Sub LoadData()
			Dim data As New WidgetData()
			RptFavoriteWidgets.DataSource = data.GetFavoriteWidgets()
			RptFavoriteWidgets.DataBind()

			'RptWidgets.DataSource = data.GetAllWidgets();
			'RptWidgets.DataBind();
		End Sub
	End Class
End Namespace
