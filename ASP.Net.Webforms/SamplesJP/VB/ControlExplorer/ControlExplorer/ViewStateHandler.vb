Imports System.Collections.Generic
Imports System.Text
Imports System.Web.UI

Namespace ControlExplorer
	Public Class PageStateAdapter
		Inherits System.Web.UI.Adapters.PageAdapter
		Public Overrides Function GetStatePersister() As PageStatePersister
			Return New SessionPageStatePersister(Me.Page)
		End Function
	End Class
End Namespace
