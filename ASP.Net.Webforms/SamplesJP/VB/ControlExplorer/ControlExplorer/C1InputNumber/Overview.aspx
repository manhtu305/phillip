﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Overview" Codebehind="Overview.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputNumeric ID="C1InputNumeric1" runat="server" ShowSpinner="true" Value="2.324" DecimalPlaces="3">
</wijmo:C1InputNumeric>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">

<p>
<strong>C1InputNumeric</strong> は、数値を編集するために特化した入力コントロールです。
このサンプルでは、既定の単純な <strong>C1InputNumeric </strong>コントロールを表示します。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

