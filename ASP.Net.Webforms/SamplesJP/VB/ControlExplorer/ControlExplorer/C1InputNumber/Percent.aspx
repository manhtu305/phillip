﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Percent" Codebehind="Percent.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputPercent ID="C1InputPercent1" runat="server" ShowSpinner="true" Value="25">
</wijmo:C1InputPercent>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">

<p>
<strong>C1InputCurrency</strong> は、パーセント値を編集するために特化した入力コントロールです。
</p>
<p>
パーセント記号は、Cultureプロパティによって決定されます。
</p>
<p>
上下キーを押すと、値を変更することができます。
<b>ShowSpinner</b> プロパティを True に設定すると、スピンボタンが有効になります。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

