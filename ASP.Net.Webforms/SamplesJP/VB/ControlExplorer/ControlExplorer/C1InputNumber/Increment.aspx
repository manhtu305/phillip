﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Increment" Codebehind="Increment.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputCurrency ID="C1InputCurrency1" runat="server" ShowSpinner="true" Value="50" MinValue="0" MaxValue="1000" Increment="10">
</wijmo:C1InputCurrency>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">

<p>
<strong>C1InputCurrency</strong> は、上下キーやスピンボタンのクリックで、指定した値だけ値を増減することができます。
</p>
<p>
インクリメント値は <b>Increment</b> プロパティで設定します。
</p>
<p>
この例では、上下キーの押下時や上下ボタンのクリック時に、10 だけ値が増減します。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

