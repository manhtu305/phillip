﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomTrigger.aspx.vb" Inherits="ControlExplorer.C1InputNumber.CustomTrigger" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

<style type="text/css"> 
	
	.dropdown-container
	{
		height: 127px;
		margin: 0;
		padding: 0;
		width: 25px;
	}
	
	.valueslider
	{
		left: 4px;
		height: 103px;
	}
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

数量：
<wijmo:C1InputNumeric ID="C1InputNumeric1" runat="server" Width="50px" Value="2" DecimalPlaces="0" MinValue="0"  MaxValue="5" ShowTrigger="true" OnClientTriggerMouseDown="triggerClicked" ButtonAlign="Left">
</wijmo:C1InputNumeric>

<div class="dropdown-container">
<div class="valueslider ui-state-default ui-corner-top"></div>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルでは、​​トリガーボタンのクリック時にカスタム UI を実装する方法を示します。 
</p>
<p>
<b>OnClientTriggerMouseDown</b> は、トリガーボタンがクリックされたときに呼び出されるクライアントコールバック関数名を指定します。
</p>
<p>
このサンプルでは、スライダーをドロップダウン表示して、​​スライダのつまみをドラッグして入力値を変更することができます。
</p>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">

<script type="text/javascript">


	$(function () {
		$(".dropdown-container").wijpopup({
			autoHide: true,
			showEffect: 'drop',
			hideEffect: 'fade'
		});
	});


	function triggerClicked(e) {
		var $input = $('#<%=C1InputNumeric1.ClientID%>');
		var val = $input.c1inputnumeric('option', 'value');

		var $volumeSlider = $('.valueslider');
		$volumeSlider.slider({
			min: 0,
			max: 5,
			value: val,
			step: 1,
			orientation: 'vertical',
			range: 'min',
			slide: function (e, ui) {
				$input.c1inputnumeric('option', 'value', ui.value);
			}
		});

		$(".dropdown-container").wijpopup('show', {
			of: $('.wijmo-wijinput'),
			at: 'right bottom',
			my: 'right top',
			offset: "4 2"
		});
	}

</script>

</asp:Content>
