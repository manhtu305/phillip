﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Currency" Codebehind="Currency.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputCurrency ID="C1InputCurrency1" runat="server" ShowSpinner="true" Value="50" MinValue="0" MaxValue="1000" >
</wijmo:C1InputCurrency>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">

<p>
<strong>C1InputCurrency</strong> は、通貨の値を編集するために特化した入力コントロールです。
</p>
<p>
通貨記号は、Culture プロパティによって決定されます。
</p>
<p>
上下キーを押すと、値を変更することができます。
<b>ShowSpinner</b> プロパティを True に設定すると、スピンボタンが有効になります。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

