﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_DropDown" Codebehind="DropDown.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputCurrency ID="C1InputCurrency1" runat="server" ShowTrigger="true">
<ComboItems>
<wijmo:C1ComboBoxItem Text="100.12" Value="100.12" /> 
<wijmo:C1ComboBoxItem Text="1200" Value="1200" /> 
<wijmo:C1ComboBoxItem Text="2000" Value="2000" /> 
<wijmo:C1ComboBoxItem Text="5200" Value="5200" /> 
</ComboItems> 
</wijmo:C1InputCurrency>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">

<p>
    <strong>C1InputCurrency </strong>は、ドロップダウンリストからの値の選択をサポートしています。
</p>
<p>
    この場合、<b>ShowTriggers</b> プロパティを True に設定して、<b>ComboItems</b> プロパティに項目リストを指定する必要があります。
</p>
<p>
    <b>ButtonAlign</b> プロパティを設定すると、トリガーボタンの配置を変更することができます。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

