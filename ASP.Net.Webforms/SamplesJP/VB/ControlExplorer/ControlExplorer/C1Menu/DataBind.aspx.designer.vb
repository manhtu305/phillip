'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Menu


	Public Partial Class DataBind

		''' <summary>
		''' Menu1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Menu1 As Global.C1.Web.Wijmo.Controls.C1Menu.C1Menu

		''' <summary>
		''' XmlDataSource1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected XmlDataSource1 As Global.System.Web.UI.WebControls.XmlDataSource

		''' <summary>
		''' SiteMapDataSource1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected SiteMapDataSource1 As Global.System.Web.UI.WebControls.SiteMapDataSource

		''' <summary>
		''' RblDataSource コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected RblDataSource As Global.System.Web.UI.WebControls.RadioButtonList
	End Class
End Namespace
