﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ClientMode.aspx.vb" Inherits="ControlExplorer.C1Menu.ClientMode" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1Menu runat="server" ID="Menu1" Orientation="Horizontal">
        <Items>
            <wijmo:C1MenuItem ID="C1MenuItem1" runat="server" Text="メニュー項目">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem ID="C1MenuItem2" runat="server" Separator="true">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem ID="C1MenuItem3" runat="server" Text="縦" Value="DynamicOrientationItem">
                <Items>
                    <wijmo:C1MenuItem ID="C1MenuItem4" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem5" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem6" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem7" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem8" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem ID="C1MenuItem9" runat="server" Text="メニュー項目">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem ID="C1MenuItem10" runat="server" Text="メニュー項目">
            </wijmo:C1MenuItem>
        </Items>
    </wijmo:C1Menu>

    <script type="text/javascript">
        $(document).ready(function () {
            $(":radio").click(function () {
                var menu = $("#<%= Menu1.ClientID %>");
                if (this.id == "rdHorizontal") {
                    menu.c1menu("option", "orientation", "horizontal");
                }
                else if (this.id == "rdVertical") {
                    menu.c1menu("option", "orientation", "vertical");
                }
            })
        })
</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p>このサンプルで、スクリプトを使用してメニューバーのクライアント オプションを変更する方法を例示します。</p>
<p>このサンプルにて、下記のオプションが使用されています。</p>
<ul>
<li>orientation</li>
</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<label>方向：</label>
<p><input type="radio" id="rdHorizontal" name="rdOrientation" /><label>水平</label>
<input type="radio" id="rdVertical" name="rdOrientation" /><label>垂直</label>
</p>
</asp:Content>
