﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Template.aspx.vb" Inherits="ControlExplorer.C1Menu.Template" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Menu runat="server" ID="Menu1">
		<Items>
			<wijmo:C1MenuItem ID="C1MenuItem3" runat="server" Text="ファイル">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem1" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">新規作成</a><span class="wijmo-wijmenu-icon-right">Ctrl+N</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem2" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">開く</a><span class="wijmo-wijmenu-icon-right">Ctrl+O</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem5" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">保存</a><span class="wijmo-wijmenu-icon-right">Ctrl+S</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem4" runat="server" Text="編集">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem6" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">元に戻す</a><span class="wijmo-wijmenu-icon-right">Ctrl+Z</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem10" runat="server" Separator="true">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem8" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">切り取り</a><span class="wijmo-wijmenu-icon-right">Ctrl+X</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem7" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">コピー</a><span class="wijmo-wijmenu-icon-right">Ctrl+C</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem9" runat="server">
						<Template>
							<div>
								<a href="#" class="wijmo-wijmenu-text">貼り付け</a><span class="wijmo-wijmenu-icon-right">Ctrl+V</span>
							</div>
						</Template>
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem11" runat="server" Text="表示">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem12" runat="server">
						<Template>
							<span>
								<input id="checkbox1" type="checkbox" /><label for="checkbox1">ステータスバー</label>
							</span>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem13" runat="server">
						<Template>
							<span>
								<input id="checkbox2" type="checkbox" /><label for="checkbox2">サイドバー</label>
							</span>
						</Template>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem14" runat="server" Separator="true">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem15" runat="server" Text="文字エンコード">
						<Items>
							<wijmo:C1MenuItem ID="C1MenuItem16" runat="server">
								<Template>
									<span>
										<input type="radio" name="radiobutton1" id="radio1" /><label for="radio1">Western (ISO-8859-1)</label>
									</span>
								</Template>
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem17" runat="server">
								<Template>
									<span>
										<input type="radio" name="radiobutton1" id="radio2" /><label for="radio2">Unicode (UTF-8)</label>
									</span>
								</Template>
							</wijmo:C1MenuItem>
						</Items>
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
		</Items>
	</wijmo:C1Menu>
	<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			$(":input").wijradio().wijcheckbox();
			$(".wijmo-wijmenu-text").parent().bind("click", function () {
				$("#<%= Menu1.ClientID %>").c1menu("hideAllMenus");
			});
			$(".wijmo-wijmenu-link").hover(function () {
				$(this).addClass("ui-state-hover");
			}, function () {
				$(this).removeClass("ui-state-hover");
			})
		});

	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>メニュー項目テンプレートを使用して、メニューにリッチコンテンツを提供することができます。</p>
    <p>このサンプルでは、メニュー、ショートカットメニュー項目、ラジオボタン、チェックボックスのあるテンプレートを提供します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
