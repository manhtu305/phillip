﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="ClientSideFunctions.aspx.vb" Inherits="ControlExplorer.C1Menu.ClientSideFunctions" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1Menu" Assembly="C1.Web.Wijmo.Controls.45"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Menu runat="server" ID="Menu1" >
        <Items>
            <wijmo:c1menuitem id="C1MenuItem1" runat="server" text="メニュー項目">
            </wijmo:c1menuitem>
            <wijmo:c1menuitem id="C1MenuItem2" runat="server" separator="true">
            </wijmo:c1menuitem>
            <wijmo:c1menuitem id="C1MenuItem3" runat="server" text="縦" value="DynamicOrientationItem">
                <Items>
                    <wijmo:C1MenuItem ID="C1MenuItem4" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem5" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem6" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem7" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem ID="C1MenuItem8" runat="server" Text="メニュー項目">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:c1menuitem>
            <wijmo:c1menuitem id="C1MenuItem9" runat="server" text="メニュー項目">
            </wijmo:c1menuitem>
            <wijmo:c1menuitem id="C1MenuItem10" runat="server" text="メニュー項目">
            </wijmo:c1menuitem>
        </Items>
    </wijmo:C1Menu>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p>このサンプルでは、クライアント側で項目を追加／削除する方法を紹介します。</p><br/>
<p>このサンプルでは、下記のメソッドが使用されています。</p>
<ul>
	<li>add</li>
	<li>remove</li>
</ul>
<p>add メソッドには、下記のパラメータがあります。</p>
<ul>
	<li><strong>item</strong>
	<p>item は、メニューやメニュー項目に追加されるマークアップを指定します。メニュー項目の場合は「&lt;a&gt;item&lt;/a&gt;」、ヘッダ項目の場合は「&lt;h3&gt;head&lt;/h3&gt;」、セパレータ項目の場合は空です。</p></li>
	<li><strong>position</li></strong>
	<p>追加される位置を指定します。</p>
</ul>
<p>remove メソッドには、下記のパラメータがあります。</p>
<ul>
	<li><strong>index</strong>
	<p>削除されるメニュー項目のインデックスを指定します。</p></li>
</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <fieldset>
        <legend>削除</legend>
        <label for="tbIndex">
            インデックス：</label>
        <input type="text" id="tbIndex" />
        <input type="button" value="削除" onclick="removeItem()" />
    </fieldset>
    <fieldset>
        <legend>追加</legend>
        <label for="tbItem">
            項目：</label>
        <input type="text" id="tbItem" />
        <label for="tbAddIndex">
            インデックス：</label>
        <input type="text" id="tbAddIndex" />
        <input type="button" value="追加" onclick="add()" />
    </fieldset>
    <script type="text/javascript">
        function removeItem() {
            var index;
            if ($("#tbIndex").val() !== "" && !isNaN($("#tbIndex").val())) {
                index = parseInt($("#tbIndex").val());
            }

            if (index !== undefined) {
                $("#<%= Menu1.ClientID %>").c1menu("remove", index);
            }
        }

        function add() {
            var index, item;
            item = $("#tbItem").val();
            if ($("#tbAddIndex").val() != "" && !isNaN($("#tbAddIndex").val())) {
                index = parseInt($("#tbAddIndex").val());
            }
            if (index !== undefined) {
                $("#<%= Menu1.ClientID %>").c1menu("add", item, index);
            }
        }
    </script>
    <asp:Button ID="Button1" runat="server" Text="ポストバック" 
        onclick="Button1_Click1" />

</asp:Content>

