﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBind.aspx.vb" Inherits="ControlExplorer.C1Menu.DataBind" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1Menu runat="server" ID="Menu1" DataSourceID="XmlDataSource1"></wijmo:C1Menu>
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" 
        DataFile="~/App_Data/menu_structure.xml" XPath="/root/menuitem">
    </asp:XmlDataSource>
    <asp:SiteMapDataSource runat="server" ID="SiteMapDataSource1" ShowStartingNode="false"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><strong>C1Menu</strong> は、XML、SiteMap 等のデータソースとの連結をサポートし、Access データソースからデータを読み込んで動的に <strong>C1Menu</strong> 階層を作成することも可能です。</p>
    <p>このサンプルでは、下記のプロパティが使用されています。</p>
    <ul>
	    <li>DataSourceID</li>
    </ul>
    <p>このサンプルでは、<strong>XmlDataSource</strong> ラジオボタンを選択すると、<strong>C1Menu</strong> が XML データソースと連結され、それ以外の場合は SiteMapDataSource と連結されます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<label>データソースを選択 </label>
<asp:RadioButtonList runat="server" ID="RblDataSource" RepeatDirection="Horizontal"  AutoPostBack="true"
        onselectedindexchanged="RblDataSource_SelectedIndexChanged">
<asp:ListItem Text="XmlDataSource&nbsp;&nbsp;" Value="XmlDataSource"></asp:ListItem>
<asp:ListItem Text="SiteMapDataSource" Value="SiteMapDataSource"></asp:ListItem>
</asp:RadioButtonList>
</asp:Content>
