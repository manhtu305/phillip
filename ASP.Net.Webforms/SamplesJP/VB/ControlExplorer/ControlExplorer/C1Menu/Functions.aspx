﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Functions.aspx.vb" Inherits="ControlExplorer.C1Menu.Functions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        div#ctl00_ctl00_MainContent_WidgetTabs_ctl00
        {
            overflow: visible;
            overflow-y: visible;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Menu runat="server" ID="Menu1" Mode="Sliding">
		<Items>
			<wijmo:C1MenuItem Text="MenuItem" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem1" Text="新着" runat="server">
				<Items>
					<wijmo:C1MenuItem runat="server" Header="true" Text="ヘッダ２">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem runat="server" Separator="true">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem runat="server" Text="エンタメ">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem2" runat="server" Text="政治">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem3" runat="server" Text="国際">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem4" runat="server" Text="文化">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem5" runat="server" Text="社会">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem6" runat="server" Text="天気">
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem7" runat="server" Text="エンタメ">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem8" runat="server" Text="エンタメトップ">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem9" runat="server" Text="舞台">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem10" runat="server" Text="映画">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem11" runat="server" Text="音楽">
						<Items>
							<wijmo:C1MenuItem ID="C1MenuItem12" runat="server" Text="オルタナティブ">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem13" runat="server" Text="カントリー">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem14" runat="server" Text="ダンス">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem15" runat="server" Text="エレクトロニカ">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem16" runat="server" Text="メタル">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem17" runat="server" Text="ポップ">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem18" runat="server" Text="ロック">
								<Items>
									<wijmo:C1MenuItem ID="C1MenuItem19" runat="server" Text="バンド">
										<Items>
											<wijmo:C1MenuItem ID="C1MenuItem22" runat="server" Text="ドッケン">
											</wijmo:C1MenuItem>
										</Items>
									</wijmo:C1MenuItem>
									<wijmo:C1MenuItem ID="C1MenuItem20" runat="server" Text="ファン クラブ">
									</wijmo:C1MenuItem>
									<wijmo:C1MenuItem ID="C1MenuItem21" runat="server" Text="歌曲">
									</wijmo:C1MenuItem>
								</Items>
							</wijmo:C1MenuItem>
						</Items>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem23" runat="server" Text="スライドショー">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem24" runat="server" Text="レッドカーペット">
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem25" Text="金融" runat="server">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem26" Text="個人" runat="server">
						<Items>
							<wijmo:C1MenuItem ID="C1MenuItem28" Text="ローン" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem29" Text="貯蓄" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem30" Text="貸付金" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem31" Text="借金" runat="server">
							</wijmo:C1MenuItem>
						</Items>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem27" Text="企業" runat="server">
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem32" Text="スポーツ" runat="server">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem33" Text="野球" runat="server">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem34" Text="サッカー" runat="server">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem35" Text="ゴルフ" runat="server">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem36" Text="一般" runat="server">
						<Items>
							<wijmo:C1MenuItem ID="C1MenuItem37" Text="テニス" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem38" Text="水泳" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem39" Text="ラグビー" runat="server">
							</wijmo:C1MenuItem>
						</Items>
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem40" Text="生活" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem41" Text="ニュース" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem42" Text="政治" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem43" Text="スポーツ" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem44" Text="小説" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem45" Text="マガジン" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem46" Text="ブック" runat="server">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem47" Text="教育" runat="server">
			</wijmo:C1MenuItem>
		</Items>
	</wijmo:C1Menu>

		<script type="text/javascript">
			var count = 0;
			$(document).ready(function () {
				$("#previous").click(function () {
					$("#<%= Menu1.ClientID %>").focus().c1menu("previous");
					count++;
				});
				$("#next").click(function () {
					$("#<%= Menu1.ClientID %>").focus().c1menu("next");
					count++;
				});
				$("#previousPage").click(function () {
					if (count === 0) {
						$("#<%= Menu1.ClientID %>").find(".wijmo-wijmenu-link:first").click();
					}
					$("#<%= Menu1.ClientID %>").c1menu("previousPage");
					count++;
				});
				$("#nextPage").click(function () {
					if (count === 0) {
						$("#<%= Menu1.ClientID %>").find(".wijmo-wijmenu-link:first").click();
					}
					$("#<%= Menu1.ClientID %>").c1menu("nextPage");
					count++;
				});
			});


	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p><strong>C1Menu</strong> コントロールは、高度なクライアント側 API を提供します。
このサンプルでは、jQuery UI と同様の手法を使用してクライアント側メソッドを呼び出す方法を紹介します。
このサンプルでは、下記のクライアント側関数が使用されています。</p>
	<ul>
		<li>previous</li>
		<li>next</li>
		<li>previousPage</li>
		<li>nextPage</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<input type="button" id="previous" value="前へ" />
	<input type="button" id="next" value="次へ" />
	<input type="button" id="previousPage" value="前のページ" />
	<input type="button" id="nextPage" value="次のページ" />
	
</asp:Content>
