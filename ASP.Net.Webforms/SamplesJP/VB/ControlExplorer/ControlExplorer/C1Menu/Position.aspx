﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Position.aspx.vb" Inherits="ControlExplorer.C1Menu.Position" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<input type="button" value="ここをクリック" id="btn1" />
	<wijmo:C1Menu runat="server" ID="Menu1" Orientation="Vertical" Trigger="#btn1" TriggerEvent="Click">
		<Items>
			<wijmo:C1MenuItem ID="C1MenuItem1" runat="server" Text="新着">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem2" runat="server" Text="エンタメ">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem3" runat="server" Text="金融">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem4" runat="server" Text="スポーツ">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem5" runat="server" Text="生活">
				<Items>
					<wijmo:C1MenuItem ID="C1MenuItem8" runat="server" Text="submenu"></wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem6" runat="server" Text="ニュース">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem7" runat="server" Text="政治">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem ID="C1MenuItem9" runat="server" Text="文化">
			</wijmo:C1MenuItem>
		</Items>
	</wijmo:C1Menu>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#my1,#my2,#at1,#at2").change(function () {
				$("#<%= Menu1.ClientID %>").c1menu("option", "position", { my: $("#my1").get(0).value + " " + $("#my2").get(0).value, at: $("#at1").get(0).value + " " + $("#at2").get(0).value });
			});
		});

	</script>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、<strong>Position</strong> プロパティを使用して、トリガー（メニューを開くためのボタンなど）に対してどこに表示するかを指定します。</p>
	<p>このサンプルでは、下記のプロパティが使用されています。</p>
	<ul>
		<li><strong>Position</strong> - 他のドキュメント要素に対するコントロールの位置。このプロパティは、jQuery UI の position ウィジェットに基づいています。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="option-row">
		<label>
			方向：</label>
		<select id="my1">
                <option value="left">左</option>
                <option value="center">中央</option>
                <option value="right">右</option>
		</select>
		<select id="my2">
                <option value="top">上</option>
                <option value="center">中央</option>
                <option value="bottom">下</option>
		</select>
	</div>
	<div class="option-row">
		<label>
			位置：</label>
		<select id="at1">
                <option value="left">左</option>
                <option value="center">中央</option>
                <option value="right">右</option>
		</select>
		<select id="at2">
                <option value="top">上</option>
                <option value="center">中央</option>
                <option value="bottom" selected="selected">下</option>
		</select>
	</div>
</asp:Content>
