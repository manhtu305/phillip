﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Icon.aspx.vb" Inherits="ControlExplorer.C1Menu.Icon" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Menu runat="server" ID="Menu1">
		<Items>
			<wijmo:C1MenuItem runat="server" Text="メニュー1" IconClass="ui-icon ui-icon-arrowthick-1-w"
				ID="MenuItem1">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー2" IconClass="ui-icon ui-icon-arrowrefresh-1-s"
				ID="C1MenuItem1">
				<Items>
					<wijmo:C1MenuItem runat="server" Text="サブメニュー1">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem8" runat="server" Text="サブメニュー2">
						<Items>
							<wijmo:C1MenuItem Text="サブメニュー21" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem15" Text="サブメニュー22" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem16" Text="サブメニュー23" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem17" Text="サブメニュー24" runat="server">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem18" Text="サブメニュー25" runat="server">
							</wijmo:C1MenuItem>
						</Items>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem9" runat="server" Text="サブメニュー3">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem10" runat="server" Text="サブメニュー4">
						<Items>
							<wijmo:C1MenuItem runat="server" Text="サブメニュー41">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem19" runat="server" Text="サブメニュー42">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem20" runat="server" Text="サブメニュー43">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem21" runat="server" Text="サブメニュー44">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem22" runat="server" Text="サブメニュー45">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem23" runat="server" Text="サブメニュー46">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem24" runat="server" Text="サブメニュー47">
							</wijmo:C1MenuItem>
							<wijmo:C1MenuItem ID="C1MenuItem25" runat="server" Text="サブメニュー48">
							</wijmo:C1MenuItem>
						</Items>
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem11" runat="server" Text="サブメニュー5">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem12" runat="server" Text="サブメニュー6">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem13" runat="server" Text="サブメニュー7">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem14" runat="server" Text="サブメニュー8">
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー3" IconClass="ui-icon ui-icon-comment"
				ID="C1MenuItem2">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー4" IconClass="ui-icon ui-icon-person"
				ID="C1MenuItem3">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー5" IconClass="ui-icon ui-icon-trash" ID="C1MenuItem4">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー6" IconClass="ui-icon ui-icon-bookmark"
				ID="C1MenuItem5">
				<Items>
					<wijmo:C1MenuItem runat="server" Text="サブメニュー61">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem26" runat="server" Text="サブメニュー62">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem27" runat="server" Text="サブメニュー63">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem28" runat="server" Text="サブメニュー64">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem29" runat="server" Text="サブメニュー65">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem30" runat="server" Text="サブメニュー66">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem31" runat="server" Text="サブメニュー67">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem32" runat="server" Text="サブメニュー68">
					</wijmo:C1MenuItem>
					<wijmo:C1MenuItem ID="C1MenuItem33" runat="server" Text="サブメニュー69">
					</wijmo:C1MenuItem>
				</Items>
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー7" IconClass="ui-icon ui-icon-clock" ID="C1MenuItem6">
			</wijmo:C1MenuItem>
			<wijmo:C1MenuItem runat="server" Text="メニュー8" IconClass="ui-icon ui-icon-minusthick"
				ID="C1MenuItem7">
			</wijmo:C1MenuItem>
		</Items>
	</wijmo:C1Menu>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、<strong>C1Menu </strong> のメニュー項目にアイコンを表示する方法を紹介します。</p>
	<p>
		メニュー項目にアイコンを表示するには、メニュー項目オブジェクトの下記のプロパティを使用します。</p>
	<ul>
		<li><strong>IconClass</strong> - 画像を表示する CSS クラスを指定します。</li>
	</ul>
	<p>
		<strong>ImagePosition </strong> プロパティを使用して画像の位置を指定することも可能です。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
