﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="OverView.aspx.vb"
    Inherits="ControlExplorer.C1Menu.OverView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        div#ctl00_ctl00_MainContent_WidgetTabs_ctl00
        {
            overflow: visible;
            overflow-y: visible;
        }
    </style>    
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <h3>既定のメニュー</h3>
    <wijmo:C1Menu runat="server" ID="Menu1" DataSourceID="XmlDataSource1">
        <HideAnimation>
            <Animated Effect="fade"></Animated>
        </HideAnimation>
        <DataBindings>
            <wijmo:C1MenuItemBinding DataMember="Menuitem" HeaderField="header" NavigateUrlField="navigateUrl"
                SeparatorField="separator" TextField="text" />
        </DataBindings>
    </wijmo:C1Menu>

    <h3>iPhoneスタイルのメニュー</h3>
    <wijmo:C1Menu runat="server" ID="Menu2" Mode="Sliding" DataSourceID="XmlDataSource1">
    </wijmo:C1Menu>
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/menu_structure.xml"
        XPath="/root/menuitem"></asp:XmlDataSource>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
	<p>
		<strong>C1Menu</strong> を使用すると、アニメーション効果、画像、チェックボックス、インタラクティブなスクロールなどを備えたメニューを作成することができます。
        また、アプリケーション内にコンテキストヘルプを表示するためにポップアップメニューを作成することも可能です。</p>
</asp:Content>
