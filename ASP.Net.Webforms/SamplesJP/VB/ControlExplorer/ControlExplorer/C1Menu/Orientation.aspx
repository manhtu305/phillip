﻿<%@ Page Language="vb" AutoEventWireup="true" CodeBehind="Orientation.aspx.vb" MasterPageFile="~/Wijmo.Master"
    Inherits="ControlExplorer.C1Menu.Orientation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Menu"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        div#ctl00_ctl00_MainContent_WidgetTabs_ctl00
        {
            overflow: visible;
            overflow-y: visible;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Menu runat="server" ID="Menu1" Orientation="Horizontal">
        <Items>
            <wijmo:C1MenuItem runat="server" Text="新着">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="エンタメ">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="政治">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="国際">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="文化">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="社会">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="天気">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="エンタメ">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="エンタメトップ">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="舞台">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="映画">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="音楽">
                        <Items>
                            <wijmo:C1MenuItem runat="server" Text="オルタナティブ">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="カントリー">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ダンス">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="エレクトロニカ">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="メタル">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ポップ">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ロック">
                                <Items>
                                    <wijmo:C1MenuItem runat="server" Text="バンド">
                                        <Items>
                                            <wijmo:C1MenuItem runat="server" Text="ドッケン">
                                            </wijmo:C1MenuItem>
                                        </Items>
                                    </wijmo:C1MenuItem>
                                    <wijmo:C1MenuItem runat="server" Text="ファン クラブ">
                                    </wijmo:C1MenuItem>
                                    <wijmo:C1MenuItem runat="server" Text="歌曲">
                                    </wijmo:C1MenuItem>
                                </Items>
                            </wijmo:C1MenuItem>
                        </Items>
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="スライドショー">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="レッドカーペット">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="金融">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="個人">
                        <Items>
                            <wijmo:C1MenuItem runat="server" Text="ローン">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="貯蓄">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="貸付金">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="借金">
                            </wijmo:C1MenuItem>
                        </Items>
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="企業">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="スポーツ">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="野球">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="サッカー">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="ゴルフ">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="一般">
                        <Items>
                            <wijmo:C1MenuItem runat="server" Text="テニス">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="水泳">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ラグビー">
                            </wijmo:C1MenuItem>
                        </Items>
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="生活">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="ニュース">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="政治">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="スポーツ">
            </wijmo:C1MenuItem>
        </Items>
    </wijmo:C1Menu>
    <br />
    <wijmo:C1Menu runat="server" ID="C1Menu1" Orientation="Vertical">
        <Items>
            <wijmo:C1MenuItem runat="server" Text="新着">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="エンタメ">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="政治">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="国際">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="文化">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="社会">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="天気">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="エンタメ">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="エンタメトップ">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="舞台">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="映画">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="音楽">
                        <Items>
                            <wijmo:C1MenuItem runat="server" Text="オルタナティブ">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="カントリー">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ダンス">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="エレクトロニカ">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="メタル">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ポップ">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ロック">
                                <Items>
                                    <wijmo:C1MenuItem runat="server" Text="バンド">
                                        <Items>
                                            <wijmo:C1MenuItem runat="server" Text="ドッケン">
                                            </wijmo:C1MenuItem>
                                        </Items>
                                    </wijmo:C1MenuItem>
                                    <wijmo:C1MenuItem runat="server" Text="ファン クラブ">
                                    </wijmo:C1MenuItem>
                                    <wijmo:C1MenuItem runat="server" Text="歌曲">
                                    </wijmo:C1MenuItem>
                                </Items>
                            </wijmo:C1MenuItem>
                        </Items>
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="スライドショー">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="レッドカーペット">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="金融">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="個人">
                        <Items>
                            <wijmo:C1MenuItem runat="server" Text="ローン">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="貯蓄">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="貸付金">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="借金">
                            </wijmo:C1MenuItem>
                        </Items>
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="企業">
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="スポーツ">
                <Items>
                    <wijmo:C1MenuItem runat="server" Text="野球">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="サッカー">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="ゴルフ">
                    </wijmo:C1MenuItem>
                    <wijmo:C1MenuItem runat="server" Text="一般">
                        <Items>
                            <wijmo:C1MenuItem runat="server" Text="テニス">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="水泳">
                            </wijmo:C1MenuItem>
                            <wijmo:C1MenuItem runat="server" Text="ラグビー">
                            </wijmo:C1MenuItem>
                        </Items>
                    </wijmo:C1MenuItem>
                </Items>
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="生活">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="ニュース">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="政治">
            </wijmo:C1MenuItem>
            <wijmo:C1MenuItem runat="server" Text="スポーツ">
            </wijmo:C1MenuItem>
        </Items>
    </wijmo:C1Menu>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Description" ID="Content3">
<p>このサンプルでは、<strong>C1Menu</strong> の方向の設定方法を紹介します。</p>
<p>このサンプルでは、以下のプロパティが利用されます。</p>
<ul>
<li>Orientation</li>
</ul>
<p><strong>メモ</strong>: <strong>Mode</strong> プロパティが <strong>Sliding</strong> に設定されている場合、<strong> Orientation</strong> プロパティが反映されません。</p>
</asp:Content>

