'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1Menu

    Partial Public Class ClientSideFunctions

        '''<summary>
        '''Menu1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Menu1 As Global.C1.Web.Wijmo.Controls.C1Menu.C1Menu

        '''<summary>
        '''C1MenuItem1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem1 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem2 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem3 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem4 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem4 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem5 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem5 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem6 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem6 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem7 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem7 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem8 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem8 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem9 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem9 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''C1MenuItem10 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1MenuItem10 As Global.C1.Web.Wijmo.Controls.C1Menu.C1MenuItem

        '''<summary>
        '''Button1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
