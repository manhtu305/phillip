﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ProgressFunction.aspx.vb" Inherits="ControlExplorer.C1ProgressBar.ProgressFunction" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <wijmo:C1ProgressBar runat="server" ID="Progressbar1"
		onruntask="Progressbar1_RunTask" StartTaskButton="btnStart" 
		StopTaskButton="btnStop">
	</wijmo:C1ProgressBar>
	<asp:Button runat="server" ID="btnStart" Text="開始" />
	<asp:Button runat="server" ID="btnStop" Text="停止" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><strong>C1ProgressBar </strong>は、サーバー上で進捗の値を更新する方法をサポートしています。</p>
    <p>進捗の値を更新するには、次のプロパティまたはイベントを使用します。</p>
	<ul>
		<li><strong>StartTaskButton</strong> - サーバー側の処理を開始するボタン ID を指定します。</li>
		<li><strong>StopTaskButton</strong> - サーバー側の処理を停止するボタン ID を指定します。</li>
		<li><strong>OnRunTask</strong> - このイベントは、StartTaskButton に関連付けられたボタンがクリックされたときに発生します。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
