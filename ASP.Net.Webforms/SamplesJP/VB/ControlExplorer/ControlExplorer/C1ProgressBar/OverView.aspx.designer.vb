'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1ProgressBar


	Public Partial Class OverView

		''' <summary>
		''' HProgressbar コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected HProgressbar As Global.C1.Web.Wijmo.Controls.C1ProgressBar.C1ProgressBar

		''' <summary>
		''' VProgressbar コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected VProgressbar As Global.C1.Web.Wijmo.Controls.C1ProgressBar.C1ProgressBar
	End Class
End Namespace
