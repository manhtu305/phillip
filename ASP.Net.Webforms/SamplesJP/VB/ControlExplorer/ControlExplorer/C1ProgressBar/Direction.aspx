﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Direction.aspx.vb" Inherits="ControlExplorer.C1ProgressBar.Direction" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ProgressBar runat="server" ID="Progressbar1" Value="50">
	</wijmo:C1ProgressBar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、クライアント側のスクリプトを使用して、<strong>C1ProgressBar</strong> コントロールの​​ラベルの配置、値、方向を動的に変更する方法を紹介します。</p>
	<p>このサンプルでは、次のプロパティが使用されています。</p>
	<ul>
		<li><strong>value </strong>- 進捗の値を変更します。</li>
		<li><strong>fillDirection </strong>- 進捗の方向を変更します。</li>
		<li><strong>labelAlign</strong> - ラベルの配置を変更します。</li>
	</ul>
	<p><strong>Value </strong>プロパティは、<strong>MinValue</strong> と <strong>MaxValue</strong> の間の値に設定できます。
    <strong>labelAlign</strong> プロパティを Running に設定すると、ラベルは進行状況インジケータと並んで実行されます。
    <strong>fillDirection </strong>を East／West に設定すると、プログレスバーは水平方向に表示され、その他の場合は垂直方向に表示されます。
    水平方向のプログレスバーでは、East に設定するとインジケータは西から東へ増加し、West に設定すると東から西へ増加します。
    垂直方向のプログレスバーでは、North に設定するとインジケータは南から北へ増加し、South に設定すると北から南へ増加します。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<label>
		値：</label>
	<input type="text" id="value" value="50" />
	<label>
		　ラベルの配置：</label>
	<select id="labelAlign">
		<option value="east">東</option>
		<option value="west">西</option>
		<option value="center" selected="selected">中央</option>
		<option value="north">北</option>
		<option value="south">南</option>
		<option value="running">インジケータ</option>
	</select>
	<label>
		　進行の向き：</label>
	<select id="fillDirection">
		<option value="east">東</option>
		<option value="west">西</option>
		<option value="south">南</option>
		<option value="north">北</option>
	</select>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#value").blur(function () {
				if ($(this).val() == "") return;
				var value = parseInt($(this).val());
				$("#<%=Progressbar1.ClientID %>").c1progressbar("option", "value", value);
			});
			$("#labelAlign").change(function () {
				$("#<%=Progressbar1.ClientID %>").c1progressbar("option", "labelAlign", $(this).val());
			});
			$("#fillDirection").change(function () {
				$("#<%=Progressbar1.ClientID %>").c1progressbar("option", "fillDirection", $(this).val());
			});
		});
				
	</script>
</asp:Content>
