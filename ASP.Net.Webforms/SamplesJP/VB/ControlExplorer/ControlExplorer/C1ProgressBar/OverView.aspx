﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="OverView.aspx.vb" Inherits="ControlExplorer.C1ProgressBar.OverView" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div>水平方向</div>
<div>
<wijmo:C1ProgressBar runat="server" ID="HProgressbar" Value="50" 
		onruntask="HProgressbar_RunTask"></wijmo:C1ProgressBar>
</div>
<br /><br />
<div style="clear:both">垂直方向</div>
<wijmo:C1ProgressBar runat="server" ID="VProgressbar" FillDirection="South" Value="50"></wijmo:C1ProgressBar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    <strong>C1ProgressBar</strong> を使用すると、固定またはリアルタイムな進捗状況のインジケータを迅速かつ簡単に表示できます。
    </p>
    <p>
    既定ではプログレスバーは水平方向で表示されますが、<strong>FillDirection</strong> プロパティを設定すると、方向を北または南に変更することができます。
    このサンプルでは、<strong>FillDirection</strong> プロパティは北に設定されています。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
