﻿Imports C1.Web.Wijmo.Controls.C1FileExplorer

Namespace ControlExplorer.C1FileExplorer
    Public Class ServerSideEvents
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            AddHandler Me.C1FileExplorer1.ItemCommand, AddressOf C1FileExplorer1_ItemCommand
        End Sub

        Private Sub C1FileExplorer1_ItemCommand(sender As Object, e As C1FileExplorerEventArgs)
            If e.Command = FileCommand.Delete Then
                e.Cancel = True
            End If
        End Sub
    End Class
End Namespace
