﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1FileExplorer

    Partial Public Class KeyboardSupports

        '''<summary>
        '''C1FileExplorer1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1FileExplorer1 As Global.C1.Web.Wijmo.Controls.C1FileExplorer.C1FileExplorer

        '''<summary>
        '''inputFocusFileExplorer コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputFocusFileExplorer As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputFocusToolbar コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputFocusToolbar As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputFocusAddressBar コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputFocusAddressBar As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputFocusTreeView コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputFocusTreeView As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputFocusGrid コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputFocusGrid As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputFocusPaging コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputFocusPaging As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputOpenContextMenu コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputOpenContextMenu As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputBack コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputBack As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputForward コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputForward As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputOpen コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputOpen As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputRefresh コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputRefresh As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputNewFolder コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputNewFolder As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputDelete コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputDelete As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''btnApply コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents btnApply As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
