﻿Namespace ControlExplorer.C1FileExplorer
    Public Class UploadFile
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            If IsPostBack Then
                c1Upload1.TargetFolder = HiddenField1.Value
                UpdatePanel1.Update()
            End If
        End Sub
    End Class
End Namespace
