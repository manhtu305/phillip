﻿Namespace ControlExplorer.C1FileExplorer
    Public Class Functions
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            If Not IsPostBack Then
                Me.C1FileExplorer1.SearchPatterns = New String() {}
            End If
        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Me.C1FileExplorer1.EnableCopy = ckxEnableCopy.Checked
            Me.C1FileExplorer1.EnableCreateNewFolder = ckxEnableCreateFolder.Checked
            Me.C1FileExplorer1.EnableOpenFile = ckxEnableOpenFile.Checked
            Me.C1FileExplorer1.AllowFileExtensionRename = ckxAllowFileExtensionRename.Checked

            Me.C1FileExplorer1.TreePanelWidth = CInt(inputTreeWidth.Value)
        End Sub
    End Class
End Namespace
