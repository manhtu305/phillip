﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="VisibleControls.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.VisibleControls" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1FileExplorer" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontent li{
            white-space: nowrap;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <asp:CheckBox ID="ckxShowToolbar" Text="ツールバーを表示" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowAddressBox" Text="アドレスボックスを表示" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowFilterTextBox" Text="フィルタボックスを表示" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowTreeView" Text="ツリービューを表示" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowGrid" Text="グリッドを表示" runat="server"  Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowListView" Text="リストビューを表示" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowContextMenu" Text="コンテキストメニューを表示" runat="server" Checked="true" />
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FileExplorer</strong>はコントロールのレイアウトをカスタマイズする便利な方法を提供します。
    </p>
    <p>
        このデモでは、指定した子コントロールを表示／非表示にします。
    </p>
</asp:Content>
