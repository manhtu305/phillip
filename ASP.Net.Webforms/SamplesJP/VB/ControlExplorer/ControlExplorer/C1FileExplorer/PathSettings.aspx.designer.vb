﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1FileExplorer

    Partial Public Class PathSettings

        '''<summary>
        '''C1FileExplorer1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1FileExplorer1 As Global.C1.Web.Wijmo.Controls.C1FileExplorer.C1FileExplorer

        '''<summary>
        '''inputViewPaths コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputViewPaths As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputInitPath コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputInitPath As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputDeletePaths コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputDeletePaths As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''inputSearchPatterns コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents inputSearchPatterns As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''btnApply コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents btnApply As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
