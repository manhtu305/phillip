﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="PathSettings.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.PathSettings" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontainer .settingcontent .fullwidth label {
            width: 120px;
        }
        
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" ViewPaths="~/C1FileExplorer" InitPath="~/C1FileExplorer" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label >表示パス:</label>
                    <wijmo:C1InputText ID="inputViewPaths" runat="server" Text="~/C1FileExplorer/Example" Width="350px"></wijmo:C1InputText>
                </li>
                <li class="fullwidth">
                    <label>初期パス:</label>
                    <wijmo:C1InputText ID="inputInitPath" runat="server" Text="~/C1FileExplorer/Example" Width="350px"></wijmo:C1InputText>
                </li>
                <li class="fullwidth">
                    <label>削除パス:</label>
                    <wijmo:C1InputText ID="inputDeletePaths" runat="server" Text="~/C1FileExplorer/Example" Width="350px"></wijmo:C1InputText>
                </li>
                <li class="fullwidth">
                    <label>検索パターン:</label>
                    <wijmo:C1InputText ID="inputSearchPatterns" runat="server" Text="*.jpg,*.png,*.jpeg,*.gif" Width="350px"></wijmo:C1InputText>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">

    <p>
        どのフォルダを表示するかを設定します。以下のプロパティで表示するファイルの種類を設定します。
    </p>
    <ul>
        <li>
            <strong>ViewPaths: </strong>ファイルエクスプローラで表示されるフォルダを設定します。
        </li>
        <li>
            <strong>InitPath: </strong>右のグリッドで一覧表示されるサブフォルダ／ファイルの初期パスを設定します。設定されない場合は、<strong>ViewPaths</strong>のパスを使用します。
        </li>
        <li>
            <strong>DeletePaths: </strong>削除できるパスを設定します。パスが<strong>ViewPaths</strong>に含まれない場合は削除することができません。
        </li>
        <li>
            <strong>SearchPatterns: </strong>表示するファイル種別。
        </li>
    </ul>

</asp:Content>
