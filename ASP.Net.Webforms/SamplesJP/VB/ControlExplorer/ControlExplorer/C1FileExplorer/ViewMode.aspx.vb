﻿Imports C1.Web.Wijmo.Controls.C1FileExplorer

Namespace ControlExplorer.C1FileExplorer
    Public Class ViewMode
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Me.C1FileExplorer1.Mode = DirectCast([Enum].Parse(GetType(ExplorerMode), dplMode.SelectedValue), ExplorerMode)
            Me.C1FileExplorer1.ViewMode = DirectCast([Enum].Parse(GetType(C1.Web.Wijmo.Controls.C1FileExplorer.ViewMode), dplViewMode.SelectedValue), C1.Web.Wijmo.Controls.C1FileExplorer.ViewMode)
        End Sub
    End Class
End Namespace
