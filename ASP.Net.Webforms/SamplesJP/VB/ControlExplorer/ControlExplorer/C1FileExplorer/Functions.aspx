﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Functions.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.Functions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>

        .settingcontent li{
            white-space: nowrap;
        }

    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" >
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <asp:CheckBox ID="ckxEnableCopy" Text="コピーを許可" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableOpenFile" Text="ファイルを開くことを許可" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxAllowFileExtensionRename" Text="ファイル拡張子の変更を許可" runat="server" Checked="false" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableCreateFolder" Text="フォルダの作成を許可" runat="server" Checked="true" />
                </li>
                <li>
                    <label>ツリーパネルの幅:</label>
                    <wijmo:C1InputNumeric ID="inputTreeWidth" DecimalPlaces="0" Value="200" runat="server"></wijmo:C1InputNumeric>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FileExplorer</strong>はフォルダの作成、削除、リネーム、コピーなど数多くの機能を提供します。
        ツールバーやコンテキストメニュー、キーボード、ショートカットキーで処理を実行することが可能です。
    </p>
    <p>
        このデモでは、コピー、新しい項目の作成、ダブルクリックでファイルを開くなどの処理を有効／無効にすることができます。
    </p>
</asp:Content>
