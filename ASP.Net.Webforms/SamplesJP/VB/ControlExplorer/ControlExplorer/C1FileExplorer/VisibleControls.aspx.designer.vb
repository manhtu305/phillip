﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1FileExplorer

    Partial Public Class VisibleControls

        '''<summary>
        '''C1FileExplorer1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1FileExplorer1 As Global.C1.Web.Wijmo.Controls.C1FileExplorer.C1FileExplorer

        '''<summary>
        '''ckxShowToolbar コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowToolbar As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ckxShowAddressBox コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowAddressBox As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ckxShowFilterTextBox コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowFilterTextBox As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ckxShowTreeView コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowTreeView As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ckxShowGrid コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowGrid As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ckxShowListView コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowListView As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ckxShowContextMenu コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ckxShowContextMenu As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''btnApply コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents btnApply As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
