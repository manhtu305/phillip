﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ViewMode.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.ViewMode" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label class="settinglegend">モード:</label>
                    <asp:DropDownList ID="dplMode" runat="server">
                        <asp:ListItem Text="既定" Value="Default" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="ファイルツリー" Value="FileTree"></asp:ListItem>
                    </asp:DropDownList>
                </li>
                <li>
                    <label class="settinglegend">ビューモード:</label>
                    <asp:DropDownList ID="dplViewMode" runat="server">
                        <asp:ListItem Text="詳細" Value="Detail" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="サムネイル" Value="Thumbnail"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <div>
        <p>
            フォルダ／ファイルの表示方法を設定します。
        </p>
        <p>
            <strong>C1FileExplorer</strong>は以下のプロパティでUIレイアウトを設定できます。
        </p>

        <strong>Mode</strong>
        <ul>
            <li>
                <strong>Default</strong> - 左のツリービューでフォルダを表示し、右のグリッドビューでファイルを表示します。
            </li>
            <li>
                <strong>FileTree</strong> - 左のツリービューだけを表示して、すべてのフォルダとファイルが表示されます。
            </li>
        </ul>

        <strong>ViewMode</strong>
        <ul>
            <li>
                <strong>Detail</strong> - 名前、サイズなどを含む右のグリッドで、フォルダとファイルが表示されます。
            </li>
            <li>
                <strong>Thumbnail</strong> - 右のグリッドでサムネイル付きでフォルダとファイルが表示されます。
            </li>
        </ul>
    </div>
</asp:Content>
