﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="KeyboardSupports.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.KeyboardSupports" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1FileExplorer" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontainer .settingcontent li label {
            width: 180px;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" VisibleControls="All" AllowPaging="true" PageSize="2" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label>コントロールにフォーカス</label>
                    <wijmo:C1InputText ID="inputFocusFileExplorer" runat="server" Text="{Ctrl}+{F2}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>ツールバーにフォーカス</label>
                    <wijmo:C1InputText ID="inputFocusToolbar" runat="server" Text="{Shift}+{1}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>アドレスバーにフォーカス</label>
                    <wijmo:C1InputText ID="inputFocusAddressBar" runat="server" Text="{Shift}+{2}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>ツリービューにフォーカス</label>
                    <wijmo:C1InputText ID="inputFocusTreeView" runat="server" Text="{Shift}+{3}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>グリッドにフォーカス</label>
                    <wijmo:C1InputText ID="inputFocusGrid" runat="server" Text="{Shift}+{4}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>ページングにフォーカス</label>
                    <wijmo:C1InputText ID="inputFocusPaging" runat="server" Text="{Shift}+{5}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>コンテキストメニュー</label>
                    <wijmo:C1InputText ID="inputOpenContextMenu" runat="server" Text="{Shift}+{M}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>戻る</label>
                    <wijmo:C1InputText ID="inputBack" runat="server" Text="{Ctrl}+{K}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>進む</label>
                    <wijmo:C1InputText ID="inputForward" runat="server" Text="{Ctrl}+{L}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>開く</label>
                    <wijmo:C1InputText ID="inputOpen" runat="server" Text="{Enter}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>更新</label>
                    <wijmo:C1InputText ID="inputRefresh" runat="server" Text="{Ctrl}+{F3}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>新しいフォルダ</label>
                    <wijmo:C1InputText ID="inputNewFolder" runat="server" Text="{Shift}+{N}" Width="90px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>削除</label>
                    <wijmo:C1InputText ID="inputDelete" runat="server" Text="{Delete}" Width="90px"></wijmo:C1InputText>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <div>
        <p>
            <strong>C1FileExplorer</strong>はナビゲーション、フォルダの作成、ファイルを開く、フォルダの削除などの処理をキーボードで行うことができます。
        </p>
        <p>
            このデモでは、既定のショートカットキーを変更することができます。
        </p>
        <p>
            処理を実行する前にコントロールにフォーカスを移動する必要があることに注意してください。
            たとえば、新しいフォルダを作成するには、<strong>{Shift}+{3}</strong>を押してツリービューにフォーカスを移動してから、<strong>{Ctrl}+{N}</strong>を押します。
        </p>
    </div>
</asp:Content>
