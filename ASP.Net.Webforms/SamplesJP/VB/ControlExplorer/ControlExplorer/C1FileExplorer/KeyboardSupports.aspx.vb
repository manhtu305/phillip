﻿Namespace ControlExplorer.C1FileExplorer
    Public Class KeyboardSupports
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Me.C1FileExplorer1.Shortcuts.FocusFileExplorer = inputFocusFileExplorer.Text
            Me.C1FileExplorer1.Shortcuts.FocusToolBar = inputFocusToolbar.Text
            Me.C1FileExplorer1.Shortcuts.FocusAddressBar = inputFocusAddressBar.Text
            Me.C1FileExplorer1.Shortcuts.FocusTreeView = inputFocusTreeView.Text
            Me.C1FileExplorer1.Shortcuts.FocusGrid = inputFocusGrid.Text
            Me.C1FileExplorer1.Shortcuts.FocusGridPagingSlider = inputFocusPaging.Text
            Me.C1FileExplorer1.Shortcuts.ContextMenu = inputOpenContextMenu.Text
            Me.C1FileExplorer1.Shortcuts.Back = inputBack.Text
            Me.C1FileExplorer1.Shortcuts.Forward = inputForward.Text
            Me.C1FileExplorer1.Shortcuts.Open = inputOpen.Text
            Me.C1FileExplorer1.Shortcuts.Refresh = inputRefresh.Text
            Me.C1FileExplorer1.Shortcuts.NewFolder = inputNewFolder.Text
            Me.C1FileExplorer1.Shortcuts.Delete = inputDelete.Text
        End Sub
    End Class
End Namespace
