﻿Imports C1.Web.Wijmo.Controls.C1FileExplorer

Namespace ControlExplorer.C1FileExplorer
    Public Class VisibleControls
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Dim visibleControls As FileExplorerControls = 0

            If ckxShowToolbar.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.Toolbar
            End If

            If ckxShowAddressBox.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.AddressBox
            End If

            If ckxShowFilterTextBox.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.FilterTextBox
            End If

            If ckxShowTreeView.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.TreeView
            End If

            If ckxShowGrid.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.Grid
            End If

            If ckxShowListView.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.ListView
            End If

            If ckxShowContextMenu.Checked Then
                visibleControls = visibleControls Or FileExplorerControls.ContextMenu
            End If

            Me.C1FileExplorer1.VisibleControls = visibleControls

        End Sub

    End Class
End Namespace
