﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.OverView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FileExplorer</strong>では、WindowsエクスプローラのようなUIで、Webサーバー上のディレクトリにアクセスすることができます。フォルダの作成、リネーム、削除、コピーなどの機能を提供します。
    </p>
    <p>
        また、キーボード操作もサポートし、カスタムショートカットキーを定義できます。
    </p>
    <p>
        <strong>C1FileExplorer</strong>は複数のレイアウトをサポートし、グリッドビュー、サムネイル、ツリーの各種形式で表示します。ツールバーやアドレスボックスのような子コントロールを非表示にすることも可能です。
    </p>
    <p>
        ファイル数が多い場合は、フィルタリングで指定したフォルダ／ファイルだけを表示したり、ページング表示することも可能です。
    </p>
</asp:Content>
