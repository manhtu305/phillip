﻿Namespace ControlExplorer.C1FileExplorer
    Public Class PathSettings
        Inherits System.Web.UI.Page

        Shared splitters As Char() = New Char() {","c, ";"c}

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            If Not IsPostBack Then
                C1FileExplorer1.ViewPaths = New String() {"~/C1FileExplorer"}
                C1FileExplorer1.DeletePaths = New String() {"~/C1FileExplorer/Example"}

                inputViewPaths.Text = String.Join(";", C1FileExplorer1.ViewPaths)
                inputDeletePaths.Text = String.Join(";", C1FileExplorer1.DeletePaths)

                inputInitPath.Text = C1FileExplorer1.InitPath
                inputSearchPatterns.Text = String.Join(";", C1FileExplorer1.SearchPatterns)
            End If
        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Me.C1FileExplorer1.ViewPaths = inputViewPaths.Text.Split(splitters, StringSplitOptions.RemoveEmptyEntries)
            Me.C1FileExplorer1.InitPath = inputInitPath.Text
            Me.C1FileExplorer1.DeletePaths = inputDeletePaths.Text.Split(splitters, StringSplitOptions.RemoveEmptyEntries)
            Me.C1FileExplorer1.SearchPatterns = inputSearchPatterns.Text.Split(splitters, StringSplitOptions.RemoveEmptyEntries)
        End Sub
    End Class
End Namespace
