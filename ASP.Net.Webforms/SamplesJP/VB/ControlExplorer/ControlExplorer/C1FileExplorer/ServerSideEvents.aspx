﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ServerSideEvents.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.ServerSideEvents" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif" >
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FileExplorer</strong>はサーバー側イベント<strong>ItemCommand</strong>を提供します。このイベントは任意のファイル／フォルダ操作を行う前にサーバー側で発生します。
    </p>
    <p>
        このイベントハンドラでは<strong>C1FileExplorerEventArgs</strong>型の引数が渡され、実行されたコマンドに関するパスとその他の必要な情報が含まれます。
    </p>
    <p>
        コマンドをキャンセルするには、<strong>args.Cancel</strong>をtrueに設定します。
    </p>
    <p>
        詳細についてはソースコードタブを参照してください。
    </p>
</asp:Content>
