﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Filter.aspx.vb" Inherits="ControlExplorer.C1FileExplorer.Filter" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1FileExplorer" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontent li{
            white-space: nowrap;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <asp:CheckBox ID="ckxEnableFilterOnEnterPressed" Text="Enterキーを押したときにフィルタを有効にします。" runat="server" />
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1FileExplorer</strong>はグリッドビューでフォルダ／ファイルのフィルタリングをサポートします。
    </p>
    <p>
        既定では任意のキーを押すたびにフィルタが実行されます。Enterキーを押したときだけフィルタを実行するには、<strong>EnableFilteringOnEnterPressed</strong>をtrueに設定します。
    </p>
</asp:Content>
