﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="CheckBox.aspx.vb" Inherits="ControlExplorer.C1TreeView.CheckBox" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView"
    TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1TreeView ID="C1TreeView1" ShowCheckBoxes="true"  runat="server">
        <Nodes>
            <wijmo:C1TreeViewNode Text="フォルダ 1">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.1">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.4">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode Text="フォルダ 2">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode Text="フォルダ 3">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
        </Nodes>
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><strong>C1TreeView </strong>では、ツリーノードに 3 つの状態のチェックボックスを表示することが可能です。</p>
    <p>このサンプルでは、下記のプロパティを使用して、チェックボックス付きのツリーノードを表示しています。</p>
	<ul>
        <li><strong>ShowCheckBoxes</strong> - True に設定するとチェックボックスを表示します 。</li>
        <li><strong>AutoCheckNodes</strong> - 親のチェック状態に応じて子ノードがチェックされるかどうかを指定します。</li>
        <li><strong>AllowTriState</strong> - ノードが不確定な状態を持つかどうかを指定します。</li>
    </ul>
	<p>3 つの状態のチェックボックスの状態は、「チェック」、「チェックなし」、「不確定」となります。
    「不確定」状態は、ノード内にチェックと未チェック、両方状態の子ノードがあることを示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
