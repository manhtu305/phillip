﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DragDrop.aspx.vb" Inherits="ControlExplorer.C1TreeView.DragDrop" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <wijmo:C1TreeView ID="C1TreeView1" AllowDrag="true" AllowDrop="true" runat="server">
            <Nodes>
                <wijmo:C1TreeViewNode Text="フォルダ 1">
                    <Nodes>
                        <wijmo:C1TreeViewNode Text="フォルダ 1.1">
                            <Nodes>
                                <wijmo:C1TreeViewNode Text="フォルダ 1.1.1">
                                </wijmo:C1TreeViewNode>
                                <wijmo:C1TreeViewNode Text="フォルダ 1.1.2">
                                </wijmo:C1TreeViewNode>
                                <wijmo:C1TreeViewNode Text="フォルダ 1.1.3">
                                </wijmo:C1TreeViewNode>
                                <wijmo:C1TreeViewNode Text="フォルダ 1.1.4">
                                </wijmo:C1TreeViewNode>
                            </Nodes>
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 1.2">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 1.3">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 1.4">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 1.5">
                        </wijmo:C1TreeViewNode>
                    </Nodes>
                </wijmo:C1TreeViewNode>
                <wijmo:C1TreeViewNode Text="フォルダ 2">
                    <Nodes>
                        <wijmo:C1TreeViewNode Text="フォルダ 2.1">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 2.2">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 2.3">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 2.4">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 2.5">
                        </wijmo:C1TreeViewNode>
                    </Nodes>
                </wijmo:C1TreeViewNode>
                <wijmo:C1TreeViewNode Text="フォルダ 3">
                    <Nodes>
                        <wijmo:C1TreeViewNode Text="フォルダ 3.1">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 3.2">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 3.3">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 3.4">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="フォルダ 3.5">
                        </wijmo:C1TreeViewNode>
                    </Nodes>
                </wijmo:C1TreeViewNode>
            </Nodes>
        </wijmo:C1TreeView>
        <p>
            ツリー間のドラッグアンドドロップ操作</p>
    </div>
    <wijmo:C1TreeView ID="C1TreeView2" AllowDrop="true" runat="server">
        <Nodes>
            <wijmo:C1TreeViewNode Text="フォルダ 1">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.1">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="フォルダ 1.1.4">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode Text="フォルダ 2">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 2.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode Text="フォルダ 3">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 3.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
        </Nodes>
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1TreeView</strong> はドラッグアンドドロップ動作をサポートし、ノードをドラッグ＆ドロップしてツリー構造を変更するができます。
    </p>
    <p>
        下記のプロパティを True に設定すると、ドラッグアンドドロップ機能が有効になります。
    </p>
    <ul>
        <li><strong>AllowDrag </strong>- ドラッグを許可します。</li>
        <li><strong>AllowDrop </strong>- ドロップを許可します。</li>
    </ul>
    <p>
        ドラッグ＆ドロップ操作は、1 つのツリービュー内や 2 つのツリービュー間で行うことが可能です。
        また、クライアント側プロパティを使用して、独自にドラッグ＆ドロップ動作をカスタマイズすることもできます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
