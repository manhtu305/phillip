﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Icon.aspx.vb" Inherits="ControlExplorer.C1TreeView.Icon" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1TreeView ID="C1TreeView1" runat="server">
        <Nodes>
            <wijmo:C1TreeViewNode ExpandedIconClass="ui-icon-folder-open" CollapsedIconClass="ui-icon-folder-collapsed"
                Text="フォルダ 1">
                <Nodes>
                    <wijmo:C1TreeViewNode ExpandedIconClass="ui-icon-folder-open" CollapsedIconClass="ui-icon-folder-collapsed"
                        Text="フォルダ 1.1">
                        <Nodes>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.1.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.1.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.1.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.1.4">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 1.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode ExpandedIconClass="ui-icon-folder-open" CollapsedIconClass="ui-icon-folder-collapsed"
                Text="フォルダ 2">
                <Nodes>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 2.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 2.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 2.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 2.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 2.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode ExpandedIconClass="ui-icon-folder-open" CollapsedIconClass="ui-icon-folder-collapsed"
                Text="フォルダ 3">
                <Nodes>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 3.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 3.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 3.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 3.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="ファイル 3.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
        </Nodes>
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<strong>C1TreeViewNode</strong> にアイコンを追加する方法を紹介します。
        さらに、<strong>C1TreeViewNode</strong> の状態に応じてアイコンの切り替える方法も紹介します。
    </p>
    <p>
        下記のプロパティを使用して、これらの機能を実現しています。
    </p>
    <ul>
        <li>ItemIconClass </li>
        <li>ExpandedIconClass </li>
        <li>CollapsedIconClass </li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
