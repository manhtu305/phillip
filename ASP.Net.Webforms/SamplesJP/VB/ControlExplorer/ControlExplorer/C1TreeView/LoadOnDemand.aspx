﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="LoadOnDemand.aspx.vb" Inherits="ControlExplorer.C1TreeView.LoadOnDemand" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SiteMapDataSource ID="SiteMapDataSource" runat="server" ShowStartingNode="False" />
    <wijmo:C1TreeView ID="C1TreeView1" ShowCheckBoxes="true" LoadOnDemand="true" DataSourceID="SiteMapDataSource" ShowExpandCollapse="true" DataBindStartLevel="0" Width="350px" runat="server">
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1TreeView </strong>は、動的なデータ（ノード）の取得をサポートします。
        親ノードが展開されたときに、AJAX を使用して子ノードが読み込まれます。
    </p>
    <p>
        このサンプルでは、下記のプロパティを使用して、ロードオンデマンドを有効にしています。
    </p>
    <ul>
        <li><strong>LoadOnDemand </strong>- True に設定すると、<strong>C1TreeView </strong>はデータを部分的に読み込みます</li>
        <li><strong>DataBindStartLevel </strong>- 動的に読み込むレベルを指定します。</li>
    </ul>
    <p>
        <strong>DataBindStartLevel</strong> プロパティが 0 に設定されている場合、最初にルートノードのみが連結されます。
        値が 1 の場合、ルートノードと第 1 階層のノードが連結されます。
        既定値は -1 で、ロードオンデマンドが無視されます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
