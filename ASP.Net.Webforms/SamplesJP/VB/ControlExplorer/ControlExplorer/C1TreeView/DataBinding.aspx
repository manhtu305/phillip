﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.vb" Inherits="ControlExplorer.C1TreeView.DataBanding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:XmlDataSource ID="XmlDataSource" runat="server" DataFile="~/App_Data/treeview_structure.xml" XPath="root/treeviewnode"></asp:XmlDataSource>
    <asp:SiteMapDataSource ID="SiteMapDataSource" runat="server" ShowStartingNode="False" />
    <wijmo:C1TreeView ID="C1TreeView1" ShowCheckBoxes="true" DataSourceID="SiteMapDataSource" ShowExpandCollapse="true" Width="350px" runat="server">
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1TreeView</strong> はデータ連結をサポートします。
        XML や SiteMap データソースと連結して、または任意のデータソースからデータを読み込んで、動的に <strong>C1TreeView</strong> 階層を作成できます。
    </p>
    <p>
       このサンプルでは、下記のプロパティを使用して、<strong>C1TreeView</strong> をデータソースと連結しています。
    </p>
    <ul>
        <li><strong>DataSourceID - </strong>連結するデータソースを指定します。</li>
        <li><strong>C1TreeViewNodeBinding - </strong><strong>XmlDataSource</strong> の属性と C1TreeView のデータフィールド間のマッピングリレーションシップを指定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <p class="ControlOptions-Option-Header">
        データソースの選択</p>
    <%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div style="clear: none; float: left; width: 190px; margin-left: 5px;">
        <asp:RadioButton ID="Select_SiteMapDataSource" CssClass="radio DataBinding" AutoPostBack="true" Text="SiteMap データソース" Checked="true" GroupName="DataSourceGroup" OnCheckedChanged="CheckChnged" runat="server" />
    </div>
    <div style="clear: none; float: left; width: 190px;">
        <asp:RadioButton ID="Select_XMLDataSource" CssClass="radio" AutoPostBack="true" Text="XML データソース" GroupName="DataSourceGroup" OnCheckedChanged="CheckChnged" runat="server" />
    </div>
    <%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
