﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ObjectModel.aspx.vb" Inherits="ControlExplorer.C1TreeView.ObjectModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function getNode() {
            return $("#<%= C1TreeView1.ClientID %>").c1treeview("findNodeByText", "フォルダ 1"); ;
        }

        function add() {
            var node = getNode();
            if (node != null)
                node.element.c1treeviewnode("add", '新しいノード', parseInt($("#addIndex").val()));
        }

        function remove() {
            var node = getNode();
            if (node != null)
                node.element.c1treeviewnode("remove", parseInt($("#removeIndex").val()));
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1TreeView ID="C1TreeView1" runat="server">
        <Nodes>
            <wijmo:C1TreeViewNode Text="フォルダ 1">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="フォルダ 1.4">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
        </Nodes>
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、クライアント側コードで <strong>C1TreeView </strong> のノードを動的に追加または削除します。</p>
    <p>
        このサンプルでは、下記のクライアント側メソッドが使用されています。</p>
    <ul>
        <li>add</li>
        <li>remove</li>
    </ul>
    <p>
        add メソッドには、下記のパラメータがあります。</p>
    <ul>
        <li><strong>node</strong> - <strong>C1TreeView</strong> や <strong>C1TreeViewNode</strong> に追加されるマークアップ／jQueryオブジェクト／ツリーノードオプションを指定します。</li>
        <li><strong>index</strong> - 同じレベルのノードのインデックスを指定します。<strong>index</strong> が null の場合、ノードが末尾に追加されます。</li>
    </ul>
    <p>
        remove メソッドには、下記のパラメータがあります。</p>
    <ul>
        <li><strong>index</strong> - <strong>C1TreeView</strong> や <strong>C1TreeViewNode</strong> の子ノードのインデックスを指定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div>
    インデックス
    <input id="addIndex" type="text" value="0" />
    <input id="addNode" onclick="add();" type="button" value="追加" />
    </div>
    <div>
    インデックス
    <input id="removeIndex" type="text" value="0" />
    <input id="removeNode" onclick="remove();" type="button" value="削除" />
    </div>
</asp:Content>
