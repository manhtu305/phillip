'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1TreeView


	Public Partial Class DataBanding

		''' <summary>
		''' XmlDataSource コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected XmlDataSource As Global.System.Web.UI.WebControls.XmlDataSource

		''' <summary>
		''' SiteMapDataSource コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected SiteMapDataSource As Global.System.Web.UI.WebControls.SiteMapDataSource

		''' <summary>
		''' C1TreeView1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1TreeView1 As Global.C1.Web.Wijmo.Controls.C1TreeView.C1TreeView

		''' <summary>
		''' Select_SiteMapDataSource コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Select_SiteMapDataSource As Global.System.Web.UI.WebControls.RadioButton

		''' <summary>
		''' Select_XMLDataSource コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Select_XMLDataSource As Global.System.Web.UI.WebControls.RadioButton
	End Class
End Namespace
