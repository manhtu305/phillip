Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls
Imports C1.Web.Wijmo.Controls.C1TreeView

Namespace ControlExplorer.C1TreeView
	Public Partial Class DataBanding
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub

		Protected Sub CheckChnged(sender As Object, e As EventArgs)
			ApplyDataSourceID()
		End Sub

		Private Sub ApplyDataSourceID()
			If Me.Select_XMLDataSource.Checked Then
				Dim binding As New C1TreeViewNodeBinding("treeviewnode")
				binding.FormatString = "{0}"
				binding.TextField = "Text"
				binding.ExpandedField = "Expanded"
				binding.ValueField = "Value"
				binding.ItemIconClassField = "ItemIconClass"
				binding.CollapsedIconClassField = "CollapsedIconClass"
				binding.ExpandedIconClassField = "ExpandedIconClass"

				C1TreeView1.DataSourceID = "XmlDataSource"

				C1TreeView1.DataBindings.Add(binding)
			Else
				C1TreeView1.DataBindings.Clear()
				C1TreeView1.DataSourceID = "SiteMapDataSource"
			End If
		End Sub
	End Class
End Namespace
