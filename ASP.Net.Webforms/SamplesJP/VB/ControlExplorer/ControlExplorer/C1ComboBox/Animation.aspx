﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1ComboBox.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script id="scriptInit" type="text/javascript">
		function changeProperties() {
			var showingSpeed, hidingSpeed, expandingSpeed, collapsingSpeed;
			showingSpeed = parseNumberFromString($('#showingSpeed').val());
			hidingSpeed = parseNumberFromString($('#hidingSpeed').val());
			var showingAnimation = {
				effect: $('#showingEffectTypes').get(0).value,
				options: null,
				speed: showingSpeed
			};
			var hidingAnimation = {
				effect: $('#hidingEffectTypes').get(0).value,
				options: null,
				speed: hidingSpeed
			};

			if ($('#showingEffectTypes').get(0).value === "none") {
				showingAnimation = null;
			}

			if ($('#hidingEffectTypes').get(0).value === "none") {
				hidingAnimation = null;
			}

			var wij = $('#<%=C1ComboBox1.ClientID%>');
			wij.c1combobox('option', 'showingAnimation', showingAnimation);
			wij.c1combobox('option', 'hidingAnimation', hidingAnimation);
		}

		function parseNumberFromString(st) {
			var i = 1000;
			try {
				i = parseInt(st);
			}
			catch (e) {
				alert(e);
			}
			return i;
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
		<style type ="text/css">
			.ui-effects-explode
			{
				z-index:99999;
			}
		</style>
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px">
	<ShowingAnimation Duration="1000">
	<Animated Effect="Scale" />
	</ShowingAnimation>
	<HidingAnimation Duration="1000">
	<Animated Effect="explode" />
	</HidingAnimation>
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1ComboBox </strong>は、ドロップダウンの表示時および非表示時のアニメーション効果をサポートしています。
    次のプロパティを設定すると、表示時および非表示時のアニメーション効果が有効になります。</p>
	<ul>
	<li><strong>ShowingAnimation </strong>- ドロップダウンリストが表示されるときに使用されます。</li>
	<li><strong>HidingAnimation </strong>- ドロップダウンリストが非表示になるときに使用されます。</li>
	</ul>
    <p>
	<strong>ShowingAnimation</strong> と <strong>HidingAnimation</strong> プロパティが設定されると、エンドユーザーはアニメーション効果を確認することができます。
	</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="demo-options">
		<!-- オプションのマークアップ -->
		<div class="option-row">
			<label for="showingSpeed">
				表示時の速度：
			</label>
			<input id="showingSpeed" type="text" value="1000" onblur="changeProperties()" />
		</div>
		<div class="option-row">
			<label for="showingEffectTypes">
				表示時の効果：
			</label>
			<select id="showingEffectTypes" name="effects" onchange="changeProperties()">
				<option value="none">なし</option>
				<option value="blind">Blind</option>
				<option value="bounce">Bounce</option>
				<option value="clip">Clip</option>
				<option value="drop">Drop</option>
				<option value="explode">Explode</option>
				<option value="fade">Fade</option>
				<option value="fold">Fold</option>
				<option value="highlight">Highlight</option>
				<option value="puff">Puff</option>
				<option value="pulsate">Pulsate</option>
				<option value="scale" selected="selected">Scale</option>
				<option value="shake">Shake</option>
				<option value="size">Size</option>
				<option value="slide">Slide</option>
			</select>
		</div>
		<div class="option-row">
			<label for="hidingSpeed">
				非表示時の速度：
			</label>
			<input id="hidingSpeed" type="text" value="1000" onblur="changeProperties()" />
		</div>
		<div class="option-row">
			<label for="hidingEffectTypes">
				非表示時の効果：
			</label>
			<select id="hidingEffectTypes" name="effects" onchange="changeProperties()">
				<option value="none">なし</option>
				<option value="blind">Blind</option>
				<option value="bounce">Bounce</option>
				<option value="clip">Clip</option>
				<option value="drop">Drop</option>
				<option value="explode" selected="selected">Explode</option>
				<option value="fade">Fade</option>
				<option value="fold">Fold</option>
				<option value="highlight">Highlight</option>
				<option value="puff">Puff</option>
				<option value="pulsate">Pulsate</option>
				<option value="scale">Scale</option>
				<option value="shake">Shake</option>
				<option value="size">Size</option>
				<option value="slide">Slide</option>
			</select>
		</div>
		<!-- オプションマークアップの終了 -->
	</div>
</asp:Content>
