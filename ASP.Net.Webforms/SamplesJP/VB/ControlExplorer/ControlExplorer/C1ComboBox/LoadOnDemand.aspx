﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="LoadOnDemand.aspx.vb" Inherits="ControlExplorer.C1ComboBox.LoadOnDemand" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h3>ItemPopulate イベントを使用</h3>
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" CallbackDataPopulate="ItemPopulate"
		EnableCallBackMode="True" OnItemPopulate="C1ComboBox1_ItemPopulate">
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
    <br />
    <br />
	<h3>CallbackDataBind イベントを使用</h3>
	<wijmo:C1ComboBox ID="C1ComboBox2" runat="server" Width="160px"  
		EnableCallBackMode="True"  oncallbackdatabind="C1ComboBox2_CallbackDataBind" DataTextField="Text" DataValueField="Value" DataSelectedField="Selected"  >
	</wijmo:C1ComboBox>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

	<p><strong>C1ComboBox </strong>コントロールは、スクロール時の動的なドロップダウンリストの読み込みをサポートしています。<strong>OnItemPopulate</strong> または <strong>OnCallbackDataBind</strong> の 2 種類の方法を使用して、リストを読み込みます。</p>
    <p>C1ComboBox がデータソースに連結されていない場合は、次のプロパティを使用して項目を生成することができます。</p>

	<p><strong>OnItemPopulate</strong> イベント：<strong>C1ComboBox</strong> がクライアント側からデータを要求するときに発生します。</p>
	<ul>
	<li>EnableCallBackMode: true</li>
	<li>CallbackDataPopulate: ItemPopulate</li>
    </ul>
	<p><strong>CallbackDataBind</strong> イベント：<strong>C1ComboBox</strong> がデータソースからデータを要求するときに発生します。</p>
	<ul>
	<li>EnableCallBackMode: true</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
