﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="DynamicData.aspx.vb" Inherits="ControlExplorer.C1ComboBox.DynamicData" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script id="scriptInit" type="text/javascript">
		function setData() {
			$('#<%=C1ComboBox1.ClientID %>').c1combobox("option", "data", [
				{
					label: 'delphi',
					value: 'delphi'
				},
				{
					label: 'visual studio',
					value: 'visual studio'
				},
				{
					label: 'flash',
					value: 'flash'
				}
			]);
		}
			
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px">
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1ComboBox </strong>は、クライアント側での <strong>C1ComboBox</strong> への動的なデータの追加をサポートしています。</p>

	<p><strong>data</strong> オプションをラベル／値の配列に設定すると、 動的なデータ追加が有効になります。</p>
	<p>例：[{label:'delphi', value: 'delphi'}]</p>
	<p>このオプションを設定すると、ドロップダウンの内容が新しい値に置き換わります。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="demo-options">
		<input type="button" value="動的なデータを設定 " onclick="setData()" />
	</div>
</asp:Content>
