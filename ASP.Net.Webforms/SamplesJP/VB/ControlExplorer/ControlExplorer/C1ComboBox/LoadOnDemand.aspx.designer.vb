'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1ComboBox

    Partial Public Class LoadOnDemand

        '''<summary>
        '''C1ComboBox1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ComboBox1 As Global.C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox

        '''<summary>
        '''C1ComboBox2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ComboBox2 As Global.C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox
    End Class
End Namespace
