﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Select.aspx.vb" Inherits="ControlExplorer.C1ComboBox.Select" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script id="scriptInit" type="text/javascript">
		function C1ComboBox1_OnClientChanged(e, data) {
			var val = data.selectedItem.value;
			$('#output').html('私は' + val + 'に住んでいます。');
		}	
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" OnClientChanged="C1ComboBox1_OnClientChanged">
		<Items>
			<wijmo:C1ComboBoxItem Value="北海道" Text="北海道"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="青森" Text="青森県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="岩手" Text="岩手県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="宮城" Text="宮城県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="秋田" Text="秋田県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="山形" Text="山形県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="福島" Text="福島県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="茨城" Text="茨城県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="栃木" Text="栃木県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="群馬" Text="群馬県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="埼玉" Text="埼玉県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="千葉" Text="千葉県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="東京" Text="東京都"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="神奈川" Text="神奈川県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="新潟" Text="新潟県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="富山" Text="富山県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="石川" Text="石川県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="福井" Text="福井県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="山梨" Text="山梨県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="長野" Text="長野県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="岐阜" Text="岐阜県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="静岡" Text="静岡県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="愛知" Text="愛知県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="三重" Text="三重県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="滋賀" Text="滋賀県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="京都" Text="京都府"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="大阪" Text="大阪府"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="兵庫" Text="兵庫県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="奈良" Text="奈良県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="和歌山" Text="和歌山県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="鳥取" Text="鳥取県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="島根" Text="島根県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="岡山" Text="岡山県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="広島" Text="広島県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="山口" Text="山口県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="徳島" Text="徳島県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="香川" Text="香川県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="愛媛" Text="愛媛県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="高知" Text="高知県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="福岡" Text="福岡県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="佐賀" Text="佐賀県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="長崎" Text="長崎県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="熊本" Text="熊本県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="大分" Text="大分県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="宮崎" Text="宮崎県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="鹿児島" Text="鹿児島県"></wijmo:C1ComboBoxItem>
			<wijmo:C1ComboBoxItem Value="沖縄" Text="沖縄県"></wijmo:C1ComboBoxItem>
		</Items>
	</wijmo:C1ComboBox>
	<p>
		<label id="output">
			選択してください。</label></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1ComboBox </strong>は、選択項目が変更されたときに発生するクライアント側のイベントをサポートします。</p>
    <p><strong>OnClientChanged</strong> プロパティが JavaScript 関数名に設定されたときに、このイベントが発生します。
    選択項目が変更されたときに、この関数が呼び出されます。</p>
	<p>エンドユーザーは、select イベントで <strong>selectedItem</strong> を取得することができます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
