﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Position.aspx.vb" Inherits="ControlExplorer.C1ComboBox.Position" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script id="scriptInit" type="text/javascript">
		function changed() {
			$("#<%=C1ComboBox1.ClientID%>").c1combobox('option', 'dropDownListPosition', {
				my: $('#my_horizontal').val() + ' ' + $('#my_vertical').val(),
				at: $('#at_horizontal').val() + ' ' + $('#at_vertical').val(),
				offset: $('#offset').val(),
				collision: $("#collision_horizontal").val() + ' ' + $("#collision_vertical").val()
			});
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" >
	<DropDownListPosition >
		<At Left="Center" Top="Bottom" />
		<My Left="Right" Top="Top" />
	</DropDownListPosition>
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
	<script type="text/javascript">
		$(document).ready(function () {
			$('.position').bind('change', changed);
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1ComboBox</strong> は、ドロップダウンリストの位置の変更をサポートしています。
	</p>
	<ul>
	<li><strong>DropDownListPosition </strong>- ドロップダウンリストの位置を変更します。</li>
	</ul>

	<p>
	このオプションを使用すると、ドロップダウンリストの位置を制御することができます。水平方向の配置、垂直方向の配置、オフセット、衝突設定がサポートされています。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="demo-options">
		<!-- オプションのマークアップ -->
		<div class="option-row">
			<label for="my_horizontal">
				ドロップダウンリスト：</label>
			<select id="my_horizontal" class="position">
				<option value="left">左</option>
				<option value="center">中央</option>
				<option value="right">右</option>
			</select>
			<select id="my_vertical" class="position">
				<option value="top">上</option>
				<option value="middle">中央</option>
				<option value="bottom">下</option>
			</select>
		</div>
		<div class="option-row">
			<label for="at_horizontal">
				テキストボックスの配置：</label>
			<select id="at_horizontal" class="position">
				<option value="left">左</option>
				<option value="center">中央</option>
				<option value="right">右</option>
			</select>
			<select id="at_vertical" class="position">
				<option value="top">上</option>
				<option value="middle">中央</option>
				<option value="bottom">下</option>
			</select>
		</div>
		<div class="option-row">
			<label for="offset">
				オフセット：</label>
			<input onblur="changed()" id="offset" type="text" size="15" />
		</div>
		<div class="option-row">
			<label for="collision_horizontal">
				水平衝突検出：</label>
			<select id="collision_horizontal">
				<option value="flip">フリップ</option>
				<option value="fit">幅に合わせる</option>
				<option value="none">なし</option>
			</select>
			<label for="collision_vertical">
				垂直衝突検出：</label>
			<select id="collision_vertical">
				<option value="flip">フリップ</option>
				<option value="fit">幅に合わせる</option>
				<option value="none">なし</option>
			</select>
		</div>
		<!-- オプションマークアップの終了 -->
	</div>
</asp:Content>
