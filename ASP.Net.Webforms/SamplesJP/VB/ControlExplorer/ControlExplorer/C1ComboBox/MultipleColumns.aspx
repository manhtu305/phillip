﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="MultipleColumns.aspx.vb" Inherits="ControlExplorer.C1ComboBox.MultipleColumns" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" DropdownHeight="500" AutoPostBack="true"
		DropdownWidth="400">
		<Columns>
			<wijmo:C1ComboBoxColumn Name="列01" />
			<wijmo:C1ComboBoxColumn Name="列02" />
			<wijmo:C1ComboBoxColumn Name="列03" />
			<wijmo:C1ComboBoxColumn Name="列04" />
		</Columns>
		<Items>
			<wijmo:C1ComboBoxItem Text="ラベル01" Value="value01" Cells="セルA0,セルA1,セルA2,セルA3" />
			<wijmo:C1ComboBoxItem Text="ラベル02" Value="value02" Cells="セルB0,セルB1,セルB2,セルB3" />
			<wijmo:C1ComboBoxItem Text="ラベル03" Value="value03" Cells="セルC0,セルC1,セルC2,セルC3" />
			<wijmo:C1ComboBoxItem Text="ラベル04" Value="value04" Cells="セルD0,セルD1,セルD2,セルD3" />
			<wijmo:C1ComboBoxItem Text="ラベル05" Value="value05" Cells="セルE0,セルE1,セルE2,セルE3" />
			<wijmo:C1ComboBoxItem Text="ラベル06" Value="value06" Cells="セルF0,セルF1,セルF2,セルF3" />
		</Items>
	</wijmo:C1ComboBox>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><strong>C1ComboBox </strong>は複数列の設定をサポートしています。</p>

	<p>次のプロパティが対応する値に設定されている場合、複数の列が有効になります。</p>
	<ul>
	<li>Columns</li>
	<li>C1ComboBoxItem.Cells</li>
	</ul>
	<p>Columns は <strong>C1ComboBox</strong> 内の列のコレクションを示します。</p>
    <p>列の数はセルの数と一致する必要があります。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
