Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1ComboBox
Imports System.Data

Namespace ControlExplorer.C1ComboBox
	Public Partial Class LoadOnDemand
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
		End Sub

		Private Function GetDataTable(name As String) As DataTable
			Dim dt As New DataTable()
			dt.Columns.Add(New DataColumn("Text"))
			dt.Columns.Add(New DataColumn("Value"))
			dt.Columns.Add(New DataColumn("Selected", GetType([Boolean])))

			For i As Integer = 0 To 29
				Dim row1 As DataRow = dt.NewRow()
				row1("Text") = name & i.ToString()
				row1("Value") = i.ToString()
				row1("Selected") = False
				dt.Rows.Add(row1)
			Next

			Return dt
		End Function

		Protected Sub C1ComboBox1_ItemPopulate(sender As Object, args As C1ComboBoxItemPopulateEventArgs)
			Dim dt As DataTable = GetDataTable("新しい言語 X")
			If args.RequestedItemCount >= dt.Rows.Count Then
				args.EndRequest = True
			End If

			Dim curOffset As Integer = args.RequestedItemCount
			Dim endOffset As Integer = Math.Min(curOffset + 5, dt.Rows.Count)

			For i As Integer = curOffset To endOffset - 1
				Dim item As New C1ComboBoxItem()
				item.Text = dt.Rows(i)("Text").ToString()
				item.Value = dt.Rows(i)("Value").ToString()
				item.Selected = CBool(dt.Rows(i)("Selected"))
				Me.C1ComboBox1.Items.Add(item)
			Next
		End Sub

		Protected Sub C1ComboBox2_CallbackDataBind(sender As Object, args As C1ComboBoxCallbackEventArgs)
			args.DataSource = GetDataTable("番号： ")
			args.PageSize = 12
		End Sub

	End Class
End Namespace
