﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="DataBinding.aspx.vb" Inherits="ControlExplorer.C1ComboBox.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" 
		DataSourceID="AccessDataSource1" DataTextField="UnitPrice" 
		DataTextFormatString="{0:C}" DataValueField="OrderID">
	</wijmo:C1ComboBox>
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/Nwind_ja.mdb"
		SelectCommand="SELECT top 100 [UnitPrice], [OrderID] FROM [Order Details]"></asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><strong>C1ComboBox </strong>コントロールはデータ連結をサポートし、テキストと値の両方のフィールドを連結することができます。</p>
    <p>次のプロパティを設定すると、データ連結が有効になります。</p>
	<ul>
	<li><strong>DataSourceID </strong>- データソースのIDを設定します。</li>
	<li><strong>DataTextField </strong>- テキスト値を読み込むデータソース内のフィールドを設定します。</li>
	<li><strong>DataValueField </strong>- 項目の値を読み込むデータソース内のフィールドを設定します。</li>
	</ul>
	<p>"Text" と "Value" が連結されると、連結された値は <strong>C1ComboBox</strong> ドロップダウンリストに挿入されます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
