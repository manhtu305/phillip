﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="DataBinding.aspx.vb" Inherits="ControlExplorer.C1AutoComplete.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1AutoComplete"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        SqlDataSourceとのデータ連結
    </p>
    <wijmo:C1AutoComplete ID="C1AutoComplete1" runat="server" Width="250px"
        DataSourceID="SqlDataSource1">
    </wijmo:C1AutoComplete>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DATADIRECTORY|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT ProductID, ProductName FROM Products">
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1AutoComplete</strong> はデータ連結をサポートし、ラベル、値、カテゴリのフィールドを連結することができます。
    </p>
    <p>
        次のプロパティを設定すると、データ連結が有効になります。
    </p>
    <ul>
        <li><strong>DataSourceID </strong>- データソースのIDを設定します。</li>
        <li><strong>DataLabelField </strong>-&nbsp;ラベルの値を読み込むデータソース内のフィールドを設定します。</li>
        <li><strong>DataValueField </strong>-&nbsp; 項目の値を読み込むデータソース内のフィールドを設定します。</li>
        <li><strong>DataCategoryField </strong>-&nbsp; カテゴリの値を読み込むデータソース内のフィールドを設定します。</li></ul>
</asp:Content>
