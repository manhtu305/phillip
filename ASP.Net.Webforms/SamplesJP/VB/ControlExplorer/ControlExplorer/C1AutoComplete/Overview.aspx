﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1AutoComplete.WebForm1" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1AutoComplete"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        概要
    </p>
    <wijmo:C1AutoComplete ID="C1AutoComplete1" runat="server" Width="250px">
        <Items>
            <wijmo:C1AutoCompleteDataItem Label="c++" Value="c++" />
            <wijmo:C1AutoCompleteDataItem Label="java" Value="java" />
            <wijmo:C1AutoCompleteDataItem Label="php" Value="php" />
            <wijmo:C1AutoCompleteDataItem Label="coldfusion" Value="coldfusion" />
            <wijmo:C1AutoCompleteDataItem Label="javascript" Value="javascript" />
            <wijmo:C1AutoCompleteDataItem Label="asp" Value="asp" />
            <wijmo:C1AutoCompleteDataItem Label="ruby" Value="ruby" />
            <wijmo:C1AutoCompleteDataItem Label="python" Value="python" />
            <wijmo:C1AutoCompleteDataItem Label="c" Value="c" />
            <wijmo:C1AutoCompleteDataItem Label="scala" Value="scala" />
            <wijmo:C1AutoCompleteDataItem Label="groovy" Value="groovy" />
            <wijmo:C1AutoCompleteDataItem Label="haskell" Value="haskell" />
            <wijmo:C1AutoCompleteDataItem Label="perl" Value="perl" />
        </Items>
    </wijmo:C1AutoComplete>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1AutoComplete </strong>は、ユーザーの入力に応じて入力候補を表示します。
        このサンプルでは、例えば"ja"と入力すると Java および JavaScript が表示されます。
    </p>
    <p>
        ユーザーはデータ項目、最小文字数、関連付けられた入力要素に対するコントロールの位置を設定可能です。
    </p>
</asp:Content>
