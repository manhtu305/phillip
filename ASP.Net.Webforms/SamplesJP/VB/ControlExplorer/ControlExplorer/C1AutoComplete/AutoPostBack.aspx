﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="AutoPostBack.aspx.vb" Inherits="ControlExplorer.C1AutoComplete.MultipleValues" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1AutoComplete"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        自動ポストバック
    </p>
    <wijmo:C1AutoComplete ID="C1AutoComplete1" runat="server" AutoPostBack="true" Width="250px" OnItemSelected="C1AutoComplete1_ItemSelected">
        <Items>
            <wijmo:C1AutoCompleteDataItem Label="c++" Value="c++" />
            <wijmo:C1AutoCompleteDataItem Label="java" Value="java" />
            <wijmo:C1AutoCompleteDataItem Label="php" Value="php" />
            <wijmo:C1AutoCompleteDataItem Label="coldfusion" Value="coldfusion" />
            <wijmo:C1AutoCompleteDataItem Label="javascript" Value="javascript" />
            <wijmo:C1AutoCompleteDataItem Label="asp" Value="asp" />
            <wijmo:C1AutoCompleteDataItem Label="ruby" Value="ruby" />
            <wijmo:C1AutoCompleteDataItem Label="python" Value="python" />
            <wijmo:C1AutoCompleteDataItem Label="c" Value="c" />
            <wijmo:C1AutoCompleteDataItem Label="scala" Value="scala" />
            <wijmo:C1AutoCompleteDataItem Label="groovy" Value="groovy" />
            <wijmo:C1AutoCompleteDataItem Label="haskell" Value="haskell" />
            <wijmo:C1AutoCompleteDataItem Label="perl" Value="perl" />
        </Items>
    </wijmo:C1AutoComplete>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:ListBox ID="ListBox1" runat="server" Width="300px" Height="300px" Font-Size="16px"></asp:ListBox>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、オートコンプリート項目を選択した際のポストバック機能を示します。
        この機能を有効にするには、AutoPostBack プロパティを True に設定します。
        例えば"ja"と入力すると、Java および JavaScript が表示されます。
    </p>
</asp:Content>
