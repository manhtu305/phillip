﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="DataBinding.aspx.vb" Inherits="C1PieChart_DataBinding" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
		    return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
		<wijmo:C1PieChart ID="C1PieChart1" runat="server" Radius="140" DataSourceID="AccessDataSource1" Height="475" Width = "756" CssClass ="ui-widget ui-widget-content ui-corner-all" EnableTouchBehavior="False">
			<Hint>
				<Content Function="hintContent" />
			</Hint>
			<Header Text="食品の消費量">
			</Header>
			<SeriesStyles>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#AFE500">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#C3FF00" ColorEnd="#AFE500"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#7FC73C">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#8EDE43" ColorEnd="#7FC73C"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#5F9996">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#6AABA7" ColorEnd="#5F9996"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#3E5F77">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#466A85" ColorEnd="#3E5F77"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#959595">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#A6A6A6" ColorEnd="#959595"></Fill>
				</wijmo:ChartStyle>
			</SeriesStyles>
			<Footer Compass="South" Visible="False">
			</Footer>
			<DataBindings>
				<wijmo:C1PieChartBinding DataField="Sales" LabelField="CategoryName"  />
			</DataBindings>
		</wijmo:C1PieChart>
		<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
			DataFile="~/App_Data/Nwind_ja.mdb" 
			SelectCommand="select CategoryName, sum(ProductSales) as Sales from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
		</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><strong>C1PieChart </strong>は、サーバー上の外部データソースとの連結をサポートします。</p><br/>
	<p>
		<b>DataSourceID</b> や <b>DataSource</b> および <b>DataBindings</b> を設定して、データ連結を有効にできます。</p>
	<p>
		以下のプロパティを使用すると、データとラベルをデータフィールドに連結することができます。</p>
	<ul>
		<li><strong>DataSourceID</strong> - App_Data/Nwind_ja.mdbにあるデータソースを指定します。</li>
		<li><strong>DataBindings</strong> - 系列の連結を指定します。</li>
		<li><strong>C1ChartBinding.XField</strong> - X を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.XFieldType</strong> - XType を指定したフィールド名と連結します。</li>
	</ul>
	<p>DataBindings は、<strong>C1PieChartBinding</strong> インスタンスのコレクションです。
    <strong>C1PieChartBinding</strong> には、以下のプロパティがあります。</p>
	<ul>
		<li><strong>DataMember</strong> -  データソースに 1 つ以上のリストがある場合、データリストの名前を指定します。</li>
		<li><strong>HintField</strong> -  ヒントコンテンツを指定したフィールド名と連結します。</li>
		<li><strong>OffsetField </strong>- オフセットを指定したフィールド名と連結します。</li>
	</ul>
	<p><strong>HintField</strong> プロパティが設定されている場合、マウスを系列上に移動すると、系列と同じインデックスのヒント値を表示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

