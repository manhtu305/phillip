﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="SeriesTransition.aspx.vb" Inherits="ControlExplorer.C1PieChart.SeriesTransition" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		function hintContent() {
		    return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
		   }

		   function changeProperties() {
			var animation = {};
			enabled = $("#chkEnabled").is(":checked"),
				duration = $("#inpDuration").val(),
				easing = $("#selEasing").val();
			animation.enabled = enabled;
			if (duration && duration.length) {
				animation.duration = parseFloat(duration);
			}
			animation.easing = easing;
			$("#<%= C1PieChart1.ClientID %>").c1piechart("option", "seriesTransition", animation);
		   }

		   function reload() {
			$("#<%= C1PieChart1.ClientID %>").c1piechart("option", "seriesList", createRandomSeriesList());
		   }

		   function createRandomSeriesList() {
			var seriesList = [],
				randomDataValuesCount = 6,
				labels = ["1月", "2月", "3月", "4月", "5月", "6月"],
				idx;
			for (idx = 0; idx < randomDataValuesCount; idx++) {
				seriesList.push({
					label: labels[idx],
					legendEntry: true,
					data: createRandomValue()
				});
			}
			return seriesList;
		   }

		   function createRandomValue() {
			var val = Math.round(Math.random() * 100);
			return val;
		   }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<input type="button" value="再描画" onclick="reload()" />
	<wijmo:C1PieChart runat="server" ID="C1PieChart1" Radius="140" Height="430" Width = "756" CssClass ="ui-widget ui-widget-content ui-corner-all" EnableTouchBehavior="False">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<SeriesList>
			<wijmo:PieChartSeries Label="1月" LegendEntry="true" Data="50" />
			<wijmo:PieChartSeries Label="2月" LegendEntry="true" Data="38" />
			<wijmo:PieChartSeries Label="3月" LegendEntry="true" Data="73" />
			<wijmo:PieChartSeries Label="4月" LegendEntry="true" Data="13" />
			<wijmo:PieChartSeries Label="5月" LegendEntry="true" Data="89" />
			<wijmo:PieChartSeries Label="6月" LegendEntry="true" Data="5" />
		</SeriesList>
		<Legend Visible="true"></Legend>
		<Footer Compass="South" Visible="False">
		</Footer>
		<Axis>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center">
				</Labels>
				<GridMajor Visible="True">
				</GridMajor>
			</Y>
		</Axis>
	</wijmo:C1PieChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
   <p><strong>C1PieChart</strong> コントロールでは、データの描画時に様々なアニメーション効果を設定できます。</p>
	<p>
		<strong>SeriesTransition</strong> プロパティを設定すると、データ描画時のアニメーション効果を制御できます。
	</p>
	<ul>
		<li><strong>SeriesTransition.Enabled </strong>- アニメーションを有効／無効にします。</li>
		<li><strong>SeriesTransition.Duration </strong>- アニメーションの継続時間を指定します。</li>
		<li><strong>SeriesTransition.Easing </strong>- アニメーションの種類を設定します。</li>
	</ul>
	<p> <strong>Easing</strong> プロパティは、下記のいずれかの値に設定できます。</p>
	<ul>
		<li>easeInCubic(">")</li>
		<li>easeOutCubic("<")</li>
		<li>easeInOutCubic("<>")</li>
		<li>easeInBack("backIn")</li>
		<li>easeOutBack("backOut")</li>
		<li>easeOutElastic("elastic")</li>
		<li>easeOutBounce("bounce")</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div>
		<label for="chkEnabled">
			系列遷移アニメーション：
		</label>
		<input id="chkEnabled" type="checkbox" checked="checked" />
		<label for="inpDuration">
			　継続時間：
		</label>
		<input id="inpDuration" type="text" value="1000" style="width:60px;" />
		<label for="selEasing">
			　イージング：
		</label>
		<select id="selEasing">
			<option value=">">></option>
			<option value="<"><</option>
			<option value="<>"><></option>
			<option value="backIn">backIn</option>
			<option value="backOut">backOut</option>
			<option value="bounce">bounce</option>
			<option value="elastic">elastic</option>
		</select>
		<input type="button" value="適用" onclick="changeProperties()" />
	</div>
</asp:Content>
