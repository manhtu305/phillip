﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="Doughnut.aspx.vb" Inherits="C1PieChart_Donut" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
		    return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1PieChart runat = "server" ID="C1PieChart1" Radius="140" InnerRadius="40" ShowChartLabels="false" Height="475" Width = "756" CssClass ="ui-widget ui-widget-content ui-corner-all" EnableTouchBehavior="False">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Legend Text="2011年5月 - 2012年5月"></Legend>
		<Header Text="システムの割合"></Header>
		<SeriesList>
			<wijmo:PieChartSeries Label="システムA" Data="5.6" Offset="30"></wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="システムB" Data="23.18"></wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="システムC" Data="56.36"></wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="システムD" Data="16.67"></wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="システムE" Data="11.77"></wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="システムF" Data="4.34"></wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="システムG" Data="5.13"></wijmo:PieChartSeries>
		</SeriesList>
	</wijmo:C1PieChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
		<b>InnerRadius</b> プロパティを設定すると、<strong>C1PieChart</strong> をドーナツグラフとして描画できます。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

