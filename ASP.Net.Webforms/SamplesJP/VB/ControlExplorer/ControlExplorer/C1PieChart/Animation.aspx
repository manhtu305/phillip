﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="Animation.aspx.vb" Inherits="C1PieChart_Animation" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
		    return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1PieChart runat="server" ID="C1PieChart1" ShowChartLabels="false" Height="460" Width = "756" CssClass ="ui-widget ui-widget-content ui-corner-all" EnableTouchBehavior="False">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Header Text="ゲーム機の市場占有率">
		</Header>
		<Animation Enabled="false" />
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#3e5f77" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#466a85" ColorEnd="#3e5f77"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#494949" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#525252" ColorEnd="#494949"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#4c5454" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#555e5e" ColorEnd="#4c5454"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#2d2d2d" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#333333" ColorEnd="#2d2d2d"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#5f9996" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#6aaba7" ColorEnd="#5f9996"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#959595" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#a6a6a6" ColorEnd="#959595"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#617779" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#6c8587" ColorEnd="#617779"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#b2c76d" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#c7de7a" ColorEnd="#b2c76d"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#7fc73c" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#8ede43" ColorEnd="#7fc73c"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#afe500" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#c3ff00" ColorEnd="#afe500"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#959595" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#a6a6a6" ColorEnd="#959595"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#4aa6e2" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#53b9fc" ColorEnd="#4aa6e2"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#4a6ee2" StrokeWidth="1.5">
				<Fill Type="LinearGradient" LinearGradientAngle="45" ColorBegin="#537bfc" ColorEnd="#4a6ee2"></Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesList>
			<wijmo:PieChartSeries Label="ゲーム1" Data="7.5">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム2" Data="6.6">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム3" Data="6.3">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム4" Data="3.1">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム5" Data="2.8">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム6" Data="1.5">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム7" Data="1.2">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム8" Data="1.1">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム9" Data="0.9">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム10" Data="0.8">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム11" Data="0.6">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム12" Data="5.4">
			</wijmo:PieChartSeries>
			<wijmo:PieChartSeries Label="ゲーム13" Data="62.2">
			</wijmo:PieChartSeries>
		</SeriesList>
	</wijmo:C1PieChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>このサンプルでは、円グラフのアニメーションをカスタマイズする方法を紹介します。</p>
	<p>下記のプロパティとメソッドを使用すると、円グラフの分割と拡大効果をカスタマイズすることができます。</p>
	<ul>
		<li><strong>Animation.Enabled</strong></li>
		<li><strong>getSector</strong> - 指定したセクターを取得します。</li>
		<li><strong>getOffset</strong> - 水平・垂直方向のオフセットを計算します。</li>
	</ul>
	<p><strong>C1PieChart </strong> では、既定で分割アニメーション効果が設定されています。
    アニメーション効果を無効にするには、<strong> Animation.Enabled</strong> を False に設定する必要があります。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<div>
		アニメーション効果：
		<select id="selAnimation">
			<option selected="selected">拡大</option>
			<option>分割</option>
			<option>なし</option>
		</select>
	</div>
	<script type="text/javascript">
		var duration = 500;
		var radiusOffset = 10;
		var offset = {};
		$(document).ready(function () {
			var piechart = "#<%=C1PieChart1.ClientID %>";
			$(piechart).bind("c1piechartmouseover", function (e, objData) {
				if (objData != null) {
					var animation = $("#selAnimation").val();

					if (animation == "なし") {
						return;
					}

					var series = objData;
					var sector = $(piechart).c1piechart("getSector", series.index);
					var shadow = sector.shadow;
					var tracker = sector.tracker;

					//スケール
					if (animation == "拡大") {
						var center = sector.center;
						sector.animate({
							transform: Raphael.format("s{0},{1},{2},{3}", 1.1, 1.1, sector.center.x, sector.center.y)
						}, duration, "elastic");

						if (shadow) {
							shadow.animate({
								transform: Raphael.format("s{0},{1},{2},{3}", 1.1, 1.1, sector.center.x, sector.center.y)
							}, duration, "elastic");
						}
						if (tracker) {
							tracker.animate({
								transform: Raphael.format("s{0},{1},{2},{3}", 1.1, 1.1, sector.center.x, sector.center.y)
							}, duration, "elastic");
						}
					}
					//分割
					else {
						offset = sector.getOffset(radiusOffset);

						sector.animate({
							translation: offset.x + " " + offset.y
						}, duration, "elastic");

						if (shadow) {
							shadow.animate({
								translation: offset.x + " " + offset.y
							}, duration, "elastic");
						}
						if (tracker) {
							tracker.animate({
								translation: offset.x + " " + offset.y
							}, duration, "elastic");
						}
					}
				}
			});

			$(piechart).bind("c1piechartmouseout", function (e, objData) {
				if (objData != null) {
					var animation = $("#selAnimation").val();

					if (animation == "なし") {
						return;
					}

					var series = objData;
					var sector = $(piechart).c1piechart("getSector", series.index);
					var shadow = sector.shadow;
					var tracker = sector.tracker;

					//スケール
					if (animation == "スケール") {
						sector.animate({
							transform: Raphael.format("s{0},{1},{2},{3}", 1, 1, sector.center.x, sector.center.y)
						}, duration, "elastic");
						if (shadow) {
							shadow.animate({
								transform: Raphael.format("s{0},{1},{2},{3}", 1, 1, sector.center.x, sector.center.y)
							}, duration, "elastic");
						}
						if (tracker) {
							tracker.animate({
								transform: Raphael.format("s{0},{1},{2},{3}", 1, 1, sector.center.x, sector.center.y)
							}, duration, "elastic");
						}
					}
					//分割
					else {
						sector.animate({
							translation: -offset.x + " " + offset.y * -1
						}, duration, "elastic");

						if (shadow) {
							shadow.animate({
								translation: -offset.x + " " + offset.y * -1
							}, duration, "elastic");
						}
						if (tracker) {
							tracker.animate({
								translation: -offset.x + " " + offset.y * -1
							}, duration, "elastic");
						}
						offset = { x: 0, y: 0 };
					}
				}
			});
		});
	</script>
</asp:Content>

