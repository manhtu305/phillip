﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/WijmoSite.Master" CodeBehind="Default.aspx.vb" Inherits="ControlExplorer.Default" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="head">
    <!--jQuery References-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeaderButtons">
    <a runat="server" href="~/Widgets.aspx" class="button">
        <img runat="server" src="~/explore/css/images/dots.png"><span>コンポーネント一覧</span></a>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeaderTitle">
    ホーム</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainContent">
    <section>
        <article class="ui-helper-clearfix">
            <aside>
                <div class="padder">
                    <h3>
                        jQuery ＋<br />Webフォーム<br />コントロール</h3>
   	                    <p>
   	                    ComponentOne for ASP.NET Wijmo は、jQuery と Wijmo 技術をベースに新開発された次世代の ASP.NET コンポーネントセットです。
                        各コンポーネントはサーバー側での開発に加え、非常にリッチなクライアント側 UI を提供します。</p>
                </div>
            </aside>
            <div class="main-content">
                <div class="padder">
                    <h3 class="page-title">
                        おすすめのコンポーネント</h3>
                    <asp:Repeater runat="server" ID="RptFavoriteWidgets">
                        <HeaderTemplate>
                            <ul class="favorite-widgets widget-icons ui-helper-clearfix">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><a runat="server" href='<%# Eval("WidgetHref") %>'>
                                <img runat="server" src='<%# Eval("Icon") %>' alt='<%# Eval("Title") %>' /><%# Eval("Title")%></a></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            <li class="explore-all"><a runat="server" href="~/Widgets.aspx">
                                <img runat="server" src="~/explore/css/images/dots94x94.png" />
                                すべてを表示</a></li>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </article>
    </section>
</asp:Content>
