﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Columns.aspx.vb" Inherits="ControlExplorer.C1GridView.Columns" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        legend
        {
            background-color: transparent;
        }
        </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" ScrollMode="Auto"
                AutoGenerateColumns="false" ShowRowHeader="true" AllowColMoving="true" AllowSorting="true">
                <Columns>
                    <wijmo:C1BoundField DataField="OrderID" HeaderText="注文コード" SortExpression="OrderID" DataFormatString="d"/>
                    <wijmo:C1BoundField DataField="ShipName" HeaderText="出荷先" SortExpression="ShipName" />
                    <wijmo:C1BoundField DataField="ShipCity" HeaderText="出荷都道府県" SortExpression="ShipCity"/>
                    <wijmo:C1Band HeaderText="時刻表">
                        <Columns>
                            <wijmo:C1BoundField DataField="OrderDate" HeaderText="注文日" DataFormatString="d" SortExpression="OrderDate" />
                            <wijmo:C1BoundField DataField="RequiredDate" HeaderText="納入期日" DataFormatString="d" SortExpression="RequiredDate" />
                            <wijmo:C1BoundField DataField="ShippedDate" HeaderText="出荷日" DataFormatString="d" SortExpression="ShippedDate" />
                        </Columns>
                    </wijmo:C1Band>
                </Columns>
            </wijmo:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 10 [OrderID], [ShipName], [ShipCity], [OrderDate], [RequiredDate], [ShippedDate] FROM ORDERS">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
      このサンプルでは、バンドを使用して、複数列ヘッダーを階層構造に整理する方法を示します。
      また、列をソートする方法と、ドラッグ＆ドロップで列を移動する方法も併せて示します。
    </p>

    <p>
       このサンプルでは、次の列プロパティを使用しています。
    </p>
    <ul>
        <li><strong>AllowColMoving</strong> - ドラッグ＆ドロップによる列の移動を許可します。</li>
        <li><strong>AllowSorting</strong> - 列のソートを許可します。</li>
        <li><strong>注文日</strong>、<strong>納入期日</strong>、<strong>出荷日</strong>の各列は、<strong>C1Band</strong> に配置されて、複数階層の列ヘッダーとして整理されます。
        </li>
    </ul>

    <p>
        列ヘッダーをクリックすると、その列でソートされます。
    </p>
    <p>
        また、ドラッグ＆ドロップにより列を移動できます。
        列はターゲット列の右側もしくは左側に挿入でき、挿入位置は列ヘッダーの間に矢印で表されます。
        バンドに列を追加するには、バンドされた列ヘッダーの中央に列をドロップします。
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <fieldset>
                <legend>列の設定</legend>
                <asp:CheckBox ID="CheckBox4" runat="server" AutoPostBack="true" Text="行ヘッダーを表示"
                    Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" />　
                <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" Text="列の移動を許可"
                    Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" />　
                <asp:CheckBox ID="CheckBox3" runat="server" AutoPostBack="true" Text="ソートを許可"
                    Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" />
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
