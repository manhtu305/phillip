﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="GroupRowsCustomStyling.aspx.vb" Inherits="ControlExplorer.C1GridView.GroupRowsCustomStyling" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td
        {
            background-color: #111111;
            color: #FFFFFF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
        AutoGenerateColumns="false" ShowGroupArea="true" AllowColMoving="true" AllowSorting="true">
        <Columns>
            <wijmo:C1BoundField DataField="ProductName" SortExpression="ProductName" HeaderText="製品名" Aggregate="Count">
                <GroupInfo Position="Header" OutlineMode="StartCollapsed" />
            </wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="OrderID" SortExpression="OrderID" HeaderText="注文コード"></wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="Quantity" SortExpression="Quantity" HeaderText="数量" Aggregate="Sum"></wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="Total" SortExpression="Total" HeaderText="合計" Aggregate="Sum" DataFormatString="c"></wijmo:C1BoundField>
        </Columns>
    </wijmo:C1GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 50 Products.ProductName, d.OrderID, d.Quantity, (d.UnitPrice * d.Quantity) as Total FROM Products INNER JOIN (SELECT details.ProductID, details.OrderID, details.UnitPrice, details.Quantity FROM [Order Details] AS details INNER JOIN (SELECT OrderID FROM Orders WHERE Year(OrderDate) = 1994) AS tmp ON details.OrderID = tmp.OrderID) as d ON Products.ProductID = d.ProductID ORDER BY d.ProductID">
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    グループヘッダー行とグループフッター行には、 それぞれ <strong>wijmo-wijgrid-groupheaderrow</strong> と <strong>wijmo-wijgrid-groupfooterrow</strong> の CSS クラスが適用されます。
    グループ行のカスタムスタイルを設定するには、CSS の子孫セレクタを定義する必要があります。
    </p>
    <p>
    このサンプルでは、<strong>.wijmo-wijgrid .wijmo-wijgrid-groupheaderrow td</strong> の CSS シーケンスを使用してグループヘッダー行のスタイルが設定されています。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
