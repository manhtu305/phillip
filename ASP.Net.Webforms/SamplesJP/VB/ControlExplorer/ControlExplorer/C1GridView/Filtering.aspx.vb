Imports C1.Web.Wijmo.Controls.C1GridView

Namespace ControlExplorer.C1GridView
	Public Partial Class Filtering
		Inherits System.Web.UI.Page
		Private flag As Boolean = False

		Protected Sub Filter(sender As Object, e As C1GridViewFilterEventArgs)
			flag = True
		End Sub

		Protected Sub C1GridView1_DataBound(sender As Object, e As EventArgs)
			If flag Then
				C1GridView1.SelectedIndex = 0
			End If
		End Sub
	End Class
End Namespace
