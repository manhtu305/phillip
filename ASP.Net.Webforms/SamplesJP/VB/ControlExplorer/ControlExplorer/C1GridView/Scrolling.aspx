﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Scrolling.aspx.vb" Inherits="ControlExplorer.C1GridView.Scrolling" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        legend
        {
            background-color: transparent;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    
    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" ScrollMode="Both" Height="400px">
            </wijmo:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 30 * FROM ORDERS">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
  <p>
    <strong>C1GridView</strong> は、水平方向および垂直方向の両方のスクロールをサポートしています。
  </p>
  <p>
    <strong>ScrollMode</strong> プロパティが None 以外の値に設定されている場合、スクロールが有効になります。
  </p>
  <p>
     スクロールを使用する場合は、<strong>C1GridView</strong> のヘッダーは自動的に固定されます。
  </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <fieldset>
                <legend>スクロールモードの選択</legend>
                <asp:RadioButtonList ID="rblScrollMode" runat="server" RepeatDirection="Horizontal"
                    AutoPostBack="True" OnSelectedIndexChanged="rblScrollMode_SelectedIndexChanged">
                    <asp:ListItem Value="None">なし&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Auto">自動&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Horizontal">水平&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Vertical">垂直&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Both" Selected="True">両方</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
