Imports System.Data
Imports System.Data.OleDb
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1GridView

Namespace ControlExplorer.C1GridView
	Public Partial Class Editing
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				UpdateView()
			End If
		End Sub

		Protected Function HtmlEncode(value As Object) As String
			Return HttpUtility.HtmlEncode(DirectCast(value, String))
		End Function

		Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)
			Dim col As C1CommandField = DirectCast(C1GridView1.Columns(C1GridView1.Columns.Count - 1), C1CommandField)
			col.ShowEditButton = CheckBox1.Checked
			col.ShowDeleteButton = CheckBox2.Checked
			UpdateView()
			UpdatePanel1.Update()
		End Sub

		Private Sub UpdateView()
			' データを連結します。
			C1GridView1.DataSource = GetDataSet()
			C1GridView1.DataBind()

			If C1GridView1.EditIndex <> -1 Then
				drawEditingSymbol(C1GridView1.EditIndex)
			End If
		End Sub

		Private Function GetDataSet() As DataTable
			Dim orders As DataTable = TryCast(Page.Session("Orders"), DataTable)
			If orders Is Nothing Then
				Using connection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True")
					Using adapter As New OleDbDataAdapter("SELECT [OrderID], [ShipName], [ShipCity], [ShippedDate] FROM ORDERS WHERE [ShippedDate] IS NOT NULL", connection)
						orders = New DataTable()
						adapter.Fill(orders)
						orders.PrimaryKey = New DataColumn() {orders.Columns("OrderID")}
						Page.Session("Orders") = orders
					End Using
				End Using
			End If

			Return orders
		End Function

		Protected Sub C1GridView1_PageIndexChanging(sender As Object, e As C1GridViewPageEventArgs)
			C1GridView1.PageIndex = e.NewPageIndex
			UpdateView()
		End Sub

		Protected Sub C1GridView1_RowEditing(sender As Object, e As C1GridViewEditEventArgs)
			C1GridView1.EditIndex = e.NewEditIndex
			UpdateView()
		End Sub

		Protected Sub C1GridView1_RowCancelingEdit(sender As Object, e As C1GridViewCancelEditEventArgs)
			C1GridView1.EditIndex = -1
			UpdateView()
		End Sub

		Protected Sub C1GridView1_RowUpdating(sender As Object, e As C1GridViewUpdateEventArgs)
			' グリッドを連結します。
			C1GridView1.DataSource = GetDataSet()

			' データソースを更新します。
			C1GridView1.Update()

			' 現在のアイテムに対して編集モードをリセットします。
			C1GridView1.EditIndex = -1

			' グリッドをリフレッシュします。
			UpdateView()
		End Sub

		Protected Sub C1GridView1_RowDeleting(sender As Object, e As C1GridViewDeleteEventArgs)
			Dim orders As DataTable = GetDataSet()
			Dim row As DataRow = orders.Rows.Find(C1GridView1.DataKeys(e.RowIndex).Value)

			If row IsNot Nothing Then
				orders.Rows.Remove(row)
				orders.AcceptChanges()
				UpdateView()
			Else
				Throw New RowNotInTableException()
			End If
		End Sub

		Protected Sub drawEditingSymbol(rowIndex As Integer)
			Dim img As New Image()
			img.ImageUrl = "Images/editing.gif"
			C1GridView1.Rows(rowIndex).Cells(0).HorizontalAlign = HorizontalAlign.Center
			C1GridView1.Rows(rowIndex).Cells(0).Controls.Add(img)
		End Sub
	End Class
End Namespace
