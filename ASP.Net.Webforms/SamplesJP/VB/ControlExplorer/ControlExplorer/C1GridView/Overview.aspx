﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1GridView.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
        AutoGenerateColumns="false" ShowRowHeader="true" AllowSorting="true" CallbackSettings-Action="All">
        <Columns>
            <wijmo:C1BoundField HeaderText="社員コード" DataField="EmployeeID" SortExpression="EmployeeID" />
            <wijmo:C1BoundField HeaderText="名字" DataField="LastName" SortExpression="LastName" />
            <wijmo:C1BoundField HeaderText="名前" DataField="FirstName" SortExpression="FirstName" />
            <wijmo:C1BoundField HeaderText="生年月日" DataField="BirthDate" DataFormatString="d" SortExpression="BirthDate" />
        </Columns>
    </wijmo:C1GridView>
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True" ProviderName="System.Data.OleDb"
        SelectCommand="SELECT [EmployeeID], [LastName], [FirstName], [BirthDate] FROM [Employees]">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1GridView</strong> は、データソースの値を表形式で表示します。各列はフィールドを、各行はレコードを表します。
    </p>
    <p>
        このサンプルでは、ソート可能な単純なグリッドを表示します。  
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
