﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Paging.aspx.vb" Inherits="ControlExplorer.C1GridView.Paging" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        legend
        {
            background-color: transparent;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManger1" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
                AutoGenerateColumns="true" ShowRowHeader="true" AllowPaging="true" PageSize="10" CallbackSettings-Action="All">
            </wijmo:C1GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT [OrderID] AS 注文コード, [ShipName] AS 出荷先名, [ShipCity] AS 出荷都道府県, [ShippedDate] AS 出荷日 FROM ORDERS">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
  <p>
    <strong>AllowPaging</strong> プロパティを True に設定すると、ページングが有効になります。
  </p>

  <p>
    ページングに関連するプロパティは次の通りです。
  </p>
  <ul>
    <li><strong>PageSize</strong> - 単一のページ上に配置する行数を設定します。</li>
    <li><strong>PagerSettings</strong> - 位置やモードなど、ページャの外観を設定します。</li>
  </ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <fieldset>
                <legend>ページングモードの選択</legend>
                <asp:RadioButtonList ID="RblMode" runat="server" Height="12px" AutoPostBack="True"
                    RepeatDirection="Horizontal" OnSelectedIndexChanged="RblMode_SelectedIndexChanged">
                    <asp:ListItem Value="Numeric" Selected="True">ページ番号&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="NumericFirstLast">ページ番号＋最初／最後&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="NextPrevious">前後&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="NextPreviousFirstLast">前後＋最初／最後</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
            <fieldset>
                <legend>位置を選択</legend>
                <asp:RadioButtonList ID="RblPosition" runat="server" Height="12px" AutoPostBack="True"
                    RepeatDirection="Horizontal" OnSelectedIndexChanged="RblPosition_SelectedIndexChanged">
                    <asp:ListItem Value="Top">上&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="Bottom" Selected="True">下&nbsp;&nbsp;</asp:ListItem>
                    <asp:ListItem Value="TopAndBottom">上下</asp:ListItem>
                </asp:RadioButtonList>
            </fieldset>
            <fieldset>
                <legend>ページサイズを設定</legend>
                <label for="TxtPageSize">ページのサイズ：</label>
                <asp:TextBox ID="TxtPageSize" runat="server" Text="10"></asp:TextBox>
                <a href="#" id="c1apply">
                <asp:Button runat="server" ID="btnApply" Text="適用" UseSubmitBehavior="false" OnClick="btnApply_Click" />
                </a>
            </fieldset>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
