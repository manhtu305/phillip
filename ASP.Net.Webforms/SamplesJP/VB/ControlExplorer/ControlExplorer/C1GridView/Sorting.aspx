﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Sorting.aspx.vb" Inherits="ControlExplorer.C1GridView.Sorting" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="false" AllowSorting="true">
		<CallbackSettings Action="Sorting" />
		<Columns>
			<wijmo:C1BoundField DataField="ProductName" SortExpression="ProductName" HeaderText="商品名" />
			<wijmo:C1BoundField DataField="OrderID" SortExpression="OrderID" HeaderText="注文コード" />
			<wijmo:C1BoundField DataField="Quantity" SortExpression="Quantity" HeaderText="数量" />
			<wijmo:C1BoundField DataField="Total" SortExpression="Total" SortDirection="Descending" HeaderText="合計" DataFormatString="c" />
		</Columns>
	</wijmo:C1GridView>

	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 15 Products.ProductName, d.OrderID, d.Quantity, (d.UnitPrice * d.Quantity) as Total FROM Products INNER JOIN (SELECT details.ProductID, details.OrderID, details.UnitPrice, details.Quantity FROM [Order Details] AS details INNER JOIN (SELECT OrderID FROM Orders WHERE Year(OrderDate) = 1994) AS tmp ON details.OrderID = tmp.OrderID) as d ON Products.ProductID = d.ProductID ORDER BY d.ProductID">
	</asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		<strong>C1GridView</strong> は、ソートをサポートしています。
	</p>
	<p>
        <strong>AllowSorting</strong> プロパティを True に設定すると、ソートが許可されます。
		列の <strong>SortExpression</strong> プロパティが空でない値に設定されている場合、その列のソートが許可されます。
	</p>

	<p>このサンプルでは、次のプロパティを使用しています。</p>
	<ul>
		<li><strong>AllowSorting</strong> - ソート機能を有効にします。</li>
		<li><strong>SortExpression</strong> - ソートに使用されるデータフィールド名を指定します。</li>
		<li><strong>SortDirection</strong> - ソート方向を指定します。</li>
	</ul>

	<p>列ヘッダーをクリックすると、その列でソートが実行されます。</p>
</asp:Content>
