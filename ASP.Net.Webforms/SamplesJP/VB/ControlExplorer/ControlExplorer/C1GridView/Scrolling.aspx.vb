Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1GridView

Namespace ControlExplorer.C1GridView
	Public Partial Class Scrolling
		Inherits System.Web.UI.Page
		Protected Sub rblScrollMode_SelectedIndexChanged(sender As Object, e As EventArgs)
			Select Case rblScrollMode.SelectedValue
				Case "None"
					C1GridView1.ScrollMode = ScrollMode.None
					Exit Select

				Case "Auto"
					C1GridView1.ScrollMode = ScrollMode.Auto
					Exit Select

				Case "Horizontal"
					C1GridView1.ScrollMode = ScrollMode.Horizontal
					Exit Select

				Case "Vertical"
					C1GridView1.ScrollMode = ScrollMode.Vertical
					Exit Select

				Case "Both"
					C1GridView1.ScrollMode = ScrollMode.Both
					Exit Select
			End Select

			C1GridView1.Width = Unit.Empty
			C1GridView1.Height = Unit.Empty

			If C1GridView1.ScrollMode <> ScrollMode.None Then
				C1GridView1.Width = New Unit(50, UnitType.Em)
				C1GridView1.Height = New Unit(40, UnitType.Em)
			End If

			UpdatePanel1.Update()
		End Sub
	End Class
End Namespace
