Imports System.Collections
Imports System.Data
Imports System.Data.OleDb
Imports System.Web.UI

Namespace ControlExplorer.C1GridView
	Public Partial Class Editing1
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				UpdateView()
			End If
		End Sub

		Protected Sub C1GridView1_RowUpdating(sender As Object, e As C1.Web.Wijmo.Controls.C1GridView.C1GridViewUpdateEventArgs)
			Dim orders As DataTable = GetDataSet()
			Dim row As DataRow = orders.Rows.Find(C1GridView1.DataKeys(e.RowIndex).Value)

			If row IsNot Nothing Then
				For Each entry As DictionaryEntry In e.NewValues
					row(DirectCast(entry.Key, String)) = entry.Value
				Next
			Else
				Throw New RowNotInTableException()
			End If
		End Sub

		Protected Sub C1GridView1_EndRowUpdated(sender As Object, e As C1.Web.Wijmo.Controls.C1GridView.C1GridViewEndRowUpdatedEventArgs)
			GetDataSet().AcceptChanges()
			UpdateView()
		End Sub

		Protected Sub C1GridView1_RowDeleting(sender As Object, e As C1.Web.Wijmo.Controls.C1GridView.C1GridViewDeleteEventArgs)
			Dim orders As DataTable = GetDataSet()
			Dim row As DataRow = orders.Rows.Find(C1GridView1.DataKeys(e.RowIndex).Value)

			If row IsNot Nothing Then
				orders.Rows.Remove(row)
				orders.AcceptChanges()
				UpdateView()
			Else
				Throw New RowNotInTableException()
			End If
		End Sub

		Private Function GetDataSet() As DataTable
			Dim orders As DataTable = TryCast(Page.Session("ClinetOrders"), DataTable)
			If orders Is Nothing Then
				Using connection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True")
					Using adapter As New OleDbDataAdapter("SELECT TOP 10 [OrderID], [ShipName], [ShipCity], [ShippedDate] FROM ORDERS WHERE [ShippedDate] IS NOT NULL", connection)
						orders = New DataTable("Orders")
						adapter.Fill(orders)
						orders.PrimaryKey = New DataColumn() {orders.Columns("OrderID")}
						Page.Session("ClinetOrders") = orders
					End Using
				End Using
			End If

			Return orders
		End Function

		Private Sub UpdateView()
            AddHandler C1GridView1.RowDeleting, AddressOf C1GridView1_RowDeleting
			C1GridView1.DataSource = GetDataSet()
			C1GridView1.DataBind()
		End Sub
	End Class
End Namespace
