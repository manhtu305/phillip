﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Filtering.aspx.vb" Inherits="ControlExplorer.C1GridView.Filtering" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .selectedRow > td
        {
            font-weight: bold !important;
        }
    </style>
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
          
            <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="false"
                DataKeyNames="CustomerID" AutoGenerateSelectButton="true" SelectedIndex="0" ShowFilter="true" ClientSelectionMode="None"
                OnFiltering="Filter" OnDataBound="C1GridView1_DataBound">
                <Columns>
                    <wijmo:C1BoundField DataField="CompanyName" HeaderText="会社名" FilterOperator="Contains" />
                    <wijmo:C1BoundField DataField="City" HeaderText="都道府県" FilterOperator="Contains"/>
                    <wijmo:C1BoundField DataField="Quantity" HeaderText="数量" FilterOperator="Equals" />
                </Columns>
                <SelectedRowStyle CssClass="selectedRow" BackColor="Orange" />
            </wijmo:C1GridView>
                   
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 10 Customers.CustomerID, CompanyName, City, Count(*) as QUANTITY FROM Customers INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID GROUP BY Customers.CustomerID, CompanyName, City">
    </asp:SqlDataSource>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1GridView</strong> は、グリッド内の行をフィルタリングするための直感的なユーザーインタフェースをサポートしています。
    </p>
    <p>
        次の列のプロパティを設定すると、フィルタリングを実現できます。
    </p>
    <ul>
      <li><strong>FilterOperator</strong> - フィルタリングの種別</li>
      <li><strong>FilterValue</strong> - フィルタリングに使用される値</li>
    </ul>

    <p>
        <strong>ShowFilter</strong> プロパティを True に設定すると、フィルタリング操作が許可されます。
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
