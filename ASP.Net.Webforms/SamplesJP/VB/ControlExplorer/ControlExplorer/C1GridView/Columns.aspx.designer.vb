'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1GridView


	Public Partial Class Columns

		''' <summary>
		''' ScriptManager1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected ScriptManager1 As Global.System.Web.UI.ScriptManager

		''' <summary>
		''' UpdatePanel1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdatePanel1 As Global.System.Web.UI.UpdatePanel

		''' <summary>
		''' C1GridView1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1GridView1 As Global.C1.Web.Wijmo.Controls.C1GridView.C1GridView

		''' <summary>
		''' SqlDataSource1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource

		''' <summary>
		''' UpdatePanel2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdatePanel2 As Global.System.Web.UI.UpdatePanel

		''' <summary>
		''' CheckBox4 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected CheckBox4 As Global.System.Web.UI.WebControls.CheckBox

		''' <summary>
		''' CheckBox1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected CheckBox1 As Global.System.Web.UI.WebControls.CheckBox

		''' <summary>
		''' CheckBox3 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected CheckBox3 As Global.System.Web.UI.WebControls.CheckBox
	End Class
End Namespace
