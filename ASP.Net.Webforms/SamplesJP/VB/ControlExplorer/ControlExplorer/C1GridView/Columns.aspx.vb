
Namespace ControlExplorer.C1GridView
	Public Partial Class Columns
		Inherits System.Web.UI.Page
		Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)
			C1GridView1.AllowColMoving = CheckBox1.Checked
			C1GridView1.AllowSorting = CheckBox3.Checked
			C1GridView1.ShowRowHeader = CheckBox4.Checked
			UpdatePanel1.Update()
		End Sub
	End Class
End Namespace
