<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="GroupArea.aspx.vb" Inherits="ControlExplorer.C1GridView.GroupArea" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
        AutoGenerateColumns="false" ShowGroupArea="true" AllowColMoving="true" AllowSorting="true">
        <Columns>
            <wijmo:C1BoundField DataField="ProductName" SortExpression="ProductName" HeaderText="商品名" Aggregate="Count">
                <GroupInfo Position="Header" OutlineMode="StartCollapsed" />
            </wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="OrderID" SortExpression="OrderID" HeaderText="注文コード"></wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="Quantity" SortExpression="Quantity" HeaderText="数量" Aggregate="Sum"></wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="Total" SortExpression="Total" HeaderText="合計" Aggregate="Sum" DataFormatString="c"></wijmo:C1BoundField>
        </Columns>
    </wijmo:C1GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 50 Products.ProductName, d.OrderID, d.Quantity, (d.UnitPrice * d.Quantity) as Total FROM Products INNER JOIN (SELECT details.ProductID, details.OrderID, details.UnitPrice, details.Quantity FROM [Order Details] AS details INNER JOIN (SELECT OrderID FROM Orders WHERE Year(OrderDate) = 1994) AS tmp ON details.OrderID = tmp.OrderID) as d ON Products.ProductID = d.ProductID ORDER BY d.ProductID">
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1GridView</strong> は、1 つまたは複数の列に対するデータのグループ化をサポートしています。
    </p>
    <p>
        <strong>GroupInfo.Position</strong> プロパティが None 以外の値に設定されている場合、列がグループ化されます。
    </p>
    <p>
        また、次のプロパティが True に設定されている場合のみ、実行時のグループ化が可能になります。
    </p>
    <ul>
        <li><strong>ShowGroupArea</strong> - グループ化領域を表示します。</li>
        <li><strong>AllowColMoving</strong> - 実行時に列をドラッグできるようにします。</li>
    </ul>
    
    <p>
        エンドユーザーは、グループ化領域に列ヘッダーをドラッグしてデータをグループ化することができます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
