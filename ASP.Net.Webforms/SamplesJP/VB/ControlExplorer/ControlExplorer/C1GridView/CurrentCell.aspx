﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CurrentCell.aspx.vb" Inherits="ControlExplorer.C1GridView.CurrentCell" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function currentCellChanging(e, args) {
            var value = "(" + args.oldCellIndex + "," + args.oldRowIndex + ") -> (" +
                args.cellIndex + "," + args.rowIndex + ")";

            writeLog(value);
        }

        function currentCellChanged(e) {
            var curCell = $(e.target).c1gridview("currentCell");

            $("#currentValue").html((curCell) ? encode(curCell.value().toLocaleString()) : "");
        }

        var log;
        function writeLog(value) {
            if (!log) {
                log = $("#log")[0];
            }

            if (log) {
                if (log.size < log.options.length + 1) {
                    log.options.length = 0;
                }
            }

            log.options[log.options.length] = new Option(value, "", false, false);
        }

        function encode(text) {
            var chars = Array("&", "<", ">", '"', "'");
            var replacements = Array("&amp;", "&lt;", "&gt;", "&quot;", "'");
            for (var i = 0; i < chars.length; i++) {
                var re = new RegExp(chars[i], "gi");
                if (re.test(text)) {
                    text = text.replace(re, replacements[i]);
                }
            }
            return text;
        }
    </script>

    <table>
        <colgroup>
            <col />
            <col width="10px" />
            <col />
        </colgroup>
        <tr>
            <td style="vertical-align: top">
                <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" AllowKeyboardNavigation="true"
                    AutoGenerateColumns="false" ShowRowHeader="true" HighlightCurrentCell="true"
                     OnClientCurrentCellChanging="currentCellChanging" OnClientCurrentCellChanged="currentCellChanged">
                    <Columns>
                        <wijmo:C1BoundField DataField="OrderID" HeaderText="注文コード" />
                        <wijmo:C1BoundField DataField="ShipName" HeaderText="出荷先" />
                        <wijmo:C1BoundField DataField="ShipCity" HeaderText="出荷都道府県" />
                        <wijmo:C1BoundField DataField="ShippedDate" HeaderText="出荷日" DataFormatString="d" />
                    </Columns>
                </wijmo:C1GridView>
            </td>
            <td></td>
            <td style="vertical-align: top">
                <p>
                    <label for="log">OnCurrentCellChanging イベントログ：</label>
                    <select multiple="multiple" id="log" size="6" style="width: 200px"></select>
                </p>
                <p>
                    カレントセル値：
                    <span id="currentValue" />
                </p>
            </td>
        </tr>
    </table>
    

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 10 [OrderID], [ShipName], [ShipCity], [ShippedDate] FROM ORDERS">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1GridView</strong> はカレントセル上の操作をサポートしています。カレントセルは、フォーカスを持つセルです。
        <strong>C1GridView</strong> はカレントセルの位置を強調表示して変更を追跡し、データ値を取得することができます。
    </p>

    <p>
       このサンプルは、次のプロパティを使用しています。
    </p>
    <ul>
        <li><strong>AllowKeyboardNavigation</strong> - キーボードによるカレントセルの位置変更を可能にします。</li>
        <li><strong>HighlightCurrentCell</strong> - カレントセルの位置を強調表示します。</li>
    </ul>

    <p>
       また、次のクライアント側イベントハンドラを使用しています。
    </p>
    <ul>
        <li><strong>OnClientCurrentCellChanging</strong> - カレントセルの位置を追跡してログに書き込みます。</li>
        <li><strong>OnClientCurrentCellChanged</strong> - カレントセルのデータ値を表示します。</li>
    </ul>

    <p>
       <strong>AllowKeyboardNavigation</strong> プロパティが False に設定されている場合、エンドユーザーはセルをクリックしてカレントセルの位置を変更できます。
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
