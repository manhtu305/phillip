﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomEditing.aspx.vb" Inherits="ControlExplorer.C1GridView.Editing" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<style type="text/css">
		legend
		{
			background-color: transparent;
		}
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>

	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
				<wijmo:C1GridView ID="C1GridView1" runat="server" AllowPaging="True" PageSize="10"
					DataKeyNames="OrderID" AutoGenerateColumns="False"
					OnPageIndexChanging="C1GridView1_PageIndexChanging"
					OnRowCancelingEdit="C1GridView1_RowCancelingEdit" OnRowEditing="C1GridView1_RowEditing"
					OnRowUpdating="C1GridView1_RowUpdating" OnRowDeleting="C1GridView1_RowDeleting">
					<PagerSettings Mode="NextPreviousFirstLast" />
					<Columns>
						<wijmo:C1TemplateField>
							<ItemStyle Width="15px" />
						</wijmo:C1TemplateField>
						<wijmo:C1BoundField DataField="OrderID" HeaderText="ID" ReadOnly="true" DataFormatString="d" />
					   
						<wijmo:C1TemplateField HeaderText="商品名">
							<UpdateBindings>
								<wijmo:C1GridViewUpdateBinding ControlProperty="tbShipName.Text" UpdateField="ShipName" />
							</UpdateBindings>
							<EditItemTemplate>
								<asp:TextBox ID="tbShipName" runat="server" Text='<%# Bind("ShipName") %>' BorderStyle="None" Width="100%" />
							</EditItemTemplate>
							<ItemTemplate>
								<%# HtmlEncode(Eval("ShipName")) %>
							</ItemTemplate>
						</wijmo:C1TemplateField>
						 
						<wijmo:C1TemplateField HeaderText="出荷都道府県">
							<UpdateBindings>
								<wijmo:C1GridViewUpdateBinding ControlProperty="tbShipCity.Text" UpdateField="ShipCity" />
							</UpdateBindings>
							<EditItemTemplate>
								<asp:TextBox ID="tbShipCity" runat="server" Text='<%# Bind("ShipCity") %>' BorderStyle="None" Width="100%" />
							</EditItemTemplate>
							<ItemTemplate>
								<%# HtmlEncode(Eval("ShipCity")) %>
							</ItemTemplate>
						</wijmo:C1TemplateField>
						<wijmo:C1CommandField ShowEditButton="True" ShowDeleteButton="false" />
					</Columns>
				</wijmo:C1GridView>
	   
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	<strong>C1GridView</strong> は、サーバー側の編集をサポートしています。
    このサンプルでは、カスタムデータ連結を使用して、セッションオブジェクトに格納されているデータセットに <strong>C1GridView</strong> を連結するする方法を示します。
    また、カスタムエディタを実装するためにテンプレートフィールドを使用する方法と、テンプレート列の <strong>UpdateBindings</strong> コレクションを使用して <strong>Update</strong> メソッドで自動的にデータセットを更新するための方法を示します。
	</p>

	<p>
	<strong>C1GridView</strong> のサーバー側の編集は行を基準にしいます。
    <strong>EditIndex</strong> プロパティで指定される 1 つのデータ行のみが編集モードになります。
	</p>

	<p>
	   このサンプルは、次の機能を実現します。
	</p>
	<ul>
	  <li><strong>テンプレートフィールド</strong> - カスタムエディタを実装するために使用します。
      ItemTemplate プロパティと HeaderTemplate プロパティは、表示／編集するセルコンテンツを指定します。</li>
	  <li><strong>更新の連結</strong> - テンプレート列のエディタをデータフィールドにリンクします。
      これにより、カスタムデータ連結の使用時に、<strong>Update</strong> メソッドを使用して自動的にデータセットを更新することができます。</li>
	  <li><strong>C1CommandField</strong> 列の <strong>ShowEditButton</strong>／<strong>ShowDeleteButton</strong> プロパティ - 
	  各データ行にEdit、Cancel、Update、Delete の各ボタンを追加します。ボタンをクリックするとデータを編集できます。</li>
	</ul>

	<p>
	   このサンプルは、次のイベントハンドラを使用します。
	</p>
	<ul>
		<li><strong>RowEditing</strong> - 行の［編集］ボタンがクリックされたときに、行を編集モードにします。</li>
		<li><strong>RowCancelingEdit</strong> - 行の［キャンセル］ボタンがクリックされたときに、行の編集をキャンセルします。</li>
		<li><strong>RowUpdating</strong> - 行の［更新］ボタンがクリックされたときに、更新内容をデータセットに反映して、編集行を通常モードにします。</li>
		<li><strong>RowDeleting</strong> - 行の［削除］ボタンがクリックされたときに、行を削除します。</li>
	</ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
		<ContentTemplate>
			<fieldset>
				<legend>編集列の設定</legend>
				<div>
					<div>
						<asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" Text="Update ボタンを表示"
							Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" />
					</div>
					<div>
						<asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="true" Text="Delete ボタンを表示"
							Checked="false" OnCheckedChanged="CheckBox1_CheckedChanged" />
					</div>
				</div>
			</fieldset>
		</ContentTemplate>
	</asp:UpdatePanel>

</asp:Content>
