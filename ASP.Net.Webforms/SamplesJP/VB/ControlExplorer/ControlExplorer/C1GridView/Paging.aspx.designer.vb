'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1GridView


	Public Partial Class Paging

		''' <summary>
		''' ScriptManger1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected ScriptManger1 As Global.System.Web.UI.ScriptManager

		''' <summary>
		''' UpdatePanel1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdatePanel1 As Global.System.Web.UI.UpdatePanel

		''' <summary>
		''' C1GridView1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1GridView1 As Global.C1.Web.Wijmo.Controls.C1GridView.C1GridView

		''' <summary>
		''' SqlDataSource1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource

		''' <summary>
		''' UpdatePanel2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdatePanel2 As Global.System.Web.UI.UpdatePanel

		''' <summary>
		''' RblMode コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected RblMode As Global.System.Web.UI.WebControls.RadioButtonList

		''' <summary>
		''' RblPosition コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected RblPosition As Global.System.Web.UI.WebControls.RadioButtonList

		''' <summary>
		''' TxtPageSize コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected TxtPageSize As Global.System.Web.UI.WebControls.TextBox

		''' <summary>
		''' btnApply コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected btnApply As Global.System.Web.UI.WebControls.Button
	End Class
End Namespace
