
Imports C1.Web.Wijmo.Controls.C1Pager
Imports C1.Web.Wijmo.Controls.C1GridView

Namespace ControlExplorer.C1GridView
	Public Partial Class Paging
		Inherits System.Web.UI.Page
		Protected Sub RblMode_SelectedIndexChanged(sender As Object, e As EventArgs)
			' 新しい設定を適用します。
			Select Case RblMode.SelectedItem.Value
				Case "Numeric"
					C1GridView1.PagerSettings.Mode = PagerMode.Numeric
					Exit Select
				Case "NextPrevious"
					C1GridView1.PagerSettings.Mode = PagerMode.NextPrevious
					Exit Select
				Case "NextPreviousFirstLast"
					C1GridView1.PagerSettings.Mode = PagerMode.NextPreviousFirstLast
					Exit Select
				Case "NumericFirstLast"
					C1GridView1.PagerSettings.Mode = PagerMode.NumericFirstLast
					Exit Select
			End Select
			UpdatePanel1.Update()
		End Sub

		Protected Sub RblPosition_SelectedIndexChanged(sender As Object, e As EventArgs)
			'  新しい設定を適用します。
			Select Case RblPosition.SelectedItem.Value
				Case "Top"
					C1GridView1.PagerSettings.Position = PagerPosition.Top
					Exit Select
				Case "Bottom"
					C1GridView1.PagerSettings.Position = PagerPosition.Bottom
					Exit Select
				Case "TopAndBottom"
					C1GridView1.PagerSettings.Position = PagerPosition.TopAndBottom
					Exit Select
			End Select
			UpdatePanel1.Update()
		End Sub

		Protected Sub btnApply_Click(sender As Object, e As EventArgs)
			Dim pageSize As Double
			If [Double].TryParse(TxtPageSize.Text, pageSize) Then
				C1GridView1.PageSize = CInt(Math.Truncate(pageSize))
			End If
			UpdatePanel1.Update()
		End Sub
	End Class
End Namespace
