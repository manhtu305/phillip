﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ClientSideSelection.aspx.vb" Inherits="ControlExplorer.C1GridView.ClientSideSelection" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        #demoTable > tbody > tr > td
        {
            vertical-align: top;
            width: 350px;
        }
        
        #logTable
        {
            table-layout: fixed;
            margin-left: 20px;
            width: 100%;
        }
    </style>

    <script type="text/javascript">
        var flag = false;

        function changeClientSelectionMode(value) {
            $('#<%= C1GridView1.ClientID %>').c1gridview("option", "clientSelectionMode", value);
        }

        function onClientSelectionChanged() {
            ensureLogTable();

            var selectedCells = $('#<%= C1GridView1.ClientID %>').c1gridview("selection").selectedCells(),
                log = $("#logTable"),
                i, cellInfo;

            log
                .find("td")
                .removeClass("ui-state-highlight")
                .html("&nbsp");

            for (i = 0; i < selectedCells.length(); i++) {
                cellInfo = selectedCells.item(i);

                $(log[0].tBodies[0].rows[cellInfo.rowIndex()].cells[cellInfo.cellIndex()])
                    .addClass("ui-state-highlight")
                    .text(cellInfo.value().toString());
            }
        }

        function ensureLogTable() {
            if (!flag) {
                var source = document.getElementById("<%= C1GridView1.ClientID %>").tBodies[0],
                    target = document.getElementById("logTable").tBodies[0],
                    i, j, row;

                for (i = 0; i < source.rows.length; i++) {
                    var row = target.insertRow(-1);
                    for (j = 0; j < source.rows[i].cells.length; j++) {
                        row.insertCell(-1).innerHTML = "&nbsp;";
                    }
                }
            }

            flag = true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    
    <table id="demoTable">
        <tr>
            <td>
                <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
                    AutoGenerateColumns="false" ClientSelectionMode="SingleRow" OnClientSelectionChanged="onClientSelectionChanged">
                    <Columns>
                        <wijmo:C1BoundField DataField="OrderID" HeaderText="ID" />
                        <wijmo:C1BoundField DataField="ShipName" HeaderText="出荷先" />
                        <wijmo:C1BoundField DataField="ShipCity" HeaderText="出荷都道府県" />
                    </Columns>
                </wijmo:C1GridView>
            </td>
            <td>
                 <table class="ui-widget ui-widget-content" rules="all" id="logTable">
                     <caption class="ui-widget-header">選択ログ</caption>
                     <colgroup>
                        <col width="50" />
                        <col width="200" />
                        <col />
                     </colgroup>
                     <tbody>
                     </tbody>
                 </table>
            </td>
        </tr>
    </table>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 10 [OrderID], [ShipName], [ShipCity] FROM ORDERS">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1GridView</strong> は、様々なクライアント側のセル選択モードをサポートしています。
        このサンプルでは、クライアント側オブジェクトモデルを使用して、サーバーに要求を送信することなく選択モードを変更する方法を示します。
    </p>
    <p>
        <strong>ClientSelectionMode</strong> プロパティが None 以外の値に設定されると、クライアント側の選択が許可されます。
    </p>
    <p>
        選択モードを指定し、1 つまたは複数の行、列、セルを選択して動作を確認してください。
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <label for="ddSelectionMode">選択モード：</label>
    <select id="ddSelectionMode" onchange="changeClientSelectionMode(this.value)" >
        <option value="none">なし</option>
        <option value="singleCell">単一セル</option>
        <option value="singleColumn">単一列</option>
        <option value="singleRow" selected="selected">単一行</option>
        <option value="singleRange">単一の範囲</option>
        <option value="multiColumn">複数列</option>
        <option value="multiRow">複数行</option>
        <option value="multiRange">複数の範囲</option>
    </select>
</asp:Content>
