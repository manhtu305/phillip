﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Aggregates.aspx.vb" Inherits="ControlExplorer.C1GridView.Aggregates" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    </script>

    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    
    <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
        AutoGenerateColumns="false">
        <Columns>
            <wijmo:C1BoundField DataField="ProductName" HeaderText="商品名" Aggregate="Count">
                <GroupInfo Position="Header" OutlineMode="StartCollapsed" />
            </wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="OrderID" HeaderText="注文コード"></wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="Quantity" HeaderText="数量" Aggregate="Sum"></wijmo:C1BoundField>
            <wijmo:C1BoundField DataField="Total" HeaderText="合計" Aggregate="Sum" DataFormatString="c"></wijmo:C1BoundField>
        </Columns>
    </wijmo:C1GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 50 Products.ProductName, d.OrderID, d.Quantity, (d.UnitPrice * d.Quantity) as Total FROM Products INNER JOIN (SELECT details.ProductID, details.OrderID, details.UnitPrice, details.Quantity FROM [Order Details] AS details INNER JOIN (SELECT OrderID FROM Orders WHERE Year(OrderDate) = 1994) AS tmp ON details.OrderID = tmp.OrderID) as d ON Products.ProductID = d.ProductID ORDER BY d.ProductID">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1GridView</strong> は、1 つまたは複数のグループ化された列に対する集計演算をサポートしています。
    </p>
    <p>
        データ行のグループごとに集計値を計算することが可能です。
        <strong>C1GridView</strong> は、合計、件数、最小、最大などの集計関数をサポートしています。
   </p>

    <p>
         このサンプルは、次の列プロパティを使用しています。
    </p>
    <ul>
        <li><strong>GroupInfo.Position</strong> - 商品名の列でグループ化しています。</li>
        <li><strong>Aggregate</strong> - 商品名での集計値を計算する為に、数量と合計の各列に設定しています。</li>
    </ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
