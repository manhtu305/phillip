﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="VirtualScrolling.aspx.vb" Inherits="ControlExplorer.C1GridView.VirtualScrolling" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    
    <wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" ScrollMode="Both" Height="400px" AllowVirtualScrolling="true" PageSize="10">
		<CallbackSettings Action="Scrolling" />
    </wijmo:C1GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DATADIRECTORY|\Nwind_ja.mdb;Persist Security Info=True"
        ProviderName="System.Data.OleDb" SelectCommand="SELECT * FROM ORDERS">
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
  <p>
    <strong>C1GridView</strong> は、大量のデータが使用されている際のレンダリング速度を上げる為の仮想スクロール機能をサポートしています。

	<p>
         このサンプルでは次のプロパティを使用しています。
    </p>
    <ul>
        <li><strong>AllowVirtualScrolling</strong> - 仮想スクロールを有効にします。</li>
        <li><strong>CallbackSettings.Action</strong> - 仮想スクロールモードで、新しいデータ部分をコールバックで送信します。（このモードでは列のグルーピングなどがサポートされません。）</li>
        <li><strong>PageSize</strong> - 一度にレンダリングする行数を指定します。</li>
    </ul>
  </p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
