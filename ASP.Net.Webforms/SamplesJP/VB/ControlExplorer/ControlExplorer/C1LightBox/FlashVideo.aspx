﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FlashVideo.aspx.vb" Inherits="ControlExplorer.C1LightBox.FlashVideo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" TextPosition="Outside" 
        ControlsPosition="Outside" ShowCounter="false" 
        FlashInstall="player/expressInstall.swf" FlvPlayer="player/player.swf">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" 
            ImageUrl="http://cdn.wijmo.com/images/flash.png" 
            LinkUrl="http://cdn.wijmo.com/movies/racing.flv" Title="レーシング" 
            Text="レーシング"/>
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルは C1LightBox での Flash ビデオの表示方法を示します。
</p>
<p>
<b>FlvPlayer</b> プロパティは、Flash ビデオプレイヤーの名前とパスを示します。
このサンプルでは、player フォルダ内の JW Player (player.swf) を使用しています。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
