﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="WMP.aspx.vb" Inherits="ControlExplorer.C1LightBox.WMP" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Wmp" MaxWidth="600" MaxHeight="450"  TextPosition="Outside" ControlsPosition="Outside" >
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Movie" Text="Movie played by Windows Media Player" 
			ImageUrl="~/C1LightBox/images/small/mediaplayer.png" 
			LinkUrl="~/C1LightBox/movies/sample.mpeg" />
	</Items>
</wijmo:C1LightBox>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
C1LightBox は Windows Media Player で再生可能なメディアのホスティングをサポートします。
</p>
<p>
URLが以下の拡張子を持つファイルである場合、既定で Windows Media Player オブジェクトを使用して再生されます。
</p>
<p>
.asf、.avi、.mpg、.mpeg、.wm、.wmv
</p>
<p>
<b>Player</b> プロパティを Wmp に設定すると、LightBox で 明示的に Windows Media Player を使用するように強制することができます。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
