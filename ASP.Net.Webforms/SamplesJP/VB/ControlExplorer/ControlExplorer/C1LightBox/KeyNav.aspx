﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="KeyNav.aspx.vb" Inherits="ControlExplorer.C1LightBox.KeyNav" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" KeyNav="true">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="スポーツ1" Text="スポーツ1"
            ImageUrl="http://lorempixum.com/120/90/sports/1" 
            LinkUrl="http://lorempixum.com/600/400/sports/1" />
        <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="スポーツ2" Text="スポーツ2"
            ImageUrl="http://lorempixum.com/120/90/sports/2" 
            LinkUrl="http://lorempixum.com/600/400/sports/2" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="スポーツ3" Text="スポーツ3"
            ImageUrl="http://lorempixum.com/120/90/sports/3" 
            LinkUrl="http://lorempixum.com/600/400/sports/3" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="スポーツ4" Text="スポーツ4"
            ImageUrl="http://lorempixum.com/120/90/sports/4" 
            LinkUrl="http://lorempixum.com/600/400/sports/4" />
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルでは、キーボード操作方法を紹介します。
</p>
<p>
<b>KeyNav</b> プロパティを True に設定すると、キーボード操作が有効になります。
</p>
<p>
既定では、次のキーがサポートされています。
</p>
<ul>
<li>左／下 - 前のページに移動します。</li>
<li>右／上 - 次のページに移動します。</li>
<li>HOME - 最初のページに移動します。</li>
<li>END - 最後のページに移動します。</li>
</ul>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
