﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="QuickTime.aspx.vb" Inherits="ControlExplorer.C1LightBox.QuickTime" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox"
    TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Qt" TextPosition="Outside" ControlsPosition="Outside" ShowCounter="false">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Despicable Me" Text="Despicable Me" 
			ImageUrl="~/C1LightBox/images/small/quicktime.png" 
			LinkUrl="http://trailers.apple.com/movies/universal/despicableme/despicableme-tlr1_r640s.mov?width=640&height=360" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">

<p>
C1LightBox は QuickTime で再生可能なメディアのホスティングをサポートします。
URL が以下の拡張子を持つファイルである場合、既定で QuickTime オブジェクトを使用して再生されます。
</p>
<p>
.dv、.mov、.moov、.movie、.mp4、.avi、.mpg、.mpeg
</p>
<p>
<b>Player</b> プロパティを Qt に設定すると、LightBox で 明示的に QuickTime を使用するように強制することができます。
</p>
</asp:Content>
