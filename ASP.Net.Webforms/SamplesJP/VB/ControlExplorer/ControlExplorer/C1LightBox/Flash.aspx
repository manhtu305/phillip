﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Flash.aspx.vb" Inherits="ControlExplorer.C1LightBox.Flash" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" TextPosition="Outside" ControlsPosition="Outside" ShowCounter="false">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" 
            ImageUrl="~/C1LightBox/images/small/happygirl.png" 
            LinkUrl="~/C1LightBox/movies/skip.swf" Title="嬉しい女性" 
            Text="嬉しい女性"/>
        <wijmo:C1LightBoxItem ID="LightBoxItem2" 
            ImageUrl="~/C1LightBox/images/small/caveman.png" 
            LinkUrl="~/C1LightBox/movies/caveman.swf" Text="穴居人" 
            Title="穴居人" />
        <wijmo:C1LightBoxItem ID="LightBoxItem3" 
            ImageUrl="~/C1LightBox/images/small/oldman.png" 
            LinkUrl="~/C1LightBox/movies/old_man.swf" Text="老人" Title="老人" />
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルは C1LightBox での SWF コンテンツの表示方法を示します。
</p>
<p>
URL に SWF ファイルを指定すると、C1LightBox は自動的に Flash オブジェクトを作成します。
</p>
<p>
ファイルリンクを明記に表示しない URL の場合は、<b>Player</b> プロパティを SWF に設定すると、強制的に Flash オブジェクトをコンテンツプレイヤーとして使用することができます。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
