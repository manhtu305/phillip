﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="SeqCounter.aspx.vb" Inherits="ControlExplorer.C1LightBox.SeqCounter" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" CounterType="Sequence">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="スポーツ1" Text="スポーツ1"
            ImageUrl="http://lorempixum.com/120/90/sports/1" 
            LinkUrl="http://lorempixum.com/600/400/sports/1" />
        <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="スポーツ2" Text="スポーツ2"
            ImageUrl="http://lorempixum.com/120/90/sports/2" 
            LinkUrl="http://lorempixum.com/600/400/sports/2" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="スポーツ3" Text="スポーツ3"
            ImageUrl="http://lorempixum.com/120/90/sports/3" 
            LinkUrl="http://lorempixum.com/600/400/sports/3" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="スポーツ4" Text="スポーツ4"
            ImageUrl="http://lorempixum.com/120/90/sports/4" 
            LinkUrl="http://lorempixum.com/600/400/sports/4" />
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルでは、連番カウンタスタイルを紹介します。
</p>
<p>
C1LightBox は 2 種類のカウンタスタイルをサポートし、<b>CounterType</b> プロパティで指定します。
</p>
<p>
<b>CounterType</b> プロパティを Default に設定すると、<b>CounterFormat</b> プロパティでカウンタテキストの形式を指定します。
既定値は "[i] of [n]" で、"[i]" は現在のページ番号を、"[n]" 総ページ数を示す組み込みパラメータです。
</p>
<p>
<b>CounterType</b> プロパティを Sequence に設定すると、<b>CounterLimit</b> プロパティを使用して表示する連番の最大値を指定します。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
