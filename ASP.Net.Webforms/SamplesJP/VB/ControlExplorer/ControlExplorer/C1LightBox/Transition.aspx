﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Transition.aspx.vb" Inherits="ControlExplorer.C1LightBox.Transition" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" >
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="抽象1" Text="抽象1"
			ImageUrl="http://lorempixum.com/120/90/abstract/1" 
			LinkUrl="http://lorempixum.com/600/400/abstract/1" />
		<wijmo:C1LightBoxItem ID="LightBoxItem2" Title="抽象2" Text="抽象2"
			ImageUrl="http://lorempixum.com/120/90/abstract/2" 
			LinkUrl="http://lorempixum.com/600/400/abstract/2" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="抽象3" Text="抽象3"
			ImageUrl="http://lorempixum.com/120/90/abstract/3" 
			LinkUrl="http://lorempixum.com/600/400/abstract/3" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="抽象4" Text="抽象4"
			ImageUrl="http://lorempixum.com/120/90/abstract/4" 
			LinkUrl="http://lorempixum.com/600/400/abstract/4" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルでは、次のページに遷移する際のアニメーション効果を示します。
</p>

<p>
<b>TransAnimation.Animated</b> プロパティを以下の任意値に設定して、アニメーションスタイルを変更できます。
</p>

<ul>
<li>None - アニメーションなし。</li>
<li>Slide - スライドするアニメーション。</li>
<li>Fade - フェードアウトするアニメーション。</li>
</ul>


<p>
<b>TransAnimation.Easing</b> プロパティはアニメーションのイージングスタイルを指定します。
<b>TransAnimation.Duration</b> プロパティはアニメーションの持続時間を指定します。
</p>


<p>
スライドアニメーションでは、<b>SlideDirection</b> プロパティを Horizontal または Vertical に設定することによって、スライドの向きを変更することができます。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">


<script type="text/javascript">
	$(function () {

		$('.option').change(function () {
			$("#<%=C1LightBox1.ClientID%>").c1lightbox('option', {
				transAnimation: { animated: $('#animation').val() },
				slideDirection: $('#direction').val()
			});
		});


	});
</script>

<div class="demo-options">
<!-- オプションのマークアップ -->
	<label>アニメーション：</label><select id="animation" class='option'>
				<option value="fade" selected='true'>フェードアウト</option>
				<option value="slide">スライド</option>
				<option value="none">なし</option>
			</select>
						
	<label>スライドの方向：</label><select id="direction" class='option'>
				<option value="horizontal" selected='true'>水平方向</option>
				<option value="vertical">垂直方向</option>
			</select>
<!--　オプションマークアップの終了 -->
</div>

</asp:Content>
