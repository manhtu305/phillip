﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FullSize.aspx.vb" Inherits="ControlExplorer.C1LightBox.FullSize" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" DialogButtons="Close, FullSize"  
		TextPosition="TitleOverlay" Modal="true">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="スポーツ1" Text="スポーツ1" 
			ImageUrl="http://lorempixum.com/120/90/sports/1" 
			LinkUrl="http://lorempixum.com/1600/1200/sports/1" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルでは、C1LightBox の全画面表示機能を紹介します。
</p>
<p>
<b>DialogButtons</b> プロパティはダイアログボックスコントロールボタンの表示／非表示を指定します。これは列挙体型のプロパティで、以下の値をサポートしています。
</p>
<ul>
<li>None - ダイアログボックスボタンを表示しません。</li>
<li>Close - 終了ボタンを表示します。</li>
<li>FullSize - 全画面表示ボタンを表示します。</li>
</ul>

<p>
フルサイズのボタンが表示されている場合、ユーザーはフルサイズのボタンをクリックしてウィンドウ全体をカバーするように LightBox を拡張することができます。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
