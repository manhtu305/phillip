﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="AutoSize.aspx.vb" Inherits="ControlExplorer.C1LightBox.AutoSize" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" AutoSize="true" KeyNav="true">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="スポーツ1" Text="スポーツ1" 
			ImageUrl="http://lorempixum.com/120/90/sports/1" 
			LinkUrl="http://lorempixum.com/600/400/sports/1" />
		<wijmo:C1LightBoxItem ID="LightBoxItem2" Title="スポーツ2" Text="スポーツ2" 
			ImageUrl="http://lorempixum.com/120/90/sports/2" 
			LinkUrl="http://lorempixum.com/800/400/sports/2" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="スポーツ3" Text="スポーツ3" 
			ImageUrl="http://lorempixum.com/120/90/sports/3" 
			LinkUrl="http://lorempixum.com/600/400/sports/3" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="スポーツ4" Text="スポーツ4" 
			ImageUrl="http://lorempixum.com/120/90/sports/4" 
			LinkUrl="http://lorempixum.com/600/300/sports/4" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
C1LightBox は、画像の幅と高さに応じて画像のサイズを自動的に調整します。これにより、画像は正しい幅／高さの比率で表示されます。
</p>
<p>
<b>AutoSize</b> プロパティを True に設定すると、サイズの自動調整が有効になります。
</p>
<p>
<b>ResizeAnimation</b> プロパティは、コンテナのサイズが変更されたときのアニメーション効果を指定します。
<b>ResizeAnimation.Animated</b> プロパティは以下の値をサポートしています。
</p>
<ul>
<li><strong>None</strong> - アニメーションなし。</li>
<li><strong>Wh</strong> - 高さの前に幅を調整します。</li>
<li><strong>Hw</strong> - 幅の前に高さを調整します。</li>
<li><strong>Sync</strong> - 幅と高さを同時に調整します。</li>
</ul>
<p>
オートサイズ機能は、画像のコンテンツの場合のみ有効になります。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">


<script type="text/javascript">
	$(function () {
		$('.option').change(function () {
			$("#<%=C1LightBox1.ClientID%>").c1lightbox('option', {
				resizeAnimation: { animated: $('#animation').val() }
			});
		});
	});
	
</script>

<div class="demo-options">
<label>アニメーション：</label>
<select id="animation" class='option'>
	<option value="sync" selected='true'>sync</option>
	<option value="wh">wh</option>
	<option value="hw">hw</option>
	<option value="none">none</option>
</select>
</div>

</asp:Content>
