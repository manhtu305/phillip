﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="TextPosition.aspx.vb" Inherits="ControlExplorer.C1LightBox.TextPosition" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Abstract1" Text="抽象1"
			ImageUrl="http://lorempixum.com/120/90/abstract/1" 
			LinkUrl="http://lorempixum.com/600/400/abstract/1" />
		<wijmo:C1LightBoxItem ID="LightBoxItem2" Title="Abstract2" Text="抽象2"
			ImageUrl="http://lorempixum.com/120/90/abstract/2" 
			LinkUrl="http://lorempixum.com/600/400/abstract/2" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="Abstract3" Text="抽象3"
			ImageUrl="http://lorempixum.com/120/90/abstract/3" 
			LinkUrl="http://lorempixum.com/600/400/abstract/3" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="Abstract4" Text="抽象4"
			ImageUrl="http://lorempixum.com/120/90/abstract/4" 
			LinkUrl="http://lorempixum.com/600/400/abstract/4" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
このサンプルでは、C1LightBox のテキストの配置やスタイルを紹介します。
</p>

<p>
テキストの配置やスタイルを変更するには、<strong>TextPosition</strong> プロパティを次の値に設定します。
</p>

<ul>
<li>None - テキストは表示されません。</li>
<li>Inside - テキストはコンテンツコンテナ内のコンテンツの横に表示されます。</li>
<li>Outside - テキストはコンテンツコンテナ外に表示されます。</li>
<li>Overlay - テキストはコンテンツコンテナ内のコンテンツの上にオーバーレイ表示されます。</li>
<li>TitleOverlay - コンテンツコンテナ内のタイトルの上に、タイトルのみがオーバーレイ表示されます。</li>
</ul>

<p>
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">

<script type="text/javascript">
	$(function () {
		$('#textposition').change(function () {
			$("#<%=C1LightBox1.ClientID%>").c1lightbox('option', 'textPosition', $(this).val());
		});

	});
</script>

<div class="demo-options">
<!-- オプションのマークアップを開始します。-->
	<h6>
		テキストの配置：</h6>
			<select id="textposition">
				<option value="inside" selected='true'>inside</option>
				<option value="outside">outside</option>
				<option value="overlay">overlay</option>
				<option value="titleOverlay">titleOverlay</option>
				<option value="none">none</option>
			</select>
<!-- オプションのマークアップを終了します。 -->
</div>

</asp:Content>
