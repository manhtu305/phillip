﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.vb"
    Inherits="ControlExplorer.C1Gallery.DataBinding" Title="Data Bind" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="C1Gallery1" runat="server" ShowTimer="True" Width="750px" Height="416px"
        ThumbsDisplay="4" LoadOnDemand="true">
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    <strong>C1Gallery</strong> はデータ連結をサポートし、画像 URL、キャプション、リンクフィールドなど、任意のデータをテンプレートに連結することができます。
    </p>
    <p>次のプロパティが設定されている場合、データ連結が有効になります。</p>
	<ul>
	<li><strong>DataSourceID </strong>- データソースの ID を設定します。</li>
	<li><strong>DataCaptionField </strong>- 画像のキャプションを読み込むデータソース内のフィールドを設定します。</li>
	<li><strong>DataLinkUrlField </strong>- リンクフィールドを読み込むデータソース内のフィールドを設定します。</li>
    <li><strong>DataImageUrlField </strong>- 画像のURLフィールドを読み込むデータソース内のフィールドを設定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:CheckBox ID="CheckBox1" AutoPostBack="true" runat="server" Text="ロードオンデマンド"
        OnCheckedChanged="CheckBox1_CheckedChanged" />
</asp:Content>
