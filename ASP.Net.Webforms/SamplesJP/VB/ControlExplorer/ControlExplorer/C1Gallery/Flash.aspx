﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Flash.aspx.vb" Inherits="ControlExplorer.C1Gallery.Flash" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="True" Width="700px" Height="600px"
        ThumbnailOrientation="Horizontal" ThumbsDisplay="4" ShowPager="false" Mode="Swf">
        <Items>
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/skip.swf" ImageUrl="http://cdn.wijmo.com/images/happygirl.png"
                Caption="Happy Girl" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/caveman.swf" ImageUrl="http://cdn.wijmo.com/images/caveman.png"
                Caption="Cave Man" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/old_man.swf" ImageUrl="http://cdn.wijmo.com/images/oldman.png"
                Caption="Old Man" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/skip.swf" ImageUrl="http://cdn.wijmo.com/images/happygirl.png"
                Caption="Happy Girl" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/caveman.swf" ImageUrl="http://cdn.wijmo.com/images/caveman.png"
                Caption="Cave Man" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/old_man.swf" ImageUrl="http://cdn.wijmo.com/images/oldman.png"
                Caption="Old Man" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/skip.swf" ImageUrl="http://cdn.wijmo.com/images/happygirl.png"
                Caption="Happy Girl" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/caveman.swf" ImageUrl="http://cdn.wijmo.com/images/caveman.png"
                Caption="Cave Man" />
            <cc1:C1GalleryItem LinkUrl="http://cdn.wijmo.com/movies/old_man.swf" ImageUrl="http://cdn.wijmo.com/images/oldman.png"
                Caption="Old Man" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<strong>C1Gallery</strong> コントロールでフレーム内に Flash コンテンツを表示します。
    </p>
    <ul>
        <li><b>Mode</b> プロパティを Swf に設定する必要があります。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
