﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1Gallery.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            var transitions = {
                animated: "fade",
                duration: 1000,
                easing: null
            }

            $('#<%=showEffectTypes.ClientID%>').change(function () {
                var ee = $("#<%=showEffectTypes.ClientID%> option:selected").val();
                $.extend(transitions, { animated: ee });
                $("#<%=Gallery.ClientID%>").c1gallery("option", "transitions", transitions);
            });
        });
    </script>
    <cc1:C1Gallery ID="Gallery" runat="server" ShowTimer="True" Width="750px" Height="256px"
        ThumbnailOrientation="Vertical" ThumbsDisplay="3" ShowPager="false">
        <Transitions>
            <Animated Disabled="false" Effect="slide" />
        </Transitions>
        <Items>
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/1" ImageUrl="http://lorempixum.com/200/150/sports/1" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/2" ImageUrl="http://lorempixum.com/200/150/sports/2" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/3" ImageUrl="http://lorempixum.com/200/150/sports/3" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/4" ImageUrl="http://lorempixum.com/200/150/sports/4" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/5" ImageUrl="http://lorempixum.com/200/150/sports/5" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/6" ImageUrl="http://lorempixum.com/200/150/sports/6" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    このサンプルでは、次の画像へ遷移する際の​​アニメーション効果を紹介します。
    この機能を使用するには、次のプロパティを設定します。</p>
    <ul>
        <li><b>Transitions</b></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
        <label for="showingEffectTypes">
            アニメーション：
        </label>
        <asp:DropDownList ID="showEffectTypes" runat="server">
            <asp:ListItem Value="blind" Selected="True">Blind</asp:ListItem>
            <asp:ListItem Value="clip">Clip</asp:ListItem>
            <asp:ListItem Value="drop">Drop</asp:ListItem>
            <asp:ListItem Value="explode">Explode</asp:ListItem>
            <asp:ListItem Value="fade">Fade</asp:ListItem>
            <asp:ListItem Value="fold">Fold</asp:ListItem>
            <asp:ListItem Value="highlight">Highlight</asp:ListItem>
            <asp:ListItem Value="puff">Puff</asp:ListItem>
            <asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
            <asp:ListItem Value="scale">Scale</asp:ListItem>
            <asp:ListItem Value="size">Size</asp:ListItem>
            <asp:ListItem Value="slide">Slide</asp:ListItem>
        </asp:DropDownList>
</asp:Content>
