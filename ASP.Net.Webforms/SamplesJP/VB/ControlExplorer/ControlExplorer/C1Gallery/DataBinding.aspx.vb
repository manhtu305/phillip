Imports System.Collections
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml.Linq
Imports System.Collections.Generic

Namespace ControlExplorer.C1Gallery
	Public Partial Class DataBinding
		Inherits System.Web.UI.Page
		Private Const TEXT As String = "{0} Vestibulum venenatis faucibus eros, vitae vulputate ipsum tempor ut. Donec ut ligula a metus volutpat sagittis. Duis sodales, lorem nec suscipit imperdiet, sapien metus tempor nibh, dapibus pulvinar lorem lacus molestie lacus. "

		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack OrElse IsCallback Then
				bind()
			End If
		End Sub

		Private Sub bind()
			Dim list1 As List(Of ContentGallery) = GetDataSource("http://lorempixum.com/200/150/sports/{0}", "http://lorempixum.com/750/300/sports/{0}")

			C1Gallery1.DataSource = list1
			C1Gallery1.DataImageUrlField = "ImgUrl"
			C1Gallery1.DataLinkUrlField = "LinkUrl"
			C1Gallery1.DataCaptionField = "Caption"
			C1Gallery1.DataBind()
		End Sub

		Private Function GetDataSource(urlFormatStr As String, linkFormatStr As String) As List(Of ContentGallery)
			Dim list As New List(Of ContentGallery)()

			For i As Integer = 1 To 10
                list.Add(New ContentGallery() With { _
                 .Content = String.Format(TEXT, String.Format("{0}.The picture one, ", i.ToString())), _
                 .LinkUrl = String.Format(linkFormatStr, i.ToString()), _
                 .ImgUrl = String.Format(urlFormatStr, i.ToString()), _
                 .Caption = String.Format("画像 {0}", i.ToString()) _
                })
            Next
            Return list
		End Function

		Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)
			C1Gallery1.LoadOnDemand = CheckBox1.Checked
			bind()
		End Sub
	End Class

	Public Class ContentGallery
		Public Property Content() As String
			Get
				Return m_Content
			End Get
			Set
				m_Content = Value
			End Set
		End Property
		Private m_Content As String

		Public Property ImgUrl() As String
			Get
				Return m_ImgUrl
			End Get
			Set
				m_ImgUrl = Value
			End Set
		End Property
		Private m_ImgUrl As String

		Public Property LinkUrl() As String
			Get
				Return m_LinkUrl
			End Get
			Set
				m_LinkUrl = Value
			End Set
		End Property
		Private m_LinkUrl As String

		Public Property Caption() As String
			Get
				Return m_Caption
			End Get
			Set
				m_Caption = Value
			End Set
		End Property
		Private m_Caption As String
	End Class
End Namespace
