'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Gallery


	Public Partial Class Orientation

		''' <summary>
		''' Gallery コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Gallery As Global.C1.Web.Wijmo.Controls.C1Gallery.C1Gallery

		''' <summary>
		''' OrientationDDL コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected OrientationDDL As Global.System.Web.UI.WebControls.DropDownList

		''' <summary>
		''' DirectionDDL コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected DirectionDDL As Global.System.Web.UI.WebControls.DropDownList

		''' <summary>
		''' ApplyBt コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected ApplyBt As Global.System.Web.UI.WebControls.Button
	End Class
End Namespace
