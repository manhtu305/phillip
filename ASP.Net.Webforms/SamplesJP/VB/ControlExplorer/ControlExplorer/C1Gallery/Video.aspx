﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Video.aspx.vb" Inherits="ControlExplorer.C1Gallery.Video" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="True" Width="750px" Height="480px"
        Mode="Swf" ThumbnailOrientation="Horizontal" ThumbsDisplay="4" ShowPager="false">
        <Items>
            <cc1:C1GalleryItem LinkUrl="https://www.youtube.com/v/J---aiyznGQ?version=3" ImageUrl="http://cdn.wijmo.com/images/keyboardcat.png" />
            <cc1:C1GalleryItem LinkUrl="http://www.youtube.com/v/FzRH3iTQPrk?version=3" ImageUrl="http://cdn.wijmo.com/images/panda.png" />
            <cc1:C1GalleryItem LinkUrl="http://www.youtube.com/v/z3U0udLH974?version=3" ImageUrl="http://cdn.wijmo.com/images/talkingcats.png" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<strong>C1Gallery</strong> コントロールで Flash 動画を表示します。
    </p>
    <ul>
        <li><b>Mode</b> プロパティを Swf に設定する必要があります。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
