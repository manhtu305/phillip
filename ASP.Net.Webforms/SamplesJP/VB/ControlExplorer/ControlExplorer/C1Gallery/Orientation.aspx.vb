Imports System.Collections
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml.Linq
Imports C1.Web.Wijmo.Controls
Imports C1.Web.Wijmo.Controls.C1Gallery

Namespace ControlExplorer.C1Gallery
	Public Partial Class Orientation
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub

		Protected Sub ApplyBt_Click(sender As Object, e As EventArgs)
			If OrientationDDL.SelectedValue = "Vertical" Then
				Gallery.Width = 750
				Gallery.Height = 256
				Gallery.ThumbnailOrientation = C1.Web.Wijmo.Controls.Orientation.Vertical
			Else
				Gallery.Width = 750
				Gallery.Height = 410
				Gallery.ThumbsDisplay = 4
				Gallery.ThumbnailOrientation = C1.Web.Wijmo.Controls.Orientation.Horizontal
			End If

			Gallery.ThumbnailDirection = If(DirectionDDL.SelectedValue = "After", ThumbsPosition.After, ThumbsPosition.Before)
		End Sub
	End Class
End Namespace
