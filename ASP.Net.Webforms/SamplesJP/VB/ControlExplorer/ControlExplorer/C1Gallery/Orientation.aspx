﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Orientation.aspx.vb"
    Inherits="ControlExplorer.C1Gallery.Orientation" Title="Orientation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="Gallery" runat="server" ShowTimer="True" Width="750px" Height="256px"
        ThumbnailOrientation="Vertical" ThumbsDisplay="3" ShowPager="false">
        <Items>
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/1" ImageUrl="http://lorempixum.com/200/150/sports/1" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/2" ImageUrl="http://lorempixum.com/200/150/sports/2" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/3" ImageUrl="http://lorempixum.com/200/150/sports/3" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/4" ImageUrl="http://lorempixum.com/200/150/sports/4" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/5" ImageUrl="http://lorempixum.com/200/150/sports/5" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/6" ImageUrl="http://lorempixum.com/200/150/sports/6" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、C1Gallery のサムネイルの方向と位置を設定しています。
    </p>
    <ul>
        <li><b>ThumbnailOrientation</b> プロパティでは、サムネイルの方向を指定します。</li>
        <li><b>ThumbnailDirection</b> プロパティでは、サムネイルの位置を指定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    向き：&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="OrientationDDL" runat="server">
        <asp:ListItem Value="Vertical">垂直方向</asp:ListItem>
        <asp:ListItem Value="Horizontal">水平方向</asp:ListItem>
    </asp:DropDownList>
    <br />
    位置：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="DirectionDDL" runat="server">
        <asp:ListItem Value="After">後ろ</asp:ListItem>
        <asp:ListItem Value="Before">前</asp:ListItem>
    </asp:DropDownList>
    <br />
    <asp:Button ID="ApplyBt" runat="server" Text="適用" onclick="ApplyBt_Click" />
</asp:Content>
