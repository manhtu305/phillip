﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Wijmo.Master" CodeBehind="ShowThumbnailCaptions.aspx.vb" Inherits=".ShowThumbnailCaptions" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="True" Width="750px" Height="416px"
        ThumbsDisplay="4" ShowPager="false" ShowThumbnailCaptions="true">
        <Items>
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/1" ImageUrl="http://lorempixum.com/200/150/sports/1"
                Caption="Word Caption 1" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/2" ImageUrl="http://lorempixum.com/200/150/sports/2"
                Caption="Word Caption 2" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/3" ImageUrl="http://lorempixum.com/200/150/sports/3"
                Caption="Word Caption 3" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/4" ImageUrl="http://lorempixum.com/200/150/sports/4"
                Caption="Word Caption 4" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/5" ImageUrl="http://lorempixum.com/200/150/sports/5"
                Caption="Word Caption 5" />
            <cc1:C1GalleryItem LinkUrl="http://lorempixum.com/750/300/sports/6" ImageUrl="http://lorempixum.com/200/150/sports/6"
                Caption="Word Caption 6" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:CheckBox ID="cbShowThumbCap" runat="server" Text="サムネイルのキャプションを表示" Checked="true"  AutoPostBack="true" 
        />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、サムネイルのキャプションを表示する方法を紹介します。
    </p>
    <ul>
        <li><b>ShowThumbnailCaptions</b> プロパティを使用して、サムネイルのキャプションを表示するかどうかを設定します。</li>
    </ul>
</asp:Content>
