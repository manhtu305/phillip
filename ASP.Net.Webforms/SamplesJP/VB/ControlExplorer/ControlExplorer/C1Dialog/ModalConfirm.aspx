﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="ModalConfirm.aspx.vb" Inherits="Dialog_ModalConfirm" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function delClick() {
            $(this).c1dialog("close");
        }
        function cancelClick() {
            $(this).c1dialog("close");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="400" Height="200" Title="アップロード完了"
        Modal="True" CloseText="Close">
        <Content>
            <p>
                <span class="ui-icon ui-icon-circle-check"></span>ファイルは正常にアップロードされました！
            </p>
        </Content>
        <ExpandingAnimation Duration="400" />
        <CollapsingAnimation Duration="300" />
        <Buttons>
            <cc1:DialogButton onclientclick="delClick" text="削除" />
            <cc1:DialogButton onclientclick="cancelClick" text="キャンセル" />
        </Buttons>
        <CaptionButtons>
            <Pin Visible="false" />
            <Refresh Visible="False" />
            <Toggle Visible="False" />
            <Minimize Visible="False" />
            <Maximize Visible="False" />
        </CaptionButtons>
    </cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    	このサンプルでは、モーダル <strong>C1Dialog </strong> を確認ダイアログボックスとして使用する方法を紹介します。
    </p>
    <ul>
        <li><b>Modal</b> プロパティを True に設定すると、この機能を使用できます。</li>
        <li><b>Buttons</b> プロパティを使用すると、ダイアログボックスの下部にボタンを追加できます。</li>
        <li><b>CaptionButtons</b> プロパティでは、キャプションバーのボタンを表示するかどうかを指定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <input type="button" value="確認ダイアログを表示" onclick="$('#<%=dialog.ClientID%>').c1dialog('open')" />
</asp:Content>
