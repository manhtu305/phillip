﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="ContentUrl.aspx.vb" Inherits="Dialog_ContentUrl" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="760px" Height="460px" Stack="True"
        CloseText="Close" Title="Wijmo" ContentUrl="http://www.wijmo.com">
        <CollapsingAnimation>
            <Animated Effect="blind" />
        </CollapsingAnimation>
    </cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    このサンプルでは、ダイアログボックス内部の iFrame に外部の URL を表示します。
    </p>
    <ul>
        <li><b>ContentUrl</b> プロパティでは、 <strong>C1Dialog</strong> コントロール内の iFrame 要素に表示する URL 文字列を設定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
