﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.Dialog.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="550px" Height="240px" Title="アニメーション">
        <Content>
            <h2>
                ダイアログ</h2>
            <br />
            <span>これは既定のダイアログです。ダイアログウインドウは移動やサイズ変更が可能で、x アイコンで閉じることができます。</span>
        </Content>
    </cc1:C1Dialog>
    <script type="text/javascript">
        function dialogMethod(p) {
            $('#<%=dialog.ClientID%>').c1dialog(p);
        }
        $(document).ready(function () {

            $('#<%=expandEffectTypes.ClientID%>').change(function () {
                var ee = $("#<%=expandEffectTypes.ClientID%> option:selected").val();
                $("#<%=dialog.ClientID%>").c1dialog("option", "expandingAnimation", { animated: ee, duration: 500 });
            });

            $('#<%=collapseEffectTypes.ClientID%>').change(function () {
                var ce = $("#<%=collapseEffectTypes.ClientID%> option:selected").val();
                $("#<%=dialog.ClientID%>").c1dialog("option", "collapsingAnimation", { animated: ce, duration: 500 });
            })

            $('#<%=showEffectTypes.ClientID%>').change(function () {
                var ee = $("#<%=showEffectTypes.ClientID%> option:selected").val();
                $("#<%=dialog.ClientID%>").c1dialog("option", "show", ee);
            });

            $('#<%=hideEffectTypes.ClientID%>').change(function () {
                var ce = $("#<%=hideEffectTypes.ClientID%> option:selected").val();
                $("#<%=dialog.ClientID%>").c1dialog("option", "hide", ce);
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    このサンプルでは、<strong>C1Dialog </strong>コントロールを開く、閉じる、展開する、縮小する際のアニメーション効果を設定する方法を示します。この機能をサポートするプロパティは次のとおりです。</p>
    <ul>
        <li><b>ExpandingAnimation</b></li>
        <li><b>CollapsingAnimation</b></li>
        <li><b>Show</b></li>
        <li><b>Hide</b></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:Button ID="Button1" runat="server" Text="ダイアログを表示" OnClientClick="dialogMethod('open'); return false;" />
    <asp:Button ID="Button2" runat="server" Text="ダイアログを非表示" OnClientClick="dialogMethod('close'); return false;" />
    <div class="option-row">
        <label for="showingEffectTypes">
            表示のアニメーション：
        </label>
        <asp:DropDownList ID="showEffectTypes" runat="server">
            <asp:ListItem Value="blind" Selected="True">Blind</asp:ListItem>
            <asp:ListItem Value="bounce">Bounce</asp:ListItem>
            <asp:ListItem Value="clip">Clip</asp:ListItem>
            <asp:ListItem Value="drop">Drop</asp:ListItem>
            <asp:ListItem Value="explode">Explode</asp:ListItem>
            <asp:ListItem Value="fade">Fade</asp:ListItem>
            <asp:ListItem Value="fold">Fold</asp:ListItem>
            <asp:ListItem Value="highlight">Highlight</asp:ListItem>
            <asp:ListItem Value="puff">Puff</asp:ListItem>
            <asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
            <asp:ListItem Value="scale">Scale</asp:ListItem>
            <asp:ListItem Value="size">Size</asp:ListItem>
            <asp:ListItem Value="slide">Slide</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="option-row">
        <label for="showingEffectTypes">
            非表示のアニメーション：
        </label>
        <asp:DropDownList ID="hideEffectTypes" runat="server">
            <asp:ListItem Value="blind">Blind</asp:ListItem>
            <asp:ListItem Value="bounce">Bounce</asp:ListItem>
            <asp:ListItem Value="clip">Clip</asp:ListItem>
            <asp:ListItem Value="drop">Drop</asp:ListItem>
            <asp:ListItem Value="explode" Selected="True">Explode</asp:ListItem>
            <asp:ListItem Value="fade">Fade</asp:ListItem>
            <asp:ListItem Value="fold">Fold</asp:ListItem>
            <asp:ListItem Value="highlight">Highlight</asp:ListItem>
            <asp:ListItem Value="puff">Puff</asp:ListItem>
            <asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
            <asp:ListItem Value="scale">Scale</asp:ListItem>
            <asp:ListItem Value="size">Size</asp:ListItem>
            <asp:ListItem Value="slide">Slide</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="option-row">
        <label for="showingEffectTypes">
            展開のアニメーション：
        </label>
        <asp:DropDownList ID="expandEffectTypes" runat="server">
            <asp:ListItem Value="blind">Blind</asp:ListItem>
            <asp:ListItem Value="bounce">Bounce</asp:ListItem>
            <asp:ListItem Value="clip">Clip</asp:ListItem>
            <asp:ListItem Value="drop">Drop</asp:ListItem>
            <asp:ListItem Value="explode">Explode</asp:ListItem>
            <asp:ListItem Value="fade">Fade</asp:ListItem>
            <asp:ListItem Value="fold">Fold</asp:ListItem>
            <asp:ListItem Value="highlight" Selected="True">Highlight</asp:ListItem>
            <asp:ListItem Value="puff">Puff</asp:ListItem>
            <asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
            <asp:ListItem Value="scale">Scale</asp:ListItem>
            <asp:ListItem Value="size">Size</asp:ListItem>
            <asp:ListItem Value="slide">Slide</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="option-row">
        <label for="showingEffectTypes">
            縮小のアニメーション：
        </label>
        <asp:DropDownList ID="collapseEffectTypes" runat="server">
            <asp:ListItem Value="blind">Blind</asp:ListItem>
            <asp:ListItem Value="bounce">Bounce</asp:ListItem>
            <asp:ListItem Value="clip">Clip</asp:ListItem>
            <asp:ListItem Value="drop">Drop</asp:ListItem>
            <asp:ListItem Value="explode">Explode</asp:ListItem>
            <asp:ListItem Value="fade">Fade</asp:ListItem>
            <asp:ListItem Value="fold">Fold</asp:ListItem>
            <asp:ListItem Value="highlight">Highlight</asp:ListItem>
            <asp:ListItem Value="puff" Selected="True">Puff</asp:ListItem>
            <asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
            <asp:ListItem Value="scale">Scale</asp:ListItem>
            <asp:ListItem Value="size">Size</asp:ListItem>
            <asp:ListItem Value="slide">Slide</asp:ListItem>
        </asp:DropDownList>
    </div>
</asp:Content>
