﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="ModalDialog.aspx.vb" Inherits="Dialog_ModalDialog" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="400px" Height="180px" Modal="true"
        Stack="True" CloseText="Close" Title="モーダルダイアログ">
        <Content>
            <p>
                モーダルオーバーレイ画面を追加すると、ページのコンテンツを暗くしてダイアログを目立たせることができます。</p>
        </Content>
        <ExpandingAnimation Duration="400" />
        <CollapsingAnimation Duration="300" />
    </cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        モーダルオーバーレイ画面を追加すると、ページのコンテンツを暗くしてダイアログを目立たせることができます。
    </p>
    <p>
        この機能は、<b>Modal</b> プロパティが True に設定されている時に有効になります。 
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <input type="button" value="ダイアログを表示" onclick="$('#<%=dialog.ClientID%>').c1dialog('open')" />
</asp:Content>
