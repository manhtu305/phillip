﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Window.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="550px" Height="240px" Title="概要">
        <Content>
            <h2>
                ダイアログ</h2>
            <br />
        </Content>
    </cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、既定の <strong>C1Dialog </strong>コントロールを表示しています。
        <strong>C1Dialog </strong>コントロールは、エラーメッセージや説明などの情報を表示するのに便利です。
        ダイアログウィンドウでは、移動やサイズ変更が可能で、x アイコンで閉じることができます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <input type="button" value="ダイアログを非表示" onclick="$('#<%=dialog.ClientID%>').c1dialog('close')" />
    <input type="button" value="ダイアログを表示" onclick="$('#<%=dialog.ClientID%>').c1dialog('open')" />
</asp:Content>
