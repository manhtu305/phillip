﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="ModalAlert.aspx.vb" Inherits="Dialog_ModalAlert" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function btnClick() {
            $(this).c1dialog("close");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="550px" Height="240px" Modal="true"
        Stack="True" CloseText="Close" Title="アップロード完了">
        <Content>
            <p>
                <span class="ui-icon ui-icon-circle-check"></span>ファイルは正常にアップロードされました！
            </p>
        </Content>
        <ExpandingAnimation Duration="400">
        </ExpandingAnimation>
        <Buttons>
            <cc1:DialogButton OnClientClick="btnClick" Text="OK" />
        </Buttons>
        <CaptionButtons>
            <Pin Visible="False" />
            <Refresh Visible="False" />
            <Toggle Visible="False" />
            <Minimize Visible="False" />
            <Maximize Visible="False" />
        </CaptionButtons>
        <CollapsingAnimation Duration="300" />
    </cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、モーダル <strong>C1Dialog </strong> を警告ダイアログボックスとして使用する方法を紹介します。
    </p>
    <ul>
        <li><b>Modal</b> プロパティを True に設定すると、この機能を使用できます。</li>
        <li><b>Buttons</b> プロパティを使用すると、ダイアログボックスの下部にボタンを追加できます。</li>
        <li><b>CaptionButtons</b> プロパティでは、キャプションバーのボタンを表示するかどうかを指定します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <input type="button" value="警告ダイアログを表示" onclick="$('#<%=dialog.ClientID%>').c1dialog('open')" />
</asp:Content>
