Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.Dialog
	Public Partial Class Animation
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				dialog.Show = showEffectTypes.SelectedValue.Trim()
				dialog.Hide = hideEffectTypes.SelectedValue.Trim()
				dialog.ExpandingAnimation.Animated.Effect = expandEffectTypes.SelectedValue.Trim()
				dialog.CollapsingAnimation.Animated.Effect = collapseEffectTypes.SelectedValue.Trim()
			End If
		End Sub
	End Class
End Namespace
