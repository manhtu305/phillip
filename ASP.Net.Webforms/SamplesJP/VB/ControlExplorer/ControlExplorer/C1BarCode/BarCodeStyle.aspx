﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="BarCodeStyle.aspx.vb" Inherits="ControlExplorer.C1BarCode.BarCodeStyle" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>

<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        .codeContainer
        {
            display: inline-block;
            padding: 20px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    
    <div >
        <div>下記のバーコードに設定されている値は"1234567890"です。</div>
        <br />
        <h4>バーコードの値を表示 : </h4>
        <wijmo:C1BarCode ID="C1BarCode1" runat="server" Text="1234567890" ShowText="true" />
        <br />
        <div>
            <h4>向きの設定 : </h4>
            <div>
                Normal
            <wijmo:C1BarCode ID="C1BarCode4" runat="server" Text="1234567890" ShowText="true" />
            </div>
            <div class="codeContainer">
                Up
            <wijmo:C1BarCode ID="C1BarCode2" runat="server" Text="1234567890" ShowText="true" BarDirection="Up" />
            </div>
            <div class="codeContainer">
                Down
            <wijmo:C1BarCode ID="C1BarCode3" runat="server" Text="1234567890" ShowText="true" BarDirection="Down" />
            </div>
        </div>
        <br />
        <div>
            <h4>背景色と前景色の設定 : </h4>
            <p>BackColor: Yellow</p>
            <p>ForColor: Blue</p>
            <wijmo:C1BarCode ID="C1BarCode5" runat="server" Text="1234567890" ShowText="true" BackColor="Yellow" ForeColor="Blue" />
        </div>
        <br />
        <div>
            <h4>テキストのフォントを設定 : </h4>
            <p>TextFont: "Consolas, 8.25pt, style=Italic"</p>
            <wijmo:C1BarCode ID="C1BarCode6" runat="server" Text="1234567890" ShowText="true" TextFont="Consolas, 8.25pt, style=Italic"/>
        </div>
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
        このサンプルでは、<strong>C1BarCode </strong>で使用可能なスタイルを紹介します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

</asp:Content>
