﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="UseBarCodeImageUrl.aspx.vb" Inherits="ControlExplorer.C1BarCode.UseBarCodeImageUrl" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    
    <div >
        <div>このバーコードに設定されている値は"1234567890"です。</div>
        <br />
        <asp:Image ID="Image1" runat="server" />
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>このサンプルでは、<strong>ImageUrl</strong> プロパティを使用してバーコード画像を <strong>Image</strong> コントロールに出力します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
    </asp:Content>