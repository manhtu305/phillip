﻿Imports C1.Web.Wijmo.Controls.C1BarCode
Imports C1.Web.Wijmo.Controls.C1ComboBox

Namespace ControlExplorer.C1BarCode
    Public Class EncodingException
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
            Dim codeType As WijmoCodeTypeEnum = DirectCast(C1ComboBox1.SelectedIndex, WijmoCodeTypeEnum)
            C1BarCode1.CodeType = codeType
            C1BarCode1.Text = C1InputText1.Text
            Dim exc As Exception = C1BarCode1.EncodingException

            Label1.Text = If(exc Is Nothing, "", exc.Message)

        End Sub
    End Class
End Namespace
