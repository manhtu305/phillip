﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="EncodingException.aspx.vb" Inherits="ControlExplorer.C1BarCode.EncodingException" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    <div >
        <br />
        <wijmo:C1BarCode ID="C1BarCode1" runat="server" AutoSize="true" />
        <br />
        <br />
        バーコードタイプを選択 : 
        <wijmo:C1ComboBox ID="C1ComboBox1" runat="server">
            <Items>
                <wijmo:C1ComboBoxItem Text="Code39" Value="Code39" Selected="true" />
                <wijmo:C1ComboBoxItem Text="Code93" Value="Code93" />
                <wijmo:C1ComboBoxItem Text="Code128" Value="Code128" />
                <wijmo:C1ComboBoxItem Text="CodeI2of5" Value="CodeI2of5" />
                <wijmo:C1ComboBoxItem Text="Codabar" Value="Codabar" />
                <wijmo:C1ComboBoxItem Text="PostNet" Value="PostNet" />
                <wijmo:C1ComboBoxItem Text="Ean13" Value="Ean13" />
                <wijmo:C1ComboBoxItem Text="Ean8" Value="Ean8" />
                <wijmo:C1ComboBoxItem Text="UpcA" Value="UpcA" />
                <wijmo:C1ComboBoxItem Text="UpcE" Value="UpcE" />
            </Items>
        </wijmo:C1ComboBox>
        <br />
        エンコードするテキストを入力 : <wijmo:C1InputText ID="C1InputText1" runat="server" MultiLine="True"></wijmo:C1InputText>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="Button1" runat="server" Text="適用" />
        <br />
        エンコードエラーの詳細 : <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p>
        <strong>C1BarCode </strong>は特定の文字列値によってエンコードされるバーコードイメージを提供します。
    </p>
    <p>
        多くのエンコードは、描画可能なテキストのタイプに制限があります。
        例えば、数値のみ設定可能な場合や特定の文字数を要求する場合があります。
    </p>
    <p>
        Text プロパティの値が現在の CodeType を使用して描画できない場合、
        バーコードは生成されず、コントロールは空白のままとなります。
    </p>
    <p>
        このケースでは、<b>EncodingException</b> プロパティによって、
        バーコードの生成を妨げた例外の詳細を返します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

</asp:Content>
