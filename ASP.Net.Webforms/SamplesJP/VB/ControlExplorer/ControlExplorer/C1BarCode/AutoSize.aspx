﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="AutoSize.aspx.vb" Inherits="ControlExplorer.C1BarCode.AutoSize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        .codeContainer
        {
            display: inline-block;
            padding: 20px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">

    <div>
        <p>AutoSizeプロパティはデフォルトで <i>false</i> になっており、バーコードのサイズ変更を可能にします。</p>
        <br />
        <wijmo:C1BarCode ID="C1BarCode4" runat="server" Text="1234567890" ShowText="true" Width="200" Height="60" />
        <p>(Width:200px;&nbsp;&nbsp;&nbsp;Height:60px)</p>
        <br />
        <wijmo:C1BarCode ID="C1BarCode2" runat="server" Text="1234" ShowText="true" Width="200" Height="60" />
        <p>(Width:200px;&nbsp;&nbsp;&nbsp;Height:60px)</p>
        <br />
        <p>AutoSizeプロパティを <i>true</i> にすると、バーコードイメージのデフォルトサイズに合わせてサイズ調整されます。</p>
        <br />
        <wijmo:C1BarCode ID="C1BarCode1" runat="server" Text="1234567890" ShowText="true" Width="200" Height="60" AutoSize="true"/>
        <br />
        <wijmo:C1BarCode ID="C1BarCode3" runat="server" Text="1234" ShowText="true" Width="200" Height="60" AutoSize="true"/>
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p>このサンプルは、<strong>AutoSize</strong> プロパティの動作を説明します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
