﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1BarCode

    Partial Public Class CodeType

        '''<summary>
        '''C1BarCode4 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode4 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode1 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode2 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode3 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode5 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode5 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode8 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode8 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode6 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode6 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode7 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode7 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode9 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode9 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode

        '''<summary>
        '''C1BarCode10 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1BarCode10 As Global.C1.Web.Wijmo.Controls.C1BarCode.C1BarCode
    End Class
End Namespace
