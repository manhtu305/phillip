﻿<%@ Page Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CodeType.aspx.vb" Inherits="ControlExplorer.C1BarCode.CodeType" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        .codeContainer
        {
            display: inline-block;
            padding: 20px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">

    <div>
            <div>
                <div class="codeContainer">
                    <h4>Code39 :</h4>
                    <p>値: C1BarCode%Code39</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode4" runat="server" Text="C1BarCode%Code39" ShowText="true" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Code93 :</h4>
                    <p>値: Code93</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode1" runat="server" Text="Code93" ShowText="true" CodeType="Code93" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Code128 :</h4>
                    <p>値: Code128</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode2" runat="server" Text="Code128" ShowText="true" CodeType="Code128" />
                    </div>
                    <br />
                </div>
            </div>
            <div>
                <div class="codeContainer">
                    <h4>CodeI2of5 :</h4>
                    <p>値: 123456789</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode3" runat="server" Text="123456789" ShowText="true" CodeType="CodeI2of5" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Codabar :</h4>
                    <p>値: 12345678901234567890</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode5" runat="server" Text="12345678901234567890" ShowText="true" CodeType="Codabar" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>PostNet :</h4>
                    <p>値: 80122</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode8" runat="server" Text="80122" ShowText="true" CodeType="PostNet" />
                    </div>
                    <br />
                </div>
                
            </div>
            <div>
                <div class="codeContainer">
                    <h4>Ean13 :</h4>
                    <p>値: 690123456789</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode6" runat="server" Text="690123456789" ShowText="true" CodeType="Ean13" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Ean8 :</h4>
                    <p>値: 4711234</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode7" runat="server" Text="4711234" ShowText="true" CodeType="Ean8" />
                    </div>
                    <br />
                </div>
                
                <div class="codeContainer">
                    <h4>UpcA :</h4>
                    <p>値: 300746606017</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode9" runat="server" Text="300746606017" ShowText="true" CodeType="UpcA" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>UpcE :</h4>
                    <p>値: 01240000001</p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode10" runat="server" Text="01240000001" ShowText="true" CodeType="UpcE" />
                    </div>
                    <br />
                </div>
            </div>

            <br />
            <br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <h2>各バーコード種の説明 :</h2>
    <br />
    <h4>Code39 :</h4>
    <p>英数字（Code 3 of 9）。</p>
    <p>これは最も広く使われているエンコーディングの1つです。</p>
    <br />
    <h4>Code93 :</h4>
    <p>Code39よりわずかに高密度な英数字エンコーディングです。</p>
    <br />
    <h4>Code128 :</h4>
    <p>非常に高密度な英数字エンコーディングです。少なくとも６キャラクタが必要です。</p>
    <br />
    <h4>CodeI2of5 :</h4>
    <p>数値エンコーディングです。</p>
    <br />
    <h4>Codabar :</h4>
    <p>数字（0～9）と記号（-$:/.+）に加えてスタート／ストップキャラクタ (A～D)をエンコードします。</p>
    <br />
    <h4>PostNet :</h4>
    <p>米国郵便サービスに使用される数値エンコーディング。</p>
    <br />
    <h4>Ean13 :</h4>
    <p>12桁の商品コード（およびコントロールによって作成される１つのチェックサム桁）をエンコードします。</p>
    <br />
    <h4>Ean8 :</h4>
    <p>7桁の商品コード（およびコントロールによって作成される１つのチェックサム桁）をエンコードします。</p>
    <br />
    <h4>UpcA</h4>
    <p>本、雑誌、新聞のほか、巷のスーパーマーケットの棚にあるほとんどすべての商品に見られる一般的なエンコードです。EAN-13 に似ていますが、11 桁の数値データと末尾にチェックデジットがエンコードされています。最初の桁は番号システムを識別し、次の５桁がメーカーコード、次の５桁が製品コード、最後にチェックデジットが続きます。</p>
    <br />
    <h4>UpcE</h4>
    <p>UPC-A のバリエーションです。 余分なゼロを省くことでバーコードのサイズをコンパクト化できます。 結果の UPC-E バーコードは、UPC-A バーコードの約半分のサイズになるため、一般に包装の非常に小さな製品で使用されています。</p>
    <p>UpcE エンコーディングを使用する場合は、UpcA エンコードを使用するように、Text プロパティを 11 桁の文字列に設定します。</p>
    <p>一部の UpcA コードは UpcE にエンコードできません。
    メーカーコードの末尾が 000、100、または 200 の場合、製品番号は 900 以下である必要があります。
    メーカーコードの末尾が 00 で、かつ 100、200、300 でない場合、 製品番号は 90 以下である必要があります。
    メーカーコードの末尾が０で、かつ 00 でない場合、製品番号は９以下である必要があります。
    メーカーコードの末尾が０でない場合、製品番号は 5～9 の範囲である必要があります。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">

</asp:Content>

