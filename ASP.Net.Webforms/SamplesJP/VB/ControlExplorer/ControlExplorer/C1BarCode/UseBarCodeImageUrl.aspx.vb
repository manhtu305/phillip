﻿Imports C1.Web.Wijmo.Controls.C1BarCode

Namespace ControlExplorer.C1BarCode
    Public Class UseBarCodeImageUrl
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                Dim barCode As New C1.Web.Wijmo.Controls.C1BarCode.C1BarCode()
                barCode.Text = "1234567890"
                barCode.CodeType = WijmoCodeTypeEnum.Code128

                Me.Image1.ImageUrl = barCode.ImageUrl
            End If
        End Sub

    End Class
End Namespace
