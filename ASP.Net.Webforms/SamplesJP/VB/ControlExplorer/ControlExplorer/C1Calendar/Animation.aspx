﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_Animation" Codebehind="Animation.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
    カレンダー月の間を<label for="easing">遷移</label>するときに使用可能なアニメーション効果が数多く用意されています。
    </p>
    <p>
    アニメーション効果は、<strong>Direction</strong>、<strong>Duration</strong>、および <strong>Easing </strong>のプロパティによって決定されます。
    </p>
    <p>
    <b>Duration</b> プロパティを 0 に設定すると、アニメーション効果が無効になります。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">


<script type="text/javascript">
	$(function () {

		$('#direction').bind("change keyup", function () {
		    $("#<%=C1Calendar1.ClientID %>").c1calendar("option", { direction: $(this).val() });
		});

		$('#duration').bind("blur", function () {
			try {
				var duration = parseInt($(this).val());
				if (duration < 0) alert("Invalid duration");
				else {
				    $("#<%=C1Calendar1.ClientID %>").c1calendar("option", { duration: duration });
				}
			}
			catch (e) {
			}
		});

		$('#easing').bind("change keyup", function () {
		    $("#<%=C1Calendar1.ClientID %>").c1calendar("option", { easing: $(this).val() });
		});


	});
	
</script>

<div class="demo-options">
<div class="option-row">
	<label for="direction">
		方向：</label>
	<select name="direction" id="direction">
		<option value="horizontal" selected="selected">水平方向</option>
		<option value="vertical">垂直方向</option>
	</select>
</div>
<div class="option-row">
	<label for="easing">
		画面遷移時の効果：</label>
	<select name="easing" id="easing">
		<option value="jswing">jswing</option>
		<option value="def">def</option>
		<option value="easeInQuad" selected="selected">easeInQuad</option>
		<option value="easeOutQuad">easeOutQuad</option>
		<option value="easeInOutQuad">easeInOutQuad</option>
		<option value="easeInCubic">easeInCubic</option>
		<option value="easeOutCubic">easeOutCubic</option>
		<option value="easeInOutCubic">easeInOutCubic</option>
		<option value="easeInQuart">easeInQuart</option>
		<option value="easeOutQuart">easeOutQuart</option>
		<option value="easeInOutQuart">easeInOutQuart</option>
		<option value="easeInQuint">easeInQuint</option>
		<option value="easeOutQuint">easeOutQuint</option>
		<option value="easeInOutQuint">easeInOutQuint</option>
		<option value="easeInSine">easeInSine</option>
		<option value="easeOutSine">easeOutSine</option>
		<option value="easeInOutSine">easeInOutSine</option>
		<option value="easeInExpo">easeInExpo</option>
		<option value="easeOutExpo">easeOutExpo</option>
		<option value="easeInOutExpo">easeInOutExpo</option>
		<option value="easeInCirc">easeInCirc</option>
		<option value="easeOutCirc">easeOutCirc</option>
		<option value="easeInOutCirc">easeInOutCirc</option>
		<option value="easeInElastic">easeInElastic</option>
		<option value="easeOutElastic">easeOutElastic</option>
		<option value="easeInOutElastic">easeInOutElastic</option>
		<option value="easeInBack">easeInBack</option>
		<option value="easeOutBack">easeOutBack</option>
		<option value="easeInOutBack">easeInOutBack</option>
		<option value="easeInBounce">easeInBounce</option>
		<option value="easeOutBounce">easeOutBounce</option>
		<option value="easeInOutBounce">easeInOutBounce</option>
	</select>
</div>
<div class="option-row">
	<label for="duration">
		持続時間：</label>
	<input name="x" type="text" id="duration" style="width: 40px;" value="250" />
</div>
</div>

</asp:Content>

