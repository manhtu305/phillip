﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="True" CodeBehind="AutoPostback.aspx.vb" Inherits="ControlExplorer.C1Calendar.AutoPostback" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" AutoPostBack="true" ShowWeekNumbers="true"  
		onselecteddateschanged="C1Calendar1_SelectedDatesChanged">
	<SelectionMode Month="True" WeekDay="True" WeekNumber="True" />
</wijmo:C1Calendar>

<p>選択された日付：</p>
	<asp:BulletedList ID="BulletedList1" runat="server">
	</asp:BulletedList>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
    カレンダーは、日付がクライアント側から選択されたときに発生する自動ポストバックをサポートしています。
    </p>
    <p>
    <strong>AutoPostBack </strong>プロパティを True に設定すると、自動ポストバックが有効になります。
    </p>
    <p>
    ユーザーはサーバー側の <strong>SelectedDatesChanged</strong> イベントによって選択された日付を取得することができます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

