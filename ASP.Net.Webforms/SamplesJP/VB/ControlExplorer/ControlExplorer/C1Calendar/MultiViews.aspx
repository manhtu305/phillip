﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_MultiViews" Codebehind="MultiViews.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" MonthRows="2" MonthCols="2">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
        <strong>C1Calendar </strong>では、一度に複数月のカレンダーを表示することが可能です。
    </p>
    <p>
        <strong>MonthRows</strong> プロパティと <strong>MonthCols</strong> プロパティを設定することで、各行と各列に表示する数を制御できます。
    </p>
    <p>
        <strong>メモ</strong>: 複数月ビューの場合は、プレビュー機能とクイックナビゲーション機能はサポートされません。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

<div class="demo-options">
	<!-- オプションのマークアップ -->
	<div class="option-row">
		<label for="rows">行：</label>
		<select name="rows" id="rows">
			<option value="1">1</option>
			<option value="2" selected="selected">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select>
	</div>
	<div class="option-row">
		<label for="cols">列：</label>
		<select name="cols" id="cols">
			<option value="1">1</option>
			<option value="2" selected="selected">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select>
	</div>
	<!-- オプションマークアップの終了 -->
</div>

<script type="text/javascript">
$(function () {
	

	$('#rows').bind("change keyup", function () {
	    $("#<%=C1Calendar1.ClientID %>").c1calendar({ monthRows: $(this).val() * 1 });
	});

	$('#cols').bind("change keyup", function () {
	    $("#<%=C1Calendar1.ClientID %>").c1calendar({ monthCols: $(this).val() * 1 });
	});
});
	
</script>

</asp:Content>

