﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_Selection" Codebehind="Selection.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<style type="text/css">
		.style1
		{
			font-weight: normal;
		}
		.style2
		{
			font-weight: bold;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" ShowWeekDays="true" ShowWeekNumbers="true">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
<p>
<strong>C1Calendar </strong> は様々な選択方法をサポートしています。
</p>

<p>
月ビューでの日付の選択方法は、<strong>SelectionMode </strong>プロパティで制御できます。
<strong>SelectionMode</strong> プロパティは、以下の 5 つのブール型の子プロパティを含んでいるオブジェクトです。
</p>

<ul>
<li>Day</li>
<li>Days</li>
<li>WeekDay</li>
<li>WeekNumber</li>
<li>Month</li>
</ul>

<p>
例えば、<strong>SelectionMode.WeekDay</strong> プロパティを True に設定すると、曜日のセルが選択可能になります。
同様に、週番号を選択可能にするには、<strong>SelectionMode.WeekNumber</strong> と <strong>ShowWeekDays</strong> プロパティを True に設定する必要があります。
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

<div class="demo-options">
	<!-- オプションのマークアップ -->
	<div class="option-row">
		<label>日：<input type="checkbox" name="selectionmode" id="day" value="day" /></label>　
		<label>複数の日：<input type="checkbox" name="selectionmode" id="days" value="days" /></label>　
		<label>曜日：<input type="checkbox" name="selectionmode" id="weekDay" value="weekDay" /></label>　
		<label>週番号：<input type="checkbox" name="selectionmode" id="weekNumber" value="weekNumber" /></label>　
		<label>月：<input type="checkbox" name="selectionmode" id="month" value="month" /></label>
	</div>
	<!-- オプションマークアップの終了 -->
</div>

<script type="text/javascript">
$(function () {

	$.each(["day", "days", "weekDay", "weekNumber", "month"], function (i, name) {
		$("#" + name).attr("checked", !!$("#<%=C1Calendar1.ClientID %>").c1calendar("option", "selectionMode")[name]);
	});

	$("input[type=checkbox]").change(function () {
		var chk = $(this);
		var selMode = $("#<%=C1Calendar1.ClientID %>").c1calendar("option", "selectionMode");
		selMode[chk.val()] = chk[0].checked;
		$("#<%=C1Calendar1.ClientID %>").c1calendar("option", "selectionMode", selMode);
	});
});
	
</script>

</asp:Content>

