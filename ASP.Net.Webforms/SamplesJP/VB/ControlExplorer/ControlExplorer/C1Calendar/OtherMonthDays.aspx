﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_OtherMonthDays" Codebehind="OtherMonthDays.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" ShowOtherMonthDays="false">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
    月ビューで前後の月の日付を非表示にできます。
    </p>
    <p>
    <b>ShowOtherMonthDays</b> プロパティを False に設定することによって、前後の月の日付を非表示にできます。
    </p>
    <p>
    <b>ShowOtherMonthDays</b> は既定で True に設定されていて、前後の月の日付が表示されます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

