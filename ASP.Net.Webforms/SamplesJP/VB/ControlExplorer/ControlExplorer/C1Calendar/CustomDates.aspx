﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomDates.aspx.vb" Inherits="ControlExplorer.C1Calendar.CustomDates" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">

    function customizeDate($daycell, date, dayType, hover, preview) {
        if (date.getMonth() === 11 && date.getDate() === 25) {
            var $img = $('<div></div>').width(16).height(16).css('background-image', 'url(images/xmas.png)');

            $daycell.attr('align', 'right').empty().append($img);

            if ($daycell.hasClass('ui-datepicker-current-day')) {
                $daycell.css('background-color', '#aaa');
            } else
                $daycell.css('background-color', hover ? 'lightgray' : '');
            return true;
        }

        return false;
    }

</script>

<wijmo:C1Calendar ID="C1Calendar1" runat="server" DisplayDate="2012-12-01" 
		onclientcustomizedate="customizeDate">
</wijmo:C1Calendar>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
    <strong>C1Calendar </strong>は、クライアント側で各日付セルの表示をカスタマイズする機能を提供します。
</p>

<p>
    <b>OnClientCustomizeDate</b> プロパティで指定されるクライアント側コールバック関数を実装することで、日付セルをカスタマイズできます。
</p>

<p>
コールバック関数の引数は次のとおりです。
<ul>
<li>
$daycell - カスタマイズする日付のテーブルセルを示すjQuery オブジェクト
</li>
<li>
date - セルの日付
</li>
<li>
dayType - 日付の形式
</li>
<li>
hover - カーソルは日付セル上にあるかどうか
</li>
<li>
preview - プレビューコンテナで描画されるかどうか
</li>
</ul>
</p>

<p>
このサンプルでは、​​クリスマスの画像を使用しましたが、任意のスタイル (カスタムCSSなど) を使用して柔軟にカスタマイズできます。
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
