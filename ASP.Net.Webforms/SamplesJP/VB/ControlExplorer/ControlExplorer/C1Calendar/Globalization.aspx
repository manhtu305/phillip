﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Globalization.aspx.vb" Inherits="ControlExplorer.C1Calendar.Globalization" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" TitleFormat="Y" ToolTipFormat="D">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    <strong>C1Calendar</strong> では、<strong>Culture </strong>プロパティを設定すると、ローカライズされたスタイルで表示できます。
	</p>
	<p>
    さらに、<strong>WeekDayFormat </strong>プロパティを設定して、曜日を省略名で表示するかどうかを判定できます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">

<p>カルチャの変更<asp:RadioButtonList ID="RadioButtonList1" runat="server" 
        AutoPostBack="True" 
        onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
    <asp:ListItem Value="zh-CN">中国語 - 簡体字</asp:ListItem>
    <asp:ListItem Value="en-CA">英語 - カナダ</asp:ListItem>
    <asp:ListItem Value="en-GB">英語 - 英国</asp:ListItem>
    <asp:ListItem Value="en-TT">英語 - トリニダード・トバゴ</asp:ListItem>
    <asp:ListItem Value="fr-CA">フランス語 - カナダ</asp:ListItem>
    <asp:ListItem Value="fr-CH">フランス語 - スイス</asp:ListItem>
    </asp:RadioButtonList>
    </p>



</asp:Content>
