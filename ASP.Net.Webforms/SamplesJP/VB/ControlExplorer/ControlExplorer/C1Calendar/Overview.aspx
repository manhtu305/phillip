﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_Overview" Codebehind="Overview.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
    <strong>C1Calendar</strong> を使用することで、Web サイト上に豊富な機能を備えたカレンダーコントロールを表示できます。
    </p>
    <p>
    柔軟にカスタマイズ可能なスタイルとナビゲーション要素を持つ単一のまたは複数の月を表示できます。
    </p>
    <p>
    このサンプルでは、既定の <strong>C1Calendar</strong> コントロールを表示します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

