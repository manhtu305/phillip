﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_MinMaxRange" Codebehind="MinMaxRange.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <style type="text/css">
        .style1
        {
            font-weight: normal;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" MinDate="2012-5-8" MaxDate="2012-5-25" DisplayDate="2012-5-1">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
    C1Calendar では、日付選択の制限をサポートします。
    </p>
    <p>
    選択可能な範囲は、<b>MinDate</b> と <b>MaxDate</b> プロパティで指定できます。
    </p>
    <p>
    また、<b>MaxDate</b> 値は <b>MinDate</b> 値より大きい値である必要があります。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

