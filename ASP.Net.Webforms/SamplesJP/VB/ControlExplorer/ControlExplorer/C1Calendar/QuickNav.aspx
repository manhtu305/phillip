﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Calendar_QuickNav" Codebehind="QuickNav.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1Calendar ID="C1Calendar1" runat="server" NavButtons="Quick">
</wijmo:C1Calendar>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
    C1Calendar はクイックナビゲーション機能を提供します。
    </p>
    <p>
    <b>NavButtons</b> プロパティを Quick に設定することによりクイックナビゲーション機能が有効になります。
    <b>QuickNavSteps</b> プロパティは、ユーザーがクイックナビゲーションボタンをクリックしたときにジャンプする月の数を決定します。
    既定値は 12 になっており、ユーザーのクリックによりカレンダーが 1 年分だけ移動します。
    </p>
    <p>
    ナビゲーションボタンを表示しない場合、<b>NavButtons</b> プロパティを None に設定します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

