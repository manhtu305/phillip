﻿Imports C1.Web.Wijmo.Controls.C1Chart

Namespace ControlExplorer.C1CandlestickChart
    Partial Public Class Trendline
        Inherits System.Web.UI.Page
        Protected Sub Page_Load(sender As Object, e As EventArgs)
            If Not IsPostBack Then
                LoadTrendlineSeries()
            End If
        End Sub

        Private Sub LoadTrendlineSeries()
            Dim valueString As String = "8,8.6,11,6.2,13.8,15,14,12,16,15,17,18,17.2,18.5,17.8,18.6,19.8,18,16.9,15.6,14.7,14.2,13.9,13.2,12.8,11.7,11.2,10.5,9.4,8.9,8.4,8,8.6,11,6.2,13.8,15,14,12,16,15,17,18,17.2"

            Dim values As String() = valueString.Split(","c)

            Dim series = New CandlestickChartSeries()
            series.IsTrendline = True
            series.Label = "Trendline"
            series.TrendlineSeries.FitType = TrendlineFitType.Polynom
            series.TrendlineSeries.Order = 4
            series.TrendlineSeries.SampleCount = 100

            For Each valueX In Me.C1CandlestickChart1.SeriesList(0).Data.X.Values
                series.TrendlineSeries.Data.X.Add(valueX.DateTimeValue)
            Next

            For Each valueY As String In values
                series.TrendlineSeries.Data.Y.Add(Double.Parse(valueY))
            Next

            Me.C1CandlestickChart1.SeriesList.Add(series)
        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Dim order As Integer = CInt(inputOrder.Value)
            Dim sampleCount As Integer = CInt(inputSampleCount.Value)
            Dim fitType As TrendlineFitType = DirectCast([Enum].Parse(GetType(TrendlineFitType), dplFitType.SelectedValue), TrendlineFitType)

            For Each series In Me.C1CandlestickChart1.SeriesList
                If series.IsTrendline Then
                    series.TrendlineSeries.FitType = fitType
                    series.TrendlineSeries.Order = order
                    series.TrendlineSeries.SampleCount = sampleCount
                End If
            Next
        End Sub
    End Class
End Namespace
