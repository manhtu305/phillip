﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Upload.Overview" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Upload" tagprefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    	<wijmo:C1Upload ID="C1Upload1" runat="server" Width="300px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Upload </strong>コントロールを使用すると、ファイルをアップロードして、アップロードの進行状況に関する総合的な情報を扱うことができます。
        非常に効率的な HttpModule は、サーバーに最小限のメモリを割り当て、1 つまたは複数ファイルのアップロードに設定を最適化できます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
