Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1ToolTip
	Public Partial Class Trigger
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

        End Sub

        Protected Sub trigger_SelectedIndexChanged(sender As Object, e As EventArgs)
            Tooltip1.Triggers = DirectCast([Enum].Parse(GetType(C1.Web.Wijmo.Controls.C1ToolTip.Trigger), trigger.SelectedValue), C1.Web.Wijmo.Controls.C1ToolTip.Trigger)
            UpdatePanel1.Update()
        End Sub
	End Class
End Namespace
