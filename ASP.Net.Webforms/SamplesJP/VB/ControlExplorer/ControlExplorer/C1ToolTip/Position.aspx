﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Position.aspx.vb" Inherits="ControlExplorer.C1ToolTip.Position" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HyperLink ID="HyperLink1" Style="color: red; font-weight: bold;" runat="server" ToolTip="ツールチップ">ツールチップ</asp:HyperLink>
    <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#HyperLink1" CloseBehavior="Sticky">
    </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<strong>position</strong> オプションを使用して、ツールチップの表示位置を変更する方法を紹介します。
    </p>
    <p>
        このサンプルでは、クライアント側の <strong>position</strong> オプションが使用して、ツールチップの位置を変更しています。
    </p>
    <p>
        ドロップダウンから位置を選択して、「適用」ボタンをクリックすると、リンクのマウスホバー時に表示されるツールチップの位置が変更されることを確認できます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div style="height: 70px">
        <div style="width: 200px; float: left;">
            <span style="padding-right: 10px;">方向：</span><select id="my1">
                <option value="left">左</option>
                <option value="center">中央</option>
                <option value="right">右</option>
            </select>
            <select id="my2">
                <option value="top">上</option>
                <option value="center">中央</option>
                <option value="bottom" selected="selected">下</option>
            </select>
            <br />
            <span style="padding-right: 10px;">位置：</span><select id="at1">
                <option value="left">左</option>
                <option value="center">中央</option>
                <option value="right" selected="selected">右</option>
            </select>
            <select id="at2">
                <option value="top">上</option>
                <option value="center">中央</option>
                <option value="bottom">下</option>
            </select>
        </div>
    </div>
    <input type="button" onclick="applyPosition();" value="適用" />
    <script id="scriptInit" type="text/javascript">
        function applyPosition() {
            $("#<%=HyperLink1.ClientID %>").c1tooltip("option", "position", { my: $("#my1").val() + " " + $("#my2").val(), at: $("#at1").val() + " " + $("#at2").val() });
        };
    </script>
</asp:Content>
