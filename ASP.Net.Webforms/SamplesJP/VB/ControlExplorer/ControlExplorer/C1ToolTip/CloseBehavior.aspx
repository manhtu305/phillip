﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CloseBehavior.aspx.vb" Inherits="ControlExplorer.C1ToolTip.CloseBehavior" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .link
        {
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3 class="ui-helper-reset ui-widget-header ui-corner-all" style="padding: 1em;">
        <asp:HyperLink ID="HyperLink1" runat="server" ToolTip="ツールチップ" CssClass="link ui-widget ui-corner-all">アンカー</asp:HyperLink>
    </h3>

            <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#HyperLink1">
            </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、様々なツールチップを閉じる動作を表示します。
    </p>
    <p>
        このサンプルでは、クライアント側の <strong>closeBehavior</strong> オプションが使用されています。
    </p>
    <p>
        <strong>closeBehavior</strong> オプションは 3 つの値を設定できます。</p>
    <ul>
        <li><strong>auto</strong> - マウスが対象から離れた場合、ツールチップが閉じられます。</li>
        <li><strong>none</strong> - コードで明示的に閉じない限り、ツールチップが閉じられません。</li>
        <li><strong>sticky</strong> - 「閉じる」ボタンをクリックするか、コードで明示的に閉じる場合を除いて、マウスが対象から離れてもツールチップが閉じられません。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label>閉じる動作:</label>
                    <asp:DropDownList ID="closeBehavior" runat="server">
                        <asp:ListItem Selected="True" Text ="自動" Value="auto"></asp:ListItem>
                        <asp:ListItem Text ="なし" Value="none"></asp:ListItem>
                        <asp:ListItem Text ="閉じるボタン" Value="sticky"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>        
    <script id="scriptInit" type="text/javascript">
        $(document).ready(function () {
            $("#<%=closeBehavior.ClientID %>").change(function () {
                $("#<%=HyperLink1.ClientID %>").c1tooltip("option", "closeBehavior", $("#<%=closeBehavior.ClientID %>").val());
            });

        });
    </script>
</asp:Content>
