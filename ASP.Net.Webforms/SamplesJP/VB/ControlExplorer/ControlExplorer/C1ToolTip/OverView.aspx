﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="OverView.aspx.vb" Inherits="ControlExplorer.C1ToolTip.OverView" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        #targetContainer
        {
            position: relative;
        }
        #targetContainer img
        {
            margin: 0;
            padding: 0;
            position: absolute;
            border: solid 3px #333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="targetContainer">
        <img src="http://lorempixel.com/g/257/109/sports/1" style="left: 0; top: 0;" title="クリケット"
            alt="" />
        <img src="http://lorempixel.com/g/257/150/sports/2" style="left: 0; top: 109px;" title="ボディボード"
            alt="" />
        <img src="http://lorempixel.com/482/180/sports/3" style="left: 0; top: 259px;" title="サイクリング"
            alt="" />
        <img src="http://lorempixel.com/g/225/256/sports/4" style="left: 257px; top: 0;" title="バレーボール"
            alt="" />
        <img src="http://lorempixel.com/111/143/sports/5" style="left: 482px; top: 0px;" title="ロック クライミング"
            alt="" />
        <img src="http://lorempixel.com/g/111/296/sports/6" style="left: 482px; top: 143px;" title="水泳"
            alt="" />
        <img src="http://lorempixel.com/g/151/200/sports/7" style="left: 593px; top: 0;" title="サッカー"
            alt="" />
        <img src="http://lorempixel.com/152/239/sports/8" style="left: 593px; top: 200px;" title="相撲"
            alt="" />
    </div>
    <wijmo:C1ToolTip runat="server" ID="ToolTip1" TargetSelector="#targetContainer [title]">
    </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Tooltip</strong> では、カスタムツールチップを作成して、ユーザにより良いインタフェースを提供できます。
    </p>
    <p>
        <strong>C1Tooltip</strong> では、動作、位置、イベントなどを制御してツールチップを表示できます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
