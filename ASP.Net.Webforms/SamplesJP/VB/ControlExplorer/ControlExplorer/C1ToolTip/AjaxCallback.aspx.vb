Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1ToolTip
	Public Partial Class AjaxCallback
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub

		Protected Sub Tooltip1_OnAjaxUpdate(e As C1.Web.Wijmo.Controls.C1ToolTip.C1ToolTipCallBackEventArgs)
			'e.Title = "Test";
			e.Content = String.Format("アンカー {0} のツールチップです。", Integer.Parse(e.Source) + 1)
		End Sub
	End Class
End Namespace
