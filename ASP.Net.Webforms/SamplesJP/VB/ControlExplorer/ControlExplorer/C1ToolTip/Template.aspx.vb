Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1ToolTip
	Public Partial Class Template
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub

		Protected Sub Tooltip1_OnContentCallBack(e As C1.Web.Wijmo.Controls.C1ToolTip.C1ToolTipCallBackEventArgs)

		End Sub

		Protected Sub Tooltip1_OnAjaxUpdate(e As C1.Web.Wijmo.Controls.C1ToolTip.C1ToolTipCallBackEventArgs)
            If e.UpdatePanel IsNot Nothing Then
                Dim c As Control = e.UpdatePanel.FindControl("HyperLink1")
                HyperLink1.Text = e.Source
            End If
        End Sub
	End Class
End Namespace
