﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="AjaxCallback.aspx.vb" Inherits="ControlExplorer.C1ToolTip.AjaxCallback" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="targetContainer">
        <ul class="ui-helper-reset ui-widget-header ui-corner-all" style="padding: 1em;">
            <li class="tooltip"><a href="#">アンカー 1</a></li>
            <li class="tooltip"><a href="#">アンカー 2</a></li>
            <li class="tooltip"><a href="#">アンカー 3</a></li>
            <li class="tooltip"><a href="#">アンカー 4</a></li>
        </ul>
    </div>
    <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#targetContainer a" EnableCallBackMode="True" OnOnAjaxUpdate="Tooltip1_OnAjaxUpdate">
    </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、AJAX でツールチップコンテンツを設定する方法を紹介します。
    </p>
    <p>
        このサンプルでは、下記のプロパティとイベントが使用されています。
    </p>
    <ul>
        <li>EnableCallBackMode</li>
        <li>OnOnAjaxUpdate</li>
    </ul>
    <p>
        <strong>EnableCallBackMode</strong> プロパティを True に設定して、コールバックを有効にしてコンテンツを生成しています。
        そして、<strong>OnOnAjaxUpdate</strong> イベントを使用して、サーバ側コードでツールチップの内容を設定しています。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
