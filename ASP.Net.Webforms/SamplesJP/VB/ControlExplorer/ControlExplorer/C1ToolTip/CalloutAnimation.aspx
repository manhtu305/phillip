﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CalloutAnimation.aspx.vb" Inherits="ControlExplorer.C1ToolTip.CalloutAnimation" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .boxes
        {
            width: 500px;
            height: 50px;
            margin-top: 100px;
        }
        .boxes div
        {
            width: 100px;
            height: 50px;
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="boxes" id="boxes">
        <div>
        </div>
        <div>
        </div>
        <div>
        </div>
        <div>
        </div>
        <div>
        </div>
    </div>
    <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#boxes" CloseBehavior="None">
        <Position>
            <My Top="Bottom" Left="Center"></My>
            <At Left="Center"></At>
            <Offset Left="50" />
        </Position>
        <Animation>
            <Animated Effect="fade"></Animated>
        </Animation>
        <ShowAnimation>
            <Animated Effect="fade"></Animated>
        </ShowAnimation>
        <HideAnimation>
            <Animated Effect="fade"></Animated>
        </HideAnimation>
    </wijmo:C1ToolTip>
    <script id="scriptInit" type="text/javascript">
        $(document).ready(function () {
            //最初のshowでツールチップ要素を作成します。
            $("#boxes").c1tooltip("show");
            var tooltip = $("#boxes").c1tooltip("widget");
            tooltip.width(500);
            tooltip.height(50);
            $("#boxes").c1tooltip("show");
            var colors = ["red", "blue", "yellow", "black", "green"];
            var colors2 = ["赤", "青", "黄", "黒", "緑"];
            $("#boxes>div").each(function (i) {
                $(this).css("background-color", colors[i]);
                $(this).bind("click", function () {
                    var value = 50 + 100 * i;
                    $("#boxes").c1tooltip("option", "content", colors2[i]);
                    $("#boxes").c1tooltip("option", "position", { offset: value + " 0" });
                });
            });

        });
            
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、クライアント側の <strong>position</strong> オプションを使用してツールチップをアニメーション表示する方法を紹介します。</p>
    <p>
        色が塗りつぶられているブロックのいずれかをクリックすると、ツールチップが移動します。</p>
    <p>
        このサンプルでは、下記のプロパティが使用されています。</p>
    <ul>
        <li>サーバー側の Position</li>
        <li>CloseBehavior</li>
        <li>クライアント側の position オプション</li>
    </ul>
    <p>
        <strong>CloseBehavior</strong> を none に設定すると、ツールチップが常に表示されます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
