﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Trigger.aspx.vb" Inherits="ControlExplorer.C1ToolTip.Trigger" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .link
        {
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h3 class="ui-helper-reset ui-widget-header ui-corner-all" style="padding: 1em;">
                <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="#" ToolTip="ツールチップ" CssClass="link ui-widget ui-corner-all">アンカー</asp:HyperLink>
            </h3>
            <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#HyperLink1" CloseBehavior="Sticky">
            </wijmo:C1ToolTip>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript">
        function FocusOnAnchor() {
            $("#<%=HyperLink1.ClientID %>").focus();
        }

        function ShowTooltip() {
            $("#<%=HyperLink1.ClientID %>").c1tooltip("show");
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、様々なイベントでツールチップを表示する方法を紹介します。
        既定では、マウスホバー時にツールチップが表示されます。
    </p>
    <p>
        このサンプルでは、下記のプロパティを使用して、ツールチップが表示されるイベントを変更しています。
    </p>
    <ul>
        <li>サーバー側の <strong>CloseBehavior</strong></li>
        <li>クライアント側の <strong>triggers</strong></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label>トリガーイベント:</label>
                            <asp:DropDownList ID="trigger" runat="server" AutoPostBack="true" OnSelectedIndexChanged="trigger_SelectedIndexChanged">
                                <asp:ListItem Selected="True">ホバー</asp:ListItem>
                                <asp:ListItem>クリック</asp:ListItem>
                                <asp:ListItem>フォーカス</asp:ListItem>
                                <asp:ListItem>右クリック</asp:ListItem>
                                <asp:ListItem>カスタム</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li class="longbutton">
                            <input type="button" value="アンカーをフォーカス" onclick="FocusOnAnchor()"/>
                        </li>
                        <li class="longbutton">
                            <input type="button" value="ツールチップの表示" onclick="ShowTooltip()"/>
                        </li>
                    </ul>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
