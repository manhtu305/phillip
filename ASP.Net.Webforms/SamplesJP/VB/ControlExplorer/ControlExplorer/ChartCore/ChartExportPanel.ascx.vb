﻿Namespace ControlExplorer.ChartCore
    Public Class ChartExportPanel
        Inherits System.Web.UI.UserControl

        Public Property ChartType() As String
            Get
                Return m_ChartType
            End Get
            Set(value As String)
                m_ChartType = value
            End Set
        End Property
        Private m_ChartType As String

        Public ReadOnly Property ChartWidgetType() As String
            Get
                If String.IsNullOrEmpty(ChartType) OrElse ChartType.Length = 1 Then
                    Return String.Empty
                End If

                Return "wijmo" & ChartType(0) & ChartType.Substring(1).ToLower()
            End Get
        End Property

        Public ReadOnly Property C1ChartWidgetName() As String
            Get
                If String.IsNullOrEmpty(ChartType) Then
                    Return String.Empty
                End If

                Return ChartType.ToLower()
            End Get
        End Property

    End Class
End Namespace
