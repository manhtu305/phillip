﻿<%@ Control Language="vb" AutoEventWireup="true" CodeBehind="ExportPdf.ascx.vb" Inherits="ControlExplorer.ChartCore.ExportPdf" %>
<script type="text/javascript">
    $(function() {
        $("#exportPdf").click(exportPdf);
    });

    function exportPdf() {
        var fileName = $("#fileName").val();
        var url = $("#serverUrl").val() + "/exportapi/chart";
        var pdfSetting = {
            imageQuality: wijmo.exporter.ImageQuality[$("#imageQuality option:selected").val()],
            compression: wijmo.exporter.CompressionType[$("#compression option:selected").val()],
            fontType: wijmo.exporter.FontType[$("#fontType option:selected").val()],
            author: $("#pdfAuthor").val(),
            creator: $("#pdfCreator").val(),
            subject: $("#pdfSubject").val(),
            title: $("#pdfTitle").val(),
            producer: $("#pdfProducer").val(),
            keywords: $("#pdfKeywords").val(),
            encryption: wijmo.exporter.PdfEncryptionType[$("#encryption option:selected").val()],
            ownerPassword: $("#ownerPassword").val(),
            userPassword: $("#userPassword").val(),
            allowCopyContent: $("#allowCopyContent").prop('checked'),
            allowEditAnnotations: $("#allowEditAnnotations").prop('checked'),
            allowEditContent: $("#allowEditContent").prop('checked'),
            allowPrint: $("#allowPrint").prop('checked')
        }
        $(":data(<%=ChartWidgetType %>)").<%=C1ChartWidgetName%>("exportChart", fileName, "pdf", pdfSetting, url);
    }
</script>
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="エクスポート" id="exportPdf"/></li>
			<li class="fullwidth">
				<label class="settinglegend">ファイルコンテンツ:</label>
			</li>
			<li>
				<label>画質:</label>
				<select id="imageQuality">
					<option selected="selected" value="Default">既定</option>
					<option value="Low">低</option>
					<option value="Medium">中</option>
					<option value="High">高</option>
				</select> 
			</li>
			<li>
				<label>圧縮率:</label>
				<select id="compression">
					<option selected="selected" value="Default">既定</option>
					<option value="None">なし</option>
					<option value="BestSpeed">低</option>
					<option value="BestCompression">高</option>
				</select> 
			</li>
			<li>
				<label>フォント種別:</label>
				<select id="fontType">
					<option value="Standard">標準</option>
					<option value="TrueType" selected="selected">TrueType</option>
					<option value="Embedded">埋め込み</option>
				</select> 
			</li>
			<li class="fullwidth">
				<label class="settinglegend">文書情報:</label>
			</li>
			<li>
				<label>作成者:</label><input type="text" value="ComponentOne" id="pdfAuthor"/>
			</li>
			<li>
				<label>アプリケーション:</label><input type="text" value="ComponentOne" id="pdfCreator"/>
			</li>
			<li>
				<label>サブタイトル:</label><input type="text" id="pdfSubject"/>
			</li>
			<li>
				<label>タイトル:</label><input type="text" value="Export" id="pdfTitle"/>
			</li>
			<li>
				<label>アプリケーション:</label><input type="text" value="ComponentOne" id="pdfProducer"/>
			</li>
			<li>
				<label>キーワード:</label><input type="text" id="pdfKeywords"/>
			</li>
			<li class="fullwidth">
				<label class="settinglegend">セキュリティ:</label>
			</li>
			<li class="fullwidth">
				<label>暗号化種別:</label>
				<select id="encryption">
					<option selected="selected" value="NotPermit">なし</option>
					<option value="Standard40">Standard40</option>
					<option value="Standard128">Standard128</option>
					<option value="Aes128">Aes128</option>
				</select> 
			</li>
			<li>
				<label>所有者パスワード:</label><input type="password" id="ownerPassword"/>
			</li>
			<li>
				<label>ユーザーパスワード:</label><input type="password" id="userPassword"/>
			</li>
			<li><input type="checkbox" checked="checked" id="allowCopyContent"/><label class="widelabel">コピー許可</label></li>
			<li><input type="checkbox" checked="checked" id="allowEditAnnotations"/><label class="widelabel">注釈の編集許可</label></li>
			<li><input type="checkbox" checked="checked" id="allowEditContent"/><label class="widelabel">コンテンツの編集許可</label></li>
			<li><input type="checkbox" checked="checked" id="allowPrint"/><label class="widelabel">印刷許可</label></li>
			<li class="fullwidth">
				<label class="settinglegend">設定:</label>
			</li>
            <li class="longinput">
				<label>サーバーURL:</label>
				<input type="text" id="serverUrl" value="http://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label>ファイル名:</label>
				<input type="text" id="fileName" value="export">
			</li>
	    </ul>
    </div>
</div>