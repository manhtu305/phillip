﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Margin.aspx.vb" Inherits="ControlExplorer.C1RadialGauge.Margin" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gauge"
    TagPrefix="Wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <Wijmo:C1RadialGauge runat="server" ID="Gauge1" Value="100" Max="150" StartAngle="-45"
        SweepAngle="270" Width = "400px" Height = "400px">
        <Cap>
            <PointerCapStyle Stroke="#7F9CAD">
                <Fill Color="#7F9CAD">
                </Fill>
            </PointerCapStyle>
        </Cap>
        <TickMajor Factor="2" Visible="True" Interval="10" Offset="15" Position="Inside">
            <TickStyle Stroke="#556A7C">
                <Fill Color="#556A7C">
                </Fill>
            </TickStyle>
        </TickMajor>
        <TickMinor Visible="True" Interval="2" Offset="20" Position="Inside">
            <TickStyle Stroke="#556A7C">
                <Fill Color="#556A7C">
                </Fill>
            </TickStyle>
        </TickMinor>
        <Pointer Length="1" Width="4" Offset="0.15">
            <PointerStyle Stroke="#BF551C">
                <Fill Color="#BF551C">
                </Fill>
            </PointerStyle>
        </Pointer>
        <Labels Offset="-10">
            <LabelStyle Stroke="#556A7C">
                <Fill Color="#556A7C">
                </Fill>
            </LabelStyle>
        </Labels>
        <Animation Duration="2000" Easing="EaseOutBack"></Animation>
        <Face>
            <FaceStyle Stroke="#E0E8EF">
                <Fill Color="#E0E8EF">
                </Fill>
            </FaceStyle>
        </Face>
    </Wijmo:C1RadialGauge>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <label>
        左余白：</label>
    <input type="text" id="txLeft" value="0" style="width:60px;" />
    <label>
        　右余白：</label>
    <input type="text" id="txRight" value="0" style="width:60px;" />
    <label>
        　上余白：</label>
    <input type="text" id="txTop" value="0" style="width:60px;" />
    <label>
        　下余白：</label>
    <input type="text" id="txBottom" value="0" style="width:60px;" />
    <input type="button" id="btnExec" value="適用" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnExec").click(function () {
                $("#<%=Gauge1.ClientID %>").c1radialgauge("option", "marginLeft", parseInt($("#txLeft").val()));
                $("#<%=Gauge1.ClientID %>").c1radialgauge("option", "marginRight", parseInt($("#txRight").val()));
                $("#<%=Gauge1.ClientID %>").c1radialgauge("option", "marginTop", parseInt($("#txTop").val()));
                $("#<%=Gauge1.ClientID %>").c1radialgauge("option", "marginBottom", parseInt($("#txBottom").val()));
            })
        });
	
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<b>MarginLeft</b>、<b>MarginTop</b>、<b>MarginRight</b>、<b>MarginBottom</b> プロパティを使用して、ゲージの余白を設定します。</p>
</asp:Content>
