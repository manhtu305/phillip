﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Position.aspx.vb" Inherits="ControlExplorer.C1RadialGauge.Position" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gauge" TagPrefix="Wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<Wijmo:C1RadialGauge runat="server" ID="Gauge1" Value="100" Max="150" 
		StartAngle="-45" SweepAngle="270" Radius="170">
		
<TickMajor Position="Inside" Factor="2" Visible="True" Offset="-5" Interval="10"></TickMajor>

<TickMinor Position="Inside" Visible="True" Offset="0" Interval="2"></TickMinor>

<Pointer Length="0.8" Width="4" Offset="0.15"></Pointer>

<Labels Offset="-10">
	<LabelStyle Stroke="#556A7C">
		<Fill Color="#556A7C">
		</Fill>
	</LabelStyle>
		</Labels>

<Animation Duration="2000" Easing="EaseOutBack"></Animation>
		<Face>
			<FaceStyle StrokeWidth="0">
			</FaceStyle>
		</Face>
		<Ranges>
			<Wijmo:GaugelRange EndDistance="1" EndValue="50" EndWidth="20" 
				StartDistance="1" StartValue="20" StartWidth="15">
				<RangeStyle Stroke="#BC8A8E">
					<Fill Color="#BC8A8E">
					</Fill>
				</RangeStyle>
			</Wijmo:GaugelRange>
		</Ranges>
		
	</Wijmo:C1RadialGauge>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
	<p>ポインタ</p>
	長さ：<input id="p_length" type="text" />
	　オフセット：<input id="p_offset" type="text" />
	<p>ラベル</p>
	オフセット：<input id="l_offset" type="text" />
	<p>目盛</p>
	オフセット：<input id="t_offset" type="text" />
	　位置：<select id="t_position"><option value="Inside">内側</option>
		<option value="outside">外側</option>
	</select>
	<p>レンジ1</p>
	開始距離：<input id="rs_distance" type="text" />
	　終了距離：<input id="re_distance" type="text" />
	<br />
	<input id="applyOption" type="button" value="適用" />
<script type="text/javascript">
	$(document).ready(function () {
		$("#applyOption").click(function () {
			var option = {}, pointer = {}, label = {}, tick = {}, range1 = {};
			pointer.length = getInputNum("p_length");
			pointer.offset = getInputNum("p_offset");
			label.offset = getInputNum("l_offset");
			tick.offset = getInputNum("t_offset");
			tick.position = $("#t_position").val();
			range1 = $("#<%=Gauge1.ClientID %>").c1radialgauge("option", "ranges")[0];
			range1.startDistance = getInputNum("rs_distance");
			range1.endDistance = getInputNum("re_distance");

			option.pointer = pointer;
			option.labels = label;
			option.tickMinor = tick;
			option.tickMajor = tick;
			option.ranges = [];
			option.ranges[0] = range1;

			$("#<%=Gauge1.ClientID %>").c1radialgauge("option", option);
		});

		var getInputNum = function (id) {
			var val = $("#" + id).val();
			return val ? parseFloat(val) : 1;
		}
	});
	
	</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、<b>TickMajor</b> と <b>TickMinor</b> の <b>Position</b> プロパティを使用して、RadialGauge の主目盛と副目盛の外観と位置をカスタマイズする方法について紹介します。</p>
    <p>Position プロパティは、Inside または Outside に設定することができます。</p>
</asp:Content>
