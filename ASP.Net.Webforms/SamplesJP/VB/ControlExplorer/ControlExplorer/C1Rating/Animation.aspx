﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1Rating.Animation" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Rating" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		function changeAnimation() {
			var val = $("#effectTypes").val();
			$("#<%=C1Rating1.ClientID %>").c1rating({ animation: {
				animated: val
			}
			});
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Rating ID="C1Rating1" runat="server" Split="2" Value="3" Count="5" ResetButton-Hint="レーティングをキャンセルします。">
		<Hint Content="非常に悪い,悪い,平均,良い,非常に良い" />
		<Animation Animated="fade" Duration="500" Easing="Linear" Delay="250" />
	</wijmo:C1Rating>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
	<select id="effectTypes" name="effects">
		<option value="scroll">Scroll</option>
		<option value="blind">Blind</option>
		<option value="bounce">Bounce</option>
		<option value="clip">Clip</option>
		<option value="drop">Drop</option>
		<option value="explode">Explode</option>
		<option value="fade">Fade</option>
		<option value="fold">Fold</option>
		<option value="highlight">Highlight</option>
		<option value="puff">Puff</option>
		<option value="pulsate">Pulsate</option>
		<option value="scale">Scale</option>
		<option value="shake">Shake</option>
		<option value="size">Size</option>
		<option value="slide">Slide</option>
	</select>
	<input id="apply" type="button" onclick="changeAnimation()" value="適用" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
<p>このサンプルでは、C1Rating コントロールのアニメーション効果を設定する方法を紹介します。
このサンプルでは、C1Rating の <b>Animation</b> オプションが使用されています。</p>

</asp:Content>
