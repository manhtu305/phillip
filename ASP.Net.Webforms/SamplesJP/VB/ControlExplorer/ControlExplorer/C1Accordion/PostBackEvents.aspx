﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="PostBackEvents.aspx.vb" Inherits="ControlExplorer.C1Accordion.AutoPostBack" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<h2>
		UpdatePanel を使用した場合：</h2>
	<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="100"
		runat="server">
		<ProgressTemplate>
			<div style="margin-top: 20px;">
				UpdatePanel1 を更新しています...</div>
		</ProgressTemplate>
	</asp:UpdateProgress>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
			<h3>
				<asp:Label ID="Label1" runat="server" Text=""></asp:Label></h3>
			<C1Accordion:C1Accordion ID="C1Accordion1" runat="server" OnSelectedIndexChanged="C1Accordion1_OnSelectedIndexChanged">
				<Panes>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
						<Header>
							手順 1
						</Header>
						<Content>
							<h1>
								手順 1</h1>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
							viverra faucibus. Mauris non vestibulum dui
						</Content>
					</C1Accordion:C1AccordionPane>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
						<Header>
							手順 2
						</Header>
						<Content>
							<h1>
								手順 2</h1>
							Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
							Curae; Vestibulum ante ipsum primis in faucibus.
						</Content>
					</C1Accordion:C1AccordionPane>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
						<Header>
							手順 3
						</Header>
						<Content>
							<h1>
								手順 3</h1>
							Sed facilisis placerat commodo. Nam odio dolor, viverra eu blandit in, hendrerit
							eu arcu. In hac habitasse platea dictumst.
						</Content>
					</C1Accordion:C1AccordionPane>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server" Expanded="True">
						<Header>
							手順 4
						</Header>
						<Content>
							<h1>
								手順 4</h1>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
							viverra faucibus. Mauris non vestibulum dui.
						</Content>
					</C1Accordion:C1AccordionPane>
				</Panes>
			</C1Accordion:C1Accordion>
		</ContentTemplate>
	</asp:UpdatePanel>
    <br />
	<h2>
		UpdatePanel を使用しない場合：</h2>
	<h3>
		<asp:Label ID="Label2" runat="server" Text=""></asp:Label></h3>
	<C1Accordion:C1Accordion ID="C1Accordion2" runat="server" OnSelectedIndexChanged="C1Accordion2_OnSelectedIndexChanged">
		<Panes>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane1" runat="server">
				<Header>
					手順 1
				</Header>
				<Content>
					<h1>
						手順 1</h1>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
					viverra faucibus. Mauris non vestibulum dui
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane2" runat="server">
				<Header>
					手順 2
				</Header>
				<Content>
					<h1>
						手順 2</h1>
					Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
					Curae; Vestibulum ante ipsum primis in faucibus.
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane3" runat="server">
				<Header>
					手順 3
				</Header>
				<Content>
					<h1>
						手順 3</h1>
					Sed facilisis placerat commodo. Nam odio dolor, viverra eu blandit in, hendrerit
					eu arcu. In hac habitasse platea dictumst.
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane4" runat="server" Expanded="True">
				<Header>
					手順 4
				</Header>
				<Content>
					<h1>
						手順 4</h1>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
					viverra faucibus. Mauris non vestibulum dui.
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<p>
		&nbsp;</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
        このサンプルでは、ページの全体ポストバック時と部分ポストバック時における <strong>C1Accordion</strong> コントロールの動作を示します。
	</p>
	<p>
	    サンプルページにはアコーディオン コントロールが2つあります。
	    1番目のアコーディオン コントロールは UpdatePanel 内に配置されており、そのサーバー側の <strong>SelectedIndexChanged </strong>イベントハンドラが「C1Accordion1_OnSelectedIndexChanged」に設定されています。
	    2番目のアコーディオン コントロールは UpdatePanel 外に配置されており、そのサーバー側の <strong>SelectedIndexChanged </strong>イベントハンドラが「C1Accordion2_OnSelectedIndexChanged」に設定されています。
	</p>
	<p>
    	<strong>SelectedIndexChanged </strong>イベントハンドラが追加されると、選択されているペインが変更される際に <strong>C1Accordion </strong>がサーバーへポストバックします。<strong> SelectedIndexChanged</strong> 
        イベントを使用せずにポストバックを発生させるには、<strong>AutoPostBack </strong>プロパティを True に設定します。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
