'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.Accordion

    Partial Public Class DataBinding

        '''<summary>
        '''C1Accordion1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Accordion1 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1Accordion

        '''<summary>
        '''AccessDataSource1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents AccessDataSource1 As Global.System.Web.UI.WebControls.AccessDataSource
    End Class
End Namespace
