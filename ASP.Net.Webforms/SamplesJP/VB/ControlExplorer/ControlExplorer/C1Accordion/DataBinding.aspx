﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.vb" Inherits="ControlExplorer.Accordion.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion" TagPrefix="C1Accordion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
		<C1Accordion:C1Accordion runat="server"
			ID="C1Accordion1"
			DataSourceID="AccessDataSource1">
		</C1Accordion:C1Accordion>

		<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
			DataFile="~/App_Data/Nwind_ja.mdb" 
			SelectCommand="SELECT [CategoryName], [Description] FROM [Categories]">
		</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
        このサンプルは、<strong>C1Accordion</strong> コントロールでデータ連結する方法を示します。
	</p>
	<p>
        このサンプルでは、<strong>C1Accordion</strong> コントロールにより <strong>Header</strong> と <strong>Content</strong> データフィールドが自動的に指定されます。
    </p>
	<p>
        <strong>HeaderField</strong> と <strong>ContentField</strong> プロパティを使用すると、Header と Content を指定したフィールドと連結することができます。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
