﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="ControlExplorer.Accordion.Accordion_Overview" CodeBehind="Overview.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Accordion:C1Accordion runat="server"
		ID="C1Accordion1">
		<Panes>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane1" runat="server">
				<Header>
					手順 1
				</Header>
				<Content>
					<h1>
						手順 1</h1>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
					viverra faucibus. Mauris non vestibulum dui
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane2" runat="server">
				<Header>
					手順 2
				</Header>
				<Content>
					<h1>
						手順 2</h1>
					Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
					Curae; Vestibulum ante ipsum primis in faucibus.
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane3" runat="server">
				<Header>
					手順 3
				</Header>
				<Content>
					<h1>
						手順 3</h1>
					Sed facilisis placerat commodo. Nam odio dolor, viverra eu blandit in, hendrerit
					eu arcu. In hac habitasse platea dictumst.
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane4" runat="server" Expanded="True">
				<Header>
					手順 4
				</Header>
				<Content>
					<h1>
						手順 4</h1>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
					viverra faucibus. Mauris non vestibulum dui.
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルは、<strong>C1Accordion </strong>の既定の動作を示します。サンプルでは、最初のアコーディオン ペインは既定で展開状態（<strong>SelectedIndex
		</strong>プロパティの値が 0）になっています。
    </p>
	<p>
		異なるアコーディオン ペインを展開状態に設定するには、<strong>SelectedIndex
		</strong>プロパティをそのペインのインデックスに設定する必要があります。
	</p>
    <p>
    	すべてのペインを縮小状態にするには、<strong>SelectedIndex </strong>プロパティの値を -1 に設定します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
