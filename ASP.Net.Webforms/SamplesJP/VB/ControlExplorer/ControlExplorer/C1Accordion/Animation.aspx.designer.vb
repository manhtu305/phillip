'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1Accordion

    Partial Public Class Animation

        '''<summary>
        '''C1Accordion1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Accordion1 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1Accordion

        '''<summary>
        '''C1AccordionPane1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane1 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1AccordionPane2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane2 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1AccordionPane3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane3 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1Accordion2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Accordion2 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1Accordion

        '''<summary>
        '''C1AccordionPane4 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane4 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1AccordionPane5 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane5 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1AccordionPane6 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane6 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1Accordion3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Accordion3 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1Accordion

        '''<summary>
        '''C1AccordionPane7 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane7 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1AccordionPane8 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane8 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''C1AccordionPane9 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1AccordionPane9 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane
    End Class
End Namespace
