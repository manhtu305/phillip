﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ClientModel.aspx.vb" Inherits="ControlExplorer.C1Accordion.ClientObjectModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<%@ Register Src="../ClientLogger.ascx" TagName="ClientLogger" TagPrefix="ClientLogger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<ClientLogger:ClientLogger ID="ClientLogger1" runat="server" />
	<C1Accordion:C1Accordion runat="server" 
		ID="C1Accordion1" 		
		OnClientSelectedIndexChanged="onClientSelectedIndexChanged">
		<Panes>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane1" runat="server">
				<Header>
					手順 1
				</Header>
				<Content>
					<h1>
						手順 1</h1>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
					viverra faucibus. Mauris non vestibulum dui
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane2" runat="server">
				<Header>
					手順 2
				</Header>
				<Content>
					<h1>
						手順 2</h1>
					Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
					Curae; Vestibulum ante ipsum primis in faucibus.
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane3" runat="server">
				<Header>
					手順 3
				</Header>
				<Content>
					<h1>
						手順 3</h1>
					Sed facilisis placerat commodo. Nam odio dolor, viverra eu blandit in, hendrerit
					eu arcu. In hac habitasse platea dictumst.
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane4" runat="server" Expanded="True">
				<Header>
					手順 4
				</Header>
				<Content>
					<h1>
						手順 4</h1>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus ac nibh
					viverra faucibus. Mauris non vestibulum dui.
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
    このサンプルは、クライアント側スクリプトでアコーディオンの動作や外観を変更する方法を示します。
    </p>
    <p>サンプルで使用されているクライアント側オプションは次の通りです。</p>
    <ul>
    <li><strong>requireOpenedPane</strong> - 少なくとも 1 つのペインを必ず展開された状態にするかどうかを指定します。</li>
    <li><strong>selectedIndex</strong> - 現在展開されているペインのインデックス。</li>
    </ul>
    <p>サンプルで使用されているクライアント側メソッドは次の通りです。</p>
    <ul>
        <li><strong>add(header, content)</strong> - アコーディオンの末尾にペインを追加します。</li>
        <li><strong>insert(index, header, content) </strong>- 指定したインデックスにアコーディオンペインを挿入します。</li>
        <li><strong>count() </strong>- アコーディオンペインの数を取得します。</li>
        <li><strong>removeAt(index) </strong>- 指定したインデックスのアコーディオンペインを削除します。</li>
        <li><strong>clear() </strong>- 全てのアコーディオンペインを削除します。</li>
    </ul>
    <p>
        <strong>OnClientSelectedIndexChanged</strong> プロパティはクライアント側の <strong>selectedIndexChanged</strong>を処理するために使用されます。
	</p>
    <p>
        下記コードは、クライアント側スクリプトで新規のアコーディオン ペインを追加する方法を示します。
    </p>
    <p>
<pre class="controldescription-code">
$("#&lt;%=C1Accordion1.ClientID%&gt;").c1accordion("add", "新規ヘッダー", "新規コンテンツ");
</pre>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<p>
    展開されているインデックス：<input type="text" id="selectedIndexInput" style="width:30px;" onclick="return false;" />
    <input type="button" value="適用" onclick="applySelectedIndex(); return false;"/>
</p>
<p>
    <label>少なくとも 1 つのペインを必ず展開：<input type="checkbox" id="requireOpenedPaneCheckBox" /> </label>
</p>
<p>
	<input type="button" value="新規ペインの追加" onclick="addNewPane(); return false;"/>
	&nbsp;	
	<input type="button" value="新規ペインの挿入" onclick="insertNewPane(); return false;"/>
	&nbsp;
	<input type="button" value="ペイン数の表示" onclick="showPanesCount(); return false;" />
	&nbsp;
	<input type="button" value="全ペインの削除" onclick="$('#<%=C1Accordion1.ClientID%>').c1accordion('clear'); return false;"/>
</p>
<p>
	<input type="text" id="paneIndexToRemove" value="0" style="width:30px;" />
	<input type="button" value="インデックスのペインを削除" 
        onclick="removePaneByIndex(); return false;" />
</p>

<script language="javascript" type="text/javascript">
	function onClientSelectedIndexChanged(ev, args) {
		log.message("selectedIndex が " + args.newIndex + "に変更されました。");
	}
	var _dynPanesCount = 0;
	function addNewPane() {
		$("#<%=C1Accordion1.ClientID%>").c1accordion("add", "新規ヘッダー " + ++_dynPanesCount, "新規コンテンツ " + _dynPanesCount);
	}
	function insertNewPane() {
		$("#<%=C1Accordion1.ClientID%>").c1accordion("insert", 0, "新規ヘッダー " + ++_dynPanesCount, "新規コンテンツ " + _dynPanesCount);
	}
	function showPanesCount() {
		alert($("#<%=C1Accordion1.ClientID%>").c1accordion("count"));
	}
	function removePaneByIndex() {
		var index = parseInt($("#paneIndexToRemove").val(), 10);
		if ((index || index === 0) && index >= 0 && index < $("#<%=C1Accordion1.ClientID%>").c1accordion("count")) {
			$("#<%=C1Accordion1.ClientID%>").c1accordion("removeAt", index);
			log.message("アコーディオンペイン " + index + " が削除されました。");
		} else {
			alert("不正なインデックス： " + $("#paneIndexToRemove").val());
		}
	}
	$(document).ready(function () {

		$("#requireOpenedPaneCheckBox")[0].checked = $("#<%=C1Accordion1.ClientID%>").c1accordion("option", "requireOpenedPane");
		$("#selectedIndexInput").val($("#<%=C1Accordion1.ClientID%>").c1accordion("option", "selectedIndex"));

		$("#requireOpenedPaneCheckBox").bind("change", function () {
			var requireOpenedPane = $("#requireOpenedPaneCheckBox")[0].checked;
			$("#<%=C1Accordion1.ClientID%>").c1accordion("option", "requireOpenedPane", requireOpenedPane);
			log.message("requireOpenedPane プロパティが " + requireOpenedPane + " に変更されました。");
		});
	});


	function applySelectedIndex() {
		var index = parseInt($("#selectedIndexInput").val(), 10);
		if ((index || index === 0) && index >= 0 && index < $("#<%=C1Accordion1.ClientID%>").c1accordion("count")) {
			$("#<%=C1Accordion1.ClientID%>").c1accordion("option", "selectedIndex", index);
			log.message("selectedIndex プロパティが " + index + " に変更されました。");
		} else {
			alert("不正なインデックス： " + $("#selectedIndexInput").val());
		}
	}
</script>
</asp:Content>
