﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ExpandDirection.aspx.vb" Inherits="ControlExplorer.C1Accordion.ExpandDirection" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Accordion:C1Accordion ID="C1Accordion1" runat="server">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
				<Header>1</Header>
				<Content>
					<p>
						Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque.
						Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a
						nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada.
						Vestibulum a velit eu ante scelerisque vulputate.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
				<Header>2</Header>
				<Content>
					<p>
						Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus
						hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum
						tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
				<Header>3</Header>
				<Content>
					<p>
						Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus
						pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque
						semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam
						nisi, eu iaculis leo purus venenatis dui.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server">
				<Header>4</Header>
				<Content>
					<p>
						Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada
						fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci luctus et
						ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.
					</p>
					<p>
						Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus. Class
						aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルは、クライアント側でコンテンツ領域の展開方向を変更する方法を示します。
	</p>
	<p>
		アコーディオン ペインの展開方向を変更するには、<b>expandDirection</b> オプションを使用します。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<script type="text/javascript" language="javascript">
    function setExpandDirection(direction) {
        // 垂直アニメーションで割り当てた固定のパディング値をクリアします。
        // サーバー側の ExpandDirection を使用する際には必要ありません。
        $("#<%=C1Accordion1.ClientID%> > .ui-accordion-content")
				.css("paddingTop", "")
				.css("paddingBottom", "");
        // expandDirection オプションを変更します。
        $("#<%=C1Accordion1.ClientID%>")
    			.c1accordion("option", "expandDirection", direction);
    }
</script>
	<fieldset class="radio">
		<legend>展開方向の選択：</legend>
		<label><input type="radio" name="accordionExpanDirection" value="top" onclick="setExpandDirection('top');" />上　</label>
		<label><input type="radio" name="accordionExpanDirection" value="right" onclick="setExpandDirection('right');" />右　</label>
		<label><input type="radio" name="accordionExpanDirection" value="bottom" onclick="setExpandDirection('bottom');" checked="checked" />下　</label>
		<label><input type="radio" name="accordionExpanDirection" value="left" onclick="setExpandDirection('left');" />左</label>
	</fieldset>
</asp:Content>
