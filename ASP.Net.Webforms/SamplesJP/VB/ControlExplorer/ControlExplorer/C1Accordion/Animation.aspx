﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1Accordion.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h3>
		アニメーションあり</h3>
	<C1Accordion:C1Accordion ID="C1Accordion1" runat="server" Animated-Effect="easeInOutCirc">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
				<Header>
					1
				</Header>
				<Content>
					<p>
						Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque.
						Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a
						nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada.
						Vestibulum a velit eu ante scelerisque vulputate.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
				<Header>
					2
				</Header>
				<Content>
					<p>
						Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus
						hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum
						tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
				<Header>
					3
				</Header>
				<Content>
					<p>
						Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus
						pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque
						semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam
						nisi, eu iaculis leo purus venenatis dui.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<br />
	<h3>
		アニメーションなし
	</h3>
	<C1Accordion:C1Accordion ID="C1Accordion2" runat="server" Animated-Disabled="True">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server">
				<Header>
					1
				</Header>
				<Content>
					<p>
						Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque.
						Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a
						nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada.
						Vestibulum a velit eu ante scelerisque vulputate.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane5" runat="server">
				<Header>
					2
				</Header>
				<Content>
					<p>
						Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus
						hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum
						tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane6" runat="server">
				<Header>
					3
				</Header>
				<Content>
					<p>
						Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus
						pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque
						semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam
						nisi, eu iaculis leo purus venenatis dui.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<br />
	<h3>
		カスタム アニメーション</h3>
	<C1Accordion:C1Accordion ID="C1Accordion3" runat="server" Animated-Effect="custom1">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane7" runat="server">
				<Header>
					1
				</Header>
				<Content>
					<p>
						Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque.
						Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a
						nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada.
						Vestibulum a velit eu ante scelerisque vulputate.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane8" runat="server">
				<Header>
					2
				</Header>
				<Content>
					<p>
						Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus
						hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum
						tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane9" runat="server">
				<Header>
					3
				</Header>
				<Content>
					<p>
						Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus
						pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque
						semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam
						nisi, eu iaculis leo purus venenatis dui.
					</p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
				this.slide(options, {
					easing: options.down ? "easeOutBounce" : "swing",
					duration: options.down ? 1000 : 200
				});
			}
		});
	</script>
	<br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルは、<strong>C1Accordion</strong> コントロールでアニメーション効果を使用する方法を示します。
    </p>
	<p>
		<strong>Animated </strong>プロパティを使用すると、アニメーション効果を変更できます。
    </p>
	<p>
        1 番目のアコーディオンでは、<strong>Animated.Effect </strong>プロパティを easeInOutCirc に設定して、イージング効果を変更しています。
    </p>
	<p>
		2 番目のアコーディオンでは、<strong>Animated-Disabled</strong> プロパティを True に設定してアニメーション効果を無効に設定しています。
    </p>
	<p>
		3 番目のアコーディオンでは、<strong>Animated.Effect</strong> プロパティを custom1 に設定し、カスタム アニメーション効果を使用しています。
    </p>
    <p>
		Animation 関数で使用可能なオプションパラメータは以下の通りです。
	</p>
	<ul>
		<li><strong>down </strong>– True の場合、展開するペインのインデックスが縮小するペインのインデックスより大きいということを示す。</li>
		<li><strong>horizontal </strong>– True の場合、アコーディオンが水平方向になっていることを示す
			(expandDirection が left や right の場合)。</li>
		<li><strong>rightToLeft</strong> – True の場合、コンテンツ要素がヘッダ要素の前に配置されていることを示す(expandDirection が top や left の場合)。 </li>
		<li><strong>toShow </strong>– 表示するコンテンツ要素を含む jQuery オブジェクト。
		</li>
		<li><strong>toHide </strong>– 非表示にするコンテンツ要素を含む jQuery オブジェクト。
		</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
