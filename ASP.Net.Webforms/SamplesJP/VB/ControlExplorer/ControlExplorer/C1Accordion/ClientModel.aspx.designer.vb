'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1Accordion

    Partial Public Class ClientObjectModel

        '''<summary>
        '''ClientLogger1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ClientLogger1 As Global.ControlExplorer.ClientLogger

        '''<summary>
        '''C1Accordion1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Accordion1 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1Accordion

        '''<summary>
        '''Accordion1Pane1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Accordion1Pane1 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''Accordion1Pane2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Accordion1Pane2 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''Accordion1Pane3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Accordion1Pane3 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane

        '''<summary>
        '''Accordion1Pane4 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Accordion1Pane4 As Global.C1.Web.Wijmo.Controls.C1Accordion.C1AccordionPane
    End Class
End Namespace
