Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1Accordion
	Public Partial Class AutoPostBack
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub
		Protected Sub C1Accordion1_OnSelectedIndexChanged(sender As Object, e As EventArgs)
			Label1.Text = String.Format("(C1Accordion1) SelectedIndex が {0} に変更されました。", C1Accordion1.SelectedIndex)
		End Sub

		Protected Sub C1Accordion2_OnSelectedIndexChanged(sender As Object, e As EventArgs)
			Label2.Text = String.Format("(C1Accordion2) SelectedIndex が {0} に変更されました。", C1Accordion2.SelectedIndex)
		End Sub

	End Class
End Namespace
