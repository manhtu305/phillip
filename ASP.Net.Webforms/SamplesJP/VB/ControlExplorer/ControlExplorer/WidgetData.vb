Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Xml.Linq

Namespace ControlExplorer
	Public Class WidgetData

		Private _ele As XElement
		Private ReadOnly Property Ele() As XElement
			Get
				If _ele Is Nothing Then
					_ele = XElement.Load(HttpContext.Current.Server.MapPath("~/ControlList.xml"))
				End If
				Return _ele
			End Get
		End Property


		Private Function GetAllWidgetData() As IEnumerable(Of XElement)
            Dim rootwidgets = From el In Ele.Elements("Control") Where el.Element("action") IsNot Nothing Select el
            Dim childWidgets = From el In Ele.Descendants("ChildControl") Select el
			Return rootwidgets.Union(childWidgets).OrderBy(Function(p) p.Attribute("name").Value)
		End Function

		Public Function GetFavoriteWidgets() As List(Of WidgetEntity)
			Dim widgets As New List(Of WidgetEntity)()
			Dim widget As WidgetEntity
			Dim res = GetAllWidgetData().Where(Function(p) p.Attribute("isFavorite") IsNot Nothing AndAlso p.Attribute("isFavorite").Value = "true")
            For Each obj In res
                widget = New WidgetEntity()
                widget.WidgetName = obj.Attribute("name").Value
                widget.WidgetHref = "~/C1" & widget.WidgetName & "/Overview.aspx"
                widget.Title = If(obj.Attribute("text") IsNot Nothing, obj.Attribute("text").Value, widget.WidgetName)
                widget.Icon = "~/explore/css/images/icons/widget/" & widget.WidgetName & ".png"
                'widget.Icon = "explore/css/images/icons/widget/accordion.png";
                widgets.Add(GetWidgetEntity(obj, True))
            Next
			Return widgets
		End Function

		Public Function GetAllWidgets() As List(Of WidgetEntity)
			Dim widgets As New List(Of WidgetEntity)()
			Dim widget As WidgetEntity
			Dim res = GetAllWidgetData()
            For Each obj In res
                widget = New WidgetEntity()
                widget.WidgetName = obj.Attribute("name").Value
                widget.WidgetHref = "~/C1" & widget.WidgetName & "/Overview.aspx"
                widget.Title = If(obj.Attribute("text") IsNot Nothing, obj.Attribute("text").Value, widget.WidgetName)
                widget.Icon = "~/explore/css/images/icons/widget/" & widget.WidgetName & ".png"
                'widget.Icon = "explore/css/images/icons/widget/accordion.png";
                widgets.Add(GetWidgetEntity(obj, False))
            Next
			Return widgets
		End Function

		Public Function GetWidgetEntity(obj As XElement, bigIcon As Boolean) As WidgetEntity
			Dim widget = New WidgetEntity()
			widget.WidgetName = obj.Attribute("name").Value
			widget.WidgetHref = "~/C1" & widget.WidgetName & "/Overview.aspx"
			widget.Title = If(obj.Attribute("text") IsNot Nothing, obj.Attribute("text").Value, widget.WidgetName)
			widget.Icon = "~/explore/css/images/icons/widget/" & widget.WidgetName & ".png"
			If bigIcon Then
				'widget.Icon = "explore/css/images/icons/widget/accordion.png";
				widget.Icon = "~/explore/css/images/icons/widget/" & widget.WidgetName & ".png"
			Else
				'widget.Icon = "explore/css/images/icons/widget/accordion.png";
				widget.Icon = "~/explore/css/images/icons/widget/" & widget.WidgetName & ".png"
			End If

			Return widget
		End Function

	End Class



	Public Class WidgetEntity
		Public Property WidgetName() As String
			Get
				Return m_WidgetName
			End Get
			Set
				m_WidgetName = Value
			End Set
		End Property
		Private m_WidgetName As String

		Public Property Icon() As String
			Get
				Return m_Icon
			End Get
			Set
				m_Icon = Value
			End Set
		End Property
		Private m_Icon As String

		Public Property Title() As String
			Get
				Return m_Title
			End Get
			Set
				m_Title = Value
			End Set
		End Property
		Private m_Title As String

		Public Property WidgetHref() As String
			Get
				Return m_WidgetHref
			End Get
			Set
				m_WidgetHref = Value
			End Set
		End Property
		Private m_WidgetHref As String
	End Class
End Namespace
