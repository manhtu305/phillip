﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBind.aspx.vb" Inherits="ControlExplorer.C1BubbleChart.DataBind" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1BubbleChart runat="server" ID="C1BubbleChart1" DataSourceID="AccessDataSource1" Height="475" Width = "756">
<Animation Duration="500" Easing="EaseOutElastic"></Animation>

<Footer Compass="South" Visible="False"></Footer>

<Axis>
	<X>
		<TextStyle Rotation="-45">
		</TextStyle>
	</X>
<Y Visible="False" Compass="West">
<Labels TextAlign="Center"></Labels>

<GridMajor Visible="True"></GridMajor>
</Y>
</Axis>
<DataBindings>
<wijmo:C1BubbleChartBinding XField="CategoryName" XFieldType="String" YField="売上" YFieldType="Number" Y1Field="CT" />
</DataBindings>
</wijmo:C1BubbleChart>

<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/Nwind_ja.mdb" 
		SelectCommand="select CategoryName, sum(ProductSales) as 売上, count(1) as CT from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><strong>C1BubbleChart </strong>は、サーバー上の外部データソースとの連結機能を提供します。</p>
	<p>
		<b>DataSourceID</b> や <b>DataSource</b> および <b>DataBindings</b> を設定してデータ連結機能を有効にできます。
		下記プロパティを使用することで X 値と Y 値を指定したデータフィールドに連結できます。</p>
	<ul>
		<li><strong>DataSourceID </strong>- データソース ID を指定します。</li>
		<li><strong>DataBindings </strong>- 系列の連結を指定します。</li>
		<li><strong>C1BubbleChartBinding.XField </strong>- X を指定したフィールド名と連結します。</li>
		<li><strong>C1BubbleChartBinding.XFieldType</strong> - XType を指定したフィールド名と連結します。</li>
		<li><strong>C1BubbleChartBinding.YField </strong>- Y を指定したフィールド名と連結します。</li>
		<li><strong>C1BubbleChartBinding.YFieldType </strong>- YType を指定したフィールド名と連結します。</li>
		<li><strong>C1BubbleChartBinding.Y1Field </strong>- Y1 を指定したフィールド名と連結します。</li>
	</ul>
	<p>DataBindings は、<b>C1BubbleChartBindings</b> のインスタンスを持つコレクションです。  
		<strong>C1BubbleChartBinding </strong>には以下のプロパティがあります。</p>
	<ul>
		<li><strong>DataMember </strong>- データソースに1つ以上のリストがある場合、データリストの名前を指定します。</li>
		<li><strong>HintField </strong>- ヒント内容を指定したフィールドと連結します。</li>
	</ul>
	<p><strong>HintField </strong>が設定されている場合、マウスを系列上に移動すると、系列と同じインデックスのヒント値を表示します。</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
