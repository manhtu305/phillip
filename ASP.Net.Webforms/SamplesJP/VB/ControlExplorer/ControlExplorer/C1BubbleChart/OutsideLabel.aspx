﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="OutsideLabel.aspx.vb" Inherits="ControlExplorer.C1BubbleChart.OutsideLabel" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    function hint() {
        return this.data.label + ' x:' + this.x + ',y:' + this.y + ",y1:" + this.data.y1;
    }

    $(document).ready(function () {

        $("#dpcompass").change(function () {
            $("#<%=BubbleChart1.ClientID %>").c1bubblechart("option", "chartLabel", { compass: $(this).val() });
        });

    });

</script>

<wijmo:C1BubbleChart runat="server" ID="BubbleChart1" MinimumSize="3" MaximumSize="15" Height="450" Width = "756">
	<Axis>
		<X Text=""></X>
		<Y Text="ハードウェア数" Compass="West"></Y>
	</Axis>
	<Hint>
		<Content Function="hint" />
	</Hint>
	<Header Text="ハードウェア分布"></Header>
	<ChartLabel Position="Outside" Compass="North"></ChartLabel>
	<SeriesList>
		<wijmo:BubbleChartSeries Label="西エリア" LegendEntry="true">
			<Data>
				<X>
					<Values>
						<wijmo:ChartXData DoubleValue="5" />
						<wijmo:ChartXData DoubleValue="14" />
						<wijmo:ChartXData DoubleValue="20" />
						<wijmo:ChartXData DoubleValue="18" />
						<wijmo:ChartXData DoubleValue="22" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="5500" />
						<wijmo:ChartYData DoubleValue="12200" />
						<wijmo:ChartYData DoubleValue="60000" />
						<wijmo:ChartYData DoubleValue="24400" />
						<wijmo:ChartYData DoubleValue="32000" />
					</Values>
				</Y>
				<Y1 DoubleValues="3,12,33,10,42" />
			</Data>
		</wijmo:BubbleChartSeries>
	</SeriesList>
</wijmo:C1BubbleChart>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
<label>位置を選択</label>
				<select id="dpcompass">
					<option>east</option>
					<option>west</option>
					<option selected="selected">north</option>
					<option>south</option>
				</select>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルは、<strong>Position</strong> プロパティを Outside に設定してグラフラベルを外側に配置し、<strong>Compass</strong> プロパティを使用して位置（North、South、East、West）を指定する方法を示します。</p>
</asp:Content>
