﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ChartMarker.aspx.vb" Inherits="ControlExplorer.C1BubbleChart.ChartMarker" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
	function hint () {
		return 'x=' + this.x + '、y=' + this.y + "、y1=" + this.data.y1;
	}
</script>
<wijmo:C1BubbleChart runat="server" ID="BubbleChart1" MinimumSize="3" MaximumSize="15" Height="475" Width = "756">
	<Axis>
		<X Text=""></X>
		<Y Text="ハードウェア数" Compass="West"></Y>
	</Axis>
	<Hint>
		<Content Function="hint" />
	</Hint>
	<Header Text="ハードウェア分布"></Header>
	<SeriesList>
		<wijmo:BubbleChartSeries Label="西エリア" LegendEntry="true">
			<Data>
				<X>
					<Values>
						<wijmo:ChartXData DoubleValue="5" />
						<wijmo:ChartXData DoubleValue="14" />
						<wijmo:ChartXData DoubleValue="20" />
						<wijmo:ChartXData DoubleValue="18" />
						<wijmo:ChartXData DoubleValue="22" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="5500" />
						<wijmo:ChartYData DoubleValue="12200" />
						<wijmo:ChartYData DoubleValue="60000" />
						<wijmo:ChartYData DoubleValue="24400" />
						<wijmo:ChartYData DoubleValue="32000" />
					</Values>
				</Y>
				<Y1 DoubleValues="3,12,33,10,42" />
			</Data>
			<Markers Type="Tri"></Markers>
		</wijmo:BubbleChartSeries>
	</SeriesList>
</wijmo:C1BubbleChart>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、SeriesList の <b>Markers.Type</b> プロパティを使用してマーカー種別を定義しています。
    マーカー種別は、円、三角形、逆三角形、正方形、ひし形、X 印に設定できます。 </p>
</asp:Content>
