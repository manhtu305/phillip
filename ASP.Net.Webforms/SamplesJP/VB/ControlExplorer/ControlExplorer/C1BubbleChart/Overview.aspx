﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1BubbleChart.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1BubbleChart runat="server" ID="C1BubbleChart1" Height="475" Width="756">
		<SeriesList>
			<wijmo:BubbleChartSeries Label="西エリア">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DoubleValue="5" />
							<wijmo:ChartXData DoubleValue="14" />
							<wijmo:ChartXData DoubleValue="20" />
							<wijmo:ChartXData DoubleValue="18" />
							<wijmo:ChartXData DoubleValue="22" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="5500" />
							<wijmo:ChartYData DoubleValue="12200" />
							<wijmo:ChartYData DoubleValue="60000" />
							<wijmo:ChartYData DoubleValue="24400" />
							<wijmo:ChartYData DoubleValue="32000" />
						</Values>
					</Y>
					<Y1 DoubleValues="3,12,33,10,42" />
				</Data>
			</wijmo:BubbleChartSeries>
		</SeriesList>
	</wijmo:C1BubbleChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルは、単純なバブルチャートを作成する方法を示しています。バブルチャートでは3次型のデータを表示できます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
