Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1Pager
	Public Partial Class Overview
		Inherits System.Web.UI.Page
		Protected Sub C1Pager1_PageIndexChanged(sender As Object, e As EventArgs)
			GridView1.PageIndex = C1Pager1.PageIndex
			GridView1.DataBind()
		End Sub

		Protected Sub GridView1_DataBound(sender As Object, e As EventArgs)
			C1Pager1.PageCount = GridView1.PageCount
		End Sub
	End Class
End Namespace
