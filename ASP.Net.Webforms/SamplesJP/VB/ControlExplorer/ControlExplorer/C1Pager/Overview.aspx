﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Pager.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Pager" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManger1"></asp:ScriptManager>
    
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <wijmo:C1Pager runat="server" ID="C1Pager1" AutoPostBack="true" Mode="NumericFirstLast"
                OnPageIndexChanged="C1Pager1_PageIndexChanged" />
            
	        <asp:GridView Width="100%" runat="server" ID="GridView1" DataSourceID="AccessDataSource1" AllowPaging="true"
	            CssClass="ui-widget" ondatabound="GridView1_DataBound">
	            <HeaderStyle CssClass="ui-widget-header" />
	            <RowStyle CssClass="ui-widget-content" />
	            <PagerSettings Visible="false" />
	        </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/Nwind_ja.mdb" 
		SelectCommand="SELECT [OrderID] AS 注文コード, [ProductID] AS 商品コード, [UnitPrice] AS 単価, [Quantity] AS 数量 FROM [Order Details]">
	</asp:AccessDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><strong>C1Pager</strong> では、ボタンをクリックして要素やページをスクロールできます。</p>
    <p>このサンプルでは、数値、［最初］、［最後］ボタンを持つ <strong>C1Pager</strong> コントロールを表示します。</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
