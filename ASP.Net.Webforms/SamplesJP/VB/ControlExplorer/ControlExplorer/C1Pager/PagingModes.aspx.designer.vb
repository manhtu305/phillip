'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Pager


	Public Partial Class Modes

		''' <summary>
		''' C1PagerNextPrevious コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNextPrevious As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNextPrevious2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNextPrevious2 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNextPrevious3 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNextPrevious3 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNextPreviousFirstLast コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNextPreviousFirstLast As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerrNextPreviousFirstLast2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerrNextPreviousFirstLast2 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNextPreviousFirstLast3 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNextPreviousFirstLast3 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNumeric コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNumeric As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNumericFirstLast コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNumericFirstLast As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNumericFirstLast2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNumericFirstLast2 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' C1PagerNumericFirstLast3 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1PagerNumericFirstLast3 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager
	End Class
End Namespace
