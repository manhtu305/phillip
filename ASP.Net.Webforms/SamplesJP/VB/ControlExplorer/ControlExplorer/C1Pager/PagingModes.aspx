﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="PagingModes.aspx.vb" Inherits="ControlExplorer.C1Pager.Modes" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Pager" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .style1
        {
            font-style: normal;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h6>NextPrevious</h6>
    <wijmo:C1Pager runat="server" ID="C1PagerNextPrevious" PageCount="150" PageIndex="40" Mode="NextPrevious" />
    <wijmo:C1Pager runat="server" ID="C1PagerNextPrevious2" PageCount="150" PageIndex="40" Mode="NextPrevious"
        FirstPageClass="" PreviousPageClass="" NextPageClass="" LastPageClass="" />
    <wijmo:C1Pager runat="server" ID="C1PagerNextPrevious3" PageCount="150" PageIndex="40" Mode="NextPrevious"
        FirstPageClass="" PreviousPageClass="" NextPageClass="" LastPageClass="" FirstPageText="<<"
        PreviousPageText="<" NextPageText=">" LastPageText=">>" />

    <h6>NextPreviousFirstLast</h6>
    <wijmo:C1Pager runat="server" ID="C1PagerNextPreviousFirstLast" PageCount="150" PageIndex="40" Mode="NextPreviousFirstLast" />
    <wijmo:C1Pager runat="server" ID="C1PagerrNextPreviousFirstLast2" PageCount="150" PageIndex="40" Mode="NextPreviousFirstLast"
        FirstPageClass="" PreviousPageClass="" NextPageClass="" LastPageClass="" />
    <wijmo:C1Pager runat="server" ID="C1PagerNextPreviousFirstLast3" PageCount="150" PageIndex="40" Mode="NextPreviousFirstLast"
        FirstPageClass="" PreviousPageClass="" NextPageClass="" LastPageClass="" FirstPageText="<<" PreviousPageText="<"
        NextPageText=">" LastPageText=">>" />

    <h6>Numeric</h6>
    <wijmo:C1Pager runat="server" ID="C1PagerNumeric" PageCount="150" PageIndex="40" Mode="Numeric" />

    <h6>NumericFirstLast</h6>
    <wijmo:C1Pager runat="server" ID="C1PagerNumericFirstLast" PageCount="150" PageIndex="40" Mode="NumericFirstLast" />
    <wijmo:C1Pager runat="server" ID="C1PagerNumericFirstLast2" PageCount="150" PageIndex="40" Mode="NumericFirstLast"
        FirstPageClass="" PreviousPageClass="" NextPageClass="" LastPageClass="" />
    <wijmo:C1Pager runat="server" ID="C1PagerNumericFirstLast3" PageCount="150" PageIndex="40" Mode="NumericFirstLast"
        FirstPageClass="" PreviousPageClass="" NextPageClass="" LastPageClass="" FirstPageText="<<" PreviousPageText="<"
        NextPageText=">" LastPageText=">>" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
       このサンプルでは、<b>Mode</b> プロパティを設定して、<strong>C1Pager</strong> コントロールを様々な方法で表示します。</p>
    <p>
       Mode プロパティに設定可能な値は次のとおりです。</p>
    <ul>
        <li><strong>NextPreviousFirstLast</strong></li>
        <li><strong>Numeric</strong></li>
        <li><strong>Previous</strong></li>
        <li><strong>NextPrevious</strong></li>
    </ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
