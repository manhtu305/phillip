'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Pager


	Public Partial Class Overview

		''' <summary>
		''' ScriptManger1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected ScriptManger1 As Global.System.Web.UI.ScriptManager

		''' <summary>
		''' UpdatePanel1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdatePanel1 As Global.System.Web.UI.UpdatePanel

		''' <summary>
		''' C1Pager1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Pager1 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager

		''' <summary>
		''' GridView1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected GridView1 As Global.System.Web.UI.WebControls.GridView

		''' <summary>
		''' AccessDataSource1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected AccessDataSource1 As Global.System.Web.UI.WebControls.AccessDataSource
	End Class
End Namespace
