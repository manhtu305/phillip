'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer

    Partial Public Class Wijmo

        '''<summary>
        '''head コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents head As Global.System.Web.UI.WebControls.ContentPlaceHolder

        '''<summary>
        '''switcherContainer コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents switcherContainer As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''WidgetNameLebel コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents WidgetNameLebel As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''navigation コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents navigation As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''WidgetSampleLebel コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents WidgetSampleLebel As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''WidgetTabs コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents WidgetTabs As Global.C1.Web.Wijmo.Controls.C1Tabs.C1Tabs

        '''<summary>
        '''MainContent コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents MainContent As Global.System.Web.UI.WebControls.ContentPlaceHolder

        '''<summary>
        '''ControlOptions コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ControlOptions As Global.System.Web.UI.WebControls.ContentPlaceHolder

        '''<summary>
        '''Description コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Description As Global.System.Web.UI.WebControls.ContentPlaceHolder

        '''<summary>
        '''docs コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents docs As Global.System.Web.UI.HtmlControls.HtmlAnchor
    End Class
End Namespace
