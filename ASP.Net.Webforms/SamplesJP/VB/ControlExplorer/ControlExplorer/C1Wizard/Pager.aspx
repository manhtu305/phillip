﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Pager.aspx.vb" Inherits="ControlExplorer.C1Wizard.Pager" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Wizard" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Pager" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <wijmo:C1Wizard ID="C1Wizard1" runat="server" NavButtons="None">
        <Steps>
            <wijmo:C1WizardStep ID="C1WizardStep1" Title="手順1">
                Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur necarcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorperleo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales	tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel	pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep2" Title="手順2">
                Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep3" Title="手順3">
                Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep4" Title="手順4">
                Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep5" Title="手順5">
                ComponentOne EnterpriseはWindowsフォーム、ASP.NET、WPF、Silverlight、Windows Phoneの各プラットフォーム向けのコンポーネントを収録しています。
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep6" Title="手順6">
                どのプラットフォームにも業務アプリケーションで要求される機能を満たすコンポーネントをバランスよく揃えているので、Windows フォームや ASP.NET はもちろんのこと、WPF や Silverlight、Windows Phone といった次世代フレームワークでも高度な機能を持つ業務アプリケーションを開発できます。
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep7" Title="手順7">
                ComponentOne Enterprise は最新技術に迅速に対応します。統合開発環境や OS のアップデート、あるいは WPF、Silverlight、Windows Phone といった新しいフレームワークが登場するごとにコンポーネントを追加収録したり、いち早く iPhone や iPad に対応したりするなど常に最新技術を開発者の皆さまに提供してきました。新しい環境が登場しても安心してご利用していただける製品です。
            </wijmo:C1WizardStep>
        </Steps>
    </wijmo:C1Wizard>

    <wijmo:C1Pager runat="server" ID="C1Pager1" Mode="NextPreviousFirstLast" OnClientPageIndexChanged="onClientPageIndexChanged"/>

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">

    <p>
    ページャーモード：
    </p> 
	<input type="radio" value="Default" name="set1" checked="checked" />既定　
	<input type="radio" value="Text" name="set1" />テキスト　
    <input type="radio" value="Custom" name="set1" />カスタムテキスト


    <script type="text/javascript">

        function onClientPageIndexChanged() {
            var pageIndex = $("#<%=C1Pager1.ClientID%>").c1pager("option", "pageIndex");
            $("#<%=C1Wizard1.ClientID%>").c1wizard({ activeIndex: pageIndex });
        };

        $(document).ready(function () {

            $(':radio').change(function (e) {
                switch ($(this).val()) {
                    case "Default":
                        $("#<%=C1Pager1.ClientID%>").c1pager('option', {
                            firstPageClass: "ui-icon-seek-first",
                            previousPageClass: "ui-icon-seek-prev",
                            nextPageClass: "ui-icon-seek-next",
                            lastPageClass: "ui-icon-seek-end",
                            firstPageText: "最初へ",
                            previousPageText: "前へ",
                            nextPageText: "次へ",
                            lastPageText: "最後へ"
                        });
                        break;

                    case "Text":
                        $("#<%=C1Pager1.ClientID%>").c1pager('option', {
                            firstPageClass: "",
                            previousPageClass: "",
                            nextPageClass: "",
                            lastPageClass: "",
                            firstPageText: "最初へ",
                            previousPageText: "前へ",
                            nextPageText: "次へ",
                            lastPageText: "最後へ"
                        });
                        break;

                    case "Custom":
                        $("#<%=C1Pager1.ClientID%>").c1pager('option', {
                            firstPageClass: "",
                            previousPageClass: "",
                            nextPageClass: "",
                            lastPageClass: "",
                            firstPageText: "|<",
                            previousPageText: "<",
                            nextPageText: ">",
                            lastPageText: ">|"
                        });
                        break;
                }
            });
        });
	</script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">

    <p>
        このサンプルでは、C1Wizard のナビゲーションボタンとして C1Pager コントロールを使用する方法を紹介します。
    </p>
    <p>
        まず、<b>NavButtons</b> プロパティを None に設定して、組み込みのナビゲーションボタンを非表示にします。
    </p>
    <p>
        その後、C1Pager コントロールを C1Wizard の直後に追加して、単純なクライアント側コードとページャーコントロールの <b>OnClientPageIndexChanged</b> プロパティを使用することで、両者のインデックスを連結することができます。
    </p>
</asp:Content>
