Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1Wizard
	Public Partial Class Pager
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not Me.Page.IsPostBack Then
				Me.C1Pager1.PageCount = Me.C1Wizard1.Steps.Count
				Me.C1Pager1.PageIndex = Me.C1Wizard1.ActiveIndex
			End If
		End Sub
	End Class
End Namespace
