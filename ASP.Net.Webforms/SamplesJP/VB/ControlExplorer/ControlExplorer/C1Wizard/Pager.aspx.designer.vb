'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Wizard


	Public Partial Class Pager

		''' <summary>
		''' C1Wizard1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Wizard1 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1Wizard

		''' <summary>
		''' C1WizardStep1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep1 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1WizardStep2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep2 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1WizardStep3 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep3 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1WizardStep4 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep4 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1WizardStep5 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep5 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1WizardStep6 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep6 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1WizardStep7 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1WizardStep7 As Global.C1.Web.Wijmo.Controls.C1Wizard.C1WizardStep

		''' <summary>
		''' C1Pager1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Pager1 As Global.C1.Web.Wijmo.Controls.C1Pager.C1Pager
	End Class
End Namespace
