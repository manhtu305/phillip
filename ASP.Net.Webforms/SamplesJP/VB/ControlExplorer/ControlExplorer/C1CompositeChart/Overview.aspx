﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1CompositeChart.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function hintContent() {
            return this.label + ' ' + this.y + '';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1CompositeChart runat="server" ID="C1CompositeChart1" Height="475" Width="756" Stacked="false" Legend-Visible="false">
        <Hint>
            <Content Function="hintContent" />
        </Hint>
        <Axis>
            <X Text="">
            </X>
            <Y Text="ハードウェア数" Compass="West">
            </Y>
        </Axis>
        <Header Text="ハードウェア分布">
        </Header>
        <SeriesList>
            <wijmo:CompositeChartSeries Label="西エリア" LegendEntry="true" Type="Column">
                <Data>
                    <X StringValues="デスクトップ, ノート, 一体型, タブレット, 携帯電話" />
                    <Y DoubleValues="5, 3, 4, 7, 2" />
                </Data>
            </wijmo:CompositeChartSeries>
            <wijmo:CompositeChartSeries Label="中央エリア" LegendEntry="true" Type="Column">
                <Data>
                    <X StringValues="デスクトップ, ノート, 一体型, タブレット, 携帯電話" />
                    <Y DoubleValues="2, 2, 3, 2, 1" />
                </Data>
            </wijmo:CompositeChartSeries>
            <wijmo:CompositeChartSeries Label="東エリア" LegendEntry="true" Type="Column">
                <Data>
                    <X StringValues="デスクトップ, ノート, 一体型, タブレット, 携帯電話" />
                    <Y DoubleValues="3, 4, 4, 2, 5" />
                </Data>
            </wijmo:CompositeChartSeries>
            <wijmo:CompositeChartSeries Label="機種1" LegendEntry="true" Type="Pie" Center="150, 150" Radius="60">
                <PieSeriesList>
                    <wijmo:PieChartSeries Label="機種2" LegendEntry="true" Data="46.78" Offset="0">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="機種3" LegendEntry="true" Data="23.18" Offset="0">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="機種4" LegendEntry="true" Data="20.25" Offset="0">
                    </wijmo:PieChartSeries>
                </PieSeriesList>
            </wijmo:CompositeChartSeries>
            <wijmo:CompositeChartSeries Label="米国" LegendEntry="true" Type="Line">
                <Data>
                    <X StringValues="デスクトップ, ノート, 一体型, タブレット, 携帯電話" />
                    <Y DoubleValues="3, 6, 2, 9, 5" />
                </Data>
            </wijmo:CompositeChartSeries>
            <wijmo:CompositeChartSeries Label="カナダ" LegendEntry="true" Type="Line">
                <Data>
                    <X StringValues="デスクトップ, ノート, 一体型, タブレット, 携帯電話" />
                    <Y DoubleValues="1, 3, 4, 7, 2" />
                </Data>
            </wijmo:CompositeChartSeries>
        </SeriesList>
    </wijmo:C1CompositeChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        C1CompositeChart は 1 つのキャンバス内に複数の種類のグラフを同時に表示することができます。 
        このサンプルでは、棒グラフと折れ線グラフと円グラフを使用しています。
    </p>
</asp:Content>
