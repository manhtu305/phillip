﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Thumbs.aspx.vb" Inherits="ControlExplorer.C1Slider.Thumbs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .header2
        {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Slider runat="server" ID="C1Slider1" Max="500" Range="false" Min="0" Step="50" Orientation="Horizontal" Values="400,40" Width="300px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Slider</strong> は境界値の設定をサポートし、ユーザーは上限と下限を示す 2 つの値を入力できます。
        境界値の設定を有効にするには、<strong>Value</strong> プロパティを 2 つの値（例：&quot;400,40&quot;）に、<strong>Range</strong> プロパティを <strong>False</strong> に設定する必要があります。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
