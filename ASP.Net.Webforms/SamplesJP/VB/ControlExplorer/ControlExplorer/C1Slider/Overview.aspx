﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Slider.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .sliderContainer
        {
        }
        .layout
        {
            float: left;
            margin: 0 10px;
        }
        .header2
        {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="sliderContainer ui-helper-clearfix">
        <div class="layout">
            <p>
                水平方向</p>
            <wijmo:C1Slider runat="server" ID="C1Slider1" Value="50" Orientation="Horizontal" Width="200px" />
        </div>
        <div class="layout">
            <p>
                垂直方向</p>
            <wijmo:C1Slider runat="server" ID="C1Slider2" Value="50" Orientation="Vertical" Height="200px" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Slider</strong> は、指定された範囲内で値を選択する単純でなじみある方法を提供します。
        <strong>Orientation</strong> プロパティを使用すると、コントロールの方向と外観を制御できます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
