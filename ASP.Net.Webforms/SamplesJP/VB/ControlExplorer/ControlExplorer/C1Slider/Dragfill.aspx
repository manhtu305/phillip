﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Dragfill.aspx.vb" Inherits="ControlExplorer.C1Slider.Dragfill" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .header2
        {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Slider runat="server" ID="C1Slider" DragFill="true" Max="500" Height="100px" Range="true" Min="0" Step="2" Orientation="Horizontal" Values="100,400" Width="300px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Slider</strong> では、2 つのサム間にある塗りつぶされた線をドラッグすることが可能です。
        下記プロパティを該当する値に設定することで、塗りつぶし線のドラッグを有効にできます。</p>
    <ul>
        <li>Range: true</li>
        <li>DragFill: true</li>
        <li>Values</li>
    </ul>
    <p>
        <strong>Range</strong> プロパティが True に設定されている場合、<strong>Values</strong> プロパティを使用して複数のサムを指定します。
        エンドユーザは範囲領域をドラッグでき、異なる色で塗りつぶされているサム間の領域が選択範囲を示します。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
