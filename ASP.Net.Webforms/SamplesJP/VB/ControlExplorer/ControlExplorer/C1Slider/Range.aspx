﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Range.aspx.vb" Inherits="ControlExplorer.C1Slider.Range" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .header2
        {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Slider runat="server" ID="C1Slider1" DragFill="false" Max="500" Range="true" Min="0" Step="2" Orientation="Horizontal" Values="100,400" Width="300px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1Slider</strong> は、2 つのサムを用いた範囲の選択をサポートします。
        2 つのサムで範囲を選択可能にするには、下記プロパティを該当する値に設定します。
    </p>
    <ul>
        <li>Range: true</li>
        <li>Values</li>
    </ul>
    <p>
        2 つのサムの間の領域は異なる背景色で表示され、選択されている範囲を示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
