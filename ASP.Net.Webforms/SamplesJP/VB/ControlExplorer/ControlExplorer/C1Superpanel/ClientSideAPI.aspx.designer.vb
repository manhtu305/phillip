'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Superpanel


	Public Partial Class ClientSideAPI

		''' <summary>
		''' C1superPanel1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1superPanel1 As Global.C1.Web.Wijmo.Controls.C1SuperPanel.C1SuperPanel

		''' <summary>
		''' inputX コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected inputX As Global.System.Web.UI.WebControls.TextBox

		''' <summary>
		''' inputY コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected inputY As Global.System.Web.UI.WebControls.TextBox
	End Class
End Namespace
