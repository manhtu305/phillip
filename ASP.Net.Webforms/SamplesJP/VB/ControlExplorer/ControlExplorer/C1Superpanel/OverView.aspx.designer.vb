'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Superpanel


	Public Partial Class OverView

		''' <summary>
		''' SuperPanel1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected SuperPanel1 As Global.C1.Web.Wijmo.Controls.C1SuperPanel.C1SuperPanel

		''' <summary>
		''' chbDisabled コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected chbDisabled As Global.System.Web.UI.WebControls.CheckBox

		''' <summary>
		''' txtDuration コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected txtDuration As Global.System.Web.UI.WebControls.TextBox

		''' <summary>
		''' DrpListEasing コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected DrpListEasing As Global.System.Web.UI.WebControls.DropDownList
	End Class
End Namespace
