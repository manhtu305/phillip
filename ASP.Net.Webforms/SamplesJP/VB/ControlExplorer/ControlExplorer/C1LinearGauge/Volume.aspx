﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Volume.aspx.vb" Inherits="ControlExplorer.C1LinearGauge.Volume" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gauge" TagPrefix="Wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="Wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .gauge
        {
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="ui-widget ui-widget-content ui-corner-all ui-helper-clearfix" style="width: 62px; padding: 4px">
        <Wijmo:C1LinearGauge runat="server" ID="Gauge" Value="50" Width="40px" Height="400" Orientation="Vertical" XAxisLength="1" XAxisLocation="0.01" CssClass="gauge">
            <TickMajor Factor="2" Visible="True" Offset="0" Interval="50">
                <TickStyle Opacity="0.4" StrokeWidth="0">
                    <Fill Color="#000000">
                    </Fill>
                </TickStyle>
            </TickMajor>
            <TickMinor Visible="False" Offset="0" Interval="5">
            </TickMinor>
            <Pointer Length="0.5" Width="4" Offset="0">
            </Pointer>
            <Labels Visible="False">
            </Labels>
            <Animation Duration="2000" Easing="EaseOutBack" Enabled="False"></Animation>
            <Face>
                <FaceStyle StrokeWidth="0" Width="0">
                </FaceStyle>
            </Face>
            <Ranges>
                <Wijmo:GaugelRange EndDistance="0.8" EndValue="60" EndWidth="0.5" StartDistance="0.8" StartValue="0" StartWidth="0">
                    <RangeStyle StrokeWidth="0">
                        <Fill ColorBegin="#0FFF03" ColorEnd="#0AD400" LinearGradientAngle="90" Type="LinearGradient">
                        </Fill>
                    </RangeStyle>
                </Wijmo:GaugelRange>
                <Wijmo:GaugelRange EndDistance="0.8" EndValue="85" EndWidth="0.6" StartDistance="0.8" StartValue="60" StartWidth="0.5">
                    <RangeStyle StrokeWidth="0">
                        <Fill ColorBegin="#F8FF01" ColorEnd="#F8FF27" LinearGradientAngle="90" Type="LinearGradient">
                        </Fill>
                    </RangeStyle>
                </Wijmo:GaugelRange>
                <Wijmo:GaugelRange EndDistance="0.8" EndValue="100" EndWidth="0.7" StartDistance="0.8" StartValue="85" StartWidth="0.6">
                    <RangeStyle StrokeWidth="0">
                        <Fill ColorBegin="#FF0F03" ColorEnd="#D40A00" LinearGradientAngle="90" Type="LinearGradient">
                        </Fill>
                    </RangeStyle>
                </Wijmo:GaugelRange>
            </Ranges>
        </Wijmo:C1LinearGauge>
        <div style="float: left;">
            <Wijmo:C1Slider ID="slider" runat="server" Height="390" Value="50" Orientation="Vertical" />
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= slider.ClientID %>").bind("c1sliderchange", function (event, ui) {
                $("#<%= Gauge.ClientID %>").c1lineargauge("option", "value", ui.value);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
	このサンプルでは、​C1Slider と C1LinearGauge​C1Slider を使用して音量コントロールを作成します。
    ゲージは音量レベルを表し、スライダが値を設定するために使用されています。</p>
</asp:Content>
