﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Orientation.aspx.vb" Inherits="ControlExplorer.C1LinearGauge.Orientation" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gauge" TagPrefix="Wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<Wijmo:C1LinearGauge runat="server" ID="Gauge1" Value="50" Width="310" Height="70">
<TickMajor Position="Inside" Factor="3">
</TickMajor>
<TickMinor Visible="true"></TickMinor>
<Labels Visible="false"></Labels>
</Wijmo:C1LinearGauge>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
<p>
<select id="orientation">
					<option>horizontal</option>
					<option>vertical</option>
				</select>
				</p>
<script type="text/javascript">
	$(document).ready(function () {
		var gaugeEle = $("#<%= Gauge1.ClientID %>"),
			width = gaugeEle.width(),
			height = gaugeEle.height();

		$("#orientation").change(function () {
			var orientation = $(this).val();
			gaugeEle.c1lineargauge("option", "orientation", orientation);
			if (orientation === "vertical") {
				gaugeEle.width(height);
				gaugeEle.height(width);
				//gaugeEle.c1lineargauge("option", "orientation", orientation);
			}
			else {
				gaugeEle.width(width);
				gaugeEle.height(height);
			}
		});
	});
	</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、C1LinearGauge を水平方向や垂直方向で表示します。
    <b>Orientation</b> プロパティを Horizontal または Vertical に設定すると、ゲージの外観を制御することができます。</p>
</asp:Content>
