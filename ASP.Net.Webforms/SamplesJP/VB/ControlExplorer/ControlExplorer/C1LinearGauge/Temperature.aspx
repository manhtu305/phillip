﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Temperature.aspx.vb" Inherits="ControlExplorer.C1LinearGauge.Temperature" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gauge" TagPrefix="Wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<Wijmo:C1LinearGauge runat="server" ID="Gauge1" Width="40" Height="400" 
		Orientation="Vertical" Value="50" XAxisLength="0.95" XAxisLocation="0.02">
	<Labels Visible="False">
		<LabelStyle FontSize="10pt" FontWeight="800">
			<Fill Color="#5A5A5A"></Fill>
		</LabelStyle>
	</Labels>
	<TickMajor Factor="2">
		<TickStyle Width="0" Opacity="0.2">
			<Fill Color="#000000"></Fill>
		</TickStyle>
	</TickMajor>
	<TickMinor Position="Center" Visible="true" Interval="5">
		<TickStyle Opacity="0.2">
			<Fill Color="#000000"></Fill>
		</TickStyle>
	</TickMinor>
	<Pointer Shape="Tri" Width="4">
		<PointerStyle Opacity="0.8">
			<Fill Color="#000000"></Fill>
		</PointerStyle>
	</Pointer>
<Animation Duration="2000" Easing="EaseOutBack" Enabled="False"></Animation>
	<Face>
		<FaceStyle>
			<Fill Type="LinearGradient" ColorBegin="#DEDEDE" ColorEnd="#CDCDCD" LinearGradientAngle="180"></Fill>
		</FaceStyle>
	</Face>
	<Ranges>
		<Wijmo:GaugelRange StartValue="0" EndValue="50" StartDistance="0.9" EndDistance="0.9" StartWidth="0.85" EndWidth="0.85">
			<RangeStyle StrokeWidth="0">
				<Fill Type="LinearGradient" ColorBegin="#CC00CC" ColorEnd="#AA00AA" LinearGradientAngle="180"></Fill>
			</RangeStyle>
		</Wijmo:GaugelRange>
	</Ranges>
</Wijmo:C1LinearGauge>
 <button id="btn" type="button">
				変更</button>
<script type="text/javascript">
	$(document).ready(function () {
	    var gaugeEle = $("#<%= Gauge1.ClientID %>"),
	        state = 0;
	    // jQuery 1.9 では toggle method が削除され toggle effect が追加
	    $("#btn").button().click(function () {
	        if (state === 0) {
	            gaugeEle.c1lineargauge("option", "ranges", [
                    {
                        startValue: 0,
                        endValue: 100,
                        startDistance: 0.9,
                        endDistance: 0.9,
                        startWidth: 0.85,
                        endWidth: 0.85,
                        style: {
                            fill: "180-#FF0000-#CC0000",
                            stroke: "none"
                        }
                    }
	            ]).c1lineargauge("redraw");
	            state = 1;
	        }
	        else {
	            gaugeEle.c1lineargauge("option", "ranges", [{
	                startValue: 0,
	                endValue: 10,
	                startDistance: 0.9,
	                endDistance: 0.9,
	                startWidth: 0.85,
	                endWidth: 0.85,
	                style: {
	                    fill: "180-#0099FF-#0066CC",
	                    stroke: "none"
	                }
	            }
	            ]).c1lineargauge("redraw");
	            state = 0;
	        }
	    })
	});
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、温度計のようなスタイルの直線形ゲージを表示します。</p>
</asp:Content>
