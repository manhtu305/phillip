Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO

Namespace ControlExplorer
	Public Partial Class GetSourceCode
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				BuildCodeView()
			End If
		End Sub

		Private Sub BuildCodeView()
			Dim pagePath As String = ""
			If Request IsNot Nothing AndAlso Request.UrlReferrer.AbsolutePath IsNot Nothing Then
				pagePath = Page.MapPath(Request.UrlReferrer.AbsolutePath)
                Dim codePath As String = pagePath & ".vb"

				LblASPX.Text = File.ReadAllText(pagePath)
				LblCS.Text = File.ReadAllText(codePath)
			End If
		End Sub
	End Class
End Namespace
