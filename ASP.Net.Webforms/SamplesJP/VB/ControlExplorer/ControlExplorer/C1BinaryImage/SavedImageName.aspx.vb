﻿Namespace ControlExplorer.C1BinaryImage
    Partial Public Class SavedImageName
        Inherits System.Web.UI.Page
        Protected Sub Page_Load(sender As Object, e As EventArgs)
            BinaryImage1.ImageData = System.IO.File.ReadAllBytes(Server.MapPath("Images/ResizeModeSample.jpg"))
            BinaryImage1.AlternateText = "Sample"
            BinaryImage1.ToolTip = "Sample"
            BinaryImage1.SavedImageName = TextBox_SavedImageName.Text
        End Sub

        Protected Sub apply_Click(sender As Object, e As EventArgs)
            BinaryImage1.SavedImageName = TextBox_SavedImageName.Text
            UpdatePanel1.Update()
        End Sub
    End Class
End Namespace