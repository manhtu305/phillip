﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1BinaryImage

    Partial Public Class SavedImageName

        '''<summary>
        '''ScriptManager1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''UpdatePanel1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''BinaryImage1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents BinaryImage1 As Global.C1.Web.Wijmo.Controls.C1BinaryImage.C1BinaryImage

        '''<summary>
        '''UpdatePanel2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''TextBox_SavedImageName コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents TextBox_SavedImageName As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''apply コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents apply As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
