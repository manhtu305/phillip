﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="SavedImageName.aspx.vb" Inherits="ControlExplorer.C1BinaryImage.SavedImageName" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BinaryImage"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="float: left; margin: 10px;">
                <wijmo:C1BinaryImage ID="BinaryImage1" SavedImageName="Sample" runat="server"/>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
        右クリックして画像を保存すると、指定したファイル名で画像が保存されます。
    </p>
    <p>
        <strong>SavedImageName</strong>プロパティを使用すると、既定のファイル名を変更できます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label class="settinglegend">SavedImageName:</label>
                        </li>
                        <li>
                            <asp:TextBox ID="TextBox_SavedImageName" runat="server" AutoPostBack="True" CausesValidation="True"
                                CssClass="valueField" Text="Sample"></asp:TextBox>
                        </li>
                    </ul>
                </div>
                <div class="settingcontrol">
                    <asp:Button ID="apply" runat="server" Text="適用" CssClass="settingapply" OnClick="apply_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
