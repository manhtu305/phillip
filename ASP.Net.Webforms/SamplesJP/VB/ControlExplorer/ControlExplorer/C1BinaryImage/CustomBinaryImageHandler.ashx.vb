﻿Imports C1.Web.Wijmo.Controls.C1BinaryImage
Imports System.Drawing.Imaging
Imports System.Drawing

Namespace ControlExplorer.C1BinaryImage
    ''' <summary>
    ''' Summary description for CustomBinaryImageHandler
    ''' </summary>
    Public Class CustomBinaryImageHandler
        Inherits C1BinaryImageHandler
        Public Overrides Function ProcessImageData(imageData As C1BinaryImageData) As C1BinaryImageData
            Using outStream = New System.IO.MemoryStream()
                Using inStream = New System.IO.MemoryStream(imageData.Data)
                    Using image = Bitmap.FromStream(inStream)
                        Dim newImage = AddWatermark(image)
                        newImage.Save(outStream, ImageFormat.Png)
                        imageData.Data = outStream.ToArray()
                        imageData.MimeType = "image/png"
                        imageData.ImageFileName += "_Watermark"
                    End Using
                End Using
            End Using

            Return MyBase.ProcessImageData(imageData)
        End Function

        Private Function AddWatermark(image As Image) As Image
            Dim watermarkString = "ComponentOne"
            Dim font = New Font("Arial", 8.0F, FontStyle.Regular)
            Dim newImage = New Bitmap(image.Width, image.Height)

            Using g = Graphics.FromImage(newImage)
                g.DrawImage(image, New Point(0, 0))
                g.DrawString(watermarkString, font, New SolidBrush(Color.White), New PointF(2, newImage.Height - font.Height - 2))
            End Using

            Return newImage
        End Function
    End Class
End Namespace
