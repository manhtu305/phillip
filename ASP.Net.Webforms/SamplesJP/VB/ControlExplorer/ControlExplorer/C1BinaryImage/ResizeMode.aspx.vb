﻿Imports C1.Web.Wijmo.Controls.C1BinaryImage

Namespace ControlExplorer.C1BinaryImage
    Partial Public Class ResizeMode
        Inherits System.Web.UI.Page
        Protected Sub Page_Load(sender As Object, e As EventArgs)
            Div_CropPosition.Visible = DropDownList_ResizeMode.SelectedValue = "Crop"

            BinaryImage2.ImageData = System.IO.File.ReadAllBytes(Server.MapPath("images/ResizeModeSample.jpg"))
            BinaryImage2.AlternateText = "ResizeModeSample"
            BinaryImage2.ToolTip = "ResizeModeSample"
            BinaryImage2.SavedImageName = "ResizeModeSample"

            If IsPostBack Then
                BinaryImage2.ResizeMode = DirectCast([Enum].Parse(GetType(ImageResizeMode), DropDownList_ResizeMode.SelectedValue), ImageResizeMode)
                BinaryImage2.CropPosition = DirectCast([Enum].Parse(GetType(ImageCropPosition), DropDownList_CropPosition.SelectedValue), ImageCropPosition)

                If DropDownList_ResizeMode.SelectedValue <> "None" Then
                    Dim width As Integer
                    If Integer.TryParse(TextBox_Width.Text, width) Then
                        BinaryImage2.Width = width
                    End If

                    Dim height As Integer
                    If Integer.TryParse(TextBox_Height.Text, height) Then
                        BinaryImage2.Height = height
                    End If
                Else
                    BinaryImage2.Width = Unit.Empty
                    BinaryImage2.Height = Unit.Empty
                End If

                UpdatePanel1.Update()
            End If
        End Sub
    End Class
End Namespace
