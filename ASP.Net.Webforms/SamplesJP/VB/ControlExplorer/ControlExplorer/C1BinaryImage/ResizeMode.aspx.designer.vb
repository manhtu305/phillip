﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1BinaryImage

    Partial Public Class ResizeMode

        '''<summary>
        '''BinaryImage1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents BinaryImage1 As Global.C1.Web.Wijmo.Controls.C1BinaryImage.C1BinaryImage

        '''<summary>
        '''ScriptManager1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''UpdatePanel1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''BinaryImage2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents BinaryImage2 As Global.C1.Web.Wijmo.Controls.C1BinaryImage.C1BinaryImage

        '''<summary>
        '''UpdatePanel2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''TextBox_Width コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents TextBox_Width As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''RequiredFieldValidator1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator

        '''<summary>
        '''RegularExpressionValidator1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator

        '''<summary>
        '''TextBox_Height コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents TextBox_Height As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''RequiredFieldValidator2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents RequiredFieldValidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator

        '''<summary>
        '''RegularExpressionValidator2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents RegularExpressionValidator2 As Global.System.Web.UI.WebControls.RegularExpressionValidator

        '''<summary>
        '''DropDownList_ResizeMode コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents DropDownList_ResizeMode As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''Div_CropPosition コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Div_CropPosition As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''DropDownList_CropPosition コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents DropDownList_CropPosition As Global.System.Web.UI.WebControls.DropDownList
    End Class
End Namespace
