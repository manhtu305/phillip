﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1SiteMap

    Partial Public Class Layout

        '''<summary>
        '''ScriptManager1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''UpdatePanel1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''C1SiteMap1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1SiteMap1 As Global.C1.Web.Wijmo.Controls.C1SiteMap.C1SiteMap

        '''<summary>
        '''C1SiteMapDataSource1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1SiteMapDataSource1 As Global.C1.Web.Wijmo.Controls.C1SiteMapDataSource.C1SiteMapDataSource

        '''<summary>
        '''UpdatePanel2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''cbxLevel1Layout コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents cbxLevel1Layout As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''numberLevel1ColumnCount コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents numberLevel1ColumnCount As Global.C1.Web.Wijmo.Controls.C1Input.C1InputNumeric

        '''<summary>
        '''tbxLevel1SeparatorText コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents tbxLevel1SeparatorText As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''numberLevel1MaxNode コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents numberLevel1MaxNode As Global.C1.Web.Wijmo.Controls.C1Input.C1InputNumeric

        '''<summary>
        '''cbxLevel2Layout コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents cbxLevel2Layout As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''numberLevel2ColumnCount コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents numberLevel2ColumnCount As Global.C1.Web.Wijmo.Controls.C1Input.C1InputNumeric

        '''<summary>
        '''tbxLevel2SeparatorText コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents tbxLevel2SeparatorText As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''numberLevel2MaxNode コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents numberLevel2MaxNode As Global.C1.Web.Wijmo.Controls.C1Input.C1InputNumeric

        '''<summary>
        '''btnApply コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents btnApply As Global.System.Web.UI.WebControls.Button
    End Class
End Namespace
