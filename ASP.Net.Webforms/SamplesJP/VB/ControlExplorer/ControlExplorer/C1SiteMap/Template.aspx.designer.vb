﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1SiteMap

    Partial Public Class Template

        '''<summary>
        '''C1SiteMapDataSource1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1SiteMapDataSource1 As Global.C1.Web.Wijmo.Controls.C1SiteMapDataSource.C1SiteMapDataSource

        '''<summary>
        '''C1SiteMap1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1SiteMap1 As Global.C1.Web.Wijmo.Controls.C1SiteMap.C1SiteMap

        '''<summary>
        '''Image1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents Image1 As Global.System.Web.UI.WebControls.Image
    End Class
End Namespace
