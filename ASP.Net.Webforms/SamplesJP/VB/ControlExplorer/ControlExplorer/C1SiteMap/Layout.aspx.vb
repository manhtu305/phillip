﻿Imports C1.Web.Wijmo.Controls.C1SiteMap

Namespace ControlExplorer.C1SiteMap
    Public Class Layout
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            If Not IsPostBack Then
                ApplySettings()
            End If
        End Sub

        Protected Sub C1SiteMap1_NodeDataBound(sender As Object, e As C1.Web.Wijmo.Controls.C1SiteMap.C1SiteMapNodeEventArgs)
            e.Node.NavigateUrl = "#"
        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            ApplySettings()

            Me.UpdatePanel1.Update()
        End Sub

        Private Sub ApplySettings()
            Me.C1SiteMap1.LevelSettings.Clear()

            'for level 1, the second level
            Dim settingLevel1 As New C1SiteMapLevelSetting()
            settingLevel1.Level = 1
            settingLevel1.Layout = DirectCast([Enum].Parse(GetType(SiteMapLayoutType), cbxLevel1Layout.SelectedValue, True), SiteMapLayoutType)

            settingLevel1.ListLayout.RepeatColumns = CInt(numberLevel1ColumnCount.Value)
            settingLevel1.SeparatorText = tbxLevel1SeparatorText.Text
            settingLevel1.MaxNodes = CInt(numberLevel1MaxNode.Value)

            'for level 2, the third level
            Dim settingLevel2 As New C1SiteMapLevelSetting()
            settingLevel2.Level = 2
            settingLevel2.Layout = DirectCast([Enum].Parse(GetType(SiteMapLayoutType), cbxLevel2Layout.SelectedValue, True), SiteMapLayoutType)

            settingLevel2.ListLayout.RepeatColumns = CInt(numberLevel2ColumnCount.Value)
            settingLevel2.MaxNodes = CInt(numberLevel2MaxNode.Value)
            settingLevel2.SeparatorText = tbxLevel2SeparatorText.Text

            Me.C1SiteMap1.LevelSettings.Add(settingLevel1)
            Me.C1SiteMap1.LevelSettings.Add(settingLevel2)
        End Sub

        Protected Sub cbxLevel1Layout_SelectedIndexChanged(sender As Object, e As EventArgs)
            If cbxLevel1Layout.SelectedIndex = 0 Then
                tbxLevel1SeparatorText.Enabled = False
                numberLevel1ColumnCount.Enabled = True
            Else
                tbxLevel1SeparatorText.Enabled = True
                numberLevel1ColumnCount.Enabled = False
            End If
        End Sub

        Protected Sub cbxLevel2Layout_SelectedIndexChanged(sender As Object, e As EventArgs)
            If cbxLevel2Layout.SelectedIndex = 0 Then
                tbxLevel2SeparatorText.Enabled = False
                numberLevel2ColumnCount.Enabled = True
            Else
                tbxLevel2SeparatorText.Enabled = True
                numberLevel2ColumnCount.Enabled = False
            End If

        End Sub
    End Class
End Namespace
