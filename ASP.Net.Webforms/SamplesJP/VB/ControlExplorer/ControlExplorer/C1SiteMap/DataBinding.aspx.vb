﻿Imports C1.Web.Wijmo.Controls.C1SiteMap

Namespace ControlExplorer.C1SiteMap
    Public Class DataBinding
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        End Sub

        Protected Sub RblDataSource_SelectedIndexChanged(sender As Object, e As EventArgs)
            C1SiteMap1.DataBindings.Clear()

            Dim datasource As String = RblDataSource.SelectedValue
            If datasource = "XmlDataSource" Then
                C1SiteMap1.DataSourceID = "XmlDataSource1"

                C1SiteMap1.DataBindings.Add(New C1SiteMapNodeBinding() With { _
                    .Level = 0, _
                    .TextField = "text" _
                })
                C1SiteMap1.DataBindings.Add(New C1SiteMapNodeBinding() With { _
                    .Level = 1, _
                    .TextField = "text" _
                })
                C1SiteMap1.DataBindings.Add(New C1SiteMapNodeBinding() With { _
                    .Level = 2, _
                    .TextField = "text" _
                })
            ElseIf datasource = "SiteMapDataSource" Then
                C1SiteMap1.DataSourceID = "SiteMapDataSource1"
            Else
                C1SiteMap1.DataSourceID = "C1SiteMapDataSource1"
            End If

            C1SiteMap1.DataBind()

            UpdatePanel1.Update()
        End Sub

        Protected Sub C1SiteMap1_NodeDataBound(sender As Object, e As C1SiteMapNodeEventArgs)
            e.Node.NavigateUrl = "#"
        End Sub

    End Class
End Namespace
