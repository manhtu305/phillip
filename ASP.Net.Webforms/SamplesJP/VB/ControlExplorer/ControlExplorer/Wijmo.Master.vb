Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Xml.Linq
Imports System.IO

Namespace ControlExplorer
	Public Partial Class Wijmo
		Inherits System.Web.UI.MasterPage
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			BindNavigation()
			RegisterCallbacks()

		End Sub


		Private Sub BindNavigation()

			Dim xEle As System.Xml.Linq.XElement
			If Cache("ControlList") Is Nothing Then
				xEle = System.Xml.Linq.XElement.Load(Server.MapPath("~/ControlList.xml"))
				Cache("ControlList") = xEle
			Else
				xEle = DirectCast(Cache("ControlList"), System.Xml.Linq.XElement)
			End If
            Dim catalogs As IEnumerable(Of XElement) = From c In xEle.Elements("Control") Select c

			Dim relativePaths = Request.AppRelativeCurrentExecutionFilePath.Split("/"C)

            Dim pageList As IEnumerable(Of XElement) = From control2 In catalogs Where control2.Attribute("name").Value.ToLower() = relativePaths(1).Replace("C1", "").ToLower() Select control2


			If pageList IsNot Nothing Then
				If pageList.Count() < 1 Then
                    Dim childControls = From c In xEle.Descendants("ChildControl") Select c

                    pageList = From control2 In childControls Where control2.Attribute("name").Value.ToLower() = relativePaths(1).Replace("C1", "").ToLower() Select control2
				End If
				If pageList.Count() > 0 Then

					Me.navigation.Visible = True
					Me.navigation.Controls.Clear()

					Dim ulSideMenu As New HtmlGenericControl("ul")
					ulSideMenu.Attributes("class") = "sample-list"
					navigation.Controls.Add(ulSideMenu)

					For Each action As XElement In pageList.First().Elements("action")
						Dim li As New HtmlGenericControl("li")

						ulSideMenu.Controls.Add(li)
						Dim link As New HtmlGenericControl("a")
						link.InnerHtml = action.Attribute("name").Value
						link.Attributes("href") = ResolveClientUrl(action.Attribute("page").Value)
						'link.Attributes["href"] = action.Attribute("page").Value.Substring(2);
						li.Controls.Add(link)


						If action.Attribute("page").Value.ToLower() = Request.AppRelativeCurrentExecutionFilePath.ToLower() Then
							li.Attributes("class") = "demo-config-on"
							Page.Title = action.Parent.Attribute("name").Value + " - " + action.Attribute("name").Value + " - ComponentOne for ASP.NET Wijmo 2012J"
							WidgetNameLebel.Text = action.Parent.Attribute("name").Value

							WidgetSampleLebel.Text = action.Attribute("name").Value
						End If
					Next
				End If
			Else
				Me.navigation.Visible = False
			End If

			'Build documentation link
            Dim docPath = "http://c1.grapecity.com/help/web/aspnet/" & "aspwij_" + relativePaths(1).ToLower().Substring(2)
            If relativePaths(1).Contains("C1Input") Then
                docPath = "http://c1.grapecity.com/help/web/aspnet/aspwij_input/"
            End If
            If relativePaths(1).Contains("C1Excel") Then
                docPath = "http://c1.grapecity.com/help/web/winasp/winasp_excel/"
            End If
            If relativePaths(1).Contains("C1PDF") Then
                docPath = "http://c1.grapecity.com/help/web/winasp/winasp_pdf/"
            End If
            docs.HRef = docPath

            'Hide theme switcher for chart pages
            If relativePaths(1).ToLower().Contains("chart") OrElse relativePaths(1).ToLower().Contains("gauge") Then
                switcherContainer.Visible = False
            End If
        End Sub

		'private void BuildCodeView()
		'{
		'    string pagePath = Page.MapPath(Request.Url.AbsolutePath);
		'    string codePath = pagePath + ".cs";

		'    LblASPX.Text = File.ReadAllText(pagePath);
		'    LblCS.Text = File.ReadAllText(codePath);
		'}

    End Class
End Namespace
