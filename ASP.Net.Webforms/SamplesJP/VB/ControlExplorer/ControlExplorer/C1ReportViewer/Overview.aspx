﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="C1ReportViewer/C1ReportXML/CommonTasks.xml" ReportName="01: Alternating Background" Zoom="Fit Width" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<strong>C1ReportViewer</strong> コントロールの既定の動作を紹介します。
    </p>
    <p>
        <strong>C1ReportViewer</strong> を使用すると、Web アプリケーションにレポートを簡単に追加できます。
        <strong>C1ReportViewer</strong> では、<strong>C1Report デザイナ</strong>で作成したレポート定義ファイルを使用することができます。
    </p>
    <p>
        このサンプルで使用されるプロパティは以下の通りです。
    </p>
    <ul>
        <li><strong>FileName </strong>- レポートファイルの相対パス。</li>
        <li><strong>Zoom</strong> - ページのズーム値。</li>
        <li><strong>Height </strong>-コントロールの高さ。このサンプルでは、475px に設定しています。</li>
        <li><strong>Width </strong>- コントロールの幅。このサンプルでは、100% に設定しています。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
