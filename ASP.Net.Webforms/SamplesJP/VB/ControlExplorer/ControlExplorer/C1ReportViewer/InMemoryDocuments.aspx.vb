
Imports C1.Web.Wijmo.Controls.C1ReportViewer
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.C1Preview
Imports System.Drawing
Namespace ControlExplorer.C1ReportViewer

	Public Partial Class InMemoryDocument
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
            C1.Web.Wijmo.Controls.C1ReportViewer.C1ReportViewer.RegisterDocument("InMemoryBasicTable", AddressOf BasicTable.MakeDoc)
		End Sub
	End Class

	''' <summary>
	''' BasicTable メモリ内のドキュメント
	''' </summary>
	Public Class BasicTable
		Public Shared Function MakeDoc() As C1PrintDocument
            Dim doc As C1PrintDocument = C1.Web.Wijmo.Controls.C1ReportViewer.C1ReportViewer.CreateC1PrintDocument()

			Dim rtxt1 As New RenderText(doc)
			rtxt1.Text = "このサンプルではC1PrintDocumentにおけるテーブルの基本機能を紹介します：" & vbLf & vbTab & "- テーブルの境界線 (GridLinesスタイルプロパティは外部の線を4個、内部の線を2個定義します）" & vbLf & vbTab & "- 単一セルとセルグループの周囲の境界線" & vbLf & vbTab & "- 非連結セルグループのスタイル属性（境界線を含む）" & vbLf & vbTab & "- 行や列方向に連結したセル" & vbLf & vbTab & "- （結合したものとそうでなものの）セル内の配置" & vbLf & vbTab & "- テーブルヘッダとフッタ" & vbLf & vbTab & "- （ページ番号やページ数のような）タグ（テーブルフッタを参照）" & vbLf & vbTab & "- 境界線、フォント、背景画像などの異なるスタイル属性" & vbLf & vbTab & "  " & vbLf
			rtxt1.Style.Font = New Font(rtxt1.Style.Font.FontFamily, 14)
			rtxt1.Style.Padding.Bottom = New C1.C1Preview.Unit("5mm")
			doc.Body.Children.Add(rtxt1)

			'
			' テーブルを作成してセルにデモ用データを設定
			Dim rt1 As New RenderTable(doc)
			Const  ROWS As Integer = 100
			Const  COLS As Integer = 4
			For row As Integer = 0 To ROWS - 1
				For col As Integer = 0 To COLS - 1
					Dim celltext As New RenderText(doc)
					celltext.Text = String.Format("セル ({0},{1})", row, col)
					' rt1.Cells(row, col)を参照すると自動的にセルが生成されることに注意してください。
					' 最初に行／列の数を指定する必要はありません。
					rt1.Cells(row, col).RenderObject = celltext
				Next
			Next
			' ドキュメントにテーブルを追加
			doc.Body.Children.Add(rt1)

			'
			' 新しいC1PrintDocumentではプログラム上の任意のポイントで変更を
			' 加えることが可能で、描画されるときに変更が反映されます。
			' いくつかの設定をテーブルに加えます：

			'
			' 標準ではテーブルは枠線を持たないため、単純な枠線を加えます：
			rt1.Style.GridLines.All = New LineDef("2pt", Color.DarkGray)
			rt1.Style.GridLines.Horz = New LineDef("1pt", Color.Blue)
			rt1.Style.GridLines.Vert = New LineDef("1pt", Color.Brown)

			'
			' テーブルヘッダとフッタ

			' テーブルヘッダを追加
			' 全体をヘッダとして設定
			rt1.RowGroups(0, 2).PageHeader = True
			rt1.RowGroups(0, 2).Style.BackgroundImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("~/C1ReportViewer/Images/orange.jpg"))
			rt1.RowGroups(0, 2).Style.BackgroundImageAlign.StretchHorz = True
			rt1.RowGroups(0, 2).Style.BackgroundImageAlign.StretchVert = True
			rt1.RowGroups(0, 2).Style.BackgroundImageAlign.KeepAspectRatio = False
			' スタイルでは多重継承をサポートします
			' グループスタイルの文字色はセルスタイルのフォントと結合されます：
			rt1.RowGroups(0, 2).Style.TextColor = Color.LightGreen
			rt1.RowGroups(0, 2).Style.GridLines.All = New LineDef("2pt", Color.DarkCyan)
			rt1.RowGroups(0, 2).Style.GridLines.Horz = LineDef.Empty
			rt1.RowGroups(0, 2).Style.GridLines.Vert = LineDef.Empty
			' ヘッダの指定したセルを設定：
			DirectCast(rt1.Cells(0, 0).RenderObject, RenderText).Text = "ヘッダ行 0"
			DirectCast(rt1.Cells(1, 0).RenderObject, RenderText).Text = "ヘッダ行 1"
			rt1.Cells(0, 1).SpanCols = 2
			rt1.Cells(0, 1).SpanRows = 2
			rt1.Cells(0, 1).RenderObject = New RenderText(doc)
			DirectCast(rt1.Cells(0, 1).RenderObject, RenderText).Text = "複数行のテーブルヘッダとフッタがサポートされています。"
			rt1.Cells(0, 1).Style.TextAlignHorz = AlignHorzEnum.Center
			rt1.Cells(0, 1).Style.Font = New Font("Arial", 14, FontStyle.Bold)

			' テーブルフッタを設定
			rt1.RowGroups(rt1.Rows.Count - 2, 2).PageFooter = True
			rt1.RowGroups(rt1.Rows.Count - 2, 2).Style.BackColor = Color.LemonChiffon
			rt1.Cells(rt1.Rows.Count - 2, 0).SpanRows = 2
			rt1.Cells(rt1.Rows.Count - 2, 0).SpanCols = rt1.Cols.Count - 1
			rt1.Cells(rt1.Rows.Count - 2, 0).Style.TextAlignHorz = AlignHorzEnum.Center
			rt1.Cells(rt1.Rows.Count - 2, 0).Style.TextAlignVert = AlignVertEnum.Center
			DirectCast(rt1.Cells(rt1.Rows.Count - 2, 0).RenderObject, RenderText).Text = "これはテーブルフッタです。"
			rt1.Cells(rt1.Rows.Count - 2, 3).SpanRows = 2
			rt1.Cells(rt1.Rows.Count - 2, 3).Style.TextAlignHorz = AlignHorzEnum.Right
			' （ページ番号／ページ数のような）タグ情報はドキュメントの任意の場所に挿入できます。
			DirectCast(rt1.Cells(rt1.Rows.Count - 2, 3).RenderObject, RenderText).Text = "ページ　[PageNo] / [PageCount]"

			'
			' テーブルではStyle.Bordersはテーブルの枠線とシームレスに結合されます：

			' 指定したセルの周囲に余白を設定するのは簡単です：
			rt1.Cells(8, 3).Style.Borders.All = New LineDef("3pt", Color.OrangeRed)
			DirectCast(rt1.Cells(8, 3).RenderObject, RenderText).Text = "cell.Style.Bordersを用いて1つのセルの周囲の境界線を簡単に設定できます。"

			'
			' セルはグループに結合可能であり、1つの要素として扱われます：

			' Rectangleオブジェクトを指定してセルをグループに結合してセルグループを定義します：
			DirectCast(rt1.Cells(3, 2).RenderObject, RenderText).Text = "セルはグループに結合可能であり、1つの要素として扱われます" & "（このグループのすべてのセルの背景をPaleGreenに設定しているように）。"
			rt1.Cells(3, 2).SpanCols = 2
			rt1.Cells(3, 2).SpanRows = 3
			Dim cells1 As Rectangle() = New Rectangle() {New Rectangle(2, 3, 2, 3), New Rectangle(0, 10, 3, 2), New Rectangle(1, 23, 2, 4), New Rectangle(1, 36, 1, 24), New Rectangle(0, 72, 3, 6)}
			Dim grp1 As New UserCellGroup(cells1)
			grp1.Style.BackColor = Color.PaleGreen
			grp1.Style.Font = New Font("Arial", 12, FontStyle.Bold)
			grp1.Style.Borders.All = New LineDef("2pt", Color.DarkGreen)
			rt1.UserCellGroups.Add(grp1)


			' 行/列の結合
			DirectCast(rt1.Cells(14, 1).RenderObject, RenderText).Text = "行や列が結合されたセルにおいても、文字列の配置がサポートされています。"
			rt1.Cells(14, 1).SpanCols = 3
			rt1.Cells(14, 1).SpanRows = 5
			rt1.Cells(14, 1).Style.Font = New Font("Arial", 12, FontStyle.Bold Or FontStyle.Italic)
			rt1.Cells(14, 1).Style.Borders.All = New LineDef("2pt", Color.DarkOrange)
			rt1.Cells(14, 1).Style.TextAlignHorz = AlignHorzEnum.Center
			rt1.Cells(14, 1).Style.TextAlignVert = AlignVertEnum.Center
            rt1.RowGroups(14, 5).SplitBehavior = SplitBehaviorEnum.Never

			Return doc
		End Function
	End Class
End Namespace
