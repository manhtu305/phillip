﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ToolbarCustomization.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.ToolBarCustomization" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="~/C1ReportViewer/C1ReportXML/BarcodeLabels.xml" ReportName="Product Labels (EAN-13, Label 5096)" Zoom="75%" CollapseToolsPanel="True" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
       <strong>C1ReportViewer</strong> のツールバーは、サーバー側またはクライアント側のコードからカスタマイズできます。</p>
    <p>
       このサンプルでは、「印刷」ボタンと「次のページ」 ボタン以降の全てのボタンを​サーバー側で削除しています。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
