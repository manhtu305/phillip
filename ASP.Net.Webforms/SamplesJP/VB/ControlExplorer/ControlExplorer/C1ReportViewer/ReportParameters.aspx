﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ReportParameters.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.ReportParameters" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="~/C1ReportViewer/RDL/ReportParameters01.rdl" Zoom="75%" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1ReportViewer</strong> は、<strong>C1Report</strong> と <strong>C1RdlReport</strong> においてパラメータをサポートしています。
    </p>
    <p>
        パラメータペインは、レポートがパラメータ入力を必要とするときコントロール上に表示され、展開・縮小が可能です。
        パラメータは、レポートの生成を開始する前にユーザーによって入力される必要があります。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
