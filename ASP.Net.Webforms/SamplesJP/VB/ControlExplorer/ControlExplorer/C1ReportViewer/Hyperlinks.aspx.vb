
Imports C1.Web.Wijmo.Controls.C1ReportViewer
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports C1.C1Preview
Namespace ControlExplorer.C1ReportViewer


	Public Partial Class Hyperlinks
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
            C1.Web.Wijmo.Controls.C1ReportViewer.C1ReportViewer.RegisterDocument("Hyperlinks", AddressOf HyperlinksDoc.MakeDoc)
		End Sub
	End Class

	Public Class HyperlinksDoc
		Public Shared Function MakeDoc() As C1PrintDocument
            Dim doc As C1PrintDocument = C1.Web.Wijmo.Controls.C1ReportViewer.C1ReportViewer.CreateC1PrintDocument()

			doc.Style.FontSize += 2

			' 先頭|前|次|最終 のページへのリンクを持つページヘッダを作成
			Dim rtnav As New RenderTable()
			' ページナビゲータのため、リンク先に移動した場合の異なる表示を抑制します。
			rtnav.Style.VisitedHyperlinkAttrs = rtnav.Style.HyperlinkAttrs
			' スペースを挿入します。
			rtnav.Style.Spacing.Bottom = "5mm"
			' ナビゲータリンクを追加します。
			rtnav.Cells(0, 0).Text = "先頭ページ"
			rtnav.Cells(0, 0).RenderObject.Hyperlink = New C1Hyperlink(New C1LinkTargetPage(PageJumpTypeEnum.First), "先頭ページに移動")
			rtnav.Cells(0, 1).Text = "前ページ"
			rtnav.Cells(0, 1).RenderObject.Hyperlink = New C1Hyperlink(New C1LinkTargetPage(PageJumpTypeEnum.Previous), "前ページに移動")
			rtnav.Cells(0, 2).Text = "次ページ"
			rtnav.Cells(0, 2).RenderObject.Hyperlink = New C1Hyperlink(New C1LinkTargetPage(PageJumpTypeEnum.[Next]), "次ページに移動")
			rtnav.Cells(0, 3).Text = "最終ページ"
			rtnav.Cells(0, 3).RenderObject.Hyperlink = New C1Hyperlink(New C1LinkTargetPage(PageJumpTypeEnum.Last), "最終ページに移動")
			doc.PageLayout.PageHeader = rtnav

			' ドキュメントの本体を作成

			' アンカーを作成
			Dim rt1 As New RenderText("この文字列にはanchor1が設定されています。")
			' ("anchor1") という名は、このリンクに飛び出すため使用されます。
			rt1.Anchors.Add(New C1Anchor("anchor1"))
			rt1.Hyperlink = New C1Hyperlink(New C1LinkTargetPage(PageJumpTypeEnum.Last), "ドキュメントの最終ページへ移動")
			doc.Body.Children.Add(rt1)

			' doc2　を開くためリンクを追加します。
			'
'			RenderText rt2 = new RenderText("'StylesInTables' ドキュメントを開く場合、ここにクリックしてください。");
'			rt2.Hyperlink = new C1Hyperlink(new C1LinkTargetFile(
'				"javascript:window.changeReport('','StylesInTables')"));
'			doc.Body.Children.Add(rt2);
'			


			' ダミー文字列を追加
			For i As Integer = 0 To 499
				doc.Body.Children.Add(New RenderText(String.Format("... filler {0} ...", i)))
			Next

			' anchor1 にハイパーリンクを追加します。
			Dim rt3 As New RenderText("クリックするとanchor1に移動します。")
			rt3.Hyperlink = New C1Hyperlink(New C1LinkTargetAnchor("anchor1"), "これはanchor1へのリンクの上にカーソルを移動すると表示される文字列です。")
			doc.Body.Children.Add(rt3)

			' 描画オブジェクトへジャンプするためにアンカーは必要ありません
			Dim rt4 As New RenderText("クリックするとドキュメントの中間へ移動します。")
            rt4.Hyperlink = New C1Hyperlink(doc.Body.Children(CInt(doc.Body.Children.Count / 2)))
			rt4.Hyperlink.StatusText = "ドキュメントの中間付近へ移動します"
			doc.Body.Children.Add(rt4)

			' URLへのハイパーリンクを持つ画像を追加
			Dim ri1 As New RenderImage(Image.FromFile(HttpContext.Current.Server.MapPath("~/C1ReportViewer/Images/google.gif")))
			ri1.Hyperlink = New C1Hyperlink(New C1LinkTargetFile("http://www.google.co.jp"), " Google で検索する... (Ctrl+クリックしてリンクを新しいウインドウで開きます。")
			doc.Body.Children.Add(ri1)


			Dim rt5 As New RenderText("alert 'Hello'.")
			rt5.Hyperlink = New C1Hyperlink(New C1LinkTargetFile("javascript:alert('Hello')"))
			rt5.Hyperlink.StatusText = "'Hello' メッセージを表示する。"
			doc.Body.Children.Add(rt5)

			Dim rt6 As New RenderText("printWithPreview")
			rt6.Hyperlink = New C1Hyperlink(New C1LinkTargetFile("exec:printWithPreview()"))
			rt6.Hyperlink.StatusText = "レポートをプレビューして印刷します。"
			doc.Body.Children.Add(rt6)


			Return doc
		End Function
	End Class

End Namespace
