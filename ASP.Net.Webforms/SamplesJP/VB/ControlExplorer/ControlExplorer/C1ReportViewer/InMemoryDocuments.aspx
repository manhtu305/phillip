﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="InMemoryDocuments.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.InMemoryDocument" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="InMemoryBasicTable" Zoom="75%" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、メモリ上にレポートを動的に作成します。</p>
    <p>
        <strong>C1ReportViewer.RegisterDocument </strong>メソッドを使用して、コードビハインドでレポート／ドキュメントを動的に登録することができます。</p>
    <p>
    	<strong>C1ReportViewer.RegisterDocument</strong> メソッドは2つのパラメータを受け取ります。最初のパラメータは動的レポート／ドキュメントの名前で、2番目のパラメータは、レポートを生成するために呼び出されるデリゲートです。</p>
    <p>
      	<strong>FileName</strong> プロパティは、<strong>C1ReportViewer.RegisterDocument</strong> メソッドの最初のパラメータとして使用される動的レポートの名前に設定する必要があります。
    </p>
    <strong>メモ</strong>:&nbsp; 複数の動的レポートを異なる名前で登録し、サーバー側またはクライアント側で <strong>FileName</strong> プロパティを変更することにより、これらのレポートを切り替えることができます。
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
