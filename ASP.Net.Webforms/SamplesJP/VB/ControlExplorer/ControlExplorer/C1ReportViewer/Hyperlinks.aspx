﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Hyperlinks.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.Hyperlinks" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="Hyperlinks" Zoom="75%" CollapseToolsPanel="True" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、レポートのハイパーリンク機能を紹介します。</p>
    <p>
        ハイパーリンクを追加すると、Webページや他のレポートを開いたり、同じレポート内の異なる場所へ移動、C1ReportViewer の外観の変更、カスタム JavaScript の実行などが可能です。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
