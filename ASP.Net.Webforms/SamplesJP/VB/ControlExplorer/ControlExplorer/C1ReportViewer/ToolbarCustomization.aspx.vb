Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Namespace ControlExplorer.C1ReportViewer
	Public Partial Class ToolBarCustomization
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			Dim toolbar As HtmlGenericControl = Me.C1ReportViewer1.ToolBar
			For i As Integer = toolbar.Controls.Count - 1 To 0 Step -1
				Dim className As String = DirectCast(toolbar.Controls(i), HtmlGenericControl).Attributes("class")
				If Not String.IsNullOrEmpty(className) AndAlso className.Contains("print") Then
					' "印刷" ボタンを削除します。
					toolbar.Controls.RemoveAt(i)
				End If
				If i > 6 Then
					toolbar.Controls.RemoveAt(i)
				End If
			Next
		End Sub
	End Class
End Namespace
