﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FlexibleSize.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.FlexibleSize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="parentContainerSample" style="width: 720px; height: 475px;">
        <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="C1ReportViewer/C1ReportXML/BarcodeLabels.xml" ReportName="Product Labels (EAN-13, Label 5096)" Zoom="Fit width" Height="100%" Width="100%" CollapseToolsPanel="True">
        </C1ReportViewer:C1ReportViewer>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1ReportViewer</strong> では、ビューアの幅や高さをパーセント単位で指定することができます。</p>
    <p>
        このサンプルで使用されるプロパティは以下の通りです。
    </p>
    <ul>
        <li><strong>Height </strong>- コントロールの高さを指定する（このサンプルでは 100% を指定）</li>
        <li><strong>Width </strong>- コントロールの幅を指定する（このサンプルでは 100% を指定）</li>
    </ul>
    <p>
        このサンプルでは、​​クライアント側スクリプトから親コンテナのサイズを変更しています。
        <strong>C1ReportViewer</strong> のレイアウトは、親コンテナのサイズ変更に応じて調整されます。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <script language="javascript" type="text/javascript">
        function changeParentSize(w, h) {
            $("#parentContainerSample").width(w).height(h);
            $("#<%=C1ReportViewer1.ClientID%>").c1reportviewer("refresh");
        }
    </script>
    <p>
        親コンテナのサイズ変更 </p>
    <ul>
        <li><a href="javascript:changeParentSize(320, 240);">親のサイズを 320x240px にする</a><br />
        </li>
        <li><a href="javascript:changeParentSize(600, 400);">親のサイズを 600x400px にする</a><br />
        </li>
        <li><a href="javascript:changeParentSize('100%', '475');">親のサイズを 100% x 475px にする</a><br />
        </li>
    </ul>
</asp:Content>
