﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ErrorHandling.aspx.vb" Inherits="ControlExplorer.C1ReportViewer.ErrorHandling" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="C1ReportViewer/C1ReportXML/BarcodeLabels.xml" ReportName="Wrong Report Name" EnableLogs="true" CollapseToolsPanel="True" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1ReportViewer</strong> コントロールは、デバッグ用に組込みログコンソールを表示することができます。</p>
    <p>
        このサンプルで使用されるプロパティは以下の通りです。
    </p>
    <ul>
        <li><strong>EnableLogs </strong>- 組込みログコンソールを使用するかどうか。このサンプルでは True に設定しています。</li>
    </ul>
    <p>
        「Wrong Report Name」というレポート名は、<strong>FileName</strong> プロパティで指定したレポートに存在しないため、ログコンソールにエラーメッセージが表示されます。</p>
    <p>
        ログコンソールには、ドキュメントの生成状態や現在のビューアの動作など、様々なメッセージが表示されます。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
