Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1EventsCalendar
	Public Partial Class CustomTimeInterval
		Inherits System.Web.UI.Page
		Protected Overrides Sub OnInit(e As EventArgs)
			Me.Session("C1EvCalSessionUsed") = True
			Dim sessionDataFileName As String = "~/C1EventsCalendar/" & [String].Format("c1evcaldata{0}.xml", Me.User.Identity.Name & Me.Session.LCID).Replace("\", "")
			Me.C1EventsCalendar1.DataStorage.DataFile = sessionDataFileName
			MyBase.OnInit(e)
		End Sub
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub
	End Class
End Namespace
