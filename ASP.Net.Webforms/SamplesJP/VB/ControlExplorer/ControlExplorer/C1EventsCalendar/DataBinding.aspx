﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.vb" Inherits="ControlExplorer.C1EventsCalendar.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px" VisibleCalendars="" SelectedDate="2012/09/26">
	<DataStorage>
		<EventStorage DataSourceID="AccessDataSource_Events">
			<Mappings>	
				<IdMapping MappingName="AppointmentId" />			
				<StartMapping MappingName="Start" />
				<EndMapping MappingName="End" />
				<SubjectMapping MappingName="Subject" />
				<LocationMapping MappingName="Location" />
				<DescriptionMapping MappingName="Description" />
				<ColorMapping MappingName="Color" />
				<CalendarMapping MappingName="Calendar" />
				<TagMapping MappingName="Tag" />
			</Mappings>
		</EventStorage>
		<CalendarStorage DataSourceID="AccessDataSource_Calendars">
			<Mappings>
				<IdMapping MappingName="CalendarId" />
				<LocationMapping MappingName="Location" />
				<ColorMapping MappingName="Color" />
				<DescriptionMapping MappingName="Description" />
				<NameMapping MappingName="Name" />
				<PropertiesMapping MappingName="Properties" />
				<TagMapping MappingName="Tag" />
			</Mappings>
		</CalendarStorage>
	</DataStorage>
	</wijmo:C1EventsCalendar>

	<asp:AccessDataSource ID="AccessDataSource_Events" runat="server" 
		DataFile="~/App_Data/EventsCalendarNwind.mdb" 
		DeleteCommand="DELETE FROM [Appointments] WHERE [AppointmentId] = ?" 
		InsertCommand="INSERT INTO [Appointments] ([AppointmentId], [Description], [End], [Location], [Start], [Subject], [Properties], [Color], [Calendar], [Tag]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
		SelectCommand="SELECT * FROM [Appointments]" 
		
		UpdateCommand="UPDATE [Appointments] SET [Description] = ?, [End] = ?, [Location] = ?, [Start] = ?, [Subject] = ?, [Properties] = ?, [Color] = ?, [Calendar] = ?, [Tag] = ? WHERE [AppointmentId] = ?">
		<DeleteParameters>
			<asp:Parameter Name="AppointmentId" Type="Object" />
		</DeleteParameters>
		<InsertParameters>
			<asp:Parameter Name="AppointmentId" Type="Object" />
			<asp:Parameter Name="Description" Type="String" />
			<asp:Parameter Name="End" Type="DateTime" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Start" Type="DateTime" />
			<asp:Parameter Name="Subject" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Calendar" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
		</InsertParameters>
		<UpdateParameters>
			<asp:Parameter Name="Description" Type="String"/>
			<asp:Parameter Name="End" Type="DateTime" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Start" Type="DateTime" />
			<asp:Parameter Name="Subject" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Calendar" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
			<asp:Parameter Name="AppointmentId" Type="Object" />
		</UpdateParameters>
	</asp:AccessDataSource>
	<asp:AccessDataSource ID="AccessDataSource_Calendars" runat="server" 
		DataFile="~/App_Data/EventsCalendarNwind.mdb" 
		DeleteCommand="DELETE FROM [Calendars] WHERE [CalendarId] = ?" 
		InsertCommand="INSERT INTO [Calendars] ([CalendarId], [Name], [Description], [Color], [Tag], [Location], [Properties]) VALUES (?, ?, ?, ?, ?, ?, ?)" 
		SelectCommand="SELECT * FROM [Calendars]" 
		
		UpdateCommand="UPDATE [Calendars] SET [Name] = ?, [Description] = ?, [Color] = ?, [Tag] = ?, [Location] = ?, [Properties] = ? WHERE [CalendarId] = ?">
		<DeleteParameters>
			<asp:Parameter Name="CalendarId" Type="String" />
		</DeleteParameters>
		<InsertParameters>
			<asp:Parameter Name="CalendarId" Type="String" />
			<asp:Parameter Name="Name" Type="String" />
			<asp:Parameter Name="Description" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
		</InsertParameters>
		<UpdateParameters>
			<asp:Parameter Name="Name" Type="String" />
			<asp:Parameter Name="Description" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
			<asp:Parameter Name="CalendarId" Type="String" />
		</UpdateParameters>
	</asp:AccessDataSource>
	<br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">



	<p>このサンプルでは、<strong>C1EventsCalendar</strong> コントロールでデータ連結する方法を紹介します。</p>
	<p>
	C1EventsCalendar をデータソースに連結するには、次の手順に従います。
	</p>
    <br />
	<p>EventStorage の設定</p>
	<ul>
		<li>
			ページにデータソースコントロールを追加します。
		</li>
		<li>
			EventStorage の DataSourceID プロパティを設定します。必要に応じて、DataMember プロパティを設定します。
		</li>
		<li>
			エンドユーザーがイベントを編集できるようする場合は、Insert/Update/Delete コマンドを生成します。
		</li>
		<li>
			EventStorage のデータマッピングを設定します。
<pre class="controldescription-code">
    &lt;EventStorage DataSourceID="AccessDataSource_Events"&gt;
        &lt;Mappings&gt;
            &lt;IdMapping MappingName="AppointmentId" /&gt;
            &lt;StartMapping MappingName="Start" /&gt;
            &lt;EndMapping MappingName="End" /&gt;
            &lt;SubjectMapping MappingName="Subject" /&gt;
            &lt;LocationMapping MappingName="Location" /&gt;
            &lt;DescriptionMapping MappingName="Description" /&gt;
            &lt;ColorMapping MappingName="Color" /&gt;
        &lt;/Mappings&gt;
    &lt;/EventStorage&gt;
</pre>
		</li>
	</ul>
    <br />
	<p>CalendarStorage の設定</p>
	<ul>
		<li>
			ページにデータソースコントロールを追加します。
		</li>
		<li>
			CalendarStorage の DataSourceID プロパティを設定します。必要に応じて、DataMember プロパティを設定します。
		</li>
		<li>
			エンドユーザーがカレンダーを編集できるようにする場合は、Insert/Update/Delete コマンドを生成します。
		</li>
		<li>
			CalendarStorage のデータマッピングを設定します。
<pre class="controldescription-code">
    &lt;CalendarStorage DataSourceID="AccessDataSource_Calendars"&gt;
        &lt;Mappings&gt;
            &lt;IdMapping MappingName="CalendarId" /&gt;
            &lt;LocationMapping MappingName="Location" /&gt;
            &lt;ColorMapping MappingName="Color" /&gt;
            &lt;DescriptionMapping MappingName="Description" /&gt;
            &lt;NameMapping MappingName="Name" /&gt;
            &lt;PropertiesMapping MappingName="Properties" /&gt;
            &lt;TagMapping MappingName="Tag" /&gt;
        &lt;/Mappings&gt;
    &lt;/CalendarStorage&gt;
</pre>
		</li>
	</ul>
</asp:Content>
