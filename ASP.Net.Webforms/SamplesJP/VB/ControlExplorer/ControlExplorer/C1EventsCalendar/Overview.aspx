﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1EventsCalendar.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px"></wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、<strong>C1EventsCalendar</strong> の既定の動作を紹介します。
	</p>
	<p>
		<strong>C1EventsCalendar</strong> コントロールはフル機能のスケジューラであり、ユーザーは予定を追加、編集、管理することができます。
	</p>
	<p>
		既定では、C1EventsCalendar はオフラインのデータソースを使用します。そのため、追加設定をしなくてもスケジューラ機能を Web ページに簡単に追加することができます。
	</p>
</asp:Content>
