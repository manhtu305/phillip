'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1EventsCalendar


	Public Partial Class DataBinding

		''' <summary>
		''' C1EventsCalendar1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1EventsCalendar1 As Global.C1.Web.Wijmo.Controls.C1EventsCalendar.C1EventsCalendar

		''' <summary>
		''' AccessDataSource_Events コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected AccessDataSource_Events As Global.System.Web.UI.WebControls.AccessDataSource

		''' <summary>
		''' AccessDataSource_Calendars コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected AccessDataSource_Calendars As Global.System.Web.UI.WebControls.AccessDataSource
	End Class
End Namespace
