﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Import.aspx.vb" Inherits="ControlExplorer.C1EventsCalendar.Import1" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
		<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" 
			ViewType="Month" SelectedDate="2011-03-01"
			Width="100%"  Height="475px"></wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、iCal 形式データからイベントをインポートする方法を紹介します。
    </p>
	<p>サンプルで使用されているメソッドは下記の通りです。</p>
	<ul>
		<li><strong>DataStorage.Import</strong> - 指定した形式のファイルからイベントカレンダーにデータをインポートします。</li>
		<li><strong>DataStorage.SaveData</strong> - データストレージの設定に応じて、データソースまたは XML ファイルにイベントデータを保存します。</li>
	</ul>
	<p>メモ：データの変更を維持するためには、インポート操作の後で SaveData() メソッドを実行しなければなりません。</p>
	<pre class="controldescription-code">
C1EventsCalendar1.DataStorage.Import(
	Server.MapPath("pens_schedule_1011_full.ics"), 
	FileFormatEnum.iCal);
C1EventsCalendar1.DataStorage.SaveData();
	</pre>
</asp:Content>
