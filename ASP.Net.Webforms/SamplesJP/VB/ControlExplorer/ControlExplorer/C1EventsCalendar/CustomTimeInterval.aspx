﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomTimeInterval.aspx.vb" Inherits="ControlExplorer.C1EventsCalendar.CustomTimeInterval" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px" TimeInterval="60" TimeIntervalHeight="25" TimeRulerInterval="120"></wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、日ビューの時間間隔を変更する方法を紹介します。
	</p>
	<p>サンプルで使用されているプロパティは以下の通りです。</p>
	<ul>
	<li><strong>TimeInterval</strong> - 日ビューの時間間隔（分）</li>
	<li><strong>TimeIntervalHeight</strong> - 日ビューの時間間隔行の高さ（ピクセル）</li>
	<li><strong>TimeRulerInterval</strong> - 日ビューの時間目盛間隔（分）</li>
	</ul>
</asp:Content>
