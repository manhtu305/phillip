Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1EventsCalendar

Namespace ControlExplorer.C1EventsCalendar
	Public Partial Class Import1
		Inherits System.Web.UI.Page
		Protected Overrides Sub OnInit(e As EventArgs)
			Me.Session("C1EvCalSessionUsed") = True
			Dim sessionDataFileName As String = "~/C1EventsCalendar/" & [String].Format("c1evcaldata{0}.xml", Me.User.Identity.Name & Me.Session.LCID).Replace("\", "")
			Me.C1EventsCalendar1.DataStorage.DataFile = sessionDataFileName
			MyBase.OnInit(e)
		End Sub
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			C1EventsCalendar1.DataStorage.Import(Server.MapPath("pens_schedule_1011_full.ics"), FileFormatEnum.iCal)
			C1EventsCalendar1.DataStorage.SaveData()
		End Sub
	End Class
End Namespace
