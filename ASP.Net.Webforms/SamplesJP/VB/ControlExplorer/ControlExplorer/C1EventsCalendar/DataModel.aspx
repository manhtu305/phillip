﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataModel.aspx.vb" Inherits="ControlExplorer.C1EventsCalendar.DataModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script src="../explore/js/amplify.core.min.js" type="text/javascript"></script>
    <script src="../explore/js/amplify.store.min.js" type="text/javascript"></script>
    <style type="text/css">
        .top-pane
        {
            margin: 1em 0;
            padding: 1em;
        }
        .calendaractions p, 
        .addnewevent, .events-title
        {
            margin-bottom: 1em;
        }
        #eventscalendar
        {
            width: 750px;
        }                
        
		.calendarslist .ui-selecting { background: #FECA40; }
		.calendarslist .ui-selected { background: #F39814; color: white; }
		.calendarslist { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		.calendarslist li { margin-left: 0px; margin: 3px; padding: 0.2em; font-size: 1em; height: 16px; }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px"
 OnClientInitialized="initialized" HeaderBarVisible="true" OnClientCalendarsChanged="calendarsChanged">
 <DataStorage DataFile=""> 
 </DataStorage>
 </wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="top-pane ui-helper-clearfix ui-widget-content ui-corner-all">
        <div class="calendaractions">
            <h3 class="events-title">
                イベント</h3>
            <div class="eventactions">
                <a class="addnewevent" href="#">ダイアログを使用して追加</a>&nbsp;&nbsp; <a class="addneweventwithoutdialog"
                    href="#">ダイアログを使用せずに追加</a>&nbsp;&nbsp; <a class="deleteEventsForActiveDay" href="#">
                        現在の日付の全イベントを削除（確認なし）</a>
            </div>
            <br />
            <br />
            <h3>
                カレンダー</h3>
            <div class="calendarslist-container">
                <ul class="calendarslist">
                    <li>読み込み中...</li>
                </ul>
            </div>
            <p>
                <a class="calendarsettings">編集</a> <a class="deletecalendar">削除</a>
            </p>
            <p>
                <a class="addnewcalendar">カレンダーの追加</a>
            </p>
        </div>
    </div>
    <script type="text/javascript">
        function initialized(e) {
            // コントロールの初期化時に、使用可能なカレンダーを読み込みます。
            loadCalendarsList();
        }
        function calendarsChanged(e, args) {
            // カレンダーのオプションが変更されたときに、使用可能なカレンダーを読み込みます。
            loadCalendarsList();
        }
        function getEvCal() {
            return $("#<%= C1EventsCalendar1.ClientID%>");
        }
        function onAddNewEventClick() {
            getEvCal().c1eventscalendar("showEditEventDialog");
        }
        function onAddNewEventWithoutDialogClick() {
            var o = {};
            o.subject = "New event";
            o.start = new Date();
            o.end = new Date(o.start.getTime() + 1000 * 60 * 60 * 2); // duration 2 hours
            getEvCal().c1eventscalendar("addEvent", o);
        }
        function onDeleteEventsForActiveDayClick() {
            var now = new Date(),
					selectedDate = getEvCal().c1eventscalendar("option", "selectedDate"),
    				start = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate()),
					end = new Date(start.getTime() + 1000 * 60 * 60 * 24),
					events, i;
            events = getEvCal().c1eventscalendar("getOccurrences", start, end);

            for (i = 0; i < events.length; i++) {
                getEvCal().c1eventscalendar("deleteEvent", events[i]);
            }

        }

        function onAddNewCalendarClick() {
            getEvCal().c1eventscalendar("showEditCalendarDialog");
        }
        function onEditCalendarClick() {
            if ($(".calendarslist").find(".ui-selected").length > 0) {
                var calName = $(".calendarslist").find(".ui-selected").find("label").html();
                getEvCal().c1eventscalendar("showEditCalendarDialog", calName);
            }
        }
        function onDeleteCalendarClick() {
            $(".calendarslist").find(".ui-selected").each(function () {
                getEvCal().c1eventscalendar("deleteCalendar", $(this).find("label").html());
            });
        }
        function loadCalendarsList() {
            var calendars = getEvCal().c1eventscalendar("option", "calendars"),
					i, c, calendarslist = $(".calendarslist");
            calendarslist.html("");
            for (i = 0, c = calendars.length; i < c; i += 1) {
                calendarslist.append("<li class=\"ui-widget-content\"><label>" + calendars[i].name + "</label></li>");
            }
            $(".calendaractions .calendarsettings").button("option", "disabled", true);
            $(".calendaractions .deletecalendar").button("option", "disabled", true);
        }

        $(document).ready(function () {
            $(".eventactions .addnewevent")/*.button()*/
					.click($.proxy(onAddNewEventClick, this));
            $(".eventactions .addneweventwithoutdialog")/*.button()*/
					.click($.proxy(onAddNewEventWithoutDialogClick, this));
            $(".eventactions .deleteEventsForActiveDay")/*.button()*/
					.click($.proxy(onDeleteEventsForActiveDayClick, this));
            //

            $(".calendaractions .addnewcalendar").button()
							.click($.proxy(onAddNewCalendarClick, this));
            $(".calendaractions .calendarsettings").button({ disabled: true })
							.click($.proxy(onEditCalendarClick, this));
            $(".calendaractions .deletecalendar").button({ disabled: true })
							.click($.proxy(onDeleteCalendarClick, this));

            $(".calendarslist").selectable({
                selected: function (event, ui) {
                    if ($(".calendarslist").find(".ui-selected").length > 0) {
                        $(".calendaractions .deletecalendar").button("option", "disabled", false);
                        if ($(".calendarslist").find(".ui-selected").length === 1) {
                            $(".calendaractions .calendarsettings").button("option", "disabled", false);
                        } else {
                            $(".calendaractions .calendarsettings").button("option", "disabled", true);
                        }
                    }
                },
                unselected: function (event, ui) {
                    if ($(".calendarslist").find(".ui-selected").length === 1) {
                        $(".calendaractions .calendarsettings").button("option", "disabled", false);
                    } else if ($(".calendarslist").find(".ui-selected").length < 1) {
                        $(".calendaractions .calendarsettings").button("option", "disabled", true);
                        $(".calendaractions .deletecalendar").button("option", "disabled", true);
                    }
                }
            });

            $(".calendarslist").on("change", "input", function () {
                var checkboxes = $(".calendarslist").find("input"), i,
					visibleCalendars = [];
                for (i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        visibleCalendars.push(checkboxes[i].value);
                    }
                }
                getEvCal().c1eventscalendar("option", "visibleCalendars", visibleCalendars);
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、クライアント側スクリプトを使用してイベントを編集する方法を紹介します。
    </p>
	<p>サンプルで使用されているオプションは下記の通りです。</p>
	<ul>
		<li><strong>calendars</strong> - 使用可能なカレンダーオブジェクトの配列（読み取り専用）</li>
		<li><strong>selectedDate</strong> - 選択された日付</li>
		<li><strong>visibleCalendars</strong> - 表示するカレンダー名の配列</li>
	</ul>
	<p>サンプルで使用されているクライアント側メソッドは下記の通りです。</p>
	<ul>
		<li><strong>getOccurrences</strong> - 定期的なイベント用に event オブジェクトの複数インスタンスを作成して配列を取得します。</li>
		<li><strong>addEvent</strong> - データソースに新規イベントを追加します。</li>
		<li><strong>deleteEvent</strong> - データソースからイベントを削除します。</li>
		<li><strong>deleteCalendar</strong> - データソースから既存のカレンダーを削除します。</li>
		<li><strong>showEditEventDialog</strong> - 組み込みの[イベントの編集]ダイアログを表示します。</li>
		<li><strong>showEditCalendarDialog</strong> - 組み込みの[カレンダーの編集]ダイアログを表示します。</li>
	</ul>
</asp:Content>