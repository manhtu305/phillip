﻿<%@ Control Language="vb" AutoEventWireup="true" CodeBehind="ClientLogger.ascx.vb"
	Inherits="ControlExplorer.ClientLogger" %>

<b><asp:Label runat="server" ID="Label1" Text="ログ"></asp:Label></b>
<div id="sampleLog" style="height: 100px; width: 100%; margin-bottom: 10px; overflow: auto;
	border: 1px dashed gray;">
</div>
<script language="javascript" type="text/javascript">
	function ClientLogger() {

	}
	ClientLogger.prototype = {
		message: function (msg) {
			$("#sampleLog").html("[" + new Date().toTimeString() + "] " + msg + "<br />" + $("#sampleLog").html());
		}	
	}
	window.log = new ClientLogger();

</script>
