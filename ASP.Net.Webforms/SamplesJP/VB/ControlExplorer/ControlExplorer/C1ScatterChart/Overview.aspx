﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1ScatterChart.Overview" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		function hintContent() {
		    return this.x + ' cm, ' + this.y + ' kg';
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ScatterChart ID="C1ScatterChart1" runat="server" Height="475" Width="756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<SeriesTransition Duration="2000" Enabled="false">
		</SeriesTransition>
		<Animation Duration="2000" Enabled="false">
		</Animation>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#AFE500">
				<Fill Color="#AFE500">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#FF9900">
				<Fill Color="#FF9900">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<Header Text="身長／体重 分布図">
		</Header>
		<Footer Compass="South" Visible="False">
		</Footer>
		<Legend>
			<Size Width="30" Height="3">
			</Size>
		</Legend>
		<Axis>
			<X Text="身長（cm）">
				<Labels>
					<AxisLabelStyle FontSize="11pt" Rotation="-45">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</Labels>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
			</X>
			<Y compass="West" text="体重（kg）" visible="False">
				<labels textalign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</labels>
				<gridmajor visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- "></GridStyle>
				</gridmajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMinor>
			</Y>
		</Axis>
	</wijmo:C1ScatterChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>C1ScatterChart では、カスタマイズ可能な散布図を作成できます。</p>
	<p>
		このサンプルでは、C1ScatterChart にてヘッダーをグラフに追加する方法、軸を書式設定する方法、グラフデータの生成方法を示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
