﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Trendline.aspx.vb" Inherits="ControlExplorer.C1ScatterChart.Trendline" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
	    function hintContent() {
	        return this.x + ' cm, ' + this.y + ' kg';
	    }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ScatterChart  ID="C1ScatterChart1" runat="server" Height="475" Width="756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<SeriesTransition Duration="2000" Enabled="false">
		</SeriesTransition>
		<Animation Duration="2000" Enabled="false">
		</Animation>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#AFE500">
				<Fill Color="#AFE500">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#FF9900">
				<Fill Color="#FF9900">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<Header Text="Height Versus Weight of 72 Individuals by Gender">
		</Header>
		<Footer Compass="South" Visible="False">
		</Footer>
		<Legend Visible="true">
			<Size Width="30" Height="3">
			</Size>
		</Legend>
		<Axis>
			<X Text="Height (cm)">
				<Labels>
					<AxisLabelStyle FontSize="11pt" Rotation="-45">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</Labels>
                <GridMajor Visible="false"></GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
			</X>
			<Y compass="West" text="Weight (kg)" visible="true" >
				<labels textalign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</labels>
				<gridmajor visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- "></GridStyle>
				</gridmajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMinor>
			</Y>
		</Axis>
	</wijmo:C1ScatterChart>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=C1ScatterChart1.ClientID%>').c1scatterchart('option', 'showChartLabels', true);
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        <strong>C1ScatterChart</strong>の近似曲線を表示します。
    </p>
    <p>
        以下のプロパティを設定して、近似曲線をカスタマイズできます。
    </p>
    <ul>
        <li>
            <strong>FitType</strong> - 近似曲線の種類を指定します。
        </li>
        <li>
            <strong>SampleCount</strong> - 関数計算のサンプル数を指定します。多項式、指数、累乗、指数、対数、フーリエでのみ有効です。
        </li>
        <li>
            <strong>Order</strong> - 多項式の次数を定義します。多項式、指数、累乗、指数、対数、フーリエでのみ有効です。
        </li>
    </ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label>次数:</label>
                    <wijmo:C1InputNumeric ID="inputOrder" runat="server" Width="80px" ShowSpinner="true" Value="4" MinValue="1" MaxValue="10" DecimalPlaces="0"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label>サンプル数:</label>
                    <wijmo:C1InputNumeric ID="inputSampleCount" runat="server" Width="80px" ShowSpinner="true" Value="100" MinValue="1" MaxValue="200" DecimalPlaces="0"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label>種類:</label>
                    <asp:DropDownList ID="dplFitType" runat="server">
                        <asp:ListItem Text="多項式" Value="Polynom" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="指数" Value="Exponent"></asp:ListItem>
                        <asp:ListItem Text="対数" Value="Logarithmic"></asp:ListItem>
                        <asp:ListItem Text="累乗" Value="Power"></asp:ListItem>
                        <asp:ListItem Text="フーリエ" Value="Fourier"></asp:ListItem>
                        <asp:ListItem Text="Xの最小" Value="MinX"></asp:ListItem>
                        <asp:ListItem Text="Yの最小" Value="MinY"></asp:ListItem>
                        <asp:ListItem Text="Xの最大" Value="MaxX"></asp:ListItem>
                        <asp:ListItem Text="Yの最大" Value="MaxY"></asp:ListItem>
                        <asp:ListItem Text="Xの平均" Value="AverageX"></asp:ListItem>
                        <asp:ListItem Text="Yの平均" Value="AverageY"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
        <div class="settingcontrol">
            <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
        </div>
    </div>
</asp:Content>
