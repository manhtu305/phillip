Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1Chart

Namespace ControlExplorer.C1ScatterChart
	Public Partial Class Overview
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack Then
				PrepareOptions()
			End If
		End Sub

		Private Sub PrepareOptions()
			Dim valuesX = New List(Of System.Nullable(Of Double))() From { _
				161.4, _
				169.0, _
				166.2, _
				159.4, _
				162.5, _
				159.0, _
				162.8, _
				159.0, _
				179.8, _
				162.9, _
				161.0, _
				151.1, _
				168.2, _
				168.9, _
				173.2, _
				174.0, _
				162.6, _
				174.0, _
				162.6, _
				161.3, _
				156.2, _
				149.9, _
				169.5, _
				160.0, _
				175.3, _
				169.5, _
				160.0, _
				172.7, _
				162.6, _
				157.5, _
				176.5, _
				164.4, _
				160.7, _
				174.0, _
				163.8 _
			}
			Dim valuesY = New List(Of System.Nullable(Of Double))() From { _
				63.4, _
				58.2, _
				58.6, _
				45.7, _
				52.2, _
				48.6, _
				57.8, _
				55.6, _
				66.8, _
				59.4, _
				53.6, _
				73.2, _
				53.4, _
				69.0, _
				58.4, _
				73.6, _
				61.4, _
				55.5, _
				63.6, _
				60.9, _
				60.0, _
				46.8, _
				57.3, _
				64.1, _
				63.6, _
				67.3, _
				75.5, _
				68.2, _
				61.4, _
				76.8, _
				71.8, _
				55.5, _
				48.6, _
				66.4, _
				67.3 _
			}

			'女性の系列一覧
			Dim series = New ScatterChartSeries()
			Me.C1ScatterChart1.SeriesList.Add(series)
			series.MarkerType = MarkerType.Circle
            series.Data.X.AddRange(valuesX.ToArray())
            series.Data.Y.AddRange(valuesY.ToArray())
			series.Label = "女"
			series.LegendEntry = True

			'男性の系列一覧
			valuesX = New List(Of System.Nullable(Of Double))() From { _
				175.0, _
				174.0, _
				165.1, _
				177.0, _
				192.0, _
				176.5, _
				169.4, _
				182.1, _
				179.8, _
				175.3, _
				184.9, _
				177.3, _
				167.4, _
				178.1, _
				168.9, _
				174.0, _
				167.6, _
				170.2, _
				167.6, _
				188.0, _
				174.0, _
				176.5, _
				180.3, _
				167.6, _
				188.0, _
				180.3, _
				167.6, _
				183.0, _
				183.0, _
				179.1, _
				170.2, _
				177.8, _
				179.1, _
				190.5, _
				177.8, _
				180.3, _
				180.3 _
			}
			valuesY = New List(Of System.Nullable(Of Double))() From { _
				70.2, _
				73.4, _
				70.5, _
				68.9, _
				102.3, _
				68.4, _
				65.9, _
				75.7, _
				84.5, _
				87.7, _
				86.4, _
				73.2, _
				53.9, _
				72.0, _
				55.5, _
				70.9, _
				64.5, _
				77.3, _
				72.3, _
				87.3, _
				80.0, _
				82.3, _
				73.6, _
				74.1, _
				85.9, _
				73.2, _
				76.3, _
				65.9, _
				90.9, _
				89.1, _
				62.3, _
				82.7, _
				79.1, _
				98.2, _
				84.1, _
				83.2, _
				83.2 _
			}

			series = New ScatterChartSeries()
			Me.C1ScatterChart1.SeriesList.Add(series)
			series.MarkerType = MarkerType.Diamond
            series.Data.X.AddRange(valuesX.ToArray())
            series.Data.Y.AddRange(valuesY.ToArray())
			series.Label = "男"
			series.LegendEntry = True
		End Sub
	End Class
End Namespace
