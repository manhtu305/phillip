﻿Imports C1.Web.Wijmo.Controls.C1Chart

Partial Public Class C1LineChart_Trendline
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            LoadSeries()
        End If
    End Sub

    Private Sub LoadSeries()
        Dim months As String() = New String() {"Jan", "Feb", "Mar", "Apr", "May", "Jun", _
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
        C1LineChart1.SeriesList.Clear()

        'add line series
        Dim series As New LineChartSeries()
        series.Markers.Visible = True
        series.Markers.Type = MarkerType.Circle
        series.FitType = LineChartFitType.Spline

        C1LineChart1.SeriesList.Add(series)

        'add trendline series
        Dim seriesT = New LineChartSeries()
        seriesT.IsTrendline = True
        seriesT.Label = "Trendline"
        seriesT.TrendlineSeries.FitType = TrendlineFitType.Polynom
        seriesT.TrendlineSeries.Order = 4
        seriesT.TrendlineSeries.SampleCount = 100

        Me.C1LineChart1.SeriesList.Add(seriesT)

        'add X data
        series.Data.X = New ChartXAxisList()
        series.Data.X.AddRange(months)

        seriesT.TrendlineSeries.Data.X.AddRange(months)

        'add Y data
        Dim random As New Random(DateTime.Now.Second)
        Dim valueY As Double = 0

        series.Data.Y = New ChartYAxisList()

        For i As Integer = 0 To 11
            valueY = random.NextDouble() * 100
            series.Data.Y.Add(valueY)

            seriesT.TrendlineSeries.Data.Y.Add(valueY)
        Next
    End Sub

    Protected Sub btnApply_Click(sender As Object, e As EventArgs)
        'reload data
        LoadSeries()

        Dim order As Integer = CInt(inputOrder.Value)
        Dim sampleCount As Integer = CInt(inputSampleCount.Value)
        Dim fitType As TrendlineFitType = DirectCast([Enum].Parse(GetType(TrendlineFitType), dplFitType.SelectedValue), TrendlineFitType)

        For Each series In Me.C1LineChart1.SeriesList
            If series.IsTrendline Then
                series.TrendlineSeries.FitType = fitType
                series.TrendlineSeries.Order = order
                series.TrendlineSeries.SampleCount = sampleCount
            End If
        Next
    End Sub
End Class
