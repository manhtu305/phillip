﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="Symbols.aspx.vb" Inherits="C1LineChart_Symbols" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type = "text/javascript">
		function hintContent() {
			return this.y;
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1LineChart runat = "server" ID="C1LineChart1"  ShowChartLabels="False" Height="475" Width = "756">
		<Header Text="2012年9月のTwitter 傾向"></Header>
		<Hint OffsetY="-10">
			<Content Function="hintContent" />
		</Hint>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<SeriesList>
			<wijmo:LineChartSeries Label="Wijmo" LegendEntry="true">
				<Markers Visible="true" Type="Circle">
					<Symbol>
						<wijmo:LineChartMarkerSymbol Index="6" Url="Images/wijmo.png" Width="30" Height="30" />
						<wijmo:LineChartMarkerSymbol Index="2" Url="Images/hatemo.png" Width="30" Height="30" />
					</Symbol>
				</Markers>
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2012-9-16" />
							<wijmo:ChartXData DateTimeValue="2012-9-17" />
							<wijmo:ChartXData DateTimeValue="2012-9-18" />
							<wijmo:ChartXData DateTimeValue="2012-9-19" />
							<wijmo:ChartXData DateTimeValue="2012-9-20" />
							<wijmo:ChartXData DateTimeValue="2012-9-21" />
							<wijmo:ChartXData DateTimeValue="2012-9-22" />
							<wijmo:ChartXData DateTimeValue="2012-9-23" />
							<wijmo:ChartXData DateTimeValue="2012-9-24" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="12" />
							<wijmo:ChartYData DoubleValue="30" />
							<wijmo:ChartYData DoubleValue="6" />
							<wijmo:ChartYData DoubleValue="22" />
							<wijmo:ChartYData DoubleValue="14" />
							<wijmo:ChartYData DoubleValue="25" />
							<wijmo:ChartYData DoubleValue="41" />
							<wijmo:ChartYData DoubleValue="14" />
							<wijmo:ChartYData DoubleValue="3" />
						</Values>
					</Y>
				</Data>
			</wijmo:LineChartSeries>
		</SeriesList>
	</wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><strong>C1LineChart </strong>では、マーカーをカスタマイズして画像などのシンボルで表示することが可能です。</p>
	<p>このサンプルでは、マーカーをカスタマイズするために下記のプロパティが使用されています。</p>
	<ul>
		<li><strong>LineChartSeries.Markers.Symbol</strong> - <strong>LineChartMarkerSymbol </strong>インスタンスのコレクション。</li>
		<li><strong>LineChartMarkerSymbol.Index</strong> - データ系列のマーカーのインデックスを指定します。</li>
		<li><strong>LineChartMarkerSymbol.Url</strong> - シンボルの画像 URL を指定します。</li>
		<li><strong>LineChartMarkerSymbol.Width</strong> - シンボルの幅を指定します。</li>
		<li><strong>LineChartMarkerSymbol.Height</strong> - シンボルの高さを指定します。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

