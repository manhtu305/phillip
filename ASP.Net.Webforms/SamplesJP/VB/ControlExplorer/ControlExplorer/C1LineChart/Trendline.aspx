﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Trendline.aspx.vb" Inherits="C1LineChart_Trendline" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function hintContent() {
            return this.data.lineSeries.label + '\n' +
			this.x + '\n' + this.y + '';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1LineChart runat="server" ID="C1LineChart1" ShowChartLabels="False" Height="475" Width="756">
        <Footer Compass="South" Visible="False"></Footer>
        <Legend Visible="false"></Legend>
        <Hint OffsetY="-10">
            <Content Function="hintContent" />
            <ContentStyle FontSize="10pt" />
        </Hint>
        <SeriesStyles>
            <wijmo:ChartStyle Stroke="#ff9900" StrokeWidth="3" />
        </SeriesStyles>
        <SeriesHoverStyles>
            <wijmo:ChartStyle StrokeWidth="4"></wijmo:ChartStyle>
        </SeriesHoverStyles>
        <Axis>
            <X Text="Month of the Year">
                <Labels>
                    <AxisLabelStyle FontSize="11pt" Rotation="-45">
                        <Fill Color="#7f7f7f"></Fill>
                    </AxisLabelStyle>
                </Labels>
                <TickMajor Position="Outside">
                    <TickStyle Stroke="#7f7f7f" />
                </TickMajor>
                <GridMajor Visible="false"></GridMajor>
                <GridMinor Visible="false"></GridMinor>
            </X>
            <Y Text="Number of Hits" AutoMax="false" AutoMin="false" Max="100" Min="0" Compass="West">
                <Labels TextAlign="Center">
                    <AxisLabelStyle FontSize="11pt">
                        <Fill Color="#7f7f7f"></Fill>
                    </AxisLabelStyle>
                </Labels>
                <GridMajor Visible="false">
                    <GridStyle Stroke="#353539" StrokeDashArray="- " />
                </GridMajor>
                <GridMinor Visible="false"></GridMinor>
                <TickMajor Position="Outside">
                    <TickStyle Stroke="#7f7f7f" />
                </TickMajor>
                <TickMinor Position="Outside">
                    <TickStyle Stroke="#7f7f7f" />
                </TickMinor>
            </Y>
        </Axis>
    </wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p>
        <strong>C1LineChart</strong>の近似曲線を表示します。
    </p>
    <p>
        以下のプロパティを設定して、近似曲線をカスタマイズできます。
    </p>
    <ul>
        <li>
            <strong>FitType</strong> - 近似曲線の種類を指定します。
        </li>
        <li>
            <strong>SampleCount</strong> - 関数計算のサンプル数を指定します。多項式、指数、累乗、指数、対数、フーリエでのみ有効です。
        </li>
        <li>
            <strong>Order</strong> - 多項式の次数を定義します。多項式、指数、累乗、指数、対数、フーリエでのみ有効です。
        </li>
    </ul>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label>次数:</label>
                    <wijmo:C1InputNumeric ID="inputOrder" runat="server" Width="80px" ShowSpinner="true" Value="4" MinValue="1" MaxValue="10" DecimalPlaces="0"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label>サンプル数:</label>
                    <wijmo:C1InputNumeric ID="inputSampleCount" runat="server" Width="80px" ShowSpinner="true" Value="100" MinValue="1" MaxValue="200" DecimalPlaces="0"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label>種類:</label>
                    <asp:DropDownList ID="dplFitType" runat="server">
                        <asp:ListItem Text="多項式" Value="Polynom" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="指数" Value="Exponent"></asp:ListItem>
                        <asp:ListItem Text="対数" Value="Logarithmic"></asp:ListItem>
                        <asp:ListItem Text="累乗" Value="Power"></asp:ListItem>
                        <asp:ListItem Text="フーリエ" Value="Fourier"></asp:ListItem>
                        <asp:ListItem Text="Xの最小" Value="MinX"></asp:ListItem>
                        <asp:ListItem Text="Yの最小" Value="MinY"></asp:ListItem>
                        <asp:ListItem Text="Xの最大" Value="MaxX"></asp:ListItem>
                        <asp:ListItem Text="Yの最大" Value="MaxY"></asp:ListItem>
                        <asp:ListItem Text="Xの平均" Value="AverageX"></asp:ListItem>
                        <asp:ListItem Text="Yの平均" Value="AverageY"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
        <div class="settingcontrol">
            <asp:Button ID="btnApply" Text="適用" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
        </div>
    </div>
</asp:Content>
