﻿Imports C1.Web.Wijmo.Controls.C1Chart

Partial Public Class C1LineChart_CustomHandleResponse
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            PrepareOptions()
        End If
    End Sub

    Private Sub PrepareOptions()
        Dim valuesX = New List(Of System.Nullable(Of DateTime))() From { _
            DateTime.Parse("1/1/2010"), _
            DateTime.Parse("2/1/2010"), _
            DateTime.Parse("3/1/2010"), _
            DateTime.Parse("4/1/2010"), _
            DateTime.Parse("5/1/2010"), _
            DateTime.Parse("6/1/2010"), _
            DateTime.Parse("7/1/2010"), _
            DateTime.Parse("8/1/2010"), _
            DateTime.Parse("9/1/2010"), _
            DateTime.Parse("10/1/2010"), _
            DateTime.Parse("11/1/2010"), _
            DateTime.Parse("12/1/2010") _
        }

        'serieslist Domestic
        Dim valuesY = New List(Of System.Nullable(Of Double))() From { _
            1983, _
            2343, _
            2593, _
            2283, _
            2574, _
            2838, _
            2382, _
            2634, _
            2938, _
            2739, _
            2983, _
            3493 _
        }

        Dim series = New LineChartSeries()
        Me.C1LineChart1.SeriesList.Add(series)
        series.Markers.Visible = True
        series.Markers.Type = MarkerType.Circle
        series.Data.X.AddRange(valuesX.ToArray())
        series.Data.Y.AddRange(valuesY.ToArray())
        series.Label = "Domestic"

        series.LegendEntry = True

        'serieslist International
        valuesY = New List(Of System.Nullable(Of Double))() From { _
            574, _
            636, _
            673, _
            593, _
            644, _
            679, _
            593, _
            139, _
            599, _
            583, _
            602, _
            690 _
        }

        series = New LineChartSeries()
        Me.C1LineChart1.SeriesList.Add(series)
        series.Markers.Visible = True
        series.Markers.Type = MarkerType.Circle
        series.Data.X.AddRange(valuesX.ToArray())
        series.Data.Y.AddRange(valuesY.ToArray())
        series.Label = "International"
        series.LegendEntry = True
    End Sub
End Class
