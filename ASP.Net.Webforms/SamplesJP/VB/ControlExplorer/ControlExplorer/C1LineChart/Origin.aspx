﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Origin.aspx.vb" Inherits="ControlExplorer.C1LineChart.Origin" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
		function hintContent() {
			return this.data.lineSeries.label + '\n' +
						this.x + '\n' + this.y + '';
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1LineChart ShowChartLabels="false" runat = "server" ID="C1LineChart1" Height="475" Width = "756">
		<Hint OffsetY="-10">
			<Content Function="hintContent" />
			<ContentStyle FontSize="10pt"></ContentStyle>
		</Hint>
		<Header Text="株価推移">
			<TextStyle>
				<Fill Color="#fafafa"></Fill>
			</TextStyle>
		</Header>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="4" />
		</SeriesHoverStyles>
<SeriesTransition Duration="2000"></SeriesTransition>

<Animation Duration="2000"></Animation>
		<SeriesList>
			<wijmo:LineChartSeries Label="株価" FitType="Spline" LegendEntry="true">
				<Markers Visible="True" Type="Circle">
				</Markers>
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2010-03-01" />
							<wijmo:ChartXData DateTimeValue="2010-06-01" />
							<wijmo:ChartXData DateTimeValue="2010-09-01" />
							<wijmo:ChartXData DateTimeValue="2010-12-01" />
							<wijmo:ChartXData DateTimeValue="2011-03-01" />
							<wijmo:ChartXData DateTimeValue="2011-06-01" />
							<wijmo:ChartXData DateTimeValue="2011-09-01" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="-6.1" />
							<wijmo:ChartYData DoubleValue="-10.4" />
							<wijmo:ChartYData DoubleValue="-2.8" />
							<wijmo:ChartYData DoubleValue="1.1" />
							<wijmo:ChartYData DoubleValue="2.1" />
							<wijmo:ChartYData DoubleValue="-1.6" />
							<wijmo:ChartYData DoubleValue="6.2" />
						</Values>
					</Y>
				</Data>
			</wijmo:LineChartSeries>
		</SeriesList>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#afe500" StrokeWidth="3" />
		</SeriesStyles>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend Visible="false">
			<Size Width="30" Height="3"></Size>
		</Legend>
        <Axis>
            <X AnnoFormatString="yyyy年MM月">
                <Labels>
                    <Style FontSize="11pt" Rotation="-60">
                        <Fill Color="#7F7F7F"></Fill>
                    </Style>
                    <AxisLabelStyle FontSize="11pt" Rotation="-60">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>

<GridMajor Visible="True"></GridMajor>

<GridMinor Visible="False"></GridMinor>

				<TickMajor Position="Outside">
<Style Stroke="#7F7F7F"></Style>

					<TickStyle Stroke="#7f7f7f"></TickStyle>
				</TickMajor>
			</X>
			<Y Compass="West" Visible="true" Origin="0">
				<Labels TextAlign="Center">
<Style FontSize="11pt">
<Fill Color="#7F7F7F"></Fill>
</Style>

					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="True">
<Style Stroke="#353539" StrokeDashArray="- "></Style>

					<GridStyle Stroke="#353539" StrokeDashArray="- "></GridStyle>
				</GridMajor>

<GridMinor Visible="False"></GridMinor>

				<TickMajor Position="Outside">
<Style Stroke="#7F7F7F"></Style>

					<TickStyle  Stroke="#7f7f7f"></TickStyle>
				</TickMajor>
				<TickMinor Position="Outside">
<Style Stroke="#7F7F7F"></Style>

					<TickStyle Stroke="#7f7f7f"></TickStyle>
				</TickMinor>
			</Y>
		</Axis>
	</wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        C1LineChart コントロールは、負の値の表示をサポートし、各軸の Origin プロパティを使用して負の値を表示できます。
        Origin プロパティでは、軸の開始点を描画する位置を指定します。
        この機能は、同じチャート上で正と負の値を表示するのに最適です。
    </p>
</asp:Content>
