﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="AreaSpline.aspx.vb" Inherits="ControlExplorer.C1LineChart.AreaSpline" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		function hintContent() {
			return this.y;
		}
		function pageLoad() {
			var resizeTimer = null;

			$(window).resize(function () {
				window.clearTimeout(resizeTimer);
				resizeTimer = window.setTimeout(function () {
					var jqLine = $("#<%= C1LineChart1.ClientID %>"),
						width = jqLine.width(),
						height = jqLine.height();

					if (!width || !height) {
						window.clearTimeout(resizeTimer);
						return;
					}

					jqLine.c1linechart("redraw", width, height);
				}, 250);
			});
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1LineChart runat="server" ID="C1LineChart1" Type="Area" ShowChartLabels="False" Height="475" Width="756">
		<Header Text="2012年9月の Twitter 傾向">
		</Header>
		<Hint OffsetY="-10">
			<Content Function="hintContent" />
		</Hint>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<SeriesList>
			<wijmo:LineChartSeries Label="Wijmo" LegendEntry="true" FitType="Spline">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2012-9-16" />
							<wijmo:ChartXData DateTimeValue="2012-9-17" />
							<wijmo:ChartXData DateTimeValue="2012-9-18" />
							<wijmo:ChartXData DateTimeValue="2012-9-19" />
							<wijmo:ChartXData DateTimeValue="2012-9-20" />
							<wijmo:ChartXData DateTimeValue="2012-9-21" />
							<wijmo:ChartXData DateTimeValue="2012-9-22" />
							<wijmo:ChartXData DateTimeValue="2012-9-23" />
							<wijmo:ChartXData DateTimeValue="2012-9-24" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="12" />
							<wijmo:ChartYData DoubleValue="30" />
							<wijmo:ChartYData DoubleValue="6" />
							<wijmo:ChartYData DoubleValue="22" />
							<wijmo:ChartYData DoubleValue="14" />
							<wijmo:ChartYData DoubleValue="25" />
							<wijmo:ChartYData DoubleValue="41" />
							<wijmo:ChartYData DoubleValue="14" />
							<wijmo:ChartYData DoubleValue="3" />
						</Values>
					</Y>
				</Data>
			</wijmo:LineChartSeries>
		</SeriesList>
	</wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、スプライン面グラフの作成方法を紹介します。
        データ点の間は、直線ではなく曲線で描画されます。
        このサンプルでは、下記のプロパティが使用されています。</p>
	<ul>
		<li><b>Type="Area"</b> - 折れ線グラフを面グラフとして描画します。</li>
		<li><b>SeriesList.LineChartSeries.FitType="Spline"</b> - 系列内の線を曲線として描画します。</li>
	</ul>
</asp:Content>
