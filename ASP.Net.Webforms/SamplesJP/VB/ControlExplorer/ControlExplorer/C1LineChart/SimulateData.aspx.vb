Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1Chart

Public Partial Class C1LineChart_SimulateData
	Inherits System.Web.UI.Page
	Protected Sub Page_Load(sender As Object, e As EventArgs)
		If Not IsPostBack Then
			PrepareOptions()
		End If
	End Sub

	Private Sub PrepareOptions()
		Dim xValues = New List(Of System.Nullable(Of Double))()
		Dim yValues = New List(Of System.Nullable(Of Double))()
		Dim randomDataValuesCount = 10
		Dim random = New Random()

        For i = 0 To randomDataValuesCount - 1
            xValues.Add(i)
            yValues.Add(random.[Next](0, 100))
        Next

		Dim series = New LineChartSeries()
		Me.C1LineChart1.SeriesList.Add(series)
		series.Markers.Visible = True
		series.Markers.Type = MarkerType.Circle
        series.Data.X.AddRange(xValues.ToArray())
        series.Data.Y.AddRange(yValues.ToArray())
		series.LegendEntry = True
	End Sub
End Class
