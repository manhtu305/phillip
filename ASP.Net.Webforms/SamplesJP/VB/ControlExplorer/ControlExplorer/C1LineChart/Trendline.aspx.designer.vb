﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class C1LineChart_Trendline

    '''<summary>
    '''C1LineChart1 コントロール。
    '''</summary>
    '''<remarks>
    '''自動生成されたフィールド。
    '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
    '''</remarks>
    Protected WithEvents C1LineChart1 As Global.C1.Web.Wijmo.Controls.C1Chart.C1LineChart

    '''<summary>
    '''inputOrder コントロール。
    '''</summary>
    '''<remarks>
    '''自動生成されたフィールド。
    '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
    '''</remarks>
    Protected WithEvents inputOrder As Global.C1.Web.Wijmo.Controls.C1Input.C1InputNumeric

    '''<summary>
    '''inputSampleCount コントロール。
    '''</summary>
    '''<remarks>
    '''自動生成されたフィールド。
    '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
    '''</remarks>
    Protected WithEvents inputSampleCount As Global.C1.Web.Wijmo.Controls.C1Input.C1InputNumeric

    '''<summary>
    '''dplFitType コントロール。
    '''</summary>
    '''<remarks>
    '''自動生成されたフィールド。
    '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
    '''</remarks>
    Protected WithEvents dplFitType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnApply コントロール。
    '''</summary>
    '''<remarks>
    '''自動生成されたフィールド。
    '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
    '''</remarks>
    Protected WithEvents btnApply As Global.System.Web.UI.WebControls.Button
End Class
