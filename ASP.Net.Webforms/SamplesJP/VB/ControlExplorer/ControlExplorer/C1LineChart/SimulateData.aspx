﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="SimulateData.aspx.vb" Inherits="C1LineChart_SimulateData" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1LineChart runat = "server" ID="C1LineChart1" ShowChartLabels="False" Shadow="False" Height="475" Width = "756">
		<Animation Enabled="False" />
		<SeriesTransition Enabled="false" />
		<Footer Compass="South" Visible="False"></Footer>
		<Legend Visible="false">
			<Size Width="30" Height="3"></Size>
		</Legend>
		<Hint Enable="false"></Hint>
		<Header Text="Wijmo の好感度"></Header>
		<Axis>
			<Y Visible="False" Compass="West" Min="0" Max="100" AutoMin="false" AutoMax="false">
				<Labels TextAlign="Center"></Labels>
				<GridMajor Visible="True"></GridMajor>
			</Y>
		</Axis>
		<SeriesStyles>
			<wijmo:ChartStyle StrokeWidth="3" Stroke="#00a6dd" />
		</SeriesStyles>
	</wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>このサンプルでは、<b>addSeriesPoint</b>、<b>getLinePath</b>、<b>getLineMarkers</b> メソッドを使用して、グラフデータをリアルタイムに更新します。</p>
	<p>このサンプルでは、以下のクライアント側メソッドが使用されています。</p>
	<ul>
		<li><strong>addSeriesPoint </strong> - 系列に新しいデータ点を追加します。</li>
		<li><strong>getLinePath</strong> - 指定した折れ線の情報を取得します。</li>
		<li><strong>getLineMarkers</strong> - 指定したマーカーの情報を取得します。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<script type="text/javascript">
		var duration = 5000;
		var idx = 10;
		var intervalRadomData = null;
		$(document).ready(function () {
			setTimeout(function () {
				animateChart();
				intervalRadomData = setInterval(function () {
					$("#<%= C1LineChart1.ClientID %>").c1linechart("addSeriesPoint", 0, { x: idx++, y: createRandomValue() }, true);
					animateChart();
				}, duration);
			}, 1000);
		});

		function animateChart() {
			var path = $("#<%= C1LineChart1.ClientID %>").c1linechart("getLinePath", 0),
				markers = $("#<%= C1LineChart1.ClientID %>").c1linechart("getLineMarkers", 0),
				box = path.getBBox(),
				width =756 / 10,
				anim = Raphael.animation({ transform: Raphael.format("...t{0},0", -width) }, duration);
			path.animate(anim);
			if (path.shadow) {
				var pathShadow = path.shadow;
				pathShadow.animate(anim);
			}
			markers.animate(anim);
			var rect = box.x + " " + (box.y - 5) + " " + box.width + " " + (box.height + 10);
			path.wijAttr("clip-rect", rect);
			markers.attr("clip-rect", rect);
		}

		function createRandomValue() {
			var val = Math.round(Math.random() * 100);
			return val;
		}

		function dispose() {
			if (intervalRadomData) {
				clearInterval(intervalRadomData);
				intervalRadomData = null;
			}
		}
	</script>
</asp:Content>

