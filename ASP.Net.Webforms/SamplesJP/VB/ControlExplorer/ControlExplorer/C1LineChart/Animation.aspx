﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1LineChart.Animation" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
		<script language = "javascript" type ="text/javascript">
		function hintContent() {
			return this.data.lineSeries.label + '\n' +
			this.x + '\n' + this.y + '';
		}

		function changeProperties() {
			var animation = {},
				seriesTransition = {},
				enabled = $("#chkEnabled").is(":checked"),
				direction = $("#selDirection").val(),
				duration = $("#inpDuration").val(),
				easing = $("#selEasing").val(),
				stEnabled = $("#chkSTEnabled").is(":checked"),
				stDuration = $("#inpSTDuration").val(),
				stEasing = $("#selSTEasing").val();
			animation.enabled = enabled;
			animation.direction = direction;
			if (duration && duration.length) {
				animation.duration = parseFloat(duration);
			}
			animation.easing = easing;

			seriesTransition.enabled = stEnabled;
			if (stDuration && stDuration.length) {
				seriesTransition.duration = parseFloat(stDuration);
			}
			seriesTransition.easing = stEasing;
			$("#<%= C1LineChart1.ClientID %>").c1linechart("option", "animation", animation);
			$("#<%= C1LineChart1.ClientID %>").c1linechart("option", "seriesTransition", seriesTransition);
		}

		function reload() {
			$("#<%= C1LineChart1.ClientID %>").c1linechart("option", "seriesList", [createRandomSeriesList("2010")]);
		}

		function createRandomSeriesList(label) {
			var data = [],
				randomDataValuesCount = 12,
				labels = ["1月", "2月", "3月", "4月", "5月", "6月",
					"7月", "8月", "9月", "10月", "11月", "12月"],
				idx;
			for (idx = 0; idx < randomDataValuesCount; idx++) {
				data.push(createRandomValue());
			}
			return {
				label: "Steam",
				legendEntry: true,
				fitType: "spline",
				markers: {
					visible: true,
					type: "circle"
				},
				data: { x: labels, y: data }
			};
		}

		function createRandomValue() {
			return Math.round(Math.random() * 100);
		}
		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<input type="button" value="再描画" onclick="reload()" />
	<wijmo:C1LineChart runat = "server" ID="C1LineChart1" BackColor = "#242529" ShowChartLabels = "false" Height="410" Width = "756">
		<Animation Direction = "Vertical"/>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend Visible="false">
		</Legend>
		<Hint OffsetY="-10">
			<Content Function="hintContent" />
			<ContentStyle FontSize="10pt" />
		</Hint>
		<SeriesList>
			<wijmo:LineChartSeries Label="Steam" LegendEntry="true" FitType="Spline">
				<Markers Visible="true" Type="Circle"></Markers>
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="1月" />
							<wijmo:ChartXData StringValue="2月" />
							<wijmo:ChartXData StringValue="3月" />
							<wijmo:ChartXData StringValue="4月" />
							<wijmo:ChartXData StringValue="5月" />
							<wijmo:ChartXData StringValue="6月" />
							<wijmo:ChartXData StringValue="7月" />
							<wijmo:ChartXData StringValue="8月" />
							<wijmo:ChartXData StringValue="9月" />
							<wijmo:ChartXData StringValue="10月" />
							<wijmo:ChartXData StringValue="11月" />
							<wijmo:ChartXData StringValue="12月" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="81" />
							<wijmo:ChartYData DoubleValue="95" />
							<wijmo:ChartYData DoubleValue="21" />
							<wijmo:ChartYData DoubleValue="88" />
							<wijmo:ChartYData DoubleValue="12" />
							<wijmo:ChartYData DoubleValue="23" />
							<wijmo:ChartYData DoubleValue="62" />
							<wijmo:ChartYData DoubleValue="79" />
							<wijmo:ChartYData DoubleValue="90" />
							<wijmo:ChartYData DoubleValue="62" />
							<wijmo:ChartYData DoubleValue="69" />
							<wijmo:ChartYData DoubleValue="46" />
						</Values>
					</Y>
				</Data>
			</wijmo:LineChartSeries>
		</SeriesList>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#ff9900" StrokeWidth="3" />
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth = "4"></wijmo:ChartStyle>
		</SeriesHoverStyles>
		<Axis>
			<X Text = "年月">
				<Labels>
					<AxisLabelStyle FontSize="11pt" Rotation="-45">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMajor>
			</X>
			<Y Text = "ヒット数" AutoMax = "false" AutoMin = "false" Max = "100" Min = "0" Compass="West">
				<Labels TextAlign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- " />
				</GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMinor>
			</Y>
		</Axis>
	</wijmo:C1LineChart>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><strong>C1LineChart</strong> では、データ描画時の様々なアニメーション効果を設定できます。</p>
	<p><strong>Animation</strong> <strong>と SeriesTransition</strong> プロパティを使用すると、データの描画時や再描画時のアニメーション効果を制御することができます。</p>
	<ul>
		<li><strong>Animation.Direction</strong> - データの描画や再描画の際、アニメーションが再生される方向を指定します。</li>
		<li><strong>Animation.Enabled</strong>／<strong>SeriesTransition.Enabled</strong>  - アニメーションを有効・無効にします。</li>
		<li><strong>Animation.Duration</strong>／<strong>SeriesTransition.Duration</strong>- アニメーションの継続期間を指定します。</li> 
		<li><strong>Animation.Easing</strong>／<strong>SeriesTransition.Easing</strong> - アニメーションの種類を設定します。</li>
	</ul>
	<p> <strong>Easing</strong> プロパティは、下記の値に設定できます。</p>
	<ul>
		<li>easeInCubic(">")</li>
		<li>easeOutCubic("<")</li>
		<li>easeInOutCubic("<>")</li>
		<li>easeInBack("backIn")</li>
		<li>easeOutBack("backOut")</li>
		<li>easeOutElastic("elastic")</li>
		<li>easeOutBounce("bounce")</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<div>
		<label for="chkEnabled">
			アニメーション：
		</label>
		<input id="chkEnabled" type="checkbox" checked="checked" />
		<label for="selDirection">
			　方向：
		</label>
		<select id="selDirection">
			<option value="horizontal">水平方向</option>
			<option value="vertical">垂直方向</option>
		</select>
		<label for="inpDuration">
			　継続期間：
		</label>
		<input id="inpDuration" type="text" value="1000" style="width:60px;" />
		<label for="selEasing">
			　イージング：
		</label>
		<select id="selEasing">
			<option value=">">></option>
			<option value="<"><</option>
			<option value="<>"><></option>
			<option value="backIn">backIn</option>
			<option value="backOut">backOut</option>
			<option value="bounce">bounce</option>
			<option value="elastic">elastic</option>
		</select><br/>
		<label for="chkSTEnabled">
			系列遷移アニメーション：
		</label>
		<input id="chkSTEnabled" type="checkbox" checked="checked" />
		<label for="inpSTDuration">
			　継続期間：
		</label>
		<input id="inpSTDuration" type="text" value="1000" style="width:60px;" />
		<label for="selSTEasing">
			　イージング：
		</label>
		<select id="selSTEasing">
			<option value=">">></option>
			<option value="<"><</option>
			<option value="<>"><></option>
			<option value="backIn">backIn</option>
			<option value="backOut">backOut</option>
			<option value="bounce">bounce</option>
			<option value="elastic">elastic</option>
			<option value="easeInCubic">easeInCubic</option>
			<option value="easeOutCubic">easeOutCubic</option>
			<option value="easeInBack">easeInBack</option>
			<option value="easeOutBack">easeOutBack</option>
			<option value="easeOutElastic">easeOutElastic</option>
			<option value="easeOutBounce">easeOutBounce</option>
		</select>
		　<input type="button" value="適用" onclick="changeProperties()" />
	</div>
</asp:Content>
