Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports C1.Web.Wijmo.Controls.C1Chart

Public Partial Class C1LineChart_OverView
	Inherits System.Web.UI.Page
	Protected Sub Page_Load(sender As Object, e As EventArgs)
		If Not IsPostBack Then
			PrepareOptions()
		End If
	End Sub

	Private Sub PrepareOptions()
		Dim valuesX = New List(Of System.Nullable(Of DateTime))() From { _
			New DateTime(2010, 10, 27, 11, 48, 0), _
			New DateTime(2010, 10, 27, 13, 47, 0), _
			New DateTime(2010, 10, 27, 15, 46, 0), _
			New DateTime(2010, 10, 27, 17, 45, 0), _
			New DateTime(2010, 10, 27, 19, 44, 0), _
			New DateTime(2010, 10, 27, 21, 43, 0), _
			New DateTime(2010, 10, 27, 23, 41, 0), _
			New DateTime(2010, 10, 28, 1, 40, 0), _
			New DateTime(2010, 10, 28, 3, 39, 0), _
			New DateTime(2010, 10, 28, 5, 38, 0), _
			New DateTime(2010, 10, 28, 7, 37, 0), _
			New DateTime(2010, 10, 28, 9, 36, 0), _
			New DateTime(2010, 10, 28, 11, 35, 0), _
			New DateTime(2010, 10, 28, 13, 34, 0), _
			New DateTime(2010, 10, 28, 15, 33, 0), _
			New DateTime(2010, 10, 28, 17, 32, 0), _
			New DateTime(2010, 10, 28, 19, 31, 0), _
			New DateTime(2010, 10, 28, 21, 30, 0), _
			New DateTime(2010, 10, 28, 23, 38, 0), _
			New DateTime(2010, 10, 29, 1, 27, 0), _
			New DateTime(2010, 10, 29, 3, 26, 0), _
			New DateTime(2010, 10, 29, 5, 25, 0), _
			New DateTime(2010, 10, 29, 7, 24, 0), _
			New DateTime(2010, 10, 29, 9, 23, 0), _
			New DateTime(2010, 10, 29, 11, 22, 0) _
		}
		Dim valuesY = New List(Of System.Nullable(Of Double))() From { _
			2665513, _
			2300921, _
			1663229, _
			1622528, _
			1472847, _
			1354026, _
			1348909, _
			1514946, _
			1746392, _
			2020481, _
			2312976, _
			2539210, _
			2657505, _
			2369938, _
			1869805, _
			1648695, _
			1529983, _
			1398148, _
			1389668, _
			1568134, _
			1787466, _
			2101460, _
			2090771, _
			2351994, _
			2537400 _
		}

		'serieslist
		Dim series = New LineChartSeries()
		Me.C1LineChart1.SeriesList.Add(series)
		series.Markers.Visible = True
		series.Markers.Type = MarkerType.Circle
        series.Data.X.AddRange(valuesX.ToArray())
        series.Data.Y.AddRange(valuesY.ToArray())
		series.Label = "Users"
		series.LegendEntry = True
	End Sub
End Class
