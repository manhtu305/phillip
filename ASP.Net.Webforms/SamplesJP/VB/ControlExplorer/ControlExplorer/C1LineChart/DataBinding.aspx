﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="DataBinding.aspx.vb" Inherits="C1LineChart_DataBinding" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <script type = "text/javascript">
		function hintContent() {
			return this.y;
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <wijmo:C1LineChart ID="C1LineChart1" runat="server" DataSourceID="AccessDataSource1" ShowChartLabels="false" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
	<Axis>
			<X>
				<Labels>
					<AxisLabelStyle  FontSize="11pt" Rotation="-45">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMajor>
			</X>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- " />
				</GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMinor>
			</Y>
		</Axis>
		<Header Text="売上"></Header>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<DataBindings>
			<wijmo:C1ChartBinding XField="CategoryName" XFieldType="String" YField="売上" YFieldType="Number" />
		</DataBindings>
	</wijmo:C1LineChart>
	
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/Nwind_ja.mdb" 
		SelectCommand="select CategoryName, sum(ProductSales) as 売上 from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p><strong>C1LineChart</strong> は、サーバー上の外部データソースとのデータ連結をサポートします。</p><br/>
	<p>
		<b>DataSourceID</b> や <b>DataSource</b> および <b>DataBindings</b> を設定すると、データ連結を有効にできます。
		下記のプロパティを使用すると、X 値と Y 値を指定したデータフィールドに連結できます。</p>
	<ul>
		<li><strong>DataSourceID</strong> - App_Data/Nwind_ja.mdbにあるデータソースを指定します。</li>
		<li><strong>DataBindings</strong> - 系列の連結を指定します。</li>
		<li><strong>C1ChartBinding.XField</strong> - X を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.XFieldType</strong> - XType を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.YField</strong> - Y を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.YFieldType</strong> - YType を指定したフィールド名と連結します。</li>
	</ul>
	<p>DataBindings は、<strong>C1ChartBinding</strong> インスタンスのコレクションです。
    <strong>C1ChartBinding</strong> には以下のプロパティがあります。</p>
	<ul>
		<li><strong>DataMember</strong> -  データソースに 1 つ以上のリストがある場合、データのリスト名を指定します。</li>
		<li><strong>HintField</strong> -  ヒントコンテンツを指定したフィールド名と連結します。</li>
	</ul>
	<p><strong>HintField</strong> プロパティが設定されている場合、マウスを系列上に移動すると、系列と同じインデックスのヒント値を表示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>
