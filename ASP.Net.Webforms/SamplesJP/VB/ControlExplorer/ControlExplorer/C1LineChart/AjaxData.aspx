﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="AjaxData.aspx.vb" Inherits="C1LineChart_AjaxData" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
	    function hintContent() {
	        return this.x + '\n ' + this.y + '';
	    }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1LineChart runat = "server" ID="C1LineChart1"  Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend>
			<Size Width="30" Height="3"></Size>
		</Legend>
		<Axis>
            <Y Text="価格" />
            <X Text="製品名">
				<Labels>
					<AxisLabelStyle Rotation="-45"></AxisLabelStyle>
				</Labels>
			</X> 
		</Axis>
		<Header Text="価格 - トップ10 - Northwind OData" />
	</wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
		このサンプルでは、外部データソースのデータを使用してクライアント側でグラフを作成する方法を示します。
        この例では、Netflix OData フィードのデータを使用しています。
	</p>
	<ul>
		<li><strong>データ URL:</strong> <a href="http://demo.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc">http://demo.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc</a> </li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var netflx = "http://demo.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc";

            $.ajax({
                dataType: "jsonp",
                url: netflx,
                jsonp: "$callback",
                success: callback
            });
        });
        function callback(result) {
            // unwrap result
            var names = [];
            var prices = [];

            var products = result["d"];

            for (var i = 0; i < products.length; i++) {

                names.push(products[i].Product_Name);
                prices.push(parseFloat(products[i].Unit_Price));
            }

            $("#<%= C1LineChart1.ClientID %>").c1linechart("option", "seriesList", [
                    {
                        label: "価格",
                        legendEntry: true,
                        fitType: "spline",
                        data: {
                            x: names,
                            y: prices
                        },
                        markers: {
                            visible: true,
                            type: "circle"
                        }
                    }
                ]);
        }

    </script>
</asp:Content>
