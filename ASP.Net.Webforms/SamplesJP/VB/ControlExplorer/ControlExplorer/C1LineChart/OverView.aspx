﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="OverView.aspx.vb" Inherits="C1LineChart_OverView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function hintContent() {
            return this.y;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1LineChart runat="server" ID="C1LineChart1" ShowChartLabels="False" Height="475" Width="756">
        <Header Text="オンライン ユーザーの数">
        </Header>
        <Footer Compass="South" Visible="False">
        </Footer>
        <Legend Visible="false">
        </Legend>
        <Hint OffsetY="-10">
            <Content Function="hintContent" />
        </Hint>
    </wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p>
        C1LineChart では、カスタマイズ可能な折れ線グラフを作成できます。
        このサンプルでは、C1LineChart のいくつかの独自機能を紹介します。</p>
    <p>
        このサンプルでは、ヘッダーの追加方法、軸の書式の設定方法、グラフラベルの追加方法、グラフデータの生成方法を示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
