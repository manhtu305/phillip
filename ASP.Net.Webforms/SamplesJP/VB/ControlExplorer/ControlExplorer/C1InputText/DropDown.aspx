﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="DropDown.aspx.vb" Inherits="ControlExplorer.C1InputText.DropDown" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1ComboBox" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <wijmo:C1InputText ID="C1InputText1" runat="server" Format="a" ShowDropDownButton="True">
         <Pickers>
             <List>
                 <wijmo:C1ComboBoxItem ID="C1ComboBoxItem1" runat="server" Text="red" Value="red" />
                 <wijmo:C1ComboBoxItem ID="C1ComboBoxItem2" runat="server" Text="green" Value="green" />
                 <wijmo:C1ComboBoxItem ID="C1ComboBoxItem3" runat="server" Text="blue" Value="blue" />
                 <wijmo:C1ComboBoxItem ID="C1ComboBoxItem4" runat="server" Text="yellow" Value="yellow" />
             </List>
         </Pickers>
     </wijmo:C1InputText>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
        <p>
        このサンプルでは、​​テキスト項目を使用してドロップダウンリストを作成する方法を示します。
    　　</p>
</asp:Content>
