﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="ReadingImeString.aspx.vb" Inherits="ControlExplorer.C1InputText.ReadingImeString" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function OnReadingImeStringOutput(sender, data) {
            var output = $("#textbox2");
            $("#textbox2").val(output.val() + data.readingString);
        }

        $(function () {
            $("#textbox2").wijtextbox();
        }
        );

    </script>
    <span>上のテキストボックスに日本語を入力すると、下のテキストボックスに読み仮名が出力されます。</span>
    <br/>
    <wijmo:C1InputText ID="C1InputText1" runat="server" ImeMode="Active" OnClientReadingImeStringOutput="OnReadingImeStringOutput"></wijmo:C1InputText>
    <br />
    <input type="text" id="textbox2" readonly="true" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、<b>OnReadingImeStringOutput</b> イベントを使用して、読み仮名を取得して出力する方法を示しています。
    </p>
</asp:Content>
