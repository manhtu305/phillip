﻿'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1InputText

    Partial Public Class DropDown

        '''<summary>
        '''C1InputText1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1InputText1 As Global.C1.Web.Wijmo.Controls.C1Input.C1InputText

        '''<summary>
        '''C1ComboBoxItem1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ComboBoxItem1 As Global.C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem

        '''<summary>
        '''C1ComboBoxItem2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ComboBoxItem2 As Global.C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem

        '''<summary>
        '''C1ComboBoxItem3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ComboBoxItem3 As Global.C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem

        '''<summary>
        '''C1ComboBoxItem4 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1ComboBoxItem4 As Global.C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem
    End Class
End Namespace
