﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Sparkline_NegativeValues" CodeBehind="NegativeValues.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Sparkline"
	TagPrefix="C1Sparkline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h3>軸の非表示</h3>
	<C1Sparkline:C1Sparkline ID="Sparkline1" runat="server">
        <SeriesList>
            <C1Sparkline:SparklineSeries Type="Area">
            </C1Sparkline:SparklineSeries>
        </SeriesList>
	</C1Sparkline:C1Sparkline>
    <h3>軸の表示</h3>
    <C1Sparkline:C1Sparkline ID="Sparkline2" runat="server" ValueAxis="true">
        <SeriesList>
            <C1Sparkline:SparklineSeries Type="Area">
            </C1Sparkline:SparklineSeries>
        </SeriesList>
	</C1Sparkline:C1Sparkline>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、<strong>C1Sparkline</strong>コントロールの軸オプションを設定します。
	</p>
	<p>
		<strong>C1Sparkline</strong>では、軸を表示して正数と負数を区別することができます。
        <strong>ValueAxis</strong>プロパティをtrueに設定すると、軸を表示します。
	</p>
</asp:Content>
