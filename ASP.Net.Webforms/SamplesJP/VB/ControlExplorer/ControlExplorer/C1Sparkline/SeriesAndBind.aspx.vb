﻿Public Class Sparkline_SeriesAndBind
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim data As Object() = {New With { _
            .Name = "Januray", _
            .Score = 73 _
        }, New With { _
            .Name = "February", _
            .Score = 95 _
        }, New With { _
            .Name = "March", _
            .Score = 89 _
        }, New With { _
            .Name = "April", _
            .Score = 66 _
        }, New With { _
            .Name = "May", _
            .Score = 50 _
        }, New With { _
            .Name = "June", _
            .Score = 65 _
        }, _
            New With { _
            .Name = "July", _
            .Score = 70 _
        }, New With { _
            .Name = "August", _
            .Score = 43 _
        }, New With { _
            .Name = "September", _
            .Score = 65 _
        }, New With { _
            .Name = "October", _
            .Score = 27 _
        }, New With { _
            .Name = "November", _
            .Score = 77 _
        }, New With { _
            .Name = "December", _
            .Score = 58 _
        }}

        Sparkline1.DataSource = data
        Sparkline1.DataBind()

    End Sub

End Class