﻿Public Class Sparkline_NegativeValues
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim data = New List(Of Double)() From {33, 11, 15, 26, 16, 27, 37, -13, -27, -8, -3, 17, 0, 22, -13, -29, 19, 8}
        Sparkline1.SeriesList(0).Data = data
        Sparkline2.SeriesList(0).Data = data
    End Sub

End Class