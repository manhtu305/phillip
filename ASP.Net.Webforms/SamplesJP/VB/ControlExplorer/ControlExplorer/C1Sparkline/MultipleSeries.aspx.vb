﻿Public Class Sparkline_MultipleSeries
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim data1 = {New With { _
            .Name = "a", _
            .Score = 73, _
            .Mood = 66 _
        }, New With { _
            .Name = "b", _
            .Score = 95, _
            .Mood = 50 _
        }, New With { _
            .Name = "c", _
            .Score = 89, _
            .Mood = 65 _
        }, New With { _
            .Name = "d", _
            .Score = 66, _
            .Mood = 70 _
        }, New With { _
            .Name = "e", _
            .Score = 50, _
            .Mood = 43 _
        }, New With { _
            .Name = "f", _
            .Score = 65, _
            .Mood = 65 _
        }, _
            New With { _
            .Name = "g", _
            .Score = 70, _
            .Mood = 27 _
        }, New With { _
            .Name = "h", _
            .Score = 43, _
            .Mood = 77 _
        }, New With { _
            .Name = "i", _
            .Score = 65, _
            .Mood = 58 _
        }, New With { _
            .Name = "j", _
            .Score = 27, _
            .Mood = 73 _
        }, New With { _
            .Name = "k", _
            .Score = 77, _
            .Mood = 95 _
        }, New With { _
            .Name = "l", _
            .Score = 58, _
            .Mood = 89 _
        }}

        Sparkline1.DataSource = data1
        Sparkline1.DataBind()

    End Sub

End Class