﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Sparkline_MultipleSeries" CodeBehind="MultipleSeries.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Sparkline"
	TagPrefix="C1Sparkline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Sparkline:C1Sparkline ID="Sparkline1" runat="server">
        <Animation Duration="800" />
        <SeriesList>
            <C1Sparkline:SparklineSeries Bind="Score" Type="Area">
                <SeriesStyle Fill-Color="Orange" Stroke="Orange"></SeriesStyle>
            </C1Sparkline:SparklineSeries>
            <C1Sparkline:SparklineSeries Bind="Mood">
                <SeriesStyle StrokeWidth="2"></SeriesStyle>
            </C1Sparkline:SparklineSeries>
        </SeriesList>
	</C1Sparkline:C1Sparkline>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、<strong>C1Sparkline</strong>コントロールで複数のデータ系列を表示します。
	</p>
	<p>
		<strong>C1Sparkline</strong>は複数のデータ系列をオーバーレイ表示することができます。
        各系列は<strong>Bind</strong>プロパティで指定されたデータオブジェクトに連結でき、<strong>SparklineSeries</strong>の<strong>Type</strong>プロパティでチャート種別を設定できます。
	</p>
</asp:Content>
