﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Sparkline_SeriesAndBind" CodeBehind="SeriesAndBind.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Sparkline"
	TagPrefix="C1Sparkline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Sparkline:C1Sparkline ID="Sparkline1" runat="server">
        <SeriesList>
            <C1Sparkline:SparklineSeries Bind="score" />
        </SeriesList>
	</C1Sparkline:C1Sparkline>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、<strong>C1Sparkline</strong>のデータ系列でデータ連結を設定します。
	</p>
	<p>
		<strong>C1Sparkline</strong>は複数のデータ系列を追加することができます。
        各系列は<strong>Bind</strong>プロパティで指定されたデータオブジェクトに連結でき、<strong>SparklineSeries</strong>の<strong>Type</strong>プロパティでチャート種別を設定できます。
	</p>
</asp:Content>
