﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Origin.aspx.vb" Inherits="ControlExplorer.C1BarChart.Origin" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type = "text/javascript">
		function hintContent() {
			return this.data.label + '\n ' + this.y + '';
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756" ShowChartLabels="false">
		<Animation Duration="1000" />
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Header Text="負の値を持つ棒グラフ"></Header>
		<Axis>
			<X Origin="2.6"></X>
			<Y Alignment="Far" Visible="true" Origin="0" Compass="West"></Y>
		</Axis>
		<SeriesList>
			<wijmo:BarChartSeries Label="系列 1" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DoubleValue="1" />
							<wijmo:ChartXData DoubleValue="2" />
							<wijmo:ChartXData DoubleValue="3" />
							<wijmo:ChartXData DoubleValue="4" />
							<wijmo:ChartXData DoubleValue="5" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="-5" />
							<wijmo:ChartYData DoubleValue="-3" />
							<wijmo:ChartYData DoubleValue="1" />
							<wijmo:ChartYData DoubleValue="7" />
							<wijmo:ChartYData DoubleValue="2" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="系列 2" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DoubleValue="1" />
							<wijmo:ChartXData DoubleValue="2" />
							<wijmo:ChartXData DoubleValue="3" />
							<wijmo:ChartXData DoubleValue="4" />
							<wijmo:ChartXData DoubleValue="5" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="-2" />
							<wijmo:ChartYData DoubleValue="-6" />
							<wijmo:ChartYData DoubleValue="2" />
							<wijmo:ChartYData DoubleValue="4" />
							<wijmo:ChartYData DoubleValue="3" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="系列 3" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DoubleValue="1" />
							<wijmo:ChartXData DoubleValue="2" />
							<wijmo:ChartXData DoubleValue="3" />
							<wijmo:ChartXData DoubleValue="4" />
							<wijmo:ChartXData DoubleValue="5" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="-3" />
							<wijmo:ChartYData DoubleValue="-5" />
							<wijmo:ChartYData DoubleValue="3" />
							<wijmo:ChartYData DoubleValue="2" />
							<wijmo:ChartYData DoubleValue="5" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
	</wijmo:C1BarChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
        C1BarChart コントロールは負の値の表示に対応しています。
        各軸の Origin プロパティは軸の開始点をどこに描画するかを示します。これは同じチャート上で正と負の値を表示するために最適な機能です。
    </p>
</asp:Content>
