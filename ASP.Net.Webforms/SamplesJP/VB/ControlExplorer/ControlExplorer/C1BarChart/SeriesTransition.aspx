﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="SeriesTransition.aspx.vb" Inherits="C1BarChart_SeriesTransition" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
			return this.data.label + '\n' + this.y + '';
		}

		function changeProperties() {
			var seriesTransition = {};
			enabled = $("#chkEnabled").is(":checked"),
				duration = $("#inpDuration").val(),
				easing = $("#selEasing").val();
			seriesTransition.enabled = enabled;
			if (duration && duration.length) {
				seriesTransition.duration = parseFloat(duration);
			}
			seriesTransition.easing = easing;
			$("#<%= C1BarChart1.ClientID %>").c1barchart("option", "seriesTransition", seriesTransition);
		}

		function reload() {
			$("#<%= C1BarChart1.ClientID %>").c1barchart("option", "seriesList", [createRandomSeriesList("2010年")]);
		}

		function createRandomSeriesList(label) {
			var data = [],
				randomDataValuesCount = 12,
				labels = ["1月", "2月", "3月", "4月", "5月", "6月",
					"7月", "8月", "9月", "10月", "11月", "12月"],
				idx;
			for (idx = 0; idx < randomDataValuesCount; idx++) {
				data.push(createRandomValue());
			}
			return {
				label: label,
				legendEntry: true,
				data: { x: labels, y: data }
			};
		}

		function createRandomValue() {
			return Math.round(Math.random() * 100);
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<input type="button" value="再描画" onclick="reload()" />
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" ClusterRadius = "5" Height="430" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<SeriesList>
			<wijmo:BarChartSeries Label="2010年" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="1月" />
							<wijmo:ChartXData StringValue="2月" />
							<wijmo:ChartXData StringValue="3月" />
							<wijmo:ChartXData StringValue="4月" />
							<wijmo:ChartXData StringValue="5月" />
							<wijmo:ChartXData StringValue="6月" />
							<wijmo:ChartXData StringValue="7月" />
							<wijmo:ChartXData StringValue="8月" />
							<wijmo:ChartXData StringValue="9月" />
							<wijmo:ChartXData StringValue="10月" />
							<wijmo:ChartXData StringValue="11月" />
							<wijmo:ChartXData StringValue="12月" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="81" />
							<wijmo:ChartYData DoubleValue="95" />
							<wijmo:ChartYData DoubleValue="21" />
							<wijmo:ChartYData DoubleValue="88" />
							<wijmo:ChartYData DoubleValue="12" />
							<wijmo:ChartYData DoubleValue="23" />
							<wijmo:ChartYData DoubleValue="62" />
							<wijmo:ChartYData DoubleValue="79" />
							<wijmo:ChartYData DoubleValue="90" />
							<wijmo:ChartYData DoubleValue="62" />
							<wijmo:ChartYData DoubleValue="69" />
							<wijmo:ChartYData DoubleValue="46" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
		<Axis>
			<Y Text = "ヒット数" AutoMax = "false" AutoMin = "false" Max = "100" Min = "0" Compass="West">
			</Y>
			<X Text = "月">
			</X>
		</Axis>
	</wijmo:C1BarChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><strong>C1BarChart</strong> では、データ描画時の様々なアニメーション効果に対応します。</p>
	<p><strong>SeriesTransition</strong> プロパティは、データを再読み込みした際にアニメーション効果を制御するために使用されます。
    既定では原点から新しい位置までアニメーション表示されますが、ここでは前回の位置から新しい位置までアニメーション表示されます。
    </p>
	<ul>
		<li>SeriesTransition.Enabled - 系列遷移アニメーションを有効・無効にします。</li>
		<li>SeriesTransition.Duration - 系列遷移アニメーションの継続時間を指定します。</li>
		<li>SeriesTransition.Easing - 系列遷移アニメーションの種類を設定します。</li>
	</ul>
	<p> <strong>Easing</strong> プロパティは下記の値に設定できます。</p>
	<ul>
		<li>easeInCubic(">")</li>
		<li>easeOutCubic("<")</li>
		<li>easeInOutCubic("<>")</li>
		<li>easeInBack("backIn")</li>
		<li>easeOutBack("backOut")</li>
		<li>easeOutElastic("elastic")</li>
		<li>easeOutBounce("bounce")</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<div>
		<label for="chkEnabled">
			系列遷移アニメーション：
		</label>
		<input id="chkEnabled" type="checkbox" checked="checked" />
		<label for="inpDuration">
			　継続時間：
		</label>
		<input id="inpDuration" type="text" value="1000" style="width:60px;" />
		<label for="selEasing">
		    　イージング：
		</label>
		<select id="selEasing">
			<option value=">">></option>
			<option value="<"><</option>
			<option value="<>"><></option>
			<option value="backIn">backIn</option>
			<option value="backOut">backOut</option>
			<option value="bounce">bounce</option>
			<option value="elastic">elastic</option>
		</select>
		<input type="button" value="適用" onclick="changeProperties()" />
	</div>
</asp:Content>

