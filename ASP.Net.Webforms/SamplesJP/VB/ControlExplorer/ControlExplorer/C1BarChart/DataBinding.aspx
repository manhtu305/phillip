﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="DataBinding.aspx.vb" Inherits="C1BarChart_DataBinding" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
			return this.data.label + '\n ' + this.y + '';
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1BarChart ID="C1BarChart1" runat="server" DataSourceID="AccessDataSource1" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Header Text="売上"></Header>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#7fc73c" Opacity="0.8">
				<Fill Color="#8ede43"></Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="1.5" Opacity="1" />
		</SeriesHoverStyles>
		<DataBindings>
			<wijmo:C1ChartBinding XField="CategoryName" XFieldType="String" YField="売上" YFieldType="Number" />
		</DataBindings>
	</wijmo:C1BarChart>
	
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/Nwind_ja.mdb" 
		SelectCommand="select CategoryName, sum(ProductSales) as 売上 from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p>
        <strong>C1BarChart</strong> は、サーバー上の外部データソースとの連結機能を提供します。
	</p>
	<p>
    	<b>DataSourceID</b> や <b>DataSource</b> および <b>DataBindings</b> を設定してデータ連結機能を有効にできます。
        下記プロパティを使用することで、X 値と Y 値を指定したデータフィールドに連結できます。
    </p>
	<ul>
		<li><strong>DataSourceID</strong> - App_Data/Nwind_ja.mdbにあるデータソースを指定します。</li>
		<li><strong>DataBindings</strong> - 系列の連結を指定します。</li>
		<li><strong>C1ChartBinding.XField</strong> - X を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.XFieldType</strong> - XType を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.YField</strong> - Y を指定したフィールド名と連結します。</li>
		<li><strong>C1ChartBinding.YFieldType</strong> - YType を指定したフィールド名と連結します。</li>
	</ul>
	<p>DataBindings は、<strong>C1ChartBinding</strong> インスタンスのコレクションです。<strong>C1ChartBinding</strong> には以下のプロパティがあります。</p>
	<ul>
		<li><strong>DataMember</strong> -  データソースに1つ以上のリストがある場合、データリストの名前を指定します。</li>
		<li><strong>HintField</strong> -  ヒント内容を指定したフィールドと連結します。</li>
	</ul>
	<p><strong>HintField</strong> プロパティが設定されている場合、マウスを系列上に移動すると、系列と同じインデックスのヒント値を表示します。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

