﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="Clusters.aspx.vb" Inherits="C1BarChart_Clusters" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756">
		<Axis>
			<X Text="パーセント (%)"></X>
			<Y Text="グラフィック カード" Compass="West"></Y>
		</Axis>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend Text="月"></Legend>
		<Header Text="ビデオカードのランキング"></Header>
		<SeriesStyles>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#2d2d2d" StrokeWidth="1.5" >
				<Fill ColorBegin="#333333" ColorEnd="#2D2D2D" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#5f9996" StrokeWidth="1.5" >
				<Fill ColorBegin="#6AABA7" ColorEnd="#5F9996" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#afe500" StrokeWidth="1.5" >
				<Fill ColorBegin="#C3FF00" ColorEnd="#AFE500" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#b2c76d" StrokeWidth="1.5" >
				<Fill ColorBegin="#C7DE7A" ColorEnd="#B2C76D" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#959595" StrokeWidth="1.5" >
				<Fill ColorBegin="#A6A6A6" ColorEnd="#959595" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
		</SeriesHoverStyles>
		<SeriesList>
			<wijmo:BarChartSeries Label="5月" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="カード1" />
							<wijmo:ChartXData StringValue="カード2" />
							<wijmo:ChartXData StringValue="カード3" />
							<wijmo:ChartXData StringValue="カード4" />
							<wijmo:ChartXData StringValue="カード5" />
							<wijmo:ChartXData StringValue="カード6" />
							<wijmo:ChartXData StringValue="カード7" />
							<wijmo:ChartXData StringValue="カード8" />
							<wijmo:ChartXData StringValue="カード9" />
							<wijmo:ChartXData StringValue="カード10" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.91" />
							<wijmo:ChartYData DoubleValue="1.90" />
							<wijmo:ChartYData DoubleValue="1.61" />
							<wijmo:ChartYData DoubleValue="2.23" />
							<wijmo:ChartYData DoubleValue="2.85" />
							<wijmo:ChartYData DoubleValue="3.64" />
							<wijmo:ChartYData DoubleValue="4.46" />
							<wijmo:ChartYData DoubleValue="5.89" />
							<wijmo:ChartYData DoubleValue="4.22" />
							<wijmo:ChartYData DoubleValue="4.66" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="6月" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="カード1" />
							<wijmo:ChartXData StringValue="カード2" />
							<wijmo:ChartXData StringValue="カード3" />
							<wijmo:ChartXData StringValue="カード4" />
							<wijmo:ChartXData StringValue="カード5" />
							<wijmo:ChartXData StringValue="カード6" />
							<wijmo:ChartXData StringValue="カード7" />
							<wijmo:ChartXData StringValue="カード8" />
							<wijmo:ChartXData StringValue="カード9" />
							<wijmo:ChartXData StringValue="カード10" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.82" />
							<wijmo:ChartYData DoubleValue="1.88" />
							<wijmo:ChartYData DoubleValue="1.77" />
							<wijmo:ChartYData DoubleValue="2.33" />
							<wijmo:ChartYData DoubleValue="2.97" />
							<wijmo:ChartYData DoubleValue="3.70" />
							<wijmo:ChartYData DoubleValue="4.42" />
							<wijmo:ChartYData DoubleValue="5.93" />
							<wijmo:ChartYData DoubleValue="4.92" />
							<wijmo:ChartYData DoubleValue="5.20" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="7月" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="カード1" />
							<wijmo:ChartXData StringValue="カード2" />
							<wijmo:ChartXData StringValue="カード3" />
							<wijmo:ChartXData StringValue="カード4" />
							<wijmo:ChartXData StringValue="カード5" />
							<wijmo:ChartXData StringValue="カード6" />
							<wijmo:ChartXData StringValue="カード7" />
							<wijmo:ChartXData StringValue="カード8" />
							<wijmo:ChartXData StringValue="カード9" />
							<wijmo:ChartXData StringValue="カード10" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.94" />
							<wijmo:ChartYData DoubleValue="1.80" />
							<wijmo:ChartYData DoubleValue="1.81" />
							<wijmo:ChartYData DoubleValue="2.23" />
							<wijmo:ChartYData DoubleValue="2.83" />
							<wijmo:ChartYData DoubleValue="3.57" />
							<wijmo:ChartYData DoubleValue="4.23" />
							<wijmo:ChartYData DoubleValue="5.90" />
							<wijmo:ChartYData DoubleValue="5.46" />
							<wijmo:ChartYData DoubleValue="5.62" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="8月" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="カード1" />
							<wijmo:ChartXData StringValue="カード2" />
							<wijmo:ChartXData StringValue="カード3" />
							<wijmo:ChartXData StringValue="カード4" />
							<wijmo:ChartXData StringValue="カード5" />
							<wijmo:ChartXData StringValue="カード6" />
							<wijmo:ChartXData StringValue="カード7" />
							<wijmo:ChartXData StringValue="カード8" />
							<wijmo:ChartXData StringValue="カード9" />
							<wijmo:ChartXData StringValue="カード10" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.89" />
							<wijmo:ChartYData DoubleValue="1.84" />
							<wijmo:ChartYData DoubleValue="1.96" />
							<wijmo:ChartYData DoubleValue="2.29" />
							<wijmo:ChartYData DoubleValue="2.93" />
							<wijmo:ChartYData DoubleValue="3.79" />
							<wijmo:ChartYData DoubleValue="4.11" />
							<wijmo:ChartYData DoubleValue="5.79" />
							<wijmo:ChartYData DoubleValue="5.84" />
							<wijmo:ChartYData DoubleValue="6.02" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="9月" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="カード1" />
							<wijmo:ChartXData StringValue="カード2" />
							<wijmo:ChartXData StringValue="カード3" />
							<wijmo:ChartXData StringValue="カード4" />
							<wijmo:ChartXData StringValue="カード5" />
							<wijmo:ChartXData StringValue="カード6" />
							<wijmo:ChartXData StringValue="カード7" />
							<wijmo:ChartXData StringValue="カード8" />
							<wijmo:ChartXData StringValue="カード9" />
							<wijmo:ChartXData StringValue="カード10" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.72" />
							<wijmo:ChartYData DoubleValue="1.80" />
							<wijmo:ChartYData DoubleValue="2.17" />
							<wijmo:ChartYData DoubleValue="2.40" />
							<wijmo:ChartYData DoubleValue="3.30" />
							<wijmo:ChartYData DoubleValue="3.97" />
							<wijmo:ChartYData DoubleValue="4.40" />
							<wijmo:ChartYData DoubleValue="6.59" />
							<wijmo:ChartYData DoubleValue="6.82" />
							<wijmo:ChartYData DoubleValue="7.04" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
	</wijmo:C1BarChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
		<strong>C1BarChart </strong>は集合棒グラフに対応しています。
		1 つ以上の <strong>BarChartSeries</strong> を指定することで、自動的に集合棒グラフが描画されます。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

