﻿Imports C1.Web.Wijmo.Controls.C1Chart

Namespace ControlExplorer.C1BarChart
    Public Class Trendline
        Inherits System.Web.UI.Page
        Protected Sub Page_Load(sender As Object, e As EventArgs)

        End Sub

        Protected Sub btnApply_Click(sender As Object, e As EventArgs)
            Dim order As Integer = CInt(inputOrder.Value)
            Dim sampleCount As Integer = CInt(inputSampleCount.Value)
            Dim fitType As TrendlineFitType = DirectCast([Enum].Parse(GetType(TrendlineFitType), dplFitType.SelectedValue), TrendlineFitType)

            For Each series In Me.C1BarChart1.SeriesList
                If series.IsTrendline Then
                    series.TrendlineSeries.FitType = fitType
                    series.TrendlineSeries.Order = order
                    series.TrendlineSeries.SampleCount = sampleCount
                End If
            Next
        End Sub
    End Class
End Namespace
