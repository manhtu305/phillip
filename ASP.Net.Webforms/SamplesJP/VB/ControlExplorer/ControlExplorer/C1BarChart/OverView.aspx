﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="OverView.aspx.vb" Inherits="C1BarChart_OverView" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
		    return this.data.label + '\n ' + this.y + '';
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Axis>
			<X Text=""></X>
			<Y Text="ハードウェア数" Compass="West"></Y>
		</Axis>
		<Header Text="ハードウェア分布"></Header>
		<SeriesList>
			<wijmo:BarChartSeries Label="西エリア" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="デスクトップ" />
							<wijmo:ChartXData StringValue="ノート" />
							<wijmo:ChartXData StringValue="一体型" />
							<wijmo:ChartXData StringValue="タブレット" />
							<wijmo:ChartXData StringValue="携帯電話" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="5" />
							<wijmo:ChartYData DoubleValue="3" />
							<wijmo:ChartYData DoubleValue="4" />
							<wijmo:ChartYData DoubleValue="7" />
							<wijmo:ChartYData DoubleValue="2" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="中央エリア" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="デスクトップ" />
							<wijmo:ChartXData StringValue="ノート" />
							<wijmo:ChartXData StringValue="一体型" />
							<wijmo:ChartXData StringValue="タブレット" />
							<wijmo:ChartXData StringValue="携帯電話" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="2" />
							<wijmo:ChartYData DoubleValue="2" />
							<wijmo:ChartYData DoubleValue="3" />
							<wijmo:ChartYData DoubleValue="2" />
							<wijmo:ChartYData DoubleValue="1" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="東エリア" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="デスクトップ" />
							<wijmo:ChartXData StringValue="ノート" />
							<wijmo:ChartXData StringValue="一体型" />
							<wijmo:ChartXData StringValue="タブレット" />
							<wijmo:ChartXData StringValue="携帯電話" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="3" />
							<wijmo:ChartYData DoubleValue="4" />
							<wijmo:ChartYData DoubleValue="4" />
							<wijmo:ChartYData DoubleValue="2" />
							<wijmo:ChartYData DoubleValue="5" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
	</wijmo:C1BarChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
	<strong>C1BarChart</strong> は、各系列をデータの横棒または縦棒として描画し、豊富なカスタマイズやアニメーションを備えています。
	このサンプルでは <strong>C1BarChart</strong> のいくつかの基本機能を示します。
    このサンプルのソースは、X 軸と Y 軸のテキストを設定する方法、ヘッダーをグラフに追加する方法、ラベルテキストを凡例に追加する方法、およびグラフデータを生成する方法を示しています。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

