﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="AjaxData.aspx.vb" Inherits="C1BarChart_AjaxData" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Axis>
			<Y Text="価格" AutoMin="true" AutoMax="true" Compass="West"></Y>
			<X Text="製品名"></X>
		</Axis>
		<Header Text="価格 トップ10 - Northwind OData"></Header>
	</wijmo:C1BarChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
		このサンプルは、外部データソースからのデータを使用してグラフを作成する方法を示します。この例では、Netflix OData フィードのデータを使用しています。
	</p>
	<ul>
		<li><strong>データ URL:</strong> <a href="http://demo.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc">http://demo.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc</a> </li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<script type = "text/javascript">
	    function hintContent() {
	        return this.data.label + '\n ' + this.y + '';
	    }
	    $(document).ready(function () {
	        var netflx = "http://demo.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc";

	        $.ajax({
	            dataType: "jsonp",
	            url: netflx,
	            jsonp: "$callback",
	            success: callback
	        });
	    });

	    function callback(result) {
	        // unwrap result
	        var names = [];
	        var prices = [];

	        var products = result["d"];

	        for (var i = 0; i < products.length; i++) {

	            names.push(products[i].Product_Name);
	            prices.push(products[i].Unit_Price);
	        }

	        $("#<%= C1BarChart1.ClientID %>").c1barchart("option", "seriesList", [
					{
					    label: "価格",
					    legendEntry: true,
					    data: {
					        x: names,
					        y: prices
					    }
					}
				]);
	    }
	</script>
</asp:Content>
