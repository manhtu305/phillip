﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Paging.aspx.vb" Inherits="ControlExplorer.C1Carousel.Paging" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h3>数字</h3>
	<wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="300px" Display="1" ShowPager ="true" PagerType = "Numbers"
		EnableTheming="True">
		<Items>
			<wijmo:C1CarouselItem ID="C1CarouselItem1" runat="server" ImageUrl="http://lorempixum.com/750/300/sports/1" Caption="スポーツ 1">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem2" runat="server" ImageUrl="http://lorempixum.com/750/300/sports/2" Caption="スポーツ 2">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem3" runat="server" ImageUrl="http://lorempixum.com/750/300/sports/3" Caption="スポーツ 3">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem4" runat="server" ImageUrl="http://lorempixum.com/750/300/sports/4" Caption="スポーツ 4">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem5" runat="server" ImageUrl="http://lorempixum.com/750/300/sports/5" Caption="スポーツ 5">
			</wijmo:C1CarouselItem>
		</Items>
		<PagerPosition>
			<My Left="Right"></My>
			<At Top="Bottom" Left="Right"></At>
		</PagerPosition>
	</wijmo:C1Carousel>
	<br/>
    <h3>ドット</h3>
	<wijmo:C1Carousel ID="C1Carousel2" runat="server" Width="750px" Height="300px" Display="1" ShowPager ="true" PagerType = "Dots"
		EnableTheming="True">
		<Items>
			<wijmo:C1CarouselItem ID="C1CarouselItem7" runat="server" ImageUrl="http://lorempixum.com/750/300/city/1" Caption="都市 1">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem8" runat="server" ImageUrl="http://lorempixum.com/750/300/city/2" Caption="都市 2">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem9" runat="server" ImageUrl="http://lorempixum.com/750/300/city/3" Caption="都市 3">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem10" runat="server" ImageUrl="http://lorempixum.com/750/300/city/4" Caption="都市 4">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem11" runat="server" ImageUrl="http://lorempixum.com/750/300/city/5" Caption="都市 5">
			</wijmo:C1CarouselItem>
		</Items>
		<PagerPosition>
			<My Left="Center" Top = "Top"></My>
			<At Left="Center" Top="Bottom"></At>
		</PagerPosition>
	</wijmo:C1Carousel>
	<br/>
    <h3>スライダー</h3>
	<wijmo:C1Carousel ID="C1Carousel3" runat="server" Width="750px" Height="300px" Display="1" ShowPager ="true" PagerType = "Slider"
		EnableTheming="True">
		<Items>
			<wijmo:C1CarouselItem ID="C1CarouselItem13" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/1" Caption="ワールド 1">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem14" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/2" Caption="ワールド 2">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem15" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/3" Caption="ワールド 3">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem16" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/4" Caption="ワールド 4">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem17" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/5" Caption="ワールド 5">
			</wijmo:C1CarouselItem>
		</Items>
		<PagerPosition>
			<My Left="Center" Top = "Top"></My>
			<At Left="Center" Top="Bottom"></At>
		</PagerPosition>
	</wijmo:C1Carousel>
	<br/><br/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、PagerType プロパティを使用して、様々なページング種類を <strong>C1Carousel</strong> コントロールに適用する方法を紹介します。
    </p>
</asp:Content>
