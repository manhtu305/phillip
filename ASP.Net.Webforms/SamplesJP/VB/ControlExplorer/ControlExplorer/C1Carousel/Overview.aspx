﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Overview.aspx.vb" Inherits="ControlExplorer.C1Carousel.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="300px" Display="1"
        EnableTheming="True">
        <Items>
            <wijmo:C1CarouselItem runat="server" ImageUrl="http://lorempixum.com/750/300/sports/1" Caption="スポーツ 1">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="http://lorempixum.com/750/300/sports/2" Caption="スポーツ 2">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="http://lorempixum.com/750/300/sports/3" Caption="スポーツ 3">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="http://lorempixum.com/750/300/sports/4" Caption="スポーツ 4">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="http://lorempixum.com/750/300/sports/5" Caption="スポーツ 5">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="http://lorempixum.com/750/300/sports/6" Caption="スポーツ 6">
            </wijmo:C1CarouselItem>
        </Items>
        <PagerPosition>
            <My Left="Right"></My>
            <At Top="Bottom" Left="Right"></At>
        </PagerPosition>
    </wijmo:C1Carousel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、リスト内の画像やその他のコンテンツを表示するのに便利な既定の <strong>C1Carousel </strong>コントロールを表示しています。
    </p>
    <p>
        カルーセルには、前後の画像に移動するためのボタンがあり、画像は自動再生することも可能です。
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
