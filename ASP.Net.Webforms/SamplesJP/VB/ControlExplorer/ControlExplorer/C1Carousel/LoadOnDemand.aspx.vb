Imports System.Collections
Imports System.Configuration
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml.Linq
Imports System.Collections.Generic

Namespace ControlExplorer.C1Carousel
	Public Partial Class LoadOnDemand
		Inherits System.Web.UI.Page
		Private Const TEXT As String = "{0} Vestibulum venenatis faucibus eros, vitae vulputate ipsum tempor ut. Donec ut ligula a metus volutpat sagittis. Duis sodales, lorem nec suscipit imperdiet, sapien metus tempor nibh, dapibus pulvinar lorem lacus molestie lacus. "

		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack OrElse IsCallback Then
				'
				Dim list1 As List(Of ContentCarousel) = GetDataSource("http://lorempixum.com/200/150/city/{0}")

				Dim list2 As List(Of ContentCarousel) = GetDataSource("http://lorempixum.com/300/200/sports/{0}")

				C1Carousel1.DataSource = list1
				C1Carousel1.DataBind()

				C1Carousel2.DataSource = list2
				C1Carousel2.DataBind()
			End If
		End Sub

		Private Function GetDataSource(urlFormatStr As String) As List(Of ContentCarousel)
			Dim list As New List(Of ContentCarousel)()

			For i As Integer = 1 To 10
                list.Add(New ContentCarousel() With { _
                  .Content = String.Format(TEXT, String.Format("{0}.The picture one, ", i.ToString())), _
                  .ImgUrl = String.Format(urlFormatStr, i.ToString()), _
                  .Caption = String.Format("画像 {0}", i.ToString()) _
                })
			Next
			Return list
		End Function
	End Class
End Namespace
