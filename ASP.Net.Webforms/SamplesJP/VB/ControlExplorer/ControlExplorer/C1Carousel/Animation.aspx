﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1Carousel.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
<script id="scriptInit" type="text/javascript">
	function changeProperties() {
		var animationOptions = {
		    queue: $('#chkQueue').is(":checked"),
			easing: $('#selEasing').val()
		};
		$('#<%=C1Carousel1.ClientID%>').c1carousel({
			animation: animationOptions
		});
	}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="300px" Display="1"
		EnableTheming="True">
		<Items>
			<wijmo:C1CarouselItem ID="C1CarouselItem1" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/1" Caption="キャプション 1">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem2" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/2" Caption="キャプション 2">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem3" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/3" Caption="キャプション 3">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem4" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/4" Caption="キャプション 4">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem5" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/5" Caption="キャプション 5">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem6" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/6" Caption="キャプション 6">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem7" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/7" Caption="キャプション 7">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem8" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/8" Caption="キャプション 8">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem9" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/9" Caption="キャプション 9">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem10" runat="server" ImageUrl="http://lorempixum.com/750/300/abstract/10" Caption="キャプション 10">
			</wijmo:C1CarouselItem>
		</Items>
		<PagerPosition>
			<My Left="Right"></My>
			<At Top="Bottom" Left="Right"></At>
		</PagerPosition>
	</wijmo:C1Carousel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
	<div>
		<label for="chkQueue">
			アニメーション効果：
		</label>
		<input id="chkQueue" type="checkbox" />
		<label for="selEasing">
			　イージング：
		</label>
		<select id="selEasing">
			<option value="easeInQuad">easeInQuad</option>
			<option value="easeOutQuad">easeOutQuad</option>
			<option value="easeInOutQuad">easeInOutQuad</option>
			<option value="easeInCubic">easeInCubic</option>
			<option value="easeOutCubic">easeOutCubic</option>
			<option value="easeInOutCubic">easeInOutCubic</option>
			<option value="easeInQuart">easeInQuart</option>
			<option value="easeOutQuart">easeOutQuart</option>
			<option value="easeInOutQuart">easeInOutQuart</option>
			<option value="easeInQuint">easeInQuint</option>
			<option value="easeOutQuint">easeOutQuint</option>
			<option value="easeInOutQuint">easeInOutQuint</option>
			<option value="easeInSine">easeInSine</option>
			<option value="easeOutSine">easeOutSine</option>
			<option value="easeInOutSine">easeInOutSine</option>
			<option value="easeInExpo">easeInExpo</option>
			<option value="easeOutExpo">easeOutExpo</option>
			<option value="easeInOutExpo">easeInOutExpo</option>
			<option value="easeInCirc">easeInCirc</option>
			<option value="easeOutCirc">easeOutCirc</option>
			<option value="easeInOutCirc">easeInOutCirc</option>
			<option value="easeInElastic">easeInElastic</option>
			<option value="easeOutElastic">easeOutElastic</option>
			<option value="easeInOutElastic">easeInOutElastic</option>
			<option value="easeInBack">easeInBack</option>
			<option value="easeOutBack">easeOutBack</option>
			<option value="easeInOutBack">easeInOutBack</option>
			<option value="easeInBounce">easeInBounce</option>
			<option value="easeOutBounce">easeOutBounce</option>
			<option value="easeInOutBounce">easeInOutBounce</option>
		</select>
		&nbsp;&nbsp;<input type="button" value="適用" onclick="changeProperties()" />
	</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは、アニメーションオプションを使用して <strong>C1Carousel</strong> コントロールにアニメーションを適用する方法を紹介します。
    </p>
</asp:Content>
