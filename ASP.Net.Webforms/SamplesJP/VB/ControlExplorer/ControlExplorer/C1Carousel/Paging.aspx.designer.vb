'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace ControlExplorer.C1Carousel

    Partial Public Class Paging

        '''<summary>
        '''C1Carousel1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Carousel1 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1Carousel

        '''<summary>
        '''C1CarouselItem1 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem1 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem2 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem3 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem4 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem4 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem5 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem5 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1Carousel2 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Carousel2 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1Carousel

        '''<summary>
        '''C1CarouselItem7 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem7 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem8 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem8 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem9 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem9 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem10 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem10 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem11 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem11 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1Carousel3 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1Carousel3 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1Carousel

        '''<summary>
        '''C1CarouselItem13 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem13 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem14 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem14 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem15 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem15 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem16 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem16 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem

        '''<summary>
        '''C1CarouselItem17 コントロール。
        '''</summary>
        '''<remarks>
        '''自動生成されたフィールド。
        '''変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        '''</remarks>
        Protected WithEvents C1CarouselItem17 As Global.C1.Web.Wijmo.Controls.C1Carousel.C1CarouselItem
    End Class
End Namespace
