Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1Carousel
	Public Partial Class DataBinding
		Inherits System.Web.UI.Page
		Private Const TEXT As String = "{0} Vestibulum venenatis faucibus eros, vitae vulputate ipsum tempor ut. Donec ut ligula a metus volutpat sagittis. Duis sodales, lorem nec suscipit imperdiet, sapien metus tempor nibh, dapibus pulvinar lorem lacus molestie lacus. "
		Protected Sub Page_Load(sender As Object, e As EventArgs)
			If Not IsPostBack OrElse IsCallback Then
				'
				Dim list1 As List(Of ContentCarousel) = GetDataSource("http://lorempixum.com/200/150/city/{0}")

				Dim list2 As List(Of ContentCarousel) = GetDataSource("http://lorempixum.com/750/300/sports/{0}")

				C1Carousel1.DataSource = list1
				C1Carousel1.DataBind()

				C1Carousel2.DataSource = list2
				C1Carousel2.DataBind()
			End If
		End Sub

		Private Function GetDataSource(urlFormatStr As String) As List(Of ContentCarousel)
			Dim list As New List(Of ContentCarousel)()

			For i As Integer = 1 To 10
                list.Add(New ContentCarousel() With { _
                 .Content = String.Format(TEXT, String.Format("{0}. ", i.ToString())), _
                 .ImgUrl = String.Format(urlFormatStr, i.ToString()), _
                 .Caption = String.Format("画像 {0}", i.ToString()) _
                })
			Next
			Return list
		End Function
	End Class

	Public Class ContentCarousel
		Public Property Content() As String
			Get
				Return m_Content
			End Get
			Set
				m_Content = Value
			End Set
		End Property
		Private m_Content As String

		Public Property ImgUrl() As String
			Get
				Return m_ImgUrl
			End Get
			Set
				m_ImgUrl = Value
			End Set
		End Property
		Private m_ImgUrl As String

		Public Property LinkUrl() As String
			Get
				Return m_LinkUrl
			End Get
			Set
				m_LinkUrl = Value
			End Set
		End Property
		Private m_LinkUrl As String

		Public Property Caption() As String
			Get
				Return m_Caption
			End Get
			Set
				m_Caption = Value
			End Set
		End Property
		Private m_Caption As String
	End Class

End Namespace
