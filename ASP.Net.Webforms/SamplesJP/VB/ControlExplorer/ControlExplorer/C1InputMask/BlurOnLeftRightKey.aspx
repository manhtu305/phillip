﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="BlurOnLeftRightKey.aspx.vb" Inherits=".BlurOnLeftRightKey1" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1InputText ID="C1InputText1" runat="server"></wijmo:C1InputText>
    <wijmo:C1InputMask ID="C1InputMask1" runat="server" BlurOnLeftRightKey="Both" MaskFormat="9999">
    </wijmo:C1InputMask>
    <wijmo:C1InputText ID="C1InputText2" runat="server"></wijmo:C1InputText>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p>
        このサンプルでは <b>BlurOnLeftRightKey</b> プロパティを <b>Both</b>  に設定しています。
        最初のフィールドにキャレットがあるときに左矢印キーを押下すると前のコントロールにフォーカスが移動し、
        最後のフィールドにキャレットがあるときに右矢印キーを押下すると、次のコントロールにフォーカスが移動します。

    </p>
</asp:Content>
