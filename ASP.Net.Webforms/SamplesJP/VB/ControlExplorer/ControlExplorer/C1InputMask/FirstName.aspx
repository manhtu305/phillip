﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputMask_FirstName" Codebehind="FirstName.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<h2>名前</h2>
<wijmo:C1InputMask ID="C1InputMask1" runat="server" Mask=">L|LLLLLLLLL" HidePromptOnLeave="true">
</wijmo:C1InputMask>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
<p>
このサンプルでは、<b>Mask</b> プロパティを使用して、文字入力のみを許可する方法を紹介します。
</p>
<p>
<b>HidePromptOnLeave</b> プロパティを true に設定すると、フォーカスがある場合のみプロンプト文字が表示されます。
</p>
<p>
">L|LLLLLLLLL" というマスクは、最初の文字が大文字のテキスト文字列を表しています。
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

</asp:Content>

