﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputMask_DropDown" Codebehind="DropDown.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<wijmo:C1InputMask ID="C1InputMask1" runat="server" Mask="000-0000" ShowTrigger="true">
<ComboItems>
<wijmo:C1ComboBoxItem Text="100-1000" Value="100-1000" /> 
<wijmo:C1ComboBoxItem Text="200-2000" Value="200-2000" /> 
<wijmo:C1ComboBoxItem Text="123-2909" Value="123-2909" /> 
</ComboItems> 
</wijmo:C1InputMask>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
<p>
<strong>C1InputMask </strong>は、ドロップダウンリストからの値の選択をサポートしています。
</p>
<p>
この場合、<b>ShowTriggers</b> プロパティを true に設定して、<b>ComboItems</b> に項目リストを指定する必要があります。
</p>
<p>
<b>ButtonAlign</b> プロパティを設定すると、トリガーボタンの配置を変更できます。
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

