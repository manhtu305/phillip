﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="BlurOnLastChar.aspx.vb" Inherits=".BlurOnLastChar1" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <wijmo:C1InputMask ID="C1InputMask1" runat="server" MaskFormat="999" BlurOnLastChar="True">
    </wijmo:C1InputMask>
    <wijmo:C1InputText ID="C1InputText1" runat="server"></wijmo:C1InputText>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
      <p>
        このサンプルでは <b>BlurOnLastChar</b> プロパティを <b>true</b> に設定しています。
        最終フィールドで入力を終了すると、フォーカスが次のコントロールに移動します。
    </p>
</asp:Content>
