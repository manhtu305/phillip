﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputMask_Password" Codebehind="Password.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">


<h2>PasswordChar = #</h2>
<label>社会保障番号 (保障付): </label>
<wijmo:C1InputMask ID="C1InputMask1" runat="server" Mask="000-00-0000" PasswordChar="#" HidePromptOnLeave="true">
</wijmo:C1InputMask>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
<p>
<b>C1InputMask</b> では、* や # などのパスワード文字を表示することができます。
このサンプルでは、社会保障番号フィールドとして使用されています。
</p>
<p>
パスワードの文字は <b>PasswordChar</b> プロパティで指定されています。
</p>
<p>
<b>PasswordChar</b> プロパティは、文字列型のプロパティです。
値が 1 文字よりも長い文字列である場合、最初の文字のみが使用されます。
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

