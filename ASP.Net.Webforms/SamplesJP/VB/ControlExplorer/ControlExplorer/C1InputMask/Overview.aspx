﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputMask_Overview" Codebehind="Overview.aspx.vb" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<h2>電話番号</h2>
<wijmo:C1InputMask ID="C1InputMask1" runat="server" Mask="(999) 000-0000" HidePromptOnLeave="true">
</wijmo:C1InputMask>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
<p>
    <strong>C1InputMask </strong>は、テキストベースのマスク入力を可能にする入力用の Web コントロールです。
</p>
<p>
    このサンプルでは、既定の単純な <strong>C1InputMask </strong>を表示します。
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

