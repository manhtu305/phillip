﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.vb" Inherits="ControlExplorer.C1Expander.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script id="scriptInit" type="text/javascript">
	    $(document).ready(function () {
	        jQuery.wijmo.wijexpander.animations.custom1 = function (options) {
	            this.slide(options, {
	                easing: "easeOutExpo",
	                duration: 900
	            });
	        }
	        jQuery.wijmo.wijexpander.animations.custom2 = function (options) {
	            if (options.expand)
	                options.content.show("puff", options, 300);
	            else
	                options.content.hide("explode", options, 300);
	        }
	    });
	</script>
	<h4>
		Expander の既定アニメーション</h4>

	<C1Expander:C1Expander runat="server" ID="C1Expander1">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>
	<br />
	<h4>
		Expander のカスタムアニメーション</h4>
	<C1Expander:C1Expander runat="server" ID="C1Expander2" Animated-Effect="custom1" Expanded="false">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>

	<br />
	<h4>
		jQuery を使用したカスタムアニメーション</h4>
	<C1Expander:C1Expander runat="server" ID="C1Expander3" Animated-Effect="custom2" Expanded="false">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>
	
	<br />
	<h4>
		アニメーションが無効な状態</h4>
	<C1Expander:C1Expander runat="server" ID="C1Expander4" Animated-Disabled="true" Expanded="false">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>

	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、異なるアニメーション効果を使用する方法を紹介しています。
		<strong>Animated </strong>プロパティを使用すると、アニメーション効果を変更できます。</p>
	<p>
		次のエキスパンダーアニメーションが使用されています。</p>
    <ul>
        <li>1 番上のエキスパンダーは、既定の展開／縮小アニメーション効果を使用しています。</li>
        <li>2 番目のエキスパンダーは、イージングを "easeOutExpo" に、継続時間を 900ms に設定したカスタムアニメーションを使用しています。</li>
        <li>3 番目のエキスパンダーは、jQuery の効果を使用したカスタムアニメーションを使用しています。
		展開アニメーションには "puff" が、縮小アニメーションには "explode" が使用されています。
	    </li>
        <li>4 番目のエキスパンダーは、アニメーション効果が無効になっています。
		<strong>Animated.Disabled</strong> プロパティが True に設定されています。 </li>
    </ul>
    <p>下記は、2 つのカスタムアニメーションの実装コードです。</p>
<pre class="controldescription-code">
    &lt;script id="script1" type="text/javascript"&gt;
        $(document).ready(function () {
            jQuery.wijmo.wijexpander.animations.custom1 = function (options) {
                this.slide(options, {
                    easing: "easeOutExpo",
                    duration: 900
                });
            }
            jQuery.wijmo.wijexpander.animations.custom2 = function (options) {
                if (options.expand)
                    options.content.show("puff", options, 300);
                else
                    options.content.hide("explode", options, 300);
            }
        });
    &lt;/script&gt;
</pre>

	<p>アニメーション効果では、次のオプションとオプションパラメータを利用できます。</p>
	<ul>
		<li><strong>expand </strong>- True の場合、コンテンツの要素が展開されます。</li>
		<li><strong>horizontal </strong>- True の場合、<strong>expandDirection</strong> が left や right に設定されているときに、エキスパンダーは横方向に配置されます。</li>
		<li><strong>content </strong>- 展開または縮小されるコンテンツ要素を含む jQuery オブジェクト。</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
