Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ControlExplorer.C1Expander
	Public Partial Class PostbackEvents
		Inherits System.Web.UI.Page
		Protected Sub Page_Load(sender As Object, e As EventArgs)

		End Sub

		Protected Sub C1Expander1_OnExpandedChanged(sender As Object, e As EventArgs)
			Label1.Text = String.Format("(C1Expander1) 展開状態は {0} に変更されました。", C1Expander1.Expanded)
		End Sub

		Protected Sub C1Expander2_OnExpandedChanged(sender As Object, e As EventArgs)
			Label2.Text = String.Format("(C1Expander2) 展開状態は {0} に変更されました。", C1Expander2.Expanded)
		End Sub
	End Class
End Namespace
