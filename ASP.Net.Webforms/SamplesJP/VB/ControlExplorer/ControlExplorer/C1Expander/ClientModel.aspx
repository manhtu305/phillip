﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="ClientModel.aspx.vb" Inherits="ControlExplorer.C1Expander.ClientModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>
<%@ Register Src="../ClientLogger.ascx" TagName="ClientLogger" TagPrefix="ClientLogger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<ClientLogger:ClientLogger ID="ClientLogger1" runat="server" />
	<C1Expander:C1Expander runat="server" ID="C1Expander1">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>このサンプルでは、クライアント側スクリプトを使用して Expander の動作と外観変更する方法を示します。</p>
    <p>このサンプルでは、次のクライアント側オプションが使用されています。</p>
    <ul>
    <li><strong>expanded -</strong> コンテンツパネルの表示状態を指定します。True の場合、コンテンツ要素が表示されます。</li>
    <li><strong>allowExpand -</strong> コントロールが展開可能かどうかを指定します。
        展開／縮小の機能を無効にする場合は、このオプションを False に設定します。</li>
    <li><strong>expandDirection -</strong> コンテンツの展開方向を指定します。
        top、right、bottom、left の値を使用できます。</li>
    </ul>
    <p>使用されるクライアント側イベントは次の通りです：</p>
    <ul>
    <li><strong>beforeCollapse</strong> - コンテンツ領域が縮小される前に発生します。イベントをキャンセルしてコンテンツ領域が縮小されるのを防ぐためには、false を返すか event.preventDefault() を呼び出します。</li>
    <li><strong>beforeExpand</strong> - コンテンツ領域が展開される前に発生します。イベントをキャンセルしてコンテンツ領域が展開されるのを防ぐためには、false を返すか event.preventDefault() を呼び出します。</li>
    <li><strong>afterCollapse</strong> - コンテンツ領域が縮小された後で発生します。</li>
    <li><strong>afterExpand</strong> - コンテンツ領域が展開された後で発生します。</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<p>
		<label>
			expanded プロパティ：
			<input type="checkbox" id="expandedOption" /></label>
	</p>
	<p>
		<label>expandDirection プロパティ：
		<select id="expandDirectionOption">
			<option value="top" selected="selected">上</option>
			<option value="right">右</option>
			<option value="bottom">下</option>
			<option value="left">左</option>
		</select>
		</label>
	</p>
	<p>
		<label>
			allowExpand プロパティ：<input type="checkbox" id="allowExpandOption" /></label>
	</p>
	<p>
		<label>
			collapse イベントの捕捉：<input type="checkbox" id="bindCollapseEvents" /></label>
	</p>
	<p>
		<label>
			expand イベントの捕捉：<input type="checkbox" id="bindExpandEvents" /></label>
	</p>
	<script language="javascript" type="text/javascript">
		$(document).ready(
		function () {
			$("#expandedOption")[0].checked = $("#<%=C1Expander1.ClientID%>").c1expander("option", "expanded");
			$("#allowExpandOption")[0].checked = $("#<%=C1Expander1.ClientID%>").c1expander("option", "allowExpand");
			$("#expandDirectionOption").val($("#<%=C1Expander1.ClientID%>").c1expander("option", "expandDirection"));
			//$("#expander").wijexpander("option", "expandDirection", $("#expandDirectionOption").get(0).value);
			$("#expandedOption").change(function () {
				var expanded = $("#expandedOption")[0].checked;
				$("#<%=C1Expander1.ClientID%>").c1expander("option", "expanded", expanded);
				log.message("expanded プロパティが " + expanded + " に変更されました。");
			})

			$("#allowExpandOption").change(function () {
				var allowExpand = $("#allowExpandOption")[0].checked;
				$("#<%=C1Expander1.ClientID%>").c1expander("option", "allowExpand", allowExpand);
				log.message("allowExpand プロパティ " + allowExpand + " に変更されました。");
			});

			$("#expandDirectionOption").change(function () {
				var selectedDir = $("#expandDirectionOption").val();
				$("#<%=C1Expander1.ClientID%>").c1expander("option", "expandDirection", selectedDir);
				log.message("expandDirection プロパティが " + selectedDir + " に変更されました。");
			})

			$("#bindExpandEvents").change(function () {
				if ($("#bindExpandEvents")[0].checked) {
					$("#<%=C1Expander1.ClientID%>").bind("c1expanderbeforeexpand.sampleExpandEvents", function (e) {
						log.message("beforeExpand イベントが発生しました。");
						/*if (確認("beforeExpand イベントをキャンセルしますか?")) {
						e.preventDefault();
						}*/
					}).bind("c1expanderafterexpand.sampleExpandEvents", function () {
					    log.message("afterExpand イベントが発生しました。");
					});
				} else {
					$("#<%=C1Expander1.ClientID%>").unbind(".sampleExpandEvents");
				}
			});

			$("#bindCollapseEvents").change(function () {
				if ($("#bindCollapseEvents")[0].checked) {
					$("#<%=C1Expander1.ClientID%>").bind("c1expanderbeforecollapse.sampleCollapseEvents", function (e) {
					    log.message("beforeCollapse イベントが発生しました。");
						/*if (確認("beforeCollapse イベントをキャンセルしますか?")) {
						e.preventDefault();
						}*/
					}).bind("c1expanderaftercollapse.sampleCollapseEvents", function () {
					    log.message("afterCollapse イベントが発生しました。");
					});
				} else {
					$("#<%=C1Expander1.ClientID%>").unbind(".sampleCollapseEvents");
				}
			});
		}
		);
	</script>
</asp:Content>
