﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ExpandDirection.aspx.vb" Inherits="ControlExplorer.C1Expander.ExpandDirection" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<style type="text/css" media="all">
		.ui-expander-left.wijmo-wijexpander .ui-expander-header,
		.ui-expander-right.wijmo-wijexpander .ui-expander-header,
		.ui-expander-left.wijmo-wijexpander .ui-expander-header a,
		.ui-expander-right.wijmo-wijexpander .ui-expander-header a {
			width: 32px;
			text-align: center;
			text-overflow:ellipsis;
			overflow:hidden;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Expander:C1Expander runat="server" ID="C1Expander1">
		<Header>
			ヘッダー
		</Header>
		<Content>
			C1Expander コントロールでは、ユーザーが Expander のヘッダーをクリックすることによって、埋め込みまたは外部コンテンツを表示／非表示にすることができます。
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

	<p>
		このサンプルでは、クライアント側で展開するコンテンツ領域の方向を変更する方法を紹介します。
	</p>
	<p>
		<b>expandDirection</b> オプションを使用して展開方向を切り替えることができます。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<script type="text/javascript" language="javascript">
	function setExpandDirection(direction) {
		$("#<%=C1Expander1.ClientID%>").c1expander("option", "expandDirection", direction);
	}
</script>
    <fieldset class="radio">
        <legend>展開方向の選択</legend>
		<label><input type="radio" name="ExpandDirection" value="top" onclick="setExpandDirection('top');" />上</label>　
		<label><input type="radio" name="ExpandDirection" value="right" onclick="setExpandDirection('right');" />右</label>　
		<label><input type="radio" name="ExpandDirection" value="bottom" onclick="setExpandDirection('bottom');" checked="checked" />下</label>　
		<label><input type="radio" name="ExpandDirection" value="left" onclick="setExpandDirection('left');" />左</label>
    </fieldset>
</asp:Content>
