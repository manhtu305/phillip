﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="PostbackEvents.aspx.vb" Inherits="ControlExplorer.C1Expander.PostbackEvents" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<h2>
		UpdatePanel を使用した場合：</h2>
	<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="100" runat="server">
		<ProgressTemplate><div style="margin-top:20px;">UpdatePanel1 をリフレッシュしています...</div></ProgressTemplate>
	</asp:UpdateProgress>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
			<h3>
				<asp:Label ID="Label1" runat="server" Text=""></asp:Label></h3>
			<C1Expander:C1Expander runat="server" ID="C1Expander1" OnExpandedChanged="C1Expander1_OnExpandedChanged">
				<Header>
					ヘッダー
				</Header>
				<Content>
					Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
					Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
					cursus in.
				</Content>
			</C1Expander:C1Expander>
		</ContentTemplate>
	</asp:UpdatePanel>

	<h2>
		UpdatePanel を使用しない場合：</h2>
	<h3>
		<asp:Label ID="Label2" runat="server" Text=""></asp:Label></h3>
	<C1Expander:C1Expander runat="server" ID="C1Expander2" OnExpandedChanged="C1Expander2_OnExpandedChanged">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

	<p>このサンプルでは、​​完全および部分的なページのポストバック中の <strong>C1Expander</strong> コントロールの動作を紹介します。</p>
	<p>このページには、2 つのエキスパンダーコントロールが含まれています。
	1 番目のエキスパンダーは、<strong>UpdatePanel</strong> 内に配置されており、2 番目のエキスパンダーは、<strong>UpdatePanel</strong> の外に配置されています。</p>
	<p><strong> ExpandedChanged</strong> イベントハンドラが追加されると、コンテンツペインは展開または縮小されたたびに、<strong>C1Expander</strong> コントロールがサーバーにポストバックします。
	<strong>ExpandedChanged</strong> イベントを使用せずにポストバックを実行したい場合は、<strong>AutoPostBack </strong>プロパティを True に設定することでできます。
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
