﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ContentUrl.aspx.vb" Inherits="ControlExplorer.C1Expander.ContentUrl" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<style type="text/css" media="all">
		.ui-expander-content 
		{
			height: 400px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Expander:C1Expander runat="server" ID="C1Expander3" ContentUrl="http://www.grapecity.com/tools/">
		<Header>
			GrapeCity Web サイト (ここをクリックして展開／縮小します。)
		</Header>
		<Content>
			
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>このサンプルでは、外部サイトからエキスパンダーのコンテンツパネルにコンテンツを読み込む方法を紹介します。</p>
	<p><strong>ContentUrl </strong>プロパティは、外部コンテンツへの URL を指定します。
	コンテンツが同じ Web サイト上にある場合、URL は絶対パスまたは相対パスのどちらかに設定できます。</p>
    <p>このサンプルでは、<strong>C1Expander</strong> の <strong>ContentUrl</strong> プロパティは "http://www.grapecity.com/tools/" に設定されています。</p>
	<p>コンテンツパネルの高さは、CSS で 400px に設定されています。</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
