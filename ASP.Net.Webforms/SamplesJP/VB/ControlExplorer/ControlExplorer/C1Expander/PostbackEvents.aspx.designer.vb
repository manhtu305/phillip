'------------------------------------------------------------------------------
' <自動生成>
'     このコードはツールによって生成されました。
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。 
' </自動生成>
'------------------------------------------------------------------------------

Namespace ControlExplorer.C1Expander


	Public Partial Class PostbackEvents

		''' <summary>
		''' ScriptManager1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected ScriptManager1 As Global.System.Web.UI.ScriptManager

		''' <summary>
		''' UpdateProgress1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdateProgress1 As Global.System.Web.UI.UpdateProgress

		''' <summary>
		''' UpdatePanel1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected UpdatePanel1 As Global.System.Web.UI.UpdatePanel

		''' <summary>
		''' Label1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Label1 As Global.System.Web.UI.WebControls.Label

		''' <summary>
		''' C1Expander1 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Expander1 As Global.C1.Web.Wijmo.Controls.C1Expander.C1Expander

		''' <summary>
		''' Label2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected Label2 As Global.System.Web.UI.WebControls.Label

		''' <summary>
		''' C1Expander2 コントロール。
		''' </summary>
		''' <remarks>
		''' 自動生成されたフィールド。
		''' 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
		''' </remarks>
		Protected C1Expander2 As Global.C1.Web.Wijmo.Controls.C1Expander.C1Expander
	End Class
End Namespace
