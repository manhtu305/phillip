﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Expander_Overview" CodeBehind="Overview.aspx.vb" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Expander:C1Expander runat="server" ID="Expander1">
		<Header>
			ヘッダー
		</Header>
		<Content>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		このサンプルでは、<strong>C1Expander </strong>コントロールの既定の動作を紹介します。
	</p>
	<p>
		<strong>C1Expander </strong>は、パネル内の埋め込みコンテンツや外部コンテンツを表示／非表示にすることができます。
		コンテンツパネルを縮小する場合は、<strong>Expanded</strong> プロパティを False に設定します。
        <strong>Expanded</strong> プロパティの既定値は True です。
	</p>
</asp:Content>
