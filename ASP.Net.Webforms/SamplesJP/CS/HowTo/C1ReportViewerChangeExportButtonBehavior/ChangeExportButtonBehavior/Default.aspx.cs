﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Text;
using System.Data;
using C1.C1Preview;

using C1.Web.Wijmo.Controls.C1ReportViewer;
using System.Web.UI.HtmlControls;

namespace ChangeExportButtonBehavior
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            C1ReportViewerToolbar toolBar = C1ReportViewer1.ToolBar;
            ((HtmlGenericControl)toolBar.Controls[1]).Style["display"] = "none"; // 元の保存(エクスポート) ボタンを隠します。
            // 独自のエクスポートボタンを追加します。
            HtmlGenericControl saveButton = new HtmlGenericControl("button");
            saveButton.Attributes["title"] = "カスタムエクスポート";
            saveButton.Attributes["class"] = "custom-export";
            saveButton.InnerHtml = "カスタム保存";
            toolBar.Controls.AddAt(1, saveButton);
        }
    }
}