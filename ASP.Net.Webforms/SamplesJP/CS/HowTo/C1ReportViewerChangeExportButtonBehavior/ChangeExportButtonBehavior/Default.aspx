﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ChangeExportButtonBehavior.Default" %>

<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1ReportViewer" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>カスタムツールバーサンプル</title>
</head>
<body>
    <form id="form1" runat="server">
	<script language="javascript" type="text/javascript">
	    $(document).ready(function () {
	        $(".custom-export").button({
	            icons: {
	                primary: "ui-icon-gear"
	            },
	            text: false
	        }).click(function () {
	            exportToFile();
	            return false;
	        });
	    });

	    function exportToFile() {
	        var viewerSelector = "#<%=C1ReportViewer1.ClientID%>";
	        var docStatus = $(viewerSelector).c1reportviewer("option", "documentStatus");
	        if (docStatus.isGenerating) {
	            alert("エクスポートは実行できません。文書はまだ生成されていません。");
	        }
	        else {
	            document.getElementById("customsaveframe").src = "CustomSaveHttpHandler.ashx?documentKey=" + docStatus.documentKey + "&exportFormat=Open%20XML%20Excel&exportFormatExt=xlsx";
	        }
	        return false;
	    }
    </script>

        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

        <cc1:C1ReportViewer ID="C1ReportViewer1" runat="server"  ReportsFolderPath="~/tempReports"
            Width="850px" Height="650px" 
            Zoom="100%"
            FileName="~/C1ReportXML/CommonTasks.xml"
            />

            <iframe id="customsaveframe" style="width:1px;height:1px;"></iframe>
    </form>
</body>
</html>
