<%@ WebHandler Language="C#" Class="C1ReportHttpHandler" %>
using System;
using System.Web;
using System.Collections;
using System.IO;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using C1.Web.Wijmo.Controls.C1ReportViewer.ReportService;

public class C1ReportHttpHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {        
        IC1WebReportService reportService = C1WebReportServiceHelper.MakeHelper((string)context.Request.Params["documentKey"]);
        // レポートをファイルにエクスポートします。
		string url = reportService.ExportToFile((string)context.Request.Params["documentKey"], context.Request.Params["exportFormat"],"");

		// url をファイルのパスに変換します。
		string pathSource = url.Substring(url.IndexOf("tempReports/"));
		pathSource = context.Server.MapPath(pathSource);
		        
        using (FileStream fsSource = new FileStream(pathSource, FileMode.Open, FileAccess.Read))
        {
            // ソースファイルをバイトアレイに読み取ります。
            byte[] bytes = new byte[fsSource.Length];
            int numBytesToRead = (int)fsSource.Length;
            int numBytesRead = 0;
            while (numBytesToRead > 0)
            {
                // 読み取り処理は、0 〜 numBytesToRead 間の任意値を返します。
                int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);
                // ファイルの最後まで達した際に、ブレークします。
                if (n == 0)
                    break;
                numBytesRead += n;
                numBytesToRead -= n;
            }
            // バッフアストリームからすべての出力コンテンツを削除します。 
            context.Response.Clear();
            // ブラウザのダウンロードダイアログのデフォルトのファイル名を指定した 
            // 出力ストリームにHTTPヘッダーを追加します。
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + (string)context.Request.Params["documentKey"] + "." + context.Request.Params["exportFormatExt"]);
            // コンテンツの長さ（ファイルサイズ）が含まれている出力ストリームに 
            // HTTPヘッダーを追加します。これによって、ブラウザは転送されるデータの量を判定することができます。 
            context.Response.AddHeader("Content-Length", bytes.Length.ToString());
            // 出力ストリームの HTTP MIME タイプを設定します。
            context.Response.ContentType = "application/octet-stream";
            // データをクライアントまで書き込みます。 
            context.Response.BinaryWrite(bytes);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}



                