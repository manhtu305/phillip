﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace PrintWithoutPreview {
    
    
    public partial class Default {
        
        /// <summary>
        /// Head1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlHead Head1;
        
        /// <summary>
        /// form1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// ScriptManager1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManager1;
        
        /// <summary>
        /// C1ReportViewer1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1ReportViewer.C1ReportViewer C1ReportViewer1;
    }
}
