﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PrintWithoutPreview.Default" %>

<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1ReportViewer" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>カスタム印刷のサンプル</title>
</head>
<body>
	<form id="form1" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>    
	<script language="javascript" type="text/javascript">
	    $(document).ready(function () {
	        $(".custom-print").button({
	            icons: {
	                primary: "ui-icon-gear"
	            }
	        }).click(function () {
	            var viewerSelector = "#<%=C1ReportViewer1.ClientID%>";
	            var docStatus = $(viewerSelector).c1reportviewer("option", "documentStatus");
	            if (docStatus.isGenerating) {
	                alert("印刷できません。文書は生成されませんでした。");
	            } else {
	                var customPrintFrame = document.getElementById("customprintframe"),
					doc = customPrintFrame.contentWindow.document;
	                var printOptions = {
	                    rangeSubset: 0, /*(0 - all/ 1 - odd/ 2 - even)*/
	                    reversePages: false,
	                    dpi: 96,
	                    width: 800,
	                    height: 600
	                };
	                $("#print_status").html("印刷レイアウトをロードしています");
	                $("#print_status").show();
	                $(viewerSelector).c1reportviewer("loadPreviewHtml", printOptions, function (data) {
	                    doc.body.innerHTML = data;
	                    // 全ての画像がロードするまで待ちます。
	                    $imgs = $(doc.body).find("img");
	                    _imagesCount = $imgs.length;
	                    for (var i = 0; i < $imgs.length; i++) {
	                        var im = $imgs[i];
	                        if (im.complete) {
	                            // 画像は既にロードされました。
	                            onImageLoad();
	                        }
	                        else {
	                            $(im).bind("load", onImageLoad);
	                        }
	                    }
	                });

	                function onImageLoad() {
	                    _imagesCount--;
	                    $("#print_status").html("(印刷の準備しています) ロードする画像：" + _imagesCount);
	                    if (_imagesCount == 0) {
	                        $("#print_status").hide();
	                        customPrintFrame.contentWindow.focus();
	                        customPrintFrame.contentWindow.print();
	                    }
	                }
	            }
	            return false;
	        });
	    });
	    var _imagesCount = 0;

	</script>
	<div id="print_status" style="display:none; position: absolute; background-color: #ffffff; text-align: center; z-index:10; top: 10px; left: 100px; width: 270px; height: 30px; border: 1px solid gray;"></div>	
		<cc1:C1ReportViewer ID="C1ReportViewer1" runat="server"  ReportsFolderPath="~/tempReports"
			Width="850px" Height="650px" 
			Zoom="100%"
			FileName="~/C1ReportXML/CommonTasks.xml"
			/>
	
	<iframe id="customprintframe" style="width:1px;height:1px;"><div id="content">!</div></iframe>            
	</form>
</body>
</html>
