﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Text;
using System.Data;
using C1.C1Preview;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using System.Web.UI.HtmlControls;

namespace PrintWithoutPreview
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            C1ReportViewerToolbar toolBar = C1ReportViewer1.ToolBar;
            ((HtmlGenericControl)toolBar.Controls[0]).Style["display"] = "none"; // 元の印刷ボタンを隠します。		
            // カスタム印刷ボタンを追加します。
            HtmlGenericControl printButton = new HtmlGenericControl("button");
            printButton.Attributes["title"] = "カスタム印刷";
            printButton.Attributes["class"] = "custom-print";
            printButton.InnerHtml = "カスタム印刷";
            toolBar.Controls.AddAt(0, printButton);
        }
    }
}